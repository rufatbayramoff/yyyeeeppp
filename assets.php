<?php
/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@webroot', __DIR__ . '/frontend/web');
Yii::setAlias('@web', '/');

return [
    // Adjust command/callback for JavaScript files compressing:
    'jsCompressor'  => 'java -jar tools/compiler.jar --js {from} --js_output_file {to}',
    // Adjust command/callback for CSS files compressing:
    'cssCompressor' => 'java -jar tools/yuicompressor.jar --type css {from} -o {to}',
    // The list of asset bundles to compress:
    'bundles'       => [
        \yii\web\JqueryAsset::class,
        \yii\web\YiiAsset::class,
        \yii\widgets\ActiveFormAsset::class,
        \yii\validators\ValidationAsset::class,
        \common\modules\translation\assets\TranslateFunctionTAsset::class,
        \frontend\assets\SocialButtonsAsset::class,
        \yii\bootstrap\BootstrapAsset::class,
        \yii\bootstrap\BootstrapPluginAsset::class,
        \frontend\assets\AppAsset::class,
        \kartik\base\WidgetAsset::class,
        \kartik\select2\Select2Asset::class,
        \kartik\typeahead\TypeaheadAsset::class,
        \kartik\typeahead\TypeaheadHBAsset::class,
        \yii\authclient\widgets\AuthChoiceAsset::class,
        \yii\authclient\widgets\AuthChoiceStyleAsset::class,
        \frontend\assets\CatalogAsset::class,
        \frontend\widgets\assets\CategoriesButtonAsset::class,
        \frontend\assets\LightboxAsset::class,
        \frontend\assets\SwiperAsset::class,
        \frontend\widgets\assets\ReviewStarsAssets::class,
        \frontend\assets\polyfill\ArrayFindAsset::class
        //'frontend\components\angular\AngularAsset'
    ],
    // Asset bundle for compression output:
    'targets'       => [
        'all' => [
            'class'    => 'yii\web\AssetBundle',
            'basePath' => '@webroot/assets',
            'baseUrl'  => '@web/assets',
            'js'       => 'js/all-{hash}.js',
            'css'      => 'css/all-{hash}.css',
        ],
    ],
    // Asset manager configuration:
    'assetManager'  => [
        'basePath' => '@webroot/assets',
        'baseUrl'  => '@web/assets',
    ],
];