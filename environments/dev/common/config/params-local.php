<?php
if (isset($_SERVER['HTTP_HOST']) && (strpos($_SERVER['HTTP_HOST'], 'backend') !== 0)) {
    $host = $_SERVER['HTTP_HOST'];
} else {
    if (file_exists('/vagrant/conf/hostid.php')) {
        $hostId = include '/vagrant/conf/hostid.php';
    } else {
        $hostId = "";
    }
    $host = 'ts.h' . $hostId . '.tsdev.work';
}
$scheme = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] . '://' : 'https://';
$url    = $scheme . $host;

return [
    'httpScheme'                       => 'https://',
    'staticUrl'                        => $url . "/static",
    'server'                           => $url,
    'siteUrl'                          => $url,
    'apiUrl'                           => $url . '/api',
    'backendServer'                    => $scheme . 'backend.h' . $hostId . '.tsdev.work',
    'braintree_default_merchant_eur'   => 'ts_eur',
    /* 'braintree_threedsecure_enable' => false, */
    /* Facebook Ps print application id */
    'facebook_ps_print_application_id' => '246622042446313',
    'facebook_ps_secrect'              => '24d2f79ed73f4fe98b8a0d4853562b54',
];