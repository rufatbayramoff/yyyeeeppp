<?php
$config = [
    'components' => [
        'formatter' => [
            'defaultTimeZone' => 'UTC'
        ],
        'request'   => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '6Gu3_WOrdT5OtLAf-8VnIyhFVLGz3q8M',
        ],
        'assetManager' => [
           'bundles' => require_once(__DIR__ . '/assets-dev.php'),
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['10.0.2.2', '::1', '10.102.0.132', '10.102.0.154']
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
