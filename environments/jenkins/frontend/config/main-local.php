<?php
$config = [
    'components' => [
        'formatter' => [
            'defaultTimeZone' => 'UTC'
        ],
        'request'   => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '6Gu3_WOrdT5OtLAf-8VnIyhFVLGz3q8M',
        ],
    ],
    'modules'    => [
        'captcha' => [
            'isDisabled' => true
        ]
    ]

];
return $config;
