<?php
if (file_exists('/vagrant/conf/hostid.php')) {
    $hostId = include '/vagrant/conf/hostid.php';
} else {
    $hostId = "";
}
//$hostId = include '/vagrant/conf/hostid.php';
return [
    'modules'    => [
        'intlDomains' => [
            'class'               => \common\modules\intlDomains\IntlDomainsModule::class,
            'mainDomain'          => 'ts.h' . $hostId . '.tsdev.work',
            'detectLanguage'      => false,
            'allowedDomains'      => [
                'ts.h' . '.tsdev.work',
                'ts.h' . $hostId . '.tsdev.work',
                'fr.h' . $hostId . '.tsdev.work',
                'cn.h' . $hostId . '.tsdev.work',
                'ru.h' . $hostId . '.tsdev.work',
                'couk.h' . $hostId . '.tsdev.work',
                'de.h' . $hostId . '.tsdev.work',
            ],
            'langDomainsMap'      => [
                'en-US' => 'ts.h' . $hostId . '.tsdev.work',
                'en-GB' => 'couk.h' . $hostId . '.tsdev.work',
                'fr'    => 'fr.h' . $hostId . '.tsdev.work',
                'zh-CN' => 'cn.h' . $hostId . '.tsdev.work',
                'ru'    => 'ru.h' . $hostId . '.tsdev.work',
                'de'    => 'de.h' . $hostId . '.tsdev.work',
            ],
            'defaultLocationsMap' => [
                'fr.h' . $hostId . '.tsdev.work'   => '88.190.229.170',
                'cn.h' . $hostId . '.tsdev.work'   => '218.107.132.66',
                'ru.h' . $hostId . '.tsdev.work'   => '195.34.27.1',
                'couk.h' . $hostId . '.tsdev.work' => '185.86.151.11',
                'de.h' . $hostId . '.tsdev.work'   => '89.246.64.51'
            ]
        ],
    ],
    'components' => [
        'db'             => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'mysql:host=localhost;dbname=yii2advanced',
            'username' => 'root',
            'password' => 'mypass',
            'charset'  => 'utf8',
        ],
        'mailer'         => [
            'class'            => 'yii\swiftmailer\Mailer',
            'viewPath'         => '@common/mail',
            'useFileTransport' => false,
            'transport'        => [
                'class' => 'Swift_SmtpTransport',
                'host'  => '127.0.0.1',
                'port'  => '1025',  // mailcatcher
            ],
        ],
    ],
];

