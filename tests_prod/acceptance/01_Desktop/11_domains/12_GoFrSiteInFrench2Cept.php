<?php 

$I = new AcceptanceTester($scenario);
// 12. If the Russian user opens the com domain anonymously, and in the browser language list the default language is Russian, then we switch to ru.

// 12. Если русский пользователь открывает домен com анонимно, и в списке языков браузере язык по умолчанию русский, то перебрасываем на ru. 
// Потенциально на любой странице может появиться редирект.
$I->am('Anonymous user or search spider');
$I->wantTo('go to treatstock.com in the Russian browser');
$I->expect('go to ru.treatstock.com');

$I->setLanguage("ru_RU");

$I->amOnUrl('https://www.treatstock.com');
$I->waitForText('Войти', 10);
$I->seeFullUrlEquals('https://ru.treatstock.com/');
$I->setLanguage("en");

