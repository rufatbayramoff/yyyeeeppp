<?php 

$I = new AcceptanceTester($scenario);
// 2. An unauthorized user on treatstock.com clearly indicates the location goes to treatstock.fr, the location is not saved.
// 2. Неавторизованный пользователь на treatstock.com явно указывает локацию, переходит на treatstock.fr, локация не сохраняться.
$I->am('Anonymous user or search spider');
$I->wantTo('specify the location and go to treatstock.fr');
$I->expect("the location is not saved"); 

$I->amOnUrl('https://www.treatstock.com/');
// $I->seeFullUrlEquals('https://www.treatstock.com/');
// $I->see('Search & compare manufacturing services worldwide');
// $I->click('.ts-user-location', '.navbar-header');
$I->waitForElement('button[title="Shipping Address"]', 10);
$I->click('button[title="Shipping Address"]');
// $I->waitForElement('#userlocator-address', 10);
$I->waitForElement('input[placeholder="Enter a location"]', 10);
// $I->fillField('#userlocator-address','Madrid Spain');
$I->fillField('input[placeholder="Enter a location"]','Madrid Spain'); // В дальнейшем возможно придется переделать с учетом надписи на французском языке.
$I->wait(2);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(3);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->waitForText('Madrid', 10);

$I->amOnUrl('https://www.treatstock.fr/');
$I->wait(3);
$I->seeFullUrlEquals('https://www.treatstock.fr/');

$I->see('Se connecter');
$I->dontSee('Madrid'); // $I->see('Kazan');

