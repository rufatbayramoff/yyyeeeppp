<?php 

$I = new AcceptanceTester($scenario);
// 3. The user sign in treatstock.com goes to the settings tab change the language to fr. There is a redirect on the domain name fr the user is automatically authorized.
// 3. Пользователь на treatstock.com авторизуется, переходит во вкладку настройки, меняет язык на fr. Происходит редирект на домен fr, при этом пользователь автоматически авторизован.
$I->am('Authorized user'); 
$I->wantTo('change the language to Français at the Profile settings tab');
$I->expect("user autorization in domain fr"); 

$I->amOnUrl('https://www.treatstock.com/');

// $I->waitForElement('div.findps-hero__btn-find', 10);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','test20190901@mail.ru');
$I->fillField('#loginform-password','dLja6Testov');
$I->click('Sign in', '.modal-body');

$I->waitForText('test201909', 10, 'span.header-bar__username'); // $I->see('test20190901');

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);	

$I->click('Profile'); // $I->click('a[title="Profile"]'); // $I->click('a[href="/user/profile"]');
$I->wait(2);
$I->click('Settings');
$I->wait(2);
// ---------------------------------------
// Смена языка через n-й элемент:
   $I->click('#select2-settingsform-current_lang-container'); // $I->click('span[title="English"]');
   $I->wait(1);
   $I->click('li.select2-results__option:nth-child(5)'); // Français
   $I->wait(2);
// ---------------------------------------
$I->click('Save Changes');
$I->wait(5);
$I->seeFullUrlEquals('https://www.treatstock.fr/my/settings');
$I->see('Enregistrer les modifications'); 
$I->wait(5);
$I->see('test201909'); // $I->see('test20190901');
// -------------------
// // Switch to English:
// $I->click('Paramètres');
// $I->wait(2);	
// $I->click('#select2-settingsform-current_lang-container');
// $I->wait(1);
// $I->click(['css' => 'li.select2-results__option:nth-child(1)']); // English
// $I->wait(2);  
// $I->click('Enregistrer les modifications'); // $I->click('Save Changes');
// $I->wait(5);
// -------------------
$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Se déconnecter');
$I->waitForText('Se connecter', 5); 
// $I->see('Se connecter');

// ---------------
// $I->click('Sign Out');
// $I->waitForText('Sign in', 5); 
// $I->see('Sign in');
