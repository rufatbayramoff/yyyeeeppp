<?php 

$I = new AcceptanceTester($scenario);
// 5.1. Authorized user on treatstock.com is in the cart (he added something to the shopping cart).
// Then he opened the site http://treatstock.fr in the parallel tab, the shopping cart is reset (will be empty).
// If the user switched the language at the bottom, the basket should not be reset.

// 5.1. Авторизованный пользователь на treatstock.com находится в корзине (добавлял что-то в корзину). 
// Открыл в параллельной вкладке сайт http://treatstock.fr - в этой вкладке корзина будет пустая.
// Если переключил язык через переключатель языка внизу, корзина сбрасываться не должна.
$I->am('Authorized user'); 
$I->wantTo('add something to the shopping cart, change domain and change the language at the bottom of the page');
$I->expect("shopping cart will not be empty"); 

$I->amOnUrl('https://www.treatstock.com');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->fillField('#loginform-email','test20190901@mail.ru');
$I->fillField('#loginform-password','dLja6Testov');
$I->click('Sign in', '.modal-body');
$I->wait(5);
$I->waitForText('test201909', 10, 'span.header-bar__username'); // $I->see('test20190901');

$I->amOnUrl('https://www.treatstock.com/3d-printable-models/587-star-saltsugar-shaker');
$I->wait(3);

$I->scrollTo(['css' => 'h3.model-printers-title__text'], 0, 0); // Прокрутить страницу до "Select 3D Print Service".
$I->see('Buy');
$I->wait(10);
$I->click('Buy'); // Добавить в корзину.
$I->wait(5);
$I->openNewTab();
$I->wait(5);
$I->amOnUrl('http://treatstock.fr'); // Изменить домен в новой вкладке.
$I->wait(5);

// Переключение языка внизу страницы:
$I->scrollTo(['css' => 'div.footer__lang'], 0, 0); // Прокрутить страницу до нижнего выбора языка.
$I->click(['css' => '.footer__lang__drop > div:nth-child(1) > a:nth-child(1)']); // Нажать на выбор языка
$I->wait(1);
$I->click(['css' => '.open > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)']); // Выбрать English
$I->wait(5);

// ВАРИАНТЫ для обновления страницы (корзины):
// $I->pressKey('body', \Facebook\WebDriver\WebDriverKeys::F5);
// $I->amOnUrl('https://www.treatstock.com/store/delivery');
// $I->click('li.header-bar__cart'); // Click shopping cart

$I->click('li.header-bar__cart'); // Click shopping cart
$I->wait(5);
$I->seeFullUrlEquals('https://www.treatstock.com/store/delivery');
$I->dontSee('Your shopping cart is empty', ['css' => 'body h2']);
$I->wait(1);

$I->closeTab();
$I->wait(5);
$I->reloadPage();
$I->wait(5);

// Очистить корзину
$I->scrollTo(['css' => 'a[href="/store/cart/clear"]'], 0, 0);
$I->click('Empty cart');
$I->wait(5);

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 

