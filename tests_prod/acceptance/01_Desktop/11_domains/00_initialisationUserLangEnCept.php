<?php 

$I = new AcceptanceTester($scenario);
// User test20190901@mail.ru on treatstock.com is authorized, changes language to English for passing tests.
// Пользователь test20190901@mail.ru на treatstock.com авторизуется, изменяет язык на English для прохождения тестов.
$I->am('Authorized user'); 
$I->wantTo('change the language to Français at the Profile settings tab');
$I->expect("user autorization in domain fr"); 

$I->amOnUrl('https://www.treatstock.com/');

// $I->waitForElement('div.findps-hero__btn-find', 10);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','test20190901@mail.ru');
$I->fillField('#loginform-password','dLja6Testov');
$I->click('Sign in', '.modal-body');

$I->waitForText('test201909', 10, 'span.header-bar__username'); // $I->see('test20190901');

// ---------------------------------------
// Смена языка наверху справа:
$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('English');
$I->waitForText('English', 10); // УТОЧНИТЬ правильность и целесообразность!
// ---------------------------------------

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);	

// ---------------
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
$I->see('Sign in');
