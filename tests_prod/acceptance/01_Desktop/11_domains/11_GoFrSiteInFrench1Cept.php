<?php 

$I = new AcceptanceTester($scenario);
// 11. If we open the domain Fr in the English browser, we do not redirect to the domain com. Site in French.
// 11. Если в английском браузере открываем домен Fr, то не производим переадресацию на домен com. Сайт выводим на французском языке.
$I->am('Anonymous user or search spider');
$I->wantTo('go to treatstock.fr in the English browser');
$I->expect("Site in French."); 

$I->amOnUrl('https://www.treatstock.com/');
$I->waitForText('Sign in', 10); 
$I->amOnUrl('https://www.treatstock.fr/');
$I->waitForText('Se connecter', 10);
$I->seeFullUrlEquals('https://www.treatstock.fr/'); 


