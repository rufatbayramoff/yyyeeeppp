<?php 

$I = new AcceptanceTester($scenario);
// 7. Changing the language in the user profile causes the domain to change. The location is saved from the profile.

// 7. Изменение языка в профиле пользователя приводит к изменению домена. Локация сохраняется из профиля.
$I->am('Authorized user'); 
$I->wantTo('Change the language in the user profile');
$I->expect("The domain will change. The location is saved from the profile."); 

$I->amOnUrl('https://www.treatstock.com');

// $I->waitForElement('div.findps-hero__btn-find', 10);
// ----------
$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->fillField('#loginform-email','test20190901@mail.ru');
$I->fillField('#loginform-password','dLja6Testov');
$I->click('Sign in', '.modal-body');
$I->waitForText('test201909', 10, 'span.header-bar__username'); // $I->see('test20190901');
// ----------
$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);	
$I->click('a[title="Profile"]'); // $I->click('Profile'); // $I->click('a[href="/user/profile"]'); // $I->click(['css' => 'span.header-bar__auth-links-item:nth-child(2) > a:nth-child(1)']);
// $I->wait(2);
// $I->see('Shipping location');
$I->waitForText('Shipping location', 10);
// ----------
// Выбор локации в профиле:
$I->click('button[title="Shipping Address"]', '.user-profile__location');
$I->waitForElement('input[placeholder="Enter a location"]', 10); // $I->waitForElement('#userlocator-address', 10);
$I->fillField('input[placeholder="Enter a location"]','Madrid Spain'); // В дальнейшем возможно придется переделать с учетом надписи на французском языке.
$I->wait(2);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(3);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->wait(5);
// ----------
$I->waitForText('Madrid', 10);
$I->click('Save Changes'); 
$I->wait(5);
$I->click('Settings');
$I->wait(2);
// ---------------------------------------
// Смена языка в профиле:
$I->click('#select2-settingsform-current_lang-container');
$I->wait(1);
// $I->click('Français'); // Не работает!
// $I->click('Français', '.select2-results__option'); // Français // Не работает!
// $I->click('Français', 'li.select2-results__option'); // Français // Не работает!
// $I->click('Français', ['css' => 'li.select2-results__option']); // Français // Не работает!
// $I->click(['css' => 'li.select2-results__option:nth-child(5)']); // Français // Работает!
$I->click('li.select2-results__option:nth-child(5)'); // Français // Работает!
$I->wait(2);   
// ---------------------------------------
// Смена языка наверху справа:
// $I->click('.t-header-bottom__lang');
// $I->wait(1);
// $I->click('Français');
// ---------------------------------------
$I->click('Save Changes'); // $I->click('Enregistrer les modifications');
$I->waitForText('Enregistrer les modifications', 15);
$I->seeFullUrlEquals('https://www.treatstock.fr/my/settings');
// ----------
$I->see('test201909'); // $I->see('test20190901');
$I->see('Madrid','.navbar-header');
// -------------------
// Switch to English:
$I->click('#select2-settingsform-current_lang-container');
$I->wait(1);
$I->click('li.select2-results__option:nth-child(1)'); // English
$I->wait(2);  
$I->click('Enregistrer les modifications'); // $I->click('Save Changes');
$I->wait(5);
// -------------------
$I->seeFullUrlEquals('https://www.treatstock.com/my/settings');
// -------------------
// Switch to English (2-й вариант):
// $I->click(['css' => '.header-bottom__lang > div:nth-child(1) > a:nth-child(1)']); // English
// $I->click(['css' => '.open > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)']);
// ----------
$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
// $I->see('Sign in');
// ----------
// ----------
// deleteAllCookies:
$I->amOnUrl('https://www.treatstock.com/robots.txt');
$I->deleteAllCookies();
$I->amOnUrl('https://www.treatstock.fr/robots.txt');
$I->deleteAllCookies();
// ----------
