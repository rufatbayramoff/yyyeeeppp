<?php 

$I = new AcceptanceTester($scenario);
// 11. If we open the domain Fr in the Russian browser, we do not redirect to the domain Ru. Site in French.
// 11. Если в русском браузере открываем домен Fr, то переадресации не производим на домен ru. Сайт выводим на французском языке.
$I->am('Anonymous user or search spider');
$I->wantTo('go to treatstock.fr in the Russian browser');
$I->expect("Site in French.");

$I->setLanguage("ru_RU");

$I->amOnUrl('https://www.treatstock.fr');
$I->waitForText('Se connecter', 10);
$I->seeFullUrlEquals('https://www.treatstock.fr/');
$I->setLanguage("en");

