<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('After click button Sing in user autorization in domain fr');
$I->expect("user autorization in domain fr"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');
$I->wait(5);
$I->see('tkenot');

$I->amOnUrl('https://www.treatstock.fr/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->see('Se connecter');

$I->click('Se connecter');
$I->wait(5);
$I->see('tkenot');

$I->wait(1);

$I->see('English');

$I->wait(3);
$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
$I->see('Sign in');