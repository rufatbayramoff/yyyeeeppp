<?php 

$I = new AcceptanceTester($scenario);
// 8. An unauthorized user opens the treatstock.fr domain, specifies the location. Then he goes to treatstock.com, the location is saved.
// 8. Неавторизованный пользователь открывает домен treatstock.fr, указывает локацию. Потом он переходит на treatstock.com, локация сохраняется.
$I->am('Anonymous user or search spider');
$I->wantTo('go to treatstock.fr and specify the location and go to treatstock.com');
$I->expect("the location is saved"); 

$I->amOnUrl('https://www.treatstock.fr/');
$I->waitForText('Se connecter', 10);

$I->click('button[title="Adresse de livraison"]'); // $I->click('.ts-user-location', '.navbar-header');
$I->waitForElement('input[placeholder="Enter a location"]', 10); // $I->waitForElement('#userlocator-address', 10);
// $I->fillField('#userlocator-address','Lège-Cap-Ferret');
$I->fillField('input[placeholder="Enter a location"]','Lège-Cap-Ferret'); // В дальнейшем возможно придется переделать с учетом надписи на французском языке.
$I->wait(2);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(3);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->waitForText('Lège-Cap-Ferret', 10); // $I->waitForText('Lège-Cap-Ferret', 10, 'button[title="Shipping Address"]');

$I->amOnUrl('https://www.treatstock.com/');
$I->wait(5);

$I->see('Sign in');
$I->see('Lège-Cap-Ferret'); // $I->see('Lège-Cap-Ferret','.navbar-header');

