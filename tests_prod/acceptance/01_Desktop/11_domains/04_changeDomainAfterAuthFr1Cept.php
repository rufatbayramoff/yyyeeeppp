<?php 

$I = new AcceptanceTester($scenario);
// 4. The user sign in treatstock.fr goes to treatstock.com the user is automatically authorized.
// 4. Пользователь на treatstock.fr авторизуется. Переходит на treatstock.com авторизация сохраняется.
$I->am('Authorized user'); 
$I->wantTo('sign in treatstock.fr');
$I->expect("user autorization in domain treatstock.com"); 

$I->amOnUrl('https://www.treatstock.fr/');

// $I->waitForElement('div.findps-hero__btn-find', 10);

$I->click('Se connecter');
$I->wait(1);
// $I->see('Se rappeler de moi');
$I->see('Se rappeler');
$I->fillField('#loginform-email','test20190901@mail.ru');
$I->fillField('#loginform-password','dLja6Testov');
// $I->click('Se connecter', '.modal-body');
$I->click('button[name="login-button"]');

$I->waitForText('test201909', 10, 'span.header-bar__username'); // $I->see('test20190901');
// -------------------------------------
// Смена языка в правом верхнем углу:
$I->click('#current-lang');
$I->wait(1);
$I->click('li.item-lang:nth-child(1)'); // English
$I->wait(5);
// -------------------------------------
$I->seeFullUrlEquals('https://www.treatstock.com/');
// $I->see('English'); // Избыточная проверка.

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out'); // Sign Out. Se déconnecter.
$I->waitForText('Sign in', 5);
// $I->see('Sign in');
