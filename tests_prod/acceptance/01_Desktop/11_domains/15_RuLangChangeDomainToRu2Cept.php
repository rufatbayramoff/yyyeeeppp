<?php 

$I = new AcceptanceTester($scenario);
// 13. An authorized user (with the language Ru installed in the profile) opens the domain .com, we move it to the domain Ru.

// 15. Авторизованный пользователь с установленным в профиле языком ru открывает домен .com, перебрасываем на домен ru.
$I->am('Authorized user'); 
$I->wantTo('set the Russian language in the user profile, go to ".com"');
$I->expect('The domain will be .Ru'); 

$I->amOnUrl('https://www.treatstock.com');

$I->waitForText('Sign in', 10); 
// ----------
$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->fillField('#loginform-email','test20190901@mail.ru');
$I->fillField('#loginform-password','dLja6Testov');
$I->click('Sign in', '.modal-body');
$I->waitForText('test201909', 10, '.header-bar__username'); // $I->see('test20190901');
// ----------
// $I->moveMouseOver('.header-bar__avatar');
// $I->wait(2);	
// $I->click('Profile'); 
// $I->wait(2);
// ----------
// ---------------------------------------
// $I->click('Settings');
// $I->wait(2);
// $I->click('#select2-settingsform-current_lang-container');
// $I->wait(1);
// $I->click(['css' => 'li.select2-results__option:nth-child(2)']); // Русский
// $I->wait(2);   
// ---------------------------------------
// $I->click('Save Changes'); // $I->click('Enregistrer les modifications');
// $I->wait(5);
// $I->see('Сохранить изменения');
// ----------
// ---------------------------------------
// Смена языка наверху справа:
$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('Русский');
$I->waitForText('Русский', 10); // УТОЧНИТЬ правильность и целесообразность!
// ---------------------------------------
// $I->wait(5);
// --------------------------------------------------------------------------------
$I->amOnUrl('https://www.treatstock.com');
// --------------------------------------------------------------------------------
$I->wait(5);
// ----------
$I->waitForText('Русский', 10);
$I->seeFullUrlEquals('https://ru.treatstock.com/');
// ----------
// $I->moveMouseOver('.header-bar__avatar');
// $I->wait(2);	
// $I->click('Профиль');
// $I->wait(2);
// ----------
// ---------------------------------------
// $I->click('Настройки');
// $I->wait(2);
// $I->click('#select2-settingsform-current_lang-container');
// $I->wait(1);
// $I->click(['css' => 'li.select2-results__option:nth-child(1)']); // English
// $I->wait(2); 
// $I->click('Сохранить изменения'); // $I->click('Save Changes');
// $I->wait(5);  
// ---------------------------------------
// ---------------------------------------
// Смена языка наверху справа:
$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('English');
// ---------------------------------------
// ----------
$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5); 
// ----------
