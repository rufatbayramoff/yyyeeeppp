<?php 

$I = new AcceptanceTester($scenario);
// 10. The domain takes precedence over the location in the language definition.
// 10. Домен имеет приоритет над локацией в определении языка.
$I->am('Anonymous user or search spider');
$I->wantTo('go to treatstock.com and specify the location and go to treatstock.fr');
$I->expect("the language will remain in accordance with the domain."); 

$I->amOnUrl('https://www.treatstock.com/');
$I->waitForText('Sign in', 10);

// $I->click('.ts-user-location', '.navbar-header');
// $I->waitForElement('#userlocator-address', 10);
$I->click('button[title="Shipping Address"]'); // Раньше было: $I->click('.ts-user-location', '.navbar-header');
$I->waitForElement('input[placeholder="Enter a location"]', 10); // $I->waitForElement('#userlocator-address', 10);
$I->fillField('input[placeholder="Enter a location"]','Madrid'); // В дальнейшем возможно придется переделать с учетом надписи на другом языке.
$I->wait(2);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(3);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->waitForText('Madrid', 10); // $I->waitForText('Madrid', 10, 'button[title="Shipping Address"]');
$I->see('English');
// $I->seeFullUrlEquals('https://www.treatstock.com/3d-printing-services/Madrid--Community-of-Madrid--ES/');
$I->amOnUrl('https://www.treatstock.fr/');
$I->wait(5);
$I->dontSee('Madrid','.navbar-header'); 
$I->see('Se connecter');
$I->see('Français'); 

