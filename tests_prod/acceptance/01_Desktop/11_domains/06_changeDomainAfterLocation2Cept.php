<?php 

$I = new AcceptanceTester($scenario);
// 6. Changing the language at the bottom of the page results in a domain change.
// If the location has been selected by the user, it should be preserved.
// If the location is defined by ip, it must be preserved.
// If the location is not automatically defined, then we change the location accordingly to the domain.

// 6. Изменение языка внизу страницы, приводит к изменению домена. 
// Если локация была выбрана пользователем, она должна сохраниться.
// Если она определена по ip, она должна сохраниться. 
// Если локация автоматически не определена, то меняем локацию соответственно домену.
// Внимание! Локация указанная в профиле пользователя и сохраненная будет сброшена, после сброса сессии/куков. Типа так и задумано - утверждено Валентиной.

$I->am('Anonymous user or search spider'); 
$I->wantTo('specify the location and change the language at the bottom of the page');
$I->expect("domain change and the location will remain unchanged"); 

$I->amOnUrl('https://www.treatstock.com');

// $I->waitForElement('div.findps-hero__btn-find', 10);

// ----------
// Для авторизации пользователя:
// $I->click('Sign in');
// $I->wait(1);
// $I->see('Remember Me');
// $I->fillField('#loginform-email','test20190901@mail.ru');
// $I->fillField('#loginform-password','dLja6Testov');
// $I->click('Sign in', '.modal-body');
// $I->wait(5);
// $I->waitForElement('[class="header-bar__username"]', 10);
// $I->see('test201909'); // $I->see('test20190901');
// $I->wait(2);
// ----------
// Сохраняем значение автоматически определенной локации до смены языка:
$I->waitForElement('button[title="Shipping Address"]', 10);
// $location1 = $I->grabTextFrom('.ts-user-location', '.navbar-header');
$location1 = $I->grabTextFrom('button[title="Shipping Address"]');
// ДОБАВИТЬ ПРОВЕРКУ значения переменной, что значение определено (НЕ ПУСТОЕ!)
echo $location1;  // ДОБАВИТЬ перевод строки после команды echo!
// ----------
// Переключение языка внизу страницы:
$I->scrollTo(['css' => 'div.footer__lang'], 0, 0); // Прокрутить страницу до нижнего выбора языка.
$I->click(['css' => '.footer__lang__drop > div:nth-child(1) > a:nth-child(1)']); // Нажать на выбор языка
$I->wait(2);
$I->click(['css' => '.open > ul:nth-child(2) > li:nth-child(5) > a:nth-child(1)']); // Выбрать Français
$I->wait(5);
// Потом снова проверяем значение локации, сравнив новое значение локации с сохраненным значением:
// $I->see($location1, '.ts-user-location');
$I->see($location1, 'button[title="Adresse de livraison"]'); // $I->see($location1, 'button[title="Shipping Address"]');
$I->wait(5);
// ----------
// Затем снова изменяем локацию:
// $I->click('.ts-user-location', '.navbar-header');
$I->click('button[title="Adresse de livraison"]'); // $I->click('button[title="Shipping Address"]');
// $I->waitForElement('#userlocator-address', 10);
$I->waitForElement('input[placeholder="Enter a location"]', 10);
// $I->fillField('#userlocator-address','Madrid Spain');
$I->fillField('input[placeholder="Enter a location"]','Madrid Spain'); // В дальнейшем возможно придется переделать с учетом надписи на французском языке.
$I->wait(2);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(4);
$I->pressKey('input[placeholder="Enter a location"]', \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->wait(7);
// ----------
$I->see('Madrid','button[title="Adresse de livraison"]'); // $I->see('Madrid','button[title="Shipping Address"]');
$I->wait(5);
// ----------
// Переключение языка внизу страницы:
$I->scrollTo(['css' => 'div.footer__lang'], 0, 0); // Прокрутить страницу до нижнего выбора языка.
$I->click(['css' => '.footer__lang__drop > div:nth-child(1) > a:nth-child(1)']); // Нажать на выбор языка
$I->wait(2);
$I->click(['css' => '.open > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)']); // Выбрать English
$I->wait(5);
// ----------
// $I->see('Madrid','.nav-tabs__container');
$I->see('Madrid','button[title="Shipping Address"]');
$I->wait(5);
// -----------------------------------------------
// ----------
// $I->moveMouseOver('.header-bar__avatar');
// $I->wait(2);
// $I->click('Sign Out');
// $I->waitForText('Sign in', 5); 
// $I->see('Sign in');
// ----------
// deleteAllCookies:
$I->amOnUrl('https://www.treatstock.com/robots.txt');
$I->deleteAllCookies();
$I->amOnUrl('https://www.treatstock.fr/robots.txt');
$I->deleteAllCookies();
// ----------
