<?php

$I = new AcceptanceTester($scenario);
$I->am('Authorized user'); 
$I->wantTo('Click Featured Services on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

// $I->waitForElement('[class="header-bar__username"]', 10);
// $I->see('tkenot');
$I->waitForText('tkenot', 10);

$I->scrollTo(['css'=>'a[class = "btn btn-danger btn-ghost ts-ga"]'],0,-200);
$I->click('a[class = "btn btn-danger btn-ghost ts-ga"]'); // click('Print Here'); // Featured Services 
// $I->wait(5);
// $I->see('About printer');
$I->waitForText('Printer location', 10); // $I->see('Printer location');
$I->seeInCurrentUrl('3d-printing-service');

$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5);