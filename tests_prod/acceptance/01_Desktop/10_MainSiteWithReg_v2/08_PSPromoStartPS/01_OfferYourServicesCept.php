<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Start 3D Print Service on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForText('tkenot', 15);

// Become a Service
$I->click('Become a Service');
$I->wait(3);
$I->waitForText('Select a service', 15);

$I->amOnUrl('https://www.treatstock.com/');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');
// Become a Manufacturer
$I->scrollTo(['css'=>'.ps-promo__block'],0,-200);
$I->click('Get Started', 'div[class = "ps-promo__text animated fadeInRight"]');
$I->wait(5);
$I->waitForText('Select a service', 15);
