<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Sign in and sign out using Google account');
$I->lookForwardTo('user will sign using Google account in succefully');

$I->amOnUrl('https://www.treatstock.com/');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->click('Google');
$I->switchToLastWindow();
$I->wait(2);
$I->fillField('input[id="identifierId"]','trurud@gmail.com');
$I->wait(3);
$I->click('#identifierNext'); // click "Next".
$I->wait(5);
$I->fillField('input[name="password"]','fkMn4jDrU7');
$I->wait(1);
$I->click('#passwordNext'); // click "Next".
$I->wait(5);
$I->switchToLastWindow();
$I->wait(8);
$I->see('Trurus');

$I->wait(1);
$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->wait(5);
$I->see('Sign in');
