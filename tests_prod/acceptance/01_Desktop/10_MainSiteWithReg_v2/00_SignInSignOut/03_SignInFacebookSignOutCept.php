<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Sign in and sign out using Facebook account');
$I->lookForwardTo('user will sign using Facebook account in succefully');

$I->amOnUrl('https://www.treatstock.com/');
$I->wait(2);
$I->deleteAllCookies();
$I->wait(2);
$I->amOnUrl('https://www.treatstock.com/');
$I->wait(2);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');
$I->click('Facebook');
$I->switchToLastWindow();
$I->wait(2);
$I->fillField('input[id="email"]','instagram4@treatstock.info');
$I->wait(1);
$I->fillField('input[id="pass"]','$$$Treatstock2103!!!');
$I->wait(1);
$I->click('#loginbutton'); // click "Log In".
$I->wait(8);
$I->switchToLastWindow();
$I->wait(5);
$I->see('emma-wil');

$I->wait(1);
$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->wait(5);
$I->see('Sign in');
$I->wait(2);
$I->deleteAllCookies();
$I->wait(2);
