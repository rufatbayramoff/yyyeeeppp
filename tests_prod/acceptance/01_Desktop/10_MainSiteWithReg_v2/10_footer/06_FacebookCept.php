<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Facebook on footer home page');
$I->expect("tab open without errors");

// delete cookies
$I->amOnUrl('https://www.facebook.com/');
$I->wait(2);
$I->deleteAllCookies();
$I->wait(2);

$I->amOnUrl('https://www.treatstock.com/');
$I->see('Search & compare manufacturing services worldwide');
$I->wait(1);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForElement('[class="header-bar__username"]', 10);
$I->see('tkenot');

$I->scrollTo(['css'=>'.t-footer-follow__fb'],0,-200);

$I->wait(1);

$I->click('.t-footer-follow__fb');
$I->wait(10);

$I->switchToLastWindow();

if (class_exists('captcha_interstitial')) {
    
    $I->see('Security Check');
    $I->amOnUrl('https://www.facebook.com/treatstock/');
    
    $I->wait(8);
    $I->see('Treatstock');
    // $I->click('#expanding_cta_close_button');
    $I->wait(3);
    $I->see('3d printing');
}
 else {
     
    $I->wait(8);
    $I->see('Treatstock');
    // $I->click('#expanding_cta_close_button');
    $I->wait(3);
    $I->see('3d printing');
}

$I->wait(3);
$I->switchToParentWindow();


