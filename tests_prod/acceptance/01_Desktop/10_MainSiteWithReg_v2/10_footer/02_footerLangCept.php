<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Language on footer home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForElement('[class="header-bar__username"]', 10);
$I->see('tkenot');

$I->scrollTo(['css'=>'.t-footer-lang'],0,-200);

$I->wait(1);

$I->click('.t-footer-lang');
$I->wait(1);
// $I->click('Русский');
$I->click('a[href = "/?ln=ru"]', '.t-footer-lang');

$I->wait(5);

$I->see('Русский');

$I->scrollTo(['css'=>'.t-footer-lang'],0,-200);

$I->wait(1);

$I->click('.t-footer-lang');
$I->wait(1);
// $I->click('English');
$I->click('a[href = "/?ln=en-US"]', '.t-footer-lang');

$I->wait(5);

$I->see('Search & compare manufacturing services worldwide');
