<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click button Instagram on footer home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForElement('[class="header-bar__username"]', 10);
$I->see('tkenot');

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-follow__fb'],0,-200);

$I->wait(1);

$I->click('.t-footer-follow__inst');
$I->wait(10);

$I->switchToLastWindow();
$I->wait(5);
$I->see('Treatstock');

$I->wait(5);
$I->switchToParentWindow();

