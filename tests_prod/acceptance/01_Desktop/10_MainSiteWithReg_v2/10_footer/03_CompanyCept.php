<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click buttons blog Company on footer home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForElement('[class="header-bar__username"]', 10);
$I->see('tkenot');

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-company__about'],0,-200);

$I->wait(1);

// About Us
$I->click('.t-footer-company__about');
$I->wait(3);
$I->see('Treatstock is a B2B smart manufacturing platform');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-company__about'],0,-200);

$I->wait(1);

// Blog
$I->click('.t-footer-company__blog');
$I->wait(3);
$I->see('Treatstock Blog');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-company__about'],0,-200);

$I->wait(1);

// Contact Us
$I->click('.t-footer-company__contact');
$I->wait(3);
$I->see('Before you contact us');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');

