<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click 3D printable models on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

// $I->waitForElement('[class="header-bar__username"]', 10);
// $I->see('tkenot');
$I->waitForText('tkenot', 10);


$I->wait(1);

$I->scrollTo(['css'=>'footer[class="footer footer--extra-info"]'],0,-200);

$I->wait(1);

$I->click('a[href = "https://www.treatstock.com/3d-printable-models/"]');

$I->wait(1);
$I->switchToLastWindow();
$I->wait(3);

$I->waitForElement('#store-item-pjax', 5);
// $I->see('All Categories');
$I->see('Categories');
$I->click('Categories');
$I->wait(1);
$I->see('Home Goods');

$I->wait(1);
// $I->switchToWindow();
$I->closeTab();

