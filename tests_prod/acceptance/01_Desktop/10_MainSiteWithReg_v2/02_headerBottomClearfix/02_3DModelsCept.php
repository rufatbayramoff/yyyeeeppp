<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click 3D Models on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

// $I->waitForElement('[class="header-bar__username"]', 10);
// $I->see('tkenot');
$I->waitForText('tkenot', 10);

$I->click('3D Models', 'nav[class = "header-bottom clearfix"]');

$I->waitForElement('#store-item-pjax', 15);
// $I->see('All Categories');
$I->see('Categories');
$I->click('Categories');
$I->wait(1);
$I->see('Home Goods');

