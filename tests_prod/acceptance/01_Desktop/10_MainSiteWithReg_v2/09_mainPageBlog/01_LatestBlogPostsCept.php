<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Latest Blog Posts on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForElement('[class="header-bar__username"]', 10);
$I->see('tkenot');

$I->wait(1);

$I->scrollTo(['css'=>'div[class="post-list swiper-container"]'],0,-200);

$I->click('a[class = "post__image-container"]');

$I->waitForElement('.post__footer--hero', 5);
$I->see('3D Print');

