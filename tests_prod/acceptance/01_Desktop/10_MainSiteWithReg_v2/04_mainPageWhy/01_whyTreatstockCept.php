<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click buttons in Why Treatstock on home page');
$I->expect("tabs open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

// $I->waitForElement('[class="header-bar__username"]', 10);
// $I->see('tkenot');
$I->waitForText('tkenot', 10);

$I->scrollTo(['css'=>'.fadeInRight'],0,-200);
$I->wait(2);
$I->click('Customers', '.main-page-why');
// Find a Service
$I->click('Find a Service', '#whyCustomers');
$I->wait(2);
$I->see('Search & compare manufacturing services worldwide');
// How it Works
$I->scrollTo(['css'=>'.fadeInRight'],0,-200);
$I->click('How it Works', '#whyCustomers');
$I->wait(2);
$I->see('How does Treatstock work?');

$I->scrollTo(['css'=>'.main-page-why'],0,-200);
$I->click('Services', '.main-page-why');
// Offer Your Servives
$I->waitForElement('.fadeInUp', 5);
$I->click('a[data-animation-in="fadeInUp"]');
$I->wait(5);
// $I->see('Start your print service');
$I->waitForText('Select a service', 10);
$I->see('My Services Dashboard');
$I->wait(3);
$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

