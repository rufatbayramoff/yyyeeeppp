<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Get Started on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForElement('[class="header-bar__username"]', 10);
$I->see('tkenot');

$I->scrollTo(['css'=>'a[class = "btn btn-danger main-page-how__cta"]'],0,-200);
$I->wait(3);
$I->click('Get Started');
// $I->wait(5);
// $I->see('Be the first to leave a review');
$I->waitForText('Compare upfront prices from manufacturers', 10);

