<?php

// https://www.treatstock.com/ 

$I = new AcceptanceTester($scenario);
$I->am('Anonymous user or search spider'); 
$I->wantTo('Click button All Materials');
$I->lookForwardTo('Window opened');

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');
$I->wait(3);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForElement('[class="header-bar__username"]', 10);
$I->see('tkenot');

$I->click('#dropdownMaterial', '.header-bar__findps-material');
$I->wait(1);
$I->click('a[title="PLA"]', '.header-bar__findps-material');

$I->waitForElement('.catalog-listview', 15);
$I->see('3D printing');