<?php

// https://www.treatstock.com/ 

$I = new AcceptanceTester($scenario);
$I->am('Anonymous user or search spider'); 
$I->wantTo('Click button Shipping Address');
$I->lookForwardTo('Window opened');

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForElement('[class="header-bar__username"]', 10);
$I->see('tkenot');

$I->click('.ts-user-location', '.navbar-header');
$I->wait(3);
$I->fillField('UserLocator[address]','Georgetown, DE, USA');
$I->wait(3);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(3);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ENTER);

$I->waitForElement('.catalog-listview', 15);
$I->see('3D printing');


