<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Upload Files in header bar');
$I->expect("Window opened without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','tkenot@mail.ru');
$I->fillField('#loginform-password','qwerty05072017');
$I->click('Sign in', '.modal-body');

$I->waitForElement('[class="header-bar__username"]', 10);
$I->see('tkenot');

$I->click('Order 3D Print', '.header-bar__findps-upload');

$I->waitForText('Browse Files', 15, '.btn.btn-danger.upload-block__btn');
$I->see('STL, PLY, 3MF, JPG, PNG and GIF files are supported');