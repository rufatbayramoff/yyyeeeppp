<?php 

$I = new AcceptanceTester($scenario);
$I->am('Authorized user');
$I->wantTo('sign in treatstock.com and I switch languages');
$I->lookForwardTo('I will see all the texts in foreign languages'); 
$I->amOnUrl('https://www.treatstock.com/');

$I->click('Sign in');
$I->wait(1);
$I->see('Remember Me');

$I->fillField('#loginform-email','test20190901@mail.ru');
$I->fillField('#loginform-password','dLja6Testov');
$I->click('Sign in', '.modal-body');
$I->wait(5);
$I->see('test201909');

$I->comment("Switch language to Russian"); 
$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('Русский');
$I->wait(5);
$I->see('Открыть сервис');
$I->see('Закажите 3D печать модели');
$I->scrollTo(['css'=>'.t-ps-promo--start-ps-btn'],0,-200);
$I->see('Начать'); // $I->see('Предложите Ваш сервис 3D печати');

$I->comment("Switch language to Japanese");
$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('日本語'); "Japanese".
$I->wait(5);
$I->see('デザイナーを検索'); // "Hire a Designer".
$I->scrollTo(['css'=>'h4[class="main-page-how__subtitle"]'],0,-200); // Make a scroll to the block ("Upload Model" data-animation-out="fadeOutLeft")
$I->see('モデルをアップロード'); // "Upload Model".

$I->comment("Switch language to Chinese");
$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('中文'); // "Chinese".
$I->wait(5);
$I->see('寻找&比较全球制作服务'); // $I->see('Search & compare manufacturing services worldwide');
$I->see('订购3D打印'); // "Order 3D Print".
$I->scrollTo(['css'=>'.t-ps-promo--start-ps-btn'],0,-200);
$I->see('最新博客'); // "Latest Blog Posts".

$I->comment("Switch language to French");
$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('Français');
$I->wait(5);
$I->see('Trouver un service'); // "Find a Service".
$I->see('Rechercher'); // "Search".
$I->scrollTo(['css'=>'a[href="#whyServices"]'], 0, -150);
$I->click('Prestations de service'); // "Services".
$I->wait(3);
$I->see('Outils commerciaux'); // "Business Tools".
$I->scrollTo(['css'=>'a[href="/3d-printing-services/all-locations/"]'], 0, -150);
$I->wait(3);
$I->see('Essayez-le maintenant'); // "Get Started".

$I->comment("Switch language to English");
$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('English');
$I->wait(5);

$I->wait(1);
$I->moveMouseOver('.header-bar__avatar');
$I->wait(2);
$I->click('Sign Out');
$I->waitForText('Sign in', 5);
