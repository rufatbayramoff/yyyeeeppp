<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Model upload and print on main site In Chinese');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');
$I->see('Search & compare manufacturing services worldwide');

$I->click('.ts-user-location', '.navbar-header');
$I->wait(8);
$I->waitForElement('.control-label', 10);
$I->fillField('UserLocator[address]','Beijing, China');
$I->wait(1);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(1);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->wait(4);

$I->amOnUrl('https://www.treatstock.com/');
$I->wait(5);
$I->click('#current-lang');
$I->wait(1);
$I->click('中文');
$I->wait(8);
// $I->see('查找全球3D打印服务');

$I->click('订购3D打印', '.findps-hero__upload'); // $I->click('Order 3D Print', '.findps-hero__upload');

$I->waitForText('浏览文件', 15, '.btn.btn-danger.upload-block__btn'); // $I->waitForText('Browse Files', 15, '.btn.btn-danger.upload-block__btn');
$I->see('STL, PLY, 3MF, JPG, PNG and GIF files are supported');
$I->wait(3);

$I->executeJS('$( ".upload-drop-file-zone" ).append((window.test.getWidgetUploadInput().attr("id", "testFileUpload")))');
$I->executeJS('$("#testFileUpload").show();');
$I->executeJS('$("#testFileUpload").css("visibility", "visible").css("width","1").css("height","1");');
$I->attachFile('#testFileUpload', 'F-47_Mini_b.stl');

$I->wait(15);
$I->see('数量'); // $I->see('Quantity');
$I->wait(1);
$I->click('下一页'); // $I->click('Next');

$I->wait(3);
echo "Step 2\n\n";
$I->waitForElement('.printers', 8); // I wait to see printers list.
$I->click('.material-switcher__item');
$I->wait(3);
$I->click('div[ng-click="changeColor(printerColor.id)"]');
$I->wait(1);
$I->waitForElement('div.printers', 8);
$I->wait(1);
$I->click('.print-by-printer-button');

$I->wait(5);
echo "Step 3\n\n";
$I->see('邮递信息'); // $I->see('Delivery details');
$I->fillField('deliveryform-contact_name', 'Test');
$I->wait(1);
$I->fillField('deliveryform-street', '6136 HAZLETT ST');
$I->wait(1);
$I->fillField('deliveryform-street2', '6136 HAZLETT ST');
$I->wait(1);
$I->fillField('deliveryform-state', 'MI');
$I->wait(1);
$I->fillField('deliveryform-zip', '48210');
$I->wait(1);
$I->fillField('deliveryform-company', 'Test');
$I->wait(1);
$I->fillField('deliveryform-phone', '+71231234567');
$I->wait(1);
$I->fillField('deliveryform-email', 'test@test.test');
$I->wait(1);
$I->fillField('deliveryform-comment', 'Test. A comment.');
$I->wait(1);
$I->click('下一页'); // $I->click('Next');

$I->wait(10);
echo "Step 4\n\n";
$I->see('支付订单'); // Payment
$I->see('账单细节'); // Billing Details

$I->switchToIFrame("braintree-dropin-frame");
$I->waitForElement('#credit-card-number', 10);
$I->wait(3);
$I->switchToIFrame();

$I->click('#current-lang');
$I->wait(3);
$I->click('English');

$I->wait(12);
$I->see('Sign in');
