<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Model upload and print on main site');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');
$I->see('Search & compare manufacturing services worldwide');

$I->click('.ts-user-location', '.navbar-header');
$I->wait(8);
$I->waitForElement('.control-label', 10);
$I->fillField('UserLocator[address]','Detroit, MI, USA');
$I->wait(1);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(1);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->wait(4);

$I->amOnUrl('https://www.treatstock.com/');
$I->see('Search & compare manufacturing services worldwide');
$I->wait(3);

$I->click('Order 3D Print', '.findps-hero__upload'); // .header-bar__findps-upload

$I->waitForText('Browse Files', 15, '.btn.btn-danger.upload-block__btn');
$I->see('STL, PLY, 3MF, JPG, PNG and GIF files are supported');
$I->wait(3);

$I->executeJS('$( ".upload-drop-file-zone" ).append((window.test.getWidgetUploadInput().attr("id", "testFileUpload")))');
$I->executeJS('$("#testFileUpload").show();');
$I->executeJS('$("#testFileUpload").css("visibility", "visible").css("width","1").css("height","1");');
$I->attachFile('#testFileUpload', 'F-47_Mini_b.stl');

$I->wait(10);
$I->see('Quantity');
$I->wait(1);
$I->click('Next');

$I->wait(3);
$I->waitForElement('.printers', 8); // I wait to see printers list.
$I->click('div[ng-click="changeColor(printerColor.id)"]');
$I->wait(1);
$I->waitForElement('div.printers', 8);
$I->wait(1);
$I->click('.print-by-printer-button');

$I->wait(5);
$I->waitForElement('#delivery', 8);
$I->see('Phone');
$I->wait(1);
$I->fillField('deliveryform-street', '6136 HAZLETT ST');
$I->wait(1);
$I->fillField('deliveryform-street2', '6136 HAZLETT ST');
$I->wait(1);
$I->fillField('deliveryform-state', 'MI');
$I->wait(1);
$I->fillField('deliveryform-contact_name', 'Test');
$I->wait(1);
$I->fillField('deliveryform-zip', '48210');
$I->wait(1);
$I->fillField('deliveryform-company', 'Test');
$I->wait(1);
$I->fillField('deliveryform-phone', '+71231234567');
$I->wait(1);
$I->fillField('deliveryform-email', 'test@test.test');
$I->wait(1);

$I->click('Next'); // $I->click('button[ng-click = "nextStep()"]');

$I->wait(8);
$I->see('Payment');
$I->see('Billing Details');
$I->switchToIFrame("braintree-dropin-frame");
$I->waitForElement('#credit-card-number', 20);
