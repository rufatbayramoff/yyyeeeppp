<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click What our customers are saying on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->scrollTo(['css'=>'.main-page-prints__gallery'],0,-200);
$I->click('img[class = "main-page-prints__img"]');
$I->wait(5);
$I->see('Print');

