<?php

$I = new AcceptanceTester($scenario);
$I->am('Anonymous user or search spider'); 
$I->wantTo('Click Featured Services on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->scrollTo(['css'=>'a[class = "btn btn-danger btn-ghost ts-ga"]'],0,-200);
$I->click('a[class = "btn btn-danger btn-ghost ts-ga"]'); // click('Print Here'); // Featured Services 
// $I->wait(5);
// $I->see('About printer');
$I->waitForText('Printer location', 10); // $I->see('Printer location');
$I->seeInCurrentUrl('3d-printing-service');

