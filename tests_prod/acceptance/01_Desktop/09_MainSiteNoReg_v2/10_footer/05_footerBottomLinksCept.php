<?php

$I = new AcceptanceTester($scenario);
$I->am('Anonymous user or search spider'); 
$I->wantTo('Click buttons bottom-links on footer home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

$I->click('Accept and Close'); // Убрать уведомление об использовании Cookie

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-menu__policy'],0,-200);

$I->wait(1);

// Privacy Policy
$I->click('.t-footer-menu__policy');
$I->wait(3);
$I->see('Privacy Statement');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-menu__policy'],0,-200);

$I->wait(1);

// Terms of Use
$I->click('.t-footer-menu__terms');
$I->wait(3);
$I->see('Treatstock Terms and Conditions');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-menu__policy'],0,-200);

$I->wait(1);

// Return Policy
$I->click('.t-footer-menu__return');
$I->wait(3);
$I->see('Return Policy');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');

