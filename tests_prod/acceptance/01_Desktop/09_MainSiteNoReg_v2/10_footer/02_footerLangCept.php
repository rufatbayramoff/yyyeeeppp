<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Language on footer home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

$I->scrollTo(['css'=>'.t-footer-lang'],0,-200);

$I->wait(1);

$I->click('.t-footer-lang');
$I->wait(1);
// $I->click('Русский');
$I->click('a[href = "/?ln=ru"]', '.t-footer-lang');

$I->wait(5);

$I->see('Русский');

$I->scrollTo(['css'=>'.t-footer-lang'],0,-200);

$I->wait(1);

$I->click('.t-footer-lang');
$I->wait(1);
// $I->click('English');
$I->click('a[href = "/?ln=en-US"]', '.t-footer-lang');

$I->wait(5);

$I->see('Search & compare manufacturing services worldwide');
