<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click button Twitter on footer home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

$I->scrollTo(['css'=>'.t-footer-follow__fb'],0,-200);

$I->wait(1);

$I->click('.t-footer-follow__twit');
$I->wait(10);

$I->switchToLastWindow();
$I->wait(5);
$I->see('Treatstock');

$I->wait(5);
$I->switchToParentWindow();
