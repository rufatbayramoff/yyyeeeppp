<?php

$I = new AcceptanceTester($scenario);
$I->am('Anonymous user or search spider'); 
$I->wantTo('Click 3D printable models on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

$I->scrollTo(['css'=>'footer[class="footer footer--extra-info"]'],0,-200);

$I->wait(1);

$I->click('a[href = "https://www.treatstock.com/3d-printable-models/"]');

$I->wait(1);
$I->switchToLastWindow();
$I->wait(3);

$I->waitForElement('#store-item-pjax', 5);
$I->see('Categories');
$I->click('Categories');
$I->wait(1);
$I->see('Home Goods');

$I->closeTab(); // вместо команды: $I->switchToWindow();
