<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click buttons blog Resorces on footer home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-resources__3d-models'],0,-200);

$I->wait(1);

// 3D Models for Printing
$I->click('.t-footer-resources__3d-models');
$I->wait(3);
$I->see('Categories');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-resources__3d-models'],0,-200);

$I->wait(1);

// Help Center
$I->click('.t-footer-resources__help');
$I->wait(3);
$I->see('Welcome to our Help Center');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-resources__3d-models'],0,-200);

$I->wait(1);

// Hire a Designer
$I->click('.t-footer-resources__hiredes');
$I->wait(3);
$I->see('Hire a Designer', 'h1');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');

// $I->executeJS('window.scrollTo(0,7000);');
$I->scrollTo(['css'=>'.t-footer-resources__3d-models'],0,-200);

$I->wait(1);

// API
$I->click('.t-footer__service--api');
$I->wait(3);
$I->see('Become an API partner');
$I->click('Treatstock');
$I->wait(3);
$I->see('Search & compare manufacturing services worldwide');