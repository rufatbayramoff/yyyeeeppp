<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Facebook on footer home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

$I->scrollTo(['css'=>'.t-footer-follow__fb'],0,-200);

$I->wait(1);

$I->click('.t-footer-follow__fb');
$I->wait(10);

$I->switchToLastWindow();

if (class_exists('captcha_interstitial')) {
    
    $I->see('Security Check');
    $I->amOnUrl('https://www.facebook.com/treatstock/');
    
    $I->wait(8);
    $I->see('Treatstock');
    // $I->click('#expanding_cta_close_button');
    $I->wait(3);
    $I->see('3d printing');
}
 else {
     
    $I->wait(8);
    $I->see('Treatstock');
    // $I->click('#expanding_cta_close_button');
    $I->wait(3);
    $I->see('3d printing');
}

$I->wait(3);
$I->switchToParentWindow();


