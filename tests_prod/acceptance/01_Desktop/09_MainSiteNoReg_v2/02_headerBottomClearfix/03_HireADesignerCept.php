<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Hire a Designer on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->click('Hire a Designer', 'nav[class = "header-bottom clearfix"]');

$I->waitForElement('div[class = "designer-card"]', 15);
$I->wait(1);
$I->see('Hire a Designer', 'h1');
