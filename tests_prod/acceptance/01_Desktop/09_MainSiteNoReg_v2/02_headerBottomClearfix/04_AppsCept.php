<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Apps on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->click('Apps', 'nav[class = "header-bottom clearfix"]');

$I->waitForElement('div[class = "apps-grid"]', 15);
$I->wait(1);
$I->see('Free Manufacturing Apps');

