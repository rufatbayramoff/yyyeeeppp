<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click choice of language in header-top_nav');
$I->expect("Language changes correctly"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('Русский');

$I->wait(5);

$I->see('Русский');

$I->click('.t-header-bottom__lang');
$I->wait(1);
$I->click('English');

$I->wait(5);

$I->see('Search & compare manufacturing services worldwide');

