<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click 3D Printing on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->click('3D Printing', 'nav[class = "header-bottom clearfix"]');

$I->waitForElement('.catalog-listview', 15);
$I->wait(1);
$I->see('3D printing');

