<?php 

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Find a Service header bar');
$I->expect("Window opened without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->click('Find a Service', '.header-bar__findps-submit');

$I->waitForElement('.catalog-listview', 15);
$I->see('3D printing');
