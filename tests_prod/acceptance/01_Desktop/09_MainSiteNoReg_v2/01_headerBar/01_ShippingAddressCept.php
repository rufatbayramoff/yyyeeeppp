<?php

// https://www.treatstock.com/ 

$I = new AcceptanceTester($scenario);
$I->am('Anonymous user or search spider'); 
$I->wantTo('Click button Shipping Address');
$I->lookForwardTo('Window opened');

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('.ts-user-location', '.navbar-header');
$I->wait(3);
$I->fillField('UserLocator[address]','Georgetown, DE, USA');
$I->wait(3);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(3);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ENTER);

$I->waitForElement('.catalog-listview', 15);
$I->see('3D printing');


