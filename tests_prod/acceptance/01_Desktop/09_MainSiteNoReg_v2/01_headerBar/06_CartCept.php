<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Cart in header-top_nav');
$I->expect("All tabs open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->wait(1);

$I->click('Cart');
$I->wait(1);
$I->waitForElement('.empty-cart-head', 10);
$I->wait(1);
$I->see('Your shopping cart is empty');