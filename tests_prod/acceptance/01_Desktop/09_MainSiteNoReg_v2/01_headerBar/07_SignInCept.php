<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Sign in on home page');
$I->expect("Tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->waitForElement('div[class = "findps-hero__btn-find"]', 10);

$I->wait(1);

$I->click('.t-header-bar__auth-icon-btn--signin');

$I->waitForElement('#loginform-rememberme', 5);
$I->wait(1);
$I->see('Remember Me');

