<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click buttons in findps-hero__loc on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('.ts-user-location', '.hero-banner'); // $I->click('.ts-user-location', '.hero-banner');

$I->wait(3);
$I->fillField('UserLocator[address]','Georgetown, DE, USA');
$I->wait(3);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
$I->wait(3);
$I->pressKey('#userlocator-address', \Facebook\WebDriver\WebDriverKeys::ENTER);

$I->waitForElement('.nav-filter__group', 15);
$I->see('3D printing');

