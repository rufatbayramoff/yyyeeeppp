<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click button Search on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('Search', '.hero-banner');

$I->waitForElement('.nav-filter__group', 15);
$I->see('3D printing');

