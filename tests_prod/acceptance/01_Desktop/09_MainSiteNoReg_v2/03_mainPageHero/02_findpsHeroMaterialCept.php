<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click buttons in findps-hero__material on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->click('#dropdownHeroMaterial', '.hero-banner');
$I->wait(1);
$I->click('a[title="PLA"]', '.hero-banner');

$I->waitForElement('.nav-filter__group', 15);
$I->see('3D printing');
