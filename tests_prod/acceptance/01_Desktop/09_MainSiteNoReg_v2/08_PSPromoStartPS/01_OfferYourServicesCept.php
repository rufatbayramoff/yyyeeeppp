<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Start 3D Print Service on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

// Become a Manufacturer
$I->scrollTo(['css'=>'.ps-promo__block'],0,-200);
$I->click('Get Started', 'div[class = "ps-promo__text animated fadeInRight"]');
$I->wait(5);
$I->see('By using this Service');
