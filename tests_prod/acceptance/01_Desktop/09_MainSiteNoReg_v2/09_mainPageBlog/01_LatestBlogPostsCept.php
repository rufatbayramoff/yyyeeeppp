<?php

$I = new AcceptanceTester($scenario);
$I->am('Registered user'); 
$I->wantTo('Click Latest Blog Posts on home page');
$I->expect("tab open without errors"); 

$I->amOnUrl('https://www.treatstock.com/');

$I->see('Search & compare manufacturing services worldwide');

$I->wait(1);

$I->scrollTo(['css'=>'div[class="post-list swiper-container"]'],0,-200);

$I->click('a[class = "post__image-container"]');

$I->waitForElement('.post__footer--hero', 5);
$I->see('3D Print');

