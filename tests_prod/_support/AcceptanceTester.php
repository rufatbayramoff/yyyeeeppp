<?php

#namespace Codeception;



/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
*/
class InternalTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

   /**
    * Define custom actions here
    */
    
   
    /**
     * Grab regex from last email and return the match ONLY
     * 
     * @param string $regex
     * @return string matched expression - in ()
     */
    public function grabMatchedFromLastEmail($regex) {
       $fullUrl = $this->grabFromLastEmail($regex);
       preg_match($regex, trim($fullUrl), $matches);
       return "/".$matches[1];
   }
    public function grabMatchedFromLastEmailTo($email,$regex) {
       $fullUrl = $this->grabFromLastEmailTo($email,$regex);
       preg_match($regex, trim($fullUrl), $matches);
       return "/".$matches[1];
   }
   
   
   public function attachFileDropzone($filename) {
    $this->executeJS('$(".dz-hidden-input").css("visibility", "visible").css("width","1").css("height","1");');
    $this->attachFile('.dz-hidden-input', $filename);       
   }
   
   public function switchToLastWindow() {
     $this->executeInSelenium(function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
      $handles=$webdriver->getWindowHandles();
      $last_window = end($handles);
      $webdriver->switchTo()->window($last_window);
     });
   }    

   public function switchToParentWindow() {
     $this->executeInSelenium(function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
      $webdriver->close();
       $handles=$webdriver->getWindowHandles();
      $last_window = end($handles);
      $webdriver->switchTo()->window($last_window);
     });
   }    

   
   public function stopTimeouts() {
       $this->executeJS("var id = window.setTimeout(function() {}, 0); while (id--) { window.clearTimeout(id); }; "
                       . "id = window.setInterval(function() {}, 0);  while (id--) { window.clearInterval(id); }; ");
   }    
   
}


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester 
{
    
    protected $internalTester;
    
    static $_disableDebugger = 0;

    public function __construct(\Codeception\Scenario $scenario)
    {
        $this->internalTester = new InternalTester($scenario);
    }
    
    public function isDebugMode() {
        global $argv;
        return (array_search("-d",$argv)!==FALSE);
    }
    
    public function __call($method, $arguments) {
        $tryCount = 3; 
        
        if (substr($method,0,5)=="tryTo"){
            $newMethod = lcfirst(substr($method,5));
            try {
//                call_user_method_array($newMethod, $this->internalTester, $arguments);
                call_user_func_array(array($this->internalTester,$newMethod), $arguments);
                return true; // no error
            } catch (Exception $ex) {
                // we have an eerrrror?
            }
            return false;
        }
        
        if ($this->isDebugMode() && self::$_disableDebugger == 0) {
            $sdefault = "\nEnter next Codeception command (nodebug,die,continue,\$I->...): ";
            $logFileName = __DIR__.'/../../common/runtime/logs/errors.log';
            //if ($method == "see") $tryCount = 5; // see N times!
            //if ($method == "fillField" || $method == "selectOption") $tryCount = 2; // try to fill in field N times!
            Do {
                try {
                    if (file_exists($logFileName)) {
                        unlink($logFileName);
                    }
                    $returnValue = call_user_func_array(array($this->internalTester,$method), $arguments);
                    if (file_exists($logFileName)) {
                        $content = file_get_contents($logFileName);
                        $tryCount = 1;
                        throw new LogicException('Errors in back log: ' . $logFileName . "\n" . $content);
                    }
                    return $returnValue;
                } catch (\Exception $e) {
                    $tryCount = $tryCount - 1;
                    sleep(3);
                    $s = "\n".$e->getMessage() . $sdefault;
                    //var_dump($method);
                    //var_dump($arguments);
                    // call_user_func_array(array($this->internalTester,"resizeWindow"), array(2000,2000));
                    call_user_func_array(array($this->internalTester,"restoreDefaultWindowSize"), array());
                    if ($method == "click" || $method == "fillField" || $method == "selectOption") {
                        // scrollTo
                        if (strpos($arguments[0],".")!==false || strpos($arguments[0],"#")!==false || strpos($arguments[0],"[")!==false ) {
                               // CSS selector: id or class or element with [] selector
                               $argument0 = array(0=>$arguments[0], 1=>0, 2=>-300);
                               // css selector inside css selector
                               if (isset($arguments[1]) && (substr($arguments[1],0,1)=="." || substr($arguments[1],0,1)=="#" || strpos($arguments[0],"[")!==false ) )  { 
                                    $argument0 = array(0=>$arguments[1]." ".$argument0[0], 1=>0, 2=>-300);
                               }
                        } else {
                               // tag content
                               // $I->scrollTo("//a[contains(.,'Next')] | //button[contains(.,'Next')]",0,-500);
                               $s = "[contains(.,'".$arguments[0]."')]";
                               $xpath = "//a".$s." | //button".$s;
                               $argument0= array(0=>$xpath, 1=>0, 2=>-300);
                               // tag content inside css selector: ID 
                               if (isset($arguments[1]) && (substr($arguments[1],0,1)=="#"))  {
                                    $cssselector = substr($arguments[1],1,999);
                                    $xpathselector = "*[@id='".$cssselector."']//";
                                    $xpath = "//".$xpathselector."a".$s." | //".$xpathselector."button".$s;
                                    $argument0 = array(0=>$xpath, 1=>0, 2=>-300);
                               }
                               // tag content inside css selector: class
                               if (isset($arguments[1]) && (substr($arguments[1],0,1)=="."))  {
                                    $cssselector = trim(substr($arguments[1],1,999));
                                    $xpathselector = "*[contains(concat(' ', normalize-space(@class), ' '),' ".$cssselector." ')]//";
                                    $xpath = "//".$xpathselector."a".$s." | //".$xpathselector."button".$s;
                                    $argument0 = array(0=>$xpath, 1=>0, 2=>-300);
                               }
                               // tag content inside css selector "[]" NOT SUPPORTED!
                               // tag content inside XPTH selectors NOT SUPPORTED!
                        }
                        try {
                            echo " scrolling to ".$argument0[0]."\n";
                            call_user_func_array(array($this->internalTester,"scrollTo"),$argument0);
                        } catch (\Exception $e2) {
                            echo "Sorry ".$argument0[0]." not found to scrollTo\n";
                        }
                    }
                    if ($tryCount == 0) {
                        $I = $this;
                        Do {
                            fwrite(STDOUT, $s);
                            $command = trim(fgets(STDIN));
                            if ($command == "die" or $command == "exit") {
                                throw $e;
                            } elseif ($command == "nodebug") {
                                self::$_disableDebugger = 1;
                                throw $e;
                            } elseif ($command == "continue" || $command == "") {
                                fwrite(STDOUT, "Continue to do one step of test\n");
                            } else {
                                try {
                                    eval($command);
                                    fwrite(STDOUT, "OK\n");
                                } catch (\Exception $e2) {
                                    fwrite(STDOUT, $e2->getMessage());
                                }
                            }
                            $s = $sdefault;
                        } while ($command != "continue" && $command != "");
                    } else {
                            fwrite(STDOUT, $s.", RETRYING...\n");
                    }
                }
            } while ($tryCount > 0);
        } else {
            // count-only proxy
            $e0 = null;
            do {
                try {
                    return call_user_func_array(array($this->internalTester,$method), $arguments);
//                    return call_user_method_array($method, $this->internalTester, $arguments);
                } catch (\Exception $e) {
                    $tryCount = $tryCount - 1;
                    $e0 = $e;
                    sleep(3);
                    call_user_func_array(array($this->internalTester,"restoreDefaultWindowSize"), array());
                    if ($method == "click" || $method == "fillField" || $method == "selectOption") {
                        // scrollTo
                        if (strpos($arguments[0],".")!==false || strpos($arguments[0],"#")!==false || strpos($arguments[0],"[")!==false ) {
                               // CSS selector: id or class or element with [] selector
                               $argument0 = array(0=>$arguments[0], 1=>0, 2=>-300);
                               // css selector inside css selector
                               if (isset($arguments[1]) && (substr($arguments[1],0,1)=="." || substr($arguments[1],0,1)=="#" || strpos($arguments[0],"[")!==false ) )  { 
                                    $argument0 = array(0=>$arguments[1]." ".$argument0[0], 1=>0, 2=>-300);
                               }
                        } else {
                               // tag content
                               // $I->scrollTo("//a[contains(.,'Next')] | //button[contains(.,'Next')]",0,-500);
                               $s = "[contains(.,'".$arguments[0]."')]";
                               $xpath = "//a".$s." | //button".$s;
                               $argument0= array(0=>$xpath, 1=>0, 2=>-300);
                               // tag content inside css selector: ID 
                               if (isset($arguments[1]) && (substr($arguments[1],0,1)=="#"))  {
                                    $cssselector = substr($arguments[1],1,999);
                                    $xpathselector = "*[@id='".$cssselector."']//";
                                    $xpath = "//".$xpathselector."a".$s." | //".$xpathselector."button".$s;
                                    $argument0 = array(0=>$xpath, 1=>0, 2=>-300);
                               }
                               // tag content inside css selector: class
                               if (isset($arguments[1]) && (substr($arguments[1],0,1)=="."))  {
                                    $cssselector = trim(substr($arguments[1],1,999));
                                    $xpathselector = "*[contains(concat(' ', normalize-space(@class), ' '),' ".$cssselector." ')]//";
                                    $xpath = "//".$xpathselector."a".$s." | //".$xpathselector."button".$s;
                                    $argument0 = array(0=>$xpath, 1=>0, 2=>-300);
                               }
                               // tag content inside css selector "[]" NOT SUPPORTED!
                               // tag content inside XPTH selectors NOT SUPPORTED!
                        }
                        try {
                            echo " scrolling to ".$argument0[0]."\n";
                            call_user_func_array(array($this->internalTester,"scrollTo"),$argument0);
                        } catch (\Exception $e2) {
                            echo "Sorry ".$argument0[0]." not found to scrollTo\n";
                        }
                    }
                    //fwrite(STDOUT, "\n".$e->getMessage().", RETRYING...\n");
                }
            } while ($tryCount > 0);
            throw ($e0);
        }
    }

}
