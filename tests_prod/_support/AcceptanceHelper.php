<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Facebook\WebDriver\Remote\WebDriverCommand;
use Facebook\WebDriver\Remote\DriverCommand;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\WebDriverBrowserType;
use Facebook\WebDriver\WebDriverPlatform;
use Facebook\WebDriver\Firefox\FirefoxProfile;
use Facebook\WebDriver\Firefox\FirefoxDriver;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use League\Flysystem\Exception;

class AcceptanceHelper extends \Codeception\Module\WebDriver
{
  var $langBrowser = "en";
  var $caps, $profile;
    
  public function _initialize() {
        // parent::_initialize();
        $this->wd_host = sprintf('http://%s:%s/wd/hub', $this->config['host'], $this->config['port']);
        $this->capabilities = $this->config['capabilities'];
        if (isset($_SERVER['BROWSER'])) {
            switch (strtolower($_SERVER['BROWSER'])) {
                case "chrome":
                    $this->capabilities[WebDriverCapabilityType::BROWSER_NAME] = strtolower($_SERVER['BROWSER']);
                    $this->config['browser'] = strtolower($_SERVER['BROWSER']);
                    break;
                case "firefox":
                    $this->capabilities[WebDriverCapabilityType::BROWSER_NAME] = strtolower($_SERVER['BROWSER']);
                    $this->config['browser'] = strtolower($_SERVER['BROWSER']);
                    break;
                default:
                    $this->capabilities[WebDriverCapabilityType::BROWSER_NAME] = $this->config['browser'];
                    break;
            }
        }
        $this->capabilities[WebDriverCapabilityType::PROXY] = $this->getProxy();
        $this->connectionTimeoutInMs = $this->config['connection_timeout'] * 1000;
        $this->requestTimeoutInMs = $this->config['request_timeout'] * 1000;

        $this->webDriver = null;

        $caps = new DesiredCapabilities(array(
          WebDriverCapabilityType::BROWSER_NAME => $this->config['browser'],
          WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANY,
        ));
        if ($this->config['browser'] == 'chrome') {
            $profile = new ChromeOptions();
            $profile->addArguments(["--lang=en","--disable-extensions"]);
            $profile->setExperimentalOption('prefs', ['intl.accept_languages' => 'en']);
            $caps->setCapability(ChromeOptions::CAPABILITY, $profile);
        }
        if ($this->config['browser'] == 'firefox') {
            // disable the "Reader View" help tooltip, which can hide elements in the window.document
            $profile = new FirefoxProfile();
            $profile->setPreference("intl.accept_languages", $this->langBrowser);
            $profile->setPreference('reader.parse-on-load.enabled', false);
            $profile->setPreference('browser.download.panel.shown', false);
            $profile->setPreference('browser.download.manager.retention', 1);
            $profile->setPreference('browser.download.animateNotifications', false);        
            $profile->setPreference('browser.download.panel.removeFinishedDownloads', true);
            $profile->setPreference('browser.download.manager.flashCount', 3000);
            $profile->setPreference('browser.download.panel.shown', true);
            $profile->setPreference('browser.helperApps.neverAsk.saveToDisk', 'application/download, application/octet-stream, application/sla, application/stl, application/vnd.ms-pki.stl');
            //$profile->setPreference('browser.download.folderList', 2);
            //$profile->setPreference('browser.download.dir', "c:\\temp\\");
            //$profile->setPreference('browser.download.manager.alertOnEXEOpen', false);
            $profile->setPreference('browser.download.manager.showWhenStarting', false);
            $profile->setPreference('browser.download.manager.focusWhenStarting', false);
            //$profile->setPreference('browser.download.useToolkitUI', true);          
            //$profile->setPreference('browser.download.useDownloadDir', false);
            //$profile->setPreference('browser.helperApps.alwaysAsk.force', false);
            $profile->setPreference('browser.download.manager.closeWhenDone', true);
            $profile->setPreference('browser.download.manager.showAlertOnComplete', false);
            $profile->setPreference('browser.download.manager.useWindow', false);
            //$profile->setPreference('services.sync.prefs.sync.browser.download.manager.showWhenStarting"', false); // Disable by firefox 57
            $profile->setPreference('security.mixed_content.block_active_content',false);
            $profile->setPreference('security.mixed_content.block_display_content',true);
            //$profile->setPreference('pdfjs.disabled', false);

            $caps->setCapability(FirefoxDriver::PROFILE, $profile);
        }

        try {
            $this->webDriver = RemoteWebDriver::create(
                $this->wd_host,
                $caps, // $this->capabilities,
                $this->connectionTimeoutInMs,
                $this->requestTimeoutInMs,
                $this->httpProxy,
                $this->httpProxyPort
            );
            // $this->_initializeSession();
            if ($this->config['browser'] == 'firefox') {
               $this->profile = $profile;
            }
            $this->caps = $caps;
        } catch (WebDriverCurlException $e) {
            throw new ConnectionException($e->getMessage() . "\n \nPlease make sure that Selenium Server or PhantomJS is running.");
        }
//        $this->webDriver->manage()->timeouts()->implicitlyWait($this->config['wait']); # Disabled by firefox 57
        $this->initialWindowSize();
  }
  
  public function restoreDefaultWindowSize() {
      $this->initialWindowSize();
  }
  
  public function setLanguage($language = "en") 
    {
        $this->langBrowser = $language;
        if ($this->config['browser'] == 'firefox') {
            $this->profile->setPreference("intl.accept_languages", $this->langBrowser);
            $this->caps->setCapability(FirefoxDriver::PROFILE, $this->profile);
        }
        if ($this->config['browser'] == 'chrome') {
            $profile = new ChromeOptions();
            $profile->addArguments(["--lang=".$language,"--disable-extensions"]);
            $profile->setExperimentalOption('prefs', ['intl.accept_languages' => $language]);
            $this->caps->setCapability(ChromeOptions::CAPABILITY, $profile);
        }
        //$this->_capabilities(function($currentCapabilities) {
        //    return $this->caps;
        //});
        $this->capabilities = $this->caps;
        $this->_closeSession($this->webDriver);
        $this->_initializeSession();
    }

    public function _before(\Codeception\TestInterface $test) {
        echo "   Waiting before start to ensure cookies deleted.\n";
        //$this->webDriver->manage()->deleteAllCookies();
        sleep(1);
        parent::_before($test);
    }
    public function _after(\Codeception\TestInterface $test) {
        $this->webDriver->executeScript("var id = window.setTimeout(function() {}, 0); while (id--) { window.clearTimeout(id); }; "
            . "id = window.setInterval(function() {}, 0);  while (id--) { window.clearInterval(id); }; return true;");
        echo "   JS timeouts stopped.\n";
        sleep(1);
        try {
            parent::_after($test);
        } catch (\Exception $e) {
            echo "Exeption: function _after " . $e->getMessage() . "\n";
            $this->deleteAllCookies();
        }
    }
    
    function deleteAllCookies() {
        $this->webDriver->manage()->deleteAllCookies();
        //$this->webDriver->getSessionStorage()->clear(); //?
        //$this->webDriver->getLocalStorage()->clear(); //?
        //$this->webDriver->executeScript("window.sessionStorage.clear();");
        //$this->webDriver->executeScript("window.localStorage.clear();");
        echo "   cookies deleted.\n";
    }

    public function seeFullUrlEquals($expected)
    {
      $actual = $this->webDriver->getCurrentURL();
      $this->assertEquals($expected, $actual);
    }
    public function dontSeeFullUrlEquals($expected)
    {
      $actual = $this->webDriver->getCurrentURL();
      $this->assertNotEquals($expected, $actual);
    }
    
    // save full copy of database to tests/_debug/$name.sql
    //  if this file does not exists, or if (force is true or omitted)
    public function makeCheckpoint($name, $forceToDo = false) {
       if (file_exists("tests/_debug/".$name.".sql") && !$forceToDo) {
           echo "   Checkpoing tests/_debug/".$name.".sql already exist.\n";
       } else {
           echo "   Making checkpoing tests/_debug/".$name.".sql...";
           $params = $this->config['dsn'];
           $command = "echo SET foreign_key_checks=0\; > tests/_debug/".$name.".sql; /usr/bin/mysqldump --add-drop-table ".$params." >> tests/_debug/".$name.".sql 2> /tmp/checkpoint.log; echo SET foreign_key_checks=1\; >> tests/_debug/".$name.".sql;";
           exec($command, $output, $result);
           if($result===0) {
               echo " done.\n";
           } else {
               echo " fail!\n";
           }
       }
    }
   
    

    // restore database from tests/_debug/$name.sql
    public function restoreCheckpoint($name)
    {
       if (file_exists("tests/_debug/".$name.".sql")) {
           echo "   Restoring checkpoing tests/_debug/".$name.".sql...";
           $params = $this->config['dsn'];
           $command = "/usr/bin/mysql ".$params." < tests/_debug/".$name.".sql 2> /tmp/checkpointin.log";
           exec($command, $output, $result);
           if($result===0) {
               echo " done.\n";
           } else {
               echo " fail!\n";
           }
       } else {
           echo "   Can'd found the file tests/_debug/".$name.".sql...";
       }
   }
    
    
}
