<?php


use common\models\PaymentExchangeRate;

class StorePricerTest extends \test\unit\CodeceptionTestCase
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $transaction;
    
    /**
     *
     * @var common\models\PaymentExchangeRate 
     */
    protected function _before()
    {        
        $this->transaction = app('db')->beginTransaction();
        $this->rate = PaymentExchangeRate::find()->latest();
    }

    protected function _after()
    {
        if($this->transaction){
            $this->transaction->rollBack();
        }
    }

    public function testPriceConverter()
    {
        $rate = $this->rate;
        $base = $rate->base;
        $testPrice = 133.5123;
        $convertedPrice = $rate->convert($testPrice, $base, 'RUB');
        
        // ASSERT 1
        $testRubPrice = $rate->getCurrencyRate('RUB')*$testPrice;
        $this->assertEquals(
            true, $testRubPrice == $convertedPrice, 
            sprintf('price converted %s result %s', $testRubPrice, $convertedPrice)
        );
        
        // ASSERT 2
        $price2 = $rate->convert($convertedPrice, 'RUB', $base);
        $priceBack = $rate->convert($price2, $base, 'RUB');
        $this->assertEquals(
            true, $priceBack == $convertedPrice, 
            sprintf('price converted back %s, %s', $convertedPrice, $price2)
        );
        // ASSERT 3
        
        // convert from two diff currencies to base
        $mixConvertRubTest = $rate->convert($testPrice, 'EUR', 'RUB');
        // check in two steps
        $mixConvertUsd = $rate->convert($testPrice, 'EUR', $base);
        $mixConvertRub = $rate->convert($mixConvertUsd, $base, 'RUB');
        $this->assertEquals(
            true, $mixConvertRub == $mixConvertRubTest, 
            sprintf('price mix converted need %s  was %s', $mixConvertRub, $mixConvertRubTest)
        );
        
        // ASSERT 4 - 
        $convertedPrice2 = $rate->convert($testPrice, $base, $base);
        $this->assertEquals(
            true, $testPrice == $convertedPrice2, 
            sprintf('price the same %s  was %s', $convertedPrice2, $testPrice)
        );
    }

    /**
     * get shipment price
     *
     * @TODO not ready
     *
     * @param string $deliveryType
     * @param float $modelWeight
     * @return array - price + currency (default USD)
     */
    private static function getShipmentPrice($deliveryType, $modelWeight)
    {
        $deliveryObj = common\models\DeliveryType::findOne(['title' => $deliveryType]);
        // fake data @TODO replace to api call to calculate
        return  ['price'=>($deliveryObj->id + $modelWeight) / 3, 'currency'=>'USD'];
    }

    /**
     * test buy process
     * 1. select model to buy
     * 2. add shipping address
     * 3. create order
     * 4.
     *
     * @param int $userId
     * @param string $userCurrency
     * @param bool $storeUnitIn
     * @param string $material
     * @param string $color
     * @throws Exception
     */
    public function buyModel($userId = 0, $userCurrency = 'USD', $storeUnitIn = false, $material = 'PLA', $color = 'blue')
    {
        // create new order 
        // current rate
        $rate = $this->rate;
        $this->tester->am('user');
        
        $this->tester->amGoingTo('buy model');
        $this->tester->amGoingTo('select model to buy');
        /** @var \common\models\StoreUnit $storeUnit */
        $storeUnit = $storeUnitIn ? $storeUnitIn : \common\models\StoreUnit::find()
            ->with(['model3d', 'model3d.model3dParts'])
            ->where(['status'=>'published'])->one();
        $storeUnitIds = [$storeUnit->id]; // store unit to buy
        
        $this->tester->amGoingTo('select material and color to print with');
        $unitOptions = [];
        foreach($storeUnitIds as $storeUnitId){
            $unitOptions[$storeUnitId] = ['color' => $color, 'material' => $material];
        }

        $this->tester->amGoingTo('select printer to buy');
        /** @var \common\models\PsPrinter $psPrinter */
        $psPrinter = \common\models\PsPrinter::find()->where(['moderator_status'=>'new'])->one();
        
        
        // get price for printer
        $modelWeight = $storeUnit->model3d->getWeight(); // TODO: ALERT COLOR TITLE Model3d::getWeight($storeUnit->model3d, $material)
        
        // ASSERT model weight in gramm
        $this->assertEquals(true, $modelWeight > 0, 'model weight is 0 ');
        $printPriceDefault = $psPrinter->getPrintPrice($modelWeight, $material, $color);   
        $printPrice = $printPriceDefault;
        
        // ASSERT print price > 0
        $this->assertEquals(
            true, $printPrice > 0, 
            sprintf('material %s weight %s price %', $material, $modelWeight, $printPrice)
        );
        
        $this->tester->amGoingTo('add shipping address');
        /** @var \common\models\UserAddress $shipAddress */
        $shipAddress = common\models\UserAddress::addRecord([
            'user_id' => $userId,
            'created_at' => dbexpr('NOW()'),
            'contact_name' => 'Bot Tester',
            'country_id' => 1,
            'region' => 'Region',
            'city' => 'Test City',
            'address' => 'Address',
            'extended_address' => 'House 2B, Building 1A',
            'zip_code' => '123101',
            'type' => 'ship'
        ]);        
        // ASSERT shipment added
        $this->assertEquals(true, $shipAddress->id > 0);
        
        $this->tester->amGoingTo('Select delivery typ');
        $deliveryType = 'Pickup';
        
        $this->tester->amGoingTo('pay for model');

        /** @var \common\models\StoreOrder $order */
        $order = \common\models\StoreOrder::addRecord([
            'user_id' => $userId,
            'ship_address_id' => $shipAddress->id,
            'bill_address_id' => $shipAddress->id,
            'price_total' => 0,
            'price_currency' => $userCurrency,
            'order_status' => 'new'
        ]);  
        
        common\models\StorePricer::initPricer([
            'user_id' => $userId, 
            'order' => $order,
            'rate' => $rate
        ]);
        $orderId = $order->id;
        $this->tester->comment('order # ' . $order->id);
        
        // ASSERT order created
        $this->assertEquals(true, $order->id > 0);        
        $result = $order->bindItems($storeUnitIds, $unitOptions);
        
        // ASSERT items added
        $this->assertEquals(true, count($result) > 0, 'bindItems');
        
        //** START pricer **/
        
        // 1) Add model price
        $modelPriceDefault = $storeUnit->getModelPrice(); 
        
        $modelPrice = $rate->convert($modelPriceDefault, $storeUnit->price_currency, $userCurrency);
        $modelPriceUsd = $rate->convert($modelPriceDefault, $storeUnit->price_currency, 'USD');
        \common\models\StorePricer::addModelPrice($orderId, $modelPrice, $storeUnit->model3d_id);
        // 2) Add print price
        \common\models\StorePricer::addPrint($orderId, $printPrice);
        // 3)  Add shipment price
        $shipmentPriceInf = self::getShipmentPrice($deliveryType, $modelWeight);
        $shipmentPrice = $rate->convert($shipmentPriceInf['price'], $shipmentPriceInf['currency'], $userCurrency);
        
        // ASSERT shipment price
        $this->assertEquals(true, $shipmentPrice > 0, 'shipment price ' . $shipmentPrice);
        \common\models\StorePricer::addShipment($orderId, $shipmentPrice);
        \common\models\StorePricer::addPackage($orderId, $shipmentPrice/2);
        // 4) Add our fee
        $feePriceUsd = 0;
        $feePrice = $rate->convert($feePriceUsd, 'USD', $userCurrency);
        \common\models\StorePricer::addFee($orderId, $feePrice);
        
        $needPrices = [$modelPrice, $printPrice, $shipmentPrice, $shipmentPrice/2, $feePrice];
        foreach ($needPrices as $k => $v) {
            $needPrices[$k] = round($v, 4);
        }
        $needPricesSum = array_sum($needPrices);
        $needTotal = round($needPricesSum, 4);
        $calcTotal = common\models\StorePricer::getTotal($orderId);
        $priceUpdated = common\models\StorePricer::refershTotal($orderId);

        // ASSERT total price calculation
        $this->assertEquals(true, $priceUpdated);
        $this->assertEquals(true, 
            $needTotal == $calcTotal, 
            sprintf("need price %s has calc %s", $needTotal, $calcTotal)
        );
    }
}
