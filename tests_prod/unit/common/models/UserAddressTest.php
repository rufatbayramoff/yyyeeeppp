<?php

/**
 * Date: 02.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class UserAddressTest extends \test\unit\CodeceptionTestCase
{
    public function testRequiredRegion()
    {
        $address = new \common\models\UserAddress();
        $ruCountry = \common\models\GeoCountry::findOne(['iso_code'=>'RU']);
        $address->populateRelation('country', $ruCountry);
        $address->setAttributes([
            'country_id' => $ruCountry->id,
            'user_id' => 1,
            'region' => 'Not empty',
            'city' => 'city',
            'address' => 'address',
        ]);
        $result = $address->validate();
        $this->assertEquals(true, $result, 'validation');
        $this->assertEquals(true, $address->isRegionRequired(), 'is region required');
        $address->region = '';
        $this->assertEquals(false, $address->validate(), 'validation empty region');
    }

    public function testRequireRegionEmpty()
    {
        $address = new \common\models\UserAddress();
        $ruCountry = \common\models\GeoCountry::findOne(['iso_code'=>'RU']);
        $address->populateRelation('country', $ruCountry);
        $address->setAttributes([
            'country_id' => $ruCountry->id,
            'user_id' => 1,
            'city' => 'city',
            'address' => 'address',
        ]);
        $result = $address->validate();
        $this->assertEquals(false, $result, 'validation');
        $this->assertEquals(true, $address->isRegionRequired(), 'is region required');
    }

}
