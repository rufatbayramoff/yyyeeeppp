<?php


namespace common\modules\seo\placeholders;

use Codeception\Util\Stub;
use common\models\base\PsPrinter;
use common\models\PrinterMaterial;
use common\models\Ps;
use common\models\User;
use test\unit\CodeceptionTestCase;
use yii\db\ActiveQueryInterface;

/**
 * Date: 20.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class PrintServicePlaceholderTest extends CodeceptionTestCase
{

    public function testGetPlaceholders()
    {

        $ph = new PrintServicePlaceholder();

        self::assertTrue(in_array('%psTitle%', $ph->getPlaceholders()));

    }

    public function testGetFilledPlaceholders()
    {
        $stubUser = Stub::make(User::class, ['username'=>'ps1']);
        $stubPrinter = Stub::make(
            PsPrinter::class,
            [
                'title'        => 'Printer Title',
                'description' => 'ps1 info',
                'getMaterials' => function () {
                    return Stub::makeEmpty(ActiveQueryInterface::class, [
                        'all' => function(){
                            $class1 =  new \stdClass();
                            $class1->material = new PrinterMaterial(['filament_title' => 'mat1']);
                            $class2 =  new \stdClass();
                            $class2->material = new PrinterMaterial(['filament_title' => 'mat2']);

                            return [ $class1, $class2];
                        }
                    ]);
                }
            ]
        );

        $ps = Stub::make( Ps::class,
            [
                'title'       => 'Ps title',
                'description' => 'Ps description'
            ]
        );
        $ps->populateRelation('psPrinters', [$stubPrinter]);
        $ps->populateRelation('user', $stubUser);

        $psPh = new PrintServicePlaceholder();
        $psPh->psLocation = 'Region, City'; // for tests
        $psPh->setDataObject($ps);

        self::assertEquals('Ps description', $psPh->getFilledPlaceholders()['psDescription']);
        self::assertEquals('Ps title', $psPh->getFilledPlaceholders()['psTitle']);
        self::assertEquals('ps1', $psPh->getFilledPlaceholders()['psUser']);
        self::assertEquals('Printer Title', $psPh->getFilledPlaceholders()['psPrinters']);
        self::assertEquals('mat1, mat2', $psPh->getFilledPlaceholders()['psMaterials']);

    }
}
