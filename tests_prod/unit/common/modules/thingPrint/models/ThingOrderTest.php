<?php
/**
 * Date: 17.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\models;
require_once __DIR__ . '/../DumpHelper.php';

use common\modules\thingPrint\components\DumpHelper;
use test\unit\CodeceptionTestCase;

class ThingOrderTest extends CodeceptionTestCase
{

    public function testGetOrder()
    {
        $json = self::getDump();

        $thingOrder = new ThingOrder($json);
        self::assertEquals($thingOrder->charges, $json['charges']);
        self::assertTrue($thingOrder->charges[0]['amount'] > 0);

        self::assertNotEmpty($thingOrder->status);
        self::assertTrue($thingOrder->is_shipped===0 || $thingOrder->is_shipped===1);
    }

    public function testThingOrder()
    {
        $json = '{
                "event": "refund_requested",
                "order": {
                    "id": "1234",
                    "thing_id": "1234",
                    "amount": "14.53",
                    "add_date": "2016-04-28 16:28:40",
                    "modified_date": "2016-04-29 08:03:38",
                    "charges": [
                    {
                      "name": "Shipping Price",
                      "amount": "2.77"
                    },
                    {
                      "name": "Print Price",
                      "amount": "6.51"
                    },
                    {
                      "name": "Fee",
                      "amount": "0.99"
                    },
                    {
                      "name": "Tip",
                      "amount": "5.00"
                    }
                  ],
                  "fees": {
                    "platform fee": "3.08",
                    "transaction fee": "0.74"
                  },
                    "validated_shipping_address": "123 Some St., Somecity, Somestate, 12345",
                    "is_shipped": "true",
                    "status": "status",
                    "note": "a note"
                }
            }';
        $json = json_decode($json, true);
        $thingOrder = new ThingOrder($json['order']);

        self::assertEquals('3.08', $thingOrder->fees['platform fee']);
        self::assertEquals('2.77', $thingOrder->charges[0]['amount']);
    }
    /**
     * for tests
     * @return mixed
     */
    public static function getDump()
    {
        return DumpHelper::api_getOrder();
    }
}
