<?php
/**
 * Date: 27.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\factories;

require_once __DIR__ . '/../DumpHelper.php';

use common\models\ThingiverseUser;
use common\models\UserAddress;
use common\modules\thingPrint\models\ThingUser;
use common\modules\thingPrint\models\ThingUserAddress;
use test\unit\CodeceptionTestCase;

class UserAddressFactoryTest extends CodeceptionTestCase
{

    public function testCreate()
    {
        $input = json_decode('{"id":31,"name":"De Facto","organisation":null,"street_address":"2221 River Bend Rd  ","dependent_locality":null,"city":"Plover","admin_area":"WI","postal_code":"54467","sorting_code":null,"country":"US"}', true);
        $thingAddress = new ThingUserAddress($input);
        $thingiverseUser = new ThingiverseUser();
        $thingiverseUser->user_id = 1;
        $address = UserAddressFactory::create($thingAddress, $thingiverseUser);

        self::assertInstanceOf(UserAddress::class, $address, 'user address object');
        self::assertEquals($address->address, $input['street_address']);
        self::assertEquals($address->city, $input['city']);
        self::assertEquals($address->region, $input['admin_area']);
        self::assertEquals($address->zip_code, $input['postal_code']);
        self::assertEquals($address->country->iso_code, $input['country']);
        self::assertEquals($address->contact_name, $input['name']);

    }
}
