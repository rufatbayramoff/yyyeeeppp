<?php
/**
 * Date: 17.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\components;

require_once __DIR__ . '/../DumpHelper.php';

use Codeception\Util\Stub;
use common\models\Model3d;
use common\modules\thingPrint\factories\Model3dFromThingFactory;
use common\modules\thingPrint\models\ThingFile;
use test\unit\CodeceptionTestCase;

class Model3dFromThingFactoryTest extends CodeceptionTestCase
{

    public function testDump()
    {
        self::assertEquals(1, 1);
    }

    public function testThingFileFilter()
    {
        $api = new ThingiverseApi();
        $apiWrapper = new ThingiverseApiWrapper($api);
        $json = json_decode($this->getJsonFiles());
        $thingFiles = [];
        foreach ($json as $file) {
            $thingFiles[] = new ThingFile($file);
        }
        $thingFiles = $apiWrapper->validateAndTrimThingFiles($thingFiles);

        self::assertEquals(count($thingFiles), 2);
    }

    private function getJsonFiles()
    {
        return '[
  {
    "id": 1721765,
    "name": "excenter_FIXED.stl",
    "size": 1168907,
    "url": "https://api-sandbox.thingiverse.com/files/1721765",
    "public_url": "https://sandbox.thingiverse.com/download:1721765",
    "download_url": "https://api-sandbox.thingiverse.com/files/1721765/download",
    "threejs_url": "http://staging-cdn.thingiverse.com/threejs_json/48/06/a3/1e/a5/df83a22dde0af6e3d9cd401d64ec5ce6.js",
    "thumbnail": "http://sandbox.thingiverse.com/img/default/Gears_thumb_medium.jpg",
    "default_image": null,
    "date": "2016-03-29 07:52:54",
    "formatted_size": "1 mb",
    "meta_data": []
  },
  {
    "id": 1721767,
    "name": "excenter_FIXED.stl",
    "size": 6826584,
    "url": "https://api-sandbox.thingiverse.com/files/1721767",
    "public_url": "https://sandbox.thingiverse.com/download:1721767",
    "download_url": "https://api-sandbox.thingiverse.com/files/1721767/download",
    "threejs_url": "http://staging-cdn.thingiverse.com/threejs_json/17/81/e4/e2/48/df83a22dde0af6e3d9cd401d64ec5ce6.js",
    "thumbnail": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_medium.jpg",
    "default_image": {
      "id": 2793469,
      "url": "http://staging-cdn.thingiverse.com/assets/56/86/ef/1d/28/excenter_FIXED.stl",
      "name": "df83a22dde0af6e3d9cd401d64ec5ce6.png",
      "sizes": [
        {
          "type": "thumb",
          "size": "large",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_large.jpg"
        },
        {
          "type": "thumb",
          "size": "medium",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_medium.jpg"
        },
        {
          "type": "thumb",
          "size": "small",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_small.jpg"
        },
        {
          "type": "thumb",
          "size": "tiny",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "featured",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_preview_featured.jpg"
        },
        {
          "type": "preview",
          "size": "card",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_preview_card.jpg"
        },
        {
          "type": "preview",
          "size": "large",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_preview_large.jpg"
        },
        {
          "type": "preview",
          "size": "medium",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_preview_medium.jpg"
        },
        {
          "type": "preview",
          "size": "small",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_preview_small.jpg"
        },
        {
          "type": "preview",
          "size": "birdwing",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_preview_birdwing.jpg"
        },
        {
          "type": "preview",
          "size": "tiny",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_preview_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "tinycard",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_preview_tinycard.jpg"
        },
        {
          "type": "display",
          "size": "large",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_display_large.jpg"
        },
        {
          "type": "display",
          "size": "medium",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_display_medium.jpg"
        },
        {
          "type": "display",
          "size": "small",
          "url": "http://staging-cdn.thingiverse.com/renders/bc/ca/a2/55/8c/df83a22dde0af6e3d9cd401d64ec5ce6_display_small.jpg"
        }
      ],
      "added": "2016-03-29T08:11:34+00:00"
    },
    "date": "2016-03-29 08:10:09",
    "formatted_size": "6 mb",
    "meta_data": []
  },
  {
    "id": 1721768,
    "name": "excenter_FIXED.stl",
    "size": 6826584,
    "url": "https://api-sandbox.thingiverse.com/files/1721768",
    "public_url": "https://sandbox.thingiverse.com/download:1721768",
    "download_url": "https://api-sandbox.thingiverse.com/files/1721768/download",
    "threejs_url": "http://staging-cdn.thingiverse.com/threejs_json/36/71/58/6a/33/df83a22dde0af6e3d9cd401d64ec5ce6.js",
    "thumbnail": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_medium.jpg",
    "default_image": {
      "id": 2793470,
      "url": "http://staging-cdn.thingiverse.com/assets/ea/36/00/c9/62/excenter_FIXED.stl",
      "name": "df83a22dde0af6e3d9cd401d64ec5ce6.png",
      "sizes": [
        {
          "type": "thumb",
          "size": "large",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_large.jpg"
        },
        {
          "type": "thumb",
          "size": "medium",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_medium.jpg"
        },
        {
          "type": "thumb",
          "size": "small",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_small.jpg"
        },
        {
          "type": "thumb",
          "size": "tiny",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_thumb_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "featured",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_preview_featured.jpg"
        },
        {
          "type": "preview",
          "size": "card",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_preview_card.jpg"
        },
        {
          "type": "preview",
          "size": "large",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_preview_large.jpg"
        },
        {
          "type": "preview",
          "size": "medium",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_preview_medium.jpg"
        },
        {
          "type": "preview",
          "size": "small",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_preview_small.jpg"
        },
        {
          "type": "preview",
          "size": "birdwing",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_preview_birdwing.jpg"
        },
        {
          "type": "preview",
          "size": "tiny",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_preview_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "tinycard",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_preview_tinycard.jpg"
        },
        {
          "type": "display",
          "size": "large",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_display_large.jpg"
        },
        {
          "type": "display",
          "size": "medium",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_display_medium.jpg"
        },
        {
          "type": "display",
          "size": "small",
          "url": "http://staging-cdn.thingiverse.com/renders/2e/c9/47/5b/95/df83a22dde0af6e3d9cd401d64ec5ce6_display_small.jpg"
        }
      ],
      "added": "2016-03-29T08:16:22+00:00"
    },
    "date": "2016-03-29 08:15:00",
    "formatted_size": "6 mb",
    "meta_data": []
  },
  {
    "id": 1721741,
    "name": "excenter.STL",
    "size": 135665,
    "url": "https://api-sandbox.thingiverse.com/files/1721741",
    "public_url": "https://sandbox.thingiverse.com/download:1721741",
    "download_url": "https://api-sandbox.thingiverse.com/files/1721741/download",
    "threejs_url": "http://staging-cdn.thingiverse.com/threejs_json/77/99/fa/5e/ec/19aa63040e0ecfe1524380dbd1bbd78a.js",
    "thumbnail": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_thumb_medium.jpg",
    "default_image": {
      "id": 2793440,
      "url": "http://staging-cdn.thingiverse.com/assets/48/bb/71/9c/15/excenter.STL",
      "name": "19aa63040e0ecfe1524380dbd1bbd78a.png",
      "sizes": [
        {
          "type": "thumb",
          "size": "large",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_thumb_large.jpg"
        },
        {
          "type": "thumb",
          "size": "medium",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_thumb_medium.jpg"
        },
        {
          "type": "thumb",
          "size": "small",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_thumb_small.jpg"
        },
        {
          "type": "thumb",
          "size": "tiny",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_thumb_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "featured",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_preview_featured.jpg"
        },
        {
          "type": "preview",
          "size": "card",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_preview_card.jpg"
        },
        {
          "type": "preview",
          "size": "large",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_preview_large.jpg"
        },
        {
          "type": "preview",
          "size": "medium",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_preview_medium.jpg"
        },
        {
          "type": "preview",
          "size": "small",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_preview_small.jpg"
        },
        {
          "type": "preview",
          "size": "birdwing",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_preview_birdwing.jpg"
        },
        {
          "type": "preview",
          "size": "tiny",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_preview_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "tinycard",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_preview_tinycard.jpg"
        },
        {
          "type": "display",
          "size": "large",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_display_large.jpg"
        },
        {
          "type": "display",
          "size": "medium",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_display_medium.jpg"
        },
        {
          "type": "display",
          "size": "small",
          "url": "http://staging-cdn.thingiverse.com/renders/d6/73/d5/0e/e1/19aa63040e0ecfe1524380dbd1bbd78a_display_small.jpg"
        }
      ],
      "added": "2016-03-28T09:15:52+00:00"
    },
    "date": "2016-03-28 09:15:26",
    "formatted_size": "132 kb",
    "meta_data": []
  }
]';

    }
}
