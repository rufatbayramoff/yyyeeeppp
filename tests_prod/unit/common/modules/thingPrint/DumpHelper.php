<?php
namespace common\modules\thingPrint\components;
/**
 * Date: 19.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class DumpHelper
{


    public static function api_getMe()
    {
        $json = '{"id":1265469,"name":"DeFa200","first_name":"De","last_name":"Facto","full_name":"De Facto","url":"https:\/\/api.thingiverse.com\/users\/DeFa200","public_url":"https:\/\/www.thingiverse.com\/DeFa200","thumbnail":"https:\/\/www.thingiverse.com\/img\/default\/avatar\/avatar_default_thumb_medium.jpg","bio":"","bio_html":"","location":"","country":"RU","industry":"Education","registered":"2017-01-16T09:47:40+00:00","last_active":"2017-01-31T12:18:53+00:00","cover_image":null,"things_url":"https:\/\/api.thingiverse.com\/users\/DeFa200\/things","copies_url":"https:\/\/api.thingiverse.com\/users\/DeFa200\/copies","likes_url":"https:\/\/api.thingiverse.com\/users\/DeFa200\/likes","printers":[],"types":[],"skill_level":"","default_license":"cc-sa","current_app":{"id":1247,"name":"TestPrint","url":"https:\/\/api.thingiverse.com\/apps\/1247","public_url":"https:\/\/www.thingiverse.com\/app:1247","thumbnail":"https:\/\/www.thingiverse.com\/img\/default\/Gears_thumb_medium.jpg","creator":{"id":1234446,"name":"DeFa100","first_name":"De","last_name":"Facto","url":"https:\/\/api.thingiverse.com\/users\/DeFa100","public_url":"https:\/\/www.thingiverse.com\/DeFa100","thumbnail":"https:\/\/www.thingiverse.com\/img\/default\/avatar\/avatar_default_thumb_medium.jpg"},"is_published":true,"is_approved":false},"email":""}';

        return json_decode($json, true);
    }

    public static function api_getThingFiles15501()
    {
        $json = '[
  {
    "id": 300180,
    "name": "Cover.stl",
    "size": 88240,
    "url": "https://api-sandbox.thingiverse.com/files/300180",
    "public_url": "https://sandbox.thingiverse.com/download:300180",
    "download_url": "https://api-sandbox.thingiverse.com/files/300180/download",
    "threejs_url": "http://cdn.thingiverse.com/threejs_json/39/ce/10/70/26/0f916ab7Cover.js",
    "thumbnail": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_thumb_medium.jpg",
    "default_image": {
      "id": 502616,
      "url": "http://cdn.thingiverse.com/assets/ef/4d/b4/7f/51/Cover.stl",
      "name": "Cover.jpg",
      "sizes": [
        {
          "type": "thumb",
          "size": "large",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_thumb_large.jpg"
        },
        {
          "type": "thumb",
          "size": "medium",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_thumb_medium.jpg"
        },
        {
          "type": "thumb",
          "size": "small",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_thumb_small.jpg"
        },
        {
          "type": "thumb",
          "size": "tiny",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_thumb_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "featured",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_preview_featured.jpg"
        },
        {
          "type": "preview",
          "size": "card",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_preview_card.jpg"
        },
        {
          "type": "preview",
          "size": "large",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_preview_large.jpg"
        },
        {
          "type": "preview",
          "size": "medium",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_preview_medium.jpg"
        },
        {
          "type": "preview",
          "size": "small",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_preview_small.jpg"
        },
        {
          "type": "preview",
          "size": "birdwing",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover.jpg"
        },
        {
          "type": "preview",
          "size": "tiny",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_preview_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "tinycard",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_preview_tinycard.jpg"
        },
        {
          "type": "display",
          "size": "large",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_display_large.jpg"
        },
        {
          "type": "display",
          "size": "medium",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_display_medium.jpg"
        },
        {
          "type": "display",
          "size": "small",
          "url": "http://cdn.thingiverse.com/renders/0a/b5/d4/bd/50/Cover_display_small.jpg"
        }
      ],
      "added": "2013-09-23T05:40:30+00:00"
    },
    "date": "2013-09-23 09:35:53",
    "formatted_size": "86 kb",
    "meta_data": []
  },
  {
    "id": 300181,
    "name": "Enclosure.stl",
    "size": 356361,
    "url": "https://api-sandbox.thingiverse.com/files/300181",
    "public_url": "https://sandbox.thingiverse.com/download:300181",
    "download_url": "https://api-sandbox.thingiverse.com/files/300181/download",
    "threejs_url": "http://cdn.thingiverse.com/threejs_json/db/9b/de/5a/a5/c1c73dc7Enclosure.js",
    "thumbnail": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_thumb_medium.jpg",
    "default_image": {
      "id": 502618,
      "url": "http://cdn.thingiverse.com/assets/70/3a/5b/2e/7e/Enclosure.stl",
      "name": "Enclosure.jpg",
      "sizes": [
        {
          "type": "thumb",
          "size": "large",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_thumb_large.jpg"
        },
        {
          "type": "thumb",
          "size": "medium",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_thumb_medium.jpg"
        },
        {
          "type": "thumb",
          "size": "small",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_thumb_small.jpg"
        },
        {
          "type": "thumb",
          "size": "tiny",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_thumb_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "featured",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_preview_featured.jpg"
        },
        {
          "type": "preview",
          "size": "card",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_preview_card.jpg"
        },
        {
          "type": "preview",
          "size": "large",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_preview_large.jpg"
        },
        {
          "type": "preview",
          "size": "medium",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_preview_medium.jpg"
        },
        {
          "type": "preview",
          "size": "small",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_preview_small.jpg"
        },
        {
          "type": "preview",
          "size": "birdwing",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure.jpg"
        },
        {
          "type": "preview",
          "size": "tiny",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_preview_tiny.jpg"
        },
        {
          "type": "preview",
          "size": "tinycard",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_preview_tinycard.jpg"
        },
        {
          "type": "display",
          "size": "large",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_display_large.jpg"
        },
        {
          "type": "display",
          "size": "medium",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_display_medium.jpg"
        },
        {
          "type": "display",
          "size": "small",
          "url": "http://cdn.thingiverse.com/renders/c6/4f/57/8a/4d/Enclosure_display_small.jpg"
        }
      ],
      "added": "2013-09-23T05:42:39+00:00"
    },
    "date": "2013-09-23 09:35:53",
    "formatted_size": "348 kb",
    "meta_data": []
  },
  {
    "id": 300188,
    "name": "Customizable_Enclosure.scad",
    "size": 5281,
    "url": "https://api-sandbox.thingiverse.com/files/300188",
    "public_url": "https://sandbox.thingiverse.com/download:300188",
    "download_url": "https://api-sandbox.thingiverse.com/files/300188/download",
    "threejs_url": "",
    "thumbnail": "http://sandbox.thingiverse.com/img/default/Gears_thumb_medium.jpg",
    "default_image": null,
    "date": "2013-09-23 09:45:39",
    "formatted_size": "5 kb",
    "meta_data": []
  }
]';
        return json_decode($json);
    }
    /**
     *  result from this request
     *   $thingApi = new ThingiverseApi($accessToken);
     *    $thingApi->getThingFiles($thingId);
     *    return $thingApi->response_data;
     *
     * @return mixed
     */
    public static function api_getThingFiles()
    {
        $json = '[{"id":3205123,"name":"l-arm.stl","size":2765784,"url":"https:\/\/api.thingiverse.com\/files\/3205123","public_url":"https:\/\/www.thingiverse.com\/download:3205123","download_url":"https:\/\/api.thingiverse.com\/files\/3205123\/download","threejs_url":"https:\/\/cdn.thingiverse.com\/threejs_json\/d0\/ca\/12\/5e\/8f\/139f1d269ed874365e9529e947f9bbc7.js","thumbnail":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_thumb_medium.jpg","default_image":{"id":5202567,"url":"https:\/\/cdn.thingiverse.com\/assets\/f5\/63\/af\/4d\/b3\/l-arm.stl","name":"139f1d269ed874365e9529e947f9bbc7.png","sizes":[{"type":"thumb","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_thumb_large.jpg"},{"type":"thumb","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_thumb_medium.jpg"},{"type":"thumb","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_thumb_small.jpg"},{"type":"thumb","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_thumb_tiny.jpg"},{"type":"preview","size":"featured","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_preview_featured.jpg"},{"type":"preview","size":"card","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_preview_card.jpg"},{"type":"preview","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_preview_large.jpg"},{"type":"preview","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_preview_medium.jpg"},{"type":"preview","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_preview_small.jpg"},{"type":"preview","size":"birdwing","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_preview_birdwing.jpg"},{"type":"preview","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_preview_tiny.jpg"},{"type":"preview","size":"tinycard","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_preview_tinycard.jpg"},{"type":"display","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_display_large.jpg"},{"type":"display","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_display_medium.jpg"},{"type":"display","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/ec\/7b\/be\/0e\/59\/139f1d269ed874365e9529e947f9bbc7_display_small.jpg"}],"added":"2017-01-17T14:13:53+00:00"},"date":"2017-01-17 14:13:39","formatted_size":"2 mb","meta_data":[]},{"id":3205124,"name":"r-arm.stl","size":3370484,"url":"https:\/\/api.thingiverse.com\/files\/3205124","public_url":"https:\/\/www.thingiverse.com\/download:3205124","download_url":"https:\/\/api.thingiverse.com\/files\/3205124\/download","threejs_url":"https:\/\/cdn.thingiverse.com\/threejs_json\/e6\/88\/7d\/c7\/e0\/df1314015badd13f50413e7549d4a9b5.js","thumbnail":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_thumb_medium.jpg","default_image":{"id":5202566,"url":"https:\/\/cdn.thingiverse.com\/assets\/5b\/f2\/55\/41\/4f\/r-arm.stl","name":"df1314015badd13f50413e7549d4a9b5.png","sizes":[{"type":"thumb","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_thumb_large.jpg"},{"type":"thumb","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_thumb_medium.jpg"},{"type":"thumb","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_thumb_small.jpg"},{"type":"thumb","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_thumb_tiny.jpg"},{"type":"preview","size":"featured","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_preview_featured.jpg"},{"type":"preview","size":"card","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_preview_card.jpg"},{"type":"preview","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_preview_large.jpg"},{"type":"preview","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_preview_medium.jpg"},{"type":"preview","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_preview_small.jpg"},{"type":"preview","size":"birdwing","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_preview_birdwing.jpg"},{"type":"preview","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_preview_tiny.jpg"},{"type":"preview","size":"tinycard","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_preview_tinycard.jpg"},{"type":"display","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_display_large.jpg"},{"type":"display","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_display_medium.jpg"},{"type":"display","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/9f\/0b\/ac\/9b\/28\/df1314015badd13f50413e7549d4a9b5_display_small.jpg"}],"added":"2017-01-17T14:13:52+00:00"},"date":"2017-01-17 14:13:40","formatted_size":"3 mb","meta_data":[]},{"id":3205125,"name":"torso.stl","size":12588584,"url":"https:\/\/api.thingiverse.com\/files\/3205125","public_url":"https:\/\/www.thingiverse.com\/download:3205125","download_url":"https:\/\/api.thingiverse.com\/files\/3205125\/download","threejs_url":"https:\/\/cdn.thingiverse.com\/threejs_json\/35\/74\/51\/17\/61\/d0b8d3a1353c0c55ba981539763d8283.js","thumbnail":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_thumb_medium.jpg","default_image":{"id":5202578,"url":"https:\/\/cdn.thingiverse.com\/assets\/7d\/8f\/c2\/02\/d2\/torso.stl","name":"d0b8d3a1353c0c55ba981539763d8283.png","sizes":[{"type":"thumb","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_thumb_large.jpg"},{"type":"thumb","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_thumb_medium.jpg"},{"type":"thumb","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_thumb_small.jpg"},{"type":"thumb","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_thumb_tiny.jpg"},{"type":"preview","size":"featured","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_preview_featured.jpg"},{"type":"preview","size":"card","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_preview_card.jpg"},{"type":"preview","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_preview_large.jpg"},{"type":"preview","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_preview_medium.jpg"},{"type":"preview","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_preview_small.jpg"},{"type":"preview","size":"birdwing","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_preview_birdwing.jpg"},{"type":"preview","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_preview_tiny.jpg"},{"type":"preview","size":"tinycard","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_preview_tinycard.jpg"},{"type":"display","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_display_large.jpg"},{"type":"display","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_display_medium.jpg"},{"type":"display","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/13\/e2\/82\/f7\/67\/d0b8d3a1353c0c55ba981539763d8283_display_small.jpg"}],"added":"2017-01-17T14:14:53+00:00"},"date":"2017-01-17 14:13:40","formatted_size":"12 mb","meta_data":[]},{"id":3205126,"name":"base.stl","size":42535784,"url":"https:\/\/api.thingiverse.com\/files\/3205126","public_url":"https:\/\/www.thingiverse.com\/download:3205126","download_url":"https:\/\/api.thingiverse.com\/files\/3205126\/download","threejs_url":"https:\/\/cdn.thingiverse.com\/threejs_json\/70\/6d\/df\/58\/8a\/57f312213586f4db5ce062b65d8a6737.js","thumbnail":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_thumb_medium.jpg","default_image":{"id":5202574,"url":"https:\/\/cdn.thingiverse.com\/assets\/31\/dc\/fd\/5b\/ec\/base.stl","name":"57f312213586f4db5ce062b65d8a6737.png","sizes":[{"type":"thumb","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_thumb_large.jpg"},{"type":"thumb","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_thumb_medium.jpg"},{"type":"thumb","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_thumb_small.jpg"},{"type":"thumb","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_thumb_tiny.jpg"},{"type":"preview","size":"featured","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_preview_featured.jpg"},{"type":"preview","size":"card","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_preview_card.jpg"},{"type":"preview","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_preview_large.jpg"},{"type":"preview","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_preview_medium.jpg"},{"type":"preview","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_preview_small.jpg"},{"type":"preview","size":"birdwing","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_preview_birdwing.jpg"},{"type":"preview","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_preview_tiny.jpg"},{"type":"preview","size":"tinycard","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_preview_tinycard.jpg"},{"type":"display","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_display_large.jpg"},{"type":"display","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_display_medium.jpg"},{"type":"display","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/02\/46\/d5\/f8\/d1\/57f312213586f4db5ce062b65d8a6737_display_small.jpg"}],"added":"2017-01-17T14:14:44+00:00"},"date":"2017-01-17 14:13:42","formatted_size":"40 mb","meta_data":[]}]';
        return json_decode($json);
    }

    public static function api_getThing()
    {
        $json = '{"id":2042991,"name":"Muscular discobolus almost no support","thumbnail":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_thumb_medium.JPG","url":"https:\/\/api.thingiverse.com\/things\/2042991","public_url":"https:\/\/www.thingiverse.com\/thing:2042991","creator":{"id":1163494,"name":"icefox1983","first_name":"Bin","last_name":"Hu","url":"https:\/\/api.thingiverse.com\/users\/icefox1983","public_url":"https:\/\/www.thingiverse.com\/icefox1983","thumbnail":"https:\/\/cdn.thingiverse.com\/renders\/87\/73\/96\/b4\/42\/77ebf484eb86cdb9f68b4a92a3f40690_thumb_medium.jpg"},"added":"2017-01-17T14:13:43+00:00","modified":"2017-01-17T14:13:43+00:00","is_published":true,"is_wip":false,"is_featured":false,"like_count":3,"is_liked":false,"collect_count":2,"is_collected":false,"default_image":{"id":5202561,"url":"https:\/\/cdn.thingiverse.com\/assets\/63\/dd\/0e\/04\/c4\/IMG_5639.JPG","name":"IMG_5639.JPG","sizes":[{"type":"thumb","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_thumb_large.JPG"},{"type":"thumb","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_thumb_medium.JPG"},{"type":"thumb","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_thumb_small.JPG"},{"type":"thumb","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_thumb_tiny.JPG"},{"type":"preview","size":"featured","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_preview_featured.JPG"},{"type":"preview","size":"card","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_preview_card.JPG"},{"type":"preview","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_preview_large.JPG"},{"type":"preview","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_preview_medium.JPG"},{"type":"preview","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_preview_small.JPG"},{"type":"preview","size":"birdwing","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_preview_birdwing.JPG"},{"type":"preview","size":"tiny","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_preview_tiny.JPG"},{"type":"preview","size":"tinycard","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_preview_tinycard.JPG"},{"type":"display","size":"large","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_display_large.JPG"},{"type":"display","size":"medium","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_display_medium.JPG"},{"type":"display","size":"small","url":"https:\/\/cdn.thingiverse.com\/renders\/61\/32\/b1\/b1\/da\/9072875ebfd7ee56358977195cce9773_display_small.JPG"}],"added":"2017-01-17T14:13:38+00:00"},"description":"I didn\'t make this scan; I found it here https:\/\/www.myminifactory.com\/object\/discobolus-at-the-british-museum-london-7896 (BTW their Scan The World program is fantastic!).\r\n\r\nI believe this discobolus may be a modern remake because the head position is the same as the wrongly restored (thus unique) British Museum replica (see http:\/\/www.thingiverse.com\/thing:1894078 and my no-support remix http:\/\/www.thingiverse.com\/thing:2026127), yet obviously different and noticeably more muscular. Authenticity aside, this muscular sculpture probably looks better on your table.\r\n\r\nSupport just isn\'t my thing (hate to remove it and hate it more when removing it leaves a scar on a beautiful model), so I cut the model up into four pieces that can be printed with almost no support and then glued together. See instruction below. My result (150% scale) came out pretty nice. Enjoy!","instructions":"","description_html":"<p>I didn\'t make this scan; I found it here <a rel=\"nofollow\" href=\"https:\/\/www.myminifactory.com\/object\/discobolus-at-the-british-museum-london-7896\">https:\/\/www.myminifactory.com\/object\/discobolus-at-the-british-museum-london-7896<\/a> (BTW their Scan The World program is fantastic!).<\/p>\n<p>I believe this discobolus may be a modern remake because the head position is the same as the wrongly restored (thus unique) British Museum replica (see <a rel=\"nofollow\" href=\"http:\/\/www.thingiverse.com\/thing:1894078\">http:\/\/www.thingiverse.com\/thing:1894078<\/a> and my no-support remix <a rel=\"nofollow\" href=\"http:\/\/www.thingiverse.com\/thing:2026127\">http:\/\/www.thingiverse.com\/thing:2026127<\/a>), yet obviously different and noticeably more muscular. Authenticity aside, this muscular sculpture probably looks better on your table.<\/p>\n<p>Support just isn\'t my thing (hate to remove it and hate it more when removing it leaves a scar on a beautiful model), so I cut the model up into four pieces that can be printed with almost no support and then glued together. See instruction below. My result (150% scale) came out pretty nice. Enjoy!<\/p>","instructions_html":"","details":"<h1 class=\"thing-component-header summary summary\">Summary<\/h1>\n<p>I didn\'t make this scan; I found it here <a rel=\"nofollow\" href=\"https:\/\/www.myminifactory.com\/object\/discobolus-at-the-british-museum-london-7896\">https:\/\/www.myminifactory.com\/object\/discobolus-at-the-british-museum-london-7896<\/a> (BTW their Scan The World program is fantastic!).<\/p>\n<p>I believe this discobolus may be a modern remake because the head position is the same as the wrongly restored (thus unique) British Museum replica (see <a rel=\"nofollow\" href=\"http:\/\/www.thingiverse.com\/thing:1894078\">http:\/\/www.thingiverse.com\/thing:1894078<\/a> and my no-support remix <a rel=\"nofollow\" href=\"http:\/\/www.thingiverse.com\/thing:2026127\">http:\/\/www.thingiverse.com\/thing:2026127<\/a>), yet obviously different and noticeably more muscular. Authenticity aside, this muscular sculpture probably looks better on your table.<\/p>\n<p>Support just isn\'t my thing (hate to remove it and hate it more when removing it leaves a scar on a beautiful model), so I cut the model up into four pieces that can be printed with almost no support and then glued together. See instruction below. My result (150% scale) came out pretty nice. Enjoy!<\/p>\n<h1 class=\"thing-component-header settings print-settings\">Print Settings<\/h1>\n<p class=\"detail-setting printer brand\"><strong>Printer Brand: <\/strong>\n         <p>Wanhao<\/p><\/p>                        <p class=\"detail-setting printer\"><strong>Printer: <\/strong>\n         <p>Wanhao Duplicator i3 V2<\/p><\/p>                        <p class=\"detail-setting rafts\"><strong>Rafts: <\/strong>\n         <p>No<\/p><\/p>                        <p class=\"detail-setting supports\"><strong>Supports: <\/strong>\n         <p>No<\/p><\/p>                        <p class=\"detail-setting resolution\"><strong>Resolution: <\/strong>\n         <p>0.8mm<\/p><\/p>                        <p class=\"detail-setting infill\"><strong>Infill: <\/strong>\n         <p>20% two steps gradual<\/p><\/p>                <br>        <p class=\"detail-setting notes\"><strong>Notes: <\/strong>\n        <\/p><p>Print base and left arm w\/o support. Print right arm with support everywhere at 75 degrees so only the four fingers get slight support. Print torso with support touching bed at 75 degrees so only two armpits get slight support. Clean and glue with E6000 (superglue dries too quickly for alignment adjustments). Put on table and admire ancient ingenuity!<\/p>\n","details_parts":[{"type":"summary","name":"Summary","required":"required","data":[{"content":"I didn\'t make this scan; I found it here https:\/\/www.myminifactory.com\/object\/discobolus-at-the-british-museum-london-7896 (BTW their Scan The World program is fantastic!).\r\n\r\nI believe this discobolus may be a modern remake because the head position is the same as the wrongly restored (thus unique) British Museum replica (see http:\/\/www.thingiverse.com\/thing:1894078 and my no-support remix http:\/\/www.thingiverse.com\/thing:2026127), yet obviously different and noticeably more muscular. Authenticity aside, this muscular sculpture probably looks better on your table.\r\n\r\nSupport just isn\'t my thing (hate to remove it and hate it more when removing it leaves a scar on a beautiful model), so I cut the model up into four pieces that can be printed with almost no support and then glued together. See instruction below. My result (150% scale) came out pretty nice. Enjoy!"}]},{"type":"settings","name":"Print Settings","data":[{"printer brand":"Wanhao","printer":"Wanhao Duplicator i3 V2","rafts":"No","supports":"No","resolution":"0.8mm","infill":"20% two steps gradual","notes":"Print base and left arm w\/o support. Print right arm with support everywhere at 75 degrees so only the four fingers get slight support. Print torso with support touching bed at 75 degrees so only two armpits get slight support. Clean and glue with E6000 (superglue dries too quickly for alignment adjustments). Put on table and admire ancient ingenuity!"}]},{"type":"tips","name":"Post-Printing"},{"type":"design","name":"How I Designed This"},{"type":"custom","name":"Custom Section"}],"license":"Creative Commons - Attribution","files_url":"https:\/\/api.thingiverse.com\/things\/2042991\/files","images_url":"https:\/\/api.thingiverse.com\/things\/2042991\/images","likes_url":"https:\/\/api.thingiverse.com\/things\/2042991\/likes","ancestors_url":"https:\/\/api.thingiverse.com\/things\/2042991\/ancestors","derivatives_url":"https:\/\/api.thingiverse.com\/things\/2042991\/derivatives","tags_url":"https:\/\/api.thingiverse.com\/things\/2042991\/tags","categories_url":"https:\/\/api.thingiverse.com\/things\/2042991\/categories","file_count":4,"layout_count":0,"layouts_url":"https:\/\/api.thingiverse.com\/layouts\/2042991","is_private":false,"is_purchased":false,"in_library":false,"print_history_count":0,"app_id":null}';
        return json_decode($json);
    }

    public static function api_getOrder($id = 384)
    {
        $dump = '{
  "id": '.$id.',
  "thing_id": 1095780,
  "amount": "15.27",
  "add_date": "2017-02-16 13:38:28",
  "modified_date": "2017-02-16 15:09:40",
  "charges": [
    {
      "name": "Shipping Price",
      "amount": "2.77"
    },
    {
      "name": "Print Price",
      "amount": "6.51"
    },
    {
      "name": "Fee",
      "amount": "0.99"
    },
    {
      "name": "Tip",
      "amount": "5.00"
    }
  ],
  "fees": {
    "platform fee": "3.08",
    "transaction fee": "0.74"
  },
  "shipping_address": {
    "name": "Nabi Defacto",
    "line1": "40 East Main Street Suite 900",
    "line2": "",
    "city": "Newark",
    "state": "DE",
    "postcode": "19711",
    "country": "US"
  },
  "validated_shipping_address": {
    "name": "Nabi Defacto",
    "organisation": "",
    "street_address": "40 East Main Street Suite 900",
    "dependent_locality": null,
    "city": "Newark",
    "postal_code": "19711",
    "sorting_code": null,
    "country": "US",
    "state": "DE"
  },
  "is_shipped": 0,
  "status": "Completed",
  "note": "note1"
}';
        //  status -  changes to Disputed - if user cancels order. Refunded
        return json_decode($dump, true);
    }
}