<?php
use common\components\PaymentExchangeRateConverter;
use common\components\ps\material\PsColorUsdPriceCalculator;
use common\models\PaymentExchangeRate;
use common\models\PsPrinterColor;
use lib\money\Currency;
use test\unit\CodeceptionTestCase;

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.10.16
 * Time: 16:32
 */
class PsColorUsdPriceUpdaterFTest extends CodeceptionTestCase
{
    public function testCalculateUsdPrice()
    {
        /** @var PsColorUsdPriceCalculator $psColorUsdPriceUpdater */
        $psColorUsdPriceUpdater = Yii::createObject(PsColorUsdPriceCalculator::class);
        /** @var PsPrinterColor $psColor */
        $psColor = Yii::createObject(PsPrinterColor::class);
        $psColor->price_currency_iso = Currency::USD;
        $psColor->price_measure = \lib\MeasurementUtil::GRAM;
        $psColor->price = 4;
        $priceUsdPerGramm = $psColorUsdPriceUpdater->calculateUsdPrice($psColor);
        self::assertEquals($priceUsdPerGramm, 4, 'Simple price per gramm');

        $psColor->price_measure = \lib\MeasurementUtil::OUNCE;
        $psColor->price = 4;
        $priceUsdPerGramm = $psColorUsdPriceUpdater->calculateUsdPrice($psColor);
        self::assertEquals($priceUsdPerGramm, 4 / 28.35, 'Simple price per ounce');
    }

    public function getPaymentExchangeStub()
    {
        /** @var PaymentExchangeRate $paymentExchange */
        $paymentExchange = Yii::createObject(PaymentExchangeRate::class);
        $paymentExchange->created_at = '2016-06-06 12:58:45';
        $paymentExchange->timestamp = 1465203615;
        $paymentExchange->base = 'USD';
        $paymentExchange->rates = '{"AED":3.67307,"AFN":69.065001,"ALL":122.2839,"AMD":478.019998,"ANG":1.788725,"AOA":165.782167,"ARS":13.89755,"AUD":1.358595,"AWG":1.793333,"AZN":1.501438,"BAM":1.721986,"BBD":2,"BDT":78.28065,"BGN":1.723345,"BHD":0.377068,"BIF":1559.549988,"BMD":1,"BND":1.361825,"BOB":6.86826,"BRL":3.5259,"BSD":1,"BTC":0.001717504809,"BTN":66.877399,"BWP":11.072838,"BYR":19953.5,"BZD":2.000663,"CAD":1.295069,"CDF":927.5,"CHF":0.975573,"CLF":0.024602,"CLP":682.308004,"CNY":6.56602,"COP":3017.313337,"CRC":536.91,"CUC":1,"CUP":0.995388,"CVE":97.273266,"CZK":23.79709,"DJF":177.72875,"DKK":6.550095,"DOP":45.7726,"DZD":109.849219,"EEK":13.773,"EGP":8.881137,"ERN":15.0015,"ETB":21.68752,"EUR":2,"FJD":2.086867,"FKP":0.692785,"GBP":0.692785,"GEL":2.13476,"GGP":0.692785,"GHS":3.84617,"GIP":0.692785,"GMD":42.49514,"GNF":7344.975098,"GTQ":7.60325,"GYD":205.941669,"HKD":7.767692,"HNL":22.58414,"HRK":6.60728,"HTG":62.31575,"HUF":274.547602,"IDR":13395.116667,"ILS":3.835116,"IMP":0.692785,"INR":66.89813,"IQD":1180.8819,"IRR":30314.5,"ISK":123.132,"JEP":0.692785,"JMD":124.332,"JOD":0.708526,"JPY":107.0022,"KES":101.0819,"KGS":68.442101,"KHR":4079.974976,"KMF":431.397284,"KPW":899.91,"KRW":1165.193332,"KWD":0.301582,"KYD":0.824383,"KZT":335.916991,"LAK":8109.975098,"LBP":1509.833333,"LKR":146.595,"LRD":90.49095,"LSL":15.13838,"LTL":3.021726,"LVL":0.619283,"LYD":1.3576,"MAD":9.64901,"MDL":19.7386,"MGA":3236.06665,"MKD":54.26764,"MMK":1193.400025,"MNT":1986.166667,"MOP":7.97219,"MRO":353.635004,"MTL":0.683738,"MUR":35.49945,"MVR":15.31,"MWK":706.919,"MXN":18.59622,"MYR":4.09643,"MZN":58.84,"NAD":15.13818,"NGN":198.106999,"NIO":28.44966,"NOK":8.179936,"NPR":106.9364,"NZD":1.443998,"OMR":0.385012,"PAB":1,"PEN":3.3341,"PGK":3.166033,"PHP":46.2439,"PKR":104.264,"PLN":3.860229,"PYG":5684.233333,"QAR":3.64011,"RON":3.979239,"RSD":108.611721,"RUB":65.72906,"RWF":739.3525,"SAR":3.750357,"SBD":7.806624,"SCR":13.02473,"SDG":6.061663,"SEK":8.143671,"SGD":1.358961,"SHP":0.692785,"SLL":3947.5,"SOS":599.970003,"SRD":6.71175,"STD":21662.5,"SVC":8.703313,"SYP":219.185665,"SZL":15.14108,"THB":35.29188,"TJS":7.86895,"TMT":3.5014,"TND":2.12125,"TOP":2.278618,"TRY":2.907665,"TTD":6.62171,"TWD":32.29702,"TZS":2191.149984,"UAH":24.86598,"UGX":3366.533333,"USD":1,"UYU":30.91476,"UZS":2942.314942,"VEF":9.9715,"VND":22397.5,"VUV":111.365001,"WST":2.521653,"XAF":581.208785,"XAG":0.0610635,"XAU":0.000808,"XCD":2.70302,"XDR":0.71035,"XOF":581.012785,"XPD":0.001821,"XPF":105.193038,"XPT":0.001019,"YER":249.083,"ZAR":15.09039,"ZMK":5252.024745,"ZMW":10.544625,"ZWL":322.387247}';
        return $paymentExchange;
    }

    public function testCalculateEurPrice()
    {
        $paymentExchange = $this->getPaymentExchangeStub();
        Yii::$container->set(
            PaymentExchangeRateConverter::class,[
                'lastPaymentExchangeRate' => $paymentExchange
        ]);

        /** @var PsColorUsdPriceCalculator $psColorUsdPriceUpdater */
        $psColorUsdPriceUpdater = Yii::createObject(PsColorUsdPriceCalculator::class);
        /** @var PsPrinterColor $psColor */
        $psColor = Yii::createObject(PsPrinterColor::class);
        $psColor->price_currency_iso = 'EUR';
        $psColor->price_measure = \lib\MeasurementUtil::GRAM;
        $psColor->price = 8;
        $priceUsdPerGramm = $psColorUsdPriceUpdater->calculateUsdPrice($psColor);
        self::assertEquals($priceUsdPerGramm, 4, 'Simple price per gramm');
    }
}