<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 15:23
 */


namespace tests\unit\modules\contentAutoBlocker\contentFilters;

use common\modules\contentAutoBlocker\components\contentFilters\WebSiteFilter;

class WebSiteFilterTest extends BaseFilterTest
{
    public function testWebsiteFilter()
    {
        $this->filter = new WebSiteFilter();
        $this->phraseCheck('SomeFilterText', false);
        $this->phraseCheck('My site www.yandex.ru', true);
        $this->phraseCheck('My is not. Site.Common.', false);
        $this->phraseCheck('My site thingiverse.com', true);
        $this->phraseCheck('My site thingiverse.org', true);
        $this->phraseCheck('My site thingiverse.net', true);
        $this->phraseCheck('My site www.blabla.blabla.uk', true);
        $this->phraseCheck('My site http://a.b', true);
        $this->phraseCheck('My site http://site', true);
        $this->phraseCheck('My site treatstock.com', false);
        $this->phraseCheck('My site www.treatstock.com', false);
        $this->phraseCheck('My site http://www.treatstock.com', false);
        $this->phraseCheck('My site https://www.treatstock.com', false);
        $this->phraseCheck('My site https://www.treatstock.com/3d-printable-models/?search=&category=', false);
        return false;
    }
}