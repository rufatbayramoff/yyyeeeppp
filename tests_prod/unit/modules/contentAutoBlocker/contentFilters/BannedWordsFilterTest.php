<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 15:24
 */

namespace common\modules\contentAutoBlocker\components\contentFilters;


use tests\unit\modules\contentAutoBlocker\contentFilters\BaseFilterTest;
use Yii;

class BannedWordsFilterTest extends  BaseFilterTest
{
    public function testEmailFilter()
    {
        $this->filter = Yii::createObject(BannedPhraseFilter::class);
        $this->phraseCheck('SomeFilterText', false);
        $this->phraseCheck('Fuck you Man!', true);
        $this->phraseCheck('Sun of the bitch!', true);
        $this->phraseCheck('DJ Fabitch sing a song.', false);
    }
}