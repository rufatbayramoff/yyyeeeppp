<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.10.16
 * Time: 10:11
 */

namespace tests\unit\modules\contentAutoBlocker\contentFilters;

use common\modules\contentAutoBlocker\components\contentFilters\ContentFilterInterface;
use test\unit\CodeceptionTestCase;

abstract class BaseFilterTest extends CodeceptionTestCase
{
    /** @var  ContentFilterInterface */
    public $filter;

    public function phraseCheck($text, $expectedResult)
    {
        $result = $this->filter->isAlarmText($text);
        self::assertEquals($expectedResult, $result, 'Source text: "' . $text . '"');
    }
}
