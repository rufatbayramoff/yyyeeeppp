#node version 7.8.0
#node exec path: /usr/local/bin/node

apt-get install nodejs npm
npm install -g n
n stable

# exit and ssh to server again

npm update npm -g
n prune
npm install --no-bin-links
cp app/config.json.example app/config.json
vim app/config.json
npm start
###########################################
# host:port/www
# host:port/jsapi/public/basrelief ( to nginx) -> data/ (links from config data http://...
# treatstock php get links config data get STL из url from config
# ssl forward www.treatstock.com/jsapi/public/ -> host:port
# models_root directory /var/www/treatstock/frontend/data

#################################################
For installing public watermark service (if mysql_host not set - service uninitialized!):
mysql -u root --password=mypass
create database watermark;
grant all privileges on watermark.* to watermark@localhost identified by 'watermark';
flush privileges;
cat app/database.sql | mysql -f -u watermark --password=watermark watermark
nginx config: root:->www/watermark/*, /jsapi/public/watermark/ (GET,POST)

################################################
#for analyze models to find defect ones run:
mkdir /path/to/bad/stl/files/placed
#and run:
node scanner.js --in=/path/to/stl/dir --out=/path/to/bad/stl/files/placed

################################################
# run in Docker
for build Docker images first make it
```bash
cp app/config.json.example app/config.json
vim app/config.json
```
after start build `docker build -t js .`

to start container you need to run the command `docker run --rm -p 5858:5858 js` (будет запущен только nodejs)
or run `docker-compose up -d` (будут запущены 4 контейнера - mysql, nodejs, nginx, adminer)

NodeJS - http://0.0.0.0:8080/jsapi/public/
Mysql admin - http://0.0.0.0:8080/adminer

# Build 
docker-compose build --pull

# push to local registry
docker image push 10.102.0.50:5000/node/js:latest

