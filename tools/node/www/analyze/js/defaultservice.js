var millingParams = {
    deltaStock: 2,
    delta2Stage: 0.7,
    instrumentRoughUsage: 1, instrumentFinishUsage: 1, // layered?
    maxRPM: 1200, // rotate per minute
    materials: [
        {material: 'steel', price: 2.50, density: 7800 },
    ],
    instruments: [
        {material: 'steel', stage:'rough',  d: 19.00, fr:0.1200, s:80,  l:3, delta:0},
        {material: 'steel', stage:'finish', d: 5.00,  fr:0.0500, s:130, l:3, delta:2.5},
    ],
    priceProcessing: 20, pricePersonnel: 20, // usd per hour
    placeTime: 30, setupTime: 1800      // sec: 1) place stock 2) setup machine
};