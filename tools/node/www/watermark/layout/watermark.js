$( document ).ready(function() {
  // init
});

var subscribe = false;
var email = "";
var pass = "";
var info = "";
var files = [];
var msgFiles = "";
var modeSameName = false;
var astronger = false;

// Tab
function openTab(evt, tabID) {

    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabID).style.display = "block";
    evt.currentTarget.className += " active";
}

( function( $, window, document, undefined )
{
    $( '.inputfile' ).each( function()
    {
        var $input	 = $( this ),
            $label	 = $input.next( 'label' ),
            labelVal = $label.html();

        $input.on( 'change', function( e )
        {
            var fileName = '';

            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else if( e.target.value )
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                $label.find( 'span' ).html( fileName );
            else
                $label.html( labelVal );
        });
        

        // Firefox bug fix
        $input
            .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
            .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    });
    
    if (location.href.endsWith("?multiple")) {
        $("#afile").attr("multiple", true);
     }
    
})( jQuery, window, document );

// Modal
var modal = document.getElementById('mainModal');
var modalClose = document.getElementsByClassName("modal__close");

$(modalClose).click(function() {
    $(modal).removeClass('open');
});

window.onclick = function(event) {
    if (event.target == modal) {
        $(modal).removeClass('open');
    }
};

function myalert(txt) {
    document.getElementById('model__content').innerHTML = txt
    $(modal).addClass('open');
    document.getElementById('processing').style.display = 'none';
    document.getElementById('modal_close_button').style.display = 'block';    
}

function inprocess() {
    document.getElementById('processing').style.display = 'block';
    document.getElementById('model__content').innerHTML = 'Processing...';
    $(modal).addClass('open');
    document.getElementById('modal_close_button').style.display = 'none';    
}
function processend() {
    $(modal).removeClass('open');
    document.getElementById('modal_close_button').style.display = 'block';    
}

$('a[data-target]').click(function () {
    var scroll_elTarget = $(this).attr('data-target');
    if ($(scroll_elTarget).length != 0) {
        $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 80}, 350);
    }
    if ($(this)) {
        setTimeout(function () {
            $('.header-nav').removeClass('open');
        }, 200);
        console.log('Close nav');
    }
    return false;
});

$('.nav-close').click(function () {
    $('.header-nav').toggleClass('open');
});

function applyWatermark() {
    subscribe = $("#asubscribe").prop('checked');
    astronger = $("#astronger").prop('checked');
    email = $("#aemail").val();
    pass = $("#apass").val();
    info = $("#ainfo").val();
    var file_data = $('#afile').prop('files')[0];

    if ( file_data== undefined ) {
        $("#afile").closest('div').find('.line__error').addClass('open');
        return false;
    } else {
        $("#afile").closest('div').find('.line__error').removeClass('open');
    }

    if ( info=="" ) {
        $("#ainfo").closest('div').find('.line__error').addClass('open');
        return false;
    } else {
        $("#ainfo").closest('div').find('.line__error').removeClass('open');
    }

    if ( email=="" ) {
        $("#aemail").closest('div').find('.line__error').addClass('open');
        return false;
    } else  {
        $("#aemail").closest('div').find('.line__error').removeClass('open');
    }

    if ( ! $("#aterms").prop('checked') ) {
        $("#aterms").closest('div').find('.line__error').addClass('open');
        return false;
    } else  {
        $("#aterms").closest('div').find('.line__error').removeClass('open');
    }
    
    msgFiles = "";
    files=[];
    Array.prototype.push.apply( files, $('#afile').prop('files') ); // <-- here
    // files = $('#afile').prop('files');
    inprocess();
    modeSameName = (location.href.endsWith("?multiple"));// (files.length > 1);
    fireApplyWatermarks();
}
function fireApplyWatermarks() {
    if (files.length > 0) {
        var file_data = files.shift();
        
        var form2 = new FormData();
        form2.append("email", email);
        form2.append("pass", pass);
        form2.append("info", info);
        form2.append("subscribe", subscribe);
        form2.append('file1', file_data);
        form2.append('sameName', modeSameName);
        form2.append('strong', astronger?"0.001":"0");
        var settings = {
          "async": true,
          "url": "/jsapi/public/watermark/encode",
          "method": "POST",
          "processData": false,
          "contentType": false,
          "cache": false,
          "dataType": "json",
          "mimeType": "multipart/form-data",
          "data": form2
        }
        $.ajax(settings).done(function (response) {
          // console.log(response);
          if (response.result == "ok" && response.file) {
              // processend();
              var url = "/jsapi/public/watermark/download?file="+encodeURIComponent(response.file)+"&email="+encodeURIComponent(response.email)+"&key="+encodeURIComponent(response.key)+"&s="+encodeURIComponent(response.s);
              // console.log(url);
              msgFiles += "<b>" + file_data.name + "</b> complete.<br>";
              setTimeout("fireApplyWatermarks()", 100);
              document.location = url;
              return;
          }
          if (response.result == "ok" && response.message) {
              // myalert(response.message);
              msgFiles += "<b>" + file_data.name + "</b> complete: " + response.message+"<br>";
              setTimeout("fireApplyWatermarks()", 100);
              return;
          }
          if (response.result == "fail" && response.message) {
              //myalert(response.message);
              msgFiles += "<b>" + file_data.name + "</b> not processed: " + response.message+"<br>";
              setTimeout("fireApplyWatermarks()", 100);
              return;
          }
          myalert('Sorry error happens');
        }).fail(function(e){
          // console.log(e);
          // processend();
          setTimeout("fireApplyWatermarks()", 100);
        });    
    } else {
        // end queue of files
        myalert(msgFiles.trim());
    }
}

function readWatermark() {
    var pass = $("#bpass").val();
    var file_data = $('#bfile').prop('files')[0];

    if ( file_data== undefined ) {
        $("#bfile").closest('div').find('.line__error').addClass('open');
        return false;
    } else {
        $("#bfile").closest('div').find('.line__error').removeClass('open');
    }

    if ( ! $("#bterms").prop('checked') ) {
        $("#bterms").closest('div').find('.line__error').addClass('open');
        return false;
    } else {
        $("#bterms").closest('div').find('.line__error').removeClass('open');
    }
    
    var form2 = new FormData();
    form2.append("pass", pass);
    form2.append('file1', file_data);
    var settings = {
      "async": true,
      "url": "/jsapi/public/watermark/decode",
      "method": "POST",
      "processData": false,
      "contentType": false,
      "cache": false,
      "dataType": "json",
      "mimeType": "multipart/form-data",
      "data": form2
    }
    inprocess();
    $.ajax(settings).done(function (response) {
      // console.log(response);
      if (response.result == "ok" && response.message) {
          myalert(response.message);
          return;
      }
      if (response.result == "fail" && response.message) {
          myalert(response.message);
          return;
      }
      if (response.result == "ok" && Object.keys(response.marks).length>0) 
      {
          myalert("Watermark found: " + Object.keys(response.marks).join());
          return;
      }
      myalert('Sorry error happens');
    }).fail(function(e){
      // console.log(e);
      processend();
    });    
}
var tmp;