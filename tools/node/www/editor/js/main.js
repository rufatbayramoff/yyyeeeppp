if (!Detector.webgl) Detector.addGetWebGLMessage();

var container, camera, scene, renderer, raycaster, geometry, controls, objects = [], 
    material, meshMain, geometry, objradius, sketchMaterial, sketchMesh, sketchGeometry,
    materialHilite, geometryHilite, meshHilite;    
var light1, light2, light3, light4;
var mouse = new THREE.Vector2();
var clickedface, mode = '3d', commandsPanel;
var sketchElements = [];



// viewer
function initViewer(w,h) {
    // camera
    camera = new THREE.PerspectiveCamera(35, w / h, 1, 10000);
    // camera.position.y = -1000; 
    // camera.position.z = 0;
    camera.updateProjectionMatrix();
    // scene
    scene = new THREE.Scene();
    raycaster = new THREE.Raycaster();    
    // renderer
    renderer = new THREE.WebGLRenderer({antialias: true, preserveDrawingBuffer: true});
    renderer.setClearColor(0xfcfcfc); 
    renderer.setSize(w, h);
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.renderReverseSided = false;
    // container
    container = document.createElement('div');
    document.getElementById('viewer').appendChild(container);
    container.appendChild(renderer.domElement);
    // controls
    controls = new THREE.TrackballControls( camera );
    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.8;
    controls.noZoom = false;
    controls.noPan = false;
    // controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;
    controls.keys = [ 65, 83, 68 ];
    controls.addEventListener( 'change', render );
    /*
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.target.set(0, 0, 0);
    controls.zoomSpeed = 0.5;
    //controls.enableDamping = true;
    //controls.dampingFactor = 0.25;
    controls.update();
    controls.addEventListener( 'change', render );
    */
    // lights
    scene.add(new THREE.HemisphereLight(0x443333, 0x111122));
    var r = 1;
    light1 = addShadowedLight(2*r, -2*r, 0, 0xffffff, 1);
    light2 = addShadowedLight(-1*r, 1*r, 1*r, 0xffffff, 0.8);
    light3 = addShadowedLight(-1*r, 0.5*r, -1*r, 0xffffff, 0.6);
    light4 = addShadowedLight(0.5*r, -0.5*r, -2*r, 0xffffff, 0.6);
    // material
    material = new THREE.MeshPhongMaterial({ // MeshStandardMaterial MeshPhongMaterial
        color: 0x335533, specular: 0x111111, shininess: 200, 
        opacity: 1, transparent: true,  // 0.8
        side: THREE.DoubleSide // , vertexColors: THREE.VertexColors
    });
    sketchMaterial = new THREE.MeshBasicMaterial( {
        color: 0x550000, specular: 0x111111, shininess: 200, 
        opacity: 0.6, transparent: true, 
        side: THREE.DoubleSide // , vertexColors: THREE.VertexColors} );
    });
    materialHilite = new THREE.MeshBasicMaterial({ // MeshStandardMaterial MeshPhongMaterial
        vertexColors: THREE.VertexColors,  // color: 0xff0000,//
        side: THREE.DoubleSide 
    });
    /*materialProblemWireframe = new THREE.LineBasicMaterial( {
        vertexColors: THREE.VertexColors, // color: 0xff0000, 
        linewidth: 3,
        linecap: 'round', //ignored by WebGLRenderer
        linejoin:  'round' //ignored by WebGLRenderer
    } ); */
    // var material = new THREE.LineBasicMaterial( { vertexColors: THREE.VertexColors } );
    //window.addEventListener('resize', onWindowResize, false);
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    renderer.domElement.ondblclick=onDoubleClick;
    renderer.domElement.onclick=onClick;
    animate();
    // render();
}    
function addShadowedLight(x, y, z, color, intensity) {
    var directionalLight = new THREE.DirectionalLight(color, intensity);
    directionalLight.position.set(x, y, z);
    scene.add(directionalLight);
    directionalLight.castShadow = true;
    var d = 1;
    directionalLight.shadow.camera.left = -d;
    directionalLight.shadow.camera.right = d;
    directionalLight.shadow.camera.top = d;
    directionalLight.shadow.camera.bottom = -d;
    directionalLight.shadow.camera.near = 1;
    directionalLight.shadow.camera.far = 15;
    directionalLight.shadow.mapSize.width = 1024;
    directionalLight.shadow.mapSize.height = 1024;
    directionalLight.shadow.bias = -0.005;
    return directionalLight;
}
function render() {
    // controls.update();
    renderer.render(scene, camera);
}
function animate() {
    requestAnimationFrame(animate);
    controls.update();
    // render();
}


// handle uploads ------------------------------------
var fileIndex = 0, firstMessageShown = true;
var geoms = {}, geomsUrl = {}, fileComments = {}, fileIds = {};
function handleFileSelectDrag(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    var files = evt.dataTransfer.files; // FileList object.
    // files is a FileList of File objects. List some properties.
    var filesSorted = [].slice.call(files).sort(function(a, b){
        return a.name == b.name ? 0 : a.name < b.name ? -1 : 1;
    });
    for (var i = 0, f; f = filesSorted[i]; i++) {
      addFile(f);
    }
}
function handleFileSelectInput(evt) {
    var files = evt.target.files; // FileList object
    // files is a FileList of File objects. List some properties.
    var filesSorted = [].slice.call(files).sort(function(a, b){
        return a.name == b.name ? 0 : a.name < b.name ? -1 : 1;
    });
    for (var i = 0, f; f = filesSorted[i]; i++) {
      addFile(f);
    }
    $('#files').val('');
}
function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}
function initDragDrop() {
        // Setup the dnd listeners.
        var dropZone = document.getElementById('dropzone');
        dropZone.addEventListener('dragover', handleDragOver, false);
        dropZone.addEventListener('drop', handleFileSelectDrag, false);
        document.getElementById('files').addEventListener('change', handleFileSelectInput, false)
}
function addFile(f) {
    fileIndex++;
    hideFirstMessage();
    var extension = f.name.split('.').pop().toLowerCase();
    /* if (extension == "csv") {
    } else */ if (extension == "stl") { 
        var reader = new FileReader();
        reader.addEventListener("load", function (ev) {
            var buffer = ev.target.result;
            var geometry = loadStl(buffer);
            showgeom(geometry);
        }, false);
        reader.readAsArrayBuffer(f);
    }
}
function hideFirstMessage() {
    if (!firstMessageShown) return;
    firstMessageShown = false;
    var el = document.getElementById('firstmessage');
    el.style.position = 'static';
    el.style.padding = '5px';
    el.style.border = 'dashed 1px black';
    el.style.margin = '0 auto 10px auto';
    var el0 = document.getElementById('dropzone');
    el0.style.border = 'none';
    el0.style.position = 'static';
    commandsPanel = document.getElementById('commands');
    commandsPanel.style.position = 'absolute';
    commandsPanel.style.top = '100px';    
    commandsPanel.style.left = '20px';    
    initViewer(800,600);
}

function showgeom(geometry1) {
        geometry = geometry1;
        if (meshMain) { scene.remove(meshMain); meshMain = null;  };

        // var geometry = loadStl(buffer);
        geometry.computeBoundingSphere();
        geometry.computeBoundingBox();
        geometry.mergeVertices();
        geometry.computeFaceNormals(); // TODO fix it?

        // show original
        meshMain = new THREE.Mesh(geometry, material );
        //meshMain.castShadow = true;
        //meshMain.receiveShadow = true;
        scene.add(meshMain);
        objects = [];
        objects.push(meshMain); 

        // Lights
        var sphere = geometry.boundingSphere;
        objradius =  sphere.radius;
        light1.position.set(2*objradius, -2*objradius, 0);
        light2.position.set(-1*objradius, 1*objradius, 1*objradius);
        light3.position.set(-1*objradius, 0.5*objradius, -1*objradius);
        light4.position.set(0.5*objradius, -0.5*objradius, -2*objradius);
        //controls.target.copy(sphere.center);
        //camera.position.set(sphere.center.x, sphere.center.y - 4*r, sphere.center.z ); 
        controls.up0.copy(new THREE.Vector3( 0 , 0.1 , 1 ));  // objradius * 5
        controls.target0.copy( sphere.center.clone()); // sphere.center);
        controls.position0.copy(sphere.center.clone().add(new THREE.Vector3( 0 , -objradius * 5, 0 )));  // 
        controls.reset();
        //controls.update();
        mode = '3d';
        render();
}

// --UI interactions --------------------------
function onDocumentMouseMove( event ) {
    event.preventDefault();
    if (mode=='3d') {
        if (!meshMain || !meshMain.geometry ) return;
        var rect = renderer.domElement.getBoundingClientRect();
        mouse.x =  (event.clientX - rect.left) * 2.0 / (rect.right - rect.left) - 1;
        mouse.y =  (event.clientY - rect.bottom) * 2.0 / (rect.top - rect.bottom) - 1;
        raycaster.setFromCamera( mouse, camera );
        intersections = raycaster.intersectObjects( objects );
        numObjects = objects.length;
        if ( intersections.length > 0 ) {
            //if ( intersected && intersected != intersections[ 0 ].object )
            //    hideIntersection();
            intersected = intersections[ 0 ];
            cleanHiliter();
            // console.log(intersected.face)
            addFaceToHiliter(intersected.face);
            clickedface = intersected.face;
            hilite();
        }
    };
}

function onClick( event ) {
    event.preventDefault();
    if (mode=='sketch') {
        var rect = renderer.domElement.getBoundingClientRect();
        mouse.x =  (event.clientX - rect.left) * 2.0 / (rect.right - rect.left) - 1;
        mouse.y =  (event.clientY - rect.bottom) * 2.0 / (rect.top - rect.bottom) - 1;
        raycaster.setFromCamera( mouse, camera );
        intersections = raycaster.intersectObjects( objects );
        numObjects = objects.length;
        if ( intersections.length > 0 ) {
            //if ( intersected && intersected != intersections[ 0 ].object )
            //    hideIntersection();
            intersected = intersections[ 0 ];
            sketchElements.push(intersected.point);
            if (sketchElements.length >= 3) {
                cleanHiliter();
                for(j = 0; j<= sketchElements.length - 3; j++)
                    publishProblemFace(sketchElements[0],sketchElements[j+1],sketchElements[j+2],0x00ff00);
                hilite();
            }
            // console.log(intersected);
        }
    };
}
function onDoubleClick(event) {
    if (mode=='3d') {
        event.preventDefault();
        // controls.target.set(100, 0, 0);
        var p1 = geometry.vertices[clickedface.a].clone(); // 
        var p2 = geometry.vertices[clickedface.b].clone();
        var p3 = geometry.vertices[clickedface.c].clone();
        controls.target0 = p1.add(p2).add(p3).multiplyScalar(0.333333333);
        controls.position0 =  controls.target0.clone().add(clickedface.normal.clone().multiplyScalar(objradius * 4));
        console.log("DBL");
        
        objects = [];
        mode = 'sketch';
        if (sketchMesh) { scene.remove(sketchMesh); sketchMesh = null;  };
        sketchGeometry = new THREE.PlaneGeometry( objradius * 2, objradius * 2 );
        sketchMesh = new THREE.Mesh( sketchGeometry, sketchMaterial );
        sketchMesh.position.copy(controls.target0);
        sketchMesh.lookAt(controls.position0);
        //sketchMesh.rotation.x = 90;
        //sketchMesh.rotation.y = 90;
        //sketchMesh.rotation.z = 90;
        scene.add( sketchMesh );
        objects.push(sketchMesh);
        controls.reset();
        sketchElements = [];
        cleanHiliter();
        commandsPanel.style.display = 'block';

    } 
}
function sketchExit(operation) {
    if (mode = 'sketch') {
        var h = '10';
        if (operation != 'cancel')
            h = prompt('height:', 10);
        if (h == null) return;
        cleanHiliter();
        if (sketchMesh) { scene.remove(sketchMesh); sketchMesh = null;  };
        var geometryOp = new THREE.Geometry();
        var n = clickedface.normal.clone().multiplyScalar(parseFloat(h));
        var microdelta = h>0?-0.001:+0.001;
        var nn = clickedface.normal.clone().multiplyScalar(microdelta);
        var side = (h > 0);
        if (sketchElements.length >= 3) {
            var j = 0;
            // todo - direction of faces???
            addNewFace(geometryOp,sketchElements[j].clone().add(nn),sketchElements[j+1].clone().add(nn),sketchElements[j+1].clone().add(n),0x00ff00,side);
            addNewFace(geometryOp,sketchElements[j].clone().add(nn),sketchElements[j].clone().add(n),sketchElements[j+1].clone().add(n),0x00ff00,!side);
            for(j = 0; j<= sketchElements.length - 3; j++) {
                // down and up walls - TODO впуклые многоугольники?
                addNewFace(geometryOp,sketchElements[0].clone().add(nn),sketchElements[j+1].clone().add(nn),sketchElements[j+2].clone().add(nn),0x00ff00,!side);
                addNewFace(geometryOp,sketchElements[0].clone().add(n),sketchElements[j+1].clone().add(n),sketchElements[j+2].clone().add(n),0x00ff00,side);
                // side wall
                addNewFace(geometryOp,sketchElements[j+1].clone().add(nn),sketchElements[j+2].clone().add(nn),sketchElements[j+2].clone().add(n),0x00ff00,side);
                addNewFace(geometryOp,sketchElements[j+1].clone().add(nn),sketchElements[j+1].clone().add(n),sketchElements[j+2].clone().add(n),0x00ff00,!side);
            }
            addNewFace(geometryOp,sketchElements[0].clone().add(nn),sketchElements[j+1].clone().add(nn),sketchElements[j+1].clone().add(n),0x00ff00,!side);
            addNewFace(geometryOp,sketchElements[0].clone().add(nn),sketchElements[0].clone().add(n),sketchElements[j+1].clone().add(n),0x00ff00,side);
        }
        var meshOp = new THREE.Mesh( geometryOp );
        var bspOp = new ThreeBSP( meshOp );
        var bspMain = new ThreeBSP( meshMain );
        if (operation == 'union') {
            var bspJoin = bspMain.union( bspOp );
            showgeom(bspJoin.toGeometry()); // toMesh?
        } else if (operation == 'subtract') {
            var bspJoin = bspMain.subtract( bspOp );
            showgeom(bspJoin.toGeometry()); // toMesh?
        } else if (operation == 'cancel') {
            showgeom(geometry); 
        }
        commandsPanel.style.display = 'none';
        /*
        geometry.verticesNeedUpdate = true;
        geometry.elementsNeedUpdate = true;
        geometry.morphTargetsNeedUpdate = true;
        geometry.uvsNeedUpdate = true;
        geometry.normalsNeedUpdate = true;
        geometry.colorsNeedUpdate = true;
        geometry.tangentsNeedUpdate = true;
        geometry.dynamic = true;        
        geometry.computeBoundingSphere();
        geometry.computeBoundingBox();
        geometry.mergeVertices();
        geometry.computeFaceNormals(); // TODO fix it?
        */       

        //objects = [];
        //objects.push(meshMain); 
        //mode = '3d';
        //render();
    }    
}

///////////////////////////////////////////////////
//     hiliter 
///////////////////////////////////////////////////


function cleanHiliter() {
    // clean up geometryHilite, meshHilite; 
    if (meshHilite) { scene.remove(meshHilite); meshHilite = null; };
    geometryHilite = new THREE.Geometry();
}
function publishProblemFace(p1,p2,p3,color) {
    var i = geometryHilite.vertices.length;
    geometryHilite.vertices.push( p1 );
    geometryHilite.vertices.push( p2 );
    geometryHilite.vertices.push( p3 );
    var face = new THREE.Face3(i, i + 1, i + 2);
    face.vertexColors[0] = new THREE.Color(color);
    face.vertexColors[1] = new THREE.Color(color);
    face.vertexColors[2] = new THREE.Color(color);
    geometryHilite.faces.push(face); // , normal = var normal = new THREE.Vector3( 0, 1, 0 );
}
function addFaceToHiliter(face) {
    var n = face.normal.clone().multiplyScalar(0.01);
    var p1 = geometry.vertices[face.a].clone().add(n); // 
    var p2 = geometry.vertices[face.b].clone().add(n);
    var p3 = geometry.vertices[face.c].clone().add(n);
    publishProblemFace(p1,p2,p3,0xff0000);
}
function hilite() {
    geometryHilite.computeFaceNormals();
    geometryHilite.computeVertexNormals();
    meshHilite = new THREE.Mesh(geometryHilite, materialHilite );
    scene.add(meshHilite);
    render();
}

function addNewFace(geometryTo, p1,p2,p3,color, inverce) {
    var i = geometryTo.vertices.length;
    geometryTo.vertices.push( p1 );
    if (inverce) {
        geometryTo.vertices.push( p3 );
        geometryTo.vertices.push( p2 );
    } else {
        geometryTo.vertices.push( p2 );
        geometryTo.vertices.push( p3 );
    }
    var face = new THREE.Face3(i, i + 1, i + 2);
    //face.vertexColors[0] = new THREE.Color(color);
    //face.vertexColors[1] = new THREE.Color(color);
    //face.vertexColors[2] = new THREE.Color(color);
    geometryTo.faces.push(face); // , normal = var normal = new THREE.Vector3( 0, 1, 0 );
}
