// var THREE = require('three');

var raycaster = new THREE.Raycaster();  
var raycasterObjects = [];
var neighborsGood = [];
var problemWireframe, problemFaces;
var genGeometry = false;
var vertexToFace, edgesToFace, lastGeometry;
var Q_UNKNOWN = 0;
var Q_GOOD = 1;
var D_UNKNOWN = 0;
var D_RIGHT = 1;
var D_WRONG = 2;
var quality;

function repair(geometry, options){
    var timestart = Date.now();
    var log = '';
    var mesh0 = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({side: THREE.DoubleSide}));
    var raycasterObjects = [];
    raycasterObjects.push( mesh0 );    
    genGeometry = options.createGeometry;
    problemWireframe = new THREE.Geometry();
    problemFaces = new THREE.Geometry();
    geometry.computeBoundingSphere();
    geometry.computeBoundingBox();
    geometry.mergeVertices();
    geometry.computeFaceNormals(); // TODO fix it?
    lastGeometry = geometry;
    
    constructVertexToFace(geometry);
    var errCount = constructEdgesToFace(geometry);
    if (errCount>0) log += errCount + ' edges broken. ';
    
    var wrongOriented = checkNormalOrientation(geometry);
    if (wrongOriented>0) log += wrongOriented + ' faces wrong oriented. ';
    
    // наличие полностью закрытых полостей (=тел)
    //   отсутствие дыр на этих полостях Closing holes - или в рамка одного объекта, или стягивание границ ближайших граней (если грянь соседй=1 и есть рядом грани)
    //   отсутствие совпадающих стенок на этих полостях
    // правильность нормали на этих полостях (невывернутость, однонаправленность) Harmonizing normals
    // непересечение полостей друг с другом (треугольника с треугольниками) ? ThreeBSP
    //   не самопересечение полостей
    //   невложенность полостей друг в друга (а если вложенность - то с правильными нормалями, как полость) - проверкой "нормаль уприрается в..."
    // минимальное необходимая толщина стенок. 
    //   Если стенка не имеет второй стороны, наращивание
    
    // ? метод перенесения треугльника поодиночке
    // work in background?
    
    /*
    
    перебрать все треугольники в один проход
      - если еще не помечен как хороший
       - если луч наружу в бесконечность а луч внутрь во внутренню стенку 
        - считаем треугольник хорошим
        - все его соседи с одной общей гранью (соседей=2) если нормаль через грань совпадает считаем хорошими и рекурсивно/через очередь далее
        - соседей (=2) с инвертированной нормалью через грань - инвертировать им нормаль и тоже считать хорошими
    проверка всех
      - если луч наружу в бесконечность или во внешнюю стенку а луч внутрь во внутренню стенку 
    
    Приоритет по убыванию хорошести
      - луч наружу в бесконечность, луч вовнурь во внутреннюю стенку
      - луч наружу в внешнюю стенку хорошего, луч вовнурь во внутренню стенку хорошего
    Плохой
      - луч наружу во внутреннюю грань
      - луч внутрь в наружную грань
    
     грани, крайние - паник! и надо ЛИБО "закрыть дыру", либо стянуть с другим полу телом - если такое тело есть и это следствие зазора...
     совпадающие грани - схлопнуть!!!
     перерать все соседей по сторонам всех треугольников объединяя их в связные тела
       - ткнули в любой треугольник
       - присоединили всех соседей
          - при каждом присоединении соседей смотрим не нужно ли перевернуть нормаль, чтобы она совпадала в одну сторону с нормалью к кому присоединяем
          - смотрим нет ли у присоединенного других уже присоединенны соседей и верная ли и сторона
          - паника: лента мебиуса???
       - повторили. Повторяем присоединяя треугольник у кого длина границы с уже присоединенными максимальная
     для каждого из тел
       - нашли нормаль не упирающуюся ни в какое тело. 
       - это передняя или задняя нормаль? суммируем голоа пропорционально площади
       - если задняя - выворачиваем тело
     самосовпадающие треугольники
       - удаление дублей
     самопересекающиеся тела
       - разбиение на внутреннее и внешнее тело
       - внутреннее тело удалить???
    */

    
    problemFaces.computeFaceNormals();
    problemFaces.computeVertexNormals();
    result = {
        "geometryProblemFaces": problemFaces,
        "geometryProblemWireframe": problemWireframe,
        // "repairedGeometry": geometry
    }
    problemWireframe = null;
    problemFaces = null;
    vertexToFace = null;
    edgesToFace = null;
    lastGeometry = null;
    result["timing"] = Date.now() - timestart;
    result["log"] = log + '&nbsp; timing: ' + result["timing"] + " msec";
    return result;
}

// visualize problems
function publishProblemLine(p1,p2,color) {
    if (genGeometry) {
        problemWireframe.vertices.push( p1 );
        problemWireframe.vertices.push( p2 );
        problemWireframe.colors.push( new THREE.Color( color ));
        problemWireframe.colors.push( new THREE.Color( color ));
    }
}
function publishProblemFace(p1,p2,p3,color) {
    if (genGeometry) {
        var i = problemFaces.vertices.length;
        problemFaces.vertices.push( p1 );
        problemFaces.vertices.push( p2 );
        problemFaces.vertices.push( p3 );
        var face = new THREE.Face3(i, i + 1, i + 2);
        face.vertexColors[0] = new THREE.Color(color);
        face.vertexColors[1] = new THREE.Color(color);
        face.vertexColors[2] = new THREE.Color(color);
        problemFaces.faces.push(face); // , normal = var normal = new THREE.Vector3( 0, 1, 0 );
        //problemFaces.colors.push( new THREE.Color( color ));
        //problemFaces.colors.push( new THREE.Color( color ));
        //problemFaces.colors.push( new THREE.Color( color ));
    }
}

// repair opertions ---------------------------
function checkNormalOrientation(geometry) {
    // prepare
    var wrongOriented = 0;
    quality = new Array(geometry.faces.length);
    for (var fx = 0; fx < geometry.faces.length; fx++) {
        quality[fx] = Q_UNKNOWN;
    }
    // first run
    for (var fx = 0; fx < geometry.faces.length; fx++) 
        if (quality[fx] == Q_UNKNOWN) {
            var direction = detectDirection(geometry, fx);
            if (direction == D_WRONG) { // invert!
                geometry.faces[fx].normal = geometry.faces[fx].normal.multiplyScalar(-1);
                wrongOriented += 1;
            };
            // now normal is right
            if (direction == D_RIGHT || direction == D_WRONG) {
                quality[fx] = Q_GOOD;
                neighborsGood.push(fx);
                chainSwapNeighbors(geometry);
            }
    }
    
    // TODO check normal forward and backward to other forward or backward (show inside)
    // TODO проверять их БЕЗ сдвига, просто исключая попадание саму себя. чтобы совпадающие грани найти
    
    // show bad areas
    for (var fx = 0; fx < geometry.faces.length; fx++) 
        if (quality[fx] == Q_UNKNOWN) {
            wrongOriented += 1;
            publishProblemFace( geometry.vertices[geometry.faces[fx].a], 
                                geometry.vertices[geometry.faces[fx].b], 
                                geometry.vertices[geometry.faces[fx].c], 
                                0x550033);
    }
    quality = [];
    return wrongOriented;
}
function chainSwapNeighbors(geometry) {
    while (neighborsGood.length > 0) {
        var facenum = neighborsGood.shift();
        // get all neighbors 
        var face = geometry.faces[facenum];
        chainSwapOneNeighbor(face.a, face.b, facenum, geometry);
        chainSwapOneNeighbor(face.a, face.c, facenum, geometry);
        chainSwapOneNeighbor(face.b, face.c, facenum, geometry);
    }
}
function chainSwapOneNeighbor(p1, p2, facenum, geometry) {
        var edge1 = getEdgeKey(p1, p2);
        var facesOfEdge = edgesToFace[edge1];
        // which is not good yet and have edge=2
        if (facesOfEdge.length == 2) {
            var otherface;
            if (facesOfEdge[0] == facenum) {
                otherface = facesOfEdge[1];
            } else {
                otherface = facesOfEdge[0];
            }
            if (quality[otherface] == Q_UNKNOWN) {
                // try to detect norml direction over edge
                // TODO!!!!! invert normal if need
                // var signOfMyNormalEdgeAEdgeBMyC = +-1
                // var otherNormalEdgeAEdgeBOdgeOtherC = ... normal
                // inver other norml if sign of my normal wrong
                // and continue
                quality[otherface] = Q_GOOD;
                neighborsGood.push(otherface);
            }
        }
    
}
function detectDirection(geometry, facenum) {
    var face = geometry.faces[facenum];
    var normal = face.normal;
    var a = geometry.vertices[face.a];
    var b = geometry.vertices[face.b];
    var c = geometry.vertices[face.c];    
    var centerPlus = a.clone().add(b).add(c).multiplyScalar(1/3).add(normal.clone().multiplyScalar(0.001));
    var lp = getRayLength(centerPlus, normal);
    if (lp < 0) {  // if outer normal points to infinity...
        return D_RIGHT;
    };
    var centerMinus = a.clone().add(b).add(c).multiplyScalar(1/3).add(normal.clone().multiplyScalar(-0.001));
    var lm = getRayLength(centerMinus, normal.clone().multiplyScalar(-1));
    if (lp < 0) {  // if outer normal points to infinity...
        return D_WRONG;
    };
    return D_UNKNOWN;
}
function getRayLength(point, axis) {
    raycaster.set( point, axis );
    var intersections = raycaster.intersectObjects( raycasterObjects );
    if (intersections.length>0){
        return intersections[ 0 ].point.distanceTo(point);
    }
    return -1;
}


/// cache operation ----------------------------------
function constructVertexToFace(geometry) {
    vertexToFace = [];
    for (var fx = 0; fx < geometry.vertices.length; fx++) {
        vertexToFace[fx] = new Array();
    }
    for (var fx = 0; fx < geometry.faces.length; fx++) {
        var f = geometry.faces[fx];
        var ax = f.a;
        var bx = f.b;
        var cx = f.c;
        vertexToFace[ax].push(fx);
        vertexToFace[bx].push(fx);
        vertexToFace[cx].push(fx);
    }
}
function constructEdgesToFace(geometry){
    var errCount = 0;
    edgesToFace = {};
    for (var fx = 0; fx < geometry.faces.length; fx++) {
        var f = geometry.faces[fx];
        var ax = f.a;
        var bx = f.b;
        var cx = f.c;
        addEdge(ax,bx,fx);
        addEdge(ax,cx,fx);
        addEdge(bx,cx,fx);
    }
    var k = 0;
    for(var key in edgesToFace) {
        var faces = edgesToFace[key];
        if (faces.length!=2) {
            // console.log("WARNING: Edge " + key + " have " + faces.length + " faces");
            if (faces.length!=2) errCount += 1;
            points = key.split("_");
            var p1 = geometry.vertices[points[0]];
            var p2 = geometry.vertices[points[1]];
            if (faces.length > 3) {
                publishProblemLine(p1,p2,0xff0000); // nore than one - red
            } else {
                publishProblemLine(p1,p2,0x0000ff); // less then one - blue
            };
            /* // show triangles closed to problems
            for (var i = 0; i < faces.length;i++) 
                publishProblemFace( geometry.vertices[geometry.faces[faces[i]].a], 
                                    geometry.vertices[geometry.faces[faces[i]].b], 
                                    geometry.vertices[geometry.faces[faces[i]].c], 
                                    0x550033); */
        }
    }    
    // TODO: geometry.mergeVertices(); on less if a lot of "less than one..."
    return errCount;    
}
function getEdgeKey(ax,bx) {
    if (bx>ax) {var t=ax; ax=bx; bx=t;}
    return ax+"_"+bx;
}
function addEdge(ax,bx,fx) {
    var edgeKey = getEdgeKey(ax,bx);
    addToEdgesToFace(edgeKey,fx)
}
function addToEdgesToFace(edgeKey,faceid) {
    if (!(edgeKey in edgesToFace)) 
         edgesToFace[edgeKey] = new Array();
    edgesToFace[edgeKey].push(faceid);
}
function deleteFromEdgesToFace(edgeKey,faceid) {
    var faceArr = edgesToFace[edgeKey];
    for (var fx = 0; fx < faceArr.length; fx++) 
        if (faceArr[fx]===faceid) {
             edgesToFace[edgeKey].splice(fx, 1);// = faceArr.splice(fx, 1)
             return true;            
    }
    console.log("ERR: trying to delete edge="+edgeKey+" from face="+faceid+" but not found!");
    return false;
}
function addToVertextToFace(vertexid,faceid) {
    if (vertexid > vertexToFace.length) { 
        console.log('cant add more than one vertex');
        return false;
    }
    if (vertexid === vertexToFace.length) { 
         vertexToFace[vertexid] = new Array();
    }
    vertexToFace[vertexid].push(faceid);
}
function deleteFromVertexToFace(vertexid,faceid) {
    var faceArr = vertexToFace[vertexid];
    for (var fx = 0; fx < faceArr.length; fx++) 
        if (faceArr[fx]===faceid) {
             vertexToFace[vertexid].splice(fx, 1);// = faceArr.splice(fx, 1)
             return true;            
    }
    console.log("ERR: trying to delete vertex="+vertexid+" from face="+faceid+" but not found!");
    return false;
}
function findVertexInFaceNotEqual(faceindex,vertext1index,vertext2index) {
    var f = lastGeometry.faces[faceindex];
    if (f.a !== vertext1index && f.a !== vertext2index) return f.a;
    if (f.b !== vertext1index && f.b !== vertext2index) return f.b;
    return f.c;
}
function findPoint4(point1,point2,pointOpposite) {
    var edgeKey2 = getEdgeKey(point1,point2);
    if (!(edgeKey2 in edgesToFace)) {
        console.log('edge '+edgeKey2+' not found'); 
        return -1;
    }
    if (edgesToFace[edgeKey2].length != 2) {
        console.log('edge '+edgeKey2+' non dual: 2!=' + edgesToFace[edgeKey2].length); 
        return -1;
    }
    var possiblePoint1 = findVertexInFaceNotEqual(edgesToFace[edgeKey2][0],point1,point2);
    var possiblePoint2 = findVertexInFaceNotEqual(edgesToFace[edgeKey2][1],point1,point2);
    return (pointOpposite === possiblePoint1 ? possiblePoint2 : possiblePoint1);
} 

//module.exports = measure;