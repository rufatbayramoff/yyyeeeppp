var DEGREE_TO_RAD = 0.0174533;
			if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
			var container;
			var camera, cameraTarget, scene, renderer;
            var controls;
            var light1, light2;
			init();
			animate();
			function init() {
				container = document.createElement( 'div' );
				document.body.appendChild( container );
				camera = new THREE.PerspectiveCamera( 35, window.innerWidth / window.innerHeight, 1, 15 );
				camera.position.set( 0, 0, 4 );
				cameraTarget = new THREE.Vector3( 0, 0, 0 );
				scene = new THREE.Scene();
				scene.fog = new THREE.Fog( 0x72645b , 2, 15 );
				// ASCII file
				var loader = new THREE.STLLoader();
				loader.load( stlName, function ( geometry ) {
					var material = new THREE.MeshPhongMaterial( { color: 0xff5533, specular: 0x111111, shininess: 200 } );
					var mesh = new THREE.Mesh( geometry, material );
                    geometry.computeBoundingSphere();
                    /* var */ sphere =  geometry.boundingSphere;
					// mesh.position.set( 0, 0, 0 );
                    geometry.center();
					mesh.rotation.set( 0 /* -90 * DEGREE_TO_RAD */ , 0, 0 );
                    var scale = 1 / sphere.radius;
					mesh.scale.set( scale, scale, scale );
					mesh.castShadow = true;
					mesh.receiveShadow = true;
					scene.add( mesh );
				} );
				// Ground
				var plane = new THREE.Mesh(
					new THREE.PlaneBufferGeometry( 40, 40 ),
					new THREE.MeshPhongMaterial( { color: 0x999999 , specular: 0x101010 } )
				);
				plane.rotation.x = -Math.PI/2;
				plane.position.y = -1;
				scene.add( plane );
				plane.receiveShadow = true;
				// Lights
				scene.add( new THREE.HemisphereLight( 0x443333, 0x111122  ) );
				light1 = addShadowedLight( 2, 2, 0, 0xffffff, 1 );
				light2 = addShadowedLight( -1, 1, 1, 0xffaa88, 0.8 );
				light3 = addShadowedLight( -1, 0.5, -1, 0xffffff, 0.6 );
				// renderer
				renderer = new THREE.WebGLRenderer( { antialias: true, preserveDrawingBuffer: true } );
				renderer.setClearColor( scene.fog.color );
				// renderer.setPixelRatio( window.devicePixelRatio );
				// renderer.setSize( window.innerWidth, window.innerHeight );
				renderer.setSize( 800, 600 );
                camera.aspect = 800/600;
				camera.updateProjectionMatrix();
				renderer.gammaInput = true;
				renderer.gammaOutput = true;
				renderer.shadowMap.enabled = true;
				renderer.shadowMap.renderReverseSided = false;
                //
				container.appendChild( renderer.domElement );
                // controls
                controls = new THREE.OrbitControls( camera, renderer.domElement );
				controls.target.set( 0, 0, 0 );
                controls.zoomSpeed = 0.5;
				controls.update();
				//
				window.addEventListener( 'resize', onWindowResize, false );

                
			}
			function addShadowedLight( x, y, z, color, intensity ) {
				var directionalLight = new THREE.DirectionalLight( color, intensity );
				directionalLight.position.set( x, y, z );
				scene.add( directionalLight );
				directionalLight.castShadow = true;
				var d = 1;
				directionalLight.shadow.camera.left = -d;
				directionalLight.shadow.camera.right = d;
				directionalLight.shadow.camera.top = d;
				directionalLight.shadow.camera.bottom = -d;
				directionalLight.shadow.camera.near = 1;
				directionalLight.shadow.camera.far = 15;
				directionalLight.shadow.mapSize.width = 1024;
				directionalLight.shadow.mapSize.height = 1024;
				directionalLight.shadow.bias = -0.005;
                return directionalLight;
			}
			function onWindowResize() {
				//camera.aspect = window.innerWidth / window.innerHeight;
				//camera.updateProjectionMatrix();
				//renderer.setSize( window.innerWidth, window.innerHeight );
			}
			function animate() {
				requestAnimationFrame( animate );
				render();
			}
			function render() {
				//var timer = Date.now() * 0.0005;
				//camera.position.x = Math.cos( timer ) * 3;
				//camera.position.z = Math.sin( timer ) * 3;
				//camera.lookAt( cameraTarget );
                controls.update();
				renderer.render( scene, camera );
			}