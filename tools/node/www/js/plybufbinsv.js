// by Asen Kurin - for binary ply export with vertex colors
// http://www.dcs.ed.ac.uk/teaching/cs4/www/graphics/Web/ply.html

var BinaryPlyWriter = (function() {
  var that = {};
  var threevector = new THREE.Vector3();
  var normalMatrixWorld = new THREE.Matrix3();
  var matrixWorld;


  var writeFloat = function(dataview, offset, float, isLittleEndian) {
    dataview.setFloat32(offset, float, isLittleEndian);
    return offset + 4;
  };
  var writeVectorFloat = function(dataview, offset, vector, isLittleEndian) {
    offset = writeFloat(dataview, offset, vector.x, isLittleEndian);
    offset = writeFloat(dataview, offset, vector.y, isLittleEndian);
    return writeFloat(dataview, offset, vector.z, isLittleEndian);
  };
  var writeVectorBin = function(dataview, offset, vector, position, isLittleEndian, isNormal) {
    if (isNormal) {
        threevector.set(vector[position], vector[position+1], vector[position+2]).applyMatrix3( normalMatrixWorld ).normalize();
    }  else {// position
        threevector.set(vector[position], vector[position+1], vector[position+2]).applyMatrix4( matrixWorld );
    }
    offset = writeFloat(dataview, offset, threevector.x, isLittleEndian);
    offset = writeFloat(dataview, offset, threevector.y, isLittleEndian);
    return writeFloat(dataview, offset, threevector.z, isLittleEndian);
  };
  var writeUInt8 = function(dataview, offset, uint) {
    dataview.setUint8(offset, uint);
    return offset + 1;
  };
  var writeVectorColor = function(dataview, offset, color, position) {
    offset = writeUInt8(dataview, offset, Math.round(color[position+0] * 255));
    offset = writeUInt8(dataview, offset, Math.round(color[position+1] * 255));
    offset = writeUInt8(dataview, offset, Math.round(color[position+2] * 255));
    offset = writeUInt8(dataview, offset, 255);
    return offset;
  };
  var writeUInt32 = function(dataview, offset, uint, isLittleEndian) {
    dataview.setUint32(offset, uint, isLittleEndian);
    return offset + 4;
  };
  
  
  
  var geometryToDataViewBin = function(mesh) {
    var geometry = mesh.geometry;
    var positions = geometry.getAttribute('position');
    var normals = geometry.getAttribute('normal');
    var colors = geometry.getAttribute('color');
    var trislen = positions.length / 9;
    
    var isLittleEndian = true; // STL files assume little endian, see wikipedia page
    matrixWorld = mesh.matrixWorld;
    normalMatrixWorld.getNormalMatrix( matrixWorld );
    
    // header
    var h = "ply\n"+
            "format binary_little_endian 1.0\n"+
            "element vertex "+(trislen*3)+"\n"+
            "property float x\n"+
            "property float y\n"+
            "property float z\n"+
            //"property float nx\n"+
            //"property float ny\n"+
            //"property float nz\n"+
            "property uchar red\n"+
            "property uchar green\n"+
            "property uchar blue\n"+
            "property uchar alpha\n"+
            "element face "+(trislen)+"\n"+
            "property list uchar int vertex_indices\n"+
            "end_header\n";
    
    var bufferSize = h.length + (( (4*3+/*4*3+*/4)*3 +1+4*3  ) * trislen);
    var buffer = new ArrayBuffer(bufferSize);
    var dv = new DataView(buffer);
    var offset = 0;

    // generate header
    for (var i=0, strLen=h.length; i<strLen; i++) {
      // buffer[i] = str.charCodeAt(i);
      dv.setUint8(i, h.charCodeAt(i));
    }
    offset += h.length; 

    // vertices
    for(var n = 0; n < trislen; n++) {
      offset = writeVectorBin(dv, offset, positions.array, n*9  , isLittleEndian);
      //offset = writeVectorBin(dv, offset, normals.array, n*9  , isLittleEndian, true);
      offset = writeVectorColor(dv, offset, colors.array,  n*9);
      offset = writeVectorBin(dv, offset, positions.array, n*9+3, isLittleEndian);
      //offset = writeVectorBin(dv, offset, normals.array, n*9+3, isLittleEndian, true);
      offset = writeVectorColor(dv, offset, colors.array,  n*9+3);
      offset = writeVectorBin(dv, offset, positions.array, n*9+6, isLittleEndian);
      //offset = writeVectorBin(dv, offset, normals.array, n*9+6, isLittleEndian, true);
      offset = writeVectorColor(dv, offset, colors.array,  n*9+6);
    }
    // faces
    for(var n = 0; n < trislen; n++) {
      offset = writeUInt8(dv, offset, 3);
      offset = writeUInt32(dv, offset, n*3+0, isLittleEndian);
      offset = writeUInt32(dv, offset, n*3+1, isLittleEndian);
      offset = writeUInt32(dv, offset, n*3+2, isLittleEndian);
    }

    return dv;
  };

  
  
  var save = function(geometry, filename) {
    var dv = geometryToDataViewBin(geometry);
    var blob = new Blob([dv], {type: 'application/octet-binary'});
    
    // FileSaver.js defines `saveAs` for saving files out of the browser
    saveAs(blob, filename);
  };

  that.save = save;
  that.geometryToDataViewBin = geometryToDataViewBin;
  return that;
}());