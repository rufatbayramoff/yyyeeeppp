            //////////////////////////////////////
            // data
            //////////////////////////////////////
			if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
			var container, animator;
			var camera, cameraTarget, scene, renderer;
            var controls;
            var light1, light2, light3;
            var material;
            var plane, geometry;
            var img = new Image();
            var canvas = document.getElementById( 'renderCanvas' );
            var imageLoader = document.getElementById('imageLoader');
            var renderIsDone = false;
            var isInverted = false;

            //////////////////////////////////////
            // visuals 
            //////////////////////////////////////
			function init() {
				container = document.createElement( 'div' );
				document.getElementById('model-col').appendChild( container );
                container.className = 'model-view';
				camera = new THREE.PerspectiveCamera( 35, window.innerWidth / window.innerHeight, 0.0001, 10000 );
				camera.position.set( 0, 0, 2 );
				cameraTarget = new THREE.Vector3( 0, 0, 0 );
				scene = new THREE.Scene();
				//scene.fog = new THREE.Fog( 0x72645b , 2, 15 );
                material = new THREE.MeshPhongMaterial( { color: 0x418CE9, specular: 0x111111, shininess: 200 } );

				// Lights
				scene.add( new THREE.HemisphereLight( 0x443333, 0x111122  ) );
				light1 = addShadowedLight( 2, 2, 0, 0xffffff, 1 );
				light2 = addShadowedLight( -1, 1, 1, 0xffaa88, 0.8 );
				light3 = addShadowedLight( -1, 0.5, -1, 0xffffff, 0.6 );
                
				// renderer
				renderer = new THREE.WebGLRenderer( { antialias: true, preserveDrawingBuffer: true } );
				//renderer.setClearColor( scene.fog.color );
                //renderer.setClearColor(0xE0E8F2 );
                renderer.setClearColor(0xFFFFFF );
				renderer.setSize( 800, 600 );
                camera.aspect = 800/600;
				camera.updateProjectionMatrix();
				renderer.gammaInput = true;
				renderer.gammaOutput = true;
				renderer.shadowMap.enabled = true;
				renderer.shadowMap.renderReverseSided = false;
                //
				container.appendChild( renderer.domElement );
                // controls
                controls = new THREE.OrbitControls( camera, renderer.domElement );
				controls.target.set( 0, 0, 0 );
                controls.zoomSpeed = 0.5;
				controls.update();
                
                //if ( renderer instanceof THREE.CanvasRenderer ) {
                //    scene.__lights = { length: 0, push: function(){}, indexOf: function (){ return -1 }, splice: function(){} }
                //    scene.__objectsAdded = { length: 0, push: function(){}, indexOf: function (){ return -1 }, splice: function(){} }
                //    scene.__objectsRemoved = { length: 0, push: function(){}, indexOf: function (){ return -1 }, splice: function(){} }
                //};
                
				//
				window.addEventListener( 'resize', onWindowResize, false );
                imageLoader.addEventListener('change', handleImage, false);
			}
			function addShadowedLight( x, y, z, color, intensity ) {
				var directionalLight = new THREE.DirectionalLight( color, intensity );
				directionalLight.position.set( x, y, z );
				scene.add( directionalLight );
				directionalLight.castShadow = true;
				var d = 1;
				directionalLight.shadow.camera.left = -d;
				directionalLight.shadow.camera.right = d;
				directionalLight.shadow.camera.top = d;
				directionalLight.shadow.camera.bottom = -d;
				directionalLight.shadow.camera.near = 1;
				directionalLight.shadow.camera.far = 15;
				directionalLight.shadow.mapSize.width = 1024;
				directionalLight.shadow.mapSize.height = 1024;
				directionalLight.shadow.bias = -0.005;
                return directionalLight;
			}
			function onWindowResize() {
				//camera.aspect = window.innerWidth / window.innerHeight;
				//camera.updateProjectionMatrix();
				//renderer.setSize( window.innerWidth, window.innerHeight );
			}
			function animate() {
				animator = requestAnimationFrame( animate );
				render();
			}
			function render() {
                controls.update();
				renderer.render( scene, camera );
			}
            function allowChange() {
                if (renderIsDone)
                    document.getElementById('handlebutton').style.display = 'block';
            }
            
            function changeColour() {
                var c = (document.getElementById('colour').value);
                material = new THREE.MeshPhongMaterial( { color: c, specular: 0x111111, shininess: 200 } );
            }
            
            //////////////////////////////////////
            // base relief
            //////////////////////////////////////
            function handleImage(e){
                handleAll();
            }
            function handleAll(){
                if (animator) cancelAnimationFrame( animator );
                var reader = new FileReader();
                document.getElementById("pleasewait0").style.display = "block";
                document.getElementById("pleasewait").style.display = "block";  
                var redrawFix = document.getElementById("pleasewait").offsetHeight;                
                reader.onload = function(event){
                    var img = new Image();
                    img.onload = function(){
                        cleanup();
                        var scale = 2000;
                        var smoothRadius = document.getElementById('smoothfactor').value;
                        var maxCanvasSize = 800;
                        maxCanvasSize =  document.getElementById('quality').value; 
                        isInverted = (document.getElementById('lightmode').value == 'convex' );
                        var minz = 9e9, maxz=-9e9, zoomx, zoomy;
                        if (img.width > img.height) {
                            canvas.width = maxCanvasSize; // 500; // img.width;
                            canvas.height = canvas.width * img.height / img.width; // img.height;
                            zoomx = 1;
                            zoomy = canvas.height / canvas.width;
                        } else {
                            canvas.height = maxCanvasSize; // 500; // img.width;
                            canvas.width = canvas.height * img.width / img.height; // img.height;
                            zoomy = 1;
                            zoomx = canvas.width / canvas.height;
                        }
                        var context = canvas.getContext( '2d' );             
                        var size = canvas.width * canvas.height;
                        context.drawImage(img,0,0, canvas.width, canvas.width * img.height / img.width );   
                        StackBlur.canvasRGBA(canvas, 0, 0, canvas.width, canvas.height, smoothRadius        );
                        var imgd = context.getImageData(0, 0, canvas.width, canvas.height);
                        var pix = imgd.data;             
                        var planew = canvas.width, planeh = canvas.height;

                        var triangles = (planew-1) * (planeh-1) * 2; // * 2 !!!
                        triangles += 4 * (planew-1) + 4 * (planeh-1); // sides
                        triangles += 2; // back 
                        geometry = new THREE.BufferGeometry();
                        var positions = new Float32Array( triangles * 3 * 3 );
                        var normals = new Float32Array( triangles * 3 * 3 ); 
                        var pA = new THREE.Vector3();
                        var pB = new THREE.Vector3();
                        var pC = new THREE.Vector3();
                        var cb = new THREE.Vector3();
                        var ab = new THREE.Vector3();
                        var dx = -0.5, dy = -0.5;
                        var ax,ay,az,bx,by,bz,cx,cy,cz,ex,ey,ez,nx,ny,nz;
                        function getZ(i,j) { 
                            return (isInverted ? 
                            ((pix[(j*planew+i)*4]+pix[(j*planew+i)*4+1]+pix[(j*planew+i)*4+2])/12.0)/scale :
                            (1.0-(pix[(j*planew+i)*4]+pix[(j*planew+i)*4+1]+pix[(j*planew+i)*4+2])/12.0)/scale );
                        }
                        var k = 0;
                          for(var i=0; i<planew-1; i++) {
                            for(var j=0; j<planeh-1; j++) {  
                                // a c
                                // e b
                                // tri 1
                                ax = ( + ( i + 0 ) * 1.0 / planew + dx ) * zoomx;
                                ay = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
                                az = getZ( i + 0, j + 0 );
                                cx = ( + ( i + 1 ) * 1.0 / planew + dx ) * zoomx;
                                cy = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
                                cz = getZ( i + 1, j + 0 );
                                bx = ( + ( i + 1 ) * 1.0 / planew + dx ) * zoomx;
                                by = ( - ( j + 1 ) * 1.0 / planeh - dy ) * zoomy;
                                bz = getZ( i + 1, j + 1 );
                                positions[ k ]     = ax;
                                positions[ k + 1 ] = ay;
                                positions[ k + 2 ] = az;
                                positions[ k + 3 ] = bx;
                                positions[ k + 4 ] = by;
                                positions[ k + 5 ] = bz;
                                positions[ k + 6 ] = cx;
                                positions[ k + 7 ] = cy;
                                positions[ k + 8 ] = cz;
                                // flat face normals
                                pA.set( ax, ay, az );
                                pB.set( bx, by, bz );
                                pC.set( cx, cy, cz );
                                cb.subVectors( pC, pB );
                                ab.subVectors( pA, pB );
                                cb.cross( ab );
                                cb.normalize();
                                nx = cb.x;
                                ny = cb.y;
                                nz = cb.z;
                                normals[ k ]     = nx;
                                normals[ k + 1 ] = ny;
                                normals[ k + 2 ] = nz;
                                normals[ k + 3 ] = nx;
                                normals[ k + 4 ] = ny;
                                normals[ k + 5 ] = nz;
                                normals[ k + 6 ] = nx;
                                normals[ k + 7 ] = ny;
                                normals[ k + 8 ] = nz;
                                k+=9;
                                // tri 2
                                //ax = ( + ( i + 0 ) * 1.0 / planew + dx ) * zoomx;
                                //ay = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
                                //az = getZ( i + 0, j + 0 );
                                ex = ( + ( i + 0 ) * 1.0 / planew + dx ) * zoomx;
                                ey = ( - ( j + 1 ) * 1.0 / planeh - dy ) * zoomy;
                                ez = getZ( i + 0, j + 1 );
                                //bx = ( + ( i + 1 ) * 1.0 / planew + dx ) * zoomx;
                                //by = ( - ( j + 1 ) * 1.0 / planeh - dy ) * zoomy;
                                //bz = getZ( i + 1, j + 1 );
                                positions[ k ]     = ax;
                                positions[ k + 1 ] = ay;
                                positions[ k + 2 ] = az;
                                positions[ k + 3 ] = ex;
                                positions[ k + 4 ] = ey;
                                positions[ k + 5 ] = ez;
                                positions[ k + 6 ] = bx;
                                positions[ k + 7 ] = by;
                                positions[ k + 8 ] = bz;
                                // flat face normals
                                pA.set( ax, ay, az );
                                pB.set( ex, ey, ez );
                                pC.set( bx, by, bz );
                                cb.subVectors( pC, pB );
                                ab.subVectors( pA, pB );
                                cb.cross( ab );
                                cb.normalize();
                                nx = cb.x;
                                ny = cb.y;
                                nz = cb.z;
                                normals[ k ]     = nx;
                                normals[ k + 1 ] = ny;
                                normals[ k + 2 ] = nz;
                                normals[ k + 3 ] = nx;
                                normals[ k + 4 ] = ny;
                                normals[ k + 5 ] = nz;
                                normals[ k + 6 ] = nx;
                                normals[ k + 7 ] = ny;
                                normals[ k + 8 ] = nz;
                                k+=9;
                                if (az < minz) minz = az;
                                if (az > maxz) maxz = az;
                                if (j==planeh-2) {
                                    if (ez < minz) minz = ez;
                                    if (ez > maxz) maxz = ez;
                                }
                            } 
                            if (cz < minz) minz = cz;
                            if (cz > maxz) maxz = cz;
                          }
                          if (bz < minz) minz = bz;
                          if (bz > maxz) maxz = bz;
                          // done: positions and normals ready, minmax found
                          // add back side:
                          var factorz = (document.getElementById('maxtickness').value - document.getElementById('mintickness').value)/(maxz - minz); 
                          var pozback = minz - document.getElementById('mintickness').value / factorz; 
                          maxxx = ( + ( planew - 1 ) * 1.0 / planew + dx ) * zoomx;
                          maxyy = ( - ( planeh - 1 ) * 1.0 / planeh - dy ) * zoomy;
                          k = addTriangle(positions, normals, k, 
                              -zoomx/2, maxyy, pozback,    -zoomx/2, zoomy/2, pozback,    maxxx, zoomy/2, pozback);
                          k = addTriangle(positions, normals, k, 
                              -zoomx/2, maxyy, pozback,    maxxx, zoomy/2, pozback,    maxxx, maxyy, pozback);
                          // 4 sides
                          for(var i=0; i<planew-1; i++) {
                            ax = ( + ( i + 0 ) * 1.0 / planew + dx ) * zoomx;
                            bx = ( + ( i + 1 ) * 1.0 / planew + dx ) * zoomx;
                            az = getZ( i + 0, planeh-1 );
                            bz = getZ( i + 1, planeh-1 );
                            k = addTriangle(positions, normals, k, 
                              ax, maxyy, az,    bx, maxyy, pozback, bx, maxyy, bz);
                            k = addTriangle(positions, normals, k, 
                              ax, maxyy, az,    ax, maxyy, pozback, bx, maxyy, pozback);
                            az = getZ( i + 0, 0 );
                            bz = getZ( i + 1, 0 );
                            k = addTriangle(positions, normals, k, 
                              ax, +zoomy/2, az,    bx, +zoomy/2, bz,    bx, +zoomy/2, pozback);
                            k = addTriangle(positions, normals, k, 
                              ax, +zoomy/2, az,    bx, +zoomy/2, pozback,    ax, +  zoomy/2, pozback);
                          }
                          for(var j=0; j<planeh-1; j++) {
                            ay = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
                            by = ( - ( j + 1 ) * 1.0 / planeh - dy ) * zoomy;
                            az = getZ( planew-1, j + 0);
                            bz = getZ( planew-1, j + 1);
                            k = addTriangle(positions, normals, k, 
                              maxxx, ay, az,    maxxx, by, bz, maxxx, by, pozback);
                            k = addTriangle(positions, normals, k, 
                              maxxx, ay,az,      maxxx, by, pozback, maxxx, ay, pozback);
                            az = getZ( 0, j + 0 );
                            bz = getZ( 0, j + 1 );
                            k = addTriangle(positions, normals, k, 
                              -zoomx/2, ay, az,        -zoomx/2,by,  pozback, -zoomx/2,by,  bz);
                            k = addTriangle(positions, normals, k, 
                              -zoomx/2,ay,az,        -zoomx/2,ay, pozback, -zoomx/2,by,  pozback);
                          }
                          // ay = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
                        function disposeArray() { /* this.array = null; */ }
                        geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ).onUpload( disposeArray ) );
                        geometry.addAttribute( 'normal', new THREE.BufferAttribute( normals, 3 ).onUpload( disposeArray ) );
                        geometry.computeBoundingSphere()
                        plane = new THREE.Mesh( geometry, material );
                        // scale
                        var factorxy = document.getElementById('maxplatesize').value; 
                        if (maxz - minz == 0) maxz = 1 + minz;
                        plane.scale.set( factorxy, factorxy, factorz );
                        camera.position.set( 0, 0, 2 * factorxy );
                        //camera.near = 0.5 * factorxy;
                        //camera.far = 15 * factorxy;
                        //scene.fog.near = 5 * factorxy;
                        //scene.fog.far = 10 * factorxy;
                        // finishing
                        scene.add(plane);
                        animate();
                        document.getElementById("pleasewait0").style.display = "none";
                        document.getElementById("pleasewait").style.display = "none";
                        document.getElementById("savebutton").style.display = "inline-block";
                        document.getElementById("printbutton").classList.remove('hide');
                        document.getElementById('handlebutton').style.display = 'none';
                        renderIsDone = true;
                        }
                    img.src = event.target.result;
                }
                if (document.getElementById('imageLoader').files.length > 0) {
                // if (e.target.files.length > 0) {
                    reader.readAsDataURL(document.getElementById('imageLoader').files[0]);
                } else {
                        document.getElementById("pleasewait0").style.display = "none";
                        document.getElementById("pleasewait").style.display = "none";
                }    
            }
            function addTriangle(positionsArray, normalsArray, positionInArrays, ax, ay, az, bx, by, bz, cx, cy, cz) {
                                positionsArray[ positionInArrays ]     = ax;
                                positionsArray[ positionInArrays + 1 ] = ay;
                                positionsArray[ positionInArrays + 2 ] = az;
                                positionsArray[ positionInArrays + 3 ] = bx;
                                positionsArray[ positionInArrays + 4 ] = by;
                                positionsArray[ positionInArrays + 5 ] = bz;
                                positionsArray[ positionInArrays + 6 ] = cx;
                                positionsArray[ positionInArrays + 7 ] = cy;
                                positionsArray[ positionInArrays + 8 ] = cz;
                                // flat face normals
                                var pA = new THREE.Vector3();
                                var pB = new THREE.Vector3();
                                var pC = new THREE.Vector3();
                                var cb = new THREE.Vector3();
                                var ab = new THREE.Vector3();
                                pA.set( ax, ay, az );
                                pB.set( bx, by, bz );
                                pC.set( cx, cy, cz );
                                cb.subVectors( pC, pB );
                                ab.subVectors( pA, pB );
                                cb.cross( ab );
                                cb.normalize();
                                var nx = cb.x;
                                var ny = cb.y;
                                var nz = cb.z;
                                normalsArray[ positionInArrays ]     = nx;
                                normalsArray[ positionInArrays + 1 ] = ny;
                                normalsArray[ positionInArrays + 2 ] = nz;
                                normalsArray[ positionInArrays + 3 ] = nx;
                                normalsArray[ positionInArrays + 4 ] = ny;
                                normalsArray[ positionInArrays + 5 ] = nz;
                                normalsArray[ positionInArrays + 6 ] = nx;
                                normalsArray[ positionInArrays + 7 ] = ny;
                                normalsArray[ positionInArrays + 8 ] = nz;
                                return positionInArrays+9;                
            }
            function cleanup() {
                        if (plane) { 
                            scene.remove(plane);
                            geometry.dispose(); 
                            geometry = null;
                            plane = null;
                                // plane.dispose(); // new
                            // geometry.dispose();
                        }
            }
            
            function saveme() {
                document.getElementById("pleasewait0").style.display = "block";
                document.getElementById("pleasewait").style.display = "block";
                BinaryStlWriter.save( plane, 'basrelief.stl' ); 
                document.getElementById("pleasewait0").style.display = "none";
                document.getElementById("pleasewait").style.display = "none";
            }
            
            
			function printAs3DClick2(){
                            document.getElementById("pleasewait0").style.display = "block";
                            document.getElementById("pleasewait").style.display = "block";
                            var form = new FormData();
                            var stldata = BinaryStlWriter.geometryToDataViewBin(plane);
                            var blob = new Blob([stldata], {type: 'application/octet-binary'});
                            form.append("file", blob, document.getElementById('imageLoader').files[0].name + ".stl");
                            form.append("description", "Generated bas-relief");
                            form.append("affiliate_currency", "USD");
                            form.append("affiliate_price", "0");
                            var settings = {
                              "async": true,
                              "crossDomain": true,
                              "url": "https://www.treatstock.com/api/v1/uploads?api-token=239b363facd4104e5657792f097c7ea6",
                              "method": "POST",
                              "headers": {
                                //"cache-control": "no-cache" 
                              },
                              "processData": false,
                              "contentType": false,
                              "dataType": "json",
                              "mimeType": "multipart/form-data",
                              "data": form
                            }
                            $.ajax(settings).done(function (response) {
                              console.log(response);
                              if (response.success == true) {
                                  ga('send', 'event', 'export', 'print', {
                                    hitCallback: function() {
                                      location.href = response.redir;
                                    }
                                  });
                              } else {
                                  alert("Something goes wrong, sorry.");
                                  document.getElementById("pleasewait0").style.display = "none";
                                  document.getElementById("pleasewait").style.display = "none";
                              }
                            });
                        }
            
            
            /// fire!
			init();
			render();