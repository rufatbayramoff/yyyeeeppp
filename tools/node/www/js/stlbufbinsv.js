// Written by Paul Kaplan
// mod by Asen Kurin - for buffered geometry 

var BinaryStlWriter = (function() {
  var that = {};
  var threevector = new THREE.Vector3();
  var normalMatrixWorld = new THREE.Matrix3();
  var matrixWorld;

  var writeVector = function(dataview, offset, vector, isLittleEndian) {
    offset = writeFloat(dataview, offset, vector.x, isLittleEndian);
    offset = writeFloat(dataview, offset, vector.y, isLittleEndian);
    return writeFloat(dataview, offset, vector.z, isLittleEndian);
  };

  var writeFloat = function(dataview, offset, float, isLittleEndian) {
    dataview.setFloat32(offset, float, isLittleEndian);
    return offset + 4;
  };

  var geometryToDataView = function(geometry) {
    var tris = geometry.faces;
    var verts = geometry.vertices;
    
    var isLittleEndian = true; // STL files assume little endian, see wikipedia page
    
    var bufferSize = 84 + (50 * tris.length);
    var buffer = new ArrayBuffer(bufferSize);
    var dv = new DataView(buffer);
    var offset = 0;

    offset += 80; // Header is empty

    dv.setUint32(offset, tris.length, isLittleEndian);
    offset += 4;

    for(var n = 0; n < tris.length; n++) {
      offset = writeVector(dv, offset, tris[n].normal, isLittleEndian);
      offset = writeVector(dv, offset, verts[tris[n].a], isLittleEndian);
      offset = writeVector(dv, offset, verts[tris[n].b], isLittleEndian);
      offset = writeVector(dv, offset, verts[tris[n].c], isLittleEndian);
      offset += 2; // unused 'attribute byte count' is a Uint16
    }

    return dv;
  };

  var writeVectorBin = function(dataview, offset, vector, position, isLittleEndian, isNormal) {
    if (isNormal) {
        threevector.set(vector[position], vector[position+1], vector[position+2]).applyMatrix3( normalMatrixWorld ).normalize();
    }  else {// position
        threevector.set(vector[position], vector[position+1], vector[position+2]).applyMatrix4( matrixWorld );
    }
    offset = writeFloat(dataview, offset, threevector.x, isLittleEndian);
    offset = writeFloat(dataview, offset, threevector.y, isLittleEndian);
    return writeFloat(dataview, offset, threevector.z, isLittleEndian);
  };
  
  
  var geometryToDataViewBin = function(mesh) {
    var geometry = mesh.geometry;
    var positions = geometry.getAttribute('position');
    var normals = geometry.getAttribute('normal');
    var trislen = positions.length / 9;
    
    var isLittleEndian = true; // STL files assume little endian, see wikipedia page
    matrixWorld = mesh.matrixWorld;
    normalMatrixWorld.getNormalMatrix( matrixWorld );
    
    var bufferSize = 84 + (50 * trislen);
    var buffer = new ArrayBuffer(bufferSize);
    var dv = new DataView(buffer);
    var offset = 0;

    offset += 80; // Header is empty

    dv.setUint32(offset, trislen, isLittleEndian);
    offset += 4;

    for(var n = 0; n < trislen; n++) {
      offset = writeVectorBin(dv, offset, normals.array,   n*9    , isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9  , isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9+3, isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9+6, isLittleEndian);
      offset += 2; // unused 'attribute byte count' is a Uint16
    }

    return dv;
  };

  
  
  var save = function(geometry, filename) {
    var dv = geometryToDataViewBin(geometry);
    var blob = new Blob([dv], {type: 'application/octet-binary'});
    
    // FileSaver.js defines `saveAs` for saving files out of the browser
    saveAs(blob, filename);
  };

  that.save = save;
  that.geometryToDataViewBin = geometryToDataViewBin;
  return that;
}());