if (!Detector.webgl) Detector.addGetWebGLMessage();

var container, camera, scene, renderer, raycaster, geometry, controls, material, materialColored, meshMain, objects,
    instrMaterial, theInstrumentMesh;
var light1, light2, light3, light4;
var mouse = new THREE.Vector2(), vivid = true;

function init(w,h) {
    // camera
    camera = new THREE.PerspectiveCamera(35, w / h, 1, 10000);
    //camera.aspect = w / h;
    //camera.updateProjectionMatrix
    // scene
    scene = new THREE.Scene();
    raycaster = new THREE.Raycaster();    
    // renderer
    renderer = new THREE.WebGLRenderer({antialias: true, preserveDrawingBuffer: true});
    renderer.setClearColor(0xfcfcfc); 
    renderer.setSize(w, h);
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.renderReverseSided = false;
    // container
    container = document.createElement('div');
    document.getElementById('viewer').appendChild(container);
    container.appendChild(renderer.domElement);
    // controls
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.target.set(0, 0, 0);
    controls.zoomSpeed = 0.5;
    controls.update();
    // lights
    scene.add(new THREE.HemisphereLight(0x443333, 0x111122));
    var r = 1;
    light1 = addShadowedLight(2*r, 2*r, 0, 0xffffff, 1);
    light2 = addShadowedLight(-1*r, 1*r, 1*r, 0xffffff, 0.8);
    light3 = addShadowedLight(-1*r, 0.5*r, -1*r, 0xffffff, 0.6);
    light4 = addShadowedLight(0.5*r, -0.5*r, -2*r, 0xffffff, 0.6);
    // material
    material = new THREE.MeshPhongMaterial({
        color: 0xff5533, specular: 0x111111, shininess: 200, 
        side: THREE.DoubleSide // , vertexColors: THREE.VertexColors
    });
    materialColored = new THREE.MeshPhongMaterial({
        color: 0xcccccc, specular: 0x111111, shininess: 200, 
        side: THREE.DoubleSide , vertexColors: THREE.VertexColors
    });
    instrMaterial = new THREE.MeshStandardMaterial( { color: 0x000088, opacity: 0.5, transparent: true } );
    //window.addEventListener('resize', onWindowResize, false);
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    renderer.domElement.ondblclick=onDoubleClick;
}    
function showStl(stlName, isColored) {  
    objects = [];
    var loader = isColored? new THREE.ST2Loader() : new THREE.STLLoader();
    // var loader = new THREE.STLLoader();
    if (meshMain) scene.remove(meshMain);
    loader.load("../"+stlName, function (geometry1) {
        console.log('loaded. parsing..');
        geometry = geometry1;
        // geometry
        geometry.computeVertexNormals();
        geometry.computeBoundingSphere();
        var sphere = geometry.boundingSphere;
        geometry.computeBoundingBox();
        var indexBox = geometry.boundingBox;
        //geometry.center();
        var r =  sphere.radius;
        meshMain = new THREE.Mesh(geometry, (isColored?materialColored:material) );
        makeInstrument(geometry.instrumentDiameter); 
        // meshMain.rotation.set(0 , 0, 0);
        // mesh.scale.set(scale, scale, scale);
        // mesh.position.set( 0, 0, 0 );
        meshMain.castShadow = true;
        meshMain.receiveShadow = true;
        // add to scene
        scene.add(meshMain);
        objects.push(meshMain);  
        // Lights
        light1.position.set(2*r, 2*r, 0);
        light2.position.set(-1*r, 1*r, 1*r);
        light3.position.set(-1*r, 0.5*r, -1*r);
        light4.position.set(0.5*r, -0.5*r, -2*r);
        controls.target.copy(sphere.center);
        //camera.lookAt(sphere.center);
        camera.position.set(sphere.center.x, sphere.center.y, sphere.center.z + 4*r); 
    });
}

function onDoubleClick(event) {
    vivid = !vivid;
}
function onDocumentMouseMove( event ) {
    event.preventDefault();
    if (!meshMain || !meshMain.geometry || !meshMain.geometry.instrumentDiameter) return;
    if (!vivid) return;
    var rect = renderer.domElement.getBoundingClientRect();
    mouse.x =  (event.clientX - rect.left) * 2.0 / (rect.right - rect.left) - 1;
    mouse.y =  (event.clientY - rect.bottom) * 2.0 / (rect.top - rect.bottom) - 1;
    raycaster.setFromCamera( mouse, camera );
    intersections = raycaster.intersectObjects( objects );
    numObjects = objects.length;
    if ( intersections.length > 0 ) {
        //if ( intersected && intersected != intersections[ 0 ].object )
        //    hideIntersection();
        intersected = intersections[ 0 ].object;
        var instrumentPos = new THREE.Vector3(
            meshMain.geometry.instrumentPositions[intersections[ 0 ].faceIndex*3+0],
            meshMain.geometry.instrumentPositions[intersections[ 0 ].faceIndex*3+1],
            meshMain.geometry.instrumentPositions[intersections[ 0 ].faceIndex*3+2]
            );
        var instrumentAxis = restoreAxis(meshMain.geometry.instrumentAxis[intersections[ 0 ].faceIndex * 3]);
        if (instrumentAxis) {
            var axisOriginal = new THREE.Vector3(0, 1, 0);
            theInstrumentMesh.quaternion.setFromUnitVectors(axisOriginal, instrumentAxis.clone().normalize());
            var p = instrumentPos.add(instrumentAxis.multiplyScalar(1000/2));
            theInstrumentMesh.position.set( p.x, p.y, p.z );
            theInstrumentMesh.visible = true;
        } else {
            theInstrumentMesh.visible = false;
        }
        // showIntersection(intersections[ 0 ].object, intersections[ 0 ].point, intersections[ 0 ].face);
        // console.log(intersections[ 0 ].faceIndex); // 4056
    //} else if ( intersected ) {
        // hideIntersection();
        // intersected = null;
    }
}
function restoreAxis(axisCode) {
         if (axisCode == 1) { return new THREE.Vector3( 1, 0, 0); }
    else if (axisCode == 2) { return new THREE.Vector3(-1, 0, 0); }
    else if (axisCode == 3) { return new THREE.Vector3( 0, 1, 0); }
    else if (axisCode == 4) { return new THREE.Vector3( 0,-1, 0); }
    else if (axisCode == 5) { return new THREE.Vector3( 0, 0, 1); }
    else if (axisCode == 6) { return new THREE.Vector3( 0, 0,-1); }
    ; return null;
}
function makeInstrument(diameter) {
        if (theInstrumentMesh) { 
            theInstrumentMesh.visible = false;
            scene.remove( theInstrumentMesh );
        };
        instrRadius = diameter/2;
        instrLen = 1000; // sphere.radius * 2; 
        theInstrumentMesh = new THREE.Mesh( new THREE.CylinderGeometry( instrRadius, instrRadius, instrLen, 100/*rsegm*/, 5/*hsegm*/ ), instrMaterial );
        theInstrumentMesh.position.set( 0, 0, 0 );
        theInstrumentMesh.visible = false;
        scene.add( theInstrumentMesh );
}

function addShadowedLight(x, y, z, color, intensity) {
    var directionalLight = new THREE.DirectionalLight(color, intensity);
    directionalLight.position.set(x, y, z);
    scene.add(directionalLight);
    directionalLight.castShadow = true;
    var d = 1;
    directionalLight.shadow.camera.left = -d;
    directionalLight.shadow.camera.right = d;
    directionalLight.shadow.camera.top = d;
    directionalLight.shadow.camera.bottom = -d;
    directionalLight.shadow.camera.near = 1;
    directionalLight.shadow.camera.far = 15;
    directionalLight.shadow.mapSize.width = 1024;
    directionalLight.shadow.mapSize.height = 1024;
    directionalLight.shadow.bias = -0.005;
    return directionalLight;
}
//function onWindowResize() {
    //camera.aspect = window.innerWidth / window.innerHeight;
    //camera.updateProjectionMatrix();
    //renderer.setSize( window.innerWidth, window.innerHeight );
//}
function render() {
    controls.update();
    renderer.render(scene, camera);
}
function animate() {
    requestAnimationFrame(animate);
    render();
}

