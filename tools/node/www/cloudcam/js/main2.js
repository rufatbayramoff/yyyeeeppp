var DEGREE_TO_RAD = 0.0174533;
if (!Detector.webgl)
    Detector.addGetWebGLMessage();
var container;
var camera, cameraTarget, scene, renderer, raycaster, intersected, theInstrumentMesh, activeInstrument, 
    meshMain, material, origGeometry, origGeometryBuffered, unbuffered, measures, geometryOffsetted,
    origGeometryLowQualityUnbuffered, globalParams,  meshMainOriginal,
    roughInstrument, finishInstrument, activeMachine, activeMaterial, activeStock;
var controls;
var light1, light2;
var mouse = new THREE.Vector2(), objects = [], objectsHQ = [], tesselationNewVertices;
var bbox, bbox2;
var instrRadius, instrLen;
var vivid = true;
var rndVec = new THREE.Vector3(3.141567,2.543657,1.3246543);
var tesselationSize = 0.5;
var instrSizesDetected = {}, instrumentActiveDetected = false;
var indexPlaneXY, indexPlaneXZ, indexPlaneYZ, indexBox;
var indexSteps = 100;
init();
animate();
function init() {
    activeInstrument = {d:2};
    tesselationSize = activeInstrument.d/4;
    document.getElementById("pleasewait0").style.display = "block";
    document.getElementById("pleasewait").style.display = "block";
    container = document.createElement('div');
    document.getElementById('model-col').appendChild(container);
    container.className = 'model-view';
    camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.set(0, 0, 4);
    cameraTarget = new THREE.Vector3(0, 0, 0);
    scene = new THREE.Scene();
    raycaster = new THREE.Raycaster();
    // raycaster.params.Points.threshold = threshold;
    //scene.fog = new THREE.Fog(0x72645b, 2, 15);
    // ASCII file
    var loader = new THREE.STLLoader();
    if (stlNameLowQuality != stlName)
        loader.load(stlNameLowQuality, function (geometry) {
            origGeometryLowQualityUnbuffered = new THREE.Geometry().fromBufferGeometry( geometry );
            origGeometryLowQualityUnbuffered.mergeVertices();
            origGeometryLowQualityUnbuffered.computeVertexNormals();
            origGeometryLowQualityUnbuffered.computeBoundingSphere();
            var mesh0 = new THREE.Mesh(origGeometryLowQualityUnbuffered, new THREE.MeshBasicMaterial({side: THREE.DoubleSide}));
            objects.push( mesh0 );
        });
    loader.load(stlName, function (geometry) {
        console.log('loaded. parsing..');
        origGeometryBuffered = geometry;
        // geometry
        geometry.computeVertexNormals();
        geometry.computeBoundingSphere();
        sphere = geometry.boundingSphere;
        geometry.computeBoundingBox();
        indexBox = geometry.boundingBox;
        geometry.center();
        var scale = 1 / sphere.radius;
        var r =  sphere.radius;
        // prepare caster
        unbuffered = new THREE.Geometry().fromBufferGeometry( geometry );
        unbuffered.mergeVertices();
        origGeometry = unbuffered.clone();
        // tesselateGeomerty(unbuffered, tesselationSize);
        // unbuffered.mergeVertices();
        // prepareFastInstrumentValidators();
        var mesh0 = new THREE.Mesh(origGeometry, new THREE.MeshBasicMaterial({side: THREE.DoubleSide}));
        if (stlNameLowQuality == stlName) {
            origGeometryLowQualityUnbuffered = origGeometry;
            objects.push( mesh0 );
        }
        objectsHQ.push( mesh0 );
        // recolorize
        // var geometryFaceted = faceteColorizeGeometry(unbuffered, activeInstrument);
        // mesh
        material = new THREE.MeshPhongMaterial({
            color: 0xff5533, specular: 0x111111, shininess: 200, 
            side: THREE.DoubleSide, vertexColors: THREE.VertexColors});
        meshMain = new THREE.Mesh(unbuffered, material);
        meshMainOriginal = meshMain;
        meshMain.rotation.set(0 /* -90 * DEGREE_TO_RAD */, 0, 0);
        // mesh.scale.set(scale, scale, scale);
        // mesh.position.set( 0, 0, 0 );
        meshMain.castShadow = true;
        meshMain.receiveShadow = true;
        // add to scene
        scene.add(meshMain);
        // Ground
        /*
        var plane = new THREE.Mesh(
                new THREE.PlaneBufferGeometry(40*r, 40*r),
                new THREE.MeshPhongMaterial({color: 0x999999, specular: 0x101010})
                );
        plane.rotation.x = -Math.PI / 2;
        plane.position.y = -r;
        scene.add(plane);
        plane.receiveShadow = true;
        */
        // Lights
        scene.add(new THREE.HemisphereLight(0x443333, 0x111122));
        light1 = addShadowedLight(2*r, 2*r, 0, 0xffffff, 1);
        light2 = addShadowedLight(-1*r, 1*r, 1*r, 0xffaa88, 0.8);
        light3 = addShadowedLight(-1*r, 0.5*r, -1*r, 0xffffff, 0.6);
        light4 = addShadowedLight(0.5*r, -0.5*r, -2*r, 0xffffff, 0.6);
        camera.position.set(0, 0, 4*r); 
        // add bounds
        
        // instrument
        remakeActiveInstrument(false);
        
        // estimate(/*geometry, geometryOffsetted*/);
        measures = measure(geometry);

        
        document.getElementById("pleasewait0").style.display = "none";
        document.getElementById("pleasewait").style.display = "none";
        document.getElementById("opertionss").style.display = "block";
        showProgress();
        
    });
    // get params
    var loaderJSON = new THREE.FileLoader();
    loaderJSON.load(
        serviceName,
        function ( text ) {
            globalParams = JSON.parse( text );
            // console.log(globalParams);
        }
    );
    
    // renderer
    renderer = new THREE.WebGLRenderer({antialias: true, preserveDrawingBuffer: true});
    // renderer.setClearColor(scene.fog.color);
    renderer.setClearColor(0xfcfcfc); // 0x72645b);
    // renderer.setPixelRatio( window.devicePixelRatio );
    // renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.setSize(400, 300);
    camera.aspect = 800 / 600;
    camera.updateProjectionMatrix();
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.renderReverseSided = false;
    //
    container.appendChild(renderer.domElement);
    // controls
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.target.set(0, 0, 0);
    controls.zoomSpeed = 0.5;
    controls.update();

    //
    //
    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    renderer.domElement.ondblclick=onDoubleClick;
}
function remakeActiveInstrument(cleanup) {
        if (cleanup) scene.remove( theInstrumentMesh );
        instrRadius = activeInstrument.d/2;
        instrLen = sphere.radius * 2; 
        var instrMaterial = new THREE.MeshStandardMaterial( { color: 0x000088, opacity: 0.5, transparent: true } );
        theInstrumentMesh = new THREE.Mesh( new THREE.CylinderGeometry( instrRadius, instrRadius, instrLen, 100/*rsegm*/, 5/*hsegm*/ ), instrMaterial );
        theInstrumentMesh.position.set( 0, 0, 0 );
        theInstrumentMesh.visible = false;
        scene.add( theInstrumentMesh );
}
function detectInstrumentSizes() {
    // objects = [];
    var minimalInstrument = {d: 2.00}; 
    // tesselateGeomerty(unbuffered, minimalInstrument.d/3);
    //prepareFastInstrumentValidators();
    geometry = origGeometryLowQualityUnbuffered; // origGeometry;
    instrSizesDetected = {};    
    var faces = geometry.faces;
    var vertices = geometry.vertices;
    var cnt = faces.length;
    for (var i = 0; i<cnt; i++) {
        if (i % 100  == 0) showProgress("processed: " + i + " from " + cnt + " triangles.");
        // find ray
        var face = faces[i];
        var normal = face.normal;
        var facea = vertices[face.a];
        var faceb = vertices[face.b];
        var facec = vertices[face.c];    
        var center = facea.clone().add(faceb).add(facec).multiplyScalar(1/3).add(normal.clone().multiplyScalar(0.001));
        // try detect if it is ray from green area?
        //var processingData = getFaceProcessingData(geometry, face, minimalInstrument, center); 
        //if (processingData && processingData.instrumentPosition) {
        if (true) {
            // try find ray length
            var l = getRayLength(center, normal);
            if (l > 0){
                var lr = l.toFixed(1);
                var tarea = areaOfTriangle(facea.x, facea.y, facea.z, faceb.x, faceb.y, faceb.z, facec.x, facec.y, facec.z);
                if (lr in instrSizesDetected) {
                    instrSizesDetected[lr].area += tarea;
                    instrSizesDetected[lr].cnt += 1;
                } else {
                    instrSizesDetected[lr] = { area: tarea, cnt: 1};
                }
            }
        } else {
            // red area - not processed
        }
    }
    var filterAreaVal = measures.area * 0.01; // 1% of area
    var filterCntVal = 100; // or more than 100 triangles
    var minInstrumentD = 1e10;
    Object.keys(instrSizesDetected).forEach(function (key) {
        if (instrSizesDetected[key].area > filterAreaVal || instrSizesDetected[key].cnt > filterCntVal) {
            console.log("size ~= " + key + ", cnt=" + instrSizesDetected[key].cnt+" area="+instrSizesDetected[key].area);
            if (minInstrumentD > key && key>0) minInstrumentD = parseFloat(key);
        }
        // do something with obj[key]
    });
    
    // find best instrument
    console.log("trying to find instrument less than " + minInstrumentD * 0.8);
    // fit rough?
    roughInstrument = _(globalParams.millingInstruments)
        .filter(function(o) { return o.quality == "rough" && o.d < minInstrumentD * 0.8; })
        .sortBy(["d"]).reverse()
        .head();
    // fit any?
    if (!roughInstrument) roughInstrument = _(globalParams.millingInstruments)
        .filter(function(o) { return o.d < minInstrumentD * 0.8; })
        .sortBy(["d"]).reverse()
        .head();    
    // smallest?
    if (!roughInstrument) roughInstrument = _(globalParams.millingInstruments)
        .sortBy(["d","quality"]) // 
        .head();  
    if (!roughInstrument) {
        alert('cant find any instrument');
        return;
    }
    showProgress("Rough instrument choosen: d=" + roughInstrument.d + "");
    
    // find stock
    var delta = globalParams.settings.stockSpacers; // mm over box
    // find finish instrument
    finishInstrument = _(globalParams.millingInstruments)
        .filter(function(o) { return o.quality == "finish" && o.d < minInstrumentD * 0.8; })
        .sortBy(["d"]).reverse()
        .head();
    if (!finishInstrument) finishInstrument = _(globalParams.millingInstruments)
        .filter(function(o) { return o.quality == "rough" && o.d < minInstrumentD * 0.8; })
        .sortBy(["d"]).reverse()
        .head();
    if (!finishInstrument) finishInstrument = roughInstrument;   
    var delta2stage = finishInstrument.delta > 0 ? finishInstrument.delta : defaultParams.defaultDeltaStage2; // mm over box
    activeInstrument = finishInstrument; // .d = minInstrumentD * 0.6; // TODO!!!
    showProgress("Finish = active instrument choosen: d=" + finishInstrument.d + "");

    // stock
    var box = indexBox;
    var material1 = new THREE.MeshStandardMaterial( {
        color: 0x000033,
        opacity: 0.1,
        transparent: true, depthWrite: false
    } );
    bbox = new THREE.Mesh( 
            new THREE.BoxGeometry( 
                box.max.x - box.min.x + delta*2, 
                box.max.y - box.min.y + delta*2, 
                box.max.z - box.min.z + delta*2),
            material1 );
    scene.add( bbox );
    //mesh.updateMatrixWorld(true);
    //var bbox2 = new THREE.BoundingBoxHelper( mesh );
    //scene.add( bbox2 );
    
    
    // stage2 area
    var material2 = new THREE.MeshStandardMaterial( { 
        color: 0x553300,
        opacity: 0.4,
        transparent: true } );
    geometryOffsetted = offsetGeometry(origGeometryBuffered, delta2stage);
    bbox2 = new THREE.Mesh( geometryOffsetted, material2 );
    scene.add( bbox2 );
    bbox2.visible = false;
    
    // showProgress();
    estimate();
}
function modelle() {
    remakeActiveInstrument(true);
    showProgress("tesselating deep...");
    tesselateGeomerty(unbuffered, activeInstrument.d/3);
    //unbuffered.mergeVertices();
    instrumentActiveDetected = true;
    showProgress("preparing validators...");
    prepareFastInstrumentValidators();
    //unbuffered.mergeVertices();

    // re-set-up raycasters
    objectsHQ = [];
    objectsHQ.push(meshMain);    
    
    // calc colored
    var geometryFaceted = faceteColorizeGeometry(unbuffered, activeInstrument);
    // show new colored
    scene.remove(meshMain);
    meshMain = new THREE.Mesh(geometryFaceted, material);
    meshMain.rotation.set(0, 0, 0);
    // mesh.scale.set(scale, scale, scale);
    // mesh.position.set( 0, 0, 0 );
    meshMain.castShadow = true;
    meshMain.receiveShadow = true;
    // add to scene
    scene.add(meshMain);
    
    
    showProgress();
}
function onDoubleClick(event) {
    vivid = !vivid;
}
function onDocumentMouseMove( event ) {
    event.preventDefault();
    if (!vivid || !instrumentActiveDetected) return;
    var rect = renderer.domElement.getBoundingClientRect();
    mouse.x =  (event.clientX - rect.left) * 2.0 / (rect.right - rect.left) - 1;
    mouse.y =  (event.clientY - rect.bottom) * 2.0 / (rect.top - rect.bottom) - 1;
    raycaster.setFromCamera( mouse, camera );
    intersections = raycaster.intersectObjects( objectsHQ );
    numObjects = objects.length;
    if ( intersections.length > 0 ) {
        if ( intersected && intersected != intersections[ 0 ].object )
            hideIntersection();
        intersected = intersections[ 0 ].object;
        showIntersection(intersections[ 0 ].object, intersections[ 0 ].point, intersections[ 0 ].face);
        // console.log(intersections[ 0 ].faceIndex); // 4056
    } else if ( intersected ) {
        hideIntersection();
        intersected = null;
    }
}
function hideIntersection()
{
    //intersected.material.color.setHex( 0xff5533 );
    theInstrumentMesh.visible = false;
}
function showIntersection(iMesh, iPoint, iFace)
{
    //intersected.material.color.setHex( 0x00ffff );

    // var axis = new THREE.Vector3(0,1,0); // we have y-oriented cylinder?
    // var delta = iFace.normal.clone().projectOnPlane(axis).normalize();
    // p.add(axis.multiplyScalar(instrLen/2));
    // p.add(delta.multiplyScalar(instrRadius));
    // p.add(axis.multiplyScalar(instrLen/2));
    iPoint = iMesh.geometry.vertices[iFace.a].clone()
        .add(iMesh.geometry.vertices[iFace.b])
        .add(iMesh.geometry.vertices[iFace.c])
        .multiplyScalar(1/3);// TODO: call at center of tri
    var processingData = getFaceProcessingData(iMesh.geometry, iFace, activeInstrument, iPoint); 
    // var processingData = getFaceProcessingData(iMesh.geometry, iFace, activeInstrument, null); 
    if (processingData && processingData.instrumentPosition) {
        var axisOriginal = new THREE.Vector3(0, 1, 0);
        theInstrumentMesh.quaternion.setFromUnitVectors(axisOriginal, processingData.instrumentPosition.axis.clone().normalize());
        
        var p = processingData.instrumentPosition.startPoint.clone()
            .add(processingData.instrumentPosition.axis.multiplyScalar(instrLen/2));
        theInstrumentMesh.position.set( p.x, p.y, p.z );
        theInstrumentMesh.visible = true;
    } else {
        theInstrumentMesh.visible = false;
    }
}

function estimate(/*geometry, geometryOffsetted*/) {
        geometry = origGeometryBuffered;
        // detectInstrumentSizes(geometry);
        var box = geometry.boundingBox;
        var delta = globalParams.settings.stockSpacers; // mm over box
        // finishInstrument ; roughInstrument;   
        var delta2stage = finishInstrument.delta > 0 ? finishInstrument.delta : defaultParams.defaultDeltaStage2; // mm over box
        //estimate
        var result = '', sum=0;
        var measuresOffsetted = measure(geometryOffsetted);
        result += 'Dimensions of object: ' + Math.round(box.max.x - box.min.x) + "x" +
                Math.round(box.max.y - box.min.y) + "x" +
                Math.round(box.max.z - box.min.z) + " mm3<br>";
        result += 'Volume of object: ' + Math.round(measures.volume) + " mm3<br>";
        var vofstock = (box.max.x - box.min.x + delta*2)*
                       (box.max.y - box.min.y + delta*2)*
                       (box.max.z - box.min.z + delta*2);
        var volof2stage = measures.area * delta2stage;       
        var volof1stage = vofstock - measures.volume - volof2stage;        
        result += 'Reserves for stock size: +' + delta + " mm on every side<br>";
        result += 'Reserves between stages: +' + delta2stage + " mm orthogonal on every face<br>";
        result += 'Volume of stock: ' + Math.round(vofstock) + " mm3<br>";
        result += 'Volume for machining 1st stage: ' + Math.round(volof1stage) + " mm3<br>";
        result += 'Volume for machining 2st stage: ' + Math.round(volof2stage) + " mm3<br>";
        result += 'Volume for machining 2st stage method 2: ' + Math.round(measuresOffsetted.volume-measures.volume) + " mm3<br>";
        result += 'Percent for machining: ' + Math.round(100*(vofstock - measures.volume)/vofstock) + " %<br>";
        
        // find machine
        var stockMaxSize = Math.max(box.max.x - box.min.x, box.max.y - box.min.y, box.max.z - box.min.z);
        activeMachine = _(globalParams.millingMachines)
            .filter(function(o) { return o.w > stockMaxSize && o.h > stockMaxSize && o.l > stockMaxSize; })
            .sortBy(["priceMachining"]) // .reverse()
            .head(); // TODO precalc price for EVERY machine???

        
        //for (key in millingParams.instruments) {
        { // rough     
            var instrument = roughInstrument;
            // d is diameter in mm
            // fr in mm per rotation
            // s in m/min
            // l is length in mm
            // delta is betwin stages 1 and 2 in mm
            var speed = Math.min(instrument.s, activeMachine.maxRPM * instrument.d * 3.14 / (1000)); // speed in m in m, limit by rpm
            var mrr = (speed*1000/3.14)*(instrument.fr)*(instrument.l) / 60; // mm3 in seconds
            var vol, usage;
            vol = volof1stage;
            usage = defaultParams.instrumentRoughUsage;
            var time = vol / (mrr * usage);
            var price = activeMachine.priceMachining * (time) / 3600;
            result += '&nbsp; Instrument: s='+instrument.s+' d='+instrument.d+' fr='+instrument.fr+' l='+instrument.l+'<br>';
            result += '&nbsp;&nbsp; Processing time : '+Math.round(time)+'sec sec<br>';
            result += '&nbsp;&nbsp; <b>processing price = USD '+Math.round(price*100)/100+'</b><br>';
            sum += price;
        }
        { // finish     
            var instrument = roughInstrument;
            // d is diameter in mm
            // fr in mm per rotation
            // s in m/min
            // l is length in mm
            // delta is betwin stages 1 and 2 in mm
            var speed = Math.min(instrument.s, activeMachine.maxRPM * instrument.d * 3.14 / (1000)); // speed in m in m, limit by rpm
            var mrr = (speed*1000/3.14)*(instrument.fr)*(instrument.l) / 60; // mm3 in seconds
            var vol, usage;
            vol = volof2stage;
            usage = defaultParams.instrumentFinishUsage;
            var time = vol / (mrr * usage);
            var price = activeMachine.priceMachining * (time) / 3600;
            result += '&nbsp; Instrument: s='+instrument.s+' d='+instrument.d+' fr='+instrument.fr+' l='+instrument.l+'<br>';
            result += '&nbsp;&nbsp; Processing time : '+Math.round(time)+'sec sec<br>';
            result += '&nbsp;&nbsp; <b>processing price = USD '+Math.round(price*100)/100+'</b><br>';
            sum += price;
        }

        activeStock = _(globalParams.stocks)
            // .filter(function(o) { return o.w > stockMaxSize && o.h > stockMaxSize && o.l > stockMaxSize; })
            .sortBy(["price"]) // .reverse()
            .head(); // TODO precalc price for EVERY machine???
        if (!activeStock) {
            alert('cant find material');
            return;
        }
        activeMaterial = _(defaultParams.materials)
            .filter(function(o) { return o.material == activeStock.material; })
            .head(); // TODO precalc price for EVERY machine???
        //for (key in millingParams.materials) {
            // var mat = millingParams.materials[key];
            // density in kg per m3
            // price in usd per kg
            var price = activeStock.price * activeMaterial.density *  (vofstock)/(1000*1000*1000);
            result += '<b>Material price = USD '+Math.round(price*100)/100+'</b><br>';
            sum += price;
        //}
        
        // TODO 6-axis
        var price = activeMachine.priceLabour * activeMachine.placetime / 3600;
        result += '<b>Stock placing and unplacing time = USD '+Math.round( price *100)/100+'</b> '+activeMachine.placetime+'sec<br>';
        sum += price;
        
        var setup = activeMachine.priceLabour * activeMachine.programmingtime / 60;
        result += "Machine set up: "+(activeMachine.programmingtime)+'minutes<br>';
        result += "Total price per pcs: USD "+Math.round( sum  *100)/100+'<br>';
        result += '<hr>';
        result += 'Price per one, if you order only one : USD '+Math.round( (sum + setup      ) *100)/100+'<br>';
        result += 'Price per one, if you order 10 pcs   : USD '+Math.round( (sum + setup/10   ) *100)/100+'<br>';
        result += 'Price per one, if you order 100 pcs  : USD '+Math.round( (sum + setup/100  ) *100)/100+'<br>';
        result += 'Price per one, if you order 1000 pcs : USD '+Math.round( (sum + setup/1000) *100)/100+'<br>';
        
        result += "<br><a href='index.php'>Go home</a>";
        document.getElementById('log').innerHTML = result;    
        showProgress();
}

vertexToFace=[];
function crossReference(g) {
    vertexToFace = [];
    for (var fx = 0; fx < g.vertices.length; fx++) {
        vertexToFace[fx] = new Array();
    }
    for (var fx = 0; fx < g.faces.length; fx++) {
        var f = g.faces[fx];
        var ax = f.a;
        var bx = f.b;
        var cx = f.c;
        vertexToFace[ax].push(fx);
        vertexToFace[bx].push(fx);
        vertexToFace[cx].push(fx);
    }
}
function getnormalizator(faces, vertexid, offsetmm) {
    var faceArr = vertexToFace[vertexid];
    var v = new THREE.Vector3(0,0,0);
    var usedNormals = [];
    for (var fx = 0; fx < faceArr.length; fx++) {
        var isUsed = false;
        for (var i = 0; i < usedNormals.length; i++) {
            if (faces[faceArr[fx]].normal.distanceTo(usedNormals[i]) < 0.25) {
                isUsed = true;
                usedNormals[i].add(faces[faceArr[fx]].normal);
                usedNormals[i].normalize();
                break;
            }
        }
        if (!isUsed){
            usedNormals.push(faces[faceArr[fx]].normal.clone());
            // v.add(faces[faceArr[fx]].normal);
        }
    }
    // v is sum of normal
    for (var fx = 0; fx < usedNormals.length; fx++) {
        v.add(usedNormals[fx]);
    }
    v = v.normalize();
    var k = 0;
    for (var fx = 0; fx < usedNormals.length; fx++) {
        // var k2 = 1/ v.clone().dot(faces[faceArr[fx]].normal);
        var k2 = 1/ v.clone().dot(usedNormals[fx]);
        if (k2 > k && k2 > 0) k = k2;
    }
    if (k>2)k=2;
    v.multiplyScalar ( k * offsetmm );
    return v; 
}

function faceteColorizeGeometry(geometry, instrument){
    // var unbuffered = new THREE.Geometry().fromBufferGeometry( geometry );
    // var unbuffered = geometry;
    // unbuffered.mergeVertices();
    // prepareFastInstrumentValidators();
    var vertices = unbuffered.vertices;
    var faces = unbuffered.faces;
    // pre-count
    var cnt = faces.length;
    // get mem
    var newpositions = new Float32Array( cnt * 3 * 3);
    var newnormals = new Float32Array( cnt * 3  * 3);
    var newcolors = new Float32Array( cnt * 3 * 3 );
    var color = new THREE.Color();
    // fill in
    for (var i = 0; i<cnt; i++) {
        if (i % 10000  == 0) showProgress("colorized: " + i + " from " + cnt + " triangles.");
        var face = faces[i];
        var normal = face.normal;
        var facea = vertices[face.a];
        var faceb = vertices[face.b];
        var facec = vertices[face.c];
        var point = facea.clone().add(faceb).add(facec).multiplyScalar(1/3);
        newpositions[ i*9+0 ] = facea.x; 
        newpositions[ i*9+1 ] = facea.y; 
        newpositions[ i*9+2 ] = facea.z;
        newpositions[ i*9+3 ] = faceb.x; 
        newpositions[ i*9+4 ] = faceb.y; 
        newpositions[ i*9+5 ] = faceb.z; 
        newpositions[ i*9+6 ] = facec.x; 
        newpositions[ i*9+7 ] = facec.y; 
        newpositions[ i*9+8 ] = facec.z; 
        newnormals[ i*9+0 ] = normal.x; 
        newnormals[ i*9+1 ] = normal.y; 
        newnormals[ i*9+2 ] = normal.z;
        newnormals[ i*9+3 ] = normal.x; 
        newnormals[ i*9+4 ] = normal.y; 
        newnormals[ i*9+5 ] = normal.z; 
        newnormals[ i*9+6 ] = normal.x; 
        newnormals[ i*9+7 ] = normal.y; 
        newnormals[ i*9+8 ] = normal.z;
        color = getFaceProcessingData(unbuffered, face, instrument, point).color; // was null)
        // color.setRGB( Math.random(), Math.random(), Math.random() );
        newcolors[ i*9+0 ] = color.r;
        newcolors[ i*9+1 ] = color.g;
        newcolors[ i*9+2 ] = color.b;
        newcolors[ i*9+3 ] = color.r;
        newcolors[ i*9+4 ] = color.g;
        newcolors[ i*9+5 ] = color.b;
        newcolors[ i*9+6 ] = color.r;
        newcolors[ i*9+7 ] = color.g;
        newcolors[ i*9+8 ] = color.b;
    }        
    // finish up
    var geometry1 = new THREE.BufferGeometry();
    function disposeArray() { /* this.array = null; */ }
    geometry1.addAttribute( 'position', new THREE.BufferAttribute( newpositions, 3 ).onUpload( disposeArray ) );
    geometry1.addAttribute( 'normal', new THREE.BufferAttribute( newnormals, 3 ).onUpload( disposeArray ) );
    geometry1.addAttribute( 'color', new THREE.BufferAttribute( newcolors, 3 ).onUpload( disposeArray ) );
    geometry1.computeBoundingBox();
    showProgress();
    return geometry1;
}
function getFaceProcessingData(geometry, face, instrument, point) {
    var vertices = geometry.vertices;
    var faces = geometry.faces;
    // try find instrument position
    var color = new THREE.Color();
    var rnd1 = 1; // 0.3 + Math.random() * 0.7;// for colorize tris
    color.setRGB( 1, 0, 0 );
    var instrumentPosition;
    if (!point) {
        // got centerpoint of tri
        point = vertices[face.a].clone().add(vertices[face.b]).add(vertices[face.c]).multiplyScalar(1/3);
    }
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(0,1,0), instrument, point )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.20 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(0,-1,0), instrument, point )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.21 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(1,0,0), instrument, point )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.22 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(-1,0,0), instrument, point )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.23 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(0,0,1), instrument, point )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.24 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(0,0,-1), instrument, point )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.25 );
    } else 
    {
        instrumentPosition = null;
        color.setRGB( 1, 0, 0 );
    }
    return {color: color, instrumentPosition: instrumentPosition};
}
function isFaceProcessingUsingAxis(geometry, face, axis, instrument, point) {
    var vertices = geometry.vertices;
    var faces = geometry.faces;
    var normal = face.normal;
    var delta = 0.01;
    // analyze
    var distToPlane = normal.clone().projectOnPlane(axis).distanceTo(normal);
    if (distToPlane < delta){
        // side of instrument
        var delta1 = normal.clone().projectOnPlane(axis).normalize();
        var instrumentStart = point.clone().add(delta1.multiplyScalar(instrument.d/2));
        if (!validateInstrument(geometry,instrumentStart, axis, instrument)) return false; // 
        return { axis: axis, mode:'side', startPoint: instrumentStart };
    }
    var distToAxis1 = normal.clone().projectOnVector(axis).distanceTo(axis);
    if (distToAxis1 < delta) {
        // front of instrument
        if (!validateInstrument(geometry,point, axis, instrument)){
            // try to find better instrument position then default
            var closestPoint = findClosestPointToAxis(geometry,point, axis, instrument);
            var instrumentStart = point.clone().sub(closestPoint).normalize()
                .multiplyScalar(instrument.d/2)
                .add(closestPoint);
            if (validateInstrument(geometry,instrumentStart, axis, instrument))     {
                return { axis: axis, mode:'front', startPoint:instrumentStart };
            } else {
                return false; 
            }
        } 
        return { axis: axis, mode:'front', startPoint:point };
    }
    //if (distToAxis2 < delta) {
        // front of instrument
    //    if (!validateInstrument(geometry,point, axis.clone().multiplyScalar(-1), instrument)) return false; // 
    //    return { axis: axis.clone().multiplyScalar(-1), mode:'front', startPoint:point };
    //}
    return false;
}
function validateInstrument(geometry,instrumentStart, axis, instrument) {
    return validateInstrumentIndexedDekart(geometry,instrumentStart, axis, instrument);
}
function getIndexVal(coordval, coordnum, roundup) { // coordnum: 0=x, 1=y, 2=z; roundup = 0=round, -1=floor, 1=ceil
    var r = 0;
    switch (coordnum * 3 + 1 + roundup) {
        case 0:
            r = Math.floor((coordval - indexBox.min.x)*indexSteps*1.0/(indexBox.max.x - indexBox.min.x));
            break;
        case 1:
            r =  Math.round((coordval - indexBox.min.x)*indexSteps*1.0/(indexBox.max.x - indexBox.min.x));
            break;
        case 2:
            r = Math.ceil ((coordval - indexBox.min.x)*indexSteps*1.0/(indexBox.max.x - indexBox.min.x));
            break;
        case 3:
            r = Math.floor((coordval - indexBox.min.y)*indexSteps*1.0/(indexBox.max.y - indexBox.min.y));
            break;
        case 4:
            r = Math.round((coordval - indexBox.min.y)*indexSteps*1.0/(indexBox.max.y - indexBox.min.y));
            break;
        case 5:
            r = Math.ceil ((coordval - indexBox.min.y)*indexSteps*1.0/(indexBox.max.y - indexBox.min.y));
            break;
        case 6:
            r = Math.floor((coordval - indexBox.min.z)*indexSteps*1.0/(indexBox.max.z - indexBox.min.z));
            break;
        case 7:
            r = Math.round((coordval - indexBox.min.z)*indexSteps*1.0/(indexBox.max.z - indexBox.min.z));
            break;
        case 8:
            r = Math.ceil ((coordval - indexBox.min.z)*indexSteps*1.0/(indexBox.max.z - indexBox.min.z));
            break;
    }
    if (r < 0) r = 0;
    if (r > 100) r = 100;
    return r;
}   
function prepareFastInstrumentValidators() {
    geometry = unbuffered;
    var vertices = geometry.vertices;
    indexBox = geometry.boundingBox;
    indexPlaneXY = [];
    indexPlaneYZ = [];
    indexPlaneXZ = [];
    for(var i = 0; i<(indexSteps+1)*(indexSteps+1); i++)  {
        indexPlaneXY[i] = [];
        indexPlaneXZ[i] = [];
        indexPlaneYZ[i] = [];
    }
    for(var i = 0; i<vertices.length; i++)  {
        var x = getIndexVal(vertices[i].x, 0, 0);
        var y = getIndexVal(vertices[i].y, 1, 0);
        var z = getIndexVal(vertices[i].z, 2, 0);
        indexPlaneXY[x*indexSteps + y].push(i);
        indexPlaneXZ[x*indexSteps + z].push(i);
        indexPlaneYZ[y*indexSteps + z].push(i);
    }
    console.log('instrument validators indexed.');
}
function validateInstrumentIndexedDekart(geometry,instrumentStart0, axis, instrument) // we assume only 3-axis x y z...
{
    geometry = unbuffered;
    var delta = 0.1;
    var vertices = geometry.vertices;
    instrumentStart = instrumentStart0.clone().add(axis.clone().normalize().multiplyScalar(delta));
    var deltar = instrument.d*(1-delta)/2;
    var minr = Math.pow(deltar, 2);
    var ix = instrumentStart.x, iy = instrumentStart.y, iz = instrumentStart.z;
    if (axis.x == 0 && axis.y == 0) { // by-z 
        var imin1 = getIndexVal(ix-deltar, 0, -1);
        var imax1 = getIndexVal(ix+deltar, 0, +1);
        var imin2 = getIndexVal(iy-deltar, 1, -1);
        var imax2 = getIndexVal(iy+deltar, 1, +1);
        if(axis.z>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneXY[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].z>iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                            return false;
                    }
                }
        if(axis.z<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneXY[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].z<iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                            return false;
                    }
                }
    }
    if (axis.x == 0 && axis.z == 0) { // by-z 
        var imin1 = getIndexVal(ix-deltar, 0, -1);
        var imax1 = getIndexVal(ix+deltar, 0, +1);
        var imin2 = getIndexVal(iz-deltar, 2, -1);
        var imax2 = getIndexVal(iz+deltar, 2, +1);
        if(axis.y>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneXZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].y>iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                            return false;
                    }
                }
        if(axis.y<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneXZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].y<iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                            return false;
                    }
                }
    }
    if (axis.y == 0 && axis.z == 0) { // by-z 
        var imin1 = getIndexVal(iy-deltar, 1, -1);
        var imax1 = getIndexVal(iy+deltar, 1, +1);
        var imin2 = getIndexVal(iz-deltar, 2, -1);
        var imax2 = getIndexVal(iz+deltar, 2, +1);
        if(axis.x>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneYZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].x>ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                            return false;
                    }
                }
        if(axis.x<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneYZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].x<ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                            return false;
                    }
                }
    }    
    return true;
}
function findClosestPointToAxis(geometry,instrumentStart0, axis, instrument) {
    // TODO move circle by-limiting-angles
    geometry = unbuffered;
    var delta = 0.1;
    var vertices = geometry.vertices;
    instrumentStart = instrumentStart0.clone().add(axis.clone().normalize().multiplyScalar(delta));
    var deltar = instrument.d*(1-delta)/2;
    var minr = 1e10;
    var closestPoint = new THREE.Vector3();
    var ix = instrumentStart.x, iy = instrumentStart.y, iz = instrumentStart.z;
    if (axis.x == 0 && axis.y == 0) { // by-z 
        var imin1 = getIndexVal(ix-deltar, 0, -1);
        var imax1 = getIndexVal(ix+deltar, 0, +1);
        var imin2 = getIndexVal(iy-deltar, 1, -1);
        var imax2 = getIndexVal(iy+deltar, 1, +1);
        if(axis.z>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneXY[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].z>iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                        {
                            minr = Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2);
                            closestPoint = vertices[i];
                        }
                    }
                }
        if(axis.z<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneXY[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].z<iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                        {
                            minr = Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2);
                            closestPoint = vertices[i];
                        }
                    }
                }
    }
    if (axis.x == 0 && axis.z == 0) { // by-z 
        var imin1 = getIndexVal(ix-deltar, 0, -1);
        var imax1 = getIndexVal(ix+deltar, 0, +1);
        var imin2 = getIndexVal(iz-deltar, 2, -1);
        var imax2 = getIndexVal(iz+deltar, 2, +1);
        if(axis.y>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneXZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].y>iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                        {
                            minr = Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2);
                            closestPoint = vertices[i];
                        }
                    }
                }
        if(axis.y<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneXZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].y<iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                        {
                            minr = Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2);
                            closestPoint = vertices[i];
                        }
                    }
                }
    }
    if (axis.y == 0 && axis.z == 0) { // by-z 
        var imin1 = getIndexVal(iy-deltar, 1, -1);
        var imax1 = getIndexVal(iy+deltar, 1, +1);
        var imin2 = getIndexVal(iz-deltar, 2, -1);
        var imax2 = getIndexVal(iz+deltar, 2, +1);
        if(axis.x>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneYZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].x>ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                        {
                            minr = Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) ;
                            closestPoint = vertices[i];
                        }
                    }
                }
        if(axis.x<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = indexPlaneYZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].x<ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                        {
                            minr = Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) ;
                            closestPoint = vertices[i];
                        }
                    }
                }
    }    
    var closestPointProjected = instrumentStart0.clone().add(
        closestPoint.clone().projectOnPlane(axis).sub(
            instrumentStart0.clone().projectOnPlane(axis)
        )
    );  
    return closestPointProjected;    
}
function validateInstrumentDekart(geometry,instrumentStart, axis, instrument) // we assume only 3-axis x y z...
{ // fast!! really! ;)
    geometry = unbuffered;
    var delta = 0.1;
    var vertices = geometry.vertices;
    instrumentStart = instrumentStart.clone().add(axis.clone().normalize().multiplyScalar(delta));
    var minr = Math.pow(instrument.d*(1-delta)/2, 2);
    var ix = instrumentStart.x, iy = instrumentStart.y, iz = instrumentStart.z;
    if (axis.x == 0 && axis.y == 0 && axis.z>0) // by-z positive
        for(var i = 0; i<vertices.length; i++) 
            if(vertices[i].z>iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                return false;
    if (axis.x == 0 && axis.y == 0 && axis.z<0) // by-z negative
        for(var i = 0; i<vertices.length; i++) 
            if(vertices[i].z<iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                return false;
    if (axis.x == 0 && axis.z == 0 && axis.y>0) // by-y positive
        for(var i = 0; i<vertices.length; i++) 
            if(vertices[i].y>iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                return false;
    if (axis.x == 0 && axis.z == 0 && axis.y<0) // by-y negative
        for(var i = 0; i<vertices.length; i++) 
            if(vertices[i].y<iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                return false;
    if (axis.y == 0 && axis.z == 0 && axis.x>0) // by-x positive
        for(var i = 0; i<vertices.length; i++) 
            if(vertices[i].x>ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                return false;
    if (axis.y == 0 && axis.z == 0 && axis.x<0) // by-x negative
        for(var i = 0; i<vertices.length; i++) 
            if(vertices[i].x<ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                return false;
    return true;
}    
function validateInstrumentProject(geometry,instrumentStart, axis, instrument)
{ // by point
    var delta = 0.001;
    var vertices = geometry.vertices;
    instrumentStart = instrumentStart.clone().add(axis.clone().normalize().multiplyScalar(delta));
    for(var i = 0; i<vertices.length; i++) {
        var p = vertices[i].clone().sub(instrumentStart);
        var d = p.dot(axis);
        var r = p./*clone().*/projectOnPlane(axis).length();
        if (d > 0 && r < instrument.d/2*(1-delta)) return false;
    }
    return true; // there are no points inside cylinder
}
function validateInstrumentRaycaster(geometry,instrumentStart, axis, instrument)
{
    // delta inside instrument
    // axis = axis.clone().normalize();
    var delta = 0.001, deltaDegree = 360/6+0.01;
    // centerline
    instrumentStart = instrumentStart.clone().add(axis.clone().normalize().multiplyScalar(delta));
    if (isRayIntersect(instrumentStart, axis)) return false;
    // TODO: diameter
    var direction1 = axis.clone().cross(rndVec).normalize();
    for(i = 0; i<360; i+=deltaDegree) {
        var direction2 = direction1.clone()
            .applyAxisAngle(axis, i * DEGREE_TO_RAD)
            .multiplyScalar(instrument.d*(1-delta)/2.0)
            .add(instrumentStart);
        if (isRayIntersect(direction2, axis)) return false;    
    }
    // TODO: flot flat front of instrument
    return true;
}
function isRayIntersect(point, axis) {
    raycaster.set( point, axis );
    var intersections = raycaster.intersectObjects( objects );
    if (intersections.length>0) return true;
    return false;
}
function getRayLength(point, axis) {
    raycaster.set( point, axis );
    var intersections = raycaster.intersectObjects( objects );
    if (intersections.length>0){
        return intersections[ 0 ].point.distanceTo(point);
    }
    return -1;
}


function offsetGeometry(geometry, offsetmm) {
    // var unbuffered = new THREE.Geometry().fromBufferGeometry( geometry );
    var muler2 = 1;  // =2 dual-mode
    // unbuffered.mergeVertices();
    crossReference(unbuffered);
    var vertices = unbuffered.vertices;
    var faces = unbuffered.faces;
    var cnt = faces.length;
    var newpositions = new Float32Array( cnt * 3 * 3 * muler2);
    var newnormals = new Float32Array( cnt * 3  * 3 * muler2);
    for (var i = 0; i<cnt; i++) {
        var face = faces[i];
        var normal = face.normal;
        var mover = normal.clone().multiplyScalar(offsetmm);
        var facea = vertices[face.a].clone().add(getnormalizator(unbuffered.faces, face.a, offsetmm) );
        var faceb = vertices[face.b].clone().add(getnormalizator(unbuffered.faces, face.b, offsetmm) );
        var facec = vertices[face.c].clone().add(getnormalizator(unbuffered.faces, face.c, offsetmm) );
        newpositions[ i*9*muler2+0 ] = facea.x; 
        newpositions[ i*9*muler2+1 ] = facea.y; 
        newpositions[ i*9*muler2+2 ] = facea.z;
        newpositions[ i*9*muler2+3 ] = faceb.x; 
        newpositions[ i*9*muler2+4 ] = faceb.y; 
        newpositions[ i*9*muler2+5 ] = faceb.z; 
        newpositions[ i*9*muler2+6 ] = facec.x; 
        newpositions[ i*9*muler2+7 ] = facec.y; 
        newpositions[ i*9*muler2+8 ] = facec.z; 
        newnormals[ i*9*muler2+0 ] = normal.x; 
        newnormals[ i*9*muler2+1 ] = normal.y; 
        newnormals[ i*9*muler2+2 ] = normal.z;
        newnormals[ i*9*muler2+3 ] = normal.x; 
        newnormals[ i*9*muler2+4 ] = normal.y; 
        newnormals[ i*9*muler2+5 ] = normal.z; 
        newnormals[ i*9*muler2+6 ] = normal.x; 
        newnormals[ i*9*muler2+7 ] = normal.y; 
        newnormals[ i*9*muler2+8 ] = normal.z;
        if (muler2==1) continue;
        mover = normal.clone().multiplyScalar(-1*offsetmm);
        facea = vertices[face.a].clone().add(mover);
        faceb = vertices[face.b].clone().add(mover);
        facec = vertices[face.c].clone().add(mover);
        newpositions[ i*9*muler2+9+0 ] = facea.x; 
        newpositions[ i*9*muler2+9+1 ] = facea.y; 
        newpositions[ i*9*muler2+9+2 ] = facea.z;
        newpositions[ i*9*muler2+9+3 ] = faceb.x; 
        newpositions[ i*9*muler2+9+4 ] = faceb.y; 
        newpositions[ i*9*muler2+9+5 ] = faceb.z; 
        newpositions[ i*9*muler2+9+6 ] = facec.x; 
        newpositions[ i*9*muler2+9+7 ] = facec.y; 
        newpositions[ i*9*muler2+9+8 ] = facec.z; 
        newpositions[ i*9*muler2+9+0 ] = normal.x; 
        newpositions[ i*9*muler2+9+1 ] = normal.y; 
        newpositions[ i*9*muler2+9+2 ] = normal.z;
        newpositions[ i*9*muler2+9+3 ] = normal.x; 
        newpositions[ i*9*muler2+9+4 ] = normal.y; 
        newpositions[ i*9*muler2+9+5 ] = normal.z; 
        newpositions[ i*9*muler2+9+6 ] = normal.x; 
        newpositions[ i*9*muler2+9+7 ] = normal.y; 
        newpositions[ i*9*muler2+9+8 ] = normal.z; 
    }
    var geometry1 = new THREE.BufferGeometry();
    geometry1.addAttribute( 'position', new THREE.BufferAttribute( newpositions, 3 ) );
    geometry1.addAttribute( 'normal', new THREE.BufferAttribute( newnormals, 3 ) );
    geometry1.computeBoundingBox();
    return geometry1;
}
function tesselateGeomerty(unbuffered, maxlen){
    // unbuffered = origGeometry.clone(); // ? re-tesselating from scratch?
    unbuffered.dynamic = true;
    var faces = unbuffered.faces;
    var cnt = faces.length;
    tesselationNewVertices = {};
    for (var i = 0; i<cnt; i++) {
        if (i % 1000  == 0) showProgress("tesselated: " + i + " from " + cnt + " triangles.");
        tesselateFace(unbuffered, i, maxlen);
    }
    showProgress("tesselated!");
    //unbuffered.computeFaceNormals();
    //unbuffered.computeVertexNormals();
    tesselationNewVertices = {};
    unbuffered.verticesNeedUpdate = true;
    // showProgress();
}
function getOrCreateVertice(vector3Vertice) {
    var key = vector3Vertice.x.toFixed(6) + "_" + vector3Vertice.y.toFixed(6) + "_" + vector3Vertice.z.toFixed(6); 
    if (! ( key in tesselationNewVertices)) {
        unbuffered.vertices.push(vector3Vertice);
        tesselationNewVertices[key] = unbuffered.vertices.length - 1;
    }
    return tesselationNewVertices[key];
}
function tesselateFace(unbuffered, faceidx, maxlen) {
    var faces = unbuffered.faces;
    var face = faces[faceidx];
    var vertices = unbuffered.vertices;
    var va = vertices[face.a], vb=vertices[face.b], vc=vertices[face.c];
    var ab = va.distanceTo(vb), ac = va.distanceTo(vc), bc = vb.distanceTo(vc);
    var m = Math.max(ab,ac,bc);
    if (m>maxlen) {
        var newv;
        if (m==ab) {
            //vertices.push(va.clone().add(vb).multiplyScalar(0.5));
            //newv = vertices.length - 1;
            newv = getOrCreateVertice(va.clone().add(vb).multiplyScalar(0.5));
            faces.push(new THREE.Face3(newv, face.b, face.c, face.normal, face.color, face.materialIndex)); 
            face.b = newv;
        } else if (m==ac) {
            //vertices.push(va.clone().add(vc).multiplyScalar(0.5));
            //newv = vertices.length - 1;
            newv = getOrCreateVertice(va.clone().add(vc).multiplyScalar(0.5));
            faces.push(new THREE.Face3(newv, face.b, face.c, face.normal, face.color, face.materialIndex));            
            face.c = newv;
        } else /*m==bc*/ {
            //vertices.push(vb.clone().add(vc).multiplyScalar(0.5));
            //newv = vertices.length - 1;
            newv = getOrCreateVertice(vb.clone().add(vc).multiplyScalar(0.5));
            faces.push(new THREE.Face3(face.a, newv, face.c, face.normal, face.color, face.materialIndex));            
            face.c = newv;
        }
        var newf = faces.length - 1;
        tesselateFace(unbuffered, faceidx, maxlen);
        tesselateFace(unbuffered, newf, maxlen);
    }
}

function addShadowedLight(x, y, z, color, intensity) {
    var directionalLight = new THREE.DirectionalLight(color, intensity);
    directionalLight.position.set(x, y, z);
    scene.add(directionalLight);
    directionalLight.castShadow = true;
    var d = 1;
    directionalLight.shadow.camera.left = -d;
    directionalLight.shadow.camera.right = d;
    directionalLight.shadow.camera.top = d;
    directionalLight.shadow.camera.bottom = -d;
    directionalLight.shadow.camera.near = 1;
    directionalLight.shadow.camera.far = 15;
    directionalLight.shadow.mapSize.width = 1024;
    directionalLight.shadow.mapSize.height = 1024;
    directionalLight.shadow.bias = -0.005;
    return directionalLight;
}
function onWindowResize() {
    //camera.aspect = window.innerWidth / window.innerHeight;
    //camera.updateProjectionMatrix();
    //renderer.setSize( window.innerWidth, window.innerHeight );
}
function animate() {
    requestAnimationFrame(animate);
    render();
}
function render() {
    //var timer = Date.now() * 0.0005;
    //camera.position.x = Math.cos( timer ) * 3;
    //camera.position.z = Math.sin( timer ) * 3;
    //camera.lookAt( cameraTarget );
    controls.update();
    renderer.render(scene, camera);
}
function hideShowAura() {
    bbox2.visible = ! bbox2.visible;
}
function hideShowStock() {
    bbox.visible = ! bbox.visible;
}
function showProgress(txt) {
    if (txt) {
        console.log(txt);
        document.getElementById("pleasewait0").innerHTML = txt;
        document.getElementById("pleasewait0").style.display = "block";
        document.getElementById("pleasewait0").offsetHeight;
    } else {
        document.getElementById("pleasewait0").style.display = "none";
        console.log("DONE ----------------------");
    }
}