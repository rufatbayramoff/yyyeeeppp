// var THREE = require('three');

function measure(geometry){
    var va,vb,vc,len;
    var verticesArray;
    var buffered = ! (geometry.faces);
    if (buffered) {
        len = geometry.attributes.position.array.length;
        verticesArray = geometry.attributes.position.array;
    } else {
        len = geometry.faces.length;
    }
    if (!len) return 0.0;

    var area = 0.0,
        volume = 0.0;
    for (var i = 0; i < len; i++) {
        if (buffered) {
            va = new THREE.Vector3(verticesArray[i+0],verticesArray[i+1],verticesArray[i+2]);
            vb = new THREE.Vector3(verticesArray[i+3],verticesArray[i+4],verticesArray[i+5]);
            vc = new THREE.Vector3(verticesArray[i+6],verticesArray[i+7],verticesArray[i+8]);
            i+=9-1;
        } else {
            va = geometry.vertices[geometry.faces[i].a];
            vb = geometry.vertices[geometry.faces[i].b];
            vc = geometry.vertices[geometry.faces[i].c];
        }
        area += areaOfTriangle(va.x, va.y, va.z, vb.x, vb.y, vb.z, vc.x, vc.y, vc.z);
        volume += volumeOfTriangle(va.x, va.y, va.z, vb.x, vb.y, vb.z, vc.x, vc.y, vc.z); // if speed up will be required
    }
    volume = Math.abs(volume);
    result = {
        "area": parseFloat(area).toFixed(2),
        "volume": parseFloat(volume).toFixed(2),
        "weight": parseFloat(getWeight(volume, area)).toFixed(2)
    }
    return result;
}


function areaOfTriangle(p1x,  p1y,  p1z,  p2x,  p2y,  p2z,  p3x,  p3y,  p3z){
    ax = p2x - p1x;
    ay = p2y - p1y;
    az = p2z - p1z;
    bx = p3x - p1x;
    by = p3y - p1y;
    bz = p3z - p1z;
    cx = ay*bz - az*by;
    cy = az*bx - ax*bz;
    cz = ax*by - ay*bx;
    return 0.5 * Math.sqrt(cx*cx + cy*cy + cz*cz);
}
function volumeOfTriangle( p1x, p1y, p1z, p2x, p2y, p2z,  p3x, p3y, p3z) {
    var v321 = p3x * p2y * p1z;
    var v231 = p2x * p3y * p1z;
    var v312 = p3x * p1y * p2z;
    var v132 = p1x * p3y * p2z;
    var v213 = p2x * p1y * p3z;
    var v123 = p1x * p2y * p3z;
    return (1.0/6.0)*(-v321 + v231 + v312 - v132 - v213 + v123);
}

/**
 *   * Weight of model =
 * = Volume * K1 +Area * K2
 * K1 = 0,00025 = 1,25*0,2/1000   where 1,25 = density of PLA, 0,2 = 20% percent infill for model
 * K2 = 0,0008 = 1,25*0,8*(1-0,2)/1000 where 0,8 is thickness, 1,25 = density of PLA, 0,2 = 20% percent infill
 *
 * @param geometry
 * @returns {number}
 */
function getWeight(volume, area)
{
    var density = 1.25;
    var infill = 0.2;
    var k1 = density * infill/1000;
    var k2 = density * 0.8 * (1 - infill) / 1000;
    var w = volume * k1 + area * k2;
    return w;
}

//module.exports = measure;