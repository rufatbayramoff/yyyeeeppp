if (!Detector.webgl) Detector.addGetWebGLMessage();

var container, camera, scene, renderer, raycaster, geometry, controls, 
    material, materialProblemWireframe, materialProblemFaces,  
    meshMain, objects, meshProblemWireframe, meshProblemFaces;
var light1, light2, light3, light4;
var mouse = new THREE.Vector2();

// viewer
function initViewer(w,h) {
    // camera
    camera = new THREE.PerspectiveCamera(35, w / h, 1, 10000);
    camera.position.y = -1000;
    // scene
    scene = new THREE.Scene();
    raycaster = new THREE.Raycaster();    
    // renderer
    renderer = new THREE.WebGLRenderer({antialias: true, preserveDrawingBuffer: true});
    renderer.setClearColor(0xfcfcfc); 
    renderer.setSize(w, h);
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.renderReverseSided = false;
    // container
    container = document.createElement('div');
    document.getElementById('viewer').appendChild(container);
    container.appendChild(renderer.domElement);
    // controls
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.target.set(0, 0, 0);
    controls.zoomSpeed = 0.5;
    controls.update();
    controls.addEventListener( 'change', render );
    // lights
    scene.add(new THREE.HemisphereLight(0x443333, 0x111122));
    var r = 1;
    light1 = addShadowedLight(2*r, -2*r, 0, 0xffffff, 1);
    light2 = addShadowedLight(-1*r, 1*r, 1*r, 0xffffff, 0.8);
    light3 = addShadowedLight(-1*r, 0.5*r, -1*r, 0xffffff, 0.6);
    light4 = addShadowedLight(0.5*r, -0.5*r, -2*r, 0xffffff, 0.6);
    // material
    material = new THREE.MeshPhongMaterial({ // MeshStandardMaterial MeshPhongMaterial
        color: 0x333333, specular: 0x111111, shininess: 200, 
        opacity: 0.8, transparent: true, 
        side: THREE.DoubleSide // , vertexColors: THREE.VertexColors
    });
    materialProblemFaces = new THREE.MeshBasicMaterial({ // MeshStandardMaterial MeshPhongMaterial
        vertexColors: THREE.VertexColors,  // color: 0xff0000,//
        side: THREE.DoubleSide 
    });
    materialProblemWireframe = new THREE.LineBasicMaterial( {
        vertexColors: THREE.VertexColors, // color: 0xff0000, 
        linewidth: 3,
        linecap: 'round', //ignored by WebGLRenderer
        linejoin:  'round' //ignored by WebGLRenderer
    } );
    // var material = new THREE.LineBasicMaterial( { vertexColors: THREE.VertexColors } );
    //window.addEventListener('resize', onWindowResize, false);
    //document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    renderer.domElement.ondblclick=onDoubleClick;
    //  animate();
    render();
}    
function addShadowedLight(x, y, z, color, intensity) {
    var directionalLight = new THREE.DirectionalLight(color, intensity);
    directionalLight.position.set(x, y, z);
    scene.add(directionalLight);
    directionalLight.castShadow = true;
    var d = 1;
    directionalLight.shadow.camera.left = -d;
    directionalLight.shadow.camera.right = d;
    directionalLight.shadow.camera.top = d;
    directionalLight.shadow.camera.bottom = -d;
    directionalLight.shadow.camera.near = 1;
    directionalLight.shadow.camera.far = 15;
    directionalLight.shadow.mapSize.width = 1024;
    directionalLight.shadow.mapSize.height = 1024;
    directionalLight.shadow.bias = -0.005;
    return directionalLight;
}
function render() {
    // controls.update();
    renderer.render(scene, camera);
}
function animate() {
    requestAnimationFrame(animate);
    render();
}

function onDoubleClick(event) {
    vivid = !vivid;
}
function onDocumentMouseMove( event ) {
    event.preventDefault();
    if (!meshMain || !meshMain.geometry || !meshMain.geometry.instrumentDiameter) return;
    if (!vivid) return;
    var rect = renderer.domElement.getBoundingClientRect();
    mouse.x =  (event.clientX - rect.left) * 2.0 / (rect.right - rect.left) - 1;
    mouse.y =  (event.clientY - rect.bottom) * 2.0 / (rect.top - rect.bottom) - 1;
    raycaster.setFromCamera( mouse, camera );
    intersections = raycaster.intersectObjects( objects );
    numObjects = objects.length;
    if ( intersections.length > 0 ) {
        //if ( intersected && intersected != intersections[ 0 ].object )
        //    hideIntersection();
        intersected = intersections[ 0 ].object;
    }
}


// handle uploads
var fileIndex = 0, firstMessageShown = true;
var geoms = {}, geomsUrl = {}, fileComments = {}, fileIds = {};
function handleFileSelectDrag(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    var files = evt.dataTransfer.files; // FileList object.
    // files is a FileList of File objects. List some properties.
    var filesSorted = [].slice.call(files).sort(function(a, b){
        return a.name == b.name ? 0 : a.name < b.name ? -1 : 1;
    });
    for (var i = 0, f; f = filesSorted[i]; i++) {
      addFile(f);
    }
}
function handleFileSelectInput(evt) {
    var files = evt.target.files; // FileList object
    // files is a FileList of File objects. List some properties.
    var filesSorted = [].slice.call(files).sort(function(a, b){
        return a.name == b.name ? 0 : a.name < b.name ? -1 : 1;
    });
    for (var i = 0, f; f = filesSorted[i]; i++) {
      addFile(f);
    }
    $('#files').val('');
}
function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}
function initDragDrop() {
        // Setup the dnd listeners.
        var dropZone = document.getElementById('dropzone');
        dropZone.addEventListener('dragover', handleDragOver, false);
        dropZone.addEventListener('drop', handleFileSelectDrag, false);
        document.getElementById('files').addEventListener('change', handleFileSelectInput, false)
}
function addFile(f) {
    fileIndex++;
    hideFirstMessage();
    var extension = f.name.split('.').pop().toLowerCase();
    if (extension == "csv") {
        /*
        var reader = new FileReader();
        reader.addEventListener("load", function (ev) {
            var csv  = ev.target.result;
            var allTextLines = csv.split(/\r\n|\n/);
            for (var i=0; i<allTextLines.length; i++) {
                var data = allTextLines[i].split(';');
                var aname = data.shift();
                var aval = "";
                for (var j=0; j<data.length; j++) {
                    aval = (aval  + " " + data[j]).trim();
                }
                fileComments[aname] = aval;
                if (fileIds[aname]) {
                    var el = document.getElementById('c_'+fileIds[aname]);
                    el.innerHTML = aval;
                }
            }        
        }, false);
        reader.readAsText(f);
        */
    } else if (extension == "stl") { 
        document.getElementById('list').innerHTML += 
            '<li class="c_li" id="i_'+fileIndex+'">' + f.name + ' - loading...</li>';
        var ii = fileIndex;
        var reader = new FileReader();
        reader.addEventListener("load", function (ev) {
            var buffer = ev.target.result;
            var geometry = loadStl(buffer);
            //loader.load( './wheel_v4.stl', function ( geometry ) {
                var repaired = analyzeProblems(geometry, {createGeometry: false});
                // geoms["i"+ii] = geom;
                geomsUrl["i"+ii] = f;
                document.getElementById('i_'+ii).innerHTML =
                          '<strong class="c_fn" >' + f.name + ': ' +
                          '<a href=# onclick="showgeom('+ii+')">show</a> '+
                          // show original, show problem, show result
                          // save as
                          // print using treatstock
                          // save as all and print treatstock all
                          '</strong><span style="color: red">'+repaired.log+'</span>';
                fileIds[f.name] = ii;
                
                /*console.log('MANUAL ---------------------------------');
                var mesh = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({side: THREE.DoubleSide}));
                var objects = [];
                objects.push( mesh );
                var raycaster = new THREE.Raycaster(); 
                // <eye>
                var eye = new THREE.Vector3(7, 0.1453980000000008, -9.996826666666665);
                var axis = new THREE.Vector3(-0, 0.021819954984507624, -0.9997619164403463);
                // </eye>
                raycaster.set( eye, axis );
                var intersections = raycaster.intersectObjects( objects, true );
                while (intersections.length>0){
                    console.log('intersection found ---------------');
                    console.log('distance=', intersections[0].distance );
                    console.log(geometry.vertices[intersections[0].face.a]);
                    console.log(geometry.vertices[intersections[0].face.b]);
                    console.log(geometry.vertices[intersections[0].face.c]);
                    intersections.shift();
                }*/
            //});
        }, false);
        reader.readAsArrayBuffer(f);
    }
}
function hideFirstMessage() {
    if (!firstMessageShown) return;
    firstMessageShown = false;
    var el = document.getElementById('firstmessage');
    el.style.position = 'static';
    el.style.padding = '5px';
    el.style.border = 'dashed 1px black';
    el.style.margin = '0 auto 10px auto';
    var el0 = document.getElementById('dropzone');
    el0.style.border = 'none';
    el0.style.position = 'static';
    initViewer(800,600);
}

function showgeom(i) {
    if (meshMain) { scene.remove(meshMain); meshMain = null;  };
    if (meshProblemWireframe) { scene.remove(meshProblemWireframe); meshProblemWireframe = null; };
    if (meshProblemFaces) { scene.remove(meshProblemFaces); meshProblemFaces = null; };    
    // var geometry = geoms["i"+i];
    var geometryName = geomsUrl["i"+i];
    var reader = new FileReader();
    reader.addEventListener("load", function (ev) {
        var buffer = ev.target.result;
        var geometry = loadStl(buffer);
        //geometry.computeFaceNormals(); // TODO fix it?

        // show original
        meshMain = new THREE.Mesh(geometry, material );
        //meshMain.castShadow = true;
        //meshMain.receiveShadow = true;
        scene.add(meshMain);

        var repaired = analyzeProblems(geometry, {createGeometry: true});
        meshProblemWireframe = new THREE.LineSegments ( repaired.geometryProblemWireframe, materialProblemWireframe );
        scene.add(meshProblemWireframe);
        meshProblemFaces = new THREE.Mesh(repaired.geometryProblemFaces, materialProblemFaces );
        scene.add(meshProblemFaces);

        // try to repir and get the list of problems and repaired
        //var repaired = repair(geometry, {});

        // objects.push(meshMain);  
            //var measures = measure(geometry, {scene:scene, /* raycastingThinWalls: true, */ supports: needSupports, supportsAsGeometry: true /* , raycastingSupports: true */ });
            //console.log(measures);
            //meshSupports = new THREE.Mesh(measures.supportsGeometry, materialSupport ); 
            //scene.add(meshSupports);
            //document.getElementById('info').innerHTML = measures["timing"]+"ms";
        // Lights
        var sphere = geometry.boundingSphere;
        var r =  sphere.radius;
        light1.position.set(2*r, -2*r, 0);
        light2.position.set(-1*r, 1*r, 1*r);
        light3.position.set(-1*r, 0.5*r, -1*r);
        light4.position.set(0.5*r, -0.5*r, -2*r);
        controls.target.copy(sphere.center);
        camera.position.set(sphere.center.x, sphere.center.y - 4*r, sphere.center.z ); 
        controls.update();
        render();
    }, false);
    reader.readAsArrayBuffer(geometryName);
}

