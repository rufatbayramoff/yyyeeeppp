// var THREE = require('three');

function measure(geometry, options){
    var timestart = Date.now();
    var va,vb,vc,len, lenPositions;
    var verticesArray;
    var buffered = ! (geometry.faces);
    if (buffered) {
        len = geometry.attributes.position.array.length;
        verticesArray = geometry.attributes.position.array;
        lenPositions = len;
    } else {
        len = geometry.faces.length;
        lenPositions = len * 3;
    }
    geometry.computeBoundingBox();
    indexBox = geometry.boundingBox;
    if (!len) return 0.0;

    var supports = options.supports ? true : false;
    var raycastingSupports = options.raycastingSupports ? true : false;
    var raycastingThinWalls = options.raycastingThinWalls ? true : false;
    var raycasting = raycastingSupports || raycastingThinWalls;
    var supportsAsGeometry = options.supportsAsGeometry ? true : false;
    var supportsAngle = options.supportsAngle ? options.supportsAngle : 50;
    var supportsVolume = 0, supportTriangleIndex = 0, triNorm;
    var directionDown = new THREE.Vector3(0,0,-1);
    var directionDownSmall = directionDown.clone().multiplyScalar(0.0001);
    var distanceCritical = 2 * Math.sin(Math.PI * (90 - supportsAngle) / (2 * 180) ) ; // 60 degrees critical angle
    var raycaster, objects, point, zlevel, octree;
    if (supports) {
        if (supportsAsGeometry) {
            positionArray = new Float32Array( lenPositions * 3 * 7); // 6 tri per every tri
            normalArray =  new Float32Array( lenPositions * 3 * 7);       
        }
    }
    if (raycasting) {
        raycaster = new THREE.Raycaster();
        var materialRaycaster = new THREE.MeshStandardMaterial( { color: 0x000088, side: THREE.DoubleSide } );
        var meshRaycaster = new THREE.Mesh(geometry, materialRaycaster ); 
        objects = [];
        objects.push(meshRaycaster);    
        octree = new THREE.Octree( {
            // uncomment below to see the octree (may kill the fps)
            // scene:  options.scene,
            // when undeferred = true, objects are inserted immediately
            // instead of being deferred until next octree.update() call
            // this may decrease performance as it forces a matrix update
            undeferred: false,
            // set the max depth of tree
            depthMax: Infinity,
            // max number of objects before nodes split or merge
            objectsThreshold: 8,
            // percent between 0 and 1 that nodes will overlap each other
            // helps insert objects that lie over more than one node
            overlapPct: 0.15
        } );
        octree.add( meshRaycaster, { useFaces: true } ); // ?
        octree.update();
    }
    
    var area = 0.0,
        volume = 0.0,
        aot = 0,
        wallsVolume = 0.0;
    var wallsMax = 1.2, wallsMin = 0.4; // max and min thickness of walls
    for (var i = 0; i < len; i++) {
        if (buffered) {
            va = new THREE.Vector3(verticesArray[i+0],verticesArray[i+1],verticesArray[i+2]);
            vb = new THREE.Vector3(verticesArray[i+3],verticesArray[i+4],verticesArray[i+5]);
            vc = new THREE.Vector3(verticesArray[i+6],verticesArray[i+7],verticesArray[i+8]);
            i+=9-1;
        } else {
            va = geometry.vertices[geometry.faces[i].a];
            vb = geometry.vertices[geometry.faces[i].b];
            vc = geometry.vertices[geometry.faces[i].c];
        }
        aot = areaOfTriangle(va.x, va.y, va.z, vb.x, vb.y, vb.z, vc.x, vc.y, vc.z);
        area += aot;
        volume += volumeOfTriangle(va.x, va.y, va.z, vb.x, vb.y, vb.z, vc.x, vc.y, vc.z); // if speed up will be required
        if (raycastingThinWalls) {
            triNormInverse =  getTriangleNormal(va.x, va.y, va.z, vb.x, vb.y, vb.z, vc.x, vc.y, vc.z).multiplyScalar(-1);
            microdelta = triNormInverse.clone().multiplyScalar(0.01);
            point = va.clone().add(vb).add(vc).multiplyScalar(1/3).add(microdelta);
            raycaster.set( point, triNormInverse );
            var octreeObjects = octree.search( point, wallsMax * 2, true, triNormInverse );
            intersections = raycaster.intersectOctreeObjects( octreeObjects );  
            if (intersections.length > 0 ) {
                l = intersections[ 0 ].point.distanceTo(point);
                if (l < wallsMin) {
                    wallsVolume += aot * wallsMax; // there are no wall here
                } else if (l < wallsMax * 2) {
                    wallsVolume += aot * (wallsMax - l/2.0);
                }
            }
        }
        // supports
        if (supports) {
            triNorm =  getTriangleNormal(va.x, va.y, va.z, vb.x, vb.y, vb.z, vc.x, vc.y, vc.z);
            if (directionDown.distanceTo(triNorm)<=distanceCritical) { // this ri is supported
                // TODO calc real volume - raycasting
                point = va.clone().add(vb).add(vc).multiplyScalar(1/3).add(directionDownSmall);
                if (raycastingSupports) {
                    raycaster.set( point, directionDown );
                    var octreeObjects = octree.search( raycaster.ray.origin, raycaster.ray.far, true, raycaster.ray.direction );
                    intersections = raycaster.intersectOctreeObjects( octreeObjects );  
                    //intersections = raycaster.intersectObjects( objects );
                    if ( intersections.length > 0 ) {
                        zlevel = intersections[ 0 ].point.z;
                    } else {
                        zlevel = 0;  // TODO!!!!!!!!!!! min z of box!
                    }
                } else {
                    zlevel = indexBox.min.z; // fast
                }
                var aot2 = areaOfTriangle(va.x, va.y, 0, vb.x, vb.y, 0, vc.x, vc.y, 0);
                supportsVolume += Math.max(0, point.z - zlevel) * aot2; // TODO - non-rectangular prism? three rays from dots?
                if (supportsAsGeometry) {
                    supportTriangleIndex = addTriangle(positionArray, normalArray, supportTriangleIndex,
                        va.x, va.y, va.z,     vb.x, vb.y, vb.z,   va.x, va.y, zlevel ); 
                    supportTriangleIndex = addTriangle(positionArray, normalArray, supportTriangleIndex,
                        va.x, va.y, zlevel,     vb.x, vb.y, vb.z,   vb.x, vb.y, zlevel ); 
                    supportTriangleIndex = addTriangle(positionArray, normalArray, supportTriangleIndex,
                        va.x, va.y, va.z,     vc.x, vc.y, vc.z,   va.x, va.y, zlevel ); 
                    supportTriangleIndex = addTriangle(positionArray, normalArray, supportTriangleIndex,
                        va.x, va.y, zlevel,     vc.x, vc.y, vc.z,   vc.x, vc.y, zlevel ); 
                    supportTriangleIndex = addTriangle(positionArray, normalArray, supportTriangleIndex,
                        vb.x, vb.y, vb.z,     vc.x, vc.y, vc.z,   vb.x, vb.y, zlevel ); 
                    supportTriangleIndex = addTriangle(positionArray, normalArray, supportTriangleIndex,
                        vb.x, vb.y, zlevel,     vc.x, vc.y, vc.z,   vc.x, vc.y, zlevel ); 
                    supportTriangleIndex = addTriangle(positionArray, normalArray, supportTriangleIndex,
                        va.x, va.y, zlevel,     vb.x, vb.y, zlevel,   vc.x, vc.y, zlevel ); 
                }
            }
        }
    }
    volume = Math.abs(volume);
    //console.log(volume);
    //console.log(wallsVolume);
    result = {
        "area": parseFloat(area).toFixed(2),
        "volume": parseFloat(volume).toFixed(2),
        "weight": parseFloat(getWeight(volume, area, 0, wallsVolume)).toFixed(2),
        "supportsVolume": 0,
        "wallsVolume": wallsVolume
    }
    if (supports) {
        result["supportsVolume"] = supportsVolume.toFixed(2);
        if (supportsAsGeometry) {
            var geometry = new THREE.BufferGeometry();
            function disposeArray() { this.array = null; }
            geometry.addAttribute( 'position', new THREE.BufferAttribute(  positionArray , 3 ).onUpload( disposeArray ) );
            geometry.addAttribute( 'normal', new THREE.BufferAttribute( normalArray , 3 ).onUpload( disposeArray ) );
            geometry.computeBoundingSphere();
            result["supportsGeometry"] = geometry;
        }
        result["weightWithSupports"] = parseFloat(getWeight(volume, area, supportsVolume.toFixed(2), wallsVolume)).toFixed(2);
    }
    result["timing"] = Date.now() - timestart;
    return result;
}


function areaOfTriangle(p1x,  p1y,  p1z,  p2x,  p2y,  p2z,  p3x,  p3y,  p3z){
    ax = p2x - p1x;
    ay = p2y - p1y;
    az = p2z - p1z;
    bx = p3x - p1x;
    by = p3y - p1y;
    bz = p3z - p1z;
    cx = ay*bz - az*by;
    cy = az*bx - ax*bz;
    cz = ax*by - ay*bx;
    return 0.5 * Math.sqrt(cx*cx + cy*cy + cz*cz);
}
function volumeOfTriangle( p1x, p1y, p1z, p2x, p2y, p2z,  p3x, p3y, p3z) {
    var v321 = p3x * p2y * p1z;
    var v231 = p2x * p3y * p1z;
    var v312 = p3x * p1y * p2z;
    var v132 = p1x * p3y * p2z;
    var v213 = p2x * p1y * p3z;
    var v123 = p1x * p2y * p3z;
    return (1.0/6.0)*(-v321 + v231 + v312 - v132 - v213 + v123);
}

function getTriangleNormal(ax, ay, az,     bx, by, bz,   cx, cy, cz) {
    var pA = new THREE.Vector3();
    var pB = new THREE.Vector3();
    var pC = new THREE.Vector3();
    var cb = new THREE.Vector3();
    var ab = new THREE.Vector3();
    pA.set( ax, ay, az );
    pB.set( bx, by, bz );
    pC.set( cx, cy, cz );
    cb.subVectors( pC, pB );
    ab.subVectors( pA, pB );
    cb.cross( ab );
    cb.normalize();
    return cb;
}
function addTriangle(positionArray, normalArray, index,   ax, ay, az,     bx, by, bz,   cx, cy, cz ) {
    //positionArray.setXYZ( index+0, ax, ay, az );
    //positionArray.setXYZ( index+1, bx, by, bz );
    //positionArray.setXYZ( index+2, cx, cy, cz );
    var positionInArrays = index * 3;
    positionArray[ positionInArrays ]     = ax;
    positionArray[ positionInArrays + 1 ] = ay;
    positionArray[ positionInArrays + 2 ] = az;
    positionArray[ positionInArrays + 3 ] = bx;
    positionArray[ positionInArrays + 4 ] = by;
    positionArray[ positionInArrays + 5 ] = bz;
    positionArray[ positionInArrays + 6 ] = cx;
    positionArray[ positionInArrays + 7 ] = cy;
    positionArray[ positionInArrays + 8 ] = cz;
    var cb = getTriangleNormal(ax, ay, az,     bx, by, bz,   cx, cy, cz);
    //normalArray.setXYZ( index+0, cb.x, cb.y, cb.z );
    //normalArray.setXYZ( index+1, cb.x, cb.y, cb.z );
    //normalArray.setXYZ( index+2, cb.x, cb.y, cb.z );
    normalArray[ positionInArrays ]     = cb.x;
    normalArray[ positionInArrays + 1 ] = cb.y;
    normalArray[ positionInArrays + 2 ] = cb.z;
    normalArray[ positionInArrays + 3 ] = cb.x;
    normalArray[ positionInArrays + 4 ] = cb.y;
    normalArray[ positionInArrays + 5 ] = cb.z;
    normalArray[ positionInArrays + 6 ] = cb.x;
    normalArray[ positionInArrays + 7 ] = cb.y;
    normalArray[ positionInArrays + 8 ] = cb.z;
    return index+3;
}


/**
 *   * Weight of model =
 * = Volume * K1 +Area * K2
 * K1 = 0,00025 = 1,25*0,2/1000   where 1,25 = density of PLA, 0,2 = 20% percent infill for model
 * K2 = 0,0008 = 1,25*0,8*(1-0,2)/1000 where 0,8 is thickness, 1,25 = density of PLA, 0,2 = 20% percent infill
 *
 * @param geometry
 * @returns {number}
 */
function getWeight(volume, area, supports, wallsVolume)
{
    var density = 1.25;
    var infill = 0.2;
    var infillSupport = 0.20;
    var k1 = infill/1000;
    var k1s = infillSupport/1000;
    var k2 = 1.2 * (1 - infill) / 1000;
    var k3 = (1 - infill) / 1000;
    var w = density * ( volume * k1 - wallsVolume * k3 + area * k2 + supports * k1s) ;
    return w;
}

//module.exports = measure;