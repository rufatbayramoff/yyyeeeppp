var repair = require('./www/repair/js/repair');
var nconf = require('nconf');
var fs = require('fs');
var path = require('path');
var dir = require('node-dir');
var md5 = require('md5');
var THREE = require('three');
var STLLoader = require('./app/three-stl-loader')(THREE);
var fs = require('fs-extra');
require('./www/repair/js/Octree');


nconf.env().argv(); 
nconf.defaults({
    'in': '/vagrant/repo/tests/_data',
    'out': '/vagrant/repo/tools/node/tmp',
   });


   
var indir = nconf.get('in');
var outdir = nconf.get('out');
var md5dataFile = outdir + '/md5.txt';
// read md5
var contents = fs.readFileSync(md5dataFile, 'utf8');
var md5doneFiles = contents.split("\n");
// console.log(md5doneFiles);

// start scanning
console.log('start scanning ['+indir+'] ...'); 
var loader = new STLLoader();
dir.readFiles(indir, {
    match: /.stl$/,
    encoding:'binary'
    }, function(err, content, filename, next) {
        if (err) throw err;
        var md5ofFile = md5(content,{encoding:'binary'});
        //console.log("[",md5ofFile,"] [",md5doneFiles[21],"]");
        //console.log(md5ofFile == md5doneFiles[21]);
        if(md5doneFiles.indexOf(md5ofFile)<0) { // not found
            console.log(filename, "\t", md5ofFile,"\tPROCESSING");
            fs.appendFile(md5dataFile, md5ofFile+"\n");
        } else { // found
            console.log(filename, "\t", md5ofFile,"\tskip");
        }
        
        if (content.length > 5000000) { // > 1m
            // too big file
            console.log("    File too big!!!");
            fs.mkdir(outdir + '/big_not_tested', function (err) {
               fs.copy(filename, outdir + '/big_not_tested/' + md5ofFile + '.stl');
            });
        } else {
            THREE.Cache.enabled = true;
            var ab = content; // data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
            THREE.Cache.add(md5ofFile, ab); 
            loader.load(md5ofFile, function (geometry) { // file loaded
                        THREE.Cache.remove(md5ofFile); 
                        var repaired = repair.analyzeProblems(geometry, {createGeometry: false});
                        console.log('  problems: ' + repaired["log"]);
                        if (repaired["errors"]>0) {
                            console.log('   copying ', filename, ' to ', outdir + '/' + md5ofFile + '.stl');
                            fs.mkdir(outdir + '/' + repaired["qualify"], function (err) {
                                fs.copy(filename, outdir + '/' + repaired["qualify"] + '/' + md5ofFile + '.stl');
                            });
                        }
            });
        };
        next();
    });



// save new md5
