// noinspection SpellCheckingInspection

let nconf           = require('nconf'),
    express         = require('express'),
    Sentry          = require('@sentry/node'),
    bodyParser      = require('body-parser'),
    cluster         = require('express-cluster'),
    mysql           = require('mysql'),
    nodeMailerClass = require('nodemailer'),
    rfs             = require('rotating-file-stream'),
    morgan          = require('morgan'),
    path            = require('path');

let controller      = {
    'basrelief'     : require('./controller_basrelief'),
    'watermark'     : require('./controller_watermark'),
    'publicmark'    : require('./controller_publicmark'),
    'measure'       : require('./controller_measure'),
    'cloudcam'      : require('./controller_cloudcam'),
    'convert'       : require('./controller_convert')
}

// noinspection JSUnresolvedFunction
let nconfEnv = nconf.env();

nconfEnv
    .argv()
    .file('./app/config.json')
    .defaults({
        'threads'       : 1,
        'server_port'   : 5858,
        'server_host'   : '10.102.0.81',
        'file_prefix'   : 'data/',
        'url_prefix'    : 'http://10.102.0.81:5858/data/',
        'models_root'   : 'data/',
        'tmp_root'      : '/tmp/',
        'servicePrefix' : 'www/cloudcam/services/',
        'mysql_host'    : '',
        'daily_limit'   : 10,
        'mail_host'     : '',
        'sentry_dsn'    : null, // "https://9a51257e13724551a1eb3c08cfd8f856@sentry.treatstock.com/10"
    });

let filePrefix      = nconf.get('file_prefix'),
    urlPrefix       = nconf.get('url_prefix'),
    servicePrefix   = nconf.get('servicePrefix'),
    port            = nconf.get('server_port'),
    host            = nconf.get('server_host'),
    threads         = nconf.get('threads');

let pool;
if (nconf.get('mysql_host') !== "") {
    // connect to DB
    pool = mysql.createPool({
        connectionLimit : threads,
        host            : nconf.get('mysql_host'),
        user            : nconf.get('mysql_user'),
        password        : nconf.get('mysql_password'),
        database        : nconf.get('mysql_database')
    });
}

let nodeMailer;
if (nconf.get('mail_host') !== "") {
    // connect to DB
    nodeMailer  = nodeMailerClass.createTransport({
        host    : nconf.get('mail_host'),
        port    : nconf.get('mail_port'),
        tls     : { rejectUnauthorized: false }
    });
}

cluster(function (worker) {
    console.log('Fork threads:' + threads+ " , worker#" + worker.id);
    let app = express();

    let urlencodedParser = bodyParser.urlencoded({ extended: true });

    let sentryDsn = nconf.get('sentry_dsn');
    if (sentryDsn) {
        Sentry.init({ dsn: sentryDsn });
        app.use(Sentry.Handlers.requestHandler());
    }

    // create a rotating write stream
    let accessLogStream = rfs.createStream('access.log', {
        interval    : '1d', // rotate daily
        path        : path.join(__dirname, 'log')
    })
    app.use(morgan('combined', {stream: accessLogStream}))

    app.post('/jsapi/public/basrelief/', function (req, res) {
        controller['basrelief'].perform(req, res, filePrefix, urlPrefix);
    });

    app.post('/jsapi/public/watermark/*', urlencodedParser, function (req, res) {
        controller['publicmark'].process(req, res, nconf.get('tmp_root'), pool, nconf, nodeMailer);
    });

    app.get('/jsapi/public/watermark/download*', urlencodedParser, function (req, res) {
        controller['publicmark'].download(req, res, nconf.get('tmp_root'), pool, nconf, nodeMailer);
    });


    // curl -d passphrase=qwerty -d filepathin=/var/www/treatstock/frontend/storage/files/bad.stl -d filepathout=/var/www/treatstock/frontend/storage/files/marked.stl -d text=ehlo http://10.102.0.81:5858/jsapi/private/watermark/encode
    // curl -d passphrase=qwerty -d filepath=data/marked.stl http://10.102.0.81:5858/jsapi/private/watermark/decode
    // curl -d passphrase=qwerty -d filepath=data/marked.stl -d findall=1 http://10.102.0.81:5858/jsapi/private/watermark/decode
    app.post('/jsapi/private/watermark/*', urlencodedParser, function (req, res) {
        controller['watermark'].process(req, res, nconf.get('models_root'));
    });

    // curl -d filepath=/var/www/treatstock/frontend/storage/files/4b/a1/6.stl  http://10.102.0.81:5858/jsapi/private/measure
    // siege -c 5 "http://10.102.0.81:5858/jsapi/private/measure POST filepath=/var/www/treatstock/frontend/storage/files/4b/a1/6.stl"
    app.post('/jsapi/private/measure', urlencodedParser, function (req, res) {
        controller['measure'].process(req, res, nconf.get('models_root'));
    });

	// debug:
	// cd /vagrant/repo/tools/node/;  node --inspect-brk=192.168.66.10:9229 --expose_gc --max-old-space-size=3072 index.js --server_port 5859 --threads=1
	// open chromt: chrome-devtools://devtools/bundled/inspector.html?experiments=true&v8only=true&ws=127.0.0.1:9229/0f2c936f-b1cd-4ac9-aab3-f63b0f33d55e
	// nodebug:
	// cd /vagrant/repo/tools/node/;  node --expose_gc --max-old-space-size=3072 index.js --server_port 5859
    // curl -d filepathin=/var/www/treatstock/frontend/storage/files/cube_gears.3mf -d filepathout=/var/www/treatstock/frontend/storage/files/tmpout/3mf_1 http://10.102.0.81:5859/jsapi/private/convert
    app.post('/jsapi/private/convert', urlencodedParser, function (req, res) {
        controller['convert'].process(req, res, nconf.get('models_root'));
    });


    // sudo su; service supervisor stop; cd /vagrant/repo/tools/node/; npm start
    // http://192.168.66.10:5858/cloudcam/
    app.post('/jsapi/private/cloudcam/upload', function (req, res) {
        controller['cloudcam'].upload(req, res, filePrefix, urlPrefix);
    });

    app.post('/jsapi/private/cloudcam/processing/*', urlencodedParser, function (req, res) {
        controller['cloudcam'].process(req, res, filePrefix, urlPrefix, servicePrefix);
    });

    if (sentryDsn) {
        app.use(Sentry.Handlers.errorHandler());
    }

    // serve static files
    app.use(express.static('www'));
    app.use("/data", express.static('data'));

    let server = app.listen(port, host, function () {
        // noinspection HttpUrlsUsage
        console.log('Web app listening on: http://' + host + ':' + port + "\nto debug, run me like: node --inspect-brk=192.168.66.10:9229 index.js\n\n")
    });
    app.timeout = 60000000;
    server.setTimeout(10 * 60 * 1000); // 10 * 60 seconds * 1000 msecs = 10 minutes
}, {count: threads}) // { count: 5, os.cpus().length }
