var fs = require('fs');
const path = require('path');
var uuid = require('uuid/v4');  
var md5 = require('md5');

var THREE = require('three');
var STLLoader = require('./three-stl-loader')(THREE);
//var STLExporter = require('three-stlexporter');
var watermarkFactory = require('./watermark');
var BinaryStlWriter = require('./stlbinexporter');
var arrayBufferToBuffer = require('arraybuffer-to-buffer');

var multiparty = require('multiparty');

var window = require('window-or-global');
var mygc = require('./mygc');
var salt = "VeryLongRandomString";
var emailAsPassword = "emailAsPassword";


var encode = function(req, res, tmp_root, pool, nconf, nodemailer) {
    res.setHeader('Content-Type', 'application/json');
    // clean up parameters
    console.log('Watermark public encode request!');
    var form = new multiparty.Form(); 
    form.autoFiles = true;
    form.maxFilesSize = 500000000;
    form.parse(req, function(err, fields, files) {
        var pass = "" + fields['pass'][0];
        var mark = "" + fields['info'][0];
        var email = "" + fields['email'][0];
        var stronger = parseFloat("0" + (fields['strong'] ? fields['strong'][0] : ""));
        var subscribe = ("" + (fields['subscribe'] && fields['subscribe'][0] == "true" ? 1 : 0));
        var sameName = ("" + (fields['sameName'] && fields['sameName'][0] == "true" ? 1 : 0));
        var filein = files['file1'][0].path;
        var originalName = files['file1'][0].originalFilename.substring(0,250) ;
        var asize = files['file1'][0].size;
        var ip = req.connection.remoteAddress;
        var outname = uuid() + ".stl";
        var fileout = tmp_root + outname;
        // email to standat lowcase
        email = email.trim().toLowerCase();
        // vaidate email 
        var match = email.match( /.+@.+\..+/i );
        if (!match || email == "") {
            res.status(200);
            console.log('email validation fail');
            res.send('{"result":"fail","message":"Email invalid"}');
            res.end();
            mygc();
            return;
        }
        // TODO: check email to limit by db
        var userid = 0, speclimit = 0, approved = 0;
        pool.query({sql:'select id,speclimit,approved from users where email=?',values:[email]}, function (error, results, fields) {
            if (error) throw error;
            if (results.length == 0) {
                // new user
                pool.query({sql:'insert into users (email,marketing) values(?,?)',values:[email,subscribe]}, function (error, results, fields) {
                    userid = results.insertId;
                    speclimit = 0;
                    logandgo(userid, originalName, asize,ip,0, sameName);
                });
            } else {
                // existing user
                userid = results[0].id;
                speclimit = results[0].speclimit;
                approved = results[0].approved;
                if (speclimit > 0) {
                    console.log('speclimit run: ' + email );
                    pool.query({sql:'update users set speclimit = speclimit - 1 where email = ?',values:[email]}, function (error, results, fields) {
                        speclimit = speclimit - 1;
                        logandgo(userid, originalName, asize,ip,1, sameName);
                    });
                } else {
                    pool.query({sql:'select count(*) as cnt from sessions where userid = ? and ts >= now() - interval 1 day',values:[userid]}, function (error, results, fields) {
                        var alreadyDone = results[0].cnt;
                        if (alreadyDone > nconf.get('daily_limit')) {
                            console.log('demo run declined: ' + email );
                            res.status(200);
                            console.log(err);
                            res.send('{"result":"fail","message":"Multiple requests for free service.  Request subscription"}');
                            res.end();
                            mygc();
                            return;
                        } else {
                            console.log('demo run approved: ' + email + ", demo counted: " + alreadyDone + " < " + nconf.get('daily_limit'));
                            logandgo(userid, originalName, asize,ip,approved, sameName);
                        }
                    });
                }
            }
        });
        // processing
        function logandgo(userid, originalName, asize,ip,approved, sameName) {
            var parts = path.parse(originalName);
            var filenameFixed = parts.name + "_marked.stl";
            if (sameName)
                filenameFixed = parts.name + ".stl";
            pool.query({sql:'insert into sessions (userid,filename,filesize,ip) values(?,?,?,?)',values:[userid,filenameFixed, asize,ip]}, function (error, results, fields) {
                var sessionid = results.insertId;
                nextStep1(approved, sessionid);
            });
        }
        function nextStep1(approved, sessionid) {
            try { // we have: mark, pass, filein, fileout
                var loader = new STLLoader();
                fs.readFile(filein, function (err, data) {
                    if (err) {
                        res.status(200);
                        console.log(err);
                        res.send('{"result":"fail","message":"Error while reading the file"}');
                        res.end();
                        mygc();
                        return;
                    }
                    THREE.Cache.enabled = true;
                    var ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
                    THREE.Cache.add(filein, ab); 
                    loader.load(filein, function (geometry) { // file loaded
                        THREE.Cache.remove(filein); 
                        
                        // make sure there is no marks with same password before
                        var marksFound = watermarkFactory.decode(geometry, pass, false);
                        watermarkFactory.cleanup();
                        if (marksFound !== false && JSON.stringify(marksFound)!="{}") {
                            res.status(200);
                            console.log('mark alredy present');
                            res.send('{"result":"fail","message":"Watermark with this password already present on this model, sorry"}');
                            res.end();
                            mygc();
                            return;
                        }
                        
                        // MAIN
                        var geometry1 = watermarkFactory.encode(geometry, mark, pass, stronger);
                        watermarkFactory.cleanup();
                        if (geometry1 === false) {
                            res.status(200);
                            console.log('encode fail');
                            res.send('{"result":"fail","message":"Watermark cannot be applied to your model. Too many vertices in mesh, or too much information to insert?"}');
                            res.end();
                            mygc();
                            return;
                        }
                        // save result
                        var material = new THREE.MeshNormalMaterial();
                        var mesh = new THREE.Mesh(geometry1, material);
                        var theArrayBuffer = BinaryStlWriter.meshUnbufferedToBinArrayBuffer(mesh); // geometryToDataViewBin
                        fs.writeFile(fileout,  arrayBufferToBuffer(theArrayBuffer),  function(err) {
                                if(err) {
                                    res.status(200);
                                    console.log('ERR: '+err);
                                    res.send('{"result":"fail"}'); // can;t write result? something strange...
                                    res.end();
                                    mygc();
                                } else {
                                    // ok! return URL of file
                                    // TODO preserve filename + _marked.stl
                                    var emailHash = new Buffer(email).toString('base64');
                                    var keyword = md5(emailHash + salt + sessionid);
                                    res.status(200);
                                    console.log(' File ready: ' + fileout);
                                    if (approved) {
                                        // TODO return a download link (if email appproved)                
                                        res.send('{"result":"ok","file":"'+outname+'","email":"'+emailHash+'","key":"'+keyword+'","s":"'+sessionid+'"}');
                                        res.end();
                                        mygc();
                                    } else {
                                        // TODO: send to email (on of ther eare no download before) OR
                                        var alink = nconf.get('publicmark_url') + "/jsapi/public/watermark/download?file="+outname+"&email="+emailHash+"&key="+keyword+"&s="+sessionid;
                                        var mailOptions = {
                                          from: nconf.get('mail_from'),
                                          to: email,
                                          subject: 'Watermark on STL file',
                                          text: "Link to download your watermarked file: \n\n" + alink + "\n\n Link will be invalid in two days."
                                        };
                                        nodemailer.sendMail(mailOptions, function(error, info){
                                          if (error) {
                                            res.status(200);
                                            console.log('ERR while sending mail'+error);
                                            res.send('{"result":"fail","message":"Error while sending email"}');
                                            res.end();
                                            mygc();
                                          } else {
                                            console.log('Email sent: ' + info.response);
                                            console.log('Direct download link: ' + alink);
                                            res.send('{"result":"ok","message":"Message with the link for watermarked file has been sent to your email"}');
                                            res.end();
                                            mygc();
                                          }
                                        });
                                    }
                                }
                            });                
                    })
                })
            } catch (e) {
                res.status(200);
                console.log(e);
                res.send('{"result":"fail"}');
                res.end();
                mygc();
            }
        }
    });
}

var decode = function(req, res, tmp_root, pool, nconf, nodemailer) {
    res.setHeader('Content-Type', 'application/json');
    // clean up parameters
    console.log('Watermark public decode request!');
    var form = new multiparty.Form(); 
    form.autoFiles = true;
    form.maxFilesSize = 500000000;
    form.parse(req, function(err, fields, files) {
        var pass = "" + fields['pass'][0];
        var modeFindAll = 1; // "" + fields['findall'][0];
        modeFindAll = (modeFindAll=="" || modeFindAll=="undefined" || modeFindAll=="0" || modeFindAll=="false" ? false : true);
        console.log(' modeFindAll= ' + modeFindAll);
        var filein = files['file1'][0].path;  
        // processing
        try { // we have: pass, filein
            var loader = new STLLoader();
            fs.readFile(filein, function (err, data) {
                if (err) {
                    res.status(200);
                    console.log(err);
                    res.send('{"result":"fail","message":"Error while reading the file"}');
                    res.end();
                    mygc();
                    return;
                }
                THREE.Cache.enabled = true;
                var ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
                THREE.Cache.add(filein, ab); 
                loader.load(filein, function (geometry) { // file loaded
                    THREE.Cache.remove(filein); 
                    
                    // MAIN
                    var marksFound = watermarkFactory.decode(geometry, pass, modeFindAll);
                    watermarkFactory.cleanup();
                    if (marksFound === false) {
                        res.status(200);
                        console.log('analysis fail');
                        res.send('{"result":"fail","message":"Watermarks not found in this model."}');
                        res.end();
                        mygc();
                        return;
                    } else {
                        res.status(200);
                        console.log(' marks found: ' + JSON.stringify(marksFound));
                        var result = {"result":"ok","marks":marksFound};
                        if (JSON.stringify(marksFound)=="{}") result = {"result":"ok","message":"Watermarks not found in this model."};
                        res.send(JSON.stringify(result));
                        res.end();
                        mygc();
                    }
                })
            })
        } catch (e) {
            res.status(200);
            console.log(e);
            res.send('{"result":"fail"}');
            res.end();
            mygc();
        }
    });
}



var encodeMultiple = function(req, res, tmp_root, pool, nconf, nodemailer) {
    res.setHeader('Content-Type', 'application/json');
    // clean up parameters
    console.log('Watermark multiple public enocde request!');
    var form = new multiparty.Form(); 
    form.autoFiles = true;
    form.maxFilesSize = 500000000;
    form.parse(req, function(err, fields, files) {
		console.log("fields=",fields);
		
        var email = "" + fields['email'][0];
        var stronger = parseFloat("0" + fields['strong'][0]);
        var filein = files['file1'][0].path;
        var originalName = files['file1'][0].originalFilename.substring(0,250) ;
        var asize = files['file1'][0].size;
        var marksAndPasses = []; // TBD
		for (var i = 0; i < fields['info[]'].length; i++) {
			marksAndPasses.push({"mark":fields['info[]'][i],"pass":fields['pass[]'][i]})
		}
        var ip = req.connection.remoteAddress;
        var outname = uuid() + ".stl";
        var fileout = tmp_root + outname;
        // vaidate email  is  password
        if (email != emailAsPassword) { 
            res.status(200);
            console.log('email validation fail');
            res.send('{"result":"fail","message":"Email invalid"}');
            res.end();
            mygc();
            return;
        }
        
		try { // we have: mark, pass, filein, fileout
			var loader = new STLLoader();
			fs.readFile(filein, function (err, data) {
				if (err) {
					res.status(200);
					console.log(err);
					res.send('{"result":"fail","message":"Error while reading the file"}');
					res.end();
					mygc();
					return;
				}
				THREE.Cache.enabled = true;
				var ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
				THREE.Cache.add(filein, ab); 
				loader.load(filein, function (geometry) { // file loaded
					THREE.Cache.remove(filein); 
					
					// MAIN
					var geometry1 = watermarkFactory.encodeMultiple(geometry, marksAndPasses, stronger);
					watermarkFactory.cleanup();
					if (geometry1 === false) {
						res.status(200);
						console.log('encode fail');
						res.send('{"result":"fail","message":"Watermark cannot be applied to your model. Too many vertices in mesh, or too much information to insert?"}');
						res.end();
						mygc();
						return;
					}
					// save result
					var material = new THREE.MeshNormalMaterial();
					var mesh = new THREE.Mesh(geometry1, material);
					var theArrayBuffer = BinaryStlWriter.meshUnbufferedToBinArrayBuffer(mesh); // geometryToDataViewBin
					fs.writeFile(fileout,  arrayBufferToBuffer(theArrayBuffer),  function(err) {
							if(err) {
								res.status(200);
								console.log('ERR: '+err);
								res.send('{"result":"fail"}'); // can;t write result? something strange...
								res.end();
								mygc();
							} else {
								// ok! return URL of file
								let sessionid = "raw";
								// TODO preserve filename + _marked.stl
								var emailHash = new Buffer(email).toString('base64');
								var keyword = md5(emailHash + salt + sessionid);
								res.status(200);
								console.log(' File ready: ' + fileout);
								// TODO return a download link (if email appproved)                
								res.send('{"result":"ok","file":"'+outname+'","email":"'+emailHash+'","key":"'+keyword+'","s":"'+sessionid+'"}');
								res.end();
								mygc();
							}
						});                
				})
			})
		} catch (e) {
			res.status(200);
			console.log(e);
			res.send('{"result":"fail"}');
			res.end();
			mygc();
		}
    });
}

var decodeMultiple = function(req, res, tmp_root, pool, nconf, nodemailer) {
    res.setHeader('Content-Type', 'application/json');
    // clean up parameters
    console.log('Watermark multiple public decode request!');
    var form = new multiparty.Form(); 
    form.autoFiles = true;
    form.maxFilesSize = 500000000;
    form.parse(req, function(err, fields, files) {
		console.log("fields=",fields);
        var passes = fields['passes[]'];
        var filein = files['file1'][0].path;  
		console.log('PASSES:', passes);
        // processing
        try { // we have: passes, filein
            var loader = new STLLoader();
            fs.readFile(filein, function (err, data) {
                if (err) {
                    res.status(200);
                    console.log(err);
                    res.send('{"result":"fail","message":"Error while reading the file"}');
                    res.end();
                    mygc();
                    return;
                }
                THREE.Cache.enabled = true;
                var ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
                THREE.Cache.add(filein, ab); 
                loader.load(filein, function (geometry) { // file loaded
                    THREE.Cache.remove(filein); 
                    
                    // MAIN
                    var marksFound = watermarkFactory.decodeMultiple(geometry, passes);
                    watermarkFactory.cleanup();
                    if (marksFound === false) {
                        res.status(200);
                        console.log('analysis fail');
                        res.send('{"result":"fail","message":"Watermarks not found in this model."}');
                        res.end();
                        mygc();
                        return;
                    } else {
                        res.status(200);
                        console.log(' marks found: ' + JSON.stringify(marksFound));
                        var result = {"result":"ok","marks":marksFound};
                        if (JSON.stringify(marksFound)=="{}") result = {"result":"ok","message":"Watermarks not found in this model."};
                        res.send(JSON.stringify(result));
                        res.end();
                        mygc();
                    }
                })
            })
        } catch (e) {
            res.status(200);
            console.log(e);
            res.send('{"result":"fail"}');
            res.end();
            mygc();
        }
    });
}

// exports --------------------------------------
var process = function(req, res, tmp_root, pool, nconf, nodemailer) {
    if (req.url == '/jsapi/public/watermark/encode') {
        return encode(req, res, tmp_root, pool, nconf, nodemailer);
    }
    if (req.url == '/jsapi/public/watermark/decode') {
        return decode(req, res, tmp_root, pool, nconf, nodemailer);
    }
    if (req.url == '/jsapi/public/watermark/encodeMultiple') {
        return encodeMultiple(req, res, tmp_root, pool, nconf, nodemailer);
    }
    if (req.url == '/jsapi/public/watermark/decodeMultiple') {
        return decodeMultiple(req, res, tmp_root, pool, nconf, nodemailer);
    }
    res.status(500);
    console.log("unknown command: "+req.url);
    res.send('{"result":"fail"}');
    res.end();
} 

var download = function(req, res, tmp_root, pool, nconf, nodemailer) {
    console.log('Downloading: ' + req.query.file);
    res.setHeader('Content-Type', 'application/download');
    res.setHeader('Content-Disposition', 'attachment;filename='+req.query.file);
    // clean up pregmatch filename 
    var match = req.query.file.match( /[a-zA-Z01-9_]+\.stl/i );
    if (!match) {
        res.status(500);
        console.log("no math to file: "+req.url);
        res.end();
    }
    var file = tmp_root + req.query.file; 
    // TODO check file exists
    var email = new Buffer(req.query.email, 'base64').toString('ascii');
    var sessionid = "" + req.query.s;
    console.log("s=" + sessionid);
    var keyword = md5(req.query.email + salt + sessionid);
    // check email and key
    if (keyword == req.query.key) {
		if (sessionid=="raw") {
			console.log("downloading "+file); // filenameFixed
			res.status(200);
			res.download(file, "signed.stl");
		} else {
			pool.query({sql:'select filename from sessions where id = ?',values:[sessionid]}, function (error, results, fields) {
				if (!error && results.length > 0) {
					var filename = results[0].filename;
					// approve email in DB
					pool.query({sql:'update users set approved = 1 where email = ?',values:[email]}, function (error, results, fields) {
						console.log("email approved: "+email);
					});
					// download
					//var parts = path.parse(filename);
					//var filenameFixed = parts.name + "_marked.stl";
					res.status(200);
					console.log("downloading "+file+" as "+filename); // filenameFixed
					res.download(file, filename);
					// res.sendFile(file);
				} else {
					res.status(500);
					console.log("sessions to file not found: "+req.url);
					res.end();
				}
			});
		};
    } else {
        // TODO noaccess
        res.status(500);
        console.log("keyword wrong: "+req.url);
        res.end();
    }
}

module.exports.encode = encode;
module.exports.decode = decode;
module.exports.encodeMultiple = encodeMultiple;
module.exports.decodeMultiple = decodeMultiple;
module.exports.download = download;
module.exports.process = process;
