
var fs = require('fs');
const path = require('path');
var uuid = require('uuid/v4');    
var THREE = require('three');
const exec = require('child_process').exec;
var STLLoader = require('./three-stl-loader')(THREE);
var Models = require('./cloudcam-models');


/**
 * @type {_}
 */
var _ = require('lodash');
var BinaryStlWriter = require('./stlbinexporter');
var arrayBufferToBuffer = require('arraybuffer-to-buffer');

var defaultParams = {
    instrumentRoughUsage: 1, instrumentFinishUsage: 1, // layered?
    materials: [
        {material: 'steel', density: 7800 }
    ],
    defaultDeltaStage2: 1.5
};
var indexSteps = 100;
    
// convert STEP IGES to STL. accuracy in mm (=0.01 for example), callback (err)
/**
 *
 * @param infilename
 * @param outfilename
 * @param accuracy
 * @param callback
 */
function convertToStl(infilename, outfilename, accuracy, callback) {
    try { 
        var extension = path.extname(infilename).toLowerCase(); 
        var commandFile = "/tmp/" + uuid() + ".cmd";
        if (extension === ".step") {
            fs.writeFile(commandFile, 
                "pload ALL\nstepread "+infilename+" a * \nincmesh a_1 "+accuracy+"\nwritestl a_1 "+outfilename+"\n", 
                function(err) {
                    var command = "/usr/bin/drawf.sh "+commandFile;
                    exec(command, function(err, stdout, stderr) {
                            callback(err); 
                        });            
                }); 
        } else if (extension === ".iges") {
            fs.writeFile(commandFile, 
                "pload ALL\nigesread "+infilename+" a * \nincmesh a "+accuracy+"\nwritestl a "+outfilename+"\n", 
                function(err) {
                    var command = "/usr/bin/drawf.sh "+commandFile;
                    exec(command, function(err, stdout, stderr) {
                            callback(err); 
                        });            
                }); 
        } else if (extension === ".stl") {
            copyFile(infilename, outfilename, callback);            
        }
    } catch (e) {
        console.log(e);
        callback("Exception");
    }
}

/**
 *
 * @param stlFileName
 * @param callback
 */
function analyze(stlFileName, callback) {
    try { 
    // load settings
    //var fs = require('fs');
    //fs.readFile(settingsFileName, 'utf8', function (err, data) {
    //    if (err) throw err;
    //    var settings = JSON.parse(data);
        // load geometry
        var loader = new STLLoader();
        fs.readFile(stlFileName, function (err, data) {
            if (err) {
                console.log('error reading file');
                callback();
                return;
            }
            try { 
                THREE.Cache.enabled = true;
                var ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
                THREE.Cache.add(stlFileName, ab); 
                loader.load(stlFileName, function (geometry) { // file loaded
                    try { 
                        THREE.Cache.remove(stlFileName); 
                        // MAIN
                        unbuffered = new THREE.Geometry().fromBufferGeometry( geometry );
                        unbuffered.mergeVertices();
                        unbuffered.computeVertexNormals();
                        unbuffered.computeBoundingSphere();
                        analyzeGeometry(unbuffered, callback);
                    } catch (e) {
                        console.log(e);
                        callback();
                    }
                });
            } catch (e) {
                console.log(e);
                callback();
            }
        });
    //});
    } catch (e) {
        console.log(e);
        callback();
    }
}

/**
 *
 * @param geometry
 * @param callback
 */
function analyzeGeometry(geometry, callback) {    
    // prepare raycaster
    var raycaster = new THREE.Raycaster();  
    var mesh0 = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({side: THREE.DoubleSide}));
    var objects = [];
    objects.push( mesh0 );    
    // measures
    var measures = measure(geometry);
    geometry.computeBoundingBox();
    var box = geometry.boundingBox;
    // detect sizes
    var instrSizesDetected = {};    
    var faces = geometry.faces;
    var vertices = geometry.vertices;
    var cnt = faces.length;
    for (var i = 0; i<cnt; i++) {
        if (i % 100  == 0) console.log("processed: " + i + " from " + cnt + " triangles.");
        // find ray
        var face = faces[i];
        var normal = face.normal;
        var facea = vertices[face.a];
        var faceb = vertices[face.b];
        var facec = vertices[face.c];    
        var center = facea.clone().add(faceb).add(facec).multiplyScalar(1/3).add(normal.clone().multiplyScalar(0.001));
        // try detect if it is ray from green area?
        //var processingData = getFaceProcessingData(geometry, face, minimalInstrument, center); 
        //if (processingData && processingData.instrumentPosition) {

        // try find ray length
        var l = getRayLength(center, normal, raycaster, objects);
        if (l > 0){
            var lr = l.toFixed(1);
            var tarea = areaOfTriangle(facea.x, facea.y, facea.z, faceb.x, faceb.y, faceb.z, facec.x, facec.y, facec.z);
            if (lr in instrSizesDetected) {
                instrSizesDetected[lr].area += tarea;
                instrSizesDetected[lr].cnt += 1;
            } else {
                instrSizesDetected[lr] = { area: tarea, cnt: 1};
            }
        }

    }
    var filterAreaVal = measures.area * 0.01; // 1% of area
    var filterCntVal = 100; // or more than 100 triangles
    var minInstrumentD = 1e10;
    Object.keys(instrSizesDetected).forEach(function (key) {
        if (instrSizesDetected[key].area > filterAreaVal || instrSizesDetected[key].cnt > filterCntVal) {
            console.log("size ~= " + key + ", cnt=" + instrSizesDetected[key].cnt+" area="+instrSizesDetected[key].area);
            if (minInstrumentD > key && key>0) minInstrumentD = parseFloat(key);
        }
        // do something with obj[key]
    });        
    console.log("minInstrumentD= " + minInstrumentD);
    // result of cosing:
    callback({result: "ok", minInstrumentD: minInstrumentD, measures: measures, box: box});
}

/**
 *
 * @param point
 * @param axis
 * @param raycaster
 * @param objects
 * @return {number}
 */
function getRayLength(point, axis, raycaster, objects) {
    raycaster.set( point, axis );
    var intersections = raycaster.intersectObjects( objects );
    if (intersections.length>0){
        return intersections[ 0 ].point.distanceTo(point);
    }
    return -1;
}

/**
 * Read service file and calculate costing
 * @param {String} service
 * @param analyze
 * @param preset
 * @param {Function} callback
 * @throws Error
 */
function costing(service, analyze, preset, callback)
{
    fs.readFile(service, 'utf8', (error, serviceData) => {

        if (error) {
            callback(undefined, error);
            return;
        }

        console.log(serviceData + "====");
        var settings = JSON.parse(serviceData);

        try {
            costingInternal(settings, analyze, preset, callback);
        }
        catch (e) {
            callback(undefined, e);
        }
    });
}

/**
 * Calculate costing
 * @param {CncService} settings
 * @param analyze
 * @param preset
 * @param {Function} callback
 */
function costingInternal(settings, analyze, preset, callback)
{
    // TODO replace to promise
    let logAndFail = function (message) {
        console.log(message);
        callback(undefined, message);
    };

    let measures = analyze.measures;

    // find best instrument
    console.log("trying to find instrument less than " + analyze.minInstrumentD * 0.8);
    let material = preset.material;

    // fit rough?
    let roughInstrument = _(settings.millingInstruments)
        .filter(o => o.material === material && o.quality === "rough" && o.d < analyze.minInstrumentD * 0.8)
        .sortBy(["d"]).reverse()
        .head();

    // fit any?
    if (!roughInstrument) roughInstrument = _(settings.millingInstruments)
        .filter(o => o.material === material && o.d < analyze.minInstrumentD * 0.8)
        .sortBy(["d"]).reverse()
        .head();

    // smallest?
    if (!roughInstrument) roughInstrument = _(settings.millingInstruments)
        .filter(o => o.material === material)
        .sortBy(["d","quality"])
        .head();

    if (!roughInstrument) {
        logAndFail('No tools have been set for the material: ' + material);
        return;
    }
    console.log("!!!! Rough instrument choosen: d=" + roughInstrument.d + "");

    // find stock
    let delta = settings.settings.stockSpacers; // mm over box
    
    // find finish instrument
    let finishInstrument = _(settings.millingInstruments)
        .filter(o => o.material === material && o.quality === "finish" && o.d < analyze.minInstrumentD * 0.8)
        .sortBy(["d"])
        .reverse()
        .head();

    if (!finishInstrument) finishInstrument = _(settings.millingInstruments)
        .filter(o => o.material === material && o.quality === "rough" && o.d < analyze.minInstrumentD * 0.8)
        .sortBy(["d"])
        .reverse()
        .head();

    if (!finishInstrument) finishInstrument = roughInstrument;

    let delta2stage = finishInstrument.delta > 0
        ? finishInstrument.delta
        : defaultParams.defaultDeltaStage2; // mm over box

    let activeInstrument = finishInstrument; // .d = minInstrumentD * 0.6; // TODO!!!

    console.log("!!! Finish = active instrument choosen: d=" + finishInstrument.d + "");

    //estimate -----------------------------

    let box = analyze.box;

    let stockComment = 'Material Cost: ' + material + ' (' +
            Math.round(box.max.x - box.min.x + delta*2 ) + " x " +
            Math.round(box.max.y - box.min.y + delta*2) + " x " +
            Math.round(box.max.z - box.min.z + delta*2) + " mm)";
    let vofstock = (box.max.x - box.min.x + delta*2)*
                   (box.max.y - box.min.y + delta*2)*
                   (box.max.z - box.min.z + delta*2);
    let volof2stage = measures.area * delta2stage;
    let volof1stage = vofstock - measures.volume - volof2stage;
    if (volof1stage < 0) volof1stage = 0;
    
    // find machine
    let stockMaxSize = Math.max(box.max.x - box.min.x, box.max.y - box.min.y, box.max.z - box.min.z);

    /** @let activeMachine CncMillingMachine */
    let activeMachine = _(settings.millingMachines)
        .filter(o => o.volumeW > stockMaxSize && o.volumeH > stockMaxSize && o.volumeL > stockMaxSize)
        .sortBy(["priceMachining"]) // .reverse()
        .head(); // TODO precalc price for EVERY machine???

    if(!activeMachine) {
        logAndFail('Sorry, the vendor doesn\'t have a bar stock big enough for this part. Max stock size ' + stockMaxSize+' mm.');
        return;
    }

    let activeStock = _(settings.stocks.freeform)
        .filter(o => o.material === material)
        .sortBy(["price"])
        .head(); // TODO precalc price for EVERY machine???

    if (!activeStock) {
        logAndFail('Suitable stock size could not be found for this material: ' + material);
        return;
    }

    let activeMaterial = _(settings.materials)
        .filter(o => o.title === material)
        .head(); // TODO precalc price for EVERY machine???

    if (!activeMaterial) {
        logAndFail('Cant find material ' + material);
        return;
    }


    /**
     * @type {PriceElement[]}
     */
    let priceElements = [];
    let sum = 0;

    let price = Math.round(100*(activeStock.price * activeMaterial.density *  (vofstock)/(1000*1000*1000)))/100;

    priceElements.push(new Models.PriceElement(stockComment, price, true));
    sum += price;


    //for (key in millingParams.instruments) {
    { // rough     
        let instrument = roughInstrument;
        // d is diameter in mm
        // fr in mm per rotation
        // s in m/min
        // l is length in mm
        // delta is betwin stages 1 and 2 in mm
        
        // проверка по мощности: N(kwt) = 0.13 * Pz * v
        // v = скорость реза м/мин
        // Pz = усилие протягивания кГ = psz * сумма наибольшей суммарной длины одновременно работающих лезвий
        // psz = сила реза в кГ на 1 мм при подаче sz 
        // TODO

        let speed = Math.min(instrument.s, activeMachine.maxRPM * instrument.d * 3.14 / (1000)); // speed in m in m, limit by rpm
        let mrr = (speed*1000/3.14)*(instrument.fr)*(instrument.l) / 60; // mm3 in seconds
        let vol, usage;
        vol = volof1stage;
        usage = defaultParams.instrumentRoughUsage;
        let time = vol / (mrr * usage);
        let price = Math.round((activeMachine.priceMachining * (time) / 3600) *100)/100;
        priceElements.push(new Models.PriceElement(
            "Roughing with Tool Diameter " + instrument.d + " mm",
            price ,
            true
        ));
        sum += price;
    }



    { // finish     
        let instrument = finishInstrument;
        // d is diameter in mm
        // fr in mm per rotation
        // s in m/min
        // l is length in mm
        // delta is betwin stages 1 and 2 in mm
        let speed = Math.min(instrument.s, activeMachine.maxRPM * instrument.d * 3.14 / (1000)); // speed in m in m, limit by rpm
        let mrr = (speed*1000/3.14)*(instrument.fr)*(instrument.l) / 60; // mm3 in seconds
        let usage = defaultParams.instrumentFinishUsage;
        let time = volof2stage / (mrr * usage);
        let price = Math.round((activeMachine.priceMachining * (time) / 3600) *100)/100;

        priceElements.push(new Models.PriceElement(
            "Finishing with Tool Diameter " + instrument.d + " mm ",
            price,
            true
        ));
        sum += price;
    }

    // postprocessing
    let postprocesses = JSON.parse(preset.postprocessing);
    postprocesses.forEach(requireProcess => {
        /** @let activeProcess CncPostprocessing */
        let activeProcess = _(settings.postprocessing)
            .filter(o => o.title === requireProcess)
            .head(); // TODO precalc price for EVERY machine???
        let price = activeProcess.pricePerPart;
        price += activeProcess.pricePerArea * measures.area / 10000; // mm2->dm2
        price += activeProcess.pricePerVolume * measures.area / 1000000; // mm3->dm3
        price = Math.round(price *100)/100;

        priceElements.push(new Models.PriceElement(
            "Post Processing",
            price,
            true
        ));

        sum += price;
    });
    
    // TODO 6-axis
    
    // setup
    price =  Math.round(100*( activeMachine.priceLabourOperator * activeMachine.placetime / 3600 ))/100;

    priceElements.push(new Models.PriceElement("Set-up cost per item", price, true));

    sum += price;

    let setup = Math.round(100*( activeMachine.priceLabourProgrammer * activeMachine.programmingtime / 60 ))/100;

    priceElements.push(new Models.PriceElement("Start-up cost", setup, false));

    let total = Math.round(100*(  sum * preset.count + setup ))/100;

    callback({priceElements: priceElements, total: total, count: preset.count, currency: "USD", instrument: activeInstrument});
}

/**
 *
 * @param finefile
 * @param fileprefix
 * @param outgeomfile
 * @param outspecfile
 * @param analyze
 * @param preset
 * @param costing
 * @param callback
 */
function makeModels(finefile, fileprefix, outgeomfile, outspecfile, analyze, preset, costing, callback) {
    try {
    // var settings = JSON.parse(data);
    // load geometry
    var loader = new STLLoader();
    fs.readFile(finefile, function (err, data) {
        if (err) {
            console.log('error reading file');
            callback();
            return;
        }
        try {
            THREE.Cache.enabled = true;
            var ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
            THREE.Cache.add(finefile, ab); 
            loader.load(finefile, function (geometry) { // file loaded
                try {
                    THREE.Cache.remove(finefile); 
                    // MAIN
                    makeModelsInternal(geometry, fileprefix, outgeomfile, outspecfile, analyze, preset, costing, callback);
                } catch (e) {
                    console.log(e);
                    callback();
                }
            });
        } catch (e) {
            console.log(e);
            callback();
        }
    });
    } catch (e) {
        console.log(e);
        callback();
    }
}

/**
 *
 * @param geometry
 * @param fileprefix
 * @param outgeomfile
 * @param outspecfile
 * @param analyze
 * @param preset
 * @param costing
 * @param callback
 */
function makeModelsInternal(geometry, fileprefix, outgeomfile, outspecfile, analyze, preset, costing, callback) {
    var activeInstrument = costing.instrument;
    console.log('Costing:' + costing);
    console.log('Modelling for instrument d=' + activeInstrument.d);
    console.log('Convert to unbuffered...');
    var unbuffered = new THREE.Geometry().fromBufferGeometry( geometry );
    unbuffered.computeBoundingBox();
    console.log("tesselating deep..." + activeInstrument.d/3);
    tesselateGeomerty(unbuffered, activeInstrument.d/3);
    //unbuffered.mergeVertices();
    console.log("preparing validators...");
    fastners = prepareFastInstrumentValidators(unbuffered);
    //unbuffered.mergeVertices();
    // re-set-up raycasters
    //var objectsHQ = [];
    //objectsHQ.push(meshMain);        
    // calc colored
    var geometryFaceted = faceteColorizeGeometry(unbuffered, activeInstrument, fastners);
    // save result
    var material = new THREE.MeshNormalMaterial();
    var mesh = new THREE.Mesh(geometryFaceted, material);
    var theArrayBuffer = BinaryStlWriter.meshBufferedToBinArrayBufferST2(mesh, activeInstrument.d); // geometryToDataViewBin
    fs.writeFile(fileprefix+outgeomfile,  arrayBufferToBuffer(theArrayBuffer),  function(err) {
            if(err) {
                callback();
            } else {
                // ok! return URL of file
                callback({result:"ok",plyColored:fileprefix+outgeomfile});
            }
    });    
}

/**
 *
 * @param unbuffered
 * @param instrument
 * @param fastners
 * @return {THREE.BufferGeometry}
 */
function faceteColorizeGeometry(unbuffered, instrument, fastners){
    // var unbuffered = new THREE.Geometry().fromBufferGeometry( geometry );
    // var unbuffered = geometry;
    // unbuffered.mergeVertices();
    // prepareFastInstrumentValidators();
    var vertices = unbuffered.vertices;
    var faces = unbuffered.faces;
    // pre-count
    var cnt = faces.length;
    // get mem
    var newpositions = new Float32Array( cnt * 3 * 3);
    var newnormals = new Float32Array( cnt * 3  * 3);
    var newcolors = new Float32Array( cnt * 3 * 3 );
    var color = new THREE.Color();
    // fill in
    for (var i = 0; i<cnt; i++) {
        if (i % 10000  == 0) console.log("colorized: " + i + " from " + cnt + " triangles.");
        var face = faces[i];
        var normal = face.normal;
        var facea = vertices[face.a];
        var faceb = vertices[face.b];
        var facec = vertices[face.c];
        var point = facea.clone().add(faceb).add(facec).multiplyScalar(1/3);
        newpositions[ i*9+0 ] = facea.x; 
        newpositions[ i*9+1 ] = facea.y; 
        newpositions[ i*9+2 ] = facea.z;
        newpositions[ i*9+3 ] = faceb.x; 
        newpositions[ i*9+4 ] = faceb.y; 
        newpositions[ i*9+5 ] = faceb.z; 
        newpositions[ i*9+6 ] = facec.x; 
        newpositions[ i*9+7 ] = facec.y; 
        newpositions[ i*9+8 ] = facec.z; 
        var faceData = getFaceProcessingData(unbuffered, face, instrument, point, fastners); // was null)
        color = faceData.color;
        if (faceData.instrumentPosition) {
            normal = faceData.instrumentPosition.startPoint; // fake normal!  axis: axis, mode:'front', startPoint:point
            newcolors[ i*9+3 ] = encodeAxis(faceData.instrumentPosition.axis); // color.r;
        } else {
            // normal any
            newcolors[ i*9+3 ] = 0;
        }
        //newcolors[ i*9+3 ] = 1;
        //normal = point;
        
        newnormals[ i*9+0 ] = normal.x; 
        newnormals[ i*9+1 ] = normal.y; 
        newnormals[ i*9+2 ] = normal.z;
        newnormals[ i*9+3 ] = normal.x; 
        newnormals[ i*9+4 ] = normal.y; 
        newnormals[ i*9+5 ] = normal.z; 
        newnormals[ i*9+6 ] = normal.x; 
        newnormals[ i*9+7 ] = normal.y; 
        newnormals[ i*9+8 ] = normal.z;
//         color.setRGB( Math.random(), Math.random(), Math.random() );
        newcolors[ i*9+0 ] = color.r;
        newcolors[ i*9+1 ] = color.g;
        newcolors[ i*9+2 ] = color.b;
        newcolors[ i*9+4 ] = color.g;
        newcolors[ i*9+5 ] = color.b;
        newcolors[ i*9+6 ] = color.r;
        newcolors[ i*9+7 ] = color.g;
        newcolors[ i*9+8 ] = color.b;
//            newcolors[ i*9+3 ] = encodeAxis(faceData.instrumentPosition.axis); // color.r;
    }        
    // finish up
    var geometry1 = new THREE.BufferGeometry();
    function disposeArray() { /* this.array = null; */ }
    geometry1.addAttribute( 'position', new THREE.BufferAttribute( newpositions, 3 ).onUpload( disposeArray ) );
    geometry1.addAttribute( 'normal', new THREE.BufferAttribute( newnormals, 3 ).onUpload( disposeArray ) );
    geometry1.addAttribute( 'color', new THREE.BufferAttribute( newcolors, 3 ).onUpload( disposeArray ) );
    geometry1.computeBoundingBox();
    console.log("geometry colored");
    return geometry1;
}

/**
 *
 * @param axis
 * @return {number}
 */
function encodeAxis(axis) {
         if (axis.x== 1) { return 1; }
    else if (axis.x==-1) { return 2; }
    else if (axis.y== 1) { return 3; }
    else if (axis.y==-1) { return 4; }
    else if (axis.z== 1) { return 5; }
    else if (axis.z==-1) { return 6; }
    return 0;
}

/**
 *
 * @param geometry
 * @param face
 * @param instrument
 * @param point
 * @param fastners
 * @return {{color: THREE.Color, instrumentPosition: *}}
 */
function getFaceProcessingData(geometry, face, instrument, point, fastners) {
    var vertices = geometry.vertices;
    var faces = geometry.faces;
    // try find instrument position
    var color = new THREE.Color();
    var rnd1 = 1; // 0.3 + Math.random() * 0.7;// for colorize tris
    color.setRGB( 1, 0, 0 );
    var instrumentPosition;
    if (!point) {
        // got centerpoint of tri
        point = vertices[face.a].clone().add(vertices[face.b]).add(vertices[face.c]).multiplyScalar(1/3);
    }
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(0,1,0), instrument, point, fastners )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.20 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(0,-1,0), instrument, point, fastners )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.21 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(1,0,0), instrument, point, fastners )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.22 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(-1,0,0), instrument, point, fastners )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.23 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(0,0,1), instrument, point, fastners )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.24 );
    } else 
    if (instrumentPosition = isFaceProcessingUsingAxis(geometry, face, new THREE.Vector3(0,0,-1), instrument, point, fastners )) // we have y-oriented cylinder?
    {
        color.setRGB( 0, rnd1, 0.25 );
    } else 
    {
        instrumentPosition = null;
        color.setRGB( 1, 0, 0 );
    }
    return {color: color, instrumentPosition: instrumentPosition};
}

/**
 *
 * @param geometry
 * @param face
 * @param axis
 * @param instrument
 * @param point
 * @param fastners
 * @return {*}
 */
function isFaceProcessingUsingAxis(geometry, face, axis, instrument, point, fastners) {
    var vertices = geometry.vertices;
    var faces = geometry.faces;
    var normal = face.normal;
    var delta = 0.01;
    // analyze
    var distToPlane = normal.clone().projectOnPlane(axis).distanceTo(normal);
    if (distToPlane < delta){
        // side of instrument
        var delta1 = normal.clone().projectOnPlane(axis).normalize();
        var instrumentStart = point.clone().add(delta1.multiplyScalar(instrument.d/2));
        if (!validateInstrument(geometry,instrumentStart, axis, instrument, fastners)) return false; // 
        return { axis: axis, mode:'side', startPoint: instrumentStart };
    }
    var distToAxis1 = normal.clone().projectOnVector(axis).distanceTo(axis);
    if (distToAxis1 < delta) {
        // front of instrument
        if (!validateInstrument(geometry,point, axis, instrument, fastners)){
            // try to find better instrument position then default
            var closestPoint = findClosestPointToAxis(geometry,point, axis, instrument, fastners);
            var instrumentStart = point.clone().sub(closestPoint).normalize()
                .multiplyScalar(instrument.d/2)
                .add(closestPoint);
            if (validateInstrument(geometry,instrumentStart, axis, instrument, fastners))     {
                return { axis: axis, mode:'front', startPoint:instrumentStart };
            } else {
                return false; 
            }
        } 
        return { axis: axis, mode:'front', startPoint:point };
    }
    //if (distToAxis2 < delta) {
        // front of instrument
    //    if (!validateInstrument(geometry,point, axis.clone().multiplyScalar(-1), instrument)) return false; // 
    //    return { axis: axis.clone().multiplyScalar(-1), mode:'front', startPoint:point };
    //}
    return false;
}

/**
 *
 * @param geometry
 * @param instrumentStart
 * @param axis
 * @param instrument
 * @param fastners
 */
function validateInstrument(geometry,instrumentStart, axis, instrument, fastners)
{
    return validateInstrumentIndexedDekart(geometry,instrumentStart, axis, instrument, fastners);
}

/**
 *
 * @param geometry
 * @param instrumentStart0
 * @param axis
 * @param instrument
 * @param fastners
 * @return {boolean}
 */
function validateInstrumentIndexedDekart(geometry,instrumentStart0, axis, instrument, fastners) // we assume only 3-axis x y z...
{
    // geometry = unbuffered;
    var indexBox = geometry.boundingBox;
    var delta = 0.1;
    var vertices = geometry.vertices;
    instrumentStart = instrumentStart0.clone().add(axis.clone().normalize().multiplyScalar(delta));
    var deltar = instrument.d*(1-delta)/2;
    var minr = Math.pow(deltar, 2);
    var ix = instrumentStart.x, iy = instrumentStart.y, iz = instrumentStart.z;
    if (axis.x == 0 && axis.y == 0) { // by-z 
        var imin1 = getIndexVal(ix-deltar, 0, -1, indexBox);
        var imax1 = getIndexVal(ix+deltar, 0, +1, indexBox);
        var imin2 = getIndexVal(iy-deltar, 1, -1, indexBox);
        var imax2 = getIndexVal(iy+deltar, 1, +1, indexBox);
        if(axis.z>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneXY[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].z>iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                            return false;
                    }
                }
        if(axis.z<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneXY[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].z<iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                            return false;
                    }
                }
    }
    if (axis.x == 0 && axis.z == 0) { // by-z 
        var imin1 = getIndexVal(ix-deltar, 0, -1, indexBox);
        var imax1 = getIndexVal(ix+deltar, 0, +1, indexBox);
        var imin2 = getIndexVal(iz-deltar, 2, -1, indexBox);
        var imax2 = getIndexVal(iz+deltar, 2, +1, indexBox);
        if(axis.y>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneXZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].y>iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                            return false;
                    }
                }
        if(axis.y<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneXZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].y<iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                            return false;
                    }
                }
    }
    if (axis.y == 0 && axis.z == 0) { // by-z 
        var imin1 = getIndexVal(iy-deltar, 1, -1, indexBox);
        var imax1 = getIndexVal(iy+deltar, 1, +1, indexBox);
        var imin2 = getIndexVal(iz-deltar, 2, -1, indexBox);
        var imax2 = getIndexVal(iz+deltar, 2, +1, indexBox);
        if(axis.x>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneYZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].x>ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                            return false;
                    }
                }
        if(axis.x<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneYZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].x<ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                            return false;
                    }
                }
    }    
    return true;
}

/**
 *
 * @param geometry
 * @param instrumentStart0
 * @param axis
 * @param instrument
 * @param fastners
 */
function findClosestPointToAxis(geometry,instrumentStart0, axis, instrument, fastners) {
    // TODO move circle by-limiting-angles
    // geometry = unbuffered;
    var indexBox = geometry.boundingBox;
    var delta = 0.1;
    var vertices = geometry.vertices;
    instrumentStart = instrumentStart0.clone().add(axis.clone().normalize().multiplyScalar(delta));
    var deltar = instrument.d*(1-delta)/2;
    var minr = 1e10;
    var closestPoint = new THREE.Vector3();
    var ix = instrumentStart.x, iy = instrumentStart.y, iz = instrumentStart.z;
    if (axis.x == 0 && axis.y == 0) { // by-z 
        var imin1 = getIndexVal(ix-deltar, 0, -1, indexBox);
        var imax1 = getIndexVal(ix+deltar, 0, +1, indexBox);
        var imin2 = getIndexVal(iy-deltar, 1, -1, indexBox);
        var imax2 = getIndexVal(iy+deltar, 1, +1, indexBox);
        if(axis.z>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneXY[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].z>iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                        {
                            minr = Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2);
                            closestPoint = vertices[i];
                        }
                    }
                }
        if(axis.z<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneXY[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].z<iz && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2) < minr)
                        {
                            minr = Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].y-iy,2);
                            closestPoint = vertices[i];
                        }
                    }
                }
    }
    if (axis.x == 0 && axis.z == 0) { // by-z 
        var imin1 = getIndexVal(ix-deltar, 0, -1, indexBox);
        var imax1 = getIndexVal(ix+deltar, 0, +1, indexBox);
        var imin2 = getIndexVal(iz-deltar, 2, -1, indexBox);
        var imax2 = getIndexVal(iz+deltar, 2, +1, indexBox);
        if(axis.y>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneXZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].y>iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                        {
                            minr = Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2);
                            closestPoint = vertices[i];
                        }
                    }
                }
        if(axis.y<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneXZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].y<iy && Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2) < minr)
                        {
                            minr = Math.pow(vertices[i].x-ix,2) + Math.pow(vertices[i].z-iz,2);
                            closestPoint = vertices[i];
                        }
                    }
                }
    }
    if (axis.y == 0 && axis.z == 0) { // by-z 
        var imin1 = getIndexVal(iy-deltar, 1, -1, indexBox);
        var imax1 = getIndexVal(iy+deltar, 1, +1, indexBox);
        var imin2 = getIndexVal(iz-deltar, 2, -1, indexBox);
        var imax2 = getIndexVal(iz+deltar, 2, +1, indexBox);
        if(axis.x>0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneYZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].x>ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                        {
                            minr = Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) ;
                            closestPoint = vertices[i];
                        }
                    }
                }
        if(axis.x<0)
            for(var i1 = imin1; i1<=imax1; i1+= 1)
                for(var i2 = imin2; i2<=imax2; i2+= 1) {
                    var lst = fastners.indexPlaneYZ[i1*indexSteps + i2];
                    for(var j = 0; j<lst.length; j++) {
                        i = lst[j];
                        if(vertices[i].x<ix && Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) < minr)
                        {
                            minr = Math.pow(vertices[i].y-iy,2) + Math.pow(vertices[i].z-iz,2) ;
                            closestPoint = vertices[i];
                        }
                    }
                }
    }    
    var closestPointProjected = instrumentStart0.clone().add(
        closestPoint.clone().projectOnPlane(axis).sub(
            instrumentStart0.clone().projectOnPlane(axis)
        )
    );  
    return closestPointProjected;    
}

/**
 *
 * @param unbuffered
 * @param maxlen
 */
function tesselateGeomerty(unbuffered, maxlen){
    // unbuffered = origGeometry.clone(); // ? re-tesselating from scratch?
    unbuffered.dynamic = true;
    var faces = unbuffered.faces;
    var cnt = faces.length;
    tesselationNewVertices = {};
    for (var i = 0; i<cnt; i++) {
        if (i % 1000  == 0) console.log("tesselated: " + i + " from " + cnt + " triangles.");
        tesselateFace(unbuffered, i, maxlen, tesselationNewVertices);
    }
    console.log("tesselated!");
    //unbuffered.computeFaceNormals();
    //unbuffered.computeVertexNormals();
    tesselationNewVertices = {};
    unbuffered.verticesNeedUpdate = true;
    // showProgress();
}

/**
 *
 * @param vector3Vertice
 * @param tesselationNewVertices
 * @param unbuffered
 * @return {*}
 */
function getOrCreateVertice(vector3Vertice, tesselationNewVertices, unbuffered) {
    var key = vector3Vertice.x.toFixed(6) + "_" + vector3Vertice.y.toFixed(6) + "_" + vector3Vertice.z.toFixed(6); 
    if (! ( key in tesselationNewVertices)) {
        unbuffered.vertices.push(vector3Vertice);
        tesselationNewVertices[key] = unbuffered.vertices.length - 1;
    }
    return tesselationNewVertices[key];
}

/**
 *
 * @param unbuffered
 * @param faceidx
 * @param maxlen
 * @param tesselationNewVertices
 */
function tesselateFace(unbuffered, faceidx, maxlen, tesselationNewVertices) {
    var faces = unbuffered.faces;
    var face = faces[faceidx];
    var vertices = unbuffered.vertices;
    var va = vertices[face.a], vb=vertices[face.b], vc=vertices[face.c];
    var ab = va.distanceTo(vb), ac = va.distanceTo(vc), bc = vb.distanceTo(vc);
    var m = Math.max(ab,ac,bc);
    if (m>maxlen) {
        var newv;
        if (m==ab) {
            //vertices.push(va.clone().add(vb).multiplyScalar(0.5));
            //newv = vertices.length - 1;
            newv = getOrCreateVertice(va.clone().add(vb).multiplyScalar(0.5), tesselationNewVertices, unbuffered);
            faces.push(new THREE.Face3(newv, face.b, face.c, face.normal, face.color, face.materialIndex)); 
            face.b = newv;
        } else if (m==ac) {
            //vertices.push(va.clone().add(vc).multiplyScalar(0.5));
            //newv = vertices.length - 1;
            newv = getOrCreateVertice(va.clone().add(vc).multiplyScalar(0.5), tesselationNewVertices, unbuffered);
            faces.push(new THREE.Face3(newv, face.b, face.c, face.normal, face.color, face.materialIndex));            
            face.c = newv;
        } else /*m==bc*/ {
            //vertices.push(vb.clone().add(vc).multiplyScalar(0.5));
            //newv = vertices.length - 1;
            newv = getOrCreateVertice(vb.clone().add(vc).multiplyScalar(0.5), tesselationNewVertices, unbuffered);
            faces.push(new THREE.Face3(face.a, newv, face.c, face.normal, face.color, face.materialIndex));            
            face.c = newv;
        }
        var newf = faces.length - 1;
        tesselateFace(unbuffered, faceidx, maxlen, tesselationNewVertices);
        tesselateFace(unbuffered, newf, maxlen, tesselationNewVertices);
    }
}

/**
 *
 * @param unbuffered
 * @return {{indexPlaneXY: Array, indexPlaneYZ: Array, indexPlaneXZ: Array}}
 */
function prepareFastInstrumentValidators(unbuffered) {
    var vertices = unbuffered.vertices;
    var indexBox = unbuffered.boundingBox;
    var indexPlaneXY = [];
    var indexPlaneYZ = [];
    var indexPlaneXZ = [];
    var i;

    for(i = 0; i<(indexSteps+1)*(indexSteps+1); i++)  {
        indexPlaneXY[i] = [];
        indexPlaneXZ[i] = [];
        indexPlaneYZ[i] = [];
    }
    for(i = 0; i<vertices.length; i++)  {
        var x = getIndexVal(vertices[i].x, 0, 0, indexBox);
        var y = getIndexVal(vertices[i].y, 1, 0, indexBox);
        var z = getIndexVal(vertices[i].z, 2, 0, indexBox);
        indexPlaneXY[x*indexSteps + y].push(i);
        indexPlaneXZ[x*indexSteps + z].push(i);
        indexPlaneYZ[y*indexSteps + z].push(i);
    }
    console.log('instrument validators indexed.');
    return {indexPlaneXY: indexPlaneXY, indexPlaneYZ: indexPlaneYZ, indexPlaneXZ: indexPlaneXZ};
}

/**
 *
 * @param coordval
 * @param coordnum
 * @param roundup
 * @param indexBox
 * @return {number}
 */
function getIndexVal(coordval, coordnum, roundup, indexBox) { // coordnum: 0=x, 1=y, 2=z; roundup = 0=round, -1=floor, 1=ceil
    var r = 0;
    switch (coordnum * 3 + 1 + roundup) {
        case 0:
            r = Math.floor((coordval - indexBox.min.x)*indexSteps*1.0/(indexBox.max.x - indexBox.min.x));
            break;
        case 1:
            r =  Math.round((coordval - indexBox.min.x)*indexSteps*1.0/(indexBox.max.x - indexBox.min.x));
            break;
        case 2:
            r = Math.ceil ((coordval - indexBox.min.x)*indexSteps*1.0/(indexBox.max.x - indexBox.min.x));
            break;
        case 3:
            r = Math.floor((coordval - indexBox.min.y)*indexSteps*1.0/(indexBox.max.y - indexBox.min.y));
            break;
        case 4:
            r = Math.round((coordval - indexBox.min.y)*indexSteps*1.0/(indexBox.max.y - indexBox.min.y));
            break;
        case 5:
            r = Math.ceil ((coordval - indexBox.min.y)*indexSteps*1.0/(indexBox.max.y - indexBox.min.y));
            break;
        case 6:
            r = Math.floor((coordval - indexBox.min.z)*indexSteps*1.0/(indexBox.max.z - indexBox.min.z));
            break;
        case 7:
            r = Math.round((coordval - indexBox.min.z)*indexSteps*1.0/(indexBox.max.z - indexBox.min.z));
            break;
        case 8:
            r = Math.ceil ((coordval - indexBox.min.z)*indexSteps*1.0/(indexBox.max.z - indexBox.min.z));
            break;
    }
    if (r < 0) r = 0;
    if (r > 100) r = 100;
    return r;
}

/**
 *
 * @param geometry
 * @return {*}
 */
function measure(geometry){
    var va,vb,vc,len;
    var verticesArray;
    var buffered = ! (geometry.faces);
    if (buffered) {
        len = geometry.attributes.position.array.length;
        verticesArray = geometry.attributes.position.array;
    } else {
        len = geometry.faces.length;
    }
    if (!len) return 0.0;

    var area = 0.0,
        volume = 0.0;
    for (var i = 0; i < len; i++) {
        if (buffered) {
            va = new THREE.Vector3(verticesArray[i],verticesArray[i+1],verticesArray[i+2]);
            vb = new THREE.Vector3(verticesArray[i+3],verticesArray[i+4],verticesArray[i+5]);
            vc = new THREE.Vector3(verticesArray[i+6],verticesArray[i+7],verticesArray[i+8]);
            i+=9-1;
        } else {
            va = geometry.vertices[geometry.faces[i].a];
            vb = geometry.vertices[geometry.faces[i].b];
            vc = geometry.vertices[geometry.faces[i].c];
        }
        area += areaOfTriangle(va.x, va.y, va.z, vb.x, vb.y, vb.z, vc.x, vc.y, vc.z);
        volume += volumeOfTriangle(va.x, va.y, va.z, vb.x, vb.y, vb.z, vc.x, vc.y, vc.z); // if speed up will be required
    }
    volume = Math.abs(volume);
    result = {
        "area": parseFloat(area).toFixed(2),
        "volume": parseFloat(volume).toFixed(2),
        "weight": parseFloat(getWeight(volume, area)).toFixed(2)
    };
    return result;
}

/**
 *
 * @param p1x
 * @param p1y
 * @param p1z
 * @param p2x
 * @param p2y
 * @param p2z
 * @param p3x
 * @param p3y
 * @param p3z
 * @return {number}
 */
function areaOfTriangle(p1x,  p1y,  p1z,  p2x,  p2y,  p2z,  p3x,  p3y,  p3z){
    ax = p2x - p1x;
    ay = p2y - p1y;
    az = p2z - p1z;
    bx = p3x - p1x;
    by = p3y - p1y;
    bz = p3z - p1z;
    cx = ay*bz - az*by;
    cy = az*bx - ax*bz;
    cz = ax*by - ay*bx;
    return 0.5 * Math.sqrt(cx*cx + cy*cy + cz*cz);
}

/**
 *
 * @param p1x
 * @param p1y
 * @param p1z
 * @param p2x
 * @param p2y
 * @param p2z
 * @param p3x
 * @param p3y
 * @param p3z
 * @return {number}
 */
function volumeOfTriangle( p1x, p1y, p1z, p2x, p2y, p2z,  p3x, p3y, p3z) {
    var v321 = p3x * p2y * p1z;
    var v231 = p2x * p3y * p1z;
    var v312 = p3x * p1y * p2z;
    var v132 = p1x * p3y * p2z;
    var v213 = p2x * p1y * p3z;
    var v123 = p1x * p2y * p3z;
    return (1.0/6.0)*(-v321 + v231 + v312 - v132 - v213 + v123);
}

/**
 *   * Weight of model =
 * = Volume * K1 +Area * K2
 * K1 = 0,00025 = 1,25*0,2/1000   where 1,25 = density of PLA, 0,2 = 20% percent infill for model
 * K2 = 0,0008 = 1,25*0,8*(1-0,2)/1000 where 0,8 is thickness, 1,25 = density of PLA, 0,2 = 20% percent infill
 *
 * @returns {number}
 * @param  {number} volume
 * @param  {number} area
 */
function getWeight(volume, area)
{
    var density = 1.25;
    var infill = 0.2;
    var k1 = density * infill/1000;
    var k2 = density * 0.8 * (1 - infill) / 1000;
    return volume * k1 + area * k2;
}

/**
 *
 * @param source
 * @param target
 * @param callback
 */
function copyFile(source, target, callback) {
  var cbCalled = false;
  var rd = fs.createReadStream(source);
  rd.on("error", function(err) {
    done(err);
  });
  var wr = fs.createWriteStream(target);
  wr.on("error", function(err) {
    done(err);
  });
  wr.on("close", function() {
    done();
  });
  rd.pipe(wr);
  function done(err) {
    if (!cbCalled) {
      callback(err);
      cbCalled = true;
    }
  }    
}

module.exports.measure = measure;
module.exports.convertToStl = convertToStl;
module.exports.analyze = analyze;
module.exports.costing = costing;
module.exports.makeModels = makeModels;
