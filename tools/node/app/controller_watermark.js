var fs = require('fs');
const path = require('path');

var THREE = require('three');
var STLLoader = require('./three-stl-loader')(THREE);
//var STLExporter = require('three-stlexporter');
var watermarkFactory = require('./watermark');
var BinaryStlWriter = require('./stlbinexporter');
var arrayBufferToBuffer = require('arraybuffer-to-buffer');

var window = require('window-or-global');
var mygc = require('./mygc');

var encode = function(req, res, models_root) {
    res.setHeader('Content-Type', 'application/json');
    // clean up parameters
    var pass = "" + req.body.passphrase;
    var mark = "" + req.body.text;
    var filein = path.normalize("" + req.body.filepathin);
    console.log('Watermark request: ' + req.body.filepathin);
    if (!filein.startsWith(models_root)) {
        res.status(500);
        console.log("Denied path of file: " + req.body.filepathin + " Allowed: " + models_root);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
        return;
    }
    var fileout = path.normalize("" + req.body.filepathout);
    if (!fileout.startsWith(models_root)) {
        res.status(500);
        console.log("Denied path of file: " + req.body.filepathout + " Allowed: " + models_root);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
        return;
    }
    // processing
    try { // we have: mark, pass, filein, fileout
        var loader = new STLLoader();
        fs.readFile(filein, function (err, data) {
            if (err) {
                res.status(500);
                console.log(err);
                res.send('{"result":"fail"}');
                res.end();
                mygc();
                return;
            }
            THREE.Cache.enabled = true;
            var ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
            THREE.Cache.add(filein, ab);
            loader.load(filein, function (geometry) { // file loaded
                try {
                    THREE.Cache.remove(filein);

                    // MAIN
                    var geometry1 = watermarkFactory.encode(geometry, mark, pass, 0);
                    watermarkFactory.cleanup();
                    if (geometry1 === false) {
                        res.status(500);
                        console.log('encode fail');
                        res.send('{"result":"fail"}');
                        res.end();
                        mygc();
                        return;
                    }
                    // save result
                    var material = new THREE.MeshNormalMaterial();
                    var mesh = new THREE.Mesh(geometry1, material);
                    var theArrayBuffer = BinaryStlWriter.meshUnbufferedToBinArrayBuffer(mesh); // geometryToDataViewBin
                    fs.writeFile(fileout,  arrayBufferToBuffer(theArrayBuffer),  function(err) {
                            if(err) {
                                res.status(500);
                                console.log('ERR: '+err);
                                res.send('{"result":"fail"}');
                                res.end();
                                mygc();
                            } else {
                                // ok! return URL of file
                                res.status(200);
                                console.log(' File ready: ' + fileout);
                                res.send('{"result":"ok"}');
                                res.end();
                                mygc();
                            }
                        });
                } catch (e) {
                    res.status(500);
                    console.log(e);
                    res.send('{"result":"fail"}');
                    res.end();
                    mygc();
                }
            })
        })
    } catch (e) {
        res.status(500);
        console.log(e);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
    }
}

var decode = function(req, res, models_root) {
    res.setHeader('Content-Type', 'application/json');
    // clean up parameters
    var pass = "" + req.body.passphrase;
    var modeFindAll = "" + req.body.findall;
    modeFindAll = (modeFindAll=="" || modeFindAll=="undefined" || modeFindAll=="0" || modeFindAll=="false" ? false : true);
    console.log(' modeFindAll= ' + modeFindAll);
    var filein = path.normalize("" + req.body.filepath);
    if (!filein.startsWith(models_root)) {
        res.status(500);
        console.log("Denied path of file: " + req.body.filepath +  + " Allowed: " + models_root);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
        return;
    }
    // processing
    try { // we have: pass, filein
        var loader = new STLLoader();
        fs.readFile(filein, function (err, data) {
            if (err) {
                res.status(500);
                console.log(err);
                res.send('{"result":"fail"}');
                res.end();
                mygc();
                return;
            }
            THREE.Cache.enabled = true;
            var ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
            THREE.Cache.add(filein, ab);
            loader.load(filein, function (geometry) { // file loaded
                THREE.Cache.remove(filein);

                // MAIN
                var marksFound = watermarkFactory.decode(geometry, pass, modeFindAll);
                watermarkFactory.cleanup();
                if (marksFound === false) {
                    res.status(500);
                    console.log('analysis fail');
                    res.send('{"result":"fail"}');
                    res.end();
                    mygc();
                    return;
                } else {
                    res.status(200);
                    console.log(' marks found: ' + JSON.stringify(marksFound));
                    var result = {"result":"ok","marks":marksFound}
                    res.send(JSON.stringify(result));
                    res.end();
                    mygc();
                }
            })
        })
    } catch (e) {
        res.status(500);
        console.log(e);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
    }
}

// exports --------------------------------------
var process = function(req, res, models_root) {
    if (req.url == '/jsapi/private/watermark/decode') {
        return decode(req, res, models_root);
    }
    if (req.url == '/jsapi/private/watermark/encode') {
        return encode(req, res, models_root);
    }
    res.status(500);
    console.log("unknown command: "+req.url);
    res.send('{"result":"fail"}');
    res.end();
}

module.exports.encode = encode;
module.exports.decode = decode;
module.exports.process = process;
