var fs = require('fs');
var uuid = require('uuid/v4');    

var multiparty = require('multiparty');

var PNG = require('pngjs').PNG;
var basrelief = require('./basrelief');
var BinaryPlyWriter = require('./plyexporter');
var BinaryStlWriter = require('./stlbinexporter');
var arrayBufferToBuffer = require('arraybuffer-to-buffer');

var perform = function(req, res, fileprefix, urlprefix) {
    // parse file upload
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    var form = new multiparty.Form(); 
    form.autoFiles = true;
    form.maxFilesSize = 200000000;
    form.parse(req, function(err, fields, files) {
        try {
            if (err) {
                res.status(500);
                console.log('Error parsing form: ' + err.stack);
            } else {
                var maxplatesize = fields['maxplatesize'][0];
                var lightmode = fields['lightmode'][0];
                var mintickness = fields['mintickness'][0];
                var maxtickness = fields['maxtickness'][0];
                var colormode = fields['colormode'][0];
                var filepath = files['file1'][0].path;
                console.log('Upload complete: ' + filepath);
                
                // process bas-relief
                fs.createReadStream(filepath)
                  .pipe(new PNG())
                  .on('parsed', function() {
                    var mesh = basrelief.create(this, maxplatesize, mintickness, maxtickness, (lightmode=='convex'), colormode);
                    console.log(' Basrelief created.');
                    var theArrayBuffer, ext;
                    // save file
                    if (colormode == 'mono') {
                      theArrayBuffer = BinaryStlWriter.meshBufferedToBinArrayBuffer(mesh);
                      ext = ".stl";
                    } else {
                      theArrayBuffer = BinaryPlyWriter.meshBufferedToBinArrayBuffer(mesh);  
                      ext = ".ply";
                    }
                    var date = new Date();
                    var datestr = (date.getFullYear()+"-" +(date.getMonth()+1)+"-" +date.getDate()+ "/");
                    var dirprefix = fileprefix + datestr;
                    if (!fs.existsSync(dirprefix)){
                        fs.mkdirSync(dirprefix);
                    }
                    var fn = uuid() + ext;
                    fs.writeFile(dirprefix + fn,  arrayBufferToBuffer(theArrayBuffer),  function(err) {
                        if(err) {
                            console.log('ERR: '+err);
                            res.send(JSON.stringify({success: false, err:'err1'}));
                            res.end();
                        } else {
                            // ok! return URL of file
                            console.log(' File ready: ' + dirprefix + fn);
                            res.send(JSON.stringify({success: true, url:(urlprefix+datestr+fn)}));
                            res.end();
                        }
                    });
                });                
            }
        } catch (e) {
            res.status(500);
            console.log(e);
            res.send(JSON.stringify({success: false, err:'err2'}));
            res.end();
        }
    });    
}

module.exports.perform = perform;