// Written by Paul Kaplan
// mod by Asen Kurin - for buffered geometry 
var THREE = require('three'); 

  var that = {};
  var threevector = new THREE.Vector3();
  var normalMatrixWorld = new THREE.Matrix3();
  var matrixWorld;

  var writeVector = function(dataview, offset, vector, isLittleEndian) {
    offset = writeFloat(dataview, offset, vector.x, isLittleEndian);
    offset = writeFloat(dataview, offset, vector.y, isLittleEndian);
    return writeFloat(dataview, offset, vector.z, isLittleEndian);
  };

  var writeFloat = function(dataview, offset, float, isLittleEndian) {
    dataview.setFloat32(offset, float, isLittleEndian);
    return offset + 4;
  };

  var meshUnbufferedToBinArrayBuffer = function(mesh) {
    var geometry = mesh.geometry;  
    var tris = geometry.faces;
    var verts = geometry.vertices;
    
    var isLittleEndian = true; // STL files assume little endian, see wikipedia page
    
    var bufferSize = 84 + (50 * tris.length);
    var buffer = new ArrayBuffer(bufferSize);
    var dv = new DataView(buffer);
    var offset = 0;

    offset += 80; // Header is empty

    dv.setUint32(offset, tris.length, isLittleEndian);
    offset += 4;

    for(var n = 0; n < tris.length; n++) {
      offset = writeVector(dv, offset, tris[n].normal, isLittleEndian);
      offset = writeVector(dv, offset, verts[tris[n].a], isLittleEndian);
      offset = writeVector(dv, offset, verts[tris[n].b], isLittleEndian);
      offset = writeVector(dv, offset, verts[tris[n].c], isLittleEndian);
      offset += 2; // unused 'attribute byte count' is a Uint16
    }

    return buffer;
  };

  var writeVectorBin = function(dataview, offset, vector, position, isLittleEndian, isNormal) {
    if (isNormal) {
        threevector.set(vector[position], vector[position+1], vector[position+2]).applyMatrix3( normalMatrixWorld ).normalize();
    }  else {// position
        threevector.set(vector[position], vector[position+1], vector[position+2]).applyMatrix4( matrixWorld );
    }
    offset = writeFloat(dataview, offset, threevector.x, isLittleEndian);
    offset = writeFloat(dataview, offset, threevector.y, isLittleEndian);
    return writeFloat(dataview, offset, threevector.z, isLittleEndian);
  };
  
  
  var meshBufferedToBinArrayBuffer = function(mesh) {
    var geometry = mesh.geometry;
    var positions = geometry.getAttribute('position');
    var normals = geometry.getAttribute('normal');
    var trislen = positions.count / 3;
    
    var isLittleEndian = true; // STL files assume little endian, see wikipedia page
    matrixWorld = mesh.matrixWorld;
    normalMatrixWorld.getNormalMatrix( matrixWorld );
    
    var bufferSize = 84 + (50 * trislen);
    var buffer = new ArrayBuffer(bufferSize);
    var dv = new DataView(buffer);
    var offset = 0;

    offset += 80; // Header is empty

    dv.setUint32(offset, trislen, isLittleEndian);
    offset += 4;

    for(var n = 0; n < trislen; n++) {
      offset = writeVectorBin(dv, offset, normals.array,   n*9    , isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9  , isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9+3, isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9+6, isLittleEndian);
      // dv.setUint16(offset,color, isLittleEndian); // color  Uint16
      offset += 2; // unused 'attribute byte count' is a Uint16
    }
    return buffer;
  };

  var meshBufferedToBinArrayBufferColored = function(mesh) {
    var geometry = mesh.geometry;
    var positions = geometry.getAttribute('position');
    var normals = geometry.getAttribute('normal');
    var colors = geometry.getAttribute('color');
    var trislen = positions.count / 3;
    
    var isLittleEndian = true; // STL files assume little endian, see wikipedia page
    matrixWorld = mesh.matrixWorld;
    normalMatrixWorld.getNormalMatrix( matrixWorld );
    
    var bufferSize = 84 + (50 * trislen);
    var buffer = new ArrayBuffer(bufferSize);
    var dv = new DataView(buffer);
    var offset = 0;

    dv.setUint32(offset+0,0x4f4c4f43 /*COLO  0x434F4C4F*/, isLittleEndian);
    dv.setUint8( offset+4, 0x52 /*'R'*/, isLittleEndian);
    dv.setUint8( offset+5, 0x3D /*'='*/, isLittleEndian);
    dv.setUint8( offset+6, 0xff , isLittleEndian); //r
    dv.setUint8( offset+7, 0xff , isLittleEndian); //g
    dv.setUint8( offset+8, 0xff , isLittleEndian); //b
    dv.setUint8( offset+9, 0xff , isLittleEndian); //a
    offset += 80; // Header is empty

    dv.setUint32(offset, trislen, isLittleEndian);
    offset += 4;
    var color;

    for(var n = 0; n < trislen; n++) {
      offset = writeVectorBin(dv, offset, normals.array,   n*9    , isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9  , isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9+3, isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9+6, isLittleEndian);
      color = Math.round(colors.array[n*9+0]* 31) *1 +  
              Math.round(colors.array[n*9+1]* 31) *32       + 
              Math.round(colors.array[n*9+2]* 31) *32*32    ;
      // console.log(color);        
      // color = 0xffff;        
      dv.setUint16(offset,color, isLittleEndian); // color  Uint16
      offset += 2; // unused 'attribute byte count' is a Uint16
    }
    return buffer;
  };

  var meshBufferedToBinArrayBufferST2 = function(mesh, dInstrument) {
    var geometry = mesh.geometry;
    var positions = geometry.getAttribute('position');
    var normals = geometry.getAttribute('normal');
    var colors = geometry.getAttribute('color');
    var trislen = positions.count / 3;
    
    var isLittleEndian = true; // STL files assume little endian, see wikipedia page
    matrixWorld = mesh.matrixWorld;
    normalMatrixWorld.getNormalMatrix( matrixWorld );
    
    var bufferSize = 84 + (50 * trislen);
    var buffer = new ArrayBuffer(bufferSize);
    var dv = new DataView(buffer);
    var offset = 0;

    dv.setUint32(offset+0,0x4f4c4f43 /*COLO  0x434F4C4F*/, isLittleEndian);
    dv.setUint8( offset+4, 0x52 /*'R'*/, isLittleEndian);
    dv.setUint8( offset+5, 0x3D /*'='*/, isLittleEndian);
    dv.setUint8( offset+6, 0xff , isLittleEndian); //r
    dv.setUint8( offset+7, 0xff , isLittleEndian); //g
    dv.setUint8( offset+8, 0xff , isLittleEndian); //b
    dv.setUint8( offset+9, 0xff , isLittleEndian); //a
    dv.setFloat32( offset+10, dInstrument , isLittleEndian);
    offset += 80; // Header is empty

    dv.setUint32(offset, trislen, isLittleEndian);
    offset += 4;
    var color, axis;

    for(var n = 0; n < trislen; n++) {
      offset = writeVectorBin(dv, offset, normals.array,   n*9    , isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9  , isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9+3, isLittleEndian);
      offset = writeVectorBin(dv, offset, positions.array, n*9+6, isLittleEndian);
      axis = colors.array[n*9+3] & 7;
      color = Math.round(colors.array[n*9+0]* 15) *1 +  
              Math.round(colors.array[n*9+1]* 15) *16       + 
              Math.round(colors.array[n*9+2]* 15) *16*16    + 
              axis                                *16*16*16 ;
      // console.log(color);        
      // color = 0xffff;        
      dv.setUint16(offset,color, isLittleEndian); // color  Uint16
      offset += 2; // unused 'attribute byte count' is a Uint16
    }
    return buffer;
  };


   
  
  var save = function(geometry, filename) {
    var dv = geometryToDataViewBin(geometry);
    var blob = new Blob([dv], {type: 'application/octet-binary'});
    
    // FileSaver.js defines `saveAs` for saving files out of the browser
    saveAs(blob, filename);
  };

module.exports.meshUnbufferedToBinArrayBuffer = meshUnbufferedToBinArrayBuffer;
module.exports.meshBufferedToBinArrayBuffer = meshBufferedToBinArrayBuffer;
module.exports.meshBufferedToBinArrayBufferColored = meshBufferedToBinArrayBufferColored;
module.exports.meshBufferedToBinArrayBufferST2 = meshBufferedToBinArrayBufferST2;