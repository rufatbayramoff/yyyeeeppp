"use strict";

/**
 * @property {*[]} millingInstruments
 * @property {CncMillingMachine[]} millingMachines
 * @property {CncStock} stocks
 * @property {CncSettings} settings
 * @property {CncMaterial[]} materials
 * @property {CncPostprocessing[]} postprocessing
 * @constructor
 */
function CncService() {}

/**
 * @property {number} stockSpacers
 * @constructor
 */
function CncSettings() {}

/**
 * @property {number} volumeW
 * @property {number} volumeH
 * @property {number} volumeL
 * @property {number} priceLabourProgrammer
 * @property {number} priceLabourOperator
 * @property {number} placetime
 * @property {number} programmingtime
 * @property {number} priceMachining
 * @property {number} maxRPM
 * @constructor
 */
function CncMillingMachine() {}

/**
 * @property {number} freeform
 * @constructor
 */
function CncStock() {}

/**
 * @property {String} title
 * @constructor
 */
function CncMaterial() {}

/**
 * @property {String} title
 * @property {number} pricePerArea
 * @property {number} pricePerVolume
 * @property {number} pricePerPart
 * @constructor
 */
function CncPostprocessing() {}


/**
 * @property {String} title
 * @property {number} price
 * @property {bool} isPerPart
 * @constructor
 */
class PriceElement
{
    constructor (title, price, isPerPart)
    {
        this.title = title;
        this.price = price;
        this.isPerPart = isPerPart;
    }
}

module.exports.PriceElement = PriceElement;