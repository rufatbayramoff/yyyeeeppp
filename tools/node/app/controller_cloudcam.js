const path = require('path');
var uuid = require('uuid/v4');
var multiparty = require('multiparty');
var cloudcam = require('./cloudcam');
var mv = require('mv');
var _ = require('lodash');

/**
 * Send fail response
 * @param response
 * @param {String|Error|*} [message]
 */
var sendFailResponse = function (response, message)
{
    /**
     * @type {{result: string, [message] : string|undefined}}
     */
    var data = {result : "fail"};

    if (!_.isUndefined(message)) {

        console.log(message);

        if (_.isString(message)) {
            data.message = message;
        }

        else if (_.isError(message)) {
            data.message = message.message;
        }

        else {
            data.message = JSON.stringify(message);
        }
    }

    response.status(500);
    response.send(data);
    response.end();
};

/**
 * Send success response
 * @param response
 * @param {*} [data]
 */
var sendSuccessResponse = function (response, data)
{
    data = _.clone(data);
    data.result = "ok";

    response.status(200);
    response.send(data);
    response.end();
};


/**
 * Upload new model file
 * @param req
 * @param res
 * @param fileprefix
 */
var upload = function(req, res, fileprefix) {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    // parse file upload
    var form = new multiparty.Form();
    form.autoFiles = true;
    form.maxFilesSize = 200000000;
    form.parse(req, function(err, fields, files) {
        try {
            if (err) {
                console.log('Error parsing form: ' + err.stack);
                res.status(500);
                res.end();
            } else {
                // var maxplatesize = fields['maxplatesize'][0];
                var filepath = files['file'][0].path;
                var filename = files['file'][0].originalFilename;
                // console.log(files);
                var extension = path.extname(filename).toLowerCase();
                if (extension === ".step" || extension === ".stp")
                {
                    extension = ".step";
                } else if (extension === ".iges" || extension === ".igs")
                {
                    extension = ".iges";
                } else  if (extension === ".stl" )
                {
                    extension = ".stl";
                } else {
                        console.log('file type wrong: ' + filename);
                        res.status(500);
                        res.end();
                }
                // TODO iges by filename???
                var newfilename = uuid() + extension;
                console.log('Upload complete: ' + filepath + ", copy to " + fileprefix + newfilename);
                mv(filepath, fileprefix + newfilename, function(err) {
                    if (err) {
                        console.log('filecopy err: ' + err);
                        res.status(500);
                        res.end();
                    } else {
                        console.log('File copied to ' + fileprefix + newfilename);
                        res.send(JSON.stringify({success: true, filepath: fileprefix + newfilename}));
                        res.end();
                    }
                });
            }
        } catch (e) {
            res.status(500);
            console.log(e);
            res.send(JSON.stringify({success: false, err: e}));
            res.end();
        }
    });
};

/**
 *
 * @param req
 * @param res
 * @param fileprefix
 */
var recognize = function(req, res, fileprefix) {
    // clean up parameters
    var filein = path.normalize("" + req.body.filepath);
    if (!filein.startsWith(fileprefix)) {
        sendFailResponse(res, "Denied path of file: " + req.body.filepath + " Allowed: " + fileprefix);
        return;
    }
    // processing
    var fileout1 = uuid() + ".stl";
    var fileout2 = uuid() + ".stl";

    try {
        cloudcam.convertToStl(filein, fileprefix + fileout1, 0.4, function(err1) {

            cloudcam.convertToStl(filein, fileprefix + fileout2, 0.01, function(err2) {

                if (err1 || err2) {
                    sendFailResponse(res, 'Conversion fail');
                }
                else {
                    console.log(' Files ready: ' + fileout1 + ", " + fileout2);
                    sendSuccessResponse(res, {rough : fileprefix+fileout1, fine : fileprefix+fileout2});
                }
            });
        });
    }
    catch (e) {
        sendFailResponse(res, e);
    }
};

/**
 *
 * @param req
 * @param res
 * @param fileprefix
 */
var analyze = function(req, res, fileprefix) {
    // clean up parameters
    var roughfile = path.normalize("" + req.body.roughfile);
    if (!roughfile.startsWith(fileprefix)) {
        res.status(500);
        console.log("Denied path of file: " + req.body.roughfile + " Allowed: " + fileprefix);
        res.send('{"result":"fail"}');
        res.end();
        return;
    }
    // processing
    try {
        console.log('Analyzing ' + roughfile);
        cloudcam.analyze(roughfile, function(result) {
            if (result) {
                res.status(200);
                console.log(' analyzed');
                res.send(JSON.stringify({result:"ok",analyzeResult:result}));
                res.end();
            } else {
                res.status(500);
                console.log(result);
                res.send('{"result":"fail"}');
                res.end();
            }
        });
    } catch (e) {
        res.status(500);
        console.log(e);
        res.send('{"result":"fail"}');
        res.end();
    }
};

/**
 *
 * @param request
 * @param response
 * @param fileprefix
 * @param urlprefix
 * @param servicePrefix
 */
var costing = function(request, response, fileprefix, urlprefix, servicePrefix)
{
    var servicefile = path.normalize("" + request.body.service);

    if (!servicefile.startsWith(servicePrefix)) {
        var message =  "Denied path of file: " + request.body.service + " Allowed: " + servicePrefix;
        sendFailResponse(response, message);
        return;
    }

    try {
        console.log("preset:", request.body.preset, "analyze:", request.body.analyze);

        var preset = JSON.parse(request.body.preset);
        var analyze = JSON.parse(request.body.analyze);

        cloudcam.costing(servicefile, analyze, preset, function(result, error) {
            if (result) {
                console.log('Costing finished');
                sendSuccessResponse(response, {costingResult:result});
            }
            else {
                sendFailResponse(response, error);
            }
        });
    }
    catch (e) {
        sendFailResponse(response, e);
    }
};

/**
 *
 * @param req
 * @param res
 * @param fileprefix
 */
var modelling = function(req, res, fileprefix) {
    // clean up parameters
    var finefile = path.normalize("" + req.body.finefile);
    if (!finefile.startsWith(fileprefix)) {
        res.status(500);
        console.log("Denied path of file: " + req.body.finefile + " Allowed: " + fileprefix);
        res.send('{"result":"fail"}');
        res.end();
        return;
    }
    // processing
    var fileoutColorsGEOM = uuid() + ".stl";
    var fileoutSpec = uuid() + ".spec";
    try {
        console.log("finefile:");
        console.log(finefile);
        console.log("preset:");
        console.log(req.body.preset);
        console.log("analyze:");
        console.log(req.body.analyze);
        console.log("costing:");
        console.log(req.body.costing);
        var preset = JSON.parse(req.body.preset);
        var analyze = JSON.parse(req.body.analyze);
        var costing = JSON.parse(req.body.costing);
        //console.log('costing ' + finefile);
        cloudcam.makeModels(finefile, fileprefix, fileoutColorsGEOM, fileoutSpec, analyze, preset, costing, function(result) {
            if (result) {
                res.status(200);
                console.log(' modelling finished');
                res.send(JSON.stringify(result));
                res.end();
            } else {
                res.status(500);
                console.log(result);
                res.send('{"result":"fail"}');
                res.end();
            }
        });
    } catch (e) {
        res.status(500);
        console.log(e);
        res.send('{"result":"fail"}');
        res.end();
    }
};


// exports --------------------------------------
/**
 *
 * @param req
 * @param res
 * @param fileprefix
 * @param urlprefix
 * @param servicePrefix
 */
var process = function(req, res, fileprefix, urlprefix, servicePrefix) {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    if (req.url === '/jsapi/private/cloudcam/processing/recognize') {
        return recognize(req, res, fileprefix, urlprefix);
    }
    if (req.url === '/jsapi/private/cloudcam/processing/analyze') {
        return analyze(req, res, fileprefix, urlprefix);
    }
    if (req.url === '/jsapi/private/cloudcam/processing/costing') {
        return costing(req, res, fileprefix, urlprefix, servicePrefix);
    }
    if (req.url === '/jsapi/private/cloudcam/processing/modelling') {
        return modelling(req, res, fileprefix, urlprefix);
    }

    sendFailResponse(res, "Unknown command: "+req.url);
};

module.exports.process = process;
module.exports.upload = upload;
