var crc = require('./crc')
var THREE = require('three');
var Blowfish = require('./blowfish');

var lastGeometry;
var vertexToFace, edgesToFace, prohibitedPoints, vertextPathOfMark, allMarksFound;
var markPrefix, markLen, markLenOfPlan, stackDeep, markString, decodedString, stronger;
var crypt;

var maxPossibleLengthOfMark = 200;
var markPrefixZero = "ABCD";

// pseudorandoms - for debug only
/*
var m_w = 123456789;
var m_z = 987654321;
var mask = 0xffffffff;
// Takes any integer
function seed(i) {
    m_w = i;
}
// Returns number between 0 (inclusive) and 1.0 (exclusive),
// just like Math.random().
function random()
{
    m_z = (36969 * (m_z & 65535) + (m_z >> 16)) & mask;
    m_w = (18000 * (m_w & 65535) + (m_w >> 16)) & mask;
    var result = ((m_z << 16) + m_w) & mask;
    result /= 4294967296;
    return result + 0.5;
}*/


//// common functions --------------------------------
function int32tostring4(i) {
    return String.fromCharCode((i & 0xff000000) >>> 24) 
          +String.fromCharCode((i & 0xff0000)   >>> 16) 
          +String.fromCharCode((i & 0xff00)     >>> 8) 
          +String.fromCharCode((i & 0xff)       >>> 0)
}
function distanceVector( v1, v2 )
{
    var dx = v1.x - v2.x;
    var dy = v1.y - v2.y;
    var dz = v1.z - v2.z;
    return Math.sqrt( dx * dx + dy * dy + dz * dz );
}


// setup and parse -----------------------------------
function setupMesh(geometry) {
    geometry.dynamic = true;
    geometry.center();
    geometry.computeBoundingSphere();
    geometry.computeBoundingBox();
    //var sphere =  geometry.boundingSphere;
    //var scale = 1 / sphere.radius;
    //var camDistance =  sphere.radius * 2.5;
    //var d = sphere.radius   ;
    // geometry.mergeVerticesRelative(sphere.radius / 100000);
    geometry.mergeVertices();
    console.log("info: vertices="+(geometry.vertices.length)+" faces="+(geometry.faces.length));
    // geometry.computeFaceNormals();
    // geometry.computeVertexNormals( true );
    lastGeometry = geometry;
    prohibitedPoints = {};
    crossReference(geometry);
    return checkModelMeshVivid();
}
function crossReference(g) {
    vertexToFace = [];
    for (var fx = 0; fx < g.vertices.length; fx++) {
        vertexToFace[fx] = new Array();
    }
    for (var fx = 0; fx < g.faces.length; fx++) {
        var f = g.faces[fx];
        var ax = f.a;
        var bx = f.b;
        var cx = f.c;
        vertexToFace[ax].push(fx);
        vertexToFace[bx].push(fx);
        vertexToFace[cx].push(fx);
    }
}
function checkModelMeshVivid(){
    var vivid = true;
    edgesToFace = {};
    for (var fx = 0; fx < lastGeometry.faces.length; fx++) {
        var f = lastGeometry.faces[fx];
        var ax = f.a;
        var bx = f.b;
        var cx = f.c;
        addEdge(ax,bx,fx);
        addEdge(ax,cx,fx);
        addEdge(bx,cx,fx);
    }
    var k = 0;
    for(var key in edgesToFace) {
        var faces = edgesToFace[key];
        if (faces.length!=2) {
            console.log("WARNING: Edge " + key + " have " + faces.length + " faces");
            //if (faces.length>2)
            //    vivid = false;
            points = key.split("_");
            prohibitedPoints[points[0]]=1;
            prohibitedPoints[points[1]]=1;
        }
    }    
    return vivid;    
}

function preparePasses(pass) {
    var pass_crc = crc(pass);
    markPrefix = "";
    for (var i = 0; i < markPrefixZero.length; i++) {
        var c = markPrefixZero.charCodeAt(i) ^ (pass_crc & 255);
        if (c==0) c = 1;
        markPrefix += String.fromCharCode(c); 
        pass_crc = pass_crc >> 8;                            
    }
    crypt = new Blowfish(pass);
    return true; // TODO (markString.length < maxPossibleLengthOfMark);
}


/// cache operation ----------------------------------
function getEdgeKey(ax,bx) {
    if (bx>ax) {var t=ax; ax=bx; bx=t;}
    return ax+"_"+bx;
}
function addEdge(ax,bx,fx) {
    var edgeKey = getEdgeKey(ax,bx);
    addToEdgesToFace(edgeKey,fx)
}
function addToEdgesToFace(edgeKey,faceid) {
    if (!(edgeKey in edgesToFace)) 
         edgesToFace[edgeKey] = new Array();
    edgesToFace[edgeKey].push(faceid);
}
function deleteFromEdgesToFace(edgeKey,faceid) {
    var faceArr = edgesToFace[edgeKey];
    for (var fx = 0; fx < faceArr.length; fx++) 
        if (faceArr[fx]===faceid) {
             edgesToFace[edgeKey].splice(fx, 1);// = faceArr.splice(fx, 1)
             return true;            
    }
    console.log("ERR: trying to delete edge="+edgeKey+" from face="+faceid+" but not found!");
    return false;
}
function addToVertextToFace(vertexid,faceid) {
    if (vertexid > vertexToFace.length) { 
        console.log('cant add more than one vertex');
        return false;
    }
    if (vertexid === vertexToFace.length) { 
         vertexToFace[vertexid] = new Array();
    }
    vertexToFace[vertexid].push(faceid);
}
function deleteFromVertexToFace(vertexid,faceid) {
    var faceArr = vertexToFace[vertexid];
    for (var fx = 0; fx < faceArr.length; fx++) 
        if (faceArr[fx]===faceid) {
             vertexToFace[vertexid].splice(fx, 1);// = faceArr.splice(fx, 1)
             return true;            
    }
    console.log("ERR: trying to delete vertex="+vertexid+" from face="+faceid+" but not found!");
    return false;
}
function findVertexInFaceNotEqual(faceindex,vertext1index,vertext2index) {
    var f = lastGeometry.faces[faceindex];
    if (f.a !== vertext1index && f.a !== vertext2index) return f.a;
    if (f.b !== vertext1index && f.b !== vertext2index) return f.b;
    return f.c;
}
function testPointUsedInPath(point1) {
    for (var fx=0; fx<vertextPathOfMark.length; fx++) 
        if (vertextPathOfMark[fx] === point1) 
            return true;
    return false;
}
function findPoint4(point1,point2,pointOpposite) {
    var edgeKey2 = getEdgeKey(point1,point2);
    if (!(edgeKey2 in edgesToFace)) {
        console.log('edge '+edgeKey2+' not found'); 
        return -1;
    }
    if (edgesToFace[edgeKey2].length != 2) {
        console.log('edge '+edgeKey2+' non dual: 2!=' + edgesToFace[edgeKey2].length); 
        return -1;
    }
    var possiblePoint1 = findVertexInFaceNotEqual(edgesToFace[edgeKey2][0],point1,point2);
    var possiblePoint2 = findVertexInFaceNotEqual(edgesToFace[edgeKey2][1],point1,point2);
    return (pointOpposite === possiblePoint1 ? possiblePoint2 : possiblePoint1);
} 
/*function findVertexClosedToPoints(point1,point2,pointOpposite) {
    var edgeKey2 = getEdgeKey(point1,point2);
    var faces = edgesToFace[edgeKey2];
    for (var fx=0; fx<faces.length; fx++) {
        var f = lastGeometry.faces[faces[fx]];
        if (f.a !== point1 && f.a !== point2 && f.a !== pointOpposite) return f.a;
        if (f.b !== point1 && f.b !== point2 && f.b !== pointOpposite) return f.b;
        if (f.c !== point1 && f.c !== point2 && f.c !== pointOpposite) return f.c;
    } 
    return 0;
}*/


////////////////////////// MAIN FUNCTIONS ----------------------
function autosign() {
    // go forward
    if (markString.trim()==="") return false;
    markString += int32tostring4(crc(markString)); // control sum                    
    markString += String.fromCharCode(13);             // end mark       
    markString = crypt.encrypt(markString);            // encrypt
    if (markString.length > maxPossibleLengthOfMark) {return false;};
    // markString = crypt.base64Encode(markString);
    // variable-length of watermarks
    markLen = markPrefixZero.length + markString.length+1; // +1 becouse of we use edges not faces                           
    var countWatermarks = 0;
    var amax = lastGeometry.faces.length;
    var aMarksCount = (amax / markLen) / 50; // 2% of mesh area will be watermarked
    if (aMarksCount < 3) aMarksCount = 3;
    if (aMarksCount >100) aMarksCount = 100;
    for (var i = 0; i < aMarksCount * 50; i++) {
        var f =  /* Math.floor(random() * amax);*/ Math.floor(Math.random() * amax);
        var startFace = lastGeometry.faces[f];
        if (hilightPoints(startFace)) {
            tmp1 = startFace.a;
            tmp2 = startFace.b;
            tmp3 = startFace.c;
            // applyTheMark();
            if (!applyTheMark()) {
                console.log("Err while applying. Marks applied: " + countWatermarks);
                // return (countWatermarks>0);
				continue;
            }
            countWatermarks++;
            // console.log("Watermark placed: " + countWatermarks + ", face " + f + " from " + amax);
            if (countWatermarks>=aMarksCount) break; // max 100
        } else {
	  console.log("hilite fail at startfacenum:",f);
	}
    } 
    console.log("Watermark placed: " + countWatermarks);
    return (countWatermarks>0);
}

function autocheck(findAll){
    var amax = lastGeometry.faces.length;
    var prefixLen = markPrefix.length;
    var countGoodMarks = 0;
    allMarksFound = {};
    var errorsOccurs = false;
    for (var i = amax-1; i>=0; i--) {
        if (i % 10000 === 0)
            console.log("tested " + Math.floor((amax-i)*100.0/amax) + "%");
        var startFace = lastGeometry.faces[i];
        for (var j=0; j<6; j++) 
            if (hilightPointsReadInner(startFace, j, prefixLen) === markPrefix) {
                // start signature found. Try to decode
                for (markLen=prefixLen+8; markLen<maxPossibleLengthOfMark+30; markLen+=8) {
                    if (markLen>=maxPossibleLengthOfMark+10) {
                        console.log('Prefix found, but there are no end marker. Is the mark broken?'); 
                        errorsOccurs = true;
                        break;
                    };                        
                    var foundMark = hilightPointsReadInner(startFace, j, markLen);
                    if (hilightPointsReadInner==="") break; // wrong length, try find new start place
                    foundMark = foundMark.substr(prefixLen,9999);
                    // foundMark = crypt.base64Decode(foundMark);
                    try {
                      foundMark = crypt.decrypt(foundMark);
                      foundMark = crypt.trimZeros(foundMark);
                      if (foundMark.charCodeAt(foundMark.length-1)===13) {
                        // test control sum
                        var crc_str = foundMark.substring(foundMark.length-5,foundMark.length-1);
                        var foundMark = foundMark.substring(0,foundMark.length-5);
                        if (int32tostring4(crc(foundMark))==crc_str) {
                            //console.log(foundMark);
                            //console.log("found at face: " + i + " direction " + j);
                            countGoodMarks++;
                            if (!(foundMark in allMarksFound)) {
                                var txt = "Found mark: \n\n" + foundMark + 
                                    "\n\nDo you want to continue scanning to found other marks?";
                                allMarksFound[foundMark] = 1; 
                                if (!findAll) {
                                    return true;
                                }
                            } else {
                                cnt = allMarksFound[foundMark];
                                allMarksFound[foundMark] = cnt + 1; 
                            }
                        } else {
                            console.log("Broken CRC of mark: " + foundMark);
                        }
                        //alert(foundMark);
                        //return true;  
                        break;
                      }
                    } catch (err) {
                        errorsOccurs = true;
                        // try more length to decode
                    }
                }
        }
    }
    if (countGoodMarks===0) {
        console.log("Nothing found. If you have used password while placing watermark, please enter in and try once more.");
        return !errorsOccurs;
    } else {
        console.log("Found " + countGoodMarks + " marks.");
        return true;
    }
}

// path processing --------------------------------------
function hilightPoints(startFaceIndex) {
    // cleanup
    if (vertexToFace.length == 0) return false;
    kglobal = 0;
    vertextPathOfMark = [];
    vertextPathOfMark.push(startFaceIndex.a);
    vertextPathOfMark.push(startFaceIndex.b);
    vertextPathOfMark.push(startFaceIndex.c);
    currentIndexOnMark = 0;
    markLenOfPlan = markLen;
    stackDeep = 0;
    return planOneStepOfMark2(startFaceIndex.a,startFaceIndex.b);    
}
function planOneStepOfMark2(centerpoint,backpoint) {
    // Треугольник: 
    // - точка голова змеи (последняя из vertextPathOfMark)
    // - centerpoint (вокруг нее крутимся)
    // - backpoint (от нее отталкиваемся)
    // точка с кото рой мы начинали вращаться - ,usedfan
    var headpoint; // = vertextPathOfMark[vertextPathOfMark.length - 1];
    var nextpoint;
    // список точек построение с которых приводит в тупик
    var deadendpoints = {};
    // копируем стартовое значение центральных точек
    var replycenterpoints = [];
    var replybackpoints = [];
    replycenterpoints.push(-1); 
    replybackpoints.push(-1); 
    
    var iternum = 0;
    while (vertextPathOfMark.length < markLenOfPlan) {
        iternum++;
        //if (iternum == 400) {
            //console.log('Prefix found, but there are no end marker. Is the mark broken?'); 
            // return false;
        if (iternum > 1000) {
            console.log('- ERROR! Too long path. Maybe mesh has the Mobius strip?'); 
            return false;
        }
        headpoint = vertextPathOfMark[vertextPathOfMark.length - 1];
        preheadpoint = vertextPathOfMark[vertextPathOfMark.length - 2];
        //находим треугольник из fan вокруг centerpoint отталкиваясь от backpoint
        //находим точку next в треугольнике напротив backpoint
        nextpoint = findPoint4(centerpoint, headpoint, backpoint);
        if (nextpoint < 0) { 
		// console.log('- cant find point4');
		return false;
	};
        var nextIsUsedBefore = testPointUsedInPath(nextpoint);
        // Если эта точка уже использвалась в другом ватермарке
        if (nextpoint in prohibitedPoints) {
            // console.log('- prohibited zone:',nextpoint); 
            return false; // we already use this point in another marks
        }            
        //(если она использовалась) и она последняя 
        if (nextpoint === preheadpoint) {
            //    - шаг на уровень назад:
            //      убираем headpoint из головы змеи
            vertextPathOfMark.pop();
            //      запрещаем использовать точку headpoint до тех пор пока не добавим хоть одну точку в змею              
            deadendpoints[headpoint] = 1;
            backpoint = replybackpoints.pop();
            centerpoint = replycenterpoints.pop();
            // если мы не можем сделать шаг назад - остался только начальный треугольник - выходим
            if (centerpoint < 0) {
                // console.log('- dead start'); 
                return false;
            }                
            continue;
        }
        //если использовалась или она запрещена к использованию
        if (nextIsUsedBefore || nextpoint in deadendpoints) {
            //    - двигаемся налево:
            //      ищем треугольник соседствующий с гранью next-head отталкиваясь от centerpoint
            //var a =  findPoint4(nextpoint, headpoint, centerpoint);
            //    centerpoint = next, backpoint = centerpoint, крутимся
            backpoint = centerpoint;
            centerpoint = nextpoint;
            continue;
        }
        //инчае - если она не использовалась 
        //    - делаем шаг:
        //      добавляем next в тело
        vertextPathOfMark.push(nextpoint);
        // запоминаем на случай возврата назад
        replycenterpoints.push(centerpoint); 
        replybackpoints.push(backpoint); 
        //      centerpoint = centerpoint, backpoint = headpoint
        backpoint = headpoint;
        //  снимаем все запреты?
        //continue;
    };
    return true;
}

function hilightPointsReadInner(startFace, markScanMode, tryMarkLen) {
    var numPoints =  (tryMarkLen*2+1) ; // we want fo
    kglobal = 0;
    vertextPathOfMark = [];
    var point1, point2, point1a, point0, point2a;
    if (markScanMode === 0 /*'ba'*/) {
        point1 = startFace.a; point2= startFace.c; point1a = startFace.b;
    } else if (markScanMode === 1 /*'bc'*/) {
        point1 = startFace.c; point2= startFace.a; point1a = startFace.b;
    } else if (markScanMode === 2 /*'ab'*/) {
        point1 = startFace.b; point2= startFace.c; point1a = startFace.a;
    } else if (markScanMode === 3 /*'ac'*/) {
        point1 = startFace.c; point2= startFace.b; point1a = startFace.a;
    } else if (markScanMode === 4 /*'ca'*/) {
        point1 = startFace.a; point2= startFace.b; point1a = startFace.c;
    } else if (markScanMode === 5 /*'cb'*/) {
        point1 = startFace.b; point2= startFace.a; point1a = startFace.c;
    }
    point2a = findPoint4(point1,point2,point1a);
    if (point2a===0) return "";
    point0 = findPoint4(point1,point2a,point2);
    if (point0===0) return "";
    
    //if (point0 === tmp1 && point1a === tmp2 && point2a == tmp3) {
    //    console.log('ge bugg');
    //}
    
    vertextPathOfMark.push(point0);
    vertextPathOfMark.push(point1);
    vertextPathOfMark.push(point1a);
    vertextPathOfMark.push(point2);
    vertextPathOfMark.push(point2a);
    currentIndexOnMark = 0;
    markLenOfPlan = tryMarkLen * 2 + 1;
    stackDeep = 0;
    //var lastMarkPathIsGood = planOneStepOfMark(point1,point0,{});
    var lastMarkPathIsGood = planOneStepOfMark2(point0,point1);
    if (lastMarkPathIsGood) {
        decodedString =  '';
        for (var fx=0; fx<tryMarkLen; fx++) {// TODO -1 ?
            p1 = lastGeometry.vertices[vertextPathOfMark[fx*2]];
            p2 = lastGeometry.vertices[vertextPathOfMark[fx*2+1]];
            p3 = lastGeometry.vertices[vertextPathOfMark[fx*2+2]];
            var r1 = distanceVector( p1, p2 );
            var r2 = distanceVector( p2, p3 );
            addfloatToDecodedString(r1,r2);
        }
        return decodedString;
    };
    return "";
}


// geometry altering ----------------------------------------
function  applyTheMark() {
    // set up
    catchTriangles = false;
    // cleanup
    // var geom = new THREE.Geometry();
    // theObjects[i].mesh.geometry.vertices = theObjects[i].geo.vertices;  
    if (!lastGeometry.vertices) {
        console.log('impassible!');
        return false;
    };
    var numpp = 0;
    // process points
    for (var fx = 1; fx < vertextPathOfMark.length; fx++) {
        // console.log("step: " +  fx);
        var a = vertextPathOfMark[fx-1];
        var b = vertextPathOfMark[fx];
        var edgekey = getEdgeKey(a,b);
        var faces = edgesToFace[edgekey]; 
        
        var vertices = lastGeometry.vertices;
        var codedValue = getSignatureFloat(fx-1);// 0.5; // Math.random(); // 0 - 1
        // TODO: has | has no defects
        /*
        var x = vertices[a].x + (vertices[b].x -  vertices[a].x ) * codedValue ; // x
        var y = vertices[a].y + (vertices[b].y -  vertices[a].y ) * codedValue ; // x
        var z = vertices[a].z + (vertices[b].z -  vertices[a].z ) * codedValue ; // x
        var v = new THREE.Vector3();
        v.x = x; v.y = y; v.z = z;
        */
        var v;
        if (stronger == 0) { 
            v = vertices[a].clone().addScaledVector(vertices[b].clone().sub(vertices[a]), codedValue);
        } else {
            var direction = lastGeometry.faces[faces[0]].normal.clone().multiplyScalar(2/5)
                .add(lastGeometry.faces[faces[0]].normal.clone().multiplyScalar(3/5)).normalize();
            var ab0 = lastGeometry.vertices[lastGeometry.faces[faces[0]].a].distanceTo(lastGeometry.vertices[lastGeometry.faces[faces[0]].b]);
            var ac0 = lastGeometry.vertices[lastGeometry.faces[faces[0]].a].distanceTo(lastGeometry.vertices[lastGeometry.faces[faces[0]].c]);
            var bc0 = lastGeometry.vertices[lastGeometry.faces[faces[0]].b].distanceTo(lastGeometry.vertices[lastGeometry.faces[faces[0]].c]);
            var ab1 = lastGeometry.vertices[lastGeometry.faces[faces[1]].a].distanceTo(lastGeometry.vertices[lastGeometry.faces[faces[1]].b]);
            var ac1 = lastGeometry.vertices[lastGeometry.faces[faces[1]].a].distanceTo(lastGeometry.vertices[lastGeometry.faces[faces[1]].c]);
            var bc1 = lastGeometry.vertices[lastGeometry.faces[faces[1]].b].distanceTo(lastGeometry.vertices[lastGeometry.faces[faces[1]].c]);
            var r = Math.min(ab0,ac0,bc0,ab1,ac1,bc1);
            // TODO: randomize r
            r = r * stronger;
            r = r * 0.5 + Math.random() * r * 0.5; // 0.05 till 0.1 % of min neighbour edge
            var ab = lastGeometry.vertices[a].distanceTo(lastGeometry.vertices[b]);
            var a1 = ab * codedValue;
            var a2 = ab * (1-codedValue);
            var newCodedValue = Math.sqrt(a1*a1-r*r) / (Math.sqrt(a1*a1-r*r) + Math.sqrt(a2*a2-r*r) )
            v = vertices[a].clone().addScaledVector(vertices[b].clone().sub(vertices[a]), newCodedValue).addScaledVector(direction, r);
        };
        if (!brokeTrinanle(faces,a,b,v)) {
            console.log('cant broke tri');
            return false;
        }
    }
    return true;
}
function brokeTrinanle(faces,a,b,v) {
    // БЕЗ ЦИКЛА! ПОТОМУ ЧТО ТОЧКА РАЗБИВАЮЩАЯ РЕБРО ДОЛЖНА БЫТ ОДНА, И ЭТО РЕБРО ДОЛЖНО КОРРЕКТНО ОТРАЖАТСЬЯ В СПИСКЕ СОСЕДЕЙ 
    // TODO или оставим на коллапсер?
    if (faces.length !== 2) { 
        console.log('faces length not equal 2 =  ' + faces.length);
        return false;
    }
    // find faces near edge a-b
    var facenum1 = faces[0];
    var face1 = lastGeometry.faces[facenum1];
    var facenum2 = faces[1];
    var face2 = lastGeometry.faces[facenum2];
    var c1 = findVertexInFaceNotEqual(facenum1,a,b);
    var c2 = findVertexInFaceNotEqual(facenum2,a,b);
    // broke edge a-b - добавить точку разбиения
    lastGeometry.vertices.push(v);
    var ab =  lastGeometry.vertices.length - 1;
    prohibitedPoints[a]=1;
    prohibitedPoints[b]=1;
    prohibitedPoints[c1]=1;
    prohibitedPoints[c2]=1;
    prohibitedPoints[ab]=1;
    // move triangles - перетащить два прилежащих треугольника к первой точке из разбиваемог отрезка
    //mesh.geometry.faces[facenum1].a = a;                
    //mesh.geometry.faces[facenum1].b = ab;                
    //mesh.geometry.faces[facenum1].c = c1;  
    changeOneVertex(facenum1,b,ab);
    //mesh.geometry.faces[facenum2].a = a;                
    //mesh.geometry.faces[facenum2].b = ab;                
    //mesh.geometry.faces[facenum2].c = c2;                
    changeOneVertex(facenum2,b,ab);
    // add new triangles - скопировать два перетащены треугольника 
    lastGeometry.faces.push(new THREE.Face3(
           lastGeometry.faces[facenum1].a,
           lastGeometry.faces[facenum1].b,
           lastGeometry.faces[facenum1].c,
           face1.normal));
    var newFaceNum1 =  lastGeometry.faces.length - 1;
    changeOneVertex(newFaceNum1,ab,b); // и перетащить их ко второй точке из разбиваемого отрезка
    changeOneVertex(newFaceNum1,a,ab);
    lastGeometry.faces.push(new THREE.Face3(
           lastGeometry.faces[facenum2].a,
           lastGeometry.faces[facenum2].b,
           lastGeometry.faces[facenum2].c,
           face2.normal));
    var newFaceNum2 =  lastGeometry.faces.length - 1;
    changeOneVertex(newFaceNum2,ab,b);
    changeOneVertex(newFaceNum2,a,ab);
    // add vertices to face for new point (ab) and correct vetices to face to old points(a b c)
    // скорректировать массив vertextoface
    deleteFromVertexToFace(b,facenum1);
    deleteFromVertexToFace(b,facenum2);
    addToVertextToFace(b,newFaceNum1);
    addToVertextToFace(b,newFaceNum2);
    addToVertextToFace(c1,newFaceNum1);
    addToVertextToFace(c2,newFaceNum2);
    addToVertextToFace(ab,facenum1);
    addToVertextToFace(ab,facenum2);
    addToVertextToFace(ab,newFaceNum1);
    addToVertextToFace(ab,newFaceNum2);
    // поправить идентификаторы faces в всех ребрах ранее граничивших с данным face (ac->old bc->new)
    // скорректировать массив edgestoface
    // ab - there is no such edge more
    deleteFromEdgesToFace(getEdgeKey(a,b), facenum1);
    deleteFromEdgesToFace(getEdgeKey(a,b), facenum2);
    // b-c1, b-c2
    deleteFromEdgesToFace(getEdgeKey(b,c1), facenum1);
    addToEdgesToFace(getEdgeKey(b,c1), newFaceNum1);
    deleteFromEdgesToFace(getEdgeKey(b,c2), facenum2);
    addToEdgesToFace(getEdgeKey(b,c2), newFaceNum2);
    // + ab-c1
    addToEdgesToFace(getEdgeKey(ab,c1), facenum1);
    addToEdgesToFace(getEdgeKey(ab,c1), newFaceNum1);
    // + ab-c2
    addToEdgesToFace(getEdgeKey(ab,c2), facenum2);
    addToEdgesToFace(getEdgeKey(ab,c2), newFaceNum2);
    // + ab-b
    addToEdgesToFace(getEdgeKey(ab,b), newFaceNum1);
    addToEdgesToFace(getEdgeKey(ab,b), newFaceNum2);
    // + a-ab
    addToEdgesToFace(getEdgeKey(a,ab), facenum1);
    addToEdgesToFace(getEdgeKey(a,ab), facenum2);
    return true;
};
function changeOneVertex(facenum,oldvetex,newvertex) {
    var face = lastGeometry.faces[facenum];
    if (face.a===oldvetex) { lastGeometry.faces[facenum].a = newvertex; return true;}
    if (face.b===oldvetex) { lastGeometry.faces[facenum].b = newvertex; return true;}
    if (face.c===oldvetex) { lastGeometry.faces[facenum].c = newvertex; return true;}
    console.log("WARN: changeOneVertex in face="+facenum+" not found vertex="+oldvetex);
    return false;
}



/// signature processing ----------------------------
function getSignatureFloat(pointNum) {
    // var markString = "Treatstock";
    // TODO: start bits
    // TODO: checksum
    var totalStr = markPrefix + markString;
    var c = 0;
    if (pointNum<totalStr.length) {
        c = totalStr.charCodeAt(pointNum) ; // TODO: bit operations
        // TODO: swap bits
    };
    var f = c * 1.0 / 255;              // TODO
    var minval = 0.1;
    var maxval = 0.9;
    return minval + (maxval - minval) * f;
    // return 0.25;
}
function addfloatToDecodedString(r1,r2) {
    var r = r1 + r2;
    var minval = 0.1;
    var maxval = 0.9;
    var f = ( r1 / r - minval ) / (maxval - minval);
    var c = Math.round( 255.0 * f );
    //if (c > 0) {
        decodedString += String.fromCharCode(c);   
    //}
}

var cleanup = function cleanup(){
    lastGeometry = null;
    vertexToFace = null;
    edgesToFace= null;
    prohibitedPoints= null;
    vertextPathOfMark= null;
    allMarksFound= null;
    markPrefix= null;
    markLen= null;
    markLenOfPlan= null;
    stackDeep= null;
    markString= null;
    decodedString= null;
}

function addEnoughFaces(facesCount) {
    if (lastGeometry.faces.length < facesCount) {
        console.log("growing up faces: " + lastGeometry.faces.length);
     while (lastGeometry.faces.length < facesCount) {
        var maxl = lastGeometry.faces.length;
        for (rndFace = 0; rndFace < maxl; rndFace++) {
            // var rndFace =  Math.floor( Math.random() * (lastGeometry.faces.length - 1) );
            var face = lastGeometry.faces[rndFace];
            var a,b;
            // find longest side
            var ab = lastGeometry.vertices[face.a].distanceTo( lastGeometry.vertices[face.b] );
            var bc = lastGeometry.vertices[face.b].distanceTo( lastGeometry.vertices[face.c] );
            var ca = lastGeometry.vertices[face.c].distanceTo( lastGeometry.vertices[face.a] );
            if (ab > bc && ab > ca) {
                a = face.a; b = face.b;
            } else if (bc > ab && bc > ca) {
                a = face.b; b = face.c;
            } else {
                a = face.c; b = face.a;
            }
            var edgekey = getEdgeKey(a,b);
            var faces = edgesToFace[edgekey]; 
            var vertices = lastGeometry.vertices;
            var codedValue = 0.3 + Math.random()*0.4;
            var x = vertices[a].x + (vertices[b].x -  vertices[a].x ) * codedValue ; // x
            var y = vertices[a].y + (vertices[b].y -  vertices[a].y ) * codedValue ; // x
            var z = vertices[a].z + (vertices[b].z -  vertices[a].z ) * codedValue ; // x
            var v = new THREE.Vector3();
            v.x = x; v.y = y; v.z = z;
            if (!brokeTrinanle(faces,a,b,v)) {
                console.log('cant broke tri');
                // return false;
		continue;
            }
            // console.log("faces: " + lastGeometry.faces.length);
        }
    };
    prohibitedPoints = {};
	    checkModelMeshVivid();
    };
    return true;
}

// API ----------------------------------------
var encode = function(geometry, mark, pass, stronger1) {
    try {
        console.log("\n\nEncoding watermark "+mark+" on geometry...");
        var geometry1;
        if (geometry.vertices)  { 
            geometry1 = geometry;
        } else {
            geometry1 = new THREE.Geometry().fromBufferGeometry( geometry ); 
        }
        // TODO if it is not buffer geometry    ?
        if (! setupMesh(geometry1)) return false;
        if (! preparePasses(pass)) return false;
        markString = mark;
        stronger = stronger1;
        addEnoughFaces(maxPossibleLengthOfMark * 30);
        if (! autosign()) return false;
        return geometry1;
    } catch (e) {
        return false;
    }
}

var decode = function(geometry, pass, findAll) {
    try {
        console.log("\n\nDecoding watermark on geometry...");
        var geometry1;
        if (geometry.vertices)  { 
            geometry1 = geometry;
        } else {
            geometry1 = new THREE.Geometry().fromBufferGeometry( geometry ); 
        }
        // TODO if it is not buffer geometry    ?
        if (! setupMesh(geometry1)) return false;
        if (! preparePasses(pass)) return false;
        if(!autocheck(findAll)) return false; // true=findAll, false=findFast
        return allMarksFound;
    } catch (e) {
        return false;
    }
}

var encodeMultiple = function(geometry, marksAndPasses, stronger1) {
    try {
        console.log("\n\nEncoding watermarks on geometry...");
        var geometry1;
        if (geometry.vertices)  { 
            geometry1 = geometry;
        } else {
            geometry1 = new THREE.Geometry().fromBufferGeometry( geometry ); 
        }
        // TODO if it is not buffer geometry    ?
		console.log('setup mesh...');
        if (! setupMesh(geometry1)) return false;
		addEnoughFaces(maxPossibleLengthOfMark * 100);
		
		for(var i = 0; i < marksAndPasses.length; i++) {
			markAndPass = marksAndPasses[i];
			var mark = markAndPass["mark"];
			var pass = markAndPass["pass"];
			console.log('doit:',mark,pass);
			console.log('prepare passes...');
			if (! preparePasses(pass)) return false;
			markString = mark;
			stronger = stronger1;
			console.log('siginind...');
			if (! autosign()) return false;
		}
        return geometry1;
    } catch (e) {
		console.log('CATCH:',e);
        return false;
    }
}

var decodeMultiple = function(geometry, passes) {
    try {
        console.log("\n\nDecoding watermark on geometry...");
        var geometry1;
        if (geometry.vertices)  { 
            geometry1 = geometry;
        } else {
            geometry1 = new THREE.Geometry().fromBufferGeometry( geometry ); 
        }
        // TODO if it is not buffer geometry    ?
        if (! setupMesh(geometry1)) return false;
		allMarksFoundWithPasses = {};
		for(var i = 0; i < passes.length; i++) {
			pass = passes[i];
			allMarksFoundWithPasses[pass] = {};
			if (!preparePasses(pass)) continue;
			autocheck(true); // true is findAll
			allMarksFoundWithPasses[pass] = allMarksFound;
		};
        return allMarksFoundWithPasses;
    } catch (e) {
        return false;
    }
}

module.exports.cleanup = cleanup;
module.exports.encode = encode;
module.exports.decode = decode;
module.exports.encodeMultiple = encodeMultiple;
module.exports.decodeMultiple = decodeMultiple;
