let fs = require('fs'),
	path = require('path'),
	THREE = require('three'),
	STLLoader = require('./three-stl-loader')(THREE),
	OBJLoader = require('./three-obj-loader');

OBJLoader(THREE);

let measure = require('./measure'),
	window = require('window-or-global'),
	mygc = require('./mygc');

let process = function(req, res, models_root) {
    res.setHeader('Content-Type', 'application/json');
    // clean up parameters
    console.log('Measure request: ' + req.body.filepath);
    let filein = path.normalize("" + req.body.filepath);
    if (!filein.startsWith(models_root)) {
        res.status(500);
        console.log("Denied path of file: " + req.body.filepath + " Allowed: " + models_root);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
        return;
    }
    // processing
    try { // we have: mark, pass, filein, fileout
		let extname = path.extname(filein).toLowerCase();
		if (extname === '.stl') {
			// STL
			let loader = new STLLoader();
			fs.readFile(filein, function (err, data) {
				if (err) {
					res.status(500);
					console.log(err);
					res.send('{"result":"fail"}');
					res.end();
					mygc();
					return;
				}
				THREE.Cache.enabled = true;
				let ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
				THREE.Cache.add(filein, ab);
				loader.load(filein, function (geometry) { // file loaded
					THREE.Cache.remove(filein);

					res.status(200);
					let result = JSON.stringify(measure(geometry, {supports: true}));
					console.log(' ready: ' + result);
					res.send(result);
					res.end();
					mygc();
				})
			});
		} else if (extname === '.obj') {
			// STL
			loader = new THREE.OBJLoader();
			fs.readFile(filein, function (err, data) {
				if (err) {
					res.status(500);
					console.log(err);
					res.send('{"result":"fail"}');
					res.end();
					mygc();
					return;
				}
				THREE.Cache.enabled = true;
				let ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
				THREE.Cache.add(filein, ab);
				loader.load(filein, function (meshGroup) { // file loaded
					THREE.Cache.remove(filein);

					let timestart = Date.now();
					let boundingBox = new THREE.Box3();
					boundingBox.setFromObject(meshGroup);
					let result = {
						"width":  boundingBox.max.x - boundingBox.min.x,
						"height": boundingBox.max.y - boundingBox.min.y,
						"length": boundingBox.max.z - boundingBox.min.z,
						"area": 0,
						"volume": 0,
						"weight": 0,
						"supportsVolume": 0,
						"faces": 0,
						"vertices": 0,
					};

					meshGroup.traverse( function ( child ) {
					    if ( child instanceof THREE.Mesh ) {
							let r = measure(child.geometry,  {supports: true }); // OBJ groups without supports ?
							result.area += parseFloat(r.area);
							result.volume += parseFloat(r.volume);
							result.weight += parseFloat(r.weight);
							result.supportsVolume += parseFloat(r.supportsVolume);
							result.faces += r.faces;
							result.vertices += r.vertices;
						}
                    } );
					result["timing"] = Date.now() - timestart;

					res.status(200);
					result = JSON.stringify(result);
					console.log(' ready: ' + result);
					res.send(result);
					res.end();
					mygc();
				})
			});
		} else {
			console.log("Unknown extension: " + extname);
			res.status(500);
			res.send('{"result":"fail"}');
			res.end();
			mygc();
		}
    } catch (e) {
        res.status(500);
        console.log(e);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
    }
}

module.exports.process = process;
