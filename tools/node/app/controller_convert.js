var fs = require('fs');
const path = require('path');

var THREE = require('three');
var window = require('window-or-global');
var mygc = require('./mygc');

var BinaryStlWriter = require('./stlbinexporter');
var ThreeMFLoader = require('./3MFLoader')(THREE);
// var OBJLoader = require('./OBJLoader')(THREE);
var OBJLoader = require('three-obj-loader');OBJLoader(THREE);
var arrayBufferToBuffer = require('arraybuffer-to-buffer');
var ab2str = require('arraybuffer-to-string')

var material = new THREE.MeshNormalMaterial();

var process = function(req, res, models_root) {
    res.setHeader('Content-Type', 'application/json');
    // clean up parameters
    console.log('Convert request: ' + req.body.filepathin);
    var filein = path.normalize("" + req.body.filepathin);
    if (!filein.startsWith(models_root)) {
        res.status(500);
        console.log("Denied path of file: " + req.body.filepathin + " Allowed: " + models_root);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
        return;
    }
    var fileout = path.normalize("" + req.body.filepathout);
    if (!fileout.startsWith(models_root)) {
        res.status(500);
        console.log("Denied path of file: " + req.body.filepathout + " Allowed: " + models_root);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
        return;
    }
	if (filein.toLowerCase().endsWith(".3mf")) {
		processMultiParts(req, res, models_root, filein, fileout,  new ThreeMFLoader(), false, false);
		return;
	}
	if (filein.toLowerCase().endsWith(".obj")) {
		processMultiParts(req, res, models_root, filein, fileout,  new THREE.OBJLoader(), true, true); //read as text, return only a joint result
		return;
	}

	// unknown extension
	res.status(500);
	console.log("Uknown extension of file: " + filein);
	res.send('{"result":"fail"}');
	res.end();
	mygc();
	return;
}

function processMultiParts(req, res, models_root, filein, fileout, loader, convertToString, returnJointOnly) {
    // processing 3mf
    try { // we have: mark, pass, filein, fileout
        // var loader = new ThreeMFLoader();
        fs.readFile(filein, function (err, data) {
            if (err) {
                res.status(500);
                console.log(err);
                res.send('{"result":"fail"}');
                res.end();
                mygc();
                return;
            }
            THREE.Cache.enabled = true;
            var ab = data.buffer; // 2 ArrayBuffer: .slice(b.byteOffset, b.byteOffset + b.byteLength);
			if (convertToString) {
				ab = ab2str(ab);
			}
            THREE.Cache.add(filein, ab);

            loader.load(filein, function (group) { // file loaded
                THREE.Cache.remove(filein);

				var promises = [];
				var fileouts = [];
				var jointGeometry = new THREE.Geometry();
				var i = 1;
				group.traverse( function ( mesh ) {
						if ( mesh instanceof THREE.Mesh ) {
							var meshname = fileout +i+".stl";
							fileouts.push(meshname);
							promises.push(processOneMesh(jointGeometry, mesh, meshname, returnJointOnly));
							i++;
						}
				});

				// write joint geometry as promise
				var mesh1 = new THREE.Mesh(jointGeometry, material);
				var theArrayBuffer = BinaryStlWriter.meshUnbufferedToBinArrayBuffer(mesh1); // meshUnbufferedToBinArrayBuffer
				promises.push( new Promise(function(resolve, reject) {
					fs.writeFile(fileout + "all.stl",  arrayBufferToBuffer(theArrayBuffer), function(err) {
						if (err) reject(err);
						else resolve('ok');
					});
				}));
				Promise.all(promises)
				 .then(function(data){
						res.status(200);
						console.log(' Files ready ');
						if (returnJointOnly) {
							res.send('{"result":"ok","joint":"'+fileout+'all.stl"}');
						} else {
							res.send('{"result":"ok","joint":"'+fileout+'all.stl","parts":'+JSON.stringify(fileouts)+'}');
						}
						res.end();
						mygc();
				 })
				 .catch(function(err){
						console.log('ERR in promise: '+err);
						try {
							res.status(500);
							res.send('{"result":"fail"}');
							res.end();
							mygc();
						} catch (e) {
							console.log('Aditional error while error reporting happens: '+e);
						}
				 });
            }, function (progressdata) {
				// do nothing
            }, function (errname) {
				// threat this error as non-critical and continue
				/*
				res.status(500);
				console.log('ERR while loading: '+errname);
				res.send('{"result":"fail"}');
				res.end();
				mygc();
				*/
			});
        })
    } catch (e) {
        res.status(500);
        console.log(e);
        res.send('{"result":"fail"}');
        res.end();
        mygc();
    }
}
function processOneMesh(jointGeometry, mesh, fileout, returnJointOnly) {
	// convert to spare geometry
	var geometry1 = mesh.geometry;
	if ( mesh.geometry instanceof THREE.BufferGeometry) {
		geometry1 = new THREE.Geometry().fromBufferGeometry( mesh.geometry );
	};
	var mesh1 = new THREE.Mesh(geometry1, material);
	// merge to joint geometry
	mesh1.updateMatrix(); // as needed
	jointGeometry.merge(mesh1.geometry, mesh1.matrix);
	// save result
	if (returnJointOnly) {
		return new Promise(function(resolve, reject) {resolve('dummy')});
	} else {
		var theArrayBuffer = BinaryStlWriter.meshUnbufferedToBinArrayBuffer(mesh1); // meshUnbufferedToBinArrayBuffer
		return new Promise(function(resolve, reject) {
			fs.writeFile(fileout,  arrayBufferToBuffer(theArrayBuffer), function(err) {
				if (err) reject(err);
				else resolve('ok');
			});
		});
	}
}

module.exports.process = process;
