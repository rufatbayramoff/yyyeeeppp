var THREE = require('three'); 

var create = function(imageobject, platesize, platetickness, relieftickness, isInverted, colormode) {
    // imageobject.width, imageobject.height, imageobject.data = r g b o [uchar] 
    // console.log(imageobject.width, imageobject.height);
    //console.log(imageobject.data);
    
    var planew = imageobject.width;
    var planeh = imageobject.height;
    var pix = imageobject.data;
    var minz = 9e9, maxz=-9e9, zoomx, zoomy;
    if (imageobject.width > imageobject.height) {
        zoomx = 1;
        zoomy = imageobject.height / imageobject.width;
    } else {
        zoomy = 1;
        zoomx = imageobject.width / imageobject.height;
    }
    
    var triangles = (planew-1) * (planeh-1) * 2; // * 2 !!!
    triangles += 4 * (planew-1) + 4 * (planeh-1); // sides
    triangles += 2; // back     
    var geometry = new THREE.BufferGeometry();
    var positions = new Float32Array( triangles * 3 * 3 );
    var normals = new Float32Array( triangles * 3 * 3 ); 
    var colors = new Float32Array( triangles * 3 * 3 ); 
    var pA = new THREE.Vector3();
    var pB = new THREE.Vector3();
    var pC = new THREE.Vector3();
    var cb = new THREE.Vector3();
    var ab = new THREE.Vector3();
    var dx = -0.5, dy = -0.5;
    var ax,ay,az,bx,by,bz,cx,cy,cz,ex,ey,ez,nx,ny,nz;
    function getZ(i,j) { 
        return (isInverted ? 
        ((pix[(j*planew+i)*4]+pix[(j*planew+i)*4+1]+pix[(j*planew+i)*4+2])/3.0) :
        (1.0-(pix[(j*planew+i)*4]+pix[(j*planew+i)*4+1]+pix[(j*planew+i)*4+2])/3.0) );
    }
    function getCL(i,j,rgb) {
        return pix[(j*planew+i)*4+rgb]/255.0;
    }
    function addTriangle(positionsArray, normalsArray, colors, positionInArrays, ax, ay, az, bx, by, bz, cx, cy, cz) {
        positionsArray[ positionInArrays ]     = ax;
        positionsArray[ positionInArrays + 1 ] = ay;
        positionsArray[ positionInArrays + 2 ] = az;
        positionsArray[ positionInArrays + 3 ] = bx;
        positionsArray[ positionInArrays + 4 ] = by;
        positionsArray[ positionInArrays + 5 ] = bz;
        positionsArray[ positionInArrays + 6 ] = cx;
        positionsArray[ positionInArrays + 7 ] = cy;
        positionsArray[ positionInArrays + 8 ] = cz;
        // flat face normals
        var pA = new THREE.Vector3();
        var pB = new THREE.Vector3();
        var pC = new THREE.Vector3();
        var cb = new THREE.Vector3();
        var ab = new THREE.Vector3();
        pA.set( ax, ay, az );
        pB.set( bx, by, bz );
        pC.set( cx, cy, cz );
        cb.subVectors( pC, pB );
        ab.subVectors( pA, pB );
        cb.cross( ab );
        cb.normalize();
        var nx = cb.x;
        var ny = cb.y;
        var nz = cb.z;
        normalsArray[ positionInArrays ]     = nx;
        normalsArray[ positionInArrays + 1 ] = ny;
        normalsArray[ positionInArrays + 2 ] = nz;
        normalsArray[ positionInArrays + 3 ] = nx;
        normalsArray[ positionInArrays + 4 ] = ny;
        normalsArray[ positionInArrays + 5 ] = nz;
        normalsArray[ positionInArrays + 6 ] = nx;
        normalsArray[ positionInArrays + 7 ] = ny;
        normalsArray[ positionInArrays + 8 ] = nz;
        // colors
        colors[positionInArrays+0] = 1;
        colors[positionInArrays+1] = 1;
        colors[positionInArrays+2] = 1;
        colors[positionInArrays+3] = 1;
        colors[positionInArrays+4] = 1;
        colors[positionInArrays+5] = 1;
        colors[positionInArrays+6] = 1;
        colors[positionInArrays+7] = 1;
        colors[positionInArrays+8] = 1;
        
        return positionInArrays+9;                
    };
    var k = 0;
    for(var i=0; i<planew-1; i++) {
        for(var j=0; j<planeh-1; j++) {  
            // a c
            // e b
            // tri 1
            ax = ( + ( i + 0 ) * 1.0 / planew + dx ) * zoomx;
            ay = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
            az = getZ( i + 0, j + 0 );
            cx = ( + ( i + 1 ) * 1.0 / planew + dx ) * zoomx;
            cy = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
            cz = getZ( i + 1, j + 0 );
            bx = ( + ( i + 1 ) * 1.0 / planew + dx ) * zoomx;
            by = ( - ( j + 1 ) * 1.0 / planeh - dy ) * zoomy;
            bz = getZ( i + 1, j + 1 );
            positions[ k ]     = ax;
            positions[ k + 1 ] = ay;
            positions[ k + 2 ] = az;
            positions[ k + 3 ] = bx;
            positions[ k + 4 ] = by;
            positions[ k + 5 ] = bz;
            positions[ k + 6 ] = cx;
            positions[ k + 7 ] = cy;
            positions[ k + 8 ] = cz;
            // flat face normals
            pA.set( ax, ay, az );
            pB.set( bx, by, bz );
            pC.set( cx, cy, cz );
            // colors
            colors[ k ]     = getCL(i+0, j+0, 0);
            colors[ k + 1 ] = getCL(i+0, j+0, 1);
            colors[ k + 2 ] = getCL(i+0, j+0, 2);
            colors[ k + 3 ] = getCL(i+1, j+1, 0);
            colors[ k + 4 ] = getCL(i+1, j+1, 1);
            colors[ k + 5 ] = getCL(i+1, j+1, 2);
            colors[ k + 6 ] = getCL(i+1, j+0, 0);
            colors[ k + 7 ] = getCL(i+1, j+0, 1);
            colors[ k + 8 ] = getCL(i+1, j+0, 2);
            // normals
            cb.subVectors( pC, pB );
            ab.subVectors( pA, pB );
            cb.cross( ab );
            cb.normalize();
            nx = cb.x;
            ny = cb.y;
            nz = cb.z;
            normals[ k ]     = nx;
            normals[ k + 1 ] = ny;
            normals[ k + 2 ] = nz;
            normals[ k + 3 ] = nx;
            normals[ k + 4 ] = ny;
            normals[ k + 5 ] = nz;
            normals[ k + 6 ] = nx;
            normals[ k + 7 ] = ny;
            normals[ k + 8 ] = nz;
            k+=9;
            // tri 2
            //ax = ( + ( i + 0 ) * 1.0 / planew + dx ) * zoomx;
            //ay = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
            //az = getZ( i + 0, j + 0 );
            ex = ( + ( i + 0 ) * 1.0 / planew + dx ) * zoomx;
            ey = ( - ( j + 1 ) * 1.0 / planeh - dy ) * zoomy;
            ez = getZ( i + 0, j + 1 );
            //bx = ( + ( i + 1 ) * 1.0 / planew + dx ) * zoomx;
            //by = ( - ( j + 1 ) * 1.0 / planeh - dy ) * zoomy;
            //bz = getZ( i + 1, j + 1 );
            positions[ k ]     = ax;
            positions[ k + 1 ] = ay;
            positions[ k + 2 ] = az;
            positions[ k + 3 ] = ex;
            positions[ k + 4 ] = ey;
            positions[ k + 5 ] = ez;
            positions[ k + 6 ] = bx;
            positions[ k + 7 ] = by;
            positions[ k + 8 ] = bz;
            // flat face normals
            pA.set( ax, ay, az );
            pB.set( ex, ey, ez );
            pC.set( bx, by, bz );
            // colors
            colors[ k ]     = getCL(i+0, j+0, 0);
            colors[ k + 1 ] = getCL(i+0, j+0, 1);
            colors[ k + 2 ] = getCL(i+0, j+0, 2);
            colors[ k + 3 ] = getCL(i+0, j+1, 0);
            colors[ k + 4 ] = getCL(i+0, j+1, 1);
            colors[ k + 5 ] = getCL(i+0, j+1, 2);
            colors[ k + 6 ] = getCL(i+1, j+1, 0);
            colors[ k + 7 ] = getCL(i+1, j+1, 1);
            colors[ k + 8 ] = getCL(i+1, j+1, 2);
            // normals
            cb.subVectors( pC, pB );
            ab.subVectors( pA, pB );
            cb.cross( ab );
            cb.normalize();
            nx = cb.x;
            ny = cb.y;
            nz = cb.z;
            normals[ k ]     = nx;
            normals[ k + 1 ] = ny;
            normals[ k + 2 ] = nz;
            normals[ k + 3 ] = nx;
            normals[ k + 4 ] = ny;
            normals[ k + 5 ] = nz;
            normals[ k + 6 ] = nx;
            normals[ k + 7 ] = ny;
            normals[ k + 8 ] = nz;
            k+=9;
            if (az < minz) minz = az;
            if (az > maxz) maxz = az;
            if (j==planeh-2) {
                if (ez < minz) minz = ez;
                if (ez > maxz) maxz = ez;
            }
        } 
        if (cz < minz) minz = cz;
        if (cz > maxz) maxz = cz;
      }
      if (bz < minz) minz = bz;
      if (bz > maxz) maxz = bz;
      if (maxz - minz == 0) maxz = 1 + minz;
      // done: positions and normals ready, minmax found
      // z = minz .. maxz
      // add back side:
      // document.getElementById('maxtickness').value = plate depth
      // document.getElementById('mintickness').value = relief depth
      var factorz = (relieftickness)/(maxz - minz); 
      var pozback = minz - platetickness / factorz; 
      maxxx = ( + ( planew - 1 ) * 1.0 / planew + dx ) * zoomx;
      maxyy = ( - ( planeh - 1 ) * 1.0 / planeh - dy ) * zoomy;
      k = addTriangle(positions, normals, colors, k, 
          -zoomx/2, maxyy, pozback,    -zoomx/2, zoomy/2, pozback,    maxxx, zoomy/2, pozback);
      k = addTriangle(positions, normals, colors, k, 
          -zoomx/2, maxyy, pozback,    maxxx, zoomy/2, pozback,    maxxx, maxyy, pozback);
      // 4 sides
      for(var i=0; i<planew-1; i++) {
        ax = ( + ( i + 0 ) * 1.0 / planew + dx ) * zoomx;
        bx = ( + ( i + 1 ) * 1.0 / planew + dx ) * zoomx;
        az = getZ( i + 0, planeh-1 );
        bz = getZ( i + 1, planeh-1 );
        k = addTriangle(positions, normals, colors, k, 
          ax, maxyy, az,    bx, maxyy, pozback, bx, maxyy, bz);
        k = addTriangle(positions, normals, colors, k, 
          ax, maxyy, az,    ax, maxyy, pozback, bx, maxyy, pozback);
        az = getZ( i + 0, 0 );
        bz = getZ( i + 1, 0 );
        k = addTriangle(positions, normals, colors, k, 
          ax, +zoomy/2, az,    bx, +zoomy/2, bz,    bx, +zoomy/2, pozback);
        k = addTriangle(positions, normals, colors, k, 
          ax, +zoomy/2, az,    bx, +zoomy/2, pozback,    ax, +  zoomy/2, pozback);
      }
      for(var j=0; j<planeh-1; j++) {
        ay = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
        by = ( - ( j + 1 ) * 1.0 / planeh - dy ) * zoomy;
        az = getZ( planew-1, j + 0);
        bz = getZ( planew-1, j + 1);
        k = addTriangle(positions, normals, colors, k, 
          maxxx, ay, az,    maxxx, by, bz, maxxx, by, pozback);
        k = addTriangle(positions, normals, colors, k, 
          maxxx, ay,az,      maxxx, by, pozback, maxxx, ay, pozback);
        az = getZ( 0, j + 0 );
        bz = getZ( 0, j + 1 );
        k = addTriangle(positions, normals, colors, k, 
          -zoomx/2, ay, az,        -zoomx/2,by,  pozback, -zoomx/2,by,  bz);
        k = addTriangle(positions, normals, colors, k, 
          -zoomx/2,ay,az,        -zoomx/2,ay, pozback, -zoomx/2,by,  pozback);
      }
      // ay = ( - ( j + 0 ) * 1.0 / planeh - dy ) * zoomy;
    function disposeArray() { /* this.array = null; */ }
    geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ).onUpload( disposeArray ) );
    geometry.addAttribute( 'normal', new THREE.BufferAttribute( normals, 3 ).onUpload( disposeArray ) );
    var materialActive;
    if (colormode=='mono') {
        materialActive = new THREE.MeshPhongMaterial( { color: 0x418CE9, specular: 0x111111, shininess: 200 } );
        //renderIsColored = false;
    } else {
        materialActive = new THREE.MeshPhongMaterial( {
                    color: 0xffffff, specular: 0x222222, shininess: 250,
                    // side: THREE.DoubleSide, 
                    vertexColors: THREE.VertexColors
                } );
        geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ).onUpload( disposeArray ) );
        //renderIsColored = true;
    }
    geometry.computeBoundingSphere()
    plane = new THREE.Mesh( geometry, materialActive );
    // scale
    var factorxy = platesize; 
    plane.geometry.scale(factorxy, factorxy, factorz);
    // plane.scale.set( factorxy, factorxy, factorz );
    //for (var y = 0; y < this.height; y++) {
    //    for (var x = 0; x < this.width; x++) {
    //        var idx = (this.width * y + x) * bytesperpixel;

            // invert color
            //this.data[idx] = 255 - this.data[idx];
            //this.data[idx+1] = 255 - this.data[idx+1];
            //this.data[idx+2] = 255 - this.data[idx+2];

            // and reduce opacity
            //this.data[idx+3] = this.data[idx+3] >> 1;
    //    }
    //}
    return plane;
};


module.exports.create = create;