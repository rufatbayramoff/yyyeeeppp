create table users (id int not null auto_increment, primary key (id));
alter table users add email varchar(1024); 
alter table users add marketing TINYINT not null default 0; 
alter table users add approved TINYINT not null default 0;
alter table users add speclimit int not null default 0;
create table sessions (id int not null auto_increment, primary key (id));
alter table sessions add ip varchar(16); 
alter table sessions add userid int null; 
alter table sessions add filesize int;
alter table sessions add filename varchar(255); 
alter table sessions add oper tinyint not null default 0;
alter table sessions add ts timestamp default now(); 
 
