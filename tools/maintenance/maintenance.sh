#!/bin/bash
# Данный скрипт переводит сайт в режим обслуживания
# принимает параметры true - перевод в обслуживание, false - снятие с обслуживания, без параметров или любой другой параметр проверяет текущее состояние
file="/var/www/treatstock/tools/maintenance/maintenance"
function outputJSON {
    echo -e "{\"maintenance\": {\n\t\"status\":\"$status\"\n\t$additional}\n}";exit
}
function maintenanceCHECK {
    if [ -f $file ] 
    then status=true;outputJSON
    else status=false;outputJSON
    fi
}
case $1 in
	true)
		# setup maintenance flag
        execute=$(touch $file 2>&1) || additional=",\"error\":\"$execute\""
        maintenanceCHECK
		;;
	false)
        # remove maintenance flag
        execute=$(rm $file 2>&1) || additional=",\"error\":\"$execute\""
        maintenanceCHECK
		;;
	*)
        maintenanceCHECK
		;;
esac