echo > /vagrant/conf/nginx.src
/vagrant/conf/0_recreatedb.sh

#cd /home/vagrant
#mkdir small_light
#cd small_light
#sudo apt-get install imagemagick libmagickwand-dev libgd-dev libpcre3-dev 
#git clone https://github.com/cubicdaiya/ngx_small_light.git
#cd ngx_small_light
cd /vagrant/repo/tools/small_light
./setup
cd /opt/nginx.src/nginx-1.10.1
./configure --with-cc-opt='-g -O2 -fPIE -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2' \
    --with-ld-opt='-Wl,-Bsymbolic-functions -fPIE -pie -Wl,-z,relro -Wl,-z,now' \
    --prefix=/usr/share/nginx \
    --conf-path=/etc/nginx/nginx.conf \
    --http-log-path=/var/log/nginx/access.log \
    --error-log-path=/var/log/nginx/error.log \
    --lock-path=/var/lock/nginx.lock \
    --pid-path=/run/nginx.pid \
    --http-client-body-temp-path=/var/lib/nginx/body \
    --http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
    --http-proxy-temp-path=/var/lib/nginx/proxy \
    --http-scgi-temp-path=/var/lib/nginx/scgi \
    --http-uwsgi-temp-path=/var/lib/nginx/uwsgi \
    --with-pcre-jit \
    --with-ipv6 \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_realip_module \
    --with-http_auth_request_module \
    --with-http_addition_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_geoip_module \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_image_filter_module \
    --with-http_mp4_module \
    --with-http_perl_module \
    --with-http_random_index_module \
    --with-http_secure_link_module \
    --with-http_v2_module \
    --with-http_sub_module \
    --with-http_xslt_module \
    --with-mail \
    --with-mail_ssl_module \
    --with-stream \
    --with-stream_ssl_module \
    --with-threads \
    --add-module=/vagrant/repo/tools/small_light
#    --add-module=/home/vagrant/small_light/ngx_small_light
cd /opt/nginx.src/nginx-1.10.1
make && make install && sudo cp -f ./objs/nginx /usr/sbin/nginx && /etc/init.d/nginx stop  && rm -Rf /var/lib/nginx/cache/* && /etc/init.d/nginx start

vim /etc/nginx/sites-enabled/frontend
        small_light on;
        location ~ colorer[^/]*/(.+)$ {
                set $file $1;
                rewrite ^ /$file;
        }
       location ~ ^\/colorer\(dw=.*,dh=.*\)(.+)\.(jpg|jpeg|png)$ {
                set $file $1.$2;
                rewrite ^ /$file;
        }

/etc/init.d/nginx stop; /etc/init.d/nginx start
cd /opt/nginx.src/nginx-1.10.1; make; make install; sudo cp -f ./objs/nginx /usr/sbin/nginx; /etc/init.d/nginx stop; /etc/init.d/nginx start

SAMPLE OF USE: http://ts.vcap.me/static/samples/sample1/