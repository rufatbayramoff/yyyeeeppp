<?php $gitCurrentBranch=exec("git branch --show-current");?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>TS <?php echo $gitCurrentBranch ?></title>
    <!-- <meta http-equiv="refresh" content="10"> -->
<style>
.grid{
    display: grid;
    grid-template-columns: repeat(6,1fr);
    grid-gap: 16px;
}
@media (max-width: 1200px) {
    .grid{
    grid-template-columns: repeat(2,1fr);
}
}
.block {
    display: flex;
    flex-direction: column;
    padding: 12px;
    max-height: 1000pt;
    border: 1px solid darkcyan;
}
.gitlog {
    overflow: auto;
    grid-column: span 3;
}
.disable {
    display: none;
}

</style>
</head>
<body>
<div class="grid">
<div class="block git"><h2>git</h2>
<?php
    set_time_limit(600);

function execPrint($command) {
    $result = array();
    exec($command, $result);
    print("<pre>");
    foreach ($result as $line) {
        print($line . "\n");
    }
    print("</pre>");
}


function git_checkout($command1) {
    execPrint("ggit checkout $command1");
}
if ($_POST["git-pull"] !="") {
    $action = $_POST["git-pull"];
    exec("cd /var/www/treatstock/;ggit pull 2>&1 > /tmp/log &");
    header('Location: index.php');
    exit ( );
}

if ($_POST["tgt-branch"] !="") {
    $tgtbranch = $_POST["tgt-branch"];
    echo "tgtbranch:  $tgtbranch";
    exec("cd /var/www/treatstock/;ggit checkout $tgtbranch 2>&1 > /tmp/log &");
    header('Location: index.php');
    exit ( );
}

if ($_POST["maintenance-true"] !="") {
    $tgtbranch = $_POST["maintenance-true"];
    echo "tgtbranch:  $tgtbranch";
    exec("/var/www/treatstock/tools/maintenance/maintenance.sh true 2>&1 > /tmp/log &");
    header('Location: index.php');
    exit ( );
}
if ($_POST["maintenance-false"] !="") {
    $tgtbranch = $_POST["maintenance-false"];
    echo "tgtbranch:  $tgtbranch";
    exec("/var/www/treatstock/tools/maintenance/maintenance.sh false 2>&1 > /tmp/log &");
    header('Location: index.php');
    exit ( );
}
if ($_POST["db-migration"] !="") {
    $tgtbranch = $_POST["db-migration"];
    exec("cd /var/www/treatstock/;ggit migration 2>&1 > /tmp/log &");
    header('Location: index.php');
    exit ( );
}

if ($_POST["db-reset"] !="") {
    $tgtbranch = $_POST["db-reset"];
    exec("cd /var/www/treatstock/;ggit db-reset 2>&1 > /tmp/log &");
    header('Location: index.php');
    exit ( );
}

if ($_POST["git-status"] !="") {
    $tgtbranch = $_POST["git-status"];
    exec("cd /var/www/treatstock/;ggit status 2>&1 > /tmp/log &");
    header('Location: index.php');
    exit ( );
}

if ($_POST["fix-permission"] !="") {
    $tgtbranch = $_POST["fix-permission"];
    exec("cd /var/www/treatstock/;ggit filepermission > /tmp/log");
    // 2>&1 && echo 'fix file permission - ok' > /tmp/log || echo 'fix file permission - we have a problem'> /tmp/log
    header('Location: index.php');
    exit ( );
}

if ($_POST["git-reset"] !="") {
    $tgtbranch = $_POST["git-reset"];
    exec("git reset --hard HEAD~1 2>&1 && echo 'git reset --hard HEAD~1 - ok' > /tmp/log || echo 'git reset --hard HEAD~1 - we have a problem'> /tmp/log");
    header('Location: index.php');
    exit ( );
}


echo "<div>Текущая ветка: ";
echo $gitCurrentBranch;
echo "</div>";

$pathToFile = '/tmp/log';
if (file_exists($pathToFile)) {
    $GetContentFile = file_get_contents($pathToFile);
}

?>

<form action="index.php" method="post">
<p>Перейти на ветку #<input type="number" name="tgt-branch" value="Task number" placeholder="Номер задачи"><input type="submit" value="Переключить" /></p>
<p>Или перейти на: <input type="submit" name="tgt-branch" value="dev">
<input type="submit" name="tgt-branch" value="prerelease"></p>
<p>Действия:
<input type="submit" name="git-pull" value="pull">
<input type="submit" name="git-status" value="status">
<input type="submit" name="git-reset" title="git reset --hard HEAD" value="Сброс изменений"></p>
</form>
</div>

<div class="block database"><h2>кнопочки</h2>
<form action="index.php" method="post">
    <div class="maintenance"><h3>db</h3>
    <input type="submit" name="db-migration" value="Миграции">
    <input class="disable" type="submit" name="db-reset" value="Reset DB & reset Project">
    <input class="disable" type="submit" name="ts-reset" value="Reset Project">
    <input class="disable" type="submit" name="db-update" value="Update DB (Slow!)">
    </div>
    <input type="submit" name="fix-permission" value="fix file permision">
    <div class="maintenance"><h3>maintenance</h3>
    <p>
    <input type="submit" name="maintenance-true" value="true">
    <input type="submit" name="maintenance-false" value="false">
    </p>
    </div>
</form>
</div>

<div class="block gitlog"><h2>результат</h2>
<pre><?php  echo $GetContentFile; ?></pre>
</div>

<div class="block links"><h2>состояние</h2>
<span><?php if (file_exists("/var/www/treatstock/tools/maintenance/maintenance")) {echo "В обслуживании";}?></span>
<pre>
<?php
echo $_SERVER['HTTP_HOST'];

echo "<br>Free disk space:";
$bytes = disk_free_space(".");
$si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
$base = 1024;
$class = min((int)log($bytes , $base) , count($si_prefix) - 1);
//echo $bytes . '<br />';
echo sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . '<br />';

//$pathToFileIdRsaPub = '/home/ubuntu/.ssh/id_rsa.pub';
//echo $pathToFileIdRsaPub;
//if (file_exists($pathToFileIdRsaPub)) {
//    $GetContentFileIdRsaPub = file_get_contents($pathToFileIdRsaPub);
//}
//echo "<p> $GetContentFileIdRsaPub </p>";

$supervisor_statuscode = exec("sudo /etc/init.d/supervisor status 1>/dev/null; echo $?");
if ($supervisor_statuscode == 0 ) {
    $supervisor_status="Всё хорошо";
 } elseif ($supervisor_statuscode == 1 ) {
$supervisor_status="Не работает";
} elseif ($supervisor_statuscode == 3 ) {
    $supervisor_status="Не работает или супервизор шрёдингера в суперпозиции (работает но нет)";
}
echo "<h3>супервизор</h3><pre>$supervisor_statuscode -  $supervisor_status\n";

?>
</pre>
</div>

<div class="block gitlog"><h2>Процессы супервизора</h2>
<?php execPrint("sudo supervisorctl status");



?>
</div>
<div class="block disable"><h2></h2>
</div>

</div><!-- end flex-->
</body>
</html>