1) install slic3r and CuraEngine
# slic3r
cd /home/vagrant
wget https://dl.slic3r.org/linux/slic3r-linux-x86_64-1-2-9-stable.tar.gz
tar xzvpf ./slic3r-linux-x86_64-1-2-9-stable.tar.gz

################# приемлимого воспроизводимого пути инсталляции кур не найдено.

# cura engine installer
sudo apt-get install cura-engine

#cura from ultimaker
https://ultimaker.com/en/products/cura-software/list     2.7

# cura: libprotobuf
wget https://github.com/google/protobuf/archive/v3.4.1.tar.gz
tar xzvpf ./v3.4.1.tar.gz
cd protobuf-3.4.1
./autogen.sh
./configure
make
sudo make install
sudo ldconfig
cd ..
# SIP
wget https://sourceforge.net/projects/pyqt/files/sip/sip-4.19.3/sip-4.19.3.tar.gz
tar xzvpf ./sip-4.19.3.tar.gz
cd sip-4.19.3/
python configure.py
make
sudo make install
cd ..
sudo apt-get install python-sip python-sip-dev python3-sip python3-sip-dev
sudo apt-get install python3-pip
sudo pip install --upgrade pip
sudo pip3 install --upgrade pip3
# cura: libarcus
sudo apt-get install python3-all-dev
sudo apt-get install python2.7-dev
git clone https://github.com/Ultimaker/libArcus.git
cd libArcus
mkdir build && cd build
cmake ..
make
sudo make install
cd ..
# cura engine
git clone https://github.com/Ultimaker/CuraEngine.git
cd CuraEngine
mkdir build && cd build
cmake ..
make
sudo make install

2) place your stl files into 'files"
3) run checkall.sh
4) upload 1.csv into http://192.168.66.10:5858/analyze/ together with your files