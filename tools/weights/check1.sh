#!/bin/bash
# weight = density * pi * d * d * l / 4
#          1.25 * 3.1415 * 2.85 * 2.85 * v / 4000   
export n=`echo $1 | cut -d "/" -f 2`

# main volume: 1.2 mm 20% rectangular 
# supports: 15% 60deg rectangular
export wc=n/a
export wc2=n/a
export ws=n/a
export ws2=n/a

export vc=`/usr/local/bin/CuraEngine slice -v -j ../cfg/ultimaker2.def.json -o ../1.gcode -l $1 2>&1 | grep "Filament:" | cut -d " " -f 2`
export wc=`echo "scale=2; $vc * 1.25 / 1000.0" | bc -l`
echo `date` $n cura w/o supports $wc g
export vc2=`/usr/local/bin/CuraEngine slice -v -j ../cfg/ultimaker2-support.def.json -o ../1.gcode -l $1 2>&1 | grep "Filament:" | cut -d " " -f 2`
export wc2=`echo "scale=2; $vc2 * 1.25 / 1000.0" | bc -l`
echo `date` $n cura w/ supports $wc2 g

export vs=`/home/vagrant/Slic3r/bin/slic3r -j 4 --fill-pattern rectilinear --fill-density 20 --nozzle-diameter 0.4 --perimeters 3 --filament-diameter 3 --layer-height 0.4 --first-layer-height 0.4 --solid-layers 3 --skirts 0 --output ../1.gcode $1 | grep "Filament " | cut -d " " -f 3 | cut -d "m" -f 1`
export ws=`echo "scale=2; $vs * 1.25 * 3.1415 * 3 * 3 / 4000.0" | bc -l`
echo `date` $n slic3r w/o supports $ws g
#export vs2=`/home/vagrant/Slic3r/bin/slic3r -j 4 --support-material --support-material-threshold 50 -support-material-extrusion-width 50% --fill-pattern rectilinear --fill-density 20 --nozzle-diameter 0.4 --perimeters 3 --filament-diameter 3 --layer-height 0.4 --first-layer-height 0.4 --solid-layers 3 --skirts 0 --output ../1.gcode $1 | grep "Filament " | cut -d " " -f 3 | cut -d "m" -f 1`
#export ws2=`echo "scale=2; $vs2 * 1.25 * 3.1415 * 3 * 3 / 4000.0" | bc -l`
#echo `date` $n slic3r w/ supports $ws2 g

echo $n\; $wc $ws - $wc2 $ws2 >> ../1.csv 

