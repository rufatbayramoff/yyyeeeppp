#!/bin/bash

# install:

##### http://dl.slic3r.org/linux/ -> unpack to /home/vagrant/Slic3r/bin/slic3r

##### https://github.com/Ultimaker/CuraEngine
# mkdir cura && cd cura
# wget https://github.com/Ultimaker/CuraEngine/archive/2.5.0.tar.gz
# wget https://github.com/Ultimaker/libArcus/archive/2.5.0.zip
# wget https://github.com/google/protobuf/archive/v3.3.0.tar.gz
# wget https://sourceforge.net/projects/pyqt/files/sip/sip-4.19.2/sip-4.19.2.tar.gz
# tar xzvpf ./v3.3.0.tar.gz
# sudo apt-get install -y python-protobuf
# cd protobuf-3.3.0
# ./autogen.sh
# ./configure
# make
# make install
# sudo ldconfig
# cd ../
# xzvpf ./sip-4.19.2.tar.gz
# cd sip-4.19.2/
# python3 configure.py
# make
# make install
# unzip 2.5.0.zip
# apt-get install python3-sip-dev sip-dev
# cd /home/vagrant/cura/libArcus-2.5.0/; rm -Rf build; mkdir build; cd build; cmake .. -DPYTHON_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") -DPYTHON_LIBRARY=$(python3 -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))") && make && make install
# cd /home/vagrant/cura/libArcus-2.5.0/; rm -Rf build; mkdir build; cd build; cmake .. -DPYTHON_INCLUDE_DIR=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") -DPYTHON_LIBRARY=/usr/lib/python3.4 && make && make install
# ln -s  /usr/local/lib/x86_64-linux-gnu/libArcus.so.3 /usr/local/lib/libArcus.so.3
# cd libArcus-2.5.0
# mkdir build && cd build
# sudo apt-get install python3-sip
# vim ../cmake/FindSIP.py
###  if True: # sys.platform == "win32":
# cmake .. \
# -DPYTHON_INCLUDE_DIR=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())")  \
# -DPYTHON_LIBRARY=$(python -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))")
# sudo add-apt-repository ppa:ubuntu-toolchain-r/test
# sudo apt-get update
# sudo apt-get install g++-4.9
# cd /home/vagrant/cura/CuraEngine-2.5.0/; rm -Rf ./build; mkdir build && cd build && cmake .. && make && make install
# make
# make install
# cd ../
# tar xzvpf ./2.5.0.tar.gz
# cd CuraEngine-2.5.0
# mkdir build && cd build
# cmake ..
# make
# make install
#!!!!!!!!!!! CuraEngine 2.3.0 working!


# https://ultimaker.com/en/products/cura-software/list -> run AppImage


cd files
echo name\;value> ../1.csv 

time find . -name \*.stl -exec ../check1.sh {} \;

rm -f ../1.gcode