#!/bin/bash
# create symlink to ImageMagick
if [ ! -e /usr/include/ImageMagick ]; then
    ln -sf /usr/include/ImageMagick-6 /usr/include/ImageMagick
fi
if [ ! -e /usr/include/ImageMagick-6/magick/magick-baseconfig.h ]; then
    ln -sf /usr/include/x86_64-linux-gnu/ImageMagick-6/magick/magick-baseconfig.h /usr/include/ImageMagick-6/magick/magick-baseconfig.h
fi
# install gsoap
if [ ! -f ./libgsoap-dev_2.8.16-2_amd64.deb ]; then
    wget https://launchpad.net/ubuntu/+source/gsoap/2.8.16-2/+build/5208555/+files/libgsoap-dev_2.8.16-2_amd64.deb -O libgsoap-dev_2.8.16-2_amd64.deb
    dpkg -i ./libgsoap-dev_2.8.16-2_amd64.deb
#    rm -f ./libgsoap-dev_2.8.16-2_amd64.deb
fi
if [ ! -f ./libgsoap4_2.8.16-2_amd64.deb ]; then
    wget https://launchpad.net/ubuntu/+source/gsoap/2.8.16-2/+build/5208555/+files/libgsoap4_2.8.16-2_amd64.deb -O libgsoap4_2.8.16-2_amd64.deb
    dpkg -i ./libgsoap4_2.8.16-2_amd64.deb
#    rm -f ./libgsoap4_2.8.16-2_amd64.deb
fi
if [ ! -f ./gsoap_2.8.16-2_amd64.deb ]; then
    wget https://launchpad.net/ubuntu/+source/gsoap/2.8.16-2/+build/5208555/+files/gsoap_2.8.16-2_amd64.deb -O gsoap_2.8.16-2_amd64.deb
    dpkg -i ./gsoap_2.8.16-2_amd64.deb
#    rm -f ./gsoap_2.8.16-2_amd64.deb
fi
#Create Symlink to library ImageMagick
if [ ! -e /usr/lib/x86_64-linux-gnu/libMagick++.so ]; then
    ln -sf /usr/lib/x86_64-linux-gnu/libMagick++-6.Q16.so /usr/lib/x86_64-linux-gnu/libMagick++.so
fi
if [ ! -e /usr/lib/x86_64-linux-gnu/libMagickCore.so ]; then
    ln -sf /usr/lib/x86_64-linux-gnu/libMagickCore-6.Q16.so /usr/lib/x86_64-linux-gnu/libMagickCore.so
fi
# add library path to ldconfig
usrlib64=`cat /etc/ld.so.conf.d/x86_64-linux-gnu_GL.conf |grep '/usr/lib64'`
usrlocallib64=`cat /etc/ld.so.conf.d/x86_64-linux-gnu_GL.conf |grep '/usr/local/lib64'`
if [ ! "$usrlib64" == "/usr/lib64" ]; then
    echo '/usr/lib64' >>/etc/ld.so.conf.d/x86_64-linux-gnu_GL.conf
fi
if [ ! "$usrlocallib64" == "/usr/local/lib64" ]; then
    echo '/usr/local/lib64' >>/etc/ld.so.conf.d/x86_64-linux-gnu_GL.conf
fi
ldconfig