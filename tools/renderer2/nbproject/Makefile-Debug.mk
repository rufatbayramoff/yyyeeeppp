#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/CMDHandler.o \
	${OBJECTDIR}/src/Viewer360.o \
	${OBJECTDIR}/src/http/http_server.o \
	${OBJECTDIR}/src/main.o \
	${OBJECTDIR}/src/renderer/RenderingFarm.o \
	${OBJECTDIR}/src/renderer/RenderingLeaf.o \
	${OBJECTDIR}/src/renderer/STLBinSaver.o
#	${OBJECTDIR}/src/soap/soapC.o \
#	${OBJECTDIR}/src/soap/soapmy_USCOREdispatcherBindingService.o \
#	${OBJECTDIR}/src/soap/stdsoap2.o
#       ${OBJECTDIR}/src/myRendSoapServer.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++11 -Wall -Wno-write-strings -DWITH_PURE_VIRTUAL -fopenmp
CXXFLAGS=-std=c++11 -Wall -Wno-write-strings -DWITH_PURE_VIRTUAL -fopenmp

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/usr/local/lib64 -L/usr/local/lib64/usr/local/lib64/osgPlugins-3.4.0 -L/usr/lib/x86_64-linux-gnu -losg -losgDB -losgUtil -losgGA -losgShadow -losgViewer -lOpenThreads -lGL -levent -lpthread -lpcrecpp -lb64 -losgText -lMagick++-6.Q16 -lMagickWand-6.Q16 -lMagickCore-6.Q16

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/renderer2

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/renderer2: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/renderer2 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/src/CMDHandler.o: src/CMDHandler.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CMDHandler.o src/CMDHandler.cpp

${OBJECTDIR}/src/Viewer360.o: src/Viewer360.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Viewer360.o src/Viewer360.cpp

${OBJECTDIR}/src/http/http_server.o: src/http/http_server.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/http
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/http/http_server.o src/http/http_server.cpp

${OBJECTDIR}/src/main.o: src/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main.o src/main.cpp

#${OBJECTDIR}/src/myRendSoapServer.o: src/myRendSoapServer.cpp 
#	${MKDIR} -p ${OBJECTDIR}/src
#	${RM} "$@.d"
#	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/myRendSoapServer.o src/myRendSoapServer.cpp

${OBJECTDIR}/src/renderer/RenderingFarm.o: src/renderer/RenderingFarm.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/renderer
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/renderer/RenderingFarm.o src/renderer/RenderingFarm.cpp

${OBJECTDIR}/src/renderer/RenderingLeaf.o: src/renderer/RenderingLeaf.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/renderer
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/renderer/RenderingLeaf.o src/renderer/RenderingLeaf.cpp

${OBJECTDIR}/src/renderer/STLBinSaver.o: src/renderer/STLBinSaver.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/renderer
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/renderer/STLBinSaver.o src/renderer/STLBinSaver.cpp

#${OBJECTDIR}/src/soap/soapC.o: src/soap/soapC.cpp 
#	${MKDIR} -p ${OBJECTDIR}/src/soap
#	${RM} "$@.d"
#	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/soap/soapC.o src/soap/soapC.cpp

#${OBJECTDIR}/src/soap/soapmy_USCOREdispatcherBindingService.o: src/soap/soapmy_USCOREdispatcherBindingService.cpp 
#	${MKDIR} -p ${OBJECTDIR}/src/soap
#	${RM} "$@.d"
#	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/soap/soapmy_USCOREdispatcherBindingService.o src/soap/soapmy_USCOREdispatcherBindingService.cpp

#${OBJECTDIR}/src/soap/stdsoap2.o: src/soap/stdsoap2.cpp 
#	${MKDIR} -p ${OBJECTDIR}/src/soap
#	${RM} "$@.d"
#	$(COMPILE.cc) -g -I/usr/local/include -I/usr/share/gsoap/import -I/usr/include/b64 -Isrc/http -Isrc/renderer -Isrc/soap -I/usr/include/ImageMagick -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/soap/stdsoap2.o src/soap/stdsoap2.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/renderer2

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
