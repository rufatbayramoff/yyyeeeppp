#!/bin/bash

rm -Rf ./cache/render/*
rm -Rf ./cache/model/edit_*.stl
#echo '/var/crashrender/core.%e.%p.%s.%t' | sudo tee /proc/sys/kernel/core_pattern 

# usage
#$1

# for running as service
$1 daemon 0.0.0.0 5556 8090 0
# http://10.102.0.90:5556/test.html

# for measure
rm -f  sample/common_test_rot.stl
#$1 rotate sample/q_test_tri.stl sample/common_test_rot.stl 0 0 10 2
