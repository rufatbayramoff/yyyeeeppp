/**
 * TODO:
 * 1) Change example to use offscreen rendering (pbuffer) so that it becomes a true commandline tool with now windows
 * 2) Make example work with other threading models than SingleThreaded
 * 3) Add support for autocapture to movies
 *
 */

#include <memory>
#include <cstdint>
#include <iostream>
#include <sstream>

#include <Magick++.h> 

#include <signal.h>


#include "RenderingFarm.h"
#include "Viewer360.h"
#include "CMDHandler.h"

//#include "myRendSoapServer.h"

void preventSIGPIPEHalt() {
    struct sigaction sa;
    sa.sa_handler = SIG_IGN;
    sa.sa_flags = 0;
    sigemptyset(&(sa.sa_mask));
    sigaction(SIGPIPE, &sa, 0);
}


//===============================================================
// MAIN
//
int main( int argc, char **argv )
{
    Magick::InitializeMagick(*argv);
    preventSIGPIPEHalt();    

    if (argc <=1 ) {
        std::cout << "USAGE 1: " << argv[0] << " daemon 10.102.0.117 5555 8089 3600" << std::endl;
        std::cout << " 10.102.0.117 = ip address to bind" << std::endl;
        std::cout << " 5555 = port for www viewer (http://)" << std::endl;
//        std::cout << " 8089 = port for renderer (soap)" << std::endl;
        std::cout << " 3600 = image cache www viewer lifetime (1 hour), 0 = no cache" << std::endl;
        std::cout << std::endl;    
        std::cout << "USAGE 2: " << std::endl;    
        std::cout << " measure sizes: (returns json)" << std::endl;    
        std::cout << "   " << argv[0] << " measure path/to/filename.stl" << std::endl;    
        std::cout << " rotate model: (result file has hidden marks, angles in degrees)" << std::endl;    
        std::cout << "   " << argv[0] << " rotate path/to/input/filename.stl path/to/result/filename.stl anglex angley anglez zoom" << std::endl;    
        return 0;
    }
    
    std::string command = argv[1];
    if (command != "daemon") {
        // std::cout << command << std::endl;    
        CMDHandler cmd(argc,argv);
        return cmd.exec();
    }    
    
    std::cout << "Render http://" << argv[2] << ":" << argv[3] << " keepalive=" << atoi(argv[5]) << std::endl;    
//    std::cout << "Soap " << argv[2] << ":" << argv[4] << std::endl;    
    std::cout << "Running..." << std::endl;    

    RenderingFarm Farm;// and work forever
    Viewer360 Viewer(&Farm, argv[2], atoi(argv[3]), atoi(argv[4])); // and run forever

    
//    myRendSoapServer mySoapService(&Farm);
//    mySoapService.go4ever(argv[2], atoi(argv[4]));

    std::cin.get();
    
    return 0;
}
