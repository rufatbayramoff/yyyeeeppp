/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   STLBinSaver.cpp
 * Author: asen
 * 
 * Created on 18 августа 2016 г., 14:46
 */

#include "STLBinSaver.h"

#include <osg/PositionAttitudeTransform>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>
#include <osgGA/OrbitManipulator>
#include <osgShadow/ShadowedScene>
#include <osgViewer/Renderer>
#include <osg/ShapeDrawable>
#include <osgText/Text3D>
#include <osgUtil/TriStripVisitor>
#include <osgUtil/SmoothingVisitor>
#include <osg/TriangleFunctor>

struct StlHeader
{
    char text[80] = "Saved from www.treatstock.com.";
    unsigned int numFacets;
};
const unsigned int sizeof_StlHeader = 84;
struct StlVector
{
    float x, y, z;
};
struct StlFacet
{
    StlVector normal;
    StlVector vertex[3];
    unsigned short color;
};
const unsigned int sizeof_StlFacet = 50;

struct PushPoints
{
    std::ofstream* m_stream;
    osg::Matrix m_mat;
    // bool m_dontSaveNormals;
    unsigned int numFacets;

    inline void operator () (const osg::Vec3& _v1, const osg::Vec3& _v2, const osg::Vec3& _v3, bool treatVertexDataAsTemporary)
    {
	this->operator()(_v1, _v2, _v3);
    }
    inline void operator () (const osg::Vec3& _v1, const osg::Vec3& _v2, const osg::Vec3& _v3) // , bool treatVertexDataAsTemporary
    {
        osg::Vec3 v1 = _v1 * m_mat;
        osg::Vec3 v2 = _v2 * m_mat;
        osg::Vec3 v3 = _v3 * m_mat;
        osg::Vec3 vV1V2 = v2 - v1;
        osg::Vec3 vV1V3 = v3 - v1;
        osg::Vec3 vNormal = vV1V2.operator ^(vV1V3);
        ////if (m_dontSaveNormals)
        ////    *m_stream << "facet normal 0 0 0" << std::endl;
        ////else
        //    *m_stream << "facet normal " << vNormal[0] << " " << vNormal[1] << " " << vNormal[2] << std::endl;
        //*m_stream << "outer loop" << std::endl;
        //*m_stream << "vertex " << v1[0] << " " << v1[1] << " " << v1[2] << std::endl;
        //*m_stream << "vertex " << v2[0] << " " << v2[1] << " " << v2[2] << std::endl;
        //*m_stream << "vertex " << v3[0] << " " << v3[1] << " " << v3[2] << std::endl;
        //*m_stream << "endloop" << std::endl;
        //*m_stream << "endfacet" << std::endl;
        StlFacet a;
        a.normal.x = vNormal.x();
        a.normal.y = vNormal.y();
        a.normal.z = vNormal.z();
        a.color = 0;
        a.vertex[0].x = v1.x();
        a.vertex[0].y = v1.y();
        a.vertex[0].z = v1.z();
        a.vertex[1].x = v2.x();
        a.vertex[1].y = v2.y();
        a.vertex[1].z = v2.z();
        a.vertex[2].x = v3.x();
        a.vertex[2].y = v3.y();
        a.vertex[2].z = v3.z();
        // *m_stream << a;
        m_stream->write((const char *)&a, 50 );// sizeof(a)); side effect - we cant use sizeof!
        numFacets++;
    }
};

STLBinSaver::STLBinSaver(std::string const & fout):
    osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ACTIVE_CHILDREN),
    counter(0) 
{
    m_fout = fout;
    m_f = new osgDB::ofstream(m_fout.c_str(),std::ofstream::binary );
    numFacets = 0;
    StlHeader a;
    // *m_f << a;
    m_f->write((const char *)&a, 84); // sizeof(a));
    
    // write header
}


STLBinSaver::~STLBinSaver() {
    // change header values
    m_f->seekp(0, std::ios_base::beg);
    StlHeader a;
    a.numFacets = numFacets;
    // *m_f << a;
    m_f->write((const char *)&a,  84); // sizeof(a));
    // close file
    m_f->close();
    delete m_f;
}

void STLBinSaver::save(osg::Node* node, std::string fileName) {
    // osgDB::writeNodeFile(*node, fileName);
    
    STLBinSaver binSaver(fileName);
    const_cast<osg::Node&>(*node).accept(binSaver);
    // if (createStlVisitor.getErrorString().empty())
    
}

void STLBinSaver::apply(osg::Geode& node)
{
    osg::Matrix mat = osg::computeLocalToWorld(getNodePath());

    //if (node.getName().empty())
    //    *m_f << "solid " << counter << std::endl;
    //else
    //    *m_f << "solid " << node.getName() << std::endl;

    for (unsigned int i = 0; i < node.getNumDrawables(); ++i)
    {
        osg::TriangleFunctor<PushPoints> tf;
        tf.m_stream = m_f;
        tf.m_mat = mat;
        tf.numFacets = 0;
        node.getDrawable(i)->accept(tf);
        numFacets += tf.numFacets;
    }

    //if (node.getName().empty())
    //    *m_f << "endsolid " << counter << std::endl;
    //else
    //    *m_f << "endsolid " << node.getName() << std::endl;

    //++counter;
    traverse(node);
}

// const std::string& STLBinSaver::getErrorString() const { return m_ErrorString; }

