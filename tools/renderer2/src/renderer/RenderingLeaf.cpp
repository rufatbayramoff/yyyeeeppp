/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RenderingLeaf.cpp
 * Author: asen
 * 
 * Created on 6 мая 2016 г., 16:03
 */

#include "RenderingLeaf.h"
#include "STLBinSaver.h"

#include <cmath>
#include <stdexcept>
#include <stdlib.h>

#include <osg/PositionAttitudeTransform>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>
#include <osgGA/OrbitManipulator>
#include <osgGA/MultiTouchTrackballManipulator>
#include <osgShadow/ShadowedScene>
#include <osgViewer/Renderer>
#include <osg/ShapeDrawable>
#include <osgText/Text3D>
#include <osgUtil/SmoothingVisitor>

#include <Magick++.h> 


/** Helper class*/
template<class T>
class FindTopMostNodeOfTypeVisitor : public osg::NodeVisitor
{
public:
    FindTopMostNodeOfTypeVisitor():
        osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN),
        _foundNode(0)
    {}
    
    void apply(osg::Node& node)
    {
        T* result = dynamic_cast<T*>(&node);
        if (result)
             _foundNode = result;
         else
             traverse(node);
     }
    
    T* _foundNode;
};

/** Convenience function*/
template<class T>
T* findTopMostNodeOfType(osg::Node* node)
{
    if (!node) return 0;

    FindTopMostNodeOfTypeVisitor<T> fnotv;
    node->accept(fnotv);
    
    return fnotv._foundNode;
}

/** Capture the frame buffer and write image to disk*/
class WindowCaptureCallback : public osg::Camera::DrawCallback
{
    RenderingLeaf * _leaf;
public:    
    WindowCaptureCallback(GLenum readBuffer, const std::string& name,  RenderingLeaf * leaf):
        _readBuffer(readBuffer),
        _fileName(name),
        _leaf(leaf)
        {
            _image = new osg::Image;
        }
    
    virtual void operator () (osg::RenderInfo& renderInfo) const
        {
            #if !defined(OSG_GLES1_AVAILABLE) && !defined(OSG_GLES2_AVAILABLE)
            glReadBuffer(_readBuffer);
            #else
            osg::notify(osg::NOTICE)<<"Error: GLES unable to do glReadBuffer"<<std::endl;
            #endif

            OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
            osg::GraphicsContext* gc = renderInfo.getState()->getGraphicsContext();
            if (gc->getTraits())
            {
                GLenum pixelFormat;

                if (gc->getTraits()->alpha)
                    pixelFormat = GL_RGBA;
                else 
                    pixelFormat = GL_RGB;
                
#if defined(OSG_GLES1_AVAILABLE) || defined(OSG_GLES2_AVAILABLE)
                 if (pixelFormat == GL_RGB)
                 {
                    GLint value = 0;
                    #ifndef GL_IMPLEMENTATION_COLOR_READ_FORMAT
                        #define GL_IMPLEMENTATION_COLOR_READ_FORMAT 0x8B9B
                    #endif
                    glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_FORMAT, &value);
                    if ( value != GL_RGB ||
                         value != GL_UNSIGNED_BYTE )
                    {
                        pixelFormat = GL_RGBA;//always supported
                    }
                 }
#endif
                int width = gc->getTraits()->width;
                int height = gc->getTraits()->height;

                // std::cout<<"Capture: size="<<width<<"x"<<height<<", format="<<(pixelFormat == GL_RGBA ? "GL_RGBA":"GL_RGB")<<std::endl;

                _image->readPixels(0, 0, width, height, pixelFormat, GL_UNSIGNED_BYTE);
            }
                
            if (!_fileName.empty())
            {
                // std::cout << "Writing to: " << _fileName << std::endl;
                osg::ref_ptr<osgDB::ReaderWriter::Options> op = new osgDB::ReaderWriter::Options(); 
                //op->setOptionString("JPEG_QUALITY 90");
                op->setOptionString("JPEG_QUALITY 90");
                osgDB::writeImageFile(*_image, _fileName, op);
            }

            sem_post(&(_leaf->renderReady));
            //std::cout << "...saved" << std::endl;

        }

protected:    
    GLenum                      _readBuffer;
    std::string                 _fileName;
    osg::ref_ptr<osg::Image>    _image;
    mutable OpenThreads::Mutex  _mutex;
};


/** Do Culling only while loading PagedLODs*/
class CustomRenderer : public osgViewer::Renderer
{
public:
    CustomRenderer(osg::Camera* camera) 
        : osgViewer::Renderer(camera),
          _cullOnly(true)
        {
        }

    /** Set flag to omit drawing in renderingTraversals */
    void setCullOnly(bool on) { _cullOnly = on; }

    virtual void operator () (osg::GraphicsContext* /*context*/)
        {
            if (_graphicsThreadDoesCull)
            {
                if (_cullOnly)
                    cull();
                else
                    cull_draw();
            }
        }

    virtual void cull()
        {
            osgUtil::SceneView* sceneView = _sceneView[0].get();
            if (!sceneView || _done ) return;
            
            updateSceneView(sceneView);
            
            osgViewer::View* view = dynamic_cast<osgViewer::View*>(_camera->getView());
            if (view) sceneView->setFusionDistance(view->getFusionDistanceMode(), view->getFusionDistanceValue());

            sceneView->inheritCullSettings(*(sceneView->getCamera()));
            sceneView->cull();
        }
    
    bool _cullOnly;
};
    


RenderingLeaf::RenderingLeaf() {
    sem_init(&renderReady, 1, 0);
    time(&activity);
    outFileName = "";
    // create & configure viewer
    char *argv1[] = {"renderer2", 
        "--filename", "webroot/tmp/img.jpg", 
        "--clear-color", "255,255,255", 
        "--window", "0", "0", "800", "600", 
        "--samples", "4", 
        "-O", "noTriStripPolygons",
        "./resource/q_test_tri.stl",    
        NULL};
    this->windoww = 800;
    this->windowh = 600;
    this->lastFoundGeodeInLoadedModel = NULL;
    int argc1 = sizeof(argv1) / sizeof(char*) - 1;
    osg::ArgumentParser arguments(&argc1, argv1);
    this->viewer = new osgViewer::Viewer(arguments);
    this->viewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
    osg::DisplaySettings::instance()->setNumOfDatabaseThreadsHint(1); 
    
    // add camera (to viewer)
    // osgGA::OrbitManipulator * ruchko = new osgGA::OrbitManipulator();
    osgGA::MultiTouchTrackballManipulator * ruchko = new osgGA::MultiTouchTrackballManipulator();
    this->viewer->setCameraManipulator( ruchko ); 
    osg::Vec3 eye = osg::Vec3(1,1,1); //  boundingSphere._center+osg::Vec3( r * sin(mul*azimuth ) * cos (mul * elevation), -1 * r * cos(mul*azimuth)* cos(mul * elevation), r * sin(mul * elevation));
    osg::Vec3 center = osg::Vec3(0,0,0); // boundingSphere._center;
    osg::Vec3 up = osg::Vec3(0.0f,0.0f,1.0f);
    this->viewer->getCameraManipulator()->setHomePosition(eye,center,up);  
    this->viewer->getCameraManipulator()->home(0); 

    // prepare texture
    //this->notexture = new osg::Texture2D;
    this->texture = new osg::Texture2D;
    this->texture->setDataVariance(osg::Object::DYNAMIC);
    this->texture->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
    this->texture->setFilter(osg::Texture::MAG_FILTER, osg::Texture::LINEAR);
    this->texture->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
    this->texture->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);
    this->texture->setWrap(osg::Texture::WRAP_R, osg::Texture::REPEAT);
    //this->texture->setWrap(osg::Texture::WRAP_Q, osg::Texture::REPEAT);
    this->texture->setResizeNonPowerOfTwoHint(false); 
    this->texture->setMaxAnisotropy(8.0f); 
    this->material = new osg::Material();
    //material->setEmission(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
    //material->setAmbient(osg::Material::FRONT,  osg::Vec4(0.3, 0.3, 0.3, 1));
    //this->material->setShininess(osg::Material::FRONT_AND_BACK, 25.0);
    this->material->setColorMode(osg::Material::AMBIENT_AND_DIFFUSE); 
    this->material->setDiffuse(osg::Material::FRONT_AND_BACK,  osg::Vec4(0.3, 0.9, 0.5, 1));
    this->texGen = new osg::TexGen(); // z-axis texurizer
    this->texGen->setPlane(osg::TexGen::S, osg::Plane(0.0, 0.0, 0.075, 0.5));
    this->texGen->setPlane(osg::TexGen::T, osg::Plane(0.0, 0.035, 0.0, 0.3));
    this->texGenSphere = new osg::TexGen(); // z-axis texurizer
    this->texGenSphere->setMode(osg::TexGen::SPHERE_MAP); // NORMAL_MAP     );
    this->texGenLinear = new osg::TexGen(); // z-axis texurizer
    this->texGenLinear->setMode(osg::TexGen::OBJECT_LINEAR  ); // NORMAL_MAP     );
    this->texGenLinear->setPlane(osg::TexGen::S, osg::Plane(0.03f,0.06f,0.0f,0.2f));
    this->texGenLinear->setPlane(osg::TexGen::T, osg::Plane(0.0f,0.06f,0.08f,0.2f));
    //this->texGenLinear->setPlane(osg::TexGen::R, osg::Plane(0.0f,0.2f,0.0f,0.0f));
    
    this->texenv = new osg::TexEnv; 
    texenv->setMode(osg::TexEnv::MODULATE); 

    // prepare
    osg::ref_ptr<CustomRenderer> customRenderer = new CustomRenderer(this->viewer->getCamera());
    this->viewer->getCamera()->setRenderer(customRenderer.get());
    this->viewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
    customRenderer->setCullOnly(false);

    // shadows
    this->sm = new osgShadow::SoftShadowMap; 
    int mapres = 4096; // 4096; // 1024;
    sm->setTextureSize(osg::Vec2s(mapres,mapres));
    //sm->setJitteringScale(16);
    //sm->setSoftnessWidth(0.01);
    sm->setAmbientBias(osg::Vec2(0.8,0.2));
    
    
    // stereo
    this->dsStereo = new osg::DisplaySettings(); 
    this->dsStereo->setStereoMode(osg::DisplaySettings::ANAGLYPHIC); // # Red-cyan ONLY !?!? 
    this->dsStereo->setStereo(true); 
    this->dsStereo->setEyeSeparation(0.09); // TODO different model sizes?
    //dsStereo->setScreenDistance(0.07);
    //dsStereo->setSplitStereoHorizontalSeparation(0);
    this->dsStereo->setSplitStereoAutoAdjustAspectRatio(true);
    this->viewer->setDisplaySettings(this->dsStereo);
    
    // prepare 
    this->pager = this->viewer->getDatabasePager();
    this->pager->setDoPreCompile(false);
    this->viewer->realize();

    this->lastTextureName = "";
    // TODO make environment and viewer
    this->area = 0;
    this->volume = 0;
    this->polygons = 0;
    this->minx=1e10;
    this->maxx=-1e10;
    this->miny=1e10;
    this->maxy=-1e10;
    this->minz=1e10;
    this->maxz=-1e10;
    this->w=0;
    this->h=0;
    this->l=0;
    
    this->polygonMode = new osg::PolygonMode;
    this->multiColorEnabled = true;

}

RenderingLeaf::RenderingLeaf(const RenderingLeaf& orig) {
    throw std::runtime_error("constructor RenderingLeaf(const RenderingLeaf& orig) prohibited.");
}

RenderingLeaf::~RenderingLeaf() {
    // TODO destroy viewer and clean up
    // delete this->viewer;
    //delete this->dsStereo;
    //delete this->sm;
    //delete this->texGen;
    sem_destroy(&renderReady);
}

void RenderingLeaf::openFile(std::string STLFileName) {
    // TODO cleanup model before
    // TODO if filename changed
    // TODO close opened before
    // TODO load new model
    // load & measure  model
    this->lock();
    time(&activity);
    
    //std::cout << " loading: " << STLFileName << std::endl;
    try {
        this->loadedModel = osgDB::readNodeFile(STLFileName,  new osgDB::Options("noTriStripPolygons"));
    } catch (...) {
        throw std::runtime_error("No stl file loaded: " + STLFileName);
    }
    if (!this->loadedModel) 
    {
        throw std::runtime_error("No stl file loaded1: " + STLFileName);
    }
    std::cout << "STL file loaded: " << STLFileName << std::endl;

    // const osg::BoundingSphere& boundingSphere= loadedModel->getBound();
    //std::cout << " calculating weight: " << STLFileName << std::endl;
    this->multiColorEnabled = true;
    this->boundingSphere= this->loadedModel->getBound();   
    this->calcWeight();
    
    if (this->w <0 || this->h <0 || this->l <0)  
    {
        throw std::runtime_error("Loaded file sizes less than zero" + STLFileName);
    }
    
    //std::cout << " preparing: " << STLFileName << std::endl; 
    // post-setup links to material and texture
    //material->setSpecular(osg::Material::FRONT,  osg::Vec4(0.2, 0.2, 0.2, 1));
    osg::StateSet *sphereStateSet = this->loadedModel->getOrCreateStateSet();
    sphereStateSet->ref();
//    sphereStateSet->setAttribute(this->material);
    
    // configure shadows
    this->scene = new osgShadow::ShadowedScene;
    const int ReceivesShadowTraversalMask = 0x1;
    const int CastsShadowTraversalMask = 0x2;
    this->scene->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
    this->scene->setCastsShadowTraversalMask(CastsShadowTraversalMask);    
    // configure light
    int uniqueLightNumber = 0;
    // чем бОльший процент приходится не перву цифру, тем бОлее резкие тени
    // чем больше сумма от единицы, тем больше пересвет
    this->lightTransform = this->addLight(osg::Vec3(3, 3, 3),uniqueLightNumber++,0.2,0.0); // ,0.7,0.3   0.9 0.4 

    // create plane
    double r = this->boundingSphere._radius * 4;
    // geode0
    this->geode = new osg::Geode(); 
    osg::Geometry* pyramidGeometry = new osg::Geometry();
    this->geode->addDrawable(pyramidGeometry); 
    osg::Vec3Array* pyramidVertices = new osg::Vec3Array;
    double rrr = r * 5;
    double hhh = minz; // - 0.2 * r;
    pyramidVertices->push_back( osg::Vec3( -1*rrr, -1*rrr, hhh) ); // front left this->boundingSphere._center ?
    pyramidVertices->push_back( osg::Vec3(  1*rrr, -1*rrr, hhh) ); // front left
    pyramidVertices->push_back( osg::Vec3(  1*rrr,  1*rrr, hhh) ); // front left
    pyramidVertices->push_back( osg::Vec3( -1*rrr,  1*rrr, hhh) ); // front left
    pyramidGeometry->setVertexArray( pyramidVertices ); 
    osg::DrawElementsUInt* pyramidFaceOne = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
    pyramidFaceOne->push_back(0);
    pyramidFaceOne->push_back(1);
    pyramidFaceOne->push_back(2);
    pyramidFaceOne->push_back(3);
    pyramidGeometry->addPrimitiveSet(pyramidFaceOne);
    osg::Material *material2 = new osg::Material();
    material2->setColorMode(osg::Material::AMBIENT_AND_DIFFUSE);
    material2->setDiffuse(osg::Material::FRONT_AND_BACK,  osg::Vec4(500, 500, 500, 1));
    osg::StateSet *psphereStateSet = this->geode->getOrCreateStateSet();
    psphereStateSet->ref();
    psphereStateSet->setAttribute(material2);
    this->geode->setNodeMask(ReceivesShadowTraversalMask);
    // geode1
    this->geode1 = new osg::Geode(); 
    osg::Geometry* pyramidGeometry1 = new osg::Geometry();
    this->geode1->addDrawable(pyramidGeometry1); 
    osg::Vec3Array* pyramidVertices1 = new osg::Vec3Array;
    rrr = r * 5;
    hhh = miny; // - 0.2 * r;
    pyramidVertices1->push_back( osg::Vec3( -1*rrr, hhh, -1*rrr) ); // front left this->boundingSphere._center ?
    pyramidVertices1->push_back( osg::Vec3(  1*rrr, hhh, -1*rrr) ); // front left
    pyramidVertices1->push_back( osg::Vec3(  1*rrr, hhh,  1*rrr) ); // front left
    pyramidVertices1->push_back( osg::Vec3( -1*rrr, hhh,  1*rrr) ); // front left
    pyramidGeometry1->setVertexArray( pyramidVertices1 ); 
    osg::DrawElementsUInt* pyramidFaceOne1 = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
    pyramidFaceOne1->push_back(0);
    pyramidFaceOne1->push_back(1);
    pyramidFaceOne1->push_back(2);
    pyramidFaceOne1->push_back(3);
    pyramidGeometry1->addPrimitiveSet(pyramidFaceOne1);
    osg::Material *material21 = new osg::Material();
    material21->setColorMode(osg::Material::AMBIENT_AND_DIFFUSE);
    material21->setDiffuse(osg::Material::FRONT_AND_BACK,  osg::Vec4(500, 500, 500, 1));
    osg::StateSet *psphereStateSet1 = this->geode1->getOrCreateStateSet();
    psphereStateSet1->ref();
    psphereStateSet1->setAttribute(material21);
    this->geode1->setNodeMask(ReceivesShadowTraversalMask);
    this->planeAdded = -1;
    
    //this->scene->addChild(this->geode);
    //this->scene->addChild(this->geode1);
    
    //osgUtil::SmoothingVisitor sv;
    //sv.setCreaseAngle(0);
    //loadedModel->accept(sv);
    
    //this->allowMultiColor(false);
    
    // all together
    this->scene->addChild(loadedModel);
    this->loadedModel->setNodeMask(CastsShadowTraversalMask);
    viewer->setSceneData(this->scene);
    
    this->lastTextureName = "";
    
    time(&activity); 
    this->unlock();
    std::cout << "STL file load finished: " << STLFileName << std::endl;
    
}


void RenderingLeaf::analysePrimSet(osg::PrimitiveSet*prset, osg::Vec3Array *verts) {
	unsigned int ic;
	unsigned int i2;
	//unsigned int nprim=0;
        //osg::notify(osg::WARN) << std::endl;
	//osg::notify(osg::WARN) << "Prim set type "<< prset->getMode() << " indices " << prset->getNumIndices() << std::endl;
	for (ic=0; ic<prset->getNumIndices(); ic++) { // NB the vertices are held in the drawable -
            double x = (* verts)[prset->index(ic)].x();
            double y = (* verts)[prset->index(ic)].y();
            double z = (* verts)[prset->index(ic)].z();
            // (* verts)[prset->index(ic)].set( x, y, z+1);
            if (x > maxx) maxx=x;
            if (x < minx) minx=x;
            if (y > maxy) maxy=y;
            if (y < miny) miny=y;
            if (z > maxz) maxz=z;
            if (z < minz) minz=z;
        }
	switch (prset->getMode()) {
            case osg::PrimitiveSet::TRIANGLES: // get vertices of triangle
		for (i2=0; i2<prset->getNumIndices()-2; i2+=3) {
                    this->polygons++;
                    double tmp = 0;
                    tmp = areaTriangle((* verts)[prset->index(i2  )].x(), (* verts)[prset->index(i2  )].y(), (* verts)[prset->index(i2  )].z(), 
                                               (* verts)[prset->index(i2+1)].x(), (* verts)[prset->index(i2+1)].y(), (* verts)[prset->index(i2+1)].z(), 
                                               (* verts)[prset->index(i2+2)].x(), (* verts)[prset->index(i2+2)].y(), (* verts)[prset->index(i2+2)].z());
                    if (tmp!=tmp) tmp = 0; // is_nan -> 0
                    this->area += tmp;
                    tmp = volumeTriangle((* verts)[prset->index(i2  )].x(), (* verts)[prset->index(i2  )].y(), (* verts)[prset->index(i2  )].z(), 
                                               (* verts)[prset->index(i2+1)].x(), (* verts)[prset->index(i2+1)].y(), (* verts)[prset->index(i2+1)].z(), 
                                               (* verts)[prset->index(i2+2)].x(), (* verts)[prset->index(i2+2)].y(), (* verts)[prset->index(i2+2)].z());
                    if (tmp!=tmp) tmp = 0; // is_nan -> 0
                    this->volume += tmp;
		}
            break;
            case osg::PrimitiveSet::TRIANGLE_STRIP: // look up how tristrips are coded
		for (i2=0; i2<prset->getNumIndices()-2; i2++) {
                    this->polygons++;
                    double tmp = 0;
                    tmp = areaTriangle((* verts)[prset->index(i2  )].x(), (* verts)[prset->index(i2  )].y(), (* verts)[prset->index(i2  )].z(), 
                                               (* verts)[prset->index(i2+1)].x(), (* verts)[prset->index(i2+1)].y(), (* verts)[prset->index(i2+1)].z(), 
                                               (* verts)[prset->index(i2+2)].x(), (* verts)[prset->index(i2+2)].y(), (* verts)[prset->index(i2+2)].z());
                    if (tmp!=tmp) tmp = 0; // is_nan -> 0
                    this->area += tmp;
                    tmp = (i2%2==0?1:-1) * 
                                volumeTriangle((* verts)[prset->index(i2  )].x(), (* verts)[prset->index(i2  )].y(), (* verts)[prset->index(i2  )].z(), 
                                               (* verts)[prset->index(i2+1)].x(), (* verts)[prset->index(i2+1)].y(), (* verts)[prset->index(i2+1)].z(), 
                                               (* verts)[prset->index(i2+2)].x(), (* verts)[prset->index(i2+2)].y(), (* verts)[prset->index(i2+2)].z());
                    if (tmp!=tmp) tmp = 0; // is_nan -> 0
                    this->volume += tmp;
		}
            break;
            //case osg::PrimitiveSet::TRIANGLE_FAN: // look up how tristrips are coded
            //	osg::notify(osg::WARN) << "Triangle_fan "  << std::endl;
            //break;
            default:
                osg::notify(osg::WARN) << "BZZ UNKNOWN primitive " << prset->getMode() << std::endl;
	// etc for all the primitive types you expect. EG quads, quadstrips lines line loops....
	}
}


inline double distance3d(double x1, double y1, double z1, double x2, double y2, double z2){
    return sqrt( (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2) + (z1-z2)*(z1-z2) );
}
inline double heron(double a, double b, double c){
    double s = (a + b + c) / 2;
    return sqrt( s*(s-a) * (s-b)*(s-c) );
}
double areaTriangle(double x1, double y1, double z1, double x2, double y2, double z2,double x3, double y3, double z3){
    double a=distance3d(x1,y1,z1,x2,y2,z2);
    double b=distance3d(x2,y2,z2,x3,y3,z3);
    double c=distance3d(x3,y3,z3,x1,y1,z1);
    return heron(a,b,c);  
}
double volumeTriangle(double x1, double y1, double z1, double x2, double y2, double z2,double x3, double y3, double z3) {
    // vcross = cross_product(p1,p2) 
    // vo=p1=xyz2 v1=p2=xyz2
    // v0[0]=x2 v0[1] = y2 v0[2] = z2 
    // v1[0]=x3 v1[1] = y3 v1[2] = z3 
    // x =   v0[1]*v1[2] - v0[2]*v1[1]
    double x = y2*z3 - z2*y3;
    // y = -(v0[0]*v1[2] - v0[2]*v1[0])
    double y = -(x2*z3 - z2*x3);
    // z =   v0[0]*v1[1] - v0[1]*v1[0]
    double z =   x2*y3 - y2*x3;
    // vcross = return [x,y,z]
    // vdot = dot_product(p0, vcross)
    // p0[0]=x1 p0[1]=y1 p0[2]=z1 vcross[0]=x vcross[1]=y vcross[2]=z
    // vec = [v0[n]*v1[n] for n in range(len(v0))]
    double vec = x1 * x + y1 * y + z1 * z;
    // vdot return sum(vec)
    // vol = vdot/6
    return vec / 6;    
}


void RenderingLeaf::analyseGeode(osg::Geode *geode) {
    this->lastFoundGeodeInLoadedModel = geode;
    for (unsigned int i=0; i<geode->getNumDrawables(); i++) {
		osg::Drawable *drawable=geode->getDrawable(i);
		osg::Geometry *geom=dynamic_cast<osg::Geometry *> (drawable);
		for (unsigned int ipr=0; ipr<geom->getNumPrimitiveSets(); ipr++) {
			osg::PrimitiveSet* prset=geom->getPrimitiveSet(ipr);
			// this->analysePrimSet(prset, dynamic_cast<const osg::Vec3Array*>(geom->getVertexArray()));
			this->analysePrimSet(prset, dynamic_cast<osg::Vec3Array*>(geom->getVertexArray()));
		}
    }
}

void RenderingLeaf::analyse(osg::Node *nd) {
	/// here you have found a group.
    osg::Geode *geode = dynamic_cast<osg::Geode *> (nd);
	if (geode) { // analyse the geode. If it isnt a geode the dynamic cast gives NULL.
		this->analyseGeode(geode);
	} else {
		osg::Group *gp = dynamic_cast<osg::Group *> (nd);
		if (gp) {
			// osg::notify(osg::WARN) << "Group "<<  gp->getName() <<std::endl;
			for (unsigned int ic=0; ic<gp->getNumChildren(); ic++) {
				this->analyse(gp->getChild(ic));
			}
		} else {
			osg::notify(osg::WARN) << "Unknown node "<<  nd <<std::endl;
		}
	}
}
// divide the geode into its drawables and primitivesets:


void RenderingLeaf::calcWeight() {
    //osg::Vec3Array* vertices = new osg::Vec3Array(4);
    //osg::Geometry* geo = this->loadedModel->getDrawable( 0 )->asGeometry(); // 0? better visitor!
    //osg::Vec3Array* vertices = geo->getVertexArray();
    //int numFaces = geo->getNumPrimitiveSets();
    //std::cout << "faces: " << numFaces << std::endl;
    this->area = 0;
    this->volume = 0;
    this->minx=1e10;
    this->maxx=-1e10;
    this->miny=1e10;
    this->maxy=-1e10;
    this->minz=1e10;
    this->maxz=-1e10;
    this->w=0;
    this->h=0;
    this->l=0;
    analyse(this->loadedModel.get());
    this->w = this->maxx - this->minx;
    this->h = this->maxy - this->miny;
    this->l = this->maxz - this->minz;
    osg::notify(osg::WARN) << "Area:  " << this->area << " Volume:  " << this->volume << " size:  w=" << this->w << "xH=" << this->h << "xL=" << this->l   <<std::endl;

}

void RenderingLeaf::rotateGeode(osg::Geode *geode,osg::Quat q, double zoom) {
    for (unsigned int i=0; i<geode->getNumDrawables(); i++) {
		osg::Drawable *drawable=geode->getDrawable(i);
		osg::Geometry *geom=dynamic_cast<osg::Geometry *> (drawable);
                osg::Vec3Array *verts = dynamic_cast<osg::Vec3Array*>(geom->getVertexArray());
                for (int ic = 0; ic < verts->capacity(); ic++) {
                    osg::Vec3d vec = (* verts)[ic];
                    //double x = (* verts)[ic].x();
                    //double y = (* verts)[ic].y();
                    //double z = (* verts)[ic].z();
                    // TODO calc  real rotation
                    //(* verts)[ic].set( x, z, y);
                    (* verts)[ic].set(q * vec * zoom);
                }
    }
}
void RenderingLeaf::rotateNode(osg::Node *nd,osg::Quat q, double zoom) {
	/// here you have found a group.
    osg::Geode *geode = dynamic_cast<osg::Geode *> (nd);
	if (geode) { // analyse the geode. If it isnt a geode the dynamic cast gives NULL.
		this->rotateGeode(geode,q,  zoom);
	} else {
		osg::Group *gp = dynamic_cast<osg::Group *> (nd);
		if (gp) {
			for (unsigned int ic=0; ic<gp->getNumChildren(); ic++) {
				this->rotateNode(gp->getChild(ic),q,  zoom);
			}
		} else {
			osg::notify(osg::WARN) << "Unknown node "<<  nd <<std::endl;
		}
	}
}

void RenderingLeaf::rotateZoom(double xAngle, double yAngle, double zAngle, double zoom) {
    // rotate
    osg::Quat q = osg::Quat( xAngle, osg::Vec3d(zoom,0,0), 
                    yAngle, osg::Vec3d(0,zoom,0), 
                    zAngle, osg::Vec3d(0,0,zoom)); 
    // osg::Vec3d rotated = vector * q; 
    // osg::notify(osg::WARN) << "Zoom: "<<  zoom <<std::endl;
    this->rotateNode(this->loadedModel.get(),q,  zoom);
    
    // TODO NORMALIZE above zero!!!
    
    // recale weights
    this->calcWeight();
}

void RenderingLeaf::saveStl(std::string stlName) {
    // osgDB::writeNodeFile(*this->loadedModel, stlName);
    STLBinSaver::save(this->loadedModel.get(), stlName);
    // TODO save as PLY
    
}

osg::PositionAttitudeTransform*  RenderingLeaf::addLight(osg::Vec3 pos,int uniqueLightNumber,double power,double powerambient) {
	osg::StateSet *lightStateSet = this->scene->getOrCreateStateSet();
	lightStateSet->ref();
	
	// osg::LightModel* lightmodel = new osg::LightModel; 
        this->lightmodel = new osg::LightModel; 
        this->lightmodel->setTwoSided(true); // deprecated
	this->lightmodel->setAmbientIntensity(osg::Vec4(powerambient,powerambient,powerambient,1.0f)); 
	lightStateSet->setAttributeAndModes(this->lightmodel, osg::StateAttribute::ON); 

	osg::PositionAttitudeTransform *lightTransform;	
	lightTransform = new osg::PositionAttitudeTransform();
        // osg::Light *light = new osg::Light();
        this->light = new osg::Light();
	// each light must have a unique number
	this->light->setLightNum(uniqueLightNumber);
	// we set the light's position via a PositionAttitudeTransform object
	this->light->setPosition(osg::Vec4(-0, -0, 0, 1.0));
	this->light->setDiffuse(osg::Vec4(power, power, power, 1.0));// color
        //double power2 = 0;
	//light->setSpecular(osg::Vec4(power2, power2, power2, 1.0));
	//light->setAmbient(osg::Vec4(power2, power2, power2, 0.00001));
	// create a light
	osg::LightSource *lightSource = new osg::LightSource();
	lightSource->setLight(this->light);
	// enable the light for the entire scene
	lightSource->setLocalStateSetModes(osg::StateAttribute::ON);
	lightSource->setStateSetModes(*lightStateSet, osg::StateAttribute::ON);
	// dd child
	lightTransform->addChild(lightSource);
	lightTransform->setPosition(pos);
	//lightTransform->setPosition(Vec3(3*r, 3*r,3*r));
	this->scene->addChild(lightTransform);         
	return lightTransform;	
};


osg::Vec4 RenderingLeaf::colorStringToVec(std::string ColorInHex, double power = 1.0) {
    // TODO
    float cr = std::stoi("0x"+ColorInHex.substr(0,2), NULL, 16) / 256.0f;
    float cg = std::stoi("0x"+ColorInHex.substr(2,2), NULL, 16) / 256.0f;
    float cb = std::stoi("0x"+ColorInHex.substr(4,2), NULL, 16) / 256.0f;
    return osg::Vec4(cr, cg, cb, power);
}

void RenderingLeaf::setBackgroundColor(std::string ColorInHex) {
    viewer->getCamera()->setClearColor(this->colorStringToVec(ColorInHex));
}

void RenderingLeaf::renderToFile(std::string STLFileName) {
    // TODO
    GLenum buffer = viewer->getCamera()->getGraphicsContext()->getTraits()->doubleBuffer ? GL_BACK : GL_FRONT;
    this->viewer->getCamera()->setFinalDrawCallback(new WindowCaptureCallback(buffer, STLFileName.c_str(), this));   
 
    // multi-windows https://www.google.ru/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&ved=0ahUKEwiYnImvy53MAhVMSZoKHZGEC0gQFgg6MAQ&url=http%3A%2F%2Fwww.openscenegraph.org%2Fprojects%2Fosg%2Fwiki%2FSupport%2FFAQ&usg=AFQjCNF9kTr6VbvGBxSAoTwfWTXdNHHnUg&sig2=8MSUwwR2s7G26Brl_efADg #sceneview1->getState()->setContextID(1);
    this->viewer->frame();// really need this?
    // Keep updating and culling until full level of detail is reached
    while(!this->viewer->done() && this->pager->getRequestsInProgress())
    {
        this->viewer->updateTraversal();
        this->viewer->renderingTraversals();
    }
    // rename((RootDir + "/tmp/img.jpg").c_str(), (RootDir + newPath).c_str());   
    //std::cout << "...wait" << std::endl;
    sem_wait(&renderReady);
    //std::cout << "...waitok" << std::endl;
    time(&activity);
}

bool RenderingLeaf::isExpired() {
    time_t now;
    time(&now);
    double seconds = difftime(now,activity);
    std::cout << "inactivity: " << seconds << std::endl;
    return (seconds > 30); // seconds inactivity -  
}

void RenderingLeaf::allowMultiColor(bool allow) {
    // std::cout << "multicolor: " << allow << std::endl;
    if (this->multiColorEnabled != allow) {
        this->multiColorEnabled = allow;
     
    //osg::Vec4 m_color;
    //m_color.set( 1.0, 1.0, 1.0, 1.0 );
    //osg::ref_ptr< osg::Vec4Array > m_colorArrays;
    //m_colorArrays = new osg::Vec4Array;
    //m_colorArrays->push_back( m_color );
     
    unsigned int numGeoms = this->lastFoundGeodeInLoadedModel->getNumDrawables();
    for( unsigned int geodeIdx = 0; geodeIdx < numGeoms; geodeIdx++ ) {
        osg::Geometry *curGeom =this->lastFoundGeodeInLoadedModel->getDrawable( geodeIdx )->asGeometry();
        if ( curGeom ) {
           std::cout << "geometry foud: " << geodeIdx << std::endl;
           osg::Vec4Array *colorArrays = dynamic_cast< osg::Vec4Array *>(curGeom->getColorArray());
           if ( colorArrays ) {
                //std::cout << "color array for set @@@@@@@@@@@@2 d" << std::endl;
                if (allow) {
                    curGeom->setColorBinding( osg::Geometry::BIND_PER_VERTEX ); // BIND_PER_VERTEX
                } else {
                    curGeom->setColorBinding( osg::Geometry::BIND_OFF ); // BIND_PER_VERTEX
                }
                //for ( unsigned int i = 0; i < colorArrays->size(); i++ ) {
                //    osg::Vec4 *color = &colorArrays->operator [](i);
                //    color->set( m_color._v[0], m_color._v[1], m_color._v[2], m_color._v[3]);
                //}
            } else {    
               //curGeom->setColorBinding( osg::Geometry::BIND_OFF  );            
               //std::cout << "color arr set " << std::endl;
               //curGeom->setColorArray( m_colorArrays.get());
               //curGeom->setColorBinding( osg::Geometry::BIND_OVERALL ); // BIND_PER_VERTEX   BIND_OVERALL    BIND_OFF       
            }                  
           curGeom->setUseVertexBufferObjects(false);
           curGeom->dirtyDisplayList();
        }
    }
    // delete m_colorArrays;
    }
}

void RenderingLeaf::setTextureFile(std::string JPGFileName, int matcap = 0){
    // TODO clean up ?!
    // load texture, material, color (to model)
    osg::StateSet *sphereStateSet = this->loadedModel->getOrCreateStateSet();
    sphereStateSet->ref();
    // std::cout << "txture: " << JPGFileName << " was " << this->lastTextureName << " leafname:" << this->outFileName << std::endl;
    if (JPGFileName != "") {
        
        if (JPGFileName == "resource/wireframe") { 
            sphereStateSet->getTextureAttributeList().clear(); 
            sphereStateSet->getTextureModeList().clear(); 
            this->polygonMode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE);
            this->lastTextureName = "";
        } else if (JPGFileName == "resource/points") {
            sphereStateSet->getTextureAttributeList().clear(); 
            sphereStateSet->getTextureModeList().clear(); 
            this->polygonMode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::POINT);
            this->lastTextureName = "";
        } else { 
            this->polygonMode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::FILL);
            if (this->lastTextureName != JPGFileName) {
                osg::Image *image = osgDB::readImageFile(JPGFileName);
                if (!image) {
                    throw std::runtime_error("No texture file loaded: " + JPGFileName);
                }
                this->texture->setImage(image);  
                this->lastTextureName = JPGFileName;
                //std::cout << "txture INSTALLED: " << JPGFileName << std::endl;
                this->allowMultiColor(false);
            };
            sphereStateSet->setTextureAttributeAndModes(0, this->texture,  osg::StateAttribute::ON  ); //| osg::StateAttribute::OVERRIDE);
            if (matcap == 1) {
                sphereStateSet->setTextureAttributeAndModes(0, this->texGenSphere);
            } else if (matcap == 3) {
                sphereStateSet->setTextureAttributeAndModes(0, this->texGenLinear);
                // sphereStateSet->setTextureMode(0,GL_TEXTURE_GEN_R,osg::StateAttribute::ON);
                //sphereStateSet->setTextureMode(0,GL_TEXTURE_2D,osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);
                //sphereStateSet->setTextureMode(0,GL_TEXTURE_GEN_S,osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);
                //sphereStateSet->setTextureMode(0,GL_TEXTURE_GEN_T,osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);
            } else {
                sphereStateSet->setTextureAttributeAndModes(0, this->texGen);
                sphereStateSet->setTextureAttribute(0,this->texenv,osg::StateAttribute::ON); 

            }
        }; 
        sphereStateSet->setAttributeAndModes( this->polygonMode, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON );    
        // stateset->removeAttribute(osg::StateAttribute::POLYGONMODE); 

    } else {
        this->allowMultiColor(true);
        this->polygonMode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::FILL);
        sphereStateSet->setAttributeAndModes( this->polygonMode, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON );    
        sphereStateSet->getTextureAttributeList().clear(); 
        sphereStateSet->getTextureModeList().clear(); 
         this->lastTextureName = "";
    };
    
    // semi-transparent glass
    // sphereStateSet->setMode(GL_BLEND, osg::StateAttribute::ON); 
    // ? geometry->dirtyDisplayList(); or geometry->setUseDisplayList(false); 
}

void RenderingLeaf::setColorDiffuse(std::string ColorInHex, double lightLevel = 1.0, double Sharpness = 0.7) {
    osg::StateSet *sphereStateSet = this->loadedModel->getOrCreateStateSet();
    sphereStateSet->ref();
    double power;// = lightLevel * Sharpness;
    double powerambient;// = lightLevel * (1 - Sharpness);

    if (ColorInHex == "mmmmmm") {
        this->material->setColorMode(osg::Material::AMBIENT_AND_DIFFUSE); 
        this->material->setDiffuse(osg::Material::FRONT_AND_BACK,  this->colorStringToVec("ffff00", 1));
        // this->material->setSpecular(osg::Material::FRONT,  this->colorStringToVec(ColorInHex));
        this->material->setAmbient(osg::Material::FRONT_AND_BACK,  this->colorStringToVec("ffff00", 1));
        sphereStateSet->setAttribute(this->material);
        //sphereStateSet->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE|osg::StateAttribute::ON); 
        sphereStateSet->setMode(GL_CULL_FACE, osg::StateAttribute::OVERRIDE|osg::StateAttribute::OFF); 
        power = lightLevel * Sharpness;
        powerambient = lightLevel * (1 - Sharpness);
        this->light->setDiffuse(osg::Vec4(1, 1, 0, 1.0));// color
        this->lightmodel->setAmbientIntensity(osg::Vec4(0,1,1,1.0f)); 
    } else if (ColorInHex != "") {
        this->material->setColorMode(osg::Material::AMBIENT_AND_DIFFUSE); 
        this->material->setDiffuse(osg::Material::FRONT_AND_BACK,  this->colorStringToVec(ColorInHex, 0.5));
        // this->material->setSpecular(osg::Material::FRONT,  this->colorStringToVec(ColorInHex));
        this->material->setAmbient(osg::Material::FRONT_AND_BACK,  this->colorStringToVec(ColorInHex, 0.5));
        sphereStateSet->setAttribute(this->material);
        //sphereStateSet->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE|osg::StateAttribute::ON); 
        sphereStateSet->setMode(GL_CULL_FACE, osg::StateAttribute::OVERRIDE|osg::StateAttribute::OFF); 
        power = lightLevel * Sharpness;
        powerambient = lightLevel * (1 - Sharpness);
        this->light->setDiffuse(osg::Vec4(power, power, power, 1.0));// color
        this->lightmodel->setAmbientIntensity(osg::Vec4(powerambient,powerambient,powerambient,1.0f)); 
    }  else {
        // TODO clear attribute!?
        sphereStateSet->removeAttribute(this->material);
        power = lightLevel * Sharpness;
        powerambient = lightLevel * (1 - Sharpness);
        //power = 0.2;
        //powerambient = 0.1;
        this->light->setDiffuse(osg::Vec4(power, power, power, 1.0));// color
        this->lightmodel->setAmbientIntensity(osg::Vec4(powerambient,powerambient,powerambient,1.0f)); 
    }
}

void RenderingLeaf::clup(float x, float y, std::string text) {
    std::cout << "Clup at " << x << " " << y <<  " :\n";
    
    osgUtil::LineSegmentIntersector* intersector = new osgUtil::LineSegmentIntersector( osgUtil::Intersector::PROJECTION, x , y );
    osgUtil::IntersectionVisitor iv( intersector );
    osg::Camera* camera = this->viewer->getCamera();
    if( !camera ) return;

    camera->accept( iv );    
    if( !intersector->containsIntersections() )  return ;

    auto intersections = intersector->getIntersections();
    std::cout << "Got " << intersections.size() << " intersections:\n";
    if (intersections.size() > 0) {
        osgUtil::LineSegmentIntersector::Intersection intersection = intersector->getFirstIntersection();
        osg::Vec3d eye, center, up;
        this->viewer->getCameraManipulator()->getHomePosition(eye, center, up);
        if (text == "") {
            this->addSphere(intersection.localIntersectionPoint, 0);
        } else if (text == "1.png") {
            this->stampImage(intersection.localIntersectionPoint, "resource/1.png");
        } else {
            this->addText(intersection.localIntersectionPoint, intersection.getLocalIntersectNormal(), text);
        };
        
        // 
    };
}

void RenderingLeaf::stampImage(osg::Vec3 pos, std::string imageName) {
    Magick::Image image;
    image.read( imageName);
    int columns=image.columns();
    int rows=image.rows();
    Magick::PixelPacket *aPixels = image.getPixels(0,0,columns,rows);

    float cubeSize = this->boundingSphere._radius /columns;
    double mul = 2.0f * M_PI / 360;
    osg::Quat q = osg::Quat( (90 - this->lastElevation) * mul, osg::Vec3d(1,0,0), // this->lastElevation,0,0
                    this->lastAzimuth * mul, osg::Vec3d(0,0,1), 
                    0, osg::Vec3d(0,0,1)); 

    
    for(ssize_t i=0; i<columns; i++)
     for(ssize_t j=0; j<rows; j++) {
        float a = ( aPixels[i*rows+j].red + aPixels[i*rows+j].green + aPixels[i*rows+j].blue ) / ( 256.0f*256 * 3);
        // std::cout << a << std::endl;
        if (a < 0.9) { // 90% white - no
            // this->addCube(pos + q*osg::Vec3(cubeSize*(j-rows/2), cubeSize*(columns/2-i), 0 /* cubeSize*a */ ), cubeSize*1.01);
            osg::Box* cube = new osg::Box(pos + q*osg::Vec3(cubeSize*(j-rows/2), cubeSize*(columns/2-i), 0), cubeSize);
            cube->setRotation(q);
            osg::ShapeDrawable* sd = new osg::ShapeDrawable(cube);
            this->lastFoundGeodeInLoadedModel->addDrawable(sd);
        };
    }    
}


void RenderingLeaf::addText(osg::Vec3 pos, osg::Vec3 cam, std::string text) {
    osg::ref_ptr<osgText::Font> font = osgText::readRefFontFile("resource/brush.ttf");
    osg::ref_ptr<osgText::Style> style = new osgText::Style;
    double thickness = 0.1f;
    style->setThicknessRatio(0.1f); // thickness
    osgText::Text3D* text3D = new osgText::Text3D;
    text3D->setFont(font.get());
    text3D->setAlignment(osgText::TextBase::AlignmentType::CENTER_CENTER);
    text3D->setStyle(style.get());
    text3D->setCharacterSize(10);
    text3D->setDrawMode(osgText::Text3D::TEXT); //  | osgText::Text3D::BOUNDINGBOX
    text3D->setAxisAlignment(osgText::Text3D::XZ_PLANE);
    text3D->setText(text);    
    //osg::Quat q = osg::Quat( cam.x(), osg::Vec3d(1,0,0), // this->lastElevation,0,0
    //                cam.y(), osg::Vec3d(0,1,0), 
    //                cam.z(), osg::Vec3d(0,0,1)); 
    double mul = 2.0f * M_PI / 360;
    osg::Quat q = osg::Quat( (90 - this->lastElevation) * mul, osg::Vec3d(1,0,0), // this->lastElevation,0,0
                    this->lastAzimuth * mul, osg::Vec3d(0,0,1), 
                    0, osg::Vec3d(0,0,1)); 
    
    double r = this->boundingSphere._radius * 4;
    osg::Vec3 eye = this->boundingSphere._center+osg::Vec3( r * sin(mul*this->lastAzimuth ) * cos (mul * this->lastElevation), -1 * r * cos(mul*this->lastAzimuth)* cos(mul * this->lastElevation), r * sin(mul * this->lastElevation));
    double thicknessReal = text3D->getBoundingBox().yMax();
    osg::Vec3 bias = pos - eye;
    bias.normalize();
    
    text3D->setRotation(q);
    text3D->setPosition(pos - bias * thicknessReal * 0.5);
    // text3D->setColor(color);
    this->lastFoundGeodeInLoadedModel->addDrawable(text3D);   
    /*
    osg::ref_ptr<osg::MatrixTransform> transform = new osg::MatrixTransform;
    const double angle = 0.8;
    const osg::Vec3d axis(0, 0, 1);
    transform->setMatrix(osg::Matrix::rotate(angle, axis));
    transform->addChild(text3D);
    this->lastFoundGeodeInLoadedModel->addChild(transform);   
     * */
}

void RenderingLeaf::addCube(osg::Vec3 pos, double rrr) {
        if (rrr == 0) rrr = this->w / 50;
        osg::ShapeDrawable* sd = new osg::ShapeDrawable(new osg::Box(osg::Vec3(pos), rrr));
        this->lastFoundGeodeInLoadedModel->addDrawable(sd);   
        
}

void RenderingLeaf::addSphere(osg::Vec3 pos, double rrr) {
        if (rrr == 0)  rrr = this->w / 50;
        osg::ShapeDrawable* sd = new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(pos), rrr));
        this->lastFoundGeodeInLoadedModel->addDrawable(sd);   
        
}


void RenderingLeaf::setShadows(bool on) {
    // TODO
    if (on) {
 	this->scene->setShadowTechnique(this->sm.get());
    } else {
 	this->scene->setShadowTechnique(NULL);
    }    
}

void RenderingLeaf::setStereoMode(int mode) {
    // TODO off shadows nd different modes
    switch (mode) {
        case RenderingLeaf::MODE_NO_STEREO:
            this->dsStereo->setStereo(false); 
            break;
        case RenderingLeaf::MODE_STEREO_ANAGLYPHIC:
            this->dsStereo->setStereoMode(osg::DisplaySettings::ANAGLYPHIC);
            this->dsStereo->setStereo(true); 
            break;
        default:
            throw std::runtime_error("unknown stereo mode");
    }
}

void RenderingLeaf::expire() {
    activity = 0;
}

void RenderingLeaf::setCameraPosition(int azimuth, int elevation, int crene, int plateposition){
    // camera
    double r = this->boundingSphere._radius * 4;
    double mul = 2.0f * M_PI / 360;
    osg::Vec3 eye, up,lightpos;
    double lightazimuth = azimuth + 25;
    
    if (plateposition==0) {
        // z axes plate
        eye = this->boundingSphere._center+osg::Vec3( r * sin(mul*azimuth ) * cos (mul * elevation), -1 * r * cos(mul*azimuth)* cos(mul * elevation), r * sin(mul * elevation));
        up = osg::Vec3(0.0f,0.0f,1.0f); // TODO crene
        lightpos = this->boundingSphere._center+osg::Vec3( r * sin(mul*lightazimuth ) * cos (mul * elevation), -1 * r * cos(mul*lightazimuth)* cos(mul * elevation), r * sin(mul * elevation));
    } else {
        // y axes plate
        eye = this->boundingSphere._center+osg::Vec3( r * sin(mul*azimuth ) * cos (mul * elevation), r * sin(mul * elevation), 1 * r * cos(mul*azimuth)* cos(mul * elevation));
        up = osg::Vec3(0.0f,1.0f,0.0f); // TODO crene
        lightpos = this->boundingSphere._center+osg::Vec3( r * sin(mul*lightazimuth ) * cos (mul * elevation), r * sin(mul * elevation), 1 * r * cos(mul*lightazimuth)* cos(mul * elevation));
    }
    osg::Quat creneq = osg::Quat( /*pitch*/0, osg::Vec3(1,0,0), 
                                  /*roll*/0, osg::Vec3(0,1,0), 
                                  /*yaw*/ mul * crene, eye); 
    up = creneq * up;
    
    /*
    osg::Vec3 eye = osg::Vec3(r,0.0f,0.0f); // TODO crene
    osg::Vec3 up = osg::Vec3(0.0f,0.0f,1.0f); // TODO crene
    osg::Quat q = osg::Quat( mul*azimuth, osg::Vec3d(1,0,0), 
                    mul*elevation, osg::Vec3d(0,1,0), 
                    mul*crene, osg::Vec3d(0,0,1)); 
    eye = q * eye;
    up = q * up;*/

    
    osg::Vec3 center = this->boundingSphere._center;
    this->viewer->getCameraManipulator()->setHomePosition(eye,center,up);  
    this->viewer->getCameraManipulator()->home(0); 
    // light
    // if (elevation > 0 && elevation < 45) elevation = 45;
    this->lightTransform->setPosition(lightpos);
    //this->lightTransform->setPosition(this->boundingSphere._center+q * osg::Vec3(r,r/10,0.0f));

    this->lastAzimuth = azimuth;
    this->lastElevation = elevation;
    this->lastCrene = crene;
    
}

void RenderingLeaf::setPlane(bool on ,int plateposition){
    if (!on) plateposition = -1;
    if (this->planeAdded != plateposition) {
        //std::cout << " plane removed: " << this->planeAdded << std::endl; 
        if (this->planeAdded==0)  this->scene->removeChild(this->geode);
        if (this->planeAdded==1)  this->scene->removeChild(this->geode1);
        //std::cout << " plane added: " << plateposition << std::endl; 
        if (plateposition==0)  this->scene->addChild(this->geode);
        if (plateposition==1)  this->scene->addChild(this->geode1);
    }
    this->planeAdded=plateposition;
}

bool RenderingLeaf::lock(){
    // TODO
    this->Mtx.lock();
    return true;
}

void RenderingLeaf::unlock(){
    // TODO
    this->Mtx.unlock();
}


