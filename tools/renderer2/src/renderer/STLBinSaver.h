/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   STLBinSaver.h
 * Author: asen
 *
 * Created on 18 августа 2016 г., 14:46
 */

#ifndef STLBINSAVER_H
#define STLBINSAVER_H

#include <osgViewer/Viewer>
#include <osg/Material>
#include <osg/TexGen>
#include <osgShadow/SoftShadowMap>
#include <osg/LightModel>
#include <osg/PolygonMode>

class STLBinSaver : public osg::NodeVisitor {
public:
    STLBinSaver(std::string const & fout);
    virtual ~STLBinSaver();
    
    static void save(osg::Node* node, std::string fileName);

    virtual void apply(osg::Geode& node);
    
private:
        int counter;
        std::ofstream* m_f;
        std::string m_fout;
        std::string m_fout_ext;
        std::string m_ErrorString;
        unsigned int numFacets;

};

#endif /* STLBINSAVER_H */

