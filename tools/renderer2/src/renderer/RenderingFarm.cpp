/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RenderingFarm.cpp
 * Author: asen
 * 
 * Created on 6 мая 2016 г., 16:02
 */

#include "RenderingFarm.h"

#include <pcrecpp.h> 
#include <sys/stat.h>
#include <iostream>
#include <algorithm>


RenderingFarm::RenderingFarm() {
    // TODO create leafs hash 
    windowUsed = 0;
}

RenderingFarm::RenderingFarm(const RenderingFarm& orig) {
    // TODO prohibit
}

RenderingFarm::~RenderingFarm() {
    // TODO destroy all leafs
}

bool RenderingFarm::mayRender(std::string STLFileName) {
    if (! this->nameIsSafe(STLFileName)) return false; // error, file name prohibited
    if (! this->fileExists(DEFAULT_PREFIX_MODEL + STLFileName)) return false; // error, not existing file prohibited
    return true;
}

RenderingLeaf* RenderingFarm::getUnused() {
    for (auto& kv : leafs) 
        if (kv.second->isExpired()) {
            std::cout << "expiring: " << kv.first << std::endl;
            RenderingLeaf *expired = kv.second;
            leafs.erase(kv.first);
            return expired;
        }
    return NULL;
}

RenderingLeaf* RenderingFarm::getBestUsed() {
    time_t best;
    time(&best);
    RenderingLeaf *bestLeaf = NULL;
    std::string bestLeafKey = "unexistent";
    for (auto& kv : leafs) {
        RenderingLeaf *expired = kv.second;
    	double seconds = difftime(best,expired->activity);
	if (seconds<0) {
		best = expired->activity;
		bestLeaf = expired;
		bestLeafKey = kv.first;
        };
    }
    if (bestLeaf!=NULL) {
            std::cout << "force expiring: " << bestLeafKey << std::endl;
            bestLeaf->expire();
            leafs.erase(bestLeafKey);
            return bestLeaf;
        }
    return NULL;
}




RenderingLeaf * RenderingFarm::getActiveLeaf(std::string STLFileName){
    return this->getActiveLeafAbsolute(STLFileName, DEFAULT_PREFIX_MODEL);
}
RenderingLeaf * RenderingFarm::getActiveLeafAbsolute(std::string STLFileName,std::string prefix) {
    if (leafs.find(STLFileName) == leafs.end()) {
        RenderingLeaf * leaf1 = this->getUnused();
        if (leaf1 == NULL) { 
	    if (windowUsed > FARM_WINDOW_LIMIT) {
		 leaf1 = this->getBestUsed();
		 if (leaf1 == NULL) {
            	     std::cout << "window limit reach! cant process: " << STLFileName << std::endl;
            	     return NULL;
                 };
	    } else {
		windowUsed++;
                leaf1 = new RenderingLeaf();
                std::cout << "New leaf created" << STLFileName << std::endl;
	    };
        };
        try {
            leafs[STLFileName] = leaf1;
            leaf1->openFile(prefix + STLFileName);
        } catch (...) {
            std::cout << "catch error while openFile on: " << STLFileName << std::endl;
            leaf1->expire();
            leafs.erase(STLFileName);
            leafs[STLFileName+"thrown"] = leaf1;
            leaf1->unlock();
            return NULL;
        }
    }
    return leafs[STLFileName];
}


std::string RenderingFarm::getRenderedFile(std::string STLFileName, bool checkFile, std::string HEXColor, int azimuth, int elevation, int crene, int mode3d, std::string textureFile, int matcap, double overLight, bool allowCache, int plateposition){
    // check safe & exists
    if (checkFile && !this->mayRender(STLFileName)) return "";
    std::cout << "RenderedFile: " << STLFileName << std::endl;
    // try to find result render file in cache 
    //int azimuth = -25;
    //int elevation = 25;
    // nt crene = 0;
    std::string textureFileEscaped = textureFile;
    std::replace( textureFileEscaped.begin(), textureFileEscaped.end(), '/', '_'); 
    std::replace( textureFileEscaped.begin(), textureFileEscaped.end(), '.', '_'); 
    std::string params = HEXColor + "_" + std::to_string(azimuth) + "_" + std::to_string(elevation) + "_" + std::to_string(crene) + "_" + std::to_string(mode3d) + "_" + textureFileEscaped + "_"+ std::to_string(matcap) + "_" + std::to_string(overLight) + "_" + std::to_string(plateposition); //  + "_" + std::to_string(textureFile); 
    std::string resultName = this->getStandartOutputName(STLFileName, params);
    if (this->fileExists(resultName) && allowCache) {
        // wait for file to be ready, if it exists
        if (leafs.find(STLFileName) != leafs.end()) {
            RenderingLeaf * leaf1 = leafs[STLFileName];
            if (leaf1->outFileName == resultName) {
                // the same file, wait!
                if (leaf1->lock()) leaf1->unlock();
            };
        }
        std::cout << "Return as cache: " << STLFileName << std::endl;
        return resultName;
    }
    std::cout << "We need to render: " << STLFileName << std::endl;
    // ohh no, we need to render...
    
    // create new leaf if not found
    /*if (leafs.find(STLFileName) == leafs.end()) {
        RenderingLeaf * leaf1 = this->getUnused();
        // canr reuse - create new
        if (leaf1 == NULL) leaf1 = new RenderingLeaf();
        leafs[STLFileName] = leaf1;
        
        leaf1->openFile(DEFAULT_PREFIX_MODEL + STLFileName);
        // TODO reuse???
    }*/
    RenderingLeaf * leaf; // leafs[STLFileName];
    try {
        leaf = this->getActiveLeaf(STLFileName); // leafs[STLFileName];
        std::cout << "We have leaf for: " << STLFileName << std::endl;
        // render
        if (leaf != NULL && leaf->lock()) {
            try {           
                std::cout << "Leaf exists: " << STLFileName << std::endl;
                bool formatMulticolor =  (this->getValidModelExtension(STLFileName) == ".ply");
                //std::cout << "texturing: " << STLFileName << std::endl;
                leaf->outFileName = resultName;
                leaf->setTextureFile(textureFile, matcap);
                double brightness = 1 + overLight;
                double ambient = 0.7;
                if (textureFile == "") { // ply? formatMulticolor ??
                    brightness = 1 + overLight;
                    ambient = 0.1;
                }
                // std::cout << "brightness: " << brightness << std::endl;
                leaf->setColorDiffuse(HEXColor, brightness, ambient);
                leaf->setCameraPosition(azimuth,elevation,crene, plateposition );
                leaf->setStereoMode(mode3d); 
                leaf->setShadows( (mode3d == RenderingLeaf::MODE_NO_STEREO) );
                leaf->setPlane( (elevation >20 && mode3d == RenderingLeaf::MODE_NO_STEREO), plateposition );   

                // std::cout << " rendering: " << STLFileName << std::endl; 
                std::cout << "Before render to file: " << STLFileName << std::endl;
                leaf->renderToFile(resultName);
                std::cout << "After render to file: " << STLFileName << std::endl;
            } catch(std::exception const & e) {       
                //your code (probably logging related code)
                resultName = "";    
            }
            leaf->unlock();
            std::cout << "Leaf processed: " << STLFileName << std::endl;

            // added to cache by default
            return resultName;
        };
    } catch (...) {
        std::cout << "strange error with leafs operations: " << STLFileName << std::endl;
        return "";
    }
    return ""; // cant lock?!
}


std::string RenderingFarm::rotateZoom(std::string STLFileName,double xAngle, double yAngle, double zAngle, double zoom) {
    RenderingLeaf * leaf = this->getActiveLeaf(STLFileName); //leafs[STLFileName];
    //std::string newname = STLFileName;
    leafs.erase(STLFileName);

    std::string tmpname2 = tempnam("cache/model/","rot_");
    tmpname2 +=  ".stl"; //farm1->getValidModelExtension(ns1__RendRotated->id);
     std::string newname = tmpname2.substr(12,9999);
    std::cout << "Rotated name: " << tmpname2 << " justname:" << newname << std::endl;
    leaf->outFileName = newname;
    leafs[newname+"willbeexpired"] = leaf;
    std::cout << "leaf inserting: " << newname << std::endl << std::flush;
    leaf->rotateZoom(xAngle,yAngle,zAngle,zoom);
    leaf->saveStl(tmpname2);
    leaf->expire();
    return newname;
}


std::string RenderingFarm::getStandartOutputName(std::string STLFileName, std::string params){
    return DEFAULT_PREFIX_RENDER + STLFileName + "_" + params + ".png";   
//    return DEFAULT_PREFIX_RENDER + STLFileName + "_" + params + ".jpg";
}

std::string RenderingFarm::getRenderedRequest(std::string request){
    // TODO parse request to fileid, colors etc.
    // TODO file must be in store
    // TODO try to find result in cache
    //   TODO not found - _render & cache
    return "";
}

std::string RenderingFarm::realRender(std::string STLFileName, std::string HEXColor) {
    // try to find leaf with this file
    // if not exist - create
    // clean up very old leafs?
    return "";
}

bool RenderingFarm::fileExists(std::string realFileName) {
    try {
        struct stat buffer;   
        return (stat (realFileName.c_str(), &buffer) == 0); 
    }catch(std::exception &e){
        return false;
    }        
}

bool RenderingFarm::nameIsSafe(std::string STLFileName) {
    if (STLFileName == "") return false;
    pcrecpp::RE re("([\\a-zA-Z01-9_]+)\\.stl");
    pcrecpp::RE re2("([\\a-zA-Z01-9_]+)\\.ply");
    pcrecpp::RE re3("([\\a-zA-Z01-9_]+)\\.jpg");
    pcrecpp::RE re4("([\\a-zA-Z01-9_]+)\\.png");
    pcrecpp::RE re5("([a-zA-Z01-9_]+)");
    return re.FullMatch(STLFileName) || re2.FullMatch(STLFileName)|| re3.FullMatch(STLFileName)|| re4.FullMatch(STLFileName)|| re5.FullMatch(STLFileName);
}

std::string RenderingFarm::getValidModelExtension(std::string FileName) {
    if (FileName == "") return ".stl";
    pcrecpp::RE re2("([\\a-zA-Z01-9_]+)\\.ply");
    return (re2.FullMatch(FileName)? ".ply":".stl") ;
}

