/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RenderingLeaf.h
 * Author: asen
 *
 * Created on 6 мая 2016 г., 16:03
 */

#ifndef RENDERINGLEAF_H
#define RENDERINGLEAF_H

#include <osgViewer/Viewer>
#include <osg/Material>
#include <osg/TexGen>
#include <osg/TexEnv>
#include <osgShadow/SoftShadowMap>
#include <osg/LightModel>
#include <osg/PolygonMode>

#include <time.h> 
#include <mutex>
#include <string>
#include <semaphore.h>


class RenderingLeaf {
public:
    RenderingLeaf();
    RenderingLeaf(const RenderingLeaf& orig);
    virtual ~RenderingLeaf();
    
    void openFile(std::string STLFileName);

    void setBackgroundColor(std::string ColorInHex);
    void setPlane(bool on, int plateposition);
    void setShadows(bool on);
    void setStereoMode(int mode);
    
    void setColorDiffuse(std::string ColorInHex, double lightLevel, double Sharpness);
    void setTextureFile(std::string JPGFileName, int matcap);
    void allowMultiColor(bool allow);

    void setCameraPosition(int azimuth, int elevation, int crene, int plateposition);
    void renderToFile(std::string PNGFileName);
    
    bool isExpired();
    void expire();
    
    // to have thread-safe operations in transaction
    bool lock();
    void unlock();
    
    void calcWeight();
    
    void rotateNode(osg::Node *nd,osg::Quat q, double zoom);
    void rotateGeode(osg::Geode *geode,osg::Quat q, double zoom);
    void rotateZoom(double xAngle, double yAngle, double zAngle, double zoom);
    void saveStl(std::string stlName);
    
    void clup(float x, float y, std::string text);
    void addCube(osg::Vec3 pos, double rrr);
    void addSphere(osg::Vec3 pos, double rrr);
    void stampImage(osg::Vec3 pos, std::string imageName);
    void addText(osg::Vec3 pos, osg::Vec3 cam, std::string text); 
    
    const static int MODE_NO_STEREO = 0;
    const static int MODE_STEREO_ANAGLYPHIC = 1;
    
    double area;
    double volume;
    int polygons;
    double minx, maxx, miny, maxy, minz, maxz;
    double w,h,l;
    int windoww,windowh;

    sem_t renderReady;
    std::string outFileName;
    time_t activity;

private:
    osg::Vec4 colorStringToVec(std::string ColorInHex, double power); 
    
    osg::PositionAttitudeTransform*  addLight(osg::Vec3 pos,int uniqueLightNumber,double power,double powerambient); 

    osgViewer::Viewer *viewer;
    osg::ref_ptr<osg::Node> loadedModel;
    osg::BoundingSphere boundingSphere;
    osg::ref_ptr<osg::Texture2D> texture;
    // osg::Texture2D *notexture;
    osg::ref_ptr<osg::Material> material;
    osg::ref_ptr<osg::TexGen> texGen;
    osg::ref_ptr<osg::TexGen> texGenSphere;
    osg::ref_ptr<osg::TexGen> texGenLinear;
    osg::ref_ptr<osg::TexEnv> texenv;
    osgDB::DatabasePager* pager;
    osg::DisplaySettings *dsStereo;
    osg::ref_ptr<osgShadow::SoftShadowMap> sm;
    osg::PositionAttitudeTransform *lightTransform;
    osg::ref_ptr<osgShadow::ShadowedScene> scene;
    // osg::Geode* geode;
    osg::ref_ptr<osg::Geode> geode;
    osg::ref_ptr<osg::Geode> geode1;
    int planeAdded = -1;
    osg::ref_ptr<osg::LightModel> lightmodel;
    osg::ref_ptr<osg::Light> light;
    osg::ref_ptr<osg::PolygonMode> polygonMode;
    osg::Geode* lastFoundGeodeInLoadedModel;
    
    std::mutex Mtx;
    std::string lastTextureName;
    
    void analyse(osg::Node *nd);
    void analyseGeode(osg::Geode *geode);
    void analysePrimSet(osg::PrimitiveSet*prset, osg::Vec3Array *verts);
    
    double lastAzimuth = 0;
    double lastElevation = 0;
    double lastCrene = 0;
    bool multiColorEnabled = true;

    
};

inline double distance3d(double x1, double y1, double z1, double x2, double y2, double z2);
inline double heron(double a, double b, double c);
double areaTriangle(double x1, double y1, double z1, double x2, double y2, double z2,double x3, double y3, double z3);
double volumeTriangle(double x1, double y1, double z1, double x2, double y2, double z2,double x3, double y3, double z3);


#endif /* RENDERINGLEAF_H */

