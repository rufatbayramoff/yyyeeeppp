/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RenderingFarm.h
 * Author: asen
 *
 * Created on 6 мая 2016 г., 16:02
 */

#ifndef RENDERINGFARM_H
#define RENDERINGFARM_H

#include <string>
#include <map>
#include "RenderingLeaf.h"

#define DEFAULT_PREFIX_MODEL "cache/model/"
#define DEFAULT_PREFIX_RENDER "cache/render/"
#define FARM_WINDOW_LIMIT 50

class RenderingFarm {
public:
    RenderingFarm();
    RenderingFarm(const RenderingFarm& orig);
    virtual ~RenderingFarm();
    
    // thread-safe functions
    bool nameIsSafe(std::string STLFileName);
    std::string getValidModelExtension(std::string FileName);
    bool mayRender(std::string STLFileName);
    std::string getRenderedFile(std::string STLFileName, bool checkFile, std::string HEXColor, int azimuth, int elevation, int crene, int mode3d, std::string textureFile, int matcap, double overLight, bool allowCache, int plateposition);
    std::string getRenderedRequest(std::string request);
    RenderingLeaf * getActiveLeaf(std::string STLFileName);
    RenderingLeaf * getActiveLeafAbsolute(std::string STLFileName,std::string prefix);
    std::string rotateZoom(std::string STLFileName,double xAngle, double yAngle, double zAngle, double zoom); // rename and return new leaf name

    
    RenderingLeaf* getUnused();
    RenderingLeaf* getBestUsed();

private:
    std::string realRender(std::string STLFileName, std::string HEXColor);
    bool fileExists(std::string STLFileName);
    std::string getStandartOutputName(std::string STLFileName, std::string params);

    std::map<std::string, RenderingLeaf*> leafs;
    int windowUsed;
   
};

#endif /* RENDERINGFARM_H */

