/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CMDHandler.h
 * Author: asen
 *
 * Created on 18 августа 2016 г., 16:57
 */

#ifndef CMDHANDLER_H
#define CMDHANDLER_H

#include <osgViewer/Viewer>
#include <osg/Material>
#include <osg/TexGen>
#include <osgShadow/SoftShadowMap>
#include <osg/LightModel>
#include <osg/PolygonMode>

#include <string>

class CMDHandler {
public:
    CMDHandler(int argc1,char **argv1);
    int exec();
    virtual ~CMDHandler();

    double area;
    double volume;
    int polygons;
    double minx, maxx, miny, maxy, minz, maxz;
    double w,h,l;
    void calcWeight();
    void rotateNode(osg::Node *nd,osg::Quat q, double zoom);
    void rotateGeode(osg::Geode *geode,osg::Quat q, double zoom);
    void rotateZoom(double xAngle, double yAngle, double zAngle, double zoom);
    void analyse(osg::Node *nd);
    void analyseGeode(osg::Geode *geode);
    void analysePrimSet(osg::PrimitiveSet*prset, osg::Vec3Array *verts);
    void saveStl(std::string stlName);

private:
    int argc;
    char** argv;
    osg::ref_ptr<osg::Node> loadedModel;
    
    static inline double distance3d(double x1, double y1, double z1, double x2, double y2, double z2);
    static inline double heron(double a, double b, double c);
    static double areaTriangle(double x1, double y1, double z1, double x2, double y2, double z2,double x3, double y3, double z3);
    static double volumeTriangle(double x1, double y1, double z1, double x2, double y2, double z2,double x3, double y3, double z3);
    
};

#endif /* CMDHANDLER_H */

