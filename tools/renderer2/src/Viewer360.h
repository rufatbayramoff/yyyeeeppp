/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Viewer360.h
 * Author: asen
 *
 * Created on 12 мая 2016 г., 17:31
 */

#ifndef VIEWER360_H
#define VIEWER360_H

#include <string>
#include "RenderingFarm.h"
#include "http_server.h"
#include <mutex>


class Viewer360 {
public:
    Viewer360(RenderingFarm* farm, std::string bindAdress, std::uint16_t bindPort, int cacheLifeTime);
    Viewer360(const Viewer360& orig);
    virtual ~Viewer360();
private:
    Network::HttpServer * Srv;
    RenderingFarm * farm1;
    int cacheLifeTime;
//    std::mutex Mtx;

};

#endif /* VIEWER360_H */

