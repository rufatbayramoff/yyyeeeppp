/* soapmy_USCOREdispatcherBindingService.cpp
   Generated by gSOAP 2.8.16 from RendererQuery.h

Copyright(C) 2000-2013, Robert van Engelen, Genivia Inc. All Rights Reserved.
The generated code is released under one of the following licenses:
GPL or Genivia's license for commercial use.
This program is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
*/

#include "soapmy_USCOREdispatcherBindingService.h"

my_USCOREdispatcherBindingService::my_USCOREdispatcherBindingService()
{	this->soap = soap_new();
	this->own = true;
	my_USCOREdispatcherBindingService_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
}

my_USCOREdispatcherBindingService::my_USCOREdispatcherBindingService(struct soap *_soap)
{	this->soap = _soap;
	this->own = false;
	my_USCOREdispatcherBindingService_init(_soap->imode, _soap->omode);
}

my_USCOREdispatcherBindingService::my_USCOREdispatcherBindingService(soap_mode iomode)
{	this->soap = soap_new();
	this->own = true;
	my_USCOREdispatcherBindingService_init(iomode, iomode);
}

my_USCOREdispatcherBindingService::my_USCOREdispatcherBindingService(soap_mode imode, soap_mode omode)
{	this->soap = soap_new();
	this->own = true;
	my_USCOREdispatcherBindingService_init(imode, omode);
}

my_USCOREdispatcherBindingService::~my_USCOREdispatcherBindingService()
{	if (this->own)
		soap_free(this->soap);
}

void my_USCOREdispatcherBindingService::my_USCOREdispatcherBindingService_init(soap_mode imode, soap_mode omode)
{	soap_imode(this->soap, imode);
	soap_omode(this->soap, omode);
	static const struct Namespace namespaces[] =
{
	{"SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/", "http://www.w3.org/*/soap-envelope", NULL},
	{"SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/", "http://www.w3.org/*/soap-encoding", NULL},
	{"xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL},
	{"xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL},
	{"ns1", "http://treatstock.com/sample.wsdl", NULL, NULL},
	{NULL, NULL, NULL, NULL}
};
	soap_set_namespaces(this->soap, namespaces);
};

void my_USCOREdispatcherBindingService::destroy()
{	soap_destroy(this->soap);
	soap_end(this->soap);
}

void my_USCOREdispatcherBindingService::reset()
{	destroy();
	soap_done(this->soap);
	soap_initialize(this->soap);
	my_USCOREdispatcherBindingService_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
}

#ifndef WITH_PURE_VIRTUAL
my_USCOREdispatcherBindingService *my_USCOREdispatcherBindingService::copy()
{	my_USCOREdispatcherBindingService *dup = SOAP_NEW_COPY(my_USCOREdispatcherBindingService);
	if (dup)
		soap_copy_context(dup->soap, this->soap);
	return dup;
}
#endif

int my_USCOREdispatcherBindingService::soap_close_socket()
{	return soap_closesock(this->soap);
}

int my_USCOREdispatcherBindingService::soap_force_close_socket()
{	return soap_force_closesock(this->soap);
}

int my_USCOREdispatcherBindingService::soap_senderfault(const char *string, const char *detailXML)
{	return ::soap_sender_fault(this->soap, string, detailXML);
}

int my_USCOREdispatcherBindingService::soap_senderfault(const char *subcodeQName, const char *string, const char *detailXML)
{	return ::soap_sender_fault_subcode(this->soap, subcodeQName, string, detailXML);
}

int my_USCOREdispatcherBindingService::soap_receiverfault(const char *string, const char *detailXML)
{	return ::soap_receiver_fault(this->soap, string, detailXML);
}

int my_USCOREdispatcherBindingService::soap_receiverfault(const char *subcodeQName, const char *string, const char *detailXML)
{	return ::soap_receiver_fault_subcode(this->soap, subcodeQName, string, detailXML);
}

void my_USCOREdispatcherBindingService::soap_print_fault(FILE *fd)
{	::soap_print_fault(this->soap, fd);
}

#ifndef WITH_LEAN
#ifndef WITH_COMPAT
void my_USCOREdispatcherBindingService::soap_stream_fault(std::ostream& os)
{	::soap_stream_fault(this->soap, os);
}
#endif

char *my_USCOREdispatcherBindingService::soap_sprint_fault(char *buf, size_t len)
{	return ::soap_sprint_fault(this->soap, buf, len);
}
#endif

void my_USCOREdispatcherBindingService::soap_noheader()
{	this->soap->header = NULL;
}

const SOAP_ENV__Header *my_USCOREdispatcherBindingService::soap_header()
{	return this->soap->header;
}

int my_USCOREdispatcherBindingService::run(int port)
{	if (soap_valid_socket(this->soap->master) || soap_valid_socket(bind(NULL, port, 100)))
	{	for (;;)
		{	if (!soap_valid_socket(accept()) || serve())
				return this->soap->error;
			soap_destroy(this->soap);
			soap_end(this->soap);
		}
	}
	else
		return this->soap->error;
	return SOAP_OK;
}

SOAP_SOCKET my_USCOREdispatcherBindingService::bind(const char *host, int port, int backlog)
{	return soap_bind(this->soap, host, port, backlog);
}

SOAP_SOCKET my_USCOREdispatcherBindingService::accept()
{	return soap_accept(this->soap);
}

#if defined(WITH_OPENSSL) || defined(WITH_GNUTLS)
int my_USCOREdispatcherBindingService::ssl_accept()
{	return soap_ssl_accept(this->soap);
}
#endif

int my_USCOREdispatcherBindingService::serve()
{
#ifndef WITH_FASTCGI
	unsigned int k = this->soap->max_keep_alive;
#endif
	do
	{

#ifndef WITH_FASTCGI
		if (this->soap->max_keep_alive > 0 && !--k)
			this->soap->keep_alive = 0;
#endif

		if (soap_begin_serve(this->soap))
		{	if (this->soap->error >= SOAP_STOP)
				continue;
			return this->soap->error;
		}
		if (dispatch() || (this->soap->fserveloop && this->soap->fserveloop(this->soap)))
		{
#ifdef WITH_FASTCGI
			soap_send_fault(this->soap);
#else
			return soap_send_fault(this->soap);
#endif
		}

#ifdef WITH_FASTCGI
		soap_destroy(this->soap);
		soap_end(this->soap);
	} while (1);
#else
	} while (this->soap->keep_alive);
#endif
	return SOAP_OK;
}

static int serve___ns1__RendRotated(my_USCOREdispatcherBindingService*);
static int serve___ns1__Rend(my_USCOREdispatcherBindingService*);

int my_USCOREdispatcherBindingService::dispatch()
{	my_USCOREdispatcherBindingService_init(this->soap->imode, this->soap->omode);
	soap_peek_element(this->soap);
	if (!soap_match_tag(this->soap, this->soap->tag, "ns1:RendRotated"))
		return serve___ns1__RendRotated(this);
	if (!soap_match_tag(this->soap, this->soap->tag, "ns1:Rend"))
		return serve___ns1__Rend(this);
	return this->soap->error = SOAP_NO_METHOD;
}

static int serve___ns1__RendRotated(my_USCOREdispatcherBindingService *service)
{	struct soap *soap = service->soap;
	struct __ns1__RendRotated soap_tmp___ns1__RendRotated;
	_ns1__RendRotatedResponse ns1__RendRotatedResponse;
	ns1__RendRotatedResponse.soap_default(soap);
	soap_default___ns1__RendRotated(soap, &soap_tmp___ns1__RendRotated);
	if (!soap_get___ns1__RendRotated(soap, &soap_tmp___ns1__RendRotated, "-ns1:RendRotated", NULL))
		return soap->error;
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap->error;
	soap->error = service->RendRotated(soap_tmp___ns1__RendRotated.ns1__RendRotated, &ns1__RendRotatedResponse);
	if (soap->error)
		return soap->error;
	soap->encodingStyle = NULL;
	soap_serializeheader(soap);
	ns1__RendRotatedResponse.soap_serialize(soap);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || ns1__RendRotatedResponse.soap_put(soap, "ns1:RendRotatedResponse", "")
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	};
	if (soap_end_count(soap)
	 || soap_response(soap, SOAP_OK)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || ns1__RendRotatedResponse.soap_put(soap, "ns1:RendRotatedResponse", "")
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap->error;
	return soap_closesock(soap);
}

static int serve___ns1__Rend(my_USCOREdispatcherBindingService *service)
{	struct soap *soap = service->soap;
	struct __ns1__Rend soap_tmp___ns1__Rend;
	_ns1__RendResponse ns1__RendResponse;
	ns1__RendResponse.soap_default(soap);
	soap_default___ns1__Rend(soap, &soap_tmp___ns1__Rend);
	if (!soap_get___ns1__Rend(soap, &soap_tmp___ns1__Rend, "-ns1:Rend", NULL))
		return soap->error;
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap->error;
	soap->error = service->Rend(soap_tmp___ns1__Rend.ns1__Rend, &ns1__RendResponse);
	if (soap->error)
		return soap->error;
	soap->encodingStyle = NULL;
	soap_serializeheader(soap);
	ns1__RendResponse.soap_serialize(soap);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || ns1__RendResponse.soap_put(soap, "ns1:RendResponse", "")
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	};
	if (soap_end_count(soap)
	 || soap_response(soap, SOAP_OK)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || ns1__RendResponse.soap_put(soap, "ns1:RendResponse", "")
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap->error;
	return soap_closesock(soap);
}
/* End of server object code */
