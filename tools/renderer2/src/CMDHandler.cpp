/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CMDHandler.cpp
 * Author: asen
 * 
 * Created on 18 августа 2016 г., 16:57
 */

#include "CMDHandler.h"
#include "STLBinSaver.h"


#include <cmath>
#include <stdexcept>
#include <stdlib.h>

#include <osg/PositionAttitudeTransform>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>
#include <osgGA/OrbitManipulator>
#include <osgShadow/ShadowedScene>
#include <osgViewer/Renderer>
#include <osg/ShapeDrawable>
#include <osgText/Text3D>

CMDHandler::CMDHandler(int argc1,char **argv1) {
    argc = argc1;
    argv = argv1;
    this->area = 0;
    this->volume = 0;
    this->polygons = 0;
    this->minx=1e10;
    this->maxx=-1e10;
    this->miny=1e10;
    this->maxy=-1e10;
    this->minz=1e10;
    this->maxz=-1e10;
    this->w=0;
    this->h=0;
    this->l=0;    
}

int CMDHandler::exec() {
    if (argc < 2) {
        std::cout << "Unknown command" << std::endl;    
        return 1; // must be at least 1 arg - filename 
    }
    std::string command = argv[1];

    if (command != "measure" && command != "rotate") {
        std::cout << "Unknown command" << std::endl;    
        return 1; // must be at least 1 arg - filename 
    }
    
    if (command == "measure" || command == "rotate") {
        // 
        std::string infilename = argv[2];
        try {
            this->loadedModel = osgDB::readNodeFile(infilename,  new osgDB::Options("noTriStripPolygons"));
        } catch (...) {
            // throw std::runtime_error("No stl file loaded: " + STLFileName);
            std::cout << "{\"success\": \"false\",\"errortext\": \"can not read file\"}" << std::endl;
            return 1;
        }
        if (!this->loadedModel) 
        {
            std::cout << "{\"success\": \"false\",\"errortext\": \"loaded model invalid\"}" << std::endl;
            return 1;
        }
        this->calcWeight();
    }

    if (command == "rotate") {
        try {
         std::string outfile = argv[3];
         double xangle = atof(argv[4]) * M_PI / 180.0;
         double yangle = atof(argv[5]) * M_PI / 180.0;
         double zangle = atof(argv[6]) * M_PI / 180.0;
         double zoom =   atof(argv[7]);
         this->rotateZoom(xangle, yangle, zangle, zoom);
         this->saveStl(outfile);
        } catch (...) {
         std::cout << "{\"success\": \"false\",\"errortext\": \"cannnot rotate\"}" << std::endl;   
        }
    }
    
    
    if (command == "measure" || command == "rotate") {
       char buffer [1000];
       sprintf(buffer, "{\"success\": \"true\",\"width\": %g, \"height\": %g, \"length\": %g, \"area\": %g, \"volume\": %g, \"polygons\": %d,\"command\":\"%s\"}", 
               this->w, this->h, this->l, 
               this->area, this->volume, this->polygons, 
               command.c_str());
       std::cout << buffer << std::endl;  
    }
    
    
    return 0;
}

CMDHandler::~CMDHandler() {
}

void CMDHandler::calcWeight() {
    this->area = 0;
    this->volume = 0;
    this->minx=1e10;
    this->maxx=-1e10;
    this->miny=1e10;
    this->maxy=-1e10;
    this->minz=1e10;
    this->maxz=-1e10;
    this->w=0;
    this->h=0;
    this->l=0;
    analyse(this->loadedModel.get());
    this->w = this->maxx - this->minx;
    this->h = this->maxy - this->miny;
    this->l = this->maxz - this->minz;
}


void CMDHandler::analyseGeode(osg::Geode *geode) {
    // this->lastFoundGeodeInLoadedModel = geode;
    for (unsigned int i=0; i<geode->getNumDrawables(); i++) {
		osg::Drawable *drawable=geode->getDrawable(i);
		osg::Geometry *geom=dynamic_cast<osg::Geometry *> (drawable);
		for (unsigned int ipr=0; ipr<geom->getNumPrimitiveSets(); ipr++) {
			osg::PrimitiveSet* prset=geom->getPrimitiveSet(ipr);
			// this->analysePrimSet(prset, dynamic_cast<const osg::Vec3Array*>(geom->getVertexArray()));
			this->analysePrimSet(prset, dynamic_cast<osg::Vec3Array*>(geom->getVertexArray()));
		}
    }
}

void CMDHandler::analyse(osg::Node *nd) {
	/// here you have found a group.
    osg::Geode *geode = dynamic_cast<osg::Geode *> (nd);
	if (geode) { // analyse the geode. If it isnt a geode the dynamic cast gives NULL.
		this->analyseGeode(geode);
	} else {
		osg::Group *gp = dynamic_cast<osg::Group *> (nd);
		if (gp) {
			// osg::notify(osg::WARN) << "Group "<<  gp->getName() <<std::endl;
			for (unsigned int ic=0; ic<gp->getNumChildren(); ic++) {
				this->analyse(gp->getChild(ic));
			}
		} else {
			// osg::notify(osg::WARN) << "Unknown node "<<  nd <<std::endl;
		}
	}
}


void CMDHandler::analysePrimSet(osg::PrimitiveSet*prset, osg::Vec3Array *verts) {
	unsigned int ic;
	unsigned int i2;
	//unsigned int nprim=0;
        //osg::notify(osg::WARN) << std::endl;
	//osg::notify(osg::WARN) << "Prim set type "<< prset->getMode() << " indices " << prset->getNumIndices() << std::endl;
	for (ic=0; ic<prset->getNumIndices(); ic++) { // NB the vertices are held in the drawable -
            double x = (* verts)[prset->index(ic)].x();
            double y = (* verts)[prset->index(ic)].y();
            double z = (* verts)[prset->index(ic)].z();
            // (* verts)[prset->index(ic)].set( x, y, z+1);
            if (x > maxx) maxx=x;
            if (x < minx) minx=x;
            if (y > maxy) maxy=y;
            if (y < miny) miny=y;
            if (z > maxz) maxz=z;
            if (z < minz) minz=z;
        }
	switch (prset->getMode()) {
            case osg::PrimitiveSet::TRIANGLES: // get vertices of triangle
		for (i2=0; i2<prset->getNumIndices()-2; i2+=3) {
                    this->polygons++;
                    double tmp = 0;
                    tmp = areaTriangle((* verts)[prset->index(i2  )].x(), (* verts)[prset->index(i2  )].y(), (* verts)[prset->index(i2  )].z(), 
                                               (* verts)[prset->index(i2+1)].x(), (* verts)[prset->index(i2+1)].y(), (* verts)[prset->index(i2+1)].z(), 
                                               (* verts)[prset->index(i2+2)].x(), (* verts)[prset->index(i2+2)].y(), (* verts)[prset->index(i2+2)].z());
                    if (tmp!=tmp) tmp = 0; // is_nan -> 0
                    this->area += tmp;
                    tmp = volumeTriangle((* verts)[prset->index(i2  )].x(), (* verts)[prset->index(i2  )].y(), (* verts)[prset->index(i2  )].z(), 
                                               (* verts)[prset->index(i2+1)].x(), (* verts)[prset->index(i2+1)].y(), (* verts)[prset->index(i2+1)].z(), 
                                               (* verts)[prset->index(i2+2)].x(), (* verts)[prset->index(i2+2)].y(), (* verts)[prset->index(i2+2)].z());
                    if (tmp!=tmp) tmp = 0; // is_nan -> 0
                    this->volume += tmp;
		}
            break;
            case osg::PrimitiveSet::TRIANGLE_STRIP: // look up how tristrips are coded
		for (i2=0; i2<prset->getNumIndices()-2; i2++) {
                    this->polygons++;
                    double tmp = 0;
                    tmp = areaTriangle((* verts)[prset->index(i2  )].x(), (* verts)[prset->index(i2  )].y(), (* verts)[prset->index(i2  )].z(), 
                                               (* verts)[prset->index(i2+1)].x(), (* verts)[prset->index(i2+1)].y(), (* verts)[prset->index(i2+1)].z(), 
                                               (* verts)[prset->index(i2+2)].x(), (* verts)[prset->index(i2+2)].y(), (* verts)[prset->index(i2+2)].z());
                    if (tmp!=tmp) tmp = 0; // is_nan -> 0
                    this->area += tmp;
                    tmp = (i2%2==0?1:-1) * 
                                volumeTriangle((* verts)[prset->index(i2  )].x(), (* verts)[prset->index(i2  )].y(), (* verts)[prset->index(i2  )].z(), 
                                               (* verts)[prset->index(i2+1)].x(), (* verts)[prset->index(i2+1)].y(), (* verts)[prset->index(i2+1)].z(), 
                                               (* verts)[prset->index(i2+2)].x(), (* verts)[prset->index(i2+2)].y(), (* verts)[prset->index(i2+2)].z());
                    if (tmp!=tmp) tmp = 0; // is_nan -> 0
                    this->volume += tmp;
		}
            break;
            //case osg::PrimitiveSet::TRIANGLE_FAN: // look up how tristrips are coded
            //	osg::notify(osg::WARN) << "Triangle_fan "  << std::endl;
            //break;
            default:
                break;
                // osg::notify(osg::WARN) << "BZZ UNKNOWN primitive " << prset->getMode() << std::endl;
	// etc for all the primitive types you expect. EG quads, quadstrips lines line loops....
	}
}

inline double CMDHandler::distance3d(double x1, double y1, double z1, double x2, double y2, double z2){
    return sqrt( (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2) + (z1-z2)*(z1-z2) );
}
inline double CMDHandler::heron(double a, double b, double c){
    double s = (a + b + c) / 2;
    return sqrt( s*(s-a) * (s-b)*(s-c) );
}
double CMDHandler::areaTriangle(double x1, double y1, double z1, double x2, double y2, double z2,double x3, double y3, double z3){
    double a=distance3d(x1,y1,z1,x2,y2,z2);
    double b=distance3d(x2,y2,z2,x3,y3,z3);
    double c=distance3d(x3,y3,z3,x1,y1,z1);
    return heron(a,b,c);  
}
double CMDHandler::volumeTriangle(double x1, double y1, double z1, double x2, double y2, double z2,double x3, double y3, double z3) {
    double x = y2*z3 - z2*y3;
    double y = -(x2*z3 - z2*x3);
    double z =   x2*y3 - y2*x3;
    double vec = x1 * x + y1 * y + z1 * z;
    return vec / 6;    
}


void CMDHandler::rotateGeode(osg::Geode *geode,osg::Quat q, double zoom) {
    for (unsigned int i=0; i<geode->getNumDrawables(); i++) {
		osg::Drawable *drawable=geode->getDrawable(i);
		osg::Geometry *geom=dynamic_cast<osg::Geometry *> (drawable);
                osg::Vec3Array *verts = dynamic_cast<osg::Vec3Array*>(geom->getVertexArray());
                for (int ic = 0; ic < verts->capacity(); ic++) {
                    osg::Vec3d vec = (* verts)[ic];
                    (* verts)[ic].set(q * vec * zoom);
                }
    }
}
void CMDHandler::rotateNode(osg::Node *nd,osg::Quat q, double zoom) {
    osg::Geode *geode = dynamic_cast<osg::Geode *> (nd);
	if (geode) { // analyse the geode. If it isnt a geode the dynamic cast gives NULL.
		this->rotateGeode(geode,q,  zoom);
	} else {
		osg::Group *gp = dynamic_cast<osg::Group *> (nd);
		if (gp) {
			for (unsigned int ic=0; ic<gp->getNumChildren(); ic++) {
				this->rotateNode(gp->getChild(ic),q,  zoom);
			}
		} else {
			// osg::notify(osg::WARN) << "Unknown node "<<  nd <<std::endl;
		}
	}
}

void CMDHandler::rotateZoom(double xAngle, double yAngle, double zAngle, double zoom) {
    // rotate
    osg::Quat q = osg::Quat( xAngle, osg::Vec3d(1,0,0), 
                    yAngle, osg::Vec3d(0,1,0), 
                    zAngle, osg::Vec3d(0,0,1)); 
    this->rotateNode(this->loadedModel.get(),q,  zoom);
    this->calcWeight();
}

void CMDHandler::saveStl(std::string stlName) {
    // osgDB::writeNodeFile(*this->loadedModel, stlName);
    STLBinSaver::save(this->loadedModel.get(), stlName);
    // TODO save as PLY
    
}