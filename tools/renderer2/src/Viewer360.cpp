/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Viewer360.cpp
 * Author: asen
 * 
 * Created on 12 мая 2016 г., 17:31
 */

#include "Viewer360.h"

#include "http_server.h"
#include "http_headers.h"
#include "http_request.h"
#include "http_content_type.h"

#include <pcrecpp.h> 
#include <mutex>
#include <iostream>
#include <fstream>
#include <ctime>
#include <iomanip>

bool requestIsSafe(std::string Path) {
    pcrecpp::RE re("/([a-zA-Z01-9_]*)\\.?([a-zA-Z01-9_]*)");
    return re.FullMatch(Path);
}

std::string dateToString(std::string format, int deltaSeconds) {
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time (&rawtime);
  rawtime = rawtime + deltaSeconds;
  timeinfo = gmtime(&rawtime);

  strftime(buffer,80,format.c_str(),timeinfo);
  std::string str(buffer);

  return str;
}

void fileCopy(std::string fileFrom, std::string fileTo) {
    std::ifstream source(fileFrom, std::ios::binary);
    std::ofstream dest(fileTo, std::ios::binary);
    dest << source.rdbuf();
    source.close();
    dest.close();    
}

/*
std::string urlDecode(std::string utftext) {
        int i = 0;
        std::string result = "";
        char c = 0,  c1 = 0, c2 = 0;

        while ( i < utftext.length ) {
            c = utftext[i];
            if (c < 128) {
                sscanf(SRC.substr(i+1,2).c_str(), "%x", &ii);
                ch=static_cast<char>(ii);
                ret+=ch;
                result += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                result += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                result += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return result;    
}*/

std::string urlDecode(std::string &SRC) {
    std::string ret;
    char ch;
    int i, ii, iii;
    for (i=0; i<SRC.length(); i++) {
        if (int(SRC[i])==37) {
            sscanf(SRC.substr(i+1,2).c_str(), "%x", &ii);
            /* iii = 0;
            if (ii > 191) {
                sscanf(SRC.substr(i+4,2).c_str(), "%x", &iii);
                // ii = ((ii & 31) << 6) | (iii & 63);
                ii = ii * 256 + iii;
                i=i+2;
            };*/
            ch=static_cast<char>(ii);
            ret+=ch;
            i=i+2;
        } else {
            ret+=SRC[i];
        }
    }
    return (ret);
}
  
bool has_suffix(const std::string &str, const std::string &suffix)
{
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

Viewer360::Viewer360(RenderingFarm* farm, std::string bindAdress, std::uint16_t bindPort, int cacheLifeTime1) {
    std::string SrvAddress= bindAdress;
    std::uint16_t SrvPort = bindPort;
    std::uint16_t SrvThreadCount = 4;
    static const std::string RootDir = "webroot";  
    static const std::string DefaultPage = "index.html";  
    bool cachewarmup = false;
    this->farm1 = farm;

    if (cacheLifeTime1 ==0) cacheLifeTime1 = -100000;
    this->cacheLifeTime = cacheLifeTime1;
    // TODO move mutex to farm/leaf
    	  try
	  {
	    Srv = new Network::HttpServer(SrvAddress, SrvPort, SrvThreadCount,
	      [&] (Network::IHttpRequestPtr req)
	      { 
                int thisReplyLifetime = this->cacheLifeTime;  
                bool thisReplyAllowCache = true;
	        std::string Path = req->GetPath();
		// TODO HOLE SECURITY DISABLE .. IN PATH !!!
                //if (has_suffix(Path, ".png")) // names as plah.stl.png
                //    Path = Path.substr(0,Path.size() - 4);
		if (Path.substr(0,5) == "/stl/" && Path.length()>5 && this->farm1->mayRender(Path.substr(5,1000))) {
                    std::string stlName = Path.substr(5,1000);
                    Network::IHttpRequest::RequestParams params = req->GetParams();
                    double azimuth = std::round(stod(params["ax"]));
                    double elevation= std::round(stod(params["ay"])); 
                    double pitch= std::round(stod(params["az"])); 
                    double overLight=0;
                    int plateback = 0;
                    if (params.find("cachewarmup") != params.end()) {
                        cachewarmup=true;
                    }
                    if (params.find("ol") != params.end()) {
                        overLight=std::round(stod(params["ol"])*100)/100; 
                    }
                    if (params.find("pb") != params.end()) {
                        plateback=std::stoi( params["pb"] ); 
                    }
                    std::string color = params["c"];
                    int c3d = stoi(params["3d"]); 
                    std::string texture = "";
                    int matcap = 0;
                    // tt = 0 no texture, plain color
                    // tt = 1 matcap
                    // tt = 2 zaxis
                    // tt = 3 linear
                    if (params.find("tt") != params.end()) {
                        // if (params["tt"] == "1") matcap = true;
                        matcap = std::stoi( params["tt"] ); 
                    }
                    if (matcap > 0) { // params["tt"] == "1" || params["tt"] == "2" ) {
                        // texture = "resource/texture_lines4.jpg"; 
                        if (!this->farm1->nameIsSafe(params["t"])) {
                            throw std::runtime_error("prohibited."); 
                        }
                        texture = "resource/" + params["t"];
                        // TODO ecurity
                    }
			
                    if (params.find("edit") != params.end()) {
                        std::string editSessionName = params["edit"];
                        if (!this->farm1->nameIsSafe(editSessionName)) throw std::runtime_error("prohibited."); 
                        // is file exists? if not - make a copy
                        if (!this->farm1->mayRender(editSessionName)) {
                            std::cout << "copy: " << DEFAULT_PREFIX_MODEL + stlName << " to " << DEFAULT_PREFIX_MODEL + editSessionName << std::endl << std::flush;
                            fileCopy(DEFAULT_PREFIX_MODEL + stlName, DEFAULT_PREFIX_MODEL + editSessionName);
                        }
                        // if yes - change stl name
                        stlName = editSessionName;
                        thisReplyLifetime = -100000;
                        thisReplyAllowCache = false;
                        
                        if (params.find("clupx") != params.end()) {
                            // Path = "/../" + this->farm1->getRenderedFile(stlName,true,color, azimuth, elevation, pitch, c3d, texture, matcap, overLight, thisReplyAllowCache);
                            RenderingLeaf * leaf = this->farm1->getActiveLeaf(stlName);
                            std::string s = params["txt"];
                            leaf->clup(stod(params["clupx"]), stod(params["clupy"]), urlDecode(s));
                            //leaf->saveStl(DEFAULT_PREFIX_MODEL + stlName);
                        }
                    }
                    
                    //std::lock_guard<std::mutex> Lock(this->Mtx);
                    std::cout << "stl: " << stlName << std::endl;
                    bool formatMulticolor =  (this->farm1->getValidModelExtension(stlName) == ".ply");
                    if (formatMulticolor && color == "") {
                        texture = "";
                        matcap = false;
                    } else {
                        if (color == "") color = "ffffff";
                    };
                    Path = "/../" + this->farm1->getRenderedFile(stlName,true,color, azimuth, elevation, pitch, c3d, texture, matcap, overLight, thisReplyAllowCache, plateback);
                    if (Path =="/../" )  {
                        std::cout << "errload: " <<stlName << std::endl;
                        throw std::runtime_error("erroload."); 
                    }
                    std::cout << "stl stage 2: " << stlName << std::endl;
                     // TODO save edited stl?
                } else if (!requestIsSafe(Path)) 
                    throw std::runtime_error("prohibited."); 

	        Path = RootDir + Path + (Path == "/" ? DefaultPage : std::string());
	        
	        std::cout << "Path: " << Path << std::endl
	             << Network::Http::Request::Header::Host::Name << ": "
	                  << req->GetHeaderAttr(Network::Http::Request::Header::Host::Value) << std::endl
	             << Network::Http::Request::Header::Referer::Name << ": "
	                  << req->GetHeaderAttr(Network::Http::Request::Header::Referer::Value) << std::endl;
	          //std::lock_guard<std::mutex> Lock(Mtx);
            req->SetResponseAttr(Network::Http::Response::Header::Server::Value, "TreatstockRend2");
		if (!cachewarmup) {
		        req->SetResponseAttr(Network::Http::Response::Header::ContentType::Value,
	                             Network::Http::Content::TypeFromFileName(Path));
		}
	        // req->SetResponseAttr(Network::Http::Response::Header::Expires::Value, dateToString("%a, %d %b %Y %I:%M:%S %Z", 3600)); // cache for a hour
                req->SetResponseAttr(Network::Http::Response::Header::Expires::Value, dateToString("%a, %d %b %Y %H:%M:%S %Z", thisReplyLifetime)); // nocache
                if (thisReplyLifetime < 0) {
                    req->SetResponseAttr(Network::Http::Response::Header::CacheControl::Value, "no-cache, no-store, must-revalidate"); // nocache
                    req->SetResponseAttr(Network::Http::Response::Header::Pragma::Value, "no-cache"); // nocache
                }
		if (cachewarmup) {
			req->SetResponseString("");
			cachewarmup=false;
		} else {
			req->SetResponseFile(Path);
		}
	      });
	  }
	  catch (std::exception const &e)
	  {
	    std::cout << "Exceptionnn: " << e.what() << std::endl;
	  }

    
}

Viewer360::Viewer360(const Viewer360& orig) {
}

Viewer360::~Viewer360() {
}

