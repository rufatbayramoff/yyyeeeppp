<?php

// sample RendServer client
$location = "http://10.102.0.90:8090/";
//$location = "http://10.102.0.208:9000/";
ini_set('soap.wsdl_cache_limit', 0);
ini_set('default_socket_timeout',1000);
$client     = new SoapClient("../src/soap/renderer.wsdl", array("trace" => 1, "exception" => 0)); // it is WSDL URL 
$client->__setLocation($location);
// TODO: cache WSDL locally?
// TODO: make sure prevent WSDL caching while testing:  sudo rm /tmp/wsdl-vagrant-*


//donum("tirano.ply");
donum("q_test_tri.stl");
//donum("pikachu1.stl");
//donum("bad.stl");
//donum("problem_5765.stl"); // problem with color
//donum("problem_5818_2.stl"); // hangs up
//donum("problem_5835_3.stl"); // good but too long
//donum("problem_5855.stl");
//donum("problem_5858.stl");
foreach (glob("bad/*.stl") as $filename) {
 echo $filename."\n";
 donum($filename);
};

function doNum($num) {
    global $client;
    $stlpath = "./".$num;
    $outpath = "./".$num."out";
    $data = file_get_contents($stlpath);
    $dataEncoded = base64_encode($data);      
    echo "call rend on $num...\n";


    $result = $client->Rend(array(
         'id'=>"prefix_".$num
        //,'azimuth'=>0
        //,'elevation'=>30
        //,'crene'=>0
        ,'plateback'=>0
        ,'materials'=>array(
                array('id'=>'green','color'=>'00ff00','ambient'=>0.0),
                //array('id'=>'clean','color'=>'','ambient'=>0.2),
                array('id'=>'natural','color'=>'','ambient'=>0.0),
                array('id'=>'white','color'=>'ffffff','ambient'=>0.4),
        )
        ,'materials360'=>array(
                //array('id'=>'graz','color'=>'ffdd00','ambient'=>0.6),
                //array('id'=>'transparent','color'=>'','ambient'=>0.2),
        )
        ,'stlbinary'=>$dataEncoded
    ));
    echo "rend ...\n";
    if (!is_array($result->renders->render)) $result->renders->render = array($result->renders->render);
    if($result->renders360 && !empty($result->renders360->render)){
        if (!is_array($result->renders360->render)) $result->renders360->render = array($result->renders360->render);
    }

    foreach($result->renders->render as $render) {
        //echo $render->materialid . " -> " . $render->pngdata . "\n\n";
        $content = base64_decode($render->pngdata);
        $f = fopen($outpath ."-rend-". $render->materialid . ".png", "wb");
        fwrite($f,$content);
        fclose($f);
    }
    if($result->renders360 && !empty($result->renders360->render)){
        foreach($result->renders360->render as $render) {
            $content = base64_decode($render->pngdata);
            $f = fopen($outpath ."-360-". $render->materialid . ".png", "wb");
            fwrite($f,$content);
            fclose($f);
        }
    }
    echo "num=".$num." volume = ".$result->measurement->volume.", area=".$result->measurement->area.
            " size=".$result->measurement->w."x".$result->measurement->h."x".$result->measurement->l.
            " polygons=".$result->measurement->polygons. "\n";

    /*
    $result = $client->RendRotated(array(
         'id'=>"prefix_".$num
        ,'materials'=>array(
                array('id'=>'blue','color'=>'0000cc','ambient'=>0.4),
                //array('id'=>'black','color'=>'111111','ambient'=>0.2),
                array('id'=>'white','color'=>'ffffff','ambient'=>0.9),
        )
        ,'materials360'=>array(
                array('id'=>'gray','color'=>'cccccc','ambient'=>0.4)
        )
        ,'rotate'=>array(
                'x'=>90,'y'=>0,'z'=>0,'zoom'=>2
        )
        ,'stlbinary'=>$dataEncoded
    ));
    echo "called...\n";
    if (!is_array($result->renders->render)) $result->renders->render = array($result->renders->render);
    if($result->renders360 && !empty($result->renders360->render)){
        if (!is_array($result->renders360->render)) $result->renders360->render = array($result->renders360->render);
    }

    foreach($result->renders->render as $render) {
        $content = base64_decode($render->pngdata);
        $f = fopen($outpath ."-rend-". $render->materialid . ".png", "wb");
        fwrite($f,$content);
        fclose($f);
    }
    if($result->renders360 && !empty($result->renders360->render)){
        foreach($result->renders360->render as $render) {
            $content = base64_decode($render->pngdata);
            $f = fopen($outpath ."-360-". $render->materialid . ".png", "wb");
            fwrite($f,$content);
            fclose($f);
        }
    }
    $content = base64_decode($result->stlbinary);
    $f = fopen($outpath ."-rotated.stl", "wb"); // .substr($num,-4)
    fwrite($f,$content);
    fclose($f);
    echo "num=".$num." volume = ".$result->measurement->volume.", area=".$result->measurement->area.
            " size=".$result->measurement->w."x".$result->measurement->h."x".$result->measurement->l.
            " polygons=".$result->measurement->polygons."\n";
        */
    } 