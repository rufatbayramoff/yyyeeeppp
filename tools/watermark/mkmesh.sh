#!/bin/bash
# Устанавливаем библиотеки для установки Qt
sudo apt-get install -y build-essential libfontconfig1 mesa-common-dev libglu1-mesa-dev

# Качаем и распаковываем Qt
if [ ! -f /opt/qt5.7.0.tar.xz ]; then
    wget http://clamav.tsdev.work/todownload/qt5.7.0.tar.xz -O /opt/qt5.7.0.tar.xz
fi
if [ ! -d /opt/Qt5.7.0 ]; then
    tar xvf /opt/qt5.7.0.tar.xz -C /opt
fi

# Клонируем репозитории
cd meshlab
git clone https://github.com/cnr-isti-vclab/meshlab
cd meshlab
git checkout v2016.12
git clean -d -f
git reset --hard
cd ..

# Библиотека vcglib обязательно нужна для meshlab
git clone http://github.com/cnr-isti-vclab/vcglib/
cd vcglib
git checkout master
git clean -d -f
git reset --hard
cd ..
# Сборка предварительных пакетов
cd meshlab/src/external
/opt/Qt5.7.0/5.7/gcc_64/bin/qmake -o makefile -recursive -spec linux-g++
make -j 4
cd ..
# ошибка в plugin io_TXT переименовываем файл
if [ ! -f plugins_experimental/io_TXT/io_TXT.pro ]; then
    mv -f plugins_experimental/io_TXT/io_txt.pro plugins_experimental/io_TXT/io_TXT.pro
fi

# Сборка самого Meshlab
/opt/Qt5.7.0/5.7/gcc_64/bin/qmake meshlab_full.pro -o makefile -spec linux-g++ -recursive
make -j 4

# собранный файл находится в src/distrib