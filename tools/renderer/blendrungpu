#!/bin/bash
### BEGIN INIT INFO
# Provides:          blender_render
# Required-Start:    blender
# Required-Stop:    
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Daemon blender render GPU
# Description:       Daemon blender render GPU
### END INIT INFO
#
# Before starting the service, the user specified in the $USER must be logged in X
# for use VideoCard chip for render.

# PID file
PIDF=/var/run/`basename $0`.pid
# User for use exec blender
USER=gchannel
# Work directory
WDIR=/home/gchannel/scripts/render/
# WDIR=/vagrant/repo/tools/renderer
# Check and use Processor core`s
SCORE=$(cat /proc/cpuinfo | grep "cpu cores" | awk '{print $4}' | tail -1)
# Check root permission
if [ `whoami` != "root" ]; then echo "Service starting at root permissions" & exit 1
fi
# Check User
if [ ! `cat /etc/passwd | grep $USER` ]; then 
    echo "User not found for starting blender:" $USER
    echo "Check "$0" for edit variable \$USER"
    exit
fi
# Symlink blender
create_symlink(){
[ -f /usr/bin/blender-daemon ] || cp /usr/bin/blender /usr/bin/blender-daemon
}
create_symlink
# Check logfile
[ -f /var/log/blender.log ] || touch /var/log/blender.log
chown $USER:$USER /var/log/blender.log
# Check status blender.
check_status_blender(){
	# Check status blender.
	if [ -f $PIDF ]; then
	    bpid=$(cat $PIDF)
	    spid=$(ps -f -p $bpid | tail -1 | awk '{print $2}')
	else
	    bpid="PID"
	    spid=0
	fi
}
bpid="PID"
spid=0
check_status_blender
# Lock file that run always
touch /var/lock/blendrungpu

start(){
	check_status_blender
	create_symlink
	if [ ! $bpid == $spid ]; then
	    # Check user logging into X
	    if [ ! `w |awk '{if ($2==":0") print $1}'` == $USER ]; then
		echo 'The user specified in the $USER must be logged in X'
		exit
	    fi
	    echo `date "+%Y-%m-%d %H:%M:%S"` "Start blender-daemon GPU..."
	    rm -f $PIDF
	    cd $WDIR
	    export DISPLAY=:0
	    start-stop-daemon -S -d $WDIR -c $USER -b -N 19 -noaudio -p $PIDF -m -x /usr/bin/blender -- -P src/renderer/RendServer.py -o //file -F JPEG -x 1 -f 1 _data/empty.blend
	else
	    echo `date "+%Y-%m-%d %H:%M:%S"` "Blender-daemon GPU is already running!"
	fi
}
stop(){
	check_status_blender
	if [ $bpid == $spid ]; then
	    echo `date "+%Y-%m-%d %H:%M:%S"` "Stop blender-daemon GPU..."
	    start-stop-daemon -K -p $PIDF
	    rm -f /usr/bin/blender-daemon
	    rm -f $PIDF
	else
	    echo `date "+%Y-%m-%d %H:%M:%S"` "Blender-daemon GPU not running!"
	fi
}
force_stop(){
	echo `date "+%Y-%m-%d %H:%M:%S"` "Kill blender-daemon GPU..."
	start-stop-daemon -K -p $PIDF -s 9
	for kpid in `ps -aux | grep blender | awk '{print $2}'`; do
	    kill -9 $kpid >/dev/null 2>&1
	done
	rm /var/lock/`basename $0`
	rm -f $PIDF
	rm -f /usr/bin/blender-daemon
}
status(){
	if [ -f $PIDF ]; then
	    bpid=$(cat $PIDF)
	    spid=$(ps -f -p $bpid | tail -1 | awk '{print $2}')
	fi
	if [ $bpid == $spid ]; then
	    echo `date "+%Y-%m-%d %H:%M:%S"` "Blender-daemon GPU is running." | tr -d "\r\n"
	    echo " Pid is" $spid
	    exit 0
	else 
	    bpid="PID"
	    spid=0
	    status=$(wget --connect-timeout 30 -O gettest.log 127.0.0.1:9000 2>&1 | grep 'HTTP')
	    r=$(echo $status | awk '{print $5}')
	    e=$(echo $status | awk '{print $6}')
	    if ([ "$e" ] && [ $e == 200 ]) || ([ "$r" ] && [ $r == 200 ]); then
		echo `date "+%Y-%m-%d %H:%M:%S"` "Blender-daemon is running another user." | tr -d "\r\n"
		spid=$(netstat -nap | grep blender | grep ":9000" | awk '{print $7}' | awk -F"/" '{print $1}')
		echo " Pid is" $spid
		exit 0
	    else
		echo `date "+%Y-%m-%d %H:%M:%S"` "Blender-daemon GPU is not runnning, or hanging."
		exit 1
	    fi
	fi
	rm -f gettest.log
}
case "$1" in
    start)
	start
    ;;
    stop)
	stop
    ;;
    force_stop)
	force_stop
    ;;
    restart|reload)
	stop && start
    ;;
    status)
	status
    ;;
    *)
    echo "Usage: /etc/init.d/"`basename $0` "{start|stop|status|restart|reload|force_stop}"
    echo "Usage: service" `basename $0` "{start|stop|status|restart|reload|force_stop}"
    exit 1
    ;;
esac
exit 0
