<?php

// sample RendServer client
$location = "http://localhost:9000/";
//$location = "http://10.102.0.208:9000/";
ini_set('soap.wsdl_cache_limit', 0);
ini_set('default_socket_timeout',1000);
$client     = new SoapClient($location, array("trace" => 1, "exception" => 0)); // it is WSDL URL 
$client->__setLocation($location);
// TODO: cache WSDL locally?
// TODO: make sure prevent WSDL caching while testing:  sudo rm /tmp/wsdl-vagrant-*


donum("./domik.stl");
//donum("/home/user/Downloads/3d/airplane.stl");

if(false){
    donum("multipart_test.stl");
    donum("Ape_support_included.stl");
    donum("buckle1.stl");
    donum("ferz.stl");
    donum("peshka.stl");
    donum("tabl.stl");
    donum("vasesupport.stl");
    donum("vader.stl");
    donum("pencilsup.stl");
    donum("buckle2.stl");
    donum("glad.stl");
}





function doNum($num) {
	global $client;
    $basename = pathinfo($num, PATHINFO_BASENAME);
    $filename = pathinfo($num, FILENAME_BASENAME);
    $stlpath =  $num;
    $outpath = "./".$filename."out";
    $data = file_get_contents($stlpath);
    $dataEncoded = base64_encode($data);      

    $result = $client->Rend(array(
         'id'=>"qwe123"
        ,'materials'=>array(
                array('id'=>'green','color'=>'00cc00','ambient'=>0.4),
                //array('id'=>'black','color'=>'111111','ambient'=>0.2),
                //array('id'=>'white','color'=>'ffffff','ambient'=>0.9),
        )
        ,'materials360'=>array(
                array('id'=>'white','color'=>'ffffff','ambient'=>0.9)
        )
        ,'stlbinary'=>$dataEncoded
    ));
    if (!is_array($result->renders->render)) $result->renders->render = array($result->renders->render);
    if($result->renders360 && !empty($result->renders360->render)){
        if (!is_array($result->renders360->render)) $result->renders360->render = array($result->renders360->render);
    }

    foreach($result->renders->render as $render) {
        $content = base64_decode($render->pngdata);
        $f = fopen($outpath ."-rend-". $render->materialid . ".png", "wb");
        fwrite($f,$content);
        fclose($f);
    }
    if($result->renders360 && !empty($result->renders360->render)){
        foreach($result->renders360->render as $render) {
            $content = base64_decode($render->pngdata);
            $f = fopen($outpath ."-360-". $render->materialid . ".png", "wb");
            fwrite($f,$content);
            fclose($f);
        }
    }
    echo "num=".$num." volume = ".$result->measurement->volume.", area=".$result->measurement->area."\n";
    
    
    $result = $client->RendRotated(array(
         'id'=>"qwe123"
        ,'materials'=>array(
                array('id'=>'blue','color'=>'0000cc','ambient'=>0.4),
                //array('id'=>'black','color'=>'111111','ambient'=>0.2),
                //array('id'=>'white','color'=>'ffffff','ambient'=>0.9),
        )
        ,'materials360'=>array(
                array('id'=>'gray','color'=>'cccccc','ambient'=>0.4)
        )
        ,'rotate'=>array(
                'x'=>90,'y'=>0,'z'=>0,'zoom'=>2
        )
        ,'stlbinary'=>$dataEncoded
    ));
    if (!is_array($result->renders->render)) $result->renders->render = array($result->renders->render);
    if($result->renders360 && !empty($result->renders360->render)){
        if (!is_array($result->renders360->render)) $result->renders360->render = array($result->renders360->render);
    }

    foreach($result->renders->render as $render) {
        $content = base64_decode($render->pngdata);
        $f = fopen($outpath ."-rend-". $render->materialid . ".png", "wb");
        fwrite($f,$content);
        fclose($f);
    }
    if($result->renders360 && !empty($result->renders360->render)){
        foreach($result->renders360->render as $render) {
            $content = base64_decode($render->pngdata);
            $f = fopen($outpath ."-360-". $render->materialid . ".png", "wb");
            fwrite($f,$content);
            fclose($f);
        }
    }
    $content = base64_decode($result->stlbinary);
    $f = fopen($outpath ."-rotated.stl", "wb");
    fwrite($f,$content);
    fclose($f);
    echo "num=".$num." volume = ".$result->measurement->volume.", area=".$result->measurement->area."\n";
    
} 