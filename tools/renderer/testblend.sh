#!/bin/bash
function createstl() {
# Create stl file to test
echo "solid Exported from Blender-2.69 (sub 0)
facet normal 0 0 0
outer loop
vertex 1.000000 1.000000 -1.000000
vertex 1.000000 -1.000000 -1.000000
vertex -1.000000 -1.000000 -1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex -1.000000 -1.000000 -1.000000
vertex -1.000000 1.000000 -1.000000
vertex 1.000000 1.000000 -1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex 1.000000 0.999999 1.000000
vertex -1.000000 1.000000 1.000000
vertex -1.000000 -1.000000 1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex -1.000000 -1.000000 1.000000
vertex 0.999999 -1.000001 1.000000
vertex 1.000000 0.999999 1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex 1.000000 1.000000 -1.000000
vertex 1.000000 0.999999 1.000000
vertex 0.999999 -1.000001 1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex 0.999999 -1.000001 1.000000
vertex 1.000000 -1.000000 -1.000000
vertex 1.000000 1.000000 -1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex 1.000000 -1.000000 -1.000000
vertex 0.999999 -1.000001 1.000000
vertex -1.000000 -1.000000 1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex -1.000000 -1.000000 1.000000
vertex -1.000000 -1.000000 -1.000000
vertex 1.000000 -1.000000 -1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex -1.000000 -1.000000 -1.000000
vertex -1.000000 -1.000000 1.000000
vertex -1.000000 1.000000 1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex -1.000000 1.000000 1.000000
vertex -1.000000 1.000000 -1.000000
vertex -1.000000 -1.000000 -1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex 1.000000 0.999999 1.000000
vertex 1.000000 1.000000 -1.000000
vertex -1.000000 1.000000 -1.000000
endloop
endfacet
facet normal 0 0 0
outer loop
vertex -1.000000 1.000000 -1.000000
vertex -1.000000 1.000000 1.000000
vertex 1.000000 0.999999 1.000000
endloop
endfacet
endsolid Exported from Blender-2.69 (sub 0)" >$1
}
control_c() {
    rm -f ./testa.stl
#    service blendrungpu stop
    exit $?
}
createstl "testa.stl"
url='http://localhost:9000'
model='testa.stl'
echo `date +%F\ %H:%M:%S` "Start..."
service blendrungpu status >/dev/null 2>&1
if [ ! $? == 0 ]; then
    service blendrungpu start
fi
listenip=$(netstat -naptu |grep 'blender' |grep 'LISTEN' | awk '{print $4}' | awk -F":" '{print $1}')
listenport=$(netstat -naptu |grep 'blender' |grep 'LISTEN' | awk '{print $4}' | awk -F":" '{print $2}')
if [ $listenip == "0.0.0.0" ]; then
    ipadr=$(ifconfig `ifconfig -s | head -2 |tail -1| awk '{print $1}'` |grep "inet addr" | awk '{print $2}' | awk -F":" '{print $2}')
    url="http://"$ipadr":"$listenport
else
    url="http://"$listenip":"$listenport
fi
echo $url
# Сycle to verify that the blender is running
trap control_c SIGINT
ex=1
sechint=0
until [ $ex -ne 1 ];
do
    sleep 5
    service blendrungpu status >/dev/null 2>&1
    if [ ! $? == 0 ]; then
	service blendrungpu start
    fi
    if [ -f /tmp/blender.crash.txt ]; then
	rm -f /tmp/blender.crash.txt
	echo `date +%F\ %H:%M:%S` "Blender $url Crash restart..."
	service blendrungpu force_stop
	service blendrungpu start
    fi
    filedate=$(expr `ls -la --time-style=+%H\ %M /var/log/blender.log | awk '{print $6}'` \* 60 + `ls -la --time-style=+%H\ %M /var/log/blender.log | awk '{print $7}'`)
    curdate=$(expr `date '+%H %M' | awk '{print $1}'` \* 60 + `date '+%H %M' | awk '{print $2}'`)
    if [ $(expr $filedate + 1) -gt $curdate ]; then
	echo `date +%F\ %H:%M:%S` "Blender $url is ok. Wait 5 second do check..."
#	echo `date +%F\ %H:%M:%S` "Blender $url working wait for check..."
    else
	if [ $sechint -ge 60 ]; then
		result=$(php -r '$location = "'$url'";ini_set("soap.wsdl_cache_limit", 0);ini_set("default_socket_timeout",1000);$client = new SoapClient($location, array("trace" => 1, "exception" => 0));$client->__setLocation($location);$num="'$model'";global $client;$stlpath = "./".$num;$outpath = "./".$num."out";$data = file_get_contents($stlpath);$dataEncoded = base64_encode($data);$result = $client->Rend(array("id"=>"qwe123","materials"=>array(array("id"=>"green","color"=>"00cc00","ambient"=>0.4)),"materials360"=>array(array("id"=>"white","color"=>"ffffff","ambient"=>0.9)),"stlbinary"=>$dataEncoded));if (!is_array($result->renders->render)) $result->renders->render = array($result->renders->render);if($result->renders360 && !empty($result->renders360->render)){if (!is_array($result->renders360->render)) $result->renders360->render = array($result->renders360->render);}foreach($result->renders->render as $render) {$content = base64_decode($render->pngdata);}if($result->renders360 && !empty($result->renders360->render)){foreach($result->renders360->render as $render) {$content = base64_decode($render->pngdata);}}echo "num=".$num." volume = ".$result->measurement->volume.", area=".$result->measurement->area." size=".$result->measurement->w."x".$result->measurement->h."x".$result->measurement->l." polygons=".$result->measurement->polygons. "\n";' 2>&1) #'
		sechint=0
		if [ ! `expr match "$result" '^num=testa.stl'` -eq 13 ]; then 
			echo `date +%F\ %H:%M:%S` "Blender $url Crash restart..."
			service blendrungpu force_stop
			service blendrungpu start
		else
			echo `date +%F\ %H:%M:%S` "Test model rendered Blender $url is ok. Wait for check..."
		fi
	fi
	sechint=$(expr $sechint + 5)
	echo `date +%F\ %H:%M:%S` "Blender $url is ok. Wait 5 second do check..."
    fi
done