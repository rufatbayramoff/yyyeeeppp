#!/bin/bash
# Select user for running blender
userrunblender=vboxu
# Check root permission
if [ `whoami` != "root" ]; then echo "Blender GPU render has to run only by root permissions" & exit 1
fi
# If script is edited it will not start
if [ $(ps -p `cat /var/run/rungpu.pid` | wc -l) -gt 1 ]; then
echo `date +The_script_is_already_running-\%Y.\%m.\%d-\%H\:\%M`
exit
fi
echo $$ >/var/run/rungpu.pid
# Preprocessing for start render GPU
rm -f ./gettest.log
# Kill all Blender process
pidstarted=$(ps -aux | grep "[b]lender" | awk '{print $2}')
echo "Killing any process Blender to start GPU render" | tr -d "\r\n"
for kpid in $pidstarted; do
    kill $kpid
    echo " ..."$kpid | tr -d "\r\n"
done
echo "."
# Change Directory to scripts directory
scrstdir=`dirname $0`
cd $scrstdir
scrstdir=$(pwd)
cd $scrstdir
# Сycle to verify that the blender is running
ex=1
until [ $ex -ne 1 ];
do
    sleep 5
    status=$(wget -O gettest.log 127.0.0.1:9000 2>&1 | grep 'HTTP')
    r=$(echo $status | awk '{print $5}')
    e=$(echo $status | awk '{print $6}')
    if ([ "$e" ] && [ $e == 200 ]) || ([ "$r" ] && [ $r == 200 ]); then
	sleep 1
    else
	for kpid in $pidstarted; do
	    if ps --pid $kpid 2>&1 1>/dev/null; then
		echo "Killing blender..." $kpid
		kill $kpid
	    fi
        done
	# Restart Render GPU
	pidstarted=""
	su -c "export DISPLAY=:0; blender -P src/renderer/RendServer.py -o //file -F JPEG -x 1 -f 1 _data/empty.blend" $userrunblender &
	pidstarted=$(ps -aux | grep "[b]lender" | awk '{print $2}')
    fi
done
for kpid in $pidstarted; do
    kill $kpid
done