# Install linux:
#    sudo apt-get install blender python3-pip python-pip 
#    sudo pip3 install pysimplesoap
#    apply patch for pysimplesoap or overwrite files
# run linux:
#    blender -t 1 -b ./_data/test.blend -P ./src/renderer/RendServer.py -o //file -F JPEG -x 1 -f 1

# install windows:
#    download and install blender https://www.blender.org/ with python, make backup of Blender\2.75\python\lib
#    copy libraries from patch folder to C:\Program Files\Blender Foundation\Blender\2.75\python\lib
# run windows GL mode: 
#    "C:\Program Files\Blender Foundation\Blender\blender.exe" -P ./src/renderer/RendServer.py -o //file -F JPEG -x 1 -f 1 ./_data/empty.blend

# weight of model = 
# = Volume * K1 +Area * K2
# K1 = 0,00025 = 1,25*0,2/1000   where 1,25 = density of PLA, 0,2 = 20% percent infill for model
# K2 = 0,0008 = 1,25*0,8*(1-0,2)/1000 where 0,8 is thickness, 1,25 = density of PLA, 0,2 = 20% percent infill
# we assume model measurements in MM

# Links 
# Blender cookbook: https://www.blender.org/api/blender_python_api_2_76_2/
# python+blender examples: http://wiki.blender.org/index.php/Dev:Py/Scripts/Cookbook/Materials/Multiple_Materials
# Surface measurement: https://github.com/ajorstad/NeuroMorph/blob/master/NeuroMorph_Toolkit/NeuroMorph_Measurement_Tools.py
# pysimplesoap: https://code.google.com/p/pysimplesoap/wiki

