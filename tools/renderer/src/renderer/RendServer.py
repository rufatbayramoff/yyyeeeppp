# Treatstock services: SOAP rendering daemon
# author: asen.kurin@gmail.com


#####################################
# code
from pysimplesoap.server import SoapDispatcher, SOAPHandler
import http.server

import ssl
import logging
import sys
import os
import time
import math
import mathutils
import base64

import bpy
from bgl import *
import bmesh



# Start Render
cam_fov = 60.0
pi = 3.14159265
defaultpov = 0
modelscale = 6
if bpy.app.background:
    modelscale = 3
    defaultpov = -20 *(pi/180.0)

# generic functions: ------------------------------
def cross_product(v0, v1):
    x =   v0[1]*v1[2] - v0[2]*v1[1]
    y = -(v0[0]*v1[2] - v0[2]*v1[0])
    z =   v0[0]*v1[1] - v0[1]*v1[0]
    return [x,y,z]
def dot_product(v0,v1):
    vec = [v0[n]*v1[n] for n in range(len(v0))]
    return sum(vec)

# get signed volume contribution from single triangle
def get_vol_tri(tri):
    # tri = [p0, p1, p2],  pn = [x, y, z]
    p0 = tri[0]
    p1 = tri[1]
    p2 = tri[2]
    vcross = cross_product(p1,p2)
    vdot = dot_product(p0, vcross)
    vol = vdot/6
    return vol
 # get area of single triangle
def get_area_tri(tri):
    # tri = [p0, p1, p2]
    p0 = tri[0]
    p1 = tri[1]
    p2 = tri[2]
    area = mathutils.geometry.area_tri(p0, p1, p2)
    return area

# from voronoi.py
def fixNonManifold(obj):
    pass
    #bpy.context.tool_settings.mesh_select_mode = (False, True, False)
    #bpy.ops.object.mode_set(mode='EDIT')
    #bpy.ops.mesh.select_all(action='DESELECT')
    #bpy.ops.mesh.select_all(action = 'SELECT')
    #bpy.ops.mesh.remove_doubles(threshold = 0.0001)

    #bpy.ops.mesh.select_all(action = 'DESELECT')
    #bpy.ops.mesh.select_non_manifold()
    #bm = bmesh.from_edit_mesh(obj)
    #verts = [v for v in bm.verts if len(v.link_edges) < 3 and v.select]
    #for v in verts:
    #    bm.verts.remove(v)

    #bpy.ops.mesh.select_all(action = 'DESELECT')
    #bpy.ops.mesh.select_non_manifold()
    #bpy.ops.mesh.edge_collapse()

    #bpy.ops.object.mode_set(mode='OBJECT')

# calculate volume of mesh (assumed to be closed surface)
def fget_vol(self):
    obj = self.data
    if hasattr(obj, 'polygons'):

        # collapse non-manifold edges
        fixNonManifold(obj)

        if False:
            return 'open mesh has no volume'
        else:
            n_faces = len(obj.polygons)
            vol = 0
            for f in range(0, n_faces):
                n_vertices = len(obj.polygons[f].vertices[:])
                if n_vertices != 3:  # faces must be triangles
                    for v in range(0, n_vertices):
                        return 0 # 'highlight a subregion'
                tri = [0] * n_vertices
                for v in range(0, n_vertices):
                    tri[v] = obj.vertices[obj.polygons[f].vertices[v]].co
                vol += get_vol_tri(tri)
            return vol
    else:
        return 'property not available'

# calculate surface area of mesh
def fget_SA(self):
    obj = self.data
    if hasattr(obj, 'polygons'):
        n_faces = len(obj.polygons)
        SA = 0
        for f in range(0, n_faces):
            n_vertices = len(obj.polygons[f].vertices[:])
            if n_vertices != 3:  # faces must be triangles
                return 0 # 'highlight a subregion'
            tri = [0] * n_vertices
            for v in range(0, n_vertices):
                tri[v] = obj.vertices[obj.polygons[f].vertices[v]].co
            SA += get_area_tri(tri)
        return SA
    else:
        return 'property not available'

def polygonsCount(self):
    obj = self.data
    if hasattr(obj, 'polygons'):
        return len(obj.polygons)
    return 0



def hex_to_rgb(value):
    value = value.lstrip('#')
    lv = len(value)
    return tuple(float(int(value[i:i + lv // 3], 16))/255.0 for i in range(0, lv, lv // 3))

def MakeMaterialColored(color, ambient):
    mat = bpy.data.materials.new("Mat1")
    #http://wiki.blender.org/index.php/Dev:Py/Scripts/Cookbook/Code_snippets/Materials_and_textures

    # diffuse
    mat.diffuse_shader = 'MINNAERT'
    mat.diffuse_color =  hex_to_rgb(color) # (0.0, 0.288, 0.0)
    mat.diffuse_intensity = 1
    mat.darkness = 0.6 # for minaert, for 'LAMBERT' darkness not need

    # specular
    #mat.specular_color = hex_to_rgb(color)
    #mat.specular_shader = 'COOKTORR'
    #mat.specular_intensity = 0.5

    # ambient and transarent
    mat.alpha = 0
    mat.ambient = ambient # 1
    return mat

def areas_tuple():
    global bpy
    res = {}
    count = 0
    for area in bpy.context.screen.areas:
        res[area.type] = count
        count += 1
    return res


def Rend(id,materials,materials360,stlbinary):
    rotate = {'x': 0,'y': 0,'z': 0,'zoom':1 }
    return RendRotated(id,materials,materials360,rotate,stlbinary)



def RendZoom(id,materials,materials360,rotate,stlbinary):
    "Render and measure STL file"
    print(rotate)

    global tempFileNumber, bpy,scene, cam_fov,pi
    tempFileNumber = tempFileNumber + 1

    start = time.perf_counter();

    # save file
    stlPath = "./tmp/tmp"+str(tempFileNumber)+".stl"
    fh = open(stlPath, "wb")
    fh.write(base64.standard_b64decode(stlbinary))
    fh.close()
    print("stl saved: " + str(time.perf_counter() - start))
    logging.info("stl saved: " + str(time.perf_counter() - start))
    # load file
    bpy.ops.import_mesh.stl(filepath=stlPath)
    print("stl imported: " + str(time.perf_counter() - start))
    logging.info("stl imported: " + str(time.perf_counter() - start))
    # Set camera and object
    print(bpy.context.object.bound_box)
    logging.info(bpy.context.object.bound_box)
    scene = bpy.data.scenes["Scene"]
    maxsize = max(bpy.context.object.dimensions[0] * 0.9, bpy.context.object.dimensions[1] * 0.9, bpy.context.object.dimensions[2]) * 1.3
    # max_dim = max(ob.dimensions[0] * 0.75, ob.dimensions[1] * 0.75,  ob.dimensions[2])
    deltaz = bpy.context.object.dimensions[2]
    bpy.context.object.location[0] = 0
    bpy.context.object.location[1] = 0
    bpy.context.object.location[2] = 0 # + deltaz

    # new algo (rotating object)
    #ob = bpy.context.selected_objects[0]
    #print(ob)
    #bpy.ops.object.select_all(action='DESELECT')
    #ob.select = True
    bpy.ops.object.origin_set(type='GEOMETRY_ORIGIN', center='BOUNDS')

    #It's easier to read 3D View space's settings through bpy.context, e.g.:
    #•Use bpy.context.space_data, if the 3D View area is active (i.e. accessed through an operator executed from the 3D View itself).
    #•Use bpy.context.area.spaces[1], if accessed through a Console whose display type is directly switched from a 3D View.
    #•Use bpy.context.screen.areas[X].spaces[0] if accessed through a Console in another area, index X must be searched beforehand.
    areas = areas_tuple()
    view3d = bpy.context.screen.areas[areas['VIEW_3D']].spaces[0]
    view3d.lens = 100


    bpy.context.object.rotation_euler[0] = rotate['x'] *(pi/180.0)
    bpy.context.object.rotation_euler[1] = rotate['y'] *(pi/180.0)
    bpy.context.object.rotation_euler[2] = rotate['z'] *(pi/180.0)

    encodedRotatedStl = ''
    if rotate['x']!=0 or rotate['y']!=0 or rotate['z']!=0 or rotate['zoom']!=1:
        print("stl exporting: " + str(time.perf_counter() - start))
        bpy.context.object.dimensions = bpy.context.object.dimensions * rotate['zoom']
        bpy.ops.export_mesh.stl(filepath=stlPath,check_existing=False)
        #bpy.context.object.dimensions = bpy.context.object.dimensions * (1.0/rotate['zoom'])
        encodedRotatedStl = base64.standard_b64encode(open(stlPath,"rb").read()).decode("utf8")
        print("stl exported: " + str(time.perf_counter() - start))
        maxsize = max(bpy.context.object.dimensions[0] * 0.9, bpy.context.object.dimensions[1] * 0.9, bpy.context.object.dimensions[2]) * 1.3
        deltaz = bpy.context.object.dimensions[2]


    bpy.context.object.dimensions = bpy.context.object.dimensions * modelscale / maxsize

    bpy.context.object.rotation_euler[2] = rotate['z'] *(pi/180.0) + 65 *(pi/180.0) + defaultpov
    #bpy.data.objects['Camera'].location.x = maxsize * 2
    #bpy.context.object.rotation_mode = XYZ ;
    #camera_fit_coords(scene, coordinates)?
    if not bpy.app.background:
        bpy.data.objects['Camera'].hide = True
        bpy.data.objects['Lamp'].hide = True

    #bpy.data.objects['Grid'].hide = True
    bpy.ops.object.select_all(action='DESELECT')

    print("weight calculating start: " + str(time.perf_counter() - start))
    logging.info("weight calculating start: " + str(time.perf_counter() - start))

    print("calc started: " + str(time.perf_counter() - start))
    resultvolume = fget_vol(bpy.context.object) * rotate['zoom'] * rotate['zoom'] * rotate['zoom']
    resultarea = fget_SA(bpy.context.object) * rotate['zoom'] * rotate['zoom']
    result_w = bpy.context.object.dimensions[0] * rotate['zoom']
    result_l = bpy.context.object.dimensions[1] * rotate['zoom']
    result_h = bpy.context.object.dimensions[2] * rotate['zoom']
    result_polygons = polygonsCount(bpy.context.object)
    print("calc finished: " + str(time.perf_counter() - start))

    if encodedRotatedStl=='':
        result = {
                'measurement': {
                    'volume': resultvolume,
                    'area': resultarea,
                    'w':result_w,'h':result_h,'l':result_l,
                    'polygons': result_polygons
                    }
            }
    else:
        result = {
                'measurement': {
                    'volume': resultvolume,
                    'area': resultarea,
                    'w':result_w,'h':result_h,'l':result_l,
                    'polygons': result_polygons
                },
                'stlbinary': encodedRotatedStl
        }

    # clean up
    print("cleaning up start: " + str(time.perf_counter() - start))
    logging.info("cleaning up start: " + str(time.perf_counter() - start))
    os.remove(stlPath);
    #bpy.data.objects[object_name].select = True
    bpy.context.object.select = True
    bpy.ops.object.delete()
    # finita
    print("finished: " + str(time.perf_counter() - start))
    logging.info("finished: " + str(time.perf_counter() - start))
    return result

def RendRotated(id,materials,materials360,rotate,stlbinary):
    "Render and measure STL file"
    print(rotate)

    global tempFileNumber, bpy,scene, cam_fov,pi
    tempFileNumber = tempFileNumber + 1

    start = time.perf_counter();

    # save file
    stlPath = "./tmp/tmp"+str(tempFileNumber)+".stl"
    fh = open(stlPath, "wb")
    fh.write(base64.standard_b64decode(stlbinary))
    fh.close()
    print("stl saved: " + str(time.perf_counter() - start))
    logging.info("stl saved: " + str(time.perf_counter() - start))
    # load file
    bpy.ops.import_mesh.stl(filepath=stlPath)
    print("stl imported: " + str(time.perf_counter() - start))
    logging.info("stl imported: " + str(time.perf_counter() - start))
    # Set camera and object
    print(bpy.context.object.bound_box)
    logging.info(bpy.context.object.bound_box)
    scene = bpy.data.scenes["Scene"]
    maxsize = max(bpy.context.object.dimensions[0] * 0.9, bpy.context.object.dimensions[1] * 0.9, bpy.context.object.dimensions[2]) * 1.3
    # max_dim = max(ob.dimensions[0] * 0.75, ob.dimensions[1] * 0.75,  ob.dimensions[2])
    deltaz = bpy.context.object.dimensions[2]
    bpy.context.object.location[0] = 0
    bpy.context.object.location[1] = 0
    bpy.context.object.location[2] = 0 # + deltaz

    # new algo (rotating object)
    #ob = bpy.context.selected_objects[0]
    #print(ob)
    #bpy.ops.object.select_all(action='DESELECT')
    #ob.select = True
    bpy.ops.object.origin_set(type='GEOMETRY_ORIGIN', center='BOUNDS')

    #It's easier to read 3D View space's settings through bpy.context, e.g.:
    #•Use bpy.context.space_data, if the 3D View area is active (i.e. accessed through an operator executed from the 3D View itself).
    #•Use bpy.context.area.spaces[1], if accessed through a Console whose display type is directly switched from a 3D View.
    #•Use bpy.context.screen.areas[X].spaces[0] if accessed through a Console in another area, index X must be searched beforehand.
    areas = areas_tuple()
    view3d = bpy.context.screen.areas[areas['VIEW_3D']].spaces[0]
    view3d.lens = 100


    bpy.context.object.rotation_euler[0] = rotate['x'] *(pi/180.0)
    bpy.context.object.rotation_euler[1] = rotate['y'] *(pi/180.0)
    bpy.context.object.rotation_euler[2] = rotate['z'] *(pi/180.0)

    encodedRotatedStl = ''
    if rotate['x']!=0 or rotate['y']!=0 or rotate['z']!=0 or rotate['zoom']!=1:
        print("stl exporting: " + str(time.perf_counter() - start))
        bpy.context.object.dimensions = bpy.context.object.dimensions * rotate['zoom']
        bpy.ops.export_mesh.stl(filepath=stlPath,check_existing=False)
        #bpy.context.object.dimensions = bpy.context.object.dimensions * (1.0/rotate['zoom'])
        encodedRotatedStl = base64.standard_b64encode(open(stlPath,"rb").read()).decode("utf8")
        print("stl exported: " + str(time.perf_counter() - start))
        maxsize = max(bpy.context.object.dimensions[0] * 0.9, bpy.context.object.dimensions[1] * 0.9, bpy.context.object.dimensions[2]) * 1.3
        deltaz = bpy.context.object.dimensions[2]


    bpy.context.object.dimensions = bpy.context.object.dimensions * modelscale / maxsize


    bpy.context.object.rotation_euler[2] = rotate['z'] *(pi/180.0) + 65 *(pi/180.0) + defaultpov
    #bpy.data.objects['Camera'].location.x = maxsize * 2
    #bpy.context.object.rotation_mode = XYZ ;
    #camera_fit_coords(scene, coordinates)?
    if not bpy.app.background:
        bpy.data.objects['Camera'].hide = True
        bpy.data.objects['Lamp'].hide = True

    #bpy.data.objects['Grid'].hide = True
    bpy.ops.object.select_all(action='DESELECT')

    # old algo (rotating camera)
    #angle = 0 * (pi/180.0)
    #scene.camera.location.x = maxsize * math.sin(angle)
    #scene.camera.location.y = -1 * maxsize * math.cos(angle)
    #scene.camera.location.z = maxsize # + deltaz
    #scene.camera.rotation_euler[0] = 45 *(pi/180.0)
    #scene.camera.rotation_euler[1] = 0
    #scene.camera.rotation_euler[2] = angle # left-right rotation - correct!



    renders = []

    for material in materials:
        color = material['material']['color']
        ambient = material['material']['ambient']
        materialid = material['material']['id']
        # set up material
        mat = MakeMaterialColored(color,ambient)
        bpy.context.object.active_material = mat

        imageext = '.png' # png
        bpy.context.scene.render.filepath = '//tmp/cam'  + str(tempFileNumber)
        print("ready to render: " + str(time.perf_counter() - start))
        logging.info("ready to render: " + str(time.perf_counter() - start))
        if bpy.app.background:
            bpy.ops.render.render(write_still=True, use_viewport=False) # , view_context=True
        else:
            #glClearColor(1.0, 1.0, 1.0, 1.0);
            bpy.ops.render.opengl(write_still=True) # , view_context=True
        print("render saved: " + str(time.perf_counter() - start))
        logging.info("render saved: " + str(time.perf_counter() - start))

        prinFileName = './tmp/cam' + str(tempFileNumber) + imageext;
        if bpy.app.background:
            prinFileName = './_data/tmp/cam' + str(tempFileNumber) + imageext;
        encoded = base64.standard_b64encode(open(prinFileName,"rb").read()).decode("utf8")
        print("render encoded: " + str(time.perf_counter() - start))
        logging.info("render encoded: " + str(time.perf_counter() - start))

        renders.append({'render': {'materialid':materialid,'pngdata':encoded}})

        # clean up
        for material1 in bpy.data.materials:
            material1.user_clear();
            bpy.data.materials.remove(material1);
        os.remove(prinFileName);

    print("render360 starting: " + str(time.perf_counter() - start))
    logging.info("render360 starting: " + str(time.perf_counter() - start))

    renders360 = []
    rendcount = 12
    for material in materials360:
        panorama = bpy.data.images.new(name="untitled", width=800, height=600*rendcount)

        color = material['material']['color']
        ambient = material['material']['ambient']
        materialid = material['material']['id']
        # set up material
        mat = MakeMaterialColored(color,ambient)
        bpy.context.object.active_material = mat

        imageext = '.png' # png
        bpy.context.scene.render.filepath = '//tmp/cam'  + str(tempFileNumber)
        for iii in range(0,rendcount):
            print(iii)
            logging.info(iii)
            bpy.context.object.rotation_euler[2] = rotate['z'] *(pi/180.0) + (iii*360/rendcount) *(pi/180.0) + defaultpov
            if bpy.app.background:
                bpy.ops.render.render(write_still=True) # , view_context=True
            else:
                bpy.ops.render.opengl(write_still=True) # , view_context=True
            prinFileName = './tmp/cam' + str(tempFileNumber) + imageext;
            if bpy.app.background:
                prinFileName = './_data/tmp/cam' + str(tempFileNumber) + imageext;
                logging.info('./_data/tmp/cam' + str(tempFileNumber) + imageext)
            realpath = os.path.abspath(prinFileName)
            image2 = bpy.data.images.load(realpath)
            panorama.pixels[iii*800*600*4:(iii+1)*800*600*4]=image2.pixels[0:800*600*4]

        panorama.filepath_raw = realpath
        panorama.file_format = 'PNG'
        panorama.save()
        encoded = base64.standard_b64encode(open(prinFileName,"rb").read()).decode("utf8")
        print("render encoded: " + str(time.perf_counter() - start))
        logging.info("render encoded: " + str(time.perf_counter() - start))

        renders360.append({'render': {'materialid':materialid,'pngdata':encoded}})

        # clean up
        for material1 in bpy.data.materials:
            material1.user_clear();
            bpy.data.materials.remove(material1);
        os.remove(prinFileName);
        # TODO REMOVE AND CLEAN UP!!!


    # combine 2out-green.png  2out-white.png
    #image1 = Image.open('./tmp/2out-green.png')
    #image2 = Image.open('./tmp/2out-white.png')
    #blank_image = Image.new("RGB", (800*2, 600))
    #blank_image.paste(image1, (0,0))
    #blank_image.paste(image2, (800,0))
    #blank_image.save('./tmp/2out-combine.png')

    #renders.append({'render360': {'materialid':materialid,'pngdata':encoded}})
    print("render360 finished: " + str(time.perf_counter() - start))
    logging.info("render360 finished: " + str(time.perf_counter() - start))

    print("weight calculating start: " + str(time.perf_counter() - start))
    logging.info("weight calculating start: " + str(time.perf_counter() - start))


    #z_dim = ob.dimensions[2]
    #print(z_dim)
    #bpy.ops.transform.translate(value=(0,0,z_dim/2.0))
    print("calc started: " + str(time.perf_counter() - start))
    resultvolume = fget_vol(bpy.context.object) * rotate['zoom'] * rotate['zoom'] * rotate['zoom']
    resultarea = fget_SA(bpy.context.object) * rotate['zoom'] * rotate['zoom']
    result_w = bpy.context.object.dimensions[0] * rotate['zoom']
    result_l = bpy.context.object.dimensions[1] * rotate['zoom']
    result_h = bpy.context.object.dimensions[2] * rotate['zoom']
    result_polygons = polygonsCount(bpy.context.object)
    print("calc finished: " + str(time.perf_counter() - start))


    if encodedRotatedStl=='':
        result = {
                'measurement': {
                    'volume': resultvolume,
                    'area': resultarea,
                    'w':result_w,'h':result_h,'l':result_l,
                    'polygons': result_polygons
                    },
                'renders': renders,
                'renders360': renders360
            }
    else:
        result = {
                'measurement': {
                    'volume': resultvolume,
                    'area': resultarea,
                    'w':result_w,'h':result_h,'l':result_l,
                    'polygons': result_polygons
                    },
                'renders': renders,
                'renders360': renders360,
                'stlbinary': encodedRotatedStl
            }

    # clean up
    print("cleaning up start: " + str(time.perf_counter() - start))
    logging.info("cleaning up start: " + str(time.perf_counter() - start))
    os.remove(stlPath);
    #bpy.data.objects[object_name].select = True
    bpy.context.object.select = True
    bpy.ops.object.delete()
    # finita
    print("finished: " + str(time.perf_counter() - start))
    logging.info("finished: " + str(time.perf_counter() - start))
    return result

    #try: except: return "err:" + sys.exc_info()[0]
    #scene.render.file_format = 'JPEG'
    #for x in range(0, 0):
    #    print ("TRY --------------------------     " , x)
    # UNCOMMENT THIS FOR OPENCL RENDERING AND USE
    ## "C:\Program Files\Blender Foundation\Blender\blender.exe" -P ./src/renderer/blenderer.py -o //file -F JPEG -x 1 -f 1 ./_data/test.blend


# main program: ----------------------------------------------
if  os.path.sep == '/':
    logging.basicConfig(format='%(asctime)s - %(message)s', filename='/var/log/blender.log', level=logging.INFO)
else:
    logging.basicConfig(format='%(asctime)s - %(message)s', filename='.\\blender.log', level=logging.INFO)
tempFileNumber = 1

dispatcher = SoapDispatcher( 'my_dispatcher',
    location = "http://localhost:8008/",
    action = 'http://localhost:8008/', # SOAPAction
    namespace = "http://treatstock.com/sample.wsdl", prefix="ns0",
    trace = True,  ns = True)

# register the user function
dispatcher.register_function('Rend', Rend,
    returns={
        'measurement': {'volume': float, 'area': float, 'w': float, 'h': float, 'l': float,'polygons':float},
        'renders':    [{'render': {'materialid':str,'pngdata':str}}],
        'renders360': [{'render': {'materialid':str,'pngdata':str}}]
        },
    args={'id': str,
          'materials':    [{'material': {'id':str,'color':str,'ambient':float}}] ,
          'materials360': [{'material': {'id':str,'color':str,'ambient':float}}] ,
          'stlbinary': str
          })
dispatcher.register_function('RendZoom', RendZoom,
    returns={
        'measurement': {'volume': float, 'area': float, 'w': float, 'h': float, 'l': float,'polygons':float}
        },
    args={'id': str,
          'rotate': {'x':float,'y':float,'z':float,'zoom':float},
          'stlbinary': str
          })
dispatcher.register_function('RendRotated', RendRotated,
    returns={
        'measurement': {'volume': float, 'area': float, 'w': float, 'h': float, 'l': float,'polygons':float},
        'renders':    [{'render': {'materialid':str,'pngdata':str}}],
        'renders360': [{'render': {'materialid':str,'pngdata':str}}],
        'stlbinary': str
        },
    args={'id': str,
          'materials':    [{'material': {'id':str,'color':str,'ambient':float}}] ,
          'materials360': [{'material': {'id':str,'color':str,'ambient':float}}] ,
          'rotate': {'x':float,'y':float,'z':float,'zoom':float},
          'stlbinary': str
          })


# set up world
scene = bpy.data.scenes["Scene"]
bpy.context.scene.world.horizon_color = (1, 1, 1)
bpy.ops.object.delete()
scene.world.light_settings.use_environment_light = True
scene.world.light_settings.gather_method = 'APPROXIMATE'
#scene.camera.data.angle = cam_fov*(pi/180.0)
#scene.camera.rotation_mode = 'XYZ'
scene.render.resolution_percentage = 100
scene.render.resolution_x = 800
scene.render.resolution_y = 600
blenderui = bpy.context.user_preferences.themes[0].view_3d.space.gradients.high_gradient = (1,1,1) # .user_interface
if not bpy.app.background: # GPU OPENGL
    print ("GL Vendor ", glGetString(GL_VENDOR))
    logging.info("GL Vendor " + glGetString(GL_VENDOR))
    print ("GL Renderer ", glGetString(GL_RENDERER))
    logging.info("GL Renderer " + glGetString(GL_RENDERER))
    print ("GL Version ", glGetString(GL_VERSION))
    logging.info("GL Version " + glGetString(GL_VERSION))
    #scene.render.resolution_percentage = 100 # 400 for better quality
    #scene.render.resolution_x = 800
    #scene.render.resolution_y = 600
scene.render.use_antialiasing = True
scene.render.use_full_sample = True

for area in bpy.context.screen.areas:
    if area.type == 'SpaceView3D':
        area.spaces[0].region_3d.view_perspective = 'CAMERA'
        area.spaces[0].show_only_render = True
        area.spaces[0].viewport_shade = 'SOLID'
        #area.spaces[0].clip_end = maxlength
        area.spaces[0].cursor_location = (1000.0, 1000.0, 1000.0)

#bpy.data.scenes[Scenename].render.image_settings.file_format = 'TIFF'
#bpy.data.scenes[Scenename].render.image_settings.quality = 80
#bpy.data.scenes[Scenename].render.image_settings.color_mode = 'RGBA'
#bpy.data.scenes[Scenename].render.resolution_x = 2048
#bpy.data.scenes[Scenename].render.resolution_y = 1080
#bpy.data.scenes[Scenename].render.resolution_percentage = 50
#bpy.data.scenes[Scenename].render.use_antialiasing = True
#bpy.data.scenes[Scenename].render.use_full_sample = True

# Run -
print("Starting server...")
logging.info("Starting server...")
httpd = http.server.HTTPServer(("", 9000), SOAPHandler)
httpd.dispatcher = dispatcher
httpd.serve_forever()
#while keep_running():
#    httpd.handle_request()

# clean up
bpy.ops.wm.quit_blender()
