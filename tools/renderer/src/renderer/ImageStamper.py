# Treatstock services: Convert grayscaled image to mesh
# author: asen.kurin@gmail.com


#####################################
# code
import logging
import sys
import os
import time
import math 
import mathutils 
from mathutils import Vector

import bpy
from bgl import * 
import bmesh

import bpy
import math

# clear mesh and object
for item in bpy.context.scene.objects:
    if item.type == 'MESH':
        bpy.context.scene.objects.unlink(item)
for item in bpy.data.objects:
    if item.type == 'MESH':
        bpy.data.objects.remove(item)
for item in bpy.data.meshes:
    bpy.data.meshes.remove(item)
for item in bpy.data.materials:
    bpy.data.materials.remove(item)

#fname = "astrid.jpg"
fname = "zulfia.jpg"
imgpath = os.path.abspath(fname)
#print(" path =",imgpath)
img = bpy.data.images.load(imgpath)
num_pixels = len(img.pixels)
width = img.size[0]
height = img.size[1]
#print(" width =",width," height=", height)
#apixels = img.pixels
RGBA_list = list(img.pixels)
grouped_list = [RGBA_list[ipx] for ipx in range(0, len(RGBA_list), 4)]




# mesh arrays
verts = []
faces = []

# mesh variables
numX = width
numY = height

# variance and scale variables
variance = .35
scale = 80
zscale = -1 * scale * 1.5/ 100 # 1.5%

# fill verts array
for i in range (0, numX):
    for j in range(0,numY):
        # nomalize range
        u = 2*(i/numX-1/2)
        v = 2*(j/numY-1/2)

        s = variance
        x = scale*u
        y = scale*v
        #z = scale*1/math.sqrt(2*math.pi*s*s)*math.exp(-(u*u+v*v)/(2*s*s))
        z = zscale * ((RGBA_list[(j * width + i)*4] + RGBA_list[(j * width + i)*4+1] + RGBA_list[(j * width + i)*4+2] )/(3.0))  

        vert = (x,y,z)
        verts.append(vert)

podl = 1.3 # +30 foll heoght of black%
verts.append((scale*-1,scale*-1,zscale*podl))
verts.append((scale*-1,scale* 1,zscale*podl))
verts.append((scale* 1,scale*-1,zscale*podl))
verts.append((scale* 1,scale* 1,zscale*podl))

# fill faces array
count = 0
for i in range (0, numY *(numX-1)):
    if count < numY-1:
        A = i
        B = i+1
        C = (i+numY)+1
        D = (i+numY)
        face = (A,B,C,D)
        faces.append(face)
        count = count + 1
    else:
        count = 0

maxx = numY * numX 
faces.append((maxx,maxx+1,maxx+3,maxx+2))
# подложку надо делать для каждого элемента!
faces.append((0,maxx,maxx+1,numY-1))
faces.append((0,maxx,maxx+2,numY*(numX-1)))
faces.append((numY-1,maxx+1,maxx+3,numY*numX-1))
faces.append((numY*(numX-1),maxx+2,maxx+3,numY*numX-1))


# create mesh and object
mesh = bpy.data.meshes.new("wave")
object = bpy.data.objects.new("wave",mesh)

# set mesh location
object.location = bpy.context.scene.cursor_location
bpy.context.scene.objects.link(object)

# create mesh from python data
mesh.from_pydata(verts,[],faces)
mesh.update(calc_edges=True)

# bpy.ops.import_mesh.stl(filepath=stlPath)
bpy.ops.object.select_all(action='SELECT')
stlPath = fname + ".stl"
bpy.ops.export_mesh.stl(filepath=stlPath,check_existing=False)

sys.exit() 