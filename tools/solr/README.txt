only for DEV! - for prod - please read more about Solr configurations.
-------------------------------------
Installing SOLR for DEV - DEV only

cd /vagrant/repo/tools/solr
wget http://apache-mirror.rbc.ru/pub/apache/lucene/solr/7.3.1/solr-7.3.1.tgz
tar -xvzf solr-7.3.1.tgz
rm solr-7.3.1.tar
./solr-7.3.1/bin/solr start -e cloud -noprompt


http://localhost:8983/

-------------------------------------
PHP Extension

apt-get install php7.1-solr


-------------------------------------
NGINX config for location:

location /solr/collection1/select {

		# Only allow GET requests
	        limit_except GET {
        	        deny all;
	        }

                # Limits on rows/start (by number of chars) to prevent deep paging craziness
                if ($arg_start ~ ....+) {
                        return 403;
                }
                if ($arg_rows ~ ....+) {
                        return 403;
                }


		#Explicitly list args to disallow
		if ($arg_qt != "") {
			return 403;
		}

		# Disallow specific params that begin with a pattern, ie stream.file stream.body etc
		if ($args ~ [\&\?]stream.*?=(.*)) {
			return 403;
		}
		proxy_pass http://127.0.0.1:8983;

		# Some shared proxy settings
		proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	}

