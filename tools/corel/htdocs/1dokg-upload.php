<?php

// C:\tools\xampp\php\php.exe sample.php
// extension=php_com_dotnet.dll в php.ini
// Go to Tools>Options>Workspace>VBA and at the bottom of the dialog, remove the checkbox from "Delay Load VBA". (Pic 6)
// Настроки - Предупреждения - снять галочки все
// Текст..	Выберите пункт Шрифты.	Выберите пункт Сопоставление шрифтов PANOSE.	В диалоговом окне Параметры сопоставления шрифтов PANOSE вы НИКОГДА НЕ ПОКАЗЫАТ

// 3 долгая загрузка - должна хотя бы курсорк часики песочные делать?
function parseInkscape(string $filePath)
{
    $handle = fopen($filePath, 'r');
    $valid = false; // init as false
    while (($buffer = fgets($handle)) !== false) {
        if (strpos($buffer, 'inkscape') !== false) {
            $valid = true;
            break; // Once you find the string, you should break out the loop.
        }
    }
    fclose($handle);
    if($valid) {
        $tmpfolder = "c:\\converter\\htdocs\\data\\";
        $uuid = uniqid();
        $newname = $tmpfolder . $uuid.'.svg';
        exec('inkscape -l --export-filename='.$newname.' '.$filePath);
        return $newname;
    }
    return $filePath;
}

$tmpfolder = "c:\\converter\\htdocs\\data\\";

if (!array_key_exists('f', $_FILES)) {
    header("HTTP/1.1 501 No input files");
    exit();
}

$ext = strtolower(substr(strrchr($_FILES["f"]["name"], '.'), 1));
if ($ext != "cdr" && $ext != "dxf" && $ext != "eps" && $ext != "svg") return; // unknown extension

$uuid = uniqid();
$newname = $tmpfolder . $uuid .".". $ext ;
move_uploaded_file($_FILES["f"]["tmp_name"], $newname );
if($ext === 'svg') {
    $newname = parseInkscape($newname);
}
// run dde request for conversion
$corel = new com("CorelDraw.Application") or die("Невозможно создать экземпляр Corel");
$corel->InitializeVBA();
$result = $corel->GMSManager->RunMacro("GlobalMacros",
    "ThisMacroStorage.ConvertToSVG",
    // "C:\\asen\\projects\\HeadRobotics.com\\cutting\\datafiles\\eps\\cartonus-christmas-tree-ornament-stars.eps"
    // "C:\\asen\\projects\\HeadRobotics.com\\cutting\\datafiles\\1\\5.cdr"
    // "C:\\asen\\projects\\HeadRobotics.com\\cutting\\datafiles\\dxf\\yon0d9g7.dxf"
    $newname
);
$iserr = substr($result, 0, 3);
if ($iserr=="ERR") {
    header("HTTP/1.1 500 Internal Server Error");
    echo $result;
    die();
}

// pack all files into one zip archive
$zip = new ZipArchive();
$zipfname = $tmpfolder . $uuid .".zip";
if ($zip->open($zipfname, ZipArchive::CREATE)!==TRUE) {
    exit("cannot open <$zipfname>\n");
}
$files = explode(",",$result);
foreach($files as $fname) {
    if ($fname != "") {
        $zip->addFile($fname,basename($fname));
        $imgfolder = basename($fname, ".svg") . "_Images";
        $imgfolder_full = dirname($fname) . "/" . $imgfolder;
        if (is_dir($imgfolder_full)) {
            $zip->addEmptyDir($imgfolder);
            foreach (glob($imgfolder_full ."/*") as $imgname) {
                $zip->addFile($imgname,$imgfolder . "/" . basename($imgname));
            };
        }
    }
};
$zip->close();

// download
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="'.basename($zipfname).'"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($zipfname));
readfile($zipfname);


