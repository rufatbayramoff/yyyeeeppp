<?php

namespace yii\web;

use common\components\AsyncSession;
use common\components\HttpRequestLogger;
use common\components\Session;
use common\components\SessionManager;
use common\models\model3d\Model3dWatermarkService;
use common\modules\rabbitMq\components\RabbitMq;
use frontend\components\FrontendWebView;
use lib\delivery\carrier\CarrierService;
use lib\delivery\delivery\DeliveryService;
use yii\redis\Connection;

/**
 * Class RedisConnection
 * @method setex($key, $expirince, $value)
 * @method del($key)
 * @method string[] keys($pattern)
 */
abstract class RedisConnection extends Connection
{
}

/**
 * @property \yii\web\RedisConnection $redis
 * @property \common\models\SystemSetting $setting
 * @property \console\jobs\QueueServiceInterface $job
 * @property \yii\sphinx\Connection $sphinx
 * @property \frontend\components\angular\AngularService $angular
 * @property \lib\geo\GeoService $geo
 * @property \lib\message\MessageServiceInterface $message
 * @property \lib\sms\SmsGatewaysBundle $smsGatewaysBundle
 * @property \common\components\order\notifyer\NotifyService $orderNotify
 * @property \frontend\components\cart\CartService $cart
 * @property \common\services\ReviewService $reviewService
 * @property CarrierService $carrierService
 * @property DeliveryService $deliveryService
 * @property Model3dWatermarkService $watermarker
 * @property RabbitMq $rabbitMq
 * @property Session $session
 * @property AsyncSession $asyncSession
 * @property SessionManager $sessionManager
 * @property HttpRequestLogger $httpRequestLogger
 * @property FrontendWebView $view
 */
abstract class Application extends \yii\base\Application
{
    /**
     * @var \yii\web\RedisConnection
     */
    public $redis;

    /**
     * @var \common\models\SystemSetting
     */
    public $setting;

    /**
     * @var \console\jobs\QueueServiceInterface
     */
    public $job;

    /**
     * @var \yii\sphinx\Connection
     */
    public $sphinx;

    /**
     * @var \frontend\components\angular\AngularService
     */
    public $angular;

    /**
     * @var \lib\geo\GeoService
     */
    public $geo;

    /**
     * @var \lib\message\MessageServiceInterface
     */
    public $message;

    /**
     * @var \lib\sms\SmsGatewaysBundle
     */
    public $smsGatewaysBundle;

    /**
     * @var \common\components\order\notifyer\NotifyService
     */
    public $orderNotify;

    /**
     * @var \frontend\components\cart\CartService
     */
    public $cart;
    /**
     * @var \common\services\ReviewService
     */
    public $reviewService;

    /**
     * @var CarrierService
     */
    public $carrierService;

    /**
     * @var DeliveryService
     */
    public $deliveryService;

    /**
     * @var Model3dWatermarkService
     */
    public $watermarker;

    /**
     * @var RabbitMq
     */
    public $rabbitMq;

    /**
     * @var Session
     */
    public $session;

    /**
     * @var AsyncSession
     */
    public $asyncSession;

    /**
     * @var SessionManager
     */
    public $sessionManager;

    /**
     * @var HttpRequestLogger
     */
    public $httpRequestLogger;

    /**
     * @var FrontendWebView
     */
    public $view;
}