#!/bin/bash
export BROWSER=firefox
java -Dwebdriver.chrome.driver="tools/chromedriver" -Dwebdriver.gecko.driver="tools/geckodriver" -Dwebdriver.server.session.timeout=86400 -Dwebdriver.server.browser.timeout=86400 -jar tools/selenium-server-standalone-3.8.1.jar -enablePassThrough false >selenium.log 2>&1 &
vagrant up
vagrant ssh --command "export BROWSER=$BROWSER; cd /vagrant/repo && /vagrant/repo/installer/conf/9_testall.sh"
kill %1
killall -9 geckodriver chromedriver


