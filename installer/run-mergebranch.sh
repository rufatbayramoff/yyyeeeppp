#/bin/bash
cd repo
user=`cat .git/config | grep -Po '(?<=\/\/).+(?=@)'`
user_email=`echo $user | sed 's/_/./g'`@treatstock.com
git config --global user.name "$user"
git config --global user.email "$user_email"
git fetch --all
branchtodev=$(git branch | grep -e '\*' | awk '{print $2}')
if [ $branchtodev == "dev" ]; then
echo "*****************************************************************************************"
echo "* Ветка не должна быть dev!"
echo "*****************************************************************************************"
exit
fi
git checkout dev -f
git pull
git checkout $branchtodev -f
git merge origin/dev --no-ff -m "Merge dev to "$branchtodev" "`date +%d.%m.%Y_%H:%M`" Auto merge script. User: $user, email: $user_email"
if [ ! $? -eq 0 ]; then
    echo "*****************************************************************************************"
    echo "Невозможно слить ветки" $branchtodev "и dev!!!"
    echo "Сообщите разрабочкику ветки" $branchtodev "что в его ветку невозможно влить dev"
    echo "*****************************************************************************************"
    git reset --hard origin/$branchtodev
    exit
fi
git push
git checkout dev -f
git pull
echo "*****************************************************************************************"
echo "*  Будет произведено слияние в ветку dev ветки "$branchtodev
echo "*  Если хотите произвести слияние нажмите кнопку \"Y\" или \"y\""
echo "*****************************************************************************************"
set wkeyread=true
while $wkeyread; do
	set keypress=""
	read -s -n 1 keypress
	case $keypress in
	[yY])
		echo $keypress
		wkeyread=false
	;;
	[nN])
		echo $keypress
		wkeyread=false
	;;
	*)
		echo "Yes or No? Your answer: " $keypress
	esac
done
if [ $keypress == "y" -o $keypress == "Y" ]; then
	git merge $branchtodev --no-ff -m "Merge "$branchtodev" to dev "`date +%d.%m.%Y_%H:%M`" Auto merge script. User: $user, email: $user_email"
	if [ $? -eq 0 ]; then
		echo "*****************************************************************************************"
		echo "*  Слияние успешно завершено. Загрузить ветку в удаленный репозиторий"
		echo "*  Если хотите загрузить ветку в удаленный репозиторий нажмите кнопку \"Y\" или \"y\""
		echo "*****************************************************************************************"
		set wkeyread=true
		while $wkeyread; do
			set keypress=""
			read -s -n 1 keypress
			case $keypress in
			[yY])
				echo $keypress
				wkeyread=false
			;;
			[nN])
				echo $keypress
				wkeyread=false
			;;
			*)
				echo "Yes or No? Your answer: " $keypress
			esac
		done
		if [ $keypress == "y" -o $keypress == "Y" ]; then
			git push
			if [ $? -eq 0 ]; then
				echo "*****************************************************************************************"
				echo "*  Загрузка успешно завершена."
				echo "*****************************************************************************************"
			else
				echo "*****************************************************************************************"
				echo "*  Ошибка загрузки. Обратитесь к разрабочику ветки или к администратору."
				echo "*****************************************************************************************"
			fi
		else
			echo "*****************************************************************************************"
			echo "*  Вы отказались от загрузки в удаленный репозиторий. Сделайте это вручную"
			echo "*****************************************************************************************"
		fi
	else
		echo "*****************************************************************************************"
		echo "*  При слиянии веток были обнаружены ошибки. Обратитесь к разработчику ветки "$branchtodev
		echo "*****************************************************************************************"
		git reset --hard origin/dev
	fi
else
	echo "*****************************************************************************************"
	echo "*  Вы не подтвердили слияние веток!"
	echo "*****************************************************************************************"
	git checkout -f $branchtodev
	git branch | grep -e '\*' | awk '{print $2}'
	cd ..
	exit 1
fi
git branch | grep -e '\*' | awk '{print $2}'
cd ..
exit 0
# Disabled
#vagrant up
#vagrant ssh -c 'sudo /vagrant/repo/installer/conf/3_upgrade.sh'
