@echo off
cd %~dp0
cd repo
FOR /F "usebackq" %%a IN (`"git branch | grep "^*" | awk '{print $2}'"`) DO (
set branchtodev=%%a
)
git checkout dev -f
cd ..
copy repo\installer\run-mergebranch.sh run-mergebranch.sh /A /V /Y
cd repo
git checkout %branchtodev% -f
cd ..
bash -c ./run-mergebranch.sh
IF %ERRORLEVEL% EQU 1 goto :ERROR
vagrant up
vagrant ssh -c "/vagrant/repo/installer/conf/3_upgrade.sh"
pause >nul
exit
:ERROR
pause >null
exit