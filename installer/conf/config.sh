#!/bin/bash
# config.sh - file for installer configuration
# Параметры инсталлятора
export fpause=false              # Делать паузы для отладки
debug=false			 # Включить или выключить вывод исполняемых комманд и основки по ошибке
environment=dev                  # Пока что жестко здесь задаём какое у нас окружение
config_dir=/usr/local/etc/treatstock # Дирктория куда сайт складывает конфиги
me=$USER
logfile=/var/log/installer-`date +%F`.log; if [ ! -f $logfile ];then sudo touch $logfile; fi; if [  $me != "root" ]; then sudo chown $me:$me $logfile; fi # Куда сохранять лог инсталлера
export report=""                   # Переменная для сохранения статистики

# PHP setting
phpver="8.0"                     # Целевая версия PHP
phpverdev="8.0"                  # Возможность зачем-то задать другую цель для dev пакетов php
export suffixnginx=-im           # Переменные для сборки nginx-im
export sdir=/var/www/treatstock  # environment $sdir - Каталог установки сайта.

# MySQL password
mysqlpasswd_dev=mypass           # Пароль mysql для дев окружения
mysqlpasswd_test=FtwwwrU23@      # Пароль mysql для тестового окружения
mysqlpasswd_prod=FtwwwrU23@      # Пароль mysql для продуктового окружения
mysqlpasswd_slave=FtwwwrU23@     # Пароль mysql для продуктового окружения
mysqlpasswd=                     # Пароль

# Color setting
RED='\033[0;31m'        # for Error
ORANGE='\033[0;33m'     # for info
GREEN='\033[0;32m'      # for OK
NC='\033[0m'            # No Color

# Параметры окружения
case $environment in
    dev)
        mysqlpasswd=$mysqlpasswd_dev
        
    ;;
    test)
        mysqlpasswd=$mysqlpasswd_test
    ;;
    prod)
        mysqlpasswd=$mysqlpasswd_prod
    ;;
    slave)
        mysqlpasswd=$mysqlpasswd_slave
    ;;
esac
# functions
function timer { echo -n `date +%H:%M:%S\ %Z`;}
function pause { if $fpause;then read -p "Press enter to continue";fi;}
function info {
    set +x
    echo -e "\n`timer`	${ORANGE}[INFO] ${1}${NC}" | sudo tee -a $logfile
    if $debug;then set -xe;fi
    pause
    }
function flog {
    echo -en "$execute\n" >> $logfile
}
function infoOk { 
    set +x
    flog "$execute"
    echo -e "`timer`	${GREEN}[OK] $1 sucsesseful ${NC}" | sudo tee -a $logfile
    if $debug;then set -xe;fi
    report=$report+$1
    }
function error {
    set +x
    echo -e "${RED} !!!!!!!!!!!\n`timer`	[ERROR] $1 \n !!!!!!!!!!! ${NC}" | sudo tee -a $logfile
    flog "$execute"
    if $debug;then set -xe;fi
    pause
    exit 1
    }
function fupdate {
    step="fupdate: apt-get update"
    execute=$(sudo apt-get update 1>/dev/null) && infoOk "$step" || error "$step \nError text:\n$execute";
    }
function finstall {
	info "finstall: Start installation $1"
	execute=$(sudo apt-get -o DPkg::Lock::Timeout=120 install -y $1 1>/dev/null) && infoOk "finstall: $1";flog "$execute" || error "finstall: Failure installation $1\nError text:\n$execute";flog "$execute"
	}
function fdropCaches {
    # man proc 5
    # /proc/sys/vm/drop_caches (since Linux 2.6.16)
    #   Writing to this file causes the kernel to drop clean caches, dentries and inodes from memory, causing that memory to become free.
    #   To free pagecache, use echo 1 > /proc/sys/vm/drop_caches; to free dentries and inodes, use echo 2 > /proc/sys/vm/drop_caches; to free pagecache, dentries and inodes, use echo 3 > /proc/sys/vm/drop_caches.
    #
    #   Because this is a nondestructive operation and dirty objects are not freeable, the user should run sync(8) first.
    sync # сбрасываем грязные страницы на диски
    echo 3 | sudo tee /proc/sys/vm/drop_caches || true # Чистим pagecache, dentrie и inode кэши
}

function fFileACL {
    info "fFileACL: starting"
    # Функция для назначения прав на проекте, используется пакет acl.
    # add $USER to www-data group
    sudo usermod -G www-data $me

    function fFileACLAction {
        find $1 --type d -exec setfacl -m u:www-data:rwx {} \;
        find $1 --type f -exec setfacl -m u:www-data:rw {} \;
    }
    # default user for all project
    sudo chown -R $me:$me $sdir
    # user permission for www user files
    sudo find $sdir/frontend/web -type f -exec setfacl -m u:www-data:rw {} \;
    sudo find $sdir/frontend/runtime -type f -exec setfacl -m u:www-data:rw {} \;
    sudo find $sdir/frontend/storage -type f -exec setfacl -m u:www-data:rw {} \;
    sudo find $sdir/backend/runtime -type f -exec setfacl -m u:www-data:rw {} \;
    sudo find $sdir/backend/web -type f -exec setfacl -m u:www-data:rw {} \;
    sudo find $sdir/common/runtime -type f -exec setfacl -m u:www-data:rw {} \;
    sudo find $sdir/console/runtime -type f -exec setfacl -m u:www-data:rw {} \;
    sudo find $sdir/tools/node/app/log -type f -exec setfacl -m u:www-data:rw {} \;
    sudo find $sdir/console/migrations -type f -exec setfacl -m u:www-data:rw {} \;
    
    # user permission for www user directory
    # fFileACLAction "$sdir/frontend/web"
    sudo find $sdir/frontend/web -type d -exec setfacl -m u:www-data:rwx {} \;
    sudo find $sdir/frontend/runtime -type d -exec setfacl -m u:www-data:rwx {} \;
    sudo find $sdir/frontend/storage -type d -exec setfacl -m u:www-data:rwx {} \;
    sudo find $sdir/backend/web -type d -exec setfacl -m u:www-data:rwx {} \;
    sudo find $sdir/backend/runtime -type d -exec setfacl -m u:www-data:rwx {} \;
    sudo find $sdir/common/runtime -type d -exec setfacl -m u:www-data:rwx {} \;
    sudo find $sdir/console/runtime -type d -exec setfacl -m u:www-data:rwx {} \;
    sudo find $sdir/tools/node/app/log -type d -exec setfacl -m u:www-data:rwx {} \; 
    sudo find $sdir/console/migrations -type d -exec setfacl -m u:www-data:rw {} \;
}
