#!/bin/bash
# environment $sdir - Каталог установки сайта.
cd /vagrant/repo

vendor/codeception/codeception/codecept build -c codeception-acceptance.yml
vendor/codeception/codeception/codecept run --tap="acceptancelog.log" --html="acceptancelog.html" -c codeception-acceptance.yml acceptance
vendor/codeception/codeception/codecept build -c codeception-unit.yml
vendor/codeception/codeception/codecept run --tap="unitlog.log" --html="unitlog.html" -c codeception-unit.yml unit
vendor/codeception/codeception/codecept run --tap="unitlog.log" --html="unitlog.html" -c codeception-unit.yml functional
