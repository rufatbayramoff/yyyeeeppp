#!/bin/bash
source "$(dirname "$0")/config.sh"
if $debug;then set -xe;fi

info "This is script $BASH_SOURCE"
cd $sdir

# remove old config
sudo rm -rf /etc/supervisor/conf.d/*

# configure supervisor
echo '[unix_http_server]
file=/var/run/supervisor.sock
chmod=0700
[supervisord]
logfile=/var/log/supervisor/supervisord.log
pidfile=/var/run/supervisord.pid
childlogdir=/var/log/supervisor
[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
[supervisorctl]
serverurl=unix:///var/run/supervisor.sock
[include]
files = /etc/supervisor/conf.d/*.conf' >/tmp/supervisord.conf
sudo mv /tmp/supervisord.conf /etc/supervisor/supervisord.conf && infoOk "supervisord.conf" || error "supervisord.conf "

# Configure web server supervisor
echo '
[inet_http_server]
port=0.0.0.0:9771
username=root
password=1234567' >/tmp/webservice.conf 
sudo mv /tmp/webservice.conf /etc/supervisor/conf.d/webservice.conf && infoOk "webservice.conf" || error "webservice.conf "

# configure rabbitmq agent treatstock.render
echo "[program:queue_render]
numprocs=1
numprocs_start=1
startsecs=3
startretries=100
autostart=true
autorestart=true
lines=250

redirect_stderr=true
stdout_logfile=/var/log/rabbit_render_queue.log

user=www-data
group=www-data

directory=$sdir
command=/usr/bin/php yii rabbit/listen treatstock.render" >/tmp/queue_render.conf 
sudo mv /tmp/queue_render.conf /etc/supervisor/conf.d/queue_render.conf && infoOk "queue_render.conf" || error "queue_render.conf "

# configure rabbitmq agent treatstock.analize-job
echo "[program:queue_analyze]
numprocs=1
numprocs_start=1
startsecs=3
startretries=100
autostart=true
autorestart=true
lines=250

redirect_stderr=true
stdout_logfile=/var/log/rabbit_analyze_queue.log

user=www-data
group=www-data

directory=$sdir
command=/usr/bin/php yii rabbit/listen treatstock.analyze-job" >/tmp/queue_analyze.conf 
sudo mv /tmp/queue_analyze.conf /etc/supervisor/conf.d/queue_analyze.conf && infoOk "queue_analyze.conf" || error "queue_analyze.conf "

# configure rabbitmq agent treatstock.parser
echo "[program:queue_parser]
numprocs=1
numprocs_start=1
startsecs=3
startretries=100
autostart=true
autorestart=true
lines=250

redirect_stderr=true
stdout_logfile=/var/log/rabbit_parser_queue.log

user=www-data
group=www-data

directory=$sdir
command=/usr/bin/php yii rabbit/listen treatstock.parser" >/tmp/queue_parser.conf 
sudo mv /tmp/queue_parser.conf /etc/supervisor/conf.d/queue_parser.conf && infoOk "queue_parser.conf" || error "queue_parser.conf "

# configure rabbitmq agent treatstock.prepare-download-files
echo "[program:queue_prepare_download_files]
numprocs=2
numprocs_start=1
startsecs=3
startretries=100
autostart=true
autorestart=true
lines=250
process_name=%(program_name)s_%(process_num)02d

redirect_stderr=true
stdout_logfile=/var/log/rabbit_prepare_download_files_queue.log

user=www-data
group=www-data

directory=$sdir
command=/usr/bin/php yii rabbit/listen treatstock.prepare-download-files" >/tmp/queue_prepare_download_files.conf 
sudo mv /tmp/queue_prepare_download_files.conf /etc/supervisor/conf.d/queue_prepare_download_files.conf && infoOk "queue_prepare_download_files.conf" || error "queue_prepare_download_files.conf "

# configure filepage-printes-daemon
echo "[program:filepage-printers-daemon]
numprocs=1
numprocs_start=1
startsecs=3
startretries=100
autostart=true
autorestart=true
lines=250

redirect_stderr=true
stdout_logfile=/var/log/filepage-printers-daemon.log

user=www-data
group=www-data

directory=$sdir
command=/usr/bin/php yii filepage-maps/filepage-printers-daemon" >/tmp/filepage-printers-daemon.conf 
sudo mv /tmp/filepage-printers-daemon.conf /etc/supervisor/conf.d/filepage-printers-daemon.conf && infoOk "filepage-printers-daemon.conf" || error "filepage-printers-daemon.conf "

# configure printers-tree-update-daemon
echo "[program:printers-tree-update-daemon]
numprocs=1
numprocs_start=1
startsecs=3
startretries=100
autostart=true
autorestart=true
lines=250

redirect_stderr=true
stdout_logfile=/var/log/printers-tree-update-daemon.log

user=www-data
group=www-data

directory=$sdir
command=/usr/bin/php yii filepage-maps/printers-tree-update-daemon" >/tmp/printers-tree-update-daemon.conf
sudo mv /tmp/printers-tree-update-daemon.conf /etc/supervisor/conf.d/printers-tree-update-daemon.conf && infoOk "printers-tree-update-daemon.conf" || error "printers-tree-update-daemon.conf "

# configure rabbitmq agent treatstock.converter 3mf
echo "[program:queue_converter]
numprocs=1
numprocs_start=1
startsecs=3
startretries=100
autostart=true
autorestart=true
lines=250

redirect_stderr=true
stdout_logfile=/var/log/rabbit_converter_queue.log

user=www-data
group=www-data

directory=$sdir
command=/usr/bin/php yii rabbit/listen treatstock.converter" >/tmp/queue_converter.conf 
sudo mv /tmp/queue_converter.conf  /etc/supervisor/conf.d/queue_converter.conf && infoOk "queue_converter.conf" || error "queue_converter.conf "

## configure solr
#echo "[program:solr]
#numprocs=1
#numprocs_start=1
#startsecs=5
#startretries=100
#autostart=true
#autorestart=true
#limit=15000
#
#redirect_stderr=true
#stdout_logfile=/var/log/solr.log
#
#user=solr
#group=solr
#
#directory=/vagrant/repo
#environment=SOLR_INCLUDE=\"/etc/default/solr.in.sh\",_JAVA_OPTIONS=\"-Djava.net.preferIPv4Stack=true\"
#command=/opt/solr/bin/solr start -cloud -f" >/tmp/solr.conf 
#sudo mv /tmp/solr.conf  /etc/supervisor/conf.d/solr.conf

# configure cutting converter
echo "[program:cutting-converter]
numprocs=1
numprocs_start=1
startsecs=3
startretries=100
autostart=true
autorestart=true
lines=250

redirect_stderr=true
stdout_logfile=/var/log/cutting-converter.log

user=www-data
group=www-data

directory=$sdir
command=/usr/bin/php yii rabbit/listen treatstock.cutting-converter" >/tmp/cutting-converter.conf
sudo mv /tmp/cutting-converter.conf /etc/supervisor/conf.d/cutting-converter.conf && infoOk "cutting-converter.conf" || error "cutting-converter.conf "

info "This is END of script $BASH_SOURCE"