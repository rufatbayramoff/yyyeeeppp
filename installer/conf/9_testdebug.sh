#!/bin/bash
source "$(dirname "$0")/config.sh"
source "$(dirname "$0")/check_env.sh"
if $debug;then set -xe;fi

cd $sdir
fdropCaches && infoOk "function fdropCaches" || error "function fdropCaches"
php init --env=Jenkins --overwrite=y


info "Kirill Machine fix"
hn=$(cat /etc/hostname)

if [[ "x$hn" == "xh9" ]]; then
    info "странные манипуляции на h9"
    sudo systemctl stop nginx
    sudo systemctl stop php${phpver}-fpm
    sudo systemctl stop supervisor
    sudo killall -9 php
    php init --env=Jenkins --overwrite=yes
    sudo systemctl start php${phpver}-fpm
    sudo systemctl start supervisor
    sudo systemctl start nginx
fi

##set ip selenium server
#ip=$(echo $SSH_CLIENT | awk '{print $1}')
#grep 'selenium.local' /etc/hosts 2>&1 >/dev/null
#if [ $? -ne 0 ]; then
#    echo -e "\n$ip selenium.local" | sudo tee -a /etc/hosts
#fi

echo -e "[client]\nuser=root\npassword=$mysqlpasswd\n">~/.my.cnf

echo "Clean database..." | tr -d '\n'
info "Зачем-то опять пересоздаём бд"
# prepare clean DB from migrations and save it as SQL dump
echo "drop database yii2advanced;" | mysql
echo "create database yii2advanced CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql
echo "CREATE USER root@'%' identified by '$mysqlpasswd'" | mysql || true
echo "CREATE USER root@localhost identified by '$mysqlpasswd'" | mysql || true
echo "grant all privileges on yii2advanced.* to root@'%'; flush privileges;" | mysql
echo "grant all privileges on yii2advanced.* to root@localhost; flush privileges;" | mysql
echo "Done"

echo "Clean Redis..." | tr -d '\n'
redis-cli flushall

# fill it with a data created in repo-UP script
echo "Restore database from dump created in repo-up script..." | tr -d '\n'
mysql yii2advanced < tests/_data/dump-test.sql
echo "DELETE FROM yii2advanced.user_location WHERE (id = 1);" | mysql
echo "Done"

info "Start migrations"
php yii migrate --interactive=0 2>&1 >/tmp/lastmigrations.log
if [ "x$?" == "x1" ]; then
    echo -e '\n'
    cat /tmp/lastmigrations.log
    echo "Error Migrations test stopped!"
    exit 1
fi
echo "Migrations Done."
echo "Set starting order number..."
echo "ALTER TABLE store_order AUTO_INCREMENT=100;" | mysql yii2advanced
echo "Done."
php yii translate/words-count-create || true


# clean up files
$sdir/installer/conf/9_test_clean.sh && infoOk "9_test_clean.sh" || error "9_test_clean.sh"
fdropCaches && infoOk "function fdropCaches" || error "function fdropCaches"

# prepare and run tests
echo "Restart supervisor..."
sudo systemctl stop supervisor
sleep 10
sudo systemctl start supervisor
sleep 10
#sudo systemctl -q restart supervisor
#if [ "x$?" == "x1" ]; then
#    echo "Error restart supervisor. Restart again..." | tr -d '\n'
#    sleep 1
#    sudo systemctl restart supervisor
#fi
#if [ "x$?" == "x0" ]; then
#    echo "Supervisor restarted."
#else
#    echo "Error restart supervisor."
#    exit 1
#fi
echo "Start script yii payment/update-rates..." | tr -d '\n'
php yii payment/update-rates
#Disabled
#php yii sypex-geo/update
fdropCaches && infoOk "function fdropCaches" || error "function fdropCaches"
sleep 1