#!/bin/bash
source "$(dirname "$0")/config.sh"
source "$(dirname "$0")/check_env.sh"
if $debug;then set -xe;fi
# STAGE0, подготовка
stage0RunTime=`date +%H:%M:%S\ %Z`
export ME=`whoami`
cd "$(dirname "$0")/../.."
export PROJECT_ROOT=`pwd`

info "This is script $BASH_SOURCE\n\t\tEnvironment - $environment"

# Проверяем есть ли скрипт перевода в обслуживание в целевой ди
if [ -f $sdir/tools/maintenance/maintenance.sh ];then
  info "site in maintenance"
  $sdir/tools/maintenance/maintenance.sh true
fi

# stop autoupdate in process
# Пока закомментировал, 1, эта штука работает не корректнона 20, и обвешана true; 2. обноления безопасности возможно стоит ставить автоматически
#sudo systemctl stop apt-daily.service || true
#sudo systemctl stop apt-daily.timer || true
#sudo systemctl kill --kill-who=all apt-daily.service || true
#sudo killall apt.systemd.daily || true

# default software packages
# Обновление репозиториев
export DEBIAN_FRONTEND=noninteractive
fupdate
finstall "dos2unix zip ntpdate software-properties-common mc htop iotop wget curl git net-tools lr pv openssh-server"

ftimezone () {
  # Установка часового пояса
  sudo sh -c "echo 'UTC' >/etc/timezone"
  sudo dpkg-reconfigure --frontend noninteractive tzdata
}
execute=$(ftimezone 2>&1) && infoOk "timezone changed";flog "$execute" || error "timezone change failure"

# Проверяем если цевая директория, если нет то создаём
if [ ! -d "/var/www/treatstock" ]; then sudo mkdir -p /var/www/treatstock; fi
# Проверяем есть ли файлы установки окружения в целевой директории,если нет то копируем весь в проект из текущего места в целевой каталог
if [ ! -f "$sdir/installer/conf/1_environment.sh" ]; then 
  sudo cp -a $PROJECT_ROOT/ $sdir/
fi

# устанавливаем права на выполнение скриптов
sudo chmod 0755 $sdir/installer/conf/*.sh

stage0EndTime=`date +%H:%M:%S\ %Z`

# Устанавливаем окружение, STAGE1
stage1RunTime=`date +%H:%M:%S\ %Z`
$sdir/installer/conf/1_environment.sh && infoOk "1_environment.sh" || error "1_environment.sh"
stage1EndTime=`date +%H:%M:%S\ %Z`

# Разворачиваем сайт, STAGE2
sudo chown -R $ME:$ME $sdir
stage2RunTime=`date +%H:%M:%S\ %Z`
$sdir/installer/conf/2_website.sh && infoOk "2_website.sh" || error "error. 2_website.sh"
if [ -f $sdir/tools/maintenance/maintenance.sh ];then
  info "site in not maintenance"
  $sdir/tools/maintenance/maintenance.sh false
fi
stage2EndTime=`date +%H:%M:%S\ %Z`

sudo apt autoremove -y

info "This is END of script $BASH_SOURCE"
echo "Statistics:"
echo "STAGE 0 runnning at "$stage0RunTime "ending "$stage0EndTime 
echo "STAGE 1 runnning at "$stage1RunTime "ending "$stage1EndTime 
echo "STAGE 2 runnning at "$stage2RunTime "ending "$stage2EndTime 

echo "config dir" $config_dir