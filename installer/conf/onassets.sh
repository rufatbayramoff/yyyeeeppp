#!/bin/bash
source "$(dirname "$0")/config.sh"
if $debug;then set -xe;fi

cd $sdir
fdropCaches && infoOk "function fdropCaches" || error "function fdropCaches"

# Install jre for compile assets
finstall default-jre
if [ ! -d frontend/web/assets/js ]; then
    sudo mkdir -p frontend/web/assets/js
    sudo chown www-data:www-data frontend/web/assets/js
fi
if [ ! -d frontend/web/assets/css ]; then
    sudo mkdir -p frontend/web/assets/css
    sudo chown www-data:www-data frontend/web/assets/css
fi

# Disable xdebug
if [ -f "$config_dir/xdebug.flag" ]; then
    sudo cp -f $config_dir/xdebug.flag $config_dir/xdebugofassets.flag
fi
sudo $sdir/installer/conf/offxdebug.sh

# Compile Asssets
php yii asset assets.php frontend/config/assets-dev.php
php yii asset assets.php frontend/config/assets-prod.php
rm -f frontend/config/main-local.php
php init --env=Jenkins --overwrite=n
cp -f environments/prod/frontend/web/index.php frontend/web/index.php
cp -f environments/prod/backend/web/index.php backend/web/index.php
sudo service php$phpver-fpm restart

# Enable AssetOn flag
echo 'assets on' >$config_dir/assetson.flag || true