#!/bin/bash
source "$(dirname "$0")/config.sh"
source "$(dirname "$0")/check_env.sh"
if $debug;then set -xe;fi

# STAGE0, подготовка
stage0RunTime=`date +%H:%M:%S\ %Z`

function reCreateDB {
    echo "drop database ts_payout;" | MYSQL_PWD=$mysqlpasswd mysql -u root || true
    echo "drop database yii2advanced;" | MYSQL_PWD=$mysqlpasswd mysql -u root || true
    echo "drop database yii2advanced_test;" | MYSQL_PWD=$mysqlpasswd mysql -u root || true
    echo "drop database ext_big;" | MYSQL_PWD=$mysqlpasswd mysql -u root || true

    echo "create database ts_payout CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root || true
    echo "create database yii2advanced CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root || true
    echo "create database yii2advanced_test CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root || true
    echo "create database ext_big CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root || true
}

function cleanDir {
    sudo rm -Rf $sdir/frontend/web/static/models/*
    sudo rm -Rf $sdir/frontend/web/static/user/*
    sudo rm -Rf $sdir/frontend/web/static/ps/*
    sudo rm -Rf $sdir/frontend/storage/*
    sudo rm -Rf $sdir/doc/api
    sudo rm -Rf $sdir/frontend/web/assets/*
}
reCreateDB && infoOk "Clear database" || error "Clear database fail"
cleanDir && infoOk "Clear directory" || error "Clear directory fail"

sudo redis-cli flushall 1>/dev/null && infoOk "redis flushall" || error "redis flushall"
sudo service php${phpver}-fpm stop || true
sudo service nginx stop || true

bash $sdir/installer/conf/1_environment.sh && infoOk "1_environment.sh" || error "1_environment.sh"
stage0EndTime=`date +%H:%M:%S\ %Z`
bash $sdir/installer/conf/2_website.sh && infoOk "2_website.sh" || error "2_website.sh"
stage1EndTime=`date +%H:%M:%S\ %Z`
bash $sdir/installer/conf/9_testdebug.sh  && infoOk "9_testdebug.sh" || error "2_website.sh"
stage2EndTime=`date +%H:%M:%S\ %Z`

info "This is END of script $BASH_SOURCE"
echo "Statistics:"
echo "I'm runnning at "$stage0RunTime
echo "STAGE 0 ending "$stage0EndTime 
echo "STAGE 1 ending "$stage1EndTime 
echo "STAGE 2 ending "$stage2EndTime 