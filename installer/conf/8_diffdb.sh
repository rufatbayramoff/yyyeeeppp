#!/bin/bash
# environment $sdir - Каталог установки сайта.
if [ -f /vagrant/conf/pathtoinstall.env ]; then
i=$(cat /vagrant/conf/pathtoinstall.env)
$i
else
export sdir=/vagrant/repo
fi

cd $sdir
sudo rm -rf tests\_data\diffs.sql
sudo rm -rf tests\_data\1dmp-diff.sql
mysqldump --user=root --password=mypass --complete-insert --extended-insert=FALSE yii2advanced > tests/_data/1dmp-diff.sql
echo 'Сделайте изменения на сайте перед созданием diff SQL файла'
echo 'После изменения на сайте нажмите Y'
set $keypress=""
while ([ "${keypress^^}" != "Y" ]); do
read -s -n 1 keypress
done
sudo rm -rf tests\_data\2dmp-diff.sql
mysqldump --user=root --password=mypass --complete-insert --extended-insert=FALSE yii2advanced > tests/_data/2dmp-diff.sql
diff -e tests/_data/1dmp-diff.sql tests/_data/2dmp-diff.sql | grep 'INSERT' > tests/_data/diffs.sql
cp tests/_data/diffs.sql /vagrant/diffs.sql
sudo rm -rf tests\_data\1dmp-diff.sql
sudo rm -rf tests\_data\2dmp-diff.sql
echo
echo "Файл diffs.sql создан в каталоге Vagrant"