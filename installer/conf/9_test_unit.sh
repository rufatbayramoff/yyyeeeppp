#!/bin/bash
# environment $sdir - Каталог установки сайта.
export sdir=/vagrant/repo

cd $sdir
#set ip selenium server
ip=$(echo $SSH_CLIENT | awk '{print $1}')
grep 'selenium.local' /etc/hosts 2>&1 >/dev/null
if [ $? -ne 0 ]; then
    echo -e "\n$ip selenium.local" | sudo tee -a /etc/hosts
fi

vendor/codeception/codeception/codecept build -c codeception-acceptance.yml
vendor/codeception/codeception/codecept run --tap="acceptancelog.log" --html="acceptancelog.html" -c codeception-acceptance.yml acceptance
vendor/codeception/codeception/codecept build -c codeception-unit.yml
vendor/codeception/codeception/codecept run --tap="unitlog.log" --html="unitlog.html" -c codeception-unit.yml unit
