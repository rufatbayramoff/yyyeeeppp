# log setting for nginx apmplify
#log_format  main_ext  '$remote_addr - $remote_user [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                      '"$http_user_agent" "$http_x_forwarded_for" '
#                      '"$host" sn="$server_name" '
#                      'rt=$request_time '
#                      'ua="$upstream_addr" us="$upstream_status" '
#                      'ut="$upstream_response_time" ul="$upstream_response_length" '
#                      'cs=$upstream_cache_status '
#                      'upstream_response_time="$upstream_response_time" '
#	              'upstream_connect_time="$upstream_connect_time" '
#        	      'upstream_header_time="$upstream_header_time"' ;

# HTTP redirect to https static.treatstock.com
server {
	server_name static.treatstock.com static.treatstock.fr static.treatstock.co.uk;
	location ~ /\.well-known/.+ {
		root /var/www/letsencrypt;
	}
	location / {
		return 301 https://$host$request_uri;
	}
}

# SSL static.treatstock.com
server {
	listen      443 ssl http2;
	server_name static.treatstock.com;
	ssl_certificate		/etc/nginx/ssl/1_treatstock.com.pem;
	ssl_certificate_key	/etc/nginx/ssl/2_treatstock.com.key;

	set_real_ip_from	74.208.155.35;
	real_ip_header    X-Real-IP;
	real_ip_recursive on;

#	include /etc/nginx/denyips;
	include /etc/nginx/denymanual;
	include /etc/nginx/denyfrontend.conf;
#	rewrite_log on;
# tuning
	sendfile on;
	tcp_nopush on;
        tcp_nodelay on;
	keepalive_timeout 30;
	keepalive_requests 100;
	reset_timedout_connection on;



#	access_log /var/log/nginx/static/static.log main;
	access_log /var/log/nginx/static/static.log main_ext buffer=8k;
	error_log /var/log/nginx/static/static-error.log;
# debug;
	root  /var/www/treatstock/frontend/web/;
#LetsEncrypt
	location ~ /\.well-known/.+ {
		root /var/www/letsencrypt;
		allow all;
	}
	location / {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		expires 30d;
		deny all;
	}
#	location /favicon.ico {
#                open_file_cache max=5000 inactive=20s; # enable disk cache for this location
#                open_file_cache_valid 30s;
#                open_file_cache_min_uses 1;
#                open_file_cache_errors off;
#}
	location ~* ^/colorer\(bw=-1,.+\)(.+)\.(jpg|jpeg|png|gif)$ {
#		rewrite ^(.+)(\(.+)(\).+)$ $1$2,jpeghint=n,progressive=y\)$3 break;
		chunked_transfer_encoding off;
		proxy_http_version 1.1;
		proxy_buffering on;
		proxy_buffers 32 4k;
		proxy_buffer_size 8k;
		proxy_cache cache;
		proxy_cache_valid 200 1d;
		proxy_cache_valid 404 1m;
		proxy_no_cache $flag_cache_empty;
		add_header X-DontCache $flag_cache_empty;
		expires	30d;
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		proxy_pass http://127.0.0.1:8989;
#		proxy_pass http://unix:/var/run/nginx-im.sock;
		header_filter_by_lua_block {
			if ngx.header.content_length == 0 then
				ngx.log(ngx.ERR,table.concat({" ",ngx.header.content_length,ngx.var.uri," "},"|"))
				ngx.exit(415)
			end
		}
if ($remote_addr = 94.180.253.137) {    
		add_header Server-Timing "$upstream_cache_status, reqt;dur=$request_time,upst;dur=$upstream_response_time";
}
	}
	location ~* '^/static/render/([0-9a-fA-F]{4}).+\.(jpeg|jpg|png|gif|JPEG|JPG|PNG)$' {
		rewrite ^/static/render/([a-z0-9][a-z0-9])([a-z0-9][a-z0-9])(.+)\.(jpeg|jpg|png|gif|JPEG|JPG|PNG) /static/render/$1/$2/$1$2$3.$4;
	}
	location ~* '^/static/render/(.+)_color_(multicolor)(.+)$' {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		expires 30d;
		open_file_cache max=10000 inactive=1m; # enable disk cache for this location
                open_file_cache_valid 10m;
                open_file_cache_min_uses 1;
                open_file_cache_errors off;

		try_files /static/render/$1_$2$3 @rimage;
	}
	location ~* ^/static/.+\.(jpeg|jpg|png|gif|mp4|JPEG|JPG|PNG|svg)$ {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		expires 30d;
		if ($http_origin ~* "^https://.+\.treatstock\.(fr|com|co.uk)$") {
			add_header Access-Control-Allow-Origin $http_origin;
		}
		open_file_cache max=10000 inactive=1m; # enable disk cache for this location
                open_file_cache_valid 10m;
                open_file_cache_min_uses 1;
                open_file_cache_errors off;

		try_files $uri @rimage;
	}
	location @rimage {
# new color multicolor width height
		rewrite '^(/static/render/)(.+)_color_(multicolor)(_ambient_)([0-9]{1,2})(.+)_([0-9]{1,4})x([0-9]{1,4})\.(jpg|jpeg|png|gif|JPEG|JPG|PNG)$' /colorer(bw=-1,dw=$7,dh=$8)$1$2_multicolor$4$5$6.$9;
# new color width height
		rewrite '^(/static/render/)(.+)_color_([0-9a-fA-F]{6})(_ambient_)([0-9]{1,2})(.+)_([0-9]{1,4})x([0-9]{1,4})\.(jpg|jpeg|png|gif|JPEG|JPG|PNG)$' /colorer(bw=-1,bc=$3,dw=$7,dh=$8)$1$2_ff0000$4$5$6.$9;
# old color width height
		rewrite '^(/static/render/)(.+)_color_([0-9a-fA-F]{6})_([0-9]{1,4})x([0-9]{1,4})\.(jpg|jpeg|png|gif|JPEG|JPG|PNG)$' /colorer(bw=-1,bc=$3,dw=$4,dh=$5)$1$2_Red.$6;
# new color
		rewrite '^(/static/render/)(.+)_color_([0-9a-fA-F]{6})(_ambient_)([0-9]{1,2})(.+)\.(jpg|jpeg|png|gif|JPEG|JPG|PNG)$' /colorer(bw=-1,bc=$3)$1$2_ff0000$4$5$6.$7;
# old color
		rewrite '^(/static/render/)(.+)_color_([0-9a-fA-F]{6})\.(jpg|jpeg|png|gif|JPEG|JPG|PNG)$' /colorer(bw=-1,bc=$3)$1$2_Red.$4;
# static width height
		rewrite '^(/static/.+/)(.+)_([0-9]{1,4})x([0-9]{1,4})\.(jpg|jpeg|png|gif|JPEG|JPG|PNG)$' /colorer(bw=-1,dw=$3,dh=$4,q=80)$1$2.$5;
		rewrite '^(/static/.+/)(.+)_([0-9]{1,4})X([0-9]{1,4})\.(jpg|jpeg|png)$' /colorer(bw=-1,dw=$3,dh=$4,da=n,ds=l,q=80)$1$2.$5;	
	}
	location ~* \.(jpeg|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar|mp4|JPEG|JPG|PNG)$ {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		expires 30d;
		open_file_cache max=10000 inactive=1m; # enable disk cache for this location
                open_file_cache_valid 10m;
                open_file_cache_min_uses 1;
                open_file_cache_errors off;

		try_files $uri =404;
	}
	location ~ ^/(protected|framework|themes/\w+/views) {
		deny  all;
	}
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}
}

# SSL static.treatstock.co.uk static.treatstock.fr
server {
	listen      443 ssl http2;
	server_name static.treatstock.fr static.treatstock.co.uk;
	ssl_certificate		/etc/nginx/ssl/1_treatstock.fr.pem;
	ssl_certificate_key	/etc/nginx/ssl/2_treatstock.fr.key;
#	rewrite_log on;
#	access_log /var/log/nginx/static/static.log main;
	access_log /var/log/nginx/static/static.log main_ext;
	error_log /var/log/nginx/static/static-error.log;
# debug;
	root  /var/www/treatstock/frontend/web/;
#LetsEncrypt
	location ~ /\.well-known/.+ {
		root /var/www/letsencrypt;
		allow all;
	}
	location / {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		expires 30d;
		deny all;
	}
	location ~ ^/colorer\(bw=-1,.+\)(.+)\.(jpg|jpeg|png|gif)$ {
		chunked_transfer_encoding off;
		proxy_http_version 1.1;
		proxy_buffering on;
		proxy_cache cache;
		proxy_cache_valid 365d;
		proxy_cache_valid 404 30m;
		expires	30d;
		add_header X-DontCache $flag_cache_empty;
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		proxy_pass http://127.0.0.1:8989;
	}
	location ~* '^/static/render/([0-9a-fA-F]{4}).+\.(jpeg|jpg|png|gif)$' {
		rewrite ^/static/render/([a-z0-9][a-z0-9])([a-z0-9][a-z0-9])(.+)\.(jpeg|jpg|png|gif) /static/render/$1/$2/$1$2$3.$4;
	}
	location ~* '^/static/render/(.+)_color_(multicolor)(.+)$' {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		expires 30d;
		try_files /static/render/$1_$2$3 @rimage;
	}
	location ~* ^/static/.+\.(jpeg|jpg|png|gif|mp4|svg)$ {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		expires 30d;
		if ($http_origin ~* "^https://.+\.treatstock\.(fr|com|co.uk)$") {
			add_header Access-Control-Allow-Origin $http_origin;
		}
		open_file_cache max=10000 inactive=1m; # enable disk cache for this location
                open_file_cache_valid 10m;
                open_file_cache_min_uses 1;
                open_file_cache_errors off;
		try_files $uri @rimage;
	}
	location @rimage {
# new color multicolor width height
		rewrite '^(/static/render/)(.+)_color_(multicolor)(_ambient_)([0-9]{1,2})(.+)_([0-9]{1,3})x([0-9]{1,3})\.(jpg|jpeg|png|gif)$' /colorer(bw=-1,dw=$7,dh=$8)$1$2_multicolor$4$5$6.$9;
# new color width height
		rewrite '^(/static/render/)(.+)_color_([0-9a-fA-F]{6})(_ambient_)([0-9]{1,2})(.+)_([0-9]{1,3})x([0-9]{1,3})\.(jpg|jpeg|png|gif)$' /colorer(bw=-1,bc=$3,dw=$7,dh=$8)$1$2_ff0000$4$5$6.$9;
# old color width height
		rewrite '^(/static/render/)(.+)_color_([0-9a-fA-F]{6})_([0-9]{1,3})x([0-9]{1,3})\.(jpg|jpeg|png|gif)$' /colorer(bw=-1,bc=$3,dw=$4,dh=$5)$1$2_Red.$6;
# new color
		rewrite '^(/static/render/)(.+)_color_([0-9a-fA-F]{6})(_ambient_)([0-9]{1,2})(.+)\.(jpg|jpeg|png|gif)$' /colorer(bw=-1,bc=$3)$1$2_ff0000$4$5$6.$7;
# old color
		rewrite '^(/static/render/)(.+)_color_([0-9a-fA-F]{6})\.(jpg|jpeg|png|gif)$' /colorer(bw=-1,bc=$3)$1$2_Red.$4;
# static width height
		rewrite '^(/static/.+/)(.+)_([0-9]{1,3})x([0-9]{1,3})\.(jpg|jpeg|png|gif)$' /colorer(bw=-1,dw=$3,dh=$4,q=80)$1$2.$5;
		rewrite '^(/static/.+/)(.+)_([0-9]{1,4})X([0-9]{1,4})\.(jpg|jpeg|png)$' /colorer(bw=-1,dw=$3,dh=$4,da=n,ds=l,q=80)$1$2.$5;
	}
	location ~ \.(png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar|mp4)$ {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		expires 30d;
		try_files $uri =404;
	}
	location ~ ^/(protected|framework|themes/\w+/views) {
		deny  all;
	}
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}
}
