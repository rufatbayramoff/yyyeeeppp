# SSL backned.test.treatstock.com
server {
	listen	443 ssl;
	server_name static.test.treatstock.com;
	ssl_certificate		/etc/nginx/ssl/1_treatstock.com.pem;
	ssl_certificate_key	/etc/nginx/ssl/2_treatstock.com.key;
#	ssl_certificate		/etc/nginx/ssl/1_treatstock_com_RSA_private.crt;
#	ssl_certificate_key	/etc/nginx/ssl/2_treatstock_com_RSA_private.key;
	auth_basic           "test";
	auth_basic_user_file /etc/nginx/pswd;
	set_real_ip_from	74.208.155.35;
	real_ip_header    X-Real-IP;
	real_ip_recursive on;
	location / {
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
		proxy_set_header X-NginX-Proxy true;
		proxy_set_header Host      $host;
		proxy_pass https://10.137.135.30;
	}
}
