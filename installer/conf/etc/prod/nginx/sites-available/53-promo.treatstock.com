# treatstock.biz
server {
	listen	80;
	server_name promo.treatstock.com;
	return 301 https://$host$request_uri;
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}
}
server {
	listen	443 ssl;
	server_name promo.treatstock.com;
	ssl_certificate		/etc/nginx/ssl/1_treatstock.com.pem;
	ssl_certificate_key	/etc/nginx/ssl/2_treatstock.com.key;
#	ssl_certificate		/etc/nginx/ssl/1_treatstock_com_RSA_private.crt;
#	ssl_certificate_key	/etc/nginx/ssl/2_treatstock_com_RSA_private.key;
	set $yii_bootstrap "index.php";
	charset utf-8;
	root  /var/www/promo.treatstock.com;
	location / {
		return 301 https://$host/widget/;
	}
	location /widget/ {
		index index.html $yii_bootstrap;
		try_files $uri $uri/ /$yii_bootstrap?$args;
	}
	location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
		try_files $uri =404;
	}
	location ~ ^/(protected|framework|themes/\w+/views) {
		deny  all;
	}
	location ~ \.php {
		fastcgi_split_path_info  ^(.+\.php)(.*)$;
		set $fsn /$yii_bootstrap;
		if (-f $document_root$fastcgi_script_name){
			set $fsn $fastcgi_script_name;
		}
		fastcgi_pass   unix:/var/run/php/php7.1-fpm.sock;
		include fastcgi_params;
		fastcgi_param  SCRIPT_FILENAME  $document_root$fsn;
		fastcgi_param  HTTPS on;
		fastcgi_param  PATH_INFO        $fastcgi_path_info;
		fastcgi_param  PATH_TRANSLATED  $document_root$fsn;
	}
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}
}