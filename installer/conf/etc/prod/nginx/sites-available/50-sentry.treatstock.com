# redirect to SSL sentry
server {
	listen	80;
	server_name sentry.treatstock.com;
	return 301 https://$host$request_uri;
	set_real_ip_from	74.208.155.35;
	real_ip_header    X-Real-IP;
	real_ip_recursive on;
	access_log /var/log/nginx/sentry/access.log;
	error_log /var/log/nginx/sentry/error.log;
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}
}
# SSL sentry
server {
	listen	443 ssl;
	server_name sentry.treatstock.com;
	ssl_certificate		/etc/nginx/ssl/1_treatstock.com.pem;
	ssl_certificate_key	/etc/nginx/ssl/2_treatstock.com.key;
	set_real_ip_from	74.208.155.35;
	real_ip_header    X-Real-IP;
	real_ip_recursive on;
	access_log /var/log/nginx/sentry/access.log;
	error_log /var/log/nginx/sentry/error.log;
	location / {
		proxy_pass         http://10.137.135.22:9443;
		proxy_set_header   Host                 $http_host;
		proxy_set_header   X-Forwarded-Proto    https;
		proxy_set_header   X-Forwarded-For      $remote_addr;
		proxy_redirect     off;
	}
}