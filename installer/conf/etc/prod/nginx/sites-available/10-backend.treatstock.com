# HTTP redirect to https backend.treatstock.com
server {
	server_name backend.treatstock.com;
	location ~ /\.well-known/.+ {
		root /var/www/letsencrypt;
	}
	return 301 https://backend.treatstock.com$request_uri;
}

# SSL www.treatstock.com
server {
	listen      443 ssl http2;
	server_name backend.treatstock.com;
	ssl_certificate		/etc/nginx/ssl/1_treatstock.com.pem;
	ssl_certificate_key	/etc/nginx/ssl/2_treatstock.com.key;
#	ssl_certificate		/etc/nginx/ssl/1_treatstock_com_RSA_private.crt;
#	ssl_certificate_key	/etc/nginx/ssl/2_treatstock_com_RSA_private.key;
	add_header Strict-Transport-Security "max-age=2592000" always;
	keepalive_timeout  60s;
	include /etc/nginx/denybackend.conf;
	set $yii_bootstrap "index.php";
	charset utf-8;
	root  /var/www/treatstock/backend/web;

	set_real_ip_from	74.208.155.35;
	real_ip_header    X-Real-IP;
	real_ip_recursive on;

#	allow 94.180.253.137; # office ip
#	allow 144.76.45.107; # zabbix server
#	allow 74.208.155.35; # Server
#	allow 92.255.198.33; # gchannel
#	deny all;
	location ~ ^app/apc\.php\?.+$ {
		satisfy any;
		allow 83.69.106.68;
		deny all;
		fastcgi_pass   unix:/var/run/php/php8.0-fpm.sock;
		include fastcgi_params;
		fastcgi_param  SCRIPT_FILENAME  $document_root$fsn;
		fastcgi_param  HTTPS on;
		fastcgi_param  PATH_INFO        $fastcgi_path_info;
		fastcgi_param  PATH_TRANSLATED  $document_root$fsn;
                fastcgi_connect_timeout 120s;
                fastcgi_read_timeout 120s;

	}
	location / {
		index  $yii_bootstrap index.html;
		try_files $uri $uri/ /$yii_bootstrap?$args;
	}
	location ~* /js/ts/.+\.js$ {
		root /var/www/treatstock/frontend/web/;
		try_files $uri =404;
	}
	location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
		try_files $uri =404;
	}
	location ~ ^/(protected|framework|themes/\w+/views) {
		deny  all;
	}
	location ~ \.php {
		fastcgi_split_path_info  ^(.+\.php)(.*)$;
		set $fsn /$yii_bootstrap;
		if (-f $document_root$fastcgi_script_name){
			set $fsn $fastcgi_script_name;
		}
		fastcgi_pass   unix:/var/run/php/php8.0-fpm.sock;
		include fastcgi_params;
		fastcgi_param  SCRIPT_FILENAME  $document_root$fsn;
		fastcgi_param  HTTPS on;
		fastcgi_param  PATH_INFO        $fastcgi_path_info;
		fastcgi_param  PATH_TRANSLATED  $document_root$fsn;
		fastcgi_connect_timeout 120s;
		fastcgi_read_timeout 120s;
	}
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}
}
