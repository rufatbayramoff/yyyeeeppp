server {
	listen		80;
	server_name	ca.treatstock.com;
	root /var/www/ca;
	access_log	/var/log/nginx/ca/access.log;
	error_log	/var/log/nginx/ca/error.log;
	set_real_ip_from	74.208.155.35;
	real_ip_header    X-Real-IP;
	real_ip_recursive on;
	location / {
		try_files $uri $uri/ =403;
	}
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}
}
