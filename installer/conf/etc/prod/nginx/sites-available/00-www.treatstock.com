############ Req limit zone include ###############
include /etc/nginx/smodule/reqlimitzone.conf;
###################################################

############# Cache for colorer ###################
include /etc/nginx/smodule/cache.conf;
###################################################

# HTTP treatstock.com, www.treatstock.com, new.treatstock.com redirecto to https
server {
	listen 80;
	server_name treatstock.com www.treatstock.com new.treatstock.com;
	location ~ /\.well-known/.+ {
		root /var/www/letsencrypt;
	}
	return 301 https://www.treatstock.com$request_uri;
}

# SSL treatstock.com redirect to https://www.treatstock.com
server {
	listen	443 ssl http2;
	server_name treatstock.com;
	ssl_certificate		/etc/nginx/ssl/1_treatstock.com.pem;
	ssl_certificate_key	/etc/nginx/ssl/2_treatstock.com.key;
	return 301 https://www.treatstock.com$request_uri;
}

# SSL www.treatstock.com ru.treatstock.com cn.treatstock.com
server {
	#Standart parameters
	listen      443 ssl http2;
	server_name www.treatstock.com de.treatstock.com ru.treatstock.com zh.treatstock.com jp.treatstock.com new.treatstock.com;
	ssl_certificate		/etc/nginx/ssl/1_treatstock.com.pem;
	ssl_certificate_key	/etc/nginx/ssl/2_treatstock.com.key;
	set $yii_bootstrap "index.php";
	charset utf-8;
	root  /var/www/treatstock/frontend/web;
	add_header Strict-Transport-Security "max-age=2592000" always;

	set_real_ip_from	74.208.155.35;
	real_ip_header    X-Real-IP;
	real_ip_recursive on;

	location ~ /\.well-known/.+ {
		root /var/www/letsencrypt;
	}

	# permanent redirect without slash
	set $redirect_true 1;
	if ($request_uri ~ ^/jsapi/public/basrelief(.*)$){
		set $redirect_true 0;
	}
	if ($request_uri ~ ^/static/status(.*)$){
		set $redirect_true 0;
	}
	if ($redirect_true) {
		rewrite ^/(.*)/$ /$1 permanent;
	}

	#################### Manual deny list ###################
#	include /etc/nginx/denyips;
	include /etc/nginx/denymanual;
	include /etc/nginx/denyfrontend.conf;
	#########################################################

	# Filter locations
	#####################################################################
	location ~* "(\'|\")(.*)(drop|insert|md5|select|union)" { deny all; }
	#####################################################################

	################# Advanced location by Rufat ########################
	location ~ /lp1 {
		root /var/www/lp1;
		index index.html;
	}
	#####################################################################

	######## location for ddosprotect #####################
	include /etc/nginx/smodule/recapcha.conf;
	#######################################################

	###### Status Supervisor from production server #######
	include /etc/nginx/smodule/statussv.conf;
	#######################################################

	######### Status RabbitMQ from production server ######
	include /etc/nginx/smodule/statusrq.conf;
	#######################################################

	######### Status solr from production server ##########
	include /etc/nginx/smodule/statussolr.conf;
	#######################################################

	################## Location for nodesj ######################################
	location ~* ^/jsapi/public/.+ {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		proxy_pass http://localhost:5858;
	}
	#############################################################################

	################### Colorer Location  #################
	include /etc/nginx/smodule/colorer.conf;
	#######################################################

	location ~* ^/(static|assets|js|fonts|css)/.+$ {
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
		expires 30d;
	}

	############## Root location ##########################
	include /etc/nginx/smodule/lfrontend.conf;
	#######################################################

	location ~ ^/api/v2 {
		access_log	/var/log/nginx/api/access.log mainapi;
		error_log	/var/log/nginx/api/error.log;
		if ( $request_uri ~* ".+private-key=(?<key>[a-z0-9]{1,128})(&.+|$)" ) {
			set $limit_uri_get $key;
			set $limit_uri_post $key;
			set $limit_uri_put $key;
		}
		if ( $request_method = "GET" ) {
			set $limit_uri_get "${limit_uri_get}:GET";
			set $limit_uri_post "";
			set $limit_uri_put "";
		}
		if ( $request_method = "POST" ) {
			set $limit_uri_post "${limit_uri_post}:POST";
			set $limit_uri_put "";
			set $limit_uri_get "";
		}
		if ( $request_method = "PUT" ) {
			set $limit_uri_put "${limit_uri_put}:PUT";
			set $limit_uri_post "";
			set $limit_uri_get "";
		}
# Set limit apikey method GET
#		limit_req zone=apikey_get burst=20 nodelay;
		limit_conn apikey_conn_get 7;
# Set limit apikey method POST
#		limit_req zone=apikey_post burst=50 nodelay;
		limit_conn apikey_conn_post 15;
# Set limit apikey method PUT
#		limit_req zone=apikey_put burst=50 nodelay;
		limit_conn apikey_conn_put 15;
# Set limit for server
#		limit_req zone=apiserver burst=160 nodelay;
		limit_conn apiserver_conn 100;
		fastcgi_split_path_info  ^(.+\.php)(.*)$;
		set $fsn /$yii_bootstrap;
		if (-f $document_root$fastcgi_script_name){
			set $fsn $fastcgi_script_name;
		}
		fastcgi_pass   unix:/var/run/php/php8.0-fpm.sock;
		include fastcgi_params;
		fastcgi_param  SCRIPT_FILENAME  $document_root$fsn;
		fastcgi_param  HTTPS on;
		fastcgi_param  PATH_INFO        $fastcgi_path_info;
		fastcgi_param  PATH_TRANSLATED  $document_root$fsn;
	}

	############## Redirect to php-fpm ###########
	include /etc/nginx/smodule/phplocation.conf;
	##############################################

	############# Static files ###################
	include /etc/nginx/smodule/staticfiles.conf;
	##############################################

	############ Deny .* files ###################
	include /etc/nginx/smodule/denylogdot.conf;
	##############################################
}
