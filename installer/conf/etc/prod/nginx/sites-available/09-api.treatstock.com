# SSL api.treatstock.com
server {
	listen 80;
	server_name api.treatstock.com;
	location / {
		default_type text/html;
		return 501 "<html><head><title>501 Not Implemented</title></head><body><center><h1>501 Not Implemented</h1></center><center><h1>HTTPS Protocol Only!</h1></center><hr><center>nginx</center></body></html>";
	}
}
server {
	listen	443 ssl;
	server_name api.treatstock.com;
	ssl_certificate		/etc/nginx/ssl/1_treatstock.com.pem;
	ssl_certificate_key	/etc/nginx/ssl/2_treatstock.com.key;
	root /var/www/html;

	set_real_ip_from	74.208.155.35;
	real_ip_header    X-Real-IP;
	real_ip_recursive on;

	access_log /var/log/nginx/api/api.log main;
#	error_log /var/log/nginx/api-error.log debug;
	location ~ /api/v1 {
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
		proxy_set_header X-NginX-Proxy true;
		proxy_pass https://www.treatstock.com$request_uri;
	}
	location ~ /api/v2 {
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
		proxy_set_header X-NginX-Proxy true;
		proxy_pass https://www.treatstock.com$request_uri;
	}
	location ~ /v1 {
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
		proxy_set_header X-NginX-Proxy true;
		proxy_pass https://www.treatstock.com/api$request_uri;
	}
	location ~ /v2 {
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
		proxy_set_header X-NginX-Proxy true;
		proxy_pass https://www.treatstock.com:443/api$request_uri;
	}
}