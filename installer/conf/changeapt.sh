#!/bin/bash
sourcesd=$(ls -la /etc/apt/sources.list.d/*.list | awk '{print $9}')
allsources="$(ls -la /etc/apt/*.list | awk '{print $9}') $(ls -la /etc/apt/sources.list.d/*.list | awk '{print $9}')"
for fname in $allsources; do
#echo $(dirname $fname)"/"$(basename $fname)".save_"
sudo mv -f $fname $(dirname $fname)"/"$(basename $fname)".save"
while read line
do
    if [ "$(echo $line | grep -E -v '^#' | grep -E -v '^$' | grep -E -v 'http://r1.tsdev.work:3142' )" != "" ]; then
	newline=`echo $line | awk -v server=$server '{gsub("http://","http://r1.tsdev.work:3142/"); print $0}'`
	echo "" >>$fname
	echo $newline >>$fname
	echo '#Original string below.' >>$fname
	echo '#'$line >>$fname
	echo "" >>$fname
    else
	echo $line >>$fname
    fi
done <$(dirname $fname)"/"$(basename $fname)".save"
done
