#!/bin/bash
pack=$(ls -la | grep tgz |grep opencascade | awk '{print $9}')
packver=$(echo $pack | awk -F"-" '{print $2}' | awk -F"." '{print $1$2$3}')
packvert=$(echo $pack | awk -F"-" '{print $2}' | awk -F"." '{print $1"."$2"."$3}')
directory=$(echo $pack | awk -F"." '{print $1"."$2"."$3}')
if [ ! -z $pack ]; then
    if [ "$packver" -ge "710" ]; then
        # Install library
        sudo apt-get install -y tcllib tklib tcl-dev tk-dev libfreetype6-dev libxt-dev libxmu-dev libxi-dev libgl1-mesa-dev libglu1-mesa-dev libfreeimage-dev libtbb-dev libgl2ps-dev doxygen libtool autoconf automake gfortran gdebi g++ csh dh-make
        sudo apt-get install -y libstdc++6:i386 libxt6:i386 libxext6:i386 libxmu6:i386
        rm -rf $directory
        tar xvf $pack
        rm -f "opencascade_$packvert.orig.tar.xz"
        cd $directory
        dh_make --createorig -s -p opencascade -e g.levkovskiy@treatstock.com -n -y
        cat debian/control | \
        sed '/Section: unknown/s/unknown/graphics/g' | \
        sed '/Homepage:/s/.*/Homepage: https:\/\/www.opencascade.com\//g' | \
        sed '/Description:/s/.*/Description: Opencascade/g' | \
        sed '/long description/s/.*/ Opencascade for nodejs/g' >debian/control.new
        rm -f debian/control
        mv -f debian/control.new debian/control
        cat debian/rules | \
        sed '/dh $@/s/.*/	dh $@ --buildsystem=cmake --parallel/g' >debian/rules.new
        echo -e '\n\n\noverride_dh_auto_configure: ' >>debian/rules.new
        echo -e '	dh_auto_configure -- -DCMAKE_BUILD_TYPE=release' >>debian/rules.new
        rm -f debian/rules
        mv -f debian/rules.new debian/rules
        sudo chmod 755 debian/rules
        mkdir -p debian/opencascade/usr/bin
        cat adm/templates/draw.sh | \
        sed '/^DRAWEXE/s/.*/DRAWEXE -b -f "$1"/g' >drawf.sh
        sudo chmod 777 drawf.sh
        echo 'drawf.sh /usr/bin' >'debian/install'
        dpkg-buildpackage -us -uc -b -j4
    else
        echo '##################################################################'
        echo '#      !!!!!!ERROR!!!!!!!                                        #'
        echo '# Need version opencascade greater or equal 7.1.0                #'
        echo '##################################################################'
    fi
else
    echo '##################################################################'
    echo '#      !!!!!!ERROR!!!!!!!                                        #'
    echo '# Not found source opencascade!                                  #'
    echo '# Please download opencascade source from:                       #'
    echo '# https://www.opencascade.com/content/latest-release             #'
    echo '##################################################################'
fi
exit