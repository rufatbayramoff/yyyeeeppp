#!/bin/bash
# Set variables
namepak="openscenegraph-3.4_"`lsb_release -r -s`
# Get Library and utilities for build deb package
sudo apt-get install -y git
sudo apt-get install -y cmake libfreetype6-dev libpng12-dev libjpeg-dev libcurl4-openssl-dev libsdl2-dev libcairo2-dev librsvg2-dev libevent-dev libpcre++-dev libb64-dev libmagick++-dev dh-make
# get release
rm -rf openscenegraph-3.4
mkdir openscenegraph-3.4
cd openscenegraph-3.4
git clone https://github.com/openscenegraph/OpenSceneGraph.git .
git checkout OpenSceneGraph-3.4
patch src/osgPlugins/stl/ReaderWriterSTL.cpp < ../ReaderWriterSTL.patch
# make deb packet
dh_make --createorig -s -p openscenegraph -e g.levkovskiy@treatstock.com -n -y
        cat debian/control | \
        sed '/Section: unknown/s/unknown/graphics/g' | \
        sed '/Homepage:/s/.*/Homepage: http:\/\/www.openscenegraph.org\//g' | \
        sed '/Description:/s/.*/Description: OpenSceneGraph/g' | \
        sed '/long description/s/.*/ OpenSceneGraph for render2/g' >debian/control.new
        rm -f debian/control
        mv -f debian/control.new debian/control
        cat debian/rules | \
        sed '/dh $@/s/.*/	dh $@ --buildsystem=cmake --parallel/g' >debian/rules.new
        echo -e '\n\n\noverride_dh_auto_configure: ' >>debian/rules.new
        echo -e '	dh_auto_configure -- -DCMAKE_BUILD_TYPE=debug' >>debian/rules.new
        rm -f debian/rules
        mv -f debian/rules.new debian/rules
        sudo chmod 755 debian/rules
        dpkg-buildpackage -us -uc -b -j4
        cd debian/openscenegraph/usr/lib64
        ln -s libOpenThreadsd.so.3.3.0 libOpenThreads.so.3.3.0
        ln -s libOpenThreadsd.so.3.3.0 libOpenThreads.so
        ln -s libOpenThreadsd.so.3.3.0 libOpenThreads.so.20
        cd ../../../../
        # add library paths
        strnum=$(grep -n '"configure"' debian/openscenegraph/DEBIAN/postinst | awk -F":" '{print $1}')
        if [ "x$strnum" != "x" ]; then
            awk '{print} NR=='$strnum' {print "	echo \"/usr/lib64\" >/etc/ld.so.conf.d/osg.conf\n	echo \"/usr/local/lib64\" >>/etc/ld.so.conf.d/osg.conf"}' debian/openscenegraph/DEBIAN/postinst >preparepostinst
            mv -f preparepostinst debian/openscenegraph/DEBIAN/postinst
        else
            echo -e "#!/bin/sh\nset -e\nif [ \"\$1\" = \"configure\" ]; then\n	echo \"/usr/lib64\" >/etc/ld.so.conf.d/osg.conf\n	echo \"/usr/local/lib64\" >>/etc/ld.so.conf.d/osg.conf\n	ldconfig\nfi\n" >debian/openscenegraph/DEBIAN/postinst
        fi
        chown root:root debian/openscenegraph/DEBIAN/postinst
        chmod 755 debian/openscenegraph/DEBIAN/postinst
        strnum=$(grep -n '"remove"' debian/openscenegraph/DEBIAN/postrm | awk -F":" '{print $1}')
        echo $strnum
        if [ "x$strnum" != "x" ]; then
            awk '{print} NR=='$strnum' {print "	rm -f /etc/ld.so.conf.d/osg.conf"}' debian/openscenegraph/DEBIAN/postrm >preparepostrm
            mv -f preparepostrm debian/openscenegraph/DEBIAN/postrm
        else
            echo -e "#!/bin/sh\nset -e\nif [ \"\$1\" = \"remove\" ]; then\n	rm -f /etc/ld.so.conf.d/osg.conf\n	ldconfig\nfi\n" >debian/openscenegraph/DEBIAN/postrm
        fi
        chown root:root debian/openscenegraph/DEBIAN/postrm
        chmod 755 debian/openscenegraph/DEBIAN/postrm
        dh_md5sums
        dh_builddeb
        mv ../openscenegraph_3.4_amd64.deb ../openscenegraph_3.4_`lsb_release -r -s`_amd64.deb