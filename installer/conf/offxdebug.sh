#!/bin/bash
source "$(dirname "$0")/config.sh"
if $debug;then set -xe;fi

sudo apt-get purge -y php-xdebug
sudo rm -rf /etc/php/$phpver/cli-noxdebug
sudo rm -rf /etc/php/$phpver/cli-xdebug
sudo rm -rf /etc/php/$phpver/mods-available/xdebug.ini
sudo service php$phpver-fpm restart
sudo rm -rf $config_dir/xdebug.flag