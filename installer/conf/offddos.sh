#!/bin/bash
# environment $sdir - Каталог установки сайта.
if [ -f /vagrant/conf/pathtoinstall.env ]; then
i=$(cat /vagrant/conf/pathtoinstall.env)
$i
else
export sdir=/vagrant/repo
fi
sudo sed -e '/\.lua/s/^./#	/g' /etc/nginx/sites-available/frontend |tee -a /tmp/frontend.on
sudo mv -f /tmp/frontend.on /etc/nginx/sites-available/frontend
