#!/bin/bash
# Скрипт проверяющий что установка запущена не под root и для проверки версии ОС.
# Нужная версия ОС - Ubuntu 20.04 
# Под пользователем root нельзя делать установку, только под ограниченным пользователем, та как это вызывает проблемы в работе инсталляции 
source "$(dirname "$0")/config.sh"

# check user id
if [ `id -u` = 0 ]; then
  error "you should NOT this run as a root. user will owner of code
  but user running this script SHOULD have home in /home and have sudo enabled"
fi
# Check os version
if [ $(lsb_release -c -s) != "focal" ];then
  error "Sorry, this installer support only Ubuntu 20 (Focal Fossa)
    You OS - $(lsb_release -c -s), please use supported OS"
fi