#!/bin/bash
set -xe
export suffixnginx=-im
sdir=/var/www/treatstock
# install nginx and set up configs

sudo systemctl stop nginx || true
sudo ps -ax | pgrep [n]ginx | xargs sudo kill -9 || true

if [ ! -d /opt/nginx.src ]; then
    sudo mkdir -p /opt/nginx.src
fi
cd /opt/nginx.src
wand=/root/ImageMagick-7.1.0-16/MagickWand/MagickWand-config
sudo ln -si $wand /usr/bin/Wand-config || true
cd $sdir/tools/small_light
sudo chmod 755 ./setup
sudo  ./setup
cd /opt/nginx.src
sudo chmod 777 .
ubucodename=`lsb_release -c | awk '{print $2}'`
maxvernginx=`apt search nginx-core 2>/dev/null |grep -E "^nginx\/$ubucodename" | awk '{print $2}' | grep -o -P '^\d+\.\d+\.\d+'`
echo $maxvernginx
if [ ! -f /opt/nginx.src/nginx-$maxvernginx.tar.gz ]; then
   sudo wget http://nginx.org/download/nginx-$maxvernginx.tar.gz
fi
tar xvf /opt/nginx.src/nginx-$maxvernginx.tar.gz
cd nginx-$maxvernginx
./configure --with-cc-opt='-g -O2 -fPIE -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2' \
    --with-ld-opt='-Wl,-Bsymbolic-functions -fPIE -pie -Wl,-z,relro -Wl,-z,now' \
    --prefix=/usr/share/nginx$suffixnginx \
    --sbin-path=/usr/sbin/nginx$suffixnginx \
    --conf-path=/etc/nginx$suffixnginx/nginx.conf \
    --http-log-path=/var/log/nginx$suffixnginx/access.log \
    --error-log-path=/var/log/nginx$suffixnginx/error.log \
    --lock-path=/var/lock/nginx$suffixnginx.lock \
    --pid-path=/run/nginx$suffixnginx.pid \
    --http-client-body-temp-path=/var/lib/nginx$suffixnginx/body \
    --http-fastcgi-temp-path=/var/lib/nginx$suffixnginx/fastcgi \
    --http-proxy-temp-path=/var/lib/nginx$suffixnginx/proxy \
    --http-scgi-temp-path=/var/lib/nginx$suffixnginx/scgi \
    --http-uwsgi-temp-path=/var/lib/nginx$suffixnginx/uwsgi \
    --with-pcre-jit \
    --with-ipv6 \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_realip_module \
    --with-http_auth_request_module \
    --with-http_addition_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_geoip_module \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_image_filter_module \
    --with-http_mp4_module \
    --with-http_perl_module \
    --with-http_random_index_module \
    --with-http_secure_link_module \
    --with-http_v2_module \
    --with-http_sub_module \
    --with-http_xslt_module \
    --with-mail \
    --with-mail_ssl_module \
    --with-stream \
    --with-stream_ssl_module \
    --with-threads \
    --with-debug \
    --add-module=$sdir/tools/small_light
make -j4
sudo make install
if [ ! -d /var/lib/nginx$suffixnginx ]; then
    sudo mkdir /var/lib/nginx$suffixnginx
fi

