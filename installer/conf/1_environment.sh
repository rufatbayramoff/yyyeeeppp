#!/bin/bash
source "$(dirname "$0")/config.sh"
source "$(dirname "$0")/check_env.sh"
if $debug;then set -xe;fi

 if [ ! -f $HOME/.ssh/known_hosts ]
    then ssh-keygen -b 2048 -t rsa -f $HOME/.ssh/known_hosts -q -N ""  && infoOk "generate SSH key" || error "generate SSH key"
fi

info "change hostname for sentry"
if [ ! -d $config_dir ];then
    sudo mkdir -p $config_dir
    sudo chown -R $USER:$USER $config_dir infoOk
fi
if [ -f /vagrant/conf/hostid.php ]; then
    cp /vagrant/conf/hostid.php $config_dir/hostid.php
fi
if [ -f "$config_dir/hostid.php" ]; then
    hostidlocal=`cat $config_dir/hostid.php | grep 'return' | awk '{print $2}' | tr -d ';'`
    if [ "x$hostidlocal" == "x" ]; then
        hostidlocal=`ifconfig |grep 'inet addr' | head -1 | awk '{print $2}' | awk -F":" '{print $2}' | awk -F"." '{print $1"_"$2"_"$3"_"$4}'`
    fi
    sudo hostname -b h$hostidlocal
    echo "h$hostidlocal.tsdev.work" | sudo tee '/etc/hostname' 2>&1 >/dev/null
	set +e
    cat /etc/hosts | grep -q "h$hostidlocal"
    if [ $? -ne 0 ]; then
        echo -e "\n127.0.0.1 h$hostidlocal" | sudo tee -a /etc/hosts 2>&1 >/dev/null
    fi
	set -e
fi

function fAddCron {
    # cron daily for dev
     echo '#!/bin/bash'"
sudo wget -q http://clamav.tsdev.work/todownload/1_linlet.net.pem -O /etc/nginx/ssl/1_linlet.net.pem
sudo wget -q http://clamav.tsdev.work/todownload/2_linlet.net.key -O /etc/nginx/ssl/2_linlet.net.key
sudo wget -q http://clamav.tsdev.work/todownload/1_control.h1.tsdev.work.pem -O /etc/nginx/ssl/1_control.h1.tsdev.work.pem
sudo wget -q http://clamav.tsdev.work/todownload/2_control.h1.tsdev.work.key -O /etc/nginx/ssl/2_control.h1.tsdev.work.key
sudo nginx -t && sudo nginx -s reload" | sudo tee /etc/cron.daily/update-ssl && sudo chmod +x /etc/cron.daily/update-ssl
}

function fAccess {
    info "add $me to sudoers witout password"
    echo "$me ALL=(ALL:ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/$me-user
    if [ `grep 7+S2WUXNFnqD pakhomov@wchannel /home/$me/.ssh/known_hosts | wc -l`=0 ];then 
        info "add admin PC to ssh"
        echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCtlpy4Dgaj4cSV/9viLU3VigABNhwUaygSOr3ONL9o6uiBU6elBrZ6BLpghmZTisFuwCdlS97CCEIoVRbyqLn69EQL5qjZ0pkl5kHE53M1D9JALyDD02DIFBOGhJ2clSwYoCLjNYQNnPaOlX+8rGXggSgcdmBye8GSRyCt9bZ6QRuW6y9zlTdQ7qQqcMMKwv3JSUb/B3XzvuifLOsEZWMn3fePqDPOUAaupJrBIxIjPDkSbH0TpRasLj02taOxGAlYFlfsc8p7VFCECA0hLILgjcqfF8QQto5o51wxnYBGPhwj4rMpjBB9h2oG7aF/lMyljCOhqnS/7+S2WUXNFnqD pakhomov@wchannel" | sudo tee /home/$me/.ssh/known_hosts
    fi
} 

# ssh configuration
sed -E 's/#PasswordAuthentication.+/PasswordAuthentication yes/g' /etc/ssh/sshd_config >/tmp/sshd_config.new
sed -E 's/PasswordAuthentication.+/PasswordAuthentication yes/g' /tmp/sshd_config.new >/tmp/sshd_config.new2
sudo mv /etc/ssh/sshd_config /etc/ssh/sshd_config.old # backup config
sudo mv /tmp/sshd_config.new2 /etc/ssh/sshd_config
sudo rm -f /tmp/sshd_config.new
sudo rm -f /tmp/sshd_config.new2
sudo chown root:root /etc/ssh/sshd_config
sudo service ssh restart || true

# Use old config to apt-get
export DEBIAN_FRONTEND=noninteractive
echo 'Dpkg::Options::{"--force-confold";"--force-confdef";"--force-confmiss";}' | sudo tee /etc/apt/apt.conf.d/local

# Create locales for console
sudo locale-gen en_US.UTF-8
sudo locale-gen ru_RU.UTF-8

# add route Artem Arno access to h1-h11.tsdev.work in openvpn
# test "1-`netstat -rn | grep '10.137.135' | awk '{print $1}'`" == "1-10.137.135.0" || sudo route add -net 10.137.135.0/24 gw 10.102.0.50

# Увеличение лимитов открытых файлов в системе.
sudo sh -c 'echo fs.file-max = 1622882 >> /etc/sysctl.conf'
sudo sh -c 'echo "*     hard     nofile     50000" >>/etc/security/limits.conf'
sudo sh -c 'echo "*     soft     nofile     50000" >>/etc/security/limits.conf'
sudo sh -c 'echo "root  hard     nofile     50000" >>/etc/security/limits.conf'
sudo sh -c 'echo "root  hard     nofile     50000" >>/etc/security/limits.conf'

# Добавление репозиторя PHP
if [ ! -f /etc/apt/sources.list.d/ondrej-ubuntu-php-`lsb_release -c -s`.list ]; then
    finstall language-pack-en-base 
    sudo LC_ALL=en_US.UTF-8 add-apt-repository -s -y ppa:ondrej/php && infoOk "PHP repository added"
fi

# Обновление репозиториев
fupdate

# add server monitor utilities
finstall "mc htop iotop sshpass pv dos2unix zip git acl"

info "install MySQL"
echo "mysql-community-server mysql-community-server/root-pass password $mysqlpasswd" | sudo debconf-set-selections
echo "mysql-community-server mysql-community-server/re-root-pass password $mysqlpasswd" | sudo debconf-set-selections
echo "mysql-community-server mysql-community-server/remove-data-dir boolean $mysqlpasswd" | sudo debconf-set-selections
echo "mysql-community-server mysql-community-server/data-dir note" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password password $mysqlpasswd" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $mysqlpasswd" | sudo debconf-set-selections
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-server 1>/dev/null && infoOk "install MySQL server" || error "install MySQL server"

step="stop MySQL"
#sudo systemctl stop mysql || sudo killall -9 mysqld || true
sudo /etc/init.d/mysql stop && infoOk "$step" || error "$step"
# cat /etc/mysql/mysql.conf.d/mysqld.cnf | sed '/bind-address/s/127.0.0.1/0.0.0.0/g' | sed '/skip-external-locking/s/skip-external-locking/#skip-external-locking/g' > /tmp/my.cnf.new
sudo cp -f /etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf.backup
sudo cp -f $sdir/installer/conf/etc/mysql/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf

step="start MySQL"
#sudo systemctl start mysql
sudo /etc/init.d/mysql start && infoOk "$step" || error "$step"

# Create database
bash $sdir/installer/conf/99_mysql_config.sh $environment $mysqlpasswd && infoOk "Successful execute script 99_mysql_config.sh" || error "error. 99_mysql_config.sh"

#### install nginx and set up configs
#function installNginx {
# Добавление репозитория nginx
finstall "curl gnupg2 ca-certificates lsb-release ubuntu-keyring"
curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor \
    | sudo tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] \
http://nginx.org/packages/ubuntu `lsb_release -cs` nginx" \
    | sudo tee /etc/apt/sources.list.d/nginx.list
if [ ! -f /etc/apt/sources.list.d/ondrej-ubuntu-nginx-`lsb_release -c -s`.list ]; then
    sudo add-apt-repository -s -y ppa:ondrej/nginx && infoOk "Nginx repository added"
fi
fupdate

#if [ ! -f /etc/apt/sources.list.d/ondrej-ubuntu-nginx-`lsb_release -c -s`.list ]; then
#    sudo add-apt-repository -s -y ppa:ondrej/nginx && infoOk "Nginx repository added"
#fi

info "install Nginx"
if [ -f /etc/nginx/nginx.conf ];then sudo mv -f /etc/nginx/nginx.conf /tmp/nginx.conf.old; fi
sudo systemctl stop nginx || true
sudo apt-get -y purge nginx* 1>/dev/null && infoOk "remove current nginx" || error "remove current nginx" # Удаляем предыдущую версию nginx для решения проблем 
#sudo ps -ax | pgrep [n]ginx | xargs sudo kill -9 || true

finstall "nginx-extras nginx-common --reinstall"
sudo systemctl enable nginx

# start install nginx
info "Начало установки nginx$suffixnginx"
if [ ! -d /opt/nginx.src ]; then
    sudo mkdir -p /opt/nginx.src
fi
cd /opt/nginx.src
#sudo apt-get build-dep -y --allow-downgrades nginx
# sudo apt-get install -y lib{geoip,ssl,xslt,perl}-dev
finstall "imagemagick libmagickwand-dev libgd-dev libpcre3-dev"
export wand=`find /usr/lib/x86_64-linux-gnu/ImageMagick-* -name Wand-config`
sudo ln -sf $wand /usr/bin/Wand-config || true
cd $sdir/tools/small_light
sudo chmod 755 ./setup
sudo  ./setup
cd /opt/nginx.src
sudo chmod 777 .
#ubucodename=`lsb_release -c | awk '{print $2}'`
#maxvernginx=`apt search nginx-core 2>/dev/null |grep -E "^nginx\/$ubucodename" | awk '{print $2}' | grep -o -P '^\d+\.\d+\.\d+'`
maxvernginx="1.20.2"
info "target version of nginx: $maxvernginx"
if [ ! -f /opt/nginx.src/nginx-$maxvernginx.tar.gz ]; then
   sudo wget -q http://nginx.org/download/nginx-$maxvernginx.tar.gz
fi
sudo tar xf /opt/nginx.src/nginx-$maxvernginx.tar.gz && infoOk "nginx decompress"
cd nginx-$maxvernginx
sudo ./configure --with-cc-opt='-g -O2 -fPIE -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2' \
    --with-ld-opt='-Wl,-Bsymbolic-functions -fPIE -pie -Wl,-z,relro -Wl,-z,now' \
    --prefix=/usr/share/nginx$suffixnginx \
    --sbin-path=/usr/sbin/nginx$suffixnginx \
    --conf-path=/etc/nginx$suffixnginx/nginx.conf \
    --http-log-path=/var/log/nginx$suffixnginx/access.log \
    --error-log-path=/var/log/nginx$suffixnginx/error.log \
    --lock-path=/var/lock/nginx$suffixnginx.lock \
    --pid-path=/run/nginx$suffixnginx.pid \
    --http-client-body-temp-path=/var/lib/nginx$suffixnginx/body \
    --http-fastcgi-temp-path=/var/lib/nginx$suffixnginx/fastcgi \
    --http-proxy-temp-path=/var/lib/nginx$suffixnginx/proxy \
    --http-scgi-temp-path=/var/lib/nginx$suffixnginx/scgi \
    --http-uwsgi-temp-path=/var/lib/nginx$suffixnginx/uwsgi \
    --with-pcre-jit \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_realip_module \
    --with-http_auth_request_module \
    --with-http_addition_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_geoip_module \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_image_filter_module \
    --with-http_mp4_module \
    --with-http_perl_module \
    --with-http_random_index_module \
    --with-http_secure_link_module \
    --with-http_v2_module \
    --with-http_sub_module \
    --with-http_xslt_module \
    --with-mail \
    --with-mail_ssl_module \
    --with-stream \
    --with-stream_ssl_module \
    --with-threads \
    --with-debug \
    --add-module=$sdir/tools/small_light 1>/dev/null && infoOk "nginx$suffixnginx source config"
sudo make -j8 1>/dev/null && infoOk "nginx$suffixnginx make complete"
sudo make install 1>/dev/null && infoOk "nginx$suffixnginx installed"
if [ ! -d /var/lib/nginx$suffixnginx ]; then
    sudo mkdir /var/lib/nginx$suffixnginx
fi
cat /etc/init.d/nginx | \
sed -r -e "/^DAEMON/s/.+/DAEMON=\/usr\/sbin\/nginx$suffixnginx/g" | \
sed -r -e "/Provides:/s/nginx/nginx$suffixnginx/g" |\
sed -r -e "/^NAME/s/.+/NAME=nginx$suffixnginx/g" | \
sed -r -e "/^DESC/s/.+/DESC=nginx$suffixnginx/g" | \
sed -r -e "/\/etc\/nginx/s/\/etc\/nginx/\/etc\/nginx$suffixnginx/g" | \
sed -r -e "/\/run\/nginx\.pid/s/nginx\.pid/nginx$suffixnginx\.pid/g" | sudo tee /etc/init.d/nginx$suffixnginx 1>/dev/null && infoOk "init scripts nginx$suffixnginx installed"
sudo chmod 755 /etc/init.d/nginx$suffixnginx
sudo chown root:root /etc/init.d/nginx$suffixnginx
sudo update-rc.d nginx$suffixnginx defaults
uver=`lsb_release -r | awk '{print $2}' | awk -F"." '{print $1}'`
if [ "$uver" \> "14" ]; then
    cat /lib/systemd/system/nginx.service | \
    sed -r -e "/\/usr\/sbin\/nginx/s/\/usr\/sbin\/nginx/\/usr\/sbin\/nginx$suffixnginx/g" | \
    sed -r -e "/nginx\.pid/s/nginx\.pid/nginx$suffixnginx\.pid/g" | sudo tee /lib/systemd/system/nginx$suffixnginx.service  1>/dev/null && infoOk "service nginx$suffixnginx installed"
    sudo systemctl daemon-reload
    sudo systemctl enable nginx$suffixnginx.service
fi

# end build nginx
sudo mkdir -p /var/lib/nginx/cache
sudo chown -R www-data:www-data /var/lib/nginx/cache

# install nginx config
sudo bash $sdir/installer/conf/99_nginx_config.sh 1>/dev/null && infoOk "Successful execute script 99_nginx_config.sh" || error "Failure of execute 99_nginx_config.sh"

# change phpver in phplocation.conf
sudo sed -i "s/php7.2-fpm/php$phpver-fpm/g" /etc/nginx/smodule/phplocation.conf
sudo sed -i "s/php7.2-fpm/php$phpver-fpm/g" /etc/nginx/smodule/recapcha.conf
sudo sed -i "s/php7.2-fpm/php$phpver-fpm/g" /etc/nginx/sites-available/phpmyadmin

#}
#installNginx
# Install sphinxsearch
finstall "sphinxsearch"

# install php
info "install php $phpver"
sudo dpkg --configure -a
#selectphp=`update-alternatives --list php | sed =  | sed 'N;s/\n/\t/' | grep "php$phpver" | awk '{print $1}'`
#echo $selectphp | sudo update-alternatives --config php
# PHP common
sudo apt-get install -y php$phpver-{fpm,common,cli,cgi,mysql,gd,curl,intl,xml,soap,mbstring,zip,bz2,bcmath,gmp,imap} && infoOk "installed php$phpver" || error "install: Failure installation"
# PHP < 8, в случае php8 mcrypt нужно убрать
#info "install php$phpver-mcrypt"
#sudo apt-get install -y php$phpver-mcrypt
# PHP additional
info "install php$phpver-{amqp,igbinary,zmq} php-pear"
sudo apt-get install -y php$phpver-{amqp,igbinary,zmq} php-pear 1>/dev/null && infoOk "installed php$phpver-{amqp,igbinary,zmq} php-pear" || error "install: Failure installation"
sudo apt-get install -y --no-install-recommends php$phpver-apcu 1>/dev/null && infoOk "installed php$phpver-apcu" || error "install: Failure installation"
sudo phpenmod amqp

#selectphp=`update-alternatives --list php | sed =  | sed 'N;s/\n/\t/' | grep "$phpver" | awk '{print $1}'`
#echo $selectphp | sudo update-alternatives --config php
#update-alternatives --list php
sudo update-alternatives --set php /usr/bin/php$phpver

# config php
sudo cp /etc/php/$phpver/fpm/php.ini /etc/php/$phpver/fpm/php.ini.bakup # Делаем бэкап
cat /etc/php/$phpver/fpm/php.ini | \
 sed '/fix_pathinfo/s/1/0/g' | \
 sed '/fix_pathinfo/s/;cgi/cgi/g' | \
 sed '/upload_max_filesize/s/2M/500M/g' | \
 sed '/post_max_size/s/8M/500M/g' | \
 sed -r '/max_execution_time/s/.*/max_execution_time = 300/g' | \
 sed -r '/max_input_time/s/.*/max_input_time = 600/g' | \
 sed '/memory_limit/s/128M/1024M/g' | sudo tee /etc/php/$phpver/fpm/php.ini 1>/dev/null

sudo cp /etc/php/$phpver/cli/php.ini /etc/php/$phpver/cli/php.ini.bakup # Делаем бэкап
cat /etc/php/$phpver/cli/php.ini | \
 sed '/fix_pathinfo/s/1/0/g' | \
 sed '/fix_pathinfo/s/;cgi/cgi/g' | \
 sed '/upload_max_filesize/s/2M/500M/g' | \
 sed '/post_max_size/s/8M/500M/g' | \
 sed -r '/max_execution_time/s/.*/max_execution_time = 300/g' | \
 sed -r '/max_input_time/s/.*/max_input_time = 600/g' | \
 sed -r 's/;date\.timezone =/date\.timezone = Europe\/Moscow/g' | sudo tee /etc/php/$phpver/cli/php.ini 1>/dev/null

sudo mkdir -p /var/run/php

echo "[www]
user = www-data
group = www-data
listen = /var/run/php/php$phpver-fpm.sock
listen.owner = www-data
listen.group = www-data
pm = dynamic
pm.max_children = 50
pm.start_servers = 20
pm.min_spare_servers = 10
pm.max_spare_servers = 20
pm.max_requests = 50000
pm.status_path = /status-fpm
rlimit_files = 102400
rlimit_core = 0
chdir = /" | sudo tee /etc/php/$phpver/fpm/pool.d/www.conf
echo "[control]
user = $me
group = $me
listen = /var/run/php/php$phpver-fpm-control.sock
listen.owner = www-data
listen.group = www-data
pm = dynamic
pm.max_children = 50
pm.start_servers = 5
pm.min_spare_servers = 5
pm.max_spare_servers = 20
pm.max_requests = 50000
pm.status_path = /status-fpm
rlimit_files = 10240
rlimit_core = 1
chdir = /" | sudo tee /etc/php/$phpver/fpm/pool.d/control.conf

if [ $environment = "dev" ];then
    info "install php-dev"
    sudo apt-get install -y php$phpverdev-dev
    sudo apt-get install -y php$phpverdev-intl
fi

# Enable xdebug if flag on only for development environment
if [ $environment = "dev" ];then
    sudo $sdir/installer/conf/onxdebug.sh $sdir $phpver $config_dir && infoOk "Successful execute script onxdebug.sh" || error " onxdebug.sh"
fi

# restart php$phpver-fpm
sudo systemctl restart php$phpverdev-fpm
echo -e "\nResult restart php: $?\n"

# install tools for STL file processing
finstall redis-server

# install supervisord and rabbitmq-server
info "install supervisord and rabbitmq-server"
sudo service rabbitmq-server stop || true
finstall "rabbitmq-server python3-pip"
sudo apt-get purge -y supervisor
sudo pip3 install supervisor
sudo mkdir -p /etc/supervisor/conf.d
sudo mkdir -p /var/log/supervisor
sudo cp -f $sdir/installer/conf/supervisor /etc/init.d
sudo chmod 755 /etc/init.d/supervisor
sudo update-rc.d supervisor defaults
sudo systemctl daemon-reload
sudo service rabbitmq-server stop
sudo rm -rf /var/lib/rabbitmq/mnesia/* 
sudo rabbitmq-plugins enable rabbitmq_management
sudo service rabbitmq-server start
sudo rabbitmqctl add_user root 1234567 || true
sudo rabbitmqctl set_user_tags root management
sudo rabbitmqctl set_permissions -p / root ".*" ".*" ".*"
echo '[{rabbit,[{tcp_listeners,[{"127.0.0.1",5672}]}]}].' | sudo tee /etc/rabbitmq/rabbitmq.config
sudo service rabbitmq-server restart

# create config supervisor
info "create config supervisor"
sudo bash $sdir/installer/conf/99_create_config_supervisor.sh && infoOk "Successful execute script 99_create_config_supervisor.sh" || error "99_create_config_supervisor.sh"

sudo systemctl enable supervisor
# в данный момент supervisor не стартанёт так как не все компоненты еще установлены.

# Install NodeApi dont move this script before install supervisor
finstall gdebi
sudo bash $sdir/installer/conf/nodeapi.sh $sdir && infoOk "Successful execute script nodeapi.sh" || error "nodeapi.sh"

if [ $environment = "dev" ] || [ $environment = "test" ];then
    # Install and configure postfix
    sudo /usr/bin/debconf-set-selections <<< "postfix postfix/mailname string ts.vcap.me"
    sudo /usr/bin/debconf-set-selections <<< "postfix postfix/main_mailer_type string 'TS dev'"
    finstall "postfix mailutils"

    # Install and config mailcatcher
    finstall "ruby-dev libsqlite3-dev ruby-full"
    if [ "$(ifconfig | grep 'inet ' | grep '10.102' | awk '{print $2}' | awk -F":" '{print $2}' | awk -F"." '{print $1$2}')" == "10102" ]; then
        gem sources -r http://rubygems.org/
        gem sources -r https://rubygems.org/
        gem sources -a http://clamav.tsdev.work:3143/
    fi
    sudo gem install mailcatcher -v 0.7.1
    sudo cp $sdir/installer/conf/mailcatcher.init /etc/init.d/mailcatcher
    sudo chmod +x /etc/init.d/mailcatcher
    sudo /etc/init.d/mailcatcher start
    sudo update-rc.d mailcatcher defaults

    if [ "$(ifconfig | grep 'inet ' | grep '10.102' | awk '{print $2}' | awk -F":" '{print $2}' | awk -F"." '{print $1$2}')" == "10102" ]; then
        gem sources -a https://rubygems.org/
    fi
fi

# Библиотеки для работы с PDF
info "установка библиотек для работы с PDF"
finstall "pdftk pdftohtml xfonts-75dpi xfonts-base"
sudo wget -q https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.bionic_amd64.deb -O wkhtmltox_0.12.6-1.bionic_amd64.deb
sudo dpkg -i ./wkhtmltox_0.12.6-1.bionic_amd64.deb
finstall xvfb

# ffmpeg for video screenshot
finstall ffmpeg

# Установка phpmyadmin
# Лучше переделать на версию из репозитория 
info "установка phpMyAdmin"
cd /var/www/
if [ ! -f /var/www/phpMyAdmin-5.1.0-all-languages.zip ]; then
	set +e
    curl --output /dev/null --silent --head --fail http://clamav.tsdev.work/todownload/phpMyAdmin-5.1.0-all-languages.zip
    if [ "$?" == "0" ]; then
		set -e
        sudo wget -q http://clamav.tsdev.work/todownload/phpMyAdmin-5.1.0-all-languages.zip
    else
		set -e
        sudo wget -q https://files.phpmyadmin.net/phpMyAdmin/5.1.0/phpMyAdmin-5.1.0-all-languages.zip
    fi
fi
sudo rm -rf /var/www/phpMyAdmin-5.1.0-all-languages
sudo rm -rf /var/www/phpmyadmin
sudo unzip /var/www/phpMyAdmin-5.1.0-all-languages.zip 1>/dev/null && infoOk "phpMyAdmin decapress"
sudo mv phpMyAdmin-5.1.0-all-languages phpmyadmin
sudo chown -R www-data:www-data /var/www/phpmyadmin
sudo cp -f $sdir/installer/conf/etc/nginx/sites-available/phpmyadmin /etc/nginx/sites-available/phpmyadmin
sudo ln -sf /etc/nginx/sites-available/phpmyadmin /etc/nginx/sites-enabled/phpmyadmin || true
passwdnginx=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-6};echo;)
echo "admin - $passwdnginx" | sudo tee $config_dir/phpmyadmin.password
finstall "apache2-utils --reinstall"
sudo htpasswd -b -c /etc/nginx/passwd.phpmyadmin admin $passwdnginx
echo "<?php
\$i = 0;
\$i++;
if(function_exists('xdebug_disable')) { xdebug_disable(); }
\$cfg['Servers'][\$i]['verbose'] = 'localhost';
\$cfg['Servers'][\$i]['host'] = 'localhost';
\$cfg['Servers'][\$i]['port'] = 3306;
\$cfg['Servers'][\$i]['socket'] = '/var/run/mysqld/mysqld.sock';
\$cfg['Servers'][\$i]['connect_type'] = 'socket';
\$cfg['Servers'][\$i]['auth_type'] = 'config';
\$cfg['Servers'][\$i]['user'] = 'root';
\$cfg['Servers'][\$i]['password'] = '$mysqlpasswd';
\$cfg['DefaultLang'] = 'en';
\$cfg['ServerDefault'] = 1;
\$cfg['UploadDir'] = '';
\$cfg['SaveDir'] = '';
\$cfg['blowfish_secret'] = 'ab347289cfe212';
?>" | sudo tee /var/www/phpmyadmin/config.inc.php 1>/dev/null && infoOk "phpMyAdmin config installed\n\nlogin: admin\npassword: $passwdnginx"

sudo service nginx restart

#Каталоги static,storage,render,files
dir="frontend/runtime/render
frontend/runtime/logs/printersBundle
frontend/web/static/user
frontend/web/static/files
frontend/web/static/render
frontend/storage
frontend/runtime"
for i in $dir; do
    sudo mkdir -p $sdir/$i && sudo chown -R www-data:www-data $sdir/$i && sudo chmod -R 755 $sdir/$i && infoOk "set dir permission" || error "set dir permission"
done

# install renderer2
info "установка render2"
cd $sdir/tools/renderer2
# rm -f ./openscenegraph_3.4.0-1_amd64.deb.*
# if [ ! -f ./openscenegraph_3.4_14.04_amd64.deb ]; then
#     sudo wget http://clamav.tsdev.work/todownload/openscenegraph_3.4_14.04_amd64.deb -O ./openscenegraph_3.4_14.04_amd64.deb
# fi
# if [ ! -f ./openscenegraph_3.4_16.04_amd64.deb ]; then
#     sudo wget http://clamav.tsdev.work/todownload/openscenegraph_3.4_16.04_amd64.deb -O ./openscenegraph_3.4_16.04_amd64.deb
# fi

# # Install packets for openscenegraph
# echo "install deb for openscenegraph"
# sudo apt-get install -y libgles2-mesa-dev libsdl2-2.0-0 libegl1-mesa-dev
# sudo apt-get install -y cmake libfreetype6-dev libpng-dev libjpeg-dev libcurl4 libcurl4-openssl-dev libsdl2-dev libcairo2-dev librsvg2-dev libevent-dev libpcre++-dev libb64-dev libmagick++-dev build-essential gdebi

# # Install openscenegraph
# info "установка openscenegraph"
# case "x$versionu" in
#     "x14.04")
#         # install gsoap
#         sudo apt-get install -y gsoap
#         sudo echo "yy" | sudo gdebi openscenegraph_3.4_14.04_amd64.deb
#         sudo dpkg -i openscenegraph_3.4_14.04_amd64.deb
#     ;;
#     "x16.04")
#         apt-get purge -y gsoap libgsoap-dev libgsoap8
#         # create symlink to ImageMagick
#         if [ ! -e /usr/include/ImageMagick ]; then
#             ln -sf /usr/include/ImageMagick-6 /usr/include/ImageMagick
#         fi
#         if [ ! -e /usr/include/ImageMagick-6/magick/magick-baseconfig.h ]; then
#             ln -sf /usr/include/x86_64-linux-gnu/ImageMagick-6/magick/magick-baseconfig.h /usr/include/ImageMagick-6/magick/magick-baseconfig.h
#         fi
#         # install gsoap
#         if [ ! -f ./libgsoap4_2.8.16-2_amd64.deb ]; then
#             wget https://launchpad.net/ubuntu/+source/gsoap/2.8.16-2/+build/5208555/+files/libgsoap4_2.8.16-2_amd64.deb -O libgsoap4_2.8.16-2_amd64.deb
#             dpkg -i ./libgsoap4_2.8.16-2_amd64.deb
#         else
#             dpkg -i ./libgsoap4_2.8.16-2_amd64.deb
#         fi
#         if [ ! -f ./libgsoap-dev_2.8.16-2_amd64.deb ]; then
#             wget https://launchpad.net/ubuntu/+source/gsoap/2.8.16-2/+build/5208555/+files/libgsoap-dev_2.8.16-2_amd64.deb -O libgsoap-dev_2.8.16-2_amd64.deb
#             dpkg -i ./libgsoap-dev_2.8.16-2_amd64.deb
#         else
#             dpkg -i ./libgsoap-dev_2.8.16-2_amd64.deb
#         fi
#         if [ ! -f ./gsoap_2.8.16-2_amd64.deb ]; then
#             wget https://launchpad.net/ubuntu/+source/gsoap/2.8.16-2/+build/5208555/+files/gsoap_2.8.16-2_amd64.deb -O gsoap_2.8.16-2_amd64.deb
#             dpkg -i ./gsoap_2.8.16-2_amd64.deb
#         else
#             dpkg -i ./gsoap_2.8.16-2_amd64.deb
#         fi
#         #Create Symlink to library ImageMagick
#         if [ ! -e /usr/lib/x86_64-linux-gnu/libMagick++.so ]; then
           #  ln -sf /usr/lib/x86_64-linux-gnu/libMagick++-6.Q16.so /usr/lib/x86_64-linux-gnu/libMagick++.so
#         fi
#         if [ ! -e /usr/lib/x86_64-linux-gnu/libMagickCore.so ]; then
           #  ln -sf /usr/lib/x86_64-linux-gnu/libMagickCore-6.Q16.so /usr/lib/x86_64-linux-gnu/libMagickCore.so
#         fi
#         apt-mark hold gsoap libgsoap-dev
#         # Install openscenegraph
#         sudo apt-get install -y libgdal-dev
#         sudo echo "yy" | sudo gdebi openscenegraph_3.4_16.04_amd64.deb
#         sudo dpkg -i openscenegraph_3.4_16.04_amd64.deb
#     ;;
#     "x18.04")
# 		# prepare imagick
#         if [ ! -e /usr/include/ImageMagick ]; then
             sudo ln -sf /usr/include/ImageMagick-6 /usr/include/ImageMagick
#         fi
#         if [ ! -e /usr/include/ImageMagick-6/magick/magick-baseconfig.h ]; then
             sudo ln -sf /usr/include/x86_64-linux-gnu/ImageMagick-6/magick/magick-baseconfig.h /usr/include/ImageMagick-6/magick/magick-baseconfig.h
#         fi
#         if [ ! -e /usr/lib/x86_64-linux-gnu/libMagick++.so ]; then
             sudo ln -sf /usr/lib/x86_64-linux-gnu/libMagick++-6.Q16.so /usr/lib/x86_64-linux-gnu/libMagick++.so
#         fi
#         if [ ! -e /usr/lib/x86_64-linux-gnu/libMagickCore.so ]; then
             sudo ln -sf /usr/lib/x86_64-linux-gnu/libMagickCore-6.Q16.so /usr/lib/x86_64-linux-gnu/libMagickCore.so
#         fi
#         # install gsoap
# 		sudo apt-get purge -y gsoap libgsoap-dev
#         if [ ! -f ./libgsoap4_2.8.16-2_amd64.deb ]; then
#             sudo wget https://launchpad.net/ubuntu/+source/gsoap/2.8.16-2/+build/5208555/+files/libgsoap4_2.8.16-2_amd64.deb -O libgsoap4_2.8.16-2_amd64.deb
#             sudo dpkg -i ./libgsoap4_2.8.16-2_amd64.deb
#         else
#             sudo dpkg -i ./libgsoap4_2.8.16-2_amd64.deb
#         fi
#         if [ ! -f ./libgsoap-dev_2.8.16-2_amd64.deb ]; then
#             sudo wget https://launchpad.net/ubuntu/+source/gsoap/2.8.16-2/+build/5208555/+files/libgsoap-dev_2.8.16-2_amd64.deb -O libgsoap-dev_2.8.16-2_amd64.deb
#             sudo dpkg -i ./libgsoap-dev_2.8.16-2_amd64.deb
#         else
#             sudo dpkg -i ./libgsoap-dev_2.8.16-2_amd64.deb
#         fi
#         if [ ! -f ./gsoap_2.8.16-2_amd64.deb ]; then
#             sudo wget https://launchpad.net/ubuntu/+source/gsoap/2.8.16-2/+build/5208555/+files/gsoap_2.8.16-2_amd64.deb -O gsoap_2.8.16-2_amd64.deb
#             sudo dpkg -i ./gsoap_2.8.16-2_amd64.deb
#         else
#             sudo dpkg -i ./gsoap_2.8.16-2_amd64.deb
#         fi
# 		# prepare other libs
# 		sudo apt-get install -y libopenscenegraph-dev 
# 		sudo add-apt-repository "deb http://security.ubuntu.com/ubuntu xenial-security main"
# 		sudo add-apt-repository "deb http://us.archive.ubuntu.com/ubuntu/ xenial main universe"
# 		frepoupdate
# 		sudo apt-get install -y libjasper1 libjasper-dev
# 		sudo apt-get remove -y libopenthreads-dev
#     # Install openscenegraph
#     sudo apt-get install -y  --allow-downgrades libgdal1-dev
# 		sudo ln -s /usr/lib/libgdal.so /usr/lib/libgdal.so.1-1.11.3 || true
# 		sudo ldconfig
#     sudo echo "yy" | sudo gdebi openscenegraph_3.4_16.04_amd64.deb
#     sudo dpkg -i openscenegraph_3.4_16.04_amd64.deb
# 		sudo rm -Rf /opt/lib64copy || true
# 		sudo cp -r /usr/lib64 /opt/lib64copy
# 	;;
# esac

# echo "/usr/lib64" | sudo tee /tmp/osgd.conf
# echo "/usr/local/lib64" | sudo tee /tmp/osgd.conf
# sudo cp -f /tmp/osgd.conf /etc/ld.so.conf.d/osgd.conf

# sudo ldconfig
# make clean || true
# make -j4 && echo renderer make OK

# sudo dpkg -r openscenegraph
# sudo apt-get install -y libcurl4 curl php$phpverdev-curl
# sudo mkdir -p /usr/lib64
# # sudo cp -r /opt/lib64copy/* /usr/lib64/
# sudo ldconfig
finstall libb64-dev # Этот пакет ставится только один
finstall "libmagick++-dev libmagick++-6.q16-dev libmagickcore-6.q16-dev libmagickwand-6.q16-dev libmagickwand-dev" # Imagemagick
finstall "libopenscenegraph-dev libopenscenegraph160" # OSG
finstall "libevent-dev librsvg2-dev libcairo2-dev libgdk-pixbuf2.0-dev libglib2.0-dev libselinux1-dev libpcre2-dev libpcre2-8-0" # Additional
sudo make clean 1>/dev/null && infoOk "make clean renderer2"
sudo make -j8 1>/dev/null && infoOk "make renderer2" || error "make renderer2 broken"
./dist/Debug/GNU-Linux/renderer2 && infoOk "renderer2 binary working" || error "renderer2 binary not work"
# ---------------- renderer done?

# set mysql timezone
mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root -pmypass mysql

# mysqld_safe stop
#ps -ax |grep mysqld_safe | grep -v 'grep' | awk '{print $1}' | xargs kill -9 || true

function fAddDums {
# Добавляем на дев машины скрипты в директорию пользователя.
# Add scripts to home dir vagrant
sudo mkdir -p $HOME/dumps
sudo chmod 777 $HOME/dumps
echo -e '#!/bin/bash'"\n
cd $HOME/dumps
apiToken=118bb38853f921d74759dd71e48ca5d0ca
function downloadErr { echo \"Error, download file\"; exit 2;}
diskroot=\$(mount | grep '^/dev' | awk '{print \$1}')
disksize=\$(df | grep \"\$diskroot\" | awk '{print \$4}')
if [ \"\$disksize\" -le \"15000000\" ]; then
    echo \"No Disk space. Minimum free size 15 Gb.\"
    echo \"Current free disk space:\" \$(df -h | grep \"\$diskroot\" | awk '{print \$4}')
    exit 1
fi
filenamedump=\"\$(date +%Y_%m_%d_00_20)_dbtreatstocko.sql.gz\"
curl -X POST -L --user dev:\$apiToken http://jenkins.treatstock.com/job/Obfuscator/ws/\$filenamedump --output \$filenamedump && echo \"download successeful\" || downloadErr
$HOME/dumps/resetDb.sh \$filenamedump\n">$HOME/dumps/getodb.sh
echo '#!/bin/bash'"
export MYSQL_PWD=$mysqlpasswd
if [ \$1 = \"\" ]; then 
    filename=dump.sql.gz
    else
    filename=\$1
fi
date
echo \"ResetDb\"
$sdir/tools/maintenance/maintenance.sh true
mysql -u root -f \"yii2advanced\" < $HOME/dumps/resetDb.sql
pv $HOME/dumps/\$filename | gunzip | mysql -u root \"yii2advanced\"
php $sdir/yii migrate --interactive=0
php $sdir/yii generator/models
$sdir/tools/maintenance/maintenance.sh false">$HOME/dumps/resetDb.sh
    echo 'DROP DATABASE yii2advanced;
CREATE DATABASE `yii2advanced` CHARACTER SET utf8 COLLATE utf8_general_ci;' >$HOME/dumps/resetDb.sql
    sudo chown -R $me:$me $HOME/dumps
    sudo chmod 755 $HOME/dumps/*.sh
}

#### Logrotate
info "configure logrotate.d"
# refs #8450
echo "$sdir/tools/node/app/log/*.log {
    daily
    missingok
    rotate 2
    create 0644 www-data www-data
}
" | sudo tee /etc/logrotate.d/nodeapplog
echo "/var/log/sphinxsearch/*.log {
    daily
    missingok
    rotate 2
    create 0600 sphinxsearch sphinxsearch
}
" | sudo tee /etc/logrotate.d/sphinxsearch
echo "/var/log/nginx/*.log
/var/log/nginx/static/*.log
/var/log/nginx/api/*.log
/var/log/nginx/sentry/*.log 
{
        daily
        missingok
        rotate 14
        compress
        delaycompress
        notifempty
        create 0640 www-data adm
        sharedscripts
        prerotate
                if [ -d /etc/logrotate.d/httpd-prerotate ]; then
                        run-parts /etc/logrotate.d/httpd-prerotate;
                fi
        endscript
        postrotate
                invoke-rc.d nginx rotate >/dev/null 2>&1
        endscript
}
/var/log/nginx-im/*.log {
        daily
        missingok
        rotate 14
        compress
        delaycompress
        notifempty
        create 0640 www-data adm
        sharedscripts
        prerotate
                if [ -d /etc/logrotate.d/httpd-prerotate ]; then
                        run-parts /etc/logrotate.d/httpd-prerotate;
                fi
        endscript
        postrotate
                invoke-rc.d nginx-im rotate >/dev/null 2>&1
        endscript
}" | sudo tee /etc/logrotate.d/nginx
echo "/var/www/treatstock/frontend/runtime/logs/*.log
/var/www/treatstock/common/runtime/logs/*.log
/var/www/treatstock/console/runtime/*.log
{
       weekly
       rotate 52
       delaycompress
       compress
       notifempty
       missingok
}" | sudo tee /etc/logrotate.d/treatstock
####

# mysqld_safe stop. After this operation dont add!!!
#ps -ax |grep mysqld_safe | grep -v 'grep' | awk '{print $1}' | xargs kill -9 || true

if [ $environment = "dev" ];then
    fAddDums
    fAddCron
    fAccess
fi

if [ ! -f /usr/local/sbin/ggit ];then 
    sudo ln -s $sdir/installer/conf/ggit.sh /usr/local/sbin/ggit
fi

info "This is END of script $BASH_SOURCE"