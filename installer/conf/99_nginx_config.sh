#!/bin/bash
source "$(dirname "$0")/config.sh"
if $debug;then set -xe;fi

cd $sdir
# config nginx and sites
sudo rm -f /etc/nginx/sites-enabled/*
sudo rm -f /etc/nginx/sites-available/*
sudo rm -f /etc/nginx/smodule/*
cp -rfT $sdir/installer/conf/etc/nginx /etc/nginx
sudo wget -q http://clamav.tsdev.work/todownload/1_linlet.net.pem -O /etc/nginx/ssl/1_linlet.net.pem
sudo wget -q http://clamav.tsdev.work/todownload/2_linlet.net.key -O /etc/nginx/ssl/2_linlet.net.key
sudo wget -q http://clamav.tsdev.work/todownload/1_control.h1.tsdev.work.pem -O /etc/nginx/ssl/1_control.h1.tsdev.work.pem
sudo wget -q http://clamav.tsdev.work/todownload/2_control.h1.tsdev.work.key -O /etc/nginx/ssl/2_control.h1.tsdev.work.key

if [ -d /var/www/payouts ]; then
    sudo umount /var/www/payouts || true
    sudo rm -rf /var/www/payouts
fi
if [ -d /vagrant/payouts ]; then
    ln -s /vagrant/payouts /var/www/payouts
else
    sudo mkdir -p /var/www/payouts
    sudo chmod 777 /var/www/payouts
fi
cp -rfT $sdir/installer/conf/etc/nginx-im /etc/nginx-im
for i in `find $sdir/installer/conf/etc/nginx/sites-available/ -maxdepth 1 -type f`; do
echo $i
fn=$(basename $i)
if [ $fn == 'payouts' ]; then
    sdirp=${sdir%/*}
    sudo awk '{gsub("root ","root '$sdirp'/"); print $0}' $i > /tmp/$fn
	sudo mv /tmp/$fn /etc/nginx/sites-available/$fn
else
    sudo awk '{gsub("root ","root '$sdir'/"); print $0}' $i > /tmp/$fn
	sudo mv /tmp/$fn /etc/nginx/sites-available/$fn
fi
if [ $fn == 'frontend' ]; then
   sudo ln -s /etc/nginx/sites-available/$fn /etc/nginx/sites-enabled/x.$fn
else
   sudo ln -s /etc/nginx/sites-available/$fn /etc/nginx/sites-enabled/$fn
fi
done
sudo systemctl restart nginx
echo "Restart nginx status: $?"
sudo systemctl restart nginx-im
echo "Restart nginx-im status: $?"