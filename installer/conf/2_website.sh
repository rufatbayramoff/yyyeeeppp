#!/bin/bash
# parameters
source "$(dirname "$0")/config.sh"
source "$(dirname "$0")/check_env.sh"
if $debug;then set -xe;fi

info "This is script $BASH_SOURCE"

fFileACL && infoOk "fFileACL: successeful" || error "fFileACL: fail"

info "\n------------------------------------------------------------
-    Starting WebSite Setting ts.h.tsdev.work              -
------------------------------------------------------------"

# set up sphinx search
# В строке 135 1_environment.sh сказано что не требуется
sphinxInit=/etc/init.d/sphinxsearch
sudo service sphinxsearch stop || true
sudo cp -f $sdir/installer/conf/sphinxsearch /etc/default/sphinxsearch
sudo cp -f $sdir/installer/conf/sphinx.conf /etc/sphinxsearch/sphinx.conf
# sudo mkdir -p /etc/sphinxsearch && sudo touch /etc/sphinxsearch/sphinx.conf # for resolve error: php yii indexer/run FATAL: config file '/etc/sphinxsearch/sphinx.conf' does not exist or is not readable
sudo chmod 777 /var/lib/sphinxsearch/data/
sudo chown -R sphinxsearch:sphinxsearch /etc/sphinxsearch/
if [ `grep "chmod 644" /etc/init.d/sphinxsearch 2>/dev/null | wc -l` = 0  ]; then
    cp /etc/init.d/sphinxsearch /tmp/sphinxsearch
    sed -i "/start-stop-daemon --start/a \        chmod 644 \$PIDFILE" /tmp/sphinxsearch
    sudo mv /tmp/sphinxsearch /etc/init.d/sphinxsearch
    infoOk "modif sphinx init"
    fi
sudo service sphinxsearch start

# set up YII and vendor's libraries by composer
cd $sdir
if [ -f $config_dir/xdebug.flag ]; then
    sudo $sdir/installer/conf/onxdebug.sh $sdir $phpver $config_dir && infoOk "ok. onxdebug.sh" || error "error. onxdebug.sh"
fi
sudo chmod 0777 $sdir/frontend/config
sudo chmod 0777 $sdir/common/config/
sudo chmod 0777 $sdir/backend/config/
sudo chmod 0777 $sdir/console/config/
sudo chmod 0777 $sdir/frontend/web/
sudo chmod 0777 $sdir/backend/web/
sudo chmod 0777 $sdir/backend/web/assets
sudo chmod 0777 $sdir/console/runtime/
sudo chmod 0777 $sdir/backend/runtime/
sudo chmod 0777 $sdir/frontend/runtime/
sudo mkdir -p $sdir/frontend/runtime/arCacheDependency && sudo chmod 0777 $sdir/frontend/runtime/arCacheDependency
sudo chmod 0777 $sdir/tools/node/app/log
if [ $environment = "dev" ];then
	info "install Development enveronment"
	php init --env=Development --overwrite=y && infoOk "development environment installed" 
fi
#sudo cp /var/www/treatstock/installer/conf/github-oauth.txt /vagrant/conf/github-oauth.txt
sudo dos2unix $sdir/installer/conf/github-oauth.txt
#php composer.phar self-update
#php composer.phar update

#sudo rm -rf console/runtime/*
#sudo rm -rf backend/runtime/*
#sudo rm -rf frontend/runtime/*
#sudo chown -R www-data:www-data console/runtime backend/runtime frontend/runtime

if [ ! -d $HOME/.config ]
    then mkdir $HOME/.config && chown $me:$me -R $HOME/.config && infoOk ".config created"
fi

php composer.phar config --global github-oauth.github.com `cat $sdir/installer/conf/github-oauth.txt`

#php composer.phar global require "fxp/composer-asset-plugin:1.*"
# php composer.phar --no-plugins install
sudo chown -R $me vendor || true
info "php composer install"
php composer.phar install && infoOk "composer installation complete" || error "composer installation failure"

# web site set up ts.vcap.me
function fmodelgen {
    info "$1 model generator (slow)"
    php yii generator/models 1>/dev/null && infoOk "$1 model generator compleate" || error "$1 model generator error"
}

sudo chmod 0777 /var/www/treatstock/common/models/base/
#sudo chown -R $me:$me console/runtime backend/runtime frontend/runtime
fmodelgen First
function fmigration {
  execute=$(php yii migrate --interactive=0 2>&1) && infoOk "YII Migrade" || error "YII Migrate problems"
  if [ `echo $execute | grep "No new migrations found. Your system is up-to-date" | wc -l` = 0 ];then
   fmodelgen Second
  fi
}
fmigration
php yii generator/create-admin 123456
php yii payment/update-rates || true
sudo chown -R www-data:www-data console/runtime backend/runtime frontend/runtime
# Disabled
#php yii sypex-geo/update

# use php yii translate/scan-for-translations
# php yii message/extract common/config/i18n.php
mkdir -p $sdir/frontend/web/assets
sudo chmod -R 777 $sdir/frontend/web/assets
sudo chmod -R 777 $sdir/tools/node/www/cloudcam/services/data || true
if [ -f "/vagrant/conf/assetson.flag" ]; then
    bash $sdir/installer/conf/onassets.sh && infoOk "ok. onassets.sh" || error "error. onassets.sh"
else
    bash $sdir/installer/conf/offassets.sh && infoOk "ok. offassets.sh" || error "error. offassets.sh"
fi
php yii indexer/run && infoOk "YII Indexer run comleate" || error "YII Indexer failure"
php yii translate/words-count-create || true


cd tools/node
sudo npm i --no-bin-links && infoOk "npn install compleate" || error "npn install failure"
cd ../..

sudo systemctl start supervisor
# Не нужно, так как выполняется во время старта супервизора
# php yii filepage-maps/printers-tree-update || true

# web site payouts.vcap.me
if [ -d $sdir/../payouts ]; then
    info "payouts installation"
    echo ""
    echo "------------------------------------------------------------"
    echo "-    Starting WebSite Setting payouts.h.tsdev.work         -"
    echo "------------------------------------------------------------"
    cd $sdir/../payouts
    status=$(git status 2>&1 | grep origin |awk '{print $1}')
    if [ "-$status" == "-Your" ]; then
        #cp -f $sdir/composer.phar $sdir/../payouts/
        #php composer.phar self-update
        #php composer.phar update
        if [ -f "/vagrant/conf/github-oauth.txt" ]; then
            php composer.phar config --global github-oauth.github.com `cat /vagrant/conf/github-oauth.txt`
        else
            php composer.phar config --global github-oauth.github.com `cat /vagrant/repo/installer/conf/github-oauth.txt`
        fi
        #php composer.phar global require "fxp/composer-asset-plugin:1.*"
        php composer.phar --no-plugins install
        sudo cp -f $sdir/installer/conf/pay-params-local.php $sdir/../payouts/config/params-local.php
        sudo cp -f $sdir/installer/conf/pay-db-local.php $sdir/../payouts/config/db-local.php
        php yii migrate --interactive=0
        fmodelgen "Three"
        php yii generator/create-admin admin 123456
    fi
    echo "------------------------------------------------------------"
    echo "-                           Done                           -"
    echo "------------------------------------------------------------"
fi

# CRON
info "cron task set up"
if [ `sudo grep cron.php /var/spool/cron/crontabs/www-data 2>/dev/null | wc -l` = 0 ]; then
    echo "* * * * * /usr/bin/php $sdir/yii schedule/run --scheduleFile=@console/cron.php 1>> /dev/null 2>&1" | sudo crontab -u www-data -
fi
# redis task processor
#sudo update-rc.d -f -n resque remove || true
#sudo rm -f /etc/init.d/resque || true

# добавление в крон скрипта проверки sphinx
if [ `sudo grep testservice.sh /var/spool/cron/crontabs/root 2>/dev/null | wc -l` = 0 ]; then
    echo "* * * * * $sdir/installer/conf/testservice.sh > /dev/null 2>&1" | sudo tee -a /var/spool/cron/crontabs/root
fi
#sudo rm -f /opt/backgroundrender.sh || true
#sudo ps -ax | grep [b]ackgroundrender.sh | awk '{print $1}' | sudo xargs kill -9 || true
sudo service cron restart
sudo crontab -l -u www-data || true

# docs
#cd $sdir
#sudo php doc/apigen.phar generate --source "./frontend,./backend,./common,./console,./lib" --destination ./doc/api

# db prepare for testing
cd $sdir
if [ -f "$sdir/tests/_data/dump-test.sql" ]; then
    unlink $sdir/tests/_data/dump-test.sql
fi

sudo chmod 0777 $sdir/tests/_data/
wget -q http://clamav.tsdev.work/todownload/dump-test.sql -O $sdir/tests/_data/dump-test.sql 1>/dev/null && infoOk "download dump-test.sql" || error infoOk "download dump-test.sql"

info "This is END of script $BASH_SOURCE"