#!/bin/bash
backupdb()
{
    MYSQL_PWD=mypass mysqldump -u root yii2advanced | gzip > /vagrant/conf/yii2advanced_dev.sql.gz
    MYSQL_PWD=mypass mysqldump -u root yii2advanced_test | gzip > /vagrant/conf/yii2advanced_test_dev.sql.gz
    MYSQL_PWD=mypass mysqldump -u root ts_payout | gzip > /vagrant/conf/ts_payout_dev.sql.gz
}
cd /vagrant/repo
dbbdir=/vagrant/repo/tests/_debug
if [ ! -f /vagrant/conf/yii2advanced_dev.sql.gz -o ! -f /vagrant/conf/yii2advanced_test_dev.sql.gz -o ! -f /vagrant/conf/ts_payout_dev.sql.gz ]; then
	echo "Сохраняем базы..." | tr -d '\n'
	backupdb
	echo "Готово"
else
	echo "Вы уверены что хотите перезаписать старые архивы базы данных?" | tr -d '\n'
	keypress="n"
	read -n 1 keypress
	echo
	if [ $keypress == "y" -o $keypress == "Y" ]; then
	echo "Сохраняем базы..." | tr -d '\n'
	backupdb
	echo "Готово"
	fi
fi
files_sql=$(find $dbbdir -type f -name *.sql -exec basename {} \; | sort)
files_sql_gz=$(find $dbbdir -type f -name *.sql.gz -exec basename {} \; | sort)
files="$files_sql $files_sql_gz"
count=1
for i in $files; do
    echo $count - $i
    backups[$count]=$i
    ((count+=1))
done

while [[ ! "$keypress" =~ ^[0-9][0-9]?[0-9]?$ ]]; do
echo "Выберите номер архивной копии для восстановления: " | tr -d '\n'
    read -n 3 keypress
if [ "$keypress" -lt "1" ]; then
    keypress="1000"
fi
if [ "$keypress" -gt "$count" ]; then
    echo
    echo "Выбрано несуществующее значение. "
    keypress="d"
fi
done
if [ "${backups[$keypress]:(-3)}" == ".gz" ]; then
    backupfile=${backups[$keypress]:0:(${#backups[$keypress]}-3)}
    gunzip -k -f $dbbdir/${backups[$keypress]}
else
    backupfile=${backups[$keypress]}
fi
export MYSQL_PWD=mypass
flush=`echo "SET FOREIGN_KEY_CHECKS=0;
SELECT Concat('DROP TABLE ',table_schema,'.',TABLE_NAME, ';')
FROM INFORMATION_SCHEMA.TABLES where (table_schema in ('yii2advanced') and table_type!='view')
UNION
SELECT Concat('DROP VIEW ',table_schema,'.',TABLE_NAME, ';')
FROM INFORMATION_SCHEMA.TABLES where (table_schema in ('yii2advanced') and table_type='view');
SET FOREIGN_KEY_CHECKS=0;" | mysql -s -B -u root yii2advanced`
echo "SET FOREIGN_KEY_CHECKS=0; $flush SET FOREIGN_KEY_CHECKS=1;" | mysql -s -B -u root yii2advanced
echo "База очищена. Код возврата Mysql: "$?
mysql -u root yii2advanced < $dbbdir/$backupfile
echo "База восстановлена из архива: "$dbbdir/$backupfile" с кодом возврата: "$?
rm -f $dbbdir/$backupfile
