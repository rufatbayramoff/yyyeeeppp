#!/bin/bash
# environment $sdir - Каталог установки сайта.
export sdir=/vagrant/repo

cd $sdir

#set ip selenium server
ip=$(echo $SSH_CLIENT | awk '{print $1}')
grep 'selenium.local' /etc/hosts 2>&1 >/dev/null
if [ $? -ne 0 ]; then
    echo -e "\n$ip selenium.local" | sudo tee -a /etc/hosts
fi

echo -e "[client]\nuser=root\npassword=mypass\n">~/.my.cnf
echo "Clean database..." | tr -d '\n'

# prepare clean DB from migrations and save it as SQL dump
echo "drop database yii2advanced;" | mysql
echo "create database yii2advanced CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql
echo "grant all privileges on yii2advanced.* to root@'%' identified by 'mypass'; flush privileges;" | mysql
echo "grant all privileges on yii2advanced.* to root@localhost identified by 'mypass'; flush privileges;" | mysql
echo "Done"

echo "Clean Redis..." | tr -d '\n'
redis-cli flushall

# fill it with a data created in repo-UP script
echo "Restore database from dump created in repo-up script..." | tr -d '\n'
mysql yii2advanced < tests/_data/dump-test.sql
echo "Done"

echo "Start migrations..." | tr -d '\n'
php yii migrate --interactive=0 2>&1 >/vagrant/lastmigrations.log
if [ "x$?" == "x1" ]; then
    echo -e '\n'
    cat /vagrant/lastmigrations.log
    echo "Error Migrations test stopped!"
    exit
fi
echo "Migrations Done."

# prepare and run tests
echo "Restart supervisor..." | tr -d '\n'
sudo systemctl -q restart supervisor
if [ "x$?" == "x1" ]; then
    echo "Error restart supervisor. Restart again..." | tr -d '\n'
    sleep 1
    sudo systemctl restart supervisor
fi
if [ "x$?" == "x0" ]; then
    echo "Supervisor restarted."
else
    echo "Error restart supervisor."
    exit
fi
echo "Start script yii payment/update-rates..." | tr -d '\n'
php yii payment/update-rates
sleep 1

vendor/codeception/codeception/codecept build -c codeception-acceptance.yml
vendor/codeception/codeception/codecept run --tap="acceptancelog.log" --html="acceptancelog.html" -d -c codeception-acceptance.yml acceptance
vendor/codeception/codeception/codecept build -c codeception-unit.yml
sudo sudo -u www-data vendor/codeception/codeception/codecept run --tap="unitlog.log" --html="unitlog.html" -c codeception-unit.yml unit

