#!/bin/bash
source "$(dirname "$0")/config.sh"
if $debug;then set -xe;fi

cd $sdir
##set ip selenium server
#ip=$(echo $SSH_CLIENT | awk '{print $1}')
#grep 'selenium.local' /etc/hosts 2>&1 >/dev/null
#if [ $? -ne 0 ]; then
#    echo -e "\n$ip selenium.local" | sudo tee -a /etc/hosts
#fi

# clean done tests
echo "Clean up done mark ..." | tr -d '\n'
sudo rm -Rf $sdir/tests/_doneMark/*
echo "Done"

# clean up files
info "В третий раз чистим файлы 8-D"
function cleanDir3 {
    sudo rm -Rf $sdir/frontend/web/static/models/*
    sudo rm -Rf $sdir/frontend/web/static/user/*
    sudo rm -Rf $sdir/frontend/web/static/ps/*
    sudo rm -Rf $sdir/doc/api
    sudo rm -Rf $sdir/frontend/storage/*
    sudo rm -Rf $sdir/tests/_output_acceptance/*
    sudo mkdir -p $sdir/tests/_output_acceptance/debug
    sudo chmod -R 777 $sdir/tests/_output_acceptance
    sudo rm -Rf $sdir/tests/_output_unit/*
    sudo rm -Rf $sdir/tests/_debug/*.sql
}
cleanDir3 && infoOk "function cleanDir3 - Done" || error "function cleanDir3 fail"