#!/bin/bash
# environment $sdir - Каталог установки сайта.
export sdir=/vagrant/repo

cd $sdir
#set ip selenium server
if [ -d "/tovagrant" ]; then
    echo "Jenkins"
    php init --env=Jenkins --overwrite=y
else
    ip=$(echo $SSH_CLIENT | awk '{print $1}')
    grep 'selenium.local' /etc/hosts 2>&1 >/dev/null
    if [ $? -ne 0 ]; then
        echo -e "\n$ip selenium.local" | sudo tee -a /etc/hosts
    fi
fi
echo -e "[client]\nuser=root\npassword=mypass\n">~/.my.cnf
echo "Clean database..." | tr -d '\n'
# prepare clean DB from migrations and save it as SQL dump
echo "drop database yii2advanced;" | mysql
echo "create database yii2advanced CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql
echo "grant all privileges on yii2advanced.* to root@'%' identified by 'mypass'; flush privileges;" | mysql
echo "grant all privileges on yii2advanced.* to root@localhost identified by 'mypass'; flush privileges;" | mysql
echo "Done."
echo "Clear redis..." |tr -d '\n'
redis-cli flushall

# fill it with a data created in repo-UP script
echo "Restore database from initial dump..." |tr -d '\n'
mysql yii2advanced < tests/_data/dump-test.sql
echo "Done"
rm -rf ~/.my.cnf

echo "Start migrations..." | tr -d '\n'
php yii migrate --interactive=0 2>&1 >/vagrant/lastmigrations.log
if [ "x$?" == "x1" ]; then
    echo -e '\n'
    cat /vagrant/lastmigrations.log
    echo "Error Migrations test stopped!"
    exit 1
fi
echo "Migrations Done."

# clean up files
$sdir/installer/conf/9_test_clean.sh
sudo chmod -R 777 $sdir/tests/_debug

# prepare and run tests
echo "Restart supervisor..." | tr -d '\n'
sudo systemctl -q restart supervisor
if [ "x$?" == "x1" ]; then
    echo "Restart supervisor Error. Restart again..." | tr -d '\n'
    sleep 1
    sudo systemctl restart supervisor
fi
if [ "x$?" == "x0" ]; then
    echo "Supervisor restarted."
else
    echo "Error restart supervisor."
    exit 1
fi

echo "start script yii payment/update-rates..." | tr -d '\n'
php yii payment/update-rates
# Disabled
#php yii sypex-geo/update

sleep 1
errortests=0

vendor/codeception/codeception/codecept build -c codeception-acceptance.yml
vendor/codeception/codeception/codecept run --tap="acceptancelog.log" --html="acceptancelog.html" -c codeception-acceptance.yml acceptance
if [ "x$?" != "x0" ]; then
    errortests=1
    if [ -d "/tovagrant" ]; then
        rm -rf /tovagrant/dumps
        mkdir -p /tovagrant/dumps
        mysqldump -uroot -pmypass yii2advanced | gzip >/tovagrant/dumps/jtest_`date +\%Y\%m\%d-\%H\%M_%z.sql.gz`
    fi
fi
mkdir -p tests/_output_unit
sudo chmod 777 tests/_output_unit
sudo chmod -R 777 /vagrant/repo/console/runtime
vendor/codeception/codeception/codecept build -c codeception-unit.yml
sudo sudo -u www-data vendor/codeception/codeception/codecept run --tap="unitlog.log" --html="unitlog.html" -c codeception-unit.yml unit
if [ "x$?" != "x0" ]; then
    errortests=1
fi
if [ -d "/tovagrant" ]; then
    cd /tovagrant
    rm -rf acceptance
    rm -rf unit
    cd ..
    mkdir -p /tovagrant/acceptance
    mkdir -p /tovagrant/unit
    cp -rf /vagrant/repo/tests/_output_acceptance/* /tovagrant/acceptance/
    cp -rf /vagrant/repo/tests/_output_unit/* /tovagrant/unit/
    cp -f /vagrant/repo/tests/_debug/*.sql /tovagrant/dumps/
    gzip /tovagrant/dumps/*.sql
    tar cvJpf /tovagrant/dumps/files_`date +\%Y\%m\%d-\%H\%M_%z`.tar.xz /vagrant/repo/frontend/web/static/files/ /vagrant/repo/frontend/web/static/render/ /vagrant/repo/frontend/web/static/user/ /vagrant/repo/frontend/storage/
    cd /tovagrant/dumps
    echo -e "<html>\n<body>" >index.html
    for f in $(ls); do
        echo "<a href=\"$f\">$f</a><br>" >>index.html
    done
    echo -e "</body>\n</html>" >>index.html
fi
exit $errortests
