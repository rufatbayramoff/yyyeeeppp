#!/bin/bash
# Проверка работы сфинкса
# Запускается по заданию из крона под рутом. Задание добавляется в выполнение в скрипте 2_website.sh в секции cron
sphinx=$(ps -aux | grep [s]earchd | wc -l)
if [ ! $sphinx -eq 2 ]; then
    echo `date +\%d.\%m.\%Y-\%H:\%M` | tr '\n' ' ...'
    echo "Перезапуск Sphinx" | tr '\n' ' '
    sudo service sphinxsearch stop >/dev/null 2>&1
    sudo service sphinxsearch start >/dev/null 2>&1
    if [ ! $? -eq 0 ]; then
        echo "Ошибка перезапуска sphinx"
    fi
    echo "ok"
fi