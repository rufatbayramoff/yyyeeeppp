#!/bin/bash
sdir=/vagrant/repo
bdir=/vagrant/conf
if [ -d "/tovagrant" ]; then
    bdir=/tovagrant/backup
fi
mkdir -p $bdir
if [ -f "/vagrant/conf/hostid.php" ]; then
    mnum=h`cat /vagrant/conf/hostid.php | grep -Eo '[0-9]'`
else
    mnum=h
fi
sshpass -prtg432 ssh -p 60425 backupstate@10.102.0.92 "mkdir -p /backup/backupstate/$mnum/" -vvv
sshpass -prtg432 scp -o StrictHostKeyChecking=no -P 60425 backupstate@10.102.0.92:/backup/backupstate/$mnum/* $bdir/
for dbfile in $bdir/*.sql.gz; do
db=$(basename $dbfile .sql.gz)
echo "drop database $db;" | mysql -u root --password=mypass
echo "create database $db CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql -u root --password=mypass
echo "grant all privileges on $db.* to root@'%' identified by 'mypass'; flush privileges;" | mysql -u root --password=mypass
zcat $dbfile | mysql -u root --password=mypass
echo "---------------------------------------------------------------------------------------------"
echo "- Restore database $db done."
echo "---------------------------------------------------------------------------------------------"
done
tar -C / -xvf $bdir/files.tar.xz
echo "---------------------------------------------------------------------------------------------"
echo "- Restore files done"
echo "---------------------------------------------------------------------------------------------"