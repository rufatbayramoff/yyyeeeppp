#!/bin/bash
# environment $sdir - Каталог установки сайта.
cd /vagrant
if [ -d /vagrant/restoredb ]; then
    gunzip /vagrant/restoredb/*
    files=$(ls -Ct /vagrant/restoredb)
    file_=`echo $files | awk '{print $1}'`
    # Создание базы данных для сайта
    echo "drop database yii2advanced;" | mysql -u root --password=mypass
    echo "create database yii2advanced CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql -u root --password=mypass
    echo "grant all privileges on yii2advanced.* to root@'%' identified by 'mypass'; flush privileges;" | mysql -u root --password=mypass
    echo "grant all privileges on yii2advanced.* to root@localhost identified by 'mypass'; flush privileges;" | mysql -u root  --password=mypass
    # Восстановление из архива
    mysql --init-command="SET SESSION FOREIGN_KEY_CHECKS=0;" -uroot -pmypass yii2advanced </vagrant/restoredb/$file_
    rm -f /vagrant/restoredb/*
    sudo service php7.1-fpm restart
fi