#!/bin/bash
# environment $sdir - Каталог установки сайта.
if [ -f /vagrant/conf/pathtoinstall.env ]; then
i=$(cat /vagrant/conf/pathtoinstall.env)
$i
else
export sdir=/vagrant/repo
fi
sudo sed '/\.lua/s/^#//g' /etc/nginx/sites-available/frontend >/tmp/frontend.on
sudo mv -f /tmp/frontend.on /etc/nginx/sites-available/frontend
