<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=ts_payout',
    'schemaCacheDuration' => 0,
    'enableSchemaCache' => false,
    'username' => 'root',
    'password' => 'mypass',
    'charset' => 'utf8',
];