#!/bin/bash
sdir=$1
export mdir=${sdir}
#sudo systemctl stop supervisor
cd $sdir/tools/node
sudo service nginx reload
mkdir -p data
cd data
wget -q http://clamav.tsdev.work/todownload/opencascade_7.1.0-1_14.04_amd64.deb -O $sdir/tools/node/data/opencascade_7.1.0-1_14.04_amd64.deb
gdebi -n $sdir/tools/node/data/opencascade_7.1.0-1_14.04_amd64.deb
cd ..
sudo apt-get install -y nodejs npm sshpass node-gyp libnode-dev 1>/dev/null
sudo npm i -g n
sudo n stable -g
sudo n prune -g
sudo npm i -g npm
sudo npm -v
sudo npm install node-gyp -g
sudo npm install --no-bin-links
sudo rm -f $sdir/tools/node/app/config.json
echo "{
    \"threads\": 2,
    \"server_port\": \"5858\",
    \"server_host\": \"0.0.0.0\",
    \"file_prefix\": \"data/\",
    \"url_prefix\": \"http://0.0.0.0:5858/data/\",
    \"models_root\": \"$mdir/frontend/\",
    \"tmp_root\": \"/tmp/\",
    \"servicePrefix\": \"www/cloudcam/services/\",
    \"mysql_host\": \"localhost\",
    \"mysql_port\": \"3306\",
    \"mysql_user\": \"watermark\",
    \"mysql_database\": \"watermark\",
    \"mysql_password\": \"watermark\",
    \"daily_limit\": 100,
    \"publicmark_url\": \"http://watermark.vcap.me\",
    \"mail_host\": \"localhost\",
    \"mail_port\": \"1025\"
}" >$sdir/tools/node/app/config.json
echo "[program:nodejs]
numprocs=1
numprocs_start=1
startsecs=3
startretries=100
autostart=true
autorestart=true

redirect_stderr=true
stdout_logfile=/var/log/node_js.log

user=www-data
group=www-data

directory=$sdir/tools/node
command=/usr/local/bin/node --expose_gc --max-old-space-size=8192 index.js" | sudo tee /etc/supervisor/conf.d/nodejs.conf 1>/dev/null
chmod 0777 $sdir/tools/node/data