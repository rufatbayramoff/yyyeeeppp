#!/bin/bash
# environment $sdir - Каталог установки сайта.
sdir=$1
phpver=$2
config_dir=$3
sudo rm -rf /etc/php/$phpver/cli-noxdebug
sudo rm -rf /etc/php/$phpver/cli-xdebug
sudo cp -rf /etc/php/$phpver/cli /etc/php/$phpver/cli-xdebug
# config php-xdebug
sudo apt-get -y install php-xdebug
sudo cp -f $sdir/installer/conf/xdebug.ini /etc/php/$phpver/mods-available/xdebug.ini
sudo chown root:root /etc/php/$phpver/mods-available/xdebug.ini
sudo chmod 644 /etc/php/$phpver/mods-available/xdebug.ini

# Enable xdebug for special cli
rm -f /etc/php/$phpver/cli-xdebug/conf.d/20-xdebug.ini
sudo ln -s /etc/php/$phpver/mods-available/xdebug.ini /etc/php/$phpver/cli-xdebug/conf.d/20-xdebug.ini

# Disable xdebug from default cli
sudo rm -f /etc/php/$phpver/cli/conf.d/20-xdebug.ini
sudo service php$phpver-fpm restart

# Add to .bashrc environment variables for xdebug
for user in `find /home -maxdepth 1 -type d`; do
    case $user in
    '/home')
    ;;
    *)
        if [ -f "$user/.bashrc" ]; then
            realuser=`cat /etc/passwd | grep $(basename $user) | awk -F":" '{print $1}'`
            if [ "x$realuser" == "x$(basename $user)" ]; then
                sudo grep -En 'PHP_INI_SCAN_DIR' $user/.bashrc >/dev/null
                if [ "x$?" != "x0" ]; then
                    strnum=$(sudo grep -En '^$' $user/.bashrc | head -1 | awk -F":" '{print $1}')
                    sudo awk '{print} NR=='$strnum' {print "# Remove symbol # below this string for enable CLI php-xdebug\n#export PHP_INI_SCAN_DIR=\"/etc/php/'$phpver'/cli-xdebug/conf.d\"\n"}' $user/.bashrc >/tmp/.bashrc.new
                    sudo mv -f $user/.bashrc $user/.bashrc.old
                    sudo mv -f /tmp/.bashrc.new $user/.bashrc
                    sudo chown $(basename $user):$(basename $user) $user/.bashrc
                    sudo chmod 644 $user/.bashrc
                fi
            fi
        fi
    ;;
    esac
done
touch $config_dir/xdebug.flag