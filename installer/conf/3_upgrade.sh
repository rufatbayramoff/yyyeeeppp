#!/bin/bash
exec 2>&1
# environment $sdir - Каталог установки сайта.
if [ -f /vagrant/conf/pathtoinstall.env ]; then
    i=$(cat /vagrant/conf/pathtoinstall.env)
    $i
else
    export sdir=/vagrant/repo
fi
cd $sdir

sudo systemctl stop supervisor
sudo service sphinxsearch stop
sudo cp -f $sdir/installer/conf/sphinx.conf /etc/sphinxsearch/sphinx.conf
sudo service sphinxsearch start
sudo chmod 0777 /var/www/treatstock/frontend/config
sudo chmod 0777 /var/www/treatstock/common/config/
sudo chmod 0777 /var/www/treatstock/backend/config/
sudo chmod 0777 /var/www/treatstock/console/config/
sudo chmod 0777 /var/www/treatstock/frontend/web/
sudo chmod 0777 /var/www/treatstock/backend/web/
php init --env=Development --overwrite=y
if [ -f "/vagrant/conf/github-oauth.txt" ]; then
    php composer.phar config --global github-oauth.github.com `cat /vagrant/conf/github-oauth.txt`
else
    php composer.phar config --global github-oauth.github.com `cat /vagrant/repo/installer/conf/github-oauth.txt`
fi
sudo php composer.phar global require "fxp/composer-asset-plugin:1.*"
sudo php composer.phar install -n
rm -rf $sdir/frontend/web/assets/*
sudo php yii migrate/up --interactive=0
sudo php yii base-models/generate
sudo php yii base-models/create-admin 123456
sudo php yii payment/update-rates

if [ -f "/vagrant/conf/assetson.flag" ]; then
    sudo bash /vagrant/repo/installer/conf/onassets.sh
else
    sudo bash /vagrant/repo/installer/conf/offassets.sh
fi
sudo php yii indexer/run
if [ -f "/vagrant/conf/translate.flag" ]; then
    sudo php yii translate/scan-for-translations
    sudo php yii translate/form-js
fi
if [ -d "$sdir/../payouts" ]; then
    cd $sdir/../payouts
    if [ -f "/vagrant/conf/github-oauth.txt" ]; then
        php composer.phar config --global github-oauth.github.com `cat /vagrant/conf/github-oauth.txt`
    else
        php composer.phar config --global github-oauth.github.com `cat /vagrant/repo/installer/conf/github-oauth.txt`
    fi
    sudo php composer.phar global require "fxp/composer-asset-plugin:1.*"
    sudo php composer.phar install -n
    sudo php yii migrate/up --interactive=0
    sudo php yii models/generate
fi
cd $sdir
sudo systemctl start supervisor
#Каталоги static и storage
mkdir -p /vagrant/repo/frontend/web/static/user
mkdir -p /vagrant/repo/frontend/web/static/files
mkdir -p /vagrant/repo/frontend/web/static/render
mkdir -p /vagrant/repo/frontend/storage
sudo chown -R www-data:www-data /vagrant/repo/frontend/storage
sudo chown -R www-data:www-data /vagrant/repo/frontend/web/static/user
sudo chown -R www-data:www-data /vagrant/repo/frontend/web/static/files
sudo chown -R www-data:www-data /vagrant/repo/frontend/web/static/render
sudo chmod -R 777 /vagrant/repo/frontend/web/static/user
sudo chmod -R 777 /vagrant/repo/frontend/web/static/files
sudo chmod -R 777 /vagrant/repo/frontend/web/static/render
sudo chmod -R 777 /vagrant/repo/frontend/storage

# update node
sudo bash installer/conf/99_create_config_supervisor.sh
sudo bash installer/conf/nodeapi.sh
sudo php yii filepage-maps/printers-tree-update
sudo service php7.1-fpm restart
echo -e "$?\n"
sudo service nginx restart
echo -e "$?\n"
