#!/bin/bash
# Сделайте ссылку на этот скрипт в /usr/local/sbin/ggit
# ln -s /opt/scripts/ggit.sh /usr/local/sbin/ggit
# environment
 set -x
sdir=/var/www/treatstock
cd $sdir
phpver=8.0
branch_current=$(git branch --show-current)
echo "Текущая ветка: $branch_current"
# functions
function info {
    echo -e "$1 "
    }
function error {
    echo -e "${RED}$1${NC}\n"
    #exit 1
    }

function NodeJS__install {
    cd $sdir/tools/node
    step="execute npm install"
    execute=$(npm i 2>&1) && info "$step - ok" || error "$step - $execute"
}

function git__checkout() {
    info "Выполнение: git checkout $exec"
    fmaintenance true 
    execute=$(/usr/bin/git checkout $exec 2>&1) && info "git checkout - ok" || error "git checkout - $execute"
    execute=$(/usr/bin/git pull 2>&1) && info "git pull - ok" || error "git pull - $execute"
    if [ $branch != "dev" ] && [ $branch != "prerelease" ]; then
    execute=$(/usr/bin/git merge dev 2>&1) && info "git merge dev - ok" || error "git merge dev - $execute"
    fi
    fmigration
    permission
    NodeJS__install
    service__reload
    fmaintenance false
    exit
}

# Функция перевода сайта в режим обслуживания на уровне nginx
function fmaintenance() {
    case $1 in
        true)
        /var/www/treatstock/tools/maintenance/maintenance.sh true
        ;;
        false)
        /var/www/treatstock/tools/maintenance/maintenance.sh false
        ;;
    esac
}

# Функция выводящая справку по использованию скрипта
function usage() {
    info "Usage:
    ggit checkout <task number> \tпереход на ветку (номер задачи из редмайн)
    ggit pull \t\t\t\tподлив изменений
    ggit merge \t\t\t\tподлив dev в текущую ветку
    ggit db-reset \t\t\t\tочистки БД
    ggit updb \t\t\t\tподлив свежей обфусцированной бд (процесс долгий!)
    ggit status \t\t\tвыводит статус
    ggit \t\t\t\tвыводит эту справку
    "
}

# Функция очищающая локальный репозиторий от измененных файлов (в разработке)
function git_clean {
    info "git_clean"
}

# Функция для поиска нужной ветки и запуска переключения на неё
function findBranch {
    # фиксированная настройка для ветки
    if [ $branch = "dev" ]
    then
        echo "Переключение на: $branch"
        exec=$branch
        git__checkout
        exit
    fi
    
    # фиксированная настройка для ветки
    if [ $branch = "prerelease" ]
    then
        echo "Переключение на: $branch"
        exec=$branch
        git__checkout
        exit
    fi

    # Собственно магия переключения ветки
    exec=$(git branch -a | grep /${branch}_ | awk -F"/" '{print $3}')
    if [ ! $exec ]
    then
        info "Ветка для $branch не найдена"
    else
        echo "Переключение на: $exec"
        git__checkout
    fi
    
}
function git__rollback {
    info "rollback"
}

function git_pull {
    info "Выполнение: git pull"
    fmaintenance true
    execute=$(/usr/bin/git pull 2>&1) && info "git pull - ok" || error "git pull - $execute"
    git pull
    fmigration
    permission
    fmaintenance false
    exit
}

function fmigration {
   execute=$(php yii migrate --interactive=0 2>&1) || error "YII Migrate problems \n $execute"
   if [ `echo $execute | grep "No new migrations found. Your system is up-to-date" | wc -l` = 0 ]
   then
     echo "Миграции были, запуск генерации ассетов"
    assets
    else 
    echo "Нет миграций, пропуск генерации ассетов"
   fi
}

function assets {
    php yii asset assets.php frontend/config/assets-dev.php
    php yii asset assets.php frontend/config/assets-prod.php
}

function permission {
    info "Переназначение прав (нет журнала)"
    sudo chown -R $USER:$USER $sdir
    sudo chown -R www-data:www-data $sdir/frontend/runtime
    sudo chown -R www-data:www-data $sdir/console/runtime
    sudo chown -R www-data:www-data $sdir/backend/runtime
    sudo chown -R www-data:www-data $sdir/common/runtime

    sudo chown -R www-data:www-data $sdir/frontend/storage

    sudo chown -R www-data:www-data $sdir/frontend/web
    sudo chown -R www-data:www-data $sdir/backend/web

    sudo chown -R www-data:www-data $sdir/frontend/config
    sudo chown -R www-data:www-data $sdir/console/config
    sudo chown -R www-data:www-data $sdir/backend/config
    sudo chown -R www-data:www-data $sdir/common/config

    sudo chmod 0777 $sdir/frontend/runtime    
    sudo chmod 0777 $sdir/console/runtime
    sudo chmod 0777 $sdir/backend/runtime
    sudo chmod 0777 $sdir/common/runtime

    sudo chmod 0777 $sdir/frontend/config
    sudo chmod 0777 $sdir/console/config
    sudo chmod 0777 $sdir/backend/config
    sudo chmod 0777 $sdir/common/config

    sudo chmod 0777 $sdir/frontend/web
    sudo chmod 0777 $sdir/backend/web
    sudo chown -R $USER:$USER frontend/web/js

    sudo chown -R www-data:www-data $sdir/tools/node/app/log
}

function service__reload {
    #sudo systemctl reload php${phpver}-fpm && info "Успешный перезапуск php${phpver}-fpm" || error "Проблемы при перезапуске php${phpver}-fpm"
    sudo systemctl restart supervisor && info "Успешный перезапуск супервизора" || error "Проблемы при перезапуске супервизора"
}

case $1 in
    pull)
    # Делаем git pull c дополнительныvb действиями в функции
        git_pull
    ;;

    checkout)
    # Переключение веток
    if [ $2_ ]
    echo "Ищем ветку: $2"
     then
        branch=$2
        findBranch
     else 
        error "Empty task number"
        usage
        exit 1
    fi
    ;;
    migration)
    # Запуск миграций и если есть миграции то и генерация ассетов
    fmigration
    ;;

    status)
    # Вывод git status
     git status
     exit 0
    ;;
    maintenance)
    # Открытие или закрытие сайта на обслуживание
    fmaintenance $2
    ;;
    db-reset)
    # Делаем сброс базы с переустрановкой проекта
    info "Выполнение: сброс бд, переустановка проекта"  > /tmp/log
    $sdir/installer/conf/0_recreatedb.sh &
    echo "Отслоеживания для этого действия нет :-D" >> /tmp/log
    ;;
    filepermission)
    # Правим права на файлы
    permission

    ;;
    *)
    # Действие по умолчанию, выводим справку по использованию
        usage
        exit 0
    ;;
esac 