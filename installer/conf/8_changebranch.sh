#!/bin/bash
backupdb()
{
    MYSQL_PWD=mypass mysqldump -u root yii2advanced | gzip > /vagrant/conf/yii2advanced_dev.sql.gz
    MYSQL_PWD=mypass mysqldump -u root yii2advanced_test | gzip > /vagrant/conf/yii2advanced_test_dev.sql.gz
    MYSQL_PWD=mypass mysqldump -u root ts_payout | gzip > /vagrant/conf/ts_payout_dev.sql.gz
}
cd /vagrant/repo
#git fetch --all -q
#if [ ! $? -eq 0 ]; then
#    echo "Репозиторий не обновлися обновите вручную..."
#    exit
#fi
# set username and email
user=`cat .git/config | grep -Po '(?<=\/\/).+(?=@)'`
user_email=`echo $user | sed 's/_/./g'`@treatstock.com
git config --global user.name "$user"
git config --global user.email "$user_email"

git branch | grep -v -E '\*' | awk '{print $1}' | xargs git branch -D
usebranch=$(git branch | grep -e '\*' | awk '{print $2}')
echo "Текущая ветка :" $usebranch
if [ ! -f /vagrant/conf/yii2advanced_dev.sql.gz -o ! -f /vagrant/conf/yii2advanced_test_dev.sql.gz -o ! -f /vagrant/conf/ts_payout_dev.sql.gz ]; then
    if [ "x$usebranch" != "xdev" ]; then
	echo "Скрипт первый раз запускается на ветке dev!!! для сохранения базы"
	exit
    else
	echo "Текущая ветка dev. Сохраняем базы..." | tr -d '\n'
	backupdb
	echo "Готово"
    fi
else
    if [ "x$usebranch" == "xdev" ]; then
	echo "Копии баз с ветки dev уже есть. Перезаписать?"
	set wkeyread=true
	while $wkeyread; do
		set $keypress=""
		read -s -n 1 keypress
		case $keypress in
		[yY])
			echo $keypress
			echo "Текущая ветка dev. Сохраняем базы..." | tr -d '\n'
			backupdb
			echo "Готово"
			wkeyread=false
		;;
		[nN])
			echo $keypress
			wkeyread=false
		;;
		*)
			echo "Yes or No? Your answer: " $keypress
		esac
	done
    fi
fi
echo "Укажите номер задачи для переключения на ветку"
#echo "Enter the task ID number to switch to branch"
echo "     dev -     dev основная ветка"
git remote show origin -n | grep -E '(\s*[0-9]{3,6}_|\s*design_[0-9]{3,6}_)' | grep -v -E 'слит|merge' | sed 's/^[ \t]*//' | awk -F"_" '{if($1 == "design") print "    "$1"_"$2" - "$0;else print "    "$1" - "$0}'
set $keypress=""
read -n 13 keypress
if [ -z "$keypress" ]; then
    echo "Вы не выбрали ветку по задаче"
    exit
fi
branch=$(git remote show origin -n | sed 's/^[ \t]*//' | grep -E "^$keypress" | grep -v -E 'слит|merge|URL')
if [ -z "$branch" ]; then
    echo "Ветка по задаче не найдена"
#    echo "A branch of the task can not be found"
    exit
fi
if [ "x$branch" == "xdev" ]; then
    if [ "x$usebranch" == "x$branch" ]; then
	echo "Если хотите сделать upgrade ветки dev то запустите repo-up-with-data"
	exit
    else
	echo "Происходит возврат на ветку dev..." | tr -d '\n'
	echo "drop database ts_payout;" | mysql -u root --password=mypass
	echo "create database ts_payout;" | mysql -u root --password=mypass
	echo "grant all privileges on ts_payout.* to root@'%' identified by 'mypass'; flush privileges;" | mysql -u root --password=mypass
	echo "grant all privileges on ts_payout.* to root@localhost identified by 'mypass'; flush privileges;" | mysql -u root  --password=mypass
	echo "drop database yii2advanced_test;" | mysql -u root --password=mypass
	echo "drop database yii2advanced;" | mysql -u root --password=mypass
	echo "create database yii2advanced;" | mysql -u root --password=mypass
	echo "create database yii2advanced_test;" | mysql -u root --password=mypass
	echo "grant all privileges on yii2advanced.* to root@'%' identified by 'mypass'; flush privileges;" | mysql -u root --password=mypass
	echo "grant all privileges on yii2advanced.* to root@localhost identified by 'mypass'; flush privileges;" | mysql -u root  --password=mypass
	echo "grant all privileges on yii2advanced_test.* to root@'%' identified by 'mypass'; flush privileges;" | mysql -u root --password=mypass
	echo "grant all privileges on yii2advanced_test.* to root@localhost identified by 'mypass'; flush privileges;" | mysql -u root  --password=mypass
	gunzip -c /vagrant/conf/yii2advanced_dev.sql.gz | mysql -uroot -pmypass yii2advanced
	gunzip -c /vagrant/conf/yii2advanced_test_dev.sql.gz |mysql -uroot -pmypass yii2advanced_test
	gunzip -c /vagrant/conf/ts_payout_dev.sql.gz |mysql -uroot -pmypass ts_payout
	echo "База восстановлена."
    fi
else
    echo "Восстановление базы с ветки dev..." | tr -d "\n"
    echo "drop database ts_payout;" | mysql -u root --password=mypass
    echo "create database ts_payout CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql -u root --password=mypass
    echo "grant all privileges on ts_payout.* to root@'%' identified by 'mypass'; flush privileges;" | mysql -u root --password=mypass
    echo "grant all privileges on ts_payout.* to root@localhost identified by 'mypass'; flush privileges;" | mysql -u root  --password=mypass
    echo "drop database yii2advanced_test;" | mysql -u root --password=mypass
    echo "drop database yii2advanced;" | mysql -u root --password=mypass
    echo "create database yii2advanced CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql -u root --password=mypass
    echo "create database yii2advanced_test CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql -u root --password=mypass
    echo "grant all privileges on yii2advanced.* to root@'%' identified by 'mypass'; flush privileges;" | mysql -u root --password=mypass
    echo "grant all privileges on yii2advanced.* to root@localhost identified by 'mypass'; flush privileges;" | mysql -u root  --password=mypass
    echo "grant all privileges on yii2advanced_test.* to root@'%' identified by 'mypass'; flush privileges;" | mysql -u root --password=mypass
    echo "grant all privileges on yii2advanced_test.* to root@localhost identified by 'mypass'; flush privileges;" | mysql -u root  --password=mypass
    gunzip -c /vagrant/conf/yii2advanced_dev.sql.gz | mysql -uroot -pmypass yii2advanced
    gunzip -c /vagrant/conf/yii2advanced_test_dev.sql.gz |mysql -uroot -pmypass yii2advanced_test
    gunzip -c /vagrant/conf/ts_payout_dev.sql.gz |mysql -uroot -pmypass ts_payout
    echo "База восстановлена."
fi
git checkout $branch -f
if [ "x$branch" != "xdev" ]; then
    git merge origin/dev -m "Script 8_changebranch.sh user:$user email:$email merge branch \"dev\" into \"$branch\""
fi
if [ "x$?" != "x0" ];then
    echo
    echo
    echo "*****************************************************************************************"
    echo "Невозможно слить ветки" $branch "и dev!!!"
    echo "*****************************************************************************************"
    echo "Через 5 секунд произойдет возврат на ветку" $usebranch "..."
    echo "*****************************************************************************************"
    echo
    echo
    echo "*****************************************************************************************"
    echo "Сообщите разрабочкику ветки" $branch "что его ветку невозможно слить с dev"
    echo "*****************************************************************************************"
    echo
    echo
    sleep 5
    git reset --hard
    git checkout $usebranch -f
else
echo "Запуск миграций после переключения ветки"
/vagrant/repo/installer/conf/3_upgrade.sh 2>&1
fi
