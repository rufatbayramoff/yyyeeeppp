#!/bin/bash
sdir=/vagrant/repo
bdir=/vagrant/conf
if [ -d "/tovagrant" ]; then
    bdir=/tovagrant/backup
fi
mkdir -p $bdir
if [ -f "/vagrant/conf/hostid.php" ]; then
    mnum=h`cat /vagrant/conf/hostid.php | grep -Eo '[0-9]{1,2}'`
else
    mnum=h
fi
sshpass -prtg432 ssh -p 60425 backupstate@10.102.0.92 "mkdir -p /backup/backupstate/$mnum/" -vvv
bases=`echo "show databases;" | mysql -u root -pmypass 2>&1 | grep -vE '(Database|mysql|sys|information_schema|performance_schema)'`
for db in $bases; do
mysqldump -u root -pmypass $db | gzip >$bdir/$db.sql.gz
sshpass -prtg432 scp -o StrictHostKeyChecking=no -P 60425 $bdir/$db.sql.gz backupstate@10.102.0.92:/backup/backupstate/$mnum/
echo "---------------------------------------------------------------------------------------------"
echo "- Database $db backup complete to file $db.sql.gz."
echo "---------------------------------------------------------------------------------------------"
done
tar cvJpf $bdir/files.tar.xz /vagrant/repo/frontend/storage /vagrant/repo/frontend/web/static/files /vagrant/repo/frontend/web/static/render
sshpass -prtg432 scp -o StrictHostKeyChecking=no -P 60425 $bdir/files.tar.xz backupstate@10.102.0.92:/backup/backupstate/$mnum/
echo "---------------------------------------------------------------------------------------------"
echo "- files frontend/storage/* frontend/web/static/* saved to files.tar.xz"
echo "---------------------------------------------------------------------------------------------"

