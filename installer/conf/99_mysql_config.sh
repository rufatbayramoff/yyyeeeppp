#!/bin/bash
source "$(dirname "$0")/config.sh"
if $debug;then set -xe;fi

case $environment in 
  dev)
    # DEV
    # Создание базы данных для payouts.vcap.me
    echo "create database IF NOT EXISTS ts_payout CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "CREATE USER IF NOT EXISTS root@'%' IDENTIFIED BY '$mysqlpasswd'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on ts_payout.* to root@'%'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on ts_payout.* to root@'%'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root

    # Создание базы данных для *.vcap.me
    echo "create database IF NOT EXISTS yii2advanced CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "create database IF NOT EXISTS yii2advanced_test CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root

    echo "grant all privileges on yii2advanced.* to root@'%'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on yii2advanced.* to root@localhost; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on yii2advanced_test.* to root@'%' ; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on yii2advanced_test.* to root@localhost; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root

    echo "create database IF NOT EXISTS ext_big CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on ext_big.* to root@localhost; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on ext_big.* to root@'%'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
  ;;
  test)
    # TEST
    # Создание базы данных для payouts.vcap.me
    echo "create database IF NOT EXISTS test_payout CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "CREATE USER IF NOT EXISTS root@'%' IDENTIFIED BY '$mysqlpasswd'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on test_payout.* to root@'%'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on test_payout.* to root@'%'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root

    # Создание базы данных для *.vcap.me
    echo "create database IF NOT EXISTS dbtreatstock_test CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on dbtreatstock_test.* to root@'%'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on dbtreatstock_test.* to root@localhost; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "create database IF NOT EXISTS ext_big CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on ext_big.* to root@localhost; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on ext_big.* to root@'%'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
  ;;
  prod)
    # PROD
    echo "create database IF NOT EXISTS dbtreatstock CHARACTER SET utf8 COLLATE utf8_general_ci;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on dbtreatstock.* to root@'%'; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
    echo "grant all privileges on dbtreatstock.* to root@localhost; flush privileges;" | MYSQL_PWD=$mysqlpasswd mysql -u root
 ;;
esac