#!/bin/bash
set -x
set -e
sudo systemctl stop knockd || true
sudo apt-get -y purge knockd
sudo apt-get -y install knockd iptables
echo '[options]
	UseSyslog

[openSSH]
	sequence    = 7000,8000,9000
	seq_timeout = 5
	command     = /sbin/iptables -I INPUT -s %IP% -p tcp --dport 22 -j ACCEPT
	tcpflags    = syn

[closeSSH]
	sequence    = 9000,8000,7000
	seq_timeout = 5
	command     = /sbin/iptables -D INPUT -s %IP% -p tcp --dport 22 -j ACCEPT
	tcpflags    = syn' >/tmp/knockd.conf
allinterfaces=`ifconfig | grep '^[a-zA-Z]' | grep -v 'lo' | awk '{print $1}' | tr '\n' ' '`
for i in $allinterfaces; do
    interfaces=${interfaces}" -i $i "
done
cat /etc/default/knockd \
 | sed -r "s/START_KNOCKD=0/START_KNOCKD=1/g" \
 | sed -r "s/#KNOCKD_OPTS=.+/KNOCKD_OPTS=\"-i $interfaces\"/g" > /tmp/knockd
sudo cp -f /tmp/knockd /etc/default/knockd
sudo cp -f /tmp/knockd.conf /etc/knockd.conf
sudo systemctl restart knockd
rules=`iptables -L |grep -E 'ctstate NEW tcp dpt:(ssh|22)' |wc -l` 
sed -r /bind-address/s/0.0.0.0/127.0.0.1/g /etc/mysql/mysql.conf.d/mysqld.cnf >/tmp/mysqld.cnf
sudo mv -f /etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.bak
sudo mv -f /tmp/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
sudo service mysql restart
# ps -ax |grep mysqld_safe | grep -v 'grep' | awk '{print $1}' | xargs sudo kill -9
for ((i=1; i <= rules; i++))
do
sudo iptables -D INPUT -p tcp -m conntrack --ctstate NEW -m tcp --source 10.102.0.0/24 --dport 22 -j ACCEPT 
sudo iptables -D INPUT -p tcp -m conntrack --ctstate NEW -m tcp --source 10.102.0.0/24 --dport 3306 -j ACCEPT
done
sudo iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m tcp --source 10.102.0.0/24 --dport 22 -j DROP
sudo iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m tcp --source 10.102.0.0/24 --dport 3306 -j DROP