#!/bin/bash
# environment $sdir - Каталог установки сайта.
export sdir=/vagrant/repo

cd $sdir

#set ip selenium server
ip=$(echo $SSH_CLIENT | awk '{print $1}')
grep 'selenium.local' /etc/hosts 2>&1 >/dev/null
if [ $? -ne 0 ]; then
    echo -e "\n$ip selenium.local" | sudo tee -a /etc/hosts
fi


#preapre test start points
#cp installer/conf/frontend-index-test.php frontend/web/index-test.php 
#cp installer/conf/backend-index-test.php backend/web/index-test.php
mkdir /vagrant/repo/tests_prod/_output_acceptance
sudo rm -Rf $sdir/tests_prod/_output_acceptance/*
sudo mkdir $sdir/tests_prod/_output_acceptance/debug
sudo chmod 777 $sdir/tests_prod/_output_acceptance/debug
sudo rm -Rf $sdir/tests_prod/_output_unit/*

vendor/codeception/codeception/codecept build -c codeception-acceptance-prod.yml
vendor/codeception/codeception/codecept run --tap="acceptancelog.log" --html="acceptancelog.html" -d -c codeception-acceptance-prod.yml acceptance

