#!/bin/bash
rules=`iptables -L |grep -E 'ctstate NEW tcp dpt:(ssh|22)' |wc -l`
sed -r /bind-address/s/127.0.0.1/0.0.0.0/g /etc/mysql/mysql.conf.d/mysqld.cnf >/tmp/mysqld.cnf
sudo mv -f /etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.bak
sudo mv -f /tmp/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
sudo service mysql restart
ps -ax |grep mysqld_safe | grep -v 'grep' | awk '{print $1}' | xargs kill -9
for ((i=1; i <= rules; i++))
do
iptables -D INPUT -p tcp -m conntrack --ctstate NEW -m tcp --source 10.102.0.0/24 --dport 22 -j DROP
iptables -D INPUT -p tcp -m conntrack --ctstate NEW -m tcp --source 10.102.0.0/24 --dport 3306 -j DROP
done
iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m tcp --source 10.102.0.0/24 --dport 22 -j ACCEPT
iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m tcp --source 10.102.0.0/24 --dport 3306 -j ACCEPT