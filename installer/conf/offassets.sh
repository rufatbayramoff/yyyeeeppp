#!/bin/bash
source "$(dirname "$0")/config.sh"
if $debug;then set -xe;fi

cd $sdir

# Disable Assets
if [ -f frontend/config/assets-prod.php ]; then 
    sudo chmod 777 frontend/config/assets-prod.php
    echo '<?php' >frontend/config/assets-prod.php
    echo 'return [' >>frontend/config/assets-prod.php
    echo '];' >>frontend/config/assets-prod.php
fi
if [ -f frontend/config/assets-dev.php ]; then 
    sudo chmod 777 frontend/config/assets-dev.php
    echo '<?php' >frontend/config/assets-dev.php
    echo 'return [' >>frontend/config/assets-dev.php
    echo '];' >>frontend/config/assets-dev.php
fi

if [ -f "$config_dir/xdebugofassets.flag" ]; then
    sudo $sdir/installer/conf/onxdebug.sh
    sudo rm -f $config_dir/xdebugofassets.flag || true
fi

# Remove Asseton Flag
rm -f $config_dir/assetson.flag || true