#!/bin/bash
dbtestuser=root
dbtestpass=FtwwwrU23@
dbtest=dbtreatstock_test
dbtesthost=test.treatstock.com
clear
class=m`date +%y%m%d_%H%M%S`_language_update
echo "<?php

use yii\db\Schema;
use yii\db\Migration;

class $class extends Migration
{
    public function up()
    {
        \$this->truncateTable(\"system_lang_message\");
        \$this->execute(\"
" >./$class.php
#echo "Enter lang iso code to copy:"
inserts=$(echo "SELECT id,title,title_original,iso_code,url from system_lang;" | mysql -B -N -h$dbtesthost -u$dbtestuser -p$dbtestpass $dbtest 2>/dev/null | sed -r $'s/\t/\047,\047/g' | sed -r $'s/^/\t(\047/g' | sed -r $'s/$/\047,\0470\047)/g')
#'
for i in $inserts; do
echo "INSERT IGNORE INTO system_lang (\`id\`,\`title\`,\`title_original\`,\`iso_code\`,\`url\`,\`is_active\`) " | tr -d '\n' >>./$class.php
echo "VALUES " | tr -d '\n' >>./$class.php
echo $i";">> ./$class.php
done
echo "
    \");

        \$path = realpath(dirname(__DIR__) . '/../db/');
        \$sql = file_get_contents(\$path . '/"$class".sql');
        \$this->execute(\$sql);
" >> ./$class.php
mysqldump --skip-lock-tables -h$dbtesthost -u$dbtestuser -p$dbtestpass $dbtest system_lang_message system_lang_source >./$class.sql
echo "

    }

    public function down()
    {
        echo "\"$class cannot be reverted.\\n\"";
        return false;
    }
}
" >> ./$class.php
mv -f ./$class.php ../../console/migrations
mv -f ./$class.sql ../../db
cd ../..

git add console/migrations/$class.php
git add db/$class.sql
git commit -a -c lang_update_$class
exit