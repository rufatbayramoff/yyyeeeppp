@echo off
REM ####################################################################
REM # SET URL=ts.h9.tsdev.work                                         #
REM ####################################################################
SET URL=localhost
cd %~dp0
REM vagrant up
del nt_test_1_base_url_report.jtl /F /S /Q
tools/apache-jmeter-3.3/bin/jmeter.bat -n -t repo/tests/loadtest/nt_test_1_base_url.jmx -l nt_test_1_base_url_report.jtl -e -o jmeter_log -Jurl=%URL%
pause

