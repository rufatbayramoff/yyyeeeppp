cd %~dp0
SET BROWSER=firefox
cd tools
IF -%PROCESSOR_ARCHITECTURE%==-AMD64 (
unzip -o geckodriver-v0.19.1-win64.zip 
) else (
unzip -o geckodriver-v0.19.1-win32.zip 
)
cd ..
start "SELENIUM" /MIN java -Dwebdriver.chrome.driver="tools/chromedriver.exe" -Dwebdriver.gecko.driver="tools/geckodriver.exe" -Dwebdriver.server.session.timeout=86400 -Dwebdriver.server.browser.timeout=86400 -jar tools/selenium-server-standalone-3.8.1.jar -enablePassThrough false

vagrant up
vagrant ssh --command "export BROWSER=%BROWSER%; export DOWNLOADFOLDER='%USERPROFILE%\Download'; cd /vagrant/repo && /vagrant/repo/installer/conf/9_testdebug.sh "
taskkill /FI "WINDOWTITLE eq SELENIUM"
taskkill /F /IM geckodriver.exe
taskkill /F /IM chromedriver.exe
pause

