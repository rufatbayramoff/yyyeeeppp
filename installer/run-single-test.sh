#/bin/bash
export BROWSER=firefox
java -Dwebdriver.chrome.driver="tools/chromedriver" -Dwebdriver.gecko.driver="tools/geckodriver" -Dwebdriver.server.session.timeout=86400 -Dwebdriver.server.browser.timeout=86400 -jar tools/selenium-server-standalone-3.8.1.jar -enablePassThrough false >selenium.log 2>&1 &
echo  export BROWSER=firefox; vendor/codeception/codeception/codecept run --html -c codeception-acceptance.yml acceptance ./tests/acceptance/01_Desktop/03_PS/01_CreatePSCept.php
vagrant ssh
kill %1
killall -9 chromedriver geckodriver
