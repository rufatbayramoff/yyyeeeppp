cd %~dp0
cd repo
git pull
cd ..
vagrant up
vagrant ssh --command "cd /vagrant/repo && /vagrant/repo/installer/conf/0_recreatedb.sh 2>&1 | tee /tmp/repo-up.log"
pause

