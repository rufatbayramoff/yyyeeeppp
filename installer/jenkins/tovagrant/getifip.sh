#!/bin/bash
if [ "x$1" == "x" ]; then
    echo "getifip.sh \"IP Address windows machine selenium server"
    exit
fi
echo -e "\n$1 selenium.local" | sudo tee -a /etc/hosts
vagrantipaddr=$(ifconfig |grep -v '127.0.0' | grep 'inet addr' | grep '10.102.0' | awk '{print $2}' | awk -F":" '{print $2}')
echo "# Created by vagrant to windows client host" >./forwinhost
echo "" >>./forwinhost
echo "$vagrantipaddr               h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr            ts.h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr            ru.h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr            cn.h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr            fr.h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr          couk.h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr       backend.h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr          mail.h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr        static.h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr   mailcatcher.h.tsdev.work" >>./forwinhost
echo "$vagrantipaddr                    vcap.me" >>./forwinhost
echo "$vagrantipaddr                 ts.vcap.me" >>./forwinhost
echo "$vagrantipaddr             static.vcap.me" >>./forwinhost
echo "$vagrantipaddr              ru.ts.vcap.me" >>./forwinhost
echo "$vagrantipaddr              cn.ts.vcap.me" >>./forwinhost
echo "$vagrantipaddr                 fr.vcap.me" >>./forwinhost
echo "$vagrantipaddr               couk.vcap.me" >>./forwinhost
echo "$vagrantipaddr            backend.vcap.me" >>./forwinhost
echo "$vagrantipaddr               mail.vcap.me" >>./forwinhost
echo "$vagrantipaddr        mailcatcher.vcap.me" >>./forwinhost
echo "$vagrantipaddr		Created file host for windows."
sshpass -p 'treerdpssh' scp -o StrictHostKeyChecking=no -P 60425 ./forwinhost user@$1:/windows/system32/drivers/etc/hosts
sshpass -p 'treerdpssh' ssh -o StrictHostKeyChecking=no -p 60425 user@$1 taskkill /F /IM chromedriver.exe
sshpass -p 'treerdpssh' ssh -o StrictHostKeyChecking=no -p 60425 user@$1 taskkill /F /IM geckodriver.exe
sshpass -p 'treerdpssh' ssh -o StrictHostKeyChecking=no -p 60425 user@$1 taskkill /F /IM java.exe
sshpass -p 'treerdpssh' ssh -o StrictHostKeyChecking=no -p 60425 user@$1 taskkill /F /IM chrome.exe
sshpass -p 'treerdpssh' ssh -o StrictHostKeyChecking=no -p 60425 user@$1 taskkill /F /IM firefox.exe
