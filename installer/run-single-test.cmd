cd %~dp0
SET BROWSER=firefox
cd tools
IF -%PROCESSOR_ARCHITECTURE%==-AMD64 (
unzip -o geckodriver-v0.19.1-win64.zip 
) else (
unzip -o geckodriver-v0.19.1-win32.zip 
)
cd ..

start "SELENIUM" /MIN java -Dwebdriver.chrome.driver="tools/chromedriver.exe" -Dwebdriver.gecko.driver="tools/geckodriver.exe" -Dwebdriver.server.session.timeout=86400 -Dwebdriver.server.browser.timeout=86400 -jar tools/selenium-server-standalone-3.8.1.jar -enablePassThrough false

echo  cd /vagrant/repo; export BROWSER=%BROWSER%; export DOWNLOADFOLDER='%USERPROFILE%\Download'; vendor/codeception/codeception/codecept run --html -c codeception-acceptance.yml acceptance tests/acceptance/01_Desktop/03_PS/01_CreatePSCept.php
vagrant ssh
taskkill /FI "WINDOWTITLE eq SELENIUM"
taskkill /F /IM geckodriver.exe
taskkill /F /IM chromedriver.exe
pause

