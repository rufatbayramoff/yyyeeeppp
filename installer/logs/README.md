# Logs directory
Installation logs:
```logformat
YYYY-MM-DD-HH-MM-i/environment
YYYY-MM-DD-HH-MM-i/install.log
YYYY-MM-DD-HH-MM-i/timing.log
```
Update logs:
```logformat
YYYY-MM-DD-HH-MM-u/environment
YYYY-MM-DD-HH-MM-u/update.log
YYYY-MM-DD-HH-MM-u/timing.log
```