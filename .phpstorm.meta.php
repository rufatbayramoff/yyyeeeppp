<?php
// This file is not a CODE, it makes no sense and won't run or validate
// Its AST serves IDE as DATA source to make advanced type inference decisions.

namespace PHPSTORM_META {                           // we want to avoid the pollution
    $STATIC_METHOD_TYPES = [
        \yii\base\Module::getModule('') => [
            "captcha" instanceof \common\modules\captcha\CaptchaModule,
        ],
        \yii\base\Module::getModule('') => [
            "translation" instanceof \common\modules\translation\TranslationModule,
        ],
        \yii\base\Module::getModule('') => [
            "model3dRender" instanceof \common\modules\model3dRender\Model3dRenderModule,
        ],
        \yii\base\Module::getModule('') => [
            "contentAutoBlocker" instanceof \common\modules\contentAutoBlocker\ContentAutoBlockerModule,
        ],
        \yii\base\Module::getModule('') => [
            "psPrinterListMapsModule" instanceof \common\modules\psPrinterListMaps\PsPrinterListMapsModule,
        ],
        \yii\base\Module::getModule('') => [
            "intlDomains" instanceof \common\modules\intlDomains\IntlDomainsModule,
        ],
        \yii\base\Module::getModule('') => [
            "cnc" instanceof \common\modules\cnc\CncModule,
        ],
        \yii\base\Module::getModule('') => [
            "printersList" instanceof \common\modules\printersList\PrintersListModule,
        ],
        \yii\base\Module::getModule('') => [
            "convert" instanceof \common\modules\convert\ConvertModule,
        ],
        \yii\base\Module::getModule('') => [
            "browser-push" instanceof \common\modules\browserPush\BrowserPushModule,
        ],
    ];
}

