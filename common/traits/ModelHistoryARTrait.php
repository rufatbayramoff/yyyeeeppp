<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.08.17
 * Time: 10:25
 */

namespace common\traits;

trait ModelHistoryARTrait
{
    public function getId()
    {
        return $this->getAttribute('id');
    }

    public function getModelId()
    {
        return $this->getAttribute('model_id');
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getAttribute('created_at');
    }

    /**
     * @return integer
     */
    public function getUserId()
    {
        return $this->getAttribute('user_id');
    }


    /**
     * @return string
     */
    public function getActionId()
    {
        return $this->getAttribute('action_id');
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->getAttribute('source');
    }

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->getAttribute('result');
    }

    public function array_diff_assoc_recursive($aArray1, $aArray2)
    {
        $aReturn = [];
        foreach ($aArray1 as $mKey => $mValue) {
            if (is_array($aArray2) && array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = $this->array_diff_assoc_recursive($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                } else {
                    if (is_array($aArray2[$mKey])) {

                        $aArray2[$mKey] = implode('', array_filter($aArray2[$mKey]));
                    }
                    if ((string)$mValue !== (string)$aArray2[$mKey]) {
                        $aReturn[$mKey] = strip_tags($mValue);
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        return $aReturn;
    }

    /**
     * @return array
     */
    public function getSourceResultDiffRemove()
    {
        $source = $this->getSource();
        $result = $this->getResult();
        return $this->array_diff_assoc_recursive($source, $result);
    }

    /**
     * @return array
     */
    public function getSourceResultDiffAdd()
    {
        $source = $this->getSource();
        $result = $this->getResult();
        return $this->array_diff_assoc_recursive($result, $source);
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->getAttribute('comment');

    }


    public function setModelId($id)
    {
        $this->setAttribute('model_id', $id);
    }

    public function setCreatedAt(string $date)
    {
        $this->setAttribute('created_at', $date);
    }

    public function setUserId(int $userId)
    {
        $this->setAttribute('user_id', $userId);
    }

    public function setActionId(string $actionId)
    {
        if ($this->hasAttribute('action_id')) {
            $this->setAttribute('action_id', $actionId);
        }
    }

    public function setSource(string $value)
    {
        $this->setAttribute('source', $value);
    }

    public function setResult(string $value)
    {
        $this->setAttribute('result', $value);
    }

    public function setComment(string $comment)
    {
        $this->setAttribute('comment', $comment);
    }
}