<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\traits\db;


/**
 * Class ActiveScopeTrait
 * @package common\components\db
 * @mixin \yii\db\ActiveQuery
 */
trait ActiveScopeTrait
{
    /**
     * Only active printers
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['is_active' => 1]);
        return $this;
    }
}