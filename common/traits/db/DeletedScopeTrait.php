<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\traits\db;

/**
 * Class DeletedScopeTrait
 * @package common\components\db
 * @mixin \yii\db\ActiveQuery
 */
trait DeletedScopeTrait
{
    /**
     * Search only deleted
     * @return $this
     */
    public function deleted()
    {
        return $this->andWhere(['is_deleted' => 1]);
    }

    /**
     * Search only not deleted
     * @return $this
     */
    public function notDeleted()
    {
        return $this->andWhere(['is_deleted' => 0]);
    }
}