<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\traits\db;
use common\components\exceptions\AssertHelper;
use common\models\base\User;


/**
 * Class ForUserScopeTrait
 * @package common\components\db
 * @mixin \yii\db\ActiveQuery
 */
trait ForUserScopeTrait
{
    /**
     * @param User $user
     * @return $this
     */
    public function forUser(User $user)
    {
        AssertHelper::assert($user->id, 'User have\'t id');
        $this->andWhere(['user_id' => $user->id]);
        return $this;
    }

}