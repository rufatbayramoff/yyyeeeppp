<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\traits;
use yii\base\Exception;
use yii\base\InlineAction;
use yii\db\Transaction;
use common\components\AnnotationHelper;

/**
 * Make controller consier transaction annotate
 * Be careful : this trait override some Controller methods
 * @package common\components
 */
trait TransactedControllerTrait
{
    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws Exception
     */
    public function beforeAction($action)
    {
        if(!($action instanceof InlineAction))
        {
            throw new Exception('Transacted trait support only inline actions');
        }

        $result = parent::beforeAction($action);

        if($result && AnnotationHelper::isMethodAnnotationPresent($this, $action->actionMethod, 'transacted'))
        {
            $this->transaction = \Yii::$app->db->beginTransaction();
        }

        return $result;
    }

    /**
     * @param \yii\base\Action $action
     * @param mixed $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        if($this->transaction && $this->transaction->isActive)
        {
            $this->transaction->commit();
        }

        return parent::afterAction($action, $result);
    }

    /**
     * @param $id
     * @param array $params
     * @return mixed
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function runAction($id, $params = [])
    {
        try
        {
            return parent::runAction($id, $params);
        }
        catch(\Exception $e)
        {
            if($this->transaction && $this->transaction->isActive)
            {
                $this->transaction->rollBack();
            }
            throw $e;
        }
    }

    /**
     * Return action transaction
     * @return Transaction
     */
    protected function getActionTransaction()
    {
        return $this->transaction;
    }
}