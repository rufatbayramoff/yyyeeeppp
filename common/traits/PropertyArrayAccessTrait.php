<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\traits;

/**
 * Class ArrayAccessTrait
 * @package common
 */
trait PropertyArrayAccessTrait
{
    /**
     * @param mixed $offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return $this->$offset !== null;
    }

    /**
     * @param mixed $offset <p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    /**
     * @param mixed $offset <p>
     */
    public function offsetUnset($offset)
    {
        $this->$offset = null;
    }
}