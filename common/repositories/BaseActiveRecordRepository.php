<?php
/**
 * User: nabi
 */

namespace common\repositories;


use common\components\BaseAR;

/**
 * Class BaseActiveRecordRepository
 * basic high level repository to save models
 *
 * @package common\repositories
 */
class BaseActiveRecordRepository
{

    /**
     * @param BaseAR $model
     * @return bool
     * @throws \yii\base\Exception
     */
    public function save(BaseAR $model)
    {
        return $model->safeSave();
    }
}