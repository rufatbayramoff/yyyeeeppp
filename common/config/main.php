<?php
// TODO переделать на переменную окружения
use common\modules\rabbitMq\components\RabbitMq;

setlocale(LC_ALL, 'en_US.UTF-8');

$cncSchema = include 'cncSchema.php';

//include  '/var/www/treatstock/vendor/webtoucher/amqp/controllers/AmqpListenerController.php';

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name'       => 'TS Dev',
    'aliases'    => [
        '@bower'          => '@vendor/bower-asset',
        '@npm'            => '@vendor/npm-asset',
        '@yii/authclient' => '@vendor/yiisoft/yii2-authclient/src'
    ],
    'modules'    => [
        'translation'             => [
            'class' => \common\modules\translation\TranslationModule::class
        ],
        'model3dRender'           => [
            'class'               => \common\modules\model3dRender\Model3dRenderModule::class,
            'renderServerAddress' => [
                'r0' => [
                    //'webdavUrl'         => 'https://r2.treatstock.com:8089/webdav/',
                    //'modelsList'        => 'https://r2.treatstock.com:8089/listing/listing.php',

                    'webdavUrl'         => 'https://r1.tsdev.work:8089/webdav/',
                    'modelsList'        => 'https://r1.tsdev.work:8089/listing/listing2.php',
                    'renderImageParams' => [
                        'imgUrl' => 'https://r1.tsdev.work/render/',
                    ]
                ]
            ]
        ],
        'contentAutoBlocker'      => [
            'class' => \common\modules\contentAutoBlocker\ContentAutoBlockerModule::class
        ],
        'psPrinterListMapsModule' => [
            'class' => \common\modules\psPrinterListMaps\PsPrinterListMapsModule::class
        ],
        'intlDomains'             => [
            'class'               => \common\modules\intlDomains\IntlDomainsModule::class,
            'allowedHeaders'      => 'Content-Type, Content-Range, Content-Disposition, Content-Description, X-Requested-With, X-CSRF-Token',
            'mainDomain'          => 'www.treatstock.com',
            'detectLanguage'      => true,
            'allowedDomains'      => [
                'treatstock.com',
                'treatstock.fr',
                'treatstock.co.uk',
                'ru.treatstock.com',
                'cn.treatstock.com',
                'de.treatstock.com',
            ],
            'langDomainsMap'      => [
                'en-US' => 'treatstock.com',
                'en-GB' => 'treatstock.co.uk',
                'fr-FR' => 'treatstock.fr',
                'zh-CN' => 'cn.treatstock.com',
                'ru'    => 'ru.treatstock.com',
                'de'    => 'de.treatstock.com',
            ],
            'defaultLocationsMap' => [
                'treatstock.fr'     => '88.190.229.170',
                'cn.treatstock.com' => '218.107.132.66',
                'ru.treatstock.com' => '195.34.27.1',
                'treatstock.co.uk'  => '185.86.151.11',
                'de.treatstock.com' => '89.246.64.51',
            ]
        ],
        'cnc'                     => [
            'class'        => \common\modules\cnc\CncModule::class,
            'rawCncSchema' => $cncSchema
        ],
        'printersList'            => [
            'class' => \common\modules\printersList\PrintersListModule::class,
        ],
        'convert'                 => [
            'class' => \common\modules\convert\ConvertModule::class,
        ],
        'browser-push'            => [
            'class' => \common\modules\browserPush\BrowserPushModule::class,
            'keys'  => [ // Can be generated  by https://web-push-codelab.glitch.me/
                \common\modules\browserPush\BrowserPushModule::PUBLIC_KEY  => 'BAaX3Zq6SdsOjTwtndxvIzWSEWMSlJQvMJCbF_VDu9qd01KjVqQQFiJ2VyUwP6cGZiuzonuPGFMrBa_0sAnyUSg',
                \common\modules\browserPush\BrowserPushModule::PRIVATE_KEY => 'HtJ3snS6tYBAio58jVJ9gyAN4l9dOf5RTJApAo641r4'
            ]
        ],
        'message'                 => [
            'class'               => \common\modules\message\MessageModule::class,
            'checkDeliveryConfig' => [
                'host'     => 'imap.yandex.ru',
                'port'     => '993',
                'login'    => 'treatstockfailedmails',
                'password' => '',
            ],
            'localDeliveryLogPath' => '/tmp/bad_email'
        ]
    ],
    'components' => [
        'db'                => [
            'queryCacheDuration' => 30
        ],
        'assetManager'      => [
            'appendTimestamp' => true,
        ],
        'redis'             => [
            'class'    => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port'     => 6379,
            'database' => 1,
        ],
        'session'           => [
            'class' => \common\components\Session::class,

        ],
        'asyncSession'      => [
            'class' => \common\components\AsyncSession::class,
        ],
        'sessionManager'    => [
            'class'           => \common\components\SessionManager::class,
            'sessionCryptKey' => 'g9!5adpDo3d4+asdcvvkFA)4'
        ],
        'setting'           => [
            'class' => 'common\models\SystemSetting'
        ],
        'formatter'         => [
            'class'          => '\yii\i18n\Formatter',
            'sizeFormatBase' => 1000
        ],
        'user'              => [
            'class' => 'common\components\UserSession'
        ],
        'cache'             => [
            'class'     => 'yii\caching\ApcCache',
            'keyPrefix' => 'tsw',
            'useApcu'   => true,
        ],
        'cache_settings'    => [
            'class'     => 'yii\caching\ApcCache',
            'keyPrefix' => 'settings1',
            'useApcu'   => true,
        ],
        'i18n'              => [
            'translations' => [
                '*'   => [
                    'class'              => 'yii\i18n\DbMessageSource',
                    'sourceMessageTable' => '{{%system_lang_source}}',
                    'messageTable'       => '{{%system_lang_message}}'
                ],
                'app' => [
                    'class'              => 'yii\i18n\DbMessageSource',
                    'sourceMessageTable' => '{{%system_lang_source}}',
                    'messageTable'       => '{{%system_lang_message}}'
                ]
            ],
        ],
        'urlManager'        => [
            'class'           => \common\components\UrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName'  => false
        ],
        'sphinx'            => [
            'class'    => 'yii\sphinx\Connection',
            'dsn'      => 'mysql:host=127.0.0.1;port=9306;',
            'username' => '',
            'password' => '',
        ],
        'fs'                => [
            'class' => 'creocoder\flysystem\LocalFilesystem',
            'path'  => '@storage',
        ],
        'log'               => [
            'traceLevel' => YII_DEBUG ? 10 : 0,
            'targets'    => require(__DIR__ . '/logs.php'),
        ],
        'geo'               => [
            'class' => '\lib\geo\GeoService'
        ],
        'authentise'        => [
            'class' => '\lib\authentise\AuthentiseService'
        ],
        'message'           => [
            'class'   => '\lib\message\MessageService',
            'senders' => [
                'email' => '\lib\message\senders\EmailSender',
                'sms'   => '\lib\message\senders\SmsSender'
            ]
        ],
        'orderNotify'       => [
            'class' => '\common\components\order\notifyer\NotifyService',
        ],
        'smsGatewaysBundle' => [
            'class'    => '\lib\sms\SmsGatewaysBundle',
            'gateways' => [
                'cmsms'  => [
                    'class' => '\lib\sms\FakeSmsGateway',
                ],
                'twilio' => [
                    'class' => '\lib\sms\FakeSmsGateway',
                ]
            ],
        ],
        'reviewService'     => [
            'class' => '\common\services\ReviewService',
        ],

        'carrierService' => [
            'class'  => \lib\delivery\carrier\CarrierService::class,
            'vendor' => [
                'class'        => \lib\delivery\carrier\vendors\easypost\EasyPost::class,
                'apikey'       => 'NSyYkGWDL9446oLRmO4NQg',
                'publicApikey' => 'cueqNZUb3ldeWTNX7MU3Mel8UXtaAMUi',
            ]
        ],

        'deliveryService'   => [
            'class' => \lib\delivery\delivery\DeliveryService::class,
        ],
        'watermarker'       => [
            'class' => \common\models\model3d\Model3dWatermarkService::class,
            // server, passphrase
        ],
        'rabbitMq'          => [
            'class'    => RabbitMq::class,
            'host'     => '127.0.0.1',
            'port'     => 5672,
            'user'     => 'root',
            'password' => '1234567',
        ],
        'mobileDetect'      => [
            'class' => '\skeeks\yii2\mobiledetect\MobileDetect'
        ],
        'vue'               => [
            'class' => \common\modules\vue\components\Vue::class
        ],
        'dosProtect'        => [
            'class' => \common\components\DosProtect::class
        ],
        'httpRequestLogger' => [
            'class' => \common\components\HttpRequestLogger::class,
        ],
        'schedule'          => 'omnilight\scheduling\Schedule',
    ]
];
