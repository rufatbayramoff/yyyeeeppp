<?php
return [
    'uploadPath'                    => '@static',
    'staticUrl'                     => 'https://www.tsdev.work/static',
    'apiUrl'                        => 'https://api.tsdev.work/',
    'server'                        => 'https://www.tsdev.work',
    'backendServer'                 => 'https://backend.treatstock.com',
    'adminEmail'                    => 'admin@tsdev.work',
    'supportEmail'                  => 'support@tsdev.work',
    'brandLabel'                    => 'TS Dev',
    'user.passwordResetTokenExpire' => 3600,
    'title'                         => 'TS Dev',
    'name'                          => 'TS Dev',
    'min_price_usd'                 => 1, // min price for order

    'ps_fee'         => 0.15, //0.10, // ts min fee from PS USD
    'ps_min_fee_usd' => 1.99, // ts min fee from PS


    'pickup_radius_km'          => 100, // default radius for pickup, if more - don't show printers
    'unpaid_order_expire'       => 24, // in hours
    'ps_search_timeout'         => 3600, // 1 hour in min.


    /***********************
     *
     *     API keys
     *
     ***********************/
    // FOR TESTING - will be removed to params-local.php later for security
    /**
     * api key for updating exchange rate
     */
    'openexchange_app_id'       => 'a65b27b423214eefb201ed64906ef287',
    'openexchange_expire_hours' => 24,

    /**
     * easypost settings
     */
    'easypost_key'              => 'NSyYkGWDL9446oLRmO4NQg',

    /**
     * paypal settings
     */

    'paypal_client_id'              => 'AX2x4GcHHCgGda9D5Oe-ECutxY9r8nnY7hXNXl3WlgJLGspsBdyREzs1mDrqPy-JfczKZzyeHwUrDiUH',
    'paypal_secret'                 => 'ECqEE8mJfbXlNDWnLbWhAfL_nDhTDnNM87X8YVGNjziAaYH19qzP1cuHUtVE7EpOe_XSQrosw_eHFXLc',
    'paypal_type'                   => "sandbox",
    "paypal_return_url"             => "https://test.treatstock.com/my/payments/transactions",

    /**
     * braintree settings
     */
    'braintree_env'                 => 'sandbox',
    'braintree_merchant'            => 't96b8q7xgpsk8xmy',
    'braintree_public'              => 't3x3pnzgjvczymqv',
    'braintree_private'             => '671372ab3dd38be0652113b94ffbd7a9',
    'braintree_threedsecure_enable' => true,

    /**
     * stripe settings
     */
    'stripe_private_key'            => 'sk_test_51Hiz6iDRRN0ITPZkYd7PQBwHwISUpgzebptaGTxkXfDLKdgPtFFXMcixEJz4ivTrQ61d41lpjTUurlf9ArGs6onk00KcScSPrV',
    'stripe_pub_key'                => 'pk_test_51Hiz6iDRRN0ITPZkVe5N3J2Q1KKuZnY6pod0sPpzDylVSX4a9mK6Ux1yjfLay32uM2sG6838a37mmIgpChtr6MQQ008dx80HPo',
    'stripe_api_version'            => '',


    //'default_pay_card_vendor'       => 'braintree',
    'default_pay_card_vendor'       => 'stripe',

    /**
     * SMS
     */
    'sms_from'                      => 'Treatstock.',
    'cmsms_token'                   => 'ac63b03c-70c2-419c-b573-5a6f7418eaaa',

    'facebook_app_id'     => '578989502249553',

    /**
     * Google
     */
    'google_maps_api_key' => 'AIzaSyAhxGe_ixC4ieueZLPAXyPWphrwgziNZy0', // it test key, for *.vcap.me and test.treatstock.com

    'google_geocode_api_key'           => 'AIzaSyD1VGxBmPEZw7IKVo9aysbTiDhmwP8d2fQ',
    /**
     * must be overridden in params-local.php in PROD
     */
    'local_render_cmd'                 => '/vagrant/repo/tools/renderer2/dist/Debug/GNU-Linux/renderer2',

    /**
     * Facebook Ps print application id
     */
    'facebook_ps_print_application_id' => '1828647664050026',
    'facebook_ps_secrect'              => '879122e8248c6c0c3830b8b7ded81144',
    'local_render_cmd'                 => '/var/www/treatstock/tools/renderer2/dist/Debug/GNU-Linux/renderer2',

    'thingprint_material'       => 'PLA',

    // sandbox data
    'thingiverse_client_id'     => '8b17fceb9b1433c84731',
    'thingiverse_client_secret' => '2687ce8aaef1479d2cbd9fe25dec528b',
    'thingiverse_url'           => 'https://sandbox.thingiverse.com',
    'thingiverse_api_url'       => 'https://api-sandbox.thingiverse.com',

    // watermark model3d signer
    'watermarkServer'           => 'http://127.0.0.1:5858',

    'systemTimezone' => 'UTC', // need UTC for production

    'httpScheme'       => 'https://',
    'sitemapAlias'     => '@frontend/web/static/sitemap',

    // Enable inspectect flag
    'enableInspectlet' => false,

    'cuttingCorelDrawConverter' => 'http://51.81.83.36:1180/1dokg-upload.php',

    // Onshape Keys
    'api.onshape.apiKey'        => 'oi0hcVSsOUfuTW8lRmpzlM1W',
    'api.onshape.secretKey'     => 'XKrp6bN6d270tGJAe7xW4ZFIT311TB3lOC69NmNwEuPzogrR',

    // for test
    'api.onshape.oauthKey'      => 'DC6N5WKOAOGHBCTEPRAJ3NIXUA43I6O72JH5OWQ=',
    'api.onshape.oauthSecret'   => 'ITZ3AHMKOK2URRYFRXNIO64SGOVYHW4GGEPIQPWKJQMZUGRPM5QQ====',
    //'api.onshape.redirectUri' => 'https://ts.vcap.me/onshape/app/auth'

    'siteMode' => 'up', // down - if upgrading

    'allowDynamicFields' => true,

    'excludeHttpRequestLogTable' => [
        'http_request_ext_data_log',
        'http_request_log'
    ],
    'fileCompression'            => [
        'compressor'         => 'gzip %s',
        'decompressor'       => [
            'gz' => 'gzip -d -k %s',
            'xz' => 'xz -d -k %s'
        ],
        'compressFileExpire' => 60 * 60 * 24 * 14,
        'compressExtension'  => ['stl']
    ]
];
