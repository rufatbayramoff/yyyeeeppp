<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.11.16
 * Time: 11:57
 */


Yii::$container->set(\yii\web\Response::class, \common\components\WebResponse::class);
Yii::$container->set(\yii\web\AssetManager::class, \common\components\WebAssetManager::class);
Yii::$container->set(\yii\validators\ExistValidator::class, \common\components\ExistsValidator::class);
if (defined('TEST_XSS') && TEST_XSS) {
    Yii::$container->set(\yii\validators\UrlValidator::class, \common\modules\xss\validators\UrlValidator::class);
    Yii::$container->set(\yii\validators\EmailValidator::class, \common\modules\xss\validators\EmailValidator::class);
}
//Yii::$container->set(\beowulfenator\JsonEditor\JsonEditorAsset::class, \frontend\assets\JsonEditorAsset::class);
