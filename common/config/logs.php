<?php
return [
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'logVars'        => ['_GET'],
        'levels'         => ['error', 'warning'],
    ],

    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['error'],
        'except'         => ['yii\base\UserException', 'GeoIp2\Exception\AddressNotFoundException', 'geo', 'lib\delivery\exceptions\HaveSuggestAddressException_http:422'],
        'logVars'        => [],
        'logFile'        => '@common/runtime/logs/errors.log'
    ],


    // to specific files
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['smsapi', 'sms'],
        'logFile'        => '@common/runtime/logs/smsapi.log'
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => ['GET'],
        'categories'     => ['easypost'],
        'logFile'        => '@common/runtime/logs/api/easyPost.log',
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['base_ar'],
        'logFile'        => '@common/runtime/logs/base_ar.log'
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['upcase_url'],
        'logFile'        => '@frontend/runtime/logs/upcase_url.log'
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'error', 'warning'],
        'logVars'        => ['_GET'],
        'categories'     => ['stripe', 'payment', 'braintree', 'payment_manager', 'pricer'],
        'logFile'        => '@console/runtime/logs/API/payment.log'
    ],

    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['order'],
        'logFile'        => '@frontend/runtime/logs/order.log',
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['pscatalog'],
        'logFile'        => '@frontend/runtime/logs/pscatalog.log',
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['tsdebug', 'geo', 'watermark'],
        'logFile'        => '@frontend/runtime/logs/tsdebug.log',
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['watermark'],
        'logFile'        => '@frontend/runtime/logs/watermark.log',
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['thingiverse'],
        'logFile'        => '@frontend/runtime/logs/thingiverse.log',
    ],


    //  to default app.log
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'error', 'warning'],
        'logVars'        => ['_GET'],
        'categories'     => ['email']
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['store']
    ],

    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['job', 'filejob', 'model3d', 'render3d', 'slicer3d', 'parser3d', 'remoteRender']
    ],

    /** frontend LOGS **/
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['osn']
    ],
    [
        'class'          => common\components\FileTarget::class,
        'enableRotation' => false,
        'levels'         => ['info', 'warning', 'error'],
        'logVars'        => [],
        'categories'     => ['upload']
    ],
];