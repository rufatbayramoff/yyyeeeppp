<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('lib', dirname(dirname(__DIR__)) .'/lib');
Yii::setAlias('tests', dirname(dirname(__DIR__)) .'/tests');
Yii::setAlias('doc', dirname(dirname(__DIR__)) .'/doc');
Yii::setAlias('static', dirname(dirname(__DIR__)) . '/frontend/web/static');
Yii::setAlias('localhost', dirname(dirname(__DIR__)) . '/frontend/web');
Yii::setAlias('storage', dirname(dirname(__DIR__)) . '/frontend/storage');

require_once (dirname(__DIR__) . "/functions.php");

Yii::$container = new \common\components\Container();

require_once __DIR__ . '/diContainerConfig.php';