<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.10.17
 * Time: 18:29
 */

namespace common\garbageCollectors;

use common\components\ArrayHelper;
use common\components\DateHelper;
use common\models\repositories\FileRepository;
use common\models\User;
use common\models\UserSession;
use Yii;

class UserSessionGarbageCollector extends BaseGarbageCollector
{
    CONST EXPIRE_TIME_LEVEL1 = 60 * 60 * 24 * 30 * 6; // Six month all
    CONST EXPIRE_TIME_LEVEL0 = 60 * 60 * 24 * 30 * 1; // One month delete not used sessions

    public function deleteLevel1()
    {
        Yii::$app->db->createCommand('SET SQL_BIG_SELECTS=1')->execute();
        $expireTime = DateHelper::subNowSec(self::EXPIRE_TIME_LEVEL0);
        do {
            $sql  = <<<SQL
SELECT
    `user_session`.id
FROM
    `user_session`
        LEFT JOIN
    `cart` ON `user_session`.`id` = `cart`.`user_session_id`
    LEFT JOIN
    `payment_invoice` ON `user_session`.`id` = `payment_invoice`.`user_session_id`
        LEFT JOIN
    `model3d` ON `user_session`.`id` = `model3d`.`user_session_id`
        LEFT JOIN
    `model3d_replica` ON `user_session`.`id` = `model3d_replica`.`user_session_id`
        LEFT JOIN
    `cutting_pack` ON `user_session`.`id` = `cutting_pack`.`user_session_id`
        LEFT JOIN
    `model3d_viewed_state` ON `user_session`.`id` = `model3d_viewed_state`.`user_session_id`
        LEFT JOIN
    `store_order` ON `user_session`.`id` = `store_order`.`user_session_id`
        LEFT JOIN
    `store_unit_shopping_candidate` ON `user_session`.`id` = `store_unit_shopping_candidate`.`user_session_id`
        LEFT JOIN
    `file` ON `user_session`.`id` = `file`.`user_session_id`
        LEFT JOIN
    `cs_window_quote` ON `user_session`.`id` = `cs_window_quote`.`user_session_id`    
WHERE
        (`cart`.`user_session_id` IS NULL)
        AND (`payment_invoice`.`user_session_id` IS NULL)
        AND (`model3d`.`user_session_id` IS NULL)
        AND (`cutting_pack`.`user_session_id` IS NULL)
        AND (`model3d_replica`.`user_session_id` IS NULL)
        AND (`model3d_viewed_state`.`user_session_id` IS NULL)
        AND (`store_order`.`user_session_id` IS NULL)
        AND (`store_unit_shopping_candidate`.`user_session_id` IS NULL)
        AND (`file`.`user_session_id` IS NULL)
        AND (`user_session`.`created_at` < '$expireTime')
        AND (`cs_window_quote`.`user_session_id` IS NULL)
LIMIT 100000
SQL;
            $rows = Yii::$app->db->createCommand($sql)->queryAll();
            $rows = ArrayHelper::getColumn($rows, 'id');
            if (($key = array_search(1, $rows)) !== false) {
                unset($rows[$key]);
            }
            $this->debugOut('Level 1. Deleted: ' . count($rows));
            UserSession::deleteAll(['id' => $rows]);
        } while ($rows);
    }

    public function deleteLevel0()
    {
        /** @var FileRepository $fileRepository */
        $fileRepository = Yii::createObject(FileRepository::class);
        $expireTime     = DateHelper::subNowSec(self::EXPIRE_TIME_LEVEL1);
        do {
            $sessions      = UserSession::find()->where(['<', 'created_at', $expireTime])->andWhere('id!=1')->limit(100000)->orderBy('created_at asc')->withoutStaticCache()->all();
            $sessionsCount = count($sessions);
            /** @var UserSession $session */
            foreach ($sessions as $session) {
                foreach ($session->files as $file) {
                    $file->updateAttributes(['user_session_id' => 1]);
                }
                foreach ($session->carts as $cart) {
                    $cart->delete();
                }
                foreach ($session->model3ds as $model3d) {
                    $model3d->updateAttributes(['user_session_id' => 1]);
                }

                foreach ($session->model3dReplicas as $model3dReplica) {
                    $model3dReplica->updateAttributes(['user_session_id' => 1]);
                }

                foreach ($session->model3dReplicas as $model3dReplica) {
                    $model3dReplica->updateAttributes(['user_session_id' => 1]);
                }

                foreach ($session->cuttingPacks as $cuttingPack) {
                    $cuttingPack->updateAttributes(['user_session_id' => 1]);
                }

                foreach ($session->storeOrders as $storeOrder) {
                    $storeOrder->updateAttributes(['user_session_id' => null]);
                }

                foreach ($session->paymentInvoices as $paymentInvoice) {
                    $paymentInvoice->updateAttributes(['user_session_id' => 1]);
                }

                foreach ($session->csWindowQuotes as $windowQuote) {
                    $windowQuote->updateAttributes(['user_session_id' => 1]);
                }

                $session->delete();
            }
            $this->debugOut('Level 0. Deleted: ' . $sessionsCount);
        } while ($sessionsCount);
    }

    public function run()
    {
        $this->debugOut('Drop user sessions start.');
        $this->deleteLevel0();
        $this->deleteLevel1();
        $this->debugOut("Drop user sessions finish\n");
    }
}