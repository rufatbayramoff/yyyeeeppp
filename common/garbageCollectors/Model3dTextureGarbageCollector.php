<?php

namespace common\garbageCollectors;

use common\components\ArrayHelper;
use common\models\Model3dTexture;
use Yii;
use yii\db\Query;
use function GuzzleHttp\Promise\all;

class Model3dTextureGarbageCollector extends BaseGarbageCollector
{

    public function run()
    {
        $sql     = '
        SELECT model3d_texture.id FROM model3d_texture WHERE id not in (SELECT x.texture_id FROM ( (SELECT model3d.model3d_texture_id as texture_id FROM model3d WHERE model3d.model3d_texture_id is not null) UNION ALL (SELECT model3d_part.model3d_texture_id as texture_id FROM model3d_part WHERE model3d_part.model3d_texture_id is not null) UNION ALL (SELECT model3d_replica.model3d_texture_id as texture_id FROM model3d_replica WHERE model3d_replica.model3d_texture_id is not null) UNION ALL (SELECT model3d_replica_part.model3d_texture_id as texture_id FROM model3d_replica_part WHERE model3d_replica_part.model3d_texture_id is not null) ) x)
        ';
        $rows    = Yii::$app->db->createCommand($sql)->queryAll();
        $idsList = [];
        $i       = 0;
        foreach ($rows as $row) {
            $idsList[$row['id']] = $row['id'];
            $i++;
            if ($i > 100) {
                $i = 0;
                Model3dTexture::deleteAll(['id' => $idsList]);
                $idsList = [];
            }
        }
        Model3dTexture::deleteAll(['id' => $idsList]);

        \Yii::$app->db->createCommand('OPTIMIZE TABLE `model3d_texture`')->execute();

        $this->debugOut('Model3d Textures removed: ' . count($rows) . " elements\n");
    }

}