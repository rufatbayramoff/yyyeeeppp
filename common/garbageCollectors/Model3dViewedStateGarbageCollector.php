<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.10.17
 * Time: 15:30
 */

namespace common\garbageCollectors;

use common\components\DateHelper;
use common\models\Model3dViewedState;

class Model3dViewedStateGarbageCollector extends BaseGarbageCollector
{
    CONST EXPIRE_TIME = 60 * 60 * 24 * 30 * 2; // Two month

    public function run()
    {
        $expireTime = DateHelper::subNowSec(self::EXPIRE_TIME);
        $count = Model3dViewedState::deleteAll(['<', 'date', $expireTime]);
        $this->debugOut('Model3dViewedState removed: ' . $count . " elements\n");
    }
}