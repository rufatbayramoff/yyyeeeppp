<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.10.17
 * Time: 15:31
 */

namespace common\garbageCollectors;

use common\components\DateHelper;

abstract class BaseGarbageCollector
{
    public function debugOut($str)
    {
        echo "\n" . DateHelper::now() . ' ' . $str;
    }

    abstract public function run();
}