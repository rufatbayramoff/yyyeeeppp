<?php

namespace common\models;

use common\components\ArrayHelper;
use yii\helpers\Json;

/**
 * Class PsCatalog
 * @package common\models
 */
class PsCatalog extends \common\models\base\PsCatalog
{

    public const CONVERSION = 'conversion_value';
    public const COUNTRY_RATING = 'company_country_rating';

    /**
     * @param int $rating
     */
    public function editRating(int $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @param array $values
     */
    public function addToLog(array $values): void
    {
        $oldValues = Json::decode($this->rating_log);
        $this->rating_log = Json::encode(ArrayHelper::merge(
            $oldValues,
            $values
        ));
    }

    public function conversion(): string
    {
        $data = Json::decode($this->rating_log);
        return $data[self::CONVERSION] ?? 0;
    }

    public function countryRating()
    {
        $data = Json::decode($this->rating_log);
        return $data[self::COUNTRY_RATING] ?? 0;
    }
}