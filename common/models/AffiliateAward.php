<?php

namespace common\models;

use lib\money\Money;

/**
 * Class AffiliateAward
 * @package common\models
 */
class AffiliateAward extends \common\models\base\AffiliateAward
{
    public const STATUS_NEW     = 'new';
    public const STATUS_PENDING = 'pending';
    public const STATUS_DONE    = 'done';

    public function getPriceMoney()
    {
        return Money::create($this->price, $this->currency);
    }

    public function getTextDescription()
    {
        return $this->affiliateSource->user->username . ' - ' . $this->price . ' ' . $this->currency;
    }

    public function getStatusList()
    {
        return [
            self::STATUS_NEW     => 'New',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_DONE    => 'Done'
        ];
    }

    public function getStatusText()
    {
        if ($this->accruedPaymentDetail) {
            return _t('user.affiliate', 'done');
        }
        if ($this->invoice->isPayed()) {
            return _t('user.affiliate', 'pending');
        }
        return _t('user.affiliate', 'new');
    }
}