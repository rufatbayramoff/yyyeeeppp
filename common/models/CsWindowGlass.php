<?php

namespace common\models;

/**
 * Class CsWindowGlass
 * @package common\models
 */
class CsWindowGlass extends \common\models\base\CsWindowGlass
{
    public $isDeleted = false;
}