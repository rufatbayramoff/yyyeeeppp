<?php

namespace common\models;

/**
 * Class PaymentReceipt
 * @package common\models
 */
class PaymentReceipt extends \common\models\base\PaymentReceipt
{
    public function getBindInvoices(): array
    {
        $receiptInvoices   = $this->order ? $this->order->paymentInvoices : [];
        if ($this->invoice) {
            $receiptInvoices[] = $this->invoice;
        }
        return $receiptInvoices;
    }

    public function getCombinedId()
    {
        $cid = $this->id ;
        if ($this->order_id) {
            $cid .= '-order-'.$this->order_id;
        }
        if ($this->invoice_uuid) {
            $cid .= '-invoice-'.$this->invoice_uuid;
        }
        return $cid;
    }
}