<?php namespace common\models;


use common\components\exceptions\DeprecatedException;
use common\components\exceptions\ValidationException;
use common\interfaces\Model3dBaseInterface;
use common\modules\product\interfaces\ProductInterface;
use common\models\query\StoreUnitQuery;
use common\services\AntiVirusService;
use console\jobs\QueueGateway;
use common\components\PaymentExchangeRateFacade;
use yii\helpers\ArrayHelper;

/**
 * Store unit logic and properties
 *
 * @property Model3d $model3d
 *
 * @method  StoreUnitQuery getStoreOrderItems()
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StoreUnit extends \common\models\base\StoreUnit
{

    /*************************************************** DEPRICATED **************************************************/
    /**
     * new status - user just published model to store
     * goes to this status from rejected, published
     *
     * @deprecated
     */
    const STATUS_NEW = 'new';

    /**
     * moderator working on this model, reviewing it.
     * this status helps to limit working on model only by one moderator
     * simply, status means moderation in process
     *
     * @deprecated
     */
    const STATUS_REVIEW = 'review';

    /**
     * goes here after re-publishing, when user edits his model
     * so moderator should look again to this model.
     * from this status goes to 'NEW' again.
     *
     * @deprecated
     */
    const STATUS_PENDING = 'pending';

    /**
     * model is published to store by moderator
     * from this status can go to NEW status
     * if user edits some model details
     *
     * @deprecated
     */
    const STATUS_PUBLISHED = 'published';

    /**
     * user started uploading a model,
     * this store unit is in draft and should not be available for publishing
     *
     * @deprecated
     */
    const STATUS_DRAFT = 'draft';

    /**
     * model is rejected with why reason
     * user can fix these problems and try to publish again.
     * goes to status 'pending'
     *
     * @deprecated
     */
    const STATUS_REJECTED = 'rejected';


    /**
     * Model is banned, and cannot be added again to store.
     * So user will see that model is banned, and button to
     * report abuse.
     *
     * @deprecated
     */
    const STATUS_BANNED = 'banned';

    /**
     * user deleted model from store
     *
     * @deprecated
     */
    const STATUS_DELETED = 'deleted';

    /**
     * @deprecated
     */
    const STATUS_UNPUBLISHED = 'unpublished';

    /*************************************************** DEPRICATED ***************************************************/

    public $licence_id = 1;

    /**
     * @return \yii\db\ActiveQuery
     * @deprecated
     */
    public function getModel3d()
    {
        return $this->hasOne(Model3d::className(), ['id' => 'model3d_id']);
    }

    /**
     *
     * @param string $status
     * @return string
     * @deprecated
     */
    public static function getStatusTxt($status)
    {
        if ($status == ProductInterface::STATUS_PUBLISH_PENDING) {
            $status = _t('site.store', 'Pending moderation');
            return $status;
        }
        $statuses = self::getStatuses();
        return isset($statuses[$status]) ? $statuses[$status] : $status;
    }

    /**
     * @return bool
     * @deprecated
     */
    public function isAvailableForBuy()
    {
        if ($this->model3d->isPublished()) {
            return true;
        }
        return false;
    }


    /**
     * get model price
     *
     * @return float
     * @deprecated
     */
    public function getModelPrice()
    {
        return $this->model3d->price_per_produce;
    }

    /**
     * get display price for end user with specified currency
     *
     * @return string
     * @deprecated
     */
    public function getDisplayPrice()
    {
        if ($this->model3d->price_per_produce == '0.00') {
            $price = 'Free';
        } else {
            $userCurrency   = \frontend\components\UserSessionFacade::getCurrency();
            $modelPriceOrig = $this->model3d->price_per_produce;
            $modelPrice     = PaymentExchangeRateFacade::convert($modelPriceOrig, $this->model3d->company->currency, $userCurrency);
            $price          = \Yii::$app->formatter->asCurrency($modelPrice, $userCurrency);
        }
        return $price;
    }

    /**
     * Check that store unit can be publish
     *
     * @param StoreUnit $storeUnit
     * @return bool
     * @throws \yii\base\NotSupportedException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     * @todo this metod id static for easy transfer to a more suitable place, like facade or \backend\models\StoreUnit
     * @deprecated
     */
    public static function getCanBePublished(StoreUnit $storeUnit)
    {
        $model3d = $storeUnit->model3d;

        // If already published
        if (in_array($storeUnit->model3d->getProductStatus(), [
            ProductInterface::STATUS_PUBLISHED_PUBLIC,
            ProductInterface::STATUS_PUBLISHED_DIRECTLY
        ], true)) {
            return false;
        }
        if (!$model3d->is_active) {
            return false;
        }
        // check antivirus jobs is success
        $modelFiles = $model3d->getActiveModel3dParts();
        // check render jobs
        $fileIds = ArrayHelper::getColumn($modelFiles, 'file_id');

        $fileJobs     = \common\models\FileJob::findAll(['file_id' => $fileIds, 'status' => 'complete']);
        $jobsComplete = [];
        foreach ($fileJobs as $fileJob) {
            $jobsComplete[$fileJob->file_id][] = $fileJob->operation;
        }
        foreach ($jobsComplete as $fileId => $jobComplete) {
            if (!in_array('render', $jobComplete)) {
                throw new \yii\base\UserException("No valid render jobs for file[$fileId]");
            }
            if (!in_array('parser', $jobComplete)) {
                throw new \yii\base\UserException("No valid parser jobs for file[$fileId]");
            }
        }

        return true;
    }

    /**
     * Delete unit
     *
     * @deprecated
     */
    public function deleteUnit()
    {
        throw new DeprecatedException();
    }

    /**
     * Calc one model rating
     *
     * @return integer
     * @deprecated
     */
    public function calcStoreUnitRating()
    {
        $model3d               = $this->model3d;
        $viewsCount            = $model3d->stat_views;
        $likeCount             = count($model3d->userCollectionModel3ds);
        $correctionCoefficient = $this->correction_sort_catalog_coefficient;
        $printedCount          = 0;
        $storeItems            = $this->storeOrderItems;
        foreach ($storeItems as $storeItem) {
            if ($storeItem->order->order_state == StoreOrder::STATE_COMPLETED) {
                $printedCount++;
            }
        }
        return $viewsCount + $likeCount * 10 + $printedCount * 30 + $correctionCoefficient;
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @deprecated
     * @internal param File $file
     */
    public function setModel3d(Model3dBaseInterface $model3d)
    {
        $this->populateRelation('model3d', $model3d);
    }

    public function validateOrException()
    {
        $this->validate();
        $this->model3d->validate();
        if ($this->hasErrors() || $this->model3d->hasErrors()) {
            throw new ValidationException([$this, $this->model3d]);
        }
    }

    /**
     * Is store unit published
     *
     * @return bool
     * @deprecated
     */
    public function isPublished()
    {
        throw new DeprecatedException();
    }

    /**
     * @param $published
     * @deprecated
     */
    public function setPublished($published)
    {
        throw new DeprecatedException();
    }

    public function isTestOrder()
    {
        return $this->id ==(int)app('setting')->get('printer.testorder.storeunit', -1);
    }
}
