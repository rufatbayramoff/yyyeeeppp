<?php

namespace common\models;

/**
 * Class SeoPage
 *
 * @package common\models
 */
class SeoPage extends \common\models\base\SeoPage
{

    private static $translateFields = ['title', 'header', 'meta_description', 'meta_keywords', 'header_text', 'footer_text', 'updated_at'];

    /**
     * @return array
     */
    public static function getTranslateFieldsList()
    {
        return self::$translateFields;
    }

    public static function setTranslateFieldList($fields)
    {
        self::$translateFields = $fields;
    }
}