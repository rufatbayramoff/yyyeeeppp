<?php namespace common\models;

use common\components\exceptions\DeprecatedException;
use common\components\PaymentExchangeRateConverter;
use lib\money\Currency;
use lib\money\Money;

/**
 * Store pricer 
 * 
 * @property integer $id
 * @property integer $order_id
 * @property integer $price_type
 * @property string $created_at
 * @property string $updated_at
 * @property string|float $price
 * @property string $currency
 * @deprecated
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StorePricer extends \common\models\base\StorePricer
{
    
    /**
     * current user id working with pricer
     * 
     * @var float
     */
    public static $userId = false;
    
    /**
     * payment exchange rate object
     * 
     * @var PaymentExchangeRate 
     */
    public static $rate = false;
    
    /**
     * order object. required to check prices currency and etc.
     * 
     * @var StoreOrder 
     */
    public static $order = false;

    /**
     * return StorePricer object not null, if not found
     * with empty price
     * @var bool
     */
    public static $stubZeroPrice = false;
    /**
     * runtime cache vars
     * @var array
     */
    public static $runtimeCache = [];

    /**
     * init pricer settings
     * 
     * @param array $params
     */
    public static function initPricer($params)
    {
        self::$userId = $params['user_id'];
        self::$rate = $params['rate'];
        self::$order = $params['order'];
    }


    /**
     * @return Money
     * @deprecated
     */
    public function getAsMoney()
    {
        throw new DeprecatedException('Depricated: StorePricer:getAsMoney');
        return Money::create($this->price, $this->currency);
    }

    /**
     * @param $amount
     * @param $txt
     * @return mixed
     * @deprecated
     * @throws DeprecatedException
     */
    public function getFee($amount, $txt){
        throw new DeprecatedException('Depricated: StorePricer:getFee');
        return $amount;
    }
    /**
     * get current currency
     * @return string
     * @deprecated
     */
    public static function getCurrentCurrency()
    {
        throw new DeprecatedException('Depricated: StorePricer:getCurrentCurrency');
        return self::$order->price_currency;
    }

    /**
     * get total price as number for order_id
     *
     * @param int $orderId
     * @param bool $cached
     * @deprecated
     * @return float
     */
    public static function getTotal($orderId, $cached = true)
    {
        throw new DeprecatedException('Depricated: StorePricer:getTotal');
        $cacheKey = 'getTotal_'.$orderId;
        if (isset(self::$runtimeCache[$cacheKey]) && $cached) {
            return self::$runtimeCache[$cacheKey];
        }
        $prices = self::find()->withoutStaticCache()->where(['order_id' => $orderId])->with(['pricerType'])->all();
        $totalPrice = StorePricerType::calcTotal($prices);
        self::$runtimeCache[$cacheKey] = $totalPrice;
        return $totalPrice;
    }

    /**
     * usage StorePricer::updatePrice($orderId, 'model', 23.34,  StorePriceHistory::ACTION_START);
     *
     * @param int $orderId
     * @param string $priceType
     * @param int $priceIn
     * @param int $actionId //in StorePricerHistory  please add new action_id if you try to modify price from another place
     * @param string $comment
     * @return bool
     * @throws \Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     * @deprecated
     */
    public static function updatePrice($orderId, $priceType, $priceIn, $actionId, $comment = '')
    {
        throw new DeprecatedException('Depricated: StorePricer:updatePrice');
        if(self::$userId===false){
            throw new \Exception('Init StorePricer before using');
        }
        $pricerTypeObj = StorePricerType::tryFind(['type' => $priceType], $priceType . ' not found');
        // first search if has
        $pricer = StorePricer::find()->withoutStaticCache()->where(
            ['order_id' => $orderId, 'pricer_type' => $pricerTypeObj->id]
        )->one();
        if ($pricer == null) {
            $pricer = new StorePricer();
            $pricer->created_at = dbexpr('NOW()');
            $pricer->order_id = $orderId;
            $pricer->pricer_type = $pricerTypeObj->id;            
            $pricer->currency = self::getCurrentCurrency();
        } 
        $transaction = app('db')->beginTransaction();
        try {
            $price = round($priceIn, 4); // round price up to 0.0000
            $oldPrice = $pricer->price ? $pricer->price : 0;
            $pricer->price = $price;
            $pricer->updated_at = dbexpr('NOW()');
            $pricer->rate_id = self::$rate->id;
            $pricer->safeSave();
            $pricerId = $pricer->id;
            $history = [
                'user_id' => self::$userId,
                'created_at' => dbexpr('NOW()'),
                'pricer_id' => $pricerId,
                'action_id' => $actionId,
                'old_price' => $oldPrice,
                'new_price' => $price,
                'comment' => $comment . ' rate_id:'.$pricer->rate_id
            ];
            if ($oldPrice !== $price) {
                StorePricerHistory::addRecord($history);
            }
            $transaction->commit();
            // log            
            unset($history['created_at']);
            \Yii::info(var_export($history, true), 'pricer');
        } catch (\Exception $e) {
            $transaction->rollback();
            \Yii::info($e->getTraceAsString(), 'pricer');
            throw $e;
        }
        return true;
    }

    /**
     * refresh total in database
     *
     * @param int $orderId
     * @return float
     * @deprecated
     */
    public static function refershTotal($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:refershTotal');
    }

    /**
     * add model price
     *
     * @param int $orderId
     * @param float $price
     * @param int $modelId
     * @return bool
     * @deprecated
     */
    public static function addModelPrice($orderId, $price, $modelId)
    {
        throw new DeprecatedException('Depricated: StorePricer:addModelPrice');
        return self::updatePrice($orderId, StorePricerType::MODEL, $price, StorePricerHistory::ACTION_SYSTEM, 'model:'.$modelId);
    }

    /**
     * @param $orderId
     * @param $price
     * @param string $comment
     * @return bool
     * @throws DeprecatedException
     * @throws \Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     * @deprecated
     */
    public static function addTransactionFee($orderId, $price, $comment = '')
    {
        throw new DeprecatedException('Depricated: StorePricer:addTransactionFee');
        return self::updatePrice($orderId, StorePricerType::TRANSACTION, $price, StorePricerHistory::ACTION_SYSTEM, $comment);
    }

    /**
     * add shipment price
     *
     * @param int $orderId
     * @param float $price
     * @param string $comment
     * @return bool
     * @throws \Exception
     * @deprecated
     */
    public static function addShipment($orderId, $price, $comment = '')
    {
        throw new DeprecatedException('Depricated: StorePricer:addTransactionFee');
        return self::updatePrice($orderId, StorePricerType::SHIPPING, $price, StorePricerHistory::ACTION_SYSTEM, $comment);
    }

    /**
     * add ps print price
     *
     * @param int $orderId
     * @param float $price
     * @param string $comment
     * @return bool
     * @throws \Exception
     * @deprecated
     */
    public static function addPrint($orderId, $price, $comment = '')
    {
        throw new DeprecatedException('Depricated: StorePricer:addPrint');
        return self::updatePrice($orderId, StorePricerType::PS_PRINT, $price, StorePricerHistory::ACTION_SYSTEM, $comment);
    }

    /**
     * add ps product price
     *
     * @param int $orderId
     * @param float $price
     * @param string $comment
     * @return bool
     * @throws \Exception
     * @deprecated
     */
    public static function addProductPrice($orderId, $price, $comment = '')
    {
        throw new DeprecatedException('Depricated: StorePricer:addProductPrice');
        return self::updatePrice($orderId, StorePricerType::PS_PRODUCT, $price, StorePricerHistory::ACTION_SYSTEM, $comment);
    }



    /**
     * @param $orderId
     * @param $price
     * @param string $comment
     * @return bool
     * @deprecated
     */
    public static function addPsFee($orderId, $price, $comment = '')
    {
        throw new DeprecatedException('Depricated: StorePricer:addPsFee');
        return self::updatePrice(
            $orderId,
            StorePricerType::PS_FEE,
            $price,
            StorePricerHistory::ACTION_SYSTEM,
            $comment
        );
    }

    /**
     * @param $orderId
     * @param $price
     * @param string $comment
     * @return bool
     * @throws \Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     * @deprecated
     */
    public static function addAffiliateFee($orderId, $price, $comment = '')
    {
        throw new DeprecatedException('Depricated: StorePricer:addAffiliateFee');
        return self::updatePrice(
            $orderId,
            StorePricerType::AFFILIATE_FEE,
            $price,
            StorePricerHistory::ACTION_SYSTEM,
            $comment
        );
    }

    /**
     * add ts fee price
     *
     * @param int $orderId
     * @param float $price
     * @return bool
     * @deprecated
     */
    public static function addFee($orderId, $price)
    {
        throw new DeprecatedException('Depricated: StorePricer:addFee');
        return self::updatePrice($orderId, StorePricerType::FEE, $price, StorePricerHistory::ACTION_SYSTEM);
    }
    
    /**
     * add package
     * 
     * @param int $orderId
     * @param float $price
     * @return boolean
     * @deprecated
     */
    public static function addPackage($orderId, $price)
    {
        throw new DeprecatedException('Depricated: StorePricer:addPackage');
        return self::updatePrice($orderId, StorePricerType::PACKAGE, $price, StorePricerHistory::ACTION_SYSTEM);
    }

    /**
     *
     * @param int $orderId
     * @param float $price
     * @return boolean
     * @deprecated
     */
    public static function addAssemble($orderId, $price)
    {
        throw new DeprecatedException('Depricated: StorePricer:addAssemble');
        return self::updatePrice($orderId, StorePricerType::ASSEMBLE, $price, StorePricerHistory::ACTION_SYSTEM);
    }

    /**
     *
     * @param int $orderId
     * @param float $price
     * @return bool
     * @deprecated
     */
    public static function addRefund($orderId, $price)
    {
        throw new DeprecatedException('Depricated: StorePricer:addRefund');
        return self::updatePrice($orderId, StorePricerType::REFUND, $price, StorePricerHistory::ACTION_SYSTEM);
    }

    /**
     * add tax
     *
     * @param int $orderId
     * @param float $price
     * @param string $comment
     * @return boolean
     * @deprecated
     */
    public static function addTax($orderId, $price, $comment = '')
    {
        throw new DeprecatedException('Depricated: StorePricer:addTax');
        return self::updatePrice($orderId, StorePricerType::TAX, $price, StorePricerHistory::ACTION_SYSTEM, $comment);
    }
    
    /**
     * add ps discount
     * 
     * @param int $orderId
     * @param float $price
     * @return boolean
     * @deprecated
     */
    public static function addPrintDiscount($orderId, $price)
    {
        throw new DeprecatedException('Depricated: StorePricer:addPrintDiscount');
        return self::updatePrice($orderId, StorePricerType::PS_DISCOUNT, $price, StorePricerHistory::ACTION_SYSTEM);
    }
        
  
    /**
     * add discount
     * 
     * @param int $orderId
     * @param int $price
     * @return boolean
     * @deprecated
     */
    public static function addDiscount($orderId, $price)
    {
        throw new DeprecatedException('Depricated: StorePricer:addDiscount');
        return self::updatePrice($orderId, StorePricerType::DISCOUNT, $price, StorePricerHistory::ACTION_SYSTEM);
    }

    /**
     * @param $orderId
     * @param $price
     * @return bool
     * @throws DeprecatedException
     * @throws \Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     * @deprecated
     */
    public static function addAddon($orderId, $price)
    {
        throw new DeprecatedException('Depricated: StorePricer:addAddon');
        return self::updatePrice($orderId, StorePricerType::ADDON, $price, StorePricerHistory::ACTION_SYSTEM);
    }

    /**
     * get price by order and type
     * 
     * @param int $orderId
     * @param string $type
     * @return StorePricer
     * @deprecated
     */
    public static function getPriceType($orderId, $type)
    {
        throw new DeprecatedException('Depricated: StorePricer:getPriceType');
        return self::find()->where([
            'order_id' => $orderId,
            'pricer_type' => $type
        ])->one();
    }
    
    /**
     * get print price
     * 
     * @param int $orderId
     * @return StorePricer
     * @deprecated
     */
    public static function getPrintPrice($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getPrintPrice');
        return self::getPriceByType($orderId, StorePricerType::PS_PRINT);
    }

    /**
     * get print price for PS to see
     *
     * @param $orderId
     * @return StorePricer
     * @deprecated
     */
    public static function getPrintPriceForPs($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getPrintPriceForPs');
        self::$stubZeroPrice = true;
        $printPrice = self::getPriceByType($orderId, StorePricerType::PS_PRINT);
        self::$stubZeroPrice = false;
        $psFee  = self::getPsFee($orderId);
        if ($psFee) {
            $result = new StorePricer();
            $result->price = PaymentExchangeRateConverter::roundUp($printPrice->price - $psFee->price);
            $result->currency = Currency::USD;
            $result->rate_id = $psFee->rate_id;
            $printPrice = $result;
        }
        return $printPrice;
    }

    /**
     * @param $orderId
     * @return StorePricer
     * @deprecated
     */
    public static function getAffiliateFee($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getAffiliateFee');
        $psFee  = self::getPriceByType($orderId, StorePricerType::AFFILIATE_FEE);
        return $psFee;
    }

    /**
     * @param $orderId
     * @return StorePricer
     * @throws DeprecatedException
     * @deprecated
     */
    public static function getProductPriceForPs($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getProductPriceForPs');
        self::$stubZeroPrice = true;
        $price = self::getPriceByType($orderId, StorePricerType::PS_PRODUCT);
        self::$stubZeroPrice = false;
        $psFee  = self::getPsFee($orderId);
        if ($psFee) {
            $result = new StorePricer();
            $result->price = $price->price - $psFee->price;
            $result->currency = Currency::USD;
            $result->rate_id = $psFee->rate_id;
            $price = $result;
        }
        return $price;
    }

    /**
     * @param $orderId
     * @return StorePricer
     * @throws DeprecatedException
     * @deprecated
     */
    public static function getProductPrice($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getProductPriceForPs');
        $price  = self::getPriceByType($orderId, StorePricerType::PS_PRODUCT);
        return $price;
    }

    /**
     * @param $orderId
     * @return StorePricer
     * @throws DeprecatedException
     * @deprecated
     */
    public static function getPsFee($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getPsFee');
        $psFee  = self::getPriceByType($orderId, StorePricerType::PS_FEE);
        return $psFee;
    }

    /**
     * @param $orderId
     * @return StorePricer
     * @throws DeprecatedException
     * @deprecated
     */
    public static function getShippingPrice($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getShippingPrice');
        return self::getPriceByType($orderId, StorePricerType::SHIPPING);
    }

    /**
     * @param $orderId
     * @return StorePricer
     * @throws DeprecatedException
     * @deprecated
     */
    public static function getAddonPrice($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getAddonPrice');
        return self::getPriceByType($orderId, StorePricerType::ADDON);
    }

    /**
     * @param $orderId
     * @return StorePricer
     * @deprecated
     */
    public static function getPackagePrice($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getPackagePrice');
        return self::getPriceByType($orderId, StorePricerType::PACKAGE);
    }

    /**
     * get model price from order
     * 
     * @param int $orderId
     * @return StorePricer
     * @deprecated
     */
    public static function getModelPrice($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getModelPrice');
        return self::getPriceByType($orderId, StorePricerType::MODEL);
    }

    /**
     * get product price from order
     *
     * @param int $orderId
     * @return StorePricer
     * @deprecated
     */
    public static function getModelProduct($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getModelProduct');
        return self::getPriceByType($orderId, StorePricerType::PS_PRODUCT);
    }
    
    /**
     * get fee price for given order
     * 
     * @param int $orderId
     * @return StorePricer
     * @deprecated
     */
    public static function getFeePrice($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getFeePrice');
        return self::getPriceByType($orderId, StorePricerType::FEE);
    }
    
    /**
     * get tax price for given order
     * 
     * @param int $orderId
     * @return StorePricer
     * @deprecated
     */
    public static function getTaxPrice($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getTaxPrice');
        return self::getPriceByType($orderId, StorePricerType::TAX);
    }
    
    /**
     * 
     * @param int $orderId
     * @return StorePricer
     * @deprecated
     */
    public static function getTotalPrice($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getTotalPrice');
        return self::getPriceByType($orderId, StorePricerType::TOTAL);
    }
    /**
     * get any order price by price type
     * 
     * @param string $orderId
     * @param string $type
     * @return StorePricer
     * @deprecated
     */
    private static function getPriceByType($orderId, $type)
    {
        throw new DeprecatedException('Depricated: StorePricer:getPriceByType');
        $cacheKey = 'orderprice_'.$orderId;
        if (isset(self::$runtimeCache[$cacheKey])) {
            $prices = self::$runtimeCache[$cacheKey];
        }else{
            $prices = self::getPrices($orderId);
            self::$runtimeCache[$cacheKey] = $prices;
        }
        $result = isset($prices[$type]) ? $prices[$type] : null;
        if(self::$stubZeroPrice && $result===null){
            $result = new StorePricer();
            $result->price = 0;
            $result->rate_id = 1;
            $result->currency = Currency::USD;
        }
        return $result;
    }
    
    /**
     * 
     * @param int $orderId
     * @return array
     * @deprecated
     */
    public static function getPrices($orderId)
    {
        throw new DeprecatedException('Depricated: StorePricer:getPrices');
        $prices = self::find()->joinWith('pricerType')->where([
            'order_id' => $orderId
        ])->all();
        
        $result = [];
        foreach($prices as $price){
            $result[$price->pricerType->type] = $price; //['price'=>$price['price'], 'currency' => $price['currency']];
        }
        
        return $result;
    }

    /**
     * @param $id
     * @return StorePricer
     * @deprecated
     */
    public static function getDiscount($id)
    {
        throw new DeprecatedException('Depricated: StorePricer:getDiscount');
        return self::getPriceByType($id, StorePricerType::DISCOUNT);
    }


    /**
     * @param $id
     * @return StorePricer
     * @deprecated
     */
    public static function getOrderFee($id)
    {
        throw new DeprecatedException('Depricated: StorePricer:getOrderFee');
        return self::getPriceByType($id, StorePricerType::FEE);
    }

    /**
     * @param $id
     * @return StorePricer
     * @deprecated
     */
    public static function getTransactionFee($id) : ?StorePricer
    {
        throw new DeprecatedException('Depricated: StorePricer:getTransactionFee');
        return self::getPriceByType($id, StorePricerType::TRANSACTION);
    }
    /**
     * https://www.braintreepayments.com/en-fr/braintree-pricing
     *
     * @param $total
     * @return mixed
     * @deprecated
     */
    public static function getTransactionFeeAmount($total)
    {
        throw new DeprecatedException('Depricated: StorePricer:getTransactionFeeAmount');
        return PaymentExchangeRateConverter::roundUp(($total * 0.039) + 0.3) +
            PaymentExchangeRateConverter::roundUp($total * 0.05);
    }

    /**
     * convert given pricer price to specified currency
     *
     * @param string $currency
     * @return StorePricer
     * @throws \yii\web\NotFoundHttpException
     * @deprecated
     */
    public function convertTo($currency)
    {
        throw new DeprecatedException('Depricated: StorePricer:convertTo');
        /** @var PaymentExchangeRate $xRate */
        $xRate = PaymentExchangeRate::tryFindByPk($this->rate_id);
        $priceX = $xRate->convert($this->price, $this->currency, $currency);
        $this->price = $priceX;
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return $this
     * @deprecated
     */
    public function round()
    {
        throw new DeprecatedException('Depricated: StorePricer:round');
        $this->price = PaymentExchangeRateConverter::round($this->price);
        return $this;
    }
}
