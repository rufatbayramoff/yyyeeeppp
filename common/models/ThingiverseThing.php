<?php

namespace common\models;

/**
 * Class ThingiverseThing
 *
 * @package common\models
 */
class ThingiverseThing extends \common\models\base\ThingiverseThing
{
    public function setThingiverseThingFiles($thingiverseThingFiles)
    {
        $this->populateRelation('thingiverseThingFiles', $thingiverseThingFiles);
    }

    public function getActiveThingiverseThingFiles()
    {
        return $this->getThingiverseThingFiles()->andWhere(['is_active' => 1])->all();
    }

    public function setModel3d(Model3d $model3d)
    {
        $this->populateRelation('model3d', $model3d);
        $this->model3d_id = $model3d->id;
    }
}