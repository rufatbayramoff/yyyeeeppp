<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.07.17
 * Time: 18:54
 */

namespace common\models\populators;

use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\ModelException;
use common\models\CompanyService;
use common\models\GeoCountry;
use common\models\PsMachineDelivery;
use common\models\PsMachineDeliveryCountry;
use common\models\UserLocation;
use Exception;
use frontend\models\site\DenyCountry;
use yii\base\BaseObject;
use yii\web\HttpException;

class PsMachinePopulator extends BaseObject
{
    public function populate(CompanyService $psMachine, $data)
    {
        if ($data) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $location = UserLocation::findOrCreateLocation($psMachine->ps->user, $data['location']);
                $denyIso = array_keys(DenyCountry::getDenyCountries());
                if (in_array($location->country->iso_code, $denyIso)) {
                    throw new HttpException(400, _t("site", "Unfortunately, Treatstock payment system does not work in your country due to PayPal restrictions."));
                }
                AssertHelper::assertSave($location);
                $psMachine->link('location', $location );
                $psMachine->setLocation($location);
                $psMachine->safeSave();
                $this->clearDelivery($psMachine);
                foreach ($data['deliveryTypes'] as $deliveryTypeInfo) {
                    $deliveryType = PsMachineDelivery::create(
                        $psMachine,
                        $deliveryTypeInfo
                    );
                    AssertHelper::assertSave($deliveryType);
                    if ($this->canDeliveryCountry($deliveryTypeInfo, $deliveryType)) {
                        foreach ($deliveryTypeInfo['countries'] as $country) {
                            if (!empty($country['price'])) {
                                $psMachineDeliveryCountry = $deliveryType->createDeliveryCountry(
                                    $this->findGeoCountry($country['id'])->id,
                                    $country['price']
                                );
                                AssertHelper::assertSave($psMachineDeliveryCountry);
                            }
                        }
                    }
                }
                $transaction->commit();
            } catch (Exception $exception) {
                $transaction->rollBack();
                logException($exception, 'tsdebug');
                throw new BusinessException($exception->getMessage());
            }

        }
    }

    protected function canDeliveryCountry(array $deliveryTypeData, PsMachineDelivery $delivery): bool
    {
        if (!isset($deliveryTypeData['countries'])) {
            return false;
        }

        if (!is_array($deliveryTypeData['countries'])) {
            return false;
        }

        return $delivery->canDeliveryToCountry();
    }

    /**
     * @param CompanyService $psMachine
     */
    private function clearDelivery(CompanyService $psMachine): void
    {
        $deliveryCountryIds = ArrayHelper::getColumn($psMachine->psMachineDeliveryCountry, 'id');
        if (!empty($deliveryCountryIds)) {
            PsMachineDeliveryCountry::deleteAll(['id' => $deliveryCountryIds]);
        }
        PsMachineDelivery::deleteAll(['ps_machine_id' => $psMachine->id]);
    }

    /**
     * @param $id
     * @return array|GeoCountry|mixed|\yii\db\ActiveRecord
     * @throws ModelException
     */
    protected function findGeoCountry($id)
    {
        $geoCountry = GeoCountry::find()->isEuropa()->where(['id' => $id])->one();
        if (!$geoCountry) {
            throw new ModelException("Country is not Europa");
        }
        return $geoCountry;
    }
}