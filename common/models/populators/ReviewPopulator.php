<?php

namespace common\models\populators;

use common\components\AbstractPopulator;
use common\models\StoreOrderReview;
use common\modules\thingPrint\factories\FileFactory;
use Yii;
use yii\web\UploadedFile;

class ReviewPopulator extends AbstractPopulator
{
    /**
     * @param StoreOrderReview $storeOrderReview
     * @param $data
     *
     * @return $this
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function populateAdmin(StoreOrderReview $storeOrderReview, $data): self
    {
        $formName = $storeOrderReview->formName();

        if (array_key_exists($formName, $data) && array_key_exists('files', $data[$formName])) {
            foreach ($data[$formName]['files'] as $fileUuid => $fileStatus) {
                if ($fileStatus === 'deleted') {
                    $storeOrderReview->deleteFilesUuid[] = $fileUuid;
                }
            }
            unset($data[$formName]['files']);
        }


        $data['files'] = UploadedFile::getInstances($storeOrderReview, 'files');

        if (array_key_exists('files', $data)) {
            /** @var FileFactory $fileFactory */
            $fileFactory = Yii::createObject(FileFactory::class);
            foreach ($data['files'] as $uploadFile) {
                $file = $fileFactory->createFileFromUploadedFile($uploadFile);
                $file->setOwner(StoreOrderReview::class, 'files_ids');
                $storeOrderReview->addFiles[] = $file;
            }
        }

        $storeOrderReview->load($data, $formName);

        return $this;
    }
}