<?php


namespace common\models\populators;


use common\components\AbstractPopulator;
use common\models\User;
use frontend\modules\mybusiness\models\CompanySettingForm;

class UserPopulator extends AbstractPopulator
{
    public function loadQuote(User $user,CompanySettingForm $form):void
    {
        if ($form->isFillQuotes()) {
            $user->allow_incoming_quotes = $form->allowIncomingQuotes;
        }
    }
}