<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.08.17
 * Time: 15:13
 */

namespace common\models\populators;

use common\models\PsCncMachine;
use common\modules\cnc\components\PsCncPopulator;
use yii\base\BaseObject;

class PsCncMachinePopulator extends BaseObject
{
    /** @var PsMachinePopulator */
    public $psMachinePopulator;

    /** @var  PsCncPopulator */
    public $psCncPopulator;

    public function injectDependencies(
        PsMachinePopulator $psMachinePopulator,
        PsCncPopulator $psCncPopulator
    ) {
        $this->psMachinePopulator = $psMachinePopulator;
        $this->psCncPopulator = $psCncPopulator;
    }

    /**
     * @param PsCncMachine $psCncMachine
     * @param $data
     * @return PsCncMachine
     */
    public function populate(PsCncMachine $psCncMachine, $data)
    {
        if (array_key_exists('psCnc', $data)) {
            $this->psCncPopulator->populate($psCncMachine->psCnc, $data['psCnc']);
        }
        if (array_key_exists('psMachine', $data)) {
            $this->psMachinePopulator->populate($psCncMachine->companyService, $data['psMachine']);
        }
        return $psCncMachine;
    }
}