<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.06.18
 * Time: 10:48
 */

namespace common\models\populators;

use backend\modules\company\services\CompanyService;
use common\components\AbstractPopulator;
use common\components\DateHelper;
use common\models\Company;
use common\models\factories\UserLocationFactory;
use common\models\GeoCountry;
use common\models\UserLocation;
use frontend\modules\mybusiness\models\CompanyProfileForm;
use frontend\modules\mybusiness\models\CompanySettingForm;
use frontend\modules\mybusiness\services\ImageManager;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

class CompanyPopulator extends AbstractPopulator
{
    /**
     * @param Company $company
     * @param $data
     * @throws InvalidConfigException
     */
    public function populate(Company $company, $data)
    {
        $companyForm = [];
        $formName = $company->formName();
        if (array_key_exists($formName, $data)) {
            $companyForm = $data[$formName];
        }
        if ($companyForm) {
            $this->loadAttributes($company, $companyForm);

        }
    }

    /**
     * @param Company $company
     * @param $data
     */
    public function loadAttributes(Company $company, $data)
    {
        $this->populateAttributes($company, $data, [
            'title',
            'description',
            'phone_code',
            'phone',
            'url',
            'website',
            'facebook',
            'instagram',
            'twitter',
            'ownership',
            'total_employees',
            'year_established',
            'annual_turnover',
        ]);
        if (array_key_exists('phone_with_code', $data)) {
            $company->phone = $data['phone_with_code'];
            $company->phone_status = Company::PHONE_STATUS_NEW;
        }
        if (!$company->url) {
            CompanyService::processUrl($company);
        }
    }

    /**
     * @param Company $company
     * @param $data
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function populateProtectedAttributes(Company $company, $data)
    {
        $formName = $company->formName();
        if (array_key_exists($formName, $data)) {
            $companyForm = $data[$formName];
            if ($companyForm) {
                $country = null;
                if (array_key_exists('country_iso', $companyForm) && $companyForm['country_iso']) {
                    $country = GeoCountry::tryFind(['iso_code' => strtolower($companyForm['country_iso'])]);
                    $company->populateRelation('country', $country);
                    $company->country_id = $country->id;
                }
                if (!empty($companyForm['location'])) {
                    $location = UserLocationFactory::createFromString($company->user, $companyForm['location'], $country);
                    if($location){
                        $company->populateRelation('location', $location);
                    }
                }
            }
        }
    }

    /**
     * @param Company $company
     * @param UserLocation $newLocation
     */
    public function loadLocation(Company $company, UserLocation $newLocation): void
    {
        $location = $company->location;
        if (!$location || !$newLocation->equal($location)) {
            $company->populateRelation('location', $newLocation);
            $company->setNeedModeratorApprove();
        }
    }

    public function loadSettings(Company $company, CompanySettingForm $form, array $phoneStatuses): void
    {
        [$phoneStatus, $smsGateway] = $phoneStatuses;
        if (!$company->equalPhone($form->phone, (string)$form->phone_code, $form->phone_country_iso)) {
            $company->phone = $form->phone;
            $company->phone_code = (string)$form->phone_code;
            $company->phone_country_iso = $form->phone_country_iso;
            $company->phone_status = $phoneStatus;
            $company->sms_gateway = $smsGateway;
        }
        $company->currency = $form->currency;
        $company->updated_at = DateHelper::now();
        $company->setNeedModeratorApprove();
    }

    /**
     * @param Company $company
     * @param CompanyProfileForm $form
     * @param int|null $coverId
     * @param array $fileIds
     */
    public function loadProfile(Company $company, CompanyProfileForm $form, ?int $coverId, array $fileIds): void
    {
        $company->website = $form->website;
        $company->area_of_activity = $form->area_of_activity;
        $company->ownership = $form->ownership;
        $company->incoterms = $form->getIncotermFormat();
        $company->year_established = $form->year_established;
        $company->total_employees = $form->total_employees;
        $company->annual_turnover = $form->annual_turnover;
        $company->facebook = $form->facebook;
        $company->instagram = $form->instagram;
        $company->twitter = $form->twitter;
        if ($coverId) {
            $company->cover_file_id = $coverId;
        }
        $company->picture_file_ids = Json::encode($fileIds);
        $company->updated_at = DateHelper::now();
        $company->setNeedModeratorApprove();
    }

    /**
     * @param Company $company
     * @param CompanyProfileForm $form
     * @param string $folder
     * @param ImageManager $photoManager
     */
    public function loadLogo(Company $company, CompanyProfileForm $form, string $folder, ImageManager $photoManager): void
    {
        if(!$form->logoFile){
            return;
        }
        array_map('unlink', glob($folder . '/ps_logo*'));
        $company->logo_circle = $photoManager->crop($form->logoFile, $form->circleForm, 'ps_logo_circle', $folder);
        $company->logo = $photoManager->crop($form->logoFile, $form->squareForm, 'ps_logo', $folder);
        $company->setNeedModeratorApprove();
    }

}