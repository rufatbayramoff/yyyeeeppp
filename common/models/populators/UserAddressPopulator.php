<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.07.18
 * Time: 15:43
 */

namespace common\models\populators;

use common\components\AbstractPopulator;
use common\components\DateHelper;
use common\models\GeoCountry;
use common\models\UserAddress;
use yii\base\BaseObject;

class UserAddressPopulator  extends AbstractPopulator
{
    /**
     * @param UserAddress $userAddress
     * @param $data
     * @throws \yii\base\InvalidConfigException
     */
    public function populate(UserAddress $userAddress, $data)
    {
        $addressForm = $data;
        $formName = $userAddress->formName();
        if (array_key_exists($formName, $data)) {
            $addressForm = $data[$formName];
        }
        if ($addressForm) {
            $this->loadAttributes($userAddress, $addressForm);

        }
    }

    /**
     * @param UserAddress $userAddress
     * @param $data
     */
    public function loadAttributes(UserAddress $userAddress, $data):void
    {
        $geoCountry = GeoCountry::findOne(['iso_code'=>$data['countryIso']]);
        $this->country_id = $geoCountry->id;
        $userAddress->populateRelation('country', $geoCountry);

        $this->populateAttributes($userAddress, $data, [
            'region',
            'city',
            'address',
            'extended_address',
            'lat',
            'lon',
            'zip_code',
            'company'
        ]);
        $userAddress->updated_at = DateHelper::now();
    }
}