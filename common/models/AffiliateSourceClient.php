<?php

namespace common\models;

/**
 * Class AffiliateSourceClient
 * @package common\models
 */
class AffiliateSourceClient extends \common\models\base\AffiliateSourceClient
{
    public const TIME_LEAVE_INTERVAL = 60*60*24*30*3; // 3 Month
}