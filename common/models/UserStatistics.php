<?php namespace common\models;

use Yii;
use common\components\ActiveQuery;

/**
 * This is the model class for table "user_statistics".
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserStatistics extends \common\models\base\UserStatistics
{
    const TYPE_MODEL3D = 'model3d';

    /**
     * up statistic value,
     * usage example: UserStatistics::for($userId)->up('downloads')->save();
     *
     * @param string $stat
     * @return UserStatistics
     * @throws \yii\base\Exception
     */
    public function up($stat)
    {
        if (!$this->hasAttribute($stat)) {
            throw new \yii\base\Exception("Cannot update statistic for $stat");
        }
        $this->$stat = intval($this->$stat) + 1;
        $this->updated_at = dbexpr('NOW()');
        return $this;
    }

    /**
     * down statistic value,
     * usage exampe: UserStatistics:for($userId)->up('downloads')->down('favorites')->save();
     * 
     * @param string $stat
     * @return \common\models\UserStatistics
     * @throws \yii\base\Exception
     */
    public function down($stat)
    {
        if (!$this->hasAttribute($stat)) {
            throw new \yii\base\Exception("Cannot update statistic for $stat");
        }
        $result = (int)$this->$stat - 1;
        $this->$stat = $result > 0 ? $result : 0;
        $this->updated_at = dbexpr('NOW()');
        return $this;
    }

    /**
     * increment statistic
     * 
     * @param int $userId
     * @return \common\models\UserStatistics
     */
    public static function to($userId)
    {
        $userStats = self::findOne(['user_id' => $userId]);
        if (!$userStats) {
            $userStats = new UserStatistics();
            $userStats->user_id = $userId;
        }
        $userStats->updated_at = dbexpr('NOW()');
        return $userStats;
    }

    /**
     * increment likes
     * 
     * @param string $objType
     * @param int $objId
     * @return boolean
     */
    public static function upLikes($objType, $objId)
    {
        if ($objType == 'model3d') {
            $model3d = Model3d::tryFindByPk($objId);
            self::to($model3d->user_id)->up('favorites')->safeSave();
        }
        return true;
    }

    /**
     * decrement likes
     * 
     * @param string $objType
     * @param int $objId
     * @return boolean
     */
    public static function downLikes($objType, $objId)
    {
        if ($objType == 'model3d') {
            $model3d = Model3d::tryFindByPk($objId);
            self::to($model3d->user_id)->down('favorites')->safeSave();
        }
        return true;
    }


    /**
     * Check that $who is favorite $whom
     * @param \common\models\base\User $who
     * @param \common\models\base\User $byWhom
     * @return bool
     */
    public static function isFavorite(\common\models\base\User $who, \common\models\base\User $byWhom)
    {
        if($who->id == $byWhom->id)
        {
            return false;
        }

        $userCollectionIds = UserCollection::find()
            ->select('id')
            ->forUser($byWhom)
            ->active()
            ->column();

        $d = UserCollectionModel3d::find()
            ->joinWith('model3d.productCommon')
            ->andWhere(['product_common.user_id' => $who->id])
            ->andWhere(['collection_id' => $userCollectionIds]);
        return $d->exists();
    }

    /**
     * @param User $user
     * @param int $duration
     * @return bool
     */
    public static function isOnline(User $user, $duration = 300) : bool
    {
        return UserStatistics::find()
            ->where([UserStatistics::column('user_id') => $user->id])
            ->andWhere(UserStatistics::column('lastonline_at') . " > NOW() - INTERVAL {$duration} SECOND")
            ->exists();
    }
}
