<?php

namespace common\models;

use common\interfaces\ModelHistoryInterface;
use common\traits\ModelHistoryARTrait;

class PsCncMachineHistory extends \common\models\base\PsCncMachineHistory implements ModelHistoryInterface
{
    use ModelHistoryARTrait;
}