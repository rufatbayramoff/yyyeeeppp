<?php

namespace common\models;

use common\modules\payment\services\RefundService;
use lib\money\Money;
use yii\base\UserException;

/**
 * Payment transaction
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class PaymentTransaction extends \common\models\base\PaymentTransaction
{
    /**
     * user byes service to print model
     */
    public const TYPE_PAYMENT = 'payment';

    /**
     * user requers his payment  back
     */
    public const TYPE_REFUND = 'refund';

    /**
     * user earns money from the system (printed or selled model)
     */
    public const TYPE_PLUS = 'plus';

    /**
     * user gets his money from the system
     */
    public const TYPE_MINUS = 'minus';


    /**
     * usually with type payment
     */
    public const VENDOR_BRAINTREE = 'braintree';

    /**
     * stripe
     */
    public const VENDOR_STRIPE = 'stripe';

    /**
     * stripe alipay
     */
    public const VENDOR_STRIPE_ALIPAY = 'stripe_alipay';

    /**
     * usually with type minus
     */
    public const VENDOR_PAYPAL = 'paypal';

    /**
     * usually with type plus - ts - treatstock
     */
    public const VENDOR_TS            = 'ts';
    public const VENDOR_BONUS         = 'bonus';
    public const VENDOR_THINGIVERSE   = 'thingiverse';
    public const VENDOR_BANK_TRANSFER = 'invoice';

    /**
     * used for payout money
     */
    public const VENDOR_PP_PAYOUT   = 'pp_payout';
    public const VENDOR_BANK_PAYOUT = 'bank_payout';


// ALTER TABLE `payment_invoice` ADD `paymeht_mehtods` SET('braintree','ts','thingiverse','invoice') NULL DEFAULT NULL AFTER `parent_invoice_uuid`;

    /**
     * This is all possible used statuses by different vendors
     */
    public const STATUS_AUTHORIZED               = 'authorized';
    public const STATUS_AUTHORIZED_EXPIRED       = 'authorization_expired';
    public const STATUS_REQUIRES_CAPTURE         = 'requires_capture';
    public const STATUS_PENDING                  = 'pending';
    public const STATUS_REFUND_REQUEST           = 'refund_request';
    public const STATUS_REFUNDED                 = 'refunded';
    public const STATUS_CANCELED                 = 'canceled'; // need if payout_transaction was manually cancelled
    public const STATUS_FAILED                   = 'failed';
    public const STATUS_PAID                     = 'paid';
    public const STATUS_SUCCEEDED                = 'succeeded';
    public const STATUS_VOIDED                   = 'voided';
    public const STATUS_UP_FAILED                = 'FAILED';
    public const STATUS_UP_ONHOLD                = 'ONHOLD';
    public const STATUS_UP_SUCCESS               = 'SUCCESS';
    public const STATUS_UP_UNCLAIMED             = 'UNCLAIMED';
    public const STATUS_UP_RETURNED              = 'RETURNED';
    public const STATUS_PROCESSING               = 'processing';
    public const STATUS_REQUIRES_ACTION          = 'requires_action';
    public const STATUS_SETTLING                 = 'settling';
    public const STATUS_SETTLED                  = 'settled';
    public const STATUS_SUBMITTED_FOR_SETTLEMENT = 'submitted_for_settlement';
    public const STATUS_SETTLEMENT_PENDING       = 'settlement_pending';
    public const STATUS_SETTLE_DECLINE           = 'settlement_declined';
    public const STATUS_REQUIRES_CONFIRMATION    = 'requires_confirmation';
    public const STATUS_REQUIRES_PAYMENT_METHOD  = 'requires_payment_method';

    protected static $types = ['payment', 'refund', 'plus', 'minus'];
    protected static $vendors = [
        self::VENDOR_BRAINTREE,
        self::VENDOR_STRIPE,
        self::VENDOR_PAYPAL,
        self::VENDOR_TS,
        self::VENDOR_BONUS,
        self::VENDOR_THINGIVERSE
    ];

    protected static $vendorToUserId = [
        self::VENDOR_BRAINTREE   => User::USER_BRAINTREE,
        self::VENDOR_STRIPE      => User::USER_STRIPE,
        self::VENDOR_PAYPAL      => User::USER_PAYPAL,
        self::VENDOR_TS          => User::USER_TS,
        self::VENDOR_BONUS       => User::USER_TS,
        self::VENDOR_THINGIVERSE => User::USER_THINGIVERSE,
    ];

    protected static $vendorStatuses = [
        self::VENDOR_BRAINTREE   => [
            self::STATUS_AUTHORIZED_EXPIRED,
            self::STATUS_AUTHORIZED,
            self::STATUS_FAILED,
            self::STATUS_REFUNDED,
            self::STATUS_SETTLED,
            self::STATUS_SETTLING,
            self::STATUS_SETTLE_DECLINE,
            self::STATUS_VOIDED
        ],
        self::VENDOR_STRIPE      => [
            self::STATUS_REQUIRES_CAPTURE,
        ],
        self::VENDOR_PAYPAL      => [
            self::STATUS_CANCELED,
            self::STATUS_UP_FAILED,
            self::STATUS_UP_ONHOLD,
            self::STATUS_PENDING,
            self::STATUS_UP_SUCCESS,
            self::STATUS_UP_UNCLAIMED,
        ],
        self::VENDOR_THINGIVERSE => [
            self::STATUS_AUTHORIZED,
            self::STATUS_REFUNDED,
            self::STATUS_SETTLED,
            self::STATUS_VOIDED,
        ],
        self::VENDOR_TS          => [
            self::STATUS_AUTHORIZED,
            self::STATUS_SETTLED,
            self::STATUS_VOIDED,
        ],
        self::VENDOR_BONUS       => [
            self::STATUS_AUTHORIZED,
            self::STATUS_SETTLED,
            self::STATUS_VOIDED,
        ],
    ];

    /**
     * Change payment transaction status
     *
     * @param int $paymentId
     * @param string $status
     */
    public static function changeStatus($paymentId, $status)
    {
        self::updateRecord($paymentId, ['status' => $status]);
    }

    /**
     *
     * @param array $data
     * @return \yii\db\ActiveRecord
     * @throws UserException
     */
    public static function addTransaction($data)
    {
        if (empty($data['transaction_id'])) {
            $data['transaction_id'] = '-';
        }
        self::validateTransaction($data);

        $data['created_at'] = dbexpr('NOW()');
        return self::addRecord($data);
    }

    public function canBeCanceled()
    {
        if (in_array($this->status, [self::STATUS_AUTHORIZED, self::STATUS_REQUIRES_CAPTURE])) {
            return true;
        }
        return false;
    }

    public function canBeSettle()
    {
        if (in_array($this->status, [self::STATUS_AUTHORIZED, self::STATUS_REQUIRES_CAPTURE])) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function canBeRefunded()
    {
        /** @var RefundService $refundService */
        $refundService = \Yii::createObject(RefundService::class);
        if ($refundService->canRefundByModerator($this)) {
            return true;
        }
        return false;
    }

    public function isPayed()
    {
        return in_array($this->status, [self::STATUS_SETTLED, self::STATUS_AUTHORIZED, self::STATUS_PAID]);
    }

    /**
     * validate payment transaction before save
     *
     * @param array $data
     * @return boolean
     * @throws UserException
     */
    private static function validateTransaction($data)
    {
        if (!$data['user_id']) {
            throw new UserException(_t('site.payment', "Transaction cannot be created."));
        }
        if (!isset($data['amount'])) {
            throw new UserException(_t('site.payment', 'No amount') . var_export($data, true));
        }
        if (empty($data['currency'])) {
            throw new UserException(_t('site.payment', 'No currency specified'));
        }
        if (empty($data['transaction_id'])) {
            throw new UserException(_t('site.payment', 'No transaction id specified'));
        }
        if (empty($data['vendor']) || !in_array($data['vendor'], self::$vendors)) {
            throw new UserException(_t('site.payment', 'No vendor') . ' ' . $data['vendor']);
        }
        if (empty($data['type']) || !in_array($data['type'], self::$types)) {
            throw new UserException(_t('site.payment', 'Not valid transaction type'));
        }
        if ($data['type'] == 'payment' && empty($data['order_id'])) {
            throw new UserException(_t('site.payment', 'This type of payment cannot be added without order'));
        }
        return true;
    }


    public function getMoneyAmount()
    {
        return Money::create($this->amount, $this->currency);
    }

    public function isBankPayoutAuthorized()
    {
        return ($this->status === PaymentTransaction::STATUS_AUTHORIZED) && ($this->vendor === PaymentTransaction::VENDOR_BANK_PAYOUT);
    }

    /**
     * @return bool|string
     */
    public function findBillInfo()
    {
        $result = false;

        if ($this->vendor === static::VENDOR_BRAINTREE && !empty($this->last_original_transaction)) {
            preg_match('#cardType=(.+?),.+maskedNumber=(.+?)[\],]#si', $this->last_original_transaction, $matchesCardType);

            if (!empty($matchesCardType)) {
                $cardType = $matchesCardType[1];
                $numbers  = '******' . substr($matchesCardType[2], 6);

                if (strpos($cardType, ',') !== false) {

                    $cardType = 'PayPal Account';
                } // if not specified - it's paypal

                $result = $cardType . ' ' . $numbers;
            } else {
                preg_match('#cardType["=]\;s:\d+?\:"(.+?)".+?maskedNumber";s:\d+:"(\d+\**?\d+)#si', $this->last_original_transaction, $matchesCardType);
                if (!empty($matchesCardType)) {
                    $cardType = $matchesCardType[1];
                    $numbers  = '******' . substr($matchesCardType[2], 6);
                    if (!empty($cardType)) {
                        $result = $cardType . ' ' . $numbers;
                    } else {
                        $result = 'PayPal Account';

                    }
                }
            }
            //
        }
        if ($this->vendor === static::VENDOR_TS) {
            $result = _t('payment.receipt', 'Treatstock Balance #{id}', ['id' => $this->user_id]);
        }
        if ($this->vendor === static::VENDOR_BONUS) {
            $result = _t('payment.receipt', 'Bonus Balance #{id}', ['id' => $this->user_id]);
        }

        return $result;
    }
}