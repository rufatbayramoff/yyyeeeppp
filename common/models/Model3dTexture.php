<?php

namespace common\models;

use common\components\Model3dHistoryConfigurator;

/**
 * Model3dTexture is combination of printer material and printer color as a one description of model3dPart texture.
 *
 * @package common\models
 */
class Model3dTexture extends \common\models\base\Model3dTexture
{
    public $isMarkForDelete;
    protected $texture = null;


    public function behaviors()
    {
        return [
            'Model3dHistory' => Model3dHistoryConfigurator::getConfig(self::class),
        ];
    }

    public function setTexture(Model3dTexture $texture)
    {
        $this->populateRelation('printerMaterial', $texture->printerMaterial);
        $this->populateRelation('printerMaterialGroup', $texture->printerMaterialGroup);
        $this->populateRelation('printerColor', $texture->printerColor);
        $this->infill = $texture->infill;
    }

    public function calculatePrinterMaterialGroup()
    {
        if ($this->printerMaterial) {
            return $this->printerMaterial->group;
        }
        $group = $this->printerMaterialGroup;
        if ($group) {
            return $group;
        }
        return null;
    }

    public function isCustomInfill()
    {
        return (bool)$this->infill;
    }

    public function calculateInfill()
    {
        if ($this->infill) {
            return $this->infill;
        }
        return $this->calculatePrinterMaterialGroup() ? $this->calculatePrinterMaterialGroup()->fill_percent : null;
    }

    public function infillText()
    {
        $currentInfill = $this->calculateInfill();
        $defaultInfill = $this->calculatePrinterMaterialGroup()->fill_percent;
        if ($currentInfill === $defaultInfill) {
            $infill = '<span>'.$currentInfill .'%</span>'. ' (' . _t('site.common', 'default') . ')';
            return $infill;
        }
        $infill = '<span style="color: #ff0000">' .$currentInfill.'%</span>';
        return $infill;
    }

    /**
     * @param PrinterMaterialGroup $printerMaterialGroup
     */
    public function setPrinterMaterialGroup(PrinterMaterialGroup $printerMaterialGroup = null)
    {
        if ($printerMaterialGroup == null) {
            $this->populateRelation('printerMaterialGroup', null);
            $this->printer_material_group_id = null;
            return;
        }

        if (!$this->calculatePrinterMaterialGroup() || ($this->calculatePrinterMaterialGroup()->id !== $printerMaterialGroup->id)) {
            $this->populateRelation('printerMaterial', null);
            $this->printer_material_id = null;
        }
        $this->populateRelation('printerMaterialGroup', $printerMaterialGroup);
    }


    /**
     * Check texture values
     *  infill value
     */
    public function checkTexture()
    {
        if ($this->infill) {
            $currentGroup = $this->calculatePrinterMaterialGroup();
            if (!$currentGroup->fill_percent_max) {
                $this->infill = null;
            } else {
                if ($currentGroup->fill_percent_max < $this->infill) {
                    $this->infill = $currentGroup->fill_percent_max;
                }
                if ($currentGroup->fill_percent > $this->infill) {
                    $this->infill = $currentGroup->fill_percent;
                }
            }
        }
    }

    /**
     * @param PrinterMaterial|null $printerMaterial
     */
    public function setPrinterMaterial($printerMaterial)
    {
        $this->populateRelation('printerMaterial', $printerMaterial);
    }

    /**
     * @param PrinterColor $printerColor
     */
    public function setPrinterColor(PrinterColor $printerColor)
    {
        $this->populateRelation('printerColor', $printerColor);
    }

    public function beforeSaveMethod($insert)
    {
        if ($this->printerMaterialGroup && (($this->printer_material_group_id !== $this->printerMaterialGroup->id) || ($this->printer_material_group_id === null))) {
            $this->printer_material_group_id = $this->printerMaterialGroup->id;
        } elseif ($this->printerMaterialGroup === null) {
            $this->printer_material_group_id = null;
        }
        if ($this->printerMaterial && (($this->printer_material_id !== $this->printerMaterial->id) || ($this->printer_material_id === null))) {
            $this->printer_material_id = $this->printerMaterial->id;
        } elseif ($this->printerMaterial === null) {
            $this->printer_material_id = null;
        }
        if ($this->printerColor && (($this->printer_color_id !== $this->printerColor->id) || ($this->printer_color_id === null))) {
            $this->printer_color_id = $this->printerColor->id;
        } elseif ($this->printerColor === null) {
            $this->printer_color_id = null;
        }
        return parent::beforeSaveMethod($insert);
    }

    /**
     * Return title of material or title of materailGroup.
     * If it not present - return null;
     *
     * @return null|string
     */
    public function getMaterialFilamentOrGroupTitle(): ?string
    {
        if ($this->printerMaterial) {
            return $this->printerMaterial->title;
        }

        if ($this->printerMaterialGroup) {
            return $this->printerMaterialGroup->title;
        }

        return null;
    }

    public function delete()
    {
        return parent::delete(); // TODO: Change the autogenerated stub
    }
}