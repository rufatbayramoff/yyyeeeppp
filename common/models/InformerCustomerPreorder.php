<?php

namespace common\models;

use common\modules\informer\interfaces\InformerStorageInterface;
use common\modules\informer\models\CustomerQuoteInformer;

/**
 * Class InformerCustomerPreorder
 * @package common\models
 */
class InformerCustomerPreorder extends \common\models\base\InformerCustomerPreorder implements InformerStorageInterface
{
    /**
     * @var string
     */
    public $type = 'customer_quote';

    /**
     * STUB, not using
     * @var array
     */
    public $paths = [];

    public $parts = [];

    public function getInformerCLass()
    {
        return CustomerQuoteInformer::class;
    }

    public function saveInformer()
    {
        self::deleteAll(['key'=>$this->key]);
        foreach ($this->parts as $partKey => $partInfo) {
            $informerStored = new self();
            $informerStored->setAttributes($this->attributes);
            $informerStored->part_key = $partKey;
            $informerStored->part_info = $partInfo;
            $informerStored->save();
        }
    }

    public static function deleteByKey($key)
    {
        self::deleteAll(['key'=>$key]);
    }

    public static function getByKey($key)
    {
        $informersStored = self::findAll(['key'=>$key]);
        if (!$informersStored) {
            return null;
        }
        /** @var InformerCustomerPreorder $informerStored */
        $informerStored = reset($informersStored);
        $informerStored->parts[$informerStored->part_key] = $informerStored->part_info;
        foreach ($informersStored as $oneStored) {
            $informerStored->parts[$oneStored->part_key] = $oneStored->part_info;
        }
        return $informerStored;
    }
}