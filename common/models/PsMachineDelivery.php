<?php

namespace common\models;

use common\components\ArrayHelper;
use common\components\ModelException;
use common\components\PaymentExchangeRateFacade;
use lib\money\Currency;
use yii\validators\NumberValidator;

/**
 * Class PsMachineDelivery
 *
 * @package common\models
 */
class PsMachineDelivery extends \common\models\base\PsMachineDelivery
{
    const SCENARIO_UPDATE = 'update';

    const RELATION_PS_PRINTER = 'psPrinter';

    const DELIVERY_DETAILS = ['carrier', 'carrier_price', 'free_delivery'];

    public function behaviors()
    {
        return [
            'history' => [
                'class'          => \common\components\HistoryBehavior::className(),
                'historyClass'   => PsPrinterHistory::className(),
                'foreignKey'     => 'ps_printer_id',
                'ownerIdField'   => 'psPrinterId',
                'actionCode'     => 'update_delivery',
                'skipAttributes' => ['updated_at', 'created_at']
            ],
        ];
    }

    public function getPsCncService()
    {
        throwException('Not realized');
    }

    public function getPsPrinterId()
    {
        return $this->psPrinter->id;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['carrier', 'required'],
            ['carrier_price', 'validateMyselfCarrierPrice'],
            ['free_delivery', 'validateFreeDeliveryPrice'],
            ['comment', 'validateComment', 'skipOnEmpty' => false],
        ]);
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_UPDATE => ['carrier', 'carrier_price', 'free_delivery', 'packing_price', 'comment'],
        ]);
    }

    /**
     *
     * @param $attribute
     */
    public function validateFreeDeliveryPrice($attribute)
    {
        if (!$this->deliveryType->is_free_delivery || !$this->free_delivery) {
            return;
        }

        // validate for numberic
        $minFreeDeliveryPrice = $this->deliveryType->getMinFreeDeliveryPriceInUSD();
        $maxFreeDeliveryPrice = 1000000;

        $validator = new NumberValidator([
            'min'      => $minFreeDeliveryPrice,
            'max'      => $maxFreeDeliveryPrice,
            'message'  => _t("site.delivery", "Free delivery price for \"{title}\" must be number", ['title' => $this->deliveryType->title]),
            'tooSmall' => _t("site.delivery", "Free delivery price for \"{title}\" couldn't be less than {minPrice}", ['title' => $this->deliveryType->title, 'minPrice' => displayAsCurrency($minFreeDeliveryPrice, Currency::USD)]),
            'tooBig'   => _t("site.delivery", "Free delivery price for \"{title}\" couldn't be more than {maxPrice}", ['title' => $this->deliveryType->title, 'maxPrice' => displayAsCurrency($maxFreeDeliveryPrice, Currency::USD)]),
        ]);

        if (!$validator->validate($this->free_delivery, $error)) {
            $this->addError($attribute, $error);
        }
    }

    /**
     * Validate carrier price then carrier is myself
     * @param $attribute
     */
    public function validateMyselfCarrierPrice($attribute)
    {
        if ($this->deliveryType->code == DeliveryType::PICKUP) {
            return;
        }
        if ($this->carrier == DeliveryType::CARRIER_MYSELF && (!$this->carrier_price || $this->carrier_price <= 0)) {
            $this->addError($attribute, _t('site.ps', 'Shipping cost must more 0'));
        }
    }

    /**
     * @param $attribute
     */
    public function validateComment($attribute)
    {
        if ($this->deliveryType->code == DeliveryType::PICKUP && !$this->comment) {
            $this->addError($attribute, _t('site.ps', 'Please fill out Working hours'));
        }
    }

    public function getPsPrinter()
    {
        return $this->hasOne(PsPrinter::class, ['id' => 'ps_printer_id'])->via('psMachine');
    }


    public function setPsMachine($psMachine)
    {
        $this->populateRelation('companyService', $psMachine);
    }

    /**
     * @param CompanyService $psMachine
     * @param array $data
     * @return static
     */
    public static function create(CompanyService $psMachine, array $data): self
    {
        $model                = new self;
        $model->ps_machine_id = $psMachine->id;
        $model->setAttributes($data);
        return $model;
    }

    /**
     * @param $countryId
     * @param $price
     * @return PsMachineDeliveryCountry
     */
    public function createDeliveryCountry($countryId, $price): PsMachineDeliveryCountry
    {
        return PsMachineDeliveryCountry::create(
            $countryId,
            $price,
            $this->id
        );
    }

    public function canDeliveryToCountry(): bool
    {
        return
            $this->delivery_type_id === DeliveryType::INTERNATIONAL_STANDART_ID &&
            $this->carrier == DeliveryType::CARRIER_MYSELF;
    }

    public function isTsDelivery(): bool
    {
        return $this->carrier === DeliveryType::CARRIER_TS;
    }

    public function isDomesticDelivery(): bool
    {
        return $this->delivery_type_id === DeliveryType::STANDART_ID;
    }

    public function isInternationalDelivery(): bool
    {
        return $this->delivery_type_id === DeliveryType::INTERNATIONAL_STANDART_ID;
    }
}