<?php

namespace common\models;

use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBaseInterface;
use common\models\model3d\Model3dBaseImg;

/**
 * Class Model3dReplicaImg
 *
 * @package common\models
 */
class Model3dReplicaImg extends \common\models\base\Model3dReplicaImg implements Model3dBaseImgInterface
{
    use Model3dBaseImg;

    public const UID_PREFIX = 'RI:';

    public function setAttachedModel3d(Model3dBaseInterface $model3d): void
    {
        $this->populateRelation('model3d', $model3d);
        $this->populateRelation('model3dReplica', $model3d);
        $this->model3d_id = $model3d->id;
    }

    public function getAttachedModel3d(): Model3dBaseInterface
    {
        return $this->model3d;
    }

    /**
     * @return Model3dBaseImgInterface
     */
    public function getOriginalModel3dImg(): Model3dBaseImgInterface
    {
        return parent::getOriginalModel3dImg()->one();
    }

    public function setOriginalModel3dImg(Model3dBaseImgInterface $originalModel3dImg)
    {
        $this->populateRelation('originalModel3dImg', $originalModel3dImg);
    }
}