<?php

namespace common\models;

use common\modules\product\interfaces\ProductInterface;
use lib\money\Currency;
use lib\money\Money;

/**
 * Class ProductSnapshot
 *
 * @package common\models
 */
class ProductSnapshot extends \common\models\base\ProductSnapshot implements ProductInterface
{
    public function getTitle(): string
    {
        return $this->description['product']['title'];
    }

    /**
     * @return User
     * @throws \yii\web\NotFoundHttpException
     */
    public function getAuthor(): ?User
    {
        return User::tryFindByPk($this->description['product']['userId']);
    }

    public function getUpdatedAt(): string
    {
        return $this->created_at;
    }

    public function getModeratedAt(): string
    {
        return $this->created_at;
    }

    public function getPublishedAt(): string
    {
        return $this->created_at;
    }

    public function isPublished(): bool
    {
        return false;
    }

    public function isActive(): bool
    {
        return true;
    }

    public function getProductStatus(): string
    {
        return $this->description['product']['productStatus'];
    }

    /**
     * Get human readable status
     *
     * @return string
     */
    public function getProductStatusLabel(): string
    {
        return $this->description['product']['productStatusLabel'];
    }

    /**
     * Disable publis_updated allow status
     */
    public function setCantBePublisehedUpdated(): void
    {
    }

    /**
     * Analyze product changed attibutes, for allow save product as published_updated
     *
     * @return bool
     */
    public function isCanBePublishUpdated(): bool
    {
        return false;
    }

    public function isAvailableInCatalog(): bool
    {
        return false;
    }

    /**
     * @return string
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function getCoverUrl(): string
    {
        $images     = $this->description['product']['images'];
        $firstImage = reset($images);
        if ($firstImage) {
            $file = $this->getFileFromSnapshot($firstImage['uuid']);
            return $file ? $file->getFileUrl() : '';
        }
        return '';
    }

    /**
     * @return File[]
     */
    public function getImages(): array
    {
        $files = [];
        foreach ($this->description['product']['images'] as $image) {
            $files[] = $this->getFileFromSnapshot($image['uuid']);
        }
        return $files;
    }

    public function amountWithCurrency(): string
    {
        return '$'.$this->description['product']['singlePrice'];
    }

    public function getDescription(): string
    {
        return $this->description['product']['description'];
    }

    /**
     * @return SiteTag[]
     * @throws \yii\web\NotFoundHttpException
     */
    public function getProductTags(): array
    {
        $siteTags = [];
        foreach ($this->description['product']['productTags'] as $productTag) {
            $siteTags[] = SiteTag::tryFindByPk($productTag['id']);
        }
        return $siteTags;
    }

    public function getProductType(): string
    {
        return ProductInterface::TYPE_PRODUCT;
    }

    public function getUuid(): string
    {
        return $this->attributes['uuid'];
    }

    public function getPublicPageUrl(array $params = []): string
    {
        return $this->product->getPublicPageUrl($params);
    }

    /**
     * @param $uuid
     * @return File|null
     */
    public function getFileFromSnapshot($uuid)
    {
        foreach ($this->productSnapshotFiles as $productSnapshotFile) {
            if ($productSnapshotFile->file_uuid === $uuid) {
                return $productSnapshotFile->file;
            }
        }
        return null;
    }

    public function getUnitType()
    {
        return $this->description['product']['unitType'];
    }

    public function getUnitTypeLabel($qty)
    {
        $unitType = $this->description['product']['unitType'];
        $titles   = Product::getUnitTypesList($qty > 1);
        return $titles[$unitType] ?? $unitType;
    }

    /**
     * @param $qty
     * @return Money|null
     */
    public function getPriceMoneyByQty($qty): ?Money
    {
        $price  = Product::getPriceByQty($this->description['product']['productPrices'], $qty);
        $currency = Currency::USD;
        if (!empty($this->description['product']['currency'])) {
            $currency = $this->description['product']['currency'];
        }
        $retVal = $price === null ? null : Money::create($price, $currency);
        return $retVal;
    }

    public function getCuid(): string
    {
        return $this->product->cuid;
    }

    public function getCompanyManufacturer(): ?Company
    {
        return $this->product->company;
    }

    public function getMoqFromPriceMoney(): ?Money
    {
        return $this->getPriceMoneyByQty(1);
    }

    public function getMinOrderQty(): float
    {
        return $this->product->getMinOrderQty();
    }

    public function getUnitTypeTitle($qty): string
    {
        return $this->getUnitTypeLabel($qty);
    }

    public function getUnitTypeCode(): string
    {
        return $this->getUnitType();
    }
}