<?php

namespace common\models;

use common\components\exceptions\DeprecatedException;
use common\components\exceptions\InvalidModelException;
use common\models\query\PaymentDetailOperationQuery;
use common\models\query\PaymentTransactionRefundQuery;
use common\modules\payment\services\PaymentService;
use frontend\models\user\UserFacade;
use lib\money\Money;
use Yii;

/**
 * Payment detail
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 *
 * @property \common\models\Payment $payment
 * @property \common\models\PaymentTransactionRefund $paymentTransactionRefund
 */
class PaymentDetail extends \common\models\base\PaymentDetail
{
    public const STATUS_NEW        = 'new';
    public const STATUS_PROCESSING = 'processing';
    public const STATUS_FAILED     = 'failed';
    public const STATUS_PAID       = 'paid';

    /**
     * Affiliate payment
     *
     */
    public const TYPE_AFFILIATE = 'affiliate';

    /**
     * any other internal payments, like payment to taxagent or another service
     *
     */
    public const TYPE_PAYMENT = 'payment';

    /**
     * user income/outcome - payment for order
     */
    public const TYPE_ORDER = 'order';

    /**
     * refund request for order
     */
    public const TYPE_REFUND_REQUEST = 'refund_request';

    /**
     * refund for order
     */
    public const TYPE_REFUND = 'refund';

    /**
     * user outcome - payout request
     */
    public const TYPE_PAYOUT = 'payout';

    /**
     * user income - awarded for printing or 3d model
     */
    public const TYPE_AWARD_PRINT = 'award_print';

    /**
     *
     */
    public const TYPE_AWARD_MODEL = 'award_model';

    /**
     * tax
     */
    public const TYPE_TAX = 'tax';

    public const TYPE_CORRECTION = 'correction';

    /**
     * award for preorder and additional services
     */
    public const TYPE_AWARD_MANUFACTURER = 'award_manufacturer';

    /**
     * get array of types with titles
     *
     * @return array
     */
    public static function getTypesWithTitles(): array
    {
        return [
            self::TYPE_AWARD_PRINT => _t('site.payments', 'Payment'),
            self::TYPE_AWARD_MODEL => _t('site.payments', 'Payment'),
            self::TYPE_PAYOUT      => _t('site.payments', 'Payout'),
            self::TYPE_TAX         => _t('site.payments', 'Tax'),
            self::TYPE_PAYMENT     => _t('site.payments', 'Payment'),
        ];
    }

    /*
     * This is fix for dates formed by migrations
     */
    public function getFormDate(): string
    {
        return $this->updated_at;
    }

    /**
     * get current type title
     *
     * @return mixed|string
     */
    public function getTypeTitle()
    {
        $types = self::getTypesWithTitles();
        return isset($types[$this->type]) ? $types[$this->type] : $this->type;
    }

    /**
     * get payment_detail types for which tax should be calculated and checked
     *
     * @return array
     * @deprecated
     */
    public static function getTypesNeedTax()
    {
        throw new DeprecatedException('Depricated: getTypesNeedTax()');
        return [
            self::TYPE_AWARD_PRINT,
            self::TYPE_AWARD_MODEL
        ];
    }

    /**
     * @return array
     * @deprecated
     */
    public static function getTypesIncome()
    {
        throw new DeprecatedException('Depricated: getTypesIncome()');
        return [
            self::TYPE_PAYMENT,
            self::TYPE_AWARD_MODEL,
            self::TYPE_AWARD_PRINT
        ];
    }

    /**
     * @return Money
     */
    public function getMoneyAmount(): Money
    {
        return Money::create($this->amount, $this->original_currency);
    }

    public function getPayment()
    {
        return $this->hasOne(Payment::class, ['id' => 'payment_id'])->via('paymentDetailOperation');
    }

    /**
     * You should use only this function to get Vendor Transaction
     *
     * @throws InvalidModelException
     */
    public function getVendorTransactionHistory(): ?PaymentTransactionHistory
    {
        $historyTransactions = $this->paymentTransactionHistories;
        $transactionHistory = reset($historyTransactions);
        if ($firstPaymentTransaction = $this->paymentTransaction) {
            if (!$transactionHistory) {
                throw new InvalidModelException($this, 'Transaction history not exists, but transaction link exists.');
            }
            if ($transactionHistory->transaction->id !== $firstPaymentTransaction->id) {
                // Payment transaction should be equal
                throw new InvalidModelException($transactionHistory, 'Database error payment transaction links do not match.');
            }
        }
        return $transactionHistory ?: null;
    }

    public function getPaymentId(): int
    {
        return $this->paymentDetailOperation->payment_id;
    }

    /**
     * Flag get money from
     */
    public function isTypeFrom(): bool
    {
        return $this->amount < 0;
    }

    /**
     * Flag get money to
     */
    public function isTypeTo(): bool
    {
        return $this->amount > 0;
    }

    public function getDescriptionWithoutTags()
    {
        return strip_tags($this->description);
    }

    public function getResultAmount(): Money
    {
        $paymentService = \Yii::createObject(PaymentService::class);
        return $paymentService->calculatePaymentDetailBalanceResult($this);
    }

    /**
     * @return PaymentDetailOperationQuery
     */
    public function getPaymentDetailOperation()
    {
        return $this->hasOne(PaymentDetailOperation::class, ['uuid' => 'payment_detail_operation_uuid']);
    }

    /**
     * @return PaymentTransactionRefundQuery
     */
    public function getPaymentTransactionRefund()
    {
        return $this->hasOne(PaymentTransactionRefund::class, ['transaction_refund_id' => 'id'])->via('paymentTransaction');
    }
}