<?php

namespace common\models;

/**
 * Class ThingiverseReport
 * @package common\models
 *
 * @property \common\models\ThingiverseOrder $thingiverseOrder
 */
class ThingiverseReport extends \common\models\base\ThingiverseReport
{

    public function getThingiverseOrder()
    {
        return $this->hasOne(ThingiverseOrder::class, ['thingiverse_order_id' => 'order_id']);
    }

    public function isDiffStatus()
    {
        $localstatus = $this->getLocalOrderThingiverseStatus();
        $thingOrderStatus = $this->thingiverseOrder->status ?? '';
        if ($this->status !== $localstatus) {
            if ($localstatus === 'Shipped' && $thingOrderStatus === 'Shipped') {
                // Status not updated yet
                return false;
            }
            if ($this->status === 'Placed' && $localstatus === 'Refunded' && $thingOrderStatus === 'Refunded') {
                // Status not updated yet
                return false;
            }
            if ($localstatus === 'Completed' && ($this->status === 'Shipped' || $thingOrderStatus === 'Shipped')) {
                // Status not updated yet
                return false;
            }
            if ($localstatus === 'Completed' && ($this->status === 'Placed')) {
                // Order was removed from peding orders, and not dowloaded
                return false;
            }

            if ($this->status === 'Disputed' && ($localstatus === 'Refunded')) {
                // Same status?
                return false;
            }
            return true;
        };
        return false;
    }

    public function getLocalOrderThingiverseStatus()
    {
        if ($this->thingiverseOrder && $this->thingiverseOrder->order) {
            $order = $this->thingiverseOrder->order;
            $invoice = $order->getPrimaryInvoice();
            if ($invoice->isRefunded() || $invoice->isCanceled()) {
                return 'Refunded';
            }
            if ($order->currentAttemp && in_array($order->currentAttemp->status, [StoreOrderAttemp::STATUS_SENT, StoreOrderAttemp::STATUS_DELIVERED])) {
                return 'Shipped';
            }
            if ($order->currentAttemp && $order->currentAttemp->status === StoreOrderAttemp::STATUS_RECEIVED) {
                return 'Completed';
            }
            return 'Placed';

        };
        return '';
    }
}