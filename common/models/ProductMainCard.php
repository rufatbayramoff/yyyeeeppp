<?php

namespace common\models;

/**
 * Class ProductMainCard
 * @package common\models
 */
class ProductMainCard extends \common\models\base\ProductMainCard
{
    public const FEATURE = 'feature';
    public const MACHINE = 'machine';
    public const MATERIAL = 'material';

    /**
     * @param $uuid
     * @return ProductMainCard
     */
    public static function createProduct($uuid, $type): ProductMainCard
    {
        $model = new self;
        $model->product_common_uid = $uuid;
        $model->type = $type;
        return $model;
    }

    public function isMachine()
    {
        return $this->type === self::MACHINE;
    }

    public function isFeature()
    {
        return $this->type === self::FEATURE;
    }

    public function isMaterial()
    {
        return $this->type === self::MATERIAL;
    }
}