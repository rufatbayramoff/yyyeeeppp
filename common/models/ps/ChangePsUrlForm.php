<?php
/**
 * Created by mitaichik
 */

namespace common\models\ps;


use common\components\BaseForm;
use common\components\exceptions\AssertHelper;
use common\models\Ps;
use yii\helpers\Inflector;

/**
 * Class ChangeUrlForm
 * @package common\models\ps
 */
class ChangePsUrlForm extends BaseForm
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var Ps
     */
    private $ps;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['url', function(){ $this->url = Inflector::slug($this->url);}],
            ['url', 'string', 'length' => [0, 255]],
            ['url', 'required'],
            ['url', 'validateUnique']

        ];
    }

    /**
     * ChangeUrlForm constructor.
     * @param Ps $ps
     */
    public function __construct(Ps $ps)
    {
        parent::__construct();
        $this->ps = $ps;
    }

    /**
     * Validate that url is unique
     */
    public function validateUnique()
    {
        $exist = Ps::find()->where(['or', ['url' => $this->url], ['url_old' => $this->url]])->andWhere(['!=', 'id', $this->ps->id])->exists();
        if ($exist) {
            $this->addError('url', _t('site.ps', 'Url already taken'));
        }
    }

    /**
     * Change url for print service
     */
    public function process()
    {
        if($this->ps->url == $this->url) {
            return;
        }

        AssertHelper::assert($this->ps->canChangeUrl(), 'Cant change ps url');
        $this->ps->url_old = $this->ps->url;
        $this->ps->url = $this->url;
        $this->ps->url_changes_count++;
        AssertHelper::assertSave($this->ps);
    }
}