<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\models\ps\printers\operations;

use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\operations\Operation;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\PsPrinter;
use common\models\StoreOrderAttemp;
use common\services\PsPrinterService;
use frontend\models\ps\PsFacade;

/**
 * Delete printer operation
 * @package common\models\ps\printers\operations
 */
class DeleteOperation extends Operation
{
    private $psPrinter;

    /**
     * @param PsPrinter $printer
     */
    function __construct(PsPrinter $printer)
    {
        $this->psPrinter = $printer;
    }

    /**
     * @return bool
     */
    protected function checkErrors()
    {
        if(PsFacade::isPrinterHaveUnclosedOrders($this->psPrinter))
        {
            $this->addError(_t('site.ps', 'This printer has unfinished orders and cannot be deleted.'));
        }
    }

    /**
     * Do operation
     */
    protected function doOperation()
    {
        $psPrinter = $this->psPrinter;
        $psPrinter->companyService->updated_at = dbexpr('NOW()');
        $psPrinter->companyService->is_deleted = 1;
        AssertHelper::assertSave($psPrinter->companyService);

        AssertHelper::assertSave($psPrinter);

        foreach ($psPrinter->companyService->storeOrderAttemps as $orderAttemp) {
            if ($orderAttemp->order->getIsTestOrder()) {
                $orderAttemp->status = StoreOrderAttemp::STATUS_CANCELED;
                $orderAttemp->cancel_initiator = ChangeOrderStatusEvent::INITIATOR_PS;
                $orderAttemp->cancelled_at     = DateHelper::now();
                $orderAttemp->safeSave();
            }
        }

        PsPrinterService::updatePrintersCache();
    }
}