<?php

namespace common\models;

use common\components\ActiveQuery;
use common\components\ArrayHelper;
use common\components\history\HistoryTrait;
use common\models\query\Model3dQuery;
use common\models\query\ProductQuery;
use common\modules\xss\helpers\XssHelper;
use frontend\models\ps\PsFacade;
use lib\message\UserContactInterface;
use yii\base\Exception;

/**
 * common user object
 * status and functions used in backend and frontend
 *
 * @property \common\models\Company $company
 * @property \common\models\Model3d[] $model3ds
 * @property \common\models\Product[] $products
 */
class User extends base\User implements UserContactInterface
{
    use HistoryTrait;

    /**
     * user deleted,banned and etc.
     */
    const STATUS_DELETED = 0;

    /**
     * fully active user
     */
    const STATUS_ACTIVE = 1;

    /**
     * if user send us delete user request
     */
    const STATUS_UNACTIVE = 5;


    /**
     * email not confirmed
     */
    const STATUS_UNCONFIRMED = 10;

    /**
     * created in backend for company
     */
    const STATUS_DRAFT_COMPANY = 15;

    /**
     * created in backend for company
     */
    const STATUS_DRAFT = 16;

    /**
     * Support user id
     */
    const USER_ID_SUPPORT = 1;

    /**
     * Bot user id
     */
    const USER_ID_BOT = 0;

    const USER_CUSTOMER_SERVICE = 35;
    const USER_CRON             = 109;
    const USER_TS               = 110; // treatstock fake user
    const USER_THINGIVERSE      = 112; // change to required id
    const USER_EASYPOST         = 120;
    const USER_TAXAGENT         = 130;
    const USER_PAYPAL           = 150;
    const USER_BRAINTREE        = 160;
    const USER_STRIPE           = 165;
    const USER_DIAX             = 170;
    const USER_EXCHANGER        = 190;

    const USER_TESTER = 999;
    const USER_ANONIM = 998;

    private static $trustLevels = [];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->uid = generateUid();
    }

    /**
     * get user statuses
     *
     * @return array
     */
    public static function getStatuses()
    {
        $statuses = [
            self::STATUS_DELETED       => _t('app', 'Deleted'),
            self::STATUS_ACTIVE        => _t('app', 'Active'),
            self::STATUS_UNACTIVE      => _t('app', 'Unactive'),
            self::STATUS_DRAFT         => _t('app', 'Draft'),
            self::STATUS_UNCONFIRMED   => _t('app', 'Unconfirmed'),
            self::STATUS_DRAFT_COMPANY => _t('app', 'Unclaimed Business'),
        ];
        return $statuses;
    }

    public function validateEmail()
    {
        if (!$this->email) {
            $this->addError('email', _t('site.user', 'Email is required'));
            return false;
        }
        return true;
    }

    /**
     * get user trust levels
     *
     * @return array
     */
    public static function getTrustLevels()
    {
        if (empty(self::$trustLevels)) {
            $levels            = [['title' => 'Simple', 'color' => 'yellow', 'code' => 'simple']];
            $trustLevel        = app('setting')->get('user.trustlevel', $levels);
            self::$trustLevels = $trustLevel;
        }
        return self::$trustLevels;
    }

    /**
     * get user trust level by code
     *
     * @param string $code
     * @return array
     */
    public static function getTrustLevel($code)
    {
        $levels    = self::getTrustLevels();
        $levelInfo = \lib\collection\CollectionDb::getRow($levels, ['code' => $code]);
        return $levelInfo;
    }

    /**
     * simple format trust level
     *
     * @param string $level
     * @return string
     */
    public function formatTrustLevel($level)
    {
        $levels = self::getTrustLevels();
        foreach ($levels as $l) {
            if ($l['code'] == $level) {
                return sprintf("<span style='padding:4px;color:white;background: %s'> &nbsp;</span> %s ", $l['color'], $l['title']);
            }
        }
        return null;
    }

    /**
     * get status text by code
     *
     * @param string $code
     * @return string
     */
    public static function getStatusText($code)
    {
        $status = self::getStatuses();
        return $status[$code];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getFullNameOrUsername()
    {
        $result = $this->username;
        if ($this->userProfile) {
            if (empty($this->userProfile->firstname)) {
            } else {
                $result = $this->userProfile->firstname . ' ' . $this->userProfile->lastname;
            }
        }
        $result = explode('-', $result);
        return $result[0];
    }

    /**
     * get user currency
     *
     * @param int $userId
     * @return string
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getUserCurrency($userId)
    {
        $user = self::tryFindByPk($userId);
        if (empty($user->userProfile)) {
            throw new \yii\base\UserException("Not valid user, no profile found");
        }
        return $user->userProfile->current_currency_iso;
    }

    /**
     * User is active
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->status == self::STATUS_ACTIVE || $this->isUnconfirmed();
    }

    public function isCustomerServiceCompany()
    {
        return $this->id === self::USER_CUSTOMER_SERVICE;
    }

    /**
     * @return bool
     */
    public function isUnconfirmed()
    {
        return $this->status === self::STATUS_UNCONFIRMED;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->status === self::STATUS_DELETED;
    }

    public function isAffiliateAllowed()
    {
        return !$this->isDeleted() && !$this->company?->isDeleted() && !$this->isAnonimUser();
    }

    /**
     * User is wiped
     *
     * @return bool
     */
    public function allowViewPublicProfile()
    {
        if (!$this->getIsActive()) {
            return false;
        }
        if ($this->userProfile && $this->userProfile->is_hidden_public_page) {
            return false;
        }
        return true;
    }

    /**
     * User is unconfirmed
     *
     * @return bool
     */
    public function getIsUnconfirmed()
    {
        return $this->status == self::STATUS_UNCONFIRMED;
    }

    public function isEmptyEmail()
    {
        $email = $this->email;
        if (defined('TEST_XSS') && TEST_XSS) {
            $email = XssHelper::cleanXssProtect($email);
        }
        return !$email;
    }

    public function invalidEmailMarker(): ?InvalidEmail
    {
        if (!$this->email) {
            return null;
        }
        $invalidEmail = InvalidEmail::find()->where(['email' => $this->email, 'ignored' => 0])->one();
        return $invalidEmail;
    }

    /**
     * Return true if user upload documet.
     *
     * @return bool
     */
    public function hasDocument(): bool
    {
        return (bool)$this->userDocuments;
    }

    /**
     * Return last uploaded user document, or null if user not uploaded document yet.
     *
     * @return UserDocument|null
     */
    public function getLastDocument(): ?UserDocument
    {
        return ArrayHelper::last($this->userDocuments);
    }

    /**
     * Retrun user contact by type
     *
     * @param string $contactType
     * @param        $onlyVerified
     * @return null|string
     * @throws Exception
     */
    public function getContactByType($contactType, $onlyVerified = true)
    {
        switch ($contactType) {
            case UserContactInterface::CONTACT_TYPE_EMAIL:
            {
                if (!$onlyVerified) {
                    return $this->email;
                }

                return in_array($this->status, [self::STATUS_ACTIVE, self::STATUS_UNACTIVE])
                    ? $this->email
                    : null;
            }

            case UserContactInterface::CONTACT_TYPE_PHONE:
            {
                $ps = PsFacade::getPsByUserId($this->id);

                if (!$ps || !$ps->phone) {
                    return null;
                }

                return !$onlyVerified || $ps->phone_status == Ps::PHONE_STATUS_CHECKED
                    ? $ps->phone_code . $ps->phone
                    : null;
            }

            default:
            {
                throw new Exception("Unknown contact type {$contactType}");
            }
        }
    }

    /**
     * @return ActiveQuery|null
     */
    public function getCompany()
    {
        return $this->hasOne(\common\models\Company::class, ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * @return Model3dQuery|null
     */
    public function getModel3ds()
    {
        return $this->hasMany(\common\models\Model3d::class, ['product_common_uid' => 'uid'])->via('productCommons');
    }

    /**
     * @return ProductQuery|null
     */
    public function getProducts()
    {
        return $this->hasMany(\common\models\Product::class, ['product_common_uid' => 'uid'])->via('productCommons');
    }

    public function changeIncomingQuote($allow_incoming_quotes): void
    {
        $this->allow_incoming_quotes = $allow_incoming_quotes;
    }


    /**
     * @return string
     */
    protected function getHistoryOwnerIdField()
    {
        return 'user_changed_id';
    }

    /**
     * Return true if user is system
     *
     * @return bool
     */
    public function getIsSystem()
    {
        return $this->id < 1000;
    }

    /**
     *
     */
    public function isAnonimUser()
    {
        return $this->id == self::USER_ANONIM;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        if (empty($this->email)) {
            // if we have user registered without email, send notifications to support, let them work )
            return 'support@treatstock.com';
        }
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasActiveCompany(): bool
    {
        $company = $this->company;
        if ($company === null) {
            return false;
        }
        return !$company->isDeleted();
    }

    /**
     * @return bool
     */
    public function allowedReceiveMessage(): bool
    {
        return (new \lib\message\UserConfig($this))->allowDialogAutoCreate();
    }

    /**
     * @return bool
     */
    public function allowCompanyDialogAutoCreate(): bool
    {
        return (new \lib\message\UserConfig($this))->allowCompanyDialogAutoCreate();
    }


    public function startCompanyCreate(): void
    {
        $this->is_company_creating_in_progress = 1;
    }

    public function finishCompanyCreate(): void
    {
        $this->is_company_creating_in_progress = 0;
    }

    public function isStatusDraft(): bool
    {
        return $this->status === self::STATUS_DRAFT;
    }

}