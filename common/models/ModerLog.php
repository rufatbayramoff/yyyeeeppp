<?php

namespace common\models;

use common\components\DateHelper;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "moder_log".
 */
class ModerLog extends \common\models\base\ModerLog
{
    const TYPE_ORDER = 'order';

    const TYPE_PS = 'ps';

    const TYPE_SERVICE = 'company_service';

    const TYPE_PRODUCT = 'product';

    const TYPE_COMPANY = 'ps';

    const TYPE_PS_PRINTER = 'ps_printer';

    const TYPE_PRINT_REVIEW = 'print_review';

    public static function getLastRejectModerLog($objectType, $objectId)
    {
        $objectIdWhere = ['object_id' => $objectId];
        if ($objectType == self::TYPE_PRODUCT) {
            $objectIdWhere = ['object_uuid' => $objectId];
        }
        $moderLog = ModerLog::find()
            ->where(['object_type' => $objectType, 'action' => CompanyService::MODERATOR_STATUS_REJECTED])
            ->andWhere($objectIdWhere)
            ->orderBy('id DESC')
            ->one();

        if (!$moderLog) {
            return 'No reject reason found.';
        }
        $reason = Json::decode($moderLog->result);
        if(!empty($reason['reason'])){
            $reason['reason'] = $reason['reason'] . '.';
        }
        return sprintf('%s %s', $reason['reason'], $reason['descr']);
    }

    /**
     * @param Ps $ps
     * @param string $action
     * @param \backend\models\user\UserAdmin $user
     * @return ModerLog
     */
    public static function createPsLog(Ps $ps,string $action,\backend\models\user\UserAdmin $user): ModerLog
    {
        $model = new self;
        $model->user_id = $user->id;
        $model->created_at = DateHelper::now();
        $model->started_at = DateHelper::now();
        $model->action = $action;
        $model->object_type = self::TYPE_COMPANY;
        $model->object_id = $ps->id;
        return $model;
    }
}