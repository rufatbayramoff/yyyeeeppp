<?php

namespace common\models;

use lib\money\Money;

/**
 * Class PaymentInvoiceItem
 * @property Money $lineTotal
 * @package common\models
 */
class PaymentInvoiceItem extends \common\models\base\PaymentInvoiceItem
{
    /**
     * @return string
     * @throws \Exception
     */
    public static function generateUuid(): string
    {
        do {
            $uuid = strtoupper(\common\components\UuidHelper::generateUuid(4));
        } while (static::find()->where(['uuid' => $uuid])->withoutStaticCache()->one());

        return $uuid;
    }

    /**
     * @return Money
     */
    public function getLineTotal(): Money
    {
        return Money::create($this->total_line, $this->paymentInvoice->currency);
    }
}