<?php
namespace common\models;

use common\components\ArrayHelper;
use Yii;

/**
 * 
 */
class PsPrinterToProperty extends \common\models\base\PsPrinterToProperty
{
    const SCENARIO_UPDATE = 'update';
    
    public function behaviors()
    {
        return [
            'history' => [
                'class' => \common\components\HistoryBehavior::className(),
                'historyClass' => PsPrinterHistory::className(),
                'foreignKey' => 'ps_printer_id',
                'ownerIdField' => 'ps_printer_id',
                'actionCode' => 'update_property',
                'skipAttributes' => ['updated_at', 'created_at']
            ],
        ];
    }
    
    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_UPDATE => ['property_id', 'value']
        ]);
    }
}