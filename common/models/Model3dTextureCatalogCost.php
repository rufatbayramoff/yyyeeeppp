<?php

namespace common\models;

/**
 * Use same class as Model3dTexture
 *
 * @package common\models
 */
class Model3dTextureCatalogCost extends Model3dTexture
{
    // Only default infill
    public $infill = null;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'model3d_texture_catalog_cost';
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getCatalogCosts()
    {
        return $this->hasMany(\common\models\CatalogCost::class, ['texture_id' => 'id'])->inverseOf('texture');
    }
}