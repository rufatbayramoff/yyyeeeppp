<?php namespace common\models;

use common\components\PaymentExchangeRateConverter;
use Yii;
use \lib\payment\PaymentManagerFacade;

/**
 * Helps to add taxes to payment details.
 *
 * Usage can be next:
 * // this call will find out current user tax rate for model and ps, and add tax rate to new records
 * PaymentDetailTaxRate::addTaxRate($paymentDetail->id);
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentDetailTaxRate extends \common\models\base\PaymentDetailTaxRate
{

    /**
     *
     * @param User $user
     * @return boolean
     */
    public static function hasUserTaxes($user)
    {
        $taxModel = $user->userTaxInfo;
        if (empty($taxModel) || $taxModel->status == UserTaxInfo::STATUS_NEW) {
            return false;
        }
        return true;
    }
}
