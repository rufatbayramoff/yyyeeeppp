<?php

namespace common\models;

use common\modules\contentAutoBlocker\components\contentFilters\BannedPhraseFilter;
use Yii;

/**
 * Class ContentFilterBannedPhrase
 *
 * @package common\models
 */
class ContentFilterBannedPhrase extends \common\models\base\ContentFilterBannedPhrase
{
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                ['phrase', 'validatePhrase']
            ]
        );
    }



    public function validatePhrase($attribute)
    {
        /** @var BannedPhraseFilter $bannedPhraseFilter */
        $bannedPhraseFilter = Yii::createObject(BannedPhraseFilter::class);
        try {
            $bannedPhraseFilter->checkOnePhrase('Some Text', $this->phrase);
        } catch (\Exception $e) {
            $this->addError('phrase', 'Invalid regular for phrase: '.$this->phrase);
            return false;
        }
        return true;
    }
}