<?php

namespace common\models;

use common\components\DateHelper;

/**
 * Class UserEmailSent
 * @package common\models
 */
class UserEmailSent extends \common\models\base\UserEmailSent
{
    /**
     * email ps printer certify notifier
     */
    const PS_PRINTER_CERTIFY = 'ps_printer_certify';

    /**
     * email reminder to publish model if not published
     * model reference in json_params
     */
    const AUTHOR_PUBLISH_MODEL = 'author_publish_model';

    /**
     *
     */
    const USER_PAY_ORDER = 'user_pay_order';

    /**
     *
     */
    const REQUEST_TEST_ORDER= 'request_test_order';

    /**
     *
     */
    const FULFILL_PROFILE= 'user_profile';

    const AUTHOR_BEST_MODEL = 'best_model';

    const ORDER_REVIEW = 'order_review';
    const ORDER_ATTEMPT_NO_TRACKING = 'order_attempt_notracking';
    public const ORDER_ATTEMPT_CLIENT_RECEIVED_ORDER = 'order_attempt_client_received_order';

    public const PS_ORDER_REVIEWED = 'ps_order_reviewed';

    /**
     * simple hash function, can be replaced with specific hash method for other notifiers
     *
     * @param array $array
     * @return string
     */
    public static function makeHash(array $array)
    {
        $result = [];
        foreach($array as $k=>$v){
            $result[] = "$k:$v";
        }
        $resultTxt = implode('_', $result);
        if(strlen($resultTxt) > 240){
            $resultTxt = md5($resultTxt);
        }
        return $resultTxt;
    }

    /**
     * @param int $time - The time in seconds that must pass
     *
     * @return bool
     */
    public function checkTimePassed(int $time): bool
    {
        $createTime = DateHelper::strtotimeUtc($this->created_at);
        $nowTime = DateHelper::strtotimeUtc(DateHelper::now());

        return (($nowTime - $createTime) >= $time);
    }

}