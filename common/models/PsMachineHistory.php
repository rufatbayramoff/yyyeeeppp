<?php

namespace common\models;

use common\interfaces\ModelHistoryInterface;
use common\traits\ModelHistoryARTrait;

/**
 * Class PsMachineHistory
 *
 * @package common\models
 */
class PsMachineHistory extends \common\models\base\PsMachineHistory implements ModelHistoryInterface
{
    use ModelHistoryARTrait;
}