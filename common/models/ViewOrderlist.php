<?php

namespace common\models;

use backend\modules\statistic\reports\BaseReportInterface;
use common\components\DateHelper;
use Yii;
use yii\db\Query;

/**
 * Class ViewOrderlist
 * @package common\models
 */
class ViewOrderlist extends \common\models\base\ViewOrderlist implements BaseReportInterface
{
    public $company_title;
    public $country_title;

    public static function primaryKey()
    {
        return ['order_id'];
    }

    private $from_date;
    private $to_date;

    /**
     * @return array
     */

    public function getColumnsNames()
    {
        $labels = $this->attributeLabels();
        $labels['preorder_id'] = Yii::t('app', 'Quotes');
        $labels['company_title'] = Yii::t('app', 'Company');
        $labels['country_title'] = Yii::t('app', 'Country');
        return $labels;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $userIds = \Yii::$app->setting->get('user.excludeReport', [1]);
        $query = self::find()->where(['NOT IN', 'buyer_id', $userIds]);
        $query->leftJoin(['company' => $this->company()],'company.id='.ViewOrderlist::column('order_id'));
        $query->andFilterWhere(['>=',ViewOrderlist::column('created_at'), $this->from_date]);
        $query->andFilterWhere(['<=',ViewOrderlist::column('created_at'), $this->to_date]);
        $query->select(['view_orderlist.*','company.company_title','company.country_title']);
        return $query->all();
    }

    public function setParams(array $params)
    {
        $this->to_date = isset($params['to_date']) ?  $params['to_date'] : null;
        $this->from_date = isset($params['from_date']) ?  $params['from_date'] : null;
    }

    public static function create()
    {
        // TODO: Implement create() method.
    }

    private function company()
    {
        $query = new Query();
        $query->select(['company_title' => 'ua.company', 'so.id','country_title' => 'gc.title']);
        $query->from('store_order so');
        $query->innerJoin('user_address ua','ua.id = so.ship_address_id');
        $query->leftJoin('geo_country gc','gc.id = ua.country_id');
        return $query;
    }
}