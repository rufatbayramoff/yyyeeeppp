<?php

namespace common\models;


/**
 *
 */
class StoreOrderDelivery extends \common\models\base\StoreOrderDelivery
{
    /**
     * @param $code
     */
    public function setDeliveryTypeByCode($code)
    {
        $deliveryType           = DeliveryType::findOne(['code' => $code]);
        $this->delivery_type_id = $deliveryType->id;
        $this->order->delivery_type_id  =$deliveryType->id;
    }

    public function getData(): array
    {
        return [
            $this->calculated_parcel_weight,
            $this->calculated_parcel_length,
            $this->calculated_parcel_width,
            $this->calculated_parcel_height,
            $this->calculated_parcel_measure
        ];
    }

    /**
     * @return bool
     */
    public function isInternational(): bool
    {
        return $this->delivery_type_id === DeliveryType::INTERNATIONAL_STANDART_ID;
    }

}