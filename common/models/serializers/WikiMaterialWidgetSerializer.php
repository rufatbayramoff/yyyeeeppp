<?php

namespace common\models\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\StoreOrderReview;
use common\models\WikiMaterial;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserUtils;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use yii\helpers\Url;

class WikiMaterialWidgetSerializer extends AbstractProperties
{
    public function getProperties()
    {
        return [
            WikiMaterial::class => [
                'code',
                'title',
                'show_widget',
                'firstPhotoUrl' => function (WikiMaterial $wikiMaterial) {
                    $firstPhoto = $wikiMaterial->getFirstPhotoFile();
                    return $firstPhoto?->getFileUrl();
                },
                'about'         => 'widget_about',
                'advantages'    => 'widget_advantages',
                'disadvantages' => 'widget_disadvantages',
                'url' => 'publicUrl'
            ]
        ];
    }
}