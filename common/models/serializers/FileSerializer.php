<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 02.08.18
 * Time: 17:07
 */

namespace common\models\serializers;


use common\components\serizaliators\AbstractProperties;
use common\models\File;
use common\models\Preorder;
use common\models\PreorderWork;
use common\models\ProductSnapshot;
use common\models\User;
use common\modules\product\serializers\ProductSnapshotSerializer;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;

class FileSerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            File::class => [
                'uuid',
                'name',
                'size',
                'createdAt' => 'created_at',
                'status',
                'md5sum'
            ]
        ];
    }
}