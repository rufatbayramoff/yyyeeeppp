<?php

namespace common\models\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\File;
use common\models\StoreOrderReview;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserUtils;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use yii\helpers\Url;

/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 19.09.18
 * Time: 11:20
 */

class ReviewSerializer extends AbstractProperties
{
    public function getProperties()
    {
        return [
            StoreOrderReview::class => [
                'comment',
                'answer' => function (StoreOrderReview $model) {
                    return $model->answer;
                },
                'ratingInfo' => function (StoreOrderReview $model) {
                    return [
                        'ratingAll' => $model->getRating(),
                        'ratingSpeed' => $model->rating_speed,
                        'ratingQuality' => $model->rating_quality,
                        'ratingCommunication' => $model->rating_communication,
                    ];
                },
                'createdAt' => function (StoreOrderReview $model) {
                    return app()->formatter->asDate($model->created_at);
                },
                'coverImgUrl' => function (StoreOrderReview $model) {
                    $file = $model->reviewCoverFile;
                    return $file ? ImageHtmlHelper::getThumbUrl($file->getFileUrl(), ImageHtmlHelper::PRINT_REVIEW_BLOCK_WIDTH, ImageHtmlHelper::PRINT_REVIEW_BLOCK_HEIGHT) : false;
                },
                'urlPs' => function (StoreOrderReview $model) {
                    return Url::toRoute(PsFacade::getPsReviewsRoute($model->ps), true);
                },
                'user' => function (StoreOrderReview $model) {
                    $user = $model->order->user;

                    return [
                        'fullName' => UserFacade::getSalesTitle($user),
                        'avatarUrl' => UserFacade::getSalesAvatarUrl($user),
                        'publicProfileUrl' => UserUtils::getUserPublicProfileUrl($user)
                    ];
                }
            ]
        ];
    }
}