<?php
namespace common\models;

use Yii;

/**
 * History
 * 
 * @TODO - rename actions to text and alter store_pricer_history table for easy read
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StorePricerHistory extends \common\models\base\StorePricerHistory
{
    /**
     * initial price calculated by system
     */
    const ACTION_START = 1;
    
    /**
     * price changed due to api requests
     */
    const ACTION_API = 2;
    
    /**
     * price changed manually
     */
    const ACTION_MANUAL = 3;
    
    /**
     * price changed due to some calculations
     */
    const ACTION_RECALC = 4;
    /**
     * price changed by ps request
     */
    const ACTION_PS = 5;
    
    /**
     * price updated in StorePricer class, called from other side
     */
    const ACTION_SYSTEM = 7;
    
     
}