<?php

namespace common\models;

/**
 * Class CuttingPackPageImage
 * @package common\models
 */
class CuttingPackPageImage extends \common\models\base\CuttingPackPageImage
{
    public const DOWNLOAD_IMAGE_URL = '/my/order-laser-cutting-ajax/part-image-download';
}