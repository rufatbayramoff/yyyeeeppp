<?php

namespace common\models;

use common\interfaces\FileBaseInterface;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use Yii;
use yii\web\UploadedFile;

/**
 * Class HomePageCategoryCard
 *
 * @property UploadedFile|null $coverImageFile
 *
 * @package common\models
 */
class HomePageCategoryCard extends \common\models\base\HomePageCategoryCard
{
    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';

    /**
     * @var UploadedFile
     */
    public $coverImageFile;

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_CREATE] = [
            '!created_at',
            '!cover_file_uuid',
            'coverImageFile',
            'position',
            'is_active',
            'product_category_id',
            'title',
            'url'
        ];

        $scenarios[self::SCENARIO_UPDATE] = [
            '!cover_file_uuid',
            'coverImageFile',
            'position',
            'is_active',
            'product_category_id',
            'title',
            'url'
        ];

        return $scenarios;
    }

    public function rules()
    {
        return [
            [['coverImageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => ['png', 'jpg', 'jpeg'], 'on' => [self::SCENARIO_CREATE]],
            [['coverImageFile'], 'file', 'extensions' => ['png', 'jpg', 'jpeg'], 'on' => [self::SCENARIO_UPDATE]],
            [['created_at', 'position', 'is_active'], 'required'],
            [['created_at'], 'safe'],
            [['is_active', 'product_category_id', 'position'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
            [['cover_file_uuid'], 'string', 'max' => 32],
            [
                ['product_category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \common\models\ProductCategory::class,
                'targetAttribute' => ['product_category_id' => 'id']
            ],
            [['title', 'url', 'product_category_id'], 'categoryValidate', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE], 'skipOnEmpty' => false],
        ];
    }

    public function categoryValidate(): void
    {
        if (
            (empty($this->url) || empty($this->title)) &&
            (empty($this->product_category_id) || !$this->productCategory)
        ) {
            $this->addError('product_category_id', 'You must fill out the "product_category_id" or "title" and "url"');
        }
    }

    /**
     * @param array $data
     * @param null $formName
     *
     * @return bool
     */
    public function loadData($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->coverImageFile = UploadedFile::getInstance($this, 'coverImageFile');

            return true;
        }

        return false;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @return bool
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function saveCategoryCard($runValidation = true, $attributeNames = null)
    {
        if ($this->validate()) {
            if ($this->coverImageFile instanceof UploadedFile) {
                $fileFactory = Yii::createObject(FileFactory::class);
                $file = $fileFactory->createFileFromUploadedFile($this->coverImageFile, FileBaseInterface::TYPE_FILE_ADMIN);

                /** @var FileRepository $fileRepository */
                $fileRepository = Yii::createObject(FileRepository::class);
                $file->setPublicMode(true);
                $file->setOwner(HomePageCategoryCard::class, 'cover_file_uuid');
                $file->setFixedPath('home-page-category-card');
                $fileRepository->save($file);

                $this->cover_file_uuid = $file->uuid;
            }

            return $this->save($runValidation, $attributeNames);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getTitleText(): string
    {
        if (!empty($this->title)) {
            return $this->title;
        }

        if ($productCategory = $this->productCategory) {
            return $productCategory->title;
        }

        return '';
    }

    /**
     * @return string
     */
    public function getViewUrl(): string
    {
        if (!empty($this->url)) {
            return HL($this->url);
        }

        $productCategory = $this->productCategory;

        if ($productCategory) {
            return HL($productCategory->getViewUrl());
        }

        return '';
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function getCoverUrl(): string
    {
        return $this->coverFile ? $this->coverFile->getFileUrl() : '';
    }
}