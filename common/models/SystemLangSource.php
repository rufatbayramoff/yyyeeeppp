<?php
namespace common\models;

use common\models\query\SystemLangSourceQuery;

/**
 * Edit this file
 * This is the model class for table "system_lang_source".
 */
class SystemLangSource extends \common\models\base\SystemLangSource
{
    /**
     * @return SystemLangSourceQuery
     */
    public static function find()
    {
        return new SystemLangSourceQuery(get_called_class());
    }

    /**
     * @param $langIsoCode
     * @return \common\components\BaseActiveQuery
     */
    public function getLangMessage($langIsoCode)
    {
        return $this->hasOne(\common\models\SystemLangMessage::class, ['id' => 'id'])->onCondition(['language' => $langIsoCode]);
    }
}