<?php
namespace common\models;

use Yii;
use  common\components\exceptions\AssertHelper;
use \console\jobs\QueueGateway;
/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class Job extends \common\models\base\Job
{
     /**
     * Statuses const
     */
    const STATUS_COMPLETE = 'complete';

    /**
     * Mark job as runuing
     */
    public function markAsRuning()
    {
        $this->status = QueueGateway::JOB_STATUS_RUNNING;
        $this->updated_at = dbexpr('NOW()');
        AssertHelper::assertSave($this);
    }

    /**
     * Mark job as complited
     */
    public function markAsCompleted()
    {
        $this->finished_at = dbexpr('NOW()');
        $this->updated_at = dbexpr('NOW()');
        $this->status = QueueGateway::JOB_STATUS_COMPLETED;
        AssertHelper::assertSave($this);
    }

    /**
     * Set job result
     * @param $result
     */
    public function setResult($result)
    {
        $this->result = \yii\helpers\Json::encode($result);
        $this->updated_at = dbexpr('NOW()');
        AssertHelper::assertSave($this);
    }

    /**
     * Mark job as failed
     */
    public function markAsFailed($result = '')
    {
        if(!empty($result)){
            $this->result =  \yii\helpers\Json::encode(['success'=>false, 'message'=>$result]);
        }
        $this->status = QueueGateway::JOB_STATUS_FAILED;
        $this->updated_at = dbexpr('NOW()');
        AssertHelper::assertSave($this);
    }

    /**
     * Return job args
     * @return mixed
     */
    public function getArgsData()
    {
        return is_array($this->args) ? $this->args : \yii\helpers\Json::decode($this->args);
    }

    /**
     * Return result data
     * @return mixed
     */
    public function getResultData()
    {
        return \yii\helpers\Json::decode($this->result);
    }
}