<?php

namespace common\models;

use common\modules\company\components\CompanyServiceInterface;
use common\modules\company\helpers\CompanyServiceUrlHelper;
use common\modules\cutting\helpers\CuttingUrlHelper;

/**
 * Class CompanyServiceType
 * @package common\models
 */
class CompanyServiceType extends \common\models\base\CompanyServiceType
{
    public function getAddUrl()
    {
        $addLinks = [
            CompanyServiceInterface::TYPE_PRINTER  => '/mybusiness/services/add-printer',
            CompanyServiceInterface::TYPE_CUTTING  => CuttingUrlHelper::addMachine(),
            CompanyServiceInterface::TYPE_CNC      => '/mybusiness/services/add-cnc',
            CompanyServiceInterface::TYPE_SERVICE  => CompanyServiceUrlHelper::add(),
            CompanyServiceInterface::TYPE_WINDOW   => '/mybusiness/cs-window/manage',
            CompanyServiceInterface::TYPE_DESIGNER => '/mybusiness/company/edit-ps?type=designing',
        ];

        $link = $addLinks[$this->name];
        return $link;
    }
}