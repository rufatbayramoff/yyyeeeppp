<?php
namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class SiteHelpCategory extends \common\models\base\SiteHelpCategory
{
    public function getSlug()
    {
        if(empty($this->slug)){
            $this->slug = Inflector::slug($this->title);
        }
        return $this->slug;
    }
}