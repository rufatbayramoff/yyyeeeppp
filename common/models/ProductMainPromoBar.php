<?php

namespace common\models;

use common\interfaces\FileBaseInterface;
use common\models\repositories\FileRepository;
use frontend\components\image\ImageHtmlHelper;
use Yii;
use yii\web\UploadedFile;

/**
 * Class ProductMainPromoBar
 * @package common\models
 */
class ProductMainPromoBar extends \common\models\base\ProductMainPromoBar
{
    public function addImage(FileAdmin $image)
    {
        if ($this->file) {
            $fileForDelete = $this->file;
            $this->file_uuid = null;
            $this->safeSave();
            $fileRepository = Yii::createObject(FileRepository::class);
            $fileRepository->delete($fileForDelete);
        }
        $image->setPublicMode(true);
        $image->setFixedPath('product_main_promobar/images');
        $image->setOwner(self::class, 'file_id');
        ImageHtmlHelper::stripExifInfo($image);
        $fileRepository = \Yii::createObject(FileRepository::class);
        $fileRepository->save($image);
        $this->file_uuid = $image->uuid;
    }
}