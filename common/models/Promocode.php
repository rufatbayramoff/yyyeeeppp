<?php

namespace common\models;

use lib\money\Money;

/**
 * Class Promocode
 * @package common\models
 */
class Promocode extends \common\models\base\Promocode
{
    const USAGE_TYPE_ONE = 'one';
    const USAGE_TYPE_MANY = 'many';
    const USAGE_TYPE_ONE_PER_USER = 'one_user';

    const DISCOUNT_TYPE_PERCENT = 'percent';
    const DISCOUNT_TYPE_FIXED = 'fixed';

    const DISCOUNT_FOR_MODEL = 'model';
    const DISCOUNT_FOR_PRINT = 'print';
    const DISCOUNT_FOR_DELIVERY = 'delivery';
    const DISCOUNT_FOR_SERVICE_FEE = 'fee';
    const DISCOUNT_FOR_ALL = 'all';

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['order_total_from'],'required'];
        return $rules;
    }

    /**
     *
     * @TODO - update to validated
     *
     * @param $array
     * @param $err
     * @return null|static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function findValid($array, $err)
    {
        return self::tryFind($array, $err);
    }

    public function behaviors()
    {
        return [
            'history' => [
                'class' => \common\components\HistoryBehavior::className(),
                'historyClass' => PromocodeHistory::className(),
                'foreignKey' => 'promocode_id'
            ],
        ];
    }

    public function getDiscountsFor()
    {
        return [
            self::DISCOUNT_FOR_MODEL => _t('site.promo', 'Discount For Model'),
            self::DISCOUNT_FOR_PRINT => _t('site.promo', 'Discount For Print'),
            self::DISCOUNT_FOR_DELIVERY => _t('site.promo', 'Discount For Delivery'),
            self::DISCOUNT_FOR_SERVICE_FEE => _t('site.promo', 'Discount For Fee'),
            self::DISCOUNT_FOR_ALL => _t('site.promo', 'Discount For All')
        ];
    }

    public function getUsageTypes()
    {
        return [
            self::USAGE_TYPE_ONE => 'One',
            self::USAGE_TYPE_MANY => 'Many',
            self::USAGE_TYPE_ONE_PER_USER => 'Per User'
        ];
    }

    public function getDiscountTypes()
    {
        return [
            self::DISCOUNT_TYPE_PERCENT => 'percent',
            self::DISCOUNT_TYPE_FIXED => 'fixed'
        ];
    }

    public function getAmount()
    {
        $amount = $this->discount_amount;
        return Money::create($amount, $this->discount_currency);
    }

    /**
     * @return bool
     */
    public function isFixedDiscount()
    {
        return $this->discount_type === Promocode::DISCOUNT_TYPE_FIXED;
    }

    public function isPercentDiscount()
    {
        return $this->discount_type === Promocode::DISCOUNT_TYPE_PERCENT;
    }

    public function isActive(): bool
    {
        return $this->is_active && $this->is_valid;
    }
}