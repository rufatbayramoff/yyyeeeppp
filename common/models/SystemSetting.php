<?php namespace common\models;

use common\components\BaseActiveQuery;
use common\components\exceptions\BusinessException;
use Yii;

/**
 * This is the model class for table "system_setting".
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class SystemSetting extends \common\models\base\SystemSetting
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'      => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value'      => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function update($runValidation = true, $attributeNames = null)
    {
        app('cache_settings')->flush();
        $res = parent::update($runValidation, $attributeNames);
        app('cache_settings')->flush();
        return $res;
    }

    /**
     * get system setting by group and key
     *
     * you can use get('group.key') - short
     *
     * @param string $groupKey
     * @param mixed $default - default value to return if config not found
     * @return array|string
     * @throws BusinessException
     * @throws \yii\base\InvalidConfigException
     */
    public function get($groupKey, $default = null)
    {
        $result = app('cache_settings')->get('sys1_' . $groupKey);
        if ($result !== false) {
            return $result;
        }
        $groupParts = explode(".", $groupKey, 2);
        if (empty($groupParts[1])) {
            throw new BusinessException(_t("site.core", "Not correct key setting {key}", ['key' => $groupKey]));
        }
        $group = $groupParts[0];
        $key   = $groupParts[1];
        /** @var SystemSettingGroup $groupRes */
        $groupRes = SystemSettingGroup::find()->where(['title' => $group])->one();
        if ($groupRes == null) {
            return $default;
        }
        $model = SystemSetting::findOne(['key' => $key, 'group_id' => $groupRes->id]);
        if ($model === null) {
            return $default;
        } else {
            $result = $model->value;
            if ($result == 'json') {
                $result = \yii\helpers\Json::decode($model->json);
            }
        }
        app('cache_settings')->set('sys1_' . $groupKey, $result);
        return $result;
    }

    public function getGroupKey()
    {
        $groupKey = $this->group->title . '.' . $this->key;
        return $groupKey;
    }

    /**
     *
     * @param string $groupKey
     * @param array $data
     * @return mixed
     */
    public function add($groupKey, array $data)
    {
        $groupParts = explode(".", $groupKey, 2);
        $group      = $groupParts[0];
        $key        = $groupParts[1];

        $groupObj = SystemSettingGroup::findOne(['title' => $group]);
        $has      = SystemSetting::findOne(['group_id' => $groupObj->id, 'key' => trim($key)]);
        if ($has) {
            return $has->value;
        }

        $newSetting = new SystemSetting();
        foreach ($data as $k => $v) {
            $newSetting->$k = $v;
        }
        $newSetting->group_id = $groupObj->id;
        $newSetting->key      = trim($key);
        $newSetting->safeSave();
        $result = $newSetting->value;
        if ($newSetting->value == 'json') {
            $result = \yii\helpers\Json::decode($newSetting->json);
        }
        return $result;
    }

    /**
     * get list by group
     *
     * @param string $groupKey
     * @return array
     * @throws \yii\base\Exception
     */
    public function getList($groupKey)
    {
        $resultCache = app('cache_settings')->get('groupsetting_' . $groupKey);
        if ($resultCache !== false) {
            return $resultCache;
        }
        /** @var SystemSettingGroup $group */
        $group = SystemSettingGroup::find()->with('systemSettings')
            ->where(['title' => $groupKey])->one();
        if ($group == null) {
            throw new \yii\base\Exception("Group not found {$groupKey}");
        }
        $settings = $group->systemSettings;
        $result   = [];
        foreach ($settings as $setting) {
            $result[$setting->key] = $setting->value;
        }
        app('cache_settings')->set('groupsetting_' . $groupKey, $result);
        return $result;
    }

    /**
     * merges system settings into array
     * example SystemSetting->getMulti(['model3d.allowextension', 'model3d.allowscreens']);
     *
     * @param string[] $groupKeys
     * @param mixed $default
     * @return mixed[]
     */
    public function getMulti($groupKeys, $default = null)
    {
        $result = [];
        foreach ($groupKeys as $groupKey) {
            $result = array_merge($result, (array)$this->get($groupKey, []));
        }
        if (empty($result)) {
            return $default;
        }
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\common\models\UserAdmin::className(), ['id' => 'created_user_id']);
    }

    /**
     *
     * @return BaseActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(\common\models\UserAdmin::className(), ['id' => 'updated_user_id']);
    }
}
