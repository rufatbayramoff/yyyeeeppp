<?php

namespace common\models;

use common\components\DateHelper;
use Yii;

/**
 * Class InvalidEmail
 * @package common\models
 */
class InvalidEmail extends \common\models\base\InvalidEmail
{
    public const TYPE_MAIL_DELIVERY_SYSTEM = 'mailDeliverySystem';
    public const TYPE_LOCAL_LOG = 'localLog';

    public static function markAsBad($email, $type)
    {
        $invalidEmail = Yii::createObject(InvalidEmail::class);
        $invalidEmail->created_at = DateHelper::now();
        $invalidEmail->email = $email;
        $invalidEmail->type  = $type;
        $invalidEmail->safeSave();
    }
}