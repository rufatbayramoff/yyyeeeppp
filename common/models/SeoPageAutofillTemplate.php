<?php

namespace common\models;

/**
 * Class SeoPageAutofillTemplate
 * @package common\models
 */
class SeoPageAutofillTemplate extends \common\models\base\SeoPageAutofillTemplate
{
    const TYPE_MODEL3D= 'model3d';
    const TYPE_PS = 'ps';
    const TYPE_PSCATALOG = 'pscatalog';

    public function rules(){
        $rules = parent::rules();
        $rules[] = ['is_default', 'validateDefault'];
        return $rules;
    }

    public function validateDefault($attribute)
    {
        $row = self::findOne(['is_default'=> 1, 'lang_iso'=>$this->lang_iso, 'type'=>$this->type]);
        if($row && $this->id!==$row->id && $this->is_default){
            $this->addError($attribute, 'Default template is already defined for ' . $this->type . ' type.');
        }
    }
}