<?php

namespace common\models;

use common\modules\cutting\helpers\CuttingUrlHelper;
use frontend\components\image\ImageHtmlHelper;
use http\Params;

/**
 * Class CuttingPackFile
 *
 * @package common\models
 * @property CuttingPackPart[] $activeCuttingPackParts
 * @property CuttingPackPart[] $produceCuttingPackParts
 */
class CuttingPackFile extends \common\models\base\CuttingPackFile
{
    public const TYPE_IMAGE              = 'image';
    public const TYPE_MODEL              = 'model';
    public const TYPE_MODEL_PREVIEW_URL  = '/';
    public const TYPE_MODEL_PREVIEW_STUB = '/static/images/cutting-format-template.svg';

    public function getTitle()
    {
        return $this->file->name;
    }

    public function getPartNum(CuttingPackPart $cuttingPart)
    {
        $i = 1;
        foreach ($this->cuttingPackPages as $oneCuttingPage) {
            foreach ($oneCuttingPage->activeCuttingPackParts as $oneCuttingPart) {
                if ($oneCuttingPart->uuid === $cuttingPart->uuid) {
                    return $i;
                }
                $i++;
            }
        }
        return '';
    }

    public function getPageNum(CuttingPackPage $cuttingPackPage)
    {
        $i = 1;
        foreach ($this->cuttingPackPages as $oneCuttingPage) {
            if ($oneCuttingPage->uuid === $cuttingPackPage->uuid) {
                return $i;
            }
            $i++;
        }
        return '';
    }

    public function isModel()
    {
        return $this->type === self::TYPE_MODEL;
    }

    public function isImage()
    {
        return $this->type === self::TYPE_IMAGE;
    }

    public function getPreviewUrl()
    {
        if ($this->isImage()) {
            return ImageHtmlHelper::getThumbUrlForFile($this->file, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL);
        }
        return param('siteUrl'). static::TYPE_MODEL_PREVIEW_STUB;
    }

    public function setSvgFile(File $svgFile)
    {
        $this->populateRelation('svgFile', $svgFile);
        $this->svg_file_uuid = $svgFile->uuid;
    }

    public function getCuttingPackParts()
    {
        return $this->hasMany(\common\models\CuttingPackPart::class, ['cutting_pack_page_uuid' => 'uuid'])->via('cuttingPackPages');
    }

    public function addPage(CuttingPackPage $cuttingPackPage)
    {
        $currentPages   = $this->cuttingPackPages;
        $currentPages[] = $cuttingPackPage;
        $this->populateRelation('cuttingPackPages', $currentPages);
    }

    public function getProduceCuttingPackParts()
    {
        $cuttingParts = $this->getActiveCuttingPackParts();
        $produceParts = [];
        foreach ($cuttingParts as $cuttingPart) {
            if ($cuttingPart->qty) {
                $produceParts[] = $cuttingPart;
            }
        }
        return $produceParts;
    }

    public function getActiveCuttingPackParts()
    {
        $cuttingParts = $this->cuttingPackParts;
        $activeParts = [];
        foreach ($cuttingParts as $cuttingPart) {
            if ($cuttingPart->is_active) {
                $activeParts[] = $cuttingPart;
            }
        }
        return $activeParts;
    }

    public function getCuttingPackPages()
    {
        return $this->hasMany(\common\models\CuttingPackPage::class, ['cutting_pack_file_uuid' => 'uuid'])->inverseOf('cuttingPackFile')->orderBy('cutting_pack_page.uuid');
    }
}