<?php namespace common\models;

use Yii;

/**
 * System lang model
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class SystemLang extends \common\models\base\SystemLang
{

    /**
     * current language
     * 
     * @var string
     */
    public static $current = null;

    public static $allActive = null;

    /**
     * get current language
     * 
     * @return string
     */
    public static function getCurrent()
    {
        if (self::$current === null) {
            // try to get from session
            try{
                if(!empty(\Yii::$app->language)){
                    self::$current = self::getLangByCode(\Yii::$app->language);
                }
            }catch(\Exception $e){                
            }
            if(self::$current===null){
                self::$current = self::getDefaultLang();
            }
        }
        return self::$current;
    }

    /** 
     * set current language
     * 
     * @param string $url
     */
    public static function setCurrent($url = null)
    {
        $language = self::getLangByUrl($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->iso_code;
    }

    /**
     * get default language
     * 
     * @return \yii\base\Model
     */
    public static function getDefaultLang()
    {
        $defaultLang = self::getLangByCode('en-US');
        if(empty($defaultLang)){
            $langObj = new SystemLang();
            $langObj->iso_code = 'en-US';
            $langObj->title = 'English';
            $langObj->title_original = 'English';
            $langObj->url = 'en';
            $langObj->is_active = 1;
            $langObj->safeSave();
            $defaultLang = SystemLang::find()->where('`iso_code` = :code', [':code' => 'en-US'])->one();
        }
        return $defaultLang;
    }


    /**
     * get language by url
     * 
     * @param string $urlCode
     * @return \yii\base\Model
     */
    public static function getLangByUrl($urlCode = null)
    {
        return self::getBy(['url'=>$urlCode]);
    }
    /**
     * get language by url
     * 
     * @param string $code
     * @return \yii\base\Model
     */
    public static function getLangByCode($code = null)
    {
        return self::getBy(['iso_code'=>$code]);
    }
    /**
     * find by column and cache result
     * 
     * @param type $column
     * @return type
     */
    private static function getBy($column)
    {
        $key = "lang_". serialize($column);
        $data = Yii::$app->cache->get($key);
        if ($data === false) {
            $data = SystemLang::find()->where($column)->one();
            Yii::$app->cache->set($key, $data);
        }
        return $data;
    }
    
    /**
     * 
     * @return SystemLang[]
     */
    public static function getAllActive()
    {
        self::$allActive = self::$allActive ?: SystemLang::find()->where('is_active=1')->orderBy('priority')->all();
        return self::$allActive;
    }



    /**
     *
     * @return SystemLang[]
     */
    public static function getAllActiveForWidget()
    {
        static $widgetLangs;
        $widgetLangs = $widgetLangs ? : SystemLang::find()->where(['is_widget_active'=>1])->orderBy('priority')->all();
        return $widgetLangs;
    }

    /**
     * get other languages - without current
     *
     * @return type
     * @throws \Exception
     */
    public static function getOther()
    {
        $langs = SystemLang::getDb()->cache(function ($db) {
            return SystemLang::find()->where(
                'id != :current_id AND is_active=1', [':current_id' => SystemLang::getCurrent()->id]
            )->all(); 
        }); 
        return $langs;
    }
}
