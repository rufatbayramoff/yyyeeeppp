<?php

namespace common\models;

use lib\money\Money;

/**
 * Class StoreOrderPosition
 *
 * @package common\models
 */
class StoreOrderPosition extends \common\models\base\StoreOrderPosition
{
    public const STATUS_NEW      = 'new';
    public const STATUS_PAID     = 'paid';
    public const STATUS_DECLINED = 'declined';
    public const STATUS_REFUNDED = 'refunded';
    public const STATUS_CANCELED = 'canceled';

    /**
     * get total amount with fee
     *
     * @return Money
     */
    public function getFullPrice(): Money
    {
        if ($this->primaryPaymentInvoice) {
            return $this->status === self::STATUS_PAID ? $this->primaryPaymentInvoice->getAmountTotalWithRefund() : $this->primaryPaymentInvoice->getAmountTotalWithoutPaymentMethodFee();
        }

        return Money::create((float)$this->amount + (float)$this->fee, $this->currency_iso);
    }

    public function getPaymentOrderIdentify(): string
    {
        return $this->order->id . 'P:' . $this->id;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isOwnForUser(User $user)
    {
        return $this->user_id === $user->id;
    }

    public function canBeCanceled()
    {
        return $this->status == self::STATUS_NEW;
    }
}