<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.05.17
 * Time: 12:27
 */

namespace common\models\relationsMappers;

use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\models\repositories\PrinterMaterialGroupRepository;

class PrinterMaterialRelationsMapper
{
    /** @var  PrinterMaterialGroupRepository */
    protected $printerMaterialGroupRepository;

    public function injectDependencies(PrinterMaterialGroupRepository $printerMaterialGroupRepository): void
    {
        $this->printerMaterialGroupRepository = $printerMaterialGroupRepository;

    }

    public function getPrinterMaterialGroup(PrinterMaterial $printerMaterial): PrinterMaterialGroup
    {
        if ($printerMaterial->isRelationPopulated('group')) {
            return $printerMaterial->group;
        }
        $printerMaterialGroup = $this->printerMaterialGroupRepository->getById($printerMaterial->printer_material_group_id);
        $printerMaterial->populateRelation('group', $printerMaterialGroup);
        return $printerMaterialGroup;
    }
}