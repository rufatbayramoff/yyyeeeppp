<?php

namespace common\models;

use common\components\ArrayHelper;
use common\interfaces\Model3dBaseInterface;
use frontend\components\cart\exceptions\CartEmptyException;

/**
 * Class Cart
 * @package common\models
 */
class Cart extends \common\models\base\Cart
{
    /**
     * Add position to cart
     *
     * @param $cartItem
     * @throws \Exception
     */
    public function addPosition(CartItem $cartItem)
    {
        $items = $this->cartItems;
        $items[] = $cartItem;
        $this->setCartItems($items);
    }

    /**
     * @param CartItem[] $cartItems
     */
    public function setCartItems($cartItems)
    {
        $this->populateRelation('cartItems', $cartItems);
    }

    /**
     * Clear cart
     */
    public function clear()
    {
        $this->setCartItems([]);
    }

    /**
     * Is cart empty
     * @return bool
     */
    public function getIsEmpty()
    {
        return count($this->cartItems)===0;
    }

    /**
     * Return machine of cart
     *
     * @return CompanyService
     */
    public function getMachine(): CompanyService
    {
        return $this->getFirstItem()->machine;
    }

    public function getQty()
    {
        return $this->getFirstItem()->qty;
    }

    /**
     * Return model of cart
     *
     * @return Model3dBaseInterface
     * @throws CartEmptyException
     */
    public function getModel3d()
    {
        return $this->getFirstItem()->model3dReplica;
    }
    /**
     * Return store unit of cart
     * @return \common\models\StoreUnit
     * @throws CartEmptyException
     */
    public function getStoreUnit()
    {
        return $this->getFirstItem()->model3dReplica->storeUnit;
    }

    /**
     * can change ps
     *
     * @return bool
     */
    public function canChangePs()
    {
        return $this->getFirstItem()->can_change_ps;
    }

    /**
     * @param bool $canChange
     */
    public function setCanChangePs($canChange)
    {
        $this->getFirstItem()->can_change_ps = $canChange ? 1 : 0;
    }

    public function afterSave($insert, $changedAttributes)
    {
        CartItem::deleteAll(['cart_id'=>$this->id]);
        foreach ($this->cartItems as $item) {
            $item->cart_id = $this->id;
            $item->safeSave();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Return first item, if it not exist - throw exception.
     * @return CartItem
     * @throws CartEmptyException
     */
    public function getFirstItem()
    {
        $this->checkNotEmpty();
        return ArrayHelper::first($this->cartItems);
    }

    public function getTotalQty()
    {
        $totalQty = 0;
        foreach($this->cartItems as $cartItem){
            $modelParts = $cartItem->model3dReplica->model3dParts;
            foreach($modelParts as $modelPart){
                $totalQty = $totalQty + $modelPart->qty;
            }
        }
        return $totalQty;
    }
    /**
     * Check that cart is not empty, if cart empty - throw exception
     * @throws CartEmptyException
     */
    public function checkNotEmpty()
    {
        if($this->getIsEmpty()){
            throw new CartEmptyException();
        }
    }
}