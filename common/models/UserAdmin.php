<?php
namespace common\models;

use Yii;

/**
 * Edit this file
 * This is the model class for table "user_admin".
 */
class UserAdmin extends \common\models\base\UserAdmin
{
    
      /**
     * user deleted,banned and etc.
     */
    const STATUS_DELETED = 0;
    
    /**
     * fully active user
     */
    const STATUS_ACTIVE = 10; 
    /**
     * user unactive - didn't use site long time (1 month?),
     * after login - mark as active
     */
    const STATUS_UNACTIVE = 5;    
     

    
    public $password;
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
     /**
     * get user statuses
     * @return array
     */
    public static function getStatuses()
    {
        $statuses = [
            self::STATUS_DELETED => _t('app', 'Deleted'),
            self::STATUS_ACTIVE => _t('app', 'Active'), 
            self::STATUS_UNACTIVE => _t('app', 'Unactive') 
        ];
        return $statuses;
    }
}