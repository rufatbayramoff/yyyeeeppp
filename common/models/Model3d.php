<?php

namespace common\models;

use common\components\DateHelper;
use common\components\Model3dHistoryConfigurator;
use common\interfaces\Model3dBaseInterface;
use common\modules\product\interfaces\ProductInterface;
use common\models\model3d\Model3dBase;
use common\models\query\Model3dQuery;
use common\modules\product\models\ProductTrait;
use lib\money\Currency;
use lib\money\Money;


/**
 * Model3d file with functions to work with bined files
 *
 * @property Model3dTexture $kitModel3dTexture
 * @property StoreUnit $storeUnitWithouMaster
 *
 * @property string $cuid
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $product_status
 * @property string $price_per_produce
 *
 * @property \common\models\User $user
 * @property \common\models\Company $company
 * @property \common\models\ProductCategory $productCategory
 *
 */
class Model3d extends \common\models\base\Model3d implements Model3dBaseInterface
{
    const SCENARIO_PUBLISH   = 'publish';
    const SCENARIO_UNPUBLISH = 'unpublished';

    public $canBePublishUpdated = true;

    use Model3dBase {
        rules as traitRules;
        setKitModelMaterialGroupAndColor as traitSetKitModelMaterialGroupAndColor;
    }

    use ProductTrait {

    }

    public const UID_PREFIX = 'M:';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'Model3dHistory' => Model3dHistoryConfigurator::getConfig(self::class),
        ];
    }

    /**
     * @param PrinterMaterialGroup $printerMaterialGroup
     * @param PrinterColor $printerColor
     */
    public function setKitModelMaterialGroupAndColor(PrinterMaterialGroup $printerMaterialGroup, PrinterColor $printerColor)
    {
        $this->traitSetKitModelMaterialGroupAndColor($printerMaterialGroup, $printerColor);
        $this->kitModel3dTexture->populateRelation('model3d', $this);
    }

    /**
     * @return array
     */
    public function rules()
    {
        $rules = array_merge(
            parent::rules(),
            $this->traitRules(),
            [
                [
                    ['id'],
                    'validateProductCategory',
                    'on' => self::SCENARIO_PUBLISH,
                ],
                [
                    ['id'],
                    'validateKitRules',
                    'on' => self::SCENARIO_PUBLISH
                ],
                [
                    ['description'],
                    'required',
                    'on' => self::SCENARIO_PUBLISH
                ],
            ]
        );
        return $rules;
    }

    public function scenarios()
    {
        $scenarios                           = parent::scenarios();
        $scenarios[self::SCENARIO_UNPUBLISH] = [];
        return $scenarios;
    }

    public function setImagesPublicMode($mode)
    {
        foreach ($this->getActiveModel3dImages() as $model3dImage) {
            $model3dImage->file->setPublicMode($mode);
            $model3dImage->file->safeSave();
        }
    }

    /**
     * /**
     * Publish model logic. Runs after moderator clicks Publish in admin.
     *
     * @return boolean
     * @throws \yii\base\Exception
     * @throws \yii\base\NotSupportedException
     */
    public function publish()
    {
        $this->tryValidatePublish();
        if (!$this->published_at) {
            $this->published_at = DateHelper::now();
        }
        $this->productCommon->product_status = ProductInterface::STATUS_PUBLISHED_PUBLIC;
        $userId                              = null;
        $msg                                 = 'Published';
        Model3dHistory::log($userId, $this, Model3dHistory::ACTION_PUBLISH, $msg);
        $this->safeSave();
        $this->productCommon->safeSave();
        $this->setImagesPublicMode(true);
    }

    /**
     * Unpublish model. Runs if admin clicks Is actve: No
     *
     * @return bool
     */
    public function unpublish()
    {
        $this->product_status = ProductInterface::STATUS_DRAFT;
        $this->updated_at     = DateHelper::now();
        $this->safeSave();
        $this->setImagesPublicMode(false);
    }


    /**
     *
     * @return bool
     */
    public function hasPublishUpdates()
    {
        return $this->hasPublishUpdates || $this->product_status === ProductInterface::STATUS_PUBLISH_PENDING;
    }


    /**
     * validate before publishing
     * called by backend
     *
     * @throws \yii\base\NotSupportedException
     */
    public function tryValidatePublish()
    {
        foreach ($this->getActiveModel3dParts() as $model3dPart) {
            if (empty($model3dPart->model3dPartProperties) || empty(floatval($model3dPart->model3dPartProperties->length))
            ) {
                throw new \yii\base\UserException('Not valid properties for 3D model file [' . $model3dPart->name . ']');
            }
        }
    }

    public function getRejectMessage()
    {
        $msg = 'Please contact us using feedback form. Thank you.';
        // get moderator log and find reject reason
        $log = ModerLog::find()->where(
            [
                'action'      => 'reject',
                'object_type' => 'model3d',
                'object_id'   => $this->id
            ]
        )->latest();
        if ($log) {
            $msg = $log['result'];
        }
        return $msg;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'model3d';
    }

    /**
     * @return Model3dQuery
     */
    public static function find()
    {
        return new Model3dQuery(get_called_class());
    }

    /**
     * @return bool
     */
    public function isCatalogCostAvailable()
    {
        if (!$this->isPublished()) {
            return true;
        }
        return false;
    }

    /**
     * @param $storeUnit
     */
    public function setMasterStoreUnit($storeUnit)
    {
        $this->populateRelation('storeUnit', $storeUnit);
        $this->master_store_unit_id = $storeUnit->id;
    }

    public function getStoreUnit()
    {
        if ($this->master_store_unit_id) {
            // Model3d copy
            return parent::getMasterStoreUnit()->inverseOf('model3d');
        }
        return parent::getStoreUnit();
    }

    public function getStoreUnitWithouMaster()
    {
        return parent::getStoreUnit();
    }

    /**
     * @return string
     */
    public function getViewBaseUrl()
    {
        return '3dmodels';
    }

    public function getSourceModel3d()
    {
        return $this->originalModel3d ?? $this;
    }

    public function getAuthor(): ?User
    {
        return $this->user;
    }

    public function getModel3dParts()
    {
        return parent::getModel3dParts()->orderBy('model3d_part.id asc');
    }

    public function setCantBePublisehedUpdated(): void
    {
        $this->canBePublishUpdated = false;
    }

    public function isCanBePublishUpdated(): bool
    {
        return $this->canBePublishUpdated;
    }

    public function getMoqFromPriceMoney(): ?Money
    {
        $currency = $this->productCommon->company ? $this->productCommon->company->currency : Currency::USD;
        return Money::create($this->price_per_produce, $currency);
    }

    public function getPriceMoneyByQty($qty): ?Money
    {
        $currency = $this->productCommon->company ? $this->productCommon->company->currency : Currency::USD;
        return Money::create($this->price_per_produce * $qty, $currency);
    }

    /**
     * @return Company
     */
    public function getCompany(): ?Company
    {
        return $this->productCommon->company ?? null;
    }

    public function getProductCategory(): ?ProductCategory
    {
        return $this->productCommon->category ?? null;
    }

    public function getUser(): ?User
    {
        return $this->productCommon->user ?? null;
    }

    public function getUser_id()
    {
        return $this->productCommon->user_id ?? 0;
    }

    public function getCreated_at()
    {
        return $this->productCommon->created_at ?? null;
    }

    public function getUpdated_at()
    {
        return $this->productCommon->updated_at ?? null;
    }

    public function getProduct_status()
    {
        return $this->productCommon->product_status ?? null;
    }

    public function getPrice_per_produce()
    {
        return $this->productCommon->single_price ?? null;
    }

    public function getCompanyManufacturer(): ?Company
    {
        return $this->company;
    }

    public function getMinOrderQty(): float
    {
        return 1;
    }

    public function getUnitTypeTitle($qty): string
    {
        return 'unit';
    }

    public function getUnitTypeCode(): string
    {
        return 'unit';
    }

    /**
     * @return bool
     */
    public function canDeleted(): bool
    {
        return !$this->model3dReplicas &&
            !$this->isByConverted() &&
            !$this->isPublished() &&
            (!$this->storeUnit || ($this->storeUnit && !$this->storeUnit->model3ds));
    }

    public function getIs_active()
    {
        return $this->productCommon->is_active ?? false;
    }

    public function getTitle(): string
    {
        return $this->productCommon->title ?? '';
    }

    public function getTitleLabel(): string
    {
        return $this->productCommon->title??'';
    }

    public function getDescription(): string
    {
        return $this->productCommon->description ?? '';
    }

    public function getCuid(): string
    {
        return $this->productCommon->uid;
    }

    public function setUser_id($userId)
    {
        $this->productCommon->user_id = $userId;
    }
}
