<?php

namespace common\models;

use yii\base\UserException;

/**
 * User and paypal information
 * - for tax checking
 * - for validating email and payouts
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserPaypal extends \common\models\base\UserPaypal
{

    const STATUS_NEW      = "new";
    const STATUS_CHECKING = "checking";
    const STATUS_FAILED   = "failed";
    const STATUS_ERROR    = "error";
    const STATUS_UNLINKED = 'unliked';
    const STATUS_OK       = "ok";


    public function behaviors()
    {
        return [
            'history' => [
                'class'        => \common\components\HistoryBehavior::className(),
                'historyClass' => UserPaypalHistory::className(),
                'foreignKey'   => 'user_paypal_id'
            ],
        ];
    }

    /**
     * validate user paypal by auth
     *
     *
     * @param int $userId
     * @param string $authCode
     * @return UserPaypal
     * @throws \Exception
     * @throws \yii\base\UserException
     */
    public static function validateByAuth($userId, $authCode)
    {
        $userPaypalOld = UserPaypal::findOne(['user_id' => $userId]);

        if ($userPaypalOld && $userPaypalOld->user && Payment::hasPendingPayouts($userPaypalOld->user)) {
            throw new UserException(
                _t('site.payout', 'Your PayPal email cannot be changed because you have pending payout transactions.')
            );
        }
        $userPaypal = $userPaypalOld ? $userPaypalOld : UserPaypal::addRecord([
            'created_at'   => dbexpr("NOW()"),
            "user_id"      => $userId,
            "check_status" => UserPaypal::STATUS_NEW
        ]);
        try {
            $clientId                 = param('paypal_client_id');
            $clientSecret             = param('paypal_secret');
            $apiContext               = \frontend\models\payment\PaypalLib::initApiContext($clientId, $clientSecret);
            $tokenInfoOrig            = new \PayPal\Api\OpenIdTokeninfo();
            $tokenInfo                = $tokenInfoOrig->createFromAuthorizationCode(
                ['code' => $authCode], $clientId, $clientSecret, $apiContext
            );
            $userPaypal->check_status = UserPaypal::STATUS_CHECKING;
            $userPaypal->safeSave();

            $userInfo = \PayPal\Api\OpenIdUserinfo::getUserinfo(
                ['access_token' => $tokenInfo->getAccessToken()], $apiContext
            );
            if ($userInfo) {
                /**
                 *     [postal_code] => 95131
                 * [locality] => San Jose
                 * [region] => CA
                 * [country] => US
                 * [street_address] => 1 Main St
                 */
                $address                  = $userInfo->getAddress();
                $userPaypal->email        = $userInfo->getEmail();
                $userPaypal->fullname     = $userInfo->getName();
                $userPaypal->country_iso  = $address->getCountry();
                $userPaypal->check_status = self::checkPaypalStatus($userPaypal);
                $userPaypal->safeSave();
            } else {
                throw new \yii\base\UserException("No user information");
            }
        } catch (\Exception $e) {
            UserPaypal::updateRecord($userPaypal->id, ['check_status' => UserPaypal::STATUS_ERROR]);
            throw $e;
        }
        return $userPaypal;
    }

    /**
     * find out paypal status check
     *
     * @param \common\models\UserPaypal $userPaypal
     * @return string - returns status for check_status column in user_paypal table
     */
    public static function checkPaypalStatus(UserPaypal $userPaypal)
    {
        $user        = User::findOne(['id' => $userPaypal->user_id]);
        $checkStatus = UserPaypal::STATUS_OK;
        // no tax information
        if (empty($user->userTaxInfo)) {
            return UserPaypal::STATUS_OK;
        }
        // users tax info country != paypal country
        $paypalCountry = $userPaypal->country_iso;
        if ($paypalCountry === 'C2') {
            // https://developer.paypal.com/docs/classic/api/country_codes/ - C2 is China
            $paypalCountry = 'CN';
        }
        if ($user->userTaxInfo->place_country != $paypalCountry) {
            // $checkStatus = UserPaypal::STATUS_FAILED;
            // we send message to support
            $emailer = new \common\components\Emailer();
            $message = sprintf("Paypal country check failed. \n"
                . "User id:  %d\n"
                . "User email: %s\n"
                . "User country: %s, Paypal country: %s",
                $userPaypal->user_id, $user->email, $user->userTaxInfo->place_country,
                $userPaypal->country_iso);

            $emailer->sendSupportMessage("Paypal check - Failed ", $message);
        }
        return $checkStatus;
    }

    /**
     * @param UserPaypal $userPaypal
     * @return bool
     */
    public static function isUserPaypalOk($userPaypal): bool
    {
        return $userPaypal && $userPaypal->check_status == self::STATUS_OK;
    }
}