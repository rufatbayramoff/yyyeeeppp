<?php

namespace common\models;

use yii\helpers\Url;

/**
 * Class CompanyVerify
 * @package common\models
 */
class CompanyVerify extends \common\models\base\CompanyVerify
{
    const METHOD_EMAIL = 'email';
    const METHOD_PHONE_CALL = 'call';
    const METHOD_SMS = 'sms';
    const METHOD_MANUAL = 'manual';
}