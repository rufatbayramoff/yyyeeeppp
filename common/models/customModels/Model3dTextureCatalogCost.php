<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 07.11.16
 * Time: 14:05
 */

namespace common\models\customModels;


use common\models\base\Model3dTexture;

class Model3dTextureCatalogCost extends Model3dTexture
{
    // Only default infill
    public $infill = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'model3d_texture_catalog_cost';
    }
}