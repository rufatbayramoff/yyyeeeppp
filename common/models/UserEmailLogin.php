<?php

namespace common\models;

/**
 * Class UserEmailLogin
 * @package common\models
 */
class UserEmailLogin extends \common\models\base\UserEmailLogin
{

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [ ['email'], 'email' ];
        return $rules;
    }

    public function getLoginLinkByHash($confirmed = false)
    {
        $params = [
            'hash' => $this->hash
        ];
        if ($confirmed) {
            $params['confirmed'] = $confirmed;
        }

        return param('server') . '/user/email-login?' . http_build_query($params);
    }

    public function equalUser(User $user): bool
    {
        return $this->user_id == $user->id;
    }
}
