<?php

namespace common\models;

use frontend\modules\mybusiness\components\ImageRotateTrait;

/**
 * Class ProductImage
 * @package common\models
 */
class ProductImage extends \common\models\base\ProductImage
{
    use ImageRotateTrait;
}