<?php

namespace common\models;

use common\interfaces\FileBaseInterface;

/**
 * Class ConnectedApp
 *
 * @package common\models
 * @property \common\models\File $logoFile
 */
class ConnectedApp extends \common\models\base\ConnectedApp
{
    public $logoFileDelete = false;

    public function load($data, $formName = null)
    {
        if (array_key_exists($this->formName(), $data)) {
            $formData = $data[$formName ?: $this->formName()];

            if (array_key_exists('logoFileDelete', $formData) && $formData['logoFileDelete']) {
                if ($this->logoFile) {
                    $this->logoFile->forDelete = true;
                }
            }
        }
        return parent::load($data, $formName);
    }

    /**
     * @param FileBaseInterface|null $logoFile
     */
    public function setLogoFile($logoFile)
    {
        $this->populateRelation('logoFile', $logoFile);
    }
}