<?php

namespace common\models;

/**
 * Class WikiMaterialPhoto
 *
 * @property \common\models\FileAdmin $file
 * @package common\models
 */
class WikiMaterialPhoto extends \common\models\base\WikiMaterialPhoto
{
    public $forDelete = false;


    public function setFile($file)
    {
        $this->populateRelation('file', $file);
        $this->file_uuid = $file->uuid;
    }

}