<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.08.16
 * Time: 14:49
 */
namespace common\models\orders\events;

use common\models\DeliveryType;
use lib\geo\models\Location;

class DeliveryInfo
{
    /**
     * @var Location
     */
    public $deliveryLocation;

    /**
     * @var DeliveryType;
     */
    public $deliveryMethod;
}