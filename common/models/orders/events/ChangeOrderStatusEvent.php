<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 14.10.15
 * Time: 16:57
 */

namespace common\models\orders\events;


use common\components\exceptions\AssertHelper;
use common\models\StoreOrder;
use yii\base\Event;

/**
 * Event after order change status and saved
 * @package common\models\orders
 */
class ChangeOrderStatusEvent extends Event
{
    const INITIATOR_SYSTEM = 0;
    const INITIATOR_MODERATOR = 1;
    const INITIATOR_CLIENT = 2;
    const INITIATOR_PS = 3;

    /**
     * Old status of order
     * @var string
     */
    private $oldStatus;

    /**
     * New status of order
     * @var string
     */
    private $newStatus;

    /**
     * Producer of event
     * @var int
     */
    private $initiator;

    /**
     * Constructor
     * @param string $oldStatus
     * @param string $newStatus
     * @param int    $initiator Iinitiator of change status
     */
    public function __construct($oldStatus, $newStatus, $initiator)
    {
        AssertHelper::assertInArray($initiator, [self::INITIATOR_SYSTEM, self::INITIATOR_CLIENT, self::INITIATOR_PS, self::INITIATOR_MODERATOR]);
        parent::__construct([]);
        $this->oldStatus = $oldStatus;
        $this->newStatus = $newStatus;
        $this->initiator = $initiator;
    }

    /**
     * get text by id
     *
     * @param $id
     * @return mixed
     */
    public static function getInitiatorLabel($id)
    {
        $list =  [
            0 => 'System',
            1 => 'Moderator',
            2 => 'Client',
            3 => 'PS'
        ];
        return isset($list[$id]) ? $list[$id] : $id;
    }
    /**
     * @return string
     */
    public function getOldStatus()
    {
        return $this->oldStatus;
    }

    /**
     * @return string
     */
    public function getNewStatus()
    {
        return $this->newStatus;
    }

    /**
     * @return StoreOrder
     */
    public function getOrder()
    {
        return $this->sender;
    }

    /**
     * @return int
     */
    public function getInitiator()
    {
        return $this->initiator;
    }
}