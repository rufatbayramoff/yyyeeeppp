<?php
namespace common\models;

use common\components\DateHelper;
use Yii;

/**
 * Edit this file
 * This is the model class for table "ps_printer_history".
 */
class PsPrinterHistory extends \common\models\base\PsPrinterHistory
{
    public const DELETE_DELIVERY_OPTION = 'delete_delivery_option';

    public static function saveHistory($psPrinterId, $userId, $actionId)
    {
        $model = new PsPrinterHistory();
        $model->ps_printer_id = $psPrinterId;
        $model->user_id = $userId;
        $model->created_at = DateHelper::now();
        $model->action_id = $actionId;
        $model->safeSave();
    }

    /**
     * @param $psPrinterId
     * @param $userId
     * @param string $data
     * @throws \yii\base\UserException
     */
    public static function saveRemoveDelivery($psPrinterId, $userId, string $data): void
    {
        $model = new PsPrinterHistory();
        $model->ps_printer_id = $psPrinterId;
        $model->user_id = $userId;
        $model->created_at = DateHelper::now();
        $model->action_id = static::DELETE_DELIVERY_OPTION;
        $model->data = $data;
        $model->safeSave();
    }
}