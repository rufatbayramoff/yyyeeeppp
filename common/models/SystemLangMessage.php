<?php
namespace common\models;

use Yii;

/**
 * Edit this file
 * This is the model class for table "system_lang_message".
 *
 * @property \common\models\SystemLangSource $systemLangSource
 */
class SystemLangMessage extends \common\models\base\SystemLangMessage
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(SystemLangSource::className(), ['id' => 'id']);   
    }

    public function getSystemLangSource()
    {
        return $this->getId0();
    }

    public function getUsagePagesCount()
    {
        return $this->systemLangSource->getSystemLangPages()->count();
    }
}