<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.08.17
 * Time: 15:01
 */

namespace common\models\validators;

use common\models\CompanyService;
use yii\web\HttpException;

class PsMachineValidator
{
    public function tryValidate(CompanyService $psMachine)
    {
        if (!$psMachine->psMachineDeliveries) {
            throw new HttpException(400, _t('ps.profile', 'Please select one of the delivery methods'));
        }
    }
}