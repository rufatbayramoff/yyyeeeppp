<?php

namespace common\models;

use common\modules\dynamicField\models\json\DynamicFieldTypeManager;
use yii\helpers\Json;

/**
 * Class DynamicField
 *
 * @package common\models
 */
class DynamicField extends \common\models\base\DynamicField
{

    const TYPE_NUMBER  = 'number';
    const TYPE_STRING  = 'string';
    const TYPE_ENUM    = 'enum';
    const TYPE_BOOLEAN = 'boolean';

    public $bindedModelList = [
        Product::class
    ];

    public $typeList = [
        self::TYPE_NUMBER,
        self::TYPE_STRING,
        self::TYPE_ENUM,
        self::TYPE_BOOLEAN,
    ];

    /**
     * This is DynamicFieldCategory current bind to dynamic field
     *
     * @var DynamicFieldCategory
     */
    public $bindDynamicFieldCategory;


    public function getBindedModelList()
    {
        return array_combine($this->bindedModelList, $this->bindedModelList);
    }

    public function getTypeList()
    {
        return array_combine($this->typeList, $this->typeList);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                ['type_params'],
                'checkTypeParams'
            ]
        ]);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function checkTypeParams()
    {
        $typeObj = DynamicFieldTypeManager::getDynamicFieldTypeObj($this->type);
        if ($typeObj) {
            $error = $typeObj->validate($this->type_params);
            if ($error) {
                $this->addError('type_params', $error);
            }

            $error = $typeObj->validateValue($this->default_value, $this->type_params);
            if ($error) {
                $this->addError('default_value', $error);
            }
        }
    }

    public function getIsRequared()
    {
        if ($this->bindDynamicFieldCategory && $this->bindDynamicFieldCategory->is_required !== null) {
            return $this->bindDynamicFieldCategory->is_required;
        }
        return $this->is_required;
    }

    public function getDefaultValue()
    {
        if ($this->bindDynamicFieldCategory && ($this->bindDynamicFieldCategory->default_value || $this->bindDynamicFieldCategory->default_value==='0')) {
            return $this->bindDynamicFieldCategory->default_value;
        }
        return $this->default_value;
    }

    /**
     * @return array
     */
    public function getTypeParams()
    {
        if ($this->bindDynamicFieldCategory && $this->bindDynamicFieldCategory->type_params) {
            return $this->bindDynamicFieldCategory->type_params;
        }
        return $this->type_params;
    }
}