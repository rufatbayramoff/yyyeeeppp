<?php

namespace common\models;

use common\models\repositories\FileRepository;

/**
 * Class CompanyCertification
 *
 * @package common\models
 */
class CompanyCertification extends \common\models\base\CompanyCertification
{

    public function afterDelete()
    {
        if ($this->file) {
            $fileRepo = new FileRepository();
            $fileRepo->delete($this->file);
        }
        return parent::afterDelete();
    }
}