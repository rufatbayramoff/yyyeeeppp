<?php

namespace common\models;

use common\components\BaseAR;
use common\models\message\exceptions\BindedObjectNotFound;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;
use yii\base\Exception;

/**
 * @property MsgMessage|null lastMessage last message in topic
 * @property MsgMember[] $members
 * @property MsgMessage[] $messages
 */
class MsgTopic extends \common\models\base\MsgTopic
{
    // Bot message topic
    const BOT_MESSAGE_ALL       = 1;
    const BOT_MESSAGE_COMPANIES = 2;

    /**
     * Binded object
     */
    const BIND_OBJECT_ORDER        = 'order';
    const BIND_OBJECT_CNC_PREORDER = 'cnc_preorder';
    const BIND_OBJECT_PREORDER     = 'preorder';


    public $isBlocked;

    /**
     * Cache for binded object
     *
     * @var mixed
     * @todo replace by https://redmine.tsdev.work/issues/441
     */
    private $bindedObject;

    /**
     * Class map for binding object
     *
     * @var string[]
     * @todo replace by https://redmine.tsdev.work/issues/441
     */
    public static $bindObjectClasses = [
        self::BIND_OBJECT_ORDER        => StoreOrder::class,
        self::BIND_OBJECT_PREORDER     => Preorder::class,
    ];

    /**
     * Relation of topic's members
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(MsgMember::class, ['topic_id' => 'id']);
    }

    /**
     * Relation of topic's messages
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(MsgMessage::class, ['topic_id' => 'id']);
    }

    /**
     * Relation for last message in topic
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLastMessage()
    {
        return $this->hasOne(MsgMessage::class, ['topic_id' => 'id'])->orderBy('created_at DESC');
    }

    /**
     * Return other users in dialog
     *
     * @param User $user user who excluded form list of topic's users
     * @return User[]
     */
    public function getOtherUsers(User $user)
    {
        $users = [];

        if ($this->isBotSupport()) {
            return [User::find()->where(['id'=>1])->one()];
        }

        foreach ($this->getMembers()->limit(100)->all() as $member) {
            if ($member->user_id != $user->id) {
                $users[] = $member->user;
            }
        }
        return $users;
    }

    public function getChatUsersList(User $user)
    {
        $users = [];

        if ($this->isBotSupport()) {
            $supportUser =User::find()->where(['id'=>1])->one();
            return  ['<a href="' . HL(UserUtils::getUserPublicProfileUrl($supportUser)) . '" target="_parent">' . H(UserFacade::getFormattedUserName($supportUser)) . '</a>'];
        }

        foreach ($this->members as $member) {
            if($member->user_id === $user->id) {
                continue;
            }
            $activeCompany = $member->user->company ?? null;
            if($activeCompany && !$activeCompany->isDeleted()) {
                $users[] = '<a href="' . HL($member->user->company->getPublicCompanyLink()) . '" target="_parent">' . H($member->user->company->title) . '</a>';
            } else {
                $users[] = '<a href="' . HL(UserUtils::getUserPublicProfileUrl($member->user)) . '" target="_parent">' . H(UserFacade::getFormattedUserName($member->user)) . '</a>';

            }
        }
        return $users;
    }

    /**
     * Check that user is member of this topic
     *
     * @param User $user
     * @return bool
     */
    public function getIsMember(User $user)
    {
        $msgMember = $this->getMembers()->andWhere(['msg_member.user_id' => $user->id])->one();
        return $msgMember ? true : false;
    }

    /**
     * Return member for user. If it not found - throw exception
     *
     * @param User $user
     * @return \common\models\MsgMember
     * @throws Exception
     */
    public function getMemberForUser(User $user)
    {
        $msgMember = $this->getMembers()->andWhere(['msg_member.user_id' => $user->id])->one();
        if (!$msgMember) {
            throw new Exception('User is not member for this topic');
        }
        return $msgMember;
    }


    /**
     * Resolve and cache binded object
     *
     * @return null|mixed
     * @throws BindedObjectNotFound
     * @todo replace by https://redmine.tsdev.work/issues/441
     */
    public function getBindedObject()
    {
        if (!$this->isBinded()) {
            return null;
        }

        if (!$this->bindedObject) {
            /** @var BaseAR $objectClass */
            $objectClass = MsgTopic::$bindObjectClasses[$this->bind_to];
            $object      = $objectClass::findByPk($this->bind_id);
            if (!$object) {
                return null;
                //throw new BindedObjectNotFound();
            }
            $this->bindedObject = $object;
        }

        return $this->bindedObject;
    }

    /**
     * Is topic binded to object
     *
     * @return bool
     */
    public function isBinded(): bool
    {
        return $this->bind_id !== null;
    }

    public function isBotSupport()
    {
        return $this->id === MsgTopic::BOT_MESSAGE_ALL || $this->id === MsgTopic::BOT_MESSAGE_COMPANIES;
    }

    /**
     * @return bool
     */
    public function isOrder(): bool
    {
        return $this->bind_to === MsgTopic::BIND_OBJECT_ORDER;
    }

    /**
     * @return bool
     */
    public function isPreorder(): bool
    {
        return $this->bind_to === MsgTopic::BIND_OBJECT_PREORDER;
    }
}