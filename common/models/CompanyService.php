<?php

namespace common\models;

use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\components\exceptions\BusinessException;
use common\components\exceptions\IllegalStateException;
use common\components\exceptions\ValidationException;
use common\interfaces\machine\DeliveryParamsInterface;
use common\models\factories\UserLocationFactory;
use common\models\loggers\PsMachineLogger;
use common\models\machines\MachineDeliveryParamsTrait;
use common\models\repositories\FileRepository;
use common\modules\cnc\repositories\PsCncMachineRepository;
use common\modules\company\components\CompanyServiceInterface;
use common\modules\company\helpers\CompanyServiceUrlHelper;
use common\modules\company\repositories\CompanyServiceTypesRepository;
use common\services\PsPrinterService;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserSessionFacade;
use lib\money\Currency;
use lib\money\Money;
use yii\base\UserException;
use yii\data\ArrayDataProvider;
use yii\helpers\Inflector;
use yii\helpers\Url;

/**
 * Class PsMachine
 *
 * @property string $titleLabel
 * @property \common\models\Company $company
 * @property PsMachineDelivery[] $deliveryTypess Allowed delivery types
 * @property PsMachineDeliveryCountry[] $psMachineDeliveryCountry
 * @property \common\models\File[] $imageFiles
 * @property \common\models\CompanyCertification[] $certifications
 * @package common\models
 */
class CompanyService extends base\CompanyService implements DeliveryParamsInterface, CompanyServiceInterface
{
    use MachineDeliveryParamsTrait;

    //  use MachineDeliveryParamsTrait;
    const ALLOWED_TYPES                          = [
        CompanyServiceInterface::TYPE_CNC     => CompanyServiceInterface::TYPE_CNC,
        CompanyServiceInterface::TYPE_SERVICE => CompanyServiceInterface::TYPE_SERVICE,
        CompanyServiceInterface::TYPE_PRINTER => CompanyServiceInterface::TYPE_PRINTER
    ];

    const MODERATOR_STATUS_NEW                   = 'new';         // Just created
    const MODERATOR_STATUS_PENDING               = 'pending';     // Pending moderation, not published
    const MODERATOR_STATUS_APPROVED              = 'approved';
    const MODERATOR_STATUS_APPROVED_REVIEWING    = 'approvedReviewing';
    const MODERATOR_STATUS_REJECTED              = 'rejected';
    const MODERATOR_STATUS_REJECTED_REVIEWING    = 'rejectedReviewing';
    const MODERATOR_STATUS_UNAVAILABLE_REVIEWING = 'unavailableReviewing';
    const MODERATOR_STATUS_DRAFT                 = 'draft';

    const MSTATUS_UPDATED = 'updated';
    const MSTATUS_CHECKING = 'checking';
    const MSTATUS_CHECKED = 'checked';
    const MSTATUS_BANNED = 'banned';


    const VISIBILITY_EVERYWHERE = 'everywhere';
    const VISIBILITY_WIDGETONLY = 'widgetOnly';
    const VISIBILITY_NOWHERE    = 'nowhere';

    const AVAILABILITY_STATUS_ACTIVE  = 'active';
    const AVAILABILITY_STATUS_PUBLIC  = 'public';
    const AVAILABILITY_STATUS_DELETED = 'deleted';
    const AVAILABILITY_STATUS_ANY     = 'any';

    const        VISIBILITY_TYPES    = [
        self::VISIBILITY_EVERYWHERE => self::VISIBILITY_EVERYWHERE,
        self::VISIBILITY_WIDGETONLY => self::VISIBILITY_WIDGETONLY,
        self::VISIBILITY_NOWHERE    => self::VISIBILITY_NOWHERE,
    ];
    const        UNIT_TYPE_HOUR      = 'hour';
    public const IS_NOT_DELETED_FLAG = 0;

    public const CERT_TYPE_REJECTED      = 'rejected';
    public const CERT_TYPE_VERIFIED      = 'verified';
    public const CERT_TYPE_PROGRESS      = 'progress';
    public const CERT_TYPE_EXPIRED       = 'expired';
    public const CERT_EXPIRE_PERIOD_DAYS = 30 * 12;


    public function getUnitTypeTitle($withPlural = true)
    {
        $qty      = $this->getMinOrderQty();
        $unitType = $this->unit_type;
        if ($qty > 1 && $withPlural) {
            $titles = self::getUnitTypesList(true);
        } else {
            $titles = self::getUnitTypesList(false);
        }
        return ($titles[$unitType] ?? $this->unit_type) ?? '';
    }

    public static function getUnitTypesList($plural = false)
    {
        $result = [
            self::UNIT_TYPE_HOUR => _t('common.product', "hour"),
            'piece'              => _t('common.product', "piece"),
            "pack"               => _t('common.product', "pack"),
            "kg"                 => _t('common.product', "kg"),
            "lb"                 => _t('common.product', "lb"),
            "m"                  => _t('common.product', "m"),
            "ft"                 => _t('common.product', "ft"),
            "m2"                 => _t('common.product', "m2"),
            "ft2"                => _t('common.product', "ft2"),
            "m3"                 => _t('common.product', "m3"),
            "ft3"                => _t('common.product', "ft3"),
            "l"                  => _t('common.product', "liter"),
            "gallon"             => _t('common.product', "gallon")
        ];
        if ($plural) {
            $result = [
                self::UNIT_TYPE_HOUR => _t('common.product', "hours"),
                'piece'              => _t('common.product', "pieces"),
                "pack"               => _t('common.product', "packs"),
                "kg"                 => _t('common.product', "kgs"),
                "lb"                 => _t('common.product', "lbs"),
                "m"                  => _t('common.product', "m"),
                "ft"                 => _t('common.product', "ft"),
                "m2"                 => _t('common.product', "m2"),
                "ft2"                => _t('common.product', "ft2"),
                "m3"                 => _t('common.product', "m3"),
                "ft3"                => _t('common.product', "ft3"),
                "l"                  => _t('common.product', "liters"),
                "gallon"             => _t('common.product', "gallons")
            ];
        }

        return $result;
    }

    /**
     * status transaction after updating
     *
     * @return array
     */
    public function getModeratorStatusAfterUpdate()
    {
        $moderatorStatusUpdateTransaction = [
            self::MODERATOR_STATUS_NEW                   => self::MODERATOR_STATUS_PENDING,
            self::MODERATOR_STATUS_DRAFT                 => self::MODERATOR_STATUS_PENDING,
            self::MODERATOR_STATUS_PENDING               => self::MODERATOR_STATUS_PENDING,
            self::MODERATOR_STATUS_APPROVED              => self::MODERATOR_STATUS_APPROVED_REVIEWING,
            self::MODERATOR_STATUS_APPROVED_REVIEWING    => self::MODERATOR_STATUS_APPROVED_REVIEWING,
            self::MODERATOR_STATUS_REJECTED              => self::MODERATOR_STATUS_REJECTED_REVIEWING,
            self::MODERATOR_STATUS_UNAVAILABLE_REVIEWING => self::MODERATOR_STATUS_UNAVAILABLE_REVIEWING,
            self::MODERATOR_STATUS_REJECTED_REVIEWING    => self::MODERATOR_STATUS_REJECTED_REVIEWING
        ];
        return !empty($moderatorStatusUpdateTransaction[$this->moderator_status]) ? $moderatorStatusUpdateTransaction[$this->moderator_status] : $this->moderator_status;
    }

    public static function  moderatedStatusList()
    {
        return [
            self::MODERATOR_STATUS_APPROVED,
            self::MODERATOR_STATUS_APPROVED_REVIEWING
        ];
    }

    /**
     * Pending moderation status list
     *
     * @return string[]
     */
    public static function pendingModerationStatusList()
    {
        return [
          self:: MODERATOR_STATUS_PENDING,
            self::MODERATOR_STATUS_APPROVED_REVIEWING,
            self::MODERATOR_STATUS_REJECTED_REVIEWING,
            self::MODERATOR_STATUS_UNAVAILABLE_REVIEWING
        ];
    }

    /**
     * @return array
     */
    public static function getVisibilityLabels()
    {
        return [
            self::VISIBILITY_EVERYWHERE => _t('ps.delivery', 'Everywhere'),
            self::VISIBILITY_WIDGETONLY => _t('ps.delivery', 'Widget only'),
            self::VISIBILITY_NOWHERE    => _t('ps.delivery', 'No'),
        ];
    }


    public static function getModeratorStatusLabels()
    {
        return [
            self::MODERATOR_STATUS_NEW                   => _t('ps.profile', 'New'),
            self::MODERATOR_STATUS_PENDING               => _t('ps.profile', 'Pending moderation'),
            self::MODERATOR_STATUS_APPROVED              => _t('ps.profile', 'Approved'),
            self::MODERATOR_STATUS_APPROVED_REVIEWING    => _t('ps.profile', 'Pending (reviewing updates)'),
            self::MODERATOR_STATUS_REJECTED              => _t('ps.profile', 'Rejected'),
            self::MODERATOR_STATUS_REJECTED_REVIEWING    => _t('ps.profile', 'Pending (reviewing updates)'),
            self::MODERATOR_STATUS_UNAVAILABLE_REVIEWING => _t('ps.profile', 'Pending (reviewing updates)'),
            self::MODERATOR_STATUS_DRAFT                 => _t('ps.profile', 'Draft')
        ];
    }

    public function getVisibilityLabel()
    {
        return $this->visibility ? $this->getVisibilityLabels()[$this->visibility] : '';
    }

    public function getModeratorStatusLabel()
    {
        return $this->moderator_status ? $this->getModeratorStatusLabels()[$this->moderator_status] : '';
    }


    public function rules()
    {
        if ($this->type === self::TYPE_CUTTING) {
            return array_merge(parent::rules(), [
                [['title', 'description'], 'required'],
            ]);
        }
        if ($this->type !== self::TYPE_SERVICE) {
            return parent::rules();
        }
        return array_merge(parent::rules(), [
            [['moq_prices'], 'required', 'message' => _t('mybusiness.product', 'Price information cannot be blank.')],
            [['moq_prices'], 'validateMoqPrices'],
        ]);
    }

    public function beforeValidate()
    {
        $this->is_location_change = (int)$this->is_location_change;
        return parent::beforeValidate();
    }

    public function validateMoqPrices()
    {
        if (empty($this->moq_prices)) {
            return;
        }
        foreach ($this->moq_prices as $moqPrice) {
            if (!is_numeric($moqPrice['moq'])) {
                $this->addError('moq_prices', _t('mybusiness.product', 'Min. order qty is invalid'));
            }
            if (empty($moqPrice['priceWithFee']) || !is_numeric($moqPrice['priceWithFee'])) {
                $this->addError('moq_prices', _t('mybusiness.product', 'Price per unit is invalid'));
            }
        }
    }

    public function beforeSaveMethod($insert)
    {
        if (!$this->isNewRecord) {
            $psMachineLogger = new PsMachineLogger();
            $psMachineLogger->log($this);
        }
        return parent::beforeSaveMethod($insert); // TODO: Change the autogenerated stub
    }


    /**
     * Allowed delivery types
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryTypess()
    {
        return $this->hasMany(PsMachineDelivery::className(), ['ps_machine_id' => 'id'])
            ->joinWith(
                [
                    'deliveryType' => function (BaseActiveQuery $query) {
                        $query->andWhere([DeliveryType::column('is_active') => 1]);
                    }
                ],
                false
            );
    }


    public function setLocation(UserLocation $location)
    {
        $this->populateRelation('location', $location);
    }

    public function setPsPrinter(PsPrinter $psPrinter)
    {
        $this->populateRelation('psPrinter', $psPrinter);
    }

    public function setCuttingMachine(CuttingMachine $cuttingMachine)
    {
        $this->populateRelation('cuttingMachine', $cuttingMachine);
    }

    public function setPsCncMachine(PsCncMachine $psCncMachine)
    {
        $this->populateRelation('psCncMachine', $psCncMachine);
    }

    public function setPs($ps)
    {
        $this->populateRelation('ps', $ps);
        $this->ps_id = $ps->id;
    }

    public function setPsMachineDeliveries($psMachineDeliveries)
    {
        $this->populateRelation('psMachineDeliveries', $psMachineDeliveries);
    }

    /**
     * @return DeliveryParamsInterface
     */
    public function asDeliveryParams(): DeliveryParamsInterface
    {
        return $this;
    }

    /**
     * @return PsPrinter
     * @throws IllegalStateException
     */
    public function asPrinter()
    {
        if (!$this->isPrinter()) {
            throw new IllegalStateException("This machine is cnc router");
        }
        return $this->psPrinter;
    }

    /**
     * @return PsCncMachine
     * @throws IllegalStateException
     */
    public function asCnc(): PsCncMachine
    {
        if (!$this->isCnc()) {
            throw new IllegalStateException("This machine is cnc router");
        }

        return \Yii::$container->get(PsCncMachineRepository::class)->getByPsMachine($this);
    }

    /**
     * @return mixed|string
     */
    public function getTitleLabel(): string
    {
        if ($this->title) {
            return $this->title;
        }
        if ($this->type == self::TYPE_PRINTER && $psPrinter = $this->asPrinter()) {
            return $psPrinter->title;
        }
        if ($this->type == self::TYPE_WINDOW) {
            return $this->company->getTitle();
        }
        if ($this->type == self::TYPE_CNC) {
            return _t('site.ps', 'CNC Service');
        }
        if ($this->id) {
            return 'Service #' . $this->id;
        }
        return '';
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->company->currency;
    }

    /**
     * This machine is CNC
     *
     * @return bool
     */
    public function isCnc(): bool
    {
        return self::TYPE_CNC == $this->type;
    }

    public function isCutting(): bool
    {
        return self::TYPE_CUTTING == $this->type;
    }

    /**
     * This machine is 3D printer
     *
     * @return bool
     */
    public function isPrinter(): bool
    {
        return self::TYPE_PRINTER == $this->type;
    }

    /**
     * Return delivery options hash
     *
     * @return string
     */
    public function getDeliveryOptionsHash()
    {
        $data = '';
        foreach ($this->deliveryTypess as $type) {
            $data .= serialize($type->getAttributes());
        }
        return md5($data);
    }

    /**
     * @param $lat
     * @param $lon
     * @param bool|true $withMeasure
     * @return string|float
     */
    public function getDistance($lat, $lon, $withMeasure = true)
    {
        if (!$this->location) {
            return '';
        }
        $distance = \lib\geo\LocationUtils::getDistance(
            $lat,
            $lon,
            $this->location->lat,
            $this->location->lon
        );
        $m        = UserSessionFacade::getMeasurement() == 'in' ? _t('site.printer', 'miles') : _t('site.printer', 'km');
        if (UserSessionFacade::getMeasurement() == 'in') {
            $distance = \lib\MeasurementUtil::kmToMiles($distance);
        }
        $distance = round($distance, 2);
        if ($withMeasure) {
            $distance = $distance . ' ' . $m;
        }
        return $distance;
    }


    /**
     * @param User|null $user
     * @param bool $forWidget
     * @return bool
     */
    public function isVisibleForUser(User $user = null, $forWidget = false): bool
    {
        if ($this->visibility == self::VISIBILITY_EVERYWHERE) {
            return true;
        }

        if ($forWidget && $this->visibility == self::VISIBILITY_WIDGETONLY) {
            return true;
        }

        if (!$user) {
            return false;
        }

        if ($this->ps->user->equals($user)) {
            return true;
        }

        return false;
    }

    public function getImageFiles()
    {
        $images = $this->companyServiceImages;
        uasort($images, function ($a, $b) {
            return $a->pos > $b->pos;
        });
        $files = ArrayHelper::getColumn($images, 'file');
        return $files;
    }

    /**
     * @return File|null
     */
    public function getImageCoverFile(): ?File
    {
        /** @var CompanyServiceImage $image */
        $image = $this->getCompanyServiceImages()->orderBy(['pos' => SORT_ASC])->one();
        return $image && $image->file ? $image->file : null;
    }

    /**
     * @param int $w
     * @param int $h
     *
     * @return string|null
     */
    public function getImageCoverUrl($w = ImageHtmlHelper::DEFAULT_WIDTH, $h = ImageHtmlHelper::DEFAULT_HEIGHT): ?string
    {
        $file = $this->getImageCoverFile();

        if ($file) {
            return ImageHtmlHelper::getThumbUrlForFile($file, $w, $h);
        }

        return null;
    }

    /**
     * Return list of images (File instances) attached to this service as portfolio images.
     *
     * @param int $limit
     * @param bool $sortForPrimary If true and service has primary image - this image will set as first image.
     * @return File[]
     */
    public function getImages($limit = 100, bool $sortForPrimary = false): array
    {
        $fileIds = CompanyServiceImage::find()
            ->select('file_id')
            ->where(['company_service_id' => $this->id])
            ->column();

        if (empty($fileIds) && $this->ps) {
            // get from company profile
            return $this->ps->getPicturesFiles();
        }

        /** @var File[] $files */
        $files = File::find()
            ->id($fileIds)
            ->limit($limit)
            ->all();

        // if service has primary image - we set it as first image

        if ($this->primary_image_id && $sortForPrimary) {
            $files              = ArrayHelper::index($files, 'id');
            $primaryImageFileId = $this->primaryImage->file_id;
            if (isset($files[$primaryImageFileId])) {
                $primaryFile = $files[$primaryImageFileId];
                unset($files[$primaryImageFileId]);
                $files = array_values($files);
                array_unshift($files, $primaryFile);
            }
        }

        return $files;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getOwner()
    {
        return $this->ps->user;
    }

    public function getType()
    {
        return $this->type;
    }

    public function isTypeWindow()
    {
        return $this->type === CompanyServiceInterface::TYPE_WINDOW;
    }

    /**
     * @return BaseActiveQuery|File|null
     */
    public function getLogoImage()
    {
        $logo = $this->primaryImage ? $this->primaryImage->file : null;
        if (!$logo) {
            // get company logo
            return $this->ps->getCircleImage($this->ps);
        }
        return ImageHtmlHelper::getThumbUrlForFile($logo, 64, 64);
    }

    public function isRejected()
    {
        return $this->moderator_status == self::MODERATOR_STATUS_REJECTED;
    }

    public function setPendingModeration()
    {
        $this->moderator_status = self::MODERATOR_STATUS_PENDING;
    }

    public function getPublicServiceUrl()
    {
        if ($this->type === self::TYPE_CUTTING) {
            return $this->company->getPublicCuttingCompanyLink();
        }

        return CompanyServiceUrlHelper::getPublicUrl($this);
    }

    /**
     * @return array
     */
    public function getBindedCompanyBlockIds()
    {
        $blockIds = $this->getCompanyBlockBinds()->withoutStaticCache()->select('block_id')->column();
        return array_map('intval', $blockIds);
    }

    /**
     * @return CompanyBlockBind[]
     */
    public function getBindedCompanyBlocks()
    {
        $blocks = $this->getCompanyBlockBinds()->joinWith('block')->withoutStaticCache()
            ->orderBy('company_block.position')->all();
        return $blocks;
    }


    public function isPublished()
    {
        return $this->visibility == self::VISIBILITY_EVERYWHERE;
    }

    public function getPublicPageUrl()
    {
        return $this->getPublicServiceUrl();
    }


    public function setCustomProperties(array $userFieldValues)
    {
        $this->custom_properties = $userFieldValues;
    }

    public function getCustomProperties()
    {
        $props  = (array)$this->custom_properties; // format ['title'=>, 'value'=>, 'position'=>]
        $result = [];
        foreach ($props as $prop) {
            $result[$prop['title']] = $prop['value'];
        }
        return $result;
    }

    public function getShipFromLocation($stub = false)
    {
        // if new , try to find from prev.product
        $shipFromLocation = null;
        if ($this->isNewRecord) {
            $shipFromLocation = UserLocation::findOne(['user_id' => $this->ps->user_id, 'location_type' => UserLocation::LOCATION_TYPE_SHIP_FROM]);
        } else {
            $shipFromLocation = UserLocation::findOne(['id' => $this->location_id]);
        }
        if ($stub && $shipFromLocation === null) {
            $shipFromLocation = new UserLocation();
        }
        return $shipFromLocation;
    }

    /**
     * @param array $obj
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function setShipFromLocation(array $obj)
    {
        $location                = UserLocationFactory::createFromGoogleApiData($this->ps->user, $obj);
        $location->location_type = UserLocation::LOCATION_TYPE_SHIP_FROM;
        $location->safeSave();
        $this->location_id = $location->id;
    }

    public function setProductVideos($videos)
    {
        $this->videos = $videos;
    }

    public function getProductVideos()
    {
        $props = $this->videos;
        return $props;
    }


    public function setMoqPrices(array $moqPrices)
    {
        $result = [];
        foreach ($moqPrices as $k => $moqPrice) {
            if (!array_key_exists('priceWithFee', $moqPrice) || !array_key_exists('moq', $moqPrice)) {
                continue;
            }
            $qtyFrom  = (int)$moqPrice['moq'];
            $price    = (float)$moqPrice['priceWithFee'];
            $result[] = ['moq' => $qtyFrom, 'priceWithFee' => $price];
        }
        $this->moq_prices = $result;
        // find min price and set to single price
        $this->single_price = $this->getMoqFromPrice(false);
    }

    public function getMoqPricesFormatted()
    {
        $moqPrices = (array)$this->moq_prices;
        $result    = [];
        foreach ($moqPrices as $k => $moqPrice) {
            $qtyFrom = (int)$moqPrice['moq'];
            $price   = (float)$moqPrice['priceWithFee'];
            $nextEl  = !empty($moqPrices[$k + 1]) ? $moqPrices[$k + 1] : null;
            $qtyTo   = null;
            if ($nextEl) {
                $qtyTo = (int)$nextEl['moq'] - 1;
            }
            $result[] = [
                'title' => $qtyTo ? $qtyFrom . ' - ' . $qtyTo : ' >= ' . $qtyFrom,
                'price' => sprintf('%s / %s', displayAsCurrency($price, 'USD'), $this->unit_type)
            ];
        }
        return $result;
    }

    public function getMoqFromPrice($noRecalc = true)
    {
        if ($noRecalc && !empty($this->single_price)) {
            return $this->single_price;
        }
        $moqPrices = (array)$this->moq_prices;
        $result    = 0;
        foreach ($moqPrices as $k => $moqPrice) {
            if (empty($moqPrice['priceWithFee'])) {
                continue;
            }
            $tmp = (float)$moqPrice['priceWithFee'];
            if ($result === 0 || $tmp < $result) {
                $result = $tmp;
            }
        }
        return $result;
    }

    public function getSinglePriceMoney(): Money
    {
        return Money::create($this->single_price, $this->company->currency);
    }

    public function tagsList():string
    {
        $tagsLabel = ' ';
        foreach ($this->tags as $tag) {
            $tagsLabel.=' #'.$tag->text;
        }
        return substr($tagsLabel, 1, 1024);
    }

    public function getMinOrderQty()
    {
        $qtys = array_column((array)$this->moq_prices, 'moq');
        if (empty($qtys)) {
            return 1;
        }
        $qtys = array_map('intval', $qtys);
        return min($qtys);
    }

    public function getMoqPriceByQty($qty)
    {
        $moqPrices = (array)$this->moq_prices;
        $result    = null;
        foreach ($moqPrices as $k => $moqPrice) {
            if ($qty >= (int)$moqPrice['moq']) {
                if ($result === null || (float)$moqPrice['priceWithFee'] < $result) {
                    $result = (float)$moqPrice['priceWithFee'];
                }
            }
        }
        return $result;
    }

    /**
     * @param File[] $files
     */
    public function addImages($files)
    {
        $currentProductImages = $this->companyServiceImages;

        /** @var FileRepository $fileRepo */
        $fileRepo = \Yii::createObject(FileRepository::class);

        foreach ($files as $index => $file) {
            $file->setPublicMode(true);
            $file->setOwner(CompanyService::class, 'file_id');
            $fileRepo->save($file);
            $companyServiceImage                     = new CompanyServiceImage();
            $companyServiceImage->company_service_id = $this->id;
            $companyServiceImage->file_id            = $file->id;
            $companyServiceImage->pos                = $index;
            $companyServiceImage->populateRelation('file', $file);
            $currentProductImages[] = $companyServiceImage;
        }
        $this->populateRelation('companyServiceImages', $currentProductImages);
    }


    public function getBindedCompanyCertificationsIds()
    {
        $ids = $this->getCompanyCertificationBinds()->withoutStaticCache()->select('certification_id')->column();
        return array_map('intval', $ids);
    }

    /**
     * @return CompanyCertificationBind[]
     */
    public function getBindedCompanyCertifications()
    {
        $blocks = $this->getCompanyCertificationBinds()->joinWith('certification')->withoutStaticCache()
            ->orderBy('company_certification_bind.id')->all();
        return $blocks;
    }

    /**
     * @return \common\models\query\PsQuery
     */
    public function getCompany()
    {
        return $this->hasOne(\common\models\Company::class, ['id' => 'ps_id'])->inverseOf('companyServices');
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getCertifications()
    {
        return $this->hasMany(\common\models\CompanyCertification::class, ['id' => 'certification_id'])
            ->viaTable('company_certification_bind', ['company_service_id' => 'id']);
    }


    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getPsMachineDeliveryCountry()
    {
        return $this->hasMany(\common\models\PsMachineDeliveryCountry::class, ['ps_machine_delivery_id' => 'id'])
            ->via('deliveryTypess');
    }


    public function getCuttingMachineProcessings()
    {
        return $this->hasMany(\common\models\CuttingMachineProcessing::class, ['cutting_machine_id' => 'cutting_machine_id'])->inverseOf('cuttingMachine');
    }

    /**
     * @return array
     */
    public function getPublicBlocks(): array
    {
        $result        = [];
        $productBlocks = $this->getCompanyServiceBlocks()->where(['is_visible' => 1])->orderBy('position')->all();
        foreach ($productBlocks as $productBlock) {
            $result[] = $productBlock;
        }
        $companyBlocks = $this->getBindedCompanyBlocks();
        foreach ($companyBlocks as $companyBlock) {
            if (!$companyBlock->block->is_visible) {
                continue;
            }
            $result[] = $companyBlock->block;
        }
        return $result;
    }

    public function getCertificationsDataProvider()
    {
        $result       = [];
        $productCerts = $this->companyServiceCertifications;
        $companyCerts = $this->certifications;
        foreach ($productCerts as $productCert) {
            $result[] = $productCert;
        }
        foreach ($companyCerts as $productCert) {
            $result[] = $productCert;
        }
        $provider = new ArrayDataProvider([
            'allModels'  => $result,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $provider;
    }

    public function getCoverUrl()
    {
        $imageFiles     = $this->imageFiles;
        $coverImageFile = reset($imageFiles);
        return $coverImageFile ?
            ImageHtmlHelper::getThumbUrlForFile($coverImageFile, ImageHtmlHelper::DEFAULT_WIDTH, ImageHtmlHelper::DEFAULT_HEIGHT) :
            '';

    }

    /**
     * @return string
     */
    public function getCreateQuoteUrl()
    {
        return '/preorder/quote/request-quote?psId=' . $this->ps_id . '&serviceId=' . $this->id;
    }

    public static function getServiceTypeLabels()
    {
        return [

        ];
    }

    public function getTypeLabel()
    {
        $repo = \Yii::createObject(CompanyServiceTypesRepository::class);
        foreach ($repo->getAll() as $serviceType) {
            if ($serviceType->name == $this->type) {
                return $serviceType->title;
            }
        }
        return $this->type;
    }

    public function getTsCertificationClass(): ?TsCertificationClass
    {
        if ($this->psPrinter) {
            return $this->psPrinter->printer->tsCertificationClass;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->is_deleted === 1;
    }

    /**
     * @return bool
     */
    public function isModeratedApproved(): bool
    {
        return in_array($this->moderator_status, self::moderatedStatusList());
    }

    /**
     * @return Company
     */
    public function getSenderCompany(): Company
    {
        return $this->company;
    }

    public function isAvailableForOffers()
    {
        return $this->isPublished() && $this->isModeratedApproved() && (!$this->isDeleted());
    }

    public function getCertificationLabel(): string
    {
        if ($this->isCertificationInRepeat()) {
            return _t('site.ps.test', 'Repeat');
        }
        if ($this->isCertificationInProgress()) {
            return _t('site.ps.test', 'Checking');
        }
        if ($this->isCertificatedFailed()) {
            return _t('site.ps.test', 'Failed');
        }
        if ($this->isCertificated()) {
            return _t('site.ps.test', 'Verified');
        }
        return _t('site.ps.test', 'Not certified');
    }

    public function getCertificationFailedReason()
    {
        if ($this->psPrinter && $this->psPrinter->psPrinterTest && $this->psPrinter->psPrinterTest->comment) {
            return $this->psPrinter->psPrinterTest->comment;
        }
        return null;
    }

    public function isCertificatedFailed(): bool
    {
        return $this->certification === self::CERT_TYPE_REJECTED;
    }

    public function isCertificated(): bool
    {
        return $this->certification === self::CERT_TYPE_VERIFIED;
    }

    public function isCertificationInChecking(): bool
    {
        if (!$this->isCertificationInProgress()) {
            return false;
        }
        if ($this->psPrinter->psPrinterTest && $this->psPrinter->psPrinterTest->isNew()) {
            return true;
        }
        return false;
    }

    public function isNotCertificated()
    {
        if ($this->isCertificationInRepeat()) {
            return false;
        }
        if ($this->isCertificationInProgress()) {
            return false;
        }
        if ($this->isCertificatedFailed()) {
            return false;
        }
        if ($this->isCertificated()) {
            return false;
        }
        return true;
    }

    public function isCertificationInRepeat(): bool
    {
        if (!$this->isCertificationInProgress()) {
            return false;
        }
        if ($this->psPrinter->psPrinterTest && $this->psPrinter->psPrinterTest->isRepeat()) {
            return true;
        }
        return false;
    }

    public function isCertificationInProgress(): bool
    {
        return $this->certification === self::CERT_TYPE_PROGRESS;
    }

    public function validateOrException()
    {
        if (!$this->validate()) {
            throw new ValidationException($this);
        }
    }

    public function setCashbackPercent(Company $company, float $percent)
    {
        $company->cashback_percent = $percent;
        $company->safeSave();
        PsPrinterService::updatePrintersCache();
    }

    public function isModeratorStatusDraft()
    {
        return $this->moderator_status === self::MODERATOR_STATUS_DRAFT;
    }

    /**
     * @throws UserException
     */
    public function changeToPendingModeration(): void
    {
        $this->moderator_status = self::MODERATOR_STATUS_PENDING;
    }
}
