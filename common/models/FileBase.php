<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.02.18
 * Time: 9:30
 */

namespace common\models;

use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\InvalidModelException;
use common\components\FileDirHelper;
use common\components\FileTypesHelper;
use common\interfaces\FileBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\repositories\FileRepository;
use common\modules\cnc\components\FileConvertor;
use common\modules\xss\helpers\XssHelper;
use DateInterval;
use DateTime;
use DateTimeZone;
use dosamigos\transliterator\TransliteratorHelper;
use frontend\models\model3d\Model3dFacade;
use Yii;
use yii\base\Exception;

trait FileBase
{
    protected $isIgnoreAccessDate = false;
    protected $lastLocalPath = 'not exits path'; // Special invalid string path

    protected $allowedExtensions = [];
    protected $temporaryUploadPath = '';

    /**
     * @var bool This file should be deleted in repository save function
     */
    public $forDelete = false;

    /*
     * Then we need fixed path or url for file
     *
     */
    public $fixedPath = '';


    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            ['uuid', 'testAllowExtension']
        ];
    }

    public function testAllowExtension(): bool
    {
        if (!$this->allowedExtensions) {
            return true;
        }
        $currentExtension = mb_strtolower($this->getFileExtension());
        if (\in_array($currentExtension, $this->allowedExtensions, true)) {
            return true;
        }
        return false;
    }

    public function scenarios(): array
    {
        return [
            FileBaseInterface::SCENARIO_CREATE => ['*']
        ];
    }

    /**
     * Save class and field for comments
     *
     * @param string $className
     * @param string $fieldName
     */
    public function setOwner($className, $fieldName): void
    {
        $this->ownerClass = $className;
        $this->ownerField = $fieldName;
    }

    /**
     * get file url to link file
     *
     * @param float|int $expireTime in seconds
     * @return string
     * @throws Exception
     * @throws \Exception
     */
    public function getFileUrl($expireTime = FileBaseInterface::FILE_EXPIRE_LINK_DEFAULT_TIME): string
    {
        $server = \Yii::$app->params['server'];
        $serverStatic = \Yii::$app->params['staticUrl'];
        if ($this->server === FileBaseInterface::PUBLIC_FILE_SERVER) {
            // Already in localhost public storage
            $postFix = '';
            if (FileTypesHelper::isImage($this)) {
                $postFix = '?date=' . DateHelper::strtotimeUtc($this->updated_at);
            }
            if (strpos($this->path, '/static/') === 0) {
                return $serverStatic . substr($this->path, 7, 4024) . '/' . $this->getStoredName() . $postFix;
            }

            return $server . $this->path . '/' . $this->getStoredName() . $postFix;
        }

        // In private or created now file
        $hash = $this->generateDownloadHash($expireTime);
        return $server . '/site/download-file?fileHash=' . $hash;
    }

    /**
     *
     * @param $expireTime
     * @return string
     * @throws Exception
     * @throws \Exception
     */
    protected function generateDownloadHash($expireTime): string
    {
        $expireHash = new FileDownloadHash();
        $expireHash->start_date = DateHelper::now();
        $expireHash->end_date = DateHelper::addNowSec($expireTime);
        $expireHash->file_id = $this->id;
        $expireHash->hash = substr(md5(random_int(0, 99999) . FileBaseInterface::EXPIRE_HASH_SALT), 0, random_int(5, 7)) . md5(random_int(0, 99999)); // Superhash :)
        $expireHash->safeSave();
        return $expireHash->hash;
    }

    public function setFixedPath($path): void
    {
        $this->fixedPath = $path;
    }

    /**
     * Form new file path
     */
    public function produceFilePath(): void
    {
        if ($this->md5sum) {
            $md5 = $this->md5sum;
        } else {
            $md5 = md5($this->user_id . $this->name);
        }
        if ($this->fixedPath) {
            $this->path = '/static/fxd/' . $this->fixedPath;
            $this->server = FileBaseInterface::PUBLIC_FILE_SERVER;
            $this->path_version = FileBaseInterface::PATH_VERSION3;
        } elseif ($this->is_public) {
            $this->path = '/static/files/' . substr($md5, 0, 2) . '/' . substr($md5, 2, 2);
            $this->server = FileBaseInterface::PUBLIC_FILE_SERVER;
            $this->path_version = FileBaseInterface::PATH_VERSION2;
        } else {
            $this->path = '/files/' . substr($md5, 0, 2) . '/' . substr($md5, 2, 2);
            $this->server = FileBaseInterface::PRIVATE_FILE_SERVER;
            $this->path_version = FileBaseInterface::PATH_VERSION2;
        }
    }

    /**
     * Get file name in storage system
     */
    public function getStoredName(): string
    {
        return $this->stored_name;
    }

    public function produceStoredName(): void
    {
        if ($this->path_version === FileBaseInterface::PATH_VERSION3) {
            $name = $this->getCleanFileNameV3();
        } elseif ($this->path_version === FileBaseInterface::PATH_VERSION2) {
            if ($this->server == FileBaseInterface::PUBLIC_FILE_SERVER) {
                $name = $this->id . '_' . (int)$this->user_id . '_' . md5($this->id . $this->name . FileBaseInterface::PATH_SALT) . '.' . $this->extension;
            } else {
                $name = $this->id . '.' . $this->extension;
            }
        } else {
            $name = $this->id . '.' . $this->extension;
        }
        $this->stored_name = $name;
    }

    public function testIsCompressed($localPath): void
    {
        if ($this->fileCompressed) {
            foreach (param('fileCompression')['decompressor'] as $ext => $compressorParams) {
                $compressedPath = $localPath . '.' . $ext;
                if (!file_exists($localPath) && file_exists($compressedPath)) {
                    $command = sprintf($compressorParams, $compressedPath);
                    exec($command);
                    $this->fileCompressed->unpacked = 1;
                    $this->fileCompressed->safeSave();
                }
            }
        }
    }

    public function setIgnoreAccessDateMode($mode): void
    {
        $this->isIgnoreAccessDate = $mode;
    }

    /**
     * Get path for local placed file. If file on remote storage, we copy file to local server.
     *
     * @return string
     * @throws \Exception
     * @throws \yii\base\ErrorException
     */
    public function getLocalTmpFilePath(): string
    {
        if ($this->getScenario() === FileBaseInterface::SCENARIO_CREATE) {
            if ($this->temporaryUploadPath) {
                return $this->temporaryUploadPath;
            } else {
                $dir = Yii::$app->getRuntimePath() . '/uploadFiles';
                $name = md5(random_int(-99999999, 99999999)) . '.' . $this->extension;
                $this->temporaryUploadPath = $dir . '/' . $name;
            }
        } else {
            $name = $this->getStoredName();
            $dir = Yii::getAlias('@' . $this->server) . $this->path;
        }
        FileDirHelper::createDir($dir);
        $localPath = $dir . '/' . $name;
        $localPath = str_replace('//', '/', $localPath);
        $this->testIsCompressed($localPath);
        $this->updateLastAccessTime($localPath);
        return $localPath;
    }

    /**
     * If file placed on remote server, put file on remove server
     *
     * @param $content
     * @throws \yii\base\InvalidConfigException
     * @throws InvalidModelException
     * @throws \Exception
     */
    public function setFileContentAndSave($content): void
    {
        $this->md5sum = md5($content);
        $localPath = $this->getLocalTmpFilePath();
        if (!$localPath) {
            throw new InvalidModelException($this, 'Can`t get local file path: ' . $this->id);
        }
        file_put_contents($localPath, $content);
        $this->saveInRepository();
    }


    /**
     * If file on remote server, we download it
     *
     * @return string
     * @throws \Exception
     */
    public function getFileContent(): ?string
    {
        $localPath = $this->getLocalTmpFilePath();
        if (!$localPath) {
            return null;
        }

        return file_get_contents($localPath);
    }

    /**
     * Return filename
     *
     * @TODO remove filtering and extension separator from here TO Clean File name
     *
     * @return string
     */
    public function getFileName(): string
    {
        $nameWithoutExtension = $this->getFileNameWithoutExtension();
        if ($nameWithoutExtension == $this->name) {
            $nameWithoutExtension = str_replace(PHP_EOL, ' ', $nameWithoutExtension);
            $nameWithoutExtension = str_replace("\n", ' ', $nameWithoutExtension);
            $nameWithoutExtension = str_replace("\r", ' ', $nameWithoutExtension);
            $nameWithoutExtension = trim($nameWithoutExtension);
            return $nameWithoutExtension . '.' . $this->extension;
        } else {
            $name = str_replace(PHP_EOL, ' ', $this->name);
            $name = str_replace("\n", ' ', $name);
            $name = str_replace("\r", ' ', $name);
            $name = trim($name);
            return $name;
        }
    }

    /**
     * @param $fileName
     * @return string
     */
    public static function cleanFileName($fileName): string
    {
        $fileNameLen = strlen($fileName);
        if ($fileNameLen > 100) {
            $fileNameLen = 100;
        }
        $cleanFileName = '';
        for ($i = 0; $i < $fileNameLen; $i++) {
            if (((ord($fileName[$i]) > 96) && (ord($fileName[$i]) < 123)) || ((ord($fileName[$i]) > 47) && (ord($fileName[$i]) < 58)) ||
                (in_array($fileName[$i], ['-', '_', '.']))) {
                $cleanFileName .= $fileName[$i];
            }
        }
        return $cleanFileName;
    }

    /**
     * Return safe file name, filtered all special chars
     *
     * @return string
     */
    public function getCleanFileNameV3(): string
    {
        $fileName = $this->getFileName();
        $fileName = strtolower($fileName);
        $fileName = str_replace(' ', '_', $fileName);
        $fileName = TransliteratorHelper::process($fileName);
        return self::cleanFileName($fileName);
    }

    /**
     * @return string
     */
    public function getFileNameWithoutExtension(): string
    {
        $extPos = mb_strrpos($this->name, '.');
        if ($extPos) {
            return mb_substr($this->name, 0, $extPos);
        } else {
            return $this->name;
        }
    }

    /**
     * @return string
     */
    public function getFileExtension(): string
    {
        return $this->extension;
    }

    /**
     * This method will be transformed to upload file to External storage method
     * AR will be saved
     *
     * @param $filePath
     *
     * @throws InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function publishFileToServerAndSave($filePath): void
    {
        $path = $this->getLocalTmpFilePath();

        if ($filePath !== $path) {
            unlink($path); // Sendfile nginx on cache disable
            copy($filePath, $path);
        }

        $this->md5sum = md5_file($path);
        $this->updated_at = DateHelper::now();
        $this->saveInRepository();
    }

    /**
     * Change public mode
     *
     * @param bool $isPublic
     */
    public function setPublicMode($isPublic): void
    {
        $this->is_public = (int)$isPublic;
    }

    /**
     * @param array $extensions
     */
    public function setAllowedExtensions($extensions): void
    {
        $this->allowedExtensions = $extensions;
    }

    /**
     * @param $seconds
     * @throws \Exception
     */
    public function expireBySeconds($seconds): void
    {
        $expireDate = new DateTime('now', new DateTimeZone('UTC'));
        $expireDate->add(new DateInterval('PT' . $seconds . 'S'));
        $this->expire = $expireDate->format('Y-m-d H:i:s');
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \yii\base\ErrorException
     */
    public function testIsExists(): bool
    {
        return file_exists($this->getLocalTmpFilePath());
    }

    public function getMd5NameSize(): string
    {
        $name = $this->name;
        if (defined('TEST_XSS') && TEST_XSS) {
            $name = XssHelper::cleanXssProtect($name);
        }
        return md5($name . '_' . $this->size);
    }

    /**
     * @return string
     * @throws Exception
     * @throws \Exception
     * @throws \yii\base\ErrorException
     * @deprecated
     * TODO: move to Model3dPartService
     */
    public function getConvertedStlPath()
    {
        AssertHelper::assert(!$this->getIsNewRecord(), "File not saved for converting to stl");

        $ext = FileConvertor::getFileExt($this->getLocalTmpFilePath());

        if (in_array($ext, [Model3dBasePartInterface::STL_FORMAT, Model3dBasePartInterface::OBJ_FORMAT, Model3dBasePartInterface::PLY_FORMAT])) {
            return $this->getLocalTmpFilePath();
        }

        if (Model3dFacade::isIgesOrStep($this->extension)) {

            $storeDir = Yii::getAlias("@frontend/runtime/convertedStl");

            FileDirHelper::createDir($storeDir);
            $filepatch = $storeDir . "/{$this->id}.stl";
            if (!file_exists($filepatch)) {
                $convertor = new FileConvertor();
                $convertor->convert($this->getLocalTmpFilePath(), $filepatch, FileConvertor::QULAITY_HIGHT);
            }

            return $filepatch;
        }

        throw new \yii\base\Exception("Wrong extension {$this->extension} for converting to stl fileId: ".$this->id);
    }

    /**
     * Mark file as inactive
     */
    public function setInactiveStatus(): void
    {
        $this->status = FileBaseInterface::STATUS_INACTIVE;
    }

    public function setActiveStatus(): void
    {
        $this->status = FileBaseInterface::STATUS_ACTIVE;
    }

    public function isActiveStatus()
    {
        return $this->status == FileBaseInterface::STATUS_ACTIVE;
    }

    /**
     * @throws InvalidModelException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function saveInRepository()
    {
        $fileRepository = Yii::createObject(FileRepository::class);
        $fileRepository->save($this);
    }
}