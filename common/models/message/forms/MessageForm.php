<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 12.10.15
 * Time: 16:37
 */

namespace common\models\message\forms;


use common\components\FileTypesHelper;
use common\components\TextHelper;
use Faker\Provider\Text;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Form for adding message
 * @package common\models\message
 */
class MessageForm extends Model
{
    const MAX_FILES_COUNT = 50;

    /**
     * Text of message
     * @var string
     */
    public $message;

    /**
     * @var UploadedFile[]
     */
    public $files = [];

    /**
     * Rules
     * @return array
     */
    public function rules()
    {
        return [
            ['message', 'string', 'max' => 2000],
            ['files', 'file', 'extensions' => FileTypesHelper::getAllowExtensions(), 'maxFiles' => self::MAX_FILES_COUNT, 'checkExtensionByMimeType' => false],
            ['message', 'validateEmpty'],
        ];
    }

    /**
     * Validate that message is not empty
     * @param $attribute
     */
    public function validateEmpty($attribute)
    {
        if (!$this->message && !$this->files) {
            $this->addError($attribute, _t('site.messages', 'Message is empty'));
        }
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $this->files = UploadedFile::getInstancesByName('files');
        $loadResult = parent::load($data, $formName);
        $this->message = TextHelper::filterUtf8($this->message);
        return $loadResult;
    }

    /**
     * Cretae message form from topic form
     * @param TopicForm $topicForm
     * @return MessageForm
     */
    public static function createFromTopicForm(TopicForm $topicForm): MessageForm
    {
        $form = new MessageForm();
        $form->message = TextHelper::filterUtf8($topicForm->message);
        $form->files = $topicForm->files;
        return $form;
    }
}