<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 13.10.15
 * Time: 10:46
 */

namespace common\models\message\forms;


use common\components\exceptions\AssertHelper;
use common\components\FileTypesHelper;
use common\components\TextHelper;
use common\models\message\builders\TopicBuilder;
use common\models\message\exceptions\BindedObjectNotFound;
use common\models\message\exceptions\InvalidBindedObject;
use common\models\MsgTopic;
use common\models\Preorder;
use common\models\StoreOrder;
use common\models\User;
use yii\base\Exception;
use yii\base\Model;
use yii\db\ActiveRecordInterface;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Form for creation new topic
 * @package common\models\message
 */
class TopicForm extends Model
{
    const SCENARIO_PERSONAL_WITH_UNKNOWN_USER = 'personalWithUnknownUser';

    /**
     * Title of topic
     * @var string
     */
    public $title;

    /**
     * First message on topic from topic's initiator
     * @var string
     */
    public $message;

    /**
     * Type of binded object, see \common\models\message\MsgTopic::$bindObjectClasses
     * @var string
     */
    public $bindTo;

    /**
     * Id of binded object
     * @var string
     */
    public $bindId;

    /**
     * User of topic's initator
     * @var User
     */
    public $initialUser;

    /**
     * Member user id.
     * It used only on scenario
     * @var int
     */
    public $memberUserId;

    /**
     * User for member of topic.
     * Now it int, but for support topic like chat we must make it as array
     * @var User
     */
    public $memberUser;

    /**
     * @var UploadedFile[]
     */
    public $files = [];

    /**
     * Cache for binded object
     * @var mixed
     */
    private $bindedObject;

    /**
     * @return array
     */
    public function rules()
    {
        return [

            ['title', 'string', 'max' => 255],
            ['message', 'string', 'max' => 2000],
            [['message'], 'required'],
            [['title'], 'required', 'message'=> _t('site.error', 'Subject cannot be blank')],

            ['memberUserId', 'integer', 'on' => self::SCENARIO_PERSONAL_WITH_UNKNOWN_USER],
            ['memberUserId', 'required', 'on' => self::SCENARIO_PERSONAL_WITH_UNKNOWN_USER],
            ['memberUserId', function()
            {
                $userIds = array_keys($this->getUsersList());
                if(!in_array($this->memberUserId, $userIds))
                {
                    $this->addError('memberUserId', _t('front.messages', 'You can\'t send message to this user'));
                }
            }, 'on' => self::SCENARIO_PERSONAL_WITH_UNKNOWN_USER],

            ['bindTo', 'in', 'range' => array_keys(MsgTopic::$bindObjectClasses)],
            ['bindId', 'integer'],
            [['bindTo', 'bindId'], function($attribute)
            {
                if($this->bindTo && !$this->bindId || $this->bindId && !$this->bindTo)
                {
                    $this->addError($attribute, _t('front.messages', 'Bind object error'));
                }
            }],
            ['files', 'file', 'extensions' => FileTypesHelper::getAllowExtensions(), 'maxFiles' => MessageForm::MAX_FILES_COUNT, 'checkExtensionByMimeType' => false],
        ];
    }

    /**
     * Factory for personal dialog with unknown user
     * @param User $initialUser
     * @return TopicForm
     */
    public static function createPersonalWithUnknownUser(User $initialUser)
    {
        $form = new TopicForm();
        $form->initialUser = $initialUser;
        $form->scenario = self::SCENARIO_PERSONAL_WITH_UNKNOWN_USER;
        return $form;
    }

    /**
     * Factory personal dialog between two users
     * @param User $initialUser
     * @param User $withUser
     * @return TopicForm
     */
    public static function createPersonal(User $initialUser, User $withUser)
    {
        $form = new TopicForm();
        $form->initialUser = $initialUser;
        $form->memberUser = $withUser;
        return $form;
    }

    /**
     * Factory dialog with support
     * @param User $user
     * @param $bindTo
     * @param $bindId
     * @return TopicForm
     */
    public static function createToSupport(User $user, $bindTo, $bindId)
    {
        $form = new TopicForm();
        $form->initialUser = $user;
        $form->memberUser = TopicBuilder::resolveSupportUser();

        if($bindTo=='order')
        {
            $form->title = _t('site.front', 'Order #{orderId}', ['orderId' => $bindId]);
        }
        $form->bindId = $bindId;
        $form->bindTo = $bindTo;
        return $form;
    }

    /**
     * @param User $user
     * @param $bindTo
     * @param $bindId
     * @return TopicForm
     * @throws Exception
     */
   public static function createForObject(User $user, $bindTo, $bindId)
    {
        if(!$bindId || !$bindTo)
        {
            throw new \InvalidArgumentException;
        }

        $form = new TopicForm();
        $form->bindId = $bindId;
        $form->bindTo = $bindTo;

        $form->initialUser = $user;
        $object = $form->resolveBindedObject();

        if($object instanceof StoreOrder)
        {
            $form->memberUser = $object->user_id == $form->initialUser->id
                ? $object->currentAttemp->interceptionCompany()->user
                : $object->user;

            $form->title = _t('site.front', 'Order #{orderId}', ['orderId' => $object->id]);
        }
        else if ($object instanceof Preorder) {

            $form->memberUser = $object->user_id == $form->initialUser->id
                ? $object->interceptionCompany->user
                : $object->user;

            $form->title = _t('site.front', 'Quote #{preorderId}', ['preorderId' => $object->id]);
        }

        else {
            throw new InvalidBindedObject($object);
        }

        return $form;
    }

    /**
     * Resolve and cache binded object
     * @return null|mixed
     * @throws Exception
     */
    public function resolveBindedObject()
    {
        if(!$this->bindId)
        {
            return null;
        }

        if(!$this->bindedObject)
        {
            /** @var ActiveRecordInterface $objectClass */
            $objectClass = MsgTopic::$bindObjectClasses[$this->bindTo];
            $object = $objectClass::findByPk($this->bindId);
            if(!$object)
            {
                throw new BindedObjectNotFound();
            }
            $this->bindedObject = $object;
        }

        return $this->bindedObject;
    }

    /**
     * Set binded object
     * @param ActiveRecordInterface $object
     */
    public function setBindedObject($object)
    {
        $class = get_class($object);
        AssertHelper::assert(in_array($class, MsgTopic::$bindObjectClasses), 'Cant set binded object');
        $this->bindId = $object->getPrimaryKey();
        $this->bindTo = array_search($class, MsgTopic::$bindObjectClasses);
        $this->bindedObject = $object;
    }

    /**
     * Return member user
     * @return User
     * @throws Exception
     */
    public function getMemberUser()
    {
        if($this->memberUser)
        {
            return $this->memberUser;
        }

        if($this->scenario == self::SCENARIO_PERSONAL_WITH_UNKNOWN_USER)
        {
            if(!$this->memberUserId)
            {
                throw new Exception('Memnser user id not set');
            }

            $this->memberUser = User::findByPk($this->memberUserId);

            if(!$this->memberUser)
            {
                throw new Exception('User not found');
            }

            return $this->memberUser;
        }

        throw new Exception('Bad logic');
    }

    /**
     * Set member user
     * @param $user
     */
    public function setMemberUser(User $user)
    {
        $this->memberUser = $user;
        if($this->scenario = self::SCENARIO_PERSONAL_WITH_UNKNOWN_USER)
        {
            $this->memberUserId = $user->id;
        }
    }


    /**
     * Return user list (like contacts) for send personal message
     * @return array
     */
    public function getUsersList()
    {
        AssertHelper::assert($this->scenario == self::SCENARIO_PERSONAL_WITH_UNKNOWN_USER, 'Cant run this metod without scenario SCENARIO_PERSONAL_WITH_UNKNOWN_USER');
        $users = User::find()
            ->select(['id', 'username'])
            ->where(['not in', 'id', [$this->initialUser->id, \common\models\User::USER_ID_SUPPORT, \common\models\User::USER_ID_BOT]])
            ->asArray()->all();

        return ArrayHelper::map($users, 'id', 'username');
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $parentLoadResult = parent::load($data, $formName);
        $this->title = TextHelper::filterUtf8($this->title);
        $this->message = TextHelper::filterUtf8($this->message);
        $this->files = UploadedFile::getInstances($this, 'files');
        return $parentLoadResult;
    }


}