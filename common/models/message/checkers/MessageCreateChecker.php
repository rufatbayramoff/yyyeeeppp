<?php
/**
 * Created by mitaichik
 */

namespace common\models\message\checkers;


use common\models\message\forms\MessageForm;
use common\models\MsgTopic;
use common\models\User;

/**
 * Interface MessageCreateChecker
 * @package common\models\message\checkers
 */
interface MessageCreateChecker
{
    /**
     * @param MsgTopic $topic
     * @param MessageForm $messageForm
     * @param User $fromUser
     */
    public function checkCreateMessage(MsgTopic $topic, MessageForm $messageForm, User $fromUser);

}