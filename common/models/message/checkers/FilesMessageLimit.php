<?php
/**
 * Created by mitaichik
 */

namespace common\models\message\checkers;


use common\models\message\exceptions\UploadManyFilesException;
use common\models\message\forms\MessageForm;
use common\models\MsgFile;
use common\models\MsgTopic;
use common\models\User;

/**
 * Class FilesMessageLimit
 * @package common\models\message\checkers
 */
class FilesMessageLimit implements MessageCreateChecker
{
    /**
     * @param MsgTopic $topic
     * @param MessageForm $messageForm
     * @param User $fromUser
     * @throws UploadManyFilesException
     */
    public function checkCreateMessage(MsgTopic $topic, MessageForm $messageForm, User $fromUser)
    {
        if (!$messageForm->files){
            return;
        }

        $currentCount = MsgFile::find()
            ->active()
            ->notExpired()
            ->forUser($fromUser)
            ->forTopic($topic)
            ->count();

        $totalFilesCount = $currentCount + count($messageForm->files);

        if ($totalFilesCount > MessageForm::MAX_FILES_COUNT) {
            throw new UploadManyFilesException();
        }
    }
}