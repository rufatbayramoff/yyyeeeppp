<?php
/**
 * Created by mitaichik
 */

namespace common\models\message\checkers;


use common\models\message\exceptions\CreateManyMessages;
use common\models\message\forms\MessageForm;
use common\models\MsgTopic;
use common\models\User;

/**
 * Class SessionMessagesLimit
 * @package common\models\message\checkers
 */
class SessionMessagesLimit implements MessageCreateChecker
{
    /**
     * @var int
     */
    private $maxMessagesPerSession;

    /**
     * SessionMessagesLimit constructor.
     */
    public function __construct()
    {
        $this->maxMessagesPerSession =  (int)\Yii::$app->setting->get('settings.max_messages_per_session', 1000);
    }

    /**
     * @param MsgTopic $topic
     * @param MessageForm $messageForm
     * @param User $fromUser
     * @throws CreateManyMessages
     */
    public function checkCreateMessage(MsgTopic $topic, MessageForm $messageForm, User $fromUser)
    {
        $currentMessagesCount = \Yii::$app->session->get('created_messages_for_session', 0);
        if ($currentMessagesCount > $this->maxMessagesPerSession) {
            throw new CreateManyMessages();
        }
        \Yii::$app->session->set('created_messages_for_session', ++$currentMessagesCount);
    }
}