<?php
/**
 * Created by mitaichik
 */

namespace common\models\message\checkers;


use common\models\message\forms\MessageForm;
use common\models\MsgMember;
use common\models\MsgMessage;
use common\models\MsgTopic;
use common\models\StoreOrder;
use common\models\User;
use yii\db\Expression;
use yii\web\TooManyRequestsHttpException;

class NewAddresatsLimit implements MessageCreateChecker
{
    /**
     * @var int
     */
    private $limitTime;

    /**
     * @var int
     */
    private $maxAddressatsOverage;

    /**
     * NewAddresatsLimit constructor.
     */
    public function __construct()
    {
        $this->limitTime = (int) \Yii::$app->setting->get("user.message_limit_time", 3600);
        $this->maxAddressatsOverage = (int) \Yii::$app->setting->get("user.message_limit_max_addressats_overage", 10);
    }


    /**
     * @param MsgTopic $topic
     * @param MessageForm $messageForm
     * @param User $fromUser
     * @throws TooManyRequestsHttpException
     */
    public function checkCreateMessage(MsgTopic $topic, MessageForm $messageForm, User $fromUser)
    {
        // if support user
        if($fromUser->getIsSystem()){
            return;
        }

        $otherUsers = $topic->getOtherUsers($fromUser);

        // if dialog with support
        foreach ($otherUsers as $otherUser){
            if ($otherUser->getIsSystem()){
                return;
            }
        }
        unset($otherUser);

        // if order dialog
        if ($topic->getBindedObject() instanceof StoreOrder){

            /** @var StoreOrder $order */
            $order = $topic->getBindedObject();

            $messageUserId = $fromUser->id;
            $clientOrderId = $order->user_id;
            $psUserId = $order->currentAttemp ? $order->currentAttemp->ps->user_id : -1;

            $isOtherUsersIsOrderMembers = false;
            foreach ($otherUsers as $otherUser){
                if ($otherUser->id == $clientOrderId || $otherUser->id == $psUserId){
                    $isOtherUsersIsOrderMembers = true;
                    break;
                }
            }
            unset($otherUser);

            if ($isOtherUsersIsOrderMembers && ($messageUserId == $clientOrderId || $messageUserId == $psUserId)){
                return;
            }
        }

        $lastAddresstatsIds = MsgMessage::find()
            ->select(MsgMember::column('user_id'))
            ->distinct()
            ->andWhere([MsgMessage::column('user_id') => $fromUser->id])
            ->andWhere(['>=', MsgMessage::column('created_at'), new Expression("NOW() + INTERVAL -{$this->limitTime} SECOND")])
            ->joinWith('topic.members')
            ->andWhere(['!=', MsgMember::column('user_id'), $fromUser->id])
            ->column();

        $newAddressatsCount = 0;

        foreach ($otherUsers as $otherUser) {
            if(!in_array($otherUser->id, $lastAddresstatsIds)){
                $lastAddresstatsIds[] = $otherUser->id;
            }
        }
        unset($otherUser);

        foreach ($lastAddresstatsIds as $addresstaId){

            if ($this->isNewAddressat($fromUser, $addresstaId)){
                $newAddressatsCount++;
            }
        }
        unset($addresstaId);

        if ($newAddressatsCount > $this->maxAddressatsOverage) {
            throw new TooManyRequestsHttpException("You have sent too many messages. Please wait a bit and continue.");
        }
    }


    /**
     * @param User $fromUser
     * @param int $addressatId
     * @return bool
     */
    private function isNewAddressat(User $fromUser, int $addressatId) : bool
    {
        /** @var MsgTopic[] $topics */
        $topics = MsgTopic::find()->betweenUsers($fromUser->id, $addressatId)->all();

        foreach ($topics as $topic){

            if (!$topic->lastMessage) {
                continue;
            }

            if ($topic->lastMessage->user_id == $addressatId){
                return false;
            }

            /** @var MsgMessage[] $messages */
            $messages = array_values($topic->messages);


            $lastMessageFromAddressat = null;
            $lastMessageFromAddressatIndex = null;
            $afterUserMessage = null;

            for($i = count($messages) - 1; $i >= 0; $i--){

                if ($messages[$i]->user_id == $fromUser->id){
                    $afterUserMessage = $messages[$i];
                    continue;
                }

                if ($messages[$i]->user_id == $addressatId){
                    $lastMessageFromAddressat = $messages[$i];
                    break;
                }
            }

            if ($lastMessageFromAddressat === null || $afterUserMessage === null){
                continue;
            }

            $messageDate = new \DateTime($afterUserMessage->created_at, new \DateTimeZone("UTC"));

            if($messageDate->add(new \DateInterval("P1D")) >= new \DateTime('now')){
                return false;
            }
        }

        return true;
    }
}