<?php
/**
 * Created by mitaichik
 */

namespace common\models\message\exceptions;


use common\components\exceptions\BusinessException;

class UploadManyFilesException extends BusinessException
{
    public function __construct()
    {
        parent::__construct(_t('site.messages', 'The number of files you can upload is limited to 50 per message. Please reduce the number of files uploaded and try again.'));
    }

}