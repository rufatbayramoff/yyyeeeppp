<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 16.10.15
 * Time: 16:51
 */

namespace common\models\message\exceptions;


use yii\base\Exception;

/**
 * Exception when user create to many messages
 * @package common\models\message\exceptions
 */
class CreateManyMessages extends Exception
{

}