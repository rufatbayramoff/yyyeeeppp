<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 16.10.15
 * Time: 16:57
 */

namespace common\models\message\exceptions;
use yii\base\Exception;


/**
 * Exception when binded object not found
 * @package common\models\message\exceptions
 */
class BindedObjectNotFound extends Exception
{

}