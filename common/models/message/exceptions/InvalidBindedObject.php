<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 16.10.15
 * Time: 16:52
 */

namespace common\models\message\exceptions;


use yii\base\Exception;

/**
 * Exception on try bind unbindeble object (see \common\models\message\Topic::$bindObjectClasses)
 * @package common\models\message\exceptions
 */
class InvalidBindedObject extends Exception
{
    /**
     * @param string $bindedObject
     */
    public function __construct($bindedObject)
    {
        \Exception::__construct('Invalid bind object with class '.get_class($bindedObject));
    }

}