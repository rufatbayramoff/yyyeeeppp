<?php
/**
 * Created by mitaichik
 */

namespace common\models\message;


use common\models\message\exceptions\InvalidBindedObject;
use common\models\MsgFolder;
use common\models\Preorder;
use common\models\StoreOrder;
use common\models\User;
use yii\base\Exception;

/**
 * Class FolderResolver
 * @package common\models\message
 */
class FolderResolver
{
    /**
     * Resolve folder for topic users.
     * Returna array of folder's ID where first item - filder for initial user, second - folder for all another users
     *
     * @param User $from
     * @param User $to
     * @param bool $toSupport
     * @param StoreOrder|Preorder $bind
     * @return int[]
     */
    public function resolve(User $from, User $to, bool $toSupport, $bind = null) : array
    {
        if ($folders = $this->resolveSupportFolder($from, $toSupport)) {
            return $folders;
        }

        if ($bind) {
            return [
                self::resolveFolderIdForObject($from, $bind),
                self::resolveFolderIdForObject($to, $bind),
            ];
        }

        return [MsgFolder::FOLDER_ID_PERSONAL, MsgFolder::FOLDER_ID_PERSONAL];
    }

    /**
     * Resolve folders if it topich with support
     *
     * @param User $from
     * @param bool $toSupport
     * @return int[]|null
     */
    private function resolveSupportFolder(User $from, bool $toSupport)
    {
        if($toSupport|| $from->id == User::USER_ID_SUPPORT){
            return [MsgFolder::FOLDER_ID_SUPPORT, MsgFolder::FOLDER_ID_SUPPORT];
        }
        return null;
    }

    /**
     * Return folder id for object for user
     *
     * @param User $user
     * @param $bind
     * @return int
     * @throws Exception
     * @throws InvalidBindedObject
     */
    public function resolveFolderIdForObject(User $user, $bind)
    {
        if($bind instanceof StoreOrder) {
            return $this->resolveForStoreOrder($user, $bind);
        }

        if($bind instanceof Preorder) {
            return $this->resolveForPreorder($user, $bind);
        }

        throw new InvalidBindedObject($bind);
    }

    /**
     * @param User $user
     * @param StoreOrder $order
     * @return int
     * @throws Exception
     */
    private function resolveForStoreOrder(User $user, StoreOrder $order)
    {
        if($order->user_id == $user->id) {
            return MsgFolder::FOLDER_ID_ORDERS;
        }

        foreach ($order->attemps as $attemp){
            if ($attemp->interceptionCompany()->user_id == $user->id) {
                return MsgFolder::FOLDER_ID_PRINT_JOBS;
            }
        }

        if($order->currentAttemp->interceptionCompany()->user_id == $user->id) {
            return $order->currentAttemp->isPrinted()
                ? MsgFolder::FOLDER_ID_PRINTED_JOBS
                : MsgFolder::FOLDER_ID_PRINT_JOBS;
        }

        throw new Exception('BadLogic');
    }

    /**
     * @param User $user
     * @param Preorder $preorder
     * @return int
     */
    private function resolveForPreorder(User $user, Preorder $preorder)
    {
        return $preorder->user_id == $user->id ? MsgFolder::FOLDER_ID_ORDERS : MsgFolder::FOLDER_ID_PRINT_JOBS;
    }
}