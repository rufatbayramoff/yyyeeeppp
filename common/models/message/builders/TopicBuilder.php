<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 12.10.15
 * Time: 17:34
 */

namespace common\models\message\builders;

use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\models\message\exceptions\InvalidBindedObject;
use common\models\message\FolderResolver;
use common\models\message\forms\MessageForm;
use common\models\message\forms\TopicForm;
use common\models\MsgMember;
use common\models\MsgTopic;
use common\models\User;
use DateTime;
use DateTimeZone;
use frontend\models\community\MessageFacade;
use yii\base\Exception;
use yii\db\Expression;
use yii\web\UploadedFile;

/**
 * Class TopicBuilder
 * @package common\models\message
 */
class TopicBuilder
{
    /**
     * Title of topic
     * @var string
     */
    private $title;

    /**
     * First initial message on topic
     * @var string
     */
    private $message;

    /**
     * Binded objec
     * @var mixed
     */
    private $object;

    /**
     * Members on topic
     * @var User[]
     */
    private $members = [];

    /**
     * User who create topic
     * @var User
     */
    private $initialUser;

    /**
     * Message to support
     * @var bool
     */
    private $toSupport = false;

    /**
     * @var UploadedFile[]
     */
    public $files = [];

    /**
     * Send other user notify about new message.
     * @var bool
     */
    public $notify = true;

    /**
     * Return builder
     * @return TopicBuilder
     */
    public static function builder()
    {
        return new static();
    }

    /**
     * Add params from topic form to builder
     * @param TopicForm $form
     * @return $this
     * @throws Exception
     * @throws InvalidBindedObject
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function fromForm(TopicForm $form)
    {
        $this
            ->setInitialUser($form->initialUser)
            ->to($form->getMemberUser())
            ->setTitle($form->title)
            ->setMessage($form->message)
            ->setFiles($form->files);

        if($bindObject = $form->resolveBindedObject())
        {
            $this->bindObject($bindObject);
        }

        return $this;
    }

    /**
     * Add support user is initial user
     * @return $this
     * @throws Exception
     */
    public function fromSupport()
    {
        $this->setInitialUser(self::resolveSupportUser());
        return $this;
    }

    /**
     * Set inital user
     * @param User $user
     * @return $this
     * @throws Exception
     */
    public function setInitialUser(User $user)
    {
        if(isset($this->members[$user->id]))
        {
            throw new Exception('User already set as no initalizer');
        }

        $this->initialUser = $user;
        return $this;
    }

    /**
     * Set topic title
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Set topic title
     * @param UploadedFile[]|null $files
     * @return $this
     */
    public function setFiles($files)
    {
        $this->files = $files ?: [];
        return $this;
    }

    /**
     * Set first initial message
     * @param $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Add member to topic
     * @param User $user
     * @return $this
     * @throws Exception
     */
    public function to(User $user)
    {
        if($this->initialUser && $this->initialUser->id == $user->id)
        {
            throw new BusinessException('You can`t send message for yourself');
        }
        $this->members[$user->id] = $user;
        return $this;
    }

    /**
     * Add members to topic
     * @param array $users
     * @return $this
     */
    public function toUsers(array $users)
    {
        foreach($users as $user)
        {
            $this->to($user);
        }
        return $this;
    }

    /**
     * Bind objec to topic
     * @param mixed $object
     * @return $this
     * @throws Exception
     */
    public function bindObject($object)
    {
        if(!in_array($class = get_class($object), MsgTopic::$bindObjectClasses))
        {
            throw new InvalidBindedObject($object);
        }

        $this->object = $object;
        return $this;
    }

    /**
     * Set that this topic tu support
     * @return $this
     */
    public function toSupport()
    {
        $this->toSupport = true;
        return $this;
    }

    /**
     * Dont send other user notify about nre message.
     *
     * @return TopicBuilder
     */
    public function dontNotify() : TopicBuilder
    {
        $this->notify = false;
        return $this;
    }

    /**
     * Create and save topic
     * @return MsgTopic
     * @throws \Exception
     */
    public function create()
    {
        AssertHelper::assert($this->initialUser, 'Initial user must be set');
        AssertHelper::assert(count($this->members) == 1, 'Supported chat only with one user');

        $transaction = \Yii::$app->db->beginTransaction();

        try{

            // create topic

            $topic = new MsgTopic();
            $topic->creator_id = $this->initialUser->id;
            $topic->created_at = (new DateTime('now' ,new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
            $topic->title = $this->title;
            $topic->last_message_time = new Expression('NOW()');
            if($this->object) {
                $topic->bind_id = $this->object->id;
                $topic->bind_to = array_search(get_class($this->object), MsgTopic::$bindObjectClasses);
            }
            AssertHelper::assertSave($topic);

            // create members

            [$initialUserFolder, $otherUserFolders] = (new FolderResolver())->resolve($this->initialUser, reset($this->members), $this->toSupport, $this->object);

            $member = new MsgMember();
            $member->topic_id = $topic->id;
            $member->user_id = $this->initialUser->id;
            $member->folder_id = $member->original_folder = $initialUserFolder;
            $member->have_unreaded_messages = 0;
            AssertHelper::assertSave($member);

            foreach($this->members as $user)
            {
                $member = new MsgMember();
                $member->topic_id = $topic->id;
                $member->user_id = $user->id;
                $member->folder_id = $member->original_folder = $otherUserFolders;
                $member->have_unreaded_messages = 0;
                AssertHelper::assertSave($member);
            }

            // cretae message
            if(!empty($this->message)){
                $messageForm = new MessageForm();
                $messageForm->message = $this->message;
                $messageForm->files = $this->files;
                MessageFacade::addMessage($topic, $this->initialUser, $messageForm, null, $this->notify);
            }
            $transaction->commit();
        }
        catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $topic;
    }

    /**
     * Resolve support user
     * @return \common\models\User
     */
    public static function resolveSupportUser()
    {
        static $user;

        if(!$user)
        {
            $user = \common\models\User::findOne(User::USER_ID_SUPPORT);
            AssertHelper::assert($user, 'Support user not found');
        }

        return $user;
    }

}