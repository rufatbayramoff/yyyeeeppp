<?php
/**
 * Created by PhpStorm.
 * User: mitaichik
 * Date: 13.10.15
 * Time: 11:12
 */

namespace common\models\message\helpers;

use common\models\base\StoreOrder;
use common\models\base\User;
use common\models\message\exceptions\InvalidBindedObject;
use common\models\MsgFolder;
use common\models\MsgTopic;
use common\models\Preorder;
use yii\base\Exception;

/**
 * Helper for gernerate routes for create/view messages
 *
 * @package common\models\message\helpers
 */
class UrlHelper
{
    /**
     * Route to create message to support
     *
     * @param mixed|null $object object for which create messages (object will bind to topic)
     * @return array
     * @throws InvalidBindedObject
     */
    public static function supportMessageRoute($object = null)
    {
        $route = ['/workbench/messages/create-support-topic'];
        if ($object) {
            self::addBindObjectParamsToRoute($route, $object);
        }
        return $route;
    }

    /**
     * Route to create message for object
     *
     * @param mixed|null $object object for which create messages (object will bind to topic)
     * @return array
     * @throws InvalidBindedObject
     */
    public static function objectMessageRoute($object)
    {
        $route = ['/workbench/messages/create-topic-for-object'];
        self::addBindObjectParamsToRoute($route, $object);
        return $route;
    }

    /**
     * Route for view topic
     *
     * @param MsgTopic $topic
     * @return array
     */
    public static function topicRoute(MsgTopic $topic)
    {
        return ['/workbench/messages/topic', 'topicId' => $topic->id];
    }

    /**
     * Route for create personal message
     *
     * @param User $withUser
     * @return array
     */
    public static function personalMessageRoute(User $withUser)
    {
        return $route = ['/workbench/messages/create-personal-topic', 'withUsername' => $withUser->username];
    }

    /**
     * @param string $username
     * @return array
     */
    public static function personalMessageRouteUsername(string $username)
    {
        return $route = ['/workbench/messages/create-personal-topic', 'withUsername' => $username];
    }

    /**
     * Route for binded object (for backend use)
     *
     * @param $object
     * @return array
     * @throws Exception
     */
    public static function bindedObjectBackendRoute($object)
    {
        if ($object instanceof StoreOrder) {
            return ['/store/store-order/view', 'id' => $object->id];
        }
        if ($object instanceof Preorder) {
            return ['/store/preorder/view', 'id' => $object->id];
        }

        return ['/'];
    }

    /**
     * Add binded object params to route
     *
     * @param array $route route
     * @param mixed $object binded object
     * @throws InvalidBindedObject
     */
    private static function addBindObjectParamsToRoute(array &$route, $object)
    {
        $objectClass = get_class($object);

        if (!in_array($objectClass, MsgTopic::$bindObjectClasses)) {
            throw new InvalidBindedObject($object);
        }

        $route['bindId'] = $object->id;
        $route['bindTo'] = array_search($objectClass, MsgTopic::$bindObjectClasses);
    }


    /**
     * Route to folder
     *
     * @param MsgFolder $folder
     * @return array
     */
    public static function toFolderRoute(MsgFolder $folder)
    {
        return ['/workbench/messages/index', 'folderAlias' => $folder->alias];
    }

    /**
     * Route for archive topic
     *
     * @param MsgTopic $topic
     * @return array
     */
    public static function archiveRoute(MsgTopic $topic)
    {
        return ['/workbench/messages/archive', 'topicId' => $topic->id];
    }

    /**
     * Route for archive topic
     *
     * @param MsgTopic $topic
     * @return array
     */
    public static function deleteRoute(MsgTopic $topic)
    {
        return ['/workbench/messages/delete-topic', 'topicId' => $topic->id];
    }


}