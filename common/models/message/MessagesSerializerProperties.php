<?php
/**
 * Created by mitaichik
 */

namespace common\models\message;


use common\models\MsgFile;
use common\models\MsgMessage;
use common\models\User;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;
use yii\helpers\Html;
use yii\helpers\Url;

class MessagesSerializerProperties extends \common\components\serizaliators\AbstractProperties
{
    /**
     * Current user
     * @var User
     */
    private $currentUser;

    /**
     * MessagesSerializer constructor.
     * @param User $currentUser
     */
    public function __construct(User $currentUser)
    {
        $this->currentUser = $currentUser;
    }

    /**
     * convert links
     *
     * @param $text
     * @return mixed
     */
    private function addLinks($text)
    {
        $messageText = preg_replace("~[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]~", "<a href=\"\\0\" target=\"_blank\">\\0</a>", $text);
        return $messageText;
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            MsgMessage::class => [
                'id',
                'text' => function(MsgMessage $message){
                    $text =  (nl2br(H($message->text)));
                    return $this->addLinks($text);
                },
                'timeString' => function(MsgMessage $message){
                    return \Yii::$app->formatter->asRelativeTime($message->created_at);
                },
                'isBlocked',
                'user',
                'files' =>  function(MsgMessage $message){
                    return $message->msgFiles;
                },
                'filesExpiredMsg' => function(MsgMessage $message) {
                    if (!$message->msgFiles) {
                        return null;
                    }
                    $expiredDate = (new \DateTime($message->created_at))->add(new \DateInterval("P2M"));
                    return _t('site.messages', 'This file will expire on {date}', ['date' => \Yii::$app->formatter->asDate($expiredDate)]);
                }
            ],
            User::class => [
                'username' => function(User $user){
                    $userName =  UserFacade::getFormattedUserName($user);
                    $companyTitle = $user->company->title ?? '';
                    if(!empty($companyTitle)) {
                        return "$userName ({$companyTitle})";
                    }
                    return $userName;
                },
                'avatar' => function(User $user){
                    if ($user->company) {
                        return Html::img($user->company->getCompanyLogoOrDefault(false, 32), [
                            'class' => 'direct-chat-img'
                        ]);
                    }

                    return UserUtils::getAvatarForUser($user, 'chat');
                },
                'isAuthor' => function(User $user){
                    return $this->currentUser->equals($user);
                },
            ],
            MsgFile::class => [
                'id',
                'filename' => function(MsgFile $file) {
                    return $file->file->name;
                },
                'url' => function(MsgFile $file) {
                    return Url::toRoute(['/workbench/messages/download-file', 'fileId' => $file->id]);
                },
            ]

        ];
    }
}