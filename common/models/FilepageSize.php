<?php

namespace common\models;

use common\components\ps\locator\Size;

/**
 * Class FilepageSize
 * @package common\models
 */
class FilepageSize extends \common\models\base\FilepageSize
{
    public function toSize()
    {
        return Size::create($this->width, $this->height, $this->length);
    }
}