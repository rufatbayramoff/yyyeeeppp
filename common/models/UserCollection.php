<?php

namespace common\models;

use common\models\query\StoreUnitQuery;
use common\modules\product\interfaces\ProductInterface;
use common\traits\db\ActiveScopeTrait;
use common\traits\db\ForUserScopeTrait;
use common\components\ActiveQuery;

/**
 * This is the model class for table "user_collection".
 */
class UserCollection extends \common\models\base\UserCollection
{
    /**
     * Const of system collections
     */
    const SYSTEM_TYPE_FILES = 'files';
    const SYSTEM_TYPE_LIKES = 'likes';

    /**
     * @return UserCollectionQuery
     */
    public static function find()
    {
        return new UserCollectionQuery(get_called_class());
    }

    /**
     * update total items in collection
     *
     * @return \common\models\UserCollection
     */
    public function countUp()
    {
        $this->items_count = $this->items_count + 1;
        $this->safeSave();
        return $this;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $this->updated_at = dbexpr("NOW()");
        return parent::save($runValidation, $attributeNames);
    }

    /**
     * update total items after item removed from collection
     *
     * @return \common\models\UserCollection
     */
    public function countDown()
    {
        $this->items_count = max(0, intval($this->items_count) - 1);
        $this->updated_at = dbexpr("NOW()");
        $this->safeSave();
        return $this;
    }

    /**
     * Return true if collection is system
     *
     * @return bool
     */
    public function getIsSystem()
    {
        return (bool)$this->system_type;
    }

    /**
     * @return ActiveQuery
     */
    public function getUserCollectionModel3ds()
    {
        return $this->hasMany(UserCollectionModel3d::class, ['collection_id' => 'id'])
            ->joinWith('model3d.productCommon')
            ->where('product_common.product_status=\'' . ProductInterface::STATUS_PUBLISHED_PUBLIC . '\'');
    }
}

/**
 * Class UserCollectionQuery
 *
 * @package common\models
 */
class UserCollectionQuery extends ActiveQuery
{
    use ActiveScopeTrait;
    use ForUserScopeTrait;

    /**
     * Exclude system collection
     *
     * @param string $systemType if set - exclude only this type, else - exclude all system collections
     * @return $this
     */
    public function notSystem($systemType = null)
    {
        $systemType === null
            ? $this->andWhere(['system_type' => null])
            : $this->andWhere(['or', ['system_type' => null], ['!=', 'system_type', $systemType]]);

        return $this;
    }
}