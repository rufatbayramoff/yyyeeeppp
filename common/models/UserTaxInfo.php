<?php

namespace common\models;

use common\components\Emailer;
use common\components\exceptions\BusinessException;
use common\components\HistoryBehavior;
use common\components\security\EncrypterForm;
use yii;
use yii\base\UserException;
use yii\helpers\ArrayHelper;

/**
 * User tax information model
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserTaxInfo extends \common\models\base\UserTaxInfo
{

    /**
     * user tax info status in queue order
     */
    const STATUS_NEW = 'new';
    const STATUS_SUBMITTED = 'submitted';
    const STATUS_UPDATED = 'updated';
    const STATUS_REVIEW = 'review';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_DECLINE = 'declined';

    /**
     * scenario types
     */
    const SCENARIO_STEP2 = 'step2';
    const SCENARIO_STEP2_NOUSA = 'step2_nousa';
    const SCENARIO_STEP4 = 'step4';
    const SCENARIO_FINISH = 'finish';

    /**
     * form types
     */
    const FORM_FW8BEN = 'fw8ben';
    const FORM_FW8BENE = 'fw8bene';
    const FORM_FW9 = 'fw9';


    /**
     * Return array of statuses
     *
     * @return string[]
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_NEW       => 'New',
            self::STATUS_SUBMITTED => 'Submitted',
            self::STATUS_UPDATED   => 'Updated',
            self::STATUS_REVIEW    => 'Review',
            self::STATUS_CONFIRMED => 'Confirmed',
            self::STATUS_DECLINE   => 'Declined'
        ];
    }

    /**
     * Behaviors : [history]
     *
     * @see \common\components\HistoryBehavior
     * @return array
     */
    public function behaviors()
    {
        return [
            'history' => [
                'class'          => HistoryBehavior::className(),
                'historyClass'   => UserTaxInfoHistory::className(),
                'foreignKey'     => 'user_tax_info_id',
                'skipAttributes' => ['identification']
            ],
        ];
    }

    /**
     *
     * @param string $id
     */
    public function setIdentification($id)
    {
        $form = new EncrypterForm();
        $this->identification = $form->encrypt($id);
        $this->encrypted = 1;
    }

    /**
     * validate step2 form
     *
     * @return boolean
     * @throws UserException
     */
    public function validateStep2()
    {
        if (empty($this->is_usa) && $this->place_country == 'US') {
            throw new BusinessException(
                _t(
                    'site.user',
                    'You indicated that you are not a US Person or US permanent resident (green card holder) but you have marked US residency. 
                Please change the entered information or contact Support Center.'
                )
            );
        }

        if ($this->classification != 'Individual') {
            if (empty($this->business_name)) {
                throw new BusinessException(_t('site.user', 'Please specify organization (business name)'));
            }
            if (empty($this->place_country)) {
                throw new BusinessException(_t('site.user', 'Please specify place of organization'));
            }
            if (empty($this->identification)) {
                $this->addError('identification', _t('site.user', 'Please specify identification number'));
                throw new BusinessException(_t('site.user', 'Please specify identification number'));
            }
        } else {
            if (empty($this->is_usa)) {
                if (empty($this->dob_date)) {
                    throw new BusinessException(_t('site.user', 'Please specify date of birth'));
                }
            }
            if (empty($this->place_country)) {
                throw new BusinessException(_t('site.user', 'Please specify country of citizenship'));
            }
        }

        if (!empty($this->dob_date)) {
            if ($this->checkDate($this->dob_date)) {
                $this->dob_date = date('Y-m-d', strtotime($this->dob_date));
            } else {
                throw new BusinessException(_t('site.user', 'Please specify correct date of birth'));
            }
        }
        return true;
    }

    private function checkDate($date)
    {
        $checkYear = function ($year) {
            if ($year > 2010 || $year < 1940) {
                return false;
            }
            return true;
        };
        if (strpos($date, '/') !== false) {
            $dateParts = explode('/', $date);
            if (!$checkYear($dateParts[2])) {
                return false;
            }
            if (!checkdate($dateParts[0], $dateParts[1], $dateParts[2])) {
                return false;
            }
        } else if (strpos($date, '-') !== false) {
            $dateParts = explode('-', $date);
            if (!$checkYear($dateParts[0])) {
                return false;
            }
            if (!checkdate($dateParts[1], $dateParts[2], $dateParts[0])) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
    /**
     * validating rules
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[0] = [
            ['is_usa'],
            'required',
            'message' => _t('app.error', ' This field cannot be left blank, please choose an option')
        ];

        return ArrayHelper::merge(
            $rules,
            [

                [
                    ['user_id', 'classification', 'name', 'address_id', 'identification_type', 'identification'],
                    'required',
                    'on' => self::SCENARIO_STEP2,
                ],
                [['dob_date'], 'date', 'format' => 'Y-m-d', 'message' => _t('site.app', 'Please specify correct date of birth.')],
                [
                    ['user_id', 'classification', 'name', 'address_id'],
                    'required',
                    'on' => self::SCENARIO_STEP2_NOUSA
                ],
                [
                    ['signature', 'sign_date'],
                    'required',
                    'on' => self::SCENARIO_STEP4
                ],
                [
                    ['sign_type'],
                    'required',
                    'on' => self::SCENARIO_FINISH
                ]
            ]
        );
    }

    /**
     * get sign date formated for tax form
     *
     * @return string
     */
    public function getTaxSignDate()
    {
        if (empty($this->sign_date) || $this->sign_date == '0000-00-00') {
            return date("m-d-Y");
        }
        return date("m-d-Y", strtotime($this->sign_date));
    }

    /**
     * date format mm-dd-YYYY with converting to YYYY-mm-dd
     *
     * @param string $value
     */
    public function setTaxSignDate($value)
    {
        // format mm-dd-yy
        $this->sign_date = self::formatTaxSignDate($value);
    }

    /**
     * get decline reason
     *
     * @param \common\models\base\UserTaxInfo $userTaxInfo
     * @return string
     */
    public static function getDeclineReason(base\UserTaxInfo $userTaxInfo)
    {
        $taxId = $userTaxInfo->id;
        /** @var \common\models\UserTaxInfoHistory $reason */
        $reason = UserTaxInfoHistory::find()->where(
            [
                'user_tax_info_id' => $taxId,
                'action_id'        => UserTaxInfoHistory::ACTION_DECLINE
            ]
        )
            ->latest();
        return $reason ? $reason->comment : UserTaxInfoHistory::ACTION_DECLINE;
    }

    /**
     *
     * @param string $value
     * @return string
     */
    public static function formatTaxSignDate($value)
    {
        $parts = explode("-", $value);
        $month = $parts[0];
        $day = $parts[1];
        $year = $parts[2];
        $formatedDate = implode("-", [$year, $month, $day]);
        return $formatedDate;
    }

    /**
     *
     * @return array
     */
    public static function getTaxClassifications()
    {
        return [
            'Individual'    => 'Individual/Sole proprietor',
            'C Corporation' => 'C Corporation',
            'S Corporation' => 'S Corporation',
            'LLC'           => 'Limited Liability Company',
            'Partnership'   => 'Partnership',
            'Trust/estate'  => 'Trust/estate',
            'Other'         => 'Other'
        ];
    }

    /**
     *
     * @return array
     */
    public static function getNonUsaTaxClassifications()
    {
        return [
            'Individual'              => 'Individual',
            'Corporation'             => 'Corporation',
            'Disregarded entity'      => 'Disregarded entity',
            'Partnership'             => 'Partnership',
            'Simple trust'            => 'Simple trust',
            'Grantor trust'           => 'Grantor trust',
            'Complex trust'           => 'Complex trust',
            'Estate'                  => 'Estate',
            'Government'              => 'Government',
            //'International organization' => 'International organization',
            'Central bank of issue'   => 'Central bank of issue',
            'Tax-exempt organization' => 'Tax-exempt organization',
            'Private foundation'      => 'Private foundation',
        ];
    }

    /**
     * get prepared data for pdf form with speicific keys to fill
     *
     * @param string $form
     * @param boolean $forPreview
     * @return array
     * @throws UserException
     */
    public function getPdfFormData($form, $forPreview = true)
    {
        switch ($form) {
            case self::FORM_FW9:
                $result = $this->getPdfFormFw9($forPreview);
                break;
            case self::FORM_FW8BEN:
                $result = $this->getPdfFormFw8ben($forPreview);
                break;
            case self::FORM_FW8BENE:
                $result = $this->getPdfFormFw8bene($forPreview);
                break;
            default:
                throw new UserException("Not supported form " . $form);
        }

        return $result;
    }

    private function getPdfFormFw8bene($forPreview = true)
    {
        /* $data = [
            0 => "name",
            1 => "place_country",            
            2 => '',
            3 => 'city',
            4 => 'country',
            5 => '', // mailing address
            6 => '', // mailing city
            7 => '', // mailing country
            8 => '',
            9 => '',
            10 => '', // ref number
            11 => '', // dob 
        ]; */
        $result = [];
        $result[0] = $this->name;
        $result[1] = $this->place_country;
        $result[47] = $this->address->address . ' ' . $this->address->extended_address;
        $result[48] = implode(" ", [$this->address->city, $this->address->region, $this->address->zip_code]);
        $result[49] = $this->address->country->iso_code;

        $classificationsType = array_keys($this->getNonUsaTaxClassifications());
        $classificationTypesRef = array_flip($classificationsType);
        $classificationId = $classificationTypesRef[$this->classification];
        $classificationValue = $classificationId;

        $result[$classificationId + 2] = $forPreview ? "&#10008;" : $classificationValue; // ✘

        $form = range(0, 150);
        $formFilled = [];
        foreach ($form as $k => $v) {
            $val = '';
            if (isset($result[$k])) {
                $val = $result[$k];
            }
            $key = $forPreview ? '{IN_' . $k . '}' : $k;
            $formFilled[$key] = $val;
        }
        return $formFilled;
    }

    /**
     * @param bool|string $forPreview
     * @return array
     */
    private function getPdfFormFw8ben($forPreview = true)
    {
        /* $data = [
            0 => "name",
            1 => "place_country",            
            2 => 'address',
            3 => 'city',
            4 => 'country',
            5 => '', // mailing address
            6 => '', // mailing city
            7 => '', // mailing country
            8 => 'identification',
            9 => '',
            10 => '', // ref number
            11 => '', // dob
            12 => '',
            13 => '',
            14 => '',
            15 => '',
            16 => '',
            20 => 'signer',
            21 => ''
        ]; */
        $result[0] = $this->name;
        $result[1] = $this->place_country;
        $result[2] = $this->address->address . ' ' . $this->address->extended_address;
        $result[3] = implode(' ', [$this->address->city, $this->address->region, $this->address->zip_code]);
        $result[4] = $this->address->country->iso_code;

        $result[11] = date('m-d-Y', strtotime($this->dob_date));
        $result[20] = $this->signature;

        $form = range(0, 21);
        $formFilled = [];
        foreach ($form as $k => $v) {
            $val = '';
            if (isset($result[$k])) {
                $val = $result[$k];
            }
            $key = $forPreview ? '{IN_' . $k . '}' : $k;
            $formFilled[$key] = $val;
        }

        return $formFilled;
    }

    /**
     *
     * @param boolean $forPreview
     * @return array
     */
    private function getPdfFormFw9($forPreview = true)
    {
        $classificationTypes = [
            2 => 'Individual',
            3 => 'C Corporation',
            4 => 'S Corporation',
            7 => 'LLC',
            5 => 'Partnership',
            6 => 'Trust/estate',
            9 => 'Other'
        ];

        $classificationTypesRef = array_flip($classificationTypes);
        /*$data = [
            0 => "name",
            1 => "business_name",            
            11 => '',
            12 => '',
            13 => 'address_street',
            14 => 'postal_code',
            15 => '',
            16 => '',
            17 => 'security_3',
            18 => 'security_2',
            19 => 'security_4',
            20 => 'ein_1',
            21 => 'ein_2'
        ]; */
        $result = [];
        $result[0] = !empty($this->name) ? $this->name : $this->business_name;
        $result[1] = $this->business_name != $result[0] ? $this->business_name : '';
        // type
        $classificationId = $classificationTypesRef[$this->classification];
        $classificationValue = $classificationId - 1;
        if ($this->classification == 'Other') {
            $result[10] = 'Other'; // change to business name?
            $classificationValue = 7;
        }
        $result[$classificationId] = $forPreview ? "&#10008;" : $classificationValue; // ✘

        $result[13] = $this->address->address . ' ' . $this->address->extended_address;
        $result[14] = implode(" ", [$this->address->city, $this->address->region, $this->address->zip_code]);
        // replaced
        if (!empty($this->identification)) {
            $this->identification = "";
        }
        $idNumbers = str_split($this->identification);
        $delim = $forPreview ? " &nbsp;&nbsp;&nbsp; " : "";
        if ($this->identification_type == 'ssn') {
            $result[17] = implode($delim, array_slice($idNumbers, 0, 3)); // 3
            $result[18] = implode($delim, array_slice($idNumbers, 3, 2)); // 2
            $result[19] = implode($delim, array_slice($idNumbers, 5)); // 4
        } else { // ein
            $result[20] = implode($delim, array_slice($idNumbers, 0, 2)); // 2
            $result[21] = implode($delim, array_slice($idNumbers, 2)); // 7
        }
        $form = range(0, 25);
        $formFilled = [];
        foreach ($form as $k => $v) {
            $val = '';
            if (isset($result[$k])) {
                $val = $result[$k];
            }
            $key = $forPreview ? '{IN_' . $k . '}' : $k;
            $formFilled[$key] = $val;
        }

        return $formFilled;
    }

    /**
     * get form type name by user tax information
     *
     * @param boolean $toUpper - make upper case for screen
     * @return string
     */
    public function getFormType($toUpper = false)
    {
        $form = self::FORM_FW8BENE;
        if (!empty($this->is_usa)) {
            $form = self::FORM_FW9;
        } else {
            if ($this->classification == 'Individual') {
                $form = self::FORM_FW8BEN;
            }
        }
        if ($toUpper) {
            $form = $this->getFormTypeTitle($form);
            // W-8BEN-E            
        }
        return $form;
    }

    /**
     *
     * @param string $form
     * @return string
     */
    public function getFormTypeTitle($form)
    {
        $title = $form;
        switch ($form) {
            case self::FORM_FW8BEN:
                $title = 'W-8BEN';
                break;
            case self::FORM_FW8BENE:
                $title = 'W-8BEN-E';
                break;
            case self::FORM_FW9:
                $title = 'W-9';
                break;
        }
        return $title;
    }

    /**
     * Get PDF file based on tax form type
     *
     * @return string
     */
    public function getPdfFormFile()
    {
        $type = $this->getFormType();
        $file = Yii::getAlias('@doc/templates/tax/' . $type . '.pdf');
        return $file;
    }

    /**
     * get user tax percent
     *
     * @param bool|string $type - type of tax rate to return, if false - returns array of taxes
     * @return array
     */
    public function getTaxPercents($type = false)
    {
        $formType = $this->getFormType();
        $taxRate = TaxCountry::getDefaultRate();
        if ($formType == self::FORM_FW9) {
            $taxRate = ['rate_ps' => 0, 'rate_model' => 0];
        } else {
            $countryRateObj = TaxCountry::find()
                ->where(['country' => $this->place_country])
                ->asArray()->one();
            if ($countryRateObj) {
                $taxRate = $countryRateObj;
            }
        }
        return $type ? $taxRate[$type] : $taxRate;
    }

    /**
     * get user tax rates as array, or integer (0-100) if type is specified
     * types from TaxCountry::RATE_PS, TaxCountry::RATE_MODEL
     *
     * @param \common\models\base\User $user
     * @param boolean $type
     * @return array|integer
     */
    public static function getUserRate(base\User $user, $type = false)
    {
        $taxModel = self::findOne(
            [
                'user_id' => $user->id,
                'status'  => [self::STATUS_SUBMITTED, self::STATUS_CONFIRMED]
            ]
        );
        if (empty($taxModel)) {
            return TaxCountry::getDefaultRate($type);
        }
        return $taxModel->getTaxPercents($type);
    }

    /**
     * send email to support, if user changed his country in tax information
     *
     */
    public function notifyIfCountryChange()
    {
        $history = UserTaxInfoHistory::find()
            ->where(['user_tax_info_id' => $this->id, 'action_id' => 'update'])
            ->orderBy('id desc')->limit(15)
            ->all();
        $currentCountry = $this->place_country;
        $approved = 0;
        foreach ($history as $k => $v) {
            if ($v->action_id == 'approved') {
                $approved++;
                if ($approved == 2) {
                    break;
                }
            }
            if (empty($v['comment'])) {
                continue;
            }
            $data = json_decode($v['comment'], true);

            if (isset($data['place_country']) && !empty($data['place_country'])) {
                if ($data['place_country']['to'] == $currentCountry) {
                    // we found in history that this is the same country that was, break foreach.
                } else {
                    // seems that last change was made, was country place, we send email
                    $emailer = new Emailer();
                    $userId = $v->userTaxInfo->user_id;
                    $msg = "User tax info. Country was changed. User ID: " . $userId;
                    $msg = $msg . sprintf("\nCountry was: %s , Country now: %s", $data['place_country']['to'], $currentCountry);
                    $emailer->sendSupportMessage("Attention. Country changed. User ID: " . $userId, $msg);

                }
                break;

            }
        }
    }

    /**
     * get user tax info status
     *
     * @param \common\models\base\User $user
     * @return boolean
     */
    public static function getTaxStatus(base\User $user)
    {
        if (empty($user->userTaxInfo)) {
            return false;
        }
        return $user->userTaxInfo->status;
    }

    /**
     * turn off notify email to user;
     *
     * @param \common\models\base\User $user
     */
    public static function taxNotifyOff(base\User $user)
    {
        if (empty($user->userTaxInfo)) {

            $userTaxInfo = UserTaxInfo::findOne(['user_id' => $user->id]);
            if (empty($userTaxInfo)) {
                self::addRecord(
                    [
                        'user_id'      => $user->id,
                        'created_at'   => dbexpr('NOW()'),
                        'updated_at'   => dbexpr('NOW()'),
                        'status'       => 'new',
                        'is_usa'       => 1,
                        'email_notify' => 0
                    ]
                );
                return;
            }
        }
        self::updateRow(['user_id' => $user->id], ['email_notify' => 0]);
    }

    public function isSubmitted(): bool
    {
        return in_array($this->status,[self::STATUS_SUBMITTED, self::STATUS_CONFIRMED]);
    }
}
