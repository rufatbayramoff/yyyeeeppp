<?php
namespace common\models;

use common\components\ArrayHelper;
use Yii;

/**
 *
 */
class PrinterTechnology extends \common\models\base\PrinterTechnology
{
    public const SLS = 'SLS';
    public const SLM = 'SLM';
    public const DMLS = 'DMLS';
    public const ADAM = 'ADAM';
    public const POLYJET = 'PolyJet';
    public const MJM = 'MJM';
    public const MJF = 'MJF';

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['code', 'unique', 'skipOnEmpty' => false],
        ]);
    }


    public function getTitleCode(){
        if(!empty($this->code)){
            return $this->code;
        }
        return self::getCodeByTitle($this->title);
    }

    public function getTitleInfo(){
        if(strpos($this->title, '(')===false){
            return trim($this->title);
        }
        $code = substr($this->title, strpos($this->title, '(')+1, -1);
        return trim($code);
    }

    public static function getCodeByTitle($title)
    {
        if(strpos($title, '(')===false){
            return trim($title);
        }
        $code = substr($title, 0, strpos($title, '('));
        return trim($code);
    }


    /**
     * Return certifications examples for certification type
     * @param string $type One of constant PsPrinterTest::TYPE_
     * @return PrinterTechnologyCertificationExample[]
     */
    public function getCertificationExmplesForType(string $type) : array
    {
        return array_filter($this->printerTechnologyCertificationExamples, function (PrinterTechnologyCertificationExample $example) use ($type) {
            return $example->type == $type;
        });
    }

    /**
     * @return string[]
     */
    public static function professionalCodeTypes(): array
    {
        return [
            self::SLS,
            self::SLM,
            self::DMLS,
            self::ADAM,
            self::POLYJET,
            self::MJM,
            self::MJF,
        ];
    }
}