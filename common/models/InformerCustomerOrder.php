<?php

namespace common\models;

use common\modules\informer\interfaces\InformerStorageInterface;
use common\modules\informer\models\CustomerOrderInformer;

/**
 * Class InformerCustomerOrder
 * @package common\models
 */
class InformerCustomerOrder extends \common\models\base\InformerCustomerOrder  implements InformerStorageInterface
{
    /**
     * @var string
     */
    public $type = 'customer_order';

    /**
     * STUB, not using
     * @var array
     */
    public $paths = [];

    public $parts = [];

    public function getInformerCLass()
    {
        return CustomerOrderInformer::class;
    }

    public function saveInformer()
    {
        self::deleteAll(['key'=>$this->key]);
        foreach ($this->parts as $partKey => $partInfo) {
            $informerStored = new InformerCustomerOrder();
            $informerStored->setAttributes($this->attributes);
            $informerStored->part_key = $partKey;
            $informerStored->part_info = $partInfo;
            $informerStored->save();
        }
    }

    public static function deleteByKey($key)
    {
        self::deleteAll(['key'=>$key]);
    }

    public static function getByKey($key)
    {
        $informersStored = self::findAll(['key'=>$key]);
        if (!$informersStored) {
            return null;
        }
        /** @var InformerStoreOrderAttempt $informerStored */
        $informerStored = reset($informersStored);
        $informerStored->parts[$informerStored->part_key] = $informerStored->part_info;
        foreach ($informersStored as $oneStored) {
            $informerStored->parts[$oneStored->part_key] = $oneStored->part_info;
        }
        return $informerStored;
    }
}