<?php

namespace common\models;
use common\components\exceptions\AssertHelper;

/**
 * Class StoreOrderAttempDelivery
 * @package common\models
 */
class StoreOrderAttempDelivery extends \common\models\base\StoreOrderAttempDelivery
{
    protected $_oldTrackingNumber;

    /**
     * Set tracking number
     *
     * @param string $number
     * @param string $shipper
     *
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function setTrackingNumber(string $number, string $shipper)
    {
        $this->_oldTrackingNumber = $this->tracking_number;
        $this->tracking_number = $number;
        $this->tracking_shipper = $shipper;
        AssertHelper::assertSave($this);
    }

    /**
     * @return bool
     */
    public function isChangedTrack(): bool
    {
        return $this->_oldTrackingNumber !== $this->tracking_number;
    }

    /**
     * @return mixed
     */
    public function getOldTrackingNumber()
    {
        return $this->_oldTrackingNumber;
    }

    public function getData(): array
    {
        return [
            $this->parcel_weight,
            $this->parcel_length,
            $this->parcel_width,
            $this->parcel_height,
            'in'
        ];
    }
}