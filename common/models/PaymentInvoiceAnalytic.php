<?php

namespace common\models;

/**
 * Class PaymentInvoiceAnalytic
 * @package common\models
 */
class PaymentInvoiceAnalytic extends base\PaymentInvoiceAnalytic
{

    public static function create(string $sort,string $paymentInvoiceUuid): self
    {
        $model = new self;
        $model->sort = $sort;
        $model->payment_invoice_uuid = $paymentInvoiceUuid;
        return $model;
    }
}