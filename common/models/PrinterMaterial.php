<?php namespace common\models;

use common\interfaces\FileOwnerInterface;
use yii\helpers\Inflector;
use common\components\ActiveQuery;

/**
 * Printer material model class.
 * Helps to find any information about material
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 *
 * @property PrinterColor[] $colors active colors of material
 */
class PrinterMaterial extends \common\models\base\PrinterMaterial implements FileOwnerInterface
{
    CONST DEFAULT_MATERIAL_FILAMENT_TITLE = 'pla';

    public $slug;

    public function getCode()
    {
        return $this->filament_title;
    }

    /**
     * Relation for colors if material
     * @return \yii\db\ActiveQuery
     */
    public function getColors()
    {
        return $this->hasMany(PrinterColor::class, ['id' => 'color_id'])
            ->where(['is_active' => 1])
            ->viaTable('printer_material_to_color', ['material_id' => 'id'], function(ActiveQuery $query){
                $query->where(['is_active' => 1]);
            });
    }

    public function getSlugifyFilament()
    {
        return self::slugifyFilament($this->filament_title);
    }

    public static function slugifyFilament($filament)
    {
        $f = str_replace('+', 'plus', $filament);
        return Inflector::slug($f);
    }
    /**
     * get material density
     * 
     * @param string $materialTitle
     * @return float
     */
    public static function getDensity($materialTitle)
    {
        /** @var PrinterMaterial $material */
        $material = ($materialTitle instanceof base\PrinterMaterial) ? $materialTitle : self::tryFind(['filament_title' =>$materialTitle]);
        return (float)$material->density;
    }
    
    /**
     * get default material+color data to show on store unit page
     * TODO: выпилить когда все ссылки будут ссылаться на PrinterMaterialSerivce::getGroupMaterialsListForColorSelector
     * 
     * @return array
     */
    public static function getDefaults()
    {
        $groupMaterials = \common\models\PrinterMaterialToColor::find()
            ->joinWith('material')
            ->where([
                PrinterMaterialToColor::column('is_active') => 1,
                'is_default'                                => 1,
                PrinterMaterial::column('is_active')        => 1
            ])
            ->with(['material', 'color', 'material.group'])
            ->orderBy(['color_id'=>SORT_ASC, 'material_id'=>SORT_ASC])
            ->all();
        
        $groups = [];
        foreach($groupMaterials as $k=>$v){
            $groupTitle = $v->material->group->title;
            if(!isset($groups[$groupTitle])){
                $groups[$groupTitle] = [];
            }
            $groups[$groupTitle][] = $v;
        } 
        return $groups;
    }
    
    /**
     * if detailed, returns array of arrays with [id,color,ambient]
     * if detaild is false (default) - just returns array of render_color [White,Red,...]
     * 
     * @param boolean $detailed
     * @return array
     */
    public static function getDefaultRenderColors($detailed = false)
    {
        $groupMaterials = self::getDefaults();
        $colorsAll = [];
        $renderColors = [];
        foreach ($groupMaterials as $materials) {
            foreach ($materials as $material) {
                $colorObj = $material->color;
                $renderColor = $colorObj->render_color;
                if (!empty($colorsAll[$renderColor])) {
                    continue; // already added
                }
                $hexColor = rgb2hex(explode(",", $colorObj->rgb));
                $ambient = 0.4;
                if (!empty($colorObj->ambient)) {
                    $ambient = intval($colorObj->ambient) / 100; // if 50, gives 0.5
                }
                $colorsAll[$renderColor] = $colorObj;
                $renderColors[] = [
                    'id' => $renderColor,
                    'color' => $hexColor,
                    'ambient' => $ambient
                ];
            }
        }
        return $detailed ? $renderColors :  array_keys($colorsAll) ;
    }
}
