<?php
namespace common\models;
use \yii\helpers\Inflector;

/**
 * 
 */
class SystemReject extends \common\models\base\SystemReject
{
    public const DECLINE_ORDER = 'decline_order';
    public const USER_CANCEL_ORDER = 'user_cancel_order';
    public const DECLINE_CNC_PREORDER_PS = 'decline_cnc_preorder';
    public const DECLINE_CNC_PREORDER_CUSTOMER = 'decline_cnc_preorder_customer';
    public const SERVICES_DECLINE_PREORDER = 'decline_preorder';
    public const CUSTOMER_DECLINE_PREORDER = 'customer_decline_preorder';
    public const PS_BANNED = 'ps_banned';
    public const PS_PRINTER_BANNED = 'ps_printer_banned';
    public const PS_PRINTER_REJECT = 'ps_printer_reject';
    public const PS_REJECT = 'ps_reject';
    public const STORE_ORDER_CANCEL = 'store_order_cancel';
    public const REPORT_MESSAGE = 'report_message';
    public const COMPANY_SERVICE_REJECT = 'company_service_reject';
    public const PRINTER_REVIEW_REJECT = 'printer_review_reject';
    public const PRODUCT_REJECT = 'product_reject';
    public const USER_DELETE_REQUEST = 'user_delete_request';
    public const PRODUCT_PREORDER_DECLINE = 'product_preorder_decline';
    public const PRODUCT_ORDER_DECLINE = 'product_order_decline';

    /**
     * Get reject groups
     *
     * @return array
     */
    public static function getGroups()
    {
        $knownGroups = [
            self::PS_PRINTER_REJECT => 'PS Printer Reject',
            self::PS_PRINTER_BANNED => 'PS Printer Banned',
            self::PS_REJECT => 'PS Reject',
            self::PS_BANNED => 'PS Banned',
            'model3d'                           => 'Model 3d',
            'user'                              => 'User',
            self::STORE_ORDER_CANCEL            => 'Moderator cancel order',
            'print_model_fail'                  => 'Print Model Fail',
            self::DECLINE_ORDER                 => 'Decline order',
            self::USER_CANCEL_ORDER             => 'User cancel order',
            self::REPORT_MESSAGE                => 'Report messages',
            self::DECLINE_CNC_PREORDER_PS       => 'CNC Preorder Decline By PS',
            self::DECLINE_CNC_PREORDER_CUSTOMER => 'CNC Preorder Decline By Customer',
            self::SERVICES_DECLINE_PREORDER     => 'Services Preorder Decline',
            self::COMPANY_SERVICE_REJECT        => 'Company Service Reject',
            self::PRODUCT_REJECT                => 'Product Reject',
            self::USER_DELETE_REQUEST           => 'User Delete Request',
            self::PRODUCT_PREORDER_DECLINE      => 'Product Preorder Decline',
            self::PRODUCT_ORDER_DECLINE         => 'Product Order Decline',
        ];

        $groups = SystemReject::find()
            ->select('group')
            ->distinct()
            ->asArray()
            ->column();

        foreach ($groups as $group) {
            if (!array_key_exists($group, $knownGroups)) {
                $knownGroups[$group] = Inflector::humanize($group, true);
            }
        }

        return $knownGroups;
    }
}
