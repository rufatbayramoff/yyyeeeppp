<?php

namespace common\models;

/**
 * Class StoreUnitShoppingCandidate
 *
 * @package common\models
 */
class StoreUnitShoppingCandidate extends \common\models\base\StoreUnitShoppingCandidate
{
    const EXPIRE_TIME = '8'; // Expire time by hours

    const TYPE_API = 'api';
    const TYPE_UTM = 'utm';
    const TYPE_PRINT_HERE = 'print_here';
}