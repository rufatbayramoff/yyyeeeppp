<?php

namespace common\models;

use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\HistoryBehavior;
use common\models\factories\FileFactory;
use common\models\query\CompanyServiceQuery;
use common\models\query\PsPrinterQuery;
use common\models\repositories\FileRepository;
use common\modules\payment\fee\FeeHelper;
use common\modules\product\interfaces\ProductInterface;
use common\services\PsPrinterService;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\user\UserFacade;
use LasseRafn\InitialAvatarGenerator\InitialAvatar;
use lib\money\Money;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\helpers\Json;
use YoHang88\LetterAvatar\LetterAvatar;

/**
 * Class Ps
 *
 * @package common\models
 *
 * @property PsPrinter[] $notDeletedPrinters
 * @property CompanyService[] $notDeletedPsMachines
 * @property PsPrinter[] $psPrinters
 * @property PsCncMachine[] $psCncMachines
 * @property File[] reviewsImageFiles
 *
 */
class Ps extends base\Ps
{
    const USTATUS_ACTIVE   = 'active';
    const USTATUS_INACTIVE = 'inactive';

    const MSTATUS_DRAFT    = 'draft';
    const MSTATUS_NEW      = 'new';
    const MSTATUS_UPDATED  = 'updated';
    const MSTATUS_CHECKING = 'checking';
    const MSTATUS_CHECKED  = 'checked';
    const MSTATUS_BANNED   = 'banned';
    const MSTATUS_REJECTED = 'rejected';

    const PHONE_STATUS_NEW = 'new';

    // phone was checked by UserSms confirmation
    const PHONE_STATUS_CHECKED  = 'checked';
    const PHONE_STATUS_CHECKING = 'checking';

    public function behaviors()
    {
        return [
            'history' => [
                'class'          => HistoryBehavior::className(),
                'historyClass'   => CompanyHistory::className(),
                'foreignKey'     => 'company_id',
                'actionCode'     => 'update',
                'skipAttributes' => ['updated_at', 'created_at']
            ],
        ];
    }

    /**
     * Relation for not deleted printers
     *
     * @return CompanyServiceQuery
     */
    public function getNotDeletedPsMachines()
    {
        return $this->getCompanyServices()->andWhere('is_deleted=0');
    }

    /**
     * Relation for not deleted printers
     *
     * @return PsPrinterQuery
     * @todo rename
     */
    public function getNotDeletedPrinters()
    {
        return $this->hasMany(PsPrinter::className(), ['id' => 'ps_printer_id'])->via('notDeletedPsMachines');
    }

    /**
     * Is PS moderated
     *
     * @return bool
     */
    public function isActive(): bool
    {
        return
            in_array($this->moderator_status,
                [
                    self::MSTATUS_UPDATED,
                    self::MSTATUS_CHECKED,
                    self::MSTATUS_CHECKING
                ], true) &&
            !$this->isDeleted();
    }

    public function isCanPrintInStore()
    {
        foreach ($this->psPrinters as $psPrinter) {
            if ($psPrinter->isCanPrintInStore() && $psPrinter->isAvailable()) {
                return true;
            }
        }
        return false;
    }

    public function getQuotesCreatedToday()
    {

        $preordersQuery = $this->getPreorders()
            ->andWhere(['preorder.created_by' => Preorder::CREATED_BY_PS])
            ->andWhere(['>=', 'preorder.created_at', DateHelper::today() . ' 00:00:00']);
        $preorders      = $preordersQuery->count();
        return $preorders;
    }

    public function getQuotesLimit()
    {
        return $this->max_outgoing_quotes ? $this->max_outgoing_quotes : Yii::$app->setting->get('system.max_outgoing_quotes');
    }

    public function isMaxQuotesCreated(): bool
    {
        $current = $this->getQuotesCreatedToday();
        $limit   = $this->getQuotesLimit();
        if ($current >= $limit) {
            return true;
        }
        return false;
    }

    /**
     * Check if current PS already moderated by another moderator
     *
     * @param $userId
     * @return bool
     */
    public function checkPsIsModerated($userId)
    {
        if ($this->moderator_status == self::MSTATUS_CHECKING) {
            $data = ModerLog::find()->where(
                [
                    'action'      => 'moderate',
                    'object_type' => 'ps',
                    'object_id'   => $this->id
                ]
            )->all();

            if (isset($data[0])) {
                foreach ($data as $item) {
                    if ($item['user_id'] != $userId) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function attributeLabels()
    {
        $labels                     = parent::attributeLabels();
        $labels['picture_file_ids'] = Yii::t('app', 'Picture File IDs');
        return $labels;
    }

    /**
     * Set moderation status
     *
     * @param $psId
     * @param $userId
     * @param $status
     * @param $comment
     *
     * @return ModerLog
     */
    public function setStatus($psId, $userId, $status, $comment = '')
    {
        $this->moderator_status = $status;
        AssertHelper::assert($this->save(false));

        $model              = new ModerLog();
        $model->user_id     = $userId;
        $model->created_at  = dbexpr('NOW()');
        $model->started_at  = dbexpr('NOW()');
        $model->action      = $status;
        $model->result      = $comment;
        $model->object_type = 'ps';
        $model->object_id   = $psId;
        AssertHelper::assertSave($model);

        PsPrinterService::updatePrintersCache();

        return $model;
    }

    /**
     * Get PS Logo
     *
     * @param bool $local
     * @return string
     * @todo Move to view logic
     */
    public function getLogoUrl($local = false, $resize = false)
    {
        return self::getCircleImageByPs($this, $resize, $resize);
    }

    public function getCompanyLogo($local = false, $resize = false)
    {
        if (!empty($this->logo)) {
            return $this->getLogoUrl($local, $resize);
        }
        if ($this->logo_file_id) {
            $path = $this->logoFile->getFileUrl();
            if ($resize) {
                $path = ImageHtmlHelper::getThumbUrl($path, $resize, $resize);
            }
            return $path;
        }

        return null;
    }

    /**
     * @param bool $local
     * @param bool $resize
     *
     * @return string
     */
    public function getCompanyLogoOrDefault($local = false, $resize = false): string
    {
        return $this->getCompanyLogo($local, $resize) ?: '/static/images/defaultPrinter.png';
    }

    /**
     * Get PS Logo full path
     *
     * @return string
     * @todo Move to view logic
     */
    public function getLogoFullPath()
    {
        $userFolder = UserFacade::getUserFolder($this->user_id);
        return Yii::getAlias('@static/') . $userFolder . '/' . $this->logo;
    }

    /**
     * @param PsPrinter|CompanyService|Ps $printer
     * @return string
     * @todo Move to view logic
     */
    public static function getCircleImage($printer): string
    {
        /** @var Ps $ps */
        $ps = ($printer instanceof Ps) ? $printer : $printer->ps;
        return self::getCircleImageByPs($ps, 64);
    }

    /**
     * @param Ps $ps
     * @param int $weight
     * @param int $height
     * @return string
     */
    public static function getCircleImageByPs(Ps $ps, $weight = 64, $height = 64)
    {
        if (empty($ps->logo_circle) && $url = $ps->getCompanyLogo(false, $weight)) {
            return $url;
        }
        $userFolder = UserFacade::getUserFolder($ps->user_id);
        $path       = $ps->logo_circle
            ? param('staticUrl') . '/' . $userFolder . '/' . $ps->logo_circle
            : false;
        if ($path === false) {
            $avatar      = new LetterAvatar($ps->title, 'square', 200);
            $dirPathMain = Yii::getAlias('@frontend/runtime/resizeImgCache/');
            if (!is_dir($dirPathMain)) {
                mkdir($dirPathMain);
            }
            $dirPath = Yii::getAlias('@frontend/runtime/resizeImgCache/pslogo_') . $ps->id . '.png';
            $avatar->saveAs($dirPath);
            $fileFactory = Yii::createObject([
                'class' => FileFactory::class,
                'user'  => $ps->user
            ]);
            $fileRepo    = Yii::createObject(FileRepository::class);
            $file        = $fileFactory->createFileFromPath($dirPath);
            $file->setPublicMode(1);
            $file->setOwner(Ps::class, 'logo_file_id');
            $fileRepo->save($file);
            $ps->logo_file_id = $file->id;
            $ps->save();
            $path = $file->getFileUrl();
        }
        if ($weight) {
            $path = ImageHtmlHelper::getThumbUrl($path, $weight, $height);
        }
        return $path;
    }

    /**
     * Is phone verefied
     *
     * @return bool
     */
    public function getIsPhoneVerified()
    {
        return $this->phone_status == self::PHONE_STATUS_CHECKED;
    }

    /**
     * Set is ps excluded from printing
     *
     * @param bool $excluded true if printer must exclude from printer locator
     */
    public function setIsExcludedFromPrinting($excluded)
    {
        AssertHelper::assertBool($excluded);
        $this->is_excluded_from_printing = (int)$excluded;
        AssertHelper::assertSave($this);
        PsPrinterService::updatePrintersCache();
    }

    /**
     * Is ps excluded from printing
     *
     * @return bool
     */
    public function getIsExcludedFromPrinting()
    {
        return (bool)$this->is_excluded_from_printing;
    }

    /**
     * Is ps rejectd
     *
     * @return string
     */
    public function getIsRejected()
    {
        return $this->moderator_status == self::MSTATUS_REJECTED;
    }

    /**
     * Return last reject log item
     *
     * @return ModerLog|null
     * @throws InvalidParamException
     */
    public function getLastRejectLog()
    {
        $log = $this->getLastRejectModerLog();
        if (!$log) {
            return false;
        }
        $rejectReasonDetails = Json::decode($log->result);
        $rejectReasonStr     = '';
        if ($rejectReasonDetails['reason']) {
            $systemReject = SystemReject::findByPk($rejectReasonDetails['reason']);
            if ($systemReject) {
                $rejectReasonStr .= $systemReject->title;
            }
        }
        if ($rejectReasonDetails['descr']) {
            $rejectReasonStr .= $rejectReasonStr ? ' - ' . $rejectReasonDetails['descr'] : $rejectReasonDetails['descr'];
        }
        return $rejectReasonStr;
    }

    private function getLastRejectModerLog()
    {
        return ModerLog::find()
            ->where(['object_type' => 'ps', 'object_id' => $this->id, 'action' => self::MSTATUS_REJECTED])
            ->orderBy('id DESC')
            ->one();

    }

    /**
     * Return first untested printer. If all printers of ps tested - return null
     *
     * @return PsPrinter|null
     */
    public function getFirstUntestedPrinter()
    {
        foreach ($this->notDeletedPrinters as $printer) {
            if (!$printer->isCertificated()) {
                return $printer;
            }
        }
        return null;
    }

    /**
     * Return image callery for ps
     *
     * @param int $limit
     * @return File[]
     */
    public function getPicturesFiles($limit = 100)
    {
        if (!$this->picture_file_ids) {
            return [];
        }
        $fileIds = Json::decode($this->picture_file_ids);

        /** @var File[] $files */
        $files = File::find()
            ->id($fileIds)
            ->limit($limit)
            ->all();

        // Sorting in the same order as ids
        usort($files, function ($a, $b) use ($fileIds) {
            $sort = array_flip($fileIds);
            return $sort[$a['id']] <=> $sort[$b['id']];
        });

        return $files;
    }

    /**
     * @return File[]
     * @throws InvalidConfigException
     */
    public function getReviewsImageFiles()
    {
        $query = File::find()
            ->joinWith('reviews', false)
            ->andWhere(['store_order_review.status' => StoreOrderReview::STATUS_MODERATED])
            ->andWhere(['store_order_review.ps_id' => $this->id])
            ->orderBy(['store_order_review.id' => SORT_DESC])
            ->limit(4);
        $files = $query->all();
        return $files;
    }

    /**
     * Return image callery for ps
     *
     * @param int $limit
     * @return File[]
     */
    public function getDesignerPicturesFiles($limit = 100)
    {
        if (!$this->designer_picture_file_ids) {
            return [];
        }
        $fileIds = Json::decode($this->designer_picture_file_ids);

        /** @var File[] $files */
        $files = File::find()
            ->id($fileIds)
            ->limit($limit)
            ->all();

        return $files;
    }


    /**
     * Can user change url
     *
     * @return bool
     */
    public function canChangeUrl()
    {
        return $this->url_changes_count < 1;
    }

    public function getPsPrinters(): PsPrinterQuery
    {
        return $this->hasMany(PsPrinter::className(), ['id' => 'ps_printer_id'])->via('companyServices');
    }

    public function getPsCncMachines()
    {
        return $this->hasMany(PsCncMachine::className(), ['id' => 'ps_cnc_machine_id'])->via('companyServices');
    }

    /**
     * Is cnc functional avaliabel for ps
     *
     * @return bool
     */
    public function isCncAllowed(): bool
    {
        return true;
        // return (bool) $this->is_cnc_allowed;
    }

    /**
     * Is PS have undeleted CNC machine
     *
     * @return bool
     */
    public function haveUndeletedCncMachine(): bool
    {
        if (!$this->isCncAllowed()) {
            return false;
        }

        foreach ($this->psCncMachines as $machine) {
            if (!$machine->companyService->is_deleted) {
                return true;
            }
        }
        return false;
    }

    public function haveWidgetAllowedCncMachine(): bool
    {
        if (!$this->isCncAllowed()) {
            return false;
        }

        foreach ($this->psCncMachines as $machine) {
            if (!$machine->companyService->is_deleted && $machine->isAllowedWidget()) {
                return true;
            }
        }
        return false;
    }


    /**
     * use PsMachine::AVAILABILITY_STATUS_ANY, PsMachine::AVAILABILITY_STATUS_DELETED
     *
     * @param string $availabilityStatus
     *
     * @return CompanyService|null
     */
    public function getCncByAvailability(string $availabilityStatus)
    {
        $result = null;
        switch ($availabilityStatus) {
            case CompanyService::AVAILABILITY_STATUS_ANY:
                if (count($this->psCncMachines) > 0) {
                    foreach ($this->psCncMachines as $machine) {
                        $result = $machine;
                    }
                }
                break;
            case CompanyService::AVAILABILITY_STATUS_ACTIVE:
                foreach ($this->psCncMachines as $machine) {
                    if (!$machine->companyService->is_deleted) {
                        $result = $machine;
                        break;
                    }
                }
                break;
            case CompanyService::AVAILABILITY_STATUS_DELETED:
                foreach ($this->psCncMachines as $machine) {
                    if ($machine->companyService->is_deleted) {
                        $result = $machine;
                        break;
                    }
                }
                break;
            case CompanyService::AVAILABILITY_STATUS_PUBLIC:
                foreach ($this->psCncMachines as $machine) {
                    if (!$machine->companyService->is_deleted && $machine->companyService->isVisibleForUser(null, true)) {
                        $result = $machine;
                        break;
                    }
                }
                break;
        }
        if ($result && $result->isAllowedWidget()) {
            return $result->companyService;
        }
        return null;
    }

    /**
     * Is PS has at least one active machine.
     *
     * Be careful! If relation <notDeletedPsMachines> already loaded - method will use this relation,
     * otherwise will make request to db.
     *
     * @return bool
     */
    public function hasActiveMachines(): bool
    {
        $isPopultaed = $this->isRelationPopulated('notDeletedPsMachines');

        if ($isPopultaed) {
            return (bool)$this->notDeletedPsMachines;
        }

        return $this->getNotDeletedPsMachines()->exists();
    }

    /**
     * @return bool
     */
    public function isSettingsSaved(): bool
    {
        return (bool)$this->phone || $this->is_backend;
    }

    public function needClaim(): bool
    {
        return (bool)$this->is_backend;
    }

    public function formatPublicEmail($email)
    {
        $emailParts = explode("@", $email);
        $emailName  = $emailParts[0];
        $len        = strlen($emailName);
        $emailName  = substr($emailName, 0, 1) . str_repeat('*', $len - 2) . substr($emailParts[0], $len - 1, 1);
        return $emailName . '@' . $emailParts[1];

    }

    /**
     * was company verified by email/sms ?
     *
     * @return bool
     */
    public function isVerfied()
    {
        return !$this->is_backend;
    }

    /**
     * Add method, for export
     *
     * @return mixed
     * @throws InvalidParamException
     */
    public function getPublicCompanyLink(string $utmSource = '')
    {
        return Yii::$app->params['siteUrl'] . '/c/' . $this->url . ($utmSource ? '?utm_source=' . $utmSource : '');
    }

    public function getPublicCompanyReviewsLink(string $utmSource = '')
    {
        return Yii::$app->params['siteUrl'] . '/c/' . $this->url . '/reviews' . ($utmSource ? '?utm_source=' . $utmSource : '');
    }

    public function getPublicProductsCompanyLink(?ProductInterface $product=null)
    {
        if ($product?->getProductType() === ProductInterface::TYPE_MODEL3D) {
            return Yii::$app->params['siteUrl'] . '/c/' . $this->url . '/3d-printable-models-store';
        }
        return Yii::$app->params['siteUrl'] . '/c/' . $this->url . '/products';
    }

    public function getPublicCncCompanyLink()
    {
        return Yii::$app->params['siteUrl'] . '/c/' . $this->url . '/cnc';
    }

    public function getPublicCuttingCompanyLink()
    {
        return Yii::$app->params['siteUrl'] . '/c/' . $this->url . '/cutting';
    }

    public function isJustCreatedCompany()
    {
        $lastPeriod = DateHelper::subNow('P3M');
        return $this->created_at > $lastPeriod;
    }

    /**
     * @return array
     */
    public function materials(): array
    {
        $printers = $this->psPrinters;
        if (!$printers) {
            return [];
        }
        $materials = [];
        foreach ($printers as $printer) {
            foreach ($printer->materials as $material) {
                $materials[] = $material->material->title;
            }
        }
        return array_unique($materials);
    }

    /**
     * @return string|null
     */
    public function minimumCharge(): ?string
    {
        if (!$this->psPrinters) {
            return null;
        }
        $minOrderPrices = array_column($this->psPrinters, 'min_order_price');
        if (count($minOrderPrices) === 0) {
            return null;
        }
        $minOrderPrice = (float)min($minOrderPrices);
        if ($minOrderPrice === 0) {
            return null;
        }
        $percent = FeeHelper::printFee(Money::create($minOrderPrice, $this->currency));
        return displayAsCurrency($minOrderPrice + $percent->getAmount(), $this->currency);
    }

    /**
     * @return string|string[]|null
     */
    public function fullLocation()
    {
        $location = $this->location;
        if (!$location) {
            return null;
        }
        return UserLocation::formatLocation($this->location, '%city%, %region%, %country%');
    }

    /**
     * @param int $size
     * @return array
     * @throws InvalidConfigException
     */
    public function gallery($size = 4): array
    {
        return array_merge($this->getPicturesFiles($size), $this->getReviewsImageFiles());
    }

    /**
     * @param string $defaultCategory
     * @return array|string[]
     */
    public function serviceCategories($defaultCategory = '3D Printing'): array
    {
        $categories = [];
        if ($this->isCanPrintInStore()) {
            $categories[] = $defaultCategory;
        }
        foreach ($this->companyServices as $service) {
            if ($service->isPublished()) {
                $categories[] = $service->category->title ?? null;
            }
        }
        return array_unique($categories);
    }


    /**
     * @return array|mixed|object|string|null
     */
    public function minimumShippingPrice()
    {
        $default               = _t('ps.shipping', 'Standard Flat Rate');
        $minimumShippingPrices = [];
        foreach ($this->psPrinters as $printer) {
            foreach ($printer->psMachineDeliveries as $psDelivery) {
                $price = $psDelivery->carrier_price
                    ? displayAsCurrency($psDelivery->carrier_price, $printer->companyService->company->currency) : '';
                if (!empty($price) && $psDelivery->carrier != 'treatstock') {
                    $minimumShippingPrices[] = $price;
                }
            }
        }
        if (!empty($minimumShippingPrices)) {
            return min($minimumShippingPrices);
        }
        return $default;
    }

    public function printersCertificationData()
    {
        return null;
    }

    public function softDelete(): void
    {
        if ($this->isDeleted()) {
            throw new \DomainException("Company is already delete");
        }
        $this->is_deleted = Company::IS_DELETED_FLAG;
        $this->updated_at = DateHelper::now();
    }

    public function restore(): void
    {
        if (!$this->isDeleted()) {
            throw new \DomainException("Company is already restore");
        }
        $this->is_deleted = Company::IS_NOT_DELETED_FLAG;
        $this->updated_at = DateHelper::now();
    }

    public function isDeleted(): bool
    {
        return $this->is_deleted === Company::IS_DELETED_FLAG;
    }

    public function isServicesActive(): bool
    {
        $services = $this->notDeletedPsMachines;
        foreach ($services as $companyService) {
            if ($companyService->isAvailableForOffers()) {
                return true;
            }
        }
        return false;
    }
}