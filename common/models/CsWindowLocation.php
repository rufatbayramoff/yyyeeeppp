<?php

namespace common\models;

/**
 * Class CsWindowLocation
 * @package common\models
 */
class CsWindowLocation extends \common\models\base\CsWindowLocation
{
    public $isDeleted = false;
}