<?php

namespace common\models;

/**
 * Class ProductExpressDelivery
 * @package common\models
 */
class ProductExpressDelivery extends \common\models\base\ProductExpressDelivery
{
    /**
     * Delivery was mark deleted at form
     *
     * @var bool
     */
    public $isDeleted;
}