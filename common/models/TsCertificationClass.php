<?php

namespace common\models;

use common\components\ArrayHelper;
use common\modules\company\components\CompanyServiceInterface;
use lib\money\Money;

/**
 * Class TsCertificationClass
 *
 * @package common\models
 * @property Money $priceMoney
 */
class TsCertificationClass extends \common\models\base\TsCertificationClass
{
    public const DEFAULT_PRINTERS_CERT = 10;
    public const MAX_SYSTEM_ID = self::DEFAULT_PRINTERS_CERT;

    public function isForPrinters()
    {
        return $this->service_type === CompanyServiceInterface::TYPE_PRINTER;
    }

    public static function getDropDownList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'title');
    }

    public function getPriceMoney()
    {
        return Money::create($this->price, $this->currency);
    }

}