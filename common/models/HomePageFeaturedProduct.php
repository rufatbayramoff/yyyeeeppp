<?php

namespace common\models;

use common\modules\product\interfaces\ProductInterface;

/**
 * Class HomePageFeaturedProduct
 * @package common\models
 * @property \common\models\Product $publishedProduct
 */
class HomePageFeaturedProduct extends \common\models\base\HomePageFeaturedProduct
{
    public static function primaryKey()
    {
        return ['featured_category_id', 'product_uuid'];
    }

    /**
     * @return \yii\db\ActiveQuery|\common\models\query\ProductQuery
     */
    public function getPublishedProduct()
    {
        return $this->hasOne(\common\models\Product::class, ['uuid' => 'product_uuid'])
            ->andWhere([
                'is_active' => 1,
                'product_status' => [ProductInterface::STATUS_PUBLISHED_PUBLIC, ProductInterface::STATUS_PUBLISH_PENDING]
            ])
            ->inverseOf('homePageFeaturedProducts');
    }
}