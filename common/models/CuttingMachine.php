<?php

namespace common\models;

use common\components\exceptions\InvalidModelException;
use common\components\exceptions\ValidationException;
use common\components\ps\locator\Size;
use common\modules\cutting\models\CutSize;
use yii\web\HttpException;

/**
 * Class CuttingMachine
 * @package common\models
 */
class CuttingMachine extends \common\models\base\CuttingMachine
{
    public const LASER_TECHNOLOGY      = 'laser';
    public const PLASMA_TECHNOLOGY     = 'plasma';
    public const CNC_ROUTER_TECHNOLOGY = 'cnc_router';
    public const WATER_JET_TECHNOLOGY  = 'water_jet';
    public const KNIFE_TECHNOLOGY      = 'knife';
    public const OXY_FUEL_TECHNOLOGY   = 'oxy-fuel';
    public const OTHER_TECHNOLOGY      = 'other';

    public const RECCOMEND_MIN_ORDER_PRICE_IN_USD = 10;

    public static function getTechnologyLabels()
    {
        return [
            static::LASER_TECHNOLOGY      => _t('site.cutting', 'Laser Cutting'),
            static::PLASMA_TECHNOLOGY     => _t('site.cutting', 'Plasma Cutting'),
            static::CNC_ROUTER_TECHNOLOGY => _t('site.cutting', 'CNC Router'),
            static::WATER_JET_TECHNOLOGY  => _t('site.cutting', 'Water-jet Cutting'),
            static::KNIFE_TECHNOLOGY      => _t('site.cutting', 'Knife'),
            static::OXY_FUEL_TECHNOLOGY   => _t('site.cutting', 'Oxy-fuel'),
            static::OTHER_TECHNOLOGY      => _t('site.cutting', 'Other')
        ];
    }

    public function getTechnologyLabel()
    {
        return static::getTechnologyLabels()[$this->technology] ?? '';
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        $noErrors = parent::validate($attributeNames, $clearErrors);
        if (!$this->validateCompanyService()) {
            $noErrors = false;
        }
        return $noErrors;
    }

    /**
     * Throw exception if validation failed
     * @throws ValidationException
     */
    public function validateOrException()
    {
        $this->validate();
        if ($this->hasErrors() || $this->companyService->hasErrors()) {
            throw new ValidationException([$this, $this->companyService]);
        }
    }

    public function validateProcessingOrException()
    {
        $invalidModels = [];
        foreach ($this->cuttingMachineProcessings as $process) {
            if (!$process->validate()) {
                throw new InvalidModelException($process);
            }
        }
    }

    public function fullValidateProcessing()
    {
        $this->validateProcessingOrException();
        foreach ($this->cuttingMachineProcessings as $process) {
            $process->scenario = CuttingMachineProcessing::SCENARIO_FULL;
            if (!$process->validate()) {
                throw new InvalidModelException($process);
            }
        }
    }

    protected function validateCompanyService()
    {
        $errors = $this->companyService->validate();
        return $errors;
    }

    public function checkProcessing()
    {
        $alarms = [];
        foreach ($this->cuttingMachineProcessings as $processing) {
            $alarms[] = $processing->checkProcessing();
        }
        return $alarms;
    }

    public function setPsMachine(CompanyService $companyService)
    {
        $this->populateRelation('companyService', $companyService);
    }

    public function getDeliveries()
    {
        return $this->getPsMachineDeliveries();
    }

    public function getWorkSize()
    {
        return CutSize::create($this->max_width, $this->max_length, 0);
    }

    public function getPsMachineDeliveries()
    {
        return $this->hasMany(\common\models\PsMachineDelivery::class, ['ps_machine_id' => 'id'])->via('companyService');
    }

    public function getProcessing(CuttingMaterial $cuttingMaterial, float $thickness): ?CuttingMachineProcessing
    {
        foreach ($this->cuttingMachineProcessings as $processing) {
            if (($processing->cutting_material_id == $cuttingMaterial->id) && (abs($thickness - $processing->thickness) < 0.001)) {
                return $processing;
            }
        }
        return null;
    }

    /**
     * @return CuttingWorkpieceMaterial[]
     */
    public function getWorkpieceMaterials(): array
    {
        $allWorkpieceMaterials     = $this->companyService->company->cuttingWorkpieceMaterials;
        $allowedWorkpieceMaterials = [];
        foreach ($this->cuttingMachineProcessings as $processing) {
            foreach ($allWorkpieceMaterials as $workpieceMaterial) {
                if (($workpieceMaterial->material_id === $processing->cutting_material_id) &&
                    ($workpieceMaterial->thickness === $processing->thickness)) {
                    $allowedWorkpieceMaterials[$workpieceMaterial->uuid] = $workpieceMaterial;
                }
            }
        }
        return $allowedWorkpieceMaterials;
    }
}