<?php
namespace common\models;

use lib\geo\models\Location;
use Yii;
use yii\base\InvalidParamException;

/**
 * Edit this file
 * This is the model class for table "geo_location".
 */
class GeoLocation extends \common\models\base\GeoLocation
{
    public function setLocationArr($locationArr)
    {
        if (array_key_exists('country', $locationArr) && $locationArr['country']) {
            $isoCode = $locationArr['country'];
            if (strlen($isoCode) === 3) {
                $geoCountry = GeoCountry::findOne(['iso_alpha3' => $isoCode]);
            } else {
                $geoCountry = GeoCountry::findOne(['iso_code' => $isoCode]);
            }

            if (!$geoCountry) {
                throw new InvalidParamException('Invalid country iso alpha3 code');
            }
            $this->country_id = $geoCountry->id;
        }
        if (array_key_exists('ip', $locationArr) && $locationArr['ip']) {
            $ip = $locationArr['ip'];
            $location = Yii::$app->geo->getLocationByDomainOrIp('', $ip, true);
            $this->setInitValuesFromLocation($location);
        }
        if (array_key_exists('lat', $locationArr) && array_key_exists('lon', $locationArr)) {
            $this->lat = $locationArr['lat'];
            $this->lon = $locationArr['lon'];
        }
    }

    /**
     * @param Location $location
     */
    public function setInitValuesFromLocation(Location $location)
    {
        $this->lat = $location->lat;
        $this->lon = $location->lon;
        if ($location->country) {
            $geoCountry = GeoCountry::findOne(['iso_code' => $location->country]);
            if ($geoCountry) {
                $this->country_id = $geoCountry->id;
                $this->populateRelation('country', $geoCountry);

                if ($location->region) {
                    $geoRegion = GeoRegion::findOne(['title' => $location->region, 'country_id' => $geoCountry->id]);
                    if ($geoRegion) {
                        $this->region_id = $geoRegion->id;
                        $this->populateRelation('region', $geoRegion);

                        if ($location->city) {
                            $geoCity = GeoCity::findOne(['title' => $location->city, 'region_id' => $geoRegion->id]);
                            if ($geoCity) {
                                $this->city_id = $geoCity->id;
                                $this->populateRelation('city', $geoCity);
                            }
                        }
                    }
                }
            }
        }
    }

}