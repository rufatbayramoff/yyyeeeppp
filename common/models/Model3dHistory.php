<?php
namespace common\models;

use DateTime;
use DateTimeZone;
use Yii;

/**
 * This is the model class for table "model3d_history".
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class Model3dHistory extends \common\models\base\Model3dHistory
{

    const ACTION_EDIT = 'edit';
    const ACTION_EDITTAG = 'edittag';
    const ACTION_REMOVE = 'remove';
    const ACTION_UPDATE = 'update';
    const ACTION_PUBLISH = 'publish';
    const ACTION_UNPUBLISH = 'unpublish';
    const ACTION_UPDATE_STORE_UNIT = 'update_store_unit';
    const ACTION_UPDATE_PART = 'update_model3d_part';
    const ACTION_UPDATE_IMG = 'update_model3d_img';
    const ACTION_UPDATE_TEXTURE = 'update_texture';

    /**
     * log model3d history changes
     *
     * @param int $userId
     * @param \common\models\base\Model3d $model3dObj
     * @param string $action
     * @param $comment
     * @param null $source
     * @param null $result
     * @return bool
     * @internal param string $message
     */
    public static function log($userId, base\Model3d $model3dObj, $action, $comment, $source = null, $result = null)
    {
        $historyObj = new Model3dHistory();
        $historyObj->action_id = $action;
        $historyObj->comment = $comment;
        $historyObj->source = $source;
        $historyObj->result = $result;
        $historyObj->user_id = $userId;
        $historyObj->model3d_id = $model3dObj->id;
        $historyObj->created_at = (new DateTime('now' ,new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        return $historyObj->safeSave();
    }
}