<?php namespace common\models;

use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use console\jobs\QueueGateway;
use Yii;

/**
 * File queue jobs
 * any job working with file table, should use this class
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class FileJob extends \common\models\base\FileJob
{
    const JOB_CONVERTER = 'converter';
    const JOB_RENDER = 'render';
    const JOB_CNC_ANALYZE = 'analyze-job';
    const JOB_RENDER_360 = 'render-360';
    const JOB_PARSER = 'parser';
    const JOB_ANTIVIRUS = 'antivirus';
    const JOB_SLICER = 'slicer';
    const JOB_PREPARE_DOWNLOAD_FILES = 'prepare-download-files';

    // Cutting
    const JOB_CUTTING_CONVERT = 'cutting-converter';

    /**
     * Mark job as complited
     */
    public function markAsCompleted()
    {
        $this->finished_at = DateHelper::now();
        $this->updated_at = DateHelper::now();
        $this->status = QueueGateway::JOB_STATUS_COMPLETED;
        AssertHelper::assertSave($this);
    }

    /**
     * Set job result
     *
     * @param $result
     */
    public function setResult($result)
    {
        $this->result = \yii\helpers\Json::encode($result);
        $this->updated_at = DateHelper::now();
        AssertHelper::assertSave($this);
    }

    /**
     * Mark job as failed
     */
    public function markAsFailed($result = '')
    {
        if (!empty($result)) {
            $this->result = \yii\helpers\Json::encode(['success' => false, 'message' => $result]);
        }
        $this->status = QueueGateway::JOB_STATUS_FAILED;
        $this->updated_at = DateHelper::now();
        AssertHelper::assertSave($this);
    }

    /**
     * Return job args
     *
     * @return mixed
     */
    public function getArgsData()
    {
        return is_array($this->args) ? $this->args : \yii\helpers\Json::decode($this->args);
    }

    /**
     * Return result data
     *
     * @return mixed
     */
    public function getResultData()
    {
        return \yii\helpers\Json::decode($this->result);
    }
}
