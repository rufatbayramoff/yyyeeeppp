<?php
/**
 * Created by mitaichik
 */

namespace common\models\machines;


use common\models\DeliveryType;
use common\models\GeoCountry;
use common\models\PaymentExchangeRate;
use common\models\PsMachineDelivery;
use common\models\StoreOrder;
use common\models\UserAddress;
use common\models\UserLocation;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserFacade;
use common\modules\payment\fee\FeeHelper;
use yii\helpers\Json;

/**
 * Trait MachineDeliveryParamsTrait
 * @package common\traits
 *
 * @property UserLocation $location
 * @property PsMachineDelivery[] $deliveryTypess
 */
trait MachineDeliveryParamsTrait 
{
    /**
     * @param string $type
     * @return array
     */
    public function getCarriers(string $type = 'domestic') : array
    {
        return [
            [
                'id'        => "treatstock",
                'is_active' => $this->canEasyPost($type),
                'title'     => _t('ps.delivery', "Treatstock Delivery Service")
            ],
            [
                'id'        => "myself",
                'is_active' => true,
                'title'     => _t('ps.delivery', "Self Ship")
            ]
        ];
    }

    /**
     * @param $type
     * @return bool
     */
    public function canEasyPost($type) : bool
    {
        if ($this->location) {
            $country = $this->location->country;
        } else {
            $country = GeoCountry::findOne(['iso_code' => UserSessionFacade::getLocation()->country]);
        }

        return $type == 'domestic' ? $country->is_easypost_domestic : $country->is_easypost_intl;
    }

    /**
     * @return string
     */
    public function getDefaultCarrier() : string
    {
        $carrier = DeliveryType::CARRIER_MYSELF;
        $userCountry = UserAddress::findUserCountry(UserFacade::getCurrentUserId());
        if ($this->location) {
            $userCountry = $this->location->country->iso_code;
        }
        $sessionLocation = UserSessionFacade::getLocation();
        if ($sessionLocation->country) {
            $userCountry = $sessionLocation->country;
        }
        $countryObj = GeoCountry::findOne(['iso_code' => $userCountry]);
        if ($countryObj && $countryObj->is_easypost_domestic && $countryObj->is_easypost_intl) {
            $carrier = DeliveryType::CARRIER_TS;
        }
        if ($userCountry == 'US') {
            $carrier = DeliveryType::CARRIER_TS;
        }
        return $carrier;
    }

    /**
     * check if for given order delivery is free
     *
     * @TODO - refactoring required
     *
     * @param StoreOrder $storeOrder
     * @return bool
     * @throws \Exception
     */
    public function isFreeDelivery(StoreOrder $storeOrder) : bool
    {
        $printPrice = $storeOrder->getPrimaryInvoice()->storeOrderAmount->getAmountPrintForPs();
        if (!$printPrice) {
            return false;
        }

        $orderDeliveryDetails = $storeOrder->getOrderDeliveryDetails();
        if ($orderDeliveryDetails && !empty($orderDeliveryDetails['free_delivery'])) {
            if ($printPrice->getAmount() >= $orderDeliveryDetails['free_delivery']) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return boolean
     */
    public function onlyPickup() : bool
    {
        if (count($this->deliveryTypess) > 1) {
            return false;
        }
        return $this->hasDeliveryPickup();
    }

    /**
     *
     * @return boolean
     */
    public function hasDeliveryPickup() : bool
    {
        return (bool)$this->getPsDeliveryTypeByCode(DeliveryType::PICKUP);
    }

    /**
     *
     * @return boolean
     */
    public function hasDomesticDelivery() : bool
    {
        return (bool)$this->getPsDeliveryTypeByCode(DeliveryType::STANDARD);
    }

    public function hasInternationalDelivery(): bool
    {
        return (bool)$this->getPsDeliveryTypeByCode(DeliveryType::INTERNATIONAL);
    }

    /**
     * only domestic delivery
     *
     * @return boolean
     */
    public function onlyDomestic() : bool
    {
        return !$this->getPsDeliveryTypeByCode(DeliveryType::INTERNATIONAL);
    }

    /**
     * Return delivery type instance by type
     *
     * @param string $deliveryType Constant like DeliveryType::PICKUP
     * @return PsMachineDelivery|null
     */
    public function getPsDeliveryTypeByCode(string $deliveryType) : ?PsMachineDelivery
    {
        foreach ($this->psMachineDeliveries as $deliveryTypeInstance) {
            if ($deliveryTypeInstance->deliveryType->code == $deliveryType) {
                return $deliveryTypeInstance;
            }
        }
        return null;
    }

    /**
     * user_location to user_address converter
     *
     * @return \common\models\UserAddress
     */
    public function getUserAddress() : UserAddress
    {
        $userAddress = $this->location->toUserAddress();
        $userAddress->phone = '+' . $this->ps->phone_code . $this->ps->phone;
        return $userAddress;
    }

    /**
     *
     *
     * @return PsMachineDelivery[]
     */
    public function getAvailableDeliverieTypes() : array
    {
        return $this->deliveryTypess;
    }

    /**
     * @return UserLocation
     */
    public function getCurrentLocation() : UserLocation
    {
        return $this->location;
    }
}