<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.05.18
 * Time: 16:39
 */

namespace common\models;


use common\components\DateHelper;
use common\components\exceptions\ValidationException;
use common\models\query\Model3dQuery;
use common\models\query\ProductQuery;

/**
 * Class Company
 * @package common\models
 *
 * @property \common\models\UserLocation calculatedLocation
 * @property \common\models\Product[] $products
 * @property \common\models\Model3d[] $model3ds
 * @property \common\models\SiteTag[] $siteTags
 * @property \common\models\StoreOrder[] $orders
 * @property PaymentCurrency $paymentCurrency
 */
class Company extends Ps
{
    public const IS_DELETED_FLAG = 1;
    public const IS_NOT_DELETED_FLAG = 0;
    public const IS_NOT_DESIGNER = 0;

    public const MANUFACTURING_BUSINESS = 'Manufacturing Business';
    public const SERVICE_BUSINESS = 'Service Business';
    public const MERCHANDISING_BUSINESS = 'Merchandising Business';
    public const AFFILIATE_BUSINESS = 'Affiliate Business';
    public const DEALER = 'Dealer';
    public const DISTRIBUTOR = 'Distributor';


    /**
     * @param $link
     * @param null $host
     *
     * @return string
     */
    public function linkFormatting($link, $host = null): string
    {
        if (empty($link)) {
            return '';
        }

        if (!preg_match('/^(http(s)?:\/\/)/ui', $link)) {
            $link = 'http://' . $link;
        }

        if ($host && $parseUrl = parse_url($link)) {
            $link = trim($host, '/') . '' . ($parseUrl['path'] ?? '/' . $parseUrl['host']);
        }

        return \H($link);
    }

    public function isCustomerServiceCompany()
    {
        return $this->user->isCustomerServiceCompany();
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getTitleLabel()
    {
        return $this->title;
    }

    public function getCalculatedLocation(): ?UserLocation
    {
        if ($this->location) {
            return $this->location;
        }
        foreach ($this->companyServices as $companyService) {
            if ($companyService->location) {
                return $companyService->location;
            }
        }
        return null;
    }

    public function getProductsStoreUrl($widget = false, $count = 100, $categoryId = null, $sort = null)
    {
        $url = $this->getPublicCompanyLink() . '/products?' .
            ($widget ? 'widget=1&' : '') .
            ($count != 100 ? 'count=' . $count . '&' : '') .
            ($categoryId ? 'category=' . $categoryId . '&' : '') .
            ($sort ? 'sort=' . $sort . '&' : '');
        return substr($url, 0, -1);
    }

    /**
     * @return string
     */
    public function getFacebookFormatLink(): string
    {
        return $this->linkFormatting($this->facebook, 'https://www.facebook.com');
    }

    /**
     * @return string
     */
    public function getInstagramFormatLink(): string
    {
        return $this->linkFormatting($this->instagram, 'https://www.instagram.com');
    }

    /**
     * @return string
     */
    public function getTwitterFormatLink(): string
    {
        return $this->linkFormatting($this->twitter, 'https://twitter.com');
    }

    public function canMakeOffer(): bool
    {
        return $this->isActive();
    }

    public function allowShowBeFirstCommentator(): bool
    {
        return !$this->psCatalog || !$this->psCatalog->rating_count;
    }

    public function getProducts(): ProductQuery
    {
        return $this->hasMany(Product::class, ['product_common_uid' => 'uid'])->via('productCommons');
    }

    public function getModel3ds(): Model3dQuery
    {
        return $this->hasMany(Model3d::class, ['product_common_uid' => 'uid'])->via('productCommons');
    }

    public function getCompanyServiceTags()
    {
        return $this->hasMany(CompanyServiceTag::class, ['company_service_id' => 'id'])->via('companyServices');
    }

    public function getSiteTags()
    {
        return $this->hasMany(\common\models\SiteTag::class, ['id' => 'tag_id'])->via('companyServiceTags');
    }


    public function productsCount(): int
    {
        return $this->getProducts()->published()->count();
    }

    public function modelsCount(): int
    {
        return $this->getModel3ds()->published()->count();
    }

    public function canPreorder(): bool
    {
        return $this->isServicesActive() ||
            $this->isCanPrintInStore() ||
            $this->modelsCount() > 0 ||
            $this->productsCount() > 0;
    }

    public function isCanCuttingInStore()
    {
        $result = $this->isServicesActive();
        if ($result === false) {
            return false;
        }
        return CuttingMachine::find()->company($this)->availableForOffers()->exists();
    }

    /**
     * @param string $title
     * @param string $description
     * @param UserLocation $location
     */
    public function edit(string $title, string $description, UserLocation $location): void
    {
        $this->title       = $title;
        $this->description = $description;
        $this->location_id = $location->id;
        $this->country_id  = $location->country_id;
        $this->updated_at  = DateHelper::now();
        $this->setNeedModeratorApprove();
    }

    /**
     * @return bool
     */
    public function serviceExist(): bool
    {
        return $this->getNotDeletedPsMachines()->exists() || $this->getNotDeletedPrinters()->exists();
    }

    /**
     * @param string|null $phone
     * @param string|null $phoneCode
     * @param string|null $phoneCountryIso
     * @return bool
     */
    public function equalPhone(
        ?string $phone,
        ?string $phoneCode,
        ?string $phoneCountryIso): bool
    {
        return
            $this->phone == $phone &&
            $this->phone_code == $phoneCode &&
            $this->phone_country_iso == $phoneCountryIso;
    }

    public function setNeedModeratorApprove(): void
    {
        if (!in_array($this->moderator_status, [Company::MSTATUS_NEW, Company::MSTATUS_DRAFT, Company::MSTATUS_BANNED])) {
            $this->moderator_status = $this->moderator_status == Ps::MSTATUS_REJECTED
                ? Ps::MSTATUS_NEW
                : Ps::MSTATUS_UPDATED;
        }
    }

    public function validateOrException()
    {
        if (!$this->validate()) {
            throw new ValidationException($this);
        }
    }

    public function getPaymentCurrency()
    {
        return $this->hasOne(\common\models\PaymentCurrency::class, ['currency_iso' => 'currency'])->inverseOf('ps');
    }

    public function getCashbackPercentPlus()
    {
        return $this->cashback_percent * 2;
    }

    public function getPossibleCountry(): ?GeoCountry
    {
        if ($this->country) {
            return $this->country;
        }
        $psPrinters = $this->getPsPrinters()->visible()->all();
        foreach ($psPrinters as $psPrinter) {
            return $psPrinter->companyService->location->country;
        }
        return null;
    }

    /**
     * @return \common\models\query\StoreOrderQuery
     */
    public function getOrders()
    {
        return $this->hasMany(\common\models\StoreOrder::class, ['id' => 'order_id'])->via('storeOrderAttemps');
    }

}