<?php

namespace common\models;

use common\components\UuidHelper;
use common\modules\product\interfaces\ProductInterface;
use lib\money\Money;

/**
 * Class ProductCommon
 * @package common\models
 * @property \common\models\Company $company
 * @property Money $singlePrice
 *
 */
class ProductCommon extends \common\models\base\ProductCommon
{
    public function rules()
    {
        return [
            [['uid', 'created_at', 'updated_at', 'is_active', 'single_price', 'price_currency'], 'required'],
            [['category_id', 'company_id', 'user_id', 'is_active'], 'integer'],
            [['product_status', 'description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['single_price'], 'number'],
            [['uid'], 'string', 'max' => 6],
            [['price_currency'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public static function generateUid(): string
    {
        do {
            $uid = strtoupper(UuidHelper::generateRandCode(6));
        } while (static::find()->where(['uid' => $uid])->withoutStaticCache()->one());

        return $uid;
    }

    public function getType()
    {
        return $this->model3d ? ProductInterface::TYPE_MODEL3D: ProductInterface::TYPE_PRODUCT;
    }

    /**
     * @return Model3d|Product
     */
    public function getExtObject()
    {
        return $this->model3d?$this->model3d:$this->product;
    }

    public function getCompany()
    {
        return $this->hasOne(\common\models\Company::class, ['id' => 'company_id']);
    }

    public function getSinglePrice():Money
    {
        return Money::create($this->single_price, $this->company->currency);
    }
}