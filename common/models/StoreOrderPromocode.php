<?php

namespace common\models;

use lib\money\Money;

/**
 * Class StoreOrderPromocode
 * @package common\models
 */
class StoreOrderPromocode extends \common\models\base\StoreOrderPromocode
{
    /**
     * @return Money
     */
    public function getAmountTotal(): Money
    {
        return Money::create($this->amount, $this->amount_currency);
    }
}