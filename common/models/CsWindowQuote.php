<?php

namespace common\models;

use yii\helpers\Url;

/**
 * Class CsWindowQuote
 * @package common\models
 */
class CsWindowQuote extends \common\models\base\CsWindowQuote
{
    public const STATUS_NEW = 'new';
    public const STATUS_VIEWED = 'viewed';
    public const STATUS_RESOLVED = 'resolved';
    public const STATUS_DELETED = 'deleted';

    /**
     * @return string
     */
    public function getUrlQuoteDetails($scheme = false): string
    {
        return Url::toRoute(['/mybusiness/cs-window/manage/quote', 'quoteUid' => $this->uid], $scheme);
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->status === static::STATUS_NEW;
    }
}