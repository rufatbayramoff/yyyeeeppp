<?php

namespace common\models;

/**
 * Class CsWindow
 * @package common\models
 */
class CsWindow extends \common\models\base\CsWindow
{

    /**
     *  New created product
     */
    public const STATUS_DRAFT = 'draft';

    /**
     * published in catalog
     */
    public const STATUS_PUBLISHED_PUBLIC = 'published_public';

    /**
     * Published was updated and need to be post checked
     */
    public const STATUS_PUBLISHED_UPDATED = 'published_updated';

    /**
     *  published but pre moderation is required.
     */
    public const STATUS_PUBLISH_NEED_MODERATION = 'publish_needmoderation';

    /**
     *  published and waiting for post moderation
     */
    public const STATUS_PUBLISH_PENDING = 'publish_pending';

    /**
     *  showed to anybody, who have direct link
     */
    public const STATUS_PUBLISHED_DIRECTLY = 'published_directly';

    /**
     * Product is rejected by moderator
     */
    public const STATUS_REJECTED = 'rejected';

    /**
     * Product will be available for only specific users
     */
    public const STATUS_PRIVATE = 'private';

    /**
     * used to save company_service.is_active field
     * @var boolean
     */
    public $is_active;
    public $status;

    public function behaviors()
    {
        return [
            'history' => [
                'class' => \common\components\HistoryBehavior::class,
                'historyClass' => CsWindowHistory::class,
                'foreignKey' => 'cs_window_id'
            ],
        ];
    }
    public function hasErrors($attribute = null)
    {
        if (!$this->isRelationPopulated('csWindowProfiles')) {
            $this->addError('csWindowProfiles', _t('mybusiness.window', 'Please add profile information'));
        }
        if (!$this->isRelationPopulated('csWindowGlasses')) {
            $this->addError('csWindowGlasses', _t('mybusiness.window', 'Please add glass information'));
        }
        return parent::hasErrors($attribute);
    }
    public static function getProductStatusLabels()
    {
        $labels = [
            self::STATUS_DRAFT              => _t('site.store', 'Draft'),
            self::STATUS_PUBLISHED_PUBLIC   => _t('site.store', 'Published'),
            self::STATUS_PUBLISHED_UPDATED       => _t('site.store', 'Published (updated)'),
            self::STATUS_PUBLISH_PENDING         => _t('site.store', 'Pending moderation'),
            self::STATUS_PUBLISHED_DIRECTLY      => _t('site.store', 'Shared by link'),
            self::STATUS_REJECTED                => _t('site.store', 'Reject'),
            self::STATUS_PRIVATE                 => _t('site.store', 'Private'),
            self::STATUS_PUBLISH_NEED_MODERATION => _t('site.store', 'Awaiting moderation'),
        ];
        return $labels;
    }
}