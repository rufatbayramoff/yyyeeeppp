<?php

namespace common\models;

use Yii;
use yii\helpers\Url;

/**
 * Class BlogPost
 * @package common\models
 */
class BlogPost extends \common\models\base\BlogPost
{
    /**
     * @param int $limit
     *
     * @return BlogPost[]
     */
    public static function getLatest($limit = 3): array
    {
        return BlogPost::find()
            ->active()
            ->orderBy(['date' => SORT_DESC])
            ->limit($limit)
            ->all();
    }

    /**
     * @return string
     */
    public function getViewUrl(): string
    {
        return Url::toRoute(['/blog/view', 'alias' => $this->alias], true);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getDateFormatted(): string
    {
        return Yii::$app->formatter->asDate($this->date);
    }
}