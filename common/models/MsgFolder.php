<?php
namespace common\models;

/**
 *
 */
class MsgFolder extends \common\models\base\MsgFolder
{
    const FOLDER_ID_ARCHIVE = 5;
    const FOLDER_ID_COMPLETED = 6;
    const FOLDER_ID_SUPPORT = 2;
    /**
     * Folder aliases
     */
    const FOLDER_ALIAS_PERSONAL = 'personal';
    /**
     * Folder ids
     */
    const FOLDER_ID_PERSONAL = 1;
    const FOLDER_ID_PRINTED_JOBS = 6;
    const FOLDER_ID_PRINT_JOBS = 3;
    const FOLDER_ID_ORDERS = 4;
}