<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.08.17
 * Time: 13:03
 */

namespace common\models\loggers;

use common\components\DateHelper;
use common\models\loggers\ModelArLogger;
use common\models\PsCncMachine;
use common\models\PsCncMachineHistory;
use common\models\CompanyService;
use common\models\PsMachineHistory;
use frontend\models\user\UserFacade;

class PsMachineLogger extends ModelArLogger
{
    public function log(CompanyService $psMachine, $isNewRecord = false)
    {
        $this->logAttributes($psMachine, PsMachineHistory::class, $isNewRecord );
    }
}