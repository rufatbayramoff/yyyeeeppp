<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.08.17
 * Time: 13:03
 */

namespace common\models\loggers;

use common\components\DateHelper;
use common\models\loggers\ModelArLogger;
use common\models\Preorder;
use common\models\PreorderHistory;
use common\models\PreorderWork;
use common\models\PsCncMachine;
use common\models\PsCncMachineHistory;
use common\models\CompanyService;
use common\models\PsMachineHistory;
use frontend\models\user\UserFacade;

class PreorderLogger extends ModelArLogger
{
    /**
     * @param Preorder $preorder
     */
    public function create(Preorder $preorder): void
    {
        $this->logAttributes($preorder, PreorderHistory::class, true, 'create');
    }

    public function createWork(PreorderWork $preorderWork): void
    {
        $this->logAttributes(
            model: $preorderWork,
            isNewRecord: true,
            loggerClass: PreorderHistory::class,
            action: 'create-work',
            masterModelId: $preorderWork->preorder_id
        );
    }

    public function logWork(PreorderWork $preorderWork)
    {
        $this->logAttributes(
            model: $preorderWork,
            isNewRecord: $preorderWork->isNewRecord,
            loggerClass: PreorderHistory::class,
            action: 'save-work',
            masterModelId: $preorderWork->preorder_id
        );
    }

    /**
     * @param Preorder $preorder
     */
    public function offer(Preorder $preorder): void
    {
        $this->logAttributes($preorder, PreorderHistory::class, false, 'offer');
    }

    /**
     * @param Preorder $preorder
     */
    public function accept(Preorder $preorder): void
    {
        $this->logAttributes($preorder, PreorderHistory::class, false, 'accept');
    }

    /**
     * @param Preorder $preorder
     */
    public function pay(Preorder $preorder): void
    {
        $this->logAttributes($preorder, PreorderHistory::class, false, 'pay');
    }

    public function log(Preorder $preorder, $action='')
    {
        $this->logAttributes($preorder, PreorderHistory::class, $preorder->isNewRecord, $action);
    }
}