<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.08.17
 * Time: 10:37
 */

namespace common\models\loggers;


use common\components\DateHelper;
use common\interfaces\ModelHistoryInterface;
use common\models\PsMachineHistory;
use common\models\User;
use frontend\models\user\UserFacade;

class ModelArLogger
{
    public function fillBaseAttributes(ModelHistoryInterface $history, $model)
    {
        if ($model->hasAttribute('id') && $model->id) {
            $history->setModelId($model->id);
        }
        $history->setCreatedAt(DateHelper::now());
        $history->setUserId(UserFacade::getCurrentUserId()??User::USER_ANONIM);
        $history->setActionId('updateAttributes');
    }

    public function logAttributes($model, $loggerClass, $isNewRecord=false, $action='', $comment='', $masterModelId=0)
    {
        $oldAttributes = [];
        $newAttributes = [];
        foreach ($model->oldAttributes as $k => $v) {
            $oldAttributes[$k] = is_array($v) ? json_encode($v) : $v;
        }
        foreach ($model->attributes as $k => $v) {
            $newAttributes[$k] = is_array($v) ? json_encode($v) : $v;
        }
        $diffBefore = array_diff($oldAttributes, $newAttributes);
        $diffAfter  = array_diff($newAttributes, $oldAttributes);
        if (!($diffBefore || $isNewRecord || $action)) {
            return null;
        }

        $modelHistory          = new $loggerClass();
        $this->fillBaseAttributes($modelHistory, $model);
        if ($masterModelId) {
            $modelHistory->setModelId($masterModelId);
        }
        if ($action) {
            $modelHistory->action_id = $action;
        }
        if ($comment) {
            $modelHistory->comment = $comment;
        }
        if ($isNewRecord) {
            $modelHistory->source = [];
            $modelHistory->result = $newAttributes;
        } else {
            $source = $diffBefore;
            if (!empty($source['description']) && is_string($source['description'])) {
                $source['description'] = 'Length: ' . strlen($source['description']);
            }
            $modelHistory->source = $source;
            $result = $diffAfter;
            if (!empty($result['description']) && is_string($result['description'])) {
                $result['description'] = 'Length: ' . strlen($result['description']);
            }
            $modelHistory->result = $result;
        }

        $modelHistory->safeSave();
    }
}