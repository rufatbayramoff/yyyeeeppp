<?php

namespace common\models\loggers;

use common\models\Preorder;
use common\models\Product;
use common\models\ProductCommon;
use common\models\ProductHistory;

class ProductLogger extends ModelArLogger
{
    /**
     * @param Preorder $preorder
     */
    public function create(Product $product): void
    {
        $this->logAttributes($product, ProductHistory::class, true, 'create', '', $product->uuid);
        $this->logAttributes($product->productCommon, ProductHistory::class, true, 'create_product_common', '', $product->uuid);
    }

    public function log(Product $product, $action = '', $comment = '')
    {
        $this->logAttributes($product, ProductHistory::class, $product->isNewRecord, $action, $comment, $product->uuid);
    }

    public function logProductCommon(ProductCommon $productCommon, $action = '')
    {
        $this->logAttributes($productCommon, ProductHistory::class, false, $action, '', $productCommon->product->uuid);
    }
}
