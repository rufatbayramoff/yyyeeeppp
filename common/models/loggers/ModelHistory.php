<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.08.17
 * Time: 16:05
 */

namespace common\models\loggers;

use common\interfaces\ModelHistoryInterface;
use common\traits\ModelHistoryARTrait;
use yii\base\Component;

class ModelHistory extends Component implements ModelHistoryInterface
{
    public $id;
    public $model_id;
    public $created_at;
    public $user_id;
    public $action_id;
    public $source;
    public $result;
    public $comment;

    public function getAttribute($name)
    {
        return $this->$name;
    }

    public function setAttribute($name, $value)
    {
        $this->$name = $value;
    }

    use ModelHistoryARTrait;
}