<?php

namespace common\models;

use common\components\ArrayHelper;
use common\modules\cutting\models\CutSize;
use common\modules\cutting\models\CuttingPartMaterialInfo;
use Yii;

/**
 * Class CuttingMachineProcessing
 * @package common\models
 */
class CuttingMachineProcessing extends \common\models\base\CuttingMachineProcessing
{
    /**
     * @var bool
     */
    public $forDelete;
    public const MIN_CUTTING_LEN = 0.0001;

    public const SCENARIO_FULL = 'full';

    public function attributeLabels()
    {
        return [
            'uuid'                => Yii::t('app', 'Uuid'),
            'cutting_machine_id'  => Yii::t('app', 'Cutting Machine'),
            'cutting_material_id' => Yii::t('app', 'Cutting Material'),
            'thickness'           => Yii::t('app', 'Thickness'),
            'cutting_price'       => Yii::t('app', 'Cutting Price'),
            'engraving_price'     => Yii::t('app', 'Engraving Price'),
        ];
    }

    public function rules():array
    {
        return ArrayHelper::merge(parent::rules(),[
            [['cutting_price', 'engraving_price'], 'required', 'on' => self::SCENARIO_FULL],
        ]);
    }

    public function getWorkpieceMaterialProcessingUuid()
    {
        return $this->cutting_material_id . '_' . $this->thickness;
    }

    /**
     * Get All possible workpiece materials
     *
     * @param CuttingPartMaterialInfo|null $cuttingPartMaterialInfo
     * @param CutSize $cutSize
     * @return CuttingWorkpieceMaterial[]
     */
    public function getWorkpieceMaterials(CutSize $cutSize, ?CuttingPartMaterialInfo $cuttingPartMaterialInfo = null): array
    {
        $workpieceMaterials = [];
        foreach ($this->cuttingMachine->companyService->company->cuttingWorkpieceMaterials as $workpieceMaterial) {
            if ($workpieceMaterial->thickness != $this->thickness) {
                continue;
            }
            if ($workpieceMaterial->material->id != $this->cuttingMaterial->id) {
                continue;
            }

            if ($cuttingPartMaterialInfo ) {
                if ($cuttingPartMaterialInfo->thickness && $cuttingPartMaterialInfo->thickness != $workpieceMaterial->thickness) {
                    continue;
                }

                if ($cuttingPartMaterialInfo->cuttingMaterial && $cuttingPartMaterialInfo->cuttingMaterial->id != $workpieceMaterial->material_id) {
                    continue;
                }

                if ($cuttingPartMaterialInfo->cuttingColor && $cuttingPartMaterialInfo->cuttingColor->id != $workpieceMaterial->color_id) {
                    continue;
                }
            }

            if ($workpieceMaterial->getWorkSize()->isMoreOrEqualThen($cutSize)) {
                $workpieceMaterials[] = $workpieceMaterial;
            }
        }
        return $workpieceMaterials;
    }

    public function getMinWorkpieceMaterial(CutSize $cutSize, ?CuttingPartMaterialInfo $cuttingPartMaterialInfo=null): ?CuttingWorkpieceMaterial
    {
        $minWorkpieceMaterial      = null;
        $minWorkpieceMaterialPrice = 99999999999999;
        foreach ($this->getWorkpieceMaterials($cutSize, $cuttingPartMaterialInfo) as $workpieceMaterial) {
            if ($workpieceMaterial->price < $minWorkpieceMaterialPrice) {
                $minWorkpieceMaterialPrice = $workpieceMaterial->price;
                $minWorkpieceMaterial      = $workpieceMaterial;
            }
        }
        return $minWorkpieceMaterial;
    }

    public function getCuttingPricePerCM()
    {
        return round($this->cutting_price*100, 2);
    }

    public function getCuttingPricePerM()
    {
        return round($this->cutting_price*1000, 2);
    }

    public function getEngravingPricePerM()
    {
        return round($this->engraving_price*1000, 2);
    }

    public function getEngravingPricePerCM()
    {
        return round($this->engraving_price*100, 2);
    }

    public function checkProcessing()
    {
        if (!$this->getCuttingPricePerM()) {
            return _t('site.cutting', 'Cutting price not setted. Service can`t be published.');
        }
        return null;
    }


}