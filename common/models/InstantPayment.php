<?php

namespace common\models;

use backend\models\user\UserAdmin;
use common\components\DateHelper;
use common\components\exceptions\BusinessException;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use yii\base\UserException;

/**
 * Class InstantPayment
 * @package common\models
 */
class InstantPayment extends \common\models\base\InstantPayment
{
    public const STATUS_NOT_PAYED        = 'not_payed';
    public const STATUS_WAIT_FOR_CONFIRM = 'wait_for_confirm';
    public const STATUS_DECLINED         = 'declined';
    public const STATUS_CANCELED         = 'canceled';
    public const STATUS_APPROVED         = 'approved';


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'descr' => Yii::t('app', 'Description'),
            'sum'   => Yii::t('app', 'Price'),
        ];
    }

    public function getAmount()
    {
        return Money::create($this->sum, $this->currency);
    }

    public function rules()
    {
        $rules = array_merge(parent::rules(), [
            [['sum'], 'number', 'min' => 4],
        ]);
        return $rules;
    }

    public function getAwardDescription()
    {
        return _t('site.order', 'Treatstock fee: {fee}', [
            'fee' => displayAsMoney($this->primaryInvoice->getAmountInstantPaymentFeeIn())
        ]);
    }

    public function getTextStatus()
    {
        $texts = [
            self::STATUS_NOT_PAYED        => _t('site.ps', 'Unpaid'),
            self::STATUS_WAIT_FOR_CONFIRM => _t('site.ps', 'Waiting for confirmation'),
            self::STATUS_DECLINED         => _t('site.ps', 'Canceled'),
            self::STATUS_APPROVED         => _t('site.ps', 'Completed'),
            self::STATUS_CANCELED         => _t('site.ps', 'Canceled'),

        ];
        return $texts[$this->status];
    }

    public function getCompanyStatusDescription()
    {
        $texts = [
            self::STATUS_NOT_PAYED        => _t('site.ps', 'Not paid yet'),
            self::STATUS_WAIT_FOR_CONFIRM => _t('site.ps', 'Please, accept instant payment'),
            self::STATUS_DECLINED         => _t('site.ps', 'Payment was declined'),
            self::STATUS_APPROVED         => _t('site.ps', 'The company received payment'),
            self::STATUS_CANCELED         => _t('site.ps', 'Payment was canceled'),

        ];
        return $texts[$this->status];
    }

    public function getClientStatusDescription()
    {
        $texts = [
            self::STATUS_NOT_PAYED        => _t('site.ps', 'Please, finish the payment process to proceed'),
            self::STATUS_WAIT_FOR_CONFIRM => _t('site.ps', 'The company is yet to accept your instant payment'),
            self::STATUS_DECLINED         => _t('site.ps', 'Your payment was declined'),
            self::STATUS_APPROVED         => _t('site.ps', 'The company received your payment'),
            self::STATUS_CANCELED         => _t('site.ps', 'Your payment was canceled'),

        ];
        return $texts[$this->status];
    }

    public function isNotPayed()
    {
        return $this->status === self::STATUS_NOT_PAYED;
    }

    public function isWaitingForConfirm()
    {
        return $this->status === self::STATUS_WAIT_FOR_CONFIRM;
    }

    public function allowCancel()
    {
        return $this->isNotPayed() || $this->isWaitingForConfirm();
    }

    public function isApproved()
    {
        return $this->status === self::STATUS_APPROVED;
    }

    public function isInternalOrder()
    {
        $internalUsers = app('setting')->get('store.internalOrdersCustomerUserId');
        if (in_array($this->from_user_id, $internalUsers)) {
            return true;
        }
        if (in_array($this->to_user_id, $internalUsers)) {
            return true;
        }
        return false;
    }

    public function checkCanBeApprovedByUserOrFail(?User $user)
    {
        if ((!$user) || (!$this->canBeApprovedByUser($user) && (!($user instanceof UserAdmin)))) {
            throw new BusinessException('Can`t be approved');
        }
    }

    public function canBeApprovedByUser(User $user)
    {
        return $this->toUser->id === $user->id;
    }
}