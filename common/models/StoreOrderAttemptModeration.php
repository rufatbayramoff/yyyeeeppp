<?php

namespace common\models;
use common\components\exceptions\AssertHelper;
use common\components\reject\RejectInterface;

/**
 * Class StoreOrderAttemptModeration
 * @package common\models
 * @property StoreOrderAttemptModerationFile[] $files
 */
class StoreOrderAttemptModeration extends \common\models\base\StoreOrderAttemptModeration
{
    const STATUS_NEW = 'new';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_REJECTED = 'rejected';

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getFiles()
    {
        return $this->getStoreOrderAttemptModerationFiles();
    }

    /**
     * Clear presvious moderation result
     */
    public function clearPreviousResult()
    {
        $this->status = self::STATUS_NEW;
        $this->reject_comment = null;
        $this->reject_reason_id = null;
        AssertHelper::assertSave($this);
    }

    /**
     *
     */
    public function acceptModeration()
    {
        $this->status = self::STATUS_ACCEPTED;
        AssertHelper::assertSave($this);
    }

    /**
     * @param string $comment
     */
    public function rejectModeration(string $comment)
    {
        $this->status = self::STATUS_REJECTED;
        $this->reject_comment = $comment;
        AssertHelper::assertSave($this);
    }

    /**
     * @return bool
     */
    public function isNew() : bool
    {
        return $this->status == self::STATUS_NEW;
    }


    /**
     * @return bool
     */
    public function isAccepted() : bool
    {
        return $this->status == self::STATUS_ACCEPTED;
    }


    /**
     * @return bool
     */
    public function isRejected() : bool
    {
        return $this->status == self::STATUS_REJECTED;
    }
}