<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.08.16
 * Time: 16:17
 */

namespace common\models\model3d;

use common\components\Model3dPartVolumeCalulator;
use common\components\Model3dWeightCalculator;
use common\components\ps\locator\Size;
use common\interfaces\Model3dBasePartInterface;
use common\models\factories\Model3dTextureFactory;
use common\models\File;
use common\models\Model3dPart;
use common\models\Model3dPartProperties;
use common\models\Model3dReplicaPart;
use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use console\jobs\model3d\ParserJob;
use yii\base\InvalidValueException;

/**
 * Class Model3dBasePart
 *
 * @property Model3dTexture $model3dTexture
 * @property File $file
 * @property Model3dPartProperties $model3dPartProperties
 * @mixin Model3dBasePartInterface
 */
trait Model3dBasePart
{
    public function isActive()
    {
        if (($this->user_status === Model3dBasePartInterface::STATUS_INACTIVE) || ($this->moderator_status === Model3dBasePartInterface::STATUS_BANNED)) {
            return false;
        }
        return true;
    }

    public function isParsed(): bool
    {
        if ($this->isCaclulatedProperties()) {
            return true;
        }
        return ParserJob::isFailed($this->file);
    }

    public function isCaclulatedProperties()
    {
        return $this->model3dPartProperties && $this->model3dPartProperties->area && $this->model3dPartProperties->volume;
    }

    public function getWeight($printerMaterial = null)
    {
        $weight = Model3dWeightCalculator::calculateModel3dPartWeight($this, $printerMaterial);
        return $weight;
    }

    public function getVolume()
    {
        return Model3dPartVolumeCalulator::calculateModel3dPartVolume($this);
    }

    public function getBoxVolume()
    {
        return $this->model3dPartProperties->width * $this->model3dPartProperties->height * $this->model3dPartProperties->length * $this->qty;
    }

    public function getTitle()
    {
        return $this->title ?? $this->file->getFileName();
    }

    public function getTitleWithoutExt()
    {
        return $this->title ?? $this->file->getFileNameWithoutExtension();
    }

    public function getIsConverted()
    {
        return $this->convertedParts ? 1 : 0;
    }

    /**
     * return size in mm.
     *
     * @return Size|null
     */
    public function getSize()
    {
        if ($this->model3dPartProperties) {
            return Size::create(
                $this->model3dPartProperties->width,
                $this->model3dPartProperties->height,
                $this->model3dPartProperties->length,
                $this->model3dPartProperties->metric
            );
        } else {
            return null;
        }
    }

    public function getOriginalSize()
    {
        $originalProperties = $this->model3dPartPropertiesOriginal ?: $this->getOriginalModel3dPartObj()->model3dPartPropertiesOriginal;
        if ($originalProperties) {
            return Size::create(
                $originalProperties->width,
                $originalProperties->height,
                $originalProperties->length,
                $originalProperties->metric
            );
        } else {
            return $this->getSize();
        }
    }

    public function setSize(Size $size)
    {
        if (!$this->model3dPartProperties) {
            $this->model3dPartProperties = new Model3dPartProperties();
        }
        $this->model3dPartProperties->width = $size->width;
        $this->model3dPartProperties->height = $size->height;
        $this->model3dPartProperties->length = $size->length;
        $this->model3dPartProperties->metric = $size->measure;
    }

    /**
     * @param PrinterMaterialGroup $materialGroup
     * @param PrinterColor $color
     */
    public function setMaterialGroupAndColor($materialGroup, $color)
    {
        $model3dTexture = $this->model3dTexture;
        if ($model3dTexture) {
            $model3dTexture->printerMaterialGroup = $materialGroup;
            $model3dTexture->printerColor = $color;
        } else {
            $model3dTexture = new Model3dTexture();
            $model3dTexture->printerMaterialGroup = $materialGroup;
            $model3dTexture->printerColor = $color;
            $this->populateRelation('model3dTexture', $model3dTexture);
        }
    }


    /**
     * @param PrinterMaterial $material
     * @param PrinterColor $color
     * @param null $infill
     */
    public function setMaterialAndColor(PrinterMaterial $material, PrinterColor $color,$infill = null)
    {
        $model3dTexture = $this->model3dTexture;
        if ($model3dTexture) {
            $model3dTexture->printerMaterialGroup = null;
            $model3dTexture->printerMaterial = $material;
            $model3dTexture->printerColor = $color;
            if($infill) {
                $model3dTexture->infill = $infill;
            }
        } else {
            $model3dTexture = new Model3dTexture();
            $model3dTexture->printerMaterial = $material;
            $model3dTexture->printerMaterialGroup = null;
            $model3dTexture->printerColor = $color;
            if($infill) {
                $model3dTexture->infill = $infill;
            }
            $this->populateRelation('model3dTexture', $model3dTexture);
        }
    }

    /**
     * Get calculated color:  if it is one color for kit, return model3d color. If it is not kit, return model3d part color.
     *
     * @return Model3dTexture
     * @throws \yii\base\ErrorException
     */
    public function getCalculatedTexture()
    {
        if ($this->getAttachedModel3d()->isOneTextureForKit()) {
            return $this->getAttachedModel3d()->getKitTexture();
        }
        return $this->getTexture();
    }

    public function getCalculatedPrintableTexture()
    {
        $texture = $this->getCalculatedTexture();
        if (($texture->printerColor->isMulticolor()) && ($this->getFormat() === Model3dBasePartInterface::STL_FORMAT)) {
            $newTexture = new Model3dTexture();
            $newTexture->printerColor = PrinterColor::findOne(['render_color' => 'White']);
            $newTexture->printerMaterial = $texture->printerMaterial;
            $texture = $newTexture;
        }
        return $texture;
    }


    public function getTexture()
    {
        $model3dTexture = $this->model3dTexture;
        if (!$model3dTexture) {
            // Return default texture. Model3d part always has texture
            $model3dTexture = Model3dTextureFactory::createModel3dTexture();
        }
        return $model3dTexture;
    }

    public function setTexture(Model3dTexture $texture)
    {
        $this->populateRelation('model3dTexture', $texture);
    }

    public function setModel3dPartProperties($model3dPartProperties)
    {
        $this->populateRelation('model3dPartProperties', $model3dPartProperties);
    }

    public function setModel3dPartCncParams($model3dPartCncParams)
    {
        $this->populateRelation('model3dPartCncParams', $model3dPartCncParams);
    }

    /**
     * @param File $file
     */
    public function setFile(File $file)
    {
        $this->populateRelation('file', $file);
    }

    public function beforeSaveMethod($insert)
    {
        if ($this->model3d && (($this->model3d_id !== $this->model3d->id) || ($this->model3d_id === null))) {
            $this->model3d_id = $this->model3d->id;
        }
        if ($this->model3dPartProperties && (($this->model3d_part_properties_id !== $this->model3dPartProperties->id) || ($this->model3d_part_properties_id === null))) {
            $this->model3d_part_properties_id = $this->model3dPartProperties->id;
        }
        if ($this->model3dTexture && (($this->model3d_texture_id !== $this->model3dTexture->id) || ($this->model3d_texture_id === null))) {
            $this->model3d_texture_id = $this->model3dTexture->id;
        }
        if ($this->file && (($this->file_id !== $this->file->id) || ($this->file_id === null))) {
            $this->file_id = $this->file->id;
        }
        return parent::beforeSaveMethod($insert);
    }

    public function afterDelete()
    {
        if ($this->model3dTexture) {
            $this->model3dTexture->delete();
        }
        if ($this->model3dPartProperties) {
            $this->model3dPartProperties->delete();
        }
    }

    public function setInactive()
    {
        $this->user_status = Model3dBasePartInterface::STATUS_INACTIVE;
        $this->resetScale();
    }

    public function setActive()
    {
        $this->user_status = Model3dBasePartInterface::STATUS_ACTIVE;
    }

    public function resetScale()
    {
        if ($this->model3dPartPropertiesOriginal) {
            $oldId = $this->model3dPartProperties->id;
            $this->model3dPartProperties->attributes =  $this->model3dPartPropertiesOriginal->attributes;
            $this->model3dPartProperties->id = $oldId;

        }
    }

    /**
     * Get current format ply, stl, etc...
     *
     * @throws \yii\base\InvalidValueException
     */
    public function getFormat()
    {
        $ext = mb_strtolower($this->file->getFileExtension());
        if (!in_array($ext, Model3dBasePartInterface::ALLOWED_FORMATS, true)) {
            throw new InvalidValueException('Model3dPart ' . $this->id . ' contains invalid file format "' . $ext . '"');
        }
        return $ext;
    }

    public function isMulticolorFormat()
    {
        if ($this->getFormat() === Model3dBasePartInterface::PLY_FORMAT) {
            return true;
        }
        return false;
    }

    public function getUid()
    {
        return self::UID_PREFIX . $this->id;
    }

    public function isScaled(): bool
    {
        if ($this->getSize() && !$this->getSize()->isEqual($this->getOriginalSize())) {
            return true;
        }
        return false;
    }

    public function getScale(): float
    {
        if ($this->isScaled()) {
            return $this->getSize()->width / $this->getOriginalSize()->width;
        }
        return 1;
    }

    public function isConverted(): bool
    {
        return $this->is_converted;
    }
}
