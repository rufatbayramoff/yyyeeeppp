<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.08.16
 * Time: 16:13
 */

namespace common\models\model3d;

use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\ModelException;
use common\components\ps\locator\Size;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\factories\Model3dTextureFactory;
use common\models\File;
use common\models\Model3d;
use common\models\model3d\events\DeleteEvent;
use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\models\SiteTag;
use common\modules\product\interfaces\ProductInterface;
use common\services\PrinterMaterialService;
use frontend\models\model3d\Model3dFacade;
use lib\MeasurementUtil;
use Yii;
use yii\base\Model;
use yii\base\UserException;
use yii\db\ActiveRecord;
use yii\db\Exception;


/**
 * Class Model3dBase
 *
 * @property Model3dTexture $kitModel3dTexture
 * @property Model3dBasePartInterface[] $model3dParts
 * @property SiteTag[] tags
 * @package common\models
 * @mixin ActiveRecord|Model|Model3d\
 */
trait Model3dBase
{
    public $hasPublishUpdates;
    public $isAnyTextureAllowed = false;


    /**
     * @return array
     */
    public function rules()
    {
        return
            [
                [
                    ['is_printer_ready'],
                    'boolean'
                ],
                [
                    ['description'],
                    'string',
                    'max' => 3000
                ],
                [
                    ['id'],
                    'validateModel3dPartExists',
                    'skipOnEmpty' => false,
                    'except'      => self::SCENARIO_EMPTY_MODEL
                ]
            ];
    }

    /**
     * @param string $title
     * @throws Exception
     */
    public function setTitle($title)
    {
        if (!$title) {
            return ;
        }

        $validUtf8 = mb_check_encoding($title, 'UTF-8');
        if (!$validUtf8) {
            throw new Exception('Not valid i18n title');
        }
        $title = iconv('UTF-8', 'UTF-8//IGNORE', $title);
        $valid = preg_match(
            '%^(?:
            [\x09\x0A\x0D\x20-\x7E]              # ASCII
            | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
            |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
            | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
            |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
            |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
            | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
            |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
            )*$%xs',
            $title
        );
        if (!$valid) {
            throw new Exception('Not valid title!');
        }
        $title = ucfirst(preg_replace('#[._-]#', ' ', $title));
        if ($this->hasAttribute('title')) {
            $this->title = mb_substr($title, 0, 45);
        } else {
            $this->productCommon->title = mb_substr($title, 0, 45);
        }
    }


    /**
     * can current model be added to collection
     *
     * @return bool
     */
    public function canAddToCollection()
    {
        return $this->isPublished();
    }

    /**
     * show or not social buttons
     *
     * @return bool
     */
    public function canShare()
    {
        return $this->canAddToCollection();
    }


    /**
     * @return bool
     */
    public function isInStore()
    {
        return $this->product_status === ProductInterface::STATUS_PUBLISHED_PUBLIC;
    }

    /**
     * @param File $file
     */
    public function setCoverFile(File $file)
    {
        // Check set cover, inside model
        $coverExists = true;
        foreach ($this->model3dParts as $model3dPart) {
            if ($model3dPart->file->id === $file->id) {
                $coverExists = true;
                break;
            }
        }
        foreach ($this->model3dImgs as $model3dImg) {
            if ($model3dImg->file->id === $file->id) {
                $coverExists = true;
                break;
            }
        }
        if (!$coverExists) {
            \Yii::warning('Invalid set cover file parameter. FileId: ' . $file->id . ' ModelId: ' . $this->id);
            throw new BusinessException('Invalid set cover file parameter.');
        }
        $this->populateRelation('coverFile', $file);
        $this->cover_file_id = $file->id;
    }

    public function zerroCoverFile()
    {
        $this->populateRelation('coverFile', null);
        $this->cover_file_id = null;
    }

    public function isEmpty(): bool
    {
        $hasFiles = (count($this->getActiveModel3dParts()) > 0) || (count($this->getActiveModel3dImages()) > 0);
        return !$hasFiles;
    }

    public function validateProductCategory()
    {
        if (!$this->productCategory) {
            $this->addError('', _t('site.model3d', 'Please, choose category for publishing model.'));
        }
    }

    /**
     * check kit rules
     *
     */
    public function validateKitRules()
    {
        if ($this->isKit() && !$this->hasImages()) {
            $this->addError('', _t('site.model3d', 'Please add a photo or image of your model fully assembled in either JPG or PNG format.'));
        }
    }

    /**
     *
     */
    public function validateModel3dPartExists()
    {
        if (!$this->getActiveModel3dParts()) {
            $this->addError('', _t('site.model3d', '3D model must contain an STL, PLY, OBJ or 3MF file.'));
        }
    }

    public function getSize()
    {
        if (!($model3dParts = $this->getActiveModel3dParts())) {
            return null;
        }
        $largestSize = null;

        foreach ($model3dParts as $model3dPart) {
            if (!$model3dPart->isCaclulatedProperties()) {
                continue;
            }
            if (!$model3dPart->qty) {
                continue;
            }
            if ($largestSize === null) {
                $largestSize = $model3dPart->getSize()->toArray();
                rsort($largestSize);
                continue;
            }
            $partSize    = $model3dPart->getSize();
            $partArrSize = $partSize->toArray();
            rsort($partArrSize);
            $largestSize = [max($largestSize[0], $partArrSize[0]), max($largestSize[1], $partArrSize[1]), max($largestSize[2], $partArrSize[2])];
        }
        return Size::create($largestSize[0], $largestSize[1], $largestSize[2]);
    }

    /**
     * get total weight of 3d model files in 3D Model (kit)
     * if one model3d part is 0, all weight is 0
     * Return weight in gram
     *
     * @return int
     */
    public function getWeight()
    {
        $res = 0;
        foreach ($this->getActiveModel3dParts() as $model3dPart) {
            if ($model3dPart->getWeight() === 0) {
                return 0;
            }
            $res += $model3dPart->getWeight();
        }
        return $res;
    }

    public function getVolume()
    {
        $res = 0;
        foreach ($this->getActiveModel3dParts() as $model3dPart) {
            $res += $model3dPart->getVolume();
        }
        return $res;
    }

    public function getBoxVolume()
    {
        $res = 0;
        foreach ($this->getActiveModel3dParts() as $model3dPart) {
            $res += $model3dPart->getBoxVolume();
        }
        return $res;
    }

    /**
     * @return Model3dBasePartInterface[]
     */
    public function getCalculatedModel3dParts()
    {
        $returnValue = [];
        foreach ($this->model3dParts as $model3dPart) {
            if ($model3dPart->isActive() && $model3dPart->isCaclulatedProperties()) {
                $returnValue[$model3dPart->file_id] = $model3dPart;
            }
        }
        return $returnValue;
    }


    /**
     * @return Model3dBasePartInterface[]
     */
    public function getActiveModel3dParts()
    {
        $returnValue = [];
        foreach ($this->model3dParts as $model3dPart) {
            if ($model3dPart->isActive()) {
                $returnValue[$model3dPart->file_id] = $model3dPart;
            }
        }
        return $returnValue;
    }

    /**
     * @return Model3dBasePartInterface[]
     */
    public function getCanceledParts()
    {
        $returnValue = [];
        foreach ($this->model3dParts as $model3dPart) {
            if (!$model3dPart->isActive()) {
                $returnValue[$model3dPart->getUid()] = $model3dPart;
            }
        }
        return $returnValue;
    }


    /**
     * @return Model3dBaseImgInterface[]
     */
    public function getActiveModel3dImages()
    {
        $returnValue = [];
        foreach ($this->model3dImgs as $model3dImg) {
            if ($model3dImg->isActive()) {
                $returnValue[$model3dImg->file_id] = $model3dImg;
            }
        }
        return $returnValue;
    }

    /**
     * Delete model from TS.
     * It only set statuses, not delete realy
     */
    public function deleteModel()
    {
        $this->productCommon->is_active = 0;
        $this->productCommon->updated_at = DateHelper::now();
        AssertHelper::assertSave($this->productCommon);
        $this->trigger(DeleteEvent::class, new DeleteEvent());
    }

    /**
     * Flag model consists of several files
     *
     * @return bool
     */
    public function isKit()
    {
        $files = $this->getActiveModel3dParts();
        return count($files) > 1;
    }

    /**
     * @return bool
     */
    public function hasImages()
    {
        $imgs = $this->getActiveModel3dImages();
        return count($imgs) > 0;
    }

    /**
     * @return bool
     */
    public function isOneTextureForKit()
    {
        if ($this->getKitTexture()) {
            return true;
        }
        return false;
    }

    public function isScaled(): bool
    {
        $model3dParts = $this->getActiveModel3dParts();
        foreach ($model3dParts as $model3dPart) {
            if ($model3dPart->isScaled()) {
                return true;
            }
        }
        return false;
    }

    public function getScale(): float
    {
        $model3dParts = $this->getActiveModel3dParts();
        $model3dPart  = reset($model3dParts);
        return $model3dPart->getScale();
    }

    /**
     * @return bool
     */
    public function isParsed(): bool
    {
        foreach ($this->getActiveModel3dParts() as $oneModel3dPart) {
            if (!$oneModel3dPart->isParsed()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    public function isCalculatedProperties()
    {
        foreach ($this->getActiveModel3dParts() as $oneModel3dPart) {
            if (!$oneModel3dPart->isCaclulatedProperties()) {
                return false;
            }
        }
        return true;
    }

    /**
     * All model parts has same colors
     */
    public function isSameTextureForAllParts()
    {
        $baseTexture = null;
        foreach ($this->getActiveModel3dParts() as $oneModel3dPart) {
            if ($baseTexture === null) {
                $baseTexture = $oneModel3dPart->getCalculatedTexture();
            } else {
                if (!PrinterMaterialService::isSameTexture($baseTexture, $oneModel3dPart->getCalculatedTexture())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * In print process, we can`t change model3d parts colors
     *
     * @return bool
     */
    public function isAnyTextureAllowed()
    {
        return $this->isAnyTextureAllowed;
    }

    public function setAnyTextureAllowed($mode)
    {
        $this->isAnyTextureAllowed = $mode;
    }

    /**
     * @return Model3dTexture|null
     */
    public function getKitModel3dTexture()
    {
        return $this->model3dTexture;
    }

    /**
     * @param Model3dTexture|null $model3dTexture
     * @return mixed
     */
    public function setKitModel3dTexture($model3dTexture)
    {
        return $this->populateRelation('model3dTexture', $model3dTexture);
    }

    /**
     * @param bool $withMarkDeleted
     * @return Model3dTexture
     */
    public function getKitTexture($withMarkDeleted = false)
    {
        $kitModel3dTexture = $this->kitModel3dTexture;
        if ($kitModel3dTexture) {
            if ($withMarkDeleted === false && $kitModel3dTexture->isMarkForDelete) {
                return null;
            }
            return $kitModel3dTexture;
        } else {
            return null;
        }
    }

    /**
     * @param Model3dTexture $kitTexture
     */
    public function setKitTexture(Model3dTexture $kitTexture)
    {
        $kitModel3dTexture = $this->kitModel3dTexture;
        if ($kitModel3dTexture) {
            $kitModel3dTexture->isMarkForDelete = false;
            $kitModel3dTexture->setTexture($kitTexture);
        } else {
            $kitModel3dTexture = Model3dTextureFactory::createModel3dTexture();
            $this->setKitModel3dTexture($kitModel3dTexture);
            $kitModel3dTexture->setTexture($kitTexture);
        }
    }

    /**
     * Установить материал и цвет модели
     *
     * @param $printerMaterial
     * @param $printerColor
     */
    public function setKitModelMaterialAndColor(PrinterMaterial $printerMaterial, PrinterColor $printerColor,$infill = null)
    {
        /** @var Model3dTexture $color */
        if ($this->isOneTextureForKit()) {
            $texture = $this->getKitTexture(true);
        } else {
            $texture = Model3dTextureFactory::createModel3dTexture();
            $this->setKitModel3dTexture($texture);
        }
        $texture->printerMaterial                 = $printerMaterial;
        $texture->printerColor                    = $printerColor;
        $this->kitModel3dTexture->isMarkForDelete = false;
        if($infill){
            $texture->infill = $infill;
        }
    }

    /**
     * @param PrinterMaterialGroup $printerMaterialGroup
     * @param PrinterColor $printerColor
     */
    public function setKitModelMaterialGroupAndColor(PrinterMaterialGroup $printerMaterialGroup, PrinterColor $printerColor)
    {
        /** @var Model3dTexture $color */
        if ($this->isOneTextureForKit()) {
            $texture = $this->getKitTexture(true);
        } else {
            $texture = Model3dTextureFactory::createModel3dTexture();
            $this->setKitModel3dTexture($texture);
        }
        $texture->printerMaterialGroup            = $printerMaterialGroup;
        $texture->printerColor                    = $printerColor;
        $this->kitModel3dTexture->isMarkForDelete = false;
    }

    /**
     * Get largest model3d part availible for printing ( QTY>0 ) and Parsed
     *
     * @return Model3dBasePartInterface|mixed|null
     */
    public function getLargestPrintingModel3dPart()
    {
        if (!($model3dParts = $this->getActiveModel3dParts())) {
            return null;
        }
        $largestPart = null;
        $largestSize = null;

        foreach ($model3dParts as $model3dPart) {
            if (!$model3dPart->isCaclulatedProperties()) {
                continue;
            }
            if (!$model3dPart->qty) {
                continue;
            }
            if ($largestPart === null) {
                $largestPart = $model3dPart;
                $largestSize = $model3dPart->getSize();
                continue;
            }
            $partSize = $model3dPart->getSize();
            if ($partSize && $partSize->isMoreThen($largestSize)) {
                $largestSize = $partSize;
                $largestPart = $model3dPart;
            }
        }
        return $largestPart;
    }


    /**
     * @return Model3dBasePartInterface|mixed|null
     */
    public function getLargestModel3dPart()
    {
        if (!($model3dParts = $this->getActiveModel3dParts())) {
            return null;
        }
        $largestPart = reset($model3dParts);
        $largestSize = $largestPart->getSize();
        foreach ($model3dParts as $model3dPart) {
            $partSize = $model3dPart->getSize();
            if ($partSize && (!$largestSize || $partSize->isMoreThen($largestSize))) {
                $largestSize = $partSize;
                $largestPart = $model3dPart;
            }
        }
        return $largestPart;
    }

    public function isMulticolorFormatModel()
    {
        foreach ($this->getActiveModel3dParts() as $model3dPart) {
            if ($model3dPart->getFormat() === Model3dBasePartInterface::PLY_FORMAT) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Model3dBasePartInterface[] $model3dParts
     */
    public function setModel3dParts($model3dParts)
    {
        $this->populateRelation('model3dParts', $model3dParts);
    }

    /**
     * @param Model3dBaseImgInterface[] $model3dImgs
     */
    public function setModel3dImgs($model3dImgs)
    {
        $this->populateRelation('model3dImgs', $model3dImgs);
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        $server = Yii::$app->params['server'];
        return $server . $this->getViewBaseUrl() . '/' . $this->id;
    }

    public function getPublicPageUrl(array $params = [], $scheme=false): string
    {
        return Model3dFacade::getStoreUrl($this, $params, $scheme);
    }

    /**
     * @throws ModelException
     */
    public function tryValidateInactive()
    {
        if (!$this->is_active) {
            throw new ModelException('Model was deleted.');
        }
    }

    /**
     * @param SiteTag[] $tags
     */
    public function setSiteTags($tags)
    {
        $this->populateRelation('tags', $tags);
    }

    public function getSiteTags()
    {
        return $this->hasMany(\common\models\SiteTag::class, ['id' => 'tag_id'])->viaTable('model3d_tag', ['model3d_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getCadFlag()
    {
        return $this->cae === Model3dBaseInterface::CAE_CAM ? 1 : 0;
    }

    public function getPrintCameraSize()
    {
        $maxSize = Size::create(0, 0, 0);
        $parts   = $this->getActiveModel3dParts();
        foreach ($parts as $model3dPart) {
            $partSize = $model3dPart->getSize();
            $partSize = $partSize->convertToMeasurement(MeasurementUtil::MM);
            if ($partSize->width > $maxSize->width) {
                $maxSize->width = $partSize->width;
            }
            if ($partSize->height > $maxSize->height) {
                $maxSize->height = $partSize->height;
            }
            if ($partSize->length > $maxSize->length) {
                $maxSize->length = $partSize->length;
            }
        }
        return $maxSize;
    }


    public function getUid()
    {
        return self::UID_PREFIX . $this->id;
    }

    public function isActive(): bool
    {
        return $this->is_active;
    }

    public function getModeratedAt(): string
    {
        return $this->getOriginalModel3d()->moderated_at;
    }

    public function getPublishedAt(): string
    {
        return $this->getOriginalModel3d()->published_at;
    }

    /**
     * @return string|null
     * @throws \Exception
     */
    public function getCoverUrl(): ?string
    {
        $coverInfo = Model3dFacade::getCover($this);
        return $coverInfo['image'];
    }

    /**
     * @return File[]
     */
    public function getImages(): array
    {
        $files = [];
        foreach ($this->getActiveModel3dImages() as $model3dImage) {
            $files[] = $model3dImage->file;
        }
        return $files;
    }

    public function getProductTags(): array
    {
        return $this->getSourceModel3d()->tags;
    }

    public function getProductType(): string
    {
        return ProductInterface::TYPE_MODEL3D;
    }
}