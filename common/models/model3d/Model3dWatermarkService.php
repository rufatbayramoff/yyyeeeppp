<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 05.04.2017
 * Time: 15:50
 */

namespace common\models\model3d;


use common\components\FileDirHelper;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\File;
use common\models\Model3d;
use common\models\StoreOrder;
use frontend\models\user\UserFacade;
use lib\d3\Model3dWatermark;
use yii\base\BaseObject;

class Model3dWatermarkService extends BaseObject
{
    /**
     * @var Model3dWatermark
     */
    private $api;

    /**
     * @var string
     */
    public $server;

    /**
     * @var string
     */
    public $password = null;


    const PASSPHRASE = 'info@treatstock.com';

    public function init()
    {
        parent::init();
        if (empty($this->password)) {
            $this->password = param('watermarkPassword', self::PASSPHRASE);
        }
        if (empty($this->server)) {
            $this->server = param('watermarkServer', 'http://192.168.66.10:5858');
        }
        $this->api = new Model3dWatermark($this->server, $this->password);

    }
    public function setPassword($psw)
    {
        $this->password = $psw;
        $this->api = new Model3dWatermark($this->server, $this->password);
    }
    /**
     * @param $path
     * @param $watermarkText
     * @return mixed
     * @throws \Exception
     * @throws \yii\base\ErrorException
     */
    public function encode($path, $watermarkText, $isDebugOutputOn=false)
    {
        $signedDir = \Yii::getAlias('@runtime') . '/watermark';
        FileDirHelper::createDir($signedDir);
        $signedFile = $signedDir.'/'.random_int(0,9999999).'_'.microtime(true);
        $this->api->isDebugOutputOn = $isDebugOutputOn;
        $result = $this->api->encode($path, $signedFile, $watermarkText);
        $apiResult = json_decode($result, true); // { result: ok }
        if (empty($apiResult['result']) || $apiResult['result'] != 'ok') {
            return false;
        }
        return $signedFile;
    }

    /**
     * @param $filePath
     * @return string
     */
    public function decode($filePath)
    {
        $result = $this->api->decodeAll($filePath);
        //$result = json_encode($result, true);
        return $result;
    }

    /**
     * generate watermark
     *
     * 'www.treatstock.com. Order 3544 27.03.2016. (c) model author.';
     *
     * @param StoreOrder $order
     * @param int $machineId
     * @return string
     */
    public function generateWatermark(StoreOrder $order, $machineId = 0)
    {
        //$author = UserFacade::getFormattedUserName($model3d->user, true);
        $watermark = sprintf('treatstock.com,%s,%s', $order->id, $machineId);
        return $watermark;
    }
}