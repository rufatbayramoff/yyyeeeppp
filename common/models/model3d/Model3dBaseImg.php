<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.08.16
 * Time: 18:29
 */

namespace common\models\model3d;

use common\components\exceptions\AssertHelper;
use common\components\ps\locator\Size;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\model3d\events\DeleteEvent;
use common\models\Model3dPart;
use common\models\Model3dReplicaPart;
use common\models\Model3dTexture;
use common\models\query\Model3dQuery;
use common\services\PrinterMaterialService;
use console\jobs\model3d\AntivirusJob;
use console\jobs\model3d\ParserJob;
use console\jobs\QueueGateway;
use frontend\models\model3d\Model3dFacade;
use frontend\models\user\UserFacade;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\sphinx\ActiveRecord;

/**
 * Class Model3dBaseImg
 *
 */
trait Model3dBaseImg
{

    public function getTitle()
    {
        return $this->title??$this->file->getFileName();
    }

    public function setAttchedModel3d(Model3dBaseInterface $model3d)
    {
        $this->populateRelation('model3d', $model3d);
        $this->model3d_id = $model3d->id;
    }


    public function beforeSaveMethod($insert)
    {
        if ($this->file && ($this->file_id !== $this->file->id || $this->file_id === null)) {
            $this->file_id = $this->file->id;
        }

        if ($this->model3d && ($this->model3d_id !== $this->model3d->id || $this->model3d_id === null)) {
            $this->model3d_id = $this->model3d->id;
        }

        return parent::beforeSaveMethod($insert);
    }

    public function isActive()
    {
        return $this->deleted_at === null;
    }

    public function getAttachedModel3d(): Model3dBaseInterface
    {
        if ($this instanceof  Model3dReplicaPart) {
            return $this->model3dReplica;
        }
        return $this->model3d;
    }

    public function getUid()
    {
        return self::UID_PREFIX.$this->id;
    }
}
