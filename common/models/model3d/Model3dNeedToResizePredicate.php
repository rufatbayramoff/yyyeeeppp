<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.05.17
 * Time: 10:39
 */

namespace common\models\model3d;

use common\components\ps\locator\Size;
use lib\MeasurementUtil;

class Model3dNeedToResizePredicate
{
    /**
     * @param Size[] $sizesMm
     * @return string
     */
    public function isNeedResizeTo(array $sizesMm)
    {
        $smallCount = 0;
        $bigCount = 0;
        $unknownCount = 0;
        foreach ($sizesMm as $sizeMm) {
            // one of the size is smaller then 2
            if ($sizeMm->height < 0.2 || $sizeMm->length < 0.2 || $sizeMm->width < 0.2) {
                $smallCount++;
            }
            if ($sizeMm->height < 2 && $sizeMm->length < 2 && $sizeMm->width < 2) {
                $smallCount++;
            }
            if ($sizeMm->height < 2 || $sizeMm->length < 2 || $sizeMm->width < 2) {
                $unknownCount++;
            }
            if ($sizeMm->height > 15 || $sizeMm->length > 15 || $sizeMm->width > 15) {
                $bigCount++;
            }
            $sizes = [$sizeMm->height, $sizeMm->length, $sizeMm->width];
            $unknownSize = 0;
            foreach ($sizes as $size) {
                if ($size > 2 && $size < 15) {
                    $unknownSize++;
                }
            }
            if ($unknownSize === 3) {
                $unknownCount++;
                continue;
            }
        }
        if ($smallCount > 0 && $bigCount > 0) {
            $unknownCount++;
        }
        if ($bigCount > 0) {
            return MeasurementUtil::MM;
        }
        if ($unknownCount > 0 || $bigCount > 0) {
            return MeasurementUtil::UNKOWN;
        }
        if ($smallCount > 0) {
            return MeasurementUtil::IN;
        }
        return MeasurementUtil::MM;
    }
}