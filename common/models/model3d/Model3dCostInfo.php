<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.11.16
 * Time: 12:07
 */

namespace common\models\model3d;


use common\models\PrinterColor;
use common\models\PrinterMaterialGroup;
use common\models\PsPrinter;

class Model3dCostInfo
{
    /**
     * @var PrinterMaterialGroup
     */
    public $printerMaterialGroup;

    /**
     * @var PrinterColor
     */
    public $printerColor;

    /**
     * @var PsPrinter
     */
    public $psPrinter;

    /**
     * @var float
     */
    public $price;

    public $estimateDeliveryCost;
}