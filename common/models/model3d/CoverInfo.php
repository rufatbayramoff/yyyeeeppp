<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\models\model3d;


use yii\base\Model;

/**
 * Model cover info
 * @todo \frontend\models\model3d\Model3dFacade::getCover должна возвращать этот объект, а не массив
 * @package common\models\model3d
 */
class CoverInfo extends Model
{
    /**
     * When cover is rendered model
     */
    const TYPE_RENDER = 'render';

    /**
     * When cover is image
     */
    const TYPE_IMAGE = 'image';

    /**
     * Url to image
     * @var string
     */
    public $image;

    /**
     * Type of cover: one of const TYPE_
     * @var string
     */
    public $type;

    /**
     * I dont now what is - it not used
     * @todo resolve
     * @var string
     */
    public $model3d;

    /**
     * File id of cover
     * @var int
     */
    public $file_id;

    /**
     * Retrun true if cover is render of model
     * @return bool
     */
    public function isModel()
    {
       return $this->type == self::TYPE_RENDER;
    }
}