<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.04.17
 * Time: 10:17
 */

namespace common\models\model3d;

use common\components\Model3dPartVolumeCalulator;
use common\components\Model3dWeightCalculator;
use common\components\ps\locator\Size;
use common\components\StubAr;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\File;
use common\models\Model3dPartCncParams;
use common\models\Model3dPartProperties;
use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use yii\base\Component;

class StubModel3dPart extends Component implements Model3dBasePartInterface
{
    use StubAr;

    public $id;

    /**
     * @var StubModel3d
     */
    public $model3d;
    public $size;
    public $materialGroup;
    public $color;
    public $qty = 1;

    /** @var  Model3dPartProperties */
    public $model3dPartProperties;

    /** @var  Model3dPartCncParams */
    public $model3dPartCncParams;

    public $volume;

    public function getWeight($printerMaterial = null)
    {
        $weight = Model3dWeightCalculator::calculateModel3dPartWeight($this, $printerMaterial);
        return $weight;
    }

    public function getVolume()
    {
        if (empty($this->volume)) {
            $this->volume = Model3dPartVolumeCalulator::calculateModel3dPartVolume($this);
        }
        return $this->volume;
    }


    /**
     * @return bool
     */
    public function isActive()
    {
        return true;
    }

    /**
     * @return Size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Stub method return 1
     *
     * @return float|null
     */
    public function getBoxVolume()
    {
        return 1;
    }

    /**
     * @param PrinterMaterialGroup $materialGroup
     * @param PrinterColor $color
     */
    public function setMaterialGroupAndColor($materialGroup, $color)
    {
        $this->materialGroup = $materialGroup;
        $this->color = $color;
    }

    /**
     * Get calculated color:  if it is one color for kit, return model3d color. If it is not kit, return model3d part color.
     *
     * @return Model3dTexture
     */
    public function getCalculatedTexture()
    {
        return $this->model3d->getKitTexture();
    }

    /**
     * Previous function with verifications:  1. stl with multicolor
     * Model3dpart printable color
     *
     * @return Model3dTexture
     */
    public function getCalculatedPrintableTexture()
    {
        return $this->model3d->getKitTexture();
    }

    /**
     * @return Model3dTexture
     */
    public function getTexture()
    {
        return $this->model3d->getKitTexture();
    }

    /**
     * @param Model3dTexture $texture
     */
    public function setTexture(Model3dTexture $texture)
    {
        $this->model3d->setKitTexture($texture);
    }

    /**
     * @param Model3dPartProperties $model3dPartProperties
     * @return mixed
     */
    public function setModel3dPartProperties($model3dPartProperties)
    {
        $this->model3dPartProperties = $model3dPartProperties;
    }

    public function setModel3dPartCncParams($model3dPartCncParams)
    {
        $this->model3dPartCncParams = $model3dPartCncParams;
    }

    /**
     * Get model3dpart format
     *
     * @return mixed
     */
    public function getFormat()
    {
        return 'stl';
    }

    /**
     * @param File $file
     * @return mixed
     */
    public function setFile(File $file)
    {
    }


    /**
     * @return bool
     */
    public function isMulticolorFormat()
    {
        return false;
    }

    /**
     * @param PrinterMaterial $material
     * @param PrinterColor $color
     */
    public function setMaterialAndColor(PrinterMaterial $material, PrinterColor $color)
    {
        // TODO: Implement setMaterialAndColor() method.
    }

    public function setAttachedModel3d(Model3dBaseInterface $model3d): void
    {
        $this->model3d = $model3d;
    }

    public function getAttachedModel3d()
    {
        return $this->model3d;
    }

    public function getOriginalModel3dPartObj()
    {
        return $this;
    }

    /**
     * @return int
     */
    public function getOriginalQty(): int
    {
        return $this->qty;
    }

    /**
     * @return bool
     */
    public function isCaclulatedProperties()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isParsed(): bool
    {
        return true;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return '';
    }

    /**
     * Set model3dpart inactive user status
     *
     * @return
     */
    public function setInactive()
    {
    }

    /**
     * Set model3dpart active user status
     *
     * @return
     */
    public function setActive()
    {
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return 0;
    }

    /**
     * @return Size
     */
    public function getOriginalSize()
    {
        return $this->size;
    }


    /**
     * @return string
     */
    public function getTitleWithoutExt()
    {
        return 'stub';
    }

    /**
     * Is converted to printable parts
     *
     * @return bool
     */
    public function isConverted(): bool
    {
        return false;
    }

    public function resetScale()
    {
    }

    public function isScaled(): bool
    {
        return false;
    }

    public function getScale(): float
    {
        return 1;
    }
}