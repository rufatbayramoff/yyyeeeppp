<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.04.17
 * Time: 10:17
 */

namespace common\models\model3d;

use common\components\DateHelper;
use common\components\StubAr;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\File;
use common\models\Model3dTexture;
use common\models\SiteTag;
use common\models\User;
use common\modules\product\interfaces\ProductInterface;
use frontend\models\user\UserFacade;
use lib\money\Money;
use yii\base\Component;

class StubModel3d extends Component implements Model3dBaseInterface
{
    use StubAr;

    public $id;
    public $user_id;
    public $model3dParts;
    public $kitTexture;
    public $source = Model3dBaseInterface::SOURCE_WEBSITE;
    public $isAnyTextureAllowed = true;
    public $storeUnit = null;
    public $product_status;


    public function getSize()
    {
        return $this->getLargestModel3dPart()->getSize();
    }

    /**
     * get total weight of 3d model files in 3D Model (kit)
     * if one model3d part is 0, all weight is 0
     * Return weight in gram
     *
     * @return int
     */
    public function getWeight()
    {
        $res = 0;
        foreach ($this->getActiveModel3dParts() as $model3dPart) {
            if ($model3dPart->getWeight() === 0) {
                return 0;
            }
            $res += $model3dPart->getWeight();
        }
        return $res;
    }

    public function getVolume()
    {
        $res = 0;
        foreach ($this->getActiveModel3dParts() as $model3dPart) {
            if ($model3dPart->getVolume() === 0) {
                return 0;
            }
            $res += $model3dPart->getVolume() * $model3dPart->qty;
        }
        return $res;
    }

    /**
     * Use stub always 1
     * @return float
     */
    public function getBoxVolume()
    {
        return 1;
    }


    /**
     * @param Model3dBasePartInterface $model3dParts
     */
    public function setModel3dParts($model3dParts)
    {
        $this->model3dParts = $model3dParts;
    }


    public function getViewUrl()
    {
        return null;
    }

    /**
     * @return Model3dBasePartInterface|null
     */
    public function getLargestModel3dPart()
    {
        return reset($this->model3dParts);
    }


    /**
     * @return bool
     */
    public function isAnyTextureAllowed()
    {
        return $this->isAnyTextureAllowed;
    }


    /**
     * @return bool
     */
    public function isOneTextureForKit()
    {
        return true;
    }

    /**
     * @param Model3dTexture $kitTexture
     */
    public function setKitTexture(Model3dTexture $kitTexture)
    {
        $this->kitTexture = $kitTexture;
    }

    /**
     * @return Model3dTexture
     */
    public function getKitTexture()
    {
        return $this->kitTexture;
    }

    /**
     * @return Model3dBasePartInterface[]
     */
    public function getActiveModel3dParts()
    {
        return $this->model3dParts;
    }


    /**
     * @param SiteTag[] $tags
     */
    public function setSiteTags($tags)
    {

    }

    /**
     * @return bool
     */
    public function isMulticolorFormatModel()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isSameTextureForAllParts()
    {
        return true;
    }

    /**
     * @return mixed
     */
    public function isCalculatedProperties()
    {
        return true;
    }


    /**
     * @return Model3dBaseImgInterface[]
     */
    public function getActiveModel3dImages()
    {
        return [];
    }

    /**
     * @return int
     */
    public function getCadFlag()
    {
        return true;
    }

    /**
     * @return mixed
     */
    public function isPublished(): bool
    {
        return $this->product_status === ProductInterface::STATUS_PUBLISHED_PUBLIC;
    }

    /**
     * @return mixed
     */
    public function setAnyTextureAllowed($isAnyTextureAllowed)
    {
        $this->isAnyTextureAllowed = $isAnyTextureAllowed;
    }

    /**
     * @return Model3dBaseInterface
     */
    public function getSourceModel3d()
    {
        // TODO: Implement getSourceModel3d() method.
        return $this;
    }

    /**
     * @return bool
     */
    public function isKit()
    {
        return count($this->model3dParts) > 1;
    }

    /**
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function getAuthor(): ?User
    {
        return UserFacade::getCurrentUser();
    }


    /**
     * @return Model3dBasePartInterface|null
     */
    public function getLargestPrintingModel3dPart()
    {
        return $this->getLargestModel3dPart();
    }

    /**
     * @return Model3dBasePartInterface[]
     */
    public function getCalculatedModel3dParts()
    {
        return $this->getActiveModel3dParts();
    }

    /**
     * @return bool
     */
    public function isParsed(): bool
    {
        return true;
    }

    /**
     * @param Model3dBaseImgInterface[] $model3dImgs
     */
    public function setModel3dImgs($model3dImgs)
    {
    }

    /**
     * @param File $file
     */
    public function setCoverFile(File $file)
    {
    }

    /**
     * @return mixed
     */
    public function zerroCoverFile()
    {
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return 0;
    }

    public function getPublishedAt(): string
    {
        return (string)DateHelper::subNowSec(60);
    }

    public function isActive(): bool
    {
        return true;

    }

    /**
     * @return Model3dBasePartInterface[]
     */
    public function getCanceledParts()
    {
        return [];
    }

    public function getUpdatedAt(): string
    {
        return (string)DateHelper::subNowSec(60);
    }

    public function getModeratedAt(): string
    {
        return (string)DateHelper::subNowSec(60);
    }

    public function getProductStatus(): string
    {
        return ProductInterface::STATUS_DRAFT;
    }

    /**
     * Get human readable status
     *
     * @return string
     */
    public function getProductStatusLabel(): string
    {
        return [];
    }

    /**
     * Disable publis_updated allow status
     */
    public function setCantBePublisehedUpdated(): void
    {
        // TODO: Implement setCantBePublisehedUpdated() method.
    }

    /**
     * Analyze product changed attibutes, for allow save product as published_updated
     *
     * @return bool
     */
    public function isCanBePublishUpdated(): bool
    {
        return false;
    }

    public function isAvailableInCatalog(): bool
    {
        return false;
    }

    public function getCoverUrl(): string
    {
        return '';
    }

    /**
     * @return File[]
     */
    public function getImages(): array
    {
        return [];
    }

    public function getTitle(): string
    {
        return 'Stub';
    }

    public function getDescription(): string
    {
        return 'Stub';
    }

    /**
     * @return SiteTag[]
     */
    public function getProductTags(): array
    {
        return [];
    }

    public function getProductType(): string
    {
        return ProductInterface::TYPE_MODEL3D;
    }

    public function getUuid(): string
    {
        return 'uuid_stub';
    }

    public function getPublicPageUrl(array $params = []): string
    {
        return 'stub';
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return !(count($this->model3dParts) > 0);
    }

    public function getPriceMoneyByQty($qty): ?Money
    {
        return null;
    }
}
