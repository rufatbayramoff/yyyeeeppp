<?php
/**
 * Created by mitaichik
 */

namespace common\models\model3d;
use yii\base\Model;

/**
 * Class PrintInstruction
 * @package common\models\model3d
 */
class PrintInstruction extends Model
{
    /**
     * @var string|null
     */
    public $rafts;

    /**
     * @var string|null
     */
    public $supports;

    /**
     * @var string|null
     */
    public $resolution;

    /**
     * @var string|null
     */
    public $infill;

    /**
     * @var string|null
     */
    public $printInstruction;

    /**
     * @var string|null
     */
    public $postPrintInstruction;

    /**
     * @var string|null
     */
    public $commonInstruction;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rafts' => _t('site.ps', 'Rafts'),
            'supports' => _t('site.ps', 'Supports'),
            'resolution' => _t('site.ps', 'Resolution'),
            'infill' => _t('site.ps', 'Infill'),
            'printInstruction' => _t('site.ps', 'Print Instruction'),
            'postPrintInstruction' => _t('site.ps', 'Post-Print Instruction'),
            'commonInstruction' => _t('site.ps', 'Common Instruction'),
        ];
    }
}