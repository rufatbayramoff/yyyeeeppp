<?php
/**
 * Date: 15.09.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\models\model3d;


use common\components\operations\Operation;
use common\interfaces\Model3dBaseInterface;
use common\models\factories\FileFactory;
use common\models\factories\Model3dPartPropertiesFactory;
use common\models\File;
use common\models\Model3d;
use lib\render\RenderException;
use lib\render\RenderLocalScale;
use Yii;

/**
 * Class ScaleOperation
 *
 * Used to scale model3d with ONE Part.
 *
 * @package common\models\model3d
 */
class ScaleKitOperation extends Operation
{

    /**
     * @var Model3dBaseInterface
     */
    private $model3d;

    /**
     * tool to scale
     * @var RenderLocalScale
     */
    private $renderer;

    /**
     * scale factor
     * @var float
     */
    private $scale;

    /**
     * scaled file
     * @var File[]
     */
    private $scaledFiles;

    /**
     * @var array
     */
    private $scaleResults;

    const MM_TO_INCHES_SCALE = 25.4; // if model in inches, multiply by this to get model in mm.

    const INCH_TO_MM_SCALE = 0.0393701;
    /**
     * ScaleOperation constructor.
     *
     * @param Model3dBaseInterface $model3d
     * @param $scale
     */
    public function __construct(Model3dBaseInterface $model3d, $scale)
    {
        $this->renderer = new RenderLocalScale();
        $this->model3d = $model3d;
        $this->scale = $scale;
    }

    /**
     *  check errors
     *  for now we support only one model part scale
     */
    public function checkErrors()
    {
        $modelPart = $this->model3d->getLargestModel3dPart();
        if ($modelPart === null) {
            $this->addError(_t('front.store', 'No 3D model part has been found to scale'));
        }
    }

    /**
     * Do operation
     *
     * @throws \yii\base\InvalidParamException
     * @throws \lib\render\RenderException
     * @throws \yii\base\InvalidConfigException
     */
    protected function doOperation()
    {
        $modelReplicaParts = $this->model3d->getModel3dParts()->all();

        $modelPartsScaled = [];
        foreach($modelReplicaParts as $modelPart){
            $fileInPath = $modelPart->file->getLocalTmpFilePath();
            /** @var FileFactory $fileFactory */
            $fileFactory = \Yii::createObject(FileFactory::class);
            $fileOut = $fileFactory->createFileFromCopy($modelPart->file);
            $fileOutPath =  $fileOut->getLocalTmpFilePath();
            try {
                $scaledResult = $this->renderer->parseJsonResult($this->renderer->scale($fileInPath, $fileOutPath, $this->scale));
                $model3dPartProperties = Model3dPartPropertiesFactory::createModel3dPartProperties($scaledResult);
                $modelPart->setFile($fileOut);
                $modelPart->setModel3dPartProperties($model3dPartProperties);
                $modelPartsScaled[] = $modelPart;
            } catch (RenderException $r) {
                \Yii::$app->log(VarDumper::dumpAsString($r), 'tsdebug');
                $this->addError(_t('front.store', $r->getMessage()));
            }
        }
        $this->model3d->setModel3dParts($modelPartsScaled);
    }

    /**
     * model is not saved. should be saved with parts later
     *
     * @return Model3dBaseInterface
     */
    public function getScaledModel()
    {
        return $this->model3d;
    }
    /**
     * get file for model3d part replica
     *
     * @return File
     */
    public function getScaledFiles()
    {
        return $this->scaledFiles;
    }

    /**
     * required to create model3d part properties
     *
     * @return mixed
     */
    public function getScaleResult()
    {
        return $this->scaleResults;
    }

}