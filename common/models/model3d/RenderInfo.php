<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\models\model3d;


use common\traits\PropertyArrayAccessTrait;
use yii\base\BaseObject;

/**
 * Class RenderInfo
 * @package common\models\model3d
 */
class RenderInfo extends BaseObject implements \ArrayAccess
{
    use PropertyArrayAccessTrait;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $fileExt;

    /**
     * Url to file
     *
     * @var string
     */
    public $url;

    /**
     * @var integer
     */
    public $model3dId;


    /**
     * Flag is this replica model
     *
     * @var bool
     */
    public $isReplicaModel;


    /**
     * Model3dPart id
     *
     * @var
     */
    public $model3dPartId;

    /**
     * Url to model stl
     *
     * @todo it's realy url?
     * @var
     */
    public $model3dUrl;

    /**
     * File id
     *
     * @var int
     */
    public $fileId;


    /**
     * @var int
     */
    public $printerColorId;


    /**
     * @var string
     */
    public $printerColorRgb;

    /**
     * @var int
     */
    public $printerMaterialId;

    /**
     * @var int
     */
    public $printerMaterialGroupId;

    /**
     * @var string
     */
    public $caption;

    /**
     * @var string
     */
    public $md5sum;

    /**
     * @var string
     */
    public $renderUrlBase;

    /**
     * @var bool
     */
    public $isParsed;
}
