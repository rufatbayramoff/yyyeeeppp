<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.05.17
 * Time: 10:41
 */

namespace common\models\model3d;

use common\components\operations\Operation;
use common\models\factories\Model3dPartPropertiesFactory;
use common\models\Model3dPartProperties;
use Yii;

class ScalePartPropertiesOperation extends Operation
{
    /**
     * scale factor
     *
     * @var float
     */
    protected $scale;

    /**
     * @var Model3dPartProperties
     */
    protected $partProperties;

    /**
     * @var Model3dPartProperties
     */
    protected $newProperties;

    /**
     * ScaleOperation constructor.
     *
     * @param $scale
     */
    public function __construct($scale)
    {
        $this->scale = $scale;
    }

    public function setModel3dPartProperties(Model3dPartProperties $partProperties)
    {
        $this->partProperties = $partProperties;
    }

    /**
     * Do operation
     */
    protected function doOperation()
    {
        /** @var Model3dPartProperties $newProperties */
        $newProperties = Model3dPartPropertiesFactory::cloneModel3dPartProperties($this->partProperties);

        $newProperties->width = $newProperties->width * $this->scale;
        $newProperties->length = $newProperties->length * $this->scale;
        $newProperties->height = $newProperties->height * $this->scale;
        $newProperties->volume = $newProperties->volume * pow($this->scale, 3);
        $newProperties->supports_volume = $newProperties->supports_volume * pow($this->scale, 3);
        $newProperties->area = $newProperties->area * pow($this->scale, 2);
        $this->newProperties = $newProperties;
    }

    public function getResult()
    {
        return $this->newProperties;
    }
}