<?php
/**
 * Created by mitaichik
 */

namespace common\models\model3d;
use yii\helpers\Json;

/**
 * Class PrintInstructionParser
 * @package common\models\model3d
 */
class PrintInstructionParser
{
    const THINGVERSE_PATTERN = '/Type: (.+)\nData: (.+)/';

    /**
     * PrintInstructionParser constructor.
     */
    private function __construct()
    {
    }

    /**
     * Singleton for parser.
     *
     * @return PrintInstructionParser
     */
    public static function instance()
    {
        static $instance;

        if ($instance === null) {
            $instance = new PrintInstructionParser();
        }

        return $instance;
    }

    /**
     * Parse string to PrintInstruction.
     *
     * @param null|string $str
     * @return PrintInstruction|null Return null if print instruction is empty.
     */
    public function parse(?string $str) : ?PrintInstruction
    {
        $str = trim($str);

        if (!$str) {
            return null;
        }

        return $this->isThingverse($str)
            ? $this->parseThingverse($str)
            : $this->parseSimple($str);
    }

    /**
     * Check that text is thingverse instruction.
     *
     * @param string $str
     * @return bool
     */
    private function isThingverse(string $str) : bool
    {
        return preg_match(self::THINGVERSE_PATTERN, $str);
    }

    /**
     * Parse simple instruction (plain text).
     *
     * @param string $str
     * @return PrintInstruction
     */
    private function parseSimple(string $str) : PrintInstruction
    {
        $instruction = new PrintInstruction();
        $instruction->commonInstruction = $str;
        return $instruction;
    }

    /**
     * Parse thingverse instruction.
     *
     * @param string $str
     * @return PrintInstruction|null
     */
    private function parseThingverse(string $str) : ?PrintInstruction
    {
        $instruction = new PrintInstruction();

        preg_match_all(self::THINGVERSE_PATTERN, $str, $matches, PREG_SET_ORDER);

        /** @var string[][] $data */
        $data = [];

        foreach ($matches as $item) {

            list(, $section, $json) = $item;

            $itemData = Json::decode($json);

            foreach ($itemData as $itemObject) {
                foreach ($itemObject as $key => $value) {
                    $data[$section][$key] = $value;
                }
            }
        }

        $instruction->rafts = $data['Print Settings']['rafts'] ?? null;
        $instruction->supports = $data['Print Settings']['supports'] ?? null;
        $instruction->resolution = $data['Print Settings']['resolution'] ?? null;
        $instruction->infill = $data['Print Settings']['infill'] ?? null;
        $instruction->printInstruction = $data['Print Settings']['notes'] ?? null;

        $instruction->postPrintInstruction = $data['Post-Printing']['content'] ?? null;
        $instruction->commonInstruction = $data['Instructions']['content'] ?? null;

        $hasData = false;
        foreach ($instruction->attributes as $attr) {
            $hasData = $hasData || $attr;
        }

        if (!$hasData) {
            return null;
        }

        return $instruction;
    }
}