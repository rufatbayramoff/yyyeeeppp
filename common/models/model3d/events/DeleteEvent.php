<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\models\model3d\events;


use common\models\Model3d;
use yii\base\Event;

/**
 * Event on delete model
 * @package common\models\model3d\events
 */
class DeleteEvent extends Event
{
    /**
     * @return Model3d
     */
    public function getModel3d()
    {
       return $this->sender;
    }
}