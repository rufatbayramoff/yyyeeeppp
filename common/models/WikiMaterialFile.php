<?php

namespace common\models;

/**
 * Class WikiMaterialFile
 *
 * @property \common\models\FileAdmin $file
 * @package common\models
 */
class WikiMaterialFile extends \common\models\base\WikiMaterialFile
{
    CONST ALLOW_EXTENSIONS = ['pdf', 'doc', 'docx', 'txt', 'odt', 'xls', 'xlsx', 'csv'];
    public $forDelete = false;

    public function setFile(FileAdmin $file)
    {
        $this->populateRelation('file', $file);
        $this->file_uuid = $file->uuid;
    }
}