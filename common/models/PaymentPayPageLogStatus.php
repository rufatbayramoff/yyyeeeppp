<?php

namespace common\models;

use common\components\DateHelper;
use yii\base\BaseObject;

/**
 * Class PaymentPayPageLogStatus
 * @package common\models
 */
class PaymentPayPageLogStatus extends \common\models\base\PaymentPayPageLogStatus
{
    public static function logStatus($logUuid, $vendor, $status)
    {
        $payPageLogStatus              = new PaymentPayPageLogStatus();
        $payPageLogStatus->uuid        = $logUuid;
        $payPageLogStatus->status_date = DateHelper::now();
        $payPageLogStatus->status      = $status;
        $payPageLogStatus->vendor      = $vendor;

        $payPageLogStatus->safeSave();
    }
}