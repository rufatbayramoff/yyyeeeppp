<?php

namespace common\models;

use common\components\ArrayHelper;
use common\components\ps\locator\materials\MaterialColorsItem;
use Yii;

/**
 * Material groups
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class PrinterMaterialGroup extends \common\models\base\PrinterMaterialGroup
{
    public static $identityMap = [];
    public const INFILL_STEP = 5;

    public function rules()
    {
        $parentRules = parent::rules();
        return array_merge(
            $parentRules,
            [
                ['wall_thickness', 'compare', 'compareValue' => 100000, 'operator' => '<=', 'type' => 'number'],
                ['fill_percent', 'compare', 'compareValue' => 100, 'operator' => '<=', 'type' => 'number'],
                ['id', 'checkInModelsUse']

            ]
        );
    }

    public function checkInModelsUse($attribute)
    {
        if (!$this->is_active) {
            $modelsList = $this->getModel3dsList();
            if ($modelsList) {
                $list = $this->formModel3dUrlList($modelsList);
                $this->addError('id', 'Group used in models: ' . $list);
            }
        }
    }

    /**
     * @param Model3d[] $list
     * @return string
     */
    public function formModel3dUrlList($list)
    {
        $returnValue = '[';
        foreach ($list as $element) {
            $returnValue .= $element->id . ', ';
        }
        $returnValue = substr($returnValue, 0, -2);
        $returnValue .= ']';
        return $returnValue;
    }

    public function getModel3dsList()
    {
        $model3ds = [];
        foreach ($this->model3dTextures as $texture) {
            if ($texture->model3d) {
                $model3ds[$texture->model3d->id] = $texture->model3d;
            }
            if ($texture->model3dPart && $texture->model3dPart->model3d) {
                $model3ds[$texture->model3dPart->model3d->id] = $texture->model3dPart->model3d;
            }
        }
        return $model3ds;
    }

    public function getInfillArray()
    {
        if (!$this->fill_percent_max) {
            return null;
        }
        $list = [];
        for ($i = $this->fill_percent; $i <= $this->fill_percent_max; $i += self::INFILL_STEP) {
            $list[] = $i;
        }
        return [
            'min'  => $this->fill_percent,
            'max'  => $this->fill_percent_max,
            'list' => $list
        ];
    }


    /**
     * @param $file File
     */
    public function setPhotoFile($file)
    {
        $this->populateRelation('photoFile', $file);
    }

    public function beforeSaveMethod($insert)
    {
        // Сохраняем id установленных связных объектов
        // TODO: рассмотреть возможность замены на уже готовый от сторонней библиотеки бехейвора
        $related = $this->getRelatedRecords();
        if (array_key_exists('photoFile', $related) && $this->photoFile) {
            $this->photo_file_uuid = $this->photoFile->uuid;
        }
        return parent::beforeSaveMethod($insert);
    }

    public function getDefaultPhotoFileUrl()
    {
        // TODO: вынести в конфиг
        return '/static/images/3drender.png';
    }

    public function isNeedSupports()
    {
        return !empty($this->need_supports);
    }


    public static function getUsageGroupsByMaterialGroupCodes($materialsGroupCodes)
    {
        $groups = \Yii::$app->setting->get('store.usage_groups');
        $result = [];
        $result[] = [
            'id'          => '0',
            'title'       => _t('site.filepageUsage', 'Any'),
            'image'       => 'https://static.treatstock.com/static/images/usage-groups/all.svg',
            'description' => _t('site.filepageUsage', 'Show all available materials'),
        ];
        if (empty($groups) || !is_array($groups)) {
            return $result;
        }

        foreach ($groups as $k => $group) {
            if (!is_array($group['groups'])) {
                continue;
            }
            if (!empty($materialsGroupCodes) && count(array_intersect($materialsGroupCodes, $group['groups'])) === 0 &&
                count(array_intersect($group['groups'], $materialsGroupCodes)) === 0
            ) {
                continue;
            }

            $title = $k;
            $description = $group['description'] !== '-' ? $group['description'] : implode(", ", $group['groups']);
            if (!empty($group['title'])) {
                $title = $group['title'];
            }
            if (!empty($group['title_' . \Yii::$app->language])) {
                $title = $group['title_' . \Yii::$app->language];
            }
            if (!empty($group['description_' . \Yii::$app->language])) {
                $description = $group['description_' . \Yii::$app->language];
            }
            $result[] = [
                'id'          => $k,
                'title'       => $title,
                'image'       => $group['image'] !== '-' ? $group['image'] : '',
                'description' => $description,
            ];
        }
        return $result;
    }

    /**
     * @param PrinterMaterial[] $printerMaterials
     * @return array
     */
    public static function getUsageGroupsByMaterials($printerMaterials)
    {
        $materialsGroupCodes = [];
        foreach ($printerMaterials as $printerMaterial) {
            $materialsGroupCodes[$printerMaterial->group->code] = $printerMaterial->group->code;
        }
        return self::getUsageGroupsByMaterialGroupCodes($materialsGroupCodes);
    }

    /**
     * get usage groupos
     * if materials group is specified, filter by these material groups
     * so if we don't have printers with given material groups, no need to show in Usage
     *
     * @param MaterialColorsItem[] $materialsGroupItem
     * @return array
     */
    public static function getUsageGroups($materialsGroupItem = [])
    {
        $materialsGroupCodes = [];
        foreach ($materialsGroupItem as $colorsItem) {
            $materialsGroupCodes[$colorsItem->materialGroup->code] = $colorsItem->materialGroup->code;
        }

        return self::getUsageGroupsByMaterialGroupCodes($materialsGroupCodes);
    }
}