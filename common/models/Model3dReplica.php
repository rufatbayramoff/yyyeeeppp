<?php

namespace common\models;

use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\model3d\Model3dBase;
use lib\money\Currency;
use lib\money\Money;

/**
 * Class Model3dReplica
 *
 *
 * @property integer $original_model3d_id
 * @package common\models
 */
class Model3dReplica extends \common\models\base\Model3dReplica implements Model3dBaseInterface
{
    use Model3dBase {
        rules as traitRules;
    }

    public const UID_PREFIX = 'MR:';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'model3d_replica';
    }

    public function getViewBaseUrl()
    {
        return '3dmodelReplicas';
    }

    /**
     * @return array
     */
    public function rules()
    {
        $rules = array_merge(
            parent::rules(),
            $this->traitRules()
        );
        return $rules;
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getModel3dParts()
    {
        return parent::getModel3dReplicaParts()->orderBy('id asc');
    }

    public function getModel3dImgs()
    {
        return $this->getModel3dReplicaImgs();
    }

    public function getStoreOrder(): ?StoreOrder
    {
        if ($orderItems = $this->storeOrderItems) {
            $item = reset($orderItems);
            return $item->order;
        }
        return null;
    }

    /**
     * @return \common\models\query\Model3dQuery
     */
    public function getOriginalModel3d()
    {
        return $this->hasOne(Model3d::class, ['id' => 'original_model3d_id']);
    }

    public function setStoreUnit($storeUnit)
    {
        $this->populateRelation('storeUnit', $storeUnit);
        $this->store_unit_id = $storeUnit->id;
    }

    public function getCae()
    {
        return $this->getOriginalModel3d()->one()->cae;
    }

    /**
     * @return int
     */
    public function getCadFlag()
    {
        return $this->getOriginalModel3d()->one()->cae === Model3dBaseInterface::CAE_CAD ? 0 : 1;
    }

    public function cleanEmptyQty()
    {
        foreach ($this->getActiveModel3dParts() as $model3dPart) {
            if (($model3dPart->qty === 0) || (!$model3dPart->isCaclulatedProperties())) {
                $model3dPart->user_status = Model3dBasePartInterface::STATUS_INACTIVE;
                $model3dPart->qty = 0;
                $model3dPart->safeSave();
            }
        }
    }

    public function getSourceModel3d()
    {
        return $this->originalModel3d;
    }


    public function isOrderPayed()
    {
        $storeOrderItems = $this->storeOrderItems;
        foreach ($storeOrderItems as $orderItem) {
            if ($orderItem->order->isPayed()) {
                return true;
            }
        }
        return false;
    }

    public function getAuthor(): ?User
    {
        return $this->getSourceModel3d()->getAuthor();
    }

    public function isPublished(): bool
    {
        return false;
    }

    public function getPublishedAt(): string
    {
        return $this->getOriginalModel3d()->published_at;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getProductStatus(): string
    {
        return $this->originalModel3d->getProductStatus();
    }

    public function getProductStatusLabel(): string
    {
        return $this->originalModel3d->getProductStatusLabel();
    }

    public function isAvailableInCatalog(): bool
    {
        return false;
    }

    public function setCantBePublisehedUpdated(): void
    {

    }

    public function isCanBePublishUpdated(): bool
    {
        return false;
    }

    public function getUpdatedAt(): string
    {
        return $this->updated_at;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUuid(): string
    {
        return $this->id;
    }

    public function getPriceMoneyByQty($qty): ?Money
    {
        return Money::create($this->price_per_produce * $qty, Currency::USD);
    }

    public function getCompanyManufacturer(): ?Company
    {
        // TODO: Implement getCompanyManufacturer() method.
    }

    public function getMoqFromPriceMoney(): ?Money
    {
        // TODO: Implement getMoqFromPriceMoney() method.
    }

    public function getMinOrderQty(): float
    {
        // TODO: Implement getMinOrderQty() method.
    }

    public function getUnitTypeTitle($qty): string
    {
        // TODO: Implement getUnitTypeTitle() method.
    }

    public function getUnitTypeCode(): string
    {
        // TODO: Implement getUnitTypeCode() method.
    }

    public function getCuid(): string
    {
        return $this->originalModel3d->cuid;
    }

   public function getTotalQty()
   {
       $totalQty = 0;
       foreach ($this->getActiveModel3dParts() as $model3dPart) {
           $totalQty+=$model3dPart->qty;
       }
       return $totalQty;
   }

   public function getTotalVolume()
   {
       $totalVolume = 0;
       foreach ($this->getActiveModel3dParts() as $model3dPart) {
           $totalVolume+=$model3dPart->getVolume();
       }
       return $totalVolume;
   }
}