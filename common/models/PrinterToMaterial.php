<?php
namespace common\models;

use Yii;

/**
 * Edit this file
 * This is the model class for table "printer_to_material".
 */
class PrinterToMaterial extends \common\models\base\PrinterToMaterial
{
    public const STATUS_ACTIVE = 1;
}