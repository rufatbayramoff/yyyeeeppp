<?php

namespace common\models;

use common\modules\api\v2\models\ApiExternalSystemConfig;
use Exception;
use yii\helpers\Json;

/**
 * Class ApiExternalSystem
 *
 * @package common\models
 */
class ApiExternalSystem extends \common\models\base\ApiExternalSystem
{
    const MODEL3D_OWNER_UPLOAD_USER         = 'upload_user';
    const MODEL3D_OWNER_API_EXTERNAL_SYSTEM = 'api_external_system';
    const ALLOWED_MODEL3D_OWNER_TYPES       = [
        ApiExternalSystem::MODEL3D_OWNER_UPLOAD_USER         => 'Upload users',
        ApiExternalSystem::MODEL3D_OWNER_API_EXTERNAL_SYSTEM => 'Api external system'
    ];

    const SYSTEM_TINKERCAD = 'Tinkercad';

    public function load($data, $formName = null)
    {
        $loadResult = parent::load($data, $formName);
        if ($this->json_config == 'null') {
            $this->json_config = [];
        }
        return $loadResult;
    }

    public function validateJson()
    {

        if (empty($this->json_config)) {
            return true;
        }
        try {
            $result = Json::decode($this->json_config);
            if (!is_array($result)) {
                throw new Exception('Not valid json');
            }
        } catch (\Exception $e) {
            $this->addError('json_config', 'JSON Error: ' . $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @return ApiExternalSystemConfig
     */
    public function getConfig()
    {
        return new ApiExternalSystemConfig($this->json_config);
    }

    public function getAffiliateSource(): ?AffiliateSource
    {
        return $this->getAffiliateSources()->active()->one();
    }
}