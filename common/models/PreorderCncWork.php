<?php

namespace common\models;

use lib\money\Money;

/**
 * Class PreorderCncWork
 * @package common\models
 */
class PreorderCncWork extends \common\models\base\PreorderCncWork
{
    public function getPriceMoney()
    {
        return Money::create($this->price, $this->preorder->currency);
    }
}