<?php namespace common\models;
use frontend\components\image\ImageHtmlHelper;
use lib\money\Currency;
use Yii;
use yii\helpers\Json;

/**
 * user profile updates are logged
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserProfile extends \common\models\base\UserProfile
{
    public const SETTINGS_NOCHECKONLINE  = 'noCheckOnline';
    public const SETTINGS_DIALOG_AUTO_CREATE  = 'allowDialogAutoCreate';
    public const SETTINGS_COMPANY_DIALOG_AUTO_CREATE  = 'allowCompanyDialogAutoCreate';
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['current_lang', 'timezone_id'], 'required'],
            [['address_id', 'is_printservice', 'is_modelshop', 'default_license_id'], 'integer'],
            [['dob_date', 'updated_at'], 'safe'],
            [['phone_confirmed'], 'boolean'],
            [['current_metrics', 'gender', 'info_about', 'info_skills'], 'string'],
            [['full_name', 'phone', 'current_currency_iso', 'timezone_id', 'firstname', 'lastname'], 'string', 'max' => 45],
            [['website'], 'string', 'max' => 250],
            [['current_lang'], 'string', 'max' => 5],
            [['avatar_url', 'background_url', 'printservice_title', 'modelshop_title'], 'string', 'max' => 255],
            [['url_facebook', 'url_instagram', 'url_twitter'], 'string', 'max' => 245],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\UserAddress::class, 'targetAttribute' => ['address_id' => 'id']],

            ['gender', 'default', 'value' => 'none'],
            ['gender', 'required'],
            ['gender', 'in', 'range' => ['male', 'female', 'none']],
            ['timezone_id', 'required']
        ];
    }

    /**
     * 
     * @return array
     */
     public function attributeLabels()
    {
        return [
            'user_id' => _t('app', 'User ID'),
            'full_name' => _t('app', 'Full Name'),
            'dob_date' => _t('app', 'Date of birth'),
            'address_id' => _t('app', 'Address ID'),
            'website' => _t('app', 'Website'),
            'phone' => _t('app', 'Phone'),
            'phone_confirmed' => _t('app', 'Phone Confirmed'),
            'updated_at' => _t('app', 'Updated At'),
            'current_lang' => _t('app', 'Current Lang'),
            'current_currency_iso' => _t('app', 'Current Currency Iso'),
            'current_metrics' => _t('app', 'Current Metrics'),
            'timezone_id' => _t('app', 'Timezone ID'),
            'firstname' => _t('app', 'Firstname'),
            'lastname' => _t('app', 'Lastname'),
            'is_printservice' => _t('app', 'Is Printservice'),
            'is_modelshop' => _t('app', 'Is Modelshop'),
            'avatar_url' => _t('app', 'Avatar Url'),
            'background_url' => _t('app', 'Background Url'),
            'gender' => _t('app', 'Gender'),
            'printservice_title' => _t('app', 'Printservice Title'),
            'modelshop_title' => _t('app', 'Modelshop Title'),
            'info_about' => _t('app', 'Info About'),
            'info_skills' => _t('app', 'Info Skills'),
            'default_license_id' => _t('app', 'Default License ID'),
        ];
    }

    /**
     * save user updates and log old value
     * 
     * @param \common\models\UserProfile $profileModel
     * @param array $post
     * @return array
     */
    public function saveUserUpdates(\common\models\UserProfile $profileModel, $post)
    {
        $field = $post['field'];
        $oldValue = $profileModel->$field;
        $profileModel->$field = $post[$field];
        
        if ($profileModel->validate() && $profileModel->save()) {
            $outputValue = $profileModel->$field;
            if ($field == 'dob_date') {
                $outputValue = date("m/d/Y", strtotime($outputValue));
            }
            $result = [
                'output' => nl2br(\H($outputValue)),
                'message' => ''
            ];
            $newValue = $profileModel->$field;
            \common\models\UserLog::log(
                $profileModel->user_id, $field, $oldValue, $newValue
            );
        } else {
            $msg = \yii\helpers\Html::errorSummary($profileModel);
            $result = [
                'output' => '', 'message' => $msg, 'success' => false
            ];
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getCurrentCurrencyIso() {
        return $this->current_currency_iso?$this->current_currency_iso: Currency::USD;
    }

    public function getPublicAvatarUrl($size=100)
    {
        $avatarUrl = $this->avatar_url;
        $avatarPath = Yii::getAlias('@static') . $avatarUrl;
        $avatarUrl = Yii::getAlias(Yii::$app->params['staticUrl']) . $avatarUrl;
        $avatarUrl = ImageHtmlHelper::getThumbUrl($avatarUrl, $size, $size);
        return $avatarUrl;
    }

    /**
     * @param array $values
     * @throws \yii\base\UserException
     */
    public function updateSetting(array $values)
    {
        if (!\is_array($this->settings)) {
            $this->settings = Json::decode($this->settings);
        }
        if (empty($this->settings)) {
            $this->settings = [];
        }
        $settings = $this->settings;
        foreach ($values as $key => $value) {
            $settings[$key] = $value;
        }
        $this->settings  = $settings;
        $this->safeSave();
    }


}
