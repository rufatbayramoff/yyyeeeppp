<?php

namespace common\models;

use Yii;

/**
 * Price types
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StorePricerType
{
    /**
     * models author price type part - (Авторское вознаграждение за модель)
     */
    const MODEL = 'model';
    /**
     * tax applied to this payment
     */
    const TAX = 'tax';
    /**
     * our fee, already calculated final price (price*0.15) - Нашего собственного fee
     */
    const FEE = 'fee';
    /**
     * shipment price part - доставку
     */
    const SHIPPING = 'shipping';
    /**
     * pack price part - Затрат на упаковку
     */
    const PACKAGE = 'package';
    /**
     * refund all price or part price
     */
    const REFUND = 'refund';
    /**
     * discount price
     */
    const DISCOUNT = 'discount';
    /**
     * PS print work price - (Затрат на печать)
     */
    const PS_PRINT = 'ps_print';

    /**
     * PS product total price
     */
    const PS_PRODUCT = 'ps_product';

    /**
     *
     */
    const PS_PARCEL_PACKAGE = 'ps_parcel_package';

    /**
     *
     */
    const PS_DISCOUNT = 'ps_discount';

    /**
     * total price calculated from all other prices
     */
    const TOTAL = 'total';

    /**
     * Transaction fee . (TotalAmount * .029) + .3
     */
    const TRANSACTION = 'transaction';

    /**
     * additional services
     */
    const ADDON = 'addon';

    /**
     * ps fee which is invisible for client and ps
     * it's subtracted from Print price after reward
     */
    const PS_FEE = 'ps_fee';

    /**
     * affiliate fee is invisible for clients
     * it's part of Service Fee on top of it,
     * but this fee is used to reward our affiliates not touching our fee
     */
    const AFFILIATE_FEE = 'affiliate_fee';
}