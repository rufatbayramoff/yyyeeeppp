<?php namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends User
{

    public $username;
    public $password;
    public $rememberMe;
    protected $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    /**
     * 
     * @return type
     */
    public function attributeLabels()
    {
        return [
            'username' => _t('front.user', 'Username'),
            'password' => _t('front.user', 'Password'),
            'rememberMe' => _t('front.user', 'Remember Me'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        $res = false;
        /* @var $request yii\web\Request */
        $request = Yii::$app->request;
        $loginLogObj = new UserLoginLog();
        $userInf = $this->find()->asArray()->where(['username'=>$this->username])->one();
        $logData = ['data' => [
            'login_type' => 'form',
            'result' => 'OK',
            'remote_addr' => $request->getUserIP(),
            'http_user_agent' => $request->getUserAgent(),
            'created_at' => new \yii\db\Expression('NOW()'),
            'http_referer' => \Yii::$app->request->referrer,
            'post_data' => \yii\helpers\Json::encode(\Yii::$app->request->post())
        ]];
        if(!empty($userInf)){
            $logData['data']['user_id'] = $userInf['id'];
        }
        if ($this->validate()) {
            $user = $this->getUser();
            $logData['data']['user_id'] = $user->id;
            $res = Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            $this->assignRoles();
        }else{
            $logData['data']['result'] = 'failed';
        } 
        $loginLogObj->load($logData, 'data');
        $loginLogObj->safeSave();
        return $res;
    }

    /**
     * 
     */
    private function assignRoles()
    {
        #$auth = Yii::$app->authManager;
        #$auth->assign($auth->getRole('owner'), $this->getUser());
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
