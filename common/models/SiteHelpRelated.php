<?php

namespace common\models;

use common\components\ArrayHelper;

/**
 * Class SiteHelpRelated
 * @package common\models
 */
class SiteHelpRelated extends \common\models\base\SiteHelpRelated
{

    public function rules()
    {
        return [
            [['help_id'], 'required'],
            [['help_id', 'related_help_id'], 'integer'],
            [['help_id', 'related_help_id'], 'unique', 'targetAttribute' => ['help_id', 'related_help_id'], 'message' => 'The combination of Help ID and Related Help ID has already been taken.'],
            [['help_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\SiteHelp::class, 'targetAttribute' => ['help_id' => 'id']],
            [['related_help_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\SiteHelp::class, 'targetAttribute' => ['related_help_id' => 'id']],
        ];
    }
    /**
     * add related helps
     *
     * @param SiteHelp $help
     * @param $post
     */
    public static function addRelated(SiteHelp $help, $post)
    {
        $newIds = array_filter($post['SiteHelpRelated']);
        if(empty($newIds['related_help_id'])){
            return;
        }
        foreach($newIds['related_help_id'] as $k=>$v){
            SiteHelpRelated::addRecord(['help_id'=>$help->id, 'related_help_id'=>$v]);
        }
    }

    /**
     * updated related helps list
     *
     * @param SiteHelp $help
     * @param $post
     */
    public static function updateRelated(SiteHelp $help, $post)
    {
        $relatedHelpIds = ArrayHelper::getColumn($help->getRelatedHelps()->all(), 'id');

        $newIds = array_filter($post['SiteHelpRelated']);

        if(!empty($newIds['related_help_id'])) {
            foreach ($newIds['related_help_id'] as $k => $v) {
                if (in_array($v, $relatedHelpIds)) {
                    continue;
                }
                SiteHelpRelated::addRecord(['help_id' => $help->id, 'related_help_id' => $v]);
            }
        }
        foreach($relatedHelpIds as $k=>$v){
            if(empty($newIds['related_help_id']) || !in_array($v, $newIds['related_help_id'])){
                SiteHelpRelated::deleteAll(['help_id'=>$help->id, 'related_help_id'=>$v]);
            }
        }

    }
}