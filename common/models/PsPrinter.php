<?php

namespace common\models;

use common\components\ActiveQuery;
use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\history\HistoryTrait;
use common\components\PaymentExchangeRateFacade;
use common\components\ps\locator\Size;
use common\models\factories\Model3dTextureFactory;
use common\models\query\StoreOrderAttempQuery;
use common\modules\payment\fee\FeeHelper;
use common\modules\xss\helpers\XssHelper;
use common\services\PrinterMaterialService;
use common\services\PsPrinterService;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\GoneHttpException;

/**
 * Ps printer class. Helps to calculate print price, and etc.
 *
 * @property PsPrinterMaterial[] $psPrinterMaterials
 * @property PsPrinterToProperty[] $psPrinterToProperties
 * @property PsPrinterMaterial[] $materials                 Allowed materials
 * @property PsPrinterToProperty[] $redifinedProperties     Materials
 * @property PsMachineDelivery[] $psMachineDeliveries       Deliveries
 * @property PsPrinterTest $test
 * @property StoreOrder $testOrder
 * @property Company $company
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class PsPrinter extends base\PsPrinter
{
    use HistoryTrait;

    const SCENARIO_UPDATE = 'update';

    const RELATION_REDEFINED_PROPERTIES = 'redifinedProperties';

    const RECCOMEND_MIN_ORDER_PRICE_IN_USD = 10;

    /**
     * Printer test status
     */
    const TEST_STATUS_NO           = 'no';
    const TEST_STATUS_COMMON       = 'common';
    const TEST_STATUS_PROFESSIONAL = 'professional';

    const PROPERTY_ALL                   = 'all';
    const PROPERTY_BUILD_VOLUME          = 'Build volume';
    const PROPERTY_LAYER_RESOLUTION_LOW  = 'Layer resolution Low';
    const PROPERTY_LAYER_RESOLUTION_HIGH = 'Layer resolution High';
    const PROPERTY_BUILD_VOLUME_ID       = 1;

    private $printPrices;

    public function behaviors()
    {
        return [
            'history' => [
                'class'          => \common\components\HistoryBehavior::className(),
                'historyClass'   => PsPrinterHistory::className(),
                'foreignKey'     => 'ps_printer_id',
                'actionCode'     => 'update',
                'skipAttributes' => ['updated_at', 'created_at']
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['positional_accuracy', 'number', 'max' => $this->printer ? $this->printer->getMaxPositionalAccuracy() : 0.001, 'min' => 0.001],
                ['description', 'string', 'max' => 1500],
                ['min_order_price', 'number', 'min' => 1],
                [['min_order_price'], 'required'],
            ]
        );
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge(
            parent::scenarios(),
            [
                self::SCENARIO_UPDATE => [
                    'title',
                    'description',
                    'printer_id',
                    'price_per_hour',
                    'price_per_volume',
                    'min_order_price',
                    'user_status',
                    'positional_accuracy'
                ],
            ]
        );
    }

    /**
     * If printer not active or in status rejected or checking or banned or deleted - can`t print
     * If ps in status rejected, new, checking, banned - can`t print
     * If user owner is deleted - cant print
     */
    public function isCanPrintInStore()
    {
        if (
            ($this->companyService->visibility === CompanyService::VISIBILITY_EVERYWHERE) &&
            $this->ps->isActive() &&
            $this->ps->user->getIsActive() &&
            $this->companyService->isModeratedApproved()
        ) {
            return true;
        }
        return false;
    }

    /**
     * Relation for materials
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(PsPrinterMaterial::class, ['ps_printer_id' => 'id'])
            ->joinWith(['material', 'material.printerToMaterials'], false)
            ->where(
                [
                    PsPrinterMaterial::column('is_active')  => 1,
                    PrinterMaterial::column('is_active')    => 1,
                    PrinterToMaterial::column('is_active')  => 1,
                    PrinterToMaterial::column('printer_id') => $this->printer_id,
                ]
            );
    }

    /**
     * Relation for redefined properties
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRedifinedProperties()
    {
        return $this->hasMany(PsPrinterToProperty::class, ['ps_printer_id' => 'id'])
            ->joinWith(
                [
                    'property' => function (ActiveQuery $query) {
                        $query->andWhere(['printer_properties.is_active' => 1]);
                    }
                ],
                false
            )
            ->andWhere(['ps_printer_to_property.is_active' => 1]);
    }


    /**
     * Test order relation
     *
     * @return StoreOrder
     */
    public function getTestOrder()
    {
        return StoreOrder::find()
            ->forPrinter($this)
            ->onlyTest()
            ->one();
    }

    /**
     * @param Ps $ps
     * @return PsPrinter
     */
    public static function factoryNew(Ps $ps)
    {
        $psMachine        = new CompanyService();
        $psMachine->ps_id = $ps->id;
        $printer          = new PsPrinter();
        $printer->setPsMachine($psMachine);
        $psMachine->setPsPrinter($printer);
        $printer->min_order_price = PaymentExchangeRateFacade::convert(self::RECCOMEND_MIN_ORDER_PRICE_IN_USD, Currency::USD, UserFacade::getCurrentCurrency(), true);
        return $printer;
    }

    public static function create(
        string $title,
        string $description
    ): self
    {
        $model                  = new self;
        $model->title           = $title;
        $model->description     = $description;
        $model->min_order_price = PaymentExchangeRateFacade::convert(self::RECCOMEND_MIN_ORDER_PRICE_IN_USD, Currency::USD, $currency, true);
        return $model;
    }


    public function setPsMachine(CompanyService $psMachine)
    {
        $this->populateRelation('companyService', $psMachine);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        if ($this->companyService && $this->companyService->isNewRecord) {
            $this->companyService->ps_printer_id = $this->id;
            $this->companyService->safeSave();
        }
    }

    /**
     * Set moderation status
     *
     * @param $psPrinterId
     * @param $userId
     * @param $status
     * @param $comment
     *
     * @return ModerLog
     */
    public function setStatus($psPrinterId, $userId, $status, $comment = '')
    {
        $this->companyService->updated_at       = DateHelper::now();
        $this->companyService->moderator_status = $status;
        $this->companyService->safeSave();
        $this->safeSave();

        $model              = new \common\models\ModerLog();
        $model->user_id     = $userId;
        $model->created_at  = DateHelper::now();
        $model->started_at  = DateHelper::now();
        $model->action      = $status;
        $model->result      = $comment;
        $model->object_type = 'ps_printer';
        $model->object_id   = $psPrinterId;
        $model->safeSave();

        PsPrinterService::updatePrintersCache();

        return $model;
    }

    /**
     *
     * @return Money
     */
    public function getMinPrintPrice(): Money
    {
        $minOrderPrice      = $this->getMinPrintPriceWithoutFee();
        $minOrderPriceMoney = Money::create($minOrderPrice, $this->company->currency);
        $feeHelper          = FeeHelper::init();
        $psFeeMin           = $feeHelper->getTsCommonFee($minOrderPriceMoney);
        return MoneyMath::sum($minOrderPriceMoney, $psFeeMin);
    }

    /**
     * @return float
     */
    public function getMinPrintPriceWithoutFee(): float
    {
        return (float)$this->min_order_price;
    }

    /**
     * get price per gram
     *
     * @param Model3dTexture $texture
     * @param bool $anonymousMode
     * @return mixed
     */
    public function getPrintPriceInfoForTexture($texture, $anonymousMode = false)
    {
        if (!$anonymousMode && UserFacade::isObjectOwner($this->ps)) { // for owner return 0 as print price
            return [
                'price_ml' => Money::zero($this->company->currency)
            ];
        }
        $uniqueCacheId = 'PrintPriceInfoTexture1_' . $this->id . '_' . ($texture->printerColor ? $texture->printerColor->render_color : 'null') . '_' .
            ($texture->printerMaterialGroup ? $texture->printerMaterialGroup->code : 'null') . '_' .
            ($texture->printerMaterial ? $texture->printerMaterial->filament_title : 'null');
        $result        = app('cache')->get($uniqueCacheId);
        if ($result !== false && 0) {
            return $result;
        }

        $psPrices = $this->getPrintPrices();
        # dd($texture);
        $color = $texture->printerColor->render_color;
        if ($texture->printerMaterial) {
            $material = $texture->printerMaterial->filament_title;
            $material = XssHelper::cleanXssProtect($material);
            if (isset($psPrices[$material]) && isset($psPrices[$material][$color])) {
                $priceInfo = $psPrices[$material][$color];
            } else {
                $priceInfo = null;
            }
        } elseif ($texture->printerMaterialGroup) {
            $cheapestPriceInfo = [];
            foreach ($psPrices as $oneColorInfo) {
                foreach ($oneColorInfo as $priceInfoColor => $onePriceInfo) {
                    if ($priceInfoColor === $color) {
                        if ($onePriceInfo['materialGroupCode']) {
                            if ($onePriceInfo['materialGroupCode'] === $texture->printerMaterialGroup->code) {
                                if (!$cheapestPriceInfo) {
                                    $cheapestPriceInfo = $onePriceInfo;
                                } elseif ($cheapestPriceInfo['price'] > $onePriceInfo['price']) {
                                    $cheapestPriceInfo = $onePriceInfo;
                                }
                            }
                        }
                    }
                }
            }
            if ($cheapestPriceInfo) {
                $priceInfo = $cheapestPriceInfo;
            } else {
                $priceInfo = null;
            }
        } else {
            $priceInfo = null;
        }
        unset($priceInfo['material']);
        app('cache')->set($uniqueCacheId, $priceInfo, mt_rand(60 * 3, 60 * 3 + 30));

        return $priceInfo;
    }

    /**
     * get current printer prices in array
     *  [material][color] = price
     *
     * @return array
     */
    public function getPrintPrices()
    {
        if ($this->printPrices) {
            return $this->printPrices;
        }
        $uniqueCacheId = 'PrintPrices_' . $this->id;
        $result        = app('cache')->get($uniqueCacheId);
        if ($result !== false) {
            return $result;
        }

        $prices = [];
        foreach ($this->materials as $k => $psMaterial) {
            $materialCode = $psMaterial->material->filament_title;
            $materialCode = XssHelper::cleanXssProtect($materialCode);
            foreach ($psMaterial->psPrinterColors as $psColor) {
                if (!isset($prices[$materialCode])) {
                    $prices[$materialCode] = [];
                }
                $row = [
                    'colorCode'         => $psColor->color->render_color,
                    'materialCode'      => $psMaterial->material->filament_title,
                    'materialGroupCode' => $psMaterial->material->group ? $psMaterial->material->group->code : null,
                    'price'             => $psColor->price,
                    'price_measure'     => $psColor->price_measure,
                    'price_gr'          => $psColor->getPricePerGr(),
                    'price_ml'          => $psColor->getPricePerMl(),
                    'price_usd_gr'      => $psColor->getPriceUsdPerGr(),
                    'price_usd_ml'      => $psColor->getPriceUsdPerMl()
                ];

                $prices[$materialCode][$psColor->color->render_color] = $row;
            }
        }
        app('cache')->set($uniqueCacheId, $prices, mt_rand(60 * 3, 60 * 3 + 30));
        $this->printPrices = $prices;
        return $prices;
    }


    /**
     * @return Model3dTexture
     */
    public function getMostCheapestTexture()
    {
        $cheapestMaterial = $chipsetColor = null;
        $cheapestPrice    = PHP_INT_MAX;

        foreach ($this->materials as $psPrinterMaterial) {
            foreach ($psPrinterMaterial->psPrinterColors as $psPrinterColor) {
                if ($psPrinterColor->price < $cheapestPrice) {
                    $cheapestPrice    = $psPrinterColor->price;
                    $cheapestMaterial = $psPrinterMaterial;
                    $chipsetColor     = $psPrinterColor;
                }
            }
        }

        if (!$cheapestMaterial) {
            return null;
        }

        $texture = Model3dTextureFactory::createModel3dTexture();
        $texture->setPrinterMaterial($cheapestMaterial->material);
        $texture->setPrinterColor($chipsetColor->color);
        return $texture;
    }

    public function getAvailableTextures()
    {
        $textures = [];
        foreach ($this->materials as $psPrinterMaterial) {
            foreach ($psPrinterMaterial->psPrinterColors as $psPrinterColor) {
                $texture = Model3dTextureFactory::createModel3dTexture();
                $texture->setPrinterColor($psPrinterColor->color);
                $texture->setPrinterMaterial($psPrinterMaterial->material);
                $textures[] = $texture;
            }
        }
        return $textures;

    }

    /**
     * get validated printer to be used by end-user
     *
     * @param int $id id of printer service
     * @return PsPrinter
     * @throws \yii\web\GoneHttpException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getValidated($id)
    {
        $item = self::tryFindByPk($id, _t('site.store', 'This printer is not found.'));
        if ($item->companyService->visibility != CompanyService::VISIBILITY_EVERYWHERE) {
            throw new GoneHttpException(_t('site.store', 'This printer is currently inactive. Set the printer as active to order a 3D print.'));
        }
        if (!UserFacade::isObjectOwner($item->ps)) {
            if (!$item->companyService->isModeratedApproved()) {
                throw new GoneHttpException(_t('site.store', 'This printer cannot be used.'));
            }
        }
        return $item;
    }

    /**
     * validate print by checking store unit model and given material and color
     *
     * @param Model3dReplica $model3dReplica
     * @throws \yii\base\UserException
     */
    public function tryValidatePrint(Model3dReplica $model3dReplica)
    {

        $printerAllowedTextures = PrinterMaterialService::createAllowedTexturesByPrinter($this);
        if (!PrinterMaterialService::isModel3dTexturesInPrinterAllowColors($model3dReplica, $printerAllowedTextures)) {
            throw new \yii\base\UserException(_t('site.store', 'This printer cannot print with given material and color'));
        }
    }

    /**
     * get image
     *
     * @return string
     */
    public function getImage()
    {
        return !empty($this->image) ? $this->image : '/static/images/3drender.png';
    }


    public function getReviewsCount()
    {
        return StoreOrderReview::find()
            ->joinWith(
                [
                    'order.currentAttemp' => function (StoreOrderAttempQuery $query) {
                        $query
                            ->forPrinterId($this->id)
                            ->inStatus(StoreOrderAttemp::STATUS_RECEIVED);
                    }
                ],
                false
            )
            ->andWhere([StoreOrderReview::column('status') => StoreOrderReview::STATUS_MODERATED])
            ->count();
    }

    /**
     * get quick link to printer
     *
     * @param CompanyService $machine
     * @return string
     */
    public static function getLink(CompanyService $machine)
    {
        $url    = PsFacade::getPsLink($machine->ps);
        $server = param('server');
        $title  = \H($machine->getTitleLabel());
        $href   = Html::a($title, $server . '/c/' . $machine->ps->url, ['target' => '_blank']);
        return $href;
    }

    public function getMaxModelLen()
    {
        $buildVolumeData = $this->getBuildVolume();
        return max($buildVolumeData[0], $buildVolumeData[1], $buildVolumeData[2]);
    }

    public function getBuildVolumeSize()
    {
        $buildVolumeData   = $this->getBuildVolume();
        $buildSize         = Size::create($buildVolumeData[0], $buildVolumeData[1], $buildVolumeData[2]);
        return $buildSize;
    }

    /**
     * @param Size $modelSize
     * @return boolean
     */
    public function canPrintSize(Size $modelSize)
    {
        $buildSize         = $this->getBuildVolumeSize();
        $isMoreThanOrEqual = $buildSize->isMoreOrEqualThen($modelSize);
        return $isMoreThanOrEqual;
    }

    public function getAcceptedOrPrintingOrdersCount(): int
    {
        return (int)StoreOrderAttemp::find()
            ->forMachineId($this->companyService->id)
            ->inStatus([StoreOrderAttemp::STATUS_ACCEPTED, StoreOrderAttemp::STATUS_PRINTING])
            ->count();
    }

    /**
     * @param Model3dTexture[] $model3dTextures
     * @return bool
     */
    public function canPrintTextures($model3dTextures)
    {
        foreach ($model3dTextures as $texture) {
            $textureExits = false;
            foreach ($this->materials as $psPrinterMaterial) {
                foreach ($psPrinterMaterial->colors as $psColor) {
                    $printerColor = $psColor->color;
                    if (!$psPrinterMaterial->material->group->is_active) {
                        continue;
                    }
                    if (($texture->printerColor->render_color === $printerColor->render_color) &&
                        ($texture->calculatePrinterMaterialGroup()) &&
                        ($texture->calculatePrinterMaterialGroup()->id === $psPrinterMaterial->material->group->id)
                    ) {
                        if (!$texture->printerMaterial || $texture->printerMaterial->filament_title == $psPrinterMaterial->material->filament_title) {
                            $textureExits = true;
                            break 2;
                        }
                    }
                }
            }
            if (!$textureExits) {
                return false;
            }

        }
        return true;
    }

    /**
     * get printer build volume.
     * if error accures, returns default build volume
     * always in mm.
     * if PS defined in in,cm - this function will convert to mm.
     *
     * @return float[]
     */
    public function getBuildVolume()
    {
        $result = [220, 220, 230];
        try {
            $properties = $this->getProperties(['code' => [PsPrinter::PROPERTY_BUILD_VOLUME]]);
            if ($properties && is_array($properties)) {
                $buildProps = array_values($properties);
                if (isset($buildProps[0])) {
                    $buildProps = $buildProps[0];
                    $result     = \frontend\models\ps\PsFacade::parseBuildVolume($buildProps['value']);
                }
            }
        } catch (\Exception $e) {
        }
        return $result;
    }


    public function getProperties($filter = [])
    {
        if (empty($filter['code'])) {
            $filter['code'] = [self::PROPERTY_BUILD_VOLUME];
        }
        /** @var \common\models\PsPrinterToProperty[] $redefinedProperties */
        $redefinedProperties = ArrayHelper::index($this->psPrinterToProperties, 'property_id');
        $result              = [];
        if (!$this->printer) {
            return [];
        }
        foreach ($this->printer->printerToProperties as $k => $val) {
            if ($filter['code'] === self::PROPERTY_ALL || in_array($val->property->code, $filter['code'])) {
                $result[$val->property_id] = [
                    'id'    => $val->property_id,
                    'code'  => $val->property->code,
                    'value' => isset($redefinedProperties[$val->property_id])
                        ? $redefinedProperties[$val->property_id]->value
                        : $val->value,
                ];
            }
        }

        return $result;
    }


    /**
     * Add material to printer
     *
     * @param PsPrinterMaterial $material
     * @todo add asertion that this color exist in orignal material
     * @todo add assertion taht this color not exist already
     */
    public function addMaterial(PsPrinterMaterial $material)
    {
        AssertHelper::assert($material->isNewRecord, 'Cant add saved color as new');
        $material->created_at = new Expression('NOW()');
        $material->updated_at = new Expression('NOW()');
        $material->is_active  = 1;
        $this->link('materials', $material);
    }

    /**
     * Add color to material
     *
     * @param PsPrinterToProperty $property
     */
    public function addRedefinedProperty(PsPrinterToProperty $property)
    {
        AssertHelper::assert($property->isNewRecord, 'Cant add saved property as new');
        $property->created_at = new Expression('NOW()');
        $property->updated_at = new Expression('NOW()');
        $property->is_active  = 1;
        $this->link(self::RELATION_REDEFINED_PROPERTIES, $property);
        PsPrinterHistory::saveHistory($this->id, UserFacade::getCurrentUserId(), 'add_redefined_property');
    }

    /**
     * Delete material
     *
     * @param PsPrinterMaterial $material
     */
    public function deleteMaterial(PsPrinterMaterial $material)
    {
        AssertHelper::assert($material->ps_printer_id == $this->id, 'Not this printer material');
        foreach ($material->colors as $color) {
            $material->unlink('colors', $color, true);
        }
        $this->unlink('materials', $material, true);
    }

    /**
     * Return printer measure
     *
     * @return string
     * @todo delete it after refactor (move measure from color to printer)
     */
    public function getMeasure()
    {
        if (isset($this->materials[0]->colors[0])) {
            return $this->materials[0]->colors[0]->price_measure;
        }
        return \common\models\PsPrinterColor::MEASURE_GRAM;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(PsPrinterTest::class, ['ps_printer_id' => 'id']);
    }

    /**
     * Return label of print status for printer owner
     */
    public function getTestStatusLabel()
    {
        return $this->companyService->getCertificationLabel();
    }

    /**
     * Is printer sertified
     *
     * @return bool
     */
    public function isCertificated()
    {
        return $this->companyService->isCertificated();
    }

    /**
     * full certificate description
     *
     * @return mixed
     */
    public function getCertificateDescription()
    {

        $labels = [
            self::TEST_STATUS_NO           => _t('site.ps.test', 'This printer is not certified yet'),
            self::TEST_STATUS_COMMON       => _t(
                'site.ps.test',
                'This printer has a \'Common Certificate\' that confirms it is capable of printing average models to a high quality.'
            ),
            self::TEST_STATUS_PROFESSIONAL => _t(
                'site.ps.test',
                'This printer has a \'Professional Certificate\' that confirms it is capable of printing complex models, with many small details, to a high quality.'
            ),
        ];

        return $labels[$this->test_status];
    }

    /**
     * get for filepage printer list certification data to display labels
     *
     * @return array
     */
    public static function getCertificationData()
    {
        return [
            self::TEST_STATUS_NO           => [
                'title'       => _t('site.ps', 'Certification: Not tested'),
                'label'       => 'cert-label--not-tested',
                'labelTxt'    => '',
                'description' => _t('site.ps.test', 'This printer is not certified yet'),
            ],
            self::TEST_STATUS_COMMON       => [
                'title'       => _t('site.ps', 'This printer has a \'Common Certificate\' that confirms it is capable of printing average models to a high quality.'),
                'label'       => 'cert-label--common',
                'labelTxt'    => '<span class="tsi tsi-checkmark"></span>',
                'description' => _t('site.ps.test', 'This printer has a \'Common Certificate\' that confirms it is capable of printing average models to a high quality.')
            ],
            self::TEST_STATUS_PROFESSIONAL => [
                'title'       => _t(
                    'site.ps',
                    'This printer has a \'Professional Certificate\' that confirms it is capable of printing complex models, with many small details, to a high quality.'
                ),
                'label'       => 'cert-label--pro',
                'labelTxt'    => 'Pro',
                'description' => _t(
                    'site.ps.test',
                    'This printer has a \'Professional Certificate\' that confirms it is capable of printing complex models, with many small details, to a high quality.'
                ),
            ],
        ];
    }

    /**
     * Is ps rejectd
     *
     * @return string
     */
    public function getIsRejected()
    {
        return $this->companyService->moderator_status == CompanyService::MODERATOR_STATUS_REJECTED;
    }

    /**
     * Return last reject log item
     *
     * @return string|null
     * @throws \yii\base\InvalidParamException
     */
    public function getLastRejectLog()
    {
        $log = $this->getLastRejectModerLog();
        if (!$log) {
            return null;
        }
        $rejectReasonDetails = Json::decode($log->result);
        $rejectReasonStr     = '';
        if ($rejectReasonDetails['reason']) {
            $systemReject    = SystemReject::findByPk($rejectReasonDetails['reason']);
            $rejectReasonStr .= $systemReject->title;
        }
        if ($rejectReasonDetails['descr']) {
            $rejectReasonStr .= $rejectReasonStr ? ' - ' . $rejectReasonDetails['descr'] : $rejectReasonDetails['descr'];
        }
        return $rejectReasonStr;
    }

    /**
     * @return ModerLog
     */
    private function getLastRejectModerLog()
    {
        return ModerLog::find()
            ->where(['object_type' => 'ps_printer', 'object_id' => $this->id, 'action' => CompanyService::MODERATOR_STATUS_REJECTED])
            ->orderBy('id DESC')
            ->one();

    }

    public function isOwnToCurrentUser()
    {
        $userId = UserFacade::getCurrentUserId();
        if ($userId && ($this->ps->user->id === $userId)) {
            return true;
        }
        return false;
    }

    /**
     * Return defalut positional accurancy for this printer
     *
     * @return float
     */
    public function getPositionalAccuracy(): float
    {
        return $this->positional_accuracy ?? $this->printer->getMaxPositionalAccuracy();
    }

    public function getPsMachine()
    {
        return parent::getCompanyService()->where([CompanyService::column('type') => 'printer']);
    }

    /**
     * @return query\CompanyServiceQuery
     */
    public function getCompanyService()
    {
        return parent::getCompanyService()->where([CompanyService::column('type') => 'printer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPs()
    {
        return $this->getCompany();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'ps_id'])->via('companyService');
    }


    public function getDeliveries()
    {
        return $this->getPsMachineDeliveries();
    }

    public function getPsMachineDeliveries()
    {
        return $this->hasMany(\common\models\PsMachineDelivery::class, ['ps_machine_id' => 'id'])->via('companyService');
    }


    public function getIs_deleted()
    {
        return $this->companyService->is_deleted;
    }

    public function getCreated_at()
    {
        return $this->companyService->created_at;
    }

    public function getUpdated_at()
    {
        return $this->companyService->updated_at;
    }

    public function isAvailable()
    {
        $yes = $this->companyService->visibility == CompanyService::VISIBILITY_EVERYWHERE;
        $yes &= $this->company->user->getIsActive();
        $yes &= $this->companyService->isModeratedApproved();
        return $yes;
    }

    public function isFilteredByMaxOrdersCount()
    {
        $ps                       = $this->ps;
        $maxActiveOrders          = \Yii::$app->setting->get('settings.max_orders_per_printer_count');
        $maxActiveOrdersJcCompany = \Yii::$app->setting->get('settings.max_orders_per_printer_count_jc_company');
        $currentOrdersCount       = $ps->psProgressOrdersCount ? $ps->psProgressOrdersCount->progress_orders_count : 0;

        if ($ps->max_progress_orders_count) {
            return $currentOrdersCount >= $this->ps->max_progress_orders_count;
        }
        if ($ps->isJustCreatedCompany()) {
            return $currentOrdersCount >= $maxActiveOrdersJcCompany;
        }
        return $currentOrdersCount >= $maxActiveOrders;
    }

    public function domesticDelivery(): ?PsMachineDelivery
    {
        foreach ($this->psMachineDeliveries as $psMachineDelivery) {
            if ($psMachineDelivery->isDomesticDelivery()) {
                return $psMachineDelivery;
            }
        }
        return null;
    }

    public function internationalDelivery(): ?PsMachineDelivery
    {
        foreach ($this->psMachineDeliveries as $psMachineDelivery) {
            if ($psMachineDelivery->isInternationalDelivery()) {
                return $psMachineDelivery;
            }
        }
        return null;
    }


    public function domesticPackagePrice(): ?Money
    {
        $delivery = $this->domesticDelivery();
        if (!$delivery || !$delivery->isTsDelivery()) {
            return null;
        }
        return Money::create($delivery->packing_price, $this->company->currency);
    }

    public function internationalPackagePrice(): ?Money
    {
        $delivery = $this->internationalDelivery();
        if (!$delivery || !$delivery->isTsDelivery()) {
            return null;
        }
        return Money::create($delivery->packing_price, $this->company->currency);
    }
}
