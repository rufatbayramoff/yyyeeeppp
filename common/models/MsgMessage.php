<?php
namespace common\models;

use common\components\ArrayHelper;

/**
 * Edit this file
 * This is the model class for table "msg_message".
 * @property \common\models\MsgTopic $topic
 */
class MsgMessage extends \common\models\base\MsgMessage
{
    public $isBlocked;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['text', 'string', 'max' => 2000],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(MsgTopic::class, ['id' => 'topic_id']);
    }

    /**
     * Check that user is author of this message
     * @param User $user
     * @return bool
     */
    public function getIsAuthor(User $user)
    {
        return $this->user_id == $user->id;
    }


    public function getMsgMembers()
    {
        return $this->hasMany(MsgMember::className(), ['topic_id' => 'id'])->viaTable('msg_topic', ['id' => 'topic_id']);
    }
}