<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * Class WikiMachineAvailableProperty
 * @property string $slugCode
 * @package common\models
 */
class WikiMachineAvailableProperty extends \common\models\base\WikiMachineAvailableProperty
{
    const SHOW_IN_CATALOG_NONE = 'dont_show';
    const SHOW_IN_CATALOG_TOP = 'in_top';
    const SHOW_IN_CATALOG_BOTTOM = 'in_bottom';

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'equipment_category_id' => Yii::t('app', 'Equipment category'),
            ]
        );
    }

    /**
     * @param string $replacement
     * @return string
     */
    public function getSlugCode($replacement = '_'): string
    {
        return Inflector::slug($this->code, $replacement);
    }

}