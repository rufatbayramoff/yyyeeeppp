<?php

namespace common\models;

use common\models\repositories\Model3dRepository;
use common\services\Model3dService;
use frontend\models\user\UserFacade;
use yii\helpers\Url;

/**
 * Class ApiFile
 *
 * @package common\models
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class ApiFile extends base\ApiFile
{
    /**
     * api file new - uploading in progress
     */
    const STATUS_NEW = 'new';

    /**
     * file expired and must be deleted
     */
    const STATUS_EXPIRED = 'expired';

    /**
     * api file upload failed
     */
    const STATUS_FAILED = 'failed';

    /**
     * api file uploaded, storeUnit created and ready to be sold
     */
    const STATUS_UPLOADED = 'uploaded';

    /**
     * api file sold and customer gets his money
     */
    const STATUS_SOLD = 'sold';


    /**
     * api file was deleted by DELETE REST api call
     */
    const STATUS_DELETED = 'deleted';

    /**
     * after how many hours to expire api file for selling
     *
     * @var int
     */
    private static $expireAfterHours = 24;


    public $fileIds;
    /**
     * get api file status available for listing (but not viewing)
     *
     * @return array
     */
    public static function getActiveStatuses()
    {
        return [
            ApiFile::STATUS_UPLOADED,
            ApiFile::STATUS_NEW,
            ApiFile::STATUS_SOLD
        ];
    }

    /**
     * get model3d file full site url
     *
     * @param bool $model3dObj
     * @return string|null
     * @throws \yii\base\InvalidParamException
     */
    public function getModel3dUrl($model3dObj = false)
    {
        $model3dObj = $model3dObj ? $model3dObj : Model3dRepository::findByFileId($this->file_id);
        if (!$model3dObj) {
            return null;
        }
        $urlParams = ['/catalog/model3d/preload', 'id' => $model3dObj->id, 'akey' => $this->getAffiliateKey()];
        $user = UserFacade::getCurrentUser(); // api user
        // @TURNED OFF
        /**
        $affResource = AffiliateResource::find()->where(['api_key'=>$user->access_token])->one();
        if(!empty($affResource->params)){
            parse_str($affResource->params, $rParams);
            if(is_array($rParams)) foreach($rParams as $k=>$v){
                $urlParams[$k] = $v;
            }
        }*/
        $url = Url::toRoute($urlParams, true);
        return $url;
    }


    /**
     * get api file key to give link for orders
     *
     * @return string
     */
    public function getAffiliateKey()
    {
        $fileId = $this->file_id?:end($this->fileIds);
        return $this->id . '-' . $fileId . '-' . $this->user_id;
    }

    /**
     * find api file by key
     *
     * @param $key
     * @return null|static
     */
    public static function findByAffiliateKey($key)
    {
        $expireAfterHours = self::$expireAfterHours;
        $parts = explode('-', $key);
        return self::find()->where(
            [
                'id'      => $parts[0],
                'file_id' => $parts[1],
                'user_id' => $parts[2]
            ]
        )->andWhere(['and', "created_at>=NOW() - INTERVAL $expireAfterHours HOUR"])->one();
    }

    /**
     * check if this api file can be sold
     *
     * @return bool
     */
    public function isForSell()
    {
        $validStatus = $this->status === ApiFile::STATUS_UPLOADED;
        return $validStatus;
    }
}