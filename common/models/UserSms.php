<?php namespace common\models;

use common\components\exceptions\AssertHelper;
use lib\message\Constants;
use lib\sms\UserUnsubscribedException;
use Yii;
use yii\helpers\Json;

/**
 * Usage:
 *  UserSms::send($userId, $phone, $msg, 'confirm');
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserSms extends \common\models\base\UserSms
{

    const STATUS_NEW = 'new';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_FAILED = 'failed';

    const TYPE_NOTIFY = 'notify';
    const TYPE_CONFIRM = 'confirm';

    /**
     * send message to user
     * @param $gatewayName
     * @param int $userId
     * @param string $toPhone
     * @param string $message
     * @param string $type ['notify', 'confirm']
     * @return UserSms
     * @throws \Exception
     */
    public static function send($gatewayName, $userId, $toPhone, $message, $type)
    {
        $gateway = Yii::$app->smsGatewaysBundle->getGatewayByName($gatewayName);
        $toPhone = self::convertNumberToE164($toPhone);

        /** @var UserSms $msg */
        $msg = UserSms::addRecord([
            'user_id' => $userId,
            'created_at' => dbexpr('NOW()'),
            'phone' => $toPhone,
            'message' => $message,
            'status' => self::STATUS_NEW,
            'type' => $type == self::TYPE_NOTIFY ? self::TYPE_NOTIFY : self::TYPE_CONFIRM,
            'updated_at' => dbexpr('NOW()'),
            'gateway' => $gatewayName,
        ]);
        try {
            $gateway->sendMessage($toPhone, $message, $msg->id);
            $msg->status = self::STATUS_ACCEPTED;
            AssertHelper::assertSave($msg);
        }

        catch (UserUnsubscribedException $e) {

            $msg->status = self::STATUS_ACCEPTED;
            AssertHelper::assertSave($msg);

            $user = User::findByPk($userId);

            $notifyConfig = [];
            foreach (Constants::getMessagesGroups() as $group) {
                $notifyConfig[$group][Constants::SENDER_TYPE_SMS] = Constants::TIME_POLICY_NEVER;
            }
            $user->notify_config = Json::encode($notifyConfig);
            AssertHelper::assertSave($user);

            return $msg;
        }

        catch (\Exception $e) {
            $msg->response = $e->getMessage();
            $msg->status = self::STATUS_FAILED;
            AssertHelper::assertSave($msg);
            throw $e;
        }
        return $msg;
    }

    /**
     * Convert number from phone format 79600123123 to SMS format
     * @param string $phoneNumber Phone number in phone format
     * @return string
     */
    public static function convertNumberToE164($phoneNumber)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $numberObj = $phoneUtil->parse('+'.$phoneNumber, null);
        $e164Phone = $phoneUtil->format($numberObj, \libphonenumber\PhoneNumberFormat::E164);
        return $e164Phone;
    }
}
