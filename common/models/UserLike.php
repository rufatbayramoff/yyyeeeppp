<?php
namespace common\models;

use League\Flysystem\Exception;
use Yii;

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserLike extends \common\models\base\UserLike
{
    
    const TYPE_MODEL3D = 'model3d';
    const TYPE_MODEL3D_IMG = 'model3d_img';
    const TYPE_PS = 'ps';
    const TYPE_PS_PRINTER = 'ps_printer';
    const TYPE_ORDER_ATTEMPT = 'order_attempt';

    /**
     * @var array
     * @todo replace by https://redmine.tsdev.work/issues/441
     */
    protected $objectTypes = [
        'model' => ['table' => 'model3d'],
        'img' => ['table' => 'model3d_img'],
        'ps' => ['table' => 'ps'],
        'ps_printer' => ['table' => 'ps_printer'],
        'pub' => ['table' => '']
    ];


    /**
     * @var array
     * @todo replace by https://redmine.tsdev.work/issues/441
     */
    private static $ojectTypeMap = [
        \common\models\Model3d::class => self::TYPE_MODEL3D,
        \common\models\base\Model3d::class => self::TYPE_MODEL3D,
        \common\models\PsPrinter::class => self::TYPE_PS_PRINTER,
    ];

    /**
     * 
     * @param string $type
     * @return string|bool
     * @todo replace by https://redmine.tsdev.work/issues/441
     */
    public function getObjectTable($type)
    {
        return $this->objectTypes[$type] ? $this->objectTypes[$type]['table'] : false;
    }

    /**
     *
     * @param string $val
     * @return bool
     * @throws \yii\base\Exception
     * @todo replace by https://redmine.tsdev.work/issues/441
     */
    public function setObjectType($val)
    {
        if(!in_array($val, array_keys($this->objectTypes))){
            throw new \yii\base\Exception('Object type not supported ' . $val);
        }
        $this->object_type = $val;
    }


    /**
     * @param $object
     * @return mixed
     * @throws Exception
     * @todo replace by https://redmine.tsdev.work/issues/441
     */
    public static function getObjectType($object)
    {
        $class = get_class($object);

        if(!array_key_exists($class, self::$ojectTypeMap))
        {
            throw new Exception("Bad bind class {$class}");
        }
        return self::$ojectTypeMap[$class];
    }
}