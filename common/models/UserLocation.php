<?php

namespace common\models;

use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\components\exceptions\ValidationException;
use common\models\factories\UserLocationFactory;
use lib\geo\GeoNames;
use lib\geo\models\Location;
use yii\helpers\Json;

/**
 * user location model with lat,lon attributes
 *
 */
class UserLocation extends \common\models\base\UserLocation
{

    /**
     * @var array
     */
    private $userDataEncoded;

    /**
     * Printer delivery address
     */
    const LOCATION_TYPE_DELIVERY = 'delivery';
    const LOCATION_TYPE_LOCATION = 'location';
    const LOCATION_TYPE_SHIP_FROM = 'ship_from';

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [
                    'zip_code',
                    'required',
                    'when' => function () {
                        return $this->location_type == self::LOCATION_TYPE_DELIVERY;
                    }
                ]
            ]
        );
    }
    public static function formatLocation(UserLocation $location, $tpl = null)
    {
        $tpl = $tpl  ? $tpl  : UserAddress::getCountryAddressTemplate($location->country_id);
        $data = [
            'address'  => $location->address,
            'city'     => $location->city->title,
            'country'  => $location->country->iso_code,
            'zip_code' => $location->zip_code,
            'region'   => $location->region ? $location->region->title : ''
        ];
        $dataTpl = array_combine(
            array_map(
                function ($k) {
                    return '%' . $k . '%';
                },
                array_keys($data)
            )
            ,
            $data
        );
        $tplKeys = array_keys($dataTpl);
        $result = str_replace($tplKeys, $dataTpl, $tpl);
        $result = str_replace(", ,", ", ", $result); // if no region speicified
        return $result;
    }



    /**
     * we save geo city from given location
     * if exists - return result
     *
     * @param Location $location
     * @return GeoCity|int
     */
    public static function factoryGeoCity(Location $location)
    {
        $geoCity = GeoNames::getCitiesByCoordsAndName($location->city, $location->lat, $location->lon);
        if (!$location->getGeoCountry()) {
            \Yii::warning("Country not found [" . $location->country . ']', 'pscatalog');
            return null;
        }
        $countryId = $location->getGeoCountry()->id;
        if ($geoCity) {
            if (!empty($location->region) && empty($geoCity->region_id)) {
                $regionId = GeoNames::getRegionOrCreate($countryId, $location->region)->id;
                $geoCity->region_id = $regionId;
                $geoCity->safeSave();
            } else {
                if (!empty($geoCity->region_id) && $geoCity->region->title != $location->region && !empty($location->region)) {
                    // try to find such region
                    $geoRegion = GeoRegion::findOne(['title' => $location->region]);
                    if (!$geoRegion && $location->region) {
                        $geoRegion = GeoNames::getRegionOrCreate($countryId, $location->region);
                    }
                    $geoCity->region_id = $geoRegion->id;
                    $geoCity->safeSave();
                }
            }
        } else {
            $regionId = null;
            if ($location->region) {
                $regionId = GeoNames::getRegionOrCreate($countryId, $location->region)->id;
            }
            $geoCity = GeoNames::getCityOrCreate([
                'country_id' => $countryId,
                'lat'        => $location->lat,
                'lon'        => $location->lon,
                'city'       => $location->city,
                'region_id'  => $regionId
            ], $regionId);
        }
        BaseActiveQuery::resetCacheDependency();
        return $geoCity;

    }

    /**
     * Find same user location or create new user location
     *
     * @param
     *            $locationData
     * @return UserLocation
     * @throws \common\components\exceptions\InvalidModelException
     */
    public static function findOrCreateLocation($user, $locationData)
    {
        $location = UserLocationFactory::createFromAddressData($user, $locationData);

        /** @var UserLocation $existLocation */
        if ($existLocation = UserLocation::find()->same($location)->one()) {
            $existLocation->setUserData($location->getUserData());
            $location = $existLocation;
        }
        return $location;
    }

    /**
     * @param $addressData
     */
    public function setUserData($addressData)
    {
        $this->user_data = Json::encode($addressData);
        $this->userDataEncoded = $addressData;
    }

    /**
     * @return array
     */
    public function getUserData()
    {
        if ($this->userDataEncoded === null) {
            $this->userDataEncoded = $this->user_data ? Json::decode($this->user_data) : [];
        }
        return $this->userDataEncoded;
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function getUserDataField($name)
    {
        $userData = $this->getUserData();
        return isset($userData[$name]) ? $userData[$name] : null;
    }

    /**
     * user_location to user_address converter
     *
     * @return \common\models\UserAddress
     */
    public function toUserAddress()
    {
        $userAddress = new UserAddress();

        $userAddress->setAttributes(
            [
                'country_id'       => $this->country_id,
                'country'          => $this->country,
                'zip_code'         => $this->zip_code,
                'region'           => $this->getUserDataField('region'),
                'city'             => $this->getUserDataField('city'),
                'address'          => $this->address,
                'extended_address' => $this->address2,
                'lat'              => $this->lat,
                'lon'              => $this->lon
            ]
        );
        return $userAddress;
    }

    public function toString()
    {
        return self::formatLocation($this, '%city%, %region%');
    }

    public function equal(UserLocation $location)
    {
        return $location->user_id === $this->user_id &&
               $location->country_id === $this->country_id &&
               $location->region_id === $this->region_id &&
               $location->city_id === $this->city_id;
    }

    /**
     * @throws ValidationException
     */
    public function validateOrException(): void
    {
        if(!$this->validate()) {
            throw new ValidationException($this);
        }
    }
}