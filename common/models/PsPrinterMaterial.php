<?php
namespace common\models;

use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use yii\db\Expression;

/**
 * Edit this file
 * This is the model class for table "ps_printer_material".
 *
 * @property PsPrinterColor[] $colors Colors
 */
class PsPrinterMaterial extends \common\models\base\PsPrinterMaterial
{
    const SCENARIO_UPDATE = 'update';
    
    public function behaviors()
    {
        return [
            'history' => [
                'class' => \common\components\HistoryBehavior::className(),
                'historyClass' => PsPrinterHistory::className(),
                'foreignKey' => 'ps_printer_id',
                'ownerIdField' => 'ps_printer_id',
                'actionCode' => 'update_material',
                'skipAttributes' => ['updated_at', 'created_at']
            ],
        ];
    }
    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_UPDATE => ['material_id']
        ]);
    }

    /**
     * Releation to printserver colors
     * @return \yii\db\ActiveQuery
     */
    public function getColors()
    {
        return $this->hasMany(PsPrinterColor::class, ['ps_material_id' => 'id'])
            ->where(['ps_printer_color.is_active' => 1]);
    }

    public function getCheapestColor()
    {
        $color = $this->colors[0];
        $minPrice = $color->price;
        foreach ($this->colors as $k => $v) {
            if ($minPrice > $v->price) {
                $color = $v;
            }
        }
        return $color;
    }

    /**
     * Add color to material
     * @param PsPrinterColor $color
     * @todo add asertion that this color exist in orignal material
     * @todo add assertion taht this color not exist already
     */
    public function addColor(PsPrinterColor $color)
    {
        AssertHelper::assert($color->isNewRecord, 'Cant add saved color as new');
        $color->created_at = new Expression('NOW()');
        $color->updated_at = new Expression('NOW()');
        $color->is_active = 1;
        $this->link('colors', $color);
    }
}