<?php

namespace common\models;

use common\models\repositories\FileRepository;
use frontend\components\image\ImageHtmlHelper;
use Yii;

/**
 * Class ProductMainSlider
 * @package common\models
 */
class ProductMainSlider extends \common\models\base\ProductMainSlider
{

    public function addImage(FileAdmin $image)
    {
        if($this->file) {
            $fileForDelete = $this->file;
            $this->file_uuid = null;
            $this->safeSave();
            $fileRepository = Yii::createObject(FileRepository::class);
            $fileRepository->delete($fileForDelete);
        }
        $image->setPublicMode(true);
        $image->setFixedPath('product_main_slider/images');
        $image->setOwner(self::class, 'file_id');
        ImageHtmlHelper::stripExifInfo($image);
        $fileRepository = \Yii::createObject(FileRepository::class);
        $fileRepository->save($image);
        $this->file_uuid = $image->uuid;
    }
}