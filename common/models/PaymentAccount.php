<?php

namespace common\models;

use common\modules\payment\exception\FatalPaymentException;
use common\modules\payment\exception\PaymentException;
use lib\money\Currency;

/**
 * Class PaymentAccount
 *
 * @package common\models
 */
class PaymentAccount extends \common\models\base\PaymentAccount
{
    public const ACCOUNT_SUPPORT                   = 1;
    public const ACCOUNT_TREATSTOCK_USD            = 110;
    public const ACCOUNT_THINGIVERSE               = 112;
    public const ACCOUNT_THINGIVERSE_FEE           = 113;
    public const ACCOUNT_EASYPOST                  = 120;
    public const ACCOUNT_TAXAGENT                  = 130;
    public const ACCOUNT_PAYPAL                    = 150;
    public const ACCOUNT_BRAINTREE                 = 160;
    public const ACCOUNT_STRIPE                    = 165;
    public const ACCOUNT_BANK_TRANSFER             = 161;
    public const ACCOUNT_ANONIM                    = 998;
    public const ACCOUNT_TREATSTOCK_DISCOUNT       = 115;
    public const ACCOUNT_TREATSTOCK_EASYPOST       = 116;
    public const ACCOUNT_TREATSTOCK_CORRECTION     = 117;
    public const ACCOUNT_TREATSTOCK_CORRECTION_EUR = 217;

    public const ACCOUNT_TREATSTOCK_EUR          = 210;
    public const ACCOUNT_EASYPOST_EUR            = 220;
    public const ACCOUNT_BRAINTREE_EUR           = 260;
    public const ACCOUNT_STRIPE_EUR              = 265;
    public const ACCOUNT_BANK_TRANSFER_EUR       = 261;
    public const ACCOUNT_ANONIM_EUR              = 298;
    public const ACCOUNT_TREATSTOCK_DISCOUNT_EUR = 215;
    public const ACCOUNT_TREATSTOCK_EASYPOST_EUR = 216;

    public const ACCOUNT_CURRENCY_USD = 111;
    public const ACCOUNT_CURRENCY_EUR = 211;

    public const ACCOUNT_TYPE_MAIN              = 'main';
    public const ACCOUNT_TYPE_AUTHORIZE         = 'authorize';
    public const ACCOUNT_TYPE_RESERVED          = 'reserved';
    public const ACCOUNT_TYPE_AWARD             = 'award';
    public const ACCOUNT_TYPE_TAX               = 'tax';
    public const ACCOUNT_TYPE_INVOICE           = 'invoice';
    public const ACCOUNT_TYPE_MANUFACTURE_AWARD = 'manufactureAward';
    public const ACCOUNT_TYPE_REFUND            = 'refund';
    public const ACCOUNT_TYPE_REFUND_REQUEST    = 'refundRequest';
    public const ACCOUNT_TYPE_PAYOUT            = 'payout';
    public const ACCOUNT_TYPE_CORRECTION        = 'correction';
    public const ACCOUNT_TYPE_FEE               = 'fee';
    public const ACCOUNT_TYPE_CONVERT_BNS_EUR   = 'convert_bns_eur';
    public const ACCOUNT_TYPE_CONVERT_BNS_USD   = 'convert_bns_usd';
    public const ACCOUNT_TYPE_CONVERT_EUR_USD   = 'convert_eur_usd';


    // Treatstock account types
    public const ACCOUNT_TYPE_EASYPOST = 'easypost';
    public const ACCOUNT_TYPE_DISCOUNT = 'discount';

    public static function getSystemAccounts(): array
    {
        return [
            self::ACCOUNT_SUPPORT,
            self::ACCOUNT_TREATSTOCK_USD,
            self::ACCOUNT_THINGIVERSE,
            self::ACCOUNT_EASYPOST,
            self::ACCOUNT_TAXAGENT,
            self::ACCOUNT_PAYPAL,
            self::ACCOUNT_BRAINTREE,
            self::ACCOUNT_ANONIM,
            self::ACCOUNT_BANK_TRANSFER,
            self::ACCOUNT_TREATSTOCK_DISCOUNT,
            self::ACCOUNT_TREATSTOCK_EASYPOST
        ];
    }

    public static function getConvertAccountTypes(): array
    {
        return [
            self::ACCOUNT_TYPE_CONVERT_BNS_EUR,
            self::ACCOUNT_TYPE_CONVERT_BNS_USD,
            self::ACCOUNT_TYPE_CONVERT_EUR_USD
        ];
    }

    public static function getConvertAccountType($from, $to): string
    {
        $map = [
            Currency::BNS . '_' . Currency::EUR => self::ACCOUNT_TYPE_CONVERT_BNS_EUR,
            Currency::EUR . '_' . Currency::BNS => self::ACCOUNT_TYPE_CONVERT_BNS_EUR,
            Currency::BNS . '_' . Currency::USD => self::ACCOUNT_TYPE_CONVERT_BNS_USD,
            Currency::USD . '_' . Currency::BNS => self::ACCOUNT_TYPE_CONVERT_BNS_USD,
            Currency::USD . '_' . Currency::EUR => self::ACCOUNT_TYPE_CONVERT_EUR_USD,
            Currency::EUR . '_' . Currency::USD => self::ACCOUNT_TYPE_CONVERT_EUR_USD
        ];
        $key = $from . '_' . $to;
        if (!array_key_exists($key, $map)) {
            throw new FatalPaymentException('Invalid convert account type');
        }
        return $map[$key];
    }

    /**
     * User Account types wich can have money
     *
     * @return array
     */
    public static function getUserAccountRealTypes(): array
    {
        return [
            self::ACCOUNT_TYPE_MAIN,
            self::ACCOUNT_TYPE_AUTHORIZE,
            self::ACCOUNT_TYPE_RESERVED,
            self::ACCOUNT_TYPE_TAX
        ];
    }

    public static function getUserAccountTypes(): array
    {
        return [
            self::ACCOUNT_TYPE_MAIN,
            self::ACCOUNT_TYPE_AUTHORIZE,
            self::ACCOUNT_TYPE_RESERVED,
            self::ACCOUNT_TYPE_AWARD,
            self::ACCOUNT_TYPE_TAX,
            self::ACCOUNT_TYPE_INVOICE,
            self::ACCOUNT_TYPE_MANUFACTURE_AWARD,
            self::ACCOUNT_TYPE_REFUND_REQUEST,
            self::ACCOUNT_TYPE_REFUND,
            self::ACCOUNT_TYPE_PAYOUT,
        ];
    }

    public static function getTreatstockAccountTypes(): array
    {
        return [
            self::ACCOUNT_TYPE_EASYPOST,
            self::ACCOUNT_TYPE_DISCOUNT
        ];
    }

    /**
     * Total summ in payment should be zerro
     */
    public static function getTechnicalZeroAccounts(): array
    {
        return [
            self::ACCOUNT_TYPE_RESERVED,
            self::ACCOUNT_TYPE_AUTHORIZE,
            self::ACCOUNT_TYPE_AWARD,
            self::ACCOUNT_TYPE_INVOICE,
            self::ACCOUNT_TYPE_MANUFACTURE_AWARD,
            self::ACCOUNT_TYPE_REFUND_REQUEST,
            self::ACCOUNT_TYPE_PAYOUT
        ];
    }

    public static function getIncomingAccounts(): array
    {
        return [
            self::ACCOUNT_BRAINTREE,
            self::ACCOUNT_BANK_TRANSFER,
            self::ACCOUNT_THINGIVERSE
        ];
    }
}