<?php
namespace common\models;

use Yii;

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class SiteHelpVote extends \common\models\base\SiteHelpVote
{
    const NO_ANSWER = 'noanswer';
    public static function getWhyNo()
    {
        return [
            "dontlike" => "I don't like the answer",
            "notaccurate" => "The information isn't accurate",
            "confusing" => "It's confusing",
            self::NO_ANSWER => "It doesn't answer my question"
        ];
    }
}