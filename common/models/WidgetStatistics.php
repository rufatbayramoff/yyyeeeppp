<?php

namespace common\models;

use common\components\ArrayHelper;

/**
 * Class WidgetStatistics
 *
 * @package common\models
 */
class WidgetStatistics extends \common\models\base\WidgetStatistics
{
    const TYPE_PS_FACEBOOK = 'psFacebook';
    const TYPE_PS_SITE = 'psSite';
    const TYPE_PRINTER_SITE = 'printerSite';
    const TYPE_DESIGNER_SITE = 'designerSite';

    public static function getTypes()
    {
        return [
            self::TYPE_PS_FACEBOOK => 'Facebook',
            self::TYPE_PS_SITE => 'Company',
            self::TYPE_PRINTER_SITE => 'Printer Widget',
            self::TYPE_DESIGNER_SITE => 'Designer Widget',
        ];
    }

    public static function getWidgetPsList()
    {
        $rows = (new \yii\db\Query())
            ->select(['ps_id', 'ps.title'])
            ->from('widget_statistics')
            ->innerJoin('ps', 'ps.id=ps_id')
            ->limit(1000)
            ->groupBy('ps_id')
            ->all();

        $d =  ArrayHelper::map($rows, 'ps_id', 'title');
        return $d;
    }
}