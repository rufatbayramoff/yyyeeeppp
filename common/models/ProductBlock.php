<?php

namespace common\models;

/**
 * Class ProductBlock
 *
 * @property \common\models\File[] $imageFiles
 * @package common\models
 */
class ProductBlock extends \common\models\base\ProductBlock
{

    public function getImageFiles()
    {
        return $this->hasMany(File::class, ['uuid' => 'file_uuid'])->via('productBlockImages');
    }

    /**
     * @return File[]
     */
    public function getImages(): array
    {
        return $this->imageFiles;
    }

    public function getImageFileByUuid($fileUuid)
    {
        foreach ($this->imageFiles as $imageFile) {
            if ($imageFile->uuid == $fileUuid) {
                return $imageFile;
            }
        }
        return null;
    }
    /**
     * @param File[] $files
     */
    public function addImageFiles($files)
    {
        $currentFiles = $this->getImages();
        foreach ($files as $file) {
            $file->setPublicMode(true);
            $file->setOwner(ProductBlock::class, 'file_uuid');
            $currentFiles[] = $file;
        }
        $this->setImageFiles($currentFiles);
    }

    public function setImageFiles($files)
    {
        $this->populateRelation('imageFiles', $files);
    }
}