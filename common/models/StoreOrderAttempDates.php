<?php

namespace common\models;

/**
 * Class StoreOrderAttempDates
 * @package common\models
 */
class StoreOrderAttempDates extends \common\models\base\StoreOrderAttempDates
{
    /**
     * Was the departure earlier
     *
     * @return bool
     */
    public function isShipped(): bool
    {
        return !empty($this->shipped_at);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function acceptedDateEnd(): string
    {
        return (new \DateTime($this->accepted_date))->format('Y-m-d 23:59:00');
    }
}