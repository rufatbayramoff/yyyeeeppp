<?php
namespace common\models;

use Yii;

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class OsnVendor extends \common\models\base\OsnVendor
{
 
    protected $_code = '';
    /**
     * removes spaces from username
     * checks if such username exists, if not prepands _osncode
     * 
     * @param string $username
     * @param string $osnCode
     * @return string
     */
    public function formatUsername($username)
    {
        $username = transliterator_transliterate('Any-Latin; Latin-ASCII; Lower()', $username);
        $username = str_replace(" ", "-", $username);
        $username = mb_strtolower($username);
        if(empty($username)){
            throw new \yii\base\UserException("Username cannot be found. Try again");
        }
        $double = \common\models\User::find()->select(['id', 'username'])
                        ->where(['username' => $username])->asArray()->one();
        if(!empty($double)){
            $username = $username . '_' . $this->_code;
        }
        return $username;
    }
    
    /**
     * update language value to iso code in yii
     * if in 'ru' outs 'ru-RU',
     *    in 'en_US' outs 'en-US'
     * currently simple realization
     * 
     * @param type $lang
     * @param type $osnCode
     * @return string
     */
    public function formatLanguage($lang)
    {
        if($lang=='ru'){
            $lang = 'ru-RU';
        }else if($lang=='en'){
            $lang = 'en-US';
        }else if($lang=='cn'){
            $lang = 'zh-CN';
        }
        $lang = str_replace("_", "-", $lang);
        return $lang;
    }
}