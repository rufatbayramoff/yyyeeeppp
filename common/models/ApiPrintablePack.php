<?php

namespace common\models;

use common\components\ArrayHelper;
use common\components\order\PriceCalculator;
use common\components\PaymentExchangeRateFacade;
use common\modules\api\v2\serializers\Model3dPartSerializer;
use lib\money\Currency;

/**
 * Class ApiPrintablePack
 *
 * @package common\models
 */
class ApiPrintablePack extends \common\models\base\ApiPrintablePack
{
    const STATUS_ACTIVE             = 'active';
    const STATUS_EXPIRED            = 'expired';
    const REASON_NOT_CALCULATED_YET = 'not_calculated_yet';
    const REASON_PRINT_IMPOSSIBLE   = 'printing_impossible';
    const REASON_LOCATION_NOT_SET   = 'client_location_not_set';

    public function fields()
    {
        return [
            'id'                  => 'id',
            'model3d_id'          => 'model3d_id',
            'created_at'          => 'created_at',
            'affiliate_price'     => 'affiliate_price',
            'affiliate_currency'  => 'affiliateCurrencyIso',
            'calculated_min_cost' => 'calculatedMinCost',
            'success'             => 'success',
            'scaleUnit'           => 'scaleUnit',
            'largestPartSize'     => 'largestPartSize',
            'parts'               => 'partsList'
        ];
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['affiliate_price', 'validateAffiliatePrice'],
        ]);

    }

    public function validateAffiliatePrice()
    {
        if (!$this->isNewRecord) {
            return true; // Don`t check old records
        }
        if (($this->apiExternalSystem->model3d_owner === ApiExternalSystem::MODEL3D_OWNER_UPLOAD_USER) && $this->affiliate_price) {
            $this->addError('affiliate_price', 'Can`t set affiliate price, if api external system model owner is User. Please set affiliate_price is 0.');
        }
    }

    public function getCalculatedMinCost()
    {
        if (!$this->model3d->isCalculatedProperties()) {
            return self::REASON_NOT_CALCULATED_YET;
        }

        $storeUnit = $this->model3d->storeUnit;

        if (!$this->geoLocation) {
            return self::REASON_LOCATION_NOT_SET;
        }

        $calcCost = PriceCalculator::calculateModel3dCatalogCostObject($storeUnit, $this->geoLocation);
        $costUsd = $calcCost->cost_usd;
        if ($costUsd !== null) {
            $costUsd = (float)$costUsd;
            $cost = PaymentExchangeRateFacade::convert($costUsd, Currency::USD, $this->affiliateCurrency->currency_iso);
            $cost = round($cost, 2);
            return [
                'materialGroup' => $calcCost->texture->printerMaterialGroup->code,
                'color'         => $calcCost->texture->printerColor->title,
                'cost'          => $cost
            ];
        } else {
            return self::REASON_PRINT_IMPOSSIBLE;
        }
    }

    public function getAffiliateCurrencyIso()
    {
        return $this->affiliateCurrency->currency_iso;
    }

    public function getSuccess()
    {
        return $this->id ? true : false;
    }

    public function getScaleUnit()
    {
        return $this->model3d->model_units;
    }

    public function getLargestPartSize()
    {
        return $this->model3d->getLargestModel3dPart()->getSize();
    }

    public function getPartsList()
    {
        $parts = $this->model3d->getActiveModel3dParts();
        $partsSerialized = Model3dPartSerializer::serialize($parts);
        $partsSerialized = ArrayHelper::index($partsSerialized, 'uid');
        return $partsSerialized;

    }
}