<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.12.17
 * Time: 11:07
 */

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\models\CompanyServiceCategory;
use creocoder\nestedsets\NestedSetsQueryBehavior;

class CompanyServiceCategoryQuery extends BaseActiveQuery
{
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    public function withoutRoot()
    {
        $this->andWhere(['<>',CompanyServiceCategory::column('id'), 1]);
        return $this;
    }

    /**
     * @return $this
     */
    public function allActive(): CompanyServiceCategoryQuery
    {
        $this->isActive()->orderBy('lft');
        return $this;
    }

    public function isActive(): CompanyServiceCategoryQuery
    {
        $this->withoutRoot()->andWhere([CompanyServiceCategory::column('is_active') => 1]);
        return $this;
    }

    public function orderByPosition(): CompanyServiceCategoryQuery
    {
        $this->orderBy(['position' => SORT_ASC]);
        return $this;
    }

    /**
     * @return $this
     */
    public function witOutCutting(): CompanyServiceCategoryQuery
    {
        $this->andWhere(['<>', 'code', CompanyServiceCategory::CODE_CNC]);
        return $this;
    }

    /**
     * @return $this
     */
    public function witOutPrinting(): CompanyServiceCategoryQuery
    {
        $this->andWhere(['<>', 'code', CompanyServiceCategory::CODE_3D_PRINTING]);
        return $this;
    }
}