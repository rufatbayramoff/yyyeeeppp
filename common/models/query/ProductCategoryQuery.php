<?php

namespace common\models\query;

use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\models\ProductCategory;
use common\models\ProductCommon;
use common\modules\product\repositories\ProductCategoryRepository;
use common\traits\db\ActiveScopeTrait;
use Yii;

/**
 * Class ProductCategoryQuery
 *
 * @package common\models
 */
class ProductCategoryQuery extends BaseActiveQuery
{
    use ActiveScopeTrait;

    /**
     * Only categories with top level (root categories)
     *
     * @return $this
     */
    public function top()
    {
        $this->andWhere(['`product_category`.`parent_id`' => 1]);
        return $this;
    }

    public function has3dModels()
    {
        $productRepository = Yii::createObject(ProductCategoryRepository::class);
        $categories = $productRepository->getModel3dCategoriesIds();
        $this->andWhere(['id' => $categories]);
    }

    public function withoutRoot()
    {
        $this->andWhere('`product_category`.`id` !=1');
        return $this;
    }

    public function level1()
    {
        $this->andWhere(['`product_category`.`parent_id`' => 1]);
        return $this;
    }

    public function level2()
    {
        $level1Categories = ProductCategory::find()->level1()->all();
        $this->andWhere(['`product_category`.`parent_id`' => ArrayHelper::getColumn($level1Categories, 'id')]);
        return $this;
    }

    public function level3()
    {
        $level2Categories = ProductCategory::find()->level2()->all();
        $this->andWhere(['`product_category`.`parent_id`' => ArrayHelper::getColumn($level2Categories, 'id')]);
        return $this;
    }

    public function finalLevel()
    {
        return $this->level3();
    }

    public function notOthers()
    {
        $this->andWhere('not(`product_category`.`title` like \'%other%\')');
        return $this;
    }


    /**
     * Childrens of category
     *
     * @param $category
     * @return $this
     */
    public function childsOf(ProductCategory $category)
    {
        $this->andWhere(['`product_category`.`parent_id`' => $category->id]);
        return $this;
    }

    public function visible($visible = 1)
    {
        $this->andWhere(['`product_category`.`is_visible`' => $visible]);
        return $this;
    }

    public function isActive()
    {
        $this->andWhere(['`product_category`.`is_active`' => 1]);
        return $this;
    }
}