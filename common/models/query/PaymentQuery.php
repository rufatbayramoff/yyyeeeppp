<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.10.18
 * Time: 9:50
 */

namespace common\models\query;

use common\components\ActiveQuery;
use common\components\BaseActiveQuery;
use common\models\Payment;

class PaymentQuery extends BaseActiveQuery
{
    public function inPayProcess()
    {
        return $this->andWhere(['payment.status' => [Payment::STATUS_NEW, Payment::STATUS_PENDING]]);
    }

    public function wasPayed()
    {
        $this->joinWith('paymentDetailOperations', false);
        $this->andWhere('payment_detail_operation.created_at is not null');
    }
}