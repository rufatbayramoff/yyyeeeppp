<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\components\BaseActiveQuery;
use common\models\MsgMember;
use common\models\MsgTopic;
use common\models\StoreOrder;
use common\models\User;

class MsgTopicQuery extends BaseActiveQuery
{
    /**
     * @param int $userId1
     * @param int $userId2
     * @return $this
     */
    public function betweenUsers(int $userId1, int $userId2)
    {
        $this
            ->joinWith('members member1user', false)
            ->joinWith('members member2user', false)
            ->andWhere([
                'member1user.user_id' => $userId1,
                'member2user.user_id' => $userId2
            ])
            ->andWhere("member1user.topic_id = member2user.topic_id");
        return $this;
    }

    /**
     * @param StoreOrder $order
     * @return MsgTopicQuery
     */
    public function forOrder(StoreOrder $order) : MsgTopicQuery
    {
        return $this->andWhere([
            MsgTopic::column('bind_id') => $order->id,
            MsgTopic::column('bind_to') => 'order',
        ]);
    }

    /**
     * @param User $user
     * @return MsgTopicQuery
     */
    public function forUser(User $user) : MsgTopicQuery
    {
        return $this
            ->joinWith('members', false)
            ->andWhere([MsgMember::column('user_id') => $user->id]);
    }
}