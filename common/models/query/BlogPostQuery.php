<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\traits\db\ActiveScopeTrait;
use common\components\ActiveQuery;

/**
 * Class BlogPostQuery
 * @package common\models\query
 */
class BlogPostQuery extends ActiveQuery
{
    use ActiveScopeTrait;

}