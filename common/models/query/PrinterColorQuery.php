<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\components\BaseActiveQuery;
use common\models\PrinterColor;

class PrinterColorQuery extends BaseActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere([PrinterColor::column('is_active') => 1]);
        return $this;
    }
}