<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.12.17
 * Time: 11:03
 */

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\models\WikiMachineAvailableProperty;

class WikiMachinePropertyQuery extends BaseActiveQuery
{
    public function filterShowInCatalog($type)
    {
        $this->joinWith('wikiMachineAvailableProperty');
        $this->andWhere(['wiki_machine_available_property.show_in_catalog' => $type]);
        return $this;
    }

    public function onlyTop()
    {
        return $this->filterShowInCatalog(WikiMachineAvailableProperty::SHOW_IN_CATALOG_TOP);
    }

    public function onlyBottom()
    {
        return $this->filterShowInCatalog(WikiMachineAvailableProperty::SHOW_IN_CATALOG_BOTTOM);
    }
}
