<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\models\base;
use common\models\User;

/**
 * Class UserQuery
 *
 * @package common\models
 */
class UserQuery extends BaseActiveQuery
{

    /**
     * Only users with confirmed email
     *
     * @return $this
     */
    public function onlyConfirmed()
    {
        $this->andWhere([User::column('status') => [User::STATUS_ACTIVE]]);
        return $this;
    }

    /**
     * Search by part of username
     *
     * @param $usernamePart
     * @return $this
     */
    public function likeUsername($usernamePart)
    {
        $this->andWhere(['like', 'username', $usernamePart]);
        return $this;
    }

    /**
     * Only in statuses active or unconfirmed
     *
     * @return $this
     */
    public function active()
    {
        $this->andWhere([User::column('status') => [User::STATUS_UNCONFIRMED, User::STATUS_ACTIVE]]);
        return $this;
    }

    /**
     * Exclude user
     *
     * @param base\User $user
     * @return $this
     */
    public function exclude(\common\models\base\User $user)
    {
        $this->andWhere(['!=', User::column('id'), $user->id]);
        return $this;
    }

    /**
     * @param int[] $userIds
     * @return $this
     */
    public function excludeIds($userIds)
    {
        if ($userIds) {
            $this->andWhere(['not in', User::column('id'), $userIds]);
        }
        return $this;
    }

    /**
     * Only system user
     *
     * @return $this
     */
    public function system()
    {
        $this->andWhere(['<', User::column('id'), 1000]);
        return $this;
    }

    /**
     * Only not system user
     *
     * @return $this
     */
    public function notSystem()
    {
        $this->andWhere(['>=', User::column('id'), 1000]);
        return $this;
    }

    public function hasCompany()
    {
        $this->joinWith('company');
        $this->andWhere('ps.id is not null');
    }

    /**
     * @return $this
     */
    public function notDeveloperAccounts()
    {
        $this->andWhere([
            'not in',
            User::column('email'),
            ['dimkreativ@gmail.com', 'hlfbt@list.ru', 'hlfbt2@list.ru', 'hlfbt3@list.ru', 'hlfbt4@list.ru', 'hlfbt5@list.ru', 'analitic1983@gmail.com']
        ]);
        return $this;
    }

    /**
     * @return $this
     */
    public function notTreatstockAccounts()
    {
        $this->andWhere(['not like', User::column('email'), ['%@treatstock.com'], false]);
        return $this;
    }

    public function notHiddenProfile()
    {
        $this->joinWith('userProfile')->andWhere(
            'user_profile.is_hidden_public_page=0'
        );
        return $this;
    }
}