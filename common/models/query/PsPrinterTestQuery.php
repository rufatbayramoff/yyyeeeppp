<?php
/**
 * Created by mitaichik
 */
namespace common\models\query;
use common\models\PsPrinterTest;
use common\components\ActiveQuery;

/**
 * Class PsPrinterTestQuery
 * @package common\models
 */
class PsPrinterTestQuery extends ActiveQuery
{
    /**
     * New test^ waiting resolving
     * @return $this
     */
    public function newTests()
    {
        $this->andWhere(['status' => PsPrinterTest::STATUS_NEW]);
        return $this;
    }
}