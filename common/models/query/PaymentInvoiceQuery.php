<?php

namespace common\models\query;

use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\models\PaymentInvoice;
use common\models\User;

class PaymentInvoiceQuery extends BaseActiveQuery
{
    public function isPayedStatus()
    {
        return $this->andWhere(['payment_invoice.status' => [PaymentInvoice::STATUS_PAID, PaymentInvoice::STATUS_PART_PAID]]);
    }

    public function isNotPayedStatus()
    {
        return $this->andWhere(['<>', 'payment_invoice.status', [PaymentInvoice::STATUS_PAID, PaymentInvoice::STATUS_PART_PAID]]);
    }

    public function wasPayed()
    {
        $this->joinWith([
            'payments' => function (PaymentQuery $paymentQuery) {
                $paymentQuery->wasPayed();
            }
        ], false);
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function withOutAffiliate(User $user): PaymentInvoiceQuery
    {
        $this->joinWith(['affiliateAward.affiliateSource']);
        $this->andWhere([
            'or',
            ['<>', 'affiliate_source.user_id', $user->id],
            ['is', 'affiliate_award.id', null]
        ]);
        return $this;
    }

    /**
     * @param string|null $to
     * @param string|null $from
     * @return $this
     */
    public function betweenUpdateAt(?string $to, ?string $from): PaymentInvoiceQuery
    {
        $this->andFilterWhere(['>=', 'payment_detail.updated_at', $from]);
        $this->andFilterWhere(['<=', 'payment_detail.updated_at', $to]);
        return $this;
    }

    public function withPaymentAccount(int $accountId): PaymentInvoiceQuery
    {
        $this->joinWith(['payments.paymentDetails']);
        $this->andWhere([
            'and',
            ['=', 'payment_detail.payment_account_id', $accountId],
            ['!=', 'payment_detail.amount', 0],
        ]);
        return $this;
    }
}