<?php

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\interfaces\Model3dBasePartInterface;
use common\models\UserSession;
use common\modules\product\interfaces\ProductInterface;
use common\models\Model3d;
use common\models\StoreUnit;
use common\models\User;
use common\components\ActiveQuery;

/**
 * Class MyQuery
 *
 * @package common\models
 */
class CsWindowQuoteQuery extends BaseActiveQuery
{
    public function user(?User $user)
    {
        if (!$user) {
            return $this->andWhere('cs_window_quote.user_id is null');
        }
        return $this->andWhere(['cs_window_quote.user_id' => $user->id]);
    }

    public function userSession(?UserSession $userSession)
    {
        if (!$userSession) {
            return $this->andWhere('cs_window_quote.user_session_id is null');
        }
        return $this->andWhere(['cs_window_quote.user_session_id' => $userSession->id]);
    }
}


