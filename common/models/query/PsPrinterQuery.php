<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;

use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\components\exceptions\AssertHelper;
use common\models\Company;
use common\models\CompanyService;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\models\PrinterTechnology;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\DeliveryType;
use common\models\PsPrinter;
use common\models\User;
use common\traits\db\DeletedScopeTrait;
use frontend\models\ps\PsFacade;
use lib\geo\GeoNames;
use common\components\ActiveQuery;
use yii\db\Query;

/**
 * Class PsPrinterQuery
 *
 * @package common\models
 */
class PsPrinterQuery extends BaseActiveQuery
{
    use DeletedScopeTrait;

    public function isCanPrintInStore()
    {
        return $this->notDeleted()->visible()->moderated();
    }

    /**
     * Search only deleted
     *
     * @return $this
     */
    public function deleted()
    {
        return $this->leftJoin('company_service as pmd', 'pmd.ps_printer_id=ps_printer.id')->andWhere(['pmd.is_deleted' => 1]);
    }

    /**
     * Search only not deleted
     *
     * @param bool $eagerLoading
     * @return $this
     */
    public function notDeleted($eagerLoading = true)
    {
        return $this->leftJoin('company_service as pmd', 'pmd.ps_printer_id=ps_printer.id')->andWhere(['pmd.is_deleted' => 0])
            ->joinWith(['companyService.ps'], $eagerLoading)
            ->joinWith('ps.user')
            ->andWhere(['<>', 'user.status', User::STATUS_DELETED]);
    }


    /**
     * @return $this
     */
    public function moderated()
    {
        $this->joinWith('companyService');
        $this->andWhere(['company_service.moderator_status' => CompanyService::moderatedStatusList()]);
        return $this;
    }

    /**
     * @param array $statuses
     * @return $this
     */
    public function inModerationStatuses(array $statuses)
    {
        AssertHelper::assert($statuses, 'Statuses is empty');
        $this->joinWith('companyService');
        $this->andWhere(['company_service.moderator_status' => $statuses]);
        return $this;
    }

    /**
     * @return $this
     */
    public function visible()
    {
        $this->joinWith('companyService');
        $this->andWhere(['company_service.visibility'=>CompanyService::VISIBILITY_EVERYWHERE]);
        return $this;
    }

    /**
     * Printers with pickup
     *
     * @return $this
     */
    public function pickupDelivery()
    {
        $this->joinWith(
            [
                'deliveries d' => function (ActiveQuery $query) {
                    $query->andWhere(['d.delivery_type_id' => DeliveryType::PICKUP_ID]);
                }
            ],
            false
        );
        return $this;
    }

    /**
     * Printers with pickup
     *
     * @return $this
     */
    public function pickupOnlyDelivery()
    {
        $this->joinWith(
            [
                'deliveries d'
            ],
            false
        );
        $this->groupBy('`ps_printer`.`id`');
        $this->having('GROUP_CONCAT(`ps_machine_delivery`.`delivery_type_id`) = \'' . DeliveryType::PICKUP_ID . '\'');
        return $this;
    }

    /**
     * Printers with pickup
     *
     * @param $countryIso
     * @return $this
     */
    public function domesticDelivery($countryIso = null)
    {
        $conditions = [
            'deliveries d' => function (ActiveQuery $query) {
                $query->andWhere(['d.delivery_type_id' => [DeliveryType::STANDART_ID]]);
            }
        ];
        if ($countryIso) {
            $conditions['psMachine.location l'] = function (ActiveQuery $query) use ($countryIso) {
                $query->andWhere(['l.country_id' => GeoNames::getCountryByISO($countryIso)->id]);
            };

        }
        $this->joinWith($conditions, false);
        return $this;
    }

    /**
     * Printers with pickup
     *
     * @return $this
     */
    public function internationalDelivery()
    {
        $this->joinWith(
            [
                'deliveries d' => function (ActiveQuery $query) {
                    $query->andWhere(['d.delivery_type_id' => [DeliveryType::INTERNATIONAL_STANDART_ID]]);
                }
            ],
            false
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function canPickupOrDeliveryToCountry($countryIso, $onlyDomestic = false)
    {
        $this->joinWith(
            [
                'deliveries d',
                'psMachine.location l'
            ],
            false
        );
        $conditions = ($onlyDomestic ? '' : '(`d`.delivery_type_id=' . DeliveryType::INTERNATIONAL_STANDART_ID . ') or ') .
            '((`d`.delivery_type_id=' . DeliveryType::STANDART_ID . ' or `d`.delivery_type_id=' . DeliveryType::PICKUP_ID . ') and `l`.country_id=' . GeoNames::getCountryByISO($countryIso)->id . ')';
        $this->andWhere($conditions);
        return $this;
    }

    public function technology(PrinterTechnology $technology)
    {
        $this->joinWith(
            'printer',
            false
        )
            ->andWhere(['printer.technology_id' => $technology->id]);
        return $this;
    }

    public function usage($usage)
    {
        return $this->materialGroups($usage['printerMaterialGroups']);
    }

    /**
     * @param PrinterMaterial[] $materials
     * @return $this
     */
    public function materials($materials)
    {
        $this->joinWith('psPrinterMaterials')
            ->andWhere(['material_id' => ArrayHelper::getColumn($materials, 'id')]);
    }

    /**
     * @param PrinterMaterialGroup[] $printerMaterialGroups
     */
    public function materialGroups($printerMaterialGroups)
    {
        $materialsId = [];
        foreach ($printerMaterialGroups as $materialGroup) {
            if (!$materialGroup) {
                continue;
            }
            foreach ($materialGroup->printerMaterials as $material) {
                $materialsId[] = $material->id;
            }
        }
        $this->joinWith('psPrinterMaterials')
            ->andWhere(['material_id' => $materialsId]);
        return $this;
    }


    /**
     * @param User $user
     * @return $this
     */
    public function forUser(User $user)
    {
        $ps = PsFacade::getPsByUserId($user->id);
        $this->joinWith(['companyService'])->andWhere(['company_service.ps_id' => $ps ? $ps->id : -1]);
        return $this;
    }

    /**
     * Only printers who resolved test order
     *
     * @return $this
     */
    public function withResolvedTestOrder()
    {
        $this->andWhere(['ps_printer.is_test_order_resolved' => 1]);
        return $this;
    }

    /**
     * Only printers who resolved test order
     *
     * @return $this
     */
    public function withoutResolvedTestOrder()
    {
        $this->andWhere(['ps_printer.is_test_order_resolved' => 0]);
        return $this;
    }

    public function onlyCertificated()
    {
        $this->andWhere("company_service.certification = '".CompanyService::CERT_TYPE_VERIFIED."'");
        return $this;
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function lastestCompanyPrinter(Company $company)
    {
        $this->forUser($company->user);
        $this->latest();
        return $this;
    }
}