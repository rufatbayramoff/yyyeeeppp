<?php

namespace common\models\query;

use common\components\ActiveQuery;
use common\traits\db\ActiveScopeTrait;

class CuttingMaterialQuery extends ActiveQuery
{
    use ActiveScopeTrait;
}
