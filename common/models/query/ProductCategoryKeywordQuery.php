<?php

namespace common\models\query;

use common\components\ActiveQuery;
use common\components\BaseActiveQuery;
use common\traits\db\ActiveScopeTrait;

/**
 * Class PrinterQuery
 * @package common\models
 */
class ProductCategoryKeywordQuery extends BaseActiveQuery
{
    use ActiveScopeTrait;
    
}
