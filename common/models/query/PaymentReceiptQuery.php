<?php

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\models\Payment;
use common\traits\db\ForUserScopeTrait;

class PaymentReceiptQuery extends BaseActiveQuery
{
    use ForUserScopeTrait;
}