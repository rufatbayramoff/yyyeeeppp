<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 10.02.17
 * Time: 16:14
 */

namespace common\models\query;

use common\models\base\MsgMessage;
use common\models\ContentFilterCheckedMessages;
use common\models\Model3d;
use common\models\MsgMember;
use common\models\StoreUnit;
use common\models\User;
use common\components\ActiveQuery;


class MsgMessageQuery extends ActiveQuery
{

    public function toUser(User $user)
    {
        $this->joinWith(['msgMembers']);
        $this->andWhere([MsgMember::column('user_id') => $user->id]);
        return $this;
    }

    /**
     * return only blocked messages
     *
     * @return $this
     */
    public function blocked()
    {
        $this->andWhere(MsgMessage::column('user_id') . '!=1');
        $this->joinWith(['contentFilterCheckedMessages']);
        $this->andWhere([ContentFilterCheckedMessages::column('isBanned') => 1]);
        return $this;
    }
}