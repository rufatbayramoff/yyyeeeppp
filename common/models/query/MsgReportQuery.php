<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\components\ActiveQuery;

class MsgReportQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function onlyNew()
    {
        return $this->andWhere(['moderated' => 0]);
    }

}