<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.12.17
 * Time: 11:07
 */

namespace common\models\query;

use common\components\BaseActiveQuery;
use creocoder\nestedsets\NestedSetsQueryBehavior;

class EquipmentCategoryQuery extends BaseActiveQuery
{
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    public function withoutRoot()
    {
        $this->andWhere('id !=1');
        return $this;
    }
}