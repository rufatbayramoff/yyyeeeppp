<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\components\BaseActiveQuery;
use common\models\SystemReject;
use common\traits\db\ActiveScopeTrait;

class SystemRejectQuery extends BaseActiveQuery
{
    use ActiveScopeTrait;

    /**
     * @return SystemRejectQuery
     */
    public function orderByPriority() : SystemRejectQuery
    {
        return $this->orderBy([
            SystemReject::column('priority') => SORT_ASC,
            SystemReject::column('id') => SORT_ASC
        ]);
    }

    /**
     * @param string $group
     * @return SystemRejectQuery
     */
    public function forGroup(string $group) : SystemRejectQuery
    {
        return $this->andWhere([SystemReject::column('group') => $group]);
    }

    /**
     * @param string $group
     * @return SystemRejectQuery
     */
    public function forSelect(string $group) :  SystemRejectQuery
    {
        return $this
            ->forGroup($group)
            ->active()
            ->orderByPriority();
    }
}