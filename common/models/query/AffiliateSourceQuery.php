<?php

namespace common\models\query;

use common\components\ActiveQuery;

class AffiliateSourceQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['inactive' => 0]);
    }
}
