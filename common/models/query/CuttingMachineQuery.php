<?php

namespace common\models\query;

use common\components\ActiveQuery;
use common\models\Company;
use common\models\CuttingMachine;
use common\models\CuttingMachineProcessing;
use common\models\DeliveryType;
use lib\geo\GeoNames;

class CuttingMachineQuery extends ActiveQuery
{
    public function availableForOffers()
    {
        $this->joinWith(['companyService' => function (CompanyServiceQuery $companyServiceQuery) {
            $companyServiceQuery->visibleEverywhere()->moderated()->notDeleted();
        }], false);
        $this->joinWith('cuttingMachineProcessings')->andWhere('cutting_machine_processing.cutting_price>'.CuttingMachineProcessing::MIN_CUTTING_LEN);
        return $this;
    }

    public function company(Company $company)
    {
        $this->joinWith('companyService');
        $this->andWhere(['company_service.ps_id' => $company->id]);
        return $this;
    }

    public function international($toCountryId)
    {
        $this->joinWith(
            [
                'deliveries',
                'companyService.location'
            ],
            false
        );
        $domesticCondition = '((`ps_machine_delivery`.delivery_type_id=' . DeliveryType::STANDART_ID . ' or `ps_machine_delivery`.delivery_type_id=' . DeliveryType::PICKUP_ID . ') and `user_location`.country_id=' . $toCountryId . ')';
        $conditions        = '(`ps_machine_delivery`.delivery_type_id=' . DeliveryType::INTERNATIONAL_STANDART_ID . ' or ' . $domesticCondition . ')';
        $this->andWhere($conditions);
        return $this;
    }

    public function domestic($countryId)
    {

        $this->joinWith(
            [
                'deliveries',
                'companyService.location'
            ],
            false
        );
        $conditions = '((`ps_machine_delivery`.delivery_type_id=' . DeliveryType::STANDART_ID . ' or `ps_machine_delivery`.delivery_type_id=' . DeliveryType::PICKUP_ID . ') and `user_location`.country_id=' . $countryId . ')';
        $this->andWhere($conditions);
        return $this;
    }
}