<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\models\SiteHelpCategory;

/**
 * Class SiteHelpCategoryQuery
 * @package common\models\query
 */
class SiteHelpCategoryQuery extends \common\components\BaseActiveQuery
{
    /**
     * @param SiteHelpCategory $parent
     * @return SiteHelpCategoryQuery
     */
    public function forParent(SiteHelpCategory $parent) : SiteHelpCategoryQuery
    {
        return $this->andWhere([SiteHelpCategory::column('parent_id') => $parent->id]);
    }

    /**
     * @return SiteHelpCategoryQuery
     */
    public function orderByPriority() : SiteHelpCategoryQuery
    {
        return $this->orderBy([
            SiteHelpCategory::column('priority') => SORT_ASC,
            SiteHelpCategory::column('id') => SORT_ASC,
        ]);
    }

    /**
     * @return SiteHelpCategoryQuery
     */
    public function withCoverFile() : SiteHelpCategoryQuery
    {
        return $this->with('coverFile');
    }

    /**
     * Select categories who have articles whith is_popular = 1
     * BE CAREFUL!! It also populate siteHelps relaton with only popular articles
     * @return SiteHelpCategoryQuery
     */
    public function hasPopularArticles() : SiteHelpCategoryQuery
    {
        $this->joinWith(['siteHelps' => function (SiteHelpQuery $query){
            $query->popular();
        }]);

        return $this;
    }

    /**
     * @return SiteHelpCategoryQuery
     */
    public function root() : SiteHelpCategoryQuery
    {
        return $this->andWhere([SiteHelpCategory::column('parent_id') => null]);
    }
}