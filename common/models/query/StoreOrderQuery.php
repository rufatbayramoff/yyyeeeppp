<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;

use common\components\ActiveQuery;
use common\components\exceptions\AssertHelper;
use common\components\order\TestOrderFactory;
use common\models\Company;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\PaymentInvoice;
use common\models\Preorder;
use common\models\PsPrinter;
use common\models\StoreOrder;
use common\models\StoreOrderDelivery;
use common\models\User;
use common\models\UserSession;

/**
 * Class StoreOrderQuery
 *
 * @package common\models
 */
class StoreOrderQuery extends ActiveQuery
{
    /**
     * With printer relations
     *
     * @return $this
     */
    public function withPrinterRelations()
    {
        $this->with(
            [
                'user',
                'user.userProfile',
                'billAddress',
                'shipAddress',
                'deliveryType',
                'storeOrderItems',
                'storeOrderItems.unit.model3d'
            ]
        );
        return $this;
    }

    public function isInterception()
    {
        $this->joinWith('attemps.preorder')->andWhere(['preorder.is_interception' => 1]);
        return $this;
    }

    public function isNotInterception()
    {
        $this->joinWith('attemps.preorder')->andWhere(['not', ['preorder.is_interception' => 1]]);
        return $this;
    }

    /**
     * @param PsPrinter $printer
     * @return $this
     */
    public function forPrinter(PsPrinter $printer)
    {
        AssertHelper::assert($printer->id, 'Cant find order for new printer');
        $this->joinWith('attemps.machine')->andWhere([CompanyService::column('ps_printer_id') => $printer->id]);

        return $this;
    }

    /**
     * @param array|int $id
     * @return $this
     */
    public function forPrinterId($id)
    {
        $this->joinWith('attemps.machine')->andWhere([CompanyService::column('ps_printer_id') => $id]);
        return $this;
    }

    public function withoutRemoved()
    {
        $this->andWhere(['<>', StoreOrder::column('order_state'), StoreOrder::STATE_REMOVED]);
        return $this;
    }

    /**
     * For status
     *
     * @param string[]|string $state
     * @return $this
     */
    public function forState($state)
    {
        $this->andWhere([StoreOrder::column('order_state') => $state]);
        return $this;
    }

    /**
     * @return StoreOrderQuery
     */
    public function inProcess()
    {
        return $this->forState(StoreOrder::STATE_PROCESSING);
    }

    public function isCompleated()
    {
        return $this->forState(StoreOrder::STATE_COMPLETED);
    }

    public function storeOrderAttemptInStatus($status)
    {
        $this->joinWith('currentAttemp');
        $this->andWhere([
            'store_order_attemp.status' => $status
        ]);
        return $this;
    }


    public function hasNewStoreOrderAttempt()
    {
        $this->joinWith('currentAttemp');
        $this->andWhere('(store_order.current_attemp_id is null or store_order_attemp.status="new")');
        return $this;
    }

    /**
     * @param string|string[] $statuses
     * @return $this
     */
    public function forPaymentStatuses($statuses)
    {
        $this->joinWith('primaryPaymentInvoice');
        $this->andWhere(['payment_invoice.status' => $statuses]);
        return $this;
    }

    public function notForPaymentStatuses($statuses)
    {
        $this->joinWith('primaryPaymentInvoice');
        $this->andWhere(['not in', 'payment_invoice.status', $statuses]);
        return $this;
    }


    /**
     * @param User $user
     * @return $this
     */
    public function forUser(User $user)
    {
        $this->andWhere([StoreOrder::column('user_id') => $user->id]);
        return $this;
    }

    public function withoutCompany(?Company $company)
    {
        if (!$company) {
            return $this;
        }
        return $this->joinWith('currentAttemp')->where(['!=', 'store_order_attemp.ps_id', $company->id]);
    }

    /**
     * @return $this
     */
    public function payed()
    {
        $this->forPaymentStatuses([
            PaymentInvoice::STATUS_AUTHORIZED,
            PaymentInvoice::STATUS_PAID,
            PaymentInvoice::STATUS_PART_PAID,
        ]);
        return $this;
    }

    /**
     * @return $this
     */
    public function onlyTest()
    {
        $this->andWhere([StoreOrder::column('user_id') => TestOrderFactory::getTestOrderUserId()]);
        return $this;
    }

    /**
     * @return $this
     */
    public function notTest()
    {
        $this->andWhere(['!=', StoreOrder::column('user_id'), TestOrderFactory::getTestOrderUserId()]);
        return $this;
    }

    public function notDispute()
    {
        $this->andWhere([StoreOrder::column('is_dispute_open') => 0]);
        return $this;
    }

    public function withoutBankInvoices()
    {
        $this->joinWith('paymentBankInvoices');
        $this->andWhere(['payment_bank_invoice.uuid' => null]);
        return $this;
    }

    /**
     * Not pickup orders
     *
     * @return $this
     */
    public function notPickup()
    {
        $this->joinWith(
            [
                'storeOrderDelivery' => function (ActiveQuery $query) {
                    $query->andWhere([StoreOrderDelivery::column('delivery_type_id') => [DeliveryType::STANDART_ID, DeliveryType::INTERNATIONAL_STANDART_ID]]);
                }
            ],
            false
        );
        return $this;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function deliveryType(int $id)
    {
        if (empty($id)) {
            return $this;
        }
        $this->joinWith(
            [
                'storeOrderDelivery' => function (ActiveQuery $query) use ($id) {
                    $query->andWhere([StoreOrderDelivery::column('delivery_type_id') => $id]);
                }
            ],
            false
        );
        return $this;
    }

    public function deliveryInternational()
    {
        $this->joinWith(
            [
                'storeOrderDelivery' => function (ActiveQuery $query) {
                    $query->andWhere([StoreOrderDelivery::column('delivery_type_id') => DeliveryType::INTERNATIONAL_STANDART_ID]);
                }
            ],
            false
        );
        return $this;
    }

    public function deliveryDomestic()
    {
        $this->joinWith(
            [
                'storeOrderDelivery' => function (ActiveQuery $query) {
                    $query->andWhere([StoreOrderDelivery::column('delivery_type_id') => DeliveryType::STANDART_ID]);
                }
            ],
            false
        );
        return $this;
    }

    public function deliveryPickup()
    {
        $this->joinWith(
            [
                'storeOrderDelivery' => function (ActiveQuery $query) {
                    $query->andWhere([StoreOrderDelivery::column('delivery_type_id') => DeliveryType::PICKUP_ID]);
                }
            ],
            false
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function treatstockCarrier()
    {
        $this->joinWith(
            [
                'storeOrderDelivery' => function (ActiveQuery $query) {
                    $query->andWhere(['like', dbexpr('JSON_EXTRACT(' . StoreOrderDelivery::column('ps_delivery_details') . ', "$.carrier")'), DeliveryType::CARRIER_TS]);
                }
            ],
            false
        );
        return $this;
    }

    public function myselfCarrier()
    {
        $this->joinWith(
            [
                'storeOrderDelivery' => function (ActiveQuery $query) {
                    $query->andWhere(['like', dbexpr('JSON_EXTRACT(' . StoreOrderDelivery::column('ps_delivery_details') . ', "$.carrier")'), DeliveryType::CARRIER_MYSELF]);
                }
            ],
            false
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function orderById()
    {
        $this->orderBy(StoreOrder::column('id'));
        return $this;
    }

    /**
     * @return $this
     */
    public function orderByIdDesc()
    {
        $this->orderBy([StoreOrder::column('id') => SORT_DESC]);
        return $this;
    }

    /**
     * @return $this
     */
    public function inChangePrinter()
    {
        $this->andWhere(
            [
                StoreOrder::column('order_state')       => [StoreOrder::STATE_PROCESSING],
                StoreOrder::column('current_attemp_id') => null
            ]
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function canChangePs()
    {
        $this->andWhere([StoreOrder::column('can_change_ps') => 1]);
        return $this;
    }

    public function anonymousOrders()
    {
        return $this->andWhere(['store_order.user_id' => User::USER_ANONIM]);
    }

    public function deliveryEmail($email)
    {
        return $this->joinWith('shipAddress')->andWhere(['user_address.email' => $email]);
    }

    /**
     * @param $status
     * @return $this
     */
    public function forCurrentAttempStatus($status)
    {
        $this->joinWith(
            [
                'currentAttemp' => function (StoreOrderAttempQuery $query) use ($status) {
                    $query->inStatus($status);
                }
            ],
            false
        );
        return $this;
    }

    /**
     * @param UserSession $userSession
     * @return $this
     */
    public function forUserSession(UserSession $userSession)
    {
        AssertHelper::assert($userSession->id);
        $this->andWhere([StoreOrder::column('user_session_id') => $userSession->id]);
        return $this;
    }

    /**
     * @param Preorder $preorder
     * @return StoreOrderQuery
     */
    public function forPreorder(Preorder $preorder): StoreOrderQuery
    {
        return $this->andWhere([StoreOrder::column('preorder_id') => $preorder->id]);
    }
}