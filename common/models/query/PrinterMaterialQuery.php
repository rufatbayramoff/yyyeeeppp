<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\components\BaseActiveQuery;
use common\models\PrinterMaterial;

class PrinterMaterialQuery extends BaseActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere([PrinterMaterial::column('is_active') => 1]);
        return $this;
    }
}