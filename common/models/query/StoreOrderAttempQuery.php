<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;

use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\models\base\StoreOrderAttemp;
use common\models\Company;
use common\models\Payment;
use common\models\PaymentInvoice;
use common\models\Ps;
use common\models\CompanyService;
use common\models\StoreOrder;
use common\models\User;

/**
 * Class StoreOrderQuery
 * @package common\models
 */
class StoreOrderAttempQuery extends BaseActiveQuery
{

    /**
     * @param array|int $id
     * @return $this
     */
    public function forPrinterId($id)
    {
        $this
            ->joinWith('machine', false)
            ->andWhere([CompanyService::column('ps_printer_id') => $id]);
        return $this;
    }

    /**
     * @param array|int $id
     * @return $this
     */
    public function forMachineId($id)
    {
        $this->andWhere([StoreOrderAttemp::column('machine_id') => $id]);
        return $this;
    }

    public function orderExists()
    {
        $this->andWhere('store_order.id is not null');
        return $this;
    }

    public function isInterceptionOrForMachineIdOrCompany($machineId, $company)
    {
        $this->joinWith('preorder')
             ->andWhere([
            'or',
            [StoreOrderAttemp::column('machine_id') => $machineId],
            [StoreOrderAttemp::column('machine_id') => null, StoreOrderAttemp::column('ps_id') => $company->id],
            ['preorder.is_interception' => 1]
        ]);
        return $this;
    }

    public function isInterceptionOrCompany(Company $company)
    {
        $machines = ArrayHelper::getColumn($company->companyServices, 'id');
        $this->isInterceptionOrForMachineIdOrCompany($machines, $company);
        return $this;
    }

    public function forMachineIdOrCompany($machineId, $company)
    {
        $this->andWhere([
            'or',
            [StoreOrderAttemp::column('machine_id') => $machineId],
            [StoreOrderAttemp::column('machine_id') => null, StoreOrderAttemp::column('ps_id') => $company->id]
        ]);
        return $this;
    }

    public function isInterception()
    {
        $this->joinWith('preorder')
             ->andWhere(['preorder.is_interception' => 1]);
        return $this;
    }

    public function isNotInterception()
    {
        $this->joinWith('preorder')
            ->andWhere(['or', ['preorder.is_interception' => 0], 'preorder.is_interception is null']);
        return $this;
    }

    public function forCompany(Company $company)
    {
        $this->andWhere([StoreOrderAttemp::column('ps_id') => $company->id]);
        return $this;
    }

    /**
     * Find attemps for undeleted machines or for PS (if attempt not have machine)
     * @param Ps $ps
     * @return $this
     */
    public function forUndeletdMachinesOrForPs(Ps $ps)
    {
        $machineIds = ArrayHelper::getColumn($ps->getNotDeletedPsMachines()->all(), 'id');

        $this->andWhere([
            'or',
            [StoreOrderAttemp::column('machine_id') => $machineIds],
            [StoreOrderAttemp::column('machine_id') => null, StoreOrderAttemp::column('ps_id') => $ps->id]
        ]);

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function forPsUser(User $user)
    {
        $ps = ArrayHelper::first($user->ps);

        if (!$ps) {
            $this->andWhere("0 = 1");
            return $this;
        }

        $this->andWhere([StoreOrderAttemp::column('ps_id') => $ps->id]);
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function forClientUser(User $user)
    {
        $this->joinWith(['order' => function (StoreOrderQuery $query) use ($user) {
            $query->forUser($user);
        }], false);
        return $this;
    }

    /**
     * @param string|string[] $status
     * @return $this
     */
    public function inStatus($status)
    {
        $this->andWhere([StoreOrderAttemp::column('status') => $status]);
        return $this;
    }

    /**
     * @param string|string[] $status
     * @return $this
     */
    public function notInStatus($status)
    {
        $this->andWhere(['not in', StoreOrderAttemp::column('status'), (array)$status]);
        return $this;
    }


    /**
     * @param StoreOrder $order
     * @return $this
     */
    public function forOrder(StoreOrder $order)
    {
        $this->andWhere([StoreOrderAttemp::column('order_id') => $order->id]);
        return $this;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @return $this
     */
    public function other(StoreOrderAttemp $attemp)
    {
        $this->andWhere(['!=', StoreOrderAttemp::column('id'), $attemp->id]);
        return $this;
    }

    /**
     * @param string|string[] $statuses
     * @return $this
     */
    public function forPaymentStatuses($statuses)
    {
        $this->joinWith(['order' => function (StoreOrderQuery $query) use ($statuses) {
            $query->forPaymentStatuses($statuses);
        }], false);
        return $this;
    }

    public function wasPayedOrQuote()
    {
        $this->joinWith(['order.primaryPaymentInvoice.payments.paymentDetailOperations']);
        $this->andWhere(['or',
            'payment_detail_operation.created_at is not null',
            'store_order_attemp.preorder_id is not null'
        ]);
        return $this;
    }

    public function wasPayed()
    {
        $this->joinWith(['order' => function (StoreOrderQuery $storeOrderQuery) {
            $storeOrderQuery->joinWith(['primaryPaymentInvoice' => function (PaymentInvoiceQuery $paymentInvoiceQuery) {
                $paymentInvoiceQuery->wasPayed();
            }], false);
        }], false);
        return $this;
    }

    public function notForPaymentStatuses($statuses)
    {
        $this->joinWith(['order' => function (StoreOrderQuery $query) use ($statuses) {
            $query->notForPaymentStatuses($statuses);
        }], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function notTest()
    {
        $this->joinWith(['order' => function (StoreOrderQuery $query) {
            $query->notTest();
        }], false);
        return $this;
    }

}