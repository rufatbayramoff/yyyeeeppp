<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.11.18
 * Time: 15:56
 */

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\models\PaymentTransaction;
use common\models\User;

class PaymentTransactionQuery extends BaseActiveQuery
{

    public function bankPayoutAuthorized()
    {
        return $this->andWhere(['status' => PaymentTransaction::STATUS_AUTHORIZED, 'vendor'=>PaymentTransaction::VENDOR_BANK_PAYOUT]);
    }

    public function sort()
    {
        return $this->orderBy('payment_transaction.created_at');
    }

    public function forUser(User $user): PaymentTransactionQuery
    {
        $this->andWhere(['user_id' => $user->id]);
        return $this;
    }
}