<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.11.18
 * Time: 11:57
 */

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\models\PaymentTransactionRefund;

class PaymentTransactionRefundQuery extends BaseActiveQuery
{
    public function item($item)
    {
        return $this->andWhere(['payment_transaction_refund.item' => $item]);
    }

    public function type($type)
    {
        return $this->andWhere(['payment_transaction_refund.refund_type' => $type]);
    }

    public function approved()
    {
        return $this->andWhere(['payment_transaction_refund.status' => PaymentTransactionRefund::STATUS_APPROVED]);
    }

    public function printForPs()
    {
        return $this->andWhere(['payment_transaction_refund.refund_type' => 'ps']);
    }

    public function printForTs()
    {
        return $this->andWhere(['payment_transaction_refund.refund_type' => 'ts']);
    }
}