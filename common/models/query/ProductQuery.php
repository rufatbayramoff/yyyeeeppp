<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 16:07
 */

namespace common\models\query;

use common\components\ActiveQuery;
use common\components\BaseActiveQuery;
use common\models\Company;
use common\models\User;
use common\modules\product\interfaces\ProductInterface;
use common\traits\db\ActiveScopeTrait;

class ProductQuery extends BaseActiveQuery
{
    use ActiveScopeTrait;

    public function active()
    {
        $this->joinWith('productCommon');
        $this->andWhere(['product_common.is_active' => 1]);
        return $this;
    }

    public function published()
    {
        $this->active();
        $this->joinWith('productCommon');
        $this->andWhere(['product_common.product_status' => ProductInterface::PUBLIC_STATUSES]);
        return $this;
    }

    public function moderated()
    {
        $this->joinWith('productCommon.company');
        $this->andWhere(['or',
            ['ps.moderator_status' => [Company::MSTATUS_CHECKED, Company::MSTATUS_UPDATED]],
            ['ps.is_backend' => 1],
            ['ps.is_deleted' => Company::IS_NOT_DELETED_FLAG],
            ]);
        return $this;
    }

    public function isAvailableInCatalog()
    {
        return $this
            ->published()
            ->moderated();
    }

    public function company(?Company $company)
    {
        if ($company) {
            $this->joinWith('productCommon');
            $this->andWhere(['product_common.company_id' => $company->id]);
        }
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function ownedByUser(User $user)
    {
        $this->joinWith('productCommon');
        $this->andWhere(['product_common.user_id' => $user->id]);
        return $this;
    }

}