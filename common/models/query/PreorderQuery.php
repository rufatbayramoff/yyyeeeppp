<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\components\BaseActiveQuery;
use common\components\DateHelper;
use common\models\base\StoreOrderAttemp;
use common\models\Company;
use common\models\CompanyService;
use common\models\PaymentInvoice;
use common\models\Preorder;
use common\models\Ps;
use common\models\StoreOrder;
use common\models\User;

class PreorderQuery extends BaseActiveQuery
{
    /**
     * @return $this
     */
    public function waitComfirm()
    {
        $this->andWhere([Preorder::column('status') => Preorder::STATUS_WAIT_CONFIRM]);
        return $this;
    }

    /**
     * @return $this
     */
    public function new()
    {
        $this->andWhere([Preorder::column('status') => Preorder::STATUS_NEW]);
        return $this;
    }

    public function submitted()
    {
        $this->andWhere([Preorder::column('status') => [Preorder::STATUS_WAIT_CONFIRM]]);
        return $this;
    }

    public function activeTab()
    {
        $this->andWhere([Preorder::column('status') => [Preorder::STATUS_NEW, Preorder::STATUS_WAIT_CONFIRM]]);
        return $this;
    }

    /**
     * @return $this
     */
    public function rejected(): self
    {
        $this->andWhere([Preorder::column('status') => [Preorder::STATUS_REJECTED_BY_CLIENT, Preorder::STATUS_REJECT_CONFIRM_BY_COMPANY]]);
        return $this;
    }

    public function inCancelProcess()
    {
        $this->andWhere([Preorder::column('status') => [
            Preorder::STATUS_CANCELED_BY_CLIENT,
            Preorder::STATUS_REJECT_CONFIRM_BY_COMPANY,
            Preorder::STATUS_REJECTED_BY_COMPANY,
        ]]);
    }


    public function draft(): self
    {
        $this->andWhere([Preorder::column('status') => Preorder::STATUS_DRAFT]);
        return $this;
    }

    public function waitingForOffer()
    {
        $this->andWhere([Preorder::column('status') => [Preorder::STATUS_NEW, Preorder::STATUS_DRAFT]]);
        return $this;
    }

    public function inNewProcess()
    {
        $this->andWhere([Preorder::column('status') => [Preorder::STATUS_NEW, Preorder::STATUS_DRAFT, Preorder::STATUS_WAIT_CONFIRM, Preorder::STATUS_REJECTED_BY_CLIENT]]);
        return $this;
    }

    /**
     * @return $this
     */
    public function newOrWaitConfirm()
    {
        $this->andWhere([Preorder::column('status') => [Preorder::STATUS_NEW, Preorder::STATUS_WAIT_CONFIRM]]);
        return $this;
    }

    public function isInterceptionOrForMachineIdOrCompany($company)
    {
        $this->andWhere([
                'or',
                ['preorder.ps_id' => $company->id],
                ['preorder.is_interception' => 1]
            ]);
        return $this;
    }

    public function isInterception()
    {
        $this->andWhere([Preorder::column('is_interception') => 1]);
        return $this;
    }

    public function isNotInterception()
    {
        $this->andWhere([Preorder::column('is_interception') => 0]);
        return $this;
    }

    /**
     * @param Ps $ps
     * @return $this
     */
    public function forPs(Ps $ps)
    {
        $this->andWhere([Preorder::column('ps_id') => $ps->id]);

        return $this;
    }

    public function notPaid()
    {
        $this->joinWith('storeOrder.primaryPaymentInvoice')
            ->andWhere("payment_invoice.status = '" . PaymentInvoice::STATUS_NEW . "' or payment_invoice.status is null");
        return $this;
    }

    public function withoutCompany(?Company $company)
    {
        if (!$company) {
            return $this;
        }
        return $this->andWhere(['!=', Preorder::column('ps_id'), $company->id]);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function forCustomer(User $user)
    {
        $this->andWhere([Preorder::column('user_id') => $user->id]);
        return $this;
    }

    public function notDraft()
    {
        return $this->andWhere(['!=', Preorder::column('status'), Preorder::STATUS_DRAFT]);
    }

    public function notAccepted()
    {
        return $this->andWhere(['!=', Preorder::column('status'), Preorder::STATUS_ACCEPTED]);
    }

    public function alarmPreorder300()
    {
        return $this->andWhere(
            [
                'and',
                ['>', 'budget', 300],
                ['status' => [Preorder::STATUS_NEW, Preorder::STATUS_REJECTED_BY_CLIENT, Preorder::STATUS_WAIT_CONFIRM]],
                [
                    '<',
                    'status_updated_at',
                    DateHelper::subNow('P1D')
                ],
                [
                    '>',
                    'status_updated_at',
                    DateHelper::subNow('P7D')
                ],
            ]
        );
    }

    public function serviceType($type)
    {
        return $this->joinWith('service')->andWhere(['company_service.type' => $type]);
    }
}