<?php


namespace common\models\query;


use common\components\BaseActiveQuery;
use common\modules\product\interfaces\ProductInterface;

class ProductCommonQuery extends BaseActiveQuery
{
    public function active()
    {
        $this->andWhere(['product_common.is_active' => 1]);
        return $this;
    }

    public function statusPublished()
    {
        $this->andWhere(['product_common.product_status' => ProductInterface::PUBLIC_STATUSES]);
        return $this;
    }

    public function activeCompany()
    {
        $this->joinWith(['company' => function(PsQuery $psQuery){
            $psQuery->active();
        }], false);
        return $this;
    }

    public function withCategory()
    {
        $this->andWhere(['IS NOT','product_common.category_id', null]);
        return $this;
    }
}