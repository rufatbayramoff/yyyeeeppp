<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\models\SiteHelp;
use common\models\SiteHelpCategory;

/**
 * Class SiteHelpQuery
 * @package common\models\query
 */
class SiteHelpQuery extends \common\components\BaseActiveQuery
{
    /**
     * @return SiteHelpQuery
     */
    public function popular() : SiteHelpQuery
    {
        return $this->andWhere([SiteHelp::column('is_popular') => 1]);
    }

    /**
     * @return SiteHelpQuery
     */
    public function active() : SiteHelpQuery
    {
        return $this->andWhere([SiteHelp::column('is_active') => 1]);
    }

    /**
     * @param SiteHelpCategory $category
     * @return SiteHelpQuery
     */
    public function forCategory(SiteHelpCategory $category) : SiteHelpQuery
    {
        return $this->andWhere([SiteHelp::column('category_id') => $category->id]);
    }

    /**
     * @param array $categoriesIds
     * @return SiteHelpQuery
     */
    public function forCategoriesIds(array $categoriesIds) : SiteHelpQuery
    {
        return $this->andWhere([SiteHelp::column('category_id') => $categoriesIds]);
    }

    /**
     * @return SiteHelpQuery
     */
    public function orderByProprity()  : SiteHelpQuery
    {
        return $this->orderBy([
            SiteHelp::column('priority') => SORT_ASC,
            SiteHelp::column('id') => SORT_ASC
        ]);
    }

    /**
     * @return SiteHelpQuery
     */
    public function orderByClicks() : SiteHelpQuery
    {
        return $this->orderBy([SiteHelp::column('clicks') => SORT_DESC]);
    }

    /**
     * @param $tag
     * @return SiteHelpQuery
     */
    public function forTag($tag): SiteHelpQuery
    {
        return $this->andWhere(['like', SiteHelp::column('tags'), $tag]);
    }
}