<?php
namespace common\models\query;


use common\components\BaseActiveQuery;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\models\WikiMaterial;

class WikiMaterialQuery extends BaseActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere([WikiMaterial::column('is_active') => 1]);
        return $this;
    }

    public function notWidget()
    {
        $this->andWhere([WikiMaterial::column('show_widget') => 0]);
        return $this;
    }

    public function showWidget()
    {
        $this->orWhere(['and',
            [WikiMaterial::column('show_widget') => 1],
            [WikiMaterial::column('is_active') => 1]
        ]);
        return $this;
    }
}