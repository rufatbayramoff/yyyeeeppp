<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\components\BaseActiveQuery;
use common\components\exceptions\AssertHelper;
use common\models\MsgFile;
use common\models\MsgTopic;
use common\models\User;

/**
 * Class MsgFileQuery
 * @package common\models\query
 */
class MsgFileQuery extends BaseActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function forId($id)
    {
        AssertHelper::assert($id);
        $this->andWhere([MsgFile::column('id') => $id]);
        return $this;
    }

    /**
     * @return $this
     */
    public function notExpired()
    {
        $this->andWhere(['>=', MsgFile::column('created_at'), dbexpr('NOW() - INTERVAL 2 MONTH')]);
        return $this;
    }

    /**
     * @param MsgTopic $topic
     * @return $this
     */
    public function forTopic(MsgTopic $topic)
    {
        $this->andWhere([MsgFile::column('topic_id') => $topic->id]);
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function forUser(User $user)
    {
        $this->andWhere([MsgFile::column('user_id') => $user->id]);
        return $this;
    }

    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere([MsgFile::column('status') => MsgFile::STATUS_ACTIVE]);
        return $this;
    }
}