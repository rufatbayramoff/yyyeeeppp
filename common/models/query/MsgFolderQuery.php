<?php


namespace common\models\query;


use common\components\BaseActiveQuery;
use common\models\MsgFolder;

class MsgFolderQuery extends BaseActiveQuery
{
    public function disablePersonal(): MsgFolderQuery
    {
        $this->andWhere(['<>','id',MsgFolder::FOLDER_ID_PERSONAL]);
        return $this;
    }
}