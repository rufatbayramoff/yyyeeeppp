<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.08.18
 * Time: 17:12
 */


namespace common\models\query;

use common\components\ActiveQuery;
use common\models\Company;
use common\models\User;
use common\modules\product\interfaces\ProductInterface;
use common\traits\db\ActiveScopeTrait;

class ProductFileQuery extends ActiveQuery
{
    public function byType($type)
    {
        return $this->andWhere(['type' => $type]);
    }
}