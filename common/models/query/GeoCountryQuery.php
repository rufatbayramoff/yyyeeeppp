<?php
namespace common\models\query;


use common\components\BaseActiveQuery;
use common\models\GeoCountry;

class GeoCountryQuery extends BaseActiveQuery
{
    public function isEuropa(): self
    {
        $this->andWhere(['is_europa' => GeoCountry::IS_EUROPA_ACTIVE]);
        return $this;
    }
}