<?php
/**
 * Created by mitaichik
 */
namespace common\models\query;
use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\UserLocation;
use common\components\ActiveQuery;

/**
 * Class UserLocationQuery
 * @package common\models
 */
class UserLocationQuery extends ActiveQuery
{
    /**
     * Find same location
     * @param UserLocation $userLocation
     * @return $this
     */
    public function same(UserLocation $userLocation)
    {
        AssertHelper::assert($userLocation->user_id, 'User id must be set for location');
        $comparedAttributes = ArrayHelper::removeValues($userLocation->attributes(), ['id', 'user_data']);
        $attributes = $userLocation->getAttributes($comparedAttributes);
        $this->andWhere($attributes);

        if (!$userLocation->isNewRecord) {
            $this->andWhere(['!=', 'id', $userLocation->id]);
        }

        return $this;
    }
}