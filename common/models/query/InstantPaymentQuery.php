<?php

namespace common\models\query;

use common\components\ActiveQuery;
use common\models\Company;
use common\models\InstantPayment;
use common\models\PaymentTransaction;
use common\models\Ps;
use common\models\User;
use Twilio\TwiML\Voice\Pay;

class InstantPaymentQuery extends ActiveQuery
{
    public function onlyWithInvoices()
    {
        $this->joinWith('primaryInvoice');
        $this->andWhere('payment_invoice.uuid is not null');
        return $this;
    }

    public function fromUser(User $user)
    {
        return $this->andWhere(['instant_payment.from_user_id'=>$user->id]);
    }

    public function forCompany(Ps $company)
    {
        return $this->andWhere(['instant_payment.to_user_id'=>$company->user->id]);
    }

    public function waitingForConfirm()
    {
        return $this->andWhere(['instant_payment.status' => InstantPayment::STATUS_WAIT_FOR_CONFIRM]);
    }

    public function authorized()
    {
        return $this->joinWith('primaryInvoice.payments.paymentDetailOperations.paymentDetails.paymentTransaction')
                    ->andWhere(['payment_transaction.status'=>[PaymentTransaction::STATUS_AUTHORIZED, PaymentTransaction::STATUS_REQUIRES_CAPTURE]]);
    }

    public function notPayed()
    {
        return $this->andWhere(['instant_payment.status' => InstantPayment::STATUS_NOT_PAYED]);
    }
}