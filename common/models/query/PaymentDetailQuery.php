<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.08.18
 * Time: 15:51
 */

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\Preorder;
use common\models\Ps;
use common\models\User;
use common\modules\payment\services\PaymentAccountService;

class PaymentDetailQuery extends BaseActiveQuery
{
    public function paymentId($paymentId)
    {
        return $this->joinWith('paymentDetailOperation')->andWhere(['payment_id' => $paymentId]);
    }

    public function joinAccount()
    {
        return $this->joinWith('paymentAccount');
    }

    public function type($type)
    {
        return $this->andWhere(['payment_detail.type' => $type]);
    }

    public function toReserved()
    {
        return $this->joinAccount()->andWhere(['payment_account.type' => PaymentAccount::ACCOUNT_TYPE_RESERVED])->andWhere('payment_detail.amount>0');
    }

    public function fromReserved()
    {
        return $this->joinAccount()->andWhere(['payment_account.type' => PaymentAccount::ACCOUNT_TYPE_RESERVED])->andWhere('payment_detail.amount<0');
    }

    public function fromAuthorize()
    {
        return $this->joinAccount()->andWhere(['payment_account.type' => PaymentAccount::ACCOUNT_TYPE_AUTHORIZE])->andWhere('payment_detail.amount<0');
    }


    public function toEasyPost()
    {
        return $this->joinAccount()->andWhere(['payment_account_id'=>PaymentAccount::ACCOUNT_EASYPOST])->andWhere('payment_detail.amount>0');
    }

    public function toInvoice()
    {
        return $this->joinAccount()->andWhere(['payment_account.type' => PaymentAccount::ACCOUNT_TYPE_INVOICE])->andWhere('payment_detail.amount>0');
    }

    public function notRefund()
    {
        return $this->andWhere(['not in', 'payment_detail.type', [PaymentDetail::TYPE_REFUND, PaymentDetail::TYPE_REFUND_REQUEST]]);
    }

    public function incoming()
    {
        $incomingAccounts = PaymentAccount::getIncomingAccounts();
        return $this->andWhere(['payment_detail.payment_account_id' => $incomingAccounts])->andWhere('payment_detail.amount<0');
    }

    public function outcoming()
    {
        return $this->joinAccount()->andWhere(['payment_account.type' => 'main']);
    }

    public function joinOperation()
    {
        return $this->joinWith('paymentDetailOperation');
    }

    public function sort()
    {
        return $this->joinOperation()->orderBy('payment_detail_operation.created_at, payment_detail.updated_at');
    }
}