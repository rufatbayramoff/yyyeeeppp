<?php


namespace common\models\query;


use common\components\ActiveQuery;
use common\modules\company\components\CompanyServiceInterface;

class CompanyServiceTypeQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function machine(): CompanyServiceTypeQuery
    {
        $this->andWhere(['name' => [
            CompanyServiceInterface::TYPE_PRINTER,
            CompanyServiceInterface::TYPE_CUTTING,
            CompanyServiceInterface::TYPE_CNC
        ]]);
        return $this;
    }

    public function withoutWindow()
    {
        $this->andWhere(['<>','name',CompanyServiceInterface::TYPE_WINDOW]);
        return $this;
    }
}