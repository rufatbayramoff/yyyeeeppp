<?php
/**
 * Created by mitaichik
 */
namespace common\models\query;
use common\components\BaseActiveQuery;
use common\models\StoreUnit;

/**
 * Class StoreUnitQuery
 * @package common\models
 */
class StoreUnitQuery extends BaseActiveQuery
{
    /**
     * Published units
     * @return $this
     */
    public function published()
    {
        $this->joinWith(['model3d' => function(Model3dQuery $model3dQuery){
            $model3dQuery->published()->activeCompany();
        }], false);
        return $this;
    }
}