<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.10.18
 * Time: 15:43
 */

namespace common\models\query;

use common\components\BaseActiveQuery;

class PaymentAccountQuery extends BaseActiveQuery
{
    public function system()
    {
        return $this->andWhere(['<', 'payment_account.user_id', 1000]);
    }

    public function users()
    {
        return $this->andWhere(['>', 'payment_account.user_id', 999]);
    }
}