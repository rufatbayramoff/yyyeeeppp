<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;

use common\models\Company;
use common\models\CompanyService;
use common\models\PrinterMaterial;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\DeliveryType;
use common\models\PrinterTechnology;

use common\components\exceptions\AssertHelper;
use common\models\User;
use common\modules\company\models\CompaniesSearchForm;
use common\traits\db\ActiveScopeTrait;
use common\traits\db\ForUserScopeTrait;
use common\components\ActiveQuery;
use lib\geo\GeoNames;
use lib\geo\models\Location;

/**
 * Class PsQuery
 *
 * @package common\models
 */
class PsQuery extends ActiveQuery
{
    use ActiveScopeTrait;
    use ForUserScopeTrait;

    /**
     * Return ps not excluded from printing
     *
     * @param Ps $ignorePs for this printservice exclude from printing will ignored
     * @return $this
     */
    public function notExcludedFromPrinting(Ps $ignorePs = null)
    {
        if ($ignorePs) {
            AssertHelper::assert(!$ignorePs->isNewRecord, 'Cant use new ps as ignore from excluded');
            $this->andWhere('(ps.is_excluded_from_printing = 0 OR ps.id = :ignorePsId)', ['ignorePsId' => $ignorePs->id]);
        } else {
            $this->andWhere(['ps.is_excluded_from_printing' => 0]);
        }
        return $this;
    }

    public function url($url)
    {
        $this->andWhere(['url' => $url]);
        return $this;
    }

    /**
     * Is success moderated
     *
     * @return $this
     */
    public function moderated()
    {
        $this->andWhere(['ps.moderator_status' => [Ps::MSTATUS_CHECKED, Ps::MSTATUS_UPDATED]]);
        return $this;
    }

    public function active()
    {
        $this->andWhere(['ps.moderator_status' => [Company::MSTATUS_CHECKED, Company::MSTATUS_UPDATED, Company::MSTATUS_DRAFT]]);
        $this->andWhere(['ps.is_deleted' => Company::IS_NOT_DELETED_FLAG]);
        $this->joinWith('user');
        $this->andWhere(['user.status' => [User::STATUS_UNCONFIRMED, User::STATUS_ACTIVE]]);
        return $this;
    }

    public function hasActiveCnc()
    {
        $this->joinWith(['companyServices' => function (CompanyServiceQuery $companyServiceQuery) {
            $companyServiceQuery->onlyCnc()->inCatalog();
        }]);
        return $this;
    }

    /**
     * show PS with active printers only
     *
     * @return $this
     */
    public function hasActivePrinters()
    {
        $this->joinWith('psPrinters.companyService');
        $this->andWhere(['company_service.visibility' => CompanyService::VISIBILITY_EVERYWHERE]);
        $this->andWhere(['company_service.moderator_status' => CompanyService::moderatedStatusList()]);
        return $this;
    }

    /**
     * @param PrinterTechnology $technology
     * @return $this
     */
    public function filterTechnology(PrinterTechnology $technology)
    {
        $this->joinWith('psPrinters.printer');
        $this->andWhere(['printer.technology_id' => $technology->id]);
        return $this;
    }

    /**
     * @param $usage
     * @return $this
     */
    public function filterUsage($usage)
    {
        //@TODO replace to repository
        $materialIds = PrinterMaterial::find()->joinWith('group')
            ->where(['printer_material_group.code' => $usage['groups']])->column();

        $this->joinWith('psPrinters.printer.printerToMaterials');
        $this->andWhere(['printer_to_material.material_id' => $materialIds]);

        return $this;
    }

    /**
     * @param PrinterMaterial $material
     * @return $this
     */
    public function filterMaterial(PrinterMaterial $material)
    {
        $this->joinWith('psPrinters.psPrinterMaterials');
        $this->andWhere(['ps_printer_material.material_id' => $material->id, 'ps_printer_material.is_active' => 1]);
        return $this;
    }

    public function filterLocation(Location $location)
    {
        $conditions = [
            'psPrinters.psPrinterDeliveries d' => function (ActiveQuery $query) {
                $query->andWhere(['d.delivery_type_id' => [DeliveryType::STANDART_ID]]);
            }
        ];
        if ($location->country) {
            $countryIso                          = $location->country;
            $conditions['psPrinters.location l'] = function (ActiveQuery $query) use ($countryIso) {
                $query->andWhere(['l.country_id' => GeoNames::getCountryByISO($countryIso)->id]);
            };
        }
        $this->joinWith($conditions, false);

        return $this;
    }

    public function findBySearchForm(CompaniesSearchForm $searchForm)
    {
        $this->joinWith('companyServices', false);
        $this->joinWith('user.products', false);
        $this->andWhere(['or', ['not', ['company_service.id' => null]], ['not', ['product.uuid' => null]]]);

        if (!empty($searchForm->search)) {
            $this->andWhere([
                'or',
                ['like', 'product.title', $searchForm->search],
                ['like', 'company_service.title', $searchForm->search],
                ['like', 'company_service.description', $searchForm->search],
                ['like', 'ps.description', $searchForm->search],
                ['like', 'ps.title', $searchForm->search]
            ]);
        }

        $this->groupBy('ps.id');
        return $this;
    }

    public function notDeleted()
    {
        $this->andWhere(['ps.is_deleted' => Company::IS_NOT_DELETED_FLAG]);
        return $this;
    }
}