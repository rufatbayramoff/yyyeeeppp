<?php
namespace common\models\query;

use common\models\SystemLangSource;
use common\components\ActiveQuery;

/**
 * Class SystemLangSourceQuery
 *
 * @package common\models
 */
class SystemLangSourceQuery extends ActiveQuery
{
    public function isChecked()
    {
        $this->andWhere([SystemLangSource::column('is_checked') => 1]);
        return $this;
    }
}

