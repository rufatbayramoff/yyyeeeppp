<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\models\base\StoreUnit;
use common\models\Ps;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderReview;
use common\components\ActiveQuery;

class StoreOrderReviewQuery extends ActiveQuery
{
    /**
     * @param Ps $ps
     * @return $this
     */
    public function forPs(Ps $ps)
    {
        $this->andWhere(['ps_id' => $ps->id]);
        return $this;
    }

    /**
     * @param StoreOrderAttemp $orderJob
     * @return $this
     */
    public function forAttemp(StoreOrderAttemp $orderJob)
    {
        $this->andWhere(['order_id' => $orderJob->order->id]);
        return $this;
    }

    /**
     * @param StoreOrderAttemp $orderJob
     * @return $this
     */
    public function forAttempPs(StoreOrderAttemp $orderJob)
    {
        $this->andWhere(['ps_id' => $orderJob->ps_id]);
        return $this;
    }

    /**
     * @param StoreUnit $unit
     * @return $this
     */
    public function forStoreUnit(StoreUnit $unit)
    {
        $this->andWhere(['store_unit_id' => $unit->id]);
        return $this;
    }

    /**
     * @return $this
     */
    public function isCanShow()
    {
        $this->andWhere(['status' => [StoreOrderReview::STATUS_MODERATED]]);
        return $this;
    }

    /**
     * @return $this
     */
    public function notModreated()
    {
        $this->andWhere(['status' => StoreOrderReview::STATUS_NEW]);
        return $this;
    }

    /**
     * @return $this
     */
    public function newAnswer()
    {
        $this->andWhere([
            'and',
            ['=', 'verified_answer_status',  StoreOrderReview::ANSWER_STATUS_NEW],
            ['is not', 'answer',  dbexpr('NULL')],
            ['<>', 'answer',  ''],
        ]);
        return $this;
    }
}