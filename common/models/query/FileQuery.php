<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;


use common\components\BaseActiveQuery;
use common\models\File;
use common\models\User;
use common\models\UserSession;
use DateTime;
use DateTimeZone;

/**
 * Class FileQuery
 * @package common\models\query
 */
class FileQuery extends BaseActiveQuery
{
    /**
     * @param string $uuid
     * @return FileQuery
     */
    public function uuid(string $uuid) : FileQuery
    {
        return $this->andWhere([ File::column('uuid') => $uuid]);
    }

    /**
     * @param string $class
     * @param string $linkField
     * @return FileQuery
     */
    public function ownerClass(string $class, string $linkField) : FileQuery
    {
        return $this->andWhere([
            File::column('ownerClass') => $class,
            File::column('ownerField') => $linkField
        ]);
    }

    /**
     * @return FileQuery
     */
    public function notExpired() : FileQuery
    {
        $nowDate = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        return $this->andWhere(['>=', File::column('expire'), $nowDate]);
    }

    /**
     * @param string|string[] $md5
     * @return FileQuery
     */
    public function md5($md5) : FileQuery
    {
        return $this->andWhere([File::column('md5sum') => $md5]);
    }

    /**
     * @return FileQuery
     */
    public function active() : FileQuery
    {
        return $this->andWhere([File::column('status') => File::STATUS_ACTIVE]);
    }

    /**
     * @param int|int[] $id
     * @return FileQuery
     */
    public function id($id) : FileQuery
    {
        return $this->andWhere([File::column('id') => $id]);
    }

    /**
     * @param $pathPart
     * @return FileQuery
     */
    public function pathLike($pathPart) : FileQuery
    {
        return $this->andWhere(['like', File::column('path'), $pathPart]);
    }

    /**
     * @return FileQuery
     */
    public function orderIdDesc() : FileQuery
    {
        return $this->orderBy([File::column('id') => SORT_DESC]);
    }


    public function user(?User $user)
    {
        if (!$user) {
            return $this->andWhere('file.user_id is null');
        }
        return $this->andWhere(['file.user_id' => $user->id]);
    }

    public function userSession(?UserSession $userSession)
    {
        if (!$userSession) {
            return $this->andWhere('file.user_session_id is null');
        }
        return $this->andWhere(['file.user_session_id' => $userSession->id]);
    }
}