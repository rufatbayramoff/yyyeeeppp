<?php
/**
 * Created by mitaichik
 */

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\interfaces\Model3dBasePartInterface;
use common\models\UserSession;
use common\modules\product\interfaces\ProductInterface;
use common\models\Model3d;
use common\models\StoreUnit;
use common\models\User;
use common\components\ActiveQuery;

/**
 * Class MyQuery
 *
 * @package common\models
 */
class Model3dQuery extends BaseActiveQuery
{

    /**
     * published by user (but no checking for moderator)
     *
     * @return $this
     */
    public function published()
    {
        $this->joinWith('productCommon');
        $this->productStatus([ProductInterface::STATUS_PUBLISHED_PUBLIC, ProductInterface::STATUS_PUBLISH_PENDING, ProductInterface::STATUS_PUBLISHED_UPDATED]);
        return $this;
    }

    public function activeCompany()
    {
        $this->joinWith(['productCommon.company' => function(PsQuery $psQuery){
            $psQuery->active();
        }], false);
        return $this;
    }

    public function productStatus(array $productStatusList)
    {
        $this->joinWith('productCommon');
        $this->andWhere(['product_status' => $productStatusList]);
        return $this;
    }


    /**
     * published in store by user and moderator
     * should not be used with myStore
     *
     * @param User $user
     * @return $this
     */
    public function storePublished(User $user = null)
    {
        $this->joinWith('productCommon');
        $this->andWhere(
            [
                'product_common.product_status' => ProductInterface::STATUS_PUBLISHED_PUBLIC,
            ]
        )->myStore($user->id ?? null);
        return $this;
    }

    public function active()
    {
        $this->joinWith('productCommon');
        return $this->andWhere(['product_common.is_active' => 1]);
    }

    public function hasActiveModel3dParts()
    {
        $this->joinWith('model3dParts');
        $this->andWhere('model3d_part.id is not null');
        $this->andWhere(['model3d_part.user_status' => Model3dBasePartInterface::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * @param $userId
     * @return $this
     */
    public function myStore($userId)
    {
        $this->with(
            'model3dParts',
            'model3dParts.file',
            'coverFile',
            'model3dTags',
            'model3dTags.tag'
        );
        $this->joinWith('storeUnit');
        $this->joinWith('productCommon');
        $this->andWhere(['product_common.is_active' => 1]);

        if ($userId) {
            $this->andFilterWhere(
                [
                    'product_common.user_id' => $userId,
                ]
            );
        }
        return $this;
    }

    public function user(?User $user)
    {
        $this->joinWith('productCommon');
        if (!$user) {
            return $this->andWhere('product_common.user_id is null');
        }
        return $this->andWhere(['product_common.user_id' => $user->id]);
    }

    public function userSession(?UserSession $userSession)
    {
        if (!$userSession) {
            return $this->andWhere('model3d.user_session_id is null');
        }
        return $this->andWhere(['model3d.user_session_id' => $userSession->id]);
    }


    /**
     * @param int[] $modeIds
     * @return $this
     */
    public function excludeIds($modeIds)
    {
        if ($modeIds) {
            $this->andWhere(['not in', Model3d::column('id'), $modeIds]);
        }
        return $this;
    }
}