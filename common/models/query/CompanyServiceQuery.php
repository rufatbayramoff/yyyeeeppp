<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.07.17
 * Time: 14:43
 */

namespace common\models\query;

use common\components\ActiveQuery;
use common\models\CompanyService;
use common\models\Ps;

class CompanyServiceQuery extends ActiveQuery
{
    /**
     * @param string $type
     * @return CompanyServiceQuery
     */
    public function byType($type) : CompanyServiceQuery
    {
        if($type){
            $this->andWhere([CompanyService::column('type') =>$type]);
        }
        return $this;
    }

    /**
     * @return CompanyServiceQuery
     */
    public function onlyCnc() : CompanyServiceQuery
    {
        return $this->byType(CompanyService::TYPE_CNC);
    }

    public function notPsPrinters() : CompanyServiceQuery
    {
        return $this->andWhere(['NOT', [CompanyService::column('type') => CompanyService::TYPE_PRINTER]]);
        //return $this->byType([CompanyService::TYPE_CNC, CompanyService::TYPE_SERVICE, CompanyService::TYPE_DESIGNER, CompanyService::TYPE_WINDOW]);
    }

    /**
     * @param string $visability
     * @return CompanyServiceQuery
     */
    public function byVisability(string $visability) : CompanyServiceQuery
    {
        $this->andWhere([CompanyService::column('visibility') => $visability]);
        return $this;
    }

    /**
     * @return CompanyServiceQuery
     */
    public function visibleEverywhere() : CompanyServiceQuery
    {
        return $this->byVisability(CompanyService::VISIBILITY_EVERYWHERE);
    }

    /**
     * @return CompanyServiceQuery
     */
    public function moderated() : CompanyServiceQuery
    {
        $this->andWhere([CompanyService::column('moderator_status') =>CompanyService::moderatedStatusList()]);
        return $this;
    }

    /**
     * @param string|string[] $status
     * @return CompanyServiceQuery
     */
    public function moderatorStatus($status) : CompanyServiceQuery
    {
        $this->andWhere([CompanyService::column('moderator_status') => $status]);
        return $this;
    }

    public function inCatalog()
    {
        $this->moderatorStatus(CompanyService::moderatedStatusList())->visibleEverywhere()->notDeleted();
        return $this;
    }

    public function activeCompany()
    {
        $this->joinWith(['company' => function (PsQuery $companyQuery) {
            $companyQuery->active();
        }], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function needModeration()
    {
        $this->moderatorStatus([
            CompanyService::MODERATOR_STATUS_PENDING,
            CompanyService::MODERATOR_STATUS_APPROVED_REVIEWING,
            CompanyService::MODERATOR_STATUS_REJECTED_REVIEWING,
            CompanyService::MODERATOR_STATUS_UNAVAILABLE_REVIEWING,
        ]);
        return $this;
    }

    public function notDeleted()
    {
        $this->andWhere([CompanyService::column('is_deleted') => 0]);
        return $this;
    }
    public function forCompany(Ps $company)
    {
        $this->andWhere([CompanyService::column('ps_id') => $company->id]);
        return $this;
    }

    /**
     * @param $categoryIds
     * @return $this
     */
    public function inCategory($categoryIds)
    {
        $this->andWhere([CompanyService::column('category_id') => $categoryIds]);
        return $this;
    }

    public function certification()
    {
        $this->notDeleted();
        $this->andWhere([CompanyService::column('certification') => CompanyService::CERT_TYPE_VERIFIED]);
        return $this;
    }

}