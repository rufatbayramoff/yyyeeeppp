<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.10.18
 * Time: 14:36
 */

namespace common\models\query;

use common\components\BaseActiveQuery;

class PaymentTransactionHistoryQuery extends BaseActiveQuery
{
    public function sort()
    {
        return $this->orderBy('created_at, id');
    }
}