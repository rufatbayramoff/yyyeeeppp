<?php


namespace common\models\query;

use common\components\BaseActiveQuery;
use common\traits\db\ActiveScopeTrait;

/**
 * Class PsPrinterQuery
 *
 * @package common\models
 */
class PsPrinterMaterialQuery extends BaseActiveQuery
{
    public function active()
    {
        $this->andWhere(['ps_printer_material.is_active' => 1]);
        return $this;
    }
}