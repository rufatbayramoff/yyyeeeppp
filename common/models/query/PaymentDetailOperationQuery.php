<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.10.18
 * Time: 10:14
 */

namespace common\models\query;

use common\components\BaseActiveQuery;
use common\models\PaymentAccount;

class PaymentDetailOperationQuery extends BaseActiveQuery
{
    public function sort()
    {
        return $this->joinWith('paymentDetails')->orderBy('created_at, payment_detail.id');
    }

    public function inReservedStatus()
    {
        return $this->joinWith('paymentAccounts')->andWhere(['payment_account.type'=>PaymentAccount::ACCOUNT_TYPE_RESERVED]);
    }

    public function easyPostTreatstock()
    {
        return $this->joinWith('paymentAccounts')->andWhere(['payment_account.id' => PaymentAccount::ACCOUNT_TREATSTOCK_EASYPOST]);
    }

    public function easyPostMain()
    {
        return $this->joinWith('paymentAccounts')->andWhere(['payment_account.id' => PaymentAccount::ACCOUNT_EASYPOST]);
    }
}