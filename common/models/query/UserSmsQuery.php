<?php
/**
 * Created by mitaichik
 */
namespace common\models\query;
use common\components\BaseActiveQuery;
use common\models\UserSms;

/**
 * Class UserSmsQuery
 * @package common\models
 */
class UserSmsQuery extends BaseActiveQuery
{
    /**
     * For phone number
     * @param string $phoneNumber Phone number (in phone format, like 79600123123)
     * @return $this
     */
    public function forPhoneNumber($phoneNumber)
    {
        $this->andWhere(['phone' => UserSms::convertNumberToE164($phoneNumber)]);
        return $this;
    }
}