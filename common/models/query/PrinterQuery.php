<?php
/**
 * Created by mitaichik
 */
namespace common\models\query;
use common\components\BaseActiveQuery;
use common\traits\db\ActiveScopeTrait;

/**
 * Class PrinterQuery
 * @package common\models
 */
class PrinterQuery extends BaseActiveQuery
{
    use ActiveScopeTrait;
}