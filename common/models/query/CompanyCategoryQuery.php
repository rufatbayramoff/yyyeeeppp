<?php


namespace common\models\query;


use common\components\ActiveQuery;
use common\models\Company;
use common\models\CompanyCategory;

class CompanyCategoryQuery extends ActiveQuery
{
    public function forCompany(Company $company): CompanyCategoryQuery
    {
        $this->andWhere([CompanyCategory::column('company_id') => $company->id]);
        return $this;
    }
}