<?php

namespace common\models;

use common\components\UuidHelper;

/**
 * Class NotifyPopup
 * @package common\models
 */
class NotifyPopup extends base\NotifyPopup
{
    public const CODE_NEW_ORDER = 'newOrder';

    public static function generateUid(): string
    {
        do {
            $uuid = strtoupper(UuidHelper::generateUuid(4));
        } while (static::find()->where(['uid' => $uuid])->withoutStaticCache()->one());

        return $uuid;
    }
}