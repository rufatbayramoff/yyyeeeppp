<?php

namespace common\models;

use common\components\DateHelper;
use yii\base\BaseObject;

/**
 * Class FileExpireHash
 * @package common\models
 */
class FileDownloadHash extends BaseObject
{
    public $file_id;
    public $start_date;
    public $end_date;
    public $hash;

    public const STORAGE_KEY = 'file_download_hash';


    public static function findByHash($hash): ?FileDownloadHash
    {
        $fileDownloadHash = new FileDownloadHash();
        $data             = \Yii::$app->get('redis')->executeCommand('GET', [
            self::STORAGE_KEY . ':' . $hash,
        ]);
        if (!$data) {
            return null;
        }
        $data                         = json_decode($data, true);
        $fileDownloadHash->file_id    = $data['file_id'];
        $fileDownloadHash->start_date = $data['start_date'];
        $fileDownloadHash->end_date   = $data['end_date'];
        $fileDownloadHash->hash       = $data['hash'];
        return $fileDownloadHash;
    }

    public function safeSave()
    {
        \Yii::$app->get('redis')->executeCommand('SET', [
            self::STORAGE_KEY . ':' . $this->hash,
            json_encode($this),
            'EX',
            DateHelper::strtotimeUtc($this->end_date) > time() ? DateHelper::strtotimeUtc($this->end_date) - time() : 60 * 10
        ]);
    }

    public function getFile(): ?File
    {
        return File::findOne(['id' => $this->file_id]);
    }

}