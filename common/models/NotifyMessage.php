<?php
namespace common\models;

use Yii;

/**
 * Edit this file
 * This is the model class for table "notify_message".
 *
 * @property User $user
 */
class NotifyMessage extends \common\models\base\NotifyMessage
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}