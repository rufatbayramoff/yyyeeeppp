<?php

namespace common\models;

/**
 * Class CsWindowProfile
 * @package common\models
 */
class CsWindowProfile extends \common\models\base\CsWindowProfile
{
    public $isDeleted = false;
}