<?php
/**
 *
 * @author Nabi <n.ibatulin@treatstock.com>
 */

namespace common\models;

/**
 * Class PaymentBankInvoiceItem
 *
 * @package common\models
 */
class PaymentBankInvoiceItem extends \common\models\base\PaymentBankInvoiceItem
{
    /**
     * prices measure by quantity
     */
    const MEASURE_QUANTITY = 'quantity';

    /**
     * if prices based per hour work
     */
    const MEASURE_HOURS = 'hours';

    /**
     * like quantity
     */
    const MEASURE_AMOUNT = 'amount';

    /**
     * usual item position with measure, qty and price info
     */
    const TYPE_POSITION = 'position';

    /**
     * all type of additional prices, like taxes, shipping, and etc.
     * these types are under invoice positions
     */
    const TYPE_PRICE = 'price';

    /**
     * all type of price which should be minused from total price
     */
    const TYPE_DISCOUNT = 'discount';

    /**
     * total price item type.
     */
    const TYPE_TOTAL_PRICE = 'total_price';



    /**
     *
     * get item type based on pricer type
     *
     * @param $pricerType
     * @return string
     */
    public function getItemTypeByPricerType($pricerType)
    {
        switch ($pricerType->operation) {
            case '-':
                $itemType = PaymentBankInvoiceItem::TYPE_DISCOUNT;
                break;
            case '.':
                $itemType = PaymentBankInvoiceItem::TYPE_TOTAL_PRICE;
                break;
            default:
                $itemType = PaymentBankInvoiceItem::TYPE_PRICE;
                break;
        }
        return $itemType;
    }
}