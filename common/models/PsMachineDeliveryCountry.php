<?php

namespace common\models;

/**
 * Class PsMachineDeliveryCountry
 * @package common\models
 */
class PsMachineDeliveryCountry extends \common\models\base\PsMachineDeliveryCountry
{

    /**
     * @param $countryId
     * @param $price
     * @param $deliveryId
     * @return static
     */
    public static function create(
        $countryId,
        $price,
        $deliveryId
    ): self
    {
        $model = new self;
        $model->geo_country_id = $countryId;
        $model->price = $price;
        $model->ps_machine_delivery_id = $deliveryId;
        return $model;
    }
}