<?php

namespace common\models;

/**
 * Class DynamicFieldCategory
 *
 * @property \common\models\DynamicField $dynamicField
 * @package common\models
 */
class DynamicFieldCategory extends \common\models\base\DynamicFieldCategory
{
    /**
     * @var string Type params as a string
     */
    public $typeParamsString;

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getDynamicField()
    {
        return $this->getFieldCode();
    }

    public function getHasTypeParams()
    {
        if ($this->type_params) {
            return true;
        }
        return false;
    }
}