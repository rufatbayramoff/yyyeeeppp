<?php

namespace common\models;

use common\components\DateHelper;
use common\components\exceptions\BusinessException;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\models\RefundRequestForm;
use common\modules\payment\services\PaymentService;
use common\modules\payment\services\RefundService;
use frontend\models\user\UserFacade;
use lib\money\Money;
use lib\money\MoneyMath;
use Yii;
use yii\base\UserException;
use yii\helpers\ArrayHelper;

/**
 * Class PaymentTransactionRefund
 *
 * @package common\models
 */
class PaymentTransactionRefund extends \common\models\base\PaymentTransactionRefund
{
    public const STATUS_APPROVED = 'approved';
    public const STATUS_NEW      = 'new';
    public const STATUS_CANCELED = 'canceled';

    /**
     * Intermediate status, used while working with a transaction (for example: refund).
     * In case of successful completion goes to "approved"
     */
    public const STATUS_IN_PROGRESS = 'in_progress';
    public const STATUS_REJECTED    = 'rejected';

    public const REFUND_TYPE_PS           = PaymentInvoice::ACCOUNTING_TYPE_PRINT;
    public const REFUND_TYPE_PS_CUTTING   = PaymentInvoice::ACCOUNTING_TYPE_CUTTING;
    public const REFUND_TYPE_MANUFACTURER = PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER;
    public const REFUND_TYPE_ALL          = 'all';
    public const REFUND_TYPE_MANUFACTURER_WITH_PACKAGE = PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2;
    public const REFUND_TYPE_PS_FEE       = PaymentInvoice::ACCOUNTING_TYPE_PS_FEE;
    public const REFUND_TYPE_SHIPPING     = PaymentInvoice::ACCOUNTING_TYPE_SHIPPING;
    public const REFUND_TYPE_FEE          = PaymentInvoice::ACCOUNTING_TYPE_FEE;
    public const REFUND_TYPE_TS           = 'ts';

    /**
     * valid transaction types for refund requests
     *
     * @var array
     */
    public $validTransactionTypes = [PaymentTransaction::TYPE_PAYMENT];

    /**
     * valid transaction status for refund request
     *
     * @var array
     */
    public $validTransactionStatus = [
        PaymentTransaction::STATUS_SETTLED,
        PaymentTransaction::STATUS_SUCCEEDED
    ];

    /**
     * no refund for these transaction vendors
     *
     * @var array
     */
    public $invalidVendors = [PaymentGateway::GATEWAY_THINGIVERSE];

    /**
     * Form checkbox is full refund
     *
     * @var bool
     */
    public $isFullRefund = true;

    protected $_availableTransactionRefundMoney;


    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['isFullRefund'], 'safe'],
            ['amount', 'double'],
            ['amount', 'filter', 'filter' => function ($value) {
                return number_format((float)$value, 2, '.', '');
            }]
        ]);
    }

    public function initWithTransaction(PaymentTransaction $transaction)
    {
        $refundService = \Yii::createObject(RefundService::class);

        $this->created_at           = DateHelper::now();
        $this->payment_invoice_uuid = $transaction->firstPaymentDetail->paymentDetailOperation->payment->paymentInvoice->uuid;
        $this->status               = self::STATUS_IN_PROGRESS;
        $this->transaction_id       = $transaction->id;
        $this->currency             = $transaction->currency;
        $this->amount               = $refundService->availableTransactionRefund($transaction)->getAmount();
        $this->user_id              = Yii::$app->getUser()->id;
    }

    public function isTotalApproved(): bool
    {
        if (!$this->transaction) {
            return false;
        }
        return $this->isApproved() && ($this->getMoneyAmount()->getAmount() == $this->transaction->getMoneyAmount()->getAmount());
    }

    public function isApproved(): bool
    {
        return $this->status === self::STATUS_APPROVED;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function isFullRefund(): bool
    {
        if (!$this->transaction) {
            return false;
        }

        /** @var RefundService $refundService */
        $refundService = \Yii::createObject(RefundService::class);

        return MoneyMath::equal($refundService->availableTransactionRefund($this->transaction), $this->getMoneyCurrencyAmount());
    }

    public function isAllowedFullRefund(): bool
    {
        return true;
    }

    public function load($data, $formName = null)
    {
        if ($formName) {
            $formData = $data[$formName];
        } else {
            $formData = $data[$this->formName()];
        }
        $this->comment        = $formData['comment'] ?? null;
        $this->refund_type    = $formData['refund_type'] ?? null;
        $this->isFullRefund   = $formData['isFullRefund'] ?? $this->isFullRefund;
        $this->transaction_id = $formData['transaction_id'] ?? null;

        if (!$this->isFullRefund) {
            $this->amount = $formData['amount'];
        }

        if (strpos($this->refund_type, '-')) {
            [$item, $type] = explode('-', $this->refund_type);
            $this->refund_type = $type;
            $this->item        = $item;
        }
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getAvailableTypes()
    {
        $paymentService = \Yii::createObject(PaymentService::class);
        if ($paymentService->isMoneyOnReservedAccount($this->transaction) || !$this->paymentInvoice->baseByPrintOrder()) {
            [$accountingList, $moneyMax] = $paymentService->getAccountingsList($this->paymentInvoice);
            return [$accountingList, $moneyMax];
        } else {
            $psWithRefund       = $this->paymentInvoice->storeOrderAmount->getAmountPrintForPsWithRefund();
            $printFeeWithRefund = $this->paymentInvoice->getAmountTotalWithRefund();

            $accountingList[self::REFUND_TYPE_PS] = 'From PS';
            $accountingList[self::REFUND_TYPE_TS] = 'From Treatstock';

            $moneyMax[self::REFUND_TYPE_PS] = ['price' => round($psWithRefund->getAmount(), 2)];
            $moneyMax[self::REFUND_TYPE_TS] = ['price' => round(MoneyMath::minus($printFeeWithRefund, $psWithRefund)->getAmount(), 2)];

            return [$accountingList, $moneyMax];
        }
    }

    public function isFullPrintRefund()
    {
        if ($this->status !== self::STATUS_NEW) {
            return false;
        }
        $printFee = $this->paymentInvoice->storeOrderAmount->getAmountPrintForPsWithRefund();
        return
            ($this->refund_type === self::REFUND_TYPE_PS) &&
            $printFee &&
            (round($printFee->getAmount(), 2) <= round($this->getMoneyAmount()->getAmount(), 2));
    }

    /**
     * @param PaymentTransaction $transaction
     * @param $amountMoney
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function validateTransaction(PaymentTransaction $transaction, $amountMoney): bool
    {
        if ($amountMoney && ($amountMoney->getAmount() <= 0)) {
            $this->addError('amount', 'Please remove the "-" from the amount.');
        }

        if (!\in_array($transaction->type, $this->validTransactionTypes, false)) {
            $this->addError('type', _t('site.order', 'Partial refunds not available for this type of transaction.'));
        }

        if (!$this->isFullRefund && \in_array($transaction->vendor, $this->invalidVendors, false)) {
            $this->addError('vendor', _t('site.order', 'Partial refund not available for API partner.'));
        }

        if (!\in_array($transaction->status, $this->validTransactionStatus, false)) {
            $this->addError('status', _t('site.order', 'Partial refunds will be available after payment is settled.'));
        }

        if ($amountMoney && MoneyMath::less($this->getAvailableTransactionRefundMoney($transaction), $amountMoney)) {
            $this->addError('amount', _t('site.order', 'Partial refund amount cannot exceed payment amount.'));
        }

        return !$this->hasErrors();
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (!parent::validate($attributeNames, $clearErrors)) {
            return false;
        }

        return $this->validateTransaction($this->transaction, $this->getMoneyCurrencyAmount());
    }

    /**
     * @return Money
     */
    public function getMoneyAmount(): Money
    {
        return Money::create($this->amount, $this->paymentInvoice->currency);
    }

    /**
     * @return Money
     */
    public function getMoneyCurrencyAmount(): Money
    {
        return Money::create($this->amount, $this->currency);
    }


    /**
     * @param PaymentTransaction $transaction
     *
     * Caches after first request, as save() is called, the refund is already created.
     * Causes problems when calling validate()
     *
     * @return Money
     * @throws \yii\base\InvalidConfigException
     */
    public function getAvailableTransactionRefundMoney(PaymentTransaction $transaction): Money
    {
        if ($this->_availableTransactionRefundMoney === null) {
            /** @var RefundService $refundService */
            $refundService                          = \Yii::createObject(RefundService::class);
            $this->_availableTransactionRefundMoney = $refundService->availableTransactionRefund($transaction);
        }

        return $this->_availableTransactionRefundMoney;
    }

    /**
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     */
    public function tryValidate(): void
    {
        $this->validate();
        $errors = $this->getErrors();

        if ($errors) {
            $firstError = reset($errors);
            throw new BusinessException($firstError);
        }
    }

    /**
     * Is refund from the manufacturer
     *
     * @return bool
     */
    public function isRefundFromManufacturer(): bool
    {
        return \in_array($this->refund_type, [self::REFUND_TYPE_PS, self::REFUND_TYPE_MANUFACTURER], true);
    }

    public function setApproved(): void
    {
        $this->status = self::STATUS_APPROVED;
    }

}