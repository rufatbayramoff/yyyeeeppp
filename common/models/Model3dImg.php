<?php

namespace common\models;

use common\components\Model3dHistoryConfigurator;
use common\components\validators\ImageFileValidator;
use common\interfaces\FileOwnerInterface;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBaseInterface;
use common\models\model3d\Model3dBaseImg;

/**
 *
 *
 */
class Model3dImg extends \common\models\base\Model3dImg implements FileOwnerInterface, Model3dBaseImgInterface
{

    use Model3dBaseImg;

    const SCENARIO_CREATE          = 'create';
    const MODEL3D_IMAGE_TYPE_OWNER = 'owner';

    public const UID_PREFIX = 'MI:';

    public function scenarios()
    {
        return array_merge(
            parent::scenarios()
            ,
            [
                self::SCENARIO_CREATE => [
                    'size',
                    'title',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                    'moderated_at',
                    'is_moderated',
                    'basename',
                    'extension',
                    'user_id',
                    'type',
                    'review_id',
                    'file'
                ]
            ]
        );
    }

    public function rules()
    {
        $parentRules = parent::rules();
        return array_merge(
            $parentRules,
            [
                [
                    ['file'],
                    ImageFileValidator::class,
                    'minWidth' => 1
                ]
            ]
        );
    }

    public function behaviors()
    {
        return [
            'Model3dHistory' => Model3dHistoryConfigurator::getConfig(self::class),
        ];
    }

    public function setTitle($title)
    {
        if (empty($title)) {
            throw new Exception("Not valid title");
        }

        $validUtf8 = mb_check_encoding($title, 'UTF-8');
        if (!$validUtf8) {
            throw new Exception("Not valid i18n title");
        }
        $title = iconv("UTF-8", "UTF-8//IGNORE", $title);
        $valid = preg_match(
            '%^(?:
            [\x09\x0A\x0D\x20-\x7E]              # ASCII
            | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
            |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
            | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
            |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
            |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
            | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
            |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
            )*$%xs',
            $title
        );
        if (!$valid) {
            throw new Exception("Not valid title!");
        }
        $title = mb_substr($title, 0, 45);
        $this->title = $title;
    }

    public function setOriginalFile(File $file)
    {
        $this->populateRelation('originalFile', $file);
        $this->original_file_id = $file->id;
    }

    public function setAttachedModel3d(Model3dBaseInterface $model3d): void
    {
        $this->populateRelation('model3d', $model3d);
    }

    /**
     * @return Model3dBaseImgInterface
     */
    public function getOriginalModel3dImg(): Model3dBaseImgInterface
    {
        return $this;
    }

    public function getAttachedModel3d(): Model3dBaseInterface
    {
        return $this->model3d;
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getModel3dImgs()
    {
        return $this->hasMany(\common\models\Model3dImg::class, ['original_model3d_img_id' => 'id']);
    }

    public function getModel3dReplicaImgs()
    {
        return $this->hasMany(\common\models\Model3dReplicaImg::class, ['original_model3d_img_id' => 'id']);
    }
}