<?php

namespace common\models;

/**
 * Class HomePageCategoryBlock
 * @package common\models
 */
class HomePageCategoryBlock extends \common\models\base\HomePageCategoryBlock
{
    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_CREATE] = [
            '!created_at',
            'position',
            'is_active',
            'product_category_id',
            'title',
            'url'
        ];

        $scenarios[self::SCENARIO_UPDATE] = [
            'position',
            'is_active',
            'product_category_id',
            'title',
            'url'
        ];

        return $scenarios;
    }

    public function rules()
    {
        return [
            [['created_at', 'position', 'is_active'], 'required'],
            [['created_at'], 'safe'],
            [['is_active', 'product_category_id', 'position'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
            [['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\ProductCategory::class, 'targetAttribute' => ['product_category_id' => 'id']],
            [['title', 'url', 'product_category_id'], 'categoryValidate', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE], 'skipOnEmpty' => false],
        ];
    }

    public function categoryValidate(): void
    {
        if (
            (empty($this->url) || empty($this->title)) &&
            (empty($this->product_category_id) || !$this->productCategory)
        ) {
            $this->addError('product_category_id', 'You must fill out the "product_category_id" or "title" and "url"');
        }
    }

    /**
     * @return string
     */
    public function getTitleText(): string
    {
        if (!empty($this->title)) {
            return $this->title;
        }

        if ($productCategory = $this->productCategory) {
            return $productCategory->title;
        }

        return '';
    }

    /**
     * @return string
     */
    public function getViewUrl(): string
    {
        if (!empty($this->url)) {
            return $this->url;
        }

        $productCategory = $this->productCategory;

        if ($productCategory) {
            return $productCategory->getViewUrl();
        }

        return '';
    }
}