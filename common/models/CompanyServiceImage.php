<?php

namespace common\models;

use frontend\modules\mybusiness\components\ImageRotateTrait;

/**
 * Class CompanyServiceImage
 * @package common\models
 */
class CompanyServiceImage extends \common\models\base\CompanyServiceImage
{
    use ImageRotateTrait;
}