<?php
namespace common\models;

use Yii;

/**
 * Edit this file
 * This is the model class for table "user_login_log".
 */
class UserLoginLog extends \common\models\base\UserLoginLog
{
    const LOGIN_TYPE_FORM = 'form';
    const LOGIN_TYPE_OSN = 'osn';
    const LOGIN_TYPE_AUTO_CREATE = 'auto-create';
}