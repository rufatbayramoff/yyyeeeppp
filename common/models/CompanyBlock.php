<?php

namespace common\models;

use Yii;
/**
 * Class CompanyBlock
 *
 * @property \common\models\File[] $imageFiles
 * @package common\models
 */
class CompanyBlock extends \common\models\base\CompanyBlock
{
    public function getImageFiles()
    {
        return $this->hasMany(File::class, ['uuid' => 'file_uuid'])->via('companyBlockImages');
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'created_at' => Yii::t('app', 'Created At'),
            'is_visible' => Yii::t('app', 'Is Visible'),
            'position' => Yii::t('app', 'Position'),
            'company_id' => Yii::t('app', 'Company ID'),
            'videos' => Yii::t('app', 'Videos'),
            'is_public_profile' => Yii::t('app', 'Display in public profile'),
        ];
    }

    /**
     * @return File[]
     */
    public function getImages(): array
    {
        return $this->imageFiles;
    }

    public function getImageFileByUuid($fileUuid)
    {
        foreach ($this->imageFiles as $imageFile) {
            if ($imageFile->uuid == $fileUuid) {
                return $imageFile;
            }
        }
        return null;
    }

    /**
     * @param File[] $files
     */
    public function addImageFiles($files)
    {
        $currentFiles = $this->getImages();
        foreach ($files as $file) {
            $file->setPublicMode(true);
            $file->setOwner(CompanyServiceBlock::class, 'file_uuid');
            $currentFiles[] = $file;
        }
        $this->setImageFiles($currentFiles);
    }

    public function setImageFiles($files)
    {
        $this->populateRelation('imageFiles', $files);
    }

    public function setCompanyVideos(array $videos)
    {
        $this->videos = $videos;
    }

    public function getCompanyBlockVideos()
    {
        $props = $this->videos;
        return $props;
    }
}