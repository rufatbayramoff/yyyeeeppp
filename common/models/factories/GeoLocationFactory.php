<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.10.16
 * Time: 11:23
 */

namespace common\models\factories;

use common\models\GeoCity;
use common\models\GeoCountry;
use common\models\GeoLocation;
use common\models\GeoRegion;
use lib\geo\models\Location;
use Yii;
use yii\base\InvalidParamException;


class GeoLocationFactory
{
    /**
     * @param Location $location
     * @return GeoLocation
     * @throws \yii\base\InvalidConfigException
     */
    public static function createByLocation(Location $location)
    {
        $geoLocation = Yii::createObject(GeoLocation::class);
        $geoLocation->setInitValuesFromLocation($location);
        return $geoLocation;
    }

    /**
     * @param array $locationArr
     * @return GeoLocation
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function create($locationArr)
    {
        /** @var GeoLocation $geoLocation */
        $geoLocation = Yii::createObject(GeoLocation::class);
        $geoLocation->setLocationArr($locationArr);
        return $geoLocation;
    }
}