<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.08.16
 * Time: 15:15
 */

namespace common\models\factories;

use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\File;
use common\models\Model3d;
use common\models\Model3dPart;
use common\models\Model3dReplicaPart;

class Model3dPartFactory
{
    /**
     * Can create model3dreplica part
     *
     * @param Model3dBaseInterface $model3d
     * @param File $file
     * @return Model3dPart
     * @throws \yii\base\Exception
     */
    public function createModel3dPart(Model3dBaseInterface $model3d, File $file): Model3dBasePartInterface
    {
        if ($model3d instanceof Model3d) {
            $model3dPart             = new Model3dPart();
            $model3dPart->model3d_id = $model3d->id;
        } else {
            $model3dPart                     = new Model3dReplicaPart();
            $model3dPart->model3d_replica_id = $model3d->id;
            $model3dPart->original_qty       = 1;
        }

        $data = [
            'format'           => $file->extension,
            'name'             => mb_substr($file->getFileName(), 0, 45),
            'moderator_status' => Model3dBaseInterface::STATUS_NEW,
            'user_status'      => 'active',
            'qty'              => 1,
            'rotated_x'        => 0,
            'rotated_y'        => 0,
            'rotated_z'        => 0
        ];
        $model3dPart->load($data, '');

        $model3dPart->populateRelation('model3d', $model3d);
        $model3dPart->setFile($file);

        if ($file->extension === Model3dBasePartInterface::PLY_FORMAT) {
            $texture = Model3dTextureFactory::createModel3dTexture(true);
            $texture->safeSave();
            $model3dPart->setTexture($texture);
        } else {
            if (!$model3d->isOneTextureForKit()) {
                $texture = Model3dTextureFactory::createModel3dTexture();
                $texture->safeSave();
                $model3dPart->setTexture($texture);
            }
        }
        return $model3dPart;
    }
}