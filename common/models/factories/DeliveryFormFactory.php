<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.11.17
 * Time: 15:43
 */

namespace common\models\factories;

use common\models\CompanyService;
use common\models\PsPrinter;
use common\models\StoreOrder;
use common\models\User;
use frontend\components\UserSessionFacade;
use frontend\models\delivery\DeliveryForm;
use frontend\models\user\UserFacade;
use lib\geo\GeoNames;

class DeliveryFormFactory
{
    /**
     * @param CompanyService $psMachine
     * @return DeliveryForm
     * @throws \lib\geo\GeoNamesException
     */
    public function createByPsMachine(CompanyService $psMachine)
    {
        /** @var User $currentUser */
        $currentUser     = UserFacade::getCurrentUser();
        $deliveryForm    = $this->baseCreatePsMachine($psMachine);
        if($currentUser && $currentUser->company && $currentUser->company->title) {
            $deliveryForm->company = $currentUser->company->title;
        }
        return $deliveryForm;
    }

    public function createApiByPsMachine(CompanyService $psMachine)
    {
        return $this->baseCreatePsMachine($psMachine);
    }

    protected function baseCreatePsMachine(CompanyService $psMachine)
    {
        /** @var User $currentUser */
        $currentUser     = UserFacade::getCurrentUser();
        $currentLocation = UserSessionFacade::getLocation();
        $deliveryForm    = new DeliveryForm($psMachine->asDeliveryParams());
        if ($currentUser) {
            $deliveryForm->email = $currentUser->email;
        }
        if ($currentLocation) {
            if ($currentLocation->country && !$deliveryForm->country) {
                $deliveryForm->country = $currentLocation->country;
            }
            if ($currentLocation->region && !$deliveryForm->state) {
                $deliveryForm->state = $currentLocation->region;
            }
            if ($currentLocation->city && !$deliveryForm->city) {
                $deliveryForm->city = $currentLocation->city;
            }
            $deliveryForm->lat = $currentLocation->lat;
            $deliveryForm->lon = $currentLocation->lon;
        }
        return $deliveryForm;
    }

    public function createByOrder(StoreOrder $storeOrder, PsPrinter $printer)
    {
        $deliveryForm          = new DeliveryForm($printer->companyService->asDeliveryParams(), $storeOrder->getFirstReplicaItem());
        $deliveryForm->phone   = $storeOrder->shipAddress->phone;
        $deliveryForm->email   = $storeOrder->shipAddress->email;
        $deliveryForm->country = $storeOrder->shipAddress->country->iso_code;
        $deliveryForm->state   = $storeOrder->shipAddress->region;
        $deliveryForm->city    = $storeOrder->shipAddress->city;
        $deliveryForm->lat     = $storeOrder->shipAddress->lat;
        $deliveryForm->lon     = $storeOrder->shipAddress->lon;
        return $deliveryForm;


    }
}