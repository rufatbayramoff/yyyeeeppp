<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.11.16
 * Time: 11:59
 */

namespace common\models\factories;

use common\components\exceptions\BusinessException;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Html;
use yii\web\UploadedFile;

class UploadedFileFactory
{
    public const TYPE_THINGIVERSE = 'thingiverse';

    public static function downloadThingiverse($thindId)
    {
        sleep(0);
    }

    /**
     * Create UploadedFile by url
     *
     * @param string $url
     * @return UploadedFile
     * @throws \yii\base\InvalidArgumentException
     */
    public static function createUploadedFile($url)
    {
        $parsedUrl = parse_url($url);
        if (empty($parsedUrl['scheme']) || !in_array($parsedUrl['scheme'], ['http', 'https', 'zip'], true)) {
            throw new BusinessException('Invalid file URL');
        }
        if (empty($parsedUrl['path'])) {
            throw new BusinessException('File path not found in URL');
        }
        if (empty($parsedUrl['path'])) {
            throw new BusinessException('File path not found in URL');
        }
        $ip = gethostbyname($parsedUrl['host']);
        if ((strlen($parsedUrl['host']) < 3) || ($ip == '127.0.0.1')) {
            throw new BusinessException('Invalid host');
        }
        $parsedFileQuery = null;
        $parsedFile      = pathinfo($parsedUrl['path']);
        if (empty($parsedFile['extension']) && !empty($parsedUrl['query'])) {
            parse_str($parsedUrl['query'], $parsedFileQuery);
            foreach ($parsedFileQuery as $q) {
                $parsedFileTmp = pathinfo($q);
                if (!empty($parsedFileTmp['extension'])) {
                    $parsedFile = $parsedFileTmp;
                }
            }
        }
        if (empty($parsedFile['extension'])) {
            throw new BusinessException('File extension not found in URL');
        }
        try {
            $context = stream_context_create(['ssl' => ['verify_peer' => false, 'verify_peer_name' => false]]);
            $url     = str_replace("&amp;", "&", $url);
            $url     = str_replace(" ", "%20", $url);
            $content = file_get_contents($url, false, $context);
            if (!$content) {
                throw new BusinessException('No data found with file URL');
            }
        } catch (\Exception $e) {
            \Yii::error($e);
            throw new BusinessException('No data found with file URL.');
        }
        $tempPath = Yii::getAlias('@frontend') . '/runtime/api_upload_file_' . date('Y-m-d_H-i-s') . '-' . mt_rand(0, 999999) . '.' . $parsedFile['extension'];
        file_put_contents($tempPath, $content);
        $uploadedFile           = new UploadedFile();
        $uploadedFile->name     = $parsedFile['basename'];
        $uploadedFile->size     = filesize($tempPath);
        $uploadedFile->tempName = $tempPath;

        return $uploadedFile;
    }

    public static function getInstances($model, $attribute)
    {
        $name = Html::getInputName($model, $attribute);
        return static::getInstancesByName($name);
    }


    /**
     * Method like UploadedFile::getInstancesByName, but with save indexes
     *
     * @param $name
     * @return UploadedFile[]
     */
    public static function getInstancesByName($name)
    {
        $filesList       = [];
        $filesForm       = $_FILES;
        $nameFormBracket = strpos($name, '[');
        $nameVar         = $name;
        if ($nameFormBracket) {
            $nameForm  = substr($name, 0, $nameFormBracket);
            $nameVar   = substr($name, $nameFormBracket + 1, -1);
            $filesForm = $_FILES[$nameForm] ?? [];
        }
        if (!$filesForm) {
            return [];
        }
        foreach ($filesForm['name'] as $key => $fileNames) {
            if (strpos($key, $nameVar) === 0) {
                foreach ($fileNames as $index => $fileName) {
                    if (!$fileName) {
                        continue;
                    }
                    $uploadedFile           = new UploadedFile();
                    $uploadedFile->name     = $fileName;
                    $uploadedFile->type     = $filesForm['type'][$key][$index];
                    $uploadedFile->size     = $filesForm['size'][$key][$index];
                    $uploadedFile->tempName = $filesForm['tmp_name'][$key][$index];
                    $filesList[$index]      = $uploadedFile;
                }
            }
        }
        return $filesList;
    }
}
