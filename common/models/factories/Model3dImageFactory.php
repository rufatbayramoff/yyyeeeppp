<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.06.16
 * Time: 18:16
 */

namespace common\models\factories;

use common\interfaces\Model3dBaseInterface;
use common\models\File;
use common\models\Model3d;
use common\models\Model3dImg;
use common\services\FileService;
use DateTime;
use DateTimeZone;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use yii\web\UploadedFile;

class Model3dImageFactory
{
    /**
     * @var FrontUser
     */
    public $user;

    /**
     * Model3dImageFactory constructor.
     *
     * @param bool|FrontUser $user
     */
    public function __construct($user = false)
    {
        if ($user !== false) {
            $this->user = $user;
        } else {
            $this->user = UserFacade::getCurrentUser();
        }
    }

    /**
     * @param $model3d Model3dBaseInterface
     * @param $file File
     * @return Model3dImg
     */
    public function createModel3dImage($model3d, $file)
    {
        $model3dImage = new Model3dImg();
        $model3dImage->setScenario(Model3dImg::SCENARIO_CREATE);
        $model3dImage->populateRelation('file', $file);
        $model3dImage->user_id = $this->user ? $this->user->id : null;
        $model3dImage->model3d_id = $model3d->id;
        $model3dImage->populateRelation('model3d', $model3d);
        $model3dImage->basename = $file->getFileNameWithoutExtension();
        $model3dImage->extension = $file->getFileExtension();
        $model3dImage->created_at = $model3dImage->updated_at = (new DateTime('now' ,new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $model3dImage->type = Model3dImg::MODEL3D_IMAGE_TYPE_OWNER;
        $model3dImage->is_moderated = 0;

        return $model3dImage;
    }
}