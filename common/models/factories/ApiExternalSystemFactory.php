<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.10.16
 * Time: 15:44
 */

namespace common\models\factories;

use common\models\ApiExternalSystem;
use Yii;

class ApiExternalSystemFactory
{
    public static function create()
    {
        $apiExternalSystem = Yii::createObject(ApiExternalSystem::class);
        $apiExternalSystem->private_key = substr(md5(mt_rand(0, 999999)), 0, 15);
        $apiExternalSystem->public_upload_key = substr(md5(mt_rand(0, 999999)) . md5(mt_rand(0, 999999)), 0, 30);
        return $apiExternalSystem;

    }
}