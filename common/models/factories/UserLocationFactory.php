<?php
/**
 * User: nabi
 */

namespace common\models\factories;


use common\components\exceptions\BusinessException;
use common\models\GeoCountry;
use common\models\User;
use common\models\UserLocation;
use lib\geo\GeoNames;
use lib\geo\models\Location;

class UserLocationFactory
{

    /**
     * @param $userId
     * @param $string - for example New York, NY, US or just New York, NY
     * @param $country GeoCountry
     */
    public static function createFromString(User $user, $string, $country = null)
    {
        if ($country) {
            $rs = explode(",", $string);
            if (count($rs) != 3) {
                $string = $string . ', ' . $country->iso_code;
            }
        }
        $r = LocationFactory::createFromString($string, ', ', true);
        if(!$r){
            return null;
        }
        return self::createFromLocation($user, $r, $string);
    }

    /**
     * @param User $user
     * @param Location $loc
     * @return UserLocation
     */
    public static function createFromLocation(User $user, Location $loc, $formattedAddress = '')
    {
        $location = new UserLocation();
        $location->user_id = $user->id;
        $location->country_id = $loc->getGeoCountry()->id;
        $location->region_id = $loc->geoRegionObject ? $loc->geoRegionObject->id : null;
        $location->city_id = $loc->geoCityObject ? $loc->geoCityObject->id : null;
        $location->address = '';
        $location->address2 = $addressData['address2'] ?? '';
        $location->lat = $loc->lat;
        $location->lon = $loc->lon;
        $location->zip_code = null;
        $location->location_type = 'location';
        $location->formatted_address = $formattedAddress;
        return $location;
    }

    /**
     * @param User $user
     * @param array $addressData
     * @return UserLocation
     */
    public static function createFromAddressData(User $user, array $addressData)
    {
        $addressData['lat'] = number_format($addressData['lat'], 5, '.', '');
        $addressData['lon'] = number_format($addressData['lon'], 5, '.', '');
        if(empty($addressData['city']) && empty($addressData['city_name'])){
            throw new BusinessException('Please specify city in address.');
        }
        $geoCity = GeoNames::getCitiesByCoordsAndName($addressData['city'] ?? $addressData['city_name'], $addressData['lat'], $addressData['lon']);

        if (!empty($addressData['country'])) {
            $country = GeoCountry::tryFind(['iso_code' => $addressData['country']]);
            $addressData['country_id'] = $country->id;
        }
        if (!empty($addressData['zip'])) {
            $addressData['zip_code'] = $addressData['zip'];
        }
        $region_id = null;
        if ($geoCity) {
            $region_id = $geoCity->region_id ?? null;
            $city_id = $geoCity->id ?? null;
        } else {
            // not found
            // we need to add region and city
            $regionName = $addressData['region'] ?? ($addressData['region_name'] ?? null);
            if ($regionName) {
                $region_id = GeoNames::getRegionOrCreate($addressData['country_id'], $regionName)->id;
            }
            $city_id = GeoNames::getCityOrCreate($addressData, $region_id)->id;
        }
        $location = new UserLocation();
        $location->user_id = $user->id;
        $location->country_id = $addressData['country_id'];
        $location->region_id = $region_id;
        $location->city_id = $city_id;
        $location->address = $addressData['address'] ?? '';
        $location->address2 = $addressData['address2'] ?? '';
        $location->lat = $addressData['lat'];
        $location->lon = $addressData['lon'];
        $location->zip_code = $addressData['zip_code'] ?? null;
        $location->location_type = 'delivery';
        $location->formatted_address = $addressData['formatted_address'] ?? null;
        $location->setUserData($addressData);
        return $location;
    }

    /**
     * @param User $user
     * @param array $obj - google api json data
     * @return UserLocation
     * @throws BusinessException
     * @throws \yii\web\NotFoundHttpException
     */
    public static function createFromGoogleApiData(User $user, array $obj)
    {
        if (!empty($obj['administrative_area_level_1'])) {
            $obj['administrative_area_level_1'] = trim($obj['administrative_area_level_1'], "'- ");
            $obj['administrative_area_level_1'] = str_replace(['/', '"', "'"], '-', $obj['administrative_area_level_1']);
        }
        $fromApi = [
            'country'      => 'country',
            'region'       => 'administrative_area_level_1',
            'city'         => 'locality',
            'zip_code'     => 'postal_code',
            'address'       => 'route',
            'streetNumber' => 'street_number',
            'formatted_address' => 'formatted_address',
            'lat'          => 'lat',
            'lon'          => 'lon'
        ];
        $addressData = [];
        foreach ($fromApi as $key => $jsonKey) {
            if (isset($obj[$jsonKey])) {
                if (in_array($key, ['city', 'state', 'country'])) {
                    $addressData[$key] = trim($obj[$jsonKey], "'- ");
                } else {
                    $addressData[$key] = $obj[$jsonKey];
                }
            }
        }
        if(empty($addressData['city']) && empty($addressData['city_name'])){
            throw new BusinessException('Please specify city in address.');
        }
        $addressData['address'] = explode(',', $obj['formatted_address'])[0];
        $country = GeoCountry::tryFind(['iso_code' => $addressData['country']]);
        $addressData['country_id'] = $country->id;
        $location = self::createFromAddressData($user, $addressData);
        return $location;
    }

    /**
     * @param User $user
     * @param Location $location
     * @return UserLocation
     */
    public function createForCompany(User $user, Location $location): UserLocation
    {
        $model = new UserLocation();
        $model->user_id = $user->id;
        $model->country_id = $location->getGeoCountryObjectCache()->id;
        $model->city_id = $location->getGeoCity()->id;
        $model->region_id = $location->getGeoCity()->region_id;
        $model->lat = $location->lat;
        $model->lon = $location->lon;
        $model->location_type = UserLocation::LOCATION_TYPE_SHIP_FROM;
        return $model;
    }
}