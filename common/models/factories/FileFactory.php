<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.06.16
 * Time: 18:35
 */


namespace common\models\factories;

use common\components\exceptions\BusinessException;
use common\interfaces\FileBaseInterface;
use common\models\File;
use common\models\FileAdmin;
use common\models\repositories\UserSessionRepository;
use common\models\user\UserIdentityProvider;
use common\models\UserSession;
use DateTime;
use DateTimeZone;
use frontend\models\user\FrontUser;
use yii\base\BaseObject;
use yii\web\UploadedFile;

class FileFactory extends BaseObject
{
    /**
     * @var FrontUser
     */
    public $user;

    /**
     * @var UserSession
     */
    public $userSession;


    /**
     * FileFactory constructor.
     *
     * @param UserIdentityProvider $userIdentityProvider
     * @param UserSessionRepository $userSessionRepository
     */
    public function injectDependencies(UserIdentityProvider $userIdentityProvider, UserSessionRepository $userSessionRepository)
    {
        if (!$this->user) {
            $this->user = $userIdentityProvider->getUser();
        }
        if (!$this->userSession) {
            $this->userSession = $userSessionRepository->getCurrent();
        }
    }

    public static function generateFileUuid()
    {
        return md5(mt_rand(-99990999, 99990999) . microtime(true) . '534tt');
    }

    /**
     * @param $uploadedFiles
     * @param string $fileType
     * @return File[]
     * @throws \Exception
     */
    public function createFilesFromUploadedFiles($uploadedFiles, $fileType = FileBaseInterface::TYPE_FILE_SIMPLE)
    {
        $files = [];
        foreach ($uploadedFiles as $index => $uploadedFile) {
            $file = $this->createFileFromUploadedFile($uploadedFile, $fileType);
            $files[$index] = $file;
        }
        return $files;
    }

    public function filterFileName($fileName)
    {
        $fileName = str_replace(['/', '\\', '%', ';', ':', '=', '<', '>', '\r','\n'], '', $fileName);
        return $fileName;
    }

    /**
     *
     * @param UploadedFile $uploadedFile
     * @param string $fileType
     *
     * @return File|FileAdmin
     * @throws BusinessException
     * @throws \yii\base\ErrorException
     */
    public function createFileFromUploadedFile($uploadedFile, $fileType = FileBaseInterface::TYPE_FILE_SIMPLE)
    {
        if ($uploadedFile->size == 0) {
            throw new BusinessException(_t('site.common', 'Uploaded file size cannot be zero.'));
        }
        if ($fileType === FileBaseInterface::TYPE_FILE_ADMIN) {
            $file = new FileAdmin();
        } else {
            $file = new File;
            $file->user_session_id = $this->userSession->id ?? 1;
        }
        $file->setScenario(FileBaseInterface::SCENARIO_CREATE);
        $file->uuid = self::generateFileUuid();
        $file->name = $this->filterFileName($uploadedFile->name);
        $file->stored_name = '';
        $file->extension = $uploadedFile->extension;

        $file->size = $uploadedFile->size ? $uploadedFile->size : filesize($uploadedFile->tempName);
        $file->last_access_at = $file->updated_at = $file->created_at = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $file->user_id = $this->user->id ?? null;
        $file->status = FileBaseInterface::STATUS_ACTIVE;
        $file->md5sum = md5_file($uploadedFile->tempName);
        $file->is_public = 0;
        $fileTmpPath = $file->getLocalTmpFilePath();
        copy($uploadedFile->tempName, $fileTmpPath); // @TODO why not move_uploaded_file ?
        $newmd5 = md5_file($fileTmpPath);
        if ($newmd5 != $file->md5sum) {
            \Yii::error("[md5 error] $fileTmpPath [old " . $file->md5sum . "] [new $newmd5]");
        }
        return $file;
    }

    /**
     * copy file based on old file, this is required for scaling and rotating feature
     * returns file which is not yet stored to DB,
     * so $file->save() call is required in other place
     *
     * @param File $fileOld
     * @return File
     * @throws \yii\base\InvalidParamException
     */
    public function createFileFromCopy(File $fileOld)
    {
        $file = new File;
        $file->setScenario(FileBaseInterface::SCENARIO_CREATE);
        $file->uuid = self::generateFileUuid();
        $file->server = $fileOld->server;
        $file->name = $fileOld->name;
        $file->stored_name = '';
        $file->extension = $fileOld->extension;
        $file->size = $fileOld->size;
        $file->last_access_at = $file->updated_at = $file->created_at = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $file->user_id = $fileOld->user_id;
        $file->user_session_id = $fileOld->user_session_id ?? 1;
        $file->status = FileBaseInterface::STATUS_ACTIVE;
        $file->md5sum = $fileOld->md5sum;
        $file->is_public = 0;
        copy($fileOld->getLocalTmpFilePath(), $file->getLocalTmpFilePath());
        return $file;
    }

    /**
     * @param $fullPath
     * @param $content
     * @return File
     */
    public function createFileFromContent($fullPath, $content)
    {
        $pathinfo = pathinfo($fullPath);
        $file = new File;
        $file->setScenario(FileBaseInterface::SCENARIO_CREATE);
        $file->uuid = self::generateFileUuid();
        $file->name = $this->filterFileName($pathinfo['basename']);
        $file->stored_name = '';
        $file->extension = $pathinfo['extension'];
        $file->size = mb_strlen($content);
        $file->last_access_at = $file->updated_at = $file->created_at = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $file->user_id = $this->user->id ?? null;
        $file->user_session_id = $this->userSession->id ?? 1;
        $file->status = FileBaseInterface::STATUS_ACTIVE;
        $file->md5sum = md5($content);
        $file->is_public = 0;
        $file->setFileContentAndSave($content);
        return $file;
    }

    /**
     * @param $fullPath
     * @param array $props
     * @return File
     */
    public function createFileFromPath($fullPath, array $props = [])
    {
        $pathinfo = pathinfo($fullPath);
        $file = new File;
        $file->setScenario(FileBaseInterface::SCENARIO_CREATE);
        $file->uuid = self::generateFileUuid();
        $file->name = $this->filterFileName($pathinfo['basename']);
        $file->stored_name = '';
        $file->extension = strtolower($pathinfo['extension']??''); // Fix for jpg files.  Example /static/files/c7/e6/940546_720x540.JPG file resizer not working
        $file->size = filesize($fullPath);
        $file->last_access_at = $file->updated_at = $file->created_at = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $file->user_id = $this->user->id ?? null;
        $file->user_session_id = $this->userSession->id ?? 1;
        $file->status = FileBaseInterface::STATUS_ACTIVE;
        $file->md5sum = md5_file($fullPath);
        $file->is_public = 0;
        foreach ($props as $k => $v) {
            $file->$k = $v;
        }
        copy($fullPath, $file->getLocalTmpFilePath());
        //$file->save();
        return $file;
    }
}