<?php
/**
 * User: nabi
 */

namespace common\models\factories;

use common\models\GeoCity;
use common\models\GeoCountry;
use common\models\GeoLocation;
use common\models\GeoRegion;
use common\models\UserAddress;
use common\models\UserLocation;
use common\modules\catalogPs\models\CatalogSearchForm;
use frontend\models\delivery\DeliveryForm;
use lib\geo\GeoNames;
use lib\geo\LocationUtils;
use lib\geo\models\Location;
use yii\helpers\Json;

class LocationFactory
{

    /**
     * @param $location
     * @return null|Location
     */
    public static function createFromString($location, $delimeter = '--', $autocreateCity = false)
    {
        if ($location == CatalogSearchForm::LOCATION_EMPTY) {
            $locationObj = new Location();
            $locationObj->city = $location;
            return $locationObj;
        }
        $locationParts = explode($delimeter, $location);
        if (count($locationParts) < 2) {
            return null;
        }
        $cityOrig = $locationParts[0];
        $city = str_replace('-', ' ', $cityOrig);
        $regionOrig = $locationParts[1];
        $region = str_replace('-', ' ', $regionOrig);

        $country = count($locationParts) > 2 ? $locationParts[2] : $locationParts[1];
        $country = trim($country, '-');
        if (count($locationParts) === 2) {
            $region = null;
        }
        if ($country === 'UK') {
            $country = 'GB';
        }
        $geoCountry = GeoCountry::findOne(['iso_code' => $country]);
        if (!$geoCountry) {
            return null;
        }
        $citySearch = str_replace(['-', "'", '"', ';'], '_', $cityOrig);
        $cityQuery = GeoCity::find()
            ->joinWith(['country', 'region'])
            ->andFilterWhere(['like', 'geo_city.title', dbexpr("'" . $citySearch . "'")])
            ->andWhere(['geo_country.iso_code' => $country]);

        $geoRegion = null;
        if (!empty($region)) {
            $regionState = '';
            if ($geoCountry->iso_code == 'US') { // we should search both with TX and Texas
                if (strlen($region) === 2) {
                    $regionState = LocationUtils::findUsaState($region);
                } else {
                    $regionState = LocationUtils::findUsaState($region, true);
                }
            }
            $regionSearch = str_replace(['-', "/", "'", '"', ';'], '_', $regionOrig);
            if ($autocreateCity) {
                $geoRegion = GeoRegion::find()->where(['title' => array_filter([$region, $regionState])])->one();
            }
            $cityQuery->andWhere(
                [
                    'OR',
                    ['like', 'geo_region.title', dbexpr("'" . $regionSearch . "'")],
                    ['geo_region.title' => array_filter([$region, $regionState])]
                ]
            );

        }
        $cityObj = $cityQuery->one();
        $lat = $lon = 0;
        if (!$cityObj) {
            if ($autocreateCity && $geoRegion) {
                // try to find any city in this region
                $cityObjNear = GeoCity::find()->where(['country_id' => $geoCountry->id, 'region_id' => $geoRegion->id])->withoutStaticCache()->one();
                if ($cityObjNear) {
                    // create new
                    $attr = $cityObjNear->getAttributes();
                    unset($attr['id']);
                    $attr['geoname_id'] = 1;
                    $attr['fcode'] = '11';
                    unset($attr['google_place_id']);
                    $attr['title'] = $cityOrig;
                    $attr['country_id'] = $geoCountry->id;
                    $cityObj = GeoCity::addRecord($attr);
                }
            }
        }
        if ($cityObj) {
            $lat = $cityObj->lat;
            $lon = $cityObj->lon;
        } else {
            return null;
        }
        $location = \Yii::createObject(
            [
                'class'           => Location::class,
                'geoCityObject'   => $cityObj,
                'geoRegionObject' => $geoRegion,
                'city'            => $cityObj->title,
                'region'          => $cityObj->region ? $cityObj->region->title : null,
                'country'         => $country,
                'lat'             => $lat,
                'lon'             => $lon
            ]
        );
        return $location;
    }

    /**
     * @param GeoCity $geoCity
     * @return Location|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function createFromGeoCity(GeoCity $cityObj): Location
    {
        $location = \Yii::createObject(
            [
                'class'   => Location::class,
                'city'    => $cityObj->title,
                'region'  => $cityObj->region ? $cityObj->region->title : null,
                'country' => $cityObj->country->iso_code,
                'lat'     => $cityObj->lat,
                'lon'     => $cityObj->lon
            ]
        );
        return $location;
    }

    public static function createFromLocation(Location $location)
    {
        $location = \Yii::createObject(
            [
                'class'   => Location::class,
                'city'    => $location->city,
                'region'  => $location->region,
                'country' => $location->country,
                'lat'     => $location->lat,
                'lon'     => $location->lon
            ]
        );
        return $location;
    }

    /**
     * @param $locationJson
     * @return Location|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function createFromLocationJson($locationJson)
    {
        return self::createFromArray(Json::decode($locationJson));
    }

    /**
     * @param array $data
     * @return Location|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function createFromArray(array $data)
    {
        return \Yii::createObject(
            [
                'class'   => Location::class,
                'city'    => $data['city'],
                'region'  => $data['region'],
                'country' => $data['country'],
                'lat'     => $data['lat'],
                'lon'     => $data['lon']
            ]
        );
    }

    /**
     * @param DeliveryForm $deliveryForm
     * @return Location|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function createFromDeliveryForm(DeliveryForm $deliveryForm)
    {
        $location = \Yii::createObject(
            [
                'class'   => Location::class,
                'city'    => $deliveryForm->city,
                'region'  => $deliveryForm->state,
                'country' => $deliveryForm->country,
                'lat'     => $deliveryForm->lat,
                'lon'     => $deliveryForm->lon
            ]
        );
        return $location;
    }

    /**
     * could be updated to API call?
     *
     * @param Location $loc
     * @return Location
     * @throws \yii\db\Exception
     */
    private static function fillLatLon(Location $loc)
    {
        $row = \Yii::$app->db->createCommand(
            "SELECT IFNULL(`geo_city`.lat, cap.lat) AS lat, IFNULL(geo_city.lon , cap.lon) AS lon
                FROM `geo_city` 
                LEFT JOIN `geo_country` ON `geo_city`.`country_id` = `geo_country`.`id` 
                LEFT JOIN `geo_city` AS cap ON cap.id=geo_country.capital_id AND  (`geo_city`.`title`='Delaware') 
                LEFT JOIN `geo_region` ON `geo_city`.`region_id` = `geo_region`.`id` 
                WHERE (`geo_country`.`iso_code`='US') LIMIT 1"
        )->queryOne(\PDO::FETCH_OBJ);
        $loc->lat = $row->lat;
        $loc->lon = $row->lon;
        return $loc;
    }

    /**
     * @param UserAddress $address
     * @return Location|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function createFromUserAddress(UserAddress $address)
    {
        $location = \Yii::createObject(
            [
                'class'   => Location::class,
                'city'    => $address->city,
                'region'  => $address->region,
                'country' => $address->country->iso_code,
                'lat'     => $address->lat,
                'lon'     => $address->lon
            ]
        );
        return $location;
    }

    public static function createFromUserLocation(UserLocation $address)
    {
        if (!$address->city || !$address->country) {
            return null;
        }
        $location = \Yii::createObject(
            [
                'class'   => Location::class,
                'city'    => $address->city->title,
                'region'  => $address->region ? $address->region->title : '',
                'country' => $address->country->iso_code,
                'lat'     => $address->lat,
                'lon'     => $address->lon
            ]
        );
        return $location;
    }

    /**
     * @param GeoLocation $geoLocation
     * @return Location|object|null
     * @throws \yii\base\InvalidConfigException
     */
    public static function createFromGeoLocation(GeoLocation $geoLocation): ?Location
    {
        $location = \Yii::createObject(
            [
                'class'   => Location::class,
                'city'    => $geoLocation->city ? $geoLocation->city->title : '',
                'region'  => $geoLocation->region ? $geoLocation->region->title : '',
                'country' => $geoLocation->country->iso_code,
                'lat'     => $geoLocation->lat,
                'lon'     => $geoLocation->lon
            ]
        );
        return $location;
    }
}