<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.08.16
 * Time: 10:31
 */

namespace common\models\factories;

use common\components\exceptions\BusinessException;
use common\models\Model3dPartProperties;
use Psr\Log\InvalidArgumentException;
use Yii;

class Model3dPartPropertiesFactory
{
    /**
     * Return stub properties object
     *
     * @param null $propertiesInfo
     * @return Model3dPartProperties
     */
    public static function createModel3dPartProperties($propertiesInfo = null)
    {
        if ($propertiesInfo) {
            if (!array_key_exists('volume', $propertiesInfo)) {
                throw new BusinessException('Invalid properties data: ' . json_encode($propertiesInfo));
            }
            $properties                   = new Model3dPartProperties();
            $propertiesInfo['volume']     = abs($propertiesInfo['volume']);
            $propertiesInfo['metric']     = 'mm';
            $propertiesInfo['created_at'] = dbexpr('NOW()');
            $properties->setData($propertiesInfo);
        } else {
            $properties = new Model3dPartProperties(
                [
                    'metric'   => 'mm',
                    'faces'    => 0,
                    'vertices' => 0,
                    'area'     => 0,
                    'volume'   => 0,
                    'width'    => 0,
                    'height'   => 0,
                    'length'   => 0
                ]
            );
        }
        return $properties;
    }

    /**
     * @param Model3dPartProperties $model3dPartProperties
     * @return Model3dPartProperties
     */
    public static function cloneModel3dPartProperties(Model3dPartProperties $model3dPartProperties)
    {
        /** @var Model3dPartProperties $newProperties */
        $newProperties             = Yii::createObject(Model3dPartProperties::class);
        $newProperties->attributes = $model3dPartProperties->attributes;
        return $newProperties;
    }
}