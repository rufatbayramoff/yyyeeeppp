<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.11.16
 * Time: 11:52
 */

namespace common\models\factories;

class UploadedFileListFactory
{
    /**
     * Create UploadedFile list by urls list
     *
     * @param $urlsList
     * @return array
     */
    public static function createUploadFilesList($urlsList)
    {
        $filesList = [];
        foreach ($urlsList as $url) {
            $uploadFile = UploadedFileFactory::createUploadedFile($url);
            $filesList[] = $uploadFile;
        }
        return $filesList;
    }
}