<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.08.16
 * Time: 16:59
 */

namespace common\models\factories;

use common\components\DateHelper;
use common\interfaces\Model3dBaseInterface;
use common\models\Model3dPartCncParams;
use common\models\Model3dPartProperties;
use common\models\Model3dReplicaImg;
use common\models\Model3dReplica;
use common\models\Model3dReplicaPart;
use frontend\models\user\UserFacade;

class Model3dReplicaFactory
{
    /**
     * @param Model3dBaseInterface $model3d
     * @param int[] $partsQty Index - part's file_id, key - qty
     * @return Model3dReplica
     * @internal param StoreUnit $storeUnit
     *
     */
    public static function createModel3dReplica(Model3dBaseInterface $model3d, array $partsQty = null): Model3dReplica
    {
        $model3dReplica = new Model3dReplica();
        $model3dReplica->attributes = $model3d->attributes;
        $model3dReplica->title = substr($model3d->title,0, 45);
        $model3dReplica->description = $model3d->getDescription();
        $model3dReplica->price_per_produce = $model3d->price_per_produce;
        $model3dReplica->price_currency  = $model3d->price_currency;
        $model3dReplica->created_at = DateHelper::now();
        $model3dReplica->updated_at = DateHelper::now();
        $model3dReplica->user_id = UserFacade::getCurrentUserId();
        $model3dReplica->user_session_id = UserFacade::getUserSession()->id ?? 1;
        $model3dReplica->is_active = 1; // Always active, can`t order deleted models
        $model3dReplica->price_per_produce = (float)$model3d->price_per_produce;
        $model3dReplica->setAnyTextureAllowed($model3d->isAnyTextureAllowed());

        $model3dReplica->id = null;
        $model3dReplica->original_model3d_id = $model3d->getSourceModel3d()->id;
        $model3dReplica->populateRelation('originalModel3d', $model3d->getSourceModel3d());
        $model3dReplicaParts = [];
        $model3dReplicaImgs = [];
        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {

            $partQty = $partsQty === null ? $model3dPart->qty : ($partsQty[$model3dPart->file->id] ?? 0);
            if (!$partQty) {
                continue;
            }

            $model3dReplicaPart = new Model3dReplicaPart();
            $model3dReplicaPart->attributes = $model3dPart->attributes;
            $model3dReplicaPart->qty = $partQty;
            $model3dReplicaPart->converted_from_part_id = $model3dPart->converted_from_part_id;
            $model3dReplicaPart->is_common_view = (int)$model3dPart->is_common_view;
            $model3dReplicaPart->original_qty = $model3dPart->getOriginalQty();
            if ($model3dPart->getSize()) {
                $model3dReplicaPart->setSize($model3dPart->getSize());
            }

            $model3dReplicaPart->id = null;
            $model3dReplicaPart->setAttachedModel3d($model3dReplica);
            $model3dReplicaPart->setOriginalModel3dPart($model3dPart->getOriginalModel3dPartObj());
            $model3dReplicaPart->original_model3d_part_id = $model3dPart->getOriginalModel3dPartObj()->id;

            if ($model3dPart->model3dPartProperties) {
                $model3dReplicaPartProperties = new Model3dPartProperties();
                $model3dReplicaPartProperties->attributes = $model3dPart->model3dPartProperties->attributes;
                $model3dReplicaPartProperties->volumeDirect = $model3dPart->model3dPartProperties->volumeDirect;
                $model3dReplicaPart->setModel3dPartProperties($model3dReplicaPartProperties);
            }

            if ($model3dPart->model3dPartCncParams) {
                $model3dReplicaPartCncParams = new Model3dPartCncParams();
                $model3dReplicaPartCncParams->attributes = $model3dPart->model3dPartCncParams->attributes;
                $model3dReplicaPart->setModel3dPartCncParams($model3dReplicaPartCncParams);
            }

            if ($model3dPart->getTexture()) {
                $model3dTexture = Model3dTextureFactory::createModel3dTexture();
                $model3dTexture->setTexture($model3dPart->getTexture());
                $model3dReplicaPart->setTexture($model3dTexture);
            }

            $model3dReplicaParts[] = $model3dReplicaPart;
        }
        foreach ($model3d->getActiveModel3dImages() as $model3dImg) {
            $model3dReplicaImg = new Model3dReplicaImg();
            $model3dReplicaImg->attributes = $model3dImg->attributes;
            $model3dReplicaImg->id = null;
            $model3dReplicaImg->setAttachedModel3d($model3dReplica);
            $model3dReplicaImg->setOriginalModel3dImg($model3dImg->getOriginalModel3dImg());
            $model3dReplicaImg->original_model3d_img_id = $model3dImg->getOriginalModel3dImg()->id;
            $model3dReplicaImgs[] = $model3dReplicaImg;
        }
        $model3dReplica->setModel3dParts($model3dReplicaParts);
        $model3dReplica->setModel3dImgs($model3dReplicaImgs);
        if ($model3d->getKitTexture()) {
            $newTexture = Model3dTextureFactory::createModel3dTexture();
            $newTexture->setTexture($model3d->getKitTexture());
            $model3dReplica->setKitModel3dTexture($newTexture);
        } else {
            $model3dReplica->model3d_texture_id = null;
        }

        return $model3dReplica;
    }
}