<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.07.16
 * Time: 10:11
 */

namespace common\models\factories;

use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialRepository;
use common\services\PrinterMaterialService;
use DomainException;
use Yii;

class Model3dTextureFactory
{
    CONST DEFAULT_COLOR = 'White';
    CONST DEFAULT_MATERIAL = 'pla';

    /**
     * @return integer
     * @throws \yii\base\InvalidConfigException
     * @throws \DomainException
     */
    protected static function getDefaultMaterialId()
    {
        $printerMaterialRepository = Yii::createObject(PrinterMaterialRepository::class);
        $material = $printerMaterialRepository->getByCode(self::DEFAULT_MATERIAL);
        if (!$material) {
            throw new DomainException('Can`t get default printer material.');
        }
        return $material->id;
    }

    /**
     * @return integer
     * @throws \yii\base\InvalidConfigException
     * @throws \DomainException
     */
    protected static function getDefaultColorId()
    {
        /** @var PrinterColorRepository $printerColorRepository */
        $printerColorRepository = Yii::createObject(PrinterColorRepository::class);
        $color = $printerColorRepository->getByCode(self::DEFAULT_COLOR);
        if (!$color) {
            throw new DomainException('Can`t get default printer color.');
        }
        return $color->id;
    }

    public static function createModel3dTexture($isMulticolor = false)
    {
        $texture = new Model3dTexture();

        if ($isMulticolor) {
            $texture->printer_color_id = PrinterColor::MULTICOLOR_ID;
            $texture->printerColor;    // init relation
            $texture->printer_material_id = PrinterMaterialService::getDefaultMulticolorMaterial()->id;
            $texture->printerMaterial; // init relation
        } else {
            $texture->printer_color_id = self::getDefaultColorId();
            $texture->printerColor;    // init relation
            $texture->printer_material_id = self::getDefaultMaterialId();
            $texture->printerMaterial; // init relation
        }
        return $texture;
    }

    /**
     * @param PrinterMaterialGroup $materialGroup
     * @param PrinterMaterial|null $material
     * @param PrinterColor $color
     * @return Model3dTexture
     */
    public static function createTexture(PrinterMaterialGroup $materialGroup, $material, PrinterColor $color)
    {
        $texture = self::createModel3dTexture();
        $texture->printerMaterialGroup = $materialGroup;
        $texture->printerMaterial = $material;
        $texture->printerColor = $color;
        return $texture;
    }
}