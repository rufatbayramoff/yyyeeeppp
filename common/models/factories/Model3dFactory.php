<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.06.16
 * Time: 14:10
 */

namespace common\models\factories;

use common\components\ArchiveManager;
use common\components\DateHelper;
use common\components\UuidHelper;
use common\interfaces\Model3dBaseInterface;
use common\models\ProductCommon;
use common\models\SiteTag;
use common\models\StoreUnit;
use common\modules\product\interfaces\ProductInterface;
use common\models\File;
use common\models\Model3d;
use common\models\Model3dImg;
use common\models\Model3dPart;
use common\models\Model3dPartCncParams;
use common\models\Model3dPartProperties;
use common\models\repositories\UserSessionRepository;
use common\models\user\UserIdentityProvider;
use common\models\UserSession;
use common\services\Model3dImageService;
use common\services\Model3dPartService;
use frontend\models\model3d\Model3dFacade;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use lib\MeasurementUtil;
use lib\money\Currency;
use yii\base\Component;

class Model3dFactory extends Component
{
    /**
     * @var FrontUser
     */
    public $user = false;

    /**
     * @var UserSession
     */
    public $userSession = null;

    /**
     * @var  ArchiveManager
     */
    public $archiveManager = null;

    /**
     * @var Model3dImageService
     */
    public $model3dImageService = null;

    public function injectDependencies(
        UserIdentityProvider  $userIdentityProvider,
        UserSessionRepository $userSessionRepository,
        ArchiveManager        $archiveManager,
        Model3dImageService   $model3dImageService
    ): void
    {
        if ($this->user === false) {
            $this->user = $userIdentityProvider->getUser();
        }
        if ($this->userSession === null) {
            $this->userSession = $userSessionRepository->getCurrent();
        }
        if ($this->archiveManager === null) {
            $this->archiveManager = $archiveManager;
        }
        if ($this->model3dImageService === null) {
            $this->model3dImageService = $model3dImageService;
        }
    }


    /**
     * @param File[] $filesList
     * @param array $properties
     * @return Model3d
     * @throws \Exception
     * @throws \lib\file\UploadException
     */
    public function createModel3dByFilesList($filesList, $properties = [])
    {
        $productCommon        = new ProductCommon();
        $productCommon->uid   = ProductCommon::generateUid();
        $productCommon->title = '';
        if (array_key_exists('title', $properties)) {
            $productCommon->title = $properties['title'];
            unset($properties['title']);
        }
        $productCommon->price_currency = Currency::USD;
        $productCommon->single_price   = 0;
        $productCommon->is_active      = 1;
        $productCommon->description    = '';
        if (array_key_exists('description', $properties)) {
            $productCommon->description = $properties['description'];
            unset($properties['description']);
        }
        $model3d                     = new Model3d();
        $model3d->uuid               = UuidHelper::generateUuid();
        $model3d->product_common_uid = $productCommon->uid;
        $model3d->populateRelation('productCommon', $productCommon);
        $productCommon->populateRelation('model3d', $model3d);

        $model3d->user_session_id = $this->userSession->id;
        $model3d->source_details  = 'null';
        $model3d->populateRelation('userSession', $this->userSession);
        $model3d->productCommon->user_id = $this->user ? $this->user->id : null;
        if ($this->user) {
            $model3d->populateRelation('user', $this->user);
            if ($this->user->company) {
                $model3d->productCommon->company_id = $this->user->company->id;
                $model3d->productCommon->populateRelation('comapny', $this->user->company);
            }
        }
        $model3d->is_printer_ready              = 0;
        $model3d->dimensions                    = '';
        $model3d->productCommon->product_status = ProductInterface::STATUS_DRAFT;
        $model3d->stat_views                    = 0;
        $model3d->scale                         = 1;
        $model3d->model_units                   = MeasurementUtil::MM;
        $model3d->productCommon->created_at     = DateHelper::now();
        $model3d->productCommon->updated_at     = DateHelper::now();

        $title           = '';
        $coverFileExists = false;
        $coverFileStl    = null;
        foreach ($filesList as $file) {
            if ($this->archiveManager->isArchive($file)) {
                $filesFromArchive = $this->archiveManager->unpackFiles($file);
                $filesList        = array_merge($filesList, $filesFromArchive);
            }
        }

        foreach ($filesList as $file) {
            /** @var File $file */
            if (!$title) {
                $title = $file->getFileNameWithoutExtension();
            }
            $type = Model3dFacade::getModel3dFileType($file->getFileName());
            if ($type === Model3dFacade::TYPE_IMAGE) {
                $coverFileExists = true;
                /** @var Model3dImageFactory $model3dImageFactory */
                $this->model3dImageService->addModel3dImgFile($model3d, $file);
            }
            if ($type === Model3dFacade::TYPE_MODEL3D) {
                $model3dPart = Model3dPartService::addModel3dPartFile($model3d, $file);
                if (!$coverFileExists) {
                    $coverFileStl = $model3dPart->file;
                }
            }
        }
        if (!$coverFileExists && $coverFileStl) {
            $model3d->setCoverFile($coverFileStl);
        }

        if (mb_strlen($title) > 45) {
            $model3d->title = mb_substr($title, 0, 42) . '...';
        } else {
            $model3d->title = $title;
        }
        $model3dTexture = Model3dTextureFactory::createModel3dTexture($model3d->isMulticolorFormatModel());
        $model3d->setKitModel3dTexture($model3dTexture);
        $model3d->setAttributes($properties);

        $storeUnit = new StoreUnit();
        $storeUnit->populateRelation('model3d', $model3d);
        $model3d->populateRelation('storeUnit', $storeUnit);

        if (array_key_exists('tags', $properties)) {
            $siteTags = [];
            foreach ($properties['tags'] as $tag) {
                $tag = trim(mb_strtolower($tag));
                $siteTag = SiteTag::find()->where(['text' => $tag])->withoutStaticCache()->one();
                if (!$siteTag) {
                    $siteTag       = new SiteTag();
                    $siteTag->init();
                    $siteTag->text = $tag;
                    $siteTag->safeSave();
                }
                $siteTags[] = $siteTag;
            }
            $model3d->setSiteTags($siteTags);
        }

        return $model3d;
    }

    /**
     * @param Model3dBaseInterface $model3dOriginal
     * @return Model3d
     * @throws \yii\base\Exception
     * @internal param StoreUnit $storeUnit
     */
    public static function createModel3dByModel3d(Model3dBaseInterface $model3dOriginal): Model3d
    {
        $productCommon                 = new ProductCommon();
        $productCommon->uid            = ProductCommon::generateUid();
        $productCommon->title          = $model3dOriginal->productCommon->title;
        $productCommon->price_currency = $model3dOriginal->productCommon->price_currency;
        $productCommon->single_price   = $model3dOriginal->productCommon->single_price;
        $productCommon->is_active      = $model3dOriginal->productCommon->is_active;
        $model3d                       = new Model3d();
        $model3d->uuid                 = UuidHelper::generateUuid();
        $model3d->product_common_uid   = $productCommon->uid;
        $model3d->populateRelation('productCommon', $productCommon);
        $productCommon->populateRelation('model3d', $model3d);

        $model3d->productCommon->user_id        = UserFacade::getCurrentUserId();
        $model3d->productCommon->company_id     = $model3dOriginal->getCompanyManufacturer() ? $model3dOriginal->getCompanyManufacturer()->id : null;
        $model3d->source_details                = $model3dOriginal->source_details ?? 'null';
        $model3d->user_session_id               = UserFacade::getUserSession()->id ?? 1;
        $model3d->productCommon->product_status = ProductInterface::STATUS_DRAFT;
        $model3d->productCommon->single_price   = $model3dOriginal->price_per_produce;
        $model3d->productCommon->created_at     = $model3dOriginal->productCommon->created_at;
        $model3d->productCommon->updated_at     = DateHelper::now();

        $model3d->setAnyTextureAllowed($model3dOriginal->isAnyTextureAllowed());

        $model3d->id                  = null;
        $model3d->original_model3d_id = $model3dOriginal->getSourceModel3d()->id;
        $model3d->populateRelation('originalModel3d', $model3dOriginal->getSourceModel3d());
        $model3dParts = [];
        $model3dImgs  = [];
        foreach ($model3dOriginal->getActiveModel3dParts() as $model3dPartOriginal) {
            $model3dPart                 = new Model3dPart();
            $model3dPart->attributes     = $model3dPartOriginal->attributes;
            $model3dPart->qty            = $model3dPartOriginal->qty;
            $model3dPart->is_common_view = (int)$model3dPartOriginal->is_common_view;
            $model3dPart->id             = null;
            $model3dPart->setAttachedModel3d($model3d);
            $model3dPart->populateRelation('originalModel3dPart', $model3dPartOriginal->getOriginalModel3dPartObj());
            $model3dPart->original_model3d_part_id = $model3dPartOriginal->getOriginalModel3dPartObj()->id;

            if ($model3dPartOriginal->model3dPartProperties) {
                $model3dPartProperties               = new Model3dPartProperties();
                $model3dPartProperties->attributes   = $model3dPartOriginal->model3dPartProperties->attributes;
                $model3dPartProperties->volumeDirect = $model3dPartOriginal->model3dPartProperties->volumeDirect;
                $model3dPartProperties->id           = null;
                $model3dPartProperties->safeSave();
                $model3dPart->setModel3dPartProperties($model3dPartProperties);
            }

            if ($model3dPartOriginal->model3dPartCncParams) {
                $model3dReplicaPartCncParams             = new Model3dPartCncParams();
                $model3dReplicaPartCncParams->attributes = $model3dPartOriginal->model3dPartCncParams->attributes;
                $model3dPart->setModel3dPartCncParams($model3dReplicaPartCncParams);
            }

            if ($model3dPartOriginal->getTexture()) {
                $model3dTexture = Model3dTextureFactory::createModel3dTexture();
                $model3dTexture->setTexture($model3dPartOriginal->getTexture());
                $model3dPart->setTexture($model3dTexture);
            }

            $model3dParts[] = $model3dPart;
        }

        foreach ($model3dOriginal->getActiveModel3dImages() as $model3dImgOrig) {
            $model3dImg             = new Model3dImg();
            $model3dImg->attributes = $model3dImgOrig->attributes;
            $model3dImg->id         = null;
            $model3dImg->setAttachedModel3d($model3d);
            $model3dImg->populateRelation('originalModel3dImg', $model3dImgOrig->getOriginalModel3dImg());
            $model3dImg->original_model3d_img_id = $model3dImgOrig->id;
            $model3dImgs[]                       = $model3dImg;
        }
        $model3d->setModel3dParts($model3dParts);
        $model3d->setModel3dImgs($model3dImgs);

        if ($model3dOriginal->getKitTexture()) {
            $newTexture = Model3dTextureFactory::createModel3dTexture();
            $newTexture->setTexture($model3dOriginal->getKitTexture());
            $model3d->setKitModel3dTexture($newTexture);
        } else {
            $model3d->model3d_texture_id = null;
        }

        return $model3d;
    }
}
