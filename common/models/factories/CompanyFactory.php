<?php
/**
 * User: nabi
 */

namespace common\models\factories;


use common\components\DateHelper;
use common\models\Company;
use common\models\Ps;
use common\models\User;
use common\models\UserLocation;
use common\modules\company\models\CompanyEntity;

class CompanyFactory
{

    /**
     * @param string $title
     * @param string $description
     * @param User $user
     * @param UserLocation $location
     * @param string $url
     * @return Company
     */
    public function create(string $title,
        string $description,
        User $user,
        UserLocation $location,
        string $url): Company
    {
        $model = new Company();
        $model->title = $title;
        $model->user_id = $user->id;
        $model->description = $description;
        $model->phone_status = Company::PHONE_STATUS_NEW;
        $model->is_designer = Company::IS_NOT_DESIGNER;
        $model->populateRelation('location', $location);
        $model->location_id = $location->id;
        $model->country_id = $location->country_id;
        $model->phone_country_iso = $location->country->iso_code;
        $model->moderator_status = Ps::MSTATUS_NEW;
        $model->created_at = DateHelper::now();
        $model->url = $url;
        $model->url_changes_count = 0;
        return $model;
    }


    public function createCompany(CompanyEntity $entity)
    {
        // create user
        $user = new User();
        $user->email = $entity->email;
        $user->status = User::STATUS_UNCONFIRMED;
        $user->password_hash = '-';

        $company = new Ps();
    }

    /**
     * @param $user
     * @return Company
     */
    public function createCompanyByUser($user)
    {
        $company = new Company();
        $company->populateRelation('user', $user);
        $company->user_id = $user->id ? $user : User::USER_ANONIM;
        $company->moderator_status = Company::MSTATUS_DRAFT;
        $company->phone_status = Ps::PHONE_STATUS_NEW;
        $company->created_at = DateHelper::now();
        $company->updated_at = DateHelper::now();
        return $company;
    }
}