<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.07.18
 * Time: 15:13
 */

namespace common\models\factories;

use common\models\UserAddress;
use lib\geo\models\Location;

class UserAddressFactory
{
    /**
     * @param Location $location
     * @return UserAddress
     */
    public function createByLocation(Location $location)
    {
        $address = new UserAddress();
        $address->populateRelation('country', $location->getGeoCountry());
        $address->country_id = $address->country->id;
        $address->city = $location->city;
        $address->region = $location->region;
        $address->lat = $location->lat;
        $address->lon = $location->lon;
        return $address;
    }
}