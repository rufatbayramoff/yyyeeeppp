<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.11.17
 * Time: 15:29
 */

namespace common\models\factories;

use common\models\DeliveryType;
use common\models\CompanyService;
use frontend\models\ps\PsMachineDeliveryInfo;
use frontend\models\ps\PsMachineDeliveryInfoLocation;

class PsMachineDeliveryInfoFactory
{
    public function createByPsMachine(CompanyService $psMachine)
    {
        $carriers = $deliveryTypes = [];
        $pickupDelivery = null;
        foreach ($psMachine->deliveryTypess as $type) {
            $deliveryTypeCode = $type->deliveryType->code;
            $deliveryTypes[] = $deliveryTypeCode;
            $carriers[$deliveryTypeCode] = $type->carrier;
            if ($deliveryTypeCode === DeliveryType::PICKUP) {
                $pickupDelivery = $type;
            }
        }

        $psMachineDeliveryInfo = new PsMachineDeliveryInfo();
        $psMachineDeliveryInfo->carriers = $carriers;
        $psMachineDeliveryInfo->deliveryTypes = $deliveryTypes;
        $psMachineDeliveryInfo->workTime = $pickupDelivery->comment??'';
        $psMachineDeliveryInfo->location = new PsMachineDeliveryInfoLocation();
        $psMachineDeliveryInfo->location->address = $psMachine->location->address;
        $psMachineDeliveryInfo->location->lat = $psMachine->location->lat;
        $psMachineDeliveryInfo->location->lon = $psMachine->location->lon;
        $psMachineDeliveryInfo->location->countryIso = $psMachine->location->country->iso_code;

        return $psMachineDeliveryInfo;
    }
}