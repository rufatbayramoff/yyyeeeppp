<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.06.18
 * Time: 16:07
 */

namespace common\models\factories;

use common\components\DateHelper;
use common\models\User;
use common\models\UserProfile;
use lib\money\Currency;

class UserFactory
{
    /**
     * @param $email
     * @return User|object
     * @throws \yii\base\InvalidConfigException
     */
    public function createUserByEmail($email)
    {
        $user = \Yii::createObject(User::class);
        $username = substr($email, 0, strpos($email, '@'));
        if (User::findOne(['username' => $username])) {
            $username = $username . rand(100, 99999);
        }
        $user->username = $username;
        $user->email = $email;
        $user->status = User::STATUS_DRAFT;
        $user->password_hash = '-';
        $user->auth_key = '-';
        $user->updated_at = $user->created_at = time();
        $user->lastlogin_at = 0;
        $user->allow_incoming_quotes = 1;

        $userProfile = new UserProfile();
        $userProfile->current_lang = 'en-US';
        $userProfile->current_currency_iso = Currency::USD;
        $userProfile->timezone_id = 'America/Los_Angeles';
        $userProfile->populateRelation('user', $userProfile);
        $user->populateRelation('userProfile', $userProfile);

        return $user;
    }
}