<?php
/**
 * Created by mitaichik
 */

namespace common\models\factories;


use common\components\DateHelper;
use common\components\FileTypesHelper;
use common\models\StoreOrderReview;
use common\models\StoreOrderReviewShare;
use common\models\User;
use common\services\ReviewService;

class ReviewShareFactory
{
    /**
     * Crete share based on review and user.
     *
     * @param StoreOrderReview $review
     * @param User|null $user
     * @return StoreOrderReviewShare
     */
    public function create(StoreOrderReview $review, User $user = null) : StoreOrderReviewShare
    {
        $files = FileTypesHelper::filterImages($review->reviewFilesSort);

        $share = new StoreOrderReviewShare();
        $share->review_id = $review->id;
        $share->user_id = $user->id ?? null;
        $share->photo_id = $files[0]->id ?? null;
        $share->text = $this->resoveText($review, $user);
        $share->created_at = DateHelper::now();
        $share->status = StoreOrderReviewShare::STATUS_NEW;
        return $share;
    }


    /**
     * Resolve text of share.
     *
     * @param StoreOrderReview $review
     * @param User|null $user
     * @return string
     */
    private function resoveText(StoreOrderReview $review, User $user = null) : string
    {
        $ps = $review->ps;

        $header = $user && ($ps->user->equals($user))
            ? _t('share', 'Look at what I made on @Treatstock')
            : _t('share', 'Review');

        if ($user && $review->order->user->equals($user)) {
            $header = _t('share', 'Look at what I had made on @Treatstock');
        }

        $rating = ReviewService::calculateAvgRating($review);
        $ratingLabel = str_repeat ('★', (int) $rating) . str_repeat('☆', 5 - (int) $rating);

        // $text = "{$ps->title} : {$ratingLabel} ($rating/5) \n{$header}";
        $text = "{$ratingLabel} ($rating/5) \n{$header}";

        $text.= $review->comment
            ? ":\n{$review->comment}"
            : '.';

        if (!$review->isAnswerRejected() && !empty($review->answer)) {
            $text .= "\n\n{$review->ps->title}: $review->answer";
        }

        return $text;
    }
}