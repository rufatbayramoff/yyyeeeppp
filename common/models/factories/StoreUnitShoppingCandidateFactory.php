<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.11.16
 * Time: 16:21
 */

namespace common\models\factories;

use common\models\Model3d;
use common\models\StoreUnitShoppingCandidate;
use common\models\UserSession;
use DateTime;
use DateTimeZone;
use frontend\models\user\UserFacade;
use Yii;


class StoreUnitShoppingCandidateFactory
{
    /**
     * @param Model3d $model3d
     * @param $type
     * @return StoreUnitShoppingCandidate
     */
    public static function createStoreUnitShoppingCandidate(Model3d $model3d, $type)
    {
        /** @var StoreUnitShoppingCandidate $shoppingCandidate */
        $shoppingCandidate = Yii::createObject(StoreUnitShoppingCandidate::class);
        $shoppingCandidate->populateRelation('storeUnit', $model3d->storeUnit);
        $shoppingCandidate->store_unit_id = $model3d->storeUnit->id  ;
        $shoppingCandidate->type = $type;
        $shoppingCandidate->create_date = (new DateTime('now' ,new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $shoppingCandidate->view_date = (new DateTime('now' ,new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $userSession =  UserFacade::getUserSession();
        $shoppingCandidate->user_session_id = $userSession->id;
        return $shoppingCandidate;
    }
}