<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.07.17
 * Time: 17:16
 */

namespace common\models\factories;

use common\components\DateHelper;
use common\models\populators\PsMachinePopulator;
use common\models\Ps;
use common\models\CompanyService;
use common\models\PsMachineDelivery;
use common\models\UserAddress;
use common\models\UserLocation;

class PcMachineFactory
{
    /**
     * @var PsMachinePopulator
     */
    public $companyServicePopulator;

    /**
     * @param PsMachinePopulator $psMachinePopulator
     */
    public function injectDependencies(PsMachinePopulator $psMachinePopulator)
    {
        $this->companyServicePopulator = $psMachinePopulator;
    }

    /**
     * @param Ps $ps
     * @param $data
     * @return CompanyService
     */
    public function createPsMachine(Ps $ps, $data)
    {
        $psMachine = new CompanyService();
        $psMachine->created_at = DateHelper::now();
        $psMachine->updated_at = DateHelper::now();
        $psMachine->moderator_status = CompanyService::MODERATOR_STATUS_NEW;
        $psMachine->visibility = CompanyService::VISIBILITY_EVERYWHERE;
        $psMachine->setPs($ps);
        $this->companyServicePopulator->populate($psMachine, $data);
        return $psMachine;
    }
}