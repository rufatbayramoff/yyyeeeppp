<?php
namespace common\models;

use Yii;

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>  
 */
class UserTaxInfoHistory extends \common\models\base\UserTaxInfoHistory
{
    const ACTION_APPROVE = 'approve';
    const ACTION_DECLINE = 'decline';
}