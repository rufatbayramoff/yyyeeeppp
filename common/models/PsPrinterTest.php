<?php

namespace common\models;

use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use Yii;
use yii\helpers\Json;
use yii\validators\FileValidator;
use yii\web\UploadedFile;

/**
 * Edit this file
 * This is the model class for table "ps_printer_test".
 */
class PsPrinterTest extends \common\models\base\PsPrinterTest
{
    const SCENARIO_START_TEST = 'startTest';

    const STATUS_NEW     = 'new';
    const STATUS_REPEAT  = 'repeat';
    const STATUS_CHECKED = 'checked';
    const STATUS_FAILED  = 'failed';

    const TYPE_NO           = 'no';
    const TYPE_COMMON       = 'common';
    const TYPE_PROFESSIONAL = 'professional';

    /**
     * @var UploadedFile[]
     */
    public $testedPhotos = [];

    /** @var string allow set current form name */
    public $currentFormName = '';

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_START_TEST => ['type', 'testedPhotos']
        ]);
    }

    /**
     * Validate uploaded photos
     */
    public function validateTestedPhotos()
    {
        if ($this->scenario != self::SCENARIO_START_TEST) {
            return;
        }

        if (!$this->testedPhotos) {
            $this->addError("testedPhotos", _t('site.ps.test', "Please, upload foto of printed model."));
            return;
        }

        if (count($this->testedPhotos) > 50) {
            $this->addError("testedPhotos", _t('site.ps.test', "Max count of photos is 50."));
            return;
        }

        $fileValidator = new FileValidator(['extensions' => 'png, jpg, jpeg, gif']);

        foreach ($this->testedPhotos as $testPhoto) {
            if (!$fileValidator->validate($testPhoto)) {
                $this->addError("testedPhotos", _t('site.ps.test', "Please, upload correct image."));
                return;
            }
        }
    }

    /**
     * @param PsPrinter $psPrinter
     * @return PsPrinterTest
     */
    public static function create(PsPrinter $psPrinter, TsInternalPurchase $lastPayedTsPurchase)
    {
        $test                = new self;
        $test->ps_printer_id = $psPrinter->id;
        $test->status        = self::STATUS_NEW;
        $test->type          = self::TYPE_PROFESSIONAL;
        $test->ts_internal_purchase_uid = $lastPayedTsPurchase->uid;
        return $test;
    }

    /**
     * Create and sae test or update it if test already exist
     */
    public function startTest()
    {
        $fileFactory    = Yii::createObject(FileFactory::class);
        $fileRepository = Yii::createObject(FileRepository::class);
        $fileIds        = $this->getFileIds();
        foreach ($this->testedPhotos as $index => $testPhoto) {
            $file = $fileFactory->createFileFromUploadedFile($testPhoto);
            $fileRepository->save($file);
            $fileIds[] = $file->id;
        }
        $this->setFileIds($fileIds);
        $this->status = self::STATUS_NEW;

        AssertHelper::assertSave($this);
    }

    /**
     * @return string
     */
    public function formName()
    {
        return $this->currentFormName;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function loadFromPost(array $data)
    {
        $this->testedPhotos = UploadedFile::getInstances($this, 'testedPhotos');
        return parent::load($data);
    }

    /**
     * @param File[] $files
     */
    public function updatePhotoFiles($post, $files)
    {
        $fileFactory    = \Yii::createObject(FileFactory::class);
        $fileRepository = \Yii::createObject(FileRepository::class);

        $currentIds   = $this->getFileIds();
        $currentFiles = $this->getFiles();
        $currentUuids = [];
        foreach ($currentFiles as $currentFile) {
            $currentUuids[$currentFile->uuid] = $currentFile;
        }


        $formName = $this->formName();
        if (array_key_exists($formName, $post)) {
            $formData = $post[$formName];
            if (array_key_exists('imageFiles', $formData)) {
                foreach ($formData['imageFiles'] as $key => $value) {
                    if ($value === 'deleted') {
                        if (array_key_exists($key, $currentUuids)) {
                            $file       = $currentUuids[$key];
                            $currentIds = array_diff($currentIds, [$file->id]);
                            $fileRepository->delete($file);
                        }
                    }
                }
            }
        }

        if ($files) {
            $imageFiles = $fileFactory->createFilesFromUploadedFiles($files);
            foreach ($imageFiles as $file) {
                $file->setOwner(PsPrinterTest::class, 'image_file_ids');
                $fileRepository->save($file);
                $currentIds[] = $file->id;
            }
        }
        $this->setFileIds($currentIds);
        $this->safeSave();
    }

    /**
     *
     */
    private function getFileIds()
    {
        return $this->image_file_ids ? Json::decode($this->image_file_ids) : [];
    }

    /**
     * @param array $ids
     */
    private function setFileIds(array $ids)
    {
        $this->image_file_ids = Json::encode($ids);
    }

    /**
     * Return files
     * @return File[]
     */
    public function getFiles()
    {
        $fileIds = Json::decode($this->image_file_ids);
        return File::findAll($fileIds);
    }

    /**
     * Is test successful checked
     * @return bool
     */
    public function isChecked(): bool
    {
        return $this->status == self::STATUS_CHECKED;
    }

    public function isRepeat(): bool
    {
        return $this->status == self::STATUS_REPEAT;
    }

    /**
     * Is test successful checked
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->status == self::STATUS_NEW;
    }

}