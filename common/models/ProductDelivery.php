<?php

namespace common\models;

/**
 * Class ProductDelivery
 *
 * @package common\models
 */
class ProductDelivery extends ProductExpressDelivery
{
    public function attributeLabels()
    {
        return array_merge(
            parent::attributes(),
            [
                'country_id'     => \Yii::t('site.store', 'Express delivery country'),
                'first_item'     => \Yii::t('app', 'First item price'),
                'following_item' => \Yii::t('app', 'Following item price'),
            ]
        );
    }

    public function getExpressDeliveryByQty($qty)
    {
        return $this->first_item + $this->following_item * ($qty - 1);
    }

}