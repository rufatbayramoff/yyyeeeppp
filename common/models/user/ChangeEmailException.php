<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\models\user;


use yii\base\Exception;

/**
 * Exception on change email problem
 * @package common\models\user
 */
class ChangeEmailException extends Exception
{

}