<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.09.17
 * Time: 17:45
 */

namespace common\models\user;

use frontend\models\user\UserFacade;

class UserIdentityProvider
{
    protected $user;

    public function getUser()
    {
        return $this->user??UserFacade::getCurrentUser();
    }

    public function setUser($user)
    {
        $this->user = $user;
    }
}