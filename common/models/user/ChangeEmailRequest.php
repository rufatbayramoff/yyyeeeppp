<?php

namespace common\models\user;

use common\components\exceptions\AssertHelper;
use common\models\User;
use DateTime;
use DateTimeZone;
use Yii;
use common\components\ActiveQuery;

/**
 * Change email user request
 */
class ChangeEmailRequest extends \common\models\base\UserEmailChangeRequest
{
    /**
     * This status set for new requests
     */
    const STATUS_UNCONFIRMED = 2;

    /**
     * This status set for realized request, after user follow to link from e-mail
     */
    const STATUS_CONFIRMED = 1;

    /**
     * This status request have then other reques (for this user) was confirm, or time of request's actual is end
     */
    const STATUS_UNREALIZED = 3;

    /**
     * @return ChangeEmailRequestQuery
     */
    public static function find()
    {
        return new ChangeEmailRequestQuery(get_called_class());
    }

    /**
     * Create new request
     * @param User $user
     * @param $newEmail
     * @return ChangeEmailRequest
     */
    public static function create(User $user, $newEmail)
    {
        // mark previos requests as unrealized
        self::markUnconfirmedAsUnrealized($user->id);

        $model = new ChangeEmailRequest();
        $model->user_id = $user->id;
        $model->email = $newEmail;
        $model->confirm_key = Yii::$app->security->generateRandomString();
        $model->status = self::STATUS_UNCONFIRMED;
        $model->create_at = (new DateTime('now' ,new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        AssertHelper::assertSave($model);
        return $model;
    }

    /**
     *
     */
    public function confirm()
    {
        $this->status = self::STATUS_CONFIRMED;
        $this->confirm_at = (new DateTime('now' ,new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        AssertHelper::assertSave($this);

        // mark other unconfirmed requests as unrealized
        self::markUnconfirmedAsUnrealized($this->user_id);
    }


    private static function markUnconfirmedAsUnrealized($userId)
    {
        AssertHelper::assert($userId, 'Invalid params userId');
        self::updateAll(['status' => self::STATUS_UNREALIZED], ['status' => self::STATUS_UNCONFIRMED, 'user_id' => $userId]);
    }
}

/**
 * Class ChangeEmailRequestQuery
 * @package common\models\user
 */
class ChangeEmailRequestQuery extends ActiveQuery
{
    /**
     * @param User $user
     * @return $this
     */
    public function forUser(User $user)
    {
        $this->andWhere(['user_id' => $user->id]);
        return $this;
    }

    /**
     * @return $this
     */
    public function unconfirmed()
    {
        $this->andWhere(['status' => ChangeEmailRequest::STATUS_UNCONFIRMED]);
        return $this;
    }

    /**
     * @param string $key
     * @return $this
     */
    public function byKey($key)
    {
        $this->andWhere(['confirm_key' => $key]);
        return $this;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function byEmail($email)
    {
        $this->andWhere(['email' => $email]);
        return $this;
    }
}
