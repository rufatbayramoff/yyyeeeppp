<?php

namespace common\models;

/**
 * Class WikiMachineProperty
 * @package common\models
 * @property string $filterValue
 * @property string $valueLowCase
 */
class WikiMachineProperty extends \common\models\base\WikiMachineProperty
{
    public function getCode()
    {
        return $this->wikiMachineAvailableProperty->code;
    }

    public function getTitle()
    {
        return $this->wikiMachineAvailableProperty->title;
    }

    public function getFilterValue()
    {
        return mb_strtolower(str_replace('/','',$this->value));
    }

    public function getValueLowCase()
    {
        return mb_strtolower($this->value);
    }
}