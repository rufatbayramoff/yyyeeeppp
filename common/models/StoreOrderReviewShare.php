<?php

namespace common\models;
use common\components\FileTypesHelper;

/**
 * Class StoreOrderReviewShare
 * @package common\models
 */
class StoreOrderReviewShare extends \common\models\base\StoreOrderReviewShare
{
    public const STATUS_NEW = 'new';
    public const STATUS_APPROVED = 'approved';
    public const STATUS_REJECTED = 'rejected';

    public const SOCIAL_TYPES = [
        'link',
        'google',
        'facebook',
        'facebook_im',
        'reddit',
        'linkedin',
        'affiliate'
    ];

    public const SOCIAL_TYPES_LABELS = [
        'link' => 'link',
        'google' => 'google',
        'facebook' => 'facebook',
        'facebook_im' => 'facebook_im',
        'reddit' => 'reddit',
        'linkedin' => 'linkedin',
        'affiliate' => 'affiliate'
    ];

    public const STATUS_LABELS = [
        'new' => 'new',
        'approved' => 'approved',
        'rejected' => 'rejected',
    ];

    /**
     * Is user can edit share on create it,
     * i. e. change share text and upload own photo.
     *
     * @param User|null $user
     * @return bool
     */
    public function isUserCanEdit(User $user = null) : bool
    {
        return $user && $this->review->order->user->equals($user);
    }

    /**
     * Is share can be viewed.
     *
     * @return bool
     */
    public function isCanShow()
    {
        return ($this->status === self::STATUS_NEW || $this->status === self::STATUS_APPROVED)
            && in_array($this->review->status, [StoreOrderReview::STATUS_NEW, StoreOrderReview::STATUS_MODERATED]);
    }


    /**
     * Return main photo for share.
     *
     * @return File|null
     */
    public function getMainPhoto() : ?File
    {
        return $this->photo;
    }

    /**
     * Return all photos for share.
     *
     * @return File[]
     */
    public function getAllPhotos() : array
    {
        $mainPhoto = $this->getMainPhoto();
        $photos = array_filter($this->review->reviewFilesSort, function (File $file) use ($mainPhoto) {
            return FileTypesHelper::isImage($file) && ($mainPhoto == null || !$mainPhoto->equals($file));
        });

        if ($mainPhoto) {
            array_unshift($photos, $mainPhoto);
        }
        return $photos;
    }
}