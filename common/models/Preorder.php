<?php

namespace common\models;

use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\reject\RejectInterface;
use common\models\loggers\PreorderLogger;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\modules\payment\fee\FeeHelper;
use lib\money\Money;
use lib\money\MoneyMath;
use Yii;

/**
 * Class Preorder
 *
 * @package common\models
 * @property \common\models\StoreOrder $storeOrder
 * @property string $updated_at
 * @property StoreOrderAttemp $storeOrderAttempt
 *
 * @property Company $company
 * @property MsgTopic $messageTopic
 * @property bool $offered The magic method: has a preorder been sent for review
 */
class Preorder extends \common\models\base\Preorder
{
    public const STATUS_NEW                       = 'new';
    public const STATUS_ACCEPTED                  = 'accepted';
    public const STATUS_REJECTED_BY_CLIENT        = 'rejected_by_client';
    public const STATUS_REJECTED_BY_COMPANY       = 'rejected_by_company';
    public const STATUS_REJECT_CONFIRM_BY_COMPANY = 'rejected_by_company_after_client_reject';
    public const STATUS_CANCELED_BY_CLIENT        = 'canceled_by_client';
    public const STATUS_DELETED_BY_CLIENT         = 'deleted_by_client';
    public const STATUS_WAIT_CONFIRM              = 'wait_confirm';
    public const STATUS_DRAFT                     = 'draft';

    public const FILTER_BY_ALL       = '';
    public const FILTER_BY_DRAFT     = 'draft';
    public const FILTER_BY_NEW       = 'new';
    public const FILTER_BY_ACTIVE    = 'active'; // active quotes (new and submitted)
    public const FILTER_BY_SUBMITTED = 'submitted';
    public const FILTER_BY_DECLINED  = 'declined';

    public const CREATED_BY_PS     = 'ps';
    public const CREATED_BY_CLIENT = 'client';

    public const CONFIRMED_HASH = 'confirmed_hash';

    public function rules()
    {
        $parentRules = parent::rules();

        $parentRules[] = [['offer_estimate_time'], 'integer', 'max' => 1000];
        $parentRules[] = [['email'], 'checkYourselfEmail'];
        return $parentRules;
    }

    function checkYourselfEmail()
    {
        if ($this->company->user->email === $this->email) {
            $this->addError('email', 'You can`t create quote for yourself');
        }
    }

    /**
     * @return Money
     */
    public function getBudgetMoney(): Money
    {
        return Money::create($this->budget, $this->currency);
    }

    /**
     * @return bool
     */
    public function getOffered(): bool
    {
        return $this->primaryPaymentInvoice !== null;
    }

    public function isCreatedByPs(): bool
    {
        return $this->created_by === self::CREATED_BY_PS;
    }

    /**
     * Calculate total cost, minus commission, without invoice
     *
     * @return Money
     * @throws \yii\base\InvalidConfigException
     */
    public function getCalcTotalCost(): Money
    {
        $costWithFee = $this->getCalcTotalCostWithFee();
        $feeHelper   = \Yii::createObject(FeeHelper::class);
        if ($this->is_interception) {
            return Money::create($costWithFee->getAmount(), $costWithFee->getCurrency());
        }
        return Money::create($costWithFee->getAmount() - $feeHelper->getQuoteFee($costWithFee)->getAmount(), $costWithFee->getCurrency());
    }

    /**
     * Calculate total amount of preorder, without invoice
     *
     * @return Money
     */
    public function getCalcTotalCostWithFee(): Money
    {
        $cost = Money::zero($this->currency);

        foreach ($this->preorderWorks as $work) {
            $cost = MoneyMath::sum($cost, $work->getCostWithFeeMoney());
        }

        return $cost;
    }

    public function getPriceTooltip($hidePaymentMethodFee = false): string
    {
        if (!$this->primaryPaymentInvoice) {
            return '';
        }
        $message = 'Manufacturing fee for the customer is {price1}. Service fee is {price2}. Your income is {price3}';

        $params = [
            'price1' => displayAsMoney($this->primaryPaymentInvoice->getAmountTotalWithRefund()),
            'price2' => displayAsMoney($this->primaryPaymentInvoice->preorderAmount->getAmountFeeWithRefund()),
            'price3' => displayAsMoney($this->primaryPaymentInvoice->preorderAmount->getAmountManufacturerWithRefund())
        ];

        if (!$hidePaymentMethodFee && $amountCardFee = $this->primaryPaymentInvoice->getAmountPaymentMethodFee()) {
            $message          = 'Manufacturing fee for the customer is {price1}. Service fee is {price2}.Payment method fee is {price4}.Your income is {price3}';
            $params['price4'] = displayAsMoney($amountCardFee);
        }

        return _t('site.ps', $message, $params);
    }

    /**
     * Is user make preorder to own cnc machine
     *
     * @return bool
     */
    public function isSelfPreorder(): bool
    {
        return $this->user && $this->user->equals($this->ps->user);
    }

    public function isDraft()
    {
        return $this->status === self::STATUS_DRAFT;
    }

    public function isNew()
    {
        return $this->status === self::STATUS_NEW;
    }

    public function isDeletedByClient()
    {
        return $this->status === self::STATUS_DELETED_BY_CLIENT;
    }

    public function changeStatus($newStatus)
    {
        $this->status            = $newStatus;
        $this->status_updated_at = DateHelper::now();
    }

    public function accept()
    {
        $this->changeStatus(Preorder::STATUS_ACCEPTED);
        $this->offer_accepted_at = DateHelper::now();
    }

    public function interceptionCompany(): Company
    {
        if ($this->is_interception) {
            return User::tryFindByPk(User::USER_CUSTOMER_SERVICE)->company;
        } else {
            return $this->company;
        }
    }

    public function isInternalOrder()
    {
        $internalUsers = app('setting')->get('store.internalOrdersCustomerUserId');
        if (in_array($this->ps_id, $internalUsers)) {
            return true;
        }
        if (in_array($this->user_id, $internalUsers)) {
            return true;
        }
        return false;
    }

    public function getStatusLabeledForPs()
    {
        return $this->getStatusLabelsForPs()[$this->status];
    }

    public function getStatusLabelsForPs()
    {
        $statuses = [
            Preorder::STATUS_DRAFT                     => _t('user.order', 'Draft'),
            Preorder::STATUS_NEW                       => _t('user.order', 'New'),
            Preorder::STATUS_ACCEPTED                  => _t('user.order', 'Accepted'),
            Preorder::STATUS_REJECTED_BY_CLIENT        => _t('user.order', 'Re-offer'),
            Preorder::STATUS_REJECT_CONFIRM_BY_COMPANY => _t('user.order', 'Declined'),
            Preorder::STATUS_REJECTED_BY_COMPANY       => _t('user.order', 'Declined'),
            Preorder::STATUS_WAIT_CONFIRM              => _t('user.order', 'Waiting for payment'),
            Preorder::STATUS_CANCELED_BY_CLIENT        => _t('user.order', 'Canceled by client'),
            Preorder::STATUS_DELETED_BY_CLIENT         => _t('user.order', 'Deleted'),

        ];
        return $statuses;
    }

    public function getStatusLabelsForAdmin()
    {
        $statuses = [
            Preorder::STATUS_DRAFT                     => _t('user.order', 'Draft'),
            Preorder::STATUS_NEW                       => _t('user.order', 'Waiting for offer'),
            Preorder::STATUS_ACCEPTED                  => _t('user.order', 'Accepted'),
            Preorder::STATUS_REJECTED_BY_CLIENT        => _t('user.order', 'Declined by client'),
            Preorder::STATUS_REJECT_CONFIRM_BY_COMPANY => _t('user.order', 'Declined by company after client'),
            Preorder::STATUS_REJECTED_BY_COMPANY       => _t('user.order', 'Declined by company'),
            Preorder::STATUS_WAIT_CONFIRM              => _t('user.order', 'Waiting for payment'),
            Preorder::STATUS_CANCELED_BY_CLIENT        => _t('user.order', 'Canceled by client'),
            Preorder::STATUS_DELETED_BY_CLIENT         => _t('user.order', 'Deleted by client'),
        ];
        return $statuses;
    }

    public function getStatusDescriptionsForClient()
    {
        $statuses = [
            Preorder::STATUS_DRAFT                     => _t('user.order', 'Waiting for offer'),
            Preorder::STATUS_NEW                       => _t('user.order', 'Waiting for offer'),
            Preorder::STATUS_ACCEPTED                  => _t('user.order', 'Accepted'),
            Preorder::STATUS_REJECTED_BY_CLIENT        => _t('user.order', 'Declined by you'),
            Preorder::STATUS_REJECT_CONFIRM_BY_COMPANY => _t('user.order', 'Declined by you'),
            Preorder::STATUS_REJECTED_BY_COMPANY       => _t('user.order', 'Declined'),
            Preorder::STATUS_WAIT_CONFIRM              => _t('user.order', 'Waiting for payment'),
            Preorder::STATUS_CANCELED_BY_CLIENT        => _t('user.order', 'Canceled'),
            Preorder::STATUS_DELETED_BY_CLIENT         => _t('user.order', 'Deleted'),
        ];
        return $statuses;
    }

    public function getLabeledStatusForAdmin()
    {
        $statusDescription = $this->getStatusLabelsForAdmin();
        return $statusDescription[$this->status];
    }

    public function getLabeledStatus()
    {
        $statusDescription = $this->getStatusDescriptionsForClient();
        return $statusDescription[$this->status];
    }

    public static function getStatusFilterForCustomer()
    {
        $statuses = [
            Preorder::STATUS_NEW                       => _t('user.order', 'Waiting for offer'),
            Preorder::STATUS_WAIT_CONFIRM              => _t('user.order', 'Waiting for payment'),
        ];
        return $statuses;
    }

    public function getUnitType()
    {
        return $this->productSnapshot->getUnitType() ?? '';
    }

    public function getUnitTypeLabel()
    {
        return $this->productSnapshot->getUnitTypeLabel($this->qty) ?? '';
    }

    public function getCompany()
    {
        return $this->hasOne(\common\models\Company::class, ['id' => 'ps_id']);
    }

    /**
     * @return bool
     */
    public function isForProduct(): bool
    {
        return (bool)$this->product_snapshot_uuid;
    }

    public function getStoreOrderAttempt(): ?StoreOrderAttemp
    {
        $attempts = $this->storeOrderAttemps;
        $attempt  = reset($attempts);
        return $attempt ? $attempt : null;
    }

    public function getStoreOrder()
    {
        return $this->hasOne(\common\models\StoreOrder::class, ['preorder_id' => 'id'])->inverseOf('preorder');
    }

    public function getMessageTopic()
    {
        return $this->hasOne(\common\models\MsgTopic::class, ['bind_id' => 'id'])->andWhere(['bind_to' => MsgTopic::BIND_OBJECT_PREORDER]);
    }

    /**
     * @return bool
     */
    public function isRejected(): bool
    {
        return in_array($this->status, [self::STATUS_REJECTED_BY_CLIENT, self::STATUS_REJECT_CONFIRM_BY_COMPANY, self::STATUS_REJECTED_BY_COMPANY]) ;
    }

    public function isCancelled(): bool
    {
        return $this->isRejected();
    }

    public function isAccepted(): bool
    {
        return $this->status == self::STATUS_ACCEPTED;
    }

    /**
     * @return bool
     */
    public function isClientCanAccept(): bool
    {
        return $this->offered && $this->status == self::STATUS_WAIT_CONFIRM;
    }

    public function isClientCanDecline(): bool
    {
        return $this->offered && $this->status == self::STATUS_WAIT_CONFIRM;
    }

    public function isClientCanCancel(): bool
    {
        return ($this->status != self::STATUS_CANCELED_BY_CLIENT) && ($this->status == self::STATUS_NEW || !$this->isClientCanDecline());
    }

    public function isClientCanDelete(): bool
    {
        return $this->status == self::STATUS_CANCELED_BY_CLIENT;
    }

    public function isClientCanRepeat()
    {
        return false;
    }

    public function getDeadline(): string
    {
        if ($this->offer_accepted_at && $this->offer_estimate_time) {
            $now    = DateHelper::now();
            $sub    = DateHelper::strtotimeUtc($this->offer_accepted_at) + 60 * 60 * 24 * $this->offer_estimate_time - strtotime($now);
            $days   = intval($sub / 60 / 60 / 24);
            $hours  = intval(($sub - ($days * 60 * 60 * 24)) / 60 / 60);
            $retVal = $days . ' days ' . $hours . ' hours';
            return $retVal;
        }
        return '';
    }


    public function load($data, $formName = null)
    {
        $formData = [];
        $formName = $formName ? $formName : $this->formName();
        if (array_key_exists($formName, $data)) {
            $formData = $data[$formName];
        }
        unset($formData['created_at']);
        parent::load($formData, '');
        if (array_key_exists('offer_description', $formData)) {
            $this->offer_description = $formData['offer_description'];
        }
        if (array_key_exists('admin_comment', $formData)) {
            $this->admin_comment = $formData['admin_comment'];
        }
        if (array_key_exists('offer_estimate_time', $formData)) {
            $this->offer_estimate_time = $formData['offer_estimate_time'];
        }
        if (array_key_exists('email', $formData) && !$this->email) {
            $this->email = $formData['email'];
        }
        if (array_key_exists('user', $formData) && array_key_exists('id', $formData['user'])) {
            $this->user_id = $formData['user']['id'];
        }
    }

    public function withdrawOffer()
    {
        if ($this->created_by === Preorder::CREATED_BY_PS) {
            $this->changeStatus(Preorder::STATUS_DRAFT);
        } else {
            $this->changeStatus(Preorder::STATUS_NEW);
        }

    }

    public function isCncType()
    {
        return $this->service ? $this->service->isCnc() : false;
    }

    public function getTotalQty()
    {
        $qty = 0;
        foreach ($this->preorderWorks as $preorderWork) {
            $qty += $preorderWork->qty;
        }
        return $qty;
    }
}
