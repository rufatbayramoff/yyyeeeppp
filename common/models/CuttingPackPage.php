<?php

namespace common\models;

/**
 * Class CuttingPackPage
 * @package common\models
 *
 * @property \common\models\CuttingPackPart[] $activeCuttingPackParts
 */
class CuttingPackPage extends \common\models\base\CuttingPackPage
{
    public $forDelete;

    public function getTitle()
    {
        if (count($this->cuttingPackFile->cuttingPackPages)>1) {
            return $this->cuttingPackFile->getTitle() . ' - ' .$this->cuttingPackFile->getPageNum($this);
        }
        return $this->cuttingPackFile->getTitle();
    }

    public function addImage(CuttingPackPageImage $image)
    {
        $images = $this->cuttingPackPageImages;
        $images[] = $image;
        $this->populateRelation('cuttingPackPageImages', $images);
    }

    public function getActiveCuttingPackParts()
    {
        $parts = $this->cuttingPackParts;
        $activeParts = [];
        foreach ($parts as $part) {
            if ($part->is_active) {
                $activeParts[] = $part;
            }
        }
        return $activeParts;
    }
}