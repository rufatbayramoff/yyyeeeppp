<?php

namespace common\models;

/**
 * Class Model3dPartCncParams
 *
 * @package common\models
 */
class Model3dPartCncParams extends \common\models\base\Model3dPartCncParams
{

    public function analyzeError(string $error): void
    {
        $this->error_type = FileJob::JOB_CNC_ANALYZE;
        $this->error_msg = $error;
    }

    public function budgetError(string $error): void
    {
        $this->error_type = 'budget';
        $this->error_msg = $error;
    }

    public function clearErrorMsg()
    {
        $this->error_type = null;
        $this->error_msg = null;
    }
}