<?php

namespace common\models;

use common\modules\payment\services\RefundService;
use frontend\models\order\StoreOrderAttemptDeadline;
use frontend\models\ps\PsFacade;
use frontend\modules\workbench\components\OrderUrlHelper;
use lib\delivery\delivery\DeliveryFacade;
use Yii;
use yii\base\Exception;

/**
 * Class StoreOrderAttemp
 *
 * @package common\models
 * @property \common\models\Company $company
 * @property \common\models\StoreOrderAttempDelivery $delivery
 * @property \common\models\StoreOrderAttemptModeration $moderation
 */
class StoreOrderAttemp extends \common\models\base\StoreOrderAttemp
{
    /**
     * order statuses for order_status
     */
    public const STATUS_QUOTE        = 'quote';
    public const STATUS_NEW          = 'new';
    public const STATUS_ACCEPTED     = 'accepted';
    public const STATUS_PRINTING     = 'printing';
    public const STATUS_PRINT_REVIEW = 'print_review';
    public const STATUS_PRINTED      = 'printed';
    public const STATUS_READY_SEND   = 'ready_send'; // after review, STATUS ready to send
    public const STATUS_SENT         = 'sent';
    public const STATUS_DELIVERED    = 'delivered';
    public const STATUS_RECEIVED     = 'received';
    public const STATUS_RETURNED     = 'returned';
    public const STATUS_CANCELED     = 'canceled';
    public const STATUS_UNPAID       = 'unpaid';

    /**
     * Company statuses groups.
     *
     * @var array
     */
    public static $companyStatusesGroups = [
        self::STATUS_QUOTE      => [self::STATUS_QUOTE],
        self::STATUS_NEW        => [self::STATUS_NEW],
        self::STATUS_ACCEPTED   => [self::STATUS_ACCEPTED, self::STATUS_PRINTING],
        self::STATUS_PRINTED    => [self::STATUS_PRINTED, self::STATUS_PRINT_REVIEW],
        self::STATUS_READY_SEND => [self::STATUS_READY_SEND],
        self::STATUS_SENT       => [self::STATUS_SENT],
        self::STATUS_DELIVERED  => [self::STATUS_DELIVERED],
        self::STATUS_RECEIVED   => [self::STATUS_RECEIVED],
        self::STATUS_CANCELED   => [self::STATUS_CANCELED],
        self::STATUS_UNPAID     => [self::STATUS_UNPAID],
    ];

    /**
     * Return array of statuses
     *
     * @return string[]
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_QUOTE        => _t('site.payment', 'Quote'),
            self::STATUS_UNPAID       => _t('site.payment', 'Incomplete'),
            self::STATUS_NEW          => _t('site.payment', 'Placed'),
            self::STATUS_ACCEPTED     => _t('site.payment', 'Accepted'),
            self::STATUS_PRINTING     => _t('site.payment', 'In production'),
            self::STATUS_PRINTED      => _t('site.payment', 'Manufactured'),
            self::STATUS_PRINT_REVIEW => _t('site.payment', 'Review'),
            self::STATUS_READY_SEND   => _t('site.payment', 'Ready for dispatch'),
            self::STATUS_SENT         => _t('site.payment', 'Dispatched'),
            self::STATUS_DELIVERED    => _t('site.payment', 'Delivered'),
            self::STATUS_RECEIVED     => _t('site.payment', 'Received'),
            self::STATUS_RETURNED     => _t('site.payment', 'Returned'),
            self::STATUS_CANCELED     => _t('site.payment', 'Canceled'),
        ];
    }

    public static function getStatusesForCustomer()
    {
        return [
            self::STATUS_QUOTE        => _t('site.payment', 'Quote'),
            self::STATUS_UNPAID       => _t('site.payment', 'Pending payment'),
            self::STATUS_NEW          => _t('site.payment', 'Placed'),
            self::STATUS_ACCEPTED     => _t('site.payment', 'In production'),
            self::STATUS_PRINTING     => _t('site.payment', 'In production'),
            self::STATUS_PRINTED      => _t('site.payment', 'Submitted for moderation'),
            self::STATUS_PRINT_REVIEW => _t('site.payment', 'Submitted for moderation'),
            self::STATUS_READY_SEND   => _t('site.payment', 'Ready for dispatch'),
            self::STATUS_SENT         => _t('site.payment', 'Dispatched'),
            self::STATUS_DELIVERED    => _t('site.payment', 'Delivered'),
            self::STATUS_RECEIVED     => _t('site.payment', 'Received'),
            self::STATUS_RETURNED     => _t('site.payment', 'Returned'),
            self::STATUS_CANCELED     => _t('site.payment', 'Canceled'),
        ];
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getDelivery()
    {
        return $this->getStoreOrderAttempDelivery();
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getModeration()
    {
        return $this->getStoreOrderAttemptModeration();
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return $this->status == self::STATUS_NEW;
    }

    /**
     * @return bool
     */
    public function isAccepted(): bool
    {
        return $this->status == self::STATUS_ACCEPTED;
    }

    /**
     * @return bool
     */
    public function isPrinting(): bool
    {
        return $this->status == self::STATUS_PRINTING;
    }

    /**
     * @return bool
     */
    public function isReadySend(): bool
    {
        return $this->status === StoreOrderAttemp::STATUS_READY_SEND;
    }

    public function isDispatched(): bool
    {
        return $this->status === StoreOrderAttemp::STATUS_SENT;
    }

    public function isDelivered(): bool
    {
        return $this->status === StoreOrderAttemp::STATUS_DELIVERED;
    }

    /**
     * @return bool
     */
    public function isFreeDelivery(): bool
    {
        return $this->machine && $this->machine->asDeliveryParams()->isFreeDelivery($this->order);
    }

    public function isPayed(): bool
    {
        return $this->order && $this->order->isPayed();
    }

    /**
     * @return string
     */
    public function getDeliveryTypesIntl()
    {
        return PsFacade::getDeliveryTypesIntl($this->order->deliveryType->code);
    }

    public function isPickupDelivery(): bool
    {
        return DeliveryFacade::isPickupDelivery($this->order);
    }

    public function isInterception(): bool
    {
        return $this->preorder ? $this->preorder->is_interception : false;
    }

    /**
     * @return bool
     */
    public function isCancelled()
    {
        return $this->status == self::STATUS_CANCELED;
    }

    public function getCustomerTextStatus()
    {
        if (!$this->order->isPayed() && !$this->order->isCancelled()) {
            return $this->getStatusesForCustomer()[self::STATUS_UNPAID];
        }
        return $this->getStatusesForCustomer()[$this->status];
    }

    public function interceptionCompany(): Company
    {
        if ($this->isInterception()) {
            return User::tryFindByPk(User::USER_CUSTOMER_SERVICE)->company;
        }
        return $this->company;
    }

    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'ps_id'])->inverseOf('storeOrderAttemps');
    }

    /**
     * @return bool
     */
    public function isResolved()
    {
        return in_array($this->status, [self::STATUS_RECEIVED, self::STATUS_CANCELED]);
    }

    /**
     * Is rejected by moderator or rejected quote
     *
     * @return bool
     */
    public function isRejected(): bool
    {
        return ($this->preorder && $this->preorder->isRejected()) || ($this->moderation && $this->moderation->status == 'rejected');
    }

    public function getRejectComment(): string
    {
        return $this->isRejected() ? ($this->moderation->reject_comment ?? '') : '';
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return $this->isResolved() && (bool)$this->dates->expired_at;
    }

    public function isCuttingOrder()
    {
        return $this->order && $this->order->hasCuttingPack();
    }

    public function is3dPrintOrder()
    {
        return $this->order && $this->order->isFor3dPrinter();
    }

    public function isQuote()
    {
        return !$this->order && $this->preorder;
    }

    public function isShippingAddressVisible()
    {
        return in_array($this->status, [
            self::STATUS_READY_SEND,
            self::STATUS_DELIVERED,
            self::STATUS_RECEIVED,
            self::STATUS_SENT
        ]);
    }


    /**
     * can user like results?
     *
     * @return bool
     */
    public function canUserLike()
    {
        return in_array($this->status, [
            self::STATUS_PRINTED,
            self::STATUS_READY_SEND,
            self::STATUS_SENT,
            self::STATUS_DELIVERED,
        ]);
    }

    /**
     * True if order already printed
     *
     * @return bool
     */
    public function isPrinted()
    {
        return in_array($this->status, [
            self::STATUS_PRINTED,
            self::STATUS_SENT,
            self::STATUS_DELIVERED,
            self::STATUS_RECEIVED,
            self::STATUS_CANCELED,
        ]);
    }

    public function isPendingModeration()
    {
        return in_array($this->status, [
            self::STATUS_PRINTED,
        ]);
    }

    /**
     * Return status group
     *
     * @return int
     * @throws Exception
     */
    public function getStatusGroup()
    {
        foreach (self::$companyStatusesGroups as $statusGroup => $statuses) {
            if (in_array($this->status, $statuses)) {
                return $statusGroup;
            }
        }
        throw new Exception("Status {$this->status} no in status group");
    }

    /**
     * @return string
     */
    public function getTitleLabel(): string
    {
        if ($this->order) {
            if ($this->preorder) {
                return _t('site.order', 'Order #{orderId} (from Quote #{quoteId})', [
                    'orderId' => $this->order->id,
                    'quoteId' => $this->preorder->id
                ]);
            } else {
                return _t('site.order', 'Order #{orderId}', ['orderId' => $this->order->id]);
            }
        }
        if ($this->preorder) {
            return _t('site.order', 'Quote #{quoteId}', [
                'quoteId' => $this->preorder->id
            ]);
        }
        return 'Attempt ' . $this->id;
    }


    /**
     * @return string
     */
    public function showAttempStatus()
    {
        $status = $this->status;
        if ($this->order->getPaymentStatus() == PaymentInvoice::STATUS_NEW) {
            $status = StoreOrderAttemp::STATUS_UNPAID;
        }
        if ($status === StoreOrderAttemp::STATUS_PRINTED || $status === StoreOrderAttemp::STATUS_ACCEPTED) {
            return StoreOrderAttemp::STATUS_PRINTING;
        }
        if ($status === StoreOrderAttemp::STATUS_READY_SEND) {
            # return StoreOrderAttemp::STATUS_PRINTED;
        }
        if ($this->order->order_state === StoreOrder::STATE_CLOSED && $this->order->getPaymentStatus() === PaymentInvoice::STATUS_NEW) {
            $status = StoreOrderAttemp::STATUS_CANCELED;
        }
        return $status;

    }

    /**
     * @param bool $nl2br
     *
     * @return string
     */
    public function getDeliveryFormatAddress($nl2br = true): string
    {
        if ($this->isPickupDelivery()) {
            $printerAddress = UserAddress::getAddressFromLocation($this->machine->location);
            return UserAddress::formatAddress($printerAddress, $nl2br);
        }

        $shipAddress = $this->order->shipAddress;

        if (!$shipAddress) {
            return '';
        }

        if (!$this->isShippingAddressVisible()) {
            // WTF: Why data changed in get method
            $shipAddress->address             = '';
            $shipAddress->first_name          = '';
            $shipAddress->last_name           = '';
            $shipAddress->common_contact_name = '';
            $shipAddress->zip_code            = '';
            $shipAddress->phone               = '';
        }

        return UserAddress::formatAddressWithFields($shipAddress, $nl2br);
    }

    /**
     * @return array|null
     */
    public function getRefundRequestByOrder(): ?array
    {
        if (!$this->order) {
            return null;
        }
        $refundService      = Yii::createObject(RefundService::class);
        $refundTransactions = $refundService->getRefundRequestByOrder($this->order);
        $lastErrorMessage   = null;
        if (!$refundTransactions) {
            $lastErrorMessage = $refundService->lastErrorMessage;
        }
        return [$refundTransactions, $lastErrorMessage];
    }

    /**
     * @return boolean
     */
    public function isAcceptedByUser()
    {
        return UserLike::find()->where([
            'object_type' => UserLike::TYPE_ORDER_ATTEMPT,
            'object_id'   => $this->id
        ])->exists();
    }

    /**
     * @return boolean
     */
    public function isDislikedByUser()
    {
        return UserLike::find()->where([
            'object_type' => UserLike::TYPE_ORDER_ATTEMPT,
            'object_id'   => $this->id,
            'is_dislike'  => 1
        ])->exists();
    }

    /**
     * @return bool
     */
    public function hasMachine(): bool
    {
        return (bool)$this->machine_id;
    }

    public function isTrackingNumberVisible()
    {
        if (in_array($this->status, [
            StoreOrderAttemp::STATUS_SENT,
            StoreOrderAttemp::STATUS_DELIVERED,
            StoreOrderAttemp::STATUS_RECEIVED,
            StoreOrderAttemp::STATUS_READY_SEND
        ])) {
            return true;
        }
        return false;
    }

    /**
     * The file was downloaded
     *
     * @return bool
     */
    public function wasDownloaded()
    {
        return !empty($this->dates->fact_printed_at);
    }

    /**
     * @return null|\yii\db\ActiveRecord|StoreOrderReview
     */
    public function getAttempReview()
    {
        return StoreOrderReview::find()
            ->forAttemp($this)
            ->forAttempPs($this)
            ->one();
    }

    /**
     * @return File|null
     */
    public function getAttemptModerationImageFile(): ?File
    {
        if ($this->storeOrderAttemptModeration) {
            /** @var StoreOrderAttemptModerationFile $moderationFile */
            $moderationFile = StoreOrderAttemptModerationFile::find()
                ->where(['attempt_id' => $this->storeOrderAttemptModeration->attempt_id])
                ->orderBy(['is_main_review_file' => SORT_DESC])
                ->limit(1)
                ->one();

            return $moderationFile ? $moderationFile->file : null;
        }

        return null;
    }


    /**
     * @param bool $scheme
     *
     * @return string
     */
    public function getViewUrl(): string
    {
        return OrderUrlHelper::viewStoreOrderAttemp($this);
    }

    public function getDeadline()
    {
        $deadlineTimer = StoreOrderAttemptDeadline::create($this, false);
        return $deadlineTimer;
    }

    public function getDeadlineCompany()
    {
        $deadlineTimer = StoreOrderAttemptDeadline::createCompany($this, false);
        return $deadlineTimer;
    }

    public function getDeadlineClient()
    {
        $deadlineTimer = StoreOrderAttemptDeadline::createClient($this);
        return $deadlineTimer;
    }


    /**
     * Get deadline in hours
     *
     * @return float|int
     */
    public function getDeadlineHours()
    {
        if ($deadlineTimer = $this->getDeadline()) {
            return $deadlineTimer->getHours();
        }
        return null;
    }

    public function fixDates()
    {
        if (!$this->dates) {
            // TODO: HOTFIX Remove after 2022 year
            $dates            = new StoreOrderAttempDates();
            $dates->attemp_id = $this->id;
            $dates->safeSave();
            $this->dates_id = $this->id;
            $this->safeSave();
        }
    }
}