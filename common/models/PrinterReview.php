<?php

namespace common\models;

use Yii;

/**
 * Class PrinterReview
 *
 * @package common\models
 */
class PrinterReview extends \common\models\base\PrinterReview
{
    const STATUS_NEW       = 'new';
    const STATUS_PUBLISHED = 'published';
    const STATUS_REJECTED  = 'rejected';

    public static function getModeratorStatusLabels()
    {
        return [
            'new'       => 'New',
            'published' => 'Published',
            'rejected'  => 'Rejected'
        ];
    }

    /**
     * @return array|mixed|null|object|string
     */
    public function getStatusLabel()
    {
        if ($this->status == PrinterReview::STATUS_NEW) {
            return _t('site.printer', 'Review submitted');
        }

        if ($this->status == PrinterReview::STATUS_PUBLISHED) {
            return _t('site.printer', 'Review published');
        }

        if ($this->status == PrinterReview::STATUS_REJECTED) {
            return _t('site.printer', 'Review rejected');
        }

        return $this->status;
    }

    public function getReviewLabels()
    {
        return [
            'print_quality'    => Yii::t('printer.review', 'Print Quality'),
            'reliability'      => Yii::t('printer.review', 'Reliability'),
            'ease_of_use'      => Yii::t('printer.review', 'Ease Of Use'),
            'failure_rate'     => Yii::t('printer.review', 'Failure Rate'),
            'running_expenses' => Yii::t('printer.review', 'Running Expenses'),
            'software'         => Yii::t('printer.review', 'Software'),
            'build_quality'    => Yii::t('printer.review', 'Build Quality'),
            'value'            => Yii::t('printer.review', 'Value'),
            'customer_service' => Yii::t('printer.review', 'Customer Service'),
            'community'        => Yii::t('printer.review', 'Community'),

        ];
    }

    /**
     * @return array
     */
    public function getReviewData()
    {
        $labels = array_keys($this->getReviewLabels());
        $labels[] = 'comments';
        $result = [];
        foreach ($labels as $label) {
            $result[$label] = $this->{$label};
        }

        return $result;
    }

    public function getRating()
    {
        $result = [];
        foreach ($this->getReviewLabels() as $k => $v) {
            $result[] = $this->{$k};
        }
        $avg = round(array_sum($result) / count($result), 2);
        return $avg;
    }
}