<?php

namespace common\models;

use common\interfaces\FileBaseInterface;

/**
 * Class FileAdmin
 * @package common\models
 */
class FileAdmin extends \common\models\base\FileAdmin implements FileBaseInterface
{
    use FileBase {
        rules as traitRules;
        scenarios as traitScenarios;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = array_merge(
            parent::rules(),
            $this->traitRules(),
            []
        );
        return $rules;
    }

    public function scenarios(): array
    {
        $rules = array_merge(
            parent::scenarios(),
            $this->traitScenarios(),
            []
        );
        return $rules;
    }

    public function getFileCompressed()
    {
        return [];
    }

    public function updateLastAccessTime($localPath)
    {

    }
}