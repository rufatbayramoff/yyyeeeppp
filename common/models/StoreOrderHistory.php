<?php
namespace common\models;
use yii\helpers\Json;

/**
 * Order updates history
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class StoreOrderHistory extends \common\models\base\StoreOrderHistory
{
    const ITEM_ADDED = 'item_added';

    
    /**
     * order updated
     */
    const ORDER_UPDATED = 'order_updated';
    const TEXTURE_UPDATE = 'texture_updated';
    const MANUAL_INVOICE_CANCEL = 'manual_invoice_cancel';
    const CHANGE_TRACKING = 'change_tracking';

    /**
     * order status updated
     */
    const ORDER_ATTEMPT_CHANGE_STATUS = 'order_status';
    const ORDER_ATTEMPT_ACCEPT = 'accept_attemp';
    const ORDER_ATTEMPT_DECLINE = 'decline_reason';

    const ORDER_ATTEMPT_MODERATOR_PRINT_REJECT = 'moderator_print_reject';
    
    /**
     * payment status updated
     */
    const PAYMENT_STATUS = 'payment_status';

    const PAYMENT_OPERATION = 'payment_operation';

    const USER_CANCEL_ORDER = 'user_cancel_order';

    const USER_REMOVE_ORDER = 'user_remove_order';
    
    const MODERATOR_CANCEL_ORDER = 'cancel_order';

    const MODERATOR_DELIVERY_TYPE = 'delivery_type';

    const MODERATOR_CHANGE_CARRIER = 'change_carrier';

    const MODERATOR_CHANGE_COMMENT = 'change_comment';

    const MODERATOR_SWITCH_DISPUTE = 'switch_dispute';
    
    const RSA_TAX = 'rsa_tax';

    const REQUEST_MORE_TIME = 'request_time';
    const SCHEDULED_TO_SENT_TIME = 'scheduled_to_sent_time';

    const POSITION_UPDATE = 'position_update';
    const POSITION_DECLINE = 'position_decline';
    const POSITION_PAID = 'position_paid';

    /**
     * @return mixed
     */
    public function getDecodedData()
    {
        return Json::decode($this->data);
    }
}