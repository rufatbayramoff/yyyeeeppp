<?php
namespace common\models;

use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Class reprsent Article for Site Help functional
 * @author Nabi Ibatulin
 */
class SiteHelp extends \common\models\base\SiteHelp
{
    /**
     * Update count of clicks for this article.
     * Add +1 to click counter filed.
     */
    public function updateClicks()
    {
        $help = self::findByPk($this->id);
        $help->updateCounters(['clicks' => 1]);
    }

    /**
     * Retrun most popular articles
     * @param int $limit max limit
     * @param SiteHelpCategory $topCategory
     * @return SiteHelp[]
     */
    public static function getPopular($limit = 3, SiteHelpCategory $topCategory)
    {
        /** @var int[] $subCategoriesIds */
        $subCategoriesIds = ArrayHelper::getColumn($topCategory->siteHelpCategories, 'id');

        /** @var SiteHelp[] $helps */
        $helps = SiteHelp::find()
            ->forCategoriesIds($subCategoriesIds)
            ->popular()
            ->orderByClicks()
            ->limit($limit)
            ->all();
        return $helps;
    }

    /**
     * Update count of view for this article.
     * Add +1 to views counter filed.
     *
     * If user view article twice or more per session, counter increase only +1.
     */
    public function updateViews()
    {
        $isViewed = app('session')->get('article_' . $this->id, false);
        if(!$isViewed ){
            $help = self::findByPk($this->id);
            $help->updateCounters(['views' => 1]);
            app('session')->set('article_' . $this->id, true);
        }
    }

    /**
     * Return slug of article
     * @return string
     */
    public function getSlug()
    {
        return Inflector::slug($this->title);
    }

    /**
     * Return array of tags.
     * If it have not tags - return empty array.
     * @return string[]
     */
    public function getTags() : array
    {
        if($this->tags) {
            return array_map('trim', explode('|', $this->tags));
        }
        return [];
    }
}