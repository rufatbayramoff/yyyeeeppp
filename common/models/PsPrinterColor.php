<?php
namespace common\models;

use lib\MeasurementUtil;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class PsPrinterColor extends \common\models\base\PsPrinterColor
{
    const MEASURE_GRAM = 'gr';
    const MEASURE_OUNCE = 'oz';

    const SCENARIO_UPDATE = 'update';

     public function behaviors()
    {
        return [
            'history' => [
                'class' => \common\components\HistoryBehavior::className(),
                'historyClass' => PsPrinterHistory::className(),
                'foreignKey' => 'ps_printer_id',
                'ownerIdField' => 'psPrinterId',
                'actionCode' => 'update_color',
                'addonFields' => ['ps_color_id'=>'id', 'new_price'=>'price'],
                'skipAttributes' => ['updated_at', 'created_at', 'price_usd_updated', 'price_usd']
            ],
        ];
    }
    public function getPsPrinterId()
    {
        return $this->psMaterial->ps_printer_id;
    } 
    
    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
           ['price', 'number', 'min' => 0.01, 'max' => 1000]
        ]);
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_UPDATE => ['price']
        ]);
    }

    public function getPriceUsdPerGr()
    {
        return $this->getPricePerGr()->convertTo(Currency::USD);
    }

    public function getPriceUsdPerMl()
    {
        return  $this->getPricePerMl()->convertTo(Currency::USD);
    }

    public function getPricePerGr()
    {
        $currency = $this->psMaterial->psPrinter->companyService->company->currency;
        if ($this->price_measure == MeasurementUtil::ML) {
            $pricePerGr = $this->price / $this->psMaterial->material->density;
            return Money::create($pricePerGr, $currency);
        } elseif ($this->price_measure === MeasurementUtil::GRAM) {
            return Money::create($this->price, $currency);
        } elseif ($this->price_measure === MeasurementUtil::OUNCE) {
            return Money::create(MeasurementUtil::convertGramToOunce($this->price), $currency);
        } else {
            throw new InvalidArgumentException('Unknown measure');
        }
    }

    public function getPricePerMl()
    {
        $currency = $this->psMaterial->psPrinter->companyService->company->currency;
        if ($this->price_measure == MeasurementUtil::ML) {
            return Money::create($this->price, $currency);
        } elseif ($this->price_measure === MeasurementUtil::GRAM) {
            $pricePerMl = $this->price * $this->psMaterial->material->density;
            return Money::create($pricePerMl, $currency);
        } elseif ($this->price_measure === MeasurementUtil::OUNCE) {
            $pricePerMl = MeasurementUtil::convertGramToOunce($this->price) * $this->psMaterial->material->density;
            return Money::create($pricePerMl, $currency);
        } else {
            throw new InvalidArgumentException('Unknown measure');
        }
    }
}