<?php

namespace common\models;

/**
 * Class UserProfileDeleteRequest
 * @package common\models
 */
class UserProfileDeleteRequest extends \common\models\base\UserProfileDeleteRequest
{
    public const APPROVE_CODE_EXPIRE_SEC = 60*60;
}