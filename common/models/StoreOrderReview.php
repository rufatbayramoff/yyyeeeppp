<?php

namespace common\models;

use frontend\models\user\UserFacade;

/**
 * Class StoreOrderReview
 *
 * @package common\models
 *
 * @property float $rating_speed
 * @property float $rating_quality
 * @property float $rating_communication
 * @property array $deleteFilesUuid
 * @property File[] $addFiles
 * @property string|null setMainImageUuid
 * @property File[] $reviewFilesSort
 * @property File $reviewCoverFile
 */
class StoreOrderReview extends \common\models\base\StoreOrderReview
{
    public const ALLOW_EXTENSIONS = ['jpg', 'png'];

    /**
     * @var File[]
     */
    public $addFiles = [];

    /**
     * file uuid
     *
     * @var array[]
     */
    public $deleteFilesUuid = [];

    /**
     * @var string|null
     */
    public $setMainImageUuid;

    public const STATUS_NEW = 'new';
    public const STATUS_MODERATED = 'moderated';
    public const STATUS_REJECTED = 'rejected';

    public const ANSWER_STATUS_NEW = 'new';
    public const ANSWER_STATUS_MODERATED = 'moderated';
    public const ANSWER_STATUS_REJECTED = 'rejected';

    public const SCENARIO_ANSWER = 'scenario_answer';
    public const SCENARIO_ANSWER_MODERATION = 'scenario_answer_moderation';

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_ANSWER] = [
            'answer'
        ];

        $scenarios[self::SCENARIO_ANSWER_MODERATION] = [
            'verified_answer_status',
            'answer_reject_comment'
        ];

        return $scenarios;
    }

    public function filesValidator()
    {
        foreach ($this->addFiles as $file) {
            if (!$file->validate()) {
                $this->addErrors($file->getErrors());
            }
        }
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['addFiles', 'filesValidator'];
        $rules[] = ['setMainImageUuid', 'string'];
        return $rules;
    }


    public function getStoreOrderCurrentAttemp()
    {
        return $this->hasOne(\common\models\StoreOrderAttemp::class, ['order_id' => 'order_id']);
    }


    /**
     * @return query\FileQuery
     */
    public function getReviewFilesSort()
    {
        return $this->getFiles()
            ->from(['rf' => File::tableName()])
                ->innerJoin(['sorf' => StoreOrderReviewFile::tableName()], 'sorf.file_uuid = uuid')
            ->orderBy([
                'sorf.is_main' => SORT_DESC,
                'sorf.created_at' => SORT_DESC
            ]);
    }

    /**
     * @return query\FileQuery
     */
    public function getReviewCoverFile()
    {
        $q = $this->getReviewFilesSort();
        $q->multiple = false;
        return $q;
    }

    /**
     * @return bool
     */
    public function haveContent(): bool
    {
        return $this->reviewFilesSort || $this->comment;
    }

    /**
     * @return bool
     */
    public function isCanShow(): bool
    {
        return $this->status === self::STATUS_MODERATED;
    }

    /**
     * @return float
     */
    public function getRating(): float
    {
        return round(($this->rating_communication + $this->rating_quality + $this->rating_speed) / 3, 1);
    }

    public function getRatingDetailLabels()
    {
        return [
            'rating_speed'         => _t('site.review', 'Speed'),
            'rating_quality'       => _t('site.review', 'Quality'),
            'rating_communication' => _t('site.review', 'Communication'),
        ];
    }

    public function getRatingDetails()
    {
        return [
            _t('site.review', 'Speed')         => $this->rating_speed,
            _t('site.review', 'Quality')       => $this->rating_quality,
            _t('site.review', 'Communication') => $this->rating_communication,
        ];
    }
    /**
     * @return bool
     */
    public function isOwnCompanyToCurrentUser(): bool
    {
        $userId = UserFacade::getCurrentUserId();

        if ($userId && ($this->ps->user_id === $userId)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function canShowAnswer(): bool
    {
        return !empty($this->answer) && (\in_array($this->verified_answer_status, [self::ANSWER_STATUS_MODERATED, self::ANSWER_STATUS_NEW], true) || $this->isOwnCompanyToCurrentUser());
    }

    /**
     * @return bool
     */
    public function isAnswerRejected(): bool
    {
        return $this->verified_answer_status === self::ANSWER_STATUS_REJECTED;
    }

    /**
     * @return bool
     */
    public function isAnswerNew(): bool
    {
        return $this->verified_answer_status === self::ANSWER_STATUS_NEW;
    }
}