<?php

namespace common\models;

use common\components\exceptions\InvalidModelException;
use common\components\UuidHelper;
use common\modules\payment\exception\FatalPaymentException;
use yii\db\Query;

/**
 * Class PaymentDetailOperation
 *
 * @package common\models
 * @property \common\models\PaymentTransaction $paymentTransaction
 */
class PaymentDetailOperation extends \common\models\base\PaymentDetailOperation
{
    /**
     * @return string
     * @throws \Exception
     */
    public static function generateUuid()
    {
        do {
            $operationUuid = strtoupper(UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('payment_detail_operation')->where(['uuid' => $operationUuid])->one();
        } while ($checkUuid);
        return $operationUuid;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTransactionHistories()
    {
        return $this->hasMany(\common\models\PaymentTransactionHistory::class, ['payment_detail_id' => 'id'])->via('paymentDetails');
    }

    /**
     * Get linked payment transaction
     * ATTENTION: Now only one payment transaction can be linked into one payment operation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTransaction()
    {
        return $this->hasOne(\common\models\PaymentTransaction::class, ['id' => 'transaction_id'])->via('paymentTransactionHistories');
    }

    /**
     * Money send from paymentDetail
     *
     * @return  PaymentDetail
     * @throws InvalidModelException
     */
    public function fromPaymentDetail()
    {
        if (count($this->paymentDetails) !== 2) {
            throw new FatalPaymentException('Invalid count of payment details in operation. Should be 2.');
        }

        foreach ($this->paymentDetails as $paymentDetail) {
            if ($paymentDetail->amount < 0) {
                return $paymentDetail;
            }
        }
        throw new FatalPaymentException('Can`t get from payment detail. Only "to" payment details.');
    }

    /**
     * Money send to PaymentDetail
     *
     * @return  PaymentDetail
     * @throws InvalidModelException
     */
    public function toPaymentDetail()
    {
        if (count($this->paymentDetails) !== 2) {
            throw new FatalPaymentException('Invalid count of payment details in operation. Should be 2.');
        }

        foreach ($this->paymentDetails as $paymentDetail) {
            if ($paymentDetail->amount > 0) {
                return $paymentDetail;
            }
        }
        throw new FatalPaymentException('Can`t get from payment detail. Only "from" payment details.');
    }

    public function getPaymentAccounts()
    {
        return $this->hasMany(PaymentAccount::class, ['id' => 'payment_account_id'])->via('paymentDetails');
    }

    /**
     * @return \common\models\query\PaymentQuery
     */
    public function getPayment()
    {
        return $this->hasOne(\common\models\Payment::class, ['id' => 'payment_id']);
    }
}