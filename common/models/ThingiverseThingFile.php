<?php

namespace common\models;

/**
 * Class ThingiverseThingFile
 *
 * @package common\models
 * @property \common\models\Model3dPart $model3dPart
 */
class ThingiverseThingFile extends \common\models\base\ThingiverseThingFile
{
    public function setFile(File $file)
    {
        $this->populateRelation('file', $file);
        $this->file_id = $file->id;
    }

    public function setThing(ThingiverseThing $thing)
    {
        $this->populateRelation('thing', $thing);
        $this->thing_id = $thing->thing_id;
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getModel3dPart()
    {
        $model3dId = $this->thing->model3d ? $this->thing->model3d->id : null;
        if (!$model3dId) {
            return null;
        }
        return $this->hasOne(\common\models\Model3dPart::class, ['file_id' => 'file_id'])->andWhere(['model3d_id' => $model3dId]);
    }
}