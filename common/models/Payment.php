<?php

namespace common\models;

use common\models\query\PaymentTransactionQuery;
use Yii;

/**
 * Payment object, which works with PaymentDetail
 *
 * @author Nabi Ibatulin
 *
 * @property PaymentTransaction $vendorTransactions
 * @property PaymentTransactionHistory $vendorTransactionHistories
 * @property PaymentDetail[] $paymentDetails
 */
class Payment extends \common\models\base\Payment
{
    /**
     * rarely used, but means that this payment is new,
     * and no operations made within this payment
     */
    const STATUS_NEW = 'new';

    /**
     * we are waiting for some process to complete.
     * for example API request started, and pending,
     * or payment pending to author/ps until order is received
     */
    const STATUS_PENDING = 'pending';

    /**
     * we mark that process worked fine, and waiting for any other processing
     * like payouts to ps or author
     */
    const STATUS_PROCESSING = 'processing';

    /**
     * all payments are paid with this payment
     * - TS got fee
     * - Author got award
     * - PS got award
     */
    const STATUS_PAID = 'paid';

    /**
     * payment is cancelled, funds returned to buyer,
     * but not completely to other participants (ps, author)
     */
    const STATUS_CANCELLED = 'cancelled';

    /**
     * all payments returend,
     * - we return amount to buyer
     * - we  get money from author, if we gave them
     * - we get money from PS, if we gave them
     */
    const STATUS_RETURNED = 'returned';

    /**
     * payment is closed, and no other operations can be made using this payment
     * should be closed using CRON job, payment is closed after user received
     * his order, and after 3-7 days after that we close payments within this order
     */
    const STATUS_CLOSED = 'closed';

    /**
     * if payment failed (usually API call), we should deny any other work,
     * before we find why this happen, after that status can be manually changed
     * to processing, pending and etc.
     */
    const STATUS_FAILED = 'failed';

    const PAYMENT_CORRECTION_ID = 1;


    /**
     * check if given user has pending payouts to paypal
     *
     * @param User $user
     * @return bool
     */
    public static function hasPendingPayouts(User $user)
    {
        return PaymentTransaction::find()
            ->where([
                'vendor'  => PaymentTransaction::VENDOR_PAYPAL,
                'status'  => PaymentTransaction::STATUS_PENDING,
                'user_id' => $user->id
            ])
            ->exists();
    }

    public function isSuccefulyPayed()
    {
        return $this->status === Payment::STATUS_PAID;
    }

    public function wasPayed()
    {
        return $this->status === Payment::STATUS_PAID || $this->status === Payment::STATUS_RETURNED || $this->status === PAYment::STATUS_CANCELLED;
    }

    public function isPayInProcess()
    {
        return $this->status === Payment::STATUS_PENDING || $this->status === Payment::STATUS_PROCESSING;
    }

    /**
     * @return \common\models\query\PaymentDetailQuery
     */
    public function getPaymentDetails()
    {
        return $this->hasMany(\common\models\PaymentDetail::class, ['payment_detail_operation_uuid' => 'uuid'])->via('paymentDetailOperations');
    }

    /**
     * @return PaymentTransactionQuery
     */
    public function getVendorTransactions()
    {
        return $this->hasMany(PaymentTransaction::class, ['first_payment_detail_id'=>'id'])->via('paymentDetails');
    }

    public function getVendorTransactionHistories()
    {
        return $this->hasMany(PaymentTransactionHistory::class, ['transaction_id'=>'id'])->via('vendorTransactions');
    }
}