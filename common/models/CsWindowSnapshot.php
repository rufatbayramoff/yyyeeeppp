<?php

namespace common\models;

/**
 * Class CsWindowSnapshot
 * @package common\models
 *
 * @property CsWindow $snapshotCsWindow
 * @property CsWindowFurniture[] $snapshotCsWindowFurnitures
 * @property CsWindowGlass[] $snapshotCsWindowGlasses
 * @property CsWindowProfile[] $snapshotCsWindowProfiles
 */
class CsWindowSnapshot extends \common\models\base\CsWindowSnapshot
{

    protected $_snapshotCsWindow;

    protected $_snapshotCsWindowProfiles;

    protected $_snapshotCsWindowGlasses;

    protected $_snapshotCsWindowFurnitures;

    /**
     * @return CsWindow
     */
    public function getSnapshotCsWindow()
    {
        if ($this->_snapshotCsWindow === null) {
            $this->_snapshotCsWindow = new CsWindow();
            $this->_snapshotCsWindow->load($this->service, '');
        }

        return $this->_snapshotCsWindow;
    }

    /**
     * @return CsWindowProfile[]
     */
    public function getSnapshotCsWindowProfiles(): array
    {
        if ($this->_snapshotCsWindowProfiles === null) {
            $this->_snapshotCsWindowProfiles = [];

            foreach ($this->profile as $k => $item) {
                $this->_snapshotCsWindowProfiles[$k] = new CsWindowProfile();
                $this->_snapshotCsWindowProfiles[$k]->id = $item['id'];
                $this->_snapshotCsWindowProfiles[$k]->load($item, '');
            }
        }

        return $this->_snapshotCsWindowProfiles;
    }

    /**
     * @return CsWindowGlass[]
     */
    public function getSnapshotCsWindowGlasses(): array
    {
        if ($this->_snapshotCsWindowGlasses === null) {
            $this->_snapshotCsWindowGlasses = [];

            foreach ($this->glass as $k => $item) {
                $this->_snapshotCsWindowGlasses[$k] = new CsWindowGlass();
                $this->_snapshotCsWindowGlasses[$k]->id = $item['id'];
                $this->_snapshotCsWindowGlasses[$k]->load($item, '');
            }
        }

        return $this->_snapshotCsWindowGlasses;
    }

    /**
     * @return CsWindowFurniture[]
     */
    public function getSnapshotCsWindowFurnitures(): array
    {
        if ($this->_snapshotCsWindowFurnitures === null) {
            $this->_snapshotCsWindowFurnitures = [];

            foreach ($this->furniture as $k => $item) {
                $this->_snapshotCsWindowFurnitures[$k] = new CsWindowFurniture();
                $this->_snapshotCsWindowFurnitures[$k]->id = $item['id'];
                $this->_snapshotCsWindowFurnitures[$k]->load($item, '');
            }
        }

        return $this->_snapshotCsWindowFurnitures;
    }

}