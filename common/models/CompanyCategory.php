<?php

namespace common\models;

use common\modules\company\components\CompanyServiceInterface;

/**
 * Class CompanyCategory
 * @package common\models
 */
class CompanyCategory extends \common\models\base\CompanyCategory
{
    public const CODE_3D_PRINTING = '3D-printing';

    public static function create(int $companyId, int $categoryId): CompanyCategory
    {
        $model = new self();
        $model->company_id = $companyId;
        $model->company_service_category_id = $categoryId;
        return $model;
    }

    /**
     * @return bool
     */
    public function needCreateService(): bool
    {
        $category = $this->companyServiceCategory;
        return $category->type() === CompanyServiceInterface::TYPE_SERVICE;
    }
}