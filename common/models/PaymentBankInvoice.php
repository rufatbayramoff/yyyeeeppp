<?php

namespace common\models;

use common\components\exceptions\DeprecatedException;
use common\modules\payment\services\InvoiceBankService;
use lib\money\Money;

/**
 * Class PaymentBankInvoice
 *
 * @property integer|null $storeOrderId
 * @package common\models
 */
class PaymentBankInvoice extends \common\models\base\PaymentBankInvoice
{
    public const STATUS_NEW      = 'new';
    public const STATUS_PAID     = 'paid';
    public const STATUS_EXPIRED  = 'expired';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_VOID     = 'void';
    public const STATUS_REFUND   = 'refund';

    /**
     * @return string
     * @throws \Exception
     */
    public static function generateUuid(): string
    {
        do {
            $uuid = strtoupper(\common\components\UuidHelper::generateUuid());
            $checkUuid = self::find()->where(['uuid' => $uuid])->asArray()->one();
        } while ($checkUuid);

        return $uuid;
    }

    /**
     * check if order can be paid by invoice
     *
     * @param StoreOrder $storeOrder
     *
     * @return bool
     * @throws \common\components\exceptions\InvalidModelException
     */
    public static function canPayByInvoice(StoreOrder $storeOrder): bool
    {
        return $storeOrder->getPrimaryInvoice()->getAmountTotal()->getAmount() >= InvoiceBankService::INVOICE_FROM_USD;
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->status === self::STATUS_NEW;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->status === self::STATUS_PAID;
    }

    public function getStoreOrderId()
    {
        $paymentInvoice = $this->paymentInvoice;
        if(!$paymentInvoice) {
            return null;
        }
        return $paymentInvoice->store_order_id;
    }

    /**
     * @param array $typePositions
     *
     * @throws DeprecatedException
     */
    public function getItemsByPosition(array $typePositions)
    {
        throw new DeprecatedException('Depricated: '.__CLASS__.'::'.__METHOD__);
    }

    /**
     * @throws DeprecatedException
     */
    public function getInvoiceTotalAmount()
    {
        throw new DeprecatedException('Depricated: '.__CLASS__.'::'.__METHOD__);
    }
}