<?php

namespace common\models;
use common\components\ArrayHelper;
use frontend\models\user\UserFacade;
use lib\money\Money;

/**
 * Class PreorderWork
 * @package common\models
 */
class PreorderWork extends \common\models\base\PreorderWork
{

    public const TYPE_PRODUCT = 'product';
    public const TYPE_SERVICE = 'service';

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['product_uuid', 'productValidator'],
            ['company_service_id', 'companyServiceValidator']
        ]);
    }

    public function productValidator()
    {
        if ($this->product_uuid) {
            if ($this->product->company->id !== $this->preorder->company->id) {
                $this->addError('Product form other company');
            }
        }
    }

    public function companyServiceValidator()
    {
        if ($this->company_service_id) {
            if ($this->companyService->company->id !== $this->preorder->company->id) {
                $this->addError('Service form other company');
            }
        }
    }

    /**
     * The amount to get the manufacturer
     *
     * @return Money
     * @throws \yii\base\InvalidConfigException
     */
    public function getCalcCostMoney() : Money
    {
        $totalCost = $this->preorder->getCalcTotalCost();
        $totalCostWithFee = $this->preorder->getCalcTotalCostWithFee();
        $percent = $totalCostWithFee->getAmount() / $totalCost->getAmount();
        $costWithoutFee = $this->cost - ( $this->cost - $this->cost / $percent);

        return Money::usd($costWithoutFee);
    }

    /**
     * Service (treatstock) fee of the cost
     *
     * @return Money
     * @throws \yii\base\InvalidConfigException
     */
    public function getCalcCostPsFee(): Money
    {
        $totalCost = $this->preorder->getCalcTotalCost();
        if ($this->preorder->is_interception) {
            return Money::zero($this->preorder->currency);
        }

        $totalCostWithFee = $this->preorder->getCalcTotalCostWithFee();
        $percent = $totalCostWithFee->getAmount() / $totalCost->getAmount();
        $psFee = $this->cost - ($this->cost / $percent);

        return Money::create($psFee, $this->preorder->currency);
    }

    /**
     * Total amount of preorder work, without invoice
     *
     * @return Money
     */
    public function getCostWithFeeMoney(): Money
    {
        return Money::create($this->cost, $this->preorder->currency);
    }

    public function getType()
    {
        if ($this->product_uuid) {
            return self::TYPE_PRODUCT;
        }
        if ($this->company_service_id) {
            return self::TYPE_SERVICE;
        }
        return '';
    }

    public function getTextTitle()
    {
        if ($this->getType() == self::TYPE_PRODUCT) {
            return _t('site.product', 'Product') . ': ' . $this->product->getTitle();
        }
        if ($this->getType() == self::TYPE_SERVICE) {
            return _t('site.service', 'Service') . ': ' . $this->companyService->getTitleLabel();
        }
        return $this->title;
    }

    public function getCoverUrl()
    {
        if ($this->getType() == self::TYPE_PRODUCT) {
            return $this->product->getCoverUrl();
        }
        if ($this->getType() == self::TYPE_SERVICE) {
            return $this->companyService->getCoverUrl();
        }
        return null;
    }

    /**
     * @return null|File
     */
    public function getCoverImage(): ?File
    {
        if ($this->getType() == self::TYPE_PRODUCT) {
            return $this->product->imageFiles[0] ?? null;
        }
        if ($this->getType() == self::TYPE_SERVICE) {
            return $this->companyService->imageFiles[0] ?? null;
        }
        return null;
    }

    public function load($data, $formName = null)
    {
        parent::load($data, $formName);
        if (array_key_exists('productUuid', $data)) {
            $this->product_uuid = $data['productUuid'];
        }
        if (array_key_exists('companyServiceId', $data)) {
            $this->company_service_id = $data['companyServiceId'];
        }
    }

    public function getUnitTypeLabel()
    {
        if ($this->getType() == self::TYPE_PRODUCT) {
            return $this->product->getUnitTypeTitle($this->qty);
        }
        return '';
    }

    public function getWorkTitle()
    {
        $title = $this->title;
        if(!empty($this->product)){
            $title = $this->product->title;
        }
        return $title;
    }
}