<?php

namespace common\models;

use common\interfaces\Model3dBaseInterface;
use common\services\Model3dViewedStateService;
use common\services\PrinterMaterialService;
use Yii;

/**
 * Class Model3dViewedState
 * @package common\models
 */
class Model3dViewedState extends \common\models\base\Model3dViewedState
{
    /** @var int  */
    public $model3dQty = 1;

    /** @var float  */
    public $model3dScale = 100;

    /** @var  Model3dBaseInterface */
    public $model3dRestored;

    /** @var array */
    public $model3dPartsQty = [];
}