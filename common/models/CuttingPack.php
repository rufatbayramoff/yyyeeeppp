<?php

namespace common\models;

use common\components\exceptions\ValidationException;
use common\components\FileTypesHelper;
use common\components\ps\locator\Size;
use common\modules\cutting\helpers\CuttingUrlHelper;
use common\modules\cutting\models\CutSize;

/**
 * Class CuttingPack
 * @package common\models
 * @property \common\models\CuttingPackFile[] $activeCuttingPackFiles
 */
class CuttingPack extends \common\models\base\CuttingPack
{
    public const SOURCE_WEBSITE = 'website';

    public const CDR_FORMAT = 'cdr';
    public const DXF_FORMAT = 'dxf';
    public const EPS_FORMAT = 'eps';
    public const PDF_FORMAT = 'pdf';
    public const SVG_FORMAT = 'svg';

    public const DEFAULT_CUTTING_MATERIAL_CODE = 'plywood';
    public const DEFAULT_CUTTING_COLOR_CODES   = ['Natural', 'Transparent'];

    public const ALLOWED_FORMATS = [
        self::CDR_FORMAT, self::DXF_FORMAT, self::EPS_FORMAT, self::SVG_FORMAT
    ];

    public function getDefaultMaterial(): CuttingMaterial
    {
        return CuttingMaterial::tryFind(['code' => self::DEFAULT_CUTTING_MATERIAL_CODE]);
    }

    public function getTitle()
    {
        if ($this->attributes['title']) {
            return $this->attributes['title'];
        }
        $title = '';
        foreach ($this->activeCuttingPackFiles as $file) {
            if ($file->isImage()) {
                continue;
            }
            $title .= $file->getTitle() . ', ';
        }
        if (mb_strlen($title) > 100) {
            $title = mb_substr($title, 0, 100) . '...';
        } else {
            $title = substr($title, 0, -2);
        }
        return $title;
    }

    /**
     * Not released, but it need for delivery customs
     *
     * @return int
     */
    public function getWeigthGr()
    {
        return 100;
    }

    public function getPreviewFile(): ?CuttingPackFile
    {
        $firstFile = null;
        foreach ($this->activeCuttingPackFiles as $cuttingPackFile) {
            if (!$firstFile) {
                $firstFile = $cuttingPackFile;
            }
            if ($cuttingPackFile->isImage()) {
                return $cuttingPackFile;
            }
        }
        return $firstFile;
    }

    public function getPreviewUrl($isAbsoluteUrl = false)
    {
        return CuttingUrlHelper::previewUrl($this, $isAbsoluteUrl);
    }

    public function getTotalQty()
    {
        $totalQty = 0;
        foreach ($this->getActiveCuttingParts() as $cuttingPackPart) {
            $totalQty += $cuttingPackPart->qty;
        }
        return $totalQty;
    }

    public function getTotalCuttingLength(): float
    {
        $length = 0;
        foreach ($this->getActiveCuttingParts() as $cuttingPackPart) {
            $length += $cuttingPackPart->cutting_length * $cuttingPackPart->qty;
        }
        return round($length, 2);
    }

    public function getTotalEngraveLength(): float
    {
        $length = 0;
        foreach ($this->getActiveCuttingParts() as $cuttingPackPart) {
            $length += $cuttingPackPart->engraving_length * $cuttingPackPart->qty;
        }
        return round($length, 2);
    }

    public function getDefaultColor(): PrinterColor
    {
        $defaultMaterial = $this->getDefaultMaterial();
        $printerColor    = $defaultMaterial->getCuttingColors()->andWhere(['printer_color.render_color' => self::DEFAULT_CUTTING_COLOR_CODES])->one();
        return $printerColor;
    }

    public function validateOrFailed()
    {
        if (!$this->validate()) {
            throw new ValidationException([$this]);
        }
        $validationFailed = [];
        foreach ($this->cuttingPackFiles as $packFile) {
            if (!$packFile->validate()) {
                $validationFailed[] = $packFile;
            }
            foreach ($packFile->cuttingPackParts as $part) {
                if (!$part->validate()) {
                    $validationFailed[] = $part;
                }
            }
        }
        if ($validationFailed) {
            throw new ValidationException($validationFailed);
        }
    }

    public function getActiveCuttingPackFiles()
    {
        return $this->getCuttingPackFiles()->andWhere(['cutting_pack_file.is_active' => 1]);
    }

    /**
     * @return CuttingPackPart[]
     */
    public function getActiveCuttingParts()
    {
        $parts = [];
        foreach ($this->cuttingPackFiles as $packFile) {
            if (!$packFile->is_active) {
                continue;
            }
            foreach ($packFile->cuttingPackPages as $page) {
                foreach ($page->cuttingPackParts as $part) {
                    if ((!$part->is_active) || (!$part->qty)) {
                        continue;
                    }
                    $parts[$part->uuid] = $part;
                }
            }
        }
        return $parts;
    }

    public function getMaxPartSize(): ?CutSize
    {
        $size = CutSize::create(0, 0, 0);
        foreach ($this->getActiveCuttingParts() as $packPart) {
            $partSize = $packPart->getSize();
            if ($partSize->isMoreOrEqualThen($size)) {
                $size = $partSize;
            }
        }
        return $size->isZero() ? null : $size;
    }

    public function isParsed()
    {
        $activeParts = $this->getActiveCuttingParts();
        if (!$activeParts) {
            return false;
        }
        foreach ($this->getActiveCuttingParts() as $packPart) {
            $partSize = $packPart->getSize();
            if (!$partSize) {
                return false;
            }
        }
        return true;
    }
}