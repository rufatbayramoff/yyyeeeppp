<?php
namespace common\models;

use common\components\UserSessionOwnersFixer;
use Yii;

/**
 * Edit this file
 * This is the model class for table "user_session".
 *
 * After login fixed is used to bind created objects to signed in user.
 *
 * @see UserSessionOwnersFixer::fixModelsOwnersForUser()
 */
class UserSession extends \common\models\base\UserSession
{
    
}