<?php
namespace common\models;

use common\components\IntlArTrait;
use Yii;

/**
 * 
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class PrinterColor extends \common\models\base\PrinterColor
{

    public const DEFAULT_COLOR = 'White';
    public const MULTICOLOR  = 'multicolor';
    public const MULTICOLOR_ID = 102;
    public const WHITE_COLOR_AMBIENT = 40;
    public const WHITE_COLOR_ID = 90;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['rgb', 'validateRgb'];
        return $rules;
    }

    public function getCode()
    {
        return $this->render_color;
    }

    public function validateRgb($attribute, $params)
    {
        $val = $this->$attribute;
        $parts = explode(",", $val);
        if(count($parts)===3){
            $partsFiltered = array_filter($parts, function($x) { return $x >= 0 && $x < 256; } );
            if(count($partsFiltered)==3){
                return true;
            }
        }
        $this->addError($attribute, 'Wrong RGB format. Use <digits>,<digits>,<digits>');
    }

    public function isMulticolor()
    {
        return $this->id === self::MULTICOLOR_ID;
    }

    public function getColorCodeOrHex()
    {
        if ($this->isMulticolor()) {
            return '';
        } else {
            return $this->getRgbHex();
        }
    }

    public function getRgbHex()
    {
        return rgb2hex(explode(",", $this->rgb));
    }
    
    /**
     * render colors available for pov-ray renderer
     * 
     * @return string
     */
    public static function getRenderColors()
    {
        $data = [
           'Black',
           'White',
           'Gray',
           'Red',
           'Green',
           'Blue',
           'Yellow',
           'Cyan',
           'Magenta',
           'Black',
           'Aquamarine',

           'BlueViolet',
           'Brown',
           'CadetBlue',
           'Coral',
           'CornflowerBlue',
           'DarkGreen',
           'DarkOliveGreen',
           'DarkOrchid',
           'DarkSlateBlue',
           'DarkSlateGray',

           'DarkTurquoise',
           'Firebrick',
           'ForestGreen',
           'Gold',
           'Goldenrod',
           'GreenYellow',
           'IndianRed',
           'Khaki',
           'LightBlue',
           'LightSteelBlue',

           'LimeGreen',
           'Maroon',
           'MediumAquamarine',
           'MediumBlue',

           'MediumForestGreen',
           'MediumGoldenrod',
           'MediumOrchid',
           'MediumSeaGreen',
           'MediumSlateBlue',
           'MediumSpringGreen',

           'MediumTurquoise',
           'MediumVioletRed',
           'MidnightBlue',
           'Navy',
           'NavyBlue',
           'Orange',
           'OrangeRed',
           'Orchid',
           'PaleGreen',
           'Pink',

           'Plum',
           'Salmon',
           'SeaGreen',
           'Sienna',
           'SkyBlue',
           'SlateBlue',
           'SpringGreen',
           'SteelBlue',
           'Tan',
           'Thistle',
 
           'Turquoise',
           'Violet',
           'VioletRed',
           'Wheat',
           'YellowGreen',
           'SummerSky',
           'RichBlue',
 
           'Brass',
           'Copper',
           'Bronze',
 
           'Bronze2',
           'Silver',
           'BrightGold',
           'OldGold',
           'Feldspar',
           'Quartz',
           'NeonPink',
           'DarkPurple',
           'NeonBlue',
           'CoolCopper',
 
           'MandarinOrange',
           'LightWood',
           'MediumWood',
           'DarkWood',
           'SpicyPink',
           'SemiSweetChoc',
           'BakersChoc',
           'Flesh',
           'NewTan',
           'NewMidnightBlue',
            
           'MandarinOrange',
           'VeryDarkBrown',
           'DarkBrown',
           'GreenCopper',
           'DkGreenCopper',
           'DustyRose',
           'HuntersGreen',
           'Scarlet',
           'DarkTan',
           'White',
        ];
        
        return $data;
    }
}