<?php

namespace common\models;

/**
 * Class ApiRequest
 * @package common\models
 */
class ApiRequest extends \common\models\base\ApiRequest
{
    const STATUS_NEW = 'new';
    const STATUS_RUNNING = 'running';
    const STATUS_OK = 'ok';
    const STATUS_FAILED = 'failed';
    
}