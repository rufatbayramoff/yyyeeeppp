<?php
namespace common\models;

use lib\money\Currency;
use yii\base\Exception;

/**
 * Payment currency
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class PaymentCurrency extends \common\models\base\PaymentCurrency
{
    /**
     * default currency
     * @var string
     */
    public static $default = Currency::USD;
    
    /**
     * current currency
     * 
     * @var string|null
     */
    public static $current = null;


    /**
     *
     * @param float $amount
     * @param int $mode
     * @return float
     */
    public function doRound($amount, $mode = PHP_ROUND_HALF_UP)
    {
        return round($amount, $this->round, $mode);
    }
    /**
     * get current currency
     * 
     * @return string
     */
    public static function getCurrent()
    {
        if (self::$current === null) {
            try {
                if (!is_guest()) {
                    $iso = \frontend\components\UserSessionFacade::getCurrency();
                    self::$current = self::findOne(['currency_iso' => $iso]);
                } else if (!empty(app()->session['current_currency'])) {
                    $sessionIso = app()->session['current_currency'];
                    self::$current = self::findOne(['currency_iso' => $sessionIso]);
                }
            } catch (\Exception $e) {
                
            }
        }
        if (self::$current === null) {
            self::$current = self::findOne(['currency_iso' => self::$default]);
        }
        return self::$current;
    }
    
    /**
     * get list of active currencies
     * 
     * @return array
     */
    public static function getAll()
    {
        $list  = PaymentCurrency::find()->where(
            ['is_active'=>1]
        )->all();  
        return $list;
    }

    public static function getAllowed()
    {
        $list  = PaymentCurrency::find()->where(
            ['currency_iso'=>Currency::ALLOW_SELECT_CURRENCIES]
        )->all();
        return $list;
    }


    /**
     * Return currecny by iso code
     * throw exception if it not found
     * @param $isoCode
     * @return PaymentCurrency
     * @throws Exception
     */
    public static function getCurrencyByIso($isoCode)
    {
        $currency = PaymentCurrency::find()
            ->where([
                'is_active'=>1,
                'currency_iso' => $isoCode,
            ])->one();

        if(!$currency)
        {
            throw new Exception("Currency {$isoCode} not found");
        }
        return $currency;
    }


}