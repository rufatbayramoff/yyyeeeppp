<?php

namespace common\models;

use common\components\ArrayHelper;
use common\interfaces\Model3dBasePartInterface;
use frontend\models\store\StoreFacade;
use Yii;

/**
 * Edit this file
 * This is the model class for table "store_order_item".
 */
class StoreOrderItem extends \common\models\base\StoreOrderItem
{
    /**
     * Return print price for file of unit
     *
     * @param int $model3dPartId
     * @return float
     * @todo refactor it to pie calculating after introduce Money objects
     */
    public function resolvePrintPriceForFile($model3dPartId)
    {
        $weights = ArrayHelper::map(StoreFacade::getSnapModel3dParts($this), 'id', function (Model3dBasePartInterface $model3dPart) {
            return $model3dPart->getWeight();
        });

        return round($this->price * $weights[$model3dPartId] / array_sum($weights), 2);
    }

    /**
     * @return bool
     */
    public function hasModel(): bool
    {
        return (bool)$this->model3d_replica_id;
    }

    public function hasCuttingPack(): bool
    {
        return (bool)$this->cuttingPack;
    }

    /**
     * @return bool
     */
    public function hasProduct(): bool
    {
        return (bool)$this->product_uuid;
    }
}