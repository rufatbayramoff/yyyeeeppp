<?php

namespace common\models;

/**
 * Class StoreOrderReviewFile
 * @package common\models
 */
class StoreOrderReviewFile extends \common\models\base\StoreOrderReviewFile
{
    public static function primaryKey()
    {
        return ['review_id', 'file_uuid'];
    }

    /**
     * @return bool
     */
    public function isStoreOrderAttemptModerationFile(): bool
    {
        return $this->file->getStoreOrderAttemptModerationFiles()->limit(1)->exists();
    }
}