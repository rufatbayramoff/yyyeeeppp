<?php

namespace common\models;

use common\components\DateHelper;
use lib\delivery\parcel\Parcel;

/**
 * Class StoreOrderDeliveryHistory
 *
 * @package common\models
 */
class StoreOrderDeliveryHistory extends base\StoreOrderDeliveryHistory
{

    public static function createWithParcel(StoreOrder $order, Parcel $parcel): StoreOrderDeliveryHistory
    {
        $model = new self;
        $model->order_id = $order->id;
        $model->weight = $parcel->weight;
        $model->height = $parcel->height;
        $model->length = $parcel->length;
        $model->width = $parcel->width;
        $model->measure = $parcel->measure;
        $model->created_at = DateHelper::now();
        $model->updated_at = DateHelper::now();
        return $model;
    }

    public function editParcel(Parcel $parcel): void
    {
        $this->width = $parcel->width;
        $this->height = $parcel->height;
        $this->length = $parcel->length;
        $this->width = $parcel->width;
        $this->measure = $parcel->measure;
        $this->updated_at = DateHelper::now();
    }

    public function getData(): array
    {
        return [
            $this->weight,
            $this->length,
            $this->width,
            $this->height,
            $this->measure
        ];
    }
}