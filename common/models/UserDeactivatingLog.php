<?php

namespace common\models;
use Yii;

/**
 * Class UserDeactivatingLog
 * @package common\models
 *
 * @property SystemReject $reasonId
 */
class UserDeactivatingLog extends \common\models\base\UserDeactivatingLog
{
    public const SCENARIO_ADD_REASON = 'add_reason';
    public const SCENARIO_NOT_VALIDATED = 'not_validated';

    public function attributeLabels()
    {
        return [
            'id' => _t('app', 'ID'),
            'reason_id' => _t('app', 'Reason'),
            'user_id' => _t('app', 'User ID'),
            'created_at' => _t('app', 'Created At'),
            'reason_title' => _t('app', 'Reason Title'),
            'reason_desc' => _t('app', 'Reason description'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_ADD_REASON] = [
            'reason_id',
            '!user_id',
            '!created_at',
            'reason_title',
            'reason_desc'
        ];

        $scenarios[self::SCENARIO_NOT_VALIDATED] = [];

        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery|SystemReject
     */
    public function getReasonId()
    {
        return $this->hasOne(SystemReject::class, ['id' => 'reason_id'])
            ->andOnCondition(['group' => SystemReject::USER_DELETE_REQUEST]);
    }

    /**
     * @return array
     */
    public function getDeactivatingReasonData() : array
    {
        $reasonModel = $this->reasonId;

        return [
            'reason_title' => $reasonModel ? $reasonModel->title : $this->reason_title,
            'reason_desc' => $this->reason_desc
        ];
    }

    /**
     * If the account is deleted, get the reason
     *
     * @return string
     */
    public function getDeactivatingReasonText() : string
    {
        $logData = $this->getDeactivatingReasonData();

        return trim(trim($logData['reason_title'], ':'). (!empty($logData['reason_desc']) ? ': '.$logData['reason_desc'] : ''));
    }
}