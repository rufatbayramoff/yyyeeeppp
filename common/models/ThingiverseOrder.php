<?php

namespace common\models;

/**
 * Class ThingiverseOrder
 * @package common\models
 */
class ThingiverseOrder extends base\ThingiverseOrder
{

    const STATUS_REFUNDED = 'Refunded';
    const STATUS_DISPUTED = 'Disputed';
    const STATUS_PLACED = 'Placed';
    const STATUS_COMPLETED = 'Completed';
    const STATUS_SHIPPED = 'Shipped';

    public function getPlatformFee()
    {
        return $this->api_json['fees']['platform fee']??null;
    }

    public function getThingiverseTransactionFee()
    {
        return $this->api_json['fees']['transaction fee']??null;
    }

    /**
     * Get total Thingiverse commission
     */
    public function getTotalThingiverseFee()
    {
        $totalFee = $this->getPlatformFee()+$this->getThingiverseTransactionFee();
        return $totalFee;
    }
}