<?php

namespace common\models;

/**
 * Class StoreOrderAttemptModerationFile
 * @package common\models
 */
class StoreOrderAttemptModerationFile extends \common\models\base\StoreOrderAttemptModerationFile
{
    /**
     * @return bool
     */
    public function isMainReviewFile(): bool
    {
        return $this->is_main_review_file === 1;
    }
}