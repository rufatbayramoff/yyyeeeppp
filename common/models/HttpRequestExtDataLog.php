<?php

namespace common\models;

/**
 * Class HttpRequestExtDataLog
 * @package common\models
 */
class HttpRequestExtDataLog extends \common\models\base\HttpRequestExtDataLog
{
    public const TYPE_AR = 'active_record';
}