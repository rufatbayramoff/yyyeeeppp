<?php
namespace common\models;

use common\components\PaymentExchangeRateConverter;
use common\components\PaymentExchangeRateFacade;
use lib\money\Currency;

/**
 * Payment exchange rate logic
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class PaymentExchangeRate extends \common\models\base\PaymentExchangeRate
{
    /**
     * @var bool
     */
    public $ratesData = false;
    
    /**
     * exchange rate is expired if rate over 12 hours
     */ 
    const EXPIRE_HOURS = 6;

    /**
     * @var PaymentExchangeRateConverter
     */
    protected $diRateConverter;



    public function injectDependencies(PaymentExchangeRateConverter $diRateConverter)
    {
        $this->diRateConverter = $diRateConverter;
    }


    private static function requestExchangeRates()
    {
        $appId = param('openexchange_app_id', 'efdfe184f2534e508c7ddd6ae43c1e0a');
        $url = 'https://openexchangerates.org/api/latest.json?app_id=' . $appId;
        $arrContextOptions = array(
            'ssl' =>array(
                'verify_peer'      =>false,
                'verify_peer_name' =>false,
            ),
        );
        $content = file_get_contents($url, false, stream_context_create($arrContextOptions));
        if (empty($content)) {
            \Yii::info('No rates', 'pricer');
            throw new \Exception("No rates");
        }
        return $content;
    }

    /**
     * @return string
     */
    private static function requestLocalExchangeRates()
    {
        $content = file_get_contents(\Yii::getAlias('@common/config/rates.json'));
        return $content;
    }


    /**
     * get fresh copy of rate
     * 
     * @return PaymentExchangeRate
     * @throws \Exception
     */
    public static function refreshRate()
    {
        $currentRate = PaymentExchangeRate::find()->orderBy('id desc')->one();
        if($currentRate && $currentRate->isFreshRate()){
            return $currentRate;
        }
        // we get just a local copy if dev environment
        if (YII_ENV === 'dev' && param('openexchange_app_id', false) === 'a65b27b423214eefb201ed64906ef287') {
            $content = self::requestLocalExchangeRates();
        } else {
            $content = self::requestExchangeRates();
        }
        $contentJson = \yii\helpers\Json::decode($content);

        // simple check with base rates = 1
        $baseRate = $contentJson['rates'][$contentJson['base']];
        if ($baseRate !== 1) {
            \Yii::info('Base rate validation error! ' . $content, 'pricer');
            throw new \Exception('Error! Base rate not equals 1');
        }
        $rates = \yii\helpers\Json::encode($contentJson['rates']);
        $rateOld = PaymentExchangeRate::findOne([
            'timestamp' => $contentJson['timestamp'],
            'base' => $contentJson['base']
        ]);
        if ($rateOld) {
            return $rateOld;
        }
        $rate = PaymentExchangeRate::addRecord([
            'created_at' => dbexpr('NOW()'),
            'timestamp' => $contentJson['timestamp'],
            'base' => $contentJson['base'],
            'rates' => $rates
        ]);
        return $rate;
    }
    
    /**
     * check if current rate is fresh
     * 
     * @return \common\models\PaymentExchangeRate
     */
    public function isFreshRate()
    {
        $updateTime = $this->timestamp;
        $diffTimeHours = (time() - $updateTime) / 60 / 60;
        $hoursToExpire = param('openexchange_expire_hours', self::EXPIRE_HOURS);
        if ($diffTimeHours > $hoursToExpire) {
            return false;
        }
        return true;
    }

    /**
     * get rates as array 
     * 
     * @return array
     */
    public function getRatesData()
    {
        if(!$this->ratesData){
            $this->ratesData = \yii\helpers\Json::decode($this->rates);
        }
        return $this->ratesData;
    }
    /**
     * find rate for currency
     * 
     * @param string $currency
     * @return float
     * @throws \Exception
     */
    public function getCurrencyRate($currency)
    {
        if(!isset($this->currencyList[$currency])){
            throw new \Exception(sprintf("[%s] not valid currency", $currency));
        }
        $rates = $this->getRatesData();
        return $rates[$currency];
    }

    /**
     * check if given iso currency is correct
     *
     * @param string $currency
     * @return bool
     * @throws \Exception
     */
    public function validateCurrency($currency)
    {
        if($currency=='RMB'){ // easypost gives RMB currency
            $currency = 'CNY';
        }
        if (!isset($this->currencyList[$currency])) {
            throw new \Exception(sprintf("[%s] not valid currency", $currency));
        }
        return $currency;
    }

    public function convert($price, $fromCurrency, $toCurrency, $round = false)
    {
        return $this->diRateConverter->convert($price, $fromCurrency, $toCurrency, $round, $this);
    }

    /**
     * @var string[]
     */
    public $currencyList = array (
        'ALL' => 'Albania Lek',
        'RMB' => 'China',
        'AFN' => 'Afghanistan Afghani',
        'ARS' => 'Argentina Peso',
        'AWG' => 'Aruba Guilder',
        'AUD' => 'Australia Dollar',
        'AZN' => 'Azerbaijan New Manat',
        'BSD' => 'Bahamas Dollar',
        'BBD' => 'Barbados Dollar',
        'BDT' => 'Bangladeshi taka',
        'BYR' => 'Belarus Ruble',
        'BZD' => 'Belize Dollar',
        'BMD' => 'Bermuda Dollar',
        'BOB' => 'Bolivia Boliviano',
        'BAM' => 'Bosnia and Herzegovina Convertible Marka',
        'BWP' => 'Botswana Pula',
        'BGN' => 'Bulgaria Lev',
        'BRL' => 'Brazil Real',
        'BND' => 'Brunei Darussalam Dollar',
        'KHR' => 'Cambodia Riel',
        'CAD' => 'Canada Dollar',
        'KYD' => 'Cayman Islands Dollar',
        'CLP' => 'Chile Peso',
        'CNY' => 'China Yuan Renminbi',
        'COP' => 'Colombia Peso',
        'CRC' => 'Costa Rica Colon',
        'HRK' => 'Croatia Kuna',
        'CUP' => 'Cuba Peso',
        'CZK' => 'Czech Republic Koruna',
        'DKK' => 'Denmark Krone',
        'DOP' => 'Dominican Republic Peso',
        'XCD' => 'East Caribbean Dollar',
        'EGP' => 'Egypt Pound',
        'SVC' => 'El Salvador Colon',
        'EEK' => 'Estonia Kroon',
        'EUR' => 'Euro Member Countries',
        'FKP' => 'Falkland Islands (Malvinas) Pound',
        'FJD' => 'Fiji Dollar',
        'GHC' => 'Ghana Cedis',
        'GIP' => 'Gibraltar Pound',
        'GTQ' => 'Guatemala Quetzal',
        'GGP' => 'Guernsey Pound',
        'GYD' => 'Guyana Dollar',
        'HNL' => 'Honduras Lempira',
        'HKD' => 'Hong Kong Dollar',
        'HUF' => 'Hungary Forint',
        'ISK' => 'Iceland Krona',
        'INR' => 'India Rupee',
        'IDR' => 'Indonesia Rupiah',
        'IRR' => 'Iran Rial',
        'IMP' => 'Isle of Man Pound',
        'ILS' => 'Israel Shekel',
        'JMD' => 'Jamaica Dollar',
        'JPY' => 'Japan Yen',
        'JEP' => 'Jersey Pound',
        'KZT' => 'Kazakhstan Tenge',
        'KPW' => 'Korea (North) Won',
        'KRW' => 'Korea (South) Won',
        'KGS' => 'Kyrgyzstan Som',
        'LAK' => 'Laos Kip',
        'LVL' => 'Latvia Lat',
        'LBP' => 'Lebanon Pound',
        'LRD' => 'Liberia Dollar',
        'LTL' => 'Lithuania Litas',
        'MKD' => 'Macedonia Denar',
        'MYR' => 'Malaysia Ringgit',
        'MUR' => 'Mauritius Rupee',
        'MXN' => 'Mexico Peso',
        'MNT' => 'Mongolia Tughrik',
        'MZN' => 'Mozambique Metical',
        'NAD' => 'Namibia Dollar',
        'NPR' => 'Nepal Rupee',
        'ANG' => 'Netherlands Antilles Guilder',
        'NZD' => 'New Zealand Dollar',
        'NIO' => 'Nicaragua Cordoba',
        'NGN' => 'Nigeria Naira',
        'NOK' => 'Norway Krone',
        'OMR' => 'Oman Rial',
        'PKR' => 'Pakistan Rupee',
        'PAB' => 'Panama Balboa',
        'PYG' => 'Paraguay Guarani',
        'PEN' => 'Peru Nuevo Sol',
        'PHP' => 'Philippines Peso',
        'PLN' => 'Poland Zloty',
        'QAR' => 'Qatar Riyal',
        'RON' => 'Romania New Leu',
        'RUB' => 'Russia Ruble',
        'SHP' => 'Saint Helena Pound',
        'SAR' => 'Saudi Arabia Riyal',
        'RSD' => 'Serbia Dinar',
        'SCR' => 'Seychelles Rupee',
        'SGD' => 'Singapore Dollar',
        'SBD' => 'Solomon Islands Dollar',
        'SOS' => 'Somalia Shilling',
        'ZAR' => 'South Africa Rand',
        'LKR' => 'Sri Lanka Rupee',
        'SEK' => 'Sweden Krona',
        'CHF' => 'Switzerland Franc',
        'SRD' => 'Suriname Dollar',
        'SYP' => 'Syria Pound',
        'TWD' => 'Taiwan New Dollar',
        'THB' => 'Thailand Baht',
        'TTD' => 'Trinidad and Tobago Dollar',
        'TRY' => 'Turkey Lira',
        'TRL' => 'Turkey Lira',
        'TVD' => 'Tuvalu Dollar',
        'UAH' => 'Ukraine Hryvna',
        'GBP' => 'United Kingdom Pound',
        Currency::USD => 'United States Dollar',
        'UYU' => 'Uruguay Peso',
        'UZS' => 'Uzbekistan Som',
        'VEF' => 'Venezuela Bolivar',
        'VND' => 'Viet Nam Dong',
        'YER' => 'Yemen Rial',
        'ZWD' => 'Zimbabwe Dollar'
    );
}