<?php

namespace common\models;

/**
 * Class PreorderFile
 * @package common\models
 */
class PreorderFile extends \common\models\base\PreorderFile
{
    /**
     * @param $fileId
     * @param $preorderId
     * @return PreorderFile
     */
    public static function create($fileId, $preorderId): PreorderFile
    {
        $model = new self;
        $model->file_id = $fileId;
        $model->preorder_id = $preorderId;
        return $model;
    }
}