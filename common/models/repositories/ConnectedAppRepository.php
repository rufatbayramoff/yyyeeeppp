<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.01.18
 * Time: 20:36
 */

namespace common\models\repositories;

use common\models\ConnectedApp;
use yii\base\Exception;

class ConnectedAppRepository
{
    /** @var FileRepository */
    public $fileRepository;

    /**
     * @param FileRepository $fileRepository
     */
    public function injectDependencies(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param ConnectedApp $app
     * @throws \common\components\exceptions\InvalidModelException
     * @throws Exception
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save(ConnectedApp $app)
    {
        $logoFileDelete = null;
        if ($app->logoFile && $app->logoFile->forDelete) {
            $logoFileDelete = $app->logoFile;
            $app->logoFile = null;
            $app->logo_file_uuid = null;
        }
        if ($app->logoFile) {
            $this->fileRepository->save($app->logoFile);
            $app->logo_file_uuid = $app->logoFile->uuid;
        }
        try {
            $app->safeSave();
        } catch (Exception $e) {
        }
        if ($logoFileDelete) {
            $this->fileRepository->delete($logoFileDelete);
        }

    }
}