<?php


namespace common\models\repositories;


use common\models\PsMachineDeliveryCountry;

class PsMachineDeliveryCountryRepository
{
    /**
     * @param string $iso
     * @param $psDeliveryId
     * @return PsMachineDeliveryCountry|null
     */
    public function findDeliveryByIso(string $iso,$psDeliveryId):?PsMachineDeliveryCountry
    {
        $query = PsMachineDeliveryCountry::find();
        $query->innerJoinWith(['geoCountry','psMachineDelivery']);
        $query->andWhere([
            'ps_machine_delivery_country.ps_machine_delivery_id' => $psDeliveryId,
            'geo_country.iso_code' => $iso
        ]);
        return $query->one();
    }
}