<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.11.16
 * Time: 16:28
 */

namespace common\models\repositories;

use backend\models\user\UserAdmin;
use common\components\UserSessionOwnersFixer;
use common\models\User;
use common\models\UserSession;
use frontend\models\user\UserFacade;

class UserSessionRepository
{
    public $currentUserSession = null;

    public static $globalUserSession = null;

    public static function getGlobalUserSession()
    {
        if (self::$globalUserSession) {
            return self::$globalUserSession;
        }
        if (!\Yii::$app->has('session')) {
            return null;
        }
        if (\Yii::$app instanceof \yii\console\Application) {
            return null;
        }
        
        $sessionId = \Yii::$app->session->get('user_session_id');
        $userSession = null;
        if ($sessionId) {
            $userSession = UserSession::findOne(['id' => $sessionId]);
        }
        $needSave = false;
        if (!$userSession) {
            $userSession = new UserSession();
            $userSession->uuid = \Yii::$app->session->getId();
            $userSession->ip = $_SERVER['REMOTE_ADDR']??'';
            $userSession->referrer = \Yii::$app->session->get('referrer');
            $needSave = true;
        }
        $userId = UserFacade::getCurrentUserId();
        if ($userId) {
            $currentUser = UserFacade::getCurrentUser();
        } else {
            $userId = null;
            $currentUser = null;
        }

        $checkflag = false;

        if (!$userSession->user_id && $userId && (!($currentUser instanceof UserAdmin))) {
            $userSession->user_id = $userId;
            $needSave = true;
            $checkflag = true;
        }
        if ($needSave) {
            $userSession->safeSave();
            \Yii::$app->session->set('user_session_id', $userSession->id);
        }
        if ($checkflag) {
            UserSessionOwnersFixer::fixModelsOwnersForUser($userSession, $currentUser);
        }

        if ($userId && $userSession->user_id !== $userId) {
            $userSession = new UserSession();
            $userSession->uuid = \Yii::$app->session->id;
            $userSession->ip = $_SERVER['REMOTE_ADDR']??'';
            $userSession->referrer = \Yii::$app->session->get('referrer');
            if ($userId) {
                $userSession->user_id = $userId;
            }
            $userSession->safeSave();
            \Yii::$app->session->set('user_session_id', $userSession->id);
        }
        self::$globalUserSession = $userSession;
        return $userSession;

    }

    /**
     * @return UserSession
     */
    public function getCurrent()
    {
        if ($this->currentUserSession) {
            return $this->currentUserSession;
        }
        return self::getGlobalUserSession();
    }

    public function setCurrent($userSessionRepository)
    {
        $this->currentUserSession = $userSessionRepository;
    }

    /**
     * @param $id
     * @return UserSession
     */
    public static function getById($id)
    {
        return UserSession::findByPk($id);
    }
}