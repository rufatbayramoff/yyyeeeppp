<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.11.16
 * Time: 16:28
 */

namespace common\models\repositories;

use common\components\ArrayHelper;
use common\models\PrinterColor;
use common\modules\xss\helpers\XssHelper;
use yii\base\BaseObject;

class PrinterColorRepository extends BaseObject
{
    protected static $colors = null;
    protected static $colorsById = null;

    public function init()
    {
        if (self::$colors === null) {
            $colors = PrinterColor::find()->all();
            self::$colors = ArrayHelper::index($colors, 'render_color');
            self::$colorsById = ArrayHelper::index($colors, 'id');
        }
    }

    /**
     * @param $code
     * @return null|PrinterColor
     */
    public function getByCode($code)
    {
        if (array_key_exists($code, self::$colors)) {
            return self::$colors[$code];
        }
        return null;
    }

    /**
     * @param $id
     * @return PrinterColor|null
     */
    public function getById($id):?PrinterColor
    {
        if (array_key_exists($id, self::$colorsById)) {
            return self::$colorsById[$id];
        }
        return null;
    }

   public function tryGetById($id): PrinterColor
   {
       $printerColor = $this->getById($id);
       if (!$printerColor) {
           $failedText = 'Not found printer color: '.$id;
           throw new \yii\web\NotFoundHttpException($failedText);
       }
       return $printerColor;
   }
}

