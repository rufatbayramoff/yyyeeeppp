<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.11.17
 * Time: 13:36
 */

namespace common\models\repositories;

use common\models\WikiMaterial;
use common\models\WikiMaterial2feature;
use common\models\WikiMaterialFeature;
use common\models\WikiMaterialGeoCity;
use common\models\WikiMaterialIndustry;
use common\models\WikiMaterialPrinterMaterial;
use common\models\WikiMaterialProperties;
use common\models\WikiMaterialRelated;
use Yii;

class WikiMaterialRepository
{
    /**
     * @param WikiMaterial $wikiMaterial
     * @throws \Exception
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function save(WikiMaterial $wikiMaterial)
    {
        $fileRepository = Yii::createObject(FileRepository::class);

        $relatedRecords = $wikiMaterial->relatedRecords;
        $wikiMaterial->safeSave();

        if (array_key_exists('wikiMaterialPhotos', $relatedRecords)) {
            foreach ($relatedRecords['wikiMaterialPhotos'] as $wikiMaterialPhoto) {
                if ($wikiMaterialPhoto->forDelete) {
                    $wikiMaterialPhoto->delete();
                    $fileRepository->delete($wikiMaterialPhoto->file);
                } else {
                    $fileRepository->save($wikiMaterialPhoto->file);
                    $wikiMaterialPhoto->wiki_material_id = $wikiMaterial->id;
                    $wikiMaterialPhoto->file_uuid = $wikiMaterialPhoto->file->uuid;
                    $wikiMaterialPhoto->safeSave();
                }
            }
        }

        if (array_key_exists('wikiMaterialFiles', $relatedRecords)) {
            foreach ($relatedRecords['wikiMaterialFiles'] as $wikiMaterialFile) {
                if ($wikiMaterialFile->forDelete) {
                    $wikiMaterialFile->delete();
                    $fileRepository->delete($wikiMaterialFile->file);
                } else {
                    $fileRepository->save($wikiMaterialFile->file);
                    $wikiMaterialFile->wiki_material_id = $wikiMaterial->id;
                    $wikiMaterialFile->safeSave();
                }
            }
        }

        WikiMaterialIndustry::deleteAll(['wiki_material_id' => $wikiMaterial->id]);

        if (array_key_exists('wikiMaterialIndustries', $relatedRecords)) {
            foreach ($relatedRecords['wikiMaterialIndustries'] as $wikiMaterialIndustry) {
                $wikiMaterialIndustry->wiki_material_id = $wikiMaterial->id;
                $wikiMaterialIndustry->safeSave();
            }
        }

        WikiMaterial2feature::deleteAll(['wiki_material_id' => $wikiMaterial->id]);

        if (array_key_exists('wikiMaterialFeatures', $relatedRecords)) {
            foreach ($relatedRecords['wikiMaterialFeatures'] as $wikiMaterialFeature) {
                $wikiMaterial2Feature = new WikiMaterial2feature(['wiki_material_id' => $wikiMaterial->id, 'wiki_material_feature_id' => $wikiMaterialFeature->id]);
                $wikiMaterial2Feature->safeSave();
            }
        }

        WikiMaterialPrinterMaterial::deleteAll(['wiki_material_id' => $wikiMaterial->id]);
        if (array_key_exists('printerMaterials', $relatedRecords)) {
            foreach ($relatedRecords['printerMaterials'] as $printerMaterial) {
                $wikiPrinterMaterial = new WikiMaterialPrinterMaterial(['wiki_material_id' => $wikiMaterial->id, 'printer_material_id' => $printerMaterial->id]);
                $wikiPrinterMaterial->safeSave();
            }
        }

        WikiMaterialRelated::deleteAll(['or', ['wiki_material_id' => $wikiMaterial->id], ['link_wiki_material_id' => $wikiMaterial->id]]);
        if (array_key_exists('wikiMaterialRelatedObjects', $relatedRecords)) {
            foreach ($relatedRecords['wikiMaterialRelatedObjects'] as $wikiMaterialRelatedObject) {
                $wikiPrinterMaterial = new WikiMaterialRelated(['wiki_material_id' => $wikiMaterial->id, 'link_wiki_material_id' => $wikiMaterialRelatedObject->id]);
                $wikiPrinterMaterial->safeSave();
            }
        }

        WikiMaterialGeoCity::deleteAll(['wiki_material_id' => $wikiMaterial->id]);
        if (array_key_exists('geoCities', $relatedRecords)) {
            foreach ($relatedRecords['geoCities'] as $geoCity) {
                $wikiMaterialGeoCity = new WikiMaterialGeoCity(['wiki_material_id' => $wikiMaterial->id, 'geo_city_id' => $geoCity->id]);
                $wikiMaterialGeoCity->safeSave();
            }
        }
        $wikiMaterial->refresh();
    }

    /**
     * @param WikiMaterial $wikiMaterial
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function delete(WikiMaterial $wikiMaterial)
    {
        $fileRepository = Yii::createObject(FileRepository::class);
        foreach ($wikiMaterial->wikiMaterialPhotos as $wikiMaterialPhoto) {
            $wikiMaterialPhoto->delete();
            $wikiMaterialPhoto->file->delete();
        }

        $wikiFiles = $wikiMaterial->wikiMaterialFiles;
        foreach ($wikiFiles as $wikiMaterialFile) {
            $wikiMaterialFile->delete();
            $fileRepository->delete($wikiMaterialFile->file);
        }
        WikiMaterialProperties::deleteAll(['wiki_material_id' => $wikiMaterial->id]);
        WikiMaterialIndustry::deleteAll(['wiki_material_id' => $wikiMaterial->id]);
        WikiMaterial2feature::deleteAll(['wiki_material_id' => $wikiMaterial->id]);
        WikiMaterialPrinterMaterial::deleteAll(['wiki_material_id' => $wikiMaterial->id]);
        WikiMaterialRelated::deleteAll(['or', ['wiki_material_id' => $wikiMaterial->id], ['link_wiki_material_id' => $wikiMaterial->id]]);
        WikiMaterialGeoCity::deleteAll(['wiki_material_id' => $wikiMaterial->id]);
        $wikiMaterial->delete();
    }
}