<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.05.17
 * Time: 9:46
 */

namespace common\models\repositories;

use common\models\GeoCountry;

class GeoCountyRepository
{
    protected static $geoCountries = [];

    /**
     * @param $isoCode
     * @return GeoCountry
     */
    public function getCountryByCode($isoCode): GeoCountry
    {
        if (array_key_exists($isoCode, self::$geoCounries)) {
            return self::$geoCountries[$isoCode];
        }
        $geoCounry = GeoCountry::find()->where(['iso_code'=>$isoCode])->one();
        self::$geoCountries[$isoCode] = $geoCounry;
        return $geoCounry;
    }
}