<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.02.18
 * Time: 18:07
 */

namespace common\models\repositories;

use common\components\exceptions\InvalidModelException;
use common\components\FileDirHelper;
use common\components\FileTypesHelper;
use common\interfaces\FileBaseInterface;
use common\models\Company;
use common\models\File;
use common\models\FileAdmin;
use common\models\StoreOrderReview;
use common\models\User;
use common\models\user\UserIdentityProvider;
use yii\db\Expression;
use yii\db\Query;

class FileRepository
{
    /** @var UserIdentityProvider */
    protected $userIdentityProvider;

    /** @var UserSessionRepository */
    protected $userSessionRepository;


    public function injectDependencies(
        UserIdentityProvider $userIdentityProvider,
        UserSessionRepository $userSessionRepository
    )
    {
        $this->userIdentityProvider  = $userIdentityProvider;
        $this->userSessionRepository = $userSessionRepository;
    }


    public function one(int $id): File
    {
        return File::tryFindByPk($id);
    }

    public function getByUuid($uuid)
    {
        $file = File::find()->uuid($uuid)->one();
        if ($file) {
            return $file;
        }
        $fileAdmin = FileAdmin::find()->andWhere(['uuid' => $uuid])->one();
        if ($fileAdmin) {
            return $fileAdmin;
        }
        return null;
    }

    /**
     * @param FileBaseInterface $file
     * @throws InvalidModelException
     * @throws \Exception
     */
    public function save(FileBaseInterface $file)
    {
        if (!FileTypesHelper::isAllowExtensions($file)) {
            throw new InvalidModelException($file, 'File extension "' . $file->getCleanFileNameV3() . '" is not in allow file extension list');
        }
        if ($file->size === 0) {
            throw new InvalidModelException($file, 'Invalid zerro file "' . $file->getCleanFileNameV3() . '" size');
        }
        $beforeSavePath = null;
        if ((!$file->path) || (isset($file->oldAttributes['is_public']) && $file->is_public !== $file->oldAttributes['is_public'])) {
            $beforeSavePath = $file->getLocalTmpFilePath();
            $file->produceFilePath();
        }

        $file->safeSave();

        if ($file->getScenario() === FileBaseInterface::SCENARIO_CREATE) {
            $file->setScenario(\yii\base\Model::SCENARIO_DEFAULT);
            $file->produceStoredName();
            $file->save(false);
        }

        $currentPath = $file->getLocalTmpFilePath();
        if ($beforeSavePath && ($beforeSavePath !== $currentPath)) {
            $newDir = dirname($currentPath);
            FileDirHelper::createDir($newDir);

            if ($file->is_public) {
                @rename($beforeSavePath, $currentPath);
            } else {
                FileDirHelper::renameForNfs($beforeSavePath, $currentPath);
            }
        }

        return;
        $newmd5 = md5_file($currentPath);
        if ($newmd5 != $file->md5sum) {
            \Yii::error("[md5 error] $currentPath [old " . $file->md5sum . "] [new $newmd5]");
            throw new \Exception('Error saving file.');
        }
    }

    public function deleteFileByIds(array $ids)
    {
        $files = File::find()->id($ids)->all();
        foreach ($files as $file) {
            $this->delete($file);
        }

    }

    /**
     * @param FileBaseInterface|File $file
     *
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete(FileBaseInterface $file)
    {
        $path = $file->getLocalTmpFilePath();

        $fileCompressed = $file->fileCompressed;

        if ($fileCompressed) {
            foreach (param('fileCompression')['decompressor'] as $ext => $compressorParams) {
                if (file_exists($path . '.' . $ext)) {
                    @unlink($path . '.' . $ext);
                }
            }

            $fileCompressed->delete();
        }

        if (file_exists($path)) {
            @unlink($path);
        }

        $file->delete();
    }

    /**
     * @param FileBaseInterface $file
     *
     */
    public function safeDelete(FileBaseInterface $file): void
    {
        $file->deleted_at = dbexpr('NOW()');
        $file->safeSave(['deleted_at']);
    }

    public function findFilesForUser($user, $userSession, $uploadedFilesUuids): array
    {
        $query = File::find();

        if ($user) {
            $query->user($user);
        } else {
            $query->userSession($userSession);
        }
        $query->andWhere(['uuid' => $uploadedFilesUuids]);
        return $query->all();
    }

    /**
     * @param $id
     * @param User $user
     * @return File[]
     * @throws \yii\web\NotFoundHttpException
     */
    public function findCurrentUserFiles($uploadedFilesUuids): array
    {
        $currentUser = $this->userIdentityProvider->getUser();
        $userSession = $this->userSessionRepository->getCurrent();

        return $this->findFilesForUser($currentUser, $userSession, $uploadedFilesUuids);
    }

    /**
     * @param Company $company
     * @param int $limit
     * @return File[]
     */
    public function lastReviewFiles(Company $company, int $limit = 4): array
    {
        $mainQuery = (new Query())->from(['file' => 'file'])
            ->innerJoin(['store_order_review_file' => 'store_order_review_file'], 'file.uuid=store_order_review_file.file_uuid')
            ->innerJoin(['store_order_review' => 'store_order_review'], 'store_order_review.id=store_order_review_file.review_id')
            ->andWhere(['store_order_review.ps_id' => $company->id])
            ->andWhere(['store_order_review.status' => StoreOrderReview::STATUS_MODERATED])
            ->orderBy(['store_order_review.id' => SORT_DESC])
            ->select(['file.id', 'store_order_review_file.review_id']);

        $fileIds = (new Query())
            ->select(['id' => new Expression('max(t.id)')])
            ->from(['t' => $mainQuery])
            ->groupBy('t.review_id')
            ->orderBy(['t.review_id' => SORT_DESC])
            ->limit($limit)
            ->column();

        return File::find()
            ->andWhere(['id' => $fileIds])
            ->all();
    }

}