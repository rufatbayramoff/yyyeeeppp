<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.12.17
 * Time: 12:23
 */

namespace common\models\repositories;

use common\models\CompanyServiceCategory;

class CompanyServiceCategoryRepository
{
    /**
     * @throws \yii\base\Exception
     */
    public function save(CompanyServiceCategory $category)
    {
        if ($category->isNewRecord) {
            if ($category->parentCategory) {
                $category->appendTo($category->parentCategory);
            } else {
                $category->safeSave();
            }
            return ;
        }
        if ($category->orginalParentCategory && $category->orginalParentCategory->id !== $category->parentCategory->id) {
            $category->appendTo($category->parentCategory);
        }
        $category->safeSave();
    }

    public function getLaserCutting()
    {
        return CompanyServiceCategory::tryFind(['code' => CompanyServiceCategory::CODE_LASER_CUTTING]);
    }

    /**
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public function all()
    {
        return CompanyServiceCategory::find()->withoutRoot()->andWhere(['is_active' => 1])->orderBy('lft')->all();
    }

    public function findBySlug(?string $slug)
    {
        return CompanyServiceCategory::findOne(['slug' => $slug]);
    }
}