<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.12.17
 * Time: 16:14
 */

namespace common\models\repositories;

use common\models\WikiMachine;

class WikiMachineRepository
{

    /** @var FileRepository */
    public $fileRepository;

    public function injectDependencies(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param WikiMachine $wikiMachine
     * @throws \Exception
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function save(WikiMachine $wikiMachine)
    {
        if ($wikiMachine->mainPhotoFile) {
            $this->fileRepository->save($wikiMachine->mainPhotoFile);
            $wikiMachine->main_photo_file_uuid = $wikiMachine->mainPhotoFile->uuid;
        }

        if ($wikiMachine->deleteMainPhotoFile) {
            $wikiMachine->main_photo_file_uuid = null;
        }


        $relatedRecords = $wikiMachine->relatedRecords;
        $wikiMachine->safeSave();

        if ($wikiMachine->deleteMainPhotoFile) {
            $this->fileRepository->delete($wikiMachine->mainPhotoFile);
        }

        if ($wikiMachine->deletedMainPhotoFiles) {
            foreach ($wikiMachine->deletedMainPhotoFiles as $deletedMailPhotoFile) {
                if ($deletedMailPhotoFile->uuid != $wikiMachine->main_photo_file_uuid) {
                    $this->fileRepository->delete($deletedMailPhotoFile);
                }
            }
        }

        if (array_key_exists('photoFiles', $relatedRecords)) {
            foreach ($relatedRecords['photoFiles'] as $photoFile) {
                if ($photoFile->forDelete) {
                    $photoFile->wikiMachinePhoto->delete();
                    $this->fileRepository->delete($photoFile);
                } else {
                    $this->fileRepository->save($photoFile);
                    $photoFile->wikiMachinePhoto->wiki_machine_id = $wikiMachine->id;
                    $photoFile->wikiMachinePhoto->photo_file_uuid = $photoFile->uuid;
                    $photoFile->wikiMachinePhoto->safeSave();
                }
            }
        }

        if (array_key_exists('files', $relatedRecords)) {
            foreach ($relatedRecords['files'] as $file) {
                if ($file->forDelete) {
                    $file->wikiMachineFile->delete();
                    $this->fileRepository->delete($file);
                } else {
                    $this->fileRepository->save($file);
                    $file->wikiMachineFile->wiki_machine_id = $wikiMachine->id;
                    $file->wikiMachineFile->file_uuid = $file->uuid;
                    $file->wikiMachineFile->safeSave();
                }
            }
        }
        $wikiMachine->refresh();
    }
}