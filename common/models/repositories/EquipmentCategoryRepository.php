<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.12.17
 * Time: 12:23
 */

namespace common\models\repositories;

use common\models\EquipmentCategory;

class EquipmentCategoryRepository
{
    /**
     * @param EquipmentCategory $equipmentCategory
     * @throws \yii\base\Exception
     */
    public function save(EquipmentCategory $equipmentCategory)
    {
        if ($equipmentCategory->isNewRecord) {
            if ($equipmentCategory->parentCategory) {
                $equipmentCategory->appendTo($equipmentCategory->parentCategory);
            } else {
                $equipmentCategory->safeSave();
            }
            return ;
        }
        if ($equipmentCategory->orginalParentCategory && $equipmentCategory->orginalParentCategory->id !== $equipmentCategory->parentCategory->id) {
            $equipmentCategory->appendTo($equipmentCategory->parentCategory);
        }
        $equipmentCategory->safeSave();
    }
}