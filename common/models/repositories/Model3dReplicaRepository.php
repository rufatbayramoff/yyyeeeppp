<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.08.16
 * Time: 18:06
 */

namespace common\models\repositories;

use common\models\Model3dReplica;
use ErrorException;

class Model3dReplicaRepository
{
    /**
     * @param Model3dReplica $model3dReplica
     * @throws ErrorException
     * @throws \yii\base\Exception
     */
    public static function save(Model3dReplica $model3dReplica)
    {
        $deleteModel3dTexture = null;
        if ($model3dReplica->model3dTexture) {
            if ($model3dReplica->model3dTexture->isMarkForDelete) {
                $deleteModel3dTexture = $model3dReplica->model3dTexture;
                $model3dReplica->model3d_texture_id = null;
            } else {
                $model3dReplica->model3dTexture->safeSave();
                $model3dReplica->model3d_texture_id = $model3dReplica->model3dTexture->id;
            }
        }
        if($model3dReplica->coverFile){
            $model3dReplica->coverFile->safeSave();
        }
        $relatedRecords = $model3dReplica->relatedRecords;
        if (!$model3dReplica->save()) {
            throw new ErrorException('Cant save model3dReplica: ' . json_encode($model3dReplica->getErrors()));
        };
        if ($deleteModel3dTexture) {
            $deleteModel3dTexture->delete();
        }
        if (array_key_exists('model3dParts', $relatedRecords)) {
            foreach ($relatedRecords['model3dParts'] as $model3dPart) {
                if ($model3dPart->model3dTexture) {
                    $model3dPart->model3dTexture->safeSave();
                }
                if ($model3dPart->model3dPartProperties) {
                    $model3dPart->model3dPartProperties->safeSave();
                    $model3dPart->model3d_part_properties_id = $model3dPart->model3dPartProperties->id;
                }
                if ($model3dPart->model3dPartCncParams) {
                    $model3dPart->model3dPartCncParams->safeSave();
                }
                if (!$model3dPart->save()) {
                    throw new ErrorException('Cant save model3dPart: ' . json_encode($model3dPart->getErrors()));
                }
            }
        }
        if (array_key_exists('model3dImgs', $relatedRecords)) {
            foreach ($relatedRecords['model3dImgs'] as $model3dImg) {
                if (!$model3dImg->save()) {
                    throw new ErrorException('Cant save model3dImg: ' . json_encode($model3dImg->getErrors()));
                }
            }
        }
    }
}