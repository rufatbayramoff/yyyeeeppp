<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.05.17
 * Time: 13:13
 */


namespace common\models\repositories;

use common\components\ArrayHelper;
use common\models\PrinterMaterialGroup;
use yii\base\BaseObject;

class PrinterMaterialGroupRepository extends BaseObject
{
    protected static $materialGroups = null;
    protected static $materialsGroupsById = null;

    public function init()
    {
        if (self::$materialGroups === null) {
            $materialGroups = PrinterMaterialGroup::find()->all();

            self::$materialGroups = ArrayHelper::index($materialGroups, 'code');
            self::$materialsGroupsById = ArrayHelper::index($materialGroups, 'id');
        }
    }

    /**
     * @param $code
     * @return null|PrinterMaterialGroup
     */
    public function getByCode($code)
    {
        if (array_key_exists($code, self::$materialGroups)) {
            return self::$materialGroups[$code];
        }
        return null;
    }


    /**
     * @param $id
     * @return null|PrinterMaterialGroup
     */
    public function getById($id)
    {
        if (array_key_exists($id, self::$materialsGroupsById)) {
            return self::$materialsGroupsById[$id];
        }
        return null;
    }

    public function tryGetById($id): PrinterMaterialGroup
    {
        $printerMaterialGroup = $this->getById($id);
        if (!$printerMaterialGroup) {
            $failedText = 'Not found printer material group: '.$id;
            throw new \yii\web\NotFoundHttpException($failedText);
        }
        return $printerMaterialGroup;
    }


    public function isGroupCodeExists($code)
    {
        return array_key_exists($code, self::$materialGroups);
    }

    /**
     * @param $materialGroupUsageFilter
     * @return PrinterMaterialGroup[]
     */
    public function getMaterialsGroupsByUsageFilter($materialGroupUsageFilter): array
    {
        $usageGroups = \Yii::$app->setting->get('store.usage_groups');
        if (!array_key_exists($materialGroupUsageFilter, $usageGroups)) {
            return [];
        }
        $groups  = $usageGroups[$materialGroupUsageFilter]['groups'];
        $resultGroups = [];
        foreach ($groups as $groupCode) {
            if ($this->isGroupCodeExists($groupCode)) {
                $resultGroups[] = $this->getByCode($groupCode);
            }
        }
        return $resultGroups;
    }

}