<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.09.16
 * Time: 13:15
 */

namespace common\models\repositories;

use common\models\CatalogCost;
use common\models\GeoCountry;
use common\models\Model3dTextureCatalogCost;
use common\models\StoreUnit;
use DateTime;
use DateTimeZone;
use lib\geo\models\Location;
use Yii;

class CatalogCostRepository
{
    public static function saveCostForStoreUnit(StoreUnit $storeUnit, Location $location, $costUsd)
    {
        $country = GeoCountry::findOne(['iso_code' => $location->country]);
        $countryId = $country->id;
        $calcCost = CatalogCost::findOne(['store_unit_id' => $storeUnit->id, 'country_id' => $countryId]);
        if (!$calcCost) {
            $calcCost = new CatalogCost();
            $calcCost->store_unit_id = $storeUnit->id;
            $calcCost->country_id = $countryId;
        }
        $calcCost->cost_usd = $costUsd;
        $calcCost->update_date = (new DateTime('now' ,new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $calcCost->safeSave();
    }

    /**
     * @param $storeUnit
     * @param $currentCountry
     * @return CatalogCost|null
     */
    public static function findCost($storeUnit, $currentCountry)
    {
        $calcCost = CatalogCost::findOne(['store_unit_id' =>$storeUnit->id, 'country_id' =>$currentCountry->id]);
        if (!$calcCost) {
            return null;
        }
        return $calcCost;
    }

    public static function invalidateCost($storeUnit)
    {
       CatalogCost::updateAll(['update_date' => CatalogCost::CATALOG_START_TIME],['store_unit_id' =>$storeUnit->id]);
    }

    public static function invalidateAllCosts()
    {
        CatalogCost::updateAll(['update_date' => CatalogCost::CATALOG_START_TIME]);
    }

    public static function truncateAllCosts(): void
    {
        Yii::$app->getDb()-> createCommand('TRUNCATE TABLE catalog_cost;')->execute();
        Model3dTextureCatalogCost::deleteAll();
    }
}