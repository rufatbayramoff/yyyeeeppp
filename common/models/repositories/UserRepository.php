<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.06.18
 * Time: 11:34
 */

namespace common\models\repositories;

use common\models\CompanyService;
use common\models\PsMachineDelivery;
use common\models\User;
use yii\base\BaseObject;

class UserRepository extends BaseObject
{
    /**
     * @param User $user
     * @throws \yii\base\Exception
     */
    public function save(User $user)
    {
        $profile = $user->userProfile;
        $user->safeSave();
        if ($profile->isNewRecord) {
            $profile->user_id = $user->id;
            $profile->safeSave();
        }
    }
}