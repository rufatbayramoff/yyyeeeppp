<?php

namespace common\models\repositories;

use common\models\CompanyService;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderReview;
use common\models\StoreOrderReviewFile;

class ReviewRepository
{
    /**
     * @param Ps $ps
     *
     * @return array [[pinter.id => pinter.title], ...]
     */
    public static function getReviewPrinterListForPs(Ps $ps): array
    {
        return StoreOrderReview::find()
            ->select([
                "IFNULL(cs.title, IFNULL(pp.title, concat('CNC Service (#', cs.ps_cnc_machine_id, ')'))) as title",
                'cs.id'
            ])
            ->from(['sor' => StoreOrderReview::tableName()])
                ->innerJoin(['so' => StoreOrder::tableName()], 'so.id = sor.order_id')
                ->innerJoin(['soa' => StoreOrderAttemp::tableName()], 'so.current_attemp_id = soa.id')
                ->innerJoin(['cs' => CompanyService::tableName()], 'cs.id = soa.machine_id')
                ->leftJoin(['pp' => PsPrinter::tableName()], 'pp.id = cs.ps_printer_id')
            ->where([
                'sor.ps_id'  => $ps->id,
                'sor.status' => StoreOrderReview::STATUS_MODERATED
            ])
            ->indexBy('id')
            ->asArray()
            ->orderBy(['title' => SORT_ASC])
            ->groupBy('cs.id')
            ->column();
    }

    /**
     * @param $psId
     *
     * @return StoreOrderReview[]
     */
    public static function getReviewsToPrintByPsId($psId): array
    {
        //
        /** @var Ps $ps */
        $ps = Ps::find()
            ->moderated()
            ->active()
            ->andWhere(['ps.id' => $psId])
            ->one();

        if ($ps) {
            return StoreOrderReview::find()
                ->from(['r' => StoreOrderReview::tableName()])
                ->innerJoin(['sorf' => StoreOrderReviewFile::tableName()], 'sorf.review_id = r.id')
                ->with(['ps', 'order.user.company', 'reviewCoverFile'])
                ->where([
                    'and',
                    ['=',  'r.ps_id', $ps->id],
                    ['=',  'r.status', StoreOrderReview::STATUS_MODERATED],
                    ['!=', 'r.comment', '']
                ])
                ->groupBy('r.id')
                ->orderBy(['created_at' => SORT_DESC])
                ->limit(5)
                ->all();
        }

        return [];
    }

    /**
     * @param array $Ids - reviews ids
     *
     * @return array
     */
    public static function getReviewsById($Ids): array
    {
        return StoreOrderReview::find()
            ->from(['sor' => StoreOrderReview::tableName()])
                ->leftJoin(['sorf' => StoreOrderReviewFile::tableName()], 'sorf.review_id = id')
                ->with(['ps', 'order.user.company', 'reviewCoverFile'])
            ->where([
                'and',
                ['in',  'sor.id', $Ids],
                ['=',  'sor.status', StoreOrderReview::STATUS_MODERATED],
            ])
            ->orderBy(['created_at' => SORT_DESC])
            ->groupBy('sor.id')
            ->limit(5)
            ->all();
    }

    public static function getReviewsToPrintByPsPrinterId($psPrinterId): array
    {
        return StoreOrderReview::find()
            ->from(['sor' => StoreOrderReview::tableName()])
                ->innerJoin(['sorf' => StoreOrderReviewFile::tableName()], 'sorf.review_id = sor.id')
                ->innerJoin(['so' => StoreOrder::tableName()], 'so.id = sor.order_id')
                ->innerJoin(['soa' => StoreOrderAttemp::tableName()], 'so.current_attemp_id = soa.id')
                ->innerJoin(['cs' => CompanyService::tableName()], 'cs.id = soa.machine_id')
            ->with(['ps', 'order.user.company', 'reviewCoverFile'])
            ->where([
                'and',
                ['=', 'cs.ps_printer_id',  $psPrinterId],
                ['=', 'sor.status', StoreOrderReview::STATUS_MODERATED],
                ['!=', 'sor.comment', '']
            ])
            ->groupBy('sor.id')
            ->orderBy(['sor.created_at' => SORT_DESC])
            ->limit(5)
            ->all();
    }


    /**
     * @return StoreOrderReview[]
     * @throws \yii\base\UserException
     */
    public static function getReviewsToPrintByBackendSetting(): array
    {

        $printReviewCount = (\Yii::$app->setting->get('printer.printReviewCount') ?? null);

        if (!$printReviewCount) {
            return [];
        }

        return StoreOrderReview::find()
            ->with(['ps', 'order.user.company', 'reviewCoverFile'])
            ->where([
                'and',
                ['=', 'status', StoreOrderReview::STATUS_MODERATED],
                ['=', 'public_to_print', 1]
            ])
            ->orderBy(['created_at' => SORT_DESC])
            ->limit((int) $printReviewCount)
            ->all();
    }
}