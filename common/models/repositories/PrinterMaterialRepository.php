<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.11.16
 * Time: 16:28
 */

namespace common\models\repositories;

use common\components\ArrayHelper;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\modules\xss\helpers\XssHelper;
use yii\base\BaseObject;

class PrinterMaterialRepository extends BaseObject
{
    /**
     * @var null|PrinterMaterial[]
     */
    protected static $materials = null;
    protected static $materialsById = null;

    /**
     * @var array|null
     */
    protected static $materialsByGroups = null;

    public function init()
    {
        if (self::$materials === null) {
            $materials = PrinterMaterial::find()->all();
            self::$materials = ArrayHelper::index($materials, 'filament_title');
            if (\defined('TEST_XSS') && TEST_XSS) {
                self::$materials = XssHelper::cleanArrayKeys(self::$materials);
            }
            self::$materialsById = ArrayHelper::index($materials, 'id');
        }
    }

    protected function formMaterialsByGroups()
    {
        if (self::$materialsByGroups === null) {
            foreach (self::$materials as $material) {
                self::$materialsByGroups[$material->group_id][$material->filament_title] = $material;
            }
        }
    }

    /**
     * @param $code
     * @return null|PrinterMaterial
     */
    public function getByCode($code)
    {
        if (array_key_exists($code, self::$materials)) {
            return self::$materials[$code];
        }
        return null;
    }

    /**
     * @param $id
     * @return null|PrinterMaterial
     */
    public function getById($id)
    {
        if (array_key_exists($id, self::$materialsById)) {
            return self::$materialsById[$id];
        }
        return null;
    }

    /**
     * @param PrinterMaterialGroup[] $printerMaterialGroups
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public function getMaterialsByGroups($printerMaterialGroups)
    {
        $this->formMaterialsByGroups();

        $returnMaterials = [];
        foreach ($printerMaterialGroups as $materialGroup) {
            $materialsInGroup = self::$materialsByGroups[$materialGroup->id];
            foreach ($materialsInGroup as $material) {
                $returnMaterials[$material->id] = $material;
            }
        }
        return $returnMaterials;
    }

    /**
     * @return PrinterMaterial[]|null
     */
    public function getAll()
    {
        return self::$materials;
    }
}

