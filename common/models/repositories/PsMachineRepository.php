<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.07.17
 * Time: 16:15
 */

namespace common\models\repositories;

use common\models\CompanyService;
use common\models\PsMachineDelivery;
use yii\base\BaseObject;

class PsMachineRepository extends BaseObject
{
    public function save(CompanyService $psMachine)
    {
        if ($psMachine->psCncMachine) {
            $psMachine->ps_cnc_machine_id = $psMachine->psCncMachine->id;
        } elseif ($psMachine->psPrinter) {
            $psMachine->ps_printer_id = $psMachine->psPrinter->id;
        }
        if ($psMachine->location) {
            $psMachine->location->safeSave();
            $psMachine->location_id = $psMachine->location->id;
        }
        $psMachine->safeSave();
        $psMachine->refresh();
    }
}