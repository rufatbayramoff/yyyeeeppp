<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.08.16
 * Time: 18:45
 */

namespace common\models\repositories;


use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\interfaces\Model3dBaseInterface;
use common\models\ApiPrintablePack;
use common\models\File;
use common\models\Model3d;
use common\models\Model3dHistory;
use common\models\Model3dImg;
use common\models\Model3dPart;
use common\models\Model3dReplica;
use common\models\Model3dReplicaImg;
use common\models\Model3dReplicaPart;
use common\models\Model3dTag;
use common\models\Model3dTexture;
use common\models\Model3dViewedState;
use common\models\ThingiverseThing;
use common\models\User;
use common\models\UserCollectionModel3d;
use common\models\UserSession;
use common\modules\product\interfaces\ProductInterface;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\BaseObject;
use yii\db\Exception;

class Model3dRepository extends BaseObject
{
    /**
     * find published model3d with store unit
     *
     * @param User $user
     * @param int $limit
     *
     * @return \common\models\Model3d[]
     */
    public static function findModel3dByUser(User $user, $limit = 12)
    {
        return Model3d::find()->joinWith('storeUnit')->where(
            [
                'user_id'        => $user->id,
                'status_product' => ProductInterface::STATUS_PUBLISHED_PUBLIC,
            ]
        )->with(
            [
                'model3dParts',
                'user.userProfile',
                'model3dParts.file',
                'model3dImgs.file',
                'coverFile',
                'model3dTags',
                'model3dTags.tag'
            ]
        )->orderBy('published_at DESC')->limit($limit)->all();
    }

    /**
     * @param null $userSession
     * @param int $limit
     *
     * @return Model3d[]
     */
    public static function findRecentModels($userSession = null, $limit = 4)
    {
        /**
         * @var $userSession UserSession
         */
        $userSession = $userSession ?: UserFacade::getUserSession();

        $query = Model3d::find()->where([
            'product_status' => [
                ProductInterface::STATUS_PUBLISHED_PUBLIC,
                ProductInterface::STATUS_PUBLISHED_DIRECTLY,
                ProductInterface::STATUS_PUBLISHED_UPDATED
            ]
        ])->with([
            'model3dParts',
            'model3dParts.file',
            'model3dImgs.file',
            'coverFile',
        ])
            ->andWhere(['source' => Model3d::SOURCE_WEBSITE])
            ->active()
            ->hasActiveModel3dParts()
            ->orderBy('model3d.id DESC')->limit($limit);
        $query->joinWith(['model3dParts']);
        $query->andWhere('model3d_part.id is not null');

        if (!empty($userSession->user_id)) {
            $query->andWhere(['user_id' => $userSession->user_id]);
        } else {
            $query->userSession($userSession);
        }
        $items = $query->all();
        return $items;
    }

    /*
     * try to find model3d by file id
     *
     * @param $fileId
     * @return Model3d|null
     */
    public static function findByFileId($fileId)
    {
        $model3dPart = Model3dPart::findOne(['file_id' => $fileId]);
        if ($model3dPart && $model3dPart->model3d) {
            return $model3dPart->model3d;
        }
        return null;
    }

    /**
     * @param Model3d $model3d
     *
     * @param $debugLog
     *
     * @return void
     * @throws Exception
     * @throws \Throwable
     */
    public static function destroyModel3d(Model3d $model3d, $debugLog = null)
    {
        $dbTransaction = app()->db->beginTransaction();

        try {
            $filesForDelete = [];

            foreach ($model3d->model3dReplicas as $model3dReplica) {
                $fileReplica = self::destroyModel3dReplica($model3dReplica);

                if (!empty($fileReplica)) {
                    $filesForDelete = ArrayHelper::merge($filesForDelete, $fileReplica);
                }
            }

            // remove part
            /** @var Model3dPart $model3dPart */
            foreach ($model3d->model3dParts as $model3dPart) {
                foreach ($model3dPart->model3dParts0 as $convertedPart) {
                    if (!$convertedPart->original_model3d_part_id) {
                        if ($convertedPart->file) {
                            $filesForDelete[] = $convertedPart->file;
                        }

                        if ($convertedPart->fileSrc) {
                            $filesForDelete[] = $convertedPart->fileSrc;
                        }
                    }

                    $convertedPart->delete();
                }

                if (!$model3dPart->original_model3d_part_id) {
                    if ($model3dPart->file) {
                        $filesForDelete[] = $model3dPart->file;
                    }

                    if ($model3dPart->fileSrc) {
                        $filesForDelete[] = $model3dPart->fileSrc;
                    }
                }

                $model3dPart->delete();
            }

            // remove images
            foreach ($model3d->model3dImgs as $model3dImg) {
                if (!$model3dImg->original_model3d_img_id) {
                    if ($model3dImg->file) {
                        $filesForDelete[] = $model3dImg->file;
                    }

                    if ($model3dImg->originalFile) {
                        $filesForDelete[] = $model3dImg->originalFile;
                    }
                }
                foreach ($model3dImg->model3dImgs as $model3dImgCopy) {
                    $model3dImgCopy->original_model3d_img_id = null;
                    $model3dImgCopy->save();
                }

                foreach ($model3dImg->model3dReplicaImgs as $model3dReplicaImgFix) {
                    if ($model3dImg->original_model3d_img_id) {
                        $model3dReplicaImgFix->original_model3d_img_id = $model3dImg->original_model3d_img_id;
                        $model3dReplicaImgFix->save(true, ['original_model3d_img_id']);
                    } else {
                        throw new BusinessException('Cant delete model3d img');
                    }
                }

                $model3dImg->delete();
            }

            Model3d::updateAll(['original_model3d_id'=>null], ['original_model3d_id'=>$model3d->id]);
            Model3d::updateAll(['master_store_unit_id'=>null], ['master_store_unit_id'=>$model3d->storeUnit->id]);

            if ($model3d->storeUnitWithouMaster) {
               $model3d->storeUnit->delete();
            }

            // Delete related data

            Model3dHistory::deleteAll(['model3d_id' => $model3d->id]);
            Model3dViewedState::deleteAll(['model3d_id' => $model3d->id]);
            ApiPrintablePack::deleteAll(['model3d_id' => $model3d->id]);
            Model3dTag::deleteAll(['model3d_id' => $model3d->id]);
            UserCollectionModel3d::deleteAll(['model3d_id' => $model3d->id]);
            ThingiverseThing::deleteAll(['model3d_id' => $model3d->id]);
            $model3dProductCommon = $model3d->productCommon;

            $model3d->delete();
            $model3dProductCommon->delete();

            // Clean  Files
            /** @var File $file */
            foreach ($filesForDelete as $file) {
                Model3d::updateAll(['cover_file_id'=>null], ['cover_file_id'=>$file->id]);

                foreach ($file->apiFiles as $apiFile) {
                    $apiFile->delete();
                }

                if (is_callable($debugLog)) {
                    $debugLog(' ' . $file->getLocalTmpFilePath());
                }

                $fileRepository = Yii::createObject(FileRepository::class);
                $fileRepository->delete($file);
            }

            $dbTransaction->commit();
            if (is_callable($debugLog)) {
                $debugLog(" deleted");
            }
        } catch (\Exception $ex) {
            $dbTransaction->rollBack();
            throw $ex;
        }
    }


    /**
     * @param Model3dReplica $model3dReplica
     *
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function destroyModel3dReplica(Model3dReplica $model3dReplica, $forceDelete=false): array
    {
        $filesForDelete = [];

        if ($model3dReplica->storeOrderItems) {
            foreach ($model3dReplica->storeOrderItems as $storeOrderItem) {
                if ($storeOrderItem->order->isPayed() && !$forceDelete) {
                    throw new BusinessException('You can not remove the replica, it is in the paid order.');
                }

                $storeOrderItem->delete();
            }
        }

        foreach ($model3dReplica->model3dReplicaParts as $model3dReplicaPart) {
            if (!$model3dReplicaPart->original_model3d_part_id) {
                if ($model3dReplicaPart->file) {
                    $filesForDelete[] = $model3dReplicaPart->file;
                }

                if ($model3dReplicaPart->fileSrc) {
                    $filesForDelete[] = $model3dReplicaPart->fileSrc;
                }
            }


            $model3dReplicaPart->delete();
        }

        foreach ($model3dReplica->model3dReplicaImgs as $model3dReplicaImg) {
            if (!$model3dReplicaImg->original_model3d_img_id && $model3dReplicaImg->file) {
                $filesForDelete[] = $model3dReplicaImg->file;
            }

            $model3dReplicaImg->delete();
        }

        foreach ($model3dReplica->cartItems as $cartItem) {
            $cartItem->cart->delete();
        }

        $model3dReplica->delete();

        return $filesForDelete;
    }

    /**
     * @param Model3dBaseInterface|Model3d $model3d
     *
     * @throws \yii\base\Exception
     * @throws \ErrorException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function save(Model3dBaseInterface $model3d)
    {
        if ($model3d instanceof Model3dReplica) {
            Model3dReplicaRepository::save($model3d);
            return;
        }
        $filesRepository = \Yii::createObject(FileRepository::class);
        $transaction     = app('db')->beginTransaction();
        try {
            $model3d->productCommon->safeSave();
            if ($model3d->coverFile) {
                $filesRepository->save($model3d->coverFile);
            }
            $relatedRecords = $model3d->relatedRecords;

            $afterSaveDeleteModel3dTextureId = null;
            if ($kitModel3dTexture = $model3d->kitModel3dTexture) {
                if ($kitModel3dTexture->isMarkForDelete) {
                    $afterSaveDeleteModel3dTextureId = $kitModel3dTexture->id;
                    $model3d->model3d_texture_id        = null;
                } else {
                    $kitModel3dTexture->safeSave();
                    $model3d->model3d_texture_id = $kitModel3dTexture->id;
                }
            }

            AssertHelper::assertSave($model3d);

            if ($afterSaveDeleteModel3dTextureId) {
                Model3dTexture::deleteAll(['id' => $afterSaveDeleteModel3dTextureId]);
            }

            if (array_key_exists('model3dParts', $relatedRecords)) {
                foreach ($relatedRecords['model3dParts'] as $model3dPart) {
                    $filesRepository->save($model3dPart->file);
                    if ($model3dPart->model3dTexture) {
                        $model3dPart->model3dTexture->safeSave();
                        $model3dPart->model3d_texture_id = $model3dPart->model3dTexture->id;
                    }
                    if ($model3dPart->model3dPartProperties) {
                        $model3dPart->model3dPartProperties->safeSave();
                        $model3dPart->model3d_part_properties_id = $model3dPart->model3dPartProperties->id;
                    }
                    if ($model3dPart->model3dPartCncParams) {
                        $model3dPart->model3dPartCncParams->safeSave();
                        $model3dPart->model3d_part_cnc_params_id = $model3dPart->model3dPartCncParams->id;
                    }
                    AssertHelper::assertSave($model3dPart);
                }
            }
            if (array_key_exists('model3dImgs', $relatedRecords)) {
                foreach ($relatedRecords['model3dImgs'] as $model3dImg) {
                    $filesRepository->save($model3dImg->file);
                    AssertHelper::assertSave($model3dImg);
                }
            }
            if (array_key_exists('storeUnit', $relatedRecords) && (!$relatedRecords['storeUnit']->model3d_id)) {
                $model3d->storeUnit->model3d_id = $model3d->id;
            }
            $model3d->storeUnit->safeSave();
            $transaction->commit();
            BaseActiveQuery::$staticCacheGlobalEnable = false;
            $model3d->refresh();
            $model3d->getActiveModel3dParts();
            $model3d->getActiveModel3dImages();
            BaseActiveQuery::$staticCacheGlobalEnable = false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param $model3dUId
     *
     * @return Model3dReplica|Model3d
     * @throws \yii\web\NotFoundHttpException
     */
    public function getByUid($model3dUId)
    {
        if (strpos($model3dUId, Model3dReplica::UID_PREFIX) === 0) { // Model3dReplica support
            $model3dReplicaId = substr($model3dUId, strlen(Model3dReplica::UID_PREFIX), 4048);
            $model3d          = Model3dReplica::tryFindByPk($model3dReplicaId);
        } elseif (strpos($model3dUId, Model3d::UID_PREFIX) === 0) {
            $modelId = substr($model3dUId, strlen(Model3d::UID_PREFIX), 4048);
            $model3d = Model3d::tryFindByPk($modelId);
        } else {
            // backward compatibility
            $model3d = Model3d::tryFindByPk($model3dUId);
        }
        return $model3d;
    }

    /**
     * @param $model3dPartUId
     *
     * @return Model3dPart|Model3dReplicaPart
     * @throws \yii\web\NotFoundHttpException
     */
    public function getPartByUid($model3dPartUId)
    {
        $model3dPart = null;
        if (strpos($model3dPartUId, Model3dReplicaPart::UID_PREFIX) === 0) { // Model3dReplica support
            $model3dReplicaPartId = substr($model3dPartUId, strlen(Model3dReplicaPart::UID_PREFIX), 4048);
            $model3dPart          = Model3dReplicaPart::tryFindByPk($model3dReplicaPartId);
        } elseif (strpos($model3dPartUId, Model3dPart::UID_PREFIX) === 0) { // Model3dPart
            $model3dPartId = substr($model3dPartUId, strlen(Model3dPart::UID_PREFIX), 4048);
            $model3dPart   = Model3dPart::tryFindByPk($model3dPartId);
        }
        return $model3dPart;
    }

    /**
     * @param $model3dImgUid
     *
     * @return Model3dImg|Model3dReplicaImg
     * @throws \yii\web\NotFoundHttpException
     */
    public function getImageByUid($model3dImgUid)
    {
        $model3dImg = null;
        if (strpos($model3dImgUid, Model3dReplicaImg::UID_PREFIX) === 0) { // Model3dReplica support
            $model3dReplicaImgId = substr($model3dImgUid, strlen(Model3dReplicaImg::UID_PREFIX), 4048);
            $model3dImg          = Model3dReplicaImg::tryFindByPk($model3dReplicaImgId);
        } elseif (strpos($model3dImgUid, Model3dImg::UID_PREFIX) === 0) { // Model3dImg
            $model3dImgId = substr($model3dImgUid, strlen(Model3dImg::UID_PREFIX), 4048);
            $model3dImg   = Model3dImg::tryFindByPk($model3dImgId);
        }
        return $model3dImg;
    }
}