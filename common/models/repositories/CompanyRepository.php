<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.06.18
 * Time: 14:23
 */

namespace common\models\repositories;

use common\models\Company;
use common\models\CompanyBlock;
use common\models\CompanyBlockImage;
use common\models\User;
use yii\base\BaseObject;
use yii\web\NotFoundHttpException;

class CompanyRepository extends BaseObject
{
    /** @var FileRepository */
    public $fileRepository;

    public function injectDependencies(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }
    /**
     * @param $id
     * @return Company
     */
    public function getById($id)
    {
        return Company::find()->where(['ps.id' => $id])->active()->one();
    }

    /**
     * @param int $id
     * @return Company
     * @throws NotFoundHttpException
     */
    public function get(int $id): Company
    {
        return Company::tryFindByPk($id);
    }

    public function saveCompany(Company $company)
    {
        if ($company->user) {
            $company->user_id = $company->user->id;
        }
        if ($company->location) {
            $company->location->user_id = $company->user_id;
            $company->location->safeSave();
            $company->location_id = $company->location->id;
        }
        $company->safeSave();
    }

    /**
     * @param Company $company
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete(Company $company)
    {
        foreach ($company->companyHistories as $history) {
            $history->delete();
        }
        $company->delete();
        $company->user->delete();
    }

    /**
     * @param CompanyBlock $companyBlock
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     */
    public function saveBlock(CompanyBlock $companyBlock)
    {
        $relatedRecords = $companyBlock->relatedRecords;
        $companyBlock->safeSave();
        if (array_key_exists('imageFiles', $relatedRecords)) {
            foreach ($relatedRecords['imageFiles'] as $imageFile) {
                if ($imageFile->forDelete) {
                    CompanyBlockImage::deleteAll(['file_uuid' => $imageFile->uuid]);
                    $this->fileRepository->delete($imageFile);
                } else {
                    if ($imageFile->isNewRecord) {
                        $this->fileRepository->save($imageFile);
                        $productImage = new CompanyBlockImage();
                        $productImage->file_uuid = $imageFile->uuid;
                        $productImage->block_id = $companyBlock->id;
                        $productImage->safeSave();
                    }
                }
            }
        }
    }

    public function findByName($username): Company
    {
        $ps = Company::findOne(['url' => $username]);

        if (!$ps) {
            \Yii::error(['username' => $username, 'referrer' => app('request')->referrer, 'userAgent' => app('request')->userAgent, 'ip' => app('request')->userIP]);
            throw new NotFoundHttpException(_t('site.error', 'PS not found'));
        }
        $user = $ps->user;
        if (!$user || $user->isDeleted()) {
            throw new NotFoundHttpException(_t('site.error', 'User not found'));
        }

        if ($ps->isDeleted()) {
            throw new NotFoundHttpException(_t('common.ps', 'The manufacturer is currently inactive.'));
        }

        return $ps;
    }

    public function getPublicByName(string $username, ?User $currentUser): Company
    {
        $company = $this->findByName($username);
        $user    = $company->user;

        if ($user->getIsSystem()) {
            throw new NotFoundHttpException(_t('site.error', 'User not found'));
        }

        if ($company->getIsRejected() && (!$currentUser || !$currentUser->equals($company->user))) {
            throw new NotFoundHttpException(_t('common.ps', 'The manufacturer is currently inactive.'));
        }

        return $company;
    }
}