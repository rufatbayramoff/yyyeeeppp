<?php
namespace common\models;

use Yii;

/**
 * Edit this file
 * This is the model class for table "user_access".
 */
class UserAccess extends \common\models\base\UserAccess
{
    public $scenario = 'default';
    public function rules()
    {
        return [
            [['is_active'], 'required', 'on'=>'admin'],
            [['user_id', 'access_id'], 'required'],
            [['user_id', 'access_id'], 'integer'],
            [['created_at'], 'safe'],
            ['user_id', 'exist',
                'targetClass' => '\common\models\User',
                'targetAttribute' => ['user_id'=>'id'],
                'message' =>  'There is no user with such id.', 
                'on'=>'default'
            ],
            ['access_id', 'exist',
                'targetClass' => '\common\models\Access',
                'targetAttribute' => ['access_id'=>'id'],
                'message' =>  'There is no access with such id.',
                'on'=>'default'
            ],
            [
                ['user_id', 'access_id'], 'unique',
                'targetAttribute' => ['user_id', 'access_id'], 
                'message' => 'The combination of User ID and Access ID has already been taken.', 
                'on'=>'default'
            ]
        ];
    }
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['admin'] = ['!is_active'];
        return $scenarios;
    }
}