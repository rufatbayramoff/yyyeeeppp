<?php
namespace common\models;

use common\components\exceptions\AssertHelper;

/**
 * Edit this file
 * This is the model class for table "msg_member".
 *
 * @property MsgFolder $originalFolder
 * @property MsgFolder $folder
 */
class MsgMember extends \common\models\base\MsgMember
{

    public const TYPE_UNREAD = 'unread';
    public const TYPE_WITHOUT_ANSWER = 'without_answer';

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolder()
    {
        return $this->hasOne(MsgFolder::className(), ['id' => 'folder_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOriginalFolder()
    {
        return $this->hasOne(MsgFolder::className(), ['id' => 'original_folder']);
    }

    /**
     * Is topic deleted
     * @return int
     */
    public function getIsDeleted()
    {
        return $this->is_deleted == 1;
    }

    /**
     * Restore deleted topic to orginal folder
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function restoreToOriginalFolder()
    {
        AssertHelper::assert($this->getIsDeleted(), 'Cant restre not deleted folder');
        $this->folder_id = $this->original_folder;
        $this->is_deleted = 0;
        AssertHelper::assertSave($this);
    }
}