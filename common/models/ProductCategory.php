<?php

namespace common\models;

use common\components\ArrayHelper;
use common\models\query\ProductCategoryQuery;
use common\models\repositories\FileRepository;
use common\modules\product\interfaces\ProductInterface;
use common\modules\product\repositories\ProductCategoryRepository;
use frontend\components\image\ImageHtmlHelper;
use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;
use Yii;
use yii\helpers\Url;

/**
 * Class ProductCategory
 *
 * @package common\models
 * @property string $viewUrl
 *
 * @property \common\models\ProductCategory[] $productCategoriesOrderBy
 */
class ProductCategory extends \common\models\base\ProductCategory
{

    const ROOT_ID = 1;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['parent_id', 'position'], 'required'],
            ['parent_id', 'checkRecursive'],
        ]);
    }

    public function checkRecursive()
    {
        if ($this->isRecursive()) {
            $this->addError('parent_id', 'Recursive parent');
        }
    }

    public function allParents()
    {
        $parents = [];
        $parent  = $this->parent;
        while ($parent) {
            $parents[] = $parent;
            $parent    = $parent->parent;
        }
        return $parents;
    }

    public function hasPublishedModels()
    {
        $productCategoryRepository = Yii::createObject(ProductCategoryRepository::class);
        $ids                       = $productCategoryRepository->getModel3dCategoriesIds();
        return in_array($this->id, $ids);
    }

    /**
     * @param bool $onlyActive
     * @param string $type
     * @return ProductCategory[]
     */
    public function getSubcategories($onlyActive = true, $onlyVisible = false, $type = '')
    {
        $categories = $this->productCategories;
        $filtered   = [];
        foreach ($categories as $category) {
            if ($onlyActive && !$category->is_active) {
                continue;
            }
            if ($type === ProductInterface::TYPE_MODEL3D && !$category->hasPublishedModels()) {
                continue;
            }
            if ($onlyVisible && !$category->is_visible) {
                continue;
            }

            $filtered[] = $category;
        }
        return $filtered;
    }

    /**
     * @return \common\models\query\ProductCategoryQuery
     */
    public function getProductCategoriesOrderBy(): ProductCategoryQuery
    {
        return $this->getProductCategories()->active()->visible()->orderBy(['position' => SORT_ASC]);
    }

    public function allChilds($onlyActive = true)
    {
        $childs = [];
        foreach ($this->getSubcategories($onlyActive) as $productCategory) {
            $childs[$productCategory->id] = $productCategory;
            $childs                       = array_merge($childs, $productCategory->allChilds($onlyActive));
        }
        return $childs;
    }

    public static function defaultCategory()
    {
        $categoryQuery = self::find()->where(['is_active' => 1, 'parent_id' => self::ROOT_ID])->orderBy('position asc');
        return $categoryQuery->one();
    }

    /**
     * @param ProductCategory $category
     * @param array[] $list
     * @param int $level
     */
    public static function addCategoryToSelect($category, &$list, $level = 0)
    {
        $title               = str_repeat('  ', $level) . $category->title;
        $list[$category->id] = $title;
        foreach ($category->getSubcategories() as $subCategory) {
            self::addCategoryToSelect($subCategory, $list, $level + 1);
        }
    }

    public static function getActiveSelectCategories($includeRoot = false)
    {
        $categories = self::find()->where('is_active=1' . ($includeRoot ? '' : ' and parent_id=1'))->orderBy('position asc')->all();
        $list       = [];
        foreach ($categories as $category) {
            self::addCategoryToSelect($category, $list);
        }
        return $list;
    }

    /**
     * @param ProductCategory $category
     * @return array|ProductCategory[]|\yii\db\ActiveRecord[]
     */
    public static function getChilds(ProductCategory $category = null)
    {
        $categoryId = $category ? $category->id : self::ROOT_ID;
        $childs     = self::find()->where(['parent_id' => $categoryId])->active()->orderBy('position asc')->all();
        return $childs;
    }

    public static function getActiveCategoriesAsTree()
    {
        $categories = self::find()->active()->orderBy('position asc')->asArray()->all();
        $categories = ArrayHelper::index($categories, 'id');
        return $categories;
    }

    /**
     * @return string
     */
    public function getViewUrl(): string
    {
        return ProductUrlHelper::productsCatalog($this->code);
    }

    public function setAsVisible()
    {
        if ($this->parent) {
            $this->parent->setAsVisible();
        }
        if (!$this->is_visible) {
            $this->is_visible = 1;
            $this->save();
        }
    }

    public function isRecursive()
    {
        $oldStackId = [$this->id];
        $current    = $this;
        while ($current->parent && $current->parent->parent) {
            $current = $current->parent;
            if (in_array($current->id, $oldStackId)) {
                return true;
            }
        }
        return false;
    }

    public function getFullTitle($lastIsBold = false)
    {
        $title      = ($lastIsBold ? '<b>' : '')
            . ($lastIsBold ? htmlspecialchars($this->title) : $this->title)
            . ($lastIsBold ? '</b>' : '');
        $current    = $this;
        $oldStackId = [$this->id];
        while ($current->parent && $current->parent->parent) {
            $title   = ($lastIsBold ? htmlspecialchars($current->parent->title) : $current->parent->title) . ' \\ ' . $title;
            $current = $current->parent;
            if (in_array($current->id, $oldStackId)) {
                return ' Recursive: ' . $title;
            }
            $oldStackId[$current->id] = $current->id;
        }
        return $title;
    }

    public function addImage(FileAdmin $image)
    {
        if ($this->file) {
            $fileForDelete   = $this->file;
            $this->file_uuid = null;
            $this->safeSave();
            $fileRepository = Yii::createObject(FileRepository::class);
            $fileRepository->delete($fileForDelete);
        }
        $image->setPublicMode(true);
        $image->setFixedPath('product_category/' . $this->code . '/images');
        $image->setOwner(self::class, 'file_id');
        ImageHtmlHelper::stripExifInfo($image);
        $fileRepository = \Yii::createObject(FileRepository::class);
        $fileRepository->save($image);
        $this->file_uuid = $image->uuid;
    }
}