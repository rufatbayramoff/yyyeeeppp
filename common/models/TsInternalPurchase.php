<?php

namespace common\models;

/**
 * Class TsInternalPurchase
 * @package common\models
 * @property PaymentInvoice $paymentInvoice
 */
class TsInternalPurchase extends \common\models\base\TsInternalPurchase
{
    public const TYPE_CERTIFICATION = 'certification';
    public const TYPE_DEPOSIT       = 'deposit';
    public const STATUS_NEW         = 'new';
    public const STATUS_PAYED       = 'payed';
    public const STATUS_CANCELED    = 'canceled';

    public function allowCancel()
    {
        return true;
    }

    public function isNotPayed()
    {
        return $this->status === self::STATUS_NEW;
    }


    public function getCompanyService():?CompanyService
    {
        if (!$this->tsInternalPurchaseCertification) {
            return null;
        }
        return $this->tsInternalPurchaseCertification->companyService;
    }


    public function getPrice()
    {
        if (!$this->primaryInvoice) {
            return null;
        }
        return $this->primaryInvoice->getAmountTotal()->getAmount();
    }

    public function isDeposit(): bool
    {
        return $this->type === self::TYPE_DEPOSIT;
    }
}