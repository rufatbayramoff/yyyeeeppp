<?php

namespace common\models;

use common\components\DateHelper;

/**
 * Class PaymentPayPageLog
 *
 * @package common\models
 * @property \common\models\PaymentPayPageLogStatus[] $statuses
 * @property \common\models\PaymentPayPageLogProcess $processLast
 * @property \common\models\PaymentPayPageLogProcess[] $processes
 */
class PaymentPayPageLog extends \common\models\base\PaymentPayPageLog
{
    public const STATUS_OPEN_PAY_PAGE    = 'open_pay_page';
    public const STATUS_PRESS_BUTTON     = 'press_button';
    public const STATUS_PROCESSED        = 'processed';
    public const STATUS_SERVER_PROCESS   = 'server_process';
    public const STATUS_DONE             = 'done';
    public const STATUS_FAILED           = 'failed';
    public const STATUS_AUTHORIZE_FAILED = 'authorize_failed';

    public const STATUS_TEXTS = [
        self::STATUS_PRESS_BUTTON     => 'Button pressed',
        self::STATUS_PROCESSED        => 'Braintree processed',
        self::STATUS_SERVER_PROCESS   => 'Server process',
        self::STATUS_DONE             => 'Done',
        self::STATUS_AUTHORIZE_FAILED => 'Authorize failed'
    ];

    protected $lastStatusValue;

    protected function getSortedStatuses(): array
    {
        $statuses = $this->statuses;
        usort($statuses, function ($statusA, $statusB) {
            return $statusA->status_date > $statusB->status_date;
        });
        return $statuses;
    }

    public function getLastVendor(): string
    {
        $statuses = $this->getSortedStatuses();
        $status   = end($statuses);
        return $status ? $status->vendor : '';
    }

    public function getLastStatus(): string
    {
        $lastStatus = PaymentPayPageLog::STATUS_OPEN_PAY_PAGE;
        foreach ($this->statuses as $payStatus) {
            if ($lastStatus == PaymentPayPageLog::STATUS_OPEN_PAY_PAGE) {
                $lastStatus = $payStatus->status;
            }
            if (($payStatus->status === PaymentPayPageLog::STATUS_AUTHORIZE_FAILED) && ($payStatus->status !== PaymentPayPageLog::STATUS_DONE)) {
                $lastStatus = PaymentPayPageLog::STATUS_AUTHORIZE_FAILED;
            }
            if ($payStatus->status === PaymentPayPageLog::STATUS_DONE) {
                $lastStatus = PaymentPayPageLog::STATUS_DONE;
            }
            if (($payStatus->status === PaymentPayPageLog::STATUS_FAILED) && ($payStatus->status !== PaymentPayPageLog::STATUS_DONE)) {
                $lastStatus = PaymentPayPageLog::STATUS_FAILED;
            }
            if (($payStatus->status === PaymentPayPageLog::STATUS_PROCESSED) && ($lastStatus != PaymentPayPageLog::STATUS_FAILED) && ($lastStatus != PaymentPayPageLog::STATUS_AUTHORIZE_FAILED) && ($lastStatus !== PaymentPayPageLog::STATUS_DONE)) {
                $lastStatus = PaymentPayPageLog::STATUS_PROCESSED;
            }
            if ($payStatus->status === PaymentPayPageLog::STATUS_SERVER_PROCESS && ($lastStatus != PaymentPayPageLog::STATUS_FAILED) && ($lastStatus != PaymentPayPageLog::STATUS_AUTHORIZE_FAILED) && ($lastStatus !== PaymentPayPageLog::STATUS_DONE)) {
                $lastStatus = PaymentPayPageLog::STATUS_SERVER_PROCESS;
            }
        }
        return $lastStatus;
    }

    public function getStatusHistory($delimiter = "\n"): string
    {
        $statuses     = $this->getSortedStatuses();
        $statusString = '';
        foreach ($statuses as $status) {
            $statusString .= $status->status_date . ' ' . $status->vendor . ': ' . $status->status . $delimiter;
        }
        return $statusString;
    }


    public function is3dSecureFailed()
    {
        if ($this->processLast && is_array($this->processLast->process_payload) &&
            (array_key_exists('type', $this->processLast->process_payload) && ($this->processLast->process_payload['type'] == 'CreditCard')) &&
            (array_key_exists('liabilityShifted', $this->processLast->process_payload) && (!$this->processLast->process_payload['liabilityShifted']))
        ) {
            return true;
        }
        return false;
    }

    public static function register($uuid, $ip, $user_agent, $invoiceUuid)
    {
        $log = PaymentPayPageLog::find()->where(['uuid' => $uuid])->one();
        if (!$log) {
            $log               = new PaymentPayPageLog();
            $log->uuid         = $uuid;
            $log->created_at   = DateHelper::now();
            $log->ip           = $ip;
            $log->user_agent   = $user_agent;
            $log->invoice_uuid = $invoiceUuid;
            $log->safeSave();
        }
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getStatuses()
    {
        return $this->hasMany(\common\models\PaymentPayPageLogStatus::class, ['uuid' => 'uuid']);
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getProcessLast()
    {
        return $this->hasOne(\common\models\PaymentPayPageLogProcess::class, ['uuid' => 'uuid'])->orderBy('payment_pay_page_log_process.date');
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getProcesses()
    {
        return $this->hasMany(\common\models\PaymentPayPageLogProcess::class, ['uuid' => 'uuid']);
    }
}