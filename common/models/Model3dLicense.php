<?php
namespace common\models;

use Yii;

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class Model3dLicense extends \common\models\base\Model3dLicense
{
   const COMMERCIAL_LICENSE_ID = 1;
   
   const FREE_LICENSE_ID = 2;
}