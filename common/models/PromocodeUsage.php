<?php

namespace common\models;

use lib\money\Money;

/**
 * Class PromocodeUsage
 * @package common\models
 */
class PromocodeUsage extends \common\models\base\PromocodeUsage
{
    /**
     * @return Money
     */
    public function getAmountTotal(): Money
    {
        return Money::create($this->amount, $this->amount_currency);
    }
}