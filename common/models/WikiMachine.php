<?php

namespace common\models;

use common\interfaces\FileBaseInterface;

/**
 * Class WikiMachine
 *
 * @package common\models
 * @property File $mainPhotoFile
 * @property File[] $photoFiles
 * @property File[] $files
 */
class WikiMachine extends \common\models\base\WikiMachine
{
    /** @var File[] */
    public $deletedMainPhotoFiles;

    /** @var bool */
    public $deleteMainPhotoFile = false;

    use \common\components\IntlArTrait;

    public function load($data, $formName = null)
    {
        $formName = $formName === null ? $this->formName() : $formName;
        if (array_key_exists($formName, $data)) {
            $formData = $data[$formName];
            if (array_key_exists('photoFiles', $formData)) {
                foreach ($formData['photoFiles'] as $key => $value) {
                    if ($value === 'deleted') {
                        if ($forDeletePhoto = $this->getPhotoByUuid($key)) {
                            $forDeletePhoto->forDelete = true;
                        }
                    }
                }
            }
            if (array_key_exists('files', $formData)) {
                foreach ($formData['files'] as $key => $value) {
                    if ($value === 'deleted') {
                        if ($forDeleteFile = $this->getFileByUuid($key)) {
                            $forDeleteFile->forDelete = true;
                        }
                    }
                }
            }

            if (array_key_exists('deleteMainPhotoFile', $formData) && $formData['deleteMainPhotoFile']) {
                $this->deleteMainPhotoFile = true;
            }
        }

        return parent::load($data, $formName);
    }

    public function setMainPhotoFile(FileBaseInterface $file)
    {
        if ($this->mainPhotoFile) {
            $this->deletedMainPhotoFiles[] = $this->mainPhotoFile;
        }
        $file->setOwner(WikiMaterial::class, 'file_uuid');
        $file->setPublicMode(true);
        $file->setFixedPath('wikiMachines/' . $this->code . '/photo');
        $this->populateRelation('mainPhotoFile', $file);
        $this->main_photo_file_uuid = $file->uuid;
    }

    /**
     * @param File[] $files
     */
    public function addPhotoFiles($files)
    {
        $currentFiles = $this->photoFiles;
        foreach ($files as $file) {
            $file->setPublicMode(true);
            $file->setOwner(WikiMaterialPhoto::class, 'photo_file_uuid');
            $file->setFixedPath('wikiMachines/' . $this->code . '/photos');
            $wikiMachinePhoto = new WikiMachinePhoto();
            $file->populateRelation('wikiMachinePhoto', $wikiMachinePhoto);
            $currentFiles[] = $file;
        }
        $this->setPhotoFiles($currentFiles);
    }

    /**
     * @param File[] $files
     */
    public function addFiles($files)
    {
        $currentFiles = $this->files;
        foreach ($files as $file) {
            $file->setPublicMode(true);
            $file->setOwner(WikiMaterialFile::class, 'file_uuid');
            $file->setFixedPath('wikiMachines/' . $this->code . '/files');
            $wikiMachineFile = new WikiMachineFile();
            $file->populateRelation('wikiMachineFile', $wikiMachineFile);
            $currentFiles[] = $file;
        }
        $this->setFiles($currentFiles);
    }

    /**
     * @param File[] $files
     */
    public function setPhotoFiles($files)
    {
        $this->populateRelation('photoFiles', $files);
    }

    public function getPhotoFiles()
    {
        return $this->hasMany(FileAdmin::class, ['uuid' => 'photo_file_uuid'])->via('wikiMachinePhotos');
    }

    public function getPhotoByUuid($uuid)
    {
        $photos = $this->photoFiles;
        foreach ($photos as $photo) {
            if ($photo->uuid === $uuid) {
                return $photo;
            }
        }
        return null;
    }

    /**
     * @param File[] $files
     */
    public function setFiles($files)
    {
        $this->populateRelation('files', $files);
    }

    public function getFiles()
    {
        return $this->hasMany(FileAdmin::class, ['uuid' => 'file_uuid'])->via('wikiMachineFiles');
    }

    public function getFileByUuid($uuid)
    {
        $files = $this->files;
        foreach ($files as $file) {
            if ($file->uuid === $uuid) {
                return $file;
            }
        }
        return null;
    }


}