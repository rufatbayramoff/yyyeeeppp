<?php

namespace common\models;

use Yii;

/**
 * Delivery types which can be used by ps_printer, and in customer Deliver to page
 * table "delivery_type".
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class DeliveryType extends \common\models\base\DeliveryType
{ 
    const PICKUP = 'pickup';
    const STANDARD = 'standard';
    const EXPRESS = 'express';
    const INTERNATIONAL = 'intl';

    const DELIVERY_ICONS = [
        self::PICKUP => 'tsi-man',
        self::STANDARD => 'tsi-truck',
        self::INTERNATIONAL => 'tsi-aircraft'
    ];

    const CARRIER_MYSELF = 'myself';
    const CARRIER_TS = 'treatstock';

    const EMPTY_ID = 0;
    const PICKUP_ID = 1;
    const STANDART_ID = 2;
    const INTERNATIONAL_STANDART_ID = 5;


    /**
     * Return min value for free delivery price in USD
     * @return float
     * @throws \Exception
     */
    public function getMinFreeDeliveryPriceInUSD()
    {
        if($this->code ==self::STANDARD)
        {
            return (float)app('setting')->get('store.min_free_price_standard');
        }
        else if($this->code == self::INTERNATIONAL)
        {
            return (float) app('setting')->get('store.min_free_price_intl');
        }
        throw new \Exception("DeliveryType {$this->code } not supported free delivery");
    }

    /**
     * Is this delivery type use EasyPost
     * @return bool
     */
    public function getIsPostDelivery()
    {
        return $this->code != DeliveryType::PICKUP;
    }

    /**
     * TODO: FIX IT to deliveryTypeIntl
     * Hack. Agreed with Arsen Kurin
     */
    public function getTitle()
    {
        if ($this->code === self::PICKUP) {
            return _t('site.store', 'Customer Pickup');
        }
        if ($this->code === self::STANDARD) {
            return _t('site.store', 'Domestic Shipping');
        }
        if ($this->code === self::INTERNATIONAL) {
            return _t('site.store', 'International Shipping');
        }
    }
    
}