<?php
namespace common\models;

use Yii;

/**
 * Edit this file
 * This is the model class for table "email_template".
 */
class EmailTemplate extends \common\models\base\EmailTemplate
{
    /**
     * Check that template have content for
     * @param $contentType
     * @return bool
     */
    public function haveContentForType($contentType)
    {
        $contentAttr = 'template_'.$contentType;
        return !empty($this->$contentAttr);
    }
}