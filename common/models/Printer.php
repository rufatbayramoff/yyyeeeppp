<?php
namespace common\models;

use common\components\ArrayHelper;
use common\models\File;
use common\modules\catalogPs\repositories\PrintedFileRepository;
use common\modules\equipments\EquipmentsModule;
use common\modules\equipments\helpers\PrinterImageHelper;
use common\modules\equipments\helpers\SimpleFile;
use common\traits\db\ActiveScopeTrait;
use frontend\components\image\ImageHtmlHelper;
use Yii;
use common\components\ActiveQuery;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 *
 * @property PrinterMaterial[] $materials       Active materials of printer
 * @property PrinterToProperty[] $properties    Active printer properties
 * @property CompanyService $companyService
 */
class Printer extends \common\models\base\Printer
{
    /**
     * @var UploadedFile
     */
    public $mainImage;

    /**
     * @var UploadedFile[]
     */
    public $moreImages;

    /**
     * @var UploadedFile[]
     */
    public $printerFiles;

    public static function findBySlug($slug)
    {
        $id = (int)$slug;
        $item = self::findOne(['id'=>$id]);
        return $item;
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['mainImage'], 'image', 'skipOnEmpty' => true],
            [['moreImages'], 'image', 'skipOnEmpty' => true, 'maxFiles' => 15],
            [['printerFiles'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 15],
        ]);
    }

    /**
     * Relation for materials
     * @return \common\components\BaseActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(PrinterMaterial::class, ['id' => 'material_id'])
            ->where(['is_active' => 1])
            ->viaTable('printer_to_material', ['printer_id' => 'id'], function(ActiveQuery $query){
                $query->where(['is_active' => 1]);
            });
    }

    /**
     * Relation for printer properties
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(PrinterToProperty::class, ['printer_id' => 'id'])
            ->where(['is_active' => 1]);
    }

    public function getSlug()
    {
        return $this->id . '-'.Inflector::slug($this->title);
    }

    public function getPublicPageUrl()
    {
        return EquipmentsModule::URL_PREFIX . '/item/'.  $this->getSlug();
    }

    /**
     * get additional images for printer
     *
     * @return SimpleFile[]
     */
    public function getImages()
    {
        /**
         * @var $imgHelper PrinterImageHelper
         */
        $imgHelper = Yii::createObject(PrinterImageHelper::class);
        $images = (array)$imgHelper->findImagesByTitle($this->title);

        if(count($images) > 0 && strpos($images[0], "_0.")!==false){
            array_shift($images); // remove first, it's log
        }
        $images =  SimpleFile::createByUrls($images);
        $files = [];
        if(!empty($this->images_json)) {
            $files =  SimpleFile::createByUrls($this->images_json);
        }
        return array_merge($files, $images);
    }

    /**
     * @return \common\models\File[]|null
     */
    public function getPrintedImages(): ?array
    {
        return PrintedFileRepository::getByPrinterMachineId($this);
    }

    /**
     * get additional files for printer
     *
     * @return SimpleFile[]
     */
    public function getPrinterFiles()
    {
        $files = [];
        if(!empty($this->files_json)) {
            $files =  SimpleFile::createByUrls($this->files_json);
        }
        return $files;
    }

    /**
     * get main image url
     *
     * @return string
     */
    public function getMainImageUrl()
    {
        if(!empty($this->image_file_id)){
            $file = File::findOne($this->image_file_id);
            return $file->getFileUrl();
        }
        // lets try to search in /static/upload catalog
        /**
         * @var $imgHelper PrinterImageHelper
         */
        $imgHelper = Yii::createObject(PrinterImageHelper::class);
        $img = $imgHelper->findLogoImageByTitle($this->title);
        if ($img) {
            return $img;
        }
        $addImages = $this->getImages();
        if ($addImages) {
            $firstAddImage = reset($addImages);
            $imgThumb = ImageHtmlHelper::getThumbUrl($firstAddImage->getStaticUrl(), 160, 90);
            return $imgThumb;
        }

        return '/static/images/defaultPrinter.png';
    }

    /**
     * Return max positional accurancy for this printer
     * @return float
     */
    public function getMaxPositionalAccuracy() : float
    {
        return $this->positional_accuracy ?? (float) \Yii::$app->setting->get("printer.default_positional_accuracy");
    }

    /**
     * @return \common\models\query\PsQuery
     */
    public function getCompanyService()
    {
        return $this->hasOne(\common\models\CompanyService::class, ['ps_printer_id' => 'id']);
    }
}