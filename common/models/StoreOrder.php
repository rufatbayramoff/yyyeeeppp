<?php namespace common\models;

use common\components\ActiveQuery;
use common\components\DateHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\DeprecatedException;
use common\components\exceptions\InvalidModelException;
use common\components\order\history\OrderHistoryService;
use common\components\order\TestOrderFactory;
use common\models\query\PaymentDetailQuery;
use frontend\models\user\UserFacade;
use lib\delivery\delivery\models\DeliveryRate;
use lib\money\Currency;
use yii\base\UserException;

/**
 * Store order
 *
 * Not paid (Не оплачен) - заказ не оплачен и деньги не зарезервированы.
 *  Reserved (Зарезервированы) - деньги зарезервированы под заказ на счете Buyer.
 *  Paid by Buyer (Оплачен покупателем) - деньги перечислены от покупателя на счет сайта TS
 *  Partially paid (Оплачен частично) - деньги перечислены частично (не полностью), либо от покупателя на счет сайта,
 *      либо от сайта получателю: Author и/или PS.
 *  Paid (Оплачен) - деньги перечислены от сайта получателю: Author, PS
 *  Voided - отменено резервирование д.с. на счете Buyer, если PS или Buyer отказались от заказа
 *  Refund - деньги возвращены Buyer после отмены
 *
 *
 *  New - новый заказ, созданный покупателем Buyer
 *  Accepted - заказ, подтвержденный PS
 *  In production - заказ, принятый PS (нажата кнопка Print)
 *  Printed - заказ, распечатанный  PS
 *  Sent - заказа, переданный доставщику
 *  Delivered - заказ, доставленный покупателю (shipment от доставщика, либо подтверждение от Buyer,
 *              либо подтверждение от PS при Pickup, либо при покупке модели без покупки услуги печати)
 *  Closed - заказ, по которому произведена оплата Author и  PS
 *  Cancelled - заказ отменен Buyer (до статуса In production), заказ отменен PS
 *  Returned - заказ возвращен от Buyer (после Delivered,  Closed)
 *
 * @property StoreOrderItem $orderItem
 * @property StoreOrderAttemp $currentAttemp
 * @property MsgTopic|null $messageTopic
 * @property MsgTopic|null $activeMessageTopic
 * @property StoreOrderAttemp[] $attemps
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 *
 * @property PaymentTransaction $paymentTransactions
 * @property Model3dReplica|null $firstReplicaItem
 */
class StoreOrder extends \common\models\base\StoreOrder
{
    /**
     * order states
     */
    const STATE_PROCESSING = 'processing';
    const STATE_CLOSED     = 'closed';
    const STATE_CANCELED   = 'canceled';
    const STATE_REMOVED    = 'removed';
    const STATE_COMPLETED  = 'completed';

    /**
     * Claim order allow period. Can open dispute after order received in period.
     */
    const DISPUTE_ALLOWED_PERIOD = 60 * 60 * 24 * 30;

    /**
     * Source
     */
    const SOURCE_THINGIVERSE = 'thingiverse';

    /** @var DeliveryRate */
    public $deliveryRate = null;

    /**
     * @return ActiveQuery
     */
    public function getAttemps()
    {
        return $this->getStoreOrderAttemps();
    }

    /**
     * checkout order. change status and billed date
     * this means that payment was made and we update payment status and payment_id
     * we need to send email/sms to print service here
     *
     * @param PaymentDetailOperation $paymentDetailOperation
     * @throws UserException
     */
    public function savePayment(PaymentDetailOperation $paymentDetailOperation)
    {
        $history = new OrderHistoryService();
        $history->logPaymentStatus($this, 'PaymentDetailOperation: ' . $paymentDetailOperation->uuid, $paymentDetailOperation->payment->status);
        $this->updated_at = DateHelper::now();
        $this->billed_at  = DateHelper::now();
        $this->safeSave();

        if ($this->user->id !== $this->currentAttemp->ps->user->id) {
            $emailer = new \common\components\Emailer();
            if ($this->getIsTestOrder()) {
                $emailer->sendPsNewTestOrder($this->currentAttemp);
            } else {
                if (!$this->isForPreorder()) {
                    $emailer->sendPsNewOrder($this->currentAttemp);
                }
            }

            $emailer->sendClientNewOrder($this);
        }
    }

    /**
     * validate order updates, check current order status
     *
     * @return boolean
     */
    public function validateOrderUpdate()
    {
        if ($this->currentAttemp && !$this->currentAttemp->isNew()) {
            return false;
        }
        return true;
    }

    /**
     * can order be canceled
     *
     * @return boolean
     */
    public function canUserCancel()
    {
        if ($this->isResolved()) {
            return false;
        }

        if (!$this->currentAttemp) {
            return false;
        }

        // user can cancel order only if it's new and payment_status is new
        if ($this->currentAttemp->isNew() && !$this->currentAttemp->is_offer) {
            return true;
        }
        // user can change status if payment_status is new - user didn't 
        if ($this->getPaymentStatus() == PaymentInvoice::STATUS_NEW) {
            return true;
        }

        return false;
    }

    public function allowOpenDispute()
    {
        $disallowStatuses = [
            StoreOrderAttemp::STATUS_RECEIVED,
            StoreOrderAttemp::STATUS_RETURNED,
            StoreOrderAttemp::STATUS_CANCELED,
        ];
        if (!$this?->currentAttemp?->status) {
            return true;
        }
        return !in_array($this->currentAttemp->status, $disallowStatuses);
    }

    public function canUserRemove()
    {
        return $this->order_state == self::STATE_CLOSED || $this->order_state == self::STATE_CANCELED;
    }

    /**
     * Can user pay for order
     *
     * @return boolean
     */
    public function canPay()
    {
        return $this->getPaymentStatus() == PaymentInvoice::STATUS_NEW && (!$this->currentAttemp || $this->currentAttemp->isNew());
    }

    /**
     *
     * @return boolean
     */
    public function canRefresh()
    {
        if ($this->order_state == self::STATE_CLOSED || $this->order_state == self::STATE_REMOVED || $this->order_state == self::STATE_COMPLETED || $this->canPay()) {
            return false;
        }
        return true;
    }


    /**
     * @return array|base\StoreOrder|null|\yii\db\ActiveRecord
     */
    public function getLatestOrderTransaction()
    {
        return $this->getPaymentTransactions()->orderBy('updated_at')->one();
    }

    public function isPickup()
    {
        return $this->deliveryType->code == DeliveryType::PICKUP;
    }

    /**
     * change payment status
     *
     * @param int|StoreOrder $orderId
     * @param int $status
     * @param null $comment - default is Change payment status
     * @return bool|StoreOrder
     */
    public static function changePaymentStatus($orderId, $status, $comment = null)
    {
        throw new DeprecatedException('Depricated method');
    }

    /**
     * validation before checkout
     *
     * @param \common\models\StoreOrder $storeOrder
     * @throws UserException
     */
    public static function validateBeforeCheckout(StoreOrder $storeOrder)
    {
        if (!$storeOrder->getPrimaryInvoice()) {
            throw new BusinessException(_t('site.store', "Order's doesn`t contain invoice"));
        }
        if ($storeOrder->isCancelled()) {
            throw new BusinessException(_t('site.store', "Order was canceled, please proceed to create a new one."));
        }
        if ($storeOrder->isPayed()) {
            throw new BusinessException(_t('site.store', "Your order is paid."));
        }
        if (!$storeOrder->getPrimaryInvoice() && $storeOrder->getPrimaryInvoice()->allowPay()) {
            throw new BusinessException(_t('site.store', "Order's payment status doesn't allow to checkout."));
        }
        if (!$storeOrder->currentAttemp || !$storeOrder->currentAttemp->isNew()) {
            throw new BusinessException(_t('site.store', "Order's status doesn't allow to checkout."));
        }

        // refs 6225 (1)
        if (
            $storeOrder->primaryPaymentInvoice->baseByPrintOrder()
            && (
                !$storeOrder->currentAttemp
                || !$storeOrder->currentAttemp->isNew()
            )
        ) {
            throw new BusinessException(_t('site.store', 'Print offer is canceled.') . $viewOrderUrl);
        }
    }


    public function isAvailableForUser()
    {
        $user        = UserFacade::getCurrentUser();
        $userSession = UserFacade::getUserSession();
        if (UserFacade::isObjectOwner($this, $user, $userSession)) {
            return true;
        }
        return false;
    }

    /**
     * Relation for order item
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItem()
    {
        return $this->hasOne(StoreOrderItem::className(), ['order_id' => 'id']);
    }

    /**
     * @return Model3dReplica|null
     */
    public function getFirstReplicaItem()
    {
        return $this->getFirstItem() ? $this->getFirstItem()->model3dReplica : null;
    }

    /**
     * @return StoreOrderItem|null
     */
    public function getFirstItem()
    {
        $items = $this->storeOrderItems;
        if ($items) {
            return reset($items);
        } else {
            return null;
        }
    }

    /**
     * Return last delivery
     *
     * @return StoreOrderDelivery
     */
    public function getLastDelivery()
    {
        $deliveries   = $this->storeOrderDelivery;
        $lastDelivery = end($deliveries);
        return $lastDelivery ?: null;
    }

    /**
     * get order delivery details
     *
     * @return array|bool
     */
    public function getOrderDeliveryDetails()
    {
        $deliveryTypeDetails   = $this->storeOrderDelivery;
        $deliveryCurrentAttemp = $this->currentAttemp->delivery ?? null;

        if ($deliveryTypeDetails || $deliveryCurrentAttemp) {
            $details = [];

            if ($deliveryTypeDetails && !empty($deliveryTypeDetails->ps_delivery_details)) {
                $details = $deliveryTypeDetails->ps_delivery_details;
            }

            if ($this->currentAttemp && $this->currentAttemp->delivery) {
                $details['tracking_number']  = isset($this->currentAttemp) ? $this->currentAttemp->delivery->tracking_number : null;
                $details['tracking_shipper'] = isset($this->currentAttemp) ? $this->currentAttemp->delivery->tracking_shipper : null;
            }

            if (!isset($details['carrier'])) {
                $details['carrier'] = DeliveryType::CARRIER_TS; // default? (
                if ($this->currentAttemp->order->isOrderProduct()) {
                    $details['carrier'] = DeliveryType::CARRIER_MYSELF; // default? (
                }
            }

            return $details;
        }
        return false;
    }


    public function isDeliveryByTreatstock()
    {
        $details = $this->getOrderDeliveryDetails();
        if ($details && $details['carrier'] === DeliveryType::CARRIER_MYSELF) {
            return false;
        }
        return true;
    }

    /**
     * Status from primary invoice
     */
    public function getPaymentStatus()
    {
        if ($this->primaryPaymentInvoice->status) {
            return $this->primaryPaymentInvoice->status;
        }
        return PaymentInvoice::STATUS_NEW;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        $title = $this->id;

        if ($this->getFirstItem() && $this->getFirstItem()->hasModel()) {
            $title .= ' ' . $this->getFirstItem()->model3dReplica->title;
        }
        return $title;
    }

    /**
     * Is order payed
     *
     * @return bool
     */
    public function isPayed(): bool
    {
        return $this->primaryPaymentInvoice ? $this->primaryPaymentInvoice->isPayed() : false;
    }

    /**
     * @return bool
     */
    public function canNotBePaid()
    {
        return $this->getPaymentStatus() !== PaymentInvoice::STATUS_NEW || ($this->currentAttemp && !$this->currentAttemp->isNew());
    }

    /**
     * Is order for testing purpose
     *
     * @return bool
     */
    public function getIsTestOrder()
    {
        return $this->user_id == TestOrderFactory::getTestOrderUserId();
    }


    public function isThingiverseOrder()
    {
        return $this->thingiverseOrder ? true : false;
    }

    public function inChangePrinter()
    {
        return ($this->order_state === StoreOrder::STATE_PROCESSING) && ($this->current_attemp_id === null);
    }

    /**
     * @return bool
     */
    public function isResolved(): bool
    {
        return \in_array($this->order_state, [self::STATE_COMPLETED, self::STATE_CLOSED, self::STATE_REMOVED], false);
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->order_state === self::STATE_COMPLETED;
    }

    /**
     * @return bool
     */
    public function isCancelled(): bool
    {
        return \in_array($this->order_state, [self::STATE_CLOSED, self::STATE_REMOVED, self::STATE_CANCELED], false);
    }

    /**
     * Is order on anonim user
     *
     * @return bool
     */
    public function isAnonim()
    {
        return $this->user_id == User::USER_ANONIM;
    }

    /**
     * Return possible user for anonim order
     *
     * @return User
     */
    public function getPossibleUser()
    {
        return $this->isAnonim() ? User::findOne(['email' => $this->shipAddress->email]) : $this->user;
    }

    public function canDispute()
    {
        $isPaid     = $this->isPayed();
        $isResolved = $this->isResolved();
        return $isPaid && !$isResolved;
    }

    /**
     * @return bool
     */
    public function isForPreorder(): bool
    {
        return (bool)$this->preorder_id;
    }

    /**
     * @return bool
     */
    public function isFor3dPrinter(): bool
    {
        return !$this->preorder_id && !$this->isOrderProduct() && $this->hasModel();
    }

    /**
     * Is order has delivery
     *
     * @return bool
     */
    public function hasDelivery(): bool
    {
        return (bool)$this->delivery_type_id;
    }

    /**
     * @return bool
     */
    public function hasDeliveryTrackingData(): bool
    {
        return ($this->currentAttemp && $this->currentAttemp->delivery);
    }

    /**
     * Is order has delivery
     *
     * @return bool
     */
    public function hasBillAddress(): bool
    {
        return (bool)$this->bill_address_id;
    }

    /**
     * @return PaymentInvoice
     * @throws InvalidModelException
     */
    public function getPrimaryInvoice(): PaymentInvoice
    {
        if (!$this->primaryPaymentInvoice) {
            throw new InvalidModelException($this, 'Primary payment invoice is null');
        }
        return $this->primaryPaymentInvoice;
    }

    /**
     * @return PaymentInvoice[]
     */
    public function getPaidInvoices(): array
    {
        $query = $this
            ->getPaymentInvoices()
            ->where(['status' => [PaymentInvoice::STATUS_AUTHORIZED, PaymentInvoice::STATUS_PAID, PaymentInvoice::STATUS_PART_PAID, PaymentInvoice::STATUS_REFUNDED]])
            ->orderBy(['created_at' => SORT_DESC]);
        return $query->all();
    }

    /*******************************/
    /** @see AffiliateRewardable */
    /*******************************/

    public function getTotalPrice()
    {
        $lastInvoice = $this->getPrimaryInvoice();
        return $lastInvoice->total_amount ?? 0;
    }

    public function getCurrency()
    {
        $lastInvoice = $this->getPrimaryInvoice();
        return $lastInvoice->currency ?? Currency::USD;
    }

    public function getObjectId()
    {
        return $this->id;
    }

    public function getUserRegisteredAt()
    {
        return $this->user->created_at;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function hasModel()
    {
        return $this->orderItem && $this->orderItem->hasModel();
    }

    public function hasCuttingPack(): bool
    {
        return $this->getCuttingPack() ? true : false;
    }

    public function getCuttingPack(): ?CuttingPack
    {
        return $this->getFirstItem() ? $this->getFirstItem()->cuttingPack : null;
    }

    public function getOrderType()
    {
        if (!$this->hasModel()) {
            return null;
        }
        $replicaItem = $this->getFirstReplicaItem();
        return $replicaItem->source;
        //


    }

    public function isInternalOrder()
    {
        $internalUsers = app('setting')->get('store.internalOrdersCustomerUserId');
        if (in_array($this->user_id, $internalUsers)) {
            return true;
        }
        if ($this->currentAttemp && in_array($this->currentAttemp->ps_id, $internalUsers)) {
            return true;
        }
        return false;
    }


    public function isOrderProduct()
    {
        $firstItem = $this->getFirstItem();
        return !empty($firstItem->product);
    }

    public function isModelUploaded()
    {
        $replicaItem = $this->getFirstReplicaItem();
        if (!$replicaItem) {
            return false;
        }
        if ($replicaItem->getAuthor() && $replicaItem->getAuthor()->id === $this->user_id) {
            return true;
        }
        return false;
    }

    /**
     * Is user make preorder to own cnc machine
     *
     * @return bool
     */
    public function isSelfOrder(): bool
    {
        return $this->currentAttemp ? $this->user->equals($this->currentAttemp->ps->user) : false;
    }


    public function getPayments()
    {
        return $this->hasMany(Payment::class, ['payment_invoice_uuid' => 'uuid'])
            ->via('paymentInvoices');
    }

    public function getPaymentDetailOperations()
    {
        return $this->hasMany(PaymentDetailOperation::class, ['payment_id' => 'id'])
            ->via('payments');
    }

    /**
     * @return PaymentDetailQuery
     */
    public function getPaymentDetails()
    {
        return $this->hasMany(PaymentDetail::class, ['payment_detail_operation_uuid' => 'uuid'])
            ->via('paymentDetailOperations');
    }

    public function getPaymentTransactionHistories()
    {
        return $this->hasMany(PaymentTransactionHistory::class, ['payment_detail_id' => 'id'])
            ->via('paymentDetails');
    }

    /**
     * @return ActiveQuery
     */
    public function getPaymentTransactions()
    {
        return $this->hasMany(\common\models\PaymentTransaction::class, ['id' => 'transaction_id'])
            ->via('paymentTransactionHistories');
    }

    /**
     * Clears the likes / dislikes of the order
     *
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function clearOrderLike()
    {
        $likeOrder = UserLike::findOne([
            'object_type' => UserLike::TYPE_ORDER_ATTEMPT,
            'object_id'   => $this->current_attemp_id
        ]);

        if ($likeOrder) {
            $likeOrder->delete();
        }
    }

    /**
     * @return StoreOrderPosition[]
     */
    public function getPayedAdditionalServices(): array
    {
        $query = $this->getStoreOrderPositions()
            ->andWhere([
                'status' => StoreOrderPosition::STATUS_PAID
            ]);
        return $query->all();
    }

    public function getPayedNotCanceledAdditionalServices(): array
    {
        $additionalServices = $this->getPayedAdditionalServices();
        $notCanceled        = [];
        foreach ($additionalServices as $additionalService) {
            if ($additionalService->primaryPaymentInvoice && !$additionalService->primaryPaymentInvoice->isCanceled()) {
                $notCanceled[] = $additionalService;
            }
        }
        return $notCanceled;
    }


    public function getPaymentBankInvoices()
    {
        return $this->hasMany(\common\models\PaymentBankInvoice::class, ['payment_invoice_uuid' => 'uuid'])->via('primaryPaymentInvoice');
    }

    public function getOrderSource()
    {
        return $this->source;
    }

    public function getMessageTopic()
    {
        return $this->hasOne(\common\models\MsgTopic::class, ['bind_id' => 'id'])->andWhere(['bind_to' => MsgTopic::BIND_OBJECT_ORDER]);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isCustomer(User $user): bool
    {
        return $this->user_id === $user->id;
    }

    public function isInterception(): bool
    {
        return (bool)$this?->preorder?->is_interception;
    }

    /**
     * @return \yii\db\ActiveQuery|null
     */
    public function getActiveMessageTopic()
    {
        $attemp = $this->currentAttemp;
        if (!$attemp) {
            return null;
        }
        $userId = $attemp->ps->user_id;
        return $this->getMessageTopic()->innerJoinWith(['members'])->andWhere(['msg_member.user_id' => $userId])->andWhere(['<>', 'msg_member.folder_id', MsgFolder::FOLDER_ID_SUPPORT]);
    }

    /**
     * @return boolean
     */
    public function isExpressDelivery(): bool
    {
        $delivery = $this->storeOrderDelivery;
        if (!$delivery) {
            return false;
        }
        return (boolean)$delivery->is_express;
    }

    public function allowedReceiveMessage(): bool
    {
        if (!$this->currentAttemp || !$this->currentAttemp->company || !$this->currentAttemp->company->user) {
            return false;
        }
        $companyDisableNotify = $this->currentAttemp->interceptionCompany()->user->allowCompanyDialogAutoCreate();
        if ($companyDisableNotify === false) {
            return false;
        }
        $customerDisableNotify = $this->user->allowedReceiveMessage();
        return !($customerDisableNotify === false);
    }

    public function getManualSettedPs()
    {
        return $this->hasOne(Company::class, ['id' => 'manual_setted_ps_id'])->inverseOf('storeOrders');
    }
}
