<?php

namespace common\models;

use Yii;


/**
 *
 * Use fcode as source type
 *
 * Edit this file
 * This is the model class for table "geo_city".
 */
class GeoCity extends \common\models\base\GeoCity
{
    public function getFullTitle()
    {
        $t1 = $this->country->title??'';
        $t2 = $this->region->title??'';
        $t3 = $this->title??'';
        $title = $t1 . ', ' . $t2 . ', ' . $t3;
        return $title;
    }
}