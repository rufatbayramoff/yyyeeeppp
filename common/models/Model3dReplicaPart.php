<?php

namespace common\models;

use common\components\Model3dWeightCalculator;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\model3d\Model3dBasePart;

/**
 * Class Model3dReplicaPart
 *
 * @package common\models
 */
class Model3dReplicaPart extends \common\models\base\Model3dReplicaPart implements Model3dBasePartInterface
{
    use Model3dBasePart {
        beforeSaveMethod as baseBeforeSaveMethod;
    }

    const UID_PREFIX = 'RP:';

    public function setAttachedModel3d(Model3dBaseInterface $model3d): void
    {
        $this->populateRelation('model3d', $model3d);
        $this->populateRelation('model3dReplica', $model3d);
    }

    public function getAttachedModel3d(): Model3dBaseInterface
    {
        return $this->model3dReplica;
    }

    public function beforeSaveMethod($insert)
    {
        $this->baseBeforeSaveMethod($insert);

        if ($this->model3d && (($this->model3d->id !== $this->model3d_replica_id) || ($this->model3d_replica_id === null))) {
            $this->model3d_replica_id = $this->model3d->id;
        }
        return parent::beforeSaveMethod($insert);
    }

    public function getModel3d()
    {
        return $this->getModel3dReplica();
    }

    // Compatible with model3d
    public function getModel3d_id()
    {
        return $this->model3d_replica_id;
    }

    public function setModel3d_id($model3dId)
    {
        $this->model3d_replica_id = $model3dId;
    }

    public function setOriginalModel3dPart($model3dPart)
    {
        $this->populateRelation('originalModel3dPart', $model3dPart);
    }

    public function getOriginalModel3dPartObj()
    {
        return $this->originalModel3dPart;
    }

    public function getUid()
    {
        return self::UID_PREFIX . $this->id;
    }

    /**
     * @return int
     */
    public function getOriginalQty(): int
    {
        return $this->original_qty;
    }

    public function getModel3dPartCncParams(): ?Model3dPartCncParams
    {
        return $this->getOriginalModel3dPartObj()->model3dPartCncParams;
    }

    public function getModel3dPartPropertiesOriginal()
    {
        return $this->originalModel3dPart->model3dPartPropertiesOriginal;
    }

    public function getConvertedParts()
    {
        return [];
    }
}