<?php

namespace common\models;

use common\components\DateHelper;
use common\interfaces\FileBaseInterface;
use yii;

/**
 * Edit this file
 * This is the model class for table "file".
 */
class File extends \common\models\base\File implements FileBaseInterface
{
    use FileBase {
        rules as traitRules;
        scenarios as traitScenarios;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = array_merge(
            parent::rules(),
            $this->traitRules(),
            []
        );
        return $rules;
    }

    public function scenarios(): array
    {
        $rules = array_merge(
            parent::scenarios(),
            $this->traitScenarios(),
            []
        );
        return $rules;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        return parent::save($runValidation, $attributeNames); // TODO: Change the autogenerated stub
    }

    /**
     * @param $localPath
     * @throws yii\db\Exception
     */
    public function updateLastAccessTime($localPath)
    {
        if ($this->lastLocalPath != $localPath && $this->id && !$this->isIgnoreAccessDate) {
            Yii::$app->getDb()->createCommand('UPDATE file SET last_access_at=\'' . DateHelper::now() . '\' WHERE id=' . $this->id)->execute();
            $this->lastLocalPath = $localPath;
        }
    }

    /**
     * @param File $file
     * @return bool
     */
    public function equalName(File $file): bool
    {
        return $this->name === $file->name;
    }
}
