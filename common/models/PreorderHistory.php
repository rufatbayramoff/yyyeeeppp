<?php

namespace common\models;

use common\interfaces\ModelHistoryInterface;
use common\traits\ModelHistoryARTrait;

/**
 * Class PreorderHistory
 * @package common\models
 */
class PreorderHistory extends \common\models\base\PreorderHistory implements  ModelHistoryInterface
{
    use ModelHistoryARTrait;

}