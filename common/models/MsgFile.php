<?php

namespace common\models;
use common\components\exceptions\AssertHelper;

/**
 * Class MsgFile
 * @package common\models
 */
class MsgFile extends \common\models\base\MsgFile
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DELETED = 'deleted';

    /**
     * @param MsgMessage $message
     * @param File $file
     * @return MsgFile
     *
     *  * @property integer $id
     * @property integer $topic_id
     * @property integer $message_id
     * @property integer $user_id
     * @property integer $file_id
     * @property string $status
     */
    public static function create(MsgMessage $message, File $file) : MsgFile
    {
        $msgFile = new MsgFile();
        $msgFile->topic_id = $message->topic_id;
        $msgFile->message_id = $message->id;
        $msgFile->user_id = $message->user_id;
        $msgFile->file_id = $file->id;
        $msgFile->status = self::STATUS_ACTIVE;
        AssertHelper::assertSave($msgFile);
        return $msgFile;
    }

    /**
     * Mark file as deleted
     */
    public function markAsDeleted()
    {
        $this->status = self::STATUS_DELETED;
        AssertHelper::assertSave($this);
    }

    /**
     * Is user have access to file
     * @param User $user
     * @return bool
     */
    public function isHaveAccess(User $user) : bool
    {
        $topic = $this->topic;
        foreach ($topic->msgMembers as $member){
            if ($member->user->equals($user)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isOwner(User $user) : bool
    {
        return $user->equals($this->user);
    }

    /**
     * Is file active
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

}