<?php
namespace common\models;

use Yii;

/**
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserRequest extends \common\models\base\UserRequest
{
    /**
     * type of user requests
     */
    const REQUEST_TYPE_DELETE = 'delete';
    const REQUEST_TYPE_RESTORE = 'restore';
    
    
    /**
     * new user request
     */
    const STATUS_NEW  = 'new';
    
    /**
     * user request complete
     */
    const STATUS_DONE = 'done';
    
    /**
     * user request was cancelled
     */
    const STATUS_CANCEL = 'cancel';
    
    /**
     * user requested delete, after restored, should be closed
     */
    const STATUS_REVERT = 'revert';
        
}