<?php

namespace common\models;

/**
 * Class HomePageFeatured
 * @package common\models
 * @property \common\models\HomePageFeaturedProduct[] $homePageFeaturedActiveProducts
 */
class HomePageFeatured extends \common\models\base\HomePageFeatured
{
    /**
     * @var array
     */
    protected $_productUuids = [];

    public function rules()
    {
        return [
            ['productUuids', function() {
                if (!is_array($this->_productUuids)) {
                    $this->addError('productUuids', 'Add products');
                }

                if (count($this->_productUuids) > 4) {
                    $this->addError('productUuids', 'Maximum 4 products');
                }

                $errorUuids = [];

                foreach ($this->_productUuids as $k => $uuid) {
                    $uuid = trim($uuid);

                    if (!$this->checkProduct($uuid)) {
                        $errorUuids[] = $uuid;
                    }

                    $this->_productUuids[$k] = $uuid;
                }

                if (!empty($errorUuids)) {
                    $this->addError('productUuids', 'Uuid "'.implode($errorUuids, ',').'" does not exist');
                }
            }, 'skipOnEmpty' => false],
            [['created_at', 'position'], 'required'],
            [['created_at'], 'safe'],
            [['is_active', 'product_category_id', 'position'], 'integer'],
            [['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\ProductCategory::class, 'targetAttribute' => ['product_category_id' => 'id']],
        ];
    }

    /**
     * @return string
     */
    public function getProductUuids(): string
    {
        if (empty($this->_productUuids)) {
            $homePageFeaturedProducts = $this->homePageFeaturedProducts;

            if ($homePageFeaturedProducts) {
                $this->_productUuids = array_map(function (HomePageFeaturedProduct $homePageFeaturedProduct) {
                    return $homePageFeaturedProduct->product_uuid;
                }, $homePageFeaturedProducts);
            }
        }

        return implode($this->_productUuids, ', ');
    }

    /**
     * @return array
     */
    public function getProductUuidsArray(): array
    {
        $res = [];

        foreach ($this->_productUuids as $productUuid) {
            $res[] = [$this->id, $productUuid];
        }

        return $res;
    }

    /**
     * @param $value
     */
    public function setProductUuids($value): void
    {
        $this->_productUuids = is_array($value) ? $value : explode(',', $value);
    }

    public function checkProduct($uuid)
    {
        return Product::find()->where(['is_active' => 1, 'uuid' => $uuid])->scalar() ? true : false;
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function saveFeatured()
    {
        if ($this->save()) {
            HomePageFeaturedProduct::deleteAll(['featured_category_id' => $this->id]);

            $this->getProductUuidsArray();

            app()->db->createCommand()
                ->batchInsert(HomePageFeaturedProduct::tableName(), [
                    'featured_category_id',
                    'product_uuid'
                ], $this->getProductUuidsArray())
                ->execute();

            return true;
        }

        return false;
    }

    /**
     * @return Product[]
     */
    public function getActiveProducts(): array
    {
        $homePageFeaturedActiveProducts = $this->homePageFeaturedActiveProducts;

        if (empty($homePageFeaturedActiveProducts)) {
            return [];
        }

        $products = [];

        foreach ($homePageFeaturedActiveProducts as $product) {
            $products[] = $product->publishedProduct;
        }


        return $products;
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getHomePageFeaturedActiveProducts()
    {
        return $this->hasMany(\common\models\HomePageFeaturedProduct::class, ['featured_category_id' => 'id'])
            ->joinWith('publishedProduct')
            ->inverseOf('featuredCategory');
    }
}