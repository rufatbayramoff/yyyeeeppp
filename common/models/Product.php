<?php

namespace common\models;

use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\components\DateHelper;
use common\components\exceptions\InvalidModelException;
use common\interfaces\FileBaseInterface;
use common\models\factories\UserLocationFactory;
use common\models\query\ProductFileQuery;
use common\modules\dynamicField\models\DynamicFieldValue;
use common\modules\dynamicField\services\DynamicFieldService;
use common\modules\payment\fee\FeeHelper;
use common\modules\product\interfaces\ProductInterface;
use common\modules\product\models\ProductTrait;
use frontend\components\image\ImageHtmlHelper;
use lib\money\Currency;
use lib\money\Money;
use yii\data\ArrayDataProvider;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class Product
 *
 * @property string $cuid
 * @property int $user_id
 * @property File[] $imageFiles
 * @property ProductFile[] $productAttachments
 * @property ProductFile[] $cadFiles
 * @property CompanyCertification[] $certifications
 * @property ProductBlock[] $productBlocks
 * @property UserLocation $shipFrom
 *
 * @package common\models
 *
 */
class Product extends \common\models\base\Product implements ProductInterface
{
    const TIME_DAY   = 'day';
    const TIME_WEEK  = 'week';
    const TIME_MONTH = 'month';
    const TIME_YEAR  = 'year';

    const UNIT_TYPE_PIECE = 'pc.';

    protected $canBePublishUpdated = true;
    public $formCategoryId;
    public $formTitle = '';
    public $formDescription = '';
    public $formSinglePrice;

    public $deleteCoverPhotoFile;

    /**
     * @var DynamicFieldValue[]
     */
    public $dynamicFieldValues;

    use ProductTrait {

    }

    public function getUnitTypeTitle($qty): string
    {
        $unitType = $this->unit_type;
        $titles   = self::getUnitTypesList($qty > 1);
        return ($titles[$unitType] ?? $this->unit_type) ?? '';
    }

    public function getUnitTypeCode(): string
    {
        return $this->unit_type;
    }

    public static function getUnitTypesList($plural = false)
    {

        $result = [
            "bag"                 => _t('common.product', 'bag'),
            "bbl."                => _t('common.product', 'barrel'),
            "bolt"                => _t('common.product', 'bolt'),
            "box"                 => _t('common.product', 'box'),
            "bunch"               => _t('common.product', 'bunch'),
            "bdl."                => _t('common.product', 'bundle'),
            "butt"                => _t('common.product', 'butt'),
            "can"                 => _t('common.product', 'can'),
            "can."                => _t('common.product', 'canister'),
            "ctn"                 => _t('common.product', 'carton'),
            "case"                => _t('common.product', 'case'),
            "cm"                  => _t('common.product', 'centimeter'),
            "cnt."                => _t('common.product', 'container'),
            "crt."                => _t('common.product', 'crate'),
            "m3"                  => _t('common.product', 'cubic meter'),
            "ft3"                 => _t('common.product', 'cubic foot'),
            "cyl."                => _t('common.product', 'cylinder'),
            "yd3"                 => _t('common.product', 'cubic yard'),
            "doz."                => _t('common.product', 'dozen'),
            "ea."                 => _t('common.product', 'each'),
            "env."                => _t('common.product', 'envelope'),
            "ft"                  => _t('common.product', 'foot'),
            "gal."                => _t('common.product', 'gallon'),
            "in"                  => _t('common.product', 'inch'),
            "kg"                  => _t('common.product', 'kilogram'),
            "kit"                 => _t('common.product', 'kit'),
            "l"                   => _t('common.product', 'liter'),
            "m"                   => _t('common.product', 'meter'),
            "t"                   => _t('common.product', 'ton'),
            "oz."                 => _t('common.product', 'ounce'),
            "pack"                => _t('common.product', 'pack'),
            "pkt."                => _t('common.product', 'packet'),
            "pair"                => _t('common.product', 'pair'),
            "plt."                => _t('common.product', 'pallet'),
            self::UNIT_TYPE_PIECE => _t('common.product', 'piece'),
            "lb."                 => _t('common.product', 'pound'),
            "roll"                => _t('common.product', 'roll'),
            "set"                 => _t('common.product', 'set'),
            "sheet"               => _t('common.product', 'sheet'),
            "ft2"                 => _t('common.product', 'square foot'),
            "m2"                  => _t('common.product', 'square meter'),
            "sqyd"                => _t('common.product', 'square yard'),
            "tube"                => _t('common.product', 'tube'),
            "unit"                => _t('common.product', 'unit'),
            "yard"                => _t('common.product', 'yard'),
        ];
        if ($plural) {
            $result = [
                "bag"                 => _t('common.product', 'bags'),
                "bbl."                => _t('common.product', 'barrels'),
                "bolt"                => _t('common.product', 'bolts'),
                "box"                 => _t('common.product', 'boxes'),
                "bunch"               => _t('common.product', 'bunches'),
                "bdl."                => _t('common.product', 'bundles'),
                "butt"                => _t('common.product', 'butts'),
                "can"                 => _t('common.product', 'cans'),
                "can."                => _t('common.product', 'canisters'),
                "ctn"                 => _t('common.product', 'cartons'),
                "case"                => _t('common.product', 'cases'),
                "cm"                  => _t('common.product', 'centimeters'),
                "cnt."                => _t('common.product', 'containers'),
                "crt."                => _t('common.product', 'crates'),
                "m3"                  => _t('common.product', 'cubic meters'),
                "ft3"                 => _t('common.product', 'cubic feet'),
                "cyl."                => _t('common.product', 'cylinders'),
                "yd3"                 => _t('common.product', 'cubic yards'),
                "doz."                => _t('common.product', 'dozens'),
                "ea."                 => _t('common.product', 'each'),
                "env."                => _t('common.product', 'envelopes'),
                "ft"                  => _t('common.product', 'feet'),
                "gal."                => _t('common.product', 'gallons'),
                "in"                  => _t('common.product', 'inches'),
                "kg"                  => _t('common.product', 'kilograms'),
                "kit"                 => _t('common.product', 'kits'),
                "l"                   => _t('common.product', 'liters'),
                "m"                   => _t('common.product', 'meter'),
                "t"                   => _t('common.product', 'tons'),
                "oz."                 => _t('common.product', 'ounces'),
                "pack"                => _t('common.product', 'packs'),
                "pkt."                => _t('common.product', 'packets'),
                "pair"                => _t('common.product', 'pairs'),
                "plt."                => _t('common.product', 'pallets'),
                self::UNIT_TYPE_PIECE => _t('common.product', 'pieces'),
                "lb."                 => _t('common.product', 'pounds'),
                "roll"                => _t('common.product', 'rolls'),
                "set"                 => _t('common.product', 'sets'),
                "sheet"               => _t('common.product', 'sheets'),
                "ft2"                 => _t('common.product', 'square feet'),
                "m2"                  => _t('common.product', 'square meters'),
                "sqyd"                => _t('common.product', 'square yards'),
                "tube"                => _t('common.product', 'tubes'),
                "unit"                => _t('common.product', 'units'),
                "yard"                => _t('common.product', 'yards'),
            ];
        }

        return $result;
    }

    public static function getUnitTypesComboList()
    {
        $br     = self::getUnitTypesList();
        $result = [];
        foreach ($br as $k => $v) {
            $result[$k] = sprintf("%s (%s)", $v, $k);
        }
        return $result;
    }

    public static function getSupplyAbilityTimeList()
    {
        return [
            self::TIME_DAY   => _t('common.product', "Day"),
            self::TIME_WEEK  => _t('common.product', "Week"),
            self::TIME_MONTH => _t('common.product', "Month"),
            self::TIME_YEAR  => _t('common.product', "Year"),
        ];
    }

    public static function getAvgProductionTimeList()
    {
        return [
            self::TIME_DAY   => _t('common.product', "Day"),
            self::TIME_WEEK  => _t('common.product', "Week"),
            self::TIME_MONTH => _t('common.product', "Month"),
            self::TIME_YEAR  => _t('common.product', "Year"),
        ];
    }

    public static function getIncotermsList()
    {
        return [
            ""    => "-",
            "EXW" => "EXW – Ex Works (named place of delivery)",
            "FCA" => "FCA – Free Carrier (named place of delivery)",
            "CPT" => "CPT – Carriage Paid To (named place of destination)",
            "CIP" => "CIP – Carriage and Insurance Paid to (named place of destination)",
            "DAT" => "DAT – Delivered At Terminal (named terminal at port or place of destination)",
            "DAP" => "DAP – Delivered At Place (named place of destination)",
            "DDP" => "DDP – Delivered Duty Paid (named place of destination)",
            "FAS" => "FAS – Free Alongside Ship (named port of shipment)",
            "FOB" => "FOB – Free on Board (named port of shipment)",
            "CFR" => "CFR – Cost and Freight (named port of destination)",
            "CIF" => "CIF – Cost, Insurance & Freight (named port of destination)"
        ];
    }

    public static function getProductPackageList()
    {
        return [
            "Paperboard boxes",
            "Corrugated boxes",
            "Plastic boxes",
            "Rigid boxes",
            "Chipboard packaging",
            "Poly bags",
            "Foil sealed bags"
        ];
    }

    public static function getOuterPackageList()
    {
        return [
            "Corrugated box",
            "Wooden box",
            "Crate",
            "Bulk box",
            "Drum",
            "Pail",
            "Pallet"
        ];
    }


    public function getImageFiles()
    {
        $productImages = $this->productImages;
        uasort(
            $productImages,
            function (ProductImage $a, ProductImage $b) {
                return $a->pos > $b->pos;
            }
        );
        $files = ArrayHelper::getColumn($productImages, 'file');
        return $files;
    }

    /**
     * get attached files from product edit
     *
     * @return File[]
     */
    public function getAttachmentFiles()
    {
        $attachments = $this->getProductAttachments()->all();
        $files       = ArrayHelper::getColumn($attachments, 'file');
        return $files;
    }

    public function updateFormAttributes()
    {
        $this->formCategoryId  = $this->productCommon->category_id;
        $this->formTitle       = $this->productCommon->title;
        $this->formDescription = $this->productCommon->description;
        $this->formSinglePrice = $this->productCommon->single_price;
    }

    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['deleteCoverPhotoFile'], 'safe'],
                [['productCategory'], 'required', 'message' => _t('mybusiness.product', 'Product category cannot be blank.')],
                [['moq_prices'], 'required', 'message' => _t('mybusiness.product', 'Product prices information cannot be blank.')],
                [['moq_prices'], 'validateMoqPrices'], //''required', 'message'=>'Product prices information is required'],
                [['dynamicFieldValues'], 'validateDynamicFields'],
                [['formCategoryId'], 'integer'],
                [['formTitle', 'formDescription'], 'string', 'skipOnEmpty' => true],
                [['formSinglePrice'], 'number', 'max' => 99999],
            ]
        );
    }

    public function validateMoqPrices()
    {
        if (empty($this->moq_prices)) {
            return;
        }
        foreach ($this->moq_prices as $moqPrice) {
            if (!is_numeric($moqPrice['moq'])) {
                $this->addError('moq_prices', _t('mybusiness.product', 'Min.order qty is invalid'));
            }
            if (empty($moqPrice['priceWithFee']) || !is_numeric($moqPrice['priceWithFee'])) {
                $this->addError('moq_prices', _t('mybusiness.product', 'Price per unit is invalid'));
            }
        }
    }

    public function validateDynamicFields()
    {
        foreach ($this->dynamicFieldValues as $dynamicFieldValue) {
            if (!$dynamicFieldValue->validate()) {
                foreach ($dynamicFieldValue->getErrors() as $error) {
                    $errorText = reset($error);
                    $this->addError('dynamicField_' . $dynamicFieldValue->dynamicField->code, $errorText);
                }
            }
        }
    }

    public function getModeratedAt(): string
    {
        return $this->moderated_at;
    }

    public function getPublishedAt(): string
    {
        return $this->published_at;
    }

    public function getCategory()
    {
        return $this->productCategory;
    }


    /**
     * @return string
     */
    public function getCoverUrl($w = ImageHtmlHelper::DEFAULT_WIDTH, $h = ImageHtmlHelper::DEFAULT_HEIGHT): string
    {
        if ($this->isNewRecord || !$this->imageFiles) {
            return '';
        }
        $imageFiles     = $this->imageFiles;
        $coverImageFile = reset($imageFiles);
        return $coverImageFile ? ImageHtmlHelper::getThumbUrlForFile($coverImageFile, $w, $h, true) : '';
    }

    /**
     * @return File[]
     */
    public function getImages(): array
    {
        $images = [];
        foreach ($this->imageFiles as $key => $imageFile) {
            if (!$imageFile->forDelete) {
                $images[$key] = $imageFile;
            }
        }
        return $images;
    }

    /**
     * @return SiteTag[]
     */
    public function getProductTags(): array
    {
        return $this->siteTags;
    }

    public function isActive(): bool
    {
        return $this->is_active;
    }

    public function setCantBePublisehedUpdated(): void
    {
        $this->canBePublishUpdated = false;
    }

    public function isCanBePublishUpdated(): bool
    {
        return $this->canBePublishUpdated;
    }

    public function getProductType(): string
    {
        return ProductInterface::TYPE_PRODUCT;
    }

    /**
     * @param array $params
     * @return string
     */
    public function getPublicPageUrl(array $params = []): string
    {
        $slug     = Inflector::slug($this->title);
        $uuid     = $this->uuid;
        $itemLink = Url::toRoute(array_merge(['/product/' . $this->uuid . '-' . $slug], $params));
        return $itemLink;
    }

    public function formName()
    {
        return 'ProductForm';
    }

    /**
     * @return Company
     */
    public function getCompany(): ?Company
    {
        return $this->productCommon ? $this->productCommon->company : null;
    }


    public function getImageFileByUuid($fileUuid)
    {
        foreach ($this->imageFiles as $imageFile) {
            if ($imageFile->uuid == $fileUuid) {
                return $imageFile;
            }
        }
        return null;
    }

    /**
     * @param File $file
     */
    public function setCoverImageFile(File $file)
    {
        $files = [$file];
        $this->addImages($files);
    }

    /**
     * @param File[] $files
     */
    public function addImages($files)
    {
        $currentProductImages = $this->productImages;
        foreach ($files as $index => $file) {
            $file->setPublicMode(true);
            $file->setOwner(ProductImages::class, 'file_uuid');
            $productImage               = new ProductImage();
            $productImage->product_uuid = $this->uuid;
            $productImage->file_uuid    = $file->uuid;
            $productImage->pos          = $index;
            $productImage->populateRelation('file', $file);
            $currentProductImages[] = $productImage;
        }
        $this->populateRelation('productImages', $currentProductImages);
    }

    /**
     * @param File[] $files
     */
    public function addAttachments($files)
    {
        $productAttachments = $this->productAttachments;
        foreach ($files as $index => $file) {
            $file->setPublicMode(true);
            $file->setOwner(ProductFile::class, 'file_id');
            $productFile               = new ProductFile();
            $productFile->product_uuid = $this->uuid;
            $productFile->created_at   = DateHelper::now();
            $productFile->title        = $file->name;
            $productFile->type         = ProductFile::TYPE_ATTACHMENT;
            $productFile->populateRelation('file', $file);
            $productAttachments[] = $productFile;
        }
        $this->populateRelation('productAttachments', $productAttachments);
    }

    public function hasDynamicValues()
    {
        return !empty($this->dynamic_fields_values);
    }

    /**
     * @return DynamicFieldValue[]
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function getDynamicValues()
    {
        if ($this->dynamicFieldValues !== null) {
            return $this->dynamicFieldValues;
        }
        $dynamicFieldsService = \Yii::createObject(DynamicFieldService::class);
        if (!$this->category) {
            return [];
        }
        $this->dynamicFieldValues = $dynamicFieldsService->getDynamicFieldValues($this->category, $this->dynamic_fields_values);
        return $this->dynamicFieldValues;
    }

    public function getDynamicValuesAsArray()
    {
        $dynamicFieldsService = \Yii::createObject(DynamicFieldService::class);
        $dynamicFieldValues   = $this->getDynamicValues();
        $result               = [];
        foreach ($dynamicFieldValues as $dynamicFieldValue) {
            $result[H($dynamicFieldValue->dynamicField->title)] = $dynamicFieldsService->displayValue($dynamicFieldValue);
        }
        return $result;
    }

    /**
     * @param $dynamicFieldCode
     * @return DynamicFieldValue|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getDynamicFieldValue($dynamicFieldCode)
    {
        return $this->getDynamicValues()[$dynamicFieldCode] ?? null;
    }

    public function setMoqPrices(array $moqPrices)
    {
        $feeHelper  = \Yii::createObject(FeeHelper::class);
        $feePercent = $feeHelper->getManufacturingFeePercent();
        $prices     = [];
        foreach ($moqPrices as $moqPrice) {
            $moqPrice['moq']          = floatval($moqPrice['moq']);
            $moqPrice['priceWithFee'] = $priceWithFee = floatval($moqPrice['priceWithFee']);
            $moqPrice['price']        = $priceWithFee - round(($feePercent * 100 * $priceWithFee / (100 + $priceWithFee * 100)), 2);

            if (floatval($moqPrice['moq']) <= 0) {
                $this->addError('moq_prices', _t('site.product', 'Quantity must be greater than 0'));
                return false;
            }
            if (floatval($moqPrice['price'] <= 0)) {
                $this->addError('moq_prices', _t('site.product', 'Price must be greater than 0'));
                return false;
            }
            if (in_array($moqPrice['moq'], $prices)) {
                $this->addError('moq_prices', _t('site.product', 'Quantity discount prices must be unique'));
                return false;
            }
            $prices[] = $moqPrice;
        }
        $this->moq_prices = $prices;
        // find min price and set to single price
        $this->productCommon->single_price = $this->getMoqFromPrice(false);
        $this->productCommon->single_price = $this->getMoqFromPrice(false);
    }

    public function getMoqPricesFormatted()
    {
        $moqPrices = (array)$this->moq_prices;

        usort(
            $moqPrices,
            function ($item, $item2) {
                return floatval($item['moq']) <=> floatval($item2['moq']);
            }
        );
        $result = [];
        foreach ($moqPrices as $k => $moqPrice) {
            $qtyFrom   = (float)$moqPrice['moq'];
            $price     = (float)$moqPrice['priceWithFee'];
            $nextEl    = !empty($moqPrices[$k + 1]) ? $moqPrices[$k + 1] : null;
            $qtyTo     = null;
            $qtyToOrig = $qtyFrom;
            if ($nextEl && $nextEl['moq']) {
                $qtyToOrig = $qtyTo = floatval($nextEl['moq']);

                $decimalPr = strlen(substr(strrchr($qtyFrom, "."), 1)) * 10;
                if ($decimalPr === 0) {
                    $decimalPr = 1;
                }
                $qtyTo = $qtyTo - (1 / ($decimalPr));
            }
            if ($qtyFrom) {
                $qtyFrom = floatval($qtyFrom);
            }
            $qtyTo = abs($qtyTo);
            $title = $qtyTo ? $qtyFrom . ' - ' . $qtyTo : ' &#8805; ' . $qtyFrom;
            if ($qtyFrom == $qtyTo) {
                $title = $qtyTo;
            }
            $result[] = [
                'title'       => $title,
                'qtyTo'       => $qtyToOrig,
                'price'       => sprintf('%s / %s', displayAsCurrency($price, 'USD'), $this->unit_type),
                'oneline'     => sprintf('<b>%s</b> from %d %s', displayAsCurrency($price, 'USD'), $qtyFrom, $this->getUnitTypeTitle($qtyFrom)),
                'qtyFrom'     => $qtyFrom,
                'priceAmount' => displayAsCurrency($price, 'USD'),
                'unitType'    => $this->getUnitTypeTitle(min($qtyTo, $qtyFrom)),
                'unitCode'    => $this->getUnitTypeCode()
            ];
        }
        if (count($result) == 1) {
            return [];
        }
        return $result;
    }

    public function getMoqFromPrice($noRecalc = true)
    {
        if ($noRecalc && !empty($this->single_price)) {
            return $this->single_price;
        }
        $moqPrices = (array)$this->moq_prices;
        $result    = 0;
        foreach ($moqPrices as $k => $moqPrice) {
            if (empty($moqPrice['priceWithFee'])) {
                continue;
            }
            $tmp = (float)$moqPrice['priceWithFee'];
            if ($result === 0 || $tmp < $result) {
                $result = $tmp;
            }
        }
        return $result;
    }

    public function getMoqFromPriceMoney(): ?Money
    {
        return Money::create($this->getMoqFromPrice(), $this->getCompanyManufacturer()->currency);
    }

    public function getMinOrderQty(): float
    {
        $qtys = array_column((array)$this->moq_prices, 'moq');
        if (empty($qtys)) {
            return 1;
        }
        return min($qtys);
    }

    public static function getPriceByQty($moqPrices, $qty)
    {
        $result = null;
        foreach ($moqPrices as $k => $moqPrice) {
            if ((array_key_exists('moq', $moqPrice)) && ($qty >= (int)$moqPrice['moq'])) {
                if ($result === null || (float)$moqPrice['priceWithFee'] < $result) {
                    $result = (float)$moqPrice['priceWithFee'];
                }
            }
        }
        return $result;
    }

    /**
     * @param $qty
     * @return Money
     */
    public function getPriceMoneyByQty($qty): ?Money
    {
        if ($qty == 1) {
            $price = $this->productCommon->single_price;
        } else {
            $price = self::getPriceByQty((array)$this->moq_prices, $qty);
        }
        return ($price !== null) ? Money::create($price, $this->company->currency) : null;
    }

    public function setCustomProperties(array $userFieldValues)
    {
        $this->custom_properties = $userFieldValues;
    }

    public function getCustomProperties()
    {
        if (empty($this->custom_properties)) {
            return [];
        }
        $props  = is_array($this->custom_properties) ? $this->custom_properties : Json::decode($this->custom_properties); // format ['title'=>, 'value'=>, 'position'=>]
        $result = [];
        foreach ($props as $prop) {
            if (empty($prop['title'])) {
                continue;
            }
            $result[$prop['title']] = $prop['value'];
        }
        return $result;
    }

    /**
     * @return \common\models\query\UserQuery
     */
    public function getShipFrom()
    {
        return $this->hasOne(\common\models\UserLocation::class, ['id' => 'ship_from_id']);
    }

    public function getProductBlocks()
    {
        return $this->hasMany(\common\models\ProductBlock::class, ['product_uuid' => 'uuid'])->inverseOf('product');
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getProductAttachments()
    {
        return $this->hasMany(
            \common\models\ProductFile::class,
            ['product_uuid' => 'uuid']
        )
            ->inverseOf('product')->andWhere(['type' => ProductFile::TYPE_ATTACHMENT]);
    }

    /**
     * @return array
     */
    public function getPublicProductBlocks()
    {
        $result        = [];
        $productBlocks = $this->getProductBlocks()->where(['is_visible' => 1])->orderBy('position')->all();
        foreach ($productBlocks as $productBlock) {
            $result[] = $productBlock;
        }
        $companyBlocks = $this->getBindedCompanyBlocks();
        foreach ($companyBlocks as $companyBlock) {
            if (!$companyBlock->block->is_visible) {
                continue;
            }
            $result[] = $companyBlock->block;
        }
        return $result;
    }

    public function getShipFromLocation($stub = false)
    {
        // if new , try to find from prev.product
        $shipFromLocation = null;
        if ($this->isNewRecord) {
            $shipFromLocation = UserLocation::findOne(['user_id' => $this->user->id, 'location_type' => UserLocation::LOCATION_TYPE_SHIP_FROM]);
        } else {
            $shipFromLocation = UserLocation::findOne(['id' => $this->ship_from_id]);
        }
        if ($stub && $shipFromLocation === null) {
            $shipFromLocation = new UserLocation();
        }
        return $shipFromLocation;
    }

    /**
     * @param array $obj
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function setShipFromLocation(array $obj)
    {
        $location                = UserLocationFactory::createFromGoogleApiData($this->user, $obj);
        $location->location_type = UserLocation::LOCATION_TYPE_SHIP_FROM;
        $location->safeSave();
        $this->ship_from_id = $location->id;
    }

    public function setProductVideos($videos)
    {
        $this->videos = $videos;
    }

    public function getProductVideos()
    {
        $props = $this->videos;
        return $props;
    }

    public function getMoqPricesJson()
    {
        return json_encode($this->moq_prices);
    }

    public function getVideosJson()
    {
        return json_encode($this->videos);
    }

    public function getCustomPropertiesJson()
    {
        return json_encode($this->custom_properties);
    }

    /**
     * @return ProductFileQuery
     */
    public function getCadFiles()
    {
        return $this->getProductFiles()->byType(ProductFile::TYPE_CAD);
    }

    public function isRejected()
    {
        return $this->product_status == self::STATUS_REJECTED;
    }

    /**
     * valid one day
     *
     * @return string
     */
    public function generatePrivateViewCode()
    {
        return md5($this->uuid . date("y-m-d") . 'ts');
    }

    public function getProductDelivery()
    {
        if (!$this->productExpressDeliveries) {
            return null;
        }
        $delivery = $this->productExpressDeliveries[0];
        return $delivery;
    }

    /**
     * @return array
     */
    public function getBindedCompanyBlockIds()
    {
        $blockIds = $this->getCompanyBlockBinds()->withoutStaticCache()->select('block_id')->column();
        return array_map('intval', $blockIds);
    }

    /**
     * @return CompanyBlockBind[]
     */
    public function getBindedCompanyBlocks()
    {
        $blocks = $this->getCompanyBlockBinds()->joinWith('block')->withoutStaticCache()
            ->orderBy('company_block.position')->all();
        return $blocks;
    }


    public function getBindedCompanyCertificationsIds()
    {
        $ids = $this->getCompanyCertificationBinds()->withoutStaticCache()->select('certification_id')->column();
        return array_map('intval', $ids);
    }

    /**
     * @return CompanyCertificationBind[]
     */
    public function getBindedCompanyCertifications()
    {
        $blocks = $this->getCompanyCertificationBinds()->joinWith('certification')->withoutStaticCache()
            ->orderBy('company_certification_bind.id')->all();
        return $blocks;
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getCertifications()
    {
        return $this->hasMany(\common\models\CompanyCertification::class, ['id' => 'certification_id'])
            ->viaTable('company_certification_bind', ['product_uuid' => 'uuid']);
    }


    /**
     * @return ArrayDataProvider
     */
    public function getCertificationsDataProvider()
    {
        $result       = [];
        $productCerts = $this->productCertifications;
        $companyCerts = $this->certifications ?? [];
        foreach ($productCerts as $productCert) {
            $result[] = $productCert;
        }
        foreach ($companyCerts as $productCert) {
            $result[] = $productCert;
        }
        $provider = new ArrayDataProvider(
            [
                'allModels'  => $result,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]
        );
        return $provider;
    }

    public function hasExpressDelivery()
    {
        return true;
    }

    public function hasWholesaleDelivery()
    {
        return false;
    }

    public function getCompanyManufacturer(): ?Company
    {
        return $this->productCommon->company;
    }

    public function getProductCategory(): ?ProductCategory
    {
        return $this->productCommon->category;
    }

    public function getUser(): ?User
    {
        return $this->productCommon->user;
    }

    public function getUser_id()
    {
        return $this->productCommon->user_id;
    }

    public function getCreated_at()
    {
        return $this->productCommon->created_at;
    }

    public function getUpdated_at()
    {
        return $this->productCommon->updated_at;
    }

    public function getProduct_status()
    {
        return $this->productCommon->product_status;
    }

    public function getPrice_per_produce()
    {
        return $this->productCommon->single_price;
    }

    public function getIs_active()
    {
        return $this->productCommon->is_active;
    }

    public function getTitle(): string
    {
        return $this->productCommon->title;
    }

    public function getTitleLabel(): string
    {
        return $this->productCommon->title;
    }

    public function getDescription(): string
    {
        return $this->productCommon->description ?? '';
    }

    public function getCuid(): string
    {
        return $this->productCommon->uid;
    }
}