<?php

namespace common\models;

/**
 * Class CompanyServiceBlock
 *
 * @property \common\models\File[] $imageFiles
 * @package common\models
 */
class CompanyServiceBlock extends \common\models\base\CompanyServiceBlock
{
    public function getImageFiles()
    {
        return $this->hasMany(File::class, ['uuid' => 'file_uuid'])->via('companyServiceBlockImages');
    }

    /**
     * @return File[]
     */
    public function getImages(): array
    {
        return $this->imageFiles;
    }

    public function getImageFileByUuid($fileUuid)
    {
        foreach ($this->imageFiles as $imageFile) {
            if ($imageFile->uuid == $fileUuid) {
                return $imageFile;
            }
        }
        return null;
    }
    /**
     * @param File[] $files
     */
    public function addImageFiles($files)
    {
        $currentFiles = $this->getImages();
        foreach ($files as $file) {
            $file->setPublicMode(true);
            $file->setOwner(CompanyServiceBlock::class, 'file_uuid');
            $currentFiles[] = $file;
        }
        $this->setImageFiles($currentFiles);
    }

    public function setImageFiles($files)
    {
        $this->populateRelation('imageFiles', $files);
    }
}