<?php
namespace common\models;

use common\components\exceptions\AssertHelper;
use Yii;

/**
 * Edit this file
 * This is the model class for table "site_search_query".
 */
class SiteSearchQuery extends \common\models\base\SiteSearchQuery
{
    /**
     * Add query to history or update frequency, if it already exist
     * @param string $query
     */
    public static function addOrUpdateFrequency($query)
    {
        AssertHelper::assert($query, 'Query cant be empty');

        $tableName = static::tableName();

        Yii::$app->db
            ->createCommand("INSERT INTO {$tableName} (query, qty) VALUES (:query, :qty) ON DUPLICATE KEY UPDATE qty = qty + 1", ['query' => $query, 'qty' => 1])
            ->execute();
    }
}