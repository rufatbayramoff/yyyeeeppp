<?php

namespace common\models;

use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\exceptions\BusinessException;
use common\components\order\anonim\AnonimOrderHelper;
use common\modules\contentAutoBlocker\components\contentFilters\BannedPhraseFilter;
use common\modules\payment\models\PaymentInvoiceAdditionalServiceAmount;
use common\modules\payment\models\PaymentInvoicePreorderAmount;
use common\modules\payment\models\PaymentInvoiceStoreOrderAmount;
use frontend\models\user\UserFacade;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use yii\base\UserException;

/**
 * Class PaymentInvoice
 *
 * @package common\models
 *
 * @property InstantPayment $instantPrimaryPayment
 * @property PaymentReceiptInvoiceComment $paymentReceiptInvoiceComment
 * @property PaymentInvoiceStoreOrderAmount $storeOrderAmount
 * @property PaymentInvoicePreorderAmount $preorderAmount
 * @property PaymentInvoiceAdditionalServiceAmount $additionalServiceAmount
 * @property PaymentCurrency $paymentCurrency
 * @property string $feeUuid
 */
class PaymentInvoice extends \common\models\base\PaymentInvoice
{
    public const EXPIRE_TIME              = 60 * 60 * 24 * 1; // One Day default
    public const EXPIRE_TIME_BANK_INVOICE = 60 * 60 * 24 * 7; // Seven days

    public const STATUS_NEW        = 'new';
    public const STATUS_AUTHORIZED = 'authorized';
    public const STATUS_PAID       = 'paid';
    public const STATUS_PART_PAID  = 'partPaid';
    public const STATUS_EXPIRED    = 'expired';
    public const STATUS_CANCELED   = 'canceled';
    public const STATUS_VOID       = 'voided';
    public const STATUS_REFUND     = 'refund';
    public const STATUS_REFUNDED   = 'refunded';


    public const ACCOUNTING_TYPE_MODEL                            = 'model';                   // Model price
    public const ACCOUNTING_TYPE_TAX                              = 'tax';                     // TAX
    public const ACCOUNTING_TYPE_MODEL_FEE                        = 'model_fee';               // Model fee
    public const ACCOUNTING_TYPE_FEE                              = 'fee';                     // Fee for additional service or preorders
    public const ACCOUNTING_TYPE_SHIPPING                         = 'shipping';                // Shipping Fee
    public const ACCOUNTING_TYPE_PACKAGE                          = 'package';                 // Package Fee
    public const ACCOUNTING_TYPE_PACKAGE_INFO                     = 'package_info';            // Package Fee
    public const ACCOUNTING_TYPE_REFUND                           = 'refund';                  // Refund
    public const ACCOUNTING_TYPE_DISCOUNT                         = 'discount';                // Discount
    public const ACCOUNTING_TYPE_PRINT                            = 'ps_print';                // Print Fee
    public const ACCOUNTING_TYPE_CUTTING                          = 'ps_cutting';              // Cutting Fee
    public const ACCOUNTING_TYPE_PS_DISCOUNT                      = 'ps_discount';             // Print Discount
    public const ACCOUNTING_TYPE_POSTPRINT                        = 'ps_postprint';            // PS after print work fee
    public const ACCOUNTING_TYPE_TRANSACTION                      = 'transaction';             // Transaction Fee
    public const ACCOUNTING_TYPE_ADDON                            = 'addon';                   // Additional Services
    public const ACCOUNTING_TYPE_PS_FEE                           = 'ps_fee';                  // PS Fee
    public const ACCOUNTING_TYPE_AFFILIATE_FEE_IN                 = 'affiliate_fee_in';        // Affiliate Fee
    public const ACCOUNTING_TYPE_AFFILIATE_FEE_OUT                = 'affiliate_fee_out';       // Affiliate Fee
    public const ACCOUNTING_TYPE_PS_PRODUCT                       = 'ps_product';              // Product total price
    public const ACCOUNTING_TYPE_INSTANT_PAYMENT                  = 'instant_payment';         // Instant payment
    public const ACCOUNTING_TYPE_TS_INTERNAL_PURCHASE_CERTIFICATE = 'ts_internal_certificate';             // TS internal purchase
    public const ACCOUNTING_TYPE_TS_INTERNAL_PURCHASE_DEPOSIT     = 'ts_internal_deposit';     // TS internal purchase deposit
    public const ACCOUNTING_TYPE_ITEMS                            = 'invoice_items';           // Positions invoice
    public const ACCOUNTING_TYPE_MANUFACTURER                     = 'manufacturer';            // Manufacturer fee
    public const ACCOUNTING_TYPE_PROMO_CODE                       = 'promo_code';              // Promo code
    public const ACCOUNTING_TYPE_BONUS                            = 'bonus';                   // Bonus
    public const ACCOUNTING_TYPE_PAYMENT_METHOD_FEE               = 'payment_method_fee';      // Payment method fee
    public const ACCOUNTING_TYPE_INSTANT_PAYMENT_FEE_IN           = 'instant_payment_fee_in';  // Instant payment fee included in price
    public const ACCOUNTING_TYPE_INSTANT_PAYMENT_FEE_OUT          = 'instant_payment_fee_out'; // Instant payment fee added to price
    public const ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS            = 'ts_bonus_accrued_ts';        // Print Fee
    public const ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS            = 'ts_bonus_accrued_ps';        // Print Fee
    public const ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE            = 'manufacturer_with_package'; //manufacture fee + package fee before 2021-11-24
    public const ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2         = 'manufacturer_with_package2'; //manufacture fee + package fee


    public const PAY_FOR_INSTANT_PAYMENT      = 'pay_for_instant_payment';
    public const PAY_FOR_ORDER_POSITION       = 'pay_for_order_position';
    public const PAY_FOR_TS_INTERNAL_PURCHASE = 'pay_for_ts_internal_purchase';
    public const PAY_FOR_PRINT_MODEL3D        = 'pay_for_print_model3d';
    public const PAY_FOR_QUOTE                = 'pay_for_quote';

    /** @var PaymentInvoiceStoreOrderAmount */
    public $storeOrderAmount;

    /** @var PaymentInvoicePreorderAmount */
    public $preorderAmount;

    public $additionalServiceAmount;

    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub

        $this->storeOrderAmount        = new PaymentInvoiceStoreOrderAmount($this);
        $this->preorderAmount          = new PaymentInvoicePreorderAmount($this);
        $this->additionalServiceAmount = new PaymentInvoiceAdditionalServiceAmount($this);
    }

    /**
     * @throws \Exception
     */
    public static function generateUuid()
    {
        /** @var BannedPhraseFilter $bannedPhraseFilter */
        $bannedPhraseFilter = \Yii::createObject(BannedPhraseFilter::class);
        $bannedPhraseFilter->setCheckMode(BannedPhraseFilter::MODEL_CHECK_WORD);

        do {
            $uuid      = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = self::find()->where(['uuid' => $uuid])->withoutStaticCache()->one();
            if ($bannedPhraseFilter->isAlarmText($uuid)) { // Bad words has checking
                continue;
            }
        } while ($checkUuid);
        return $uuid;
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getPaymentReceiptInvoiceComment()
    {
        $currentUserId = UserFacade::getCurrentUserId();
        return $this->hasOne(PaymentReceiptInvoiceComment::class, ['payment_invoice_uuid' => 'uuid'])
            ->leftJoin('payment_receipt', 'payment_receipt.id = payment_receipt_invoice_comment.receipt_id')
            ->andWhere(['payment_receipt.user_id' => $currentUserId]);
    }

    /**
     * @return \common\models\query\InstantPaymentQuery
     */
    public function getInstantPrimaryPayment()
    {
        return $this->hasOne(\common\models\InstantPayment::class, ['primary_invoice_uuid' => 'uuid'])->inverseOf('primaryInvoice');
    }

    /**
     * @return \common\models\query\InstantPaymentQuery
     */
    public function getInstantPayment()
    {
        return $this->hasOne(\common\models\InstantPayment::class, ['uuid' => 'instant_payment_uuid'])->inverseOf('paymentInvoices');
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getPaymentCurrency()
    {
        return $this->hasOne(\common\models\PaymentCurrency::class, ['currency_iso' => 'currency'])->inverseOf('paymentInvoices');
    }

    /**
     * @return array
     */
    public function getAccountingTypes(): array
    {
        return [
            self::ACCOUNTING_TYPE_MODEL,
            self::ACCOUNTING_TYPE_TAX,
            self::ACCOUNTING_TYPE_FEE,
            self::ACCOUNTING_TYPE_SHIPPING,
            self::ACCOUNTING_TYPE_PACKAGE,
            self::ACCOUNTING_TYPE_REFUND,
            self::ACCOUNTING_TYPE_DISCOUNT,
            self::ACCOUNTING_TYPE_PRINT,
            self::ACCOUNTING_TYPE_PS_DISCOUNT,
            self::ACCOUNTING_TYPE_POSTPRINT,
            self::ACCOUNTING_TYPE_TRANSACTION,
            self::ACCOUNTING_TYPE_ADDON,
            self::ACCOUNTING_TYPE_PS_FEE,
            self::ACCOUNTING_TYPE_AFFILIATE_FEE_IN,
            self::ACCOUNTING_TYPE_PS_PRODUCT,
            self::ACCOUNTING_TYPE_ITEMS,
            self::ACCOUNTING_TYPE_MANUFACTURER,
            self::ACCOUNTING_TYPE_PROMO_CODE,
            self::ACCOUNTING_TYPE_BONUS,
            self::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE
        ];
    }

    /**
     * @return bool
     */
    public function hasPayedPayments(): bool
    {
        foreach ($this->payments as $payment) {
            if ($payment->status === Payment::STATUS_NEW && $payment->paymentDetails) {
                return true;
            }
            if ($payment->wasPayed() || $payment->isPayInProcess()) {
                return true;
            }
        }
        return false;
    }

    public function isPayed()
    {
        return in_array($this->status, [self::STATUS_AUTHORIZED, self::STATUS_PAID, self::STATUS_PART_PAID]);
    }

    public function isPartPayed()
    {
        return $this->status === self::STATUS_PART_PAID;
    }

    public function isRefunded()
    {
        return $this->status === self::STATUS_REFUNDED;
    }

    public function isCanceled()
    {
        return $this->status === self::STATUS_CANCELED;
    }

    public function allowManualCancel()
    {
        return $this->storeOrder && $this->storeOrder->isCancelled() && in_array($this->status, [PaymentInvoice::STATUS_AUTHORIZED, PaymentInvoice::STATUS_PAID, PaymentInvoice::STATUS_PART_PAID]);
    }

    /**
     * @param string $type
     * @param array $data
     * @param null $item
     */
    public function addAccounting($type, array $data, $item = null): void
    {
        if ($item) {
            $this->accounting['items'][$item][$type] = $data;
        } else {
            $this->accounting[$item][$type] = $data;
        }
    }

    /**
     * @return bool
     */
    public function canBePaidOrFail(): void
    {
        if (!$this->isAvailableForUser()) {
            throw new BusinessException(_t('site.store', "Invoice payment unavailable for current user"));
        }

        if ($this->orderPosition) {

        } elseif ($storeOrder = $this->storeOrder) {
            AnonimOrderHelper::checkOrderAccess($storeOrder);
            StoreOrder::validateBeforeCheckout($storeOrder);
        }

        if ($this->status !== self::STATUS_NEW) {
            throw new BusinessException(_t('site.store', "Invoice can`t be paid."));
        };
    }

    /**
     * @return bool
     */
    public function allowPay(): bool
    {
        return !$this->hasPayedPayments();
    }

    /**
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function isAvailableForUser()
    {
        $user        = UserFacade::getCurrentUser();
        $userSession = UserFacade::getUserSession();
        if (UserFacade::isObjectOwner($this, $user, $userSession)) {
            return true;
        }
        return false;
    }

    public function isInternalOrder()
    {
        $internalUsers = app('setting')->get('store.internalOrdersCustomerUserId');
        if ($this->company && in_array($this->company->user_id, $internalUsers)) {
            return true;
        }
        if ($this->storeOrder?->isInternalOrder()) {
            return true;
        }
        if ($this->preorder?->isInternalOrder()) {
            return true;
        }
        if ($this->instantPayment?->isInternalOrder()) {
            return true;
        }
        if ($this->instantPayment && $this->instantPayment->isInternalOrder()) {
            return true;
        }
        return false;
    }

    /**
     * @return Money
     */
    public function getAmountRefund()
    {
        $query  = $this->getPaymentTransactionRefunds()->approved();
        $amount = $query->sum('amount');
        return Money::create($amount, $this->currency);
    }

    public function getAmountTotal(): Money
    {
        return Money::create($this->total_amount, $this->currency);
    }

    /**
     * @return Money
     */
    public function getAmountTotalWithoutPaymentMethodFee(): Money
    {
        $amountTotal   = $this->getAmountTotalWithRefund();
        $amountCartFee = $this->getAmountPaymentMethodFee();

        if (!$amountCartFee) {
            return $amountTotal;
        }

        return MoneyMath::minus($amountTotal, $amountCartFee);
    }

    /**
     * @return Money|null
     */
    public function getAmountPromoCode(): ?Money
    {
        return $this->getAmountByType(self::ACCOUNTING_TYPE_PROMO_CODE);
    }

    /**
     * @return Money|null
     */
    public function getAmountBonus(): ?Money
    {
        return $this->getAmountByType(self::ACCOUNTING_TYPE_BONUS);
    }

    public function getAmountBonusAccrued(): ?Money
    {
        return MoneyMath::sum($this->getAmountBonusAccruedFromPs(), $this->getAmountBonusAccruedFromTs());
    }

    public function getAmountBonusAccruedFromPs(): ?Money
    {
        return $this->getAmountByType(self::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS);
    }

    public function getAmountBonusAccruedFromTs(): ?Money
    {
        return $this->getAmountByType(self::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS);
    }

    public function getAmountBonusWithRefund(): ?Money
    {
        $amount      = $this->getAmountBonus();
        $refund      = $this->getAmountRefund();
        $returnValue = $refund && $amount ? MoneyMath::minus($amount, $refund) : $amount;
        return $returnValue;
    }

    /**
     * @return Money
     */
    public function getAmountTotalWithoutPromoCode(): Money
    {
        $moneyPromoCode = $this->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_PROMO_CODE);

        if ($moneyPromoCode) {
            return MoneyMath::sum($this->getAmountTotal(), $moneyPromoCode);
        }

        return $this->getAmountTotal();
    }

    public function getAmountTotalWithoutBonus(): Money
    {
        $moneyPromoCode = $this->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_BONUS);

        if ($moneyPromoCode) {
            return MoneyMath::sum($this->getAmountTotal(), $moneyPromoCode);
        }

        return $this->getAmountTotal();
    }


    /**
     * @return Money
     */
    public function getAmountTotalWithoutPromoCodeWithRefund(): Money
    {
        $moneyPromoCode = $this->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_PROMO_CODE);

        if ($moneyPromoCode) {
            return MoneyMath::sum($this->getAmountTotalWithRefund(), $moneyPromoCode);
        }

        return $this->getAmountTotalWithRefund();
    }

    /**
     * @return Money
     */
    public function getAmountTotalWithRefund()
    {
        $amount = $this->getAmountTotal();
        $refund = $this->getAmountRefund();
        $bonus  = $this->getAmountBonus();
        if ($bonus) {
            return Money::zero($amount->getCurrency()); // Patch for bonus payment with refund
        }
        $returnValue = $refund ? MoneyMath::minus($amount, $refund) : $amount;
        return $returnValue;
    }

    /**
     * @param $type
     *
     * @return Money|null
     */
    public function getAmountByType($type): ?Money
    {
        if (!array_key_exists($type, $this->accounting) || !array_key_exists('price', $this->accounting[$type])) {
            return null;
        }
        return Money::create($this->accounting[$type]['price'], $this->accounting[$type]['currency']);
    }

    public function hasAmountType($type): bool
    {
        return array_key_exists($type, $this->accounting) && array_key_exists('price', $this->accounting[$type]);
    }

    /**
     * If you wan`t filter by item use FALSE value
     *
     * @param $type
     * @param null $item
     *
     * @return Money
     */
    public function getAmountRefundByTypeAndItem($type, $item = null): Money
    {
        $query = $this->getPaymentTransactionRefunds()->approved();

        if ($item !== false) {
            $query->item($item);
        }

        $query->type($type);

        $amount = $query->sum('amount');
        return Money::create($amount, $this->currency); // Currently only usd support
    }

    public function getSortedPaymentInvoiceItems()
    {
        $items = $this->paymentInvoiceItems;
        uasort($items, function (PaymentInvoiceItem $a, PaymentInvoiceItem $b) {
            return $a->pos <=> $b->pos;
        });
        return $items;
    }

    /**
     * @param $itemKey
     * @param $type
     *
     * @return Money|null
     */
    public function getItemAmountByType($itemKey, $type): ?Money
    {
        if (!array_key_exists($itemKey, $this->accounting['invoice_items']) || !array_key_exists($type,
                $this->accounting['invoice_items'][$itemKey]) || !array_key_exists('price', $this->accounting['invoice_items'][$itemKey][$type])) {
            return null;
        }
        return Money::create($this->accounting['invoice_items'][$itemKey][$type]['price'], $this->currency);
    }

    /**
     * @param $itemKey
     * @param $type
     *
     * @return Money|null
     */
    public function getItemRefundByType($itemKey, $type): ?Money
    {
        $query  = $this->getPaymentTransactionRefunds()->approved()->item($itemKey)->type($type);
        $amount = $query->sum('amount');
        return Money::create($amount, Currency::USD); // Currently only usd support
    }

    /**
     * @param $itemKey
     * @param $type
     *
     * @return Money|null
     */
    public function getItemAmountByTypeWithRefund($itemKey, $type): ?Money
    {
        $amount = $this->getItemAmountByType($itemKey, $type);
        $refund = $this->getItemRefundByType($itemKey, $type);
        return $amount ? MoneyMath::minus($amount, $refund) : null;
    }

    /**
     * @param $type
     *
     * @return Money|null
     */
    public function getAmountByTypeWithRefund($type): ?Money
    {
        $amount = $this->getAmountByType($type);
        $refund = $this->getAmountRefundByTypeAndItem($type);
        return $amount ? MoneyMath::minus($amount, $refund) : null;
    }

    /**
     * @return Money|null
     */
    public function getAmountTreatstockFeeWithRefund(): ?Money
    {
        if ($this->baseByPrintOrder()) {
            return $this->storeOrderAmount->getAmountPsFeeWithRefund();
        } elseif ($this->baseByPreorder()) {
            return $this->preorderAmount->getAmountFeeWithRefund();
        } elseif ($this->baseByAdditionalService()) {
            return $this->additionalServiceAmount->getAmountFeeWithRefund();
        }

        return null;
    }

    /**
     * @return Money|null
     */
    public function getAmountManufacturerAwardWithRefund(): ?Money
    {
        if ($this->baseByPrintOrder()) {
            return $this->storeOrderAmount->getAmountPrintForPsWithRefund();
        }

        if ($this->baseByCuttingOrder()) {
            return $this->storeOrderAmount->getAmountCuttingForPsWithRefund();
        }

        if ($this->baseByAdditionalService()) {
            return $this->preorderAmount->getAmountManufacturerWithRefund();
        }

        if ($this->baseByPreorder()) {
            return $this->additionalServiceAmount->getAmountManufacturerWithRefund();
        }

        return null;
    }

    /**
     * @return Money|null
     */
    public function getAmountPaymentMethodFee(): ?Money
    {
        return $this->getAmountByType(self::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE);
    }

    public function getAmountPsFee(): ?Money
    {
        return $this->getAmountByType(self::ACCOUNTING_TYPE_PS_FEE);
    }

    /**
     * This is fee in treatstock fee
     *
     * @return Money|null
     */
    public function getAffiliateFeeIn(): ?Money
    {
        return $this->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_AFFILIATE_FEE_IN);
    }

    /**
     * This is fee additional to total price cost
     *
     * @return Money|null
     */
    public function getAffiliateExclusive(): ?Money
    {
        return $this->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_AFFILIATE_FEE_OUT);
    }

    /**
     * @param $type
     *
     * @return Money|null
     */
    public function getItemsSubTotalByType($type): ?Money
    {
        if (array_key_exists(static::ACCOUNTING_TYPE_ITEMS, $this->accounting)) {
            $cost     = 0;
            $nullData = true;

            foreach ($this->accounting[static::ACCOUNTING_TYPE_ITEMS] as $item) {
                if (isset($item[$type]) && $item[$type]['price']) {
                    $cost     += $item[$type]['price'];
                    $nullData = false;
                }
            }

            if (!$nullData) {
                return Money::create($cost, $this->currency);
            }
        }

        return null;
    }

    /**
     * @param $type
     * @param $invoiceItemUuid
     *
     * @return Money|null
     */
    public function getItemsSubTotalByTypeAndUuid($type, $invoiceItemUuid): ?Money
    {
        if (
            !array_key_exists(static::ACCOUNTING_TYPE_ITEMS, $this->accounting)
            || !array_key_exists($invoiceItemUuid, $this->accounting[static::ACCOUNTING_TYPE_ITEMS])
            || !array_key_exists($type, $this->accounting[static::ACCOUNTING_TYPE_ITEMS][$invoiceItemUuid])
            || !array_key_exists('price', $this->accounting[static::ACCOUNTING_TYPE_ITEMS][$invoiceItemUuid][$type])
        ) {
            return null;
        }

        return Money::create($this->accounting[static::ACCOUNTING_TYPE_ITEMS][$invoiceItemUuid][$type]['price'], $this->currency);
    }

    /**
     * @return Money|null
     */
    public function getAmountInstantPaymentFeeIn(): ?Money
    {
        return Money::create($this->accounting[static::ACCOUNTING_TYPE_INSTANT_PAYMENT_FEE_IN]['price'] ?? 0, $this->currency);
    }

    public function getAmountInstantPaymentFeeOut(): ?Money
    {
        return Money::create($this->accounting[static::ACCOUNTING_TYPE_INSTANT_PAYMENT_FEE_OUT]['price'] ?? 0, $this->currency);
    }

    /**
     * @return Money|null
     */
    public function getAmountInstantPaymentAward(): ?Money
    {
        $total = $this->accounting[static::ACCOUNTING_TYPE_INSTANT_PAYMENT]['price'];
        $feeIn = $this->accounting[static::ACCOUNTING_TYPE_INSTANT_PAYMENT_FEE_IN]['price'] ?? 0;
        return Money::create($total - $feeIn, $this->currency);
    }

    /**
     * @param $status
     */
    public function setNewStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isSettleSubmit(): bool
    {
        return (bool)$this->is_settle_submit;
    }

    /**
     * @return Payment|null
     */
    public function getActivePayment(): ?Payment
    {
        $payments = $this->payments;
        return (\is_array($payments) && $payments) ? reset($payments) : null;
    }

    public function getActivePaymentFirstTransaction(): ?PaymentTransaction
    {
        foreach ($this->payments as $payment) {
            if ($transactions = $payment->vendorTransactions) {
                $firstTransaction = reset($transactions);
                return $firstTransaction;
            }
        }
        return null;
    }

    /**
     * @return bool|string
     */
    public function transactionsFindBillInfo()
    {
        if ($transaction = $this->getActivePaymentFirstTransaction()) {
            return $transaction->findBillInfo();
        }
        return false;
    }

    /**
     * The invoice was created based on preorder
     *
     * @return bool
     */
    public function baseByPreorder(): bool
    {
        return (bool)$this->preorder_id;
    }

    /**
     * The invoice was created based on store_order_position
     *
     * @return bool
     */
    public function baseByAdditionalService(): bool
    {
        return (bool)$this->order_position_id;
    }

    /**
     * Instant payment
     *
     * @return bool
     */
    public function baseByInstantPayment(): bool
    {
        return (bool)$this->instant_payment_uuid;
    }

    /**
     * TsInternalPurchase
     *
     * @return bool
     */
    public function baseByTsInternalPurchase(): bool
    {
        return (bool)$this->ts_internal_purchase_uid;
    }

    /**
     * The invoice was created based on store_order
     *
     * @return bool
     */
    public function baseByPrintOrder(): bool
    {
        return $this->store_order_id && $this->storeOrder->hasModel() && !$this->storeOrder->hasCuttingPack() && !$this->baseByAdditionalService() && !$this->preorder;
    }

    public function baseByCuttingOrder(): bool
    {
        return $this->store_order_id && $this->storeOrder->hasCuttingPack();
    }

    /**
     * @return bool
     */
    public function getAllowPromoCode(): bool
    {
        return $this->storeOrder && $this->storeOrder->hasModel() && !$this->getAmountBonus();
    }

    /**
     * @return bool
     */
    public function hasPromoCode(): bool
    {
        return (bool)$this->storeOrderPromocode;
    }

    /**
     * there is a promotional code and it is used
     *
     * @return bool
     */
    public function hasUsedPromoCode(): bool
    {
        return (bool)$this->promocodeUsage;
    }

    /**
     * @return null|string
     */
    public function getRefundComment(): ?string
    {
        $refunds = $this->getPaymentTransactionRefunds()->approved()->all();

        if ($refunds) {
            return implode(',', ArrayHelper::getColumn($refunds, 'comment'));
        }

        return null;
    }

    /**
     * @throws UserException
     */
    public function setPaymentInProgress()
    {
        $isUpdated = PaymentInvoice::updateRow(
            [
                'and',
                [
                    'or',
                    ['<', 'payment_invoice.start_pay_progress_date', DateHelper::subNowSec(10)],
                    ['payment_invoice.start_pay_progress_date' => null]

                ],
                ['uuid' => $this->uuid]
            ],
            [
                'payment_invoice.start_pay_progress_date' => DateHelper::now()
            ]);
        if (!$isUpdated) {
            throw new BusinessException(_t('site.payment', 'Sorry, previous payment transaction in progress'));
        }
    }

    /**
     * @throws BusinessException
     */
    public function setPrimaryInvoiceForRelatedObjects(): void
    {
        if ($this->baseByAdditionalService()) {
            $this->orderPosition->primary_payment_invoice_uuid = $this->uuid;
            $this->orderPosition->safeSave(['primary_payment_invoice_uuid']);
        } elseif ($this->baseByPrintOrder() || $this->baseByCuttingOrder() || $this->baseByPreorder()) {
            if ($this->storeOrder) {
                $this->storeOrder->primary_payment_invoice_uuid = $this->uuid;
                $this->storeOrder->safeSave(['primary_payment_invoice_uuid']);
            }

            if ($this->preorder) {
                $this->preorder->primary_payment_invoice_uuid = $this->uuid;
                $this->preorder->safeSave(['primary_payment_invoice_uuid']);
            }
        }
    }

    /**
     * @param $method
     *
     * @return bool
     */
    public function canPaidByMethodType($method, ?User $user = null): bool
    {
        if ($method === PaymentTransaction::VENDOR_TS && (!$user || $this->user_id != $user->id)) {
            return false;
        }
        if ($method === PaymentTransaction::VENDOR_BONUS && (!$user || $this->user_id != $user->id)) {
            return false;
        }
        foreach ($this->paymentInvoicePaymentMethods as $paymentMethod) {
            if ($method === $paymentMethod->vendor || (($method==PaymentTransaction::VENDOR_STRIPE_ALIPAY || $method==PaymentTransaction::VENDOR_STRIPE) && $paymentMethod->vendor===PaymentTransaction::VENDOR_BRAINTREE)) {
                return true;
            }
        }

        return false;
    }

    public function paymentFor(): string
    {
        if ($this->instantPayment) {
            return self::PAY_FOR_INSTANT_PAYMENT;
        }
        if ($this->storeOrderPosition) {
            return self::PAY_FOR_ORDER_POSITION;
        }
        if ($this->tsInternalPurchase) {
            return self::PAY_FOR_TS_INTERNAL_PURCHASE;
        }
        if ($this->preorder) {
            return self::PAY_FOR_QUOTE;
        }
        return self::PAY_FOR_PRINT_MODEL3D;
    }

    /**
     * @return bool
     */
    public function isInvoiceGroup(): bool
    {
        return (bool)$this->invoice_group_uuid;
    }


    public function getStatusText()
    {
        $texts = [
            self::STATUS_NEW        => _t('site.payment', 'Unpaid'),
            self::STATUS_AUTHORIZED => _t('site.payment', 'Authorized'),
            self::STATUS_PAID       => _t('site.payment', 'Paid'),
            self::STATUS_PART_PAID  => _t('site.payment', 'Partial paid'),
            self::STATUS_EXPIRED    => _t('site.payment', 'Expired'),
            self::STATUS_CANCELED   => _t('site.payment', 'Canceled'),
            self::STATUS_VOID       => _t('site.payment', 'Void'),
            self::STATUS_REFUNDED   => _t('site.payment', 'Refunded'),
            self::STATUS_REFUND     => _t('site.payment', 'Refund'),
        ];
        return $texts[$this->status];
    }

    public function getCustomerStatusText()
    {
        $texts = [
            self::STATUS_NEW        => _t('site.payment', 'Unpaid'),
            self::STATUS_AUTHORIZED => _t('site.payment', 'Paid'),
            self::STATUS_PAID       => _t('site.payment', 'Paid'),
            self::STATUS_PART_PAID  => _t('site.payment', 'Partial paid'),
            self::STATUS_EXPIRED    => _t('site.payment', 'Expired'),
            self::STATUS_CANCELED   => _t('site.payment', 'Canceled'),
            self::STATUS_VOID       => _t('site.payment', 'Void'),
            self::STATUS_REFUNDED   => _t('site.payment', 'Refunded'),
            self::STATUS_REFUND     => _t('site.payment', 'Refund'),
        ];
        return $texts[$this->status];
    }

    public function getFeeUuid($prefix = 'T'): string
    {
        return "$this->uuid-{$prefix}";
    }


    /**
     * @return Money|null
     */
    public function getCompanyFee()
    {
        if ($this->baseByPrintOrder()) {
            return $this->storeOrderAmount->getAmountPsFee();
        }

        if ($this->baseByAdditionalService()) {
            return $this->additionalServiceAmount->getAmountFee();
        }

        if ($this->baseByPreorder()) {
            return $this->preorderAmount->getAmountFee();
        }

        return null;
    }

    /**
     * @return Money|null
     */
    public function getManufacturerAward()
    {
        if ($this->baseByPrintOrder()) {
            return $this->storeOrderAmount->getAmountPrintForPs();
        }

        if ($this->baseByCuttingOrder()) {
            return $this->storeOrderAmount->getAmountCuttingForPs();
        }

        if ($this->baseByAdditionalService()) {
            return $this->additionalServiceAmount->getAmountManufacturer();
        }

        if ($this->baseByPreorder()) {
            return $this->preorderAmount->getAmountManufacturer();
        }

        return null;
    }

    /**
     * @param User|\backend\models\user\UserAdmin $user
     * @return bool
     */
    public function checkAccessForUser($user): bool
    {
        if ($user instanceof \backend\models\user\UserAdmin) {
            return true;
        }

        if ($this->user_id === $user->id) {
            return true;
        }

        // Owner of company
        if ($this->company && $this->company->user_id === $user->id) {
            return true;
        }

        // Order creator
        if ($this->storeOrder->user_id === $user->id) {
            return true;
        }

        // Order Performer
        if ($this->storeOrder && $this->storeOrder->currentAttemp && $this->storeOrder->currentAttemp->ps->user_id === $user->id) {
            return true;
        }

        return false;
    }

    public function getLinkedAffiliateAward(): ?AffiliateAward
    {
        $affiliateAward = null;
        if ($this->affiliateAward) {
            $affiliateAward = $this->affiliateAward;
        }
        if ($this->parentInvoice) {
            $affiliateAward = $this->parentInvoice->affiliateAward;
        }
        if ($affiliateAward && $affiliateAward->affiliateSource && $affiliateAward->affiliateSource->user_id == $this->user_id) {
            $affiliateAward = null;
        }
        return $affiliateAward;
    }

    public function resetAccountingBonusAccuredAndSave()
    {
        $this->resetAccountingBonusAccured();
        $this->safeSave(['accounting']);
    }

    public function resetAccountingBonusAccured()
    {
        $accounting = $this->accounting;
        if (!empty($accounting[self::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS])) {
            unset($accounting[self::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS]);
            $this->accounting = $accounting;
        }
        if (!empty($accounting[self::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS])) {
            unset($accounting[self::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS]);
            $this->accounting = $accounting;
        }
    }
}