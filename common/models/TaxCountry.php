<?php namespace common\models;

use Yii;

/**
 * Tax information based on country
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class TaxCountry extends \common\models\base\TaxCountry
{

    /**
     * print service tax rate
     * @var string
     */
    const RATE_PS = 'rate_ps';
    
    /**
     * model3d author tax rate
     * @var string
     */
    const RATE_MODEL = 'rate_model';

    /**
     * default tax rates, if country not found
     *
     * @param bool $type
     * @return array
     */
    public static function getDefaultRate($type = false)
    {
        $rates = [
            self::RATE_PS => 30,
            self::RATE_MODEL => 30
        ];
        return $type ? $rates[$type] : $rates;
    }

    /**
     * get tax rates for USA
     * 
     * @return array
     */
    public static function getUsaRate()
    {
        return [
            self::RATE_PS => 0,
            self::RATE_MODEL => 0
        ];
    }
    
    /**
     * get rates title to display in user profile
     * 
     * @return array
     */
    public static function getRateTitles()
    {
        return [
            self::RATE_PS => _t('site.tax', 'Print Service Tax Rate'),
            self::RATE_MODEL => _t('site.tax', '3D Model author Tax Rate'),
        ];
    }
    
    /**
     * 
     * @param array $rates
     * @return string
     */
    public static function getRatesAsHtml($rates)
    {
        $result = [];
        $titles = self::getRateTitles();
        
        foreach($rates as $rateType=>$rate)
        {
            if(in_array($rateType, array_keys($titles))){                
                
                $result[] = $titles[$rateType] . ' ' .$rate . '%';
            }
        }
        
        return implode("<br />", $result);
    }
}
