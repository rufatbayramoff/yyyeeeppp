<?php

namespace common\models;

use common\components\DateHelper;
use yii\base\BaseObject;

/**
 * Class PaymentPayPageLogProcess
 * @package common\models
 */
class PaymentPayPageLogProcess extends \common\models\base\PaymentPayPageLogProcess
{
    public static function logProcess($uuid, $data)
    {
        $logProccess                  = new PaymentPayPageLogProcess();
        $logProccess->uuid            = $uuid;
        $logProccess->process_err     = '';
        $logProccess->process_payload = '';
        $logProccess->date = DateHelper::now();
        $logProccess->load($data);
        $logProccess->safeSave();

    }

    public function load($data, $formName = null)
    {
        $this->process_payload = json_decode($data['payload'] ?? '', true) ?? '';
        $this->process_err     = json_decode($data['err'] ?? '', true) ?? '';
        $this->process_message = $data['errorText'] ?? '';
    }
}