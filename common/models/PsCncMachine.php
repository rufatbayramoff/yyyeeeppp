<?php

namespace common\models;

use common\modules\cnc\factories\PsCncFactory;
use common\modules\cnc\models\PsCnc;
use Yii;

/**
 * Class PsCncMachine
 *
 * @package common\models
 */
class PsCncMachine extends \common\models\base\PsCncMachine
{
    /** @var  PsCnc */
    public $psCnc;

    public function setPsMachine(CompanyService $psMachine)
    {
        $this->populateRelation('companyService', $psMachine);
    }

    public function populate($data)
    {
        Yii::$app->getModule('cnc')->psCncPopulator->populate($this->psCnc, $data);
    }

    public function getPsMachineId()
    {
        return $this->companyService->id;
    }

    public function getPsCncDescription()
    {
        if ($this->psCnc) {
            return $this->psCnc;
        }
        $psCncFactory = Yii::createObject(PsCncFactory::class);
        $description = $this->description;
        $this->psCnc = $psCncFactory->createPsCnc($this->companyService->ps, $description);
        return $this->psCnc;
    }

    public function isAllowedWidget()
    {
        return true;
    }

    public function resolveMachineUri($url = 'www/cloudcam/services/data',$ext = 'json'): string
    {
        return "{$url}/cnc_machine_{$this->getPsMachineId()}.{$ext}";
    }
}