<?php

namespace common\models;

use common\interfaces\ModelHistoryInterface;
use common\traits\ModelHistoryARTrait;

/**
 * Class ProductHistory
 * @package common\models
 */
class ProductHistory extends \common\models\base\ProductHistory implements ModelHistoryInterface
{
    use ModelHistoryARTrait;


    public function setModelId($id)
    {
        $this->setAttribute('model_uuid', $id);
    }

    public function getModelId()
    {
        return $this->getAttribute('model_uuid');
    }
}