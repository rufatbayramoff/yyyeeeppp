<?php namespace common\models;

use Yii;

/**
 * 
 * This is the model class for table "user_log".
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UserLog extends \common\models\base\UserLog
{

    /**
     * log user self updates (email, profile and etc)
     * 
     * @param int $userId
     * @param string $field
     * @param mixed $oldValue
     * @param mixed $newValue
     */
    public static function log($userId, $field, $oldValue, $newValue)
    {
        $userLogObj = new UserLog();
        $userLogObj->user_id = $userId;
        $userLogObj->created_at = new \yii\db\Expression('NOW()');
        $userLogObj->field = $field;
        $userLogObj->old_value = (string)$oldValue;
        $userLogObj->new_value = (string)$newValue;
        $userLogObj->safeSave();
        if($userLogObj->hasErrors()){
            \Yii::error(\yii\helpers\Html::errorSummary($userLogObj));
        }
    }
}
