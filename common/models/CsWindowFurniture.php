<?php

namespace common\models;

/**
 * Class CsWindowFurniture
 * @package common\models
 */
class CsWindowFurniture extends \common\models\base\CsWindowFurniture
{
    public $isDeleted = false;
}