<?php

namespace common\models;

/**
 * Class WikiMaterialIndustry
 * @package common\models
 */
class WikiMaterialIndustry extends \common\models\base\WikiMaterialIndustry
{
    public function setWikiMaterial($wikiMaterial)
    {
        $this->populateRelation('wikiMaterial', $wikiMaterial);
        $this->wiki_material_id = $wikiMaterial->id;
    }
}