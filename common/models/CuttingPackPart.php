<?php

namespace common\models;

use common\components\ps\locator\Size;
use common\modules\cutting\models\CutSize;

/**
 * Class CuttingPackPart
 * @package common\models
 */
class CuttingPackPart extends \common\models\base\CuttingPackPart
{
    public $forDelete = 0;
    public const DEFAULT_DENSITY = 650 / 1000000000; // plywood density

    public function getPreviewUrl()
    {
        return $this->image ?? CuttingPackFile::TYPE_MODEL_PREVIEW_STUB;
    }

    public function getImageType()
    {
        if (strpos($this->image, 'data:image/svg+xml;base64,') === 0) {
            return 'image/svg+xml';
        }
        return 'image';
    }

    public function getImageContent()
    {
        if (!$this->image) {
            return '';
        }
        if (strpos($this->image, 'data:image/svg+xml;base64,') === 0) {
            return base64_decode(substr($this->image, 26));
        }
        if (strpos($this->image, 'data:image/png;base64,') === 0) {
            return base64_decode(substr($this->image, 22));
        }
        return '';
    }

    public function getTitle()
    {
        return $this->cuttingPackPage->cuttingPackFile->getTitle() . ' - ' . $this->cuttingPackPage->cuttingPackFile->getPartNum($this);
    }

    public function getWeight()
    {
        $weight = $this->width * $this->height * $this->calcThickness() * self::DEFAULT_DENSITY / 1000; // 100 - MAGIC
        return $weight;
    }

    public function calcThickness()
    {
        return $this->thickness ? $this->thickness : $this->cuttingPackPage->cuttingPackFile->cuttingPack->thickness;
    }

    public function calcMaterial(): ?CuttingMaterial
    {
        return $this->material ? $this->material : $this->cuttingPackPage->cuttingPackFile->cuttingPack->material;
    }

    public function calcColor(): ?PrinterColor
    {
        return $this->color ? $this->color : $this->cuttingPackPage->cuttingPackFile->cuttingPack->color;
    }

    public function getSize()
    {
        return CutSize::create($this->width, $this->height, $this->thickness ? $this->thickness : $this->cuttingPackPage->cuttingPackFile->cuttingPack->thickness);
    }

    public function getArea()
    {
        return $this->width * $this->height;
    }
}