<?php

namespace common\models;

use Yii;

/**
 * Class CuttingMaterial
 *
 * @property \common\models\PrinterColor[] $cuttingColors
 * @package common\models
 */
class CuttingMaterial extends \common\models\base\CuttingMaterial
{
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'default_thickness' => Yii::t('app', 'Default Thickness (mm)'),
            'default_width' => Yii::t('app', 'Default Width (mm)'),
            'default_length' => Yii::t('app', 'Default Length (mm)'),
        ]);
    }

    /**
     * @return \common\components\BaseActiveQuery
     */
    public function getCuttingColors()
    {
        return $this->hasMany(\common\models\PrinterColor::class, ['id' => 'cutting_color_id'])->via('cuttingMaterialColors');
    }

}