<?php

namespace common\models;

/**
 * Class HttpRequestLog
 * @package common\models
 */
class HttpRequestLog extends \common\models\base\HttpRequestLog
{
    CONST REQUEST_TYPE_API = 'api';
    CONST REQUEST_TYPE_THINGIVERSE_HOOK = 'thingiverseHook';
    CONST REQUEST_TYPE_RESTORE_SESSION = 'restoreSession';
    CONST REQUEST_TYPE_COMMON = 'common';
    CONST REQUEST_TYPE_COMMON_AJAX = 'commonAjax';
    CONST REQUEST_TYPE_BACKEND = 'backend';

    const DELIMETER_MARKER = "^\n^";
}