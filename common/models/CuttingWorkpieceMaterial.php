<?php

namespace common\models;

use common\components\ps\locator\Size;
use common\modules\cutting\models\CutSize;
use Yii;

/**
 * Class CuttingWorkpieceMaterial
 * @package common\models
 */
class CuttingWorkpieceMaterial extends \common\models\base\CuttingWorkpieceMaterial
{
    public $forDelete = 0;

    public function attributeLabels()
    {
        return [
            'uuid'        => Yii::t('app', 'Uuid'),
            'company_id'  => Yii::t('app', 'Company'),
            'material_id' => Yii::t('app', 'Material'),
            'color_id'    => Yii::t('app', 'Color'),
            'width'       => Yii::t('app', 'Width'),
            'height'      => Yii::t('app', 'Height'),
            'thickness'   => Yii::t('app', 'Thickness'),
            'price'       => Yii::t('app', 'Price'),
        ];
    }

    public function getWorkSize(): CutSize
    {
        return CutSize::create($this->width, $this->height, $this->thickness);
    }

    public function getProcessingTitle()
    {
        return $this->material->title . ' Thickness ' . $this->thickness . 'mm';
    }

    public function getArea()
    {
        return $this->width * $this->height;
    }
}