<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.04.17
 * Time: 14:45
 */

namespace common\components;

use yii\base\Module;

abstract class BaseModule extends Module
{
    /**
     * @return array
     */
    abstract protected function getComponentsList();


    protected function initComponentsModule($componentsList)
    {
        foreach ($componentsList as $componentName => $componentClass) {
            $this->$componentName->setModule($this);
        }
    }

    /**
     * Init module config
     *
     * @throws \yii\base\InvalidParamException
     */
    protected function initConfig()
    {
    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();

        $this->initConfig();
        $componentsList = $this->getComponentsList();
        $this->setComponents($componentsList);
        $this->initComponentsModule($componentsList);
    }
}