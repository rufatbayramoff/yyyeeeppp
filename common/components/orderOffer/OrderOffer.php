<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.12.17
 * Time: 9:55
 */

namespace common\components\orderOffer;

use common\components\helpers\PeliabilityHelper;
use common\components\helpers\ResponseTimeHelper;
use common\models\PsPrinter;
use lib\money\Money;

class OrderOffer
{
    public const PRINTER_TYPE_MANUAL_SETTED = 'manualSetted';
    public const PRINTER_TYPE_USER          = 'user';
    public const PRINTER_TYPE_CLOSEST       = 'closest';
    public const PRINTER_TYPE_MAIN          = 'main';
    public const PRINTER_TYPE_USER_LAST     = 'userLastPrinter';



    /**
     * @var PsPrinter
     */
    protected $protectedPrinter;

    /** @var string */
    public $printerItemType;

    /** @var Money */
    public $anonimysPrice;

    /** @var Money */
    public $userPrice;

    /** @var Money */
    public $anonymousPrintPrice;

    /** @var float */
    public $tsBonusAmount = 0;

    /** @var int */
    protected $protectedPrinterId = null;

    // DELIVERY INFO

    /**
     * @var Money
     */
    public $estimateDeliveryCost;

    /**
     * @var Money
     */
    public $packingPrice;

    /**
     * @var array
     */
    public $deliveryTypes = [];


    /**
     * @return PsPrinter
     */
    public function getPrinter()
    {
        if ((!$this->protectedPrinter) && $this->protectedPrinterId) {
            $this->protectedPrinter = PsPrinter::findByPk($this->protectedPrinterId);
        }
        return $this->protectedPrinter;
    }

    public function hasPickupOnly()
    {
        return $this->protectedPrinter->companyService->onlyPickup();
    }

    public function getPrinterId()
    {
        return $this->protectedPrinterId ? $this->protectedPrinterId : $this->protectedPrinter->id;
    }

    public function setPrinter(PsPrinter $printer): void
    {
        $this->protectedPrinter = $printer;
    }

    public function setPrinterId(int $printerId): void
    {
        $this->protectedPrinterId = $printerId;
    }

    public function isAnonymousItem()
    {
        if ($this->printerItemType == self::PRINTER_TYPE_MAIN) {
            return true;
        }
        return false;
    }

    public function peliabilityFormat(): string
    {
        $conversion = $this->getPrinter()?->company?->psCatalog?->conversion();
        return PeliabilityHelper::format($conversion);
    }

    /**
     * @return string|null
     */
    public function responseTimeFormat(): ?string
    {
        $responseTime = $this->getPrinter()?->company?->response_time;
        return ResponseTimeHelper::response($responseTime);
    }

    public function certificatedMachine()
    {
        return $this->getPrinter()->company->getCompanyServices()->certification()->count();
    }
}