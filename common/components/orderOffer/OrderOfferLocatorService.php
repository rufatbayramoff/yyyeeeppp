<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.12.17
 * Time: 9:38
 */

namespace common\components\orderOffer;

use common\components\ArrayHelper;
use common\components\order\PriceCalculator;
use common\components\ps\locator\filter\FilterProcessor;
use common\components\ps\locator\GeoRadius;
use common\components\ps\locator\materials\MaterialColorItem;
use common\components\ps\locator\materials\MaterialColorsItem;
use common\components\ps\locator\PrinterRepository;
use common\interfaces\Model3dBaseInterface;
use common\models\AffiliateSource;
use common\models\DeliveryType;
use common\models\factories\LocationFactory;
use common\models\factories\Model3dTextureFactory;
use common\models\GeoCountry;
use common\models\Model3dTexture;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\models\repositories\PrinterMaterialRepository;
use common\models\User;
use common\modules\payment\components\BonusCalculator;
use common\modules\payment\fee\FeeHelper;
use common\modules\printersList\models\AnswerColorInfo;
use common\modules\printersList\models\AnswerInfo;
use common\modules\printersList\models\RequestInfo;
use common\modules\printersList\services\RequestInfoService;
use common\modules\psPrinterListMaps\components\FilepageVolumeRepository;
use common\services\Model3dService;
use common\services\PrinterMaterialService;
use frontend\models\serialize\Model3dTextureSerializer;
use lib\delivery\delivery\DeliveryService;
use lib\delivery\parcel\Parcel;
use lib\delivery\parcel\ParcelFactory;
use lib\geo\models\Location;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use Psr\Log\InvalidArgumentException;
use Yii;
use yii\base\BaseObject;

class OrderOfferLocatorService
{
    /** @var User */
    public $user;

    /** @var Location */
    public $location;

    /** @var bool */
    public $onlyCertificated;

    /** @var string[] */
    public $currencies = [Currency::USD];

    /**
     * @var bool
     */
    public $onlyInternational = false;


    /** @var int */
    public $manualSettedPsId;

    /** @var int */
    public $manualSettedPsPrinterId;

    /** @var string */
    public $sortMode = RequestInfo::SORT_STRATEGY_DEFAULT;

    /**
     *  You can set only one material for allowed colors
     *
     * @var PrinterMaterial[]
     */
    public $allowedOnlyMaterials = [];

    /**
     *  All possible materials
     *
     * @var array PrinterMaterial
     */
    public $allPossibleMaterials = [];

    /**
     * Common list
     *
     * @var OrderOffer[]
     */
    public $orderOfferItems = [];

    /**
     * Old usage printers, own printers list
     *
     * @var OrderOffer[]
     */
    public $topOrderOfferItems = [];

    /**
     * Manual setted printers list
     *
     * @var OrderOffer[]
     */
    public $manualSettedOrderOfferItems = [];

    /**
     * @var MaterialColorItem[]
     */
    public $availableColorGroupsForCurrentPrintersList = [];

    /**
     * Array of available colors for materials
     *
     * @var array
     */
    public $availableColorsMaterials = [];

    /** @var string */
    public $emptyPrintersListReason;

    /** @var string */
    public $affiliateOrderType;


    // DI


    /** @var FilepageVolumeRepository */
    public $filepageWeightRepository;

    /** @var RequestInfoService */
    public $requestInfoService;

    /** @var PrinterMaterialRepository */
    public $printerMaterialRepository;

    /** @var PrinterMaterialGroupRepository */
    public $printerMaterialGroupRepository;

    /** @var PrinterColorRepository */
    public $printerColorRepository;

    /** @var PrinterRepository */
    public $printerRepository;

    /** @var DeliveryService */
    public $deliveryService;

    /**
     * @var FeeHelper
     */
    private $feeHelper;

    public function injectDependencies(
        FilepageVolumeRepository $filepageWeightRepository,
        RequestInfoService $requestInfoService,
        PrinterMaterialGroupRepository $printerMaterialGroupRepository,
        PrinterColorRepository $printerColorRepository,
        PrinterRepository $printerRepository,
        DeliveryService $deliveryService,
        PrinterMaterialRepository $printerMaterialRepository,
        FeeHelper $feeHelper
    ): void
    {
        $this->filepageWeightRepository       = $filepageWeightRepository;
        $this->requestInfoService             = $requestInfoService;
        $this->printerMaterialGroupRepository = $printerMaterialGroupRepository;
        $this->printerColorRepository         = $printerColorRepository;
        $this->printerRepository              = $printerRepository;
        $this->deliveryService                = $deliveryService;
        $this->printerMaterialRepository      = $printerMaterialRepository;
        $this->feeHelper                      = $feeHelper;
    }


    /**
     * @param User|null $user
     * @param Location $location
     * @param array $topOrderOfferItems
     * @param bool $onlyCertificated
     */
    public function initPrintersBundle($user, Location $location, array $topOrderOfferItems = [], bool $onlyCertificated = false)
    {
        $this->user               = $user;
        $this->topOrderOfferItems = $topOrderOfferItems;
        $this->initAnonymousBundle($location, $onlyCertificated);
    }

    /**
     * @param Location|null $location
     * @param bool $onlyCertificated
     */
    public function initAnonymousBundle($location, $onlyCertificated = false)
    {
        // Add puerto rico country fix change
        $this->location         = LocationFactory::createFromLocation($location);
        $this->onlyCertificated = $onlyCertificated;
    }

    /**
     * @param $affiliateOrderType
     */
    public function setAffiliateOrderType($affiliateOrderType)
    {
        $this->affiliateOrderType = $affiliateOrderType;
    }

    /**
     * @return OrderOffer[]
     */
    public function getAllOffersList()
    {
        $manualList = $this->manualSettedOrderOfferItems;
        $topList    = $this->topOrderOfferItems;
        $list       = $this->orderOfferItems;
        if (param('printers_limit_count')) {
            $list = array_slice($this->orderOfferItems, 0, param('printers_limit_count'));
        }
        return array_merge($manualList, $topList, $list);
    }

    /**
     * @param int $start
     * @param int $count
     * @return OrderOffer[]
     */
    public function getPaginationList($start = 0, $count = 999999)
    {
        $allList = $this->getAllOffersList();
        $result  = [];
        $i       = 0;
        foreach ($allList as $key => $item) {
            if ($i >= ($start + $count)) {
                break;
            }

            if ($i >= $start) {
                $result[$key] = $item;
            }
            $i++;
        }
        return $result;
    }

    /**
     * @param PrinterMaterial[] $allowedMaterials
     */
    public function setAllowedMaterials($allowedMaterials): void
    {
        $this->allowedOnlyMaterials = $allowedMaterials;
    }

    /**
     * @param $currency string
     */
    public function setCurrency($currency): void
    {
        $this->currencies = [$currency];
    }

    /**
     * @param $sortMode string
     */
    public function setSortMode($sortMode): void
    {
        $this->sortMode = $sortMode;
    }

    public function fixGamebodyResin(?AffiliateSource $affiliateSource)
    {
        if ($affiliateSource && $affiliateSource->uuid === 'xIjzuI4') {
            $this->allowedOnlyMaterials = PrinterMaterialGroup::tryFind(['code' => 'Resin'])->printerMaterials;
        }
    }

    /**
     * @param $psPrinter
     * @param $model3d
     * @return OrderOffer
     */
    protected function formOrderOfferByPsPrinter(PsPrinter $psPrinter, Model3dBaseInterface $model3d, bool $directServiceSelect = false): OrderOffer
    {
        $orderOffer = new OrderOffer();
        $orderOffer->setPrinter($psPrinter);
        $originalTextureInfo = PrinterMaterialService::serializeModel3dTexture($model3d);
        Model3dService::setCheapestMaterialInPrinter($model3d, $psPrinter);
        $orderOffer->anonymousPrintPrice = PriceCalculator::calculateModel3dPrintPriceWithPackageFee($model3d, $psPrinter, true, true, $this->onlyInternational);
        if ($directServiceSelect) {
            $orderOffer->printerItemType = OrderOffer::PRINTER_TYPE_MANUAL_SETTED;
            $orderOffer->tsBonusAmount   = BonusCalculator::calcInstantOrderDirectService($psPrinter->company, $orderOffer->anonymousPrintPrice->getAmount(), $this->user);
        }

        PrinterMaterialService::unSerializeModel3dTexture($model3d, $originalTextureInfo);
        return $orderOffer;
    }

    /**
     * @param PsPrinter $psPrinter
     * @param $materialsColors
     *
     * @return bool
     */
    public function formMaterialGroupColors(PsPrinter $psPrinter, &$materialsColors): bool
    {
        $wasAllowedMaterials = false;
        foreach ($psPrinter->psPrinterMaterials as $psPrinterMaterial) {
            $printerMaterial = $psPrinterMaterial->material;
            $materialGroup   = $printerMaterial->group;
            if (!$materialGroup || empty($materialGroup->is_active)) {
                continue;
            }
            $this->allPossibleMaterials[$printerMaterial->id] = $printerMaterial;

            if ($this->allowedOnlyMaterials && !array_key_exists($printerMaterial->id, $this->allowedOnlyMaterials)) {
                continue;
            }
            $wasAllowedMaterials = true;

            foreach ($psPrinterMaterial->colors as $psPrinterColor) {
                $materialColorsItem             = new MaterialColorItem();
                $materialColorsItem->colorId    = $psPrinterColor->color_id;
                $materialColorsItem->rating     = 10; // Magical number, can set any number  more 1
                $materialColorsItem->materialId = $psPrinterMaterial->material->id;
                $materialKey                    = $materialColorsItem->materialId . '_' . $materialColorsItem->colorId;
                $materialsColors[$materialKey]  = $materialColorsItem;

                $materialColorsItem             = new MaterialColorItem();
                $materialColorsItem->colorId    = $psPrinterColor->color_id;
                $materialColorsItem->rating     = 10; // Magical number, can set any number  more 1
                $materialColorsItem->groupId    = $psPrinterMaterial->material->group_id;
                $materialColorsItem->materialId = null;
                $groupKey                       = 'g' . $psPrinterMaterial->material->group_id . '_' . $materialColorsItem->colorId;
                $materialsColors[$groupKey]     = $materialColorsItem;
            }
        }
        return $wasAllowedMaterials;
    }

    /**
     * @param $model3d
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \yii\web\NotFoundHttpException
     */
    public function formManualSettedPs(Model3dBaseInterface $model3d): void
    {
        /** @var MaterialColorItem[] $materialsColors */
        $materialsColors            = [];
        $this->allPossibleMaterials = [];

        if ($this->manualSettedPsPrinterId) {
            $printer = PsPrinter::tryFindByPk($this->manualSettedPsPrinterId);
            if (!$printer->isAvailable()) {
                throw new InvalidArgumentException('Printer is not availible');
            }
            $printers[] = $printer;
        } else {
            $ps       = Ps::tryFindByPk($this->manualSettedPsId);
            $printers = $ps->getNotDeletedPrinters()->visible()->all();
        }

        $allowedTextures = PrinterMaterialService::getModelTextures($model3d);

        if (!$printers) {
            $this->emptyPrintersListReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_NO_ACTIVE_MACHINES;
            return;
        }

        $printers = FilterProcessor::filterBySize($printers, $model3d->getSize());

        if (!$printers) {
            $this->emptyPrintersListReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_LARGE_SIZE;
            return;
        }

        $hasDeliveryToCustomerCountry = false;

        /** @var PsPrinter $printer */
        foreach ($printers as $printer) {
            if (!$printer->isCanPrintInStore()) {
                continue;
            }

            // Bug #5872 - the list only for the specified country
            if ($this->checkPrinterDeliveryUserLocation($printer)) {
                continue;
            }
            $hasDeliveryToCustomerCountry = true;

            $wasAllowedMaterials = $this->formMaterialGroupColors($printer, $materialsColors);

            if (!$wasAllowedMaterials) {
                continue;
            }

            if ($printer->canPrintTextures($allowedTextures)) {
                $orderOffer                                         = $this->formOrderOfferByPsPrinter($printer, $model3d, true);
                $this->orderOfferItems[$orderOffer->getPrinterId()] = $orderOffer;
            }
        }

        if (!$hasDeliveryToCustomerCountry) {
            $this->emptyPrintersListReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_NO_DELIVERY;
            return;
        }

        if (!$this->orderOfferItems && !$model3d->isAnyTextureAllowed()) {
            $this->emptyPrintersListReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_NO_TEXTURE;
            return;
        }

        if (!$this->orderOfferItems && $materialsColors && $model3d->isAnyTextureAllowed()) {
            $materialColor = reset($materialsColors);
            $color         = $materialColor->getColor();
            if ($color) {
                $group = $materialColor->getGroup();
                if (!$group) {
                    $group = $materialColor->getMaterial()->group;
                }
                $resetTexture = Model3dTextureFactory::createTexture($group, $materialColor->getMaterial(), $color);
                $model3d->setKitTexture($resetTexture);
                foreach ($printers as $printer) {
                    // Bug #5872 - the list only for the specified country
                    if ($this->checkPrinterDeliveryUserLocation($printer)) {
                        continue;
                    }

                    if ($printer->canPrintTextures([$resetTexture])) {
                        $orderOffer                                         = $this->formOrderOfferByPsPrinter($printer, $model3d, true);
                        $this->orderOfferItems[$orderOffer->getPrinterId()] = $orderOffer;
                    }
                }
            }
        }

        $this->availableColorGroupsForCurrentPrintersList = $materialsColors;

        if (!$this->orderOfferItems) {
            $this->emptyPrintersListReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_NO_ACTIVE_MACHINES;
        }
    }

    /**
     * Checking whether the current printer can deliver to the user location
     *
     * @param PsPrinter $printer
     *
     * @return bool
     */
    protected function checkPrinterDeliveryUserLocation(PsPrinter $printer): bool
    {
        return $this->location &&
            $printer?->companyService?->location?->country_id !== $this->location->geoCountryObjectCache->id &&
            !$printer->companyService->hasInternationalDelivery();
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param Model3dTexture[] $allowedTextures
     * @param $materialsColors
     */
    public function formUserPrintersList(Model3dBaseInterface $model3d, $allowedTextures, &$materialsColors)
    {
        /** @var PsPrinter $userLastPrinters */
        $userLastPrinters = $this->printerRepository->getLastUserPrintersQuery($this->user)->all();

        $userLastPrinters = FilterProcessor::filterBySize($userLastPrinters, $model3d->getSize());
        $wasSetted        = false;

        foreach ($userLastPrinters as $userLastPrinter) {
            if ($userLastPrinter->canPrintTextures($allowedTextures) && !$wasSetted) {
                $orderOffer                                         = $this->formOrderOfferByPsPrinter($userLastPrinter, $model3d);
                $orderOffer->printerItemType                        = OrderOffer::PRINTER_TYPE_USER;
                $this->orderOfferItems[$orderOffer->getPrinterId()] = $orderOffer;
                $wasSetted                                          = true;
            }
            $this->formMaterialGroupColors($userLastPrinter, $materialsColors);
        }
    }

    /**
     * Form printers list for nearestLocation
     *
     * @param Model3dBaseInterface $model3d
     * @param Model3dTexture[] $allowedTextures
     * @param $materialsColors
     */
    public function formNearestLocation(Model3dBaseInterface $model3d, $allowedTextures, &$materialsColors)
    {
        if (!$this->location->getCoord()) {
            return;
        }
        // When we try to chane printer, and don`t have coordinates: don`t filter by radius.
        $geoRadius               = GeoRadius::create($this->location->getCoord(), 100);
        $closestPrinters         = $this->printerRepository->getClosestPickupPrinters($this->user, $geoRadius, $this->location->country);
        $filteredClosestPrinters = FilterProcessor::filterPrintersByRadius($closestPrinters, $geoRadius);
        if ($this->onlyCertificated) {
            $filteredClosestPrinters = FilterProcessor::filterOnlyCertificated($closestPrinters);
        }

        $filteredClosestPrinters = FilterProcessor::filterBySize($filteredClosestPrinters, $model3d->getSize());

        foreach ($filteredClosestPrinters as $closestPrinter) {
            if ($closestPrinter->canPrintTextures($allowedTextures)) {
                $offer                                         = $this->formOrderOfferByPsPrinter($closestPrinter, $model3d);
                $offer->printerItemType                        = OrderOffer::PRINTER_TYPE_MAIN;
                $this->orderOfferItems[$offer->getPrinterId()] = $offer;
            }
            $this->formMaterialGroupColors($closestPrinter, $materialsColors);
        }
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @throws \yii\web\NotFoundHttpException
     */
    public function formOffersListForModel(Model3dBaseInterface $model3d)
    {
        if ($this->manualSettedOrderOfferItems) {
            // Manual setted items
            return;
        }

        if ($this->manualSettedPsId || $this->manualSettedPsPrinterId) {
            $this->formManualSettedPs($model3d);
            return;
        }

        $this->emptyPrintersListReason = '';
        $this->orderOfferItems         = [];
        $this->allPossibleMaterials    = [];
        $materialsColors               = [];

        $allowedTextures = PrinterMaterialService::getModelTextures($model3d);

        if ($this->user) {
            $this->formUserPrintersList($model3d, $allowedTextures, $materialsColors);
        }
        $this->formNearestLocation($model3d, $allowedTextures, $materialsColors);

        array_walk($this->currencies, function(&$value) {$value = strtolower($value);});
        $request       = $this->requestInfoService->createRequestForModel3d($model3d, $this->location, $this->onlyCertificated, $this->allowedOnlyMaterials, $this->currencies);
        $request->sort = $this->sortMode;

        if ($this->onlyInternational) {
            $request->onlyInternational = true;
        }

        /** @var AnswerInfo $answerInfo */
        $answerInfo = \Yii::$app->getModule('printersList')->printersListDaemon->sendRequest($request);

        foreach ($answerInfo->offers as $offer) {
            $orderOffer = new OrderOffer();
            $orderOffer->setPrinterId($offer->printerId);
            $orderOffer->anonymousPrintPrice = $offer->price;
            if (BonusCalculator::isAllowBonusForUser($this->user)) {
                $orderOffer->tsBonusAmount = $offer->tsBonusAmount;
            }
            $orderOffer->printerItemType                        = OrderOffer::PRINTER_TYPE_MAIN;
            $this->orderOfferItems[$orderOffer->getPrinterId()] = $orderOffer;
        }


        $materialsColors = [];
        foreach ($answerInfo->allowedMaterialColors as $key => $colorItemInfo) {
            $materialColorItem     = MaterialColorItem::create($colorItemInfo);
            $materialsColors[$key] = $materialColorItem;
        }

        $this->availableColorGroupsForCurrentPrintersList = $materialsColors;

        if ($answerInfo->resetTextureInfo) {
            $this->wasTextureReset = true;
            $materialGroup         = $this->printerMaterialGroupRepository->getById($answerInfo->resetTextureInfo->materialGroupId);
            $color                 = $this->printerColorRepository->tryGetById($answerInfo->resetTextureInfo->materialColorId);
            $texture               = Model3dTextureFactory::createTexture($materialGroup, null, $color);
            $model3d->setKitTexture($texture);
        }

        if (!$this->orderOfferItems) {
            if ($answerInfo->emptyReason) {
                $this->emptyPrintersListReason = $answerInfo->emptyReason;
            }
        }
    }

    /**
     * @param OrderOffer[] $offersList
     * @param Model3dBaseInterface $model3d
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function formCosts($offersList, Model3dBaseInterface $model3d, $skipOwner = false)
    {
        $parcel          = ParcelFactory::createFromModel($model3d);
        $bonusCalculator = \Yii::createObject(BonusCalculator::class);
        foreach ($offersList as $orderOffer) {
            $currency         = $orderOffer->anonymousPrintPrice->getCurrency();
            $model3dAnonymous = $model3d->storeUnit ? PriceCalculator::calculateModel3dPrice($model3d, $currency, true) : Money::zero();
            $model3dPrice     = $model3d->storeUnit ? PriceCalculator::calculateModel3dPrice($model3d, $currency) : Money::zero();
            $printPrice       = $printPriceAnonymous = $orderOffer->anonymousPrintPrice;
            if ($this->user && $orderOffer->getPrinter()->ps->user_id === $this->user->id && !$skipOwner) {
                $printPrice = Money::zero();
            }
            $orderOffer->userPrice     = MoneyMath::sum($model3dPrice, $printPrice);
            $orderOffer->anonimysPrice = MoneyMath::sum($model3dAnonymous, $printPriceAnonymous);

            [$estimateDeliveryCost, $orderOffer->deliveryTypes, $packingPrice] = $this->resolveDeliveryRateAndLabels(
                $orderOffer->getPrinter(),
                $orderOffer->anonymousPrintPrice,
                $this->location,
                $parcel
            );

            $orderOffer->estimateDeliveryCost = $estimateDeliveryCost;
            $orderOffer->packingPrice         = $packingPrice;
        }
    }

    /**
     * @param PsPrinter $printer
     * @param Money $printPrice
     * @param Location $location
     * @param Parcel $parcel
     * @return array
     */
    protected function resolveDeliveryRateAndLabels(PsPrinter $printer, Money $printPrice, Location $location, Parcel $parcel): array
    {
        $estimateRates = $this->deliveryService->getAvaliableRates($printer->companyService->asDeliveryParams(), $printPrice, $location, $parcel);

        $totalPrice   = null;
        $packingPrice = null;
        $labels       = [];

        foreach ($estimateRates as $rate) {
            if ($rate->code === DeliveryType::PICKUP) {
                continue;
            }
            if ($rate->code === DeliveryType::STANDARD || $rate->code === DeliveryType::INTERNATIONAL) {
                if (!$totalPrice) {
                    $totalPrice   = $rate->getDeliveryMoney();
                    $packingPrice = $rate->packingCost;
                }
            }

            $labels[] = $rate->code;
        }

        return [$totalPrice, $labels, $packingPrice];
    }


    public function getVerbMessages()
    {
        $emptyPrintersListReason = $this->emptyPrintersListReason;

        if ($emptyPrintersListReason === AnswerInfo::EMPTY_PRINTERS_LIST_REASON_NO_ACTIVE_PRINTERS) {
            return [
                _t(
                    'front.store',
                    'This print service doesn\'t have any active or published printers on Treatstock.'
                )
            ];
        }
        if ($emptyPrintersListReason === AnswerInfo::EMPTY_PRINTERS_LIST_REASON_LARGE_SIZE) {
            return [
                _t(
                    'front.store',
                    'The dimensions of the model are too large for 3D printing. Please use the scale feature to reduce the size of the model.'
                )
            ];
        }
        if ($emptyPrintersListReason === AnswerInfo::EMPTY_PRINTERS_LIST_REASON_NO_TEXTURE) {
            return [
                _t('site.printers', 'No manufacturers were found with the combination of materials and colors you have selected.')
            ];
        }
        return [
            _t(
                'site.store',
                'This print service does not have any active printers on Treatstock. Please contact the print service using Treatstock messages to calculate the cost of printing your models.'
            )
        ];
    }
}