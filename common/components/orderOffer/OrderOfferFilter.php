<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.01.18
 * Time: 15:44
 */

namespace common\components\orderOffer;

use common\models\DeliveryType;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\StoreOrderDelivery;
use yii\helpers\Json;

class OrderOfferFilter
{

    /**
     * @param OrderOffer[] $orderOffers
     * @param DeliveryType $deliveryType
     * @param StoreOrderDelivery|null $deliveryParams
     * @return OrderOffer[]
     */
    public function filterByDeliveryType($orderOffers, DeliveryType $deliveryType, StoreOrderDelivery $deliveryParams = null)
    {
        $needCarrier = $deliveryParams ? $deliveryParams->ps_delivery_details['carrier'] : null;

        $items = array_filter(
            $orderOffers,
            function (OrderOffer $orderOfferItem) use ($deliveryType, $needCarrier, $deliveryParams) {
                foreach ($orderOfferItem->getPrinter()->psMachineDeliveries as $printerDeliveryType) {
                    if ($printerDeliveryType->deliveryType->code === $deliveryType->code) {

                        if (!$needCarrier) {
                            return true;
                        }

                        if ($deliveryParams) {

                            if ($needCarrier != $printerDeliveryType->carrier) {
                                continue;
                            }

                            if ($deliveryType->code == DeliveryType::INTERNATIONAL
                                && $deliveryParams->order->shipAddress->country->iso_code == $orderOfferItem->getPrinter()->companyService->location->country->iso_code
                            ) {
                                continue;
                            }

                            return true;
                        }
                    }
                }
                return false;
            }
        );
        return $items;
    }

    /**
     * @param array $formattedOffers
     * @return array
     */
    public function filterCanceled($formattedOffers, $hideCancelled): array
    {
        if (!$hideCancelled) {
            return $formattedOffers;
        }
        $items = [];
        foreach ($formattedOffers as $oneOffer) {
            if (!$oneOffer['isCancelled']) {
                $items[] = $oneOffer;
            }
        }
        return $items;
    }
}