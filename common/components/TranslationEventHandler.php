<?php
namespace common\components;

use yii\i18n\MissingTranslationEvent;

/**
 * Helps to identify not translated messages on website
 */
class TranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event) {
        #$event->translatedMessage = "@MISSING: {$event->category}.{$event->message} FOR LANGUAGE {$event->language} @";
        // keep origin for now 
        if (app('session')->get('translateMode', false)) { 
            #$event->translatedMessage = "^^" . $event->category . "|" . $event->message . "^^";
        }
        /*
        $event->translatedMessage = sprintf(
            '<span class="js-translate" data-category="%s" data-lang="%s">%s <span class="tsi tsi-globe"></span></span>', 
            $event->category,
            $event->language,
            $event->message
        );*/
    }
}