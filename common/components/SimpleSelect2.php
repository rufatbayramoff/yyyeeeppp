<?php
namespace common\components;

/**
 * SimpleSelect2 - easy way to get select2 configuration
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class SimpleSelect2
{

    /**
     * 
     * @param \common\components\BaseAR $class
     * @param string $key
     * @param string $value
     * @return array
     */
    public function get(BaseAR $class, $key = 'id', $value = 'title')
    {        
        return [ 
            'data' => \yii\helpers\ArrayHelper::map($class::find()->asArray()->all(),  $key, $value),
            'options' => ['placeholder' => 'Select'],
            'pluginLoading' => true,
            'pluginOptions' => [
                'allowClear' => true
            ], 
        ];
    }

    /**
     *
     * @param string|array $url
     * @param mixed $k
     * @param mixed $initValue
     * @param array $options
     * @return array
     */
    public static function getAjax($url, $k, $initValue = '', $options = [])
    {
        $default =  [
            'initValueText' => $initValue,
            'data' => $initValue,
            'options' => ['placeholder' => 'Search'], 
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 2,
            ],
        ];
        $data = array_merge($default, $options);
        $data['pluginOptions']['ajax'] =   [
            'url' => \yii\helpers\Url::to($url),
            'dataType' => 'json',
            'data' => new \yii\web\JsExpression(
                'function(params) { return {q:params.term, page:params.page, k:"'.$k.'"}; }'
             )
        ];
        return $data;
    }
}
