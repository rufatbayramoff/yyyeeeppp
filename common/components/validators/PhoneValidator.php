<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\validators;


use Closure;
use common\components\exceptions\AssertHelper;
use common\modules\xss\helpers\XssHelper;
use lib\geo\GeoNames;
use yii\base\InvalidConfigException;
use yii\validators\Validator;

/**
 * Validate phone number
 *
 * @package common\components\validators
 */
class PhoneValidator extends Validator
{
    /**
     * @var string|Closure|null
     */
    public $phoneCodeAttribute;

    /**
     * @var string|Closure
     */
    public $phoneCountryIsoAttribute;

    /**
     * Is true this validator will also coorect phone number with code after success validate.
     * Correction means set right value to phoneCodeAttribute and
     *
     * @var bool
     */
    public $correctPhone = false;

    /**
     *
     */
    public function init()
    {
        parent::init();
        AssertHelper::assert($this->phoneCountryIsoAttribute, 'Field phoneCountryIsoAttribute must be set', InvalidConfigException::class);
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        try {
            $number = $model->$attribute;
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            $numberObj = $phoneUtil->parse($number, $this->resolvePhoneCountryIso($model));
            if (!$phoneUtil->isValidNumber($numberObj)) {
                $this->addError($model, $attribute, $this->getMessage());
            }

            if ($this->correctPhone) {
                if ($this->phoneCodeAttribute) {
                    $model->{$this->phoneCodeAttribute} = $numberObj->getCountryCode();
                }
                $model->$attribute = $numberObj->getNationalNumber();
            }
        } catch (\libphonenumber\NumberParseException $e) {
            $this->addError($model, $attribute, $this->getMessage());
        }
    }

    /**
     * @param $model
     * @return mixed
     */
    private function resolvePhoneCountryIso($model)
    {
        return $model->{$this->phoneCountryIsoAttribute};
    }

    /**
     * Return error message
     *
     * @return string
     */
    private function getMessage()
    {
        return $this->message ?: _t('site.user', 'Invalid phone number');
    }
}