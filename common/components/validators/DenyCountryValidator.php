<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\validators;


use common\models\GeoCountry;
use frontend\models\site\DenyCountry;
use yii\validators\Validator;

class DenyCountryValidator extends Validator
{
    /**
     * @param \yii\base\Model $model
     * @param string          $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $denyIso = array_keys(DenyCountry::getDenyCountries());

        if (is_integer($model->$attribute)) {
            $geoCountry = GeoCountry::findByPk($model->$attribute);
            if (!$geoCountry->paypal_payout) {
                $this->addError($model, $attribute, $this->getMessage());
            }
        } else {
            if (in_array($model->$attribute, $denyIso)) {
                $this->addError($model, $attribute, $this->getMessage());
            }
        }
    }

    /**
     * Return error message
     * @return string
     */
    private function getMessage()
    {
        return $this->message ?: _t("site", "Unfortunately, Treatstock payment system does not work in your country due to PayPal restrictions.");
    }
}