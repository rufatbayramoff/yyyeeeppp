<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\validators;


use common\models\base\GeoCountry;
use yii\validators\Validator;

/**
 * Validate that country is code exist
 * @package common\components\validators
 */
class CoutryIsoValidator extends Validator
{
    /**
     * @param \yii\base\Model $model
     * @param string          $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $isoCode = $model->$attribute;
        $isExist = GeoCountry::find()
            ->where(['iso_code' => $isoCode])
            ->exists();

        if(!$isExist)
        {
            $this->addError($model, $attribute, $this->getMessage($isoCode));
        }
    }

    /**
     * Return error message
     * @param string $isoCode
     * @return string
     */
    private function getMessage($isoCode)
    {
        return $this->message ?: _t('site.geo', 'Unknown country ISO code {isoCode}', ['isoCode' => $isoCode]);
    }
}