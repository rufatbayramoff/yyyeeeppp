<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.06.16
 * Time: 18:23
 */

namespace common\components\validators;


use common\models\File;
use yii\validators\Validator;

class ImageFileValidator extends Validator
{
    public $minWidth = 1;

    protected function validateValue($file)
    {
        return ;
        list($width, $height, $type, $attr)  = getimagesize($file->getLocalTmpFilePath());

        if ($width<$this->minWidth) {
            return ['The image "{file}" is failed or too small. The width cannot be smaller than {limit, number} {limit, plural, one{pixel} other{pixels}}.', [
                'file' => $file->getFileName(),
                'limit' => $this->minWidth,
            ]];
        }
        return null;
    }
}