<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 02.11.16
 * Time: 18:16
 */

namespace common\components;

use Codeception\Lib\Interfaces\Web;
use common\models\HttpRequestExtDataLog;
use common\models\HttpRequestLog;
use common\models\repositories\UserSessionRepository;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Component;
use yii\helpers\Json;

/**
 * Class HttpRequestLogger
 * @package common\components
 *
 * @property HttpRequestLog $currentRequestLog
 * @property float $startTime
 */
class HttpRequestLogger extends Component
{
    protected $startTime;

    protected $currentRequestLog;

    /**
     * @param $requestType
     *
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function initRequestLog($requestType): void
    {
        if ($this->currentRequestLog) {
            // Reinit request type
            $this->currentRequestLog->request_type = $requestType;
            return;
        }

        /** @var HttpRequestLog $currentRequestLog */
        $currentRequestLog = Yii::createObject(HttpRequestLog::class);

        $this->startTime = microtime(true);

        $currentRequestLog->host = $_SERVER['HTTP_HOST'];
        $currentRequestLog->url = $_SERVER['REQUEST_URI'];
        $currentRequestLog->remote_addr = $_SERVER['REMOTE_ADDR'];
        $currentRequestLog->params_get = $_GET;
        $currentRequestLog->params_post = $_POST;
        $currentRequestLog->params_body = \Yii::$app->getRequest()->getBodyParams();
        $currentRequestLog->params_files = $_FILES;
        $currentRequestLog->request_type = $requestType;
        $currentRequestLog->user_agent = Yii::$app->request->userAgent ?? '';
        $currentRequestLog->referrer = Yii::$app->request->referrer ?? '';
        $currentRequestLog->headers = '""';
        $currentRequestLog->cookie = utf8ize($_COOKIE);

        $this->currentRequestLog = $currentRequestLog;

        if ($this->isSaveOnInitRequestLog()) {
            $currentRequestLog->safeSave();
        }

        // Save id in session for sentry view
        Yii::$app->session->set('HttpRequestLogId', $currentRequestLog->id);
    }

    /**
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function initCommonRequestLog(): void
    {
        if (param('saveAllHttpRequestsLog') && (!HttpRequestHelper::isBot(Yii::$app->request))) {
            $type = HttpRequestLog::REQUEST_TYPE_COMMON;
            if (Yii::$app->request->isAjax) {
                $type = HttpRequestLog::REQUEST_TYPE_COMMON_AJAX;
            }
            $this->initRequestLog($type);
        }
    }

    /**
     * @return bool
     */
    protected function isSaveOnInitRequestLog(): bool
    {
        if (!$this->currentRequestLog->isNewRecord) {
            return true;
        }

        if ($this->currentRequestLog->request_type === HttpRequestLog::REQUEST_TYPE_BACKEND) {
            return false;
        }

        return true;
    }

    protected function isSaveAnswer(): bool
    {
        if ($this->currentRequestLog->request_type === HttpRequestLog::REQUEST_TYPE_API) {
            return true;
        }
        return false;
    }

    /**
     * @param $answer
     * @param array $headers
     * @param int $code
     *
     * @throws \yii\base\Exception
     */
    public function closeRequest($answer, array $headers = [], $code = 200): void
    {
        $currentRequestLog = $this->currentRequestLog;

        if (!$currentRequestLog) {
            return;
        }

        $session = [
            'id' => Yii::$app->session->id,
        ];

        foreach (Yii::$app->session as $name => $value) {
            $session[$name] = $value;
        }

        $currentRequestLog->session = utf8ize($session);

        $globalUserSession = UserSessionRepository::getGlobalUserSession();

        if ($globalUserSession && $globalUserSession->id) {
            $currentRequestLog->user_session_id = $globalUserSession->id;
        }

        $currentRequestLog->run_time_milisec = (int) ((microtime(true) - $this->startTime) * 1000);

        $answerJson = [];

        if ($currentRequestLog->request_type !== HttpRequestLog::REQUEST_TYPE_COMMON) {
            $answerJson = $answer;
            if (\is_string($answer) && json_decode($answer . '', true)) {
                // Common request, doesn`t log answer
                $answerJson = json_decode($answer . '', true);
            }
        }
        if ($answer instanceof WebResponse) {
            $answerJson = 'raw';
        }

        $currentRequestLog->answer = $answerJson;
        $currentRequestLog->headers = $headers;
        $currentRequestLog->responce_code = $code;

        if ($this->isSaveOnInitRequestLog()) {
            $transaction = app('db')->getTransaction();
            if ($transaction  && $transaction->getIsActive()) {
                $transaction->rollBack();
            }
        }
        if ($this->isSaveAnswer()) {
            $currentRequestLog->safeSave();
        }
        $this->currentRequestLog = null;
    }

    /**
     * @param $logText
     * @param $externalData
     * @param null $type
     *
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function logRequestInfo($logText, $externalData, $type = null): void
    {
        if (!$this->currentRequestLog) {
            throw new  InvalidParamException('HttpRequestLogger not inited yet!');
        }

        if ($this->currentRequestLog->isNewRecord) {
            $this->currentRequestLog->safeSave();
        }

        /** @var HttpRequestExtDataLog $logExtData */
        $logExtData = Yii::createObject(HttpRequestExtDataLog::class);

        $logExtData->request_id = $this->currentRequestLog->id;
        $logExtData->log_text = $logText;
        $logExtData->ext_data = $externalData;
        $logExtData->type = $type;
        $logExtData->safeSave();
    }

    /**
     * @param BaseAR $model
     * @param $externalData
     *
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function logActiveRecordInfo(BaseAR $model, $externalData): void
    {
        $excludeTables = param('excludeHttpRequestLogTable');

        if ($excludeTables && \in_array($model::tableName(), $excludeTables, false)) {
            return;
        }

        if (!$this->currentRequestLog) {
            throw new InvalidParamException('HttpRequestLogger not inited yet!');
        }

        if ($this->currentRequestLog->isNewRecord) {
            $this->currentRequestLog->safeSave();
        }

        /** @var HttpRequestExtDataLog $logExtData */
        $logExtData = Yii::createObject(HttpRequestExtDataLog::class);

        $logExtData->request_id = $this->currentRequestLog->id;
        $logExtData->log_text = 'BaseAR';
        $logExtData->ext_data = $externalData;
        $logExtData->type = HttpRequestExtDataLog::TYPE_AR;
        $logExtData->object_type = $model::tableName();

        $objectId = $model->primaryKey;

        if (is_array($objectId)) {
            $objectId = Json::encode($objectId);
        }

        $logExtData->object_id = (string) $objectId;
        $logExtData->safeSave();
    }
}