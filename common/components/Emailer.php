<?php

namespace common\components;

use backend\models\ps\PsPrinterRejectForm;
use backend\models\ps\PsRejectForm;
use common\components\order\anonim\AnonimOrderHelper;
use common\components\reject\RejectInterface;
use common\models\CompanyService;
use common\models\CsWindowQuote;
use common\models\DeliveryType;
use common\models\InstantPayment;
use common\models\InvalidEmail;
use common\models\MsgMessage;
use common\models\PaymentBankInvoice;
use common\models\PaymentTransactionRefund;
use common\models\Preorder;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\PsPrinterTest;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use common\models\StoreOrderReview;
use common\models\ThingiverseOrder;
use common\models\User;
use common\models\user\ChangeEmailRequest;
use common\models\UserAddress;
use common\models\UserEmailLogin;
use common\models\UserRequest;
use common\modules\instantPayment\helpers\InstantPaymentUrlHelper;
use common\modules\payment\models\RefundRequestForm;
use common\modules\thingPrint\components\ThingiverseOrderReview;
use common\modules\xss\helpers\XssHelper;
use common\services\UserEmailLoginService;
use DateTime;
use DateTimeZone;
use frontend\models\ps\PsFacade;
use frontend\modules\workbench\components\OrderUrlHelper;
use lib\message\Constants;
use lib\message\MessageServiceInterface;
use lib\message\senders\EmailSender;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * Emailer - general class to work with email templates and etc.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Emailer
{
    /**
     * which layouts to use for html and for text emails
     * layouts in common/mail/ dir
     *
     * @var array
     */
    public $emailViews = ['html' => 'blankEmailHtml', 'text' => 'blankEmail'];
    /**
     * @var MessageServiceInterface
     */
    private $sender;
    /**
     *  files to attach to email
     *
     * @var array
     */
    private $attachments;

    /**
     *
     */
    public function __construct()
    {
        $this->sender = Yii::$app->message;
    }


    /**
     * @param $fileName
     * @param $filePath
     * @return $this
     */
    public function attachFile($fileName, $filePath)
    {
        $this->attachments[$fileName] = $filePath;
        return $this;
    }

    /**
     * Sent generated mail.
     * used by other functions.
     *
     * @param      $to
     * @param      $subject
     * @param      $messageText
     * @param null $messageHtml
     * @return bool
     * @deprecated It old method - use Yii::app()->message->send
     */
    public function sendMessage($to, $subject, $messageText, $messageHtml = null)
    {
        if (strpos($to, 'wiped_') === 0) {
            return false;
        }
        $invalidEmail = InvalidEmail::find()->where(['email' => $to])->one();
        if ($invalidEmail && !$invalidEmail->ignored) {
            return false;
        }
        $data = [
            'content'      => $messageText,
            'content_html' => $messageHtml ?: $messageText = str_replace("\n", "</p>\n<p>", '<p>' . $messageText . '</p>'), //nl2br(),
            'subject'      => $subject,
        ];
        if (defined('TEST_XSS') && TEST_XSS) {
            $to = XssHelper::cleanXssProtect($to);
        }
        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer  = \Yii::$app->mailer;
        $result  = true;
        $message = $mailer->compose(
            $this->emailViews, [
            'content'     => $data['content'],
            'subject'     => $subject,
            'contentHtml' => $data['content_html']
        ])->setFrom(\Yii::$app->params['supportEmail'])
            ->setTo($to)
            ->setSubject($data['subject']);
        if (!empty($this->attachments)) {
            foreach ($this->attachments as $fileName => $filePath) {
                if (is_numeric($fileName)) {
                    $message->attach($filePath);
                } else {
                    $message->attach($filePath, ['fileName' => $fileName]);
                }
            }
        }
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $mailer->getSwiftMailer()->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
        $sendResult = $message->send();
        if (!$sendResult) {

            \Yii::error($logger->dump(), 'email');
            $result = false;
        }
        return $result;
    }

    /**
     * send email to confirm email
     *
     * @param \common\models\User $user
     */
    public function sendConfirmEmail(User $user)
    {
        $params = [
            'username' => $user->username,
            'link'     => \Yii::$app->urlManager->createAbsoluteUrl(['user/email-confirm', 'key' => $user->auth_key])
        ];
        $this->sender->sendForce($user, 'confirmEmail', $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * Send message to old email about create request to change email
     *
     * @param User $user
     * @param ChangeEmailRequest $request
     */
    public function sendCreateChangeEmailRequest(User $user, ChangeEmailRequest $request)
    {
        $params = [
            'username' => $user->username,
            'oldEmail' => $user->email,
            'newEmail' => $request->email,
        ];
        $this->sender->sendForce($user, 'createChangeEmailRequest', $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * Send message to new email to confirm new email
     *
     * @param User $user
     * @param ChangeEmailRequest $request
     */
    public function sendConfirmChangeEmailRequest(User $user, ChangeEmailRequest $request)
    {
        $params      = [
            'username' => $user->username,
            'link'     => \Yii::$app->urlManager->createAbsoluteUrl(['/user/change-email-confirm', 'key' => $request->confirm_key])
        ];
        $user->email = $request->email;
        $this->sender->sendForce($user, 'confirmChangeEmailRequest', $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * send email after signup
     *
     * @param User $user
     * @param        $password
     * @param string $template
     */
    public function sendSignupEmail(User $user, $password, $template = 'register')
    {
        $params = [
            'username' => $user->getFullNameOrUsername(),
            'name'     => $user->username,
            'link'     => \Yii::$app->urlManager->createAbsoluteUrl(['user/email-confirm', 'key' => $user->auth_key]),
            'email'    => $user->email,
            'password' => $password
        ];

        $this->sender->sendForce($user, $template, $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * send email after signup using OSN
     * currently doublicates sendSignupEmail method
     *
     * @param User $user
     * @param      $password
     */
    public function sendSignupByOsnEmail(User $user, $password)
    {
        $this->sendSignupEmail($user, $password, 'registerOsn');
    }

    /**
     * sents link to request password reset
     *
     * @param User $user
     */
    public function sendPasswordReset(User $user)
    {
        $params = [
            'username' => $user->username,
            'link'     => Yii::$app->urlManager->createAbsoluteUrl(['user/reset-password', 'token' => $user->password_reset_token])
        ];

        $this->sender->sendForce($user, 'forgotPassword', $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * if user changes his password, we notify him with this email
     *
     * @param User $user
     * @param string $newPassword
     */
    public function sendPasswordChanged($user, $newPassword)
    {
        $params = [
            'username' => $user->username,
            'email'    => $user->email,
            'password' => $newPassword
        ];

        $this->sender->sendForce($user, 'changePassword', $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * Restore user request
     *
     * @param UserRequest $userRequest
     */
    public function sendRestoreRequest(UserRequest $userRequest)
    {
        $user = $userRequest->user;

        $params = [
            'link'     => param('server') . '/user/restore?code=' . $userRequest->code,
            'username' => $user->username,
        ];

        $this->sender->sendForce($user, 'restoreUserRequest', $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * Send message that order was printed^ but not sended.
     *
     * @param StoreOrderAttemp $attemp
     */
    public function sendDeliveryWait(StoreOrderAttemp $attemp)
    {
        $user   = $attemp->ps->user;
        $params = [
            'psName'   => $attemp->ps->title,
            'username' => $user->username,
            'orderId'  => $attemp->order->id
        ];
        $this->sender->send($user, 'deliveryWait', $params);
    }

    /**
     * Send message that order was shipped without tracking number, domestic
     *
     * @param StoreOrderAttemp $attemp
     */
    public function sendOrderClientDomesticNoTrackingNumber(StoreOrderAttemp $attemp)
    {
        $user   = $attemp->order->user;
        $params = [
            'username'       => $user->getFullNameOrUsername(),
            'orderLink'      => param('server') . '/workbench/order/view/' . $attemp->order->id,
            'orderClickLink' => Html::a('#' . $attemp->order_id, param('server') . '/workbench/order/view/' . $attemp->order->id),
            'orderId'        => $attemp->order->id,
            'carrier'        => $attemp->delivery->tracking_shipper,
        ];
        $tpl    = 'client.order.withoutTracking10days';
        if ($attemp->order->delivery_type_id === DeliveryType::INTERNATIONAL_STANDART_ID) {
            $tpl = 'client.order.withoutTracking20days';
        }
        if ($attemp->order->isThingiverseOrder()) {
            $userEmail = ThingiverseOrderReview::getThingiverseUserEmail($attemp->order->thingiverseOrder);
            if ($userEmail) {
                $this->sender->sendForceEmail($userEmail, $tpl, $params, "en-US");
            }
        } else {
            $this->sender->send($user, $tpl, $params);
        }
    }

    /**
     * Send message that order was shipped without tracking number, international
     *
     * @param StoreOrderAttemp $attemp
     */
    public function sendOrderClientIntlNoTrackingNumber(StoreOrderAttemp $attemp)
    {
        $user   = $attemp->order->user;
        $params = [
            'username'  => $user->getFullNameOrUsername(),
            'orderLink' => param('server') . '/workbench/order/view/' . $attemp->order->id,
            'orderId'   => $attemp->order->id,
            'carrier'   => $attemp->delivery->tracking_shipper,
        ];
        $tpl    = 'client.order.withoutTracking10days';
        if ($attemp->order->isThingiverseOrder()) {
            $userEmail = ThingiverseOrderReview::getThingiverseUserEmail($attemp->order->thingiverseOrder);
            if ($userEmail) {
                $this->sender->sendForceEmail($userEmail, $tpl, $params, "en-US");
            }
        } else {
            $this->sender->send($user, $tpl, $params);
        }
    }

    public function sendClientModeratorDeclineOrder(StoreOrder $order, $reason)
    {
        $params = [
            'orderId'          => $order->id,
            'clientName'       => $order->user->username,
            'psName'           => $order->currentAttemp ? $order->currentAttemp->ps->title : "",
            'returnPolicyLink' => param('server') . '/site/return-policy',
            'reason'           => $reason ? $reason['reason'] : null,
            'comment'          => $reason ? $reason['description'] : null,
        ];
        if ($order->isThingiverseOrder()) {
            $this->sender->sendForceEmail(
                $order->user->thingiverseUser->email,
                'moderatorDeclineOrder',
                $params,
                "en-US"
            );
            return;
        }
        $this->sender->send($order->user, 'moderatorDeclineOrder', $params);
    }

    /**
     * @param $order \common\models\StoreOrder
     * @param RejectInterface $declineForm
     */
    public function sendClientPsDeclineNewOrder(StoreOrder $order, RejectInterface $declineForm)
    {
        $params = [
            'orderId'          => $order->id,
            'orderUrl'         => param('server') . '/workbench/order/view/' . $order->id,
            'clientName'       => $order->user->username,
            'psName'           => $order->currentAttemp ? $order->currentAttemp->ps->title : '',
            'returnPolicyLink' => param('server') . '/site/return-policy',
            'reason'           => $declineForm->getReason(),
            'comment'          => $declineForm->getComment(),
        ];
        if ($order->isThingiverseOrder()) {
            $this->sender->sendForceEmail(
                $order->user->thingiverseUser->email,
                'psDeclineOrder',
                $params,
                'en-US'
            );
        } else {
            $this->sender->send($order->user, 'psDeclineOrder', $params);
        }
    }

    /**
     * Order cancel time expired
     *
     * @link https://redmine.tsdev.work/issues/182
     * @param StoreOrderAttemp $attemp
     */
    public function sendClientCancelOrderPrintingTimeExpired(StoreOrderAttemp $attemp)
    {
        $params = [
            'orderUsername' => $attemp->order->user->username,
            'orderId'       => $attemp->order->id,
            'psName'        => $attemp->ps->title,
        ];

        $this->sender->send($attemp->order->user, 'cancelOrderPrintingTimeExpired', $params);
    }


    public function sendPsClientDislike(StoreOrderAttemp $currentAttemp)
    {
        $order = $currentAttemp->order;
        $this->sender->send($currentAttemp->ps->user, 'ps.order.clientDislike', [
            'link'     => param('server') . '/workbench/service-order/view?id=' . $order->id,
            'orderId'  => $order->id,
            'username' => $currentAttemp->ps->user->username,
            'order_id' => $order->id,
            'psName'   => $order->currentAttemp->ps->title,
        ]);
    }

    /**
     *
     * @param \common\models\StoreOrder $order
     */
    public function sendPsNewOrder(StoreOrderAttemp $orderAttemp)
    {
        $userEmailLoginService = Yii::createObject(UserEmailLoginService::class);

        $order  = $orderAttemp->order;
        $user   = $orderAttemp->ps->user;
        $params = [
            'username' => $user->username,
            'link'     => OrderUrlHelper::viewStoreOrderAttemp($orderAttemp, true),
            'orderId'  => $order->id,
            'order_id' => $order->id,
            'psName'   => $order->currentAttemp->ps->title,
        ];

        if ($user->isUnconfirmed()) {
            $emailLogin     = $userEmailLoginService->generateUserEmailLogin($user, $user->email, $params['link']);
            $params['link'] = $emailLogin->getLoginLinkByHash(true);
        }

        $this->sender->send($user, 'psNewOrder', $params);
    }

    /**
     *
     * @param \common\models\StoreOrderAttemp $orderAttemp
     */
    public function sendPsNewTestOrder(StoreOrderAttemp $orderAttemp)
    {
        $order = $orderAttemp->order;
        $user  = $orderAttemp->ps->user;

        $params = [
            'link'     => OrderUrlHelper::viewStoreOrderAttemp($orderAttemp, true),
            'username' => $user->username,
            'orderId'  => $order->id,
            'psName'   => $order->currentAttemp->ps->title,
        ];

        $this->sender->send($user, 'ps.testorder.new', $params);
    }

    /**
     * send offer order email
     *
     * @param StoreOrder $order
     * @param PsPrinter $printer
     */
    public function sendPsOfferOrder(StoreOrderAttemp $attemp)
    {
        $order = $attemp->order;
        $user  = $attemp->ps->user;

        $params = [
            'link'     => OrderUrlHelper::viewStoreOrderAttemp($attemp, true),
            'username' => $user->username,
            'orderId'  => $order->id,
            'psName'   => $attemp->ps->title,
        ];

        $this->sender->send($user, 'psNewOrderOffer', $params);
    }

    /**
     * Send messaget to our support email with error of getting postal label
     *
     * @param StoreOrder $order
     * @param mixed $log
     *            error text or response of postal
     * @link https://redmine.tsdev.work/issues/1089
     * @todo it bad solution, better way - use yii log route with category
     */
    public function sendSupportGetPostalLabelsError(StoreOrder $order, $log = null)
    {
        $message = "User {$order->user->username}, email {$order->user->email} can't get postal label for order {$order->id} <br/><br/>Error: <br/><br/>" . '<pre>' . print_r($log,
                true) . '</pre>';
        $this->sendSupportMessage('Error on get postal label', $message);
    }

    /**
     * Send message to own support
     *
     * @param string $subject
     * @param string $message
     */
    public function sendSupportMessage($subject, $message)
    {
        $this->sendMessage(\Yii::$app->params['supportEmail'], $subject, $message);
    }

    /**
     * @param User $user
     * @param int $days
     */
    public function sendFirstTaxMessage(User $user, $days = 30)
    {
        $this->sender->sendForce($user, 'firstTaxMessage', ['days' => $days, 'username' => $user->getFullNameOrUsername()], Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * @param User $user
     * @param                               $reason
     */
    public function sendTaxDeclineMessage(User $user, $reason)
    {
        $params = [
            'username' => $user->username,
            'reason'   => $reason,
            'taxLink'  => param('server') . '/my/taxes'
        ];

        $this->sender->sendForce($user, 'declineTaxInfo', $params, Constants::SENDER_TYPE_EMAIL);
    }


    /**
     * Ps request more time for print order
     *
     * @param StoreOrder $order
     */
    public function sendOrderRequestMoreTime(StoreOrder $order)
    {
        $params = [
            'username'      => $order->user->username,
            'orderId'       => $order->id,
            'newPrintDate'  => Yii::$app->formatter->asDate($order->currentAttemp->dates->plan_printed_at),
            'psName'        => $order->currentAttemp->ps->title,
            'psContactLink' => Url::toRoute(['/workbench/messages/create-topic-for-object', 'bindId' => $order->id, 'bindTo' => 'order'], true)
        ];

        if ($order->isThingiverseOrder()) {
            $this->sender->sendForceEmail(
                $order->user->thingiverseUser->email,
                'requestMoreTime',
                $params,
                "en-US"
            );
            return;
        }

        $this->sender->sendForce($order->user, 'requestMoreTime', $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * Letter about model award
     *
     * @param StoreOrder $order
     */
    public function sendModelAward(StoreOrder $order)
    {
        $params = [
            'username'  => $order->orderItem->unit->model3d->user->username,
            'modelname' => $order->orderItem->unit->model3d->title,
        ];

        $this->sender->send($order->orderItem->unit->model3d->user, 'modelAward', $params);
    }

    /**
     * @param StoreOrderAttemp $attemp
     */
    public function sendPsAcceptOrderAttemp(StoreOrderAttemp $attemp)
    {
        $params = [
            'link'             => param('server') . '/workbench/service-order/view?id=' . $attemp->order_id,
            'orderId'          => $attemp->order->id,
            'psName'           => $attemp->ps->title,
            'acceptOrdersLink' => param('server') . Url::toRoute(['/workbench/service-orders', 'statusGroup' => StoreOrderAttemp::STATUS_ACCEPTED])
        ];

        $this->sender->send($attemp->ps->user, 'psAcceptOrder', $params);
    }

    /**
     * @param $order
     */
    public function sendPsAcceptButCancel(StoreOrder $order)
    {
        $params = [
            'orderId' => $order->id,
            'link'    => param('server') . '/workbench/service-order/view?id=' . $order->id,
            'psName'  => $order->currentAttemp->ps->title,
        ];

        $this->sender->send($order->currentAttemp->ps->user, 'psAcceptButCancel', $params);
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param RejectInterface $declineForm
     */
    public function sendPsClientCancelNewOrder(StoreOrderAttemp $attemp, RejectInterface $declineForm)
    {
        $params = [
            'orderId' => $attemp->order->id,
            'link'    => param('server') . '/workbench/service-order/view?id=' . $attemp->order_id,
            'psName'  => $attemp->ps->title,
            'reason'  => $declineForm->getReason(),
            'comment' => $declineForm->getComment(),
        ];

        $this->sender->send($attemp->ps->user, 'psClientCancelNewOrder', $params);
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param RejectInterface $declineForm
     */
    public function sendPsModeratorCancelOrder(StoreOrderAttemp $attemp, RejectInterface $declineForm)
    {
        $params = [
            'psName'        => $attemp->ps->title,
            'orderId'       => $attemp->order->id,
            'orderUrl'      => param('server') . '/workbench/order/view/' . $attemp->order->id,
            'reason'        => $declineForm->getReason() . ($declineForm->getComment() ? ' (' . $declineForm->getComment() . ')' : ''),
            'cancelReason'  => $declineForm->getReason(),
            'cancelComment' => $declineForm->getComment(),
        ];

        $this->sender->send($attemp->ps->user, 'psCancelOrder', $params);
    }

    /**
     * @param StoreOrderAttemp $attemp
     */
    public function sendPsPrintedOrder(StoreOrderAttemp $attemp)
    {
        $params = [
            'orderId' => $attemp->order->id,
            'psName'  => $attemp->ps->title,
            'link'    => param('server') . '/workbench/service-order/view?id=' . $attemp->order_id,
        ];

        $this->sender->send($attemp->ps->user, 'psPrintedOrder', $params);
    }

    /**
     * @param StoreOrderAttemp $attemp
     */
    public function sendPsPrintedReviewedOrder(StoreOrderAttemp $attemp)
    {
        $params = [
            'orderId' => $attemp->order->id,
            'psName'  => $attemp->ps->title,
            'link'    => param('server') . '/workbench/service-order/view?id=' . $attemp->order_id,
        ];

        $orderDetails = $attemp->order->getOrderDeliveryDetails();
        $templateCode = 'psPrintedReviewed';
        if (is_array($orderDetails) && $orderDetails['carrier'] == DeliveryType::CARRIER_MYSELF) {
            $templateCode = 'psPrintedReviewed.selfship';
        }
        $deliveryTypeDetail = $attemp->order->storeOrderDelivery;
        if ($deliveryTypeDetail && $deliveryTypeDetail->deliveryType->code === DeliveryType::PICKUP) {
            $templateCode = 'psPrintedReviewed.pickup';
        }
        $this->sender->send($attemp->ps->user, $templateCode, $params);
    }

    /**
     * @param StoreOrderAttemp $attemp
     */
    public function sendPsSentOrder(StoreOrderAttemp $attemp)
    {
        $params = [
            'link'    => param('server') . '/workbench/service-order/view?id=' . $attemp->order_id,
            'orderId' => $attemp->order->id,
            'psName'  => $attemp->ps->title,
        ];

        $this->sender->send($attemp->ps->user, 'psSentOrder', $params);
    }

    /**
     * @param StoreOrderAttemp $attemp
     */
    public function sendPsReceivedOrder(StoreOrderAttemp $attemp)
    {
        $params = [
            'link'    => param('server') . '/workbench/service-order/view?id=' . $attemp->order_id,
            'orderId' => $attemp->order->id,
            'psName'  => $attemp->ps->title,
        ];

        $this->sender->send($attemp->ps->user, 'psReceivedOrder', $params);
    }

    /**
     * @param $order
     */
    public function sendClientNewOrder(StoreOrder $order)
    {
        $htmlOrderList = app()->view->render('@common/mail/clientNewOrderBlank.php', ['storeOrder' => $order]);
        $htmlOrderList = str_replace("\n", ' ', $htmlOrderList);
        $params        = [
            'orderId'       => $order->id,
            'htmlOrderList' => $htmlOrderList,
            'link'          => param('server') . '/workbench/order/view/' . $order->id
        ];

        if ($order->isAnonim()) {
            if (!$order->shipAddress) {
                return;
            }
            $params['clientName'] = $order->shipAddress->getContact_name();
            $this->sender->sendForceEmail($order->shipAddress->email, 'clientNewOrder', $params, "en-US");
            return;
        }
        if ($order->user->isUnconfirmed()) {
            $user                  = $order->user;
            $userEmailLoginService = Yii::createObject(UserEmailLoginService::class);
            $emailLogin            = $userEmailLoginService->generateUserEmailLogin($user, $user->email, $params['link']);
            $params['link']        = $emailLogin->getLoginLinkByHash(true);
        }

        $params['clientName'] = $order->user->username;
        $this->sender->send($order->user, 'clientNewOrder', $params);
    }

    /**
     * @param $order
     */
    public function sendClientNewAnonimOrder(StoreOrder $order)
    {
        $user = $order->getPossibleUser();

        $params = [
            'orderId'          => $order->id,
            'clientName'       => $user->username,
            'confirmOrderLink' => AnonimOrderHelper::createConfirmLink($order),
        ];

        $this->sender->sendForce($user, 'client.order.new.anonim', $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * @param $order
     */
    public function sendClientPrintStartOrder(StoreOrder $order)
    {
        $params = [
            'orderId'    => $order->id,
            'clientName' => $order->user->username,
        ];

        $this->sender->send($order->user, 'clientPrintStartOrder', $params);
    }

    /**
     * @param StoreOrder $order
     * @internal param $params
     */
    public function sendClientPrintStartOfferedOrderDelivery(StoreOrder $order)
    {
        $params = [
            'orderId'    => $order->id,
            'clientName' => $order->user->username,
            'newPs'      => $order->currentAttemp->ps->title,
            'oldPs'      => $order->previousAttemp->ps->title,
        ];
        if ($order->currentAttemp->ps_id == $order->previousAttemp->ps_id) {
            return;
        }
        $this->sender->send($order->user, 'client.order.orderoffer.accept.delivery', $params);
    }

    /**
     * @param StoreOrder $order
     * @internal param $params
     */
    public function sendClientPrintStartOfferedOrderPickup(StoreOrder $order)
    {
        $params = [
            'orderId'    => $order->id,
            'clientName' => $order->user->username,
            'newPs'      => $order->currentAttemp->ps->title,
            'oldPs'      => $order->previousAttemp->ps->title,
            'newAddress' => UserAddress::formatAddress($order->currentAttemp->machine->getUserAddress(), false, false)
        ];
        if ($order->currentAttemp->ps_id == $order->previousAttemp->ps_id) {
            return;
        }
        $this->sender->send($order->user, 'client.order.orderoffer.accept.pickup', $params);
    }

    /**
     * send after PS clicks Set As Printed
     *
     * @param StoreOrder $order
     */
    public function sendClientCanAcceptOrder(StoreOrder $order)
    {
        $orderLink = param('server') . '/workbench/order/view/' . $order->id;
        if ($order->isThingiverseOrder()) {
            $hash      = ThingiverseOrderReview::getOrderAccessHash($order);
            $orderLink = param('server') . '/workbench/order/thing-review/' . $order->id . '?hash=' . $hash;
        }
        $params = [
            'orderId'    => $order->id,
            'orderLink'  => $orderLink,
            'clientName' => $order->user->username,
        ];

        $this->sender->send($order->user, 'clientCanAcceptOrder', $params);
    }

    /**
     * @param $order
     */
    public function sendClientPrintedOrder(StoreOrder $order)
    {
        if ($order->deliveryType && $order->deliveryType->code == DeliveryType::PICKUP) {

            $machine = $order->currentAttemp->machine;

            $printerAddress = UserAddress::getAddressFromLocation($machine->location);
            $address        = UserAddress::formatAddress($printerAddress, false);

            $deliveryParams = $machine->getPsDeliveryTypeByCode(DeliveryType::PICKUP);

            $params = [
                'username'        => $order->user->username,
                'orderId'         => $order->id,
                'psName'          => $machine->ps->title,
                'printer_address' => $address,
                'working_hours'   => $deliveryParams ? $deliveryParams->comment : ''
            ];

            $this->sender->send($order->user, 'psOrderPrintedPickUp', $params);
        } else {
            $params = [
                'orderId'    => $order->id,
                'clientName' => $order->user->username,
            ];

            $this->sender->send($order->user, 'clientPrintedOrder', $params);
        }
    }


    /**
     * @param $order
     */
    public function sendClientSentOrder(StoreOrder $order)
    {
        $deliveryDetail = $order->getOrderDeliveryDetails();
        $params         = [
            'orderId'         => $order->id,
            'clientName'      => $order->user->username,
            'trackingNumber'  => !empty($deliveryDetail['tracking_number']) ? $deliveryDetail['tracking_number'] : 'N/A',
            'trackingShipper' => !empty($deliveryDetail['tracking_shipper']) ? $deliveryDetail['tracking_shipper'] : 'N/A',
            'trackingCarrier' => !empty($deliveryDetail['tracking_shipper']) ? $deliveryDetail['tracking_shipper'] : 'N/A',
        ];
        if ($order->deliveryType && $order->deliveryType->code == DeliveryType::PICKUP) {

        } else {
            $this->sender->send($order->user, 'clientSentOrder', $params);
        }
    }

    /**
     * The customer receives a message when the order tracking number is changed
     *
     * @param $order
     */
    public function sendClientDeliveryTrackChanged(StoreOrder $order)
    {
        $deliveryDetail = $order->getOrderDeliveryDetails();

        $this->sender->send($order->user, 'clientDeliveryTrackChanged', [
            'orderId'           => $order->id,
            'name'              => $order->user->username,
            'newTrackingNumber' => !empty($deliveryDetail['tracking_number']) ? $deliveryDetail['tracking_number'] : 'N/A',
            'link'              => param('siteUrl') . Url::toRoute(['/workbench/service-order/view', 'id' => $order->id])
        ]);
    }

    /**
     * @param $order
     */
    public function sendClientReceivedOrder(StoreOrder $order)
    {
        $params = [
            'orderId'    => $order->id,
            'clientName' => $order->user->username,
        ];

        $this->sender->send($order->user, 'clientReceivedOrder', $params);
    }

    public function sendClientReceivedOrderTrustpilot(StoreOrder $order)
    {
        $this->sender->send($order->user, 'clientReceivedOrderTrustpilot', [
            'orderId'    => $order->id,
            'clientName' => $order->user->username,
        ]);
    }

    /**
     * @param $order
     */
    public function sendClientPsAcceptButCancel(StoreOrder $order)
    {
        $params = [
            'orderId'             => $order->id,
            'clientName'          => $order->user->username,
            'psName'              => $order->currentAttemp->ps->title,
            'chooseAnotherPsLink' => param('server')
        ];

        $this->sender->send($order->user, 'clientPsAcceptButCancel', $params);
    }

    /**
     * @param User $fromUser
     * @param User $toUser
     * @param MsgMessage $message
     */
    public function sendNewMessage(User $fromUser, User $toUser, MsgMessage $message)
    {
        if ($toUser->getIsSystem()) {
            return;
        }

        $params = [
            'username'        => $toUser->username,
            'messageUsername' => $fromUser->username,
            'messageDate'     => (new DateTime($message->created_at,
                new DateTimeZone('UTC')))->setTimezone(new DateTimeZone($toUser->userProfile->timezone_id))->format("m/d/Y H:i"),
            'messageLink'     => param('server') . '/workbench/messages',
        ];

        $this->sender->send($toUser, 'userNewMessage', $params);
    }

    /**
     *
     * @param User $fromUser
     * @param User $toUser
     * @param MsgMessage $message
     * @param StoreOrder $order
     */
    public function sendNewOrderMessage(User $fromUser, User $toUser, MsgMessage $message, StoreOrder $order)
    {
        if ($toUser->getIsSystem()) {
            return;
        }

        $params = [
            'username'        => $toUser->username,
            'messageUsername' => $fromUser->username,
            'link'            => param('server') . '/workbench/order/view/' . $order->id,
            'messageDate'     => (new DateTime($message->created_at,
                new DateTimeZone('UTC')))->setTimezone(new DateTimeZone($toUser->userProfile->timezone_id))->format("m/d/Y H:i"),
            'orderId'         => $order->id,
        ];
        if ($order->isThingiverseOrder() && $toUser->id == $order->user_id) {
            $this->sender->sendForceEmail(
                $order->user->thingiverseUser->email,
                'userNewOrderMessage',
                $params,
                'en-US'
            );
            return;
        }

        $this->sender->send($toUser, 'userNewOrderMessage', $params);
    }

    /**
     *
     * @param User $fromUser
     * @param User $toUser
     * @param MsgMessage $message
     * @param Preorder $preorder
     */
    public function sendNewPreorderMessage(User $fromUser, User $toUser, MsgMessage $message, Preorder $preorder)
    {
        if ($toUser->getIsSystem()) {
            return;
        }

        $params = [
            'preorderId'      => $preorder->id,
            'username'        => $toUser->username,
            'messageLink'     => param('server') . '/workbench/messages',
            'messageUsername' => $fromUser->username,
            'messageDate'     => (new DateTime($message->created_at,
                new DateTimeZone('UTC')))->setTimezone(new DateTimeZone($toUser->userProfile->timezone_id))->format("m/d/Y H:i"),
        ];

        $this->sender->send($toUser, 'preorder.userNewMessage', $params);
    }

    /**
     * Send message that printer test is success
     *
     * @param PsPrinterTest $test
     */
    public function sendPrinterTestSuccess(PsPrinterTest $test)
    {
        $params = [
            'printerName' => $test->psPrinter->title,
            'psName'      => $test->psPrinter->ps->title,
            'psLink'      => param('server') . '/mybusiness/company',
        ];
        $this->sender->send($test->psPrinter->ps->user, 'ps.test.success', $params);
    }

    /**
     * Send message that printer test is failed
     *
     * @param PsPrinterTest $test
     */
    public function sendPrinterTestFailed(PsPrinterTest $test)
    {
        $params = [
            'printerName'  => $test->psPrinter->title,
            'psName'       => $test->psPrinter->ps->title,
            'rejectReason' => $test->comment,
        ];
        $this->sender->send($test->psPrinter->ps->user, 'ps.test.failed', $params);
    }

    /**
     * Send message about printer moderation failed
     *
     * @param PsPrinter $psPrinter
     * @param PsPrinterRejectForm $rejectForm
     */
    public function sendPrinterModerationReject(PsPrinter $psPrinter, PsPrinterRejectForm $rejectForm)
    {
        $params = [
            'printerName' => $psPrinter->title,
            'psName'      => $psPrinter->ps->title,

            'reason'       => $rejectForm->getReason(),
            'comment'      => $rejectForm->getComment(),
            'psLink'       => param('server') . '/mybusiness/company/edit-ps',
            'rejectReason' => $rejectForm->getText(),
        ];
        $this->sender->send($psPrinter->ps->user, 'ps.printer.moderation.reject', $params);
    }

    /**
     * Send message to PS that PS is succesful moderated.
     *
     * @param Ps $ps
     */
    public function sendPsModerationApproved(Ps $ps): void
    {
        $params = [
            'companyName' => $ps->title,
            'username'    => $ps->user->getFullNameOrUsername()
        ];
        $this->sender->send($ps->user, 'ps.moderation.approved', $params);
    }

    /**
     * Send message to PS that PS is banned.
     *
     * @param Ps $ps
     * @param RejectInterface $reject
     */
    public function sendPsBanned(Ps $ps, RejectInterface $reject): void
    {
        $params = [
            'companyName' => $ps->title,
            'reason'      => $reject->getReason(),
            'comment'     => $reject->getComment(),
        ];
        $this->sender->send($ps->user, 'ps.moderation.banned', $params);
    }

    /**
     * Send message about ps moderation failed
     *
     * @param Ps $ps
     * @param PsRejectForm $rejectForm
     */
    public function sentPsModerationReject(Ps $ps, PsRejectForm $rejectForm)
    {
        $rejectReason = $rejectForm->getText();
        $rejectReason = $rejectReason ? _t('site.ps', 'Reason') . ': ' . $rejectReason : '';

        $params = [
            'psName'       => $ps->title,
            'reason'       => $rejectForm->getReason(),
            'comment'      => $rejectForm->getComment(),
            'rejectReason' => $rejectReason,
            'psLink'       => param('server') . '/mybusiness/company/edit-ps',
        ];
        $this->sender->send($ps->user, 'ps.moderation.reject', $params);
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param DynamicModel $rejectForm
     */
    public function sendPsPrintRejectOrder(StoreOrderAttemp $attemp, DynamicModel $rejectForm)
    {
        $params = [
            'link'    => param('server') . '/workbench/service-order/view?id=' . $attemp->order_id,
            'orderId' => $attemp->order_id,
            'reason'  => $rejectForm->reason,
            'psName'  => $attemp->ps ? $attemp->ps->title : ''
        ];

        $this->sender->send($attemp->ps->user, 'psPrintReject', $params);
    }

    /**
     * Send message to PS that test order is success printed
     *
     * @param Ps $ps
     */
    public function sendPsSuccessTestOrder(Ps $ps)
    {
        $params = [
            'psName' => $ps->title,
        ];
        $this->sender->send($ps->user, 'ps.testorder.success', $params);
    }

    /**
     * @param StoreOrder $order
     */
    public function sendClientOrderIsDelivered(StoreOrder $order)
    {
        $params = [
            'orderId'    => $order->id,
            'clientName' => $order->user->username,
            'ordersLink' => param('server') . '/workbench/orders'
        ];

        $this->sender->send($order->user, 'client.order.delivered', $params);
    }

    /**
     * Send email to PS when admin change offer
     *
     * @param StoreOrderAttemp $orderAttemp
     */
    public function sendPsAdminChangeOffer(StoreOrderAttemp $orderAttemp)
    {
        $params = [
            'link'    => param('server') . '/workbench/service-order/view?id=' . $orderAttemp->order->id,
            'orderId' => $orderAttemp->order_id,
        ];
        $this->sender->send($orderAttemp->ps->user, 'ps.order.adminChangeOffer', $params);
    }

    /**
     * @param StoreOrderPosition $position
     */
    public function sendPositionNewPs(StoreOrderPosition $position)
    {
        if (!$position->order->currentAttemp) {
            return;
        }
        $psUser = $position->order->currentAttemp->ps->user;
        $params = [
            'link'           => param('server') . '/workbench/service-order/view?id=' . $position->order->id,
            'name'           => $psUser->username,
            'orderId'        => $position->order_id,
            'positionTitle'  => $position->title,
            'positionAmount' => displayAsCurrency($position->amount, $position->currency_iso),
        ];

        $this->sender->send($psUser, 'ps.order.positionNew', $params);
    }

    /**
     * @param StoreOrderPosition $position
     * @param $clientName
     * @param UserEmailLogin $userEmailLogin
     */
    public function sendPositionNewClient(StoreOrderPosition $position, $clientName, UserEmailLogin $userEmailLogin)
    {
        $params = [
            'name'           => $clientName,
            'orderId'        => $position->order_id,
            'positionTitle'  => $position->title,
            'loginLink'      => $userEmailLogin->getLoginLinkByHash(),
            'positionAmount' => displayAsCurrency((float)$position->amount + (float)$position->fee, $position->currency_iso),
        ];
        if (!$position->order->isThingiverseOrder()) {
            $this->sender->send($position->order->user, 'client.order.positionNew', $params);
        } else {
            $params['link'] = '';
            $this->sender->sendForceEmail(
                $userEmailLogin->email,
                'client.order.positionNew',
                $params,
                "en-US"
            );
        }
    }

    /**
     * @param StoreOrderPosition $position
     */
    public function sendPositionPaid(StoreOrderPosition $position)
    {
        if (!$position->order->currentAttemp) {
            return;
        }
        $psUser = $position->order->currentAttemp->ps->user;

        $params = [
            'link'           => param('server') . '/workbench/service-order/view?id=' . $position->order->id,
            'name'           => $psUser->username,
            'orderId'        => $position->order_id,
            'positionTitle'  => $position->title,
            'positionAmount' => displayAsCurrency((float)$position->amount + (float)$position->fee, $position->currency_iso),
        ];
        $this->sender->send($psUser, 'ps.order.positionPaid', $params);
    }

    /**
     * @param StoreOrderPosition $position
     * @param $message
     */
    public function sendPositionDeclined(StoreOrderPosition $position, $message)
    {
        $psUser = $position->order->currentAttemp->ps->user;
        $params = [
            'link'           => param('server') . '/workbench/service-order/view?id=' . $position->order->id,
            'name'           => $psUser->username,
            'orderId'        => $position->order_id,
            'positionTitle'  => $position->title,
            'positionAmount' => displayAsCurrency((float)$position->amount + (float)$position->fee, $position->currency_iso),
            'message'        => $message
        ];
        $this->sender->send($psUser, 'ps.order.positionDeclined', $params);
    }


    /**
     * @param PsPrinter $psPrinter
     * @param User $fromUser
     * @throws \yii\base\InvalidParamException
     */
    public function sendCertifyPrinterEmail(PsPrinter $psPrinter, User $fromUser)
    {
        $sender      = new EmailSender();
        $messageText = _t('site.ps', 'Thank you for adding your 3D printer on Treatstock!
You are now part of our global 3D printing network on Treatstock. Make sure you certify your printer to take full advantage of these benefits:
    
     * Only certified machines can receive orders from our catalog of 3D models
     * Only certified machines can receive orders from several key API partners
     * Customers trust and therefore send more print requests to certified machines
Get Certified: {link}
     
Treatstock Team.', ['link' => 'https://www.treatstock.com/mybusiness/services/certification?utm_source=email&utm_medium=image&utm_campaign=certify']);

        $messageHtml = file_get_contents(Yii::getAlias('@common/mail/certify-email.html'));
        $sender->send($psPrinter->ps->user->email,
            _t('site.ps', 'Thank you for adding your 3D printer on Treatstock!'),
            $messageText, $messageHtml,
            $fromUser);
    }

    /**
     * @param StoreOrder $order
     * @param $link
     */
    public function sendOrderReviewEmail(StoreOrder $order, $link)
    {
        if ($order->isForPreorder()) {
            return;
        }
        $toUser = $order->user;

        $fromUser    = User::findByPk(User::USER_ID_SUPPORT);
        $sender      = new EmailSender();
        $view        = new View();
        $messageHtml = $view->render('@common/mail/thingiverse-report.html.php', [
            'order'                 => $order,
            'link'                  => $link,
            'reviewModerationImage' => $order->currentAttemp->getAttemptModerationImageFile()
        ]);

        $messageText = _t(
            'site.email',
            'Thank you for your order on Treatstock! If you have a moment, please leave a review for the manufacturer {link}',
            ['link' => $link]
        );
        $sender->send($toUser->getEmail(), _t('site.email', 'Leave a review for Order #') . $order->id, $messageText, $messageHtml, $fromUser);
    }

    /**
     *
     * @param StoreOrder $order
     * @param $toUserEmail
     */
    public function sendOrderReviewEmailThingiverse(StoreOrder $order, $toUserEmail)
    {
        $fromUser    = User::findByPk(User::USER_ID_SUPPORT);
        $hash        = ThingiverseOrderReview::getOrderAccessHash($order);
        $link        = param('server') . sprintf('/workbench/order/add-anonymouse-review/?orderId=%d&hash=%s', $order->id, $hash);
        $sender      = new EmailSender();
        $view        = new View();
        $messageHtml = $view->render('@common/mail/thingiverse-report.html.php', [
            'order'                 => $order,
            'link'                  => $link,
            'reviewModerationImage' => $order->currentAttemp->getAttemptModerationImageFile()
        ]);

        $messageText = _t(
            'site.email',
            'Thank you for your order on Treatstock! If you have a moment, please leave a review for the print service and share your experience with the Thingiverse community {link}',
            ['link' => $link]
        );
        $sender->send($toUserEmail, _t('site.email', 'Thingiverse Order Review'), $messageText, $messageHtml, $fromUser);
    }

    /**
     * @param $user
     * @param $code
     * @param $params
     */
    public function sendForce($user, $code, $params)
    {
        $this->sender->sendForce($user, $code, $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * @param StoreOrderAttemp $orderAttemp
     */
    public function sendPsOpenDispute(StoreOrderAttemp $orderAttemp)
    {
        $params = [
            'orderId' => $orderAttemp->order_id,
            'link'    => param('server') . '/workbench/service-order/view?id=' . $orderAttemp->order_id,
            'psName'  => $orderAttemp->ps->title
        ];
        if ($orderAttemp->order->isThingiverseOrder()) {
            $this->sender->send($orderAttemp->ps->user, 'ps.order.openDisputeThingiverse', $params);
        } else {
            $this->sender->send($orderAttemp->ps->user, 'ps.order.openDispute', $params);
        }
    }

    public function sendPsCloseDispute(StoreOrderAttemp $orderAttemp)
    {
        $params = [
            'link'    => param('server') . '/workbench/service-order/view?id=' . $orderAttemp->order_id,
            'orderId' => $orderAttemp->order_id,
            'psName'  => $orderAttemp->ps->title,
        ];
        $this->sender->send($orderAttemp->ps->user, 'ps.order.closedDispute', $params);
    }

    /**
     * refund requested - by moderator
     *
     * @param RefundRequestForm $refundModel
     */
    public function sendRefundRequested(RefundRequestForm $refundModel)
    {
        // send message to client and ps
        $order  = $refundModel->transaction->order;
        $params = [
            'orderId'      => $order->id,
            'clientName'   => $order->user->username,
            'psName'       => $order->currentAttemp ? $order->currentAttemp->ps->title : "",
            'refundAmount' => displayAsMoney($refundModel->amountMoney),
            'comment'      => $refundModel->comment,
        ];
        $this->sender->send($order->user, 'partialRefundOrder', $params);
    }

    /**
     * ps pre requested partial refund
     *
     * @param StoreOrder $storeOrder
     * @param PaymentTransactionRefund $refundModel
     */
    public function sendPreRefundRequested(StoreOrder $storeOrder, PaymentTransactionRefund $refundModel)
    {
        // send message to support
        $this->sendSupportMessage('PS issued partial refund for Order #' . $storeOrder->id, sprintf(
            'Partial Refund for order #%s issued from PS. Refund amount %s. %s',
            $storeOrder->id, displayAsCurrency($refundModel->amount, $refundModel->currency),
            Html::a('Open in backend', param('backendServer') . '/store/store-order/view?id=' . $storeOrder->id)
        ));
    }

    /**
     * template code: user.loginByEmailLink
     *
     * @param UserEmailLogin $emailLogin
     * @param string $password
     */
    public function sendLoginLinkByEmail(UserEmailLogin $emailLogin, $password = '')
    {
        $params = [
            'user'           => $emailLogin->user->getFullNameOrUsername(),
            'passwordSetted' => $password ? _t('site.user', 'Your password: ') . $password : '.',
            'hash'           => $emailLogin->hash,
            'requestedUrl'   => $emailLogin->requested_url,
            'urlWithAccess'  => $emailLogin->getLoginLinkByHash(),
            'linkWithAccess' => Html::a($emailLogin->getLoginLinkByHash(), $emailLogin->getLoginLinkByHash()),
        ];
        $this->sender->sendForceEmail($emailLogin->email, "user.loginByEmailLink", $params, "en-US");
    }

    /**
     * @param UserEmailLogin $emailLogin
     * @param ThingiverseOrder $thingiverseOrder
     * @param string $template - which template to use for this order
     */
    public function sendLoginLinkByEmailThingiverse(UserEmailLogin $emailLogin, ThingiverseOrder $thingiverseOrder, $template = '')
    {
        if (empty($template)) {
            $template = 'user.emailThingiverseOrderLink';
        }
        $userName = $emailLogin->user->getFullNameOrUsername();
        if (strpos($userName, '_tg')) {
            $userNameParts = explode("_tg", $userName, 2);
            $userName      = $userNameParts[0];
        }

        $htmlOrderList = app()->view->render('@common/mail/clientNewOrderBlank.php', ['storeOrder' => $thingiverseOrder->order]);
        $htmlOrderList = str_replace("\n", ' ', $htmlOrderList);

        $params = [
            'user'               => $userName,
            'hash'               => $emailLogin->hash,
            'requestedUrl'       => $emailLogin->requested_url,
            'urlWithAccess'      => $emailLogin->getLoginLinkByHash(),
            'orderId'            => $thingiverseOrder->order_id,
            'thingiverseOrderId' => $thingiverseOrder->thingiverse_order_id,
            'linkWithAccess'     => Html::a($emailLogin->getLoginLinkByHash(), $emailLogin->getLoginLinkByHash()),
            'htmlOrderList'      => $htmlOrderList
        ];
        $this->sender->sendForceEmail($emailLogin->email, $template, $params, "en-US");
    }

    /**
     * Send message to PS that PS is succesful moderated.
     *
     * @param CompanyService $service
     */
    public function sendPsServiceApproved(CompanyService $service): void
    {
        $params = [
            'companyName' => $service->ps->title,
            'serviceName' => $service->getTitleLabel()
        ];
        $this->sender->send($service->ps->user, 'ps.service.modeartion.approved', $params);
    }

    public function sendClientOrderReviewBad(StoreOrder $order)
    {
        $params = [
            'orderId'    => $order->id,
            'clientName' => $order->user->getFullNameOrUsername(),
        ];
        $tpl    = 'client.order.badreview';
        if ($order->isThingiverseOrder()) {
            $userEmail = ThingiverseOrderReview::getThingiverseUserEmail($order->thingiverseOrder);
            if ($userEmail) {
                $this->sender->sendForceEmail($userEmail, $tpl, $params, "en-US");
            }
        } else {
            $this->sender->send($order->user, $tpl, $params);
        }
    }

    /**
     * @param StoreOrderReview $review
     */
    public function sendClientReviewAnswer(StoreOrderReview $review)
    {
        $this->sender->send($review->order->user, 'clientReviewAnswer', [
            'clientName' => $review->order->user->getFullNameOrUsername(),
            'psName'     => Html::encode($review->ps->title),
            'orderId'    => $review->order->id,
            'link'       => Url::toRoute(PsFacade::getPsReviewsRoute($review->ps, ['#' => 'user-review-' . $review->id]), true)
        ]);
    }

    /**
     * @param StoreOrderReview $review
     */
    public function sendPsOrderReviewed(StoreOrderReview $review): void
    {
        $this->sender->send($review->ps->user, 'psOrderReviewed', [
            'clientName' => $review->order->user->getFullNameOrUsername(),
            'psName'     => Html::encode($review->ps->title),
            'orderId'    => $review->order->id
        ]);
    }

    /**
     * @param CsWindowQuote $quote
     */
    public function sendQuoteWindowCalculator(CsWindowQuote $quote): void
    {
        $this->sender->send($quote->companyService->getOwner(), 'quoteWindowCalculator', [
            'companyName' => $quote->companyService->getTitleLabel(),
            'link'        => $quote->getUrlQuoteDetails(true)
        ]);
    }

    /**
     * @param InstantPayment $instantPayment
     */
    public function sendNewInstantPayment(InstantPayment $instantPayment)
    {
        $this->sender->send($instantPayment->toUser, 'newInstantPayment', [
            'companyName' => $instantPayment->toUser->company->getTitle(),
            'fromUser'    => $instantPayment->fromUser->getFullNameOrUsername(),
            'link'        => param('server') . InstantPaymentUrlHelper::getView($instantPayment)
        ]);
    }

    public function sendBankInvoicePayment(PaymentBankInvoice $bankInvoice, string $pdf)
    {
        $msg   = 'Bank Invoice created for invoice ' . $bankInvoice->payment_invoice_uuid;
        $user  = $bankInvoice->paymentInvoice->user;
        $email = new Emailer();
        $email->attachFile('invoice.pdf', $pdf);
        $email->sendMessage($user->email, $msg, $msg);
    }
}
