<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.12.16
 * Time: 16:00
 */

namespace common\components;


use common\interfaces\FileBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\repositories\FileRepository;
use lib\file\UploadException;
use Yii;

class ArchiveManager
{
    const ARCHIVE_FORMATS = ['zip'];

    public function isArchive(FileBaseInterface $file): bool
    {
        $ext = $file->getFileExtension();
        if (in_array($ext, ArchiveManager::ARCHIVE_FORMATS, true)) {
            return true;
        }
        return false;
    }

    /**
     * Unpack files
     *
     * @param File $file
     * @return \common\models\File[]
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws UploadException
     */
    public function unpackFiles(FileBaseInterface $file)
    {
        $filePath = $file->getLocalTmpFilePath();
        $toFolder = Yii::$app->getRuntimePath() . '/unzip/' . date('y_m_d') . '_' . md5_file($filePath);
        $za = new \ZipArchive();
        if ($za->open($filePath) !== true) {
            throw new UploadException(_t('site.upload', 'Cannot open zip file.'));
        }
        $za->extractTo($toFolder);
        $za->close();
        $objects = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($toFolder), \RecursiveIteratorIterator::SELF_FIRST);
        $result = [];
        foreach ($objects as $name => $object) {
            if (is_dir($name)) {
                continue;
            }
            /** @var FileFactory $fileFactory */
            $fileFactory = Yii::createObject(FileFactory::class);
            $fileRepository = Yii::createObject(FileRepository::class);

            $file = $fileFactory->createFileFromPath($name);
            if (!FileTypesHelper::isAllowExtensions($file)) {
                continue;
            }
            $fileRepository->save($file);
            $result[] = $file;
        }
        return $result;
    }
}