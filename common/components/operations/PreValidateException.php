<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\operations;


/**
 * Post validate exception
 * @package common\components\operations
 */
class PreValidateException extends ValidateException
{
    /**
     * @param Operation $operation
     */
    public function __construct(Operation $operation)
    {
        parent::__construct('Pre validate exception', $operation);
    }
}