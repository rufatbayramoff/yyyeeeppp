<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\operations;

/**
 * Base class for operation
 * @package common\components
 */
abstract class Operation
{
    /**
     * @var string[]
     */
    private $errors = [];

    /**
     * @param ...$args
     * @return static
     */
/*    final public static function create(...$args)
    {
        return new static(...$args);
    }*/

    /**
     * @return bool
     */
    final public function validate()
    {
        $this->checkErrors();
        return !$this->hasErrors();
    }

    /**
     * Pre-validation
     * @return bool
     */
    protected function checkErrors()
    {

    }

    /**
     * @return bool
     */
    final protected function hasErrors()
    {
        return !empty($this->errors);
    }

    /**
     * Return errors
     * @return string[]
     */
    final public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Post validation
     * @return bool
     */
    protected function postValidate()
    {
        return empty($this->errors);
    }

    /**
     * Add error
     * @param string $error
     */
    final protected function addError($error)
    {
        $this->errors[] = $error;
    }

    /**
     * Validate and execute operation
     * @param bool $throwPrevalidateException
     * @return bool
     * @throws PostValidateException
     * @throws PreValidateException
     */
    final public function execute($throwPrevalidateException = false)
    {
        if(!$this->validate())
        {
            if($throwPrevalidateException)
            {
                throw $this->createPreValidationException();
            }
            else
            {
                return false;
            }
        }

        $this->doOperation();

        if(!$this->postValidate())
        {
            throw $this->createPostValidationException();
        }

        return true;
    }

    /**
     * Create pre validate exception
     * It may be override for different purpose
     * @return PreValidateException
     */
    protected function createPreValidationException()
    {
        return new PreValidateException($this);
    }

    /**
     * Create post validate exception.
     * It may be override for different purpose
     * @return PostValidateException
     */
    protected function createPostValidationException()
    {
        return new PostValidateException($this);
    }

    /**
     * Do operation
     */
    abstract protected function doOperation();
}