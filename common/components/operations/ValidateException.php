<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\operations;

use yii\base\Exception;

/**
 * Post validate exception
 * @package common\components\operations
 */
abstract class ValidateException extends Exception
{
    /**
     * @var Operation
     */
    private $operation;

    /**
     * @param string $message
     * @param Operation $operation
     */
    public function __construct($message, Operation $operation)
    {
        $this->operation = $operation;
        \Exception::__construct($message.' '.get_class($operation));
    }

    /**
     * @return Operation
     */
    public function getOperation()
    {
        return $this->operation;
    }
}