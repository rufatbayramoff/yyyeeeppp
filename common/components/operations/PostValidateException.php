<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\operations;



/**
 * Post validate exception
 * @package common\components\operations
 */
class PostValidateException extends ValidateException
{
    /**
     * @param Operation $operation
     */
    public function __construct(Operation $operation)
    {
        parent::__construct('Post validate exception', $operation);
    }
}