<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.05.17
 * Time: 11:51
 */

namespace common\components;

use common\interfaces\Model3dBasePartInterface;
use common\modules\psPrinterListMaps\components\FilepageVolumeRepository;
use common\services\Model3dPartService;

class Model3dPartVolumeCalulator
{
    /**
     *  var k1s = infillSupport/1000;
     *
     * @param Model3dBasePartInterface $model3dPart
     * @return int|null
     */
    public static function calculateModel3dPartVolume(Model3dBasePartInterface $model3dPart)
    {
        if (!$model3dPart->isCaclulatedProperties()) {
            return 0;
        }

        $texture = $model3dPart->getCalculatedTexture();
        $filament = Model3dPartService::getModel3dPartInfo($model3dPart);

        if (!$filament) {
            return 0;
        }
        if (!$filament->area) {
            return 0;
        }

        if ($texture->calculatePrinterMaterialGroup()->fill_percent_max) {
            // May be different
            $infill = $texture->infill;
            if ($infill<$texture->calculatePrinterMaterialGroup()->fill_percent || $infill>$texture->calculatePrinterMaterialGroup()->fill_percent_max) {
                $infill = $texture->calculatePrinterMaterialGroup()->fill_percent;
            }
        } else {
            $infill = $texture->calculatePrinterMaterialGroup()->fill_percent;
        }
        $infill = $infill/100;
        $thickness = $texture->calculatePrinterMaterialGroup()->wall_thickness / 100;

        $volumeInside = ($filament->volume - $filament->area*$thickness);
        $volumeInside = max($volumeInside, 0 );
        $volume = $volumeInside * $infill + $filament->area * $thickness + $filament->supports_volume * $infill;
        $volume = $volume/1000; // Convert to ml
        $volume = max($volume, 1);
        $volume = round($volume);
        return $volume * $model3dPart->qty;
    }

    public static function calcualteModel3dPartSupportVolume(Model3dBasePartInterface $model3dPart)
    {
        $texture = $model3dPart->getCalculatedTexture();
        $filament = Model3dPartService::getModel3dPartInfo($model3dPart);

        if (!$filament) {
            return 0;
        }
        if (!$filament->area) {
            return 0;
        }


        $supportVolume = 0;
        if ($texture->calculatePrinterMaterialGroup()->isNeedSupports()) {
            $supportInfill = $texture->calculatePrinterMaterialGroup()->fill_percent / 100;
            $ks1 = $supportInfill / 1000;
            $supportVolume = $filament->supports_volume * $ks1;
        }

        return $supportVolume * $model3dPart->qty;
    }
}