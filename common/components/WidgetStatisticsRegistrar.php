<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.03.17
 * Time: 11:21
 */

namespace common\components;


use common\models\Ps;
use common\models\PsPrinter;
use common\models\User;
use common\models\WidgetStatistics;
use DateTime;
use DateTimeZone;
use Yii;

class WidgetStatisticsRegistrar
{
    /**
     * @param $type
     * @param User|PsPrinter|Ps|null $object
     * @param null $hostingPage
     */
    public static function registerWidgetStatistics($type, $object, $hostingPage = null)
    {
        if (!$hostingPage) {
            if (!array_key_exists('HTTP_REFERER', $_SERVER)) {
                return '';
            }
            $hostingPage = $_SERVER['HTTP_REFERER'];
        }
        if (0 === strpos($hostingPage, Yii::$app->params['siteUrl'])) {
            // Don`t register self url
            return;
        }

        $conditions = [
            'hosting_page' => $hostingPage,
            'type'         => $type,

        ];

        $ps = $psPrinter = $designer = null;
        if (($type === WidgetStatistics::TYPE_PS_SITE) || ($type === WidgetStatistics::TYPE_PS_FACEBOOK)) {
            $conditions['ps_id'] = $object->id;
            $ps = $object;
        } elseif ($type === WidgetStatistics::TYPE_PRINTER_SITE) {
            $conditions['ps_printer_id'] = $object->id;
            $psPrinter = $object;
        } elseif ($type === WidgetStatistics::TYPE_DESIGNER_SITE) {
            $conditions['designer_id'] = $object->id;
            $designer = $object;
        }

        /** @var WidgetStatistics $widgetStatistics */
        $widgetStatistics = WidgetStatistics::findOne($conditions);
        if (!$widgetStatistics) {
            $widgetStatistics = new WidgetStatistics();
            $widgetStatistics->type = $type;
            $widgetStatistics->hosting_page = $hostingPage;
            $widgetStatistics->ps_id = $ps ? $ps->id : null;
            $widgetStatistics->ps_printer_id = $psPrinter ? $psPrinter->id : null;
            $widgetStatistics->designer_id = $designer ? $designer->id : null;
            $widgetStatistics->first_visit = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        }
        $widgetStatistics->last_visit = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $widgetStatistics->views_count++;
        $widgetStatistics->safeSave();
    }
}