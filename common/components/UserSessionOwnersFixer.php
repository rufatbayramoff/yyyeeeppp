<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.12.16
 * Time: 10:02
 */

namespace common\components;

use common\components\order\anonim\OrderMover;
use common\models\CsWindowQuote;
use common\models\File;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\models\StoreOrder;
use common\models\User;
use common\models\UserSession;
use frontend\models\user\UserFacade;
use Yii;

class UserSessionOwnersFixer
{
    // Not for all models user session exists, if user session not exists, and user is is null, setup current user id

    public const FIX_MODELS_LIST = [
        Model3d::class        => [
            'model3dImgs',
            'model3dHistories'
        ],
        Model3dReplica::class => [
            'model3dReplicaImgs',
        ],
        File::class => [],
        CsWindowQuote::class => []
    ];

    public static function fixRelationData($relationData, $user)
    {
        if (is_array($relationData)) {
            foreach ($relationData as $relationElement) {
                if (!$relationElement->user_id) {
                    $relationElement->user_id = $user->id;
                    $relationElement->save(false);
                }
            }
        } else {
            if (!$relationData->user_id) {
                $relationData->user_id = $user->id;
                $relationData->save(false);
            }
        }
    }

    public static function fixModelsOwnersForUser(UserSession $userSession, User $user)
    {
        foreach (self::FIX_MODELS_LIST as $masterClass => $relations) {
            /** @var BaseAR $masterObj */
            $masterObj = Yii::createObject($masterClass);
            $masterModels = $masterObj::find()->user(null)->userSession($userSession)->all();
            foreach ($masterModels as $masterModel) {
                $masterModel->user_id = $user->id;
                $masterModel->save(false);
                foreach ($relations as $relationName => $relationInfo) {
                    if (is_array($relationInfo)) {
                        foreach ($relationInfo as $subRelation) {
                            foreach ($masterModel->$relationName as $relationData) {
                                $subRelationData = $relationData->$subRelation;
                                self::fixRelationData($subRelationData, $user);
                            }
                        }
                    } else {
                        $relationData = $masterModel->$relationInfo;
                        self::fixRelationData($relationData, $user);
                    }
                }
            }
        }


        // fix orders

        $anonimUser = UserFacade::getAnonimUser();

        /** @var StoreOrder $needFixOrders */
        $needFixOrders = StoreOrder::find()
            ->forUser($anonimUser)
            ->forUserSession($userSession)
            ->all();

        if ($needFixOrders) {
            $orderMover = new OrderMover();
            foreach ($needFixOrders as $needFixOrder) {
                $orderMover->move($needFixOrder, $anonimUser, $user);
            }
        }
    }
}