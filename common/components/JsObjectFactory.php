<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.06.16
 * Time: 14:42
 */

namespace common\components;

use yii\web\View;

class JsObjectFactory
{
    /**
     * Create Js object with params
     *
     * @param $className
     * @param $objectName
     * @param $initData
     * @param View $view
     * @return string
     */
    public static function createJsObject($className, $objectName, $initData, $view)
    {
        $script = "window.$objectName = commonJs.clone($className); $objectName.init(".json_encode($initData, JSON_UNESCAPED_UNICODE).");";
        $view->registerJs($script, $view::POS_END);
    }
}