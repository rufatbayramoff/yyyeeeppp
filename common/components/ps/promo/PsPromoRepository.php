<?php

namespace common\components\ps\promo;

use common\models\PrinterTechnology;
use common\models\Ps;
use yii\db\Expression;
use yii\db\Query;

class PsPromoRepository
{
    /**
     * @param int $limit
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getBest3dPrintServices($limit = 10): array
    {
        $query = Ps::find()->active();
        $query->innerJoin(['store_order' => $this->storeOrder()],'ps.id=store_order.user_id');
        $query->with('psCatalog');
        $query->orderBy(['store_order.cnt' => SORT_DESC]);
        $query->limit($limit);
        return $query->all();
    }

    /**
     * @return Query
     */
    protected function storeOrder(): Query
    {
        $query = new Query();
        $query->select(['cnt' => 'count(*)', 'user_id']);
        $query->from('store_order');
        $query->where(['between', 'created_at', new Expression('(NOW() - INTERVAL 30 DAY)'), new Expression('NOW()')]);
        $query->groupBy(['user_id']);
        return $query;
    }
}