<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\ps\locator;


use common\components\exceptions\AssertHelper;

/**
 * Class GeoRadius
 * @package common\components\ps\locator
 * @todo move to geo package
 */
class GeoRadius
{
    /**
     * @var Coord
     */
    private $center;

    /**
     * @var float
     */
    private $radius;

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @param Coord $center
     * @param       $radius
     * @return GeoRadius
     */
    public static function create(Coord $center, $radius)
    {
        AssertHelper::assertNumeric($radius, "Radius have bad value {$radius}");

        $geoRadius = new GeoRadius();
        $geoRadius->center = $center;
        $geoRadius->radius = $radius;
        return $geoRadius;
    }

    /**
     * @return Coord
     */
    public function getCenter()
    {
        return $this->center;
    }

    /**
     * @return float
     */
    public function getRadius()
    {
        return $this->radius;
    }
}