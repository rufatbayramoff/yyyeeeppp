<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\ps\locator\filter;

use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\components\ps\locator\GeoRadius;
use common\components\ps\locator\Size;
use common\models\DeliveryType;
use common\models\PrinterMaterialGroup;
use common\models\StoreOrderDelivery;
use common\models\Model3dTexture;

/**
 * Class Filter
 *
 * @package common\components\ps\locator
 */
class Filter implements FilterBuilderInterface, FilterInterface
{
    /**
     * @var GeoRadius
     */
    private $radius;

    /**
     * @var string[]
     */
    private $colors = [];

    /**
     * @var PrinterMaterialGroup
     */
    private $materialGroup = null;

    /**
     * @var string[]
     */
    private $materials = [];

    /**
     * @var string
     */
    private $country;

    /**
     * @var float
     */
    private $modelPriceInUSD;

    /**
     * @var float
     */
    private $weight;

    /**
     * @var Size
     */
    private $size;

    /**
     * @var Model3dTexture[]
     */
    private $textures;

    /**
     * @var DeliveryType
     */
    private $deliveryType;

    /**
     * @var StoreOrderDelivery
     */
    private $deliveryParams;


    /**
     * @var integer
     */
    private $maxOrdersCount;

    /**
     * @var bool
     */
    private $onlyCertificated;

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * @return FilterBuilderInterface
     */
    public static function builder()
    {
        return new Filter();
    }

    /** Builder implementation */

    /**
     * @param GeoRadius|float $radius
     * @return FilterBuilderInterface
     */
    public function forRadius(GeoRadius $radius)
    {
        $this->radius = $radius;
        return $this;
    }

    /**
     * @param $color
     * @return FilterBuilderInterface
     */
    public function forColor($color)
    {
        $this->colors = array_unique(ArrayHelper::merge($this->colors, (array)$color));
        return $this;
    }

    /**
     * @param $material
     * @return FilterBuilderInterface
     */
    public function forMaterial($material)
    {
        $this->materials = array_unique(ArrayHelper::merge($this->materials, (array)$material));
        return $this;
    }

    public function forMaterialGroup(PrinterMaterialGroup $materialGroup)
    {
        $this->materialGroup = $materialGroup;
        return $this;
    }

    /**
     * @param $country
     * @return FilterBuilderInterface
     */
    public function forCoutry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @param $maxPrice
     * @return FilterBuilderInterface
     */
    public function forModelPriceInUSD($maxPrice)
    {
        AssertHelper::assertNumeric($maxPrice, "Max price have bad value {$maxPrice}");
        $this->modelPriceInUSD = $maxPrice;
        return $this;
    }

    /**
     * @param DeliveryType $deliveryType
     * @param StoreOrderDelivery $deliveryParams
     * @return $this
     */
    public function forDeliveryType(DeliveryType $deliveryType, StoreOrderDelivery $deliveryParams = null)
    {
        $this->deliveryType = $deliveryType;
        $this->deliveryParams = $deliveryParams;
        return $this;
    }

    /**
     * @param Size $size
     * @return $this
     */
    public function forSize(Size $size)
    {
        $this->size = $size;
        return $this;
    }

    public function forMaxOrdersCount($ordersCount)
    {
        $this->maxOrdersCount = $ordersCount;
        return $this;
    }

    public function forTextures($textures)
    {
        $this->textures = $textures;
        return $this;
    }

    public function onlyCertificated()
    {
        $this->onlyCertificated = true;
        return $this;
    }

    /**
     * @param float $weight
     * @return $this
     */
    public function forWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return FilterInterface
     */
    public function build()
    {
        return $this;
    }

    /** Filter implementation */

    /**
     * @return GeoRadius
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return ArrayHelper::first($this->colors);
    }

    /**
     * @return string
     */
    public function getMaterial()
    {
        return ArrayHelper::first($this->materials);
    }

    public function getMaterialGroup()
    {
        return $this->materialGroup;
    }

    /**
     * @return string
     */
    public function getTextures()
    {
        return $this->textures;
    }

    /**
     * @return DeliveryType
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return float
     */
    public function getModelPriceInUSD()
    {
        return $this->modelPriceInUSD;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return Size
     */
    public function getSize()
    {
        return $this->size;
    }

    public function getMaxOrdersCount()
    {
        return $this->maxOrdersCount;
    }

    /**
     * @return FilterBuilderInterface
     */
    public function newBuilder()
    {
        return clone $this;
    }

    /**
     * @return StoreOrderDelivery
     */
    public function getDeliveryParams()
    {
        return $this->deliveryParams;
    }

    /**
     * @return bool
     */
    public function getIsOnlyCertificated()
    {
        return $this->onlyCertificated;
    }
}