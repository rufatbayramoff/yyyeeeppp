<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\ps\locator\filter;


use common\components\ps\locator\GeoRadius;
use common\components\ps\locator\Size;
use common\models\PsPrinter;

class FilterProcessor
{
    /**
     * filter given printers array by max distance in radius
     *
     * @param PsPrinter[] $printers
     * @param GeoRadius|int $radius
     * @return PsPrinter[]
     */
    public static function filterPrintersByRadius($printers, GeoRadius $radius)
    {
        $result = [];
        foreach ($printers as $psPrinter) {
            $printerDistance = $psPrinter->companyService->getDistance($radius->getCenter()->getLat(), $radius->getCenter()->getLon(), false);
            if ($printerDistance < $radius->getRadius()) {
                $result[] = $psPrinter;
            }
        }
        return $result;
    }


    /**
     * @param PsPrinter[] $printers
     * @return PsPrinter[]
     */
    public static function filterOnlyCertificated($printers)
    {
        return array_filter(
            $printers,
            function (PsPrinter $psPrinter) {
                return $psPrinter->isCertificated();
            }
        );
    }

    /**
     * @param $printers
     * @param Size $size
     * @return PsPrinter[]
     */
    public static function filterBySize($printers, Size $size): array
    {
        return array_filter(
            $printers,
            function (PsPrinter $psPrinter) use ($size) {
                return $psPrinter->canPrintSize($size);
            }
        );
    }
}