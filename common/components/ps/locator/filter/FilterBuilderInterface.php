<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\ps\locator\filter;


use common\components\ps\locator\GeoRadius;
use common\components\ps\locator\Size;
use common\models\DeliveryType;
use common\models\PrinterMaterialGroup;
use common\models\StoreOrderDelivery;

/**
 * Interface FilterBuilderInterface
 * @package common\components\ps\locator\filter
 */
interface FilterBuilderInterface
{
    /**
     * @param GeoRadius $radius
     * @return $this
     */
    public function forRadius(GeoRadius $radius);

    /**
     * @param string|string[] $color
     * @return $this
     */
    public function forColor($color);

    /**
     * @param string|string[] $material
     * @return $this
     */
    public function forMaterial($material);

    /**
     * @param PrinterMaterialGroup $materialGroup
     * @return $this
     */
    public function forMaterialGroup(PrinterMaterialGroup $materialGroup);

    /**
     * @param string $country
     * @return $this
     */
    public function forCoutry($country);


    /**
     * @param float $maxPrice
     * @return $this
     */
    public function forModelPriceInUSD($maxPrice);

    /**
     * @param DeliveryType $deliveryType
     * @param StoreOrderDelivery $deliveryParams
     * @return $this
     */
    public function forDeliveryType(DeliveryType $deliveryType, StoreOrderDelivery $deliveryParams = null);

    /**
     * @param float $weight
     * @return $this
     */
    public function forWeight($weight);

    /**
     * @param Size $size
     * @return $this
     */
    public function forSize(Size $size);


    /**
     * @param $ordersCount
     * @return mixed
     */
    public function forMaxOrdersCount($ordersCount);

    /**
     * @param $textures
     * @return mixed
     */
    public function forTextures($textures);

    /**
     * @return mixed
     */
    public function onlyCertificated();

    /**
     * @return FilterInterface
     */
    public function build();


}