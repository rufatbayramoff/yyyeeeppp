<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\ps\locator\filter;


use common\components\ps\locator\GeoRadius;
use common\components\ps\locator\Size;
use common\models\DeliveryType;
use common\models\PrinterMaterialGroup;
use common\models\StoreOrderDelivery;
use common\models\Model3dTexture;

/**
 * Interface FilterInterface
 *
 * @package common\components\ps\locator\filter
 */
interface FilterInterface
{
    /**
     * @return GeoRadius
     */
    public function getRadius();

    /**
     * @return string
     */
    public function getColor();

    /**
     * @return string
     */
    public function getMaterial();

    /**
     * @return PrinterMaterialGroup
     */
    public function getMaterialGroup();

    /**
     * @return Model3dTexture[]
     */
    public function getTextures();

    /**
     * @return DeliveryType
     */
    public function getDeliveryType();

    /**
     * @return string
     */
    public function getCountry();

    /**
     * @return float
     */
    public function getModelPriceInUSD();

    /**
     * @return float
     */
    public function getWeight();

    /**
     * @return Size
     */
    public function getSize();

    /**
     * @return integer
     */
    public function getMaxOrdersCount();

    /**
     * @return StoreOrderDelivery
     */
    public function getDeliveryParams();

    /**
     * @return FilterBuilderInterface
     */
    public function newBuilder();

    /**
     * @return bool
     */
    public function getIsOnlyCertificated();

}