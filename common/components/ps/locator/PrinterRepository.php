<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\ps\locator;


use common\models\CompanyService;
use common\models\StoreOrderAttemp;
use common\models\StoreOrder;
use common\models\User;
use common\models\UserLocation;
use common\models\PsPrinter;
use common\models\query\PsQuery;
use common\models\query\PsPrinterQuery;
use frontend\models\ps\PsFacade;
use lib\geo\GeoNames;
use lib\geo\LocationUtils;

class PrinterRepository
{
    /**
     * @param User|null $user Current user for which create bundle
     * @param GeoRadius $radius
     * @param           $countryIso
     * @param bool $onlyPickupDelivery
     * @return PsPrinter[]
     */
    public function getClosestPickupPrinters(User $user = null, GeoRadius $radius, $countryIso, $onlyPickupDelivery = true)
    {
        $area = LocationUtils::getPointArea($radius);

        $fromLat = round($area['from']['lat'], 5);
        $toLat = round($area['to']['lat'], 5);
        if ($fromLat > $toLat) {
            $tmp = $fromLat;
            $fromLat = $toLat;
            $toLat = $tmp;
        }
        $fromLon = round($area['from']['lon'], 5);
        $toLon = round($area['to']['lon'], 5);
        if ($fromLon > $toLon) {
            $tmp = $fromLon;
            $fromLon = $toLon;
            $toLon = $tmp;
        }
        $locLat = UserLocation::column('lat');
        $locLon = UserLocation::column('lon');

        $printersQuery = self::getCommonQuery($user);
        if ($onlyPickupDelivery) {
            $printersQuery = $printersQuery->pickupOnlyDelivery();
        } else {
            $printersQuery = $printersQuery->pickupDelivery();
        }
        $printers = $printersQuery
            ->joinWith('psMachine.location', false)
            ->andWhere(
                "($locLat between :fromLat AND :toLat) AND ($locLon between :fromLon AND :toLon)",
                [
                    ':fromLat' => $fromLat,
                    ':toLat'   => $toLat,
                    ':fromLon' => $fromLon,
                    ':toLon'   => $toLon,
                ]
            )
            ->andWhere([UserLocation::column('country_id') => GeoNames::getCountryByISO($countryIso)->id])
            ->all();

        return $printers;
    }

    /**
     * @param User|null $user Current user for which create bundle
     * @param string $countryIso
     * @return \common\models\PsPrinter[]
     */
    public function getDomesticPrinters(User $user = null, $countryIso)
    {
        return self::getCommonQuery($user)
            ->domesticDelivery($countryIso)
            ->all();
    }

    /**
     * @todo need sort?
     * @param User|null $user Current user for which create bundle
     * @return \common\models\PsPrinter[]
     */
    public function getInternationalPrinters(User $user = null)
    {
        return self::getCommonQuery($user)
            ->internationalDelivery()
            ->all();
    }

    /**
     * @param User $user
     * @return PsPrinterQuery
     */
    public function getUserPrintersQuery(User $user)
    {
        return PsPrinter::find()->forUser($user)->notDeleted();
    }

    /**
     * @param User $user
     * @return PsPrinter[]
     */
    public function getUserPrinters(User $user)
    {
        return $this->getUserPrintersQuery($user)->all();
    }


    /**
     * @param $user
     * @return PsPrinterQuery
     */
    public function getLastUserPrintersQuery($user)
    {
        $printerIds = StoreOrderAttemp::find()
            ->select(CompanyService::column('ps_printer_id'))
            ->joinWith('machine', false)
            ->forClientUser($user)
            ->inStatus(StoreOrderAttemp::STATUS_RECEIVED)
            ->orderBy(StoreOrder::column('id') . ' DESC')
            ->column();

        $printerIds = array_unique($printerIds);

        return self::getCommonQuery($user)
            ->andWhere([PsPrinter::column('id') => $printerIds]);
    }

    /**
     * @param User $user
     * @return \common\models\PsPrinter[]
     */
    public function getLastUserPrinters(User $user)
    {
        return $this->getLastUserPrintersQuery($user)->all();
    }

    /**
     * Get printers possible to public printing
     *
     * @return PsPrinterQuery
     */
    public static function getPublicActive()
    {
        return PsPrinter::find()
            ->joinWith(
                [
                    'ps' => function (PsQuery $query) {
                        $query
                            ->notExcludedFromPrinting(null)
                            ->active();
                    }
                ]
            )
            ->notDeleted()
            ->moderated()
            ->visible();
    }

    /**
     * @param User|null $user Current user for which create bundle
     * @return PsPrinterQuery
     */
    public static function getCommonQuery(User $user = null)
    {
        return PsPrinter::find()
            ->joinWith(
                [
                    'ps' => function (PsQuery $query) use ($user) {
                        $query
                            ->notExcludedFromPrinting($user ? PsFacade::getPsByUserId($user->id) : null)
                            ->moderated();
                    }
                ],
                false
            )
            ->joinWith('psMachine.deliveryTypess', false)
            ->notDeleted()
            ->moderated()
            ->visible()
            ->limit(100000);
    }
}