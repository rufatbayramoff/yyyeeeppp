<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.12.18
 * Time: 14:35
 */

namespace common\components\ps\locator\materials;


use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use common\models\PsPrinter;
use common\models\PsPrinterColor;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\models\repositories\PrinterMaterialRepository;
use common\modules\printersList\models\AnswerColorInfo;

class MaterialColorItem
{
    /**
     * Group Id
     *
     * @var int
     */
    public $groupId;

    /** @var PrinterMaterialGroup */
    protected $group;

    /**
     * Material id
     *
     * @var int
     */
    public $materialId;

    /** @var PrinterMaterial */
    protected $material;

    /**
     * Color id
     *
     * @var int
     */
    public $colorId;

    /** @var PrinterColor */
    protected $color;

    /**
     * Color Rating
     *
     * @var float
     */
    public $rating;

    public static function create(AnswerColorInfo $colorInfo)
    {
        $object = new MaterialColorItem();
        $object->groupId = $colorInfo->groupId;
        $object->materialId = $colorInfo->materialId;
        $object->colorId = $colorInfo->colorId;
        $object->rating = $colorInfo->rating;
        return $object;
    }

    /**
     * @return PrinterColor|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getColor(): ?PrinterColor
    {
        if (!$this->colorId) {
            return null;
        }
        if ($this->color) {
            return $this->color;
        }
        $printerColorRepository = \Yii::createObject(PrinterColorRepository::class);
        $this->color = $printerColorRepository->getById($this->colorId);
        return $this->color;
    }

    /**
     * @return PrinterMaterial
     * @throws \yii\base\InvalidConfigException
     */
    public function getMaterial(): ?PrinterMaterial
    {
        if (!$this->materialId) {
            return null;
        }
        if ($this->material) {
            return $this->material;
        }
        $printerMaterialRepository = \Yii::createObject(PrinterMaterialRepository::class);
        $this->material = $printerMaterialRepository->getById($this->materialId);
        return $this->material;
    }

    /**
     * @return PrinterMaterialGroup
     * @throws \yii\base\InvalidConfigException
     */
    public function getGroup(): ?PrinterMaterialGroup
    {
        if (!$this->groupId) {
            return null;
        }
        if ($this->group) {
            return $this->group;
        }
        $printerMaterialGroupRepository = \Yii::createObject(PrinterMaterialGroupRepository::class);
        $this->group = $printerMaterialGroupRepository->getById($this->groupId);
        return $this->group;
    }

}
