<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\ps\locator\materials;


use common\models\PrinterColor;
use common\models\PrinterMaterialGroup;
use common\models\PsPrinter;
use common\models\PsPrinterColor;

class MaterialColorsItem
{
    /**
     * @var PrinterMaterialGroup
     */
    public $materialGroup;

    /**
     * @var PrinterColor[]
     */
    public $colors;

    /**
     * Printer Color ratings
     *
     * @var array
     */
    public $colorRatings;


    /**
     * @var PsPrinterColor[]
     */
    public $psPrinterColors;

    /**
     * @var array
     */
    public $minPrintCosts;


    public $material;

    /**
     * with keys color_id and value ps_id
     * @var array
     */
    public $printers;

    /**
     * @var array [materialId => [PrinterColor, PrinterColor, ...]]
     */
    public $colorsGroupedByMaterialId = [];
}