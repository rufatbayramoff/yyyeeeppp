<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\ps\locator;
use common\components\exceptions\AssertHelper;

/**
 * Class Coord
 * @package common\components\ps\locator
 * @todo move to geo package
 */
class Coord
{
    /**
     * @var float
     */
    private $lat;

    /**
     * @var float
     */
    private $lon;

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * Float
     * @param $lat
     * @param $lon
     * @return Coord
     */
    public static function create($lat, $lon)
    {
        AssertHelper::assertNumeric($lat, "Lat is bad {$lat}");
        AssertHelper::assertNumeric($lon, "Lon is bad {$lon}");

        $coord = new Coord();
        $coord->lat = $lat;
        $coord->lon = $lon;
        return $coord;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }
}