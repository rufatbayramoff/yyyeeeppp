<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\ps\locator;

use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\models\FilepageSize;
use frontend\models\user\UserFacade;
use lib\MeasurementUtil;
use yii\base\InvalidArgumentException;

/**
 * Class Size
 *
 * @package common\components\ps\locator
 * @todo move to dimensions package
 */
class Size
{
    /**
     * @var float
     */
    public $length = 0;

    /**
     * @var float
     */
    public $width = 0;

    /**
     * @var float
     */
    public $height = 0;

    public $measure = self::TYPE_MM;

    const TYPE_MM = 'mm';
    const TYPE_CM = 'cm';
    const TYPE_INCH = 'in';

    const MAX_SIZE = 9999999;
    const MIN_SIZE = 0.001;

    /**
     *
     */
    private function __construct()
    {
    }

    /**
     * Factory
     *
     * @param float $width
     * @param float $height
     * @param float $length
     * @param string $measure
     * @return Size
     */
    public static function create($width, $height, $length, $measure = self::TYPE_MM)
    {
        AssertHelper::assertNumeric($width, "Width has bad value {$width}", BusinessException::class);
        AssertHelper::assertNumeric($height, "Height has bad value {$height}",  BusinessException::class);
        AssertHelper::assertNumeric($length, "Length has bad value {$length}",  BusinessException::class);

        $size = new Size();

        $size->width = (float)$width;
        $size->height = (float)$height;
        $size->length = (float)$length;
        $size->measure = $measure;
        return $size;
    }

    public static function createByFilepageSize(FilepageSize $filepageSize)
    {
        $size = new Size();

        $size->width = (float)$filepageSize->width;
        $size->height = (float)$filepageSize->height;
        $size->length = (float)$filepageSize->length;
        $size->measure = self::TYPE_MM;
        return $size;
    }


    public static function createBySizeString($sizeString)
    {
        $sizeArray = explode('x', $sizeString);
        rsort($sizeArray);

        $size = new Size();
        $size->width = (float)$sizeArray[0];
        $size->height = (float)$sizeArray[1];
        $size->length = (float)$sizeArray[2];
        $size->measure = self::TYPE_MM;
        return $size;

    }

    /**
     * @param $scaleBy
     * @return Size
     */
    public function scale($scaleBy)
    {
        $width = $this->width * $scaleBy;
        $height = $this->height * $scaleBy;
        $length = $this->length * $scaleBy;
        return self::create($width, $height, $length, $this->measure);
    }

    public function round()
    {
        $this->width = round($this->width, 2);
        $this->height = round($this->height, 2);
        $this->length = round($this->length, 2);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaxSide()
    {
        return max([$this->width, $this->height, $this->length]);
    }

    /**
     * get min from WxHxL
     *
     * @return mixed
     */
    public function getMinSide()
    {
        return min([$this->width, $this->height, $this->length]);
    }

    /**
     * convert to user measurement if required
     *
     * @param string $newMeasure
     * @return \common\components\ps\locator\Size
     */
    public function convertToMeasurement($newMeasure)
    {
        if ($this->measure !== $newMeasure) {
            $width = MeasurementUtil::convertSize($this->width, $this->measure, $newMeasure);
            $height = MeasurementUtil::convertSize($this->height, $this->measure, $newMeasure);
            $length = MeasurementUtil::convertSize($this->length, $this->measure, $newMeasure);
            return self::create($width, $height, $length, $newMeasure);
        }
        $clone = clone $this;
        return $clone;
    }

    public function toAlignedString()
    {
        $p1 = $this->width;
        $p2 = $this->height;
        $p3 = $this->length;
        if ($p2 > $p1) {
            $p = $p1;
            $p1 = $p2;
            $p2 = $p;
        }
        if ($p3 > $p1) {
            $p = $p1;
            $p1 = $p3;
            $p3 = $p;
        }
        if ($p3 > $p2) {
            $p = $p2;
            $p2 = $p3;
            $p3 = $p;
        }

        return str_pad((int)$p3, 8, '0', STR_PAD_LEFT) . 'x' . str_pad((int)$p2, 8, '0', STR_PAD_LEFT) . 'x' . str_pad((int)$p1, 8, '0', STR_PAD_LEFT);
    }

    /**
     * Return true if this size more then $size
     *
     * @param Size $size
     * @return bool
     */
    public function isMoreThen(Size $size)
    {
        $thisSizesData = $this->toArray();
        $otherSizesData = $size->toArray();

        sort($thisSizesData, SORT_NUMERIC);
        sort($otherSizesData, SORT_NUMERIC);

        for ($i = 0; $i < 3; $i++) {
            if ($thisSizesData[$i] <= $otherSizesData[$i]) {
                return false;
            }
        }
        return true;
    }

    public function isMoreOrEqualThen(Size $size)
    {
        // localSize
        $p1 = $this->width;
        $p2 = $this->height;
        $p3 = $this->length;
        if ($p2 > $p1) {
            $p = $p1;
            $p1 = $p2;
            $p2 = $p;
        }
        if ($p3 > $p1) {
            $p = $p1;
            $p1 = $p3;
            $p3 = $p;
        }
        if ($p3 > $p2) {
            $p = $p2;
            $p2 = $p3;
            $p3 = $p;
        }

        // getSize
        $o1 = $size->width;
        $o2 = $size->height;
        $o3 = $size->length;
        if ($o2 > $o1) {
            $o = $o1;
            $o1 = $o2;
            $o2 = $o;
        }
        if ($o3 > $o1) {
            $o = $o1;
            $o1 = $o3;
            $o3 = $o;
        }
        if ($o3 > $o2) {
            $o = $o2;
            $o2 = $o3;
            $o3 = $o;
        }


        if ($p1 >= $o1 && $p2 >= $o2 && $p3 >= $o3) {
            return true;
        }
        return false;
    }

    public function isEqual(Size $size)
    {
        $thisSizesData = $this->toArray();
        $otherSizesData = $size->toArray();

        sort($thisSizesData, SORT_NUMERIC);
        sort($otherSizesData, SORT_NUMERIC);

        for ($i = 0; $i < 3; $i++) {
            if ($thisSizesData[$i] != $otherSizesData[$i]) {
                return false;
            }
        }
        return true;
    }

    public function isZero()
    {
        return ($this->width === 0) && ($this->height === 0) && ($this->length=== 0);
    }

    public function validateOrFail()
    {
        if (($this->width>self::MAX_SIZE) || ($this->height>self::MAX_SIZE) || ($this->length>self::MAX_SIZE)) {
            throw new InvalidArgumentException('Max size invalid');
        }

        if (($this->width<self::MIN_SIZE) || ($this->height<self::MIN_SIZE) || ($this->length<self::MIN_SIZE)) {
            throw new InvalidArgumentException('Min size invalid');
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [$this->width, $this->height, $this->length];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode(' x ', $this->toArray());
    }

    public function toStringWithMeasurement($measure = null)
    {
        // default is mm, convert to in
        // 1 oz = 25.4 mm
        $currentMeasurement = $measure ? $measure : UserFacade::getCurrentMeasurement();
        $sizes = [
            $this->width,
            $this->height,
            $this->length,
        ];
        if ($currentMeasurement === self::TYPE_INCH) {
            $currentMeasurement = _t('site.user', 'in');
            foreach ($sizes as $k => $v) {
                $sizes[$k] = round($v / 25.4, 2);
            }
        } elseif ($currentMeasurement === 'cm' || $currentMeasurement === self::TYPE_MM) {
            foreach ($sizes as $k => $v) {
                $sizes[$k] = round($v, 2);
            }
            $currentMeasurement = _t('site.user', 'mm');
        }
        return implode(' x ', $sizes) . ' ' . $currentMeasurement;
    }

    /**
     * get max w x h x l props in model3d files
     *
     * @param array $props
     * @return Size
     */
    public static function getMaxFileSize($props)
    {
        $max = [
            'width'  => 0,
            'height' => 0,
            'length' => 0
        ];
        foreach ($props as $prop) {
            $max['width'] = (float)$prop['width'] > $max['width'] ? (float)$prop['width'] : $max['width'];
            $max['height'] = (float)$prop['height'] > $max['height'] ? (float)$prop['height'] : $max['height'];
            $max['length'] = (float)$prop['length'] > $max['length'] ? (float)$prop['length'] : $max['length'];
        }
        return Size::create($max['width'], $max['height'], $max['length']);
    }
}
