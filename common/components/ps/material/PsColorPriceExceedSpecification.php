<?php
/**
 * Created by mitaichik
 */

namespace common\components\ps\material;

use common\models\CompanyService;
use common\models\PsPrinter;
use common\models\query\PsPrinterQuery;
use frontend\components\UserSessionFacade;
use common\components\PaymentExchangeRateFacade;
use lib\money\Currency;

/**
 * Class PriceExceedResolver
 * @package common\components\ps\material
 */
class PsColorPriceExceedSpecification
{
    /**
     * Default recomend min usd per gramm for print
     * @var float
     */
    public $defaultRecomendPricePerGrammMin = 0.2;

    /**
     * Default recomend max usd per gramm for print
     * @var float
     */
    public $defaultRecomendPricePerGrammMax = 10.0;

    public function getSatisfiedIds(): array
    {
        $psPrintersQuery = PsPrinter::find()
            ->visible()
            ->inModerationStatuses(CompanyService::pendingModerationStatusList())
            ->joinWith([
                'psPrinterMaterials.material',
                'psPrinterMaterials.psPrinterColors',
            ], false);
        $psPrinters = $psPrintersQuery->all();
        $ids = [];
        foreach ($psPrinters as $psPrinter) {
            if ($this->isSatisfied($psPrinter)) {
                $ids[$psPrinter->id] = $psPrinter->id;
            }
        }
        return $ids;
    }

    public function satisfiedCount()
    {
        return count($this->getSatisfiedIds());
    }

    /**
     * Prepare query for select printers with only exceed recomend price of color
     * @param PsPrinterQuery|null $query If null - it create and return new query, else - prepare exist query
     * @return PsPrinterQuery
     */
    public function satisfiedQuery(PsPrinterQuery $query)
    {
        $ids = $this->getSatisfiedIds();
        $query->andWhere(['`ps_printer`.`id`' => array_values($ids)]);
        return $query;
    }

    /**
     * Is printer have materials witch exceed colors
     * @param PsPrinter $printer
     *
     * @return bool
     */
    public function isSatisfied(PsPrinter $printer)
    {
        foreach ($printer->materials as $psMaterial){

            $min = PaymentExchangeRateFacade::convert($psMaterial->material->recomend_price_per_gramm_min ?: $this->defaultRecomendPricePerGrammMin, Currency::USD, $printer->companyService->company->currency);
            $max = PaymentExchangeRateFacade::convert($psMaterial->material->recomend_price_per_gramm_max ?: $this->defaultRecomendPricePerGrammMax, Currency::USD, $printer->companyService->company->currency);

            foreach ($psMaterial->colors as $psColor){
                if($psColor->price < $min || $psColor->price > $max){
                    return true;
                }
            }
        }
        return false;
    }
}