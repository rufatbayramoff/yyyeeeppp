<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.10.17
 * Time: 13:38
 */
namespace common\components\exceptions;


use yii\web\HttpException;

class NoActiveModel3d extends HttpException
{
    public function __construct($status, $message = null, $code = 0, \Exception $previous = null)
    {
        $status = 418;
        parent::__construct($status, $message, $code, $previous);
    }
}