<?php

namespace common\components\exceptions;


use yii\base\Model;
use yii\web\HttpException;

/**
 * Validation model exception
 * @package common\components\exceptions
 */
class ValidationException extends HttpException implements ValidationExceptionInterface
{
    /**
     * Invalid models
     * @var Model[]
     */
    protected $models;


    /**
     * @param $models
     * @param string|null $message
     */
    public function __construct($models, $message = null)
    {
        $this->models              = $models;
        if ($message === null) {
            $message = 'Data Validation Failed.';
        }
        parent::__construct(422, $message);
    }

    /**
     * Convert error (or errors) to array of errors
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function errorsArray()
    {
        $formErrors = [];
        foreach ($this->models as $model) {
            $formName   = $model->formName();
            foreach ($model->getFirstErrors() as $key => $value) {
                $formErrors[$formName . '[' . $key . ']'] = $value;
            }
        }
        return $formErrors;
    }
}