<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\exceptions;


use yii\base\Exception;

class AssertException extends Exception
{

}