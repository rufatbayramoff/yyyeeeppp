<?php
namespace common\components\exceptions;

use Throwable;
use yii\web\HttpException;

class OnlyPostRequestException extends BusinessException {
    public function __construct($message = 'Only post request', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}