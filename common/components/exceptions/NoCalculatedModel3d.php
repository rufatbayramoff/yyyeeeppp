<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.02.18
 * Time: 14:02
 */
namespace common\components\exceptions;

use yii\web\HttpException;

class NoCalculatedModel3d extends HttpException
{
    public function __construct($status, $message = null, $code = 0, \Exception $previous = null)
    {
        $status = 419;
        parent::__construct($status, $message, $code, $previous);
    }
}