<?php
/**
 * Created by mitaichik
 */

namespace common\components\exceptions;

/**
 * Interface ErrorCodeExceptionInterface
 * @package common\components\exceptions
 */
interface ErrorCodeExceptionInterface
{
    public function getErrorCode();
}