<?php


namespace common\components\exceptions;



class ParcelCalculateException extends BusinessException implements ErrorCodeExceptionInterface, DataExceptionInterface
{
    protected $orderId;

    public function __construct(int $orderId, string $message)
    {
        parent::__construct($message);
        $this->orderId = $orderId;
    }

    public function getErrorCode()
    {
       return 'ParcelCalcFailed';
    }

    public function getData()
    {
        return ['id' => $this->orderId];
    }
}