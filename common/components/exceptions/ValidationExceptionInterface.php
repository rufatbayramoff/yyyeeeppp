<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\exceptions;


interface ValidationExceptionInterface
{
    /**
     * Convert error (or errors) to array of errors
     * @return array
     */
    public function errorsArray();
}