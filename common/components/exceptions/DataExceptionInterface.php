<?php
/**
 * Created by mitaichik
 */

namespace common\components\exceptions;


interface DataExceptionInterface
{
    public function getData();
}