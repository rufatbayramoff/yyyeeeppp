<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\exceptions;


use yii\base\Model;
use yii\web\HttpException;

/**
 * Invalid model exception
 * @package common\components\exceptions
 */
class InvalidModelException extends HttpException implements ArrayErrorsExceptionInterface
{
    /**
     * Invalid model
     * @var Model
     */
    private $model;

    /**
     * @param Model $model
     * @param string|null $message
     */
    public function __construct(Model $model, $message = null)
    {
        $this->model = $model;

        if ($message === null) {
            $message = 'Data Validation Failed.';
            if (YII_DEBUG) {
                $message .= ' ' . print_r($this->errorsArray(), true);
            }
        }
        parent::__construct(422, $message);
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Convert error (or errors) to array of errors
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function errorsArray()
    {
        return $this->model->getFirstErrors();
    }
}