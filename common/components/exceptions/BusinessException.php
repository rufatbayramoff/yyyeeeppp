<?php

namespace common\components\exceptions;

use yii\base\UserException;

/**
 * Class BusinessException
 *
 * Business logic exception. Not fatal errors.
 *
 * @package common\components\exceptions
 */
class BusinessException extends UserException
{
}
