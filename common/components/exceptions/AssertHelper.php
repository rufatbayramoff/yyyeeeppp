<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */
namespace common\components\exceptions;
use yii\base\Model;
use yii\db\ActiveRecordInterface;
use yii\helpers\Json;

/**
 * Хелпер для проверки утверждений
 */
class AssertHelper
{
    /**
     * Проверяет условие, если false - ызывает эксепшн
     * @param $condition
     * @param null $errorMessage
     * @param string $exceptionClass
     */
    public static function assert($condition, $errorMessage = null, $exceptionClass = AssertException::class)
    {
        if (!$condition) {
            $errorMessage = $errorMessage ?: 'Error';
            throw new $exceptionClass($errorMessage);
        }
    }

    /**
     * Сохраняет модель. Если не получается - эксепшн
     * @param ActiveRecordInterface|Model $model
     * @param null $message
     * @throws InvalidModelException
     */
    public static function assertSave(ActiveRecordInterface $model, $message = null)
    {
        if(!$model->save())
        {
            throw new InvalidModelException($model, \yii\helpers\Html::errorSummary($model));
        }
    }

    /**
     * Проверяет валидность модели
     * @param Model $model
     * @param null|string $errorMessage
     * @param string $exceptionClass
     */
    public static function assertValidate(Model $model, $errorMessage = null, $exceptionClass = InvalidModelException::class)
    {
        if (!$model->validate())
        {
            throw new $exceptionClass($model, $errorMessage);
        }
    }

    /**
     * Assert that $value is numeric
     * @param      $value
     * @param null $errorMessage
     * @param      $class
     */
    public static function assertNumeric($value, $errorMessage = null, $class = \InvalidArgumentException::class)
    {
        $errorMessage = $errorMessage ?: "Invalid numeric value {$value}";
        self::assert(is_numeric($value), $errorMessage, $class);
    }

    /**
     * Assert that $value is string
     * @param      $value
     * @param null $errorMessage
     * @param      $class
     */
    public static function assertString($value, $errorMessage = null, $class = \InvalidArgumentException::class)
    {
        $errorMessage = $errorMessage ?: "Invalid string value {$value}";
        self::assert(is_string($value), $errorMessage, $class);
    }
    
    /**
     * Assert that $value is string
     * @param      $value
     * @param null $errorMessage
     * @param      $class
     */
    public static function assertBool($value, $errorMessage = null, $class = \InvalidArgumentException::class)
    {
        $errorMessage = $errorMessage ?: "Invalid bool value {$value}";
        self::assert(is_bool($value), $errorMessage, $class);
    }

    /**
     * Check that objects in array instance of $class
     * @param array $objects
     * @param       $class
     * @param null  $errorMessage
     * @param       $exceptionClass
     */
    public static function assertInstances(array $objects, $class, $errorMessage = null, $exceptionClass = \InvalidArgumentException::class)
    {
        foreach($objects as $index => $object)
        {
            if(!($object instanceof $class))
            {
                $errorMessage = $errorMessage ?: "Item {$index} not instance of {$class}";
                throw new $exceptionClass($errorMessage);
            }
        }
    }

    /**
     * @param $object
     * @param $class
     * @param null $errorMessage
     * @param $exceptionClass
     */
    public static function assertInstance($object, $class, $errorMessage = null, $exceptionClass = \InvalidArgumentException::class)
    {
        if(!($object instanceof $class))
        {
            $errorMessage = $errorMessage ?: "Object not instance of {$class}";
            throw new $exceptionClass($errorMessage);
        }
    }

    /**
     * Check that objects in array instance of $class
     * @param       $needle
     * @param array $array
     * @param null  $errorMessage
     * @param       $exceptionClass
     */
    public static function assertInArray($needle, array $array, $errorMessage = null, $exceptionClass = \InvalidArgumentException::class)
    {
        if(!in_array($needle, $array))
        {
            $errorMessage = $errorMessage ?: "Item not in array";
            throw new $exceptionClass($errorMessage);
        }
    }

    /**
     * Check that model is not new.
     *
     * @param ActiveRecordInterface $model
     * @param null $message
     */
    public static function assertNotNew(ActiveRecordInterface $model, $message = null)
    {
        self::assert(!$model->getIsNewRecord(), $message);
    }

    /**
     * Delete model.
     *
     * @param ActiveRecordInterface $model
     * @param null $message
     */
    public static function assertDelete(ActiveRecordInterface $model, $message = null)
    {
        self::assert($model->delete() !== false, $message);
    }
}