<?php

namespace common\components;

class CookieSameSiteDetector
{
    public static function isSameSiteCookies()
    {
        if ($_SERVER && array_key_exists('REQUEST_URI', $_SERVER) &&
            (strpos($_SERVER['REQUEST_URI'], 'widget') ||
                strpos($_SERVER['REQUEST_URI'], 'do-restore-session') ||
                strpos($_SERVER['REQUEST_URI'], 'set-session') ||
                strpos($_SERVER['REQUEST_URI'], 'onshape/app')
            )
        ) {


            return true;
        }

        return false;
    }
}
