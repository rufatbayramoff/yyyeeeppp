<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.10.16
 * Time: 11:46
 */

namespace common\components;

use ReflectionClass;
use yii\di\Instance;

class Container extends \yii\di\Container {
    protected function build($class, $params, $config)
    {
        $object = parent::build($class, $params, $config);
        list($reflection, $dependencies) = parent::getDependencies($class);

        if ($reflection) {
            /** @var ReflectionClass $reflection */
            $reflection = new ReflectionClass($class);
            if ($reflection->hasMethod('injectDependencies')) {
                $methodDependencies = [];
                $diMethod = $reflection->getMethod('injectDependencies');
                foreach ($diMethod->getParameters() as $param) {
                    if ($param->isDefaultValueAvailable()) {
                        $methodDependencies[] = $param->getDefaultValue();
                    } else {
                        $c = $param->getType();
                        $methodDependencies[] = Instance::of($c === null ? null : $c->getName());
                    }
                }
                if ($methodDependencies) {
                    $methodDependencies = $this->resolveDependencies($methodDependencies, $reflection);
                    call_user_func_array([$object,'injectDependencies'], $methodDependencies);
                }
            }
        }
        return $object;
    }
}