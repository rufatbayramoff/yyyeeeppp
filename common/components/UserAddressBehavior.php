<?php
namespace common\components;

use yii\db\ActiveRecord;

/**
 * UserAddressBehavior - helps to create table inheritance in database from user_address table
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserAddressBehavior extends \yii\base\Behavior
{
    /**
     * how is owner extended from user_addresss table
     * @var type 
     */
    public $fk = 'address_id';
    public $pk = 'id';
    public $className;
    public $attributes;
    public function init()
    {
        parent::init();
        $this->className = \common\models\UserAddress::className();
    }
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }
    public function attach($owner)
    {
        parent::attach($owner);

        if (empty($owner->{$this->fk})) {
            //throw new \yii\base\InvalidConfigException('Please specify fk for attached owner', 101);
        }
    }

    /**
     * Relation to model 
     * @param $language
     * @return ActiveQuery
     */
    public function getAddress()
    { 
        return $this->owner->hasOne($this->className, [$this->fk => $this->pk]);
    }

    public function beforeValidate()
    {
        foreach ($this->owner->attributes as $attribute) {            
        }
    }
    
    public function beforeInsert($event)
    {
        
    }
   
    public function beforeUpdate($event)
    {
        
    } 
    public function afterInsert($event)
    {
        
    }
    public function afterUpdate($event)
    {
        
    }
    public function afterFind()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner; 
        foreach ($this->attributes as $attribute) {
            if ($owner->hasAttribute($attribute)) {
                $owner->setAttribute($attribute, $attribute);
            }
        }
    }
}
