<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.02.17
 * Time: 13:34
 */



namespace common\components;

use Psr\Log\InvalidArgumentException;
use yii\base\InvalidParamException;

class FaceBookHelper
{

    protected static function base64UrlDecode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }


    public static function decodeSignedRequest(  $input,   $secret,   $max_age = 3600)
    {
        list($encoded_sig, $encoded_envelope) = explode('.', $input, 2);
        $envelope = json_decode(self::base64UrlDecode($encoded_envelope), true);
        $algorithm = $envelope['algorithm'];
        if ($algorithm !== 'HMAC-SHA256') {
            throw new InvalidArgumentException('Invalid request. (Unsupported algorithm.)');
        }
        if ($envelope['issued_at'] < time() - $max_age) {
            throw new InvalidArgumentException('Invalid request. (Too old.)');
        }
        if (self::base64UrlDecode($encoded_sig) !== hash_hmac('sha256', $encoded_envelope, $secret, $raw = true)
        ) {
            throw new InvalidArgumentException('Invalid request. (Invalid signature.)');
        }
        return $envelope;
    }
}