<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.11.17
 * Time: 17:06
 */

namespace common\components;

class ErrorListHelper
{
    static public function addErrorsFormPrefix($errorsList, $prefix)
    {
        $returnValue = [];
        foreach ($errorsList as $key => $value) {
            $textValue = reset($value); // Get first error
            $returnValue[$prefix.'-'.$key] = $textValue;
        }
        return $returnValue;
    }
}