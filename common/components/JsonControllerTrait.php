<?php
/**
 * Date: 30.11.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\components;


use yii\web\Response;

trait JsonControllerTrait
{
    /**
     * used to return json in Controllers
     *
     * @param array|string $data
     * @param int|null $status Http status
     * @return array
     */
    public function jsonReturn($data, $status = null)
    {
        if($status){
            \Yii::$app->response->statusCode = $status;
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    /**
     *
     * @param array $data
     * @return array
     */
    public function jsonSuccess(array $data = [])
    {
        return $this->jsonReturn(['success' => true] + $data);
    }

    /**
     * returns failure with data
     *
     * @param array $data
     * @return array
     */
    public function jsonFailure(array $data)
    {
        return $this->jsonReturn(array_merge(['success' => false], $data));
    }

    /**
     *
     * @param string $msg
     * @return array
     */
    public function jsonMessage($msg)
    {
        return $this->jsonReturn(['success' => true, 'message' => $msg]);
    }

    /**
     *
     * @param string $msg
     * @return array
     */
    public function jsonError($msg)
    {
        return $this->jsonReturn(['success' => false, 'message' => $msg], 406);
    }
}