<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components;

/**
 * Helper for work with PhpDoc annotations
 * @package common\components
 */
class AnnotationHelper
{
    /**
     * @param string|object $class Class name or object instance
     * @param string $method Method name
     * @param string $annotation Annotation name
     * @return bool
     */
    public static function isMethodAnnotationPresent($class, $method, $annotation)
    {
        $classRef = new \ReflectionClass($class);
        $methodRef = $classRef->getMethod($method);
        $phpDoc = $methodRef->getDocComment();
        return strpos($phpDoc, self::generateAnnotationName($annotation)) !== false;
    }

    /**
     * Generate annotation name
     * @param $annotation
     * @return string
     */
    private static function generateAnnotationName($annotation)
    {
        return '@'.$annotation;
    }
}