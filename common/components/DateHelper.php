<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.04.17
 * Time: 10:03
 */

namespace common\components;

use DateInterval;
use DateTime;
use DateTimeZone;

class DateHelper
{
    public static function now($timezone = 'UTC'): string
    {
        return (new DateTime('now', new DateTimeZone($timezone)))->format('Y-m-d H:i:s');
    }

    public static function today($timezone = 'UTC'): string
    {
        return (new DateTime('now', new DateTimeZone($timezone)))->format('Y-m-d');
    }


    public static function subNow($interval): string
    {
        return (new DateTime('now', new DateTimeZone('UTC')))->sub(new DateInterval($interval))->format('Y-m-d H:i:s');
    }

    public static function subNowSec(int $sec): string
    {
        return self::subNow('PT' . $sec . 'S');
    }

    public static function add($date, $interval): string
    {
        return (new DateTime($date, new DateTimeZone('UTC')))->add(new DateInterval($interval))->format('Y-m-d H:i:s');
    }

    public static function addNow($interval): string
    {
        return (new DateTime('now', new DateTimeZone('UTC')))->add(new DateInterval($interval))->format('Y-m-d H:i:s');
    }

    public static function addNowSec(int $sec): string
    {
        return self::addNow('PT' . $sec . 'S');
    }

    public static function convert($time, $currentTimezon, $newTimezone): string
    {
        $date = new DateTime($time, new DateTimeZone($currentTimezon));
        $date->setTimezone(new DateTimeZone($newTimezone));
        return $date->format('Y-m-d H:i:s');
    }

    public static function strtotimeUtc($time): int
    {
        $utc = new DateTimeZone('UTC');
        $dt  = new DateTime($time, $utc);
        return $dt->format('U');
    }

    public static function filterDate(string $date): string
    {
        return date('Y-m-d', strtotime($date));
    }

    /**
     * @param $time
     * @return DateInterval|false
     * @throws \Exception
     */
    public static function diffNow($time)
    {
        return (new DateTime('now', new DateTimeZone('UTC')))->diff(new DateTime($time,new DateTimeZone('UTC')));
    }

    /**
     * @param int $day
     * @return int
     */
    public static function dayToSeconds(int $day): int
    {
        return $day * 24 * 60 * 60;
    }
}