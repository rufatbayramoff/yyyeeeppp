<?php


namespace common\components\helpers;


class ResponseTimeHelper
{
    public const TWO_HOUR = 2;
    public const TWENTY_HOUR = 12;
    public const TWENTY_FOUR_HOUR = 24;
    public const FORTY_EIGHT_HOUR = 48;

    /**
     * @param int|null $response
     * @return string|null
     */
    public static function response(?int $response): ?string
    {
        if($response === null) {
            return _t('site.ps','more').' '.self::FORTY_EIGHT_HOUR.'h';
        }
        if($response < self::TWO_HOUR) {
            return _t('site.ps','less').' '.self::TWO_HOUR.'h';
        }
        if($response < self::TWENTY_HOUR) {
            return _t('site.ps','less').' '.self::TWENTY_HOUR.'h';
        }
        if($response < self::TWENTY_FOUR_HOUR) {
            return _t('site.ps','less').' '.self::TWENTY_FOUR_HOUR.'h';
        }
        if($response < self::FORTY_EIGHT_HOUR) {
            return _t('site.ps','less').' '.self::FORTY_EIGHT_HOUR.'h';
        }
        return _t('site.ps','more').' '.self::FORTY_EIGHT_HOUR.'h';
    }
}