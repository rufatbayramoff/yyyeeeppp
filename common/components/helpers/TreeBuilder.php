<?php
/**
 * User: nabi
 */

namespace common\components\helpers;


class TreeBuilder
{
    /***
     * Base in data
     *
     * @var array
     */
    protected $data = [];

    protected $relations = [];

    /***
     * Tree data
     *
     * @var array
     */
    protected $tree = [];

    /***
     * Array of items for tree building
     *
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }


    public function build()
    {
        $items = &$this->getData();
        $this->buildDescendantsRelations();
        foreach ($items as $k => $v) {
            if ($v['parent_id'] === null) {
                array_push($this->tree, $this->buildNodeTree($v, $this->relations));
            }
        }
        return $this->tree;
    }

    public function buildNodeTree($root_id = 0, &$descendants = [])
    {
        $tree = $root_id;
        $root_id = $tree['root_id'];
        if (isset($descendants[$root_id])) {
            foreach ($descendants[$root_id] as &$item) {
                $tree['descendants'][] = $this->buildNodeTree($item, $descendants);
            }
            unset($item);
        }
        return $tree;
    }

    /*
     * Build descendants relations
     * Array root_id => array children_id
     * */
    public function buildDescendantsRelations()
    {
        $items = &$this->getData();
        foreach ($items as $k => $v) {
            $id = ($v['parent_id'] === null) ? null : (int)$v['parent_id'];
            if ($id !== null) {
                if (!isset($this->relations[$id])) {
                    $this->relations[$id] = [];
                }
                array_push($this->relations[$id], $k);
            }
        }
    }

    public function getTree()
    {
        return $this->tree;
    }

    public function &getRelations()
    {
        return $this->relations;
    }

    protected function &getData()
    {
        return $this->data;
    }
}