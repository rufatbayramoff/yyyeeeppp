<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.04.19
 * Time: 9:38
 */

namespace common\components\helpers;

class BenchmarkHelper
{
    public static $logStart = '';
    public static $log = '';

    public static function logDebug($text)
    {
        if (!self::$logStart) {
            self::$logStart = microtime(true);
        }
        self::$log .= date('H-i-s') . ' ' . round(microtime(true) - self::$logStart, 3) . " \t" . $text . "\n";
    }

    public static function flush()
    {
        $fname = '/var/log/bench/' . date('Y-m-d_H-i-s') . '_' . round(microtime(true) - self::$logStart, 3) . '_' . mt_rand(0, 99999);
        file_put_contents($fname, self::$log, FILE_APPEND);
    }
}