<?php

namespace common\components\helpers;

class PeliabilityHelper
{
    public const HIGH_CONVERSION = 'high';
    public const GOOD_CONVERSION = 'good';
    public const MIDDLE_CONVERSION = 'average';
    public const NO_INFO_CONVERSION = 'no info';

    /**
     * @param $peliability
     * @return string
     */
    public static function format($peliability): string
    {
        $conversion = (int)$peliability;
        if($conversion > 60) {
            return  self::HIGH_CONVERSION;
        }
        if($conversion > 50) {
            return  self::GOOD_CONVERSION;
        }
        if($conversion > 30) {
            return  self::MIDDLE_CONVERSION;
        }
        return self::NO_INFO_CONVERSION;
    }
}