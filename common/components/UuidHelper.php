<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.04.18
 * Time: 15:27
 */

namespace common\components;

class UuidHelper
{
    public const RAND_CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    public const BEGIN_TIME = 1523277224.117; //2018 9 Apr

    public const ADD_LEN = 7;

    public static function toBase($num)
    {
        $b = strlen(self::RAND_CHARS);
        $base = self::RAND_CHARS;
        $r = $num % $b;
        $res = $base[$r];
        $q = floor($num / $b);
        while ($q) {
            $r = $q % $b;
            $q = floor($q / $b);
            $res = $base[$r] . $res;
        }
        return $res;
    }

    /**
     * @param int $len
     * @return string
     * @throws \Exception
     */
    public static function generateRandCode($len = 5): string
    {
        $randCharsCount = strlen(self::RAND_CHARS) - 1;
        $returnValue = '';
        for ($i = 0; $i < $len; $i++) {
            $returnValue .= self::RAND_CHARS[random_int(0, $randCharsCount)];
        }
        return $returnValue;
    }

    /**
     * @param null $addLen
     * @return string
     * @throws \Exception
     */
    public static function generateUuid($addLen = null): string
    {
        if (!$addLen) {
            $addLen = self::ADD_LEN;
        }
        $microtime = microtime(true);
        $nowTime = ($microtime - self::BEGIN_TIME) * 1000;
        $microtimeEnc = self::toBase($nowTime);
        $resUuid = $microtimeEnc;
        $resUuid .= self::generateRandCode($addLen);
        return $resUuid;
    }
}