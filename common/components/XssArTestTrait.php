<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.10.18
 * Time: 13:46
 */

namespace common\components;

/**
 * Trait XssArTestTrait
 *
 * @package common\components
 * @mixin BaseAR
 */
trait XssArTestTrait
{
    public static function getXssTestText()
    {
        //return "?<script>console.log(window)</script>'><script>console.log(window)</script>\"><script>console.log(window)</script>";
        return "<\">";
    }

    protected function fixXssSkiplist()
    {
        return ['user' => ['password_hash', 'login'], 'user_admin' => ['username', 'password_hash'], 'system_setting' => ['json', 'value'], 'file' => ['path', 'stored_name']];
    }

    protected function isXssCheckColumn($attributeName): bool
    {
        if ($attributeName === 'code') {
            return false;
        }
        $metaColumn = self::getTableSchema()->getColumn($attributeName);
        if ($metaColumn) {
            if ($metaColumn->phpType === 'string' && $metaColumn->size > 50) {
                return true;
            }
        }
        return false;
    }

    protected function isAllowedNullValue($attributeName): bool
    {
        $metaColumn = self::getTableSchema()->getColumn($attributeName);
        return $metaColumn->allowNull;
    }

    /**
     * @param $attributeName
     * @param $attributeValue
     * @return string
     */
    protected function fixXssAttribute($attributeName, $attributeValue)
    {
        if ($this->isXssCheckColumn($attributeName)) {
            // Get only text values, that can contain xss
            $attributeValue .= self::getXssTestText();
        }
        return $attributeValue;
    }

    /**
     * Fix xss attributes
     *
     * @throws \yii\base\InvalidConfigException
     */
    protected function fixXssAttributes(): void
    {
        $skipList = $this->fixXssSkiplist();
        $skipAttributes = $skipList[$this->tableName()] ?? [];

        foreach ($this->attributes as $attributeName => $attributeValue) {
            if (\in_array($attributeName, $skipAttributes, true)) {
                continue;
            }
            $newAttributeValue = $this->fixXssAttribute($attributeName, $attributeValue);
            if ($attributeValue !== $newAttributeValue) {
                $this->setAttribute($attributeName, $newAttributeValue);
            }
        }
    }

    protected function removeXssAttribute($attributeName, $attributeValue)
    {
        if ($this->isXssCheckColumn($attributeName)) {
            if ($attributeValue && is_string($attributeValue)) {
                $attributeValue = str_replace(self::getXssTestText(), '', $attributeValue);
                if (!$attributeValue) {
                    if ($this->isAllowedNullValue($attributeName)) {
                        $attributeValue = null;
                    } else {
                        $attributeValue = '';
                    }
                }
            }
        }
        return $attributeValue;
    }

    protected function removeXssInput(): void
    {
        foreach ($this->attributes as $attributeName => $attributeValue) {
            $newAttributeValue = $this->removeXssAttribute($attributeName, $attributeValue);
            if ($attributeValue !== $newAttributeValue) {
                $this->setAttribute($attributeName, $newAttributeValue);
            }
        }
    }
}