<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.10.16
 * Time: 13:32
 */

namespace common\components;

use Yii;

class PaymentExchangeRateFacade
{

    public static function convert($price, $fromCurrency, $toCurrency, $round = false)
    {
        /** @var PaymentExchangeRateConverter $converter */
        $converter = Yii::createObject(PaymentExchangeRateConverter::class);
        return $converter->convert($price, $fromCurrency, $toCurrency, $round);
    }

    public static function getCurrentRate()
    {
        $paymentExchangeRateConverter = Yii::createObject(PaymentExchangeRateConverter::class);
        return $paymentExchangeRateConverter->getLastPaymentExchangeRate();
    }
}