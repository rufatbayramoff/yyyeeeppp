<?php

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.06.16
 * Time: 17:25
 */

namespace common\components;

class UrlHelper
{
    public static function getParameter($url, $varname)
    {
        $parsedUrl = parse_url($url);
        $query     = [];

        if (array_key_exists('query', $parsedUrl)) {
            parse_str($parsedUrl['query'], $query);
            return $query[$varname] ?? null;
        }
        return null;
    }

    public static function deleteGetParameter($url, $varname)
    {
        $parsedUrl = parse_url($url);
        $query     = [];

        if (array_key_exists('query', $parsedUrl)) {
            parse_str($parsedUrl['query'], $query);
            unset($query[$varname]);
        }

        $path  = isset($parsedUrl['path']) ? $parsedUrl['path'] : '';
        $query = !empty($query) ? '?' . http_build_query($query) : '';

        return (array_key_exists('scheme', $parsedUrl) ? $parsedUrl['scheme'] . '://' : '') . ($parsedUrl['host'] ?? '') . $path . $query;
    }

    public static function replaceDomain($url, $domain): string
    {
        $parsed = parse_url($url);
        $scheme = param('httpScheme');
        if (array_key_exists('scheme', $parsed)) {
            $scheme = $parsed['scheme'] . '://';
        }
        return $scheme . $domain . (array_key_exists('path', $parsed) ? $parsed['path'] : '') . (array_key_exists('query', $parsed) ? '?' . $parsed['query'] : '');
    }

    public static function addGetParameter($url, $parameter, $value)
    {
        $paramValue = $parameter . '=' . urlencode($value);
        $url        = self::deleteGetParameter($url, $parameter);
        if (strpos($url, '?')) {
            return $url . '&' . $paramValue;
        }
        return $url . '?' . $paramValue;
    }

    public static function getDomainFromUrl($url)
    {
        $parsed = parse_url($url);
        return $parsed['host'] ?? '';
    }

    public static function getUrlWithoutParams($url)
    {
        $parsed = parse_url($url);
        return $parsed['path'] ?? '';
    }
}