<?php namespace common\components\security;

/**
 * EncryptForm
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class EncrypterForm extends \yii\base\Model
{

    /**
     * private key used to decrypt
     * 
     * @var type 
     */
    public $privateFile = '@common/components/security/security_rsa';

    /**
     * public key used  to 
     * @var string
     */
    public $pubkeyFile = '@common/components/security/security_rsa.pem.pub';

    /**
     * data to encrypt
     * @var string
     */
    public $source;

    /**
     * password for private key
     * @var string
     */
    public $passphrase = '';

    public function rules()
    {
        return [
            [['pubkeyFile', 'privateFile', 'passphrase', 'source', 'cryptFile'], 'required']
        ];
    }

    /**
     * encrypt source and save result to encryptFile
     * @return type
     */
    public function encrypt($source = "")
    {
        if(!empty($source)){
            $this->source=  $source;
        }
        $fp = fopen($this->getPubkeyFile(), "r");
        $pub_key = fread($fp, 8192);
        fclose($fp);
        $pubKey = openssl_get_publickey($pub_key);
        openssl_public_encrypt($this->source, $crypttext, $pubKey);
        return $crypttext;
    }

    /**
     * encrypt source and save result to encryptFile
     * 
     * @return string
     */
    public function decrypt($crypttext)
    {
        $result = false;
        try{
            $fpPk = fopen($this->getPrivateKeyFile(), "r");
            $priv_key = fread($fpPk, 8192);
            fclose($fpPk);
            $res = openssl_get_privatekey($priv_key, $this->passphrase);
            if(!$res){
                throw new \Exception("Not correct passphrase");
            }
            openssl_private_decrypt($crypttext, $newsource, $res);
            $result = \yii\helpers\Json::decode($newsource);
        }catch(\Exception $e){
            logException($e, 'ts_debug');
            throw new \yii\base\UserException($e->getMessage());
        }
        return $result;
    }
 
    /**
     * 
     * @return string
     */
    public function getPrivateKeyFile()
    {
        return \Yii::getAlias($this->privateFile);
    }

    /**
     * 
     * @return string
     */
    public function getPubkeyFile()
    {
        return \Yii::getAlias($this->pubkeyFile);
    }
}
