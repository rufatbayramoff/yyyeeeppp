<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.06.17
 * Time: 18:06
 */

namespace common\components;

use common\models\Cart;
use common\models\repositories\UserSessionRepository;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use lib\crypto\Crypto;
use Yii;
use yii\base\Component;

class SessionManager extends Component
{
    const REDIRECT_CODE = 307;

    const EXPIRE_DATE_SEC = 60;

    const RESTORE_SESSION_SALT = 'g9gFPx90qa;l4-zso9wspjaLdoiq09asj';

    public $sessionCryptKey;

    /** @var  Crypto */
    public $crypto;

    public function getMainDomain()
    {
        return \Yii::$app->getModule('intlDomains')->mainDomain;
    }

    public function getMainUrl()
    {
        return param('httpScheme') . $this->getMainDomain();
    }

    public function getAllowedBackUrls()
    {
        $resultUrls = [];
        foreach (\Yii::$app->getModule('intlDomains')->allowedDomains as $allowedDomain) {
            $resultUrls[] = param('httpScheme') . $allowedDomain;
        }
        return $resultUrls;
    }

    public function injectDependencies(Crypto $crypto)
    {
        $this->crypto = $crypto;
    }


    public function encodeRedirectParams($redirectParams)
    {
        $redirectParamsJson = json_encode($redirectParams);
        return $this->crypto->encrypt($redirectParamsJson, $this->sessionCryptKey);
    }

    public function decodeRedirectParams($redirectParams)
    {
        $redirectParamsJson = $this->crypto->decrypt($redirectParams, $this->sessionCryptKey);
        $returnRedirectParams = json_decode($redirectParamsJson, true);
        return $returnRedirectParams;
    }

    public function checkBackUrl($backUrl): bool
    {
        foreach ($this->getAllowedBackUrls() as $allowedBackUrl) {
            if (strpos($backUrl, $allowedBackUrl) === 0) {
                return true;
            }
        }
        return false;
    }

    public function removeSessionFromUrl($currentUrl)
    {
        $currentUrl = UrlHelper::deleteGetParameter($currentUrl, 'restore-session');
        $currentUrl = UrlHelper::deleteGetParameter($currentUrl, 'restore-session-salt');
        $currentUrl = UrlHelper::deleteGetParameter($currentUrl, 'set-session');
        $currentUrl = UrlHelper::deleteGetParameter($currentUrl, 'dp');
        return $currentUrl;
    }

    public function getRestoreSessionSalt()
    {
        if (Yii::$app->session->get('is-global-session')) {
            return md5(Yii::$app->session->id . self::RESTORE_SESSION_SALT);
        }
        return  null;
    }

    public function addRestoreSessionFlagToUrl($url)
    {
        $url = UrlHelper::addGetParameter($url, 'restore-session', 1);
        if ($currentRestoreSessionSalt = $this->getRestoreSessionSalt()) {
            $url = UrlHelper::addGetParameter($url, 'restore-session-salt', $currentRestoreSessionSalt);
        }
        if (isset($_COOKIE['dp']) && $dosProtection = $_COOKIE['dp']) {
            $url = UrlHelper::addGetParameter($url, 'dp', $dosProtection);
        }
        return $url;
    }

    public function getRestoreSessionRedirectUrl(): string
    {
        $scheme = ($_SERVER['REQUEST_SCHEME'] ?? 'http') . '://';
        $currentDomain = $_SERVER['HTTP_HOST'];

        $currentUrl = \Yii::$app->request->url;
        $currentUrl = $this->removeSessionFromUrl($currentUrl);

        $redirectParams = [
            'backUrl'        => $scheme . $currentDomain . $currentUrl,
            'date'           => DateHelper::now(),
            'oldUserSession' => UserFacade::getUserSession()->id
        ];
        $redirectParamsEncoded = $this->encodeRedirectParams($redirectParams);

        return $this->getMainUrl() . '/do-restore-session?redirectParams=' . $redirectParamsEncoded;
    }

    /**
     * @return bool
     */
    public function checkRestoreSession($forceCheck = false)
    {
        if (\Yii::$app->request->get('restore-session') || $forceCheck) {
            $diffSessionDetected = false;
            if (Yii::$app->request->get('restore-session-salt') && Yii::$app->session->get('is-global-session')) {
                $currentRestoreSessionSalt = md5(Yii::$app->session->id . self::RESTORE_SESSION_SALT);
                if ($currentRestoreSessionSalt != Yii::$app->request->get('restore-session-salt')) {
                    $diffSessionDetected = true;
                }
            }
            if (!Yii::$app->session->get('is-global-session') || $diffSessionDetected) {
                $redirectUrl = $this->getRestoreSessionRedirectUrl();
                \Yii::$app->response->redirect($redirectUrl, self::REDIRECT_CODE, false);
                return false;
            }
            if (Yii::$app->request->getIsGet() && Yii::$app->request->get('restore-session')) {
                $redirectUrl = Yii::$app->request->url;
                $redirectUrl = $this->removeSessionFromUrl($redirectUrl);
                \Yii::$app->response->redirect($redirectUrl, self::REDIRECT_CODE, false);
                return false;
            }
        }
        return true;
    }

    public function doRestoreSession($redirectParams)
    {
        $sessionParam = $this->decodeRedirectParams($redirectParams);
        $expireDate = DateHelper::subNowSec(self::EXPIRE_DATE_SEC);
        if ($sessionParam && $sessionParam['date'] > $expireDate && $this->checkBackUrl($sessionParam['backUrl'])) {
            $setSessionParams = [
                'backUrl'         => $sessionParam['backUrl'],
                'date'            => DateHelper::now(),
                'mainSessionId'   => Yii::$app->session->getId(),
                'dbUserSessionId' => UserFacade::getUserSession()->id,
                'oldUserSession'  => $sessionParam['oldUserSession']
            ];
            $sessionParamsEncrypt = $this->encodeRedirectParams($setSessionParams);
            $backUrlHost = UrlHelper::getDomainFromUrl($sessionParam['backUrl']);
            $setSessionUrl = param('httpScheme') . $backUrlHost . '/set-session?sessionParams=' . $sessionParamsEncrypt;
            \Yii::$app->response->redirect($setSessionUrl, self::REDIRECT_CODE, false);
            return true;
        }
        return false;
    }

    public function setSession($redirectParams)
    {
        $sessionParams = $this->decodeRedirectParams($redirectParams);
        $expireDate = DateHelper::subNowSec(self::EXPIRE_DATE_SEC);
        if ($sessionParams && $sessionParams['date'] > $expireDate && $this->checkBackUrl($sessionParams['backUrl'])) {
            $mainSessionId = $sessionParams['mainSessionId'];
            Yii::$app->session->close();
            Yii::$app->session->setId($mainSessionId);
            Yii::$app->session->open();
            $oldUserSession = UserSessionRepository::getById($sessionParams['oldUserSession']);
            $newUserSession = UserSessionRepository::getById($sessionParams['dbUserSessionId']);
            if (!$newUserSession) {
                echo 'Db user session not found';
                return false;
            }
            if ($newUserSession->uuid != $mainSessionId) {
                echo 'Db user session uuid is not equal with session id';
                return false;
            }
            if ($oldUserSession && !$oldUserSession->user && Yii::$app->session->get('__id')) {
                $user = FrontUser::tryFindByPk(Yii::$app->session->get('__id'));
                $oldUserSession->user_id = $user->id;
                $oldUserSession->save(false);
                UserSessionOwnersFixer::fixModelsOwnersForUser($oldUserSession, $user);
            }
            if ($newUserSession && $oldUserSession) {
                // Move carts from oldUserSession to new
                Cart::updateAll(['user_session_id' => $newUserSession->id], ['user_session_id' => $oldUserSession->id]);
            }
            \Yii::$app->response->redirect($sessionParams['backUrl'], self::REDIRECT_CODE, false);
            return true;
        }
        return false;
    }

    public function pingGlobalSessionImg()
    {
        if (!Yii::$app->session->get('is-global-session')) {
            $url = $this->addRestoreSessionFlagToUrl('/ping-global-session');
            echo '<img src="'.$url.'" style="height: 0px; weight: 0px;display:none;"/>';
        }
    }

    public function isInSyncSessionProcess()
    {
        if ((strpos(Yii::$app->request->url, '/do-restore-session?') === 0) || (strpos(Yii::$app->request->url, '/set-session?') === 0)) {
            return true;
        }
        return false;
    }
}