<?php
namespace common\components;

class UrlManager extends \yii\web\UrlManager
{
    public function createUrl($params)
    {
        $url = parent::createUrl($params);
        $url = str_replace('??', '?', $url);
        $url = str_replace('/?', '?', $url);
        $url = rtrim( $url, '/');
        return $url;
    }
}