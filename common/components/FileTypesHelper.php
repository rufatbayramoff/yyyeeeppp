<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.12.17
 * Time: 11:31
 */

namespace common\components;

use common\interfaces\FileBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\CuttingPack;
use common\models\File;

class FileTypesHelper
{
    public const ALLOW_DOCS_EXTENSIONS = ['pdf', 'doc', 'docx', 'txt', 'odt', 'xls', 'xlsx', 'csv'];
    public const ALLOW_IMAGES_EXTENSIONS = ['jpg', 'jpeg', 'gif', 'png'];
    public const ALLOW_VIDEO_EXTENSIONS = ['mp4', 'webm', 'ogg', 'ogv', 'avi', 'mng', 'mov', 'qt', '3gp', '3g2'];
    public const ALLOW_ARCHIVE_EXTENSIONS = ['zip'];
    public const RENDER_EXTENSIONS = ['stl',  'ply'];

    public const ALLOW_OTHER_3DMODELS_EXTENSIONS = [
        'dxf', // Drawing Exchange Format File
        'prt', // Pro/ENGINEER / CREO Part File
        'neu', // Pro/ENGINEER Neutral File

        'asm', // Solid Edge Assembly FileBaseInterface / CREO ASSEMBLY
        'dft', // Solid Edge Draft Document
        'psm', // Solid Edge Sheet Metal File
        'pwd', // Solid Edge Weldment Document
        'par', // SolidEdge part

        'sldasm', // SolidWorks Assembly File
        'slddrw', // SolidWorks Drawing File
        'sldprt', // SolidWorks Part File
        'edrw', // SolidWorks eDrawings File
        'svd', // SolidView Design File

        'dwg', // AutoCAD Drawing Database File
        'dws', // AutoCAD Drawing Standards File
        'dwt', // AutoCAD Drawing Template
        'dxe', // AutoCAD Data Extraction Template
        'pwt', // AutoCAD Publish To Web Template

        'fbx', // Autodesk exchange format
        '123c', // Autodesk 123C Drawing
        '123d', // Autodesk 123D Drawing
        '123dx', // 123D Design Model File

        'ipj', // Inventor Project File
        'ipn', // Inventor Presentation File
        'ipt', // Inventor Part File
        'iam', // Inventor Assembly File
        'idv', // Inventor Design View File
        'idw', // Inventor Drawing

        'model', // CATIA 3D Model FIle
        'mdl', // CATIA 3D Model FIle
        'catdrawing', // CATIA V5 Drawing
        'catpart', // CATIA V5 Part File
        'catproduct', // CATIA V5 Assembly File
        'cgr', // CATIA Graphical Representation File
        'dlv', // CATIA 4 Export File

        'x_b', // Parasolid Model Part File
        'x_t', // Parasolid Model Part File

        'fz', // Fritzing Project File
        'fzb', // Fritzing Bin File
        'fzm', // Fritzing Module File
        'fzp', // Fritzing Part File
        'fzz', // Fritzing Shareable Project File

        'fcstd', // FreeCAD Document
        'g', // BRL-CAD Geometry File
        'scad', // OpenSCAD Script
        'xpk', // Ideas Exchange Package
        'cdw', // KOMPAS Document
        'dwf', // Design Web Format
        'jt', // JT OPEN

        'cad', // BobCAD-CAM File
        'dgk', // Delcam 3D Model File
        'cnc', // CNC Machine Tool Path
        'nc', // Mastercam Numerical Control File
        'nc1', // Mastercam Numerical Control File

        '3ds', // Autodesk 3ds Max
        'prj', // 3D Studio Mesh
        'shp', // 3D Studio shape file
        'blend', // is the format used by Blender
        'dae', // was designed as format for COLLAborative Design Activity (COLLADA)
        'obj', // geometry definition file format by Wavefront
        'off', // Object FileBaseInterface Format
        'sc1', // - Sculptris
        'skp', // is used by Google sketchup.
        'wrl', // virtual reality modelling language
        'vrml', // virtual reality modelling language
        'x3d', // is the successor of VRML
        'xsi', // - SoftImage
        'ztl', // - Zbrush
        'xyz', // 3d format for computer chemistry and biology.
    ];

    public const IMAGES_EXTENSIONS = ['jpg', 'png', 'gif', 'jpeg'];

    /**
     * @param FileBaseInterface $file
     * @param $extList
     * @return bool
     */
    public static function isFileInExt(FileBaseInterface $file, $extList) : bool
    {
        if (in_array(strtolower($file->getFileExtension()), $extList)) {
            return true;
        };
        return false;
    }

    public static function isImagePath(string $filePath) : bool
    {
        $pathinfo = pathinfo($filePath);
        $extension = strtolower($pathinfo['extension']);
        return in_array($extension, self::IMAGES_EXTENSIONS);
    }

    /**
     * @param FileBaseInterface $file
     * @return bool
     */
    public static function isImage(FileBaseInterface $file) : bool
    {
        return self::isFileInExt($file, self::IMAGES_EXTENSIONS);
    }

    /**
     * @param FileBaseInterface $file
     * @return bool
     */
    public static function isVideo(FileBaseInterface $file) : bool
    {
        return self::isFileInExt($file, self::ALLOW_VIDEO_EXTENSIONS);
    }

    /**
     * Is extension allowed for current user
     * @param FileBaseInterface $file
     * @return bool
     */
    public static function isAllowExtensions(FileBaseInterface $file) : bool
    {
        return self::isFileInExt($file, self::getAllowExtensions());
    }

    /**
     * Return list of all allowed extensions in our system.
     * @return string[]
     */
    public static function getAllowExtensions() : array
    {
        return array_merge(
            self::ALLOW_DOCS_EXTENSIONS,
            self::ALLOW_IMAGES_EXTENSIONS,
            self::ALLOW_VIDEO_EXTENSIONS,
            Model3dBasePartInterface::ALLOWED_FORMATS,
            CuttingPack::ALLOWED_FORMATS,
            self::ALLOW_ARCHIVE_EXTENSIONS,
            self::ALLOW_OTHER_3DMODELS_EXTENSIONS,
            CuttingPack::ALLOWED_FORMATS
        );
    }

    /**
     * Remove from array all files except images.
     *
     * @param File[] $files
     * @return File[]
     */
    public static function filterImages(array $files) : array
    {
        return array_filter($files, function (File $file) {
            return FileTypesHelper::isImage($file);
        });
    }

    /**
     * Form string for dropaone accept-files attribute
     * @param string[] $filetypes Allowed extensions
     * @return string
     */
    public static function dropzoneAcceptStr(array $filetypes) : string 
    {
        return '.'. implode(', .', $filetypes);
    }

    public static function isRenderable(File $file)
    {
        return self::isFileInExt($file, self::RENDER_EXTENSIONS);
    }
}