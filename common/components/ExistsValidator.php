<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.05.17
 * Time: 10:53
 */

namespace common\components;


/**
 * Class ExistsValidator
 * Disable exists validator, because more additional fk request. If request is broken, it will be failed anyway.
 *
 * @package common\components
 */
class ExistsValidator extends \yii\validators\ExistValidator
{
    public function validateAttribute($model, $attribute)
    {
    }
}