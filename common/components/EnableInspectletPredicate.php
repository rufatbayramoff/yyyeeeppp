<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.05.18
 * Time: 10:21
 */

namespace common\components;

class EnableInspectletPredicate
{
    public static function checkEnable()
    {
        $currentUrl = \Yii::$app->request->url;
        if (strpos($currentUrl, '/my/print-model3d/widget') === 0) {
            return false;
        }
        if (strpos($currentUrl, '/guide') === 0) {
            return false;
        }
        if (strpos($currentUrl, '/material') === 0) {
            return false;
        }
        if (strpos($currentUrl, '/machines') === 0) {
            return false;
        }
        if (strpos($currentUrl, '/blog') === 0) {
            return false;
        }
        return true;
    }
}