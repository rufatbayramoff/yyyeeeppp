<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.06.17
 * Time: 17:31
 */

namespace common\components;

/**
 * Class Session
 *
 * @package common\components
 */
class Session extends \yii\redis\Session
{
    public function init()
    {
        $this->keyPrefix = 'php_session:';
        return parent::init();
    }

    public function getMainDomain()
    {
        return \Yii::$app->getModule('intlDomains')->mainDomain;
    }

    protected function getCurrentDomain()
    {
        return $_SERVER['HTTP_HOST'] ?? \Yii::$app->getModule('intlDomains')->mainDomain;
    }

    public function open()
    {
        if ($this->getIsActive()) {
            return;
        }
        $currentDomain = $this->getCurrentDomain();
        if ($currentDomain === $this->getMainDomain()) {
            parent::open();
            $this->set('is-global-session', true);
            return;
        }
        parent::open();
    }

    public function destroy()
    {
        $this->removeAll();
    }

    public function destroyCompletely()
    {
        session_regenerate_id(true);
    }


    public function getCookieParams()
    {
        $params = parent::getCookieParams();
        if (!$params['domain']) {
            // Auto add mutlidomain for subdomains
            $mainDomain = $this->getMainDomain();
            if ($mainDomain) {
                $dotPos = strpos($mainDomain, '.');
                $mainDomain = substr($mainDomain, $dotPos+1, 1024);
                if (strpos($this->getCurrentDomain(), $mainDomain) !== false) {
                    $params['domain'] = '.' . $mainDomain;
                }
            }
        }
        if (CookieSameSiteDetector::isSameSiteCookies()) {
           $params['samesite']='none';
           $params['secure'] = 'Secure';
        }
        return $params;
    }

    public function getCalculateRedisKey()
    {
        return $this->calculateKey($this->id);
    }
}