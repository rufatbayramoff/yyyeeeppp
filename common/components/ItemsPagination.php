<?php

namespace common\components;

use Yii;
use yii\data\Pagination;

class ItemsPagination extends Pagination
{
    public function createUrl($page, $pageSize = null, $absolute = false)
    {
        return \common\components\UrlHelper::addGetParameter(Yii::$app->request->url, 'page', ($page + 1));
    }
}
