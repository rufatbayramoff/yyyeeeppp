<?php

namespace common\components\hiredesigner;
use common\models\Model3d;
use common\models\User;


/**
 * Class HireDesignerRepository
 * @package common\components\hiredesigner
 */
class HireDesignerRepository
{
    /**
     * @param HireDesigner $hireDesigner
     */
    public function save(HireDesigner $hireDesigner)
    {
        $old = $this->load();
        $logFile = \Yii::getAlias('@runtime/logs/hire_designer_'.date('Y-m-d-H-i-s'));
        file_put_contents($logFile, serialize($this->toRawData($old)));

        $data = $this->toRawData($hireDesigner);
        \Yii::$app->redis->set("hire_designer", serialize($data));
    }

    /**
     * @return HireDesigner
     */
    public function load()
    {
        $data = \Yii::$app->redis->get("hire_designer");
        $result = new HireDesigner();

        if (!$data) {
            return $result;
        }

        $data = unserialize($data);

        foreach ($data as $designerId => $modelsIds) {

            $designer = User::tryFindByPk($designerId);

            if ($designer->status != User::STATUS_ACTIVE) {
                continue;
            }

            $models = [];

            foreach ($modelsIds as $modelId) {
                $model = Model3d::tryFindByPk($modelId);
                if (!$model->isPublished()) {
                    continue;
                }
                $models[] = $model;
            }

            if (!$models) {
                continue;
            }

            $item = new HireDesignerItem();
            $item->designer = $designer;
            $item->models = $models;

            $result->items[] = $item;
        }

        return $result;
    }

    /**
     *
     * @param HireDesigner $hireDesigner
     * @return array
     */
    private function toRawData(HireDesigner $hireDesigner)
    {
        $data = [];
        foreach ($hireDesigner->items as $item) {
            $data[$item->designer->id] = [];
            foreach ($item->models as $model) {
                $data[$item->designer->id][] = $model->id;
            }
        }
        return $data;
    }
}