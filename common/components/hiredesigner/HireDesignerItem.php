<?php
/**
 * Created by mitaichik
 */

namespace common\components\hiredesigner;


use common\models\Model3d;
use common\models\User;

/**
 * Class HireDesignerItem
 * @package common\components\hiredesigner
 */
class HireDesignerItem
{
    /**
     * @var User
     */
    public $designer;

    /**
     * @var Model3d[]
     */
    public $models = [];
}