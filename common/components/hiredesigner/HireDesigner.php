<?php
/**
 * Created by mitaichik
 */

namespace common\components\hiredesigner;

/**
 * Class HireDesigner
 * @package common\components\hiredesigner
 */
class HireDesigner
{
    /**
     * @var HireDesignerItem[]
     */
    public $items = [];
}