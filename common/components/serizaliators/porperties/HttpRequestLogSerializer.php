<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.12.18
 * Time: 9:59
 */

namespace common\components\serizaliators\porperties;


use common\components\serizaliators\AbstractProperties;
use common\models\HttpRequestExtDataLog;
use common\models\HttpRequestLog;

/**
 *
 * @property array $properties
 */
class HttpRequestLogSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            HttpRequestLog::class        => [
                'id',
                'host',
                'url',
                'remoteAddr'     => 'remote_addr',
                'paramsGet'      => 'params_get',
                'paramsPost'     => 'params_post',
                'paramsFiles'    => 'params_files',
                'session',
                'cookie',
                'answer',
                'headers',
                'responceCode'   => 'responce_code',
                'createDate'     => 'create_date',
                'runTimeMilisec' => 'run_time_milisec',
                'requestType'    => 'request_type',
                'userAgent'      => 'user_agent',
                'referrer',
                'userSessionId'  => 'user_session_id',
                'extData'        => function (HttpRequestLog $httpRequestLog) {
                    return $httpRequestLog->httpRequestExtDataLogs;
                }
            ],
            HttpRequestExtDataLog::class => [
                'logText'   => 'log_text',
                'extData'   => 'ext_data',
                'type'       => 'type',
                'objectType' => 'object_type',
                'objectId'   => 'object_id'
            ]
        ];
    }
}