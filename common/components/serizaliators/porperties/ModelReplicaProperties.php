<?php
/**
 * Created by mitaichik
 */

namespace common\components\serizaliators\porperties;


use common\components\serizaliators\AbstractProperties;
use common\models\Model3dReplica;
use common\models\Model3dReplicaPart;
use common\services\Model3dPartService;
use frontend\models\model3d\Model3dFacade;

class ModelReplicaProperties extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [

            Model3dReplica::class => [
                'previewUrl' => function(Model3dReplica $replica) {
                    return  Model3dFacade::getCover($replica)['image'];
                },
                'parts' => 'activeModel3dParts',
            ],

            Model3dReplicaPart::class => [
                'id',
                'previewUrl' => function(Model3dReplicaPart $part) {
                    return Model3dPartService::getModel3dPartRenderUrl($part);
                },
            ]
        ];
    }
}