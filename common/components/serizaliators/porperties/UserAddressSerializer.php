<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.07.18
 * Time: 11:15
 */

namespace common\components\serizaliators\porperties;

use common\components\serizaliators\AbstractProperties;
use common\models\UserAddress;
use common\models\UserLocation;
use function foo\func;

class UserAddressSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            UserAddress::class => [
                'countryIso'       => function (UserAddress $address) {
                    return $address->country->iso_code;
                },
                'region',
                'city',
                'address',
                'company',
                'extended_address',
                'lat',
                'lon',
                'zipCode'          => 'zip_code',
                'formattedAddress' => function (UserAddress $address) {
                    return UserAddress::formatAddress($address);
                }
            ]
        ];
    }
}