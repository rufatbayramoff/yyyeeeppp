<?php
/**
 * Created by mitaichik
 */

namespace common\components\serizaliators\porperties;


use common\components\serizaliators\AbstractProperties;
use lib\delivery\delivery\models\DeliveryRate;
use lib\money\Money;

class DeliveryRateProperties extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            DeliveryRate::class => [
                'title',
                'code',
                'isEstimate',
                'delveiryCost',
                'packingCost',
                'cost',
            ],
            Money::class => MoneyProperties::class
        ];
    }
}