<?php
/**
 * Created by mitaichik
 */

namespace common\components\serizaliators\porperties;


use common\components\serizaliators\AbstractProperties;
use common\models\PrinterColor;
use common\models\PrinterMaterial;

class MaterialAndColorsProperties extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            PrinterMaterial::class => [
                'id',
                'title',
                'colors'
            ],
            PrinterColor::class => [
                'id',
                'title'
            ]
        ];
    }
}