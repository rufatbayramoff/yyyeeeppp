<?php
/**
 * Created by mitaichik
 */

namespace common\components\serizaliators\porperties;


use common\components\serizaliators\AbstractProperties;
use common\models\UserAddress;

/**
 * Class UserAddressAsUserLocatonProperties
 * @package common\components\serizaliators\porperties
 */
class UserAddressAsUserLocatonProperties extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            UserAddress::class => [
                'country_id',
                'country' => 'country.iso_code',
                'region',
                'city',
                'address',
                'address2' => 'extended_address',
                'zip_code'
            ],
        ];
    }
}