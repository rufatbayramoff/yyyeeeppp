<?php
/**
 * Created by mitaichik
 */

namespace common\components\serizaliators\porperties;


use common\components\serizaliators\AbstractProperties;
use lib\money\Money;

class MoneyProperties extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            Money::class => [
                'amount' => function(Money $money){
                    return (float)$money->getAmount();
                },
                'currency' => function(Money $money){
                    return $money->getCurrency();
                },
            ],
        ];
    }
}