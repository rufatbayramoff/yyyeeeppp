<?php
/**
 * Created by mitaichik
 */

namespace common\components\serizaliators\porperties;


use common\components\FileTypesHelper;
use common\components\serizaliators\AbstractProperties;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderAttemptModeration;
use common\models\StoreOrderAttemptModerationFile;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\order\StoreOrderAttemptDeadline;

/**
 * Class PrintAttempProperties
 * @package common\components\serizaliators\porperties
 */
class PrintAttempProperties extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [

            StoreOrderAttemp::class => [
                'id',
                'moderation'
            ],

            StoreOrderAttemptModeration::class => [
                'files',
                'status',
                'reject_comment',
            ],

            StoreOrderAttemptModerationFile::class => [
                'id',
                'imgUrl' => function (StoreOrderAttemptModerationFile $attemptModerationFile) {
                    if (FileTypesHelper::isVideo($attemptModerationFile->file)) {
                        return $attemptModerationFile->file->getFileUrl();
                    }
                    return ImageHtmlHelper::getThumbUrl($attemptModerationFile->file->getFileUrl(), 160, 120);
                },
                'imgUrlOrigin' => function (StoreOrderAttemptModerationFile $attemptModerationFile) {
                    return $attemptModerationFile->file->getFileUrl();
                },
                'fileUuid' => function (StoreOrderAttemptModerationFile $attemptModerationFile) {
                    return $attemptModerationFile->file->uuid;
                },
                'isVideo' => function (StoreOrderAttemptModerationFile $attemptFile) {
                    return FileTypesHelper::isVideo($attemptFile->file);
                },
                'attempDeadline' => function (StoreOrderAttemptModerationFile $attemptModerationFile) {
                    $deadlineTimer = StoreOrderAttemptDeadline::create($attemptModerationFile->attempt->attempt);
                    return $deadlineTimer->getDate('Y-m-d H:i:s');
                }
            ]
        ];
    }
}