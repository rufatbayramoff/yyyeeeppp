<?php
/**
 * Created by mitaichik
 */

namespace common\components\serizaliators\porperties;


use common\components\FileTypesHelper;
use common\components\serizaliators\AbstractProperties;
use common\components\ShareUrlHelper;
use common\models\File;
use common\models\Ps;
use common\models\StoreOrderReview;
use common\models\StoreOrderReviewShare;
use common\models\User;
use common\services\WidgetService;
use frontend\components\image\ImageHtmlHelper;
use Yii;

class ReviewShareSerializer extends AbstractProperties
{
    /**
     * @var User
     */
    private $user;

    /**
     * ReviewShareSerializer constructor.
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        parent::__construct([]);
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return [

            StoreOrderReviewShare::class => [
                'review_id',
                'text',
                'photo_id',
                'review',
                'social_type',
                'canEdit'      => function (StoreOrderReviewShare $share): bool {
                    return $share->isUserCanEdit($this->user);
                },
                'url'          => function (StoreOrderReviewShare $share): ?string {
                    return $share->isNewRecord
                        ? null
                        : ShareUrlHelper::url($share, true);
                },
                'affiliateUrl' => function (StoreOrderReviewShare $share): ?string {
                    return \Yii::$app->params['siteUrl'] . '/order-upload?userUid=' . $share->user->uid;
                },
                'photoUrl'     => function (StoreOrderReviewShare $share): ?string {
                    return $share->isNewRecord
                        ? null
                        : ShareUrlHelper::mainPhotoUrl($share, true);
                }
            ],

            StoreOrderReview::class => [
                'id',
                'files' => function (StoreOrderReview $review) {
                    return array_values(FileTypesHelper::filterImages($review->reviewFilesSort));
                },
                'ps'
            ],

            File::class => [
                'id',
                'previewUrl' => function (File $file) {
                    return ImageHtmlHelper::getThumbUrlForFile($file, 256, 256);
                },
                'sourceUrl'  => function (File $file) {
                    return $file->getFileUrl();
                }
            ],

            Ps::class => [
                'id',
                'title',
                'logo'         => function (Ps $ps) {
                    return $ps->getCircleImageByPs($ps, 100, 100);
                },
                'rating'       => function (Ps $ps) {
                    return $ps->psCatalog->rating_avg ?? 0;
                },
                'reviewsCount' => function (Ps $ps) {
                    return $ps->psCatalog->rating_count ?? 0;
                }
            ]

        ];
    }
}