<?php
/**
 * Created by mitaichik
 */

namespace common\components\serizaliators;


use common\components\ArrayHelper;
use common\modules\xss\helpers\XssHelper;
use yii\base\Exception;

class Serializer
{
    /**
     * @var array
     */
    private $properties = [];

    /**
     * Serializator constructor.
     *
     * @param array ...$serializtors
     */
    public function __construct(...$serializtors)
    {
        foreach ($serializtors as $serializtor) {
            $this->addProperties($serializtor);
        }
    }

    /**
     * @param array ...$serializtors
     * @return static
     */
    public static function create(...$serializtors)
    {
        return new static($serializtors);
    }

    /**
     * @param string|AbstractProperties $serializtor
     * @return $this
     * @throws Exception
     */
    public function addProperties($serializtor)
    {
        /** @var AbstractProperties $serializtor */
        $serializtor = $serializtor instanceof AbstractProperties ? $serializtor : new $serializtor();

        foreach ($serializtor->getProperties() as $className => $classDescription) {

            if (is_string($classDescription)) {
                $this->addProperties($classDescription);
                continue;
            }

            if (array_key_exists($className, $this->properties)) {
                if (array_diff(array_keys($this->properties[$className]), array_keys($classDescription))) {
                    throw new Exception("Serializator already have params for {$className}");
                }
                continue;
            }
            $this->properties[$className] = $classDescription;
        }
        return $this;
    }

    /**
     * Serialize object to array
     *
     * @param $object
     * @return array
     */
    public function serialize($object)
    {
        $serialized = ArrayHelper::toArray($object, $this->properties);
        /*
        if (defined('TEST_XSS') && TEST_XSS) {
            $serialized = XssHelper::cleanArray($serialized);
        }*/
        return $serialized;
    }
}