<?php
/**
 * Created by mitaichik
 */

namespace common\components\serizaliators;

use yii\base\BaseObject;

/**
 * Interface SerializatorInterface
 * @package common\components\serizaliators
 */
abstract class AbstractProperties extends BaseObject
{
    /**
     * @return array
     */
    public abstract function getProperties();

    /**
     * Create serializer for current class
     * @return Serializer
     */
    public static function createSerializer()
    {
        return new Serializer(static::class);
    }

    /**
     * Serialize use curent class
     * @param $object
     * @return array
     */
    public static function serialize($object)
    {
        return static::createSerializer()->serialize($object);
    }
}