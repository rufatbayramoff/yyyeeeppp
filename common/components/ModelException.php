<?php

/**
 * ModelException - exceptions from ./models/ classes
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */

namespace common\components;

class ModelException extends \Exception
{
    
}
