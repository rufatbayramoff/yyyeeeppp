<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.09.17
 * Time: 15:37
 */

namespace common\components;

use yii\web\Request;

class HttpRequestHelper
{
    public static function isBot(Request $request)
    {
        $userAgent = $request->userAgent;
        if (empty($userAgent)) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'] ?? '';
        }
        if (strpos(strtolower($userAgent), 'Yahoo! Slurp;') !== false) {
            return true;
        }
        if (strpos(strtolower($userAgent), 'bot') !== false) {
            return true;
        }

        return false;
    }

    public static function isWidget(Request $request)
    {
        if (strpos($request->getUrl(), '/widget') > 0) {
            return true;
        }
        return false;
    }
}