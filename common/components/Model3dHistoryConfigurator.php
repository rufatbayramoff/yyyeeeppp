<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.11.16
 * Time: 17:16
 */

namespace common\components;

use common\models\Model3d;
use common\models\Model3dHistory;
use common\models\Model3dImg;
use common\models\Model3dPart;
use common\models\Model3dTexture;
use common\models\StoreUnit;
use Psr\Log\InvalidArgumentException;

class Model3dHistoryConfigurator
{
    public static function getConfig($class)
    {
        $configMap =  [
            Model3d::class =>  [
                'class'                     => Model3dHistoryBehaviour::class,
                'actionName'                => Model3dHistory::ACTION_UPDATE,
                'skipAttributes'            => ['updated_at', 'model3d_texture_id', 'created_at'],
                'getModel3dFunction'        => function ($model) {
                    return $model;
                },
                'formAttributesLogFunction' => function ($changedAttributes, $model) {
                    return $changedAttributes;
                }
            ],
            StoreUnit::class => [
                'class'              => Model3dHistoryBehaviour::class,
                'actionName'         => Model3dHistory::ACTION_UPDATE_STORE_UNIT,
                'getModel3dFunction' => function ($model) {
                    return $model->model3d;
                },
            ],
            Model3dPart::class => [
                'class'              => Model3dHistoryBehaviour::class,
                'actionName'         => Model3dHistory::ACTION_UPDATE_PART,
                'activeAttributes'   => ['id', 'name', 'qty'],
                'getModel3dFunction' => function ($model) {
                    return $model->model3d;
                },
                'formCommentFunction' => function ($changedAttributes, $model) {
                    return json_encode([
                        'id'   => $model->id,
                        'name' => $model->name
                    ]);
                },
            ],
            Model3dTexture::class => [
                'class'              => Model3dHistoryBehaviour::class,
                'actionName'         => Model3dHistory::ACTION_UPDATE_TEXTURE,
                'activeAttributes'   => ['id', 'printer_material_group_id', 'printer_material_id', 'printer_color_id'],
                'getModel3dFunction' => function ($model) {
                    if ($model->model3d) {
                        return $model->model3d;
                    }
                    if ($model->model3dPart) {
                        return $model->model3dPart->model3d;
                    }
                    return null;
                },
                'formAttributesLogFunction' => function ($changedAttributes, $model) {
                    if (!$changedAttributes) {
                        return [];
                    }
                    $masterObject = null;
                    if ($model->model3d) {
                        $masterObject = ['Model3d' => $model->model3d->id . ' - ' . $model->model3d->title];
                    }
                    if ($model->model3dPart) {
                        $masterObject = ['Model3dPart' => $model->model3dPart->id . ' - ' . $model->model3dPart->name];
                    }
                    if (!$masterObject) {
                        return [];
                    }
                    $resultInfo = [];
                    if ($model->printerMaterialGroup) {
                        $resultInfo['materialGroup'] = $model->printerMaterialGroup->code;
                    }
                    if ($model->printerMaterial) {
                        $resultInfo['material'] = $model->printerMaterial->filament_title;
                    }
                    if ($model->printerColor) {
                        $resultInfo['color'] = $model->printerColor->render_color;
                    }
                    $resultLog = [
                        'masterObject' => [
                            'source' => null,
                            'result' => $masterObject
                        ],
                        'materialInfo' => [
                            'source' => null,
                            'result' => $resultInfo
                        ]
                    ];
                    return $resultLog;
                }
            ],
            Model3dImg::class => [
                'class'              => Model3dHistoryBehaviour::class,
                'actionName'         => Model3dHistory::ACTION_UPDATE_IMG,
                'skipAttributes'   => ['created_at', 'updated_at', 'qty'],
                'getModel3dFunction' => function ($model) {
                    return $model->model3d;
                },
            ],
        ];

        if (!array_key_exists($class, $configMap)) {
            throw new InvalidArgumentException('Invalid model3d history configurator class');
        }
        return $configMap[$class];
    }
}