<?php
/**
 * Created by mitaichik
 */

namespace common\components;

/**
 * Class IpComparator
 * @package common\components
 */
class IpComparator
{
    /**
     * Compare ip addresses.
     * Support mask like in gii
     * @param string $ip1 Real ip
     * @param string[]|string $ip2 Real ip or mask. Support arrays of ips or masks 
     * @return bool
     */
    public static function compare($ip1, $ip2)
    {
        $ip2 = is_array($ip2) ? $ip2 : [$ip2];
        foreach ($ip2 as $ip){
            if ($ip === '*' || $ip === $ip1 || (($pos = strpos($ip, '*')) !== false && !strncmp($ip1, $ip, $pos))){
                return true;
            }
        }
        return false;
    }
}