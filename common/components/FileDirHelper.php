<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.06.17
 * Time: 14:20
 */

namespace common\components;

use yii\helpers\BaseFileHelper;

class FileDirHelper
{
    public static function createDir($dir): void
    {
        if (!is_dir($dir)) {
            $returnValue = @mkdir($dir, 0777, true);
            if (!$returnValue) {
                if (!is_dir($dir)) {
                    throw new \yii\base\ErrorException('Can`t create directory: ' . $dir);
                }
            }
        }
    }

    /**
     * @param $currentPath
     * @param $path
     */
    public static function renameForNfs($currentPath, $path)
    {
        if ($currentPath === $path) {
            return;
        }
        if (copy($currentPath, $path)) {
            unlink($currentPath);
        } else {
            throw new \Exception('Error moving file [NFS]');
        }
    }

    public static function clearDirectory($path)
    {
        $items = glob($path.'/*');
        foreach ($items as $item) {
            if (is_dir($item)) {
                BaseFileHelper::removeDirectory($item);
            } else {
                unlink($item);
            }
        }
    }
}