<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.11.16
 * Time: 15:35
 */

namespace common\components;


use common\models\Model3dHistory;
use frontend\models\user\UserFacade;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class Model3dHistoryBehaviour extends Behavior
{
    public $actionName = 'unknown';
    public $skipAttributes = [];
    public $getModel3dFunction;
    public $formAttributesLogFunction = null;
    public $formCommentFunction = null;
    public $activeAttributes = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'logModel3dHistoryUpdate',
            ActiveRecord::EVENT_AFTER_INSERT => 'logModel3dHistoryInsert',
        ];
    }

    public function writeAttributesToLog($attributesToLog, $comment)
    {
        if (!empty($attributesToLog)) {
            $source = [];
            $result = [];
            foreach ($attributesToLog as $attribute => $values) {
                $source[$attribute] = $values['source'];
                $result[$attribute] = $values['result'];
            }
            $userId = UserFacade::getCurrentUserId();

            $model3d = call_user_func($this->getModel3dFunction, $this->owner);
            Model3dHistory::log($userId, $model3d, $this->actionName, $comment, json_encode($source), json_encode($result));
        }
    }

    public function logChangedAttributes($attributes)
    {
        $attributesToLog = [];
        foreach ($attributes as $k => $at) {
            if ($attributes[$k] == $this->owner->attributes[$k]) {
                continue;
            }
            if (in_array($k, $this->skipAttributes, true)) {
                continue;
            }

            if ($this->activeAttributes && (!in_array($k, $this->activeAttributes, true))) {
                continue;
            }
            $attributesToLog[$k]  = [
                'source' => $attributes[$k],
                'result' => $this->owner->attributes[$k]
            ];
        }
        if ($this->formAttributesLogFunction) {
            $attributesToLog = call_user_func($this->formAttributesLogFunction, $attributesToLog, $this->owner);
        }
        if ($this->formCommentFunction) {
            $comment = call_user_func($this->formCommentFunction, $attributesToLog, $this->owner);
        } else {
            $comment = null;
        }
        $this->writeAttributesToLog($attributesToLog, $comment);
    }

    public function logModel3dHistoryUpdate($event)
    {
        $attributes = $event->changedAttributes;
        $this->logChangedAttributes($attributes);
    }

    public function logModel3dHistoryInsert($event)
    {
        $attributes = $event->changedAttributes;
        $this->logChangedAttributes($attributes);
    }
}