<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.06.18
 * Time: 15:00
 */

namespace common\components;

use common\models\repositories\UserSessionRepository;

class AsyncSession
{
    public function getTimeout()
    {
        return 864000; // 10 days
    }

    protected function getSessionId()
    {
        $sessionId = \Yii::$app->session->get('user_session_id');
        if (!$sessionId) {
            $sessionId = \Yii::$app->session->getId();
        }
        return $sessionId;
    }

    protected function getRKey($key)
    {
        return 'as_' . $key . ':as_' . $this->getSessionId();
    }

    public function set($key, $value)
    {
        \Yii::$app->get('redis')->executeCommand('SET', [
            $this->getRKey($key),
            json_encode($value),
            'EX',
            $this->getTimeout()
        ]);

    }

    public function get($key, $defaultValue = [])
    {
        $data = \Yii::$app->get('redis')->executeCommand('GET', [
            $this->getRKey($key),
        ]);
        if ($data === null) {
            $data = $defaultValue;
        } else {
            $data = json_decode($data, true);
        }
        return $data;
    }

    protected function resetSessionId($sessionId)
    {
        $searchKeyCondition = '*as_' .$sessionId;
        $keys = \Yii::$app->redis->executeCommand('KEYS', [$searchKeyCondition]);
        if ($keys) {
            \Yii::$app->redis->executeCommand('DEL', $keys);
        }
    }

    public function reset()
    {
        $userSessionId = \Yii::$app->session->get('user_session_id');
        if ($userSessionId) {
            $this->resetSessionId($userSessionId);
        }
        $this->resetSessionId(\Yii::$app->session->getId());
    }
}