<?php

namespace common\components;

class TextHelper
{
    public static function filterUtf8($string)
    {
        return preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $string);
    }
}