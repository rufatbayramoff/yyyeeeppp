<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.08.16
 * Time: 17:24
 */


namespace common\components;


use common\interfaces\Model3dBasePartInterface;
use common\models\Model3dPart;
use common\models\PrinterMaterial;
use common\modules\psPrinterListMaps\components\FilepageVolumeRepository;
use common\services\Model3dPartService;
use common\services\Model3dService;
use lib\MeasurementUtil;

class Model3dWeightCalculator
{
    /**
     * Calulate weight for model file
     *
     * Weight of model =
     * = Volume * K1 +Area * K2
     * K1 = 0,00025 = 1,25*0,2/1000   where 1,25 = density of PLA, 0,2 = 20% percent infill for model
     * K2 = 0,0008 = 1,25*0,8*(1-0,2)/1000 where 0,8 is thickness, 1,25 = density of PLA, 0,2 = 20% percent infill
     * we assume model measurements in MM
     *
     * @param  Model3dBasePartInterface $model3dPart
     * @param PrinterMaterial|null $printerMaterial
     * @return float
     * @internal param $material
     */
    public static function calculateModel3dPartWeight(Model3dBasePartInterface $model3dPart, $printerMaterial = null)
    {
        $texture = $model3dPart->getCalculatedTexture();

        if ($model3dPart->model3dPartProperties && $model3dPart->model3dPartProperties->volumeDirect) {
            $volume = $model3dPart->model3dPartProperties->volumeDirect;
        } else {
            $volume = $model3dPart->getVolume(); // qty inside
        }

        if ($printerMaterial !== null) {
            $density = $printerMaterial->density;
        } else {
            if ($material = $texture->printerMaterial) {
                $density = $material->density;
            } else {
                $materialGroup = $texture->printerMaterialGroup;
                $density = $materialGroup->density;
            }
        }

        $weight = $density * $volume;
        return $weight;
    }

    public static function calculateModel3dPartSupportWeight(Model3dBasePartInterface $model3dPart, $printerMaterial = null)
    {
        $texture = $model3dPart->getCalculatedTexture();
        $supportsVolume = Model3dPartVolumeCalulator::calcualteModel3dPartSupportVolume($model3dPart);
        if ($printerMaterial !== null) {
            $density = $printerMaterial->density;
        } else {
            if ($material = $texture->printerMaterial) {
                $density = $material->density;
            } else {
                $materialGroup = $texture->printerMaterialGroup;
                $density = $materialGroup->density;
            }
        }

        $weight = $density * $supportsVolume;
        return $weight;
    }
}