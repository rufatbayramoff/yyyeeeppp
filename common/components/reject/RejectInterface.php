<?php
/**
 * Created by mitaichik
 */

namespace common\components\reject;


interface RejectInterface
{
    /**
     * Return reject comment.
     * Return only part which input user or moderator, without reason.
     * @return string|null
     */
    public function getComment() : ?string;

    /**
     * Return reason title, which was selected from reason list.
     * @return string|null
     */
    public function getReason() : ?string;

    /**
     * Formatted text represent comment and reason title
     * @return string
     */
    public function getText() : string;

    /**
     * gets one line text, like:
     * Reason text. Description.
     *
     * @return string
     */
    public function getTextOneLine(): string;
}