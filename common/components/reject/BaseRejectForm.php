<?php
/**
 * Created by mitaichik
 */

namespace common\components\reject;


use common\components\ArrayHelper;
use common\models\SystemReject;
use yii\base\Model;

/**
 * Class BaseRejectForm
 * @package common\components\reject
 */
abstract class BaseRejectForm extends Model implements RejectInterface
{
    /**
     * Reject reason id.
     * Id of SystemReject record.
     * @var int
     */
    public $reasonId;

    /**
     * Comment for reject reason.
     * @var string
     */
    public $reasonDescription;

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            ['reasonId', 'number'],
            ['reasonDescription', 'safe']
        ];
    }

    /**
     * Return group of reject reasons for form.
     * List of group you can find in SystemReject constants or system_reject table.
     * @return string
     */
    abstract protected static function getRejectGroup() : string;

    /**
     * Return array of reject reasons as id => title.
     * Result can be userful for select input.
     * @return array
     */
    public static function getSuggestList() : array
    {
        $data = SystemReject::find()
            ->forSelect(static::getRejectGroup())
            ->all();

        return ArrayHelper::map($data, 'id', 'title');
    }

    /**
     * Return array of reject reasons as [['id' => '...', 'title' => '...' ]]
     * Result can be userful for angular.
     * @return array
     */
    public static function getSuggestObjectList() : array
    {
        $data = SystemReject::find()
            ->forSelect(static::getRejectGroup())
            ->all();

        return ArrayHelper::toArray($data, [SystemReject::class => ['id', 'title']]);
    }

    /**
     * @inheritdoc
     */
    public function getComment() : ?string
    {
        return $this->reasonDescription;
    }

    /**
     * @inheritdoc
     */
    public function getReason() : ?string
    {
        if ($this->reasonId) {
            $systemReject = SystemReject::findByPk($this->reasonId);
            if ($systemReject) {
                return $systemReject->title;
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getText() : string
    {
        $text = '';

        if($this->reasonId){
            $systemReject = SystemReject::findByPk($this->reasonId);
            $title = $systemReject->title ?? null;

            $text.= _t('site.reject', 'Reason: {reason}', ['reason' => $title]);
        }

        if($this->reasonDescription){

            if ($text) {
                $text.=', ';
            }
            $text.= _t('site.reject', 'Description: {description}', ['description' => $this->reasonDescription]);
        }

        return $text;
    }

    /**
     * @inheritdoc
     */
    public function getTextOneLine(): string
    {
        $text = [];
        if ($this->reasonId) {
            $systemReject = SystemReject::findByPk($this->reasonId);
        }
        if ($this->reasonDescription) {
            $text[] = $this->reasonDescription;
        }
        return implode(". ", $text);
    }
}