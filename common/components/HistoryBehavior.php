<?php

namespace common\components;

use yii\helpers\Json;

/**
 * HistoryBehavior
 *
 * Usage example:
 *
 *  public function behaviors()
 * {
 * return [
 * 'history' => [
 * 'class' => \common\components\HistoryBehavior::className(),
 * 'historyClass' => PaymentTransactionHistory::className(), // not required if with _history prefix
 * 'foreignKey' => 'transaction_id'
 * ],
 * ];
 * }
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class HistoryBehavior extends \yii\base\Behavior
{
    /**
     * class to be used for saving history
     *
     * @var string
     */
    public $historyClass;

    /**
     * fk with owner
     *
     * @var string
     */
    public $foreignKey;
    /**
     * get fk value before validate
     *
     * @var string
     */
    public $foreignKeyValue;
    /**
     *
     * @var array
     */
    private $oldAttributes = [];
    /**
     * which attributes to skip
     *
     * @var array
     */
    public $skipAttributes = [];

    /**
     * with which action_id to log history
     *
     * @var string
     */
    public $actionCode = 'update';

    public $ownerIdField = 'id';

    /**
     * which fields to save from current table to history table, without checking if they are changed or not.
     * For example ['id'=>'object_related_id'] where we have 2 tables, object + object_related and use only 1 history table object_history
     *
     * @var array
     */
    public $addonFields = [];

    public function events()
    {
        return [
            \yii\db\ActiveRecord::EVENT_AFTER_UPDATE    => 'afterUpdate',
            \yii\db\ActiveRecord::EVENT_BEFORE_DELETE   => 'beforeDelete',
            \yii\db\ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    /**
     * prepare configuration with owner
     *
     * @param type $owner
     * @throws \Exception
     */
    public function attach($owner)
    {
        parent::attach($owner);
        if (empty($this->historyClass)) {
            $this->historyClass = get_class($this->owner) . 'History';
        }
        if (empty($this->foreignKey)) {
            throw new \Exception("Please specify foreignKey for history behavior");
        }
    }


    /**
     *
     * @param \yii\base\Event $e
     */
    public function beforeValidate($e)
    {
        $this->oldAttributes = $this->owner->getAttributes();
    }

    /**
     * call save history on update
     *
     * @param type $e
     */
    public function afterUpdate($e)
    {
        $this->saveHistory($e);
    }

    /**
     *
     * @param \yii\base\ModelEvent $e
     * @return boolean
     */
    public function beforeDelete($e)
    {
        if (empty($this->owner->{$this->ownerIdField})) {
            return true;
        }
        $sender = $e->sender;
        if (empty($sender)) {
            return true;
        }
        $realChanged = [];
        foreach ($sender->getOldAttributes() as $k => $v) {
            if ($k == 'updated_at' || in_array($k, $this->skipAttributes)) {
                continue;
            }
            $realChanged[$k] = $v;
        }
        if (empty($realChanged)) {
            return true;
        }
        call_user_func([$this->historyClass, 'addRecord'], [
            $this->foreignKey => $this->owner->{$this->ownerIdField},
            'created_at'      => dbexpr('now()'),
            'action_id'       => $this->actionCode . "_delete",
            'comment'         => \yii\helpers\Json::encode($realChanged),
            'user_id'         => (int)\frontend\models\user\UserFacade::getCurrentUserId()
        ]);
    }

    /**
     * save history
     *
     * @param \yii\db\AfterSaveEvent $e
     */
    protected function saveHistory(\yii\db\AfterSaveEvent $e)
    {
        $userId = (int)\frontend\models\user\UserFacade::getCurrentUserId();
        $changeAttributes = $e->changedAttributes;

        if (empty($changeAttributes)) {
            return true;
        }

        $realChanged = [];
        foreach ($changeAttributes as $k => $v) {
            if ($k == 'updated_at' || in_array($k, $this->skipAttributes)) {
                continue;
            }
            if (isset($this->oldAttributes[$k]) && $this->oldAttributes[$k] != $v) {
                $realChanged[$k] = ['from' => $v, 'to' => $this->oldAttributes[$k]]; // Save from to
            }
        }

        if (empty($realChanged)) {
            return true;
        }
        if (!empty($this->addonFields)) {
            foreach ($this->addonFields as $k => $v) {
                $this->addonFields[$k] = $this->owner->$v;
            }
            $realChanged = array_merge($realChanged, $this->addonFields);
        }
        call_user_func([$this->historyClass, 'addRecord'], [
            $this->foreignKey => $this->owner->{$this->ownerIdField},
            'created_at'      => DateHelper::now(),
            'action_id'       => $this->actionCode,
            'comment'         => Json::encode($realChanged),
            'user_id'         => $userId
        ]);
    }
}
