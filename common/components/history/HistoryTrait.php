<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\history;


use common\components\ArrayHelper;
use frontend\models\user\UserFacade;
use yii\db\Expression;
use yii\helpers\Json;

/**
 * Class HistoryTrait
 * @package common\components\history
 */
trait HistoryTrait
{
    /**
     * Add history record
     * @param string      $action  Action
     * @param mixed|null  $data    Log data
     * @param string|null $comment Comment
     */
    public function addHistoryRecord($action, $data = null, $comment = null)
    {
        $data = $data ? Json::encode(ArrayHelper::toArray($data)) : null;

        \Yii::$app->db->createCommand()
            ->insert($this->getHistoryTable(), [
                $this->getHistoryOwnerIdField() => $this->getPrimaryKey(),
                'user_id' => (int)UserFacade::getCurrentUserId(),
                'created_at' => new Expression('NOW()'),
                'action_id' => $action,
                'comment' => $comment,
                'data' => $data
            ])
            ->execute();
    }

    /**
     * Return table name with historu log
     * @return string
     */
    protected function getHistoryTable()
    {
        return $this->tableName().'_history';
    }

    /**
     * Return field in history table, who must contain id of owner model
     * @todo remove after reducing tables to a single mind
     * @return string
     */
    protected function getHistoryOwnerIdField()
    {
        return $this->tableName().'_id';
    }
}