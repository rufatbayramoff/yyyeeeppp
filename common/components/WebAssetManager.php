<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 02.12.16
 * Time: 15:42
 */

namespace common\components;

use yii\helpers\FileHelper;
use yii\web\AssetManager;

class WebAssetManager extends AssetManager
{
    /**
     * Don`t check updates for this dirs list
     */
    protected const SKIP_DIRS = [
        'app/vendor',
        'modules/translation'
    ];

    protected function publishDirectory($src, $options)
    {
        if (!$this->linkAssets && !$this->forceCopy && empty($options['forceCopy']) && YII_DEBUG) {
            $inSkipList = false;
            foreach (self::SKIP_DIRS as $skipDir) {
                if (strpos($src, $skipDir)) {
                    $inSkipList = true;
                    break;
                }
            }
            if (!$inSkipList) {
                $dir = $this->hash($src);
                $dstDir = $this->basePath . DIRECTORY_SEPARATOR . $dir;
                // Check files dates and renew files
                FileHelper::copyDirectory(
                    $src,
                    $dstDir,
                    [
                        'beforeCopy' => function ($from, $to) {
                            if (is_file($from)) {
                                if (@filemtime($to) > @filemtime($from)) {
                                    // Same file date, dont copy it
                                    return false;
                                }
                            }
                            return true;
                        }

                    ]
                );
            }
        }
        return parent::publishDirectory($src, $options); // TODO: Change the autogenerated stub

    }
}