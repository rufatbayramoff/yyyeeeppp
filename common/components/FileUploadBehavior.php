<?php
namespace common\components;

use yii\base\Behavior;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\validators\Validator;
use yii\web\UploadedFile;

/**
 * Upload file behavior
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class FileUploadBehavior extends Behavior
{
    /**
     * default file attribute;
     * @var type 
     */
    public $fileAttribute = 'file';    
    
    public $uploadPath = '@frontend/web/static/uploads';
     
    /**
     * limit file types
     * 
     * @var type 
     */
    public $fileTypes = 'png,jpeg,jpg';

    public function events()
    {
        return [
            Model::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }
    
    /**
     * 
     * @param type $event
     * @return boolean
     */
    public function beforeValidate($event)
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        if ($model->{$this->fileAttribute} instanceof UploadedFile) {
            $file = $model->{$this->fileAttribute};
        } else {
            $file = UploadedFile::getInstance($model, $this->fileAttribute);
        }
        if ($file == null) {
            // some hack to prevent rewrite image if already uploaded
            $oldName = app('request')->post('old_' . $this->fileAttribute);
            if (!empty($oldName)) {
                $this->owner->setAttribute($this->fileAttribute, $oldName);
            }
            return true;
        }
        if ($file && $file->name) {
            $model->{$this->fileAttribute} = $file;
            $validator = Validator::createValidator(
                'file', 
                $model, 
                $this->fileAttribute,  [
                'extensions'=>$this->fileTypes,
            ]);
            $validator->validateAttribute($model, $this->fileAttribute);
        }
        return $this->saveFile($file);
    }    
    /**
     * save uploaded file
     * 
     * @param UploadedFile $file
     * @return type
     */
    private function saveFile(UploadedFile $file)
    {
        // save file    
        $prefix = \Yii::getAlias($this->uploadPath);
        if(!file_exists($prefix)){            
            throw \lib\file\UploadException("Specified folder doesn't exist");
        }
        $baseName = $this->sanitize($file->baseName);
        $uploadedFile  = $baseName . '.' . $file->extension;        
        $this->owner->setAttribute($this->fileAttribute, $uploadedFile);
        $imgUrl = $prefix . DIRECTORY_SEPARATOR . $uploadedFile;
        return $file->saveAs($imgUrl);
    }
    
    /**
     * add uploader hidden elements and image preview
     * 
     * @return type
     */
    public function initUploader()
    {
        $preview = '';
        $currentVal = $this->owner->{$this->fileAttribute};
        if(!empty($currentVal)){
            $url = param('staticUrl') .'/uploads/' . $currentVal;
            $preview = \yii\helpers\Html::img($url, ['width'=>150]);
        }
        $hidden = \yii\helpers\Html::hiddenInput('old_'.$this->fileAttribute, $currentVal);        
        return $preview . $hidden;
    }
    /**
     * 
     * http://stackoverflow.com/questions/2668854/sanitizing-strings-to-make-them-url-and-filename-safe
     * 
     * @param type $string
     * @param type $force_lowercase
     * @param type $anal
     * @return type
     */
    private function sanitize($string, $force_lowercase = true, $anal = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                       "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                       "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
        return ($force_lowercase) ? mb_strtolower($clean, 'UTF-8') : $clean;
    }
}