<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.04.18
 * Time: 15:05
 */

namespace common\components;

use common\models\factories\FileFactory;
use common\models\factories\UploadedFileFactory;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

class AbstractPopulator
{
    /** @var FileFactory */
    public $fileFactory;

    public function __construct(FileFactory $fileFactory)
    {
        $this->fileFactory = $fileFactory;
    }

    protected function populateAttributes($model, $data, $attributes)
    {
        foreach ($attributes as $attribute) {
            if (array_key_exists($attribute, $data)) {
                $model->$attribute = $data[$attribute];
            }
            $attributeCamelCase = Inflector::variablize($attribute);
            if (($attribute !== $attributeCamelCase) && array_key_exists($attributeCamelCase, $data)) {
                $model->$attribute = $data[$attributeCamelCase];
            }
        }
    }

    protected function populateFileAttributes($filesData, $getFileByUuidFunction)
    {
        foreach ($filesData as $key => $value) {
            if ($value === 'deleted') {
                if ($forDeletePhoto = $getFileByUuidFunction($key)) {
                    $forDeletePhoto->forDelete = true;
                }
            }
        }
    }

    /**
     * @param $model
     * @param $fileParameterName string Parameter $model should have method set$fileParameterName
     * @see Product::setCoverImageFile
     * @throws \Exception
     */
    public function populateFile($model, $fileParameterName, UploadedFile $uploadedFile = null)
    {
        $uploadFile = $uploadedFile ? $uploadedFile : UploadedFile::getInstance($model, $fileParameterName);
        if ($uploadFile) {
            $file = $this->fileFactory->createFileFromUploadedFile($uploadFile);
            $setMethodName = 'set' . ucfirst($fileParameterName);
            $model->$setMethodName($file);
        }
        return $this;
    }


    /**
     * @param $model
     * @param $filesParameterName
     * @throws \Exception
     */
    public function populateFiles($model, $filesParameterName, array $uploadedFiles = null)
    {
        $uploadFiles = $uploadedFiles ? $uploadedFiles : UploadedFileFactory::getInstances($model, $filesParameterName);
        if ($uploadFiles) {
            $imageFiles = $this->fileFactory->createFilesFromUploadedFiles($uploadFiles);
            $addMethodName = 'add' . ucfirst($filesParameterName);
            $model->$addMethodName($imageFiles);
        }
        return $this;
    }
}