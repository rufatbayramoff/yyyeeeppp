<?php

namespace common\components;

use common\components\exceptions\BusinessException;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\VarDumper;

/**
 * Active Record extended functions
 * All base/models are extended from this class
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class BaseAR extends \yii\db\ActiveRecord
{
    public static $enableLog = false;

    use XssArTestTrait;

    public function init()
    {
        parent::init();
        $this->includeEvents();
    }

    /**
     * include model events if exists
     *
     * @throws \yii\base\InvalidParamException
     */
    private function includeEvents()
    {
        $class = str_replace('common\models\\', '', get_called_class());
        $classFile = str_replace("\\", DIRECTORY_SEPARATOR, $class);
        $path = Yii::getAlias('@common/events/' . $classFile);

        if (file_exists($path . '.php')) {
            require_once(Yii::getAlias($path . '.php'));
        }
    }

    public function update($runValidation = true, $attributeNames = null)
    {
        if (self::tableName() == 'system_setting') { // dirty hack
            app('cache_settings')->flush();
        }
        return parent::update($runValidation, $attributeNames);
    }

    /**
     * get table column for joins
     *
     * @param string $colName
     * @return string
     */
    public static function column($colName)
    {
        return static::tableName() . '.' . $colName;
    }

    /**
     * find model object
     *
     * @return BaseActiveQuery
     */
    public static function find()
    {
        return \Yii::createObject(BaseActiveQuery::className(), [get_called_class()]);
    }

    public static function instantiate($row)
    {
        return \Yii::createObject(static::class);
    }

    /**
     * fast way to add record to table
     *
     * @param array $params
     * @return static
     */
    public static function addRecord(array $params)
    {
        $obj = \Yii::createObject(get_called_class());
        foreach ($params as $k => $v) {
            $obj->$k = $v;
        }
        $obj->safeSave();
        /** @var static $obj */
        return $obj;
    }

    /**
     * try to update record
     *
     * @param int $id
     * @param array $params
     * @return static
     */
    public static function updateRecord($id, array $params)
    {
        $class = \Yii::createObject(get_called_class());
        /** @var BaseAR $obj */
        $obj = $class::findByPk($id);
        $obj->setAttributes($params);
        $obj->safeSave();
        return $obj;
    }

    /**
     * BaseAR::update('age > 30', ['status' => 1]);
     *
     * @param array $condition
     * @param array $attributes
     * @return integer number of rows affected by the execution.
     */
    public static function updateRow($condition, $attributes)
    {
        $command = static::getDb()->createCommand();
        $command->update(static::tableName(), $attributes, $condition);
        return $command->execute();
    }

    /**
     * @param $pk
     * @return null|static
     */
    public static function findByPk($pk)
    {
        $pkField = static::primaryKey();
        return self::findOne([$pkField[0] => (int)$pk]);
    }

    /**
     *
     * @param integer $pk
     * @param string $failedText
     * @return static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function tryFindByPk($pk, $failedText = 'Not found')
    {
        $obj = self::findOne(['id' => (int)$pk]);
        if (empty($obj)) {
            \Yii::info(VarDumper::dumpAsString($pk) . static::className(), 'base_ar');
            throw new \yii\web\NotFoundHttpException($failedText);
        }
        return $obj;
    }

    /**
     *
     * @param $args
     * @param string $failedText
     * @return static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function tryFind($args, $failedText = 'Not found')
    {
        $obj = self::findOne($args);
        if (empty($obj)) {
            \Yii::info(VarDumper::dumpAsString($args) . static::className(), 'base_ar');
            throw new \yii\web\NotFoundHttpException($failedText);
        }
        return $obj;
    }

    /**
     * saves model and checks if hasErrors, if has - logs them
     *
     * @param null $attributeNames
     *
     * @return bool
     * @throws BusinessException
     */
    public function safeSave($attributeNames = null)
    {
        $result = $this->save(true, $attributeNames);
        if ($this->hasErrors()) {
            \Yii::info(var_export(self::className(), true), 'base_ar');
            \Yii::info(var_export($this->getErrors(), true), 'base_ar');
            throw new BusinessException(self::className() . ": " . \yii\helpers\Html::errorSummary($this), true);
        }
        return $result;
    }


    public function beforeSaveMethod($insert)
    {
        if (\defined('TEST_XSS') && TEST_XSS) {
            $this->removeXssInput();
        }
        return true;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if (!$this->beforeSaveMethod($this->isNewRecord)) {
            return false;
        }
        if (in_array(static::tableName(), BaseActiveQuery::getModelsToCache())) {
            touch(BaseActiveQuery::getCacheDependencyFilePath());
        }
        return parent::save($runValidation, $attributeNames); // TODO: Change the autogenerated stub
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     *
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (BaseAR::$enableLog) {
            app()->httpRequestLogger->logActiveRecordInfo($this, $this->getDiffLogAttributes($changedAttributes));
        }

        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function afterFind()
    {
        parent::afterFind(); // TODO: Change the autogenerated stub
        if (\defined('TEST_XSS') && TEST_XSS) {
            $this->fixXssAttributes();
        }
    }

    /**
     * @param $changedAttributes
     *
     * @return array
     */
    protected function getDiffLogAttributes($changedAttributes): array
    {
        if (empty($changedAttributes)) {
            return [];
        }

        $diff = [];

        foreach ($changedAttributes as $attr => $value) {
            $newValue = $this->getAttribute($attr);

            if ($value == $newValue) {
                continue;
            }

            if ($newValue instanceof Expression) {
                if (mb_strtolower((string)$newValue) === 'now()') {
                    $newValue = DateHelper::now();
                } else {
                    $newValue = (string)$newValue;
                }
            }

            $diff[$attr] = [
                'oldValue' => $value,
                'newValue' => $newValue
            ];
        }

        return $diff;
    }

    /**
     * Fast way to get data provider for given ActiveRecord class
     *
     * @param array $where
     * @param int $pageSize
     * @param array $params
     * @return ActiveDataProvider
     */
    public static function getDataProvider(array $where = [], $pageSize = 20, $params = [])
    {
        $items = self::find()->where($where);
        $dataProvider = new ActiveDataProvider(
            ArrayHelper::merge(
                [
                    'query'      => $items,
                    'pagination' => [
                        'pageSize' => $pageSize,
                    ],

                ],
                $params
            )
        );
        return $dataProvider;
    }
}
