<?php
/**
 * Created by mitaichik
 */

namespace common\components;


use common\models\StoreOrderReviewShare;
use frontend\models\user\UserFacade;
use yii\helpers\Url;

class ShareUrlHelper
{
    /**
     * URL to share page.
     *
     * @param StoreOrderReviewShare $share
     * @param bool $public
     * @return string
     */
    public static function url(StoreOrderReviewShare $share, bool $public = false) : string
    {
        return Url::toRoute(['/reviews/view-share', 'shareId' => $share->id, 'reviewId' => $share->review_id], $public);
    }

    /**
     * URL to main photo of share (with watermark).
     *
     * @param StoreOrderReviewShare $share
     * @param bool $public
     * @return string|null
     */
    public static function mainPhotoUrl(StoreOrderReviewShare $share, bool $public = false) : ?string
    {
        if (!$share->photo) {
            return null;
        }

        $relativeUrl = UserFacade::getUserFolderUrl($share->review->ps->user_id, true) . "/share/share_{$share->id}.jpg";

        return $public
            ? param('server') . $relativeUrl
            : $relativeUrl;
    }
}