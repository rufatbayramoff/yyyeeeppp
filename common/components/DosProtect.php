<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.10.17
 * Time: 15:49
 */

namespace common\components;

use Yii;
use yii\base\Component;
use yii\web\Cookie;

class DosProtect extends Component
{
    public $salt = 'VKs=d9apwksAKspowdja';

    public function checkDpKey($dpKey)
    {
        [$checkMd5, $checkTimestamp, $checkRandInt] = explode('_', $dpKey);

        $remoteAddr = $_SERVER['REMOTE_ADDR'];
        $md5Str = $this->salt . '_' . $remoteAddr . '_' . $checkTimestamp . '_' . $checkRandInt;
        $md5 = md5($md5Str);

        if (($md5 === $checkMd5) && ($checkTimestamp > (time() - 60 * 60 * 24 * 30))) {
            return true;
        }
        return false;

    }

    public function getCookieDomain()
    {
        $mainDomain = $_SERVER['HTTP_HOST'] ?? \Yii::$app->getModule('intlDomains')->mainDomain;
        $tod1 = strrpos($mainDomain, '.');
        if ($tod1) {
            $shiftedDomain = substr($mainDomain, 0, $tod1);
            $tod2 = strrpos($shiftedDomain, '.');
            if ($tod2) {
                $mainDomain = substr($mainDomain, $tod2, 1024);
            }
        }
        return $mainDomain;
    }

    public function requestGetToCookieDpParam()
    {
        if (($dpKey = Yii::$app->request->get('dp')) && (!isset($_COOKIE['dp']))) {
            if ($this->checkDpKey($dpKey)) {
                $cookieDomain = $this->getCookieDomain();
                setcookie(
                    'dp',
                    $dpKey,
                    time() + 60 * 60 * 24 * 30,
                    '/',
                    $cookieDomain,
                    false,
                    true
                );
            }
        }

    }
}