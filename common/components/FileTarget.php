<?php

namespace common\components;

class FileTarget extends \yii\log\FileTarget
{
    public function formatMessage($message)
    {
        $parentMessage = parent::formatMessage($message);
        if ((YII_ENV === 'dev' || YII_ENV === 'jenkins') && (array_key_exists('HTTP_RUNNINGTEST', $_SERVER))) {
            return $parentMessage ."\nTest: ". $_SERVER['HTTP_RUNNINGTEST'];

        }
        return $parentMessage;
    }
}