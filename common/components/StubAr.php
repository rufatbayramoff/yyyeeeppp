<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.04.17
 * Time: 10:46
 */

namespace common\components;


trait StubAr
{
    public function getErrors()
    {
        return [];
    }

    public function safeSave()
    {
    }

    public static function primaryKey()
    {
        return 'id';
    }

    public function attributes()
    {
        return [];
    }

    public function getAttributes()
    {
        $attributes = get_object_vars($this);
        return $attributes;
    }

    public function getAttribute($name)
    {
        return $this->$name;
    }

    public function setAttribute($name, $value)
    {
        $this->$name = $value;
    }

    public function hasAttribute($name)
    {
        if (property_exists($this, $name)) {
            return true;
        }
        return false;
    }

    public function getPrimaryKey($asArray = false)
    {
        return $this->id;
    }

    public function getOldPrimaryKey($asArray = false)
    {
        return null;
    }

    public static function isPrimaryKey($keys)
    {
        return false;
    }

    public static function find()
    {
        return [];
    }

    public static function findOne($condition)
    {
        return [];
    }

    public static function findAll($condition)
    {
        return [];
    }


    public static function updateAll($attributes, $condition = null)
    {
    }

    public static function deleteAll($condition = null)
    {
    }

    public function save($runValidation = true, $attributeNames = null)
    {
    }

    public function insert($runValidation = true, $attributes = null)
    {
    }

    public function update($runValidation = true, $attributeNames = null)
    {
    }

    public function delete()
    {
    }

    public function getIsNewRecord()
    {
        return true;
    }

    public function equals($record)
    {
        return false;
    }


    public function getRelation($name, $throwException = true)
    {
        return null;
    }

    public function populateRelation($name, $records)
    {
    }

    public function link($name, $model, $extraColumns = [])
    {
    }


    public function unlink($name, $model, $delete = false)
    {
    }


    public static function getDb()
    {
    }
}
