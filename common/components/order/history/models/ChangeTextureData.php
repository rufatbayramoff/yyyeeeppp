<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history\models;


class ChangeTextureData
{
    /**
     * @var ChangeTextureItem
     */
    public $oldTexture;

    /**
     * @var ChangeTextureItem
     */
    public $newTexture;

    public function __construct(ChangeTextureItem $oldTexture, ChangeTextureItem $newTexture)
    {
        $this->oldTexture = $oldTexture;
        $this->newTexture = $newTexture;
    }
}