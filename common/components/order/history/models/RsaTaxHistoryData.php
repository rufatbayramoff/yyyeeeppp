<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history\models;


class RsaTaxHistoryData
{
    /**
     * @var float
     */
    public $fee;

    /**
     * @var float
     */
    public $rsaFee;

}