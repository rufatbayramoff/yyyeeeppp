<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history\models;


class ChangePaymentStatusHistoryData
{

    /**
     * @var string
     */
    public $newStatus;

    /**
     * @var string
     */
    public $oldStatus;
}