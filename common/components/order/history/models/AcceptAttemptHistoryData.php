<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history\models;


class AcceptAttemptHistoryData
{
    /**
     * @var int
     */
    public $attemptId;
}