<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history\models;


class ChangeTrackingHistoryData
{
    /**
     * @var int
     */
    public $attemptId;

    /**
     * @var string
     */
    public $newShipper;

    /**
     * @var string
     */
    public $newNumber;

    /**
     * @var string
     */
    public $oldShipper;

    /**
     * @var string
     */
    public $oldNumber;
}