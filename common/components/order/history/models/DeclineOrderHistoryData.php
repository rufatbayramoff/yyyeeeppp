<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history\models;


class DeclineOrderHistoryData
{
    /**
     * @var string
     */
    public $reason;

    /**
     * @var string
     */
    public $comment;
}