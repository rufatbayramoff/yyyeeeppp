<?php
/**
 * User: nabi
 */

namespace common\components\order\history\models;


class RequestMoreTimeHistoryData
{
    public $attemptId;
    public $oldTime;
    public $time;
    public $reason;
}