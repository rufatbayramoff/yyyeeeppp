<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history\models;


class DeclineAttemptHistoryData
{
    /**
     * @var int
     */
    public $attemptId;

    /**
     * @var string
     */
    public $reason;

    /**
     * @var string
     */
    public $comment;
}