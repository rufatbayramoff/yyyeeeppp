<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history\models;


use common\models\StoreOrder;

class ChangeTextureItem
{
    /**
     * @var mixed
     */
    public $kitTexture;

    /**
     * @var mixed
     */
    public $partsTextures;

    /**
     * ChangeTextureData constructor.
     * @param StoreOrder $order
     */
    public function __construct(StoreOrder $order)
    {
        $replica = $order->orderItem->model3dReplica;
        if ($replica->isOneTextureForKit()) {
            $texture = $replica->kitModel3dTexture ?: $replica->getLargestModel3dPart()->getTexture();
            $this->kitTexture = [
                'materialId' => $texture->printer_material_id,
                'colorId' => $texture->printer_color_id,
                'infill'  => $texture->calculateInfill()
            ];
        }
        else {
            $this->partsTextures = [];
            foreach ($replica->getActiveModel3dParts() as $modelPart){
                $texture =  $modelPart->getTexture();
                $this->partsTextures[$modelPart->id] = [
                    'materialId' => $texture->printer_material_id,
                    'colorId' => $texture->printer_color_id,
                    'infill'  => $texture->calculateInfill()
                ];
            }
        }
    }
}