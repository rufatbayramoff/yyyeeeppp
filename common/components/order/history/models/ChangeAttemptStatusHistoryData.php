<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history\models;


class ChangeAttemptStatusHistoryData
{
    /**
     * @var int
     */
    public $attemptId;

    /**
     * @var string
     */
    public $newStatus;

    /**
     * @var string
     */
    public $oldStatus;
}