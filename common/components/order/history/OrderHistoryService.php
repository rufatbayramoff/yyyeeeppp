<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\history;


use common\components\exceptions\AssertHelper;
use common\components\order\history\models\AcceptAttemptHistoryData;
use common\components\order\history\models\ChangeAttemptStatusHistoryData;
use common\components\order\history\models\ChangePaymentStatusHistoryData;
use common\components\order\history\models\ChangeTextureData;
use common\components\order\history\models\ChangeTextureItem;
use common\components\order\history\models\ChangeTrackingHistoryData;
use common\components\order\history\models\DeclineAttemptHistoryData;
use common\components\order\history\models\DeclineOrderHistoryData;
use common\components\order\history\models\ItemAddHistoryData;
use common\components\order\history\models\RequestMoreTimeHistoryData;
use common\components\order\history\models\RsaTaxHistoryData;
use common\components\reject\RejectInterface;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\models\StoreOrder;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderHistory;
use common\models\StoreOrderItem;
use common\models\StoreOrderPosition;
use common\models\User;
use frontend\models\user\FrontUser;
use lib\money\Money;
use yii\helpers\Json;
use yii\web\Application;

/**
 * Class OrderHistoryService
 *
 * @package common\components\order\history
 */
class OrderHistoryService
{
    /**
     * @param StoreOrderAttemp $attempt
     */
    public function logAcceptAttempt(StoreOrderAttemp $attempt)
    {
        $data            = new AcceptAttemptHistoryData();
        $data->attemptId = $attempt->id;

        $this->log($attempt->order, StoreOrderHistory::ORDER_ATTEMPT_ACCEPT, $data, 'Attempt is accepted');
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param RejectInterface $declineForm
     */
    public function logDeclineAttempt(StoreOrderAttemp $attempt, RejectInterface $declineForm)
    {
        $data            = new DeclineAttemptHistoryData();
        $data->attemptId = $attempt->id;
        $data->reason    = $declineForm->getReason();
        $data->comment   = $declineForm->getComment();

        $this->log($attempt->order, StoreOrderHistory::ORDER_ATTEMPT_DECLINE, $data, 'Attempt is declined');
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param string $previousStatus
     */
    public function logReceived(StoreOrderAttemp $attempt, string $previousStatus)
    {
        $this->logAttemptChangeStatus($attempt, $attempt->status, $previousStatus, 'Attempt is received');
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param string $previousStatus
     */
    public function logPrintedAttempt(StoreOrderAttemp $attempt, string $previousStatus)
    {
        $this->logAttemptChangeStatus($attempt, $attempt->status, $previousStatus, 'Attempt is printed');
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param string $previousStatus
     */
    public function logReadyToSent(StoreOrderAttemp $attempt, string $previousStatus)
    {
        $this->logAttemptChangeStatus($attempt, $attempt->status, $previousStatus, 'Attempt is ready to send');
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param string $previousStatus
     */
    public function logPrintingAttempt(StoreOrderAttemp $attempt, string $previousStatus)
    {
        $this->logAttemptChangeStatus($attempt, $attempt->status, $previousStatus, 'Printing attempt');
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param string $previousStatus
     */
    public function logDeliveredAttempt(StoreOrderAttemp $attempt, string $previousStatus)
    {
        $this->logAttemptChangeStatus($attempt, $attempt->status, $previousStatus, 'Attempt is delivered');
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param string $previousStatus
     */
    public function logSendAttempt(StoreOrderAttemp $attempt, string $previousStatus)
    {
        $this->logAttemptChangeStatus($attempt, $attempt->status, $previousStatus, 'Attempt is sent');
    }

    public function logFailedPayment(StoreOrder $order, $operationName, $message)
    {
        $this->log($order, StoreOrderHistory::PAYMENT_OPERATION, [
            'operation' => $operationName,
            'message'   => $message
        ], 'Payment operation failed');
    }

    /**
     * @param StoreOrder $order
     * @param \common\components\reject\RejectInterface $declineForm
     * @param int $initiator
     */
    public function logDeclineOrder(StoreOrder $order, RejectInterface $declineForm, int $initiator)
    {
        $action = $initiator == ChangeOrderStatusEvent::INITIATOR_CLIENT
            ? StoreOrderHistory::USER_CANCEL_ORDER
            : StoreOrderHistory::MODERATOR_CANCEL_ORDER;

        $comment = $initiator == ChangeOrderStatusEvent::INITIATOR_CLIENT
            ? 'User decline order'
            : 'Moderator or system decline order';

        $data          = new DeclineOrderHistoryData();
        $data->reason  = $declineForm->getReason();
        $data->comment = $declineForm->getComment();

        $this->log($order, $action, $data, $comment);
    }

    public function logSwitchDispute(StoreOrder $order)
    {
        $comment = 'Dispute is ' . ($order->is_dispute_open ? 'open' : 'closed');
        $this->log($order, StoreOrderHistory::MODERATOR_SWITCH_DISPUTE, '', $comment);
    }

    public function logComment(StoreOrder $order, $oldComment, $newComment)
    {
        $commentInfo = [
            'oldText' => $oldComment,
            'newText' => $newComment
        ];
        $this->log($order, StoreOrderHistory::MODERATOR_CHANGE_COMMENT, $commentInfo, 'Change comment');
    }

    /**
     * @param StoreOrder $order
     * @param ChangeTextureItem $oldTexture
     */
    public function logChangeTextures(StoreOrder $order, ChangeTextureItem $oldTexture)
    {
        $data = new ChangeTextureData($oldTexture, new ChangeTextureItem($order));
        $this->log($order, StoreOrderHistory::TEXTURE_UPDATE, $data, 'Change models textures');
    }

    public function logManualCancel(PaymentInvoice $invoice)
    {
        $this->log($invoice->storeOrder, StoreOrderHistory::MANUAL_INVOICE_CANCEL, ['invoiceUuid' => $invoice->uuid], 'Manual invoice cancel');
    }

    /**
     * @param StoreOrder $order
     */
    public function logGoToChangeOffer(StoreOrder $order)
    {
        $this->log($order, StoreOrderHistory::ORDER_UPDATED, null, 'Go to change offer');
    }

    /**
     * @param StoreOrder $order
     * @param float $fee
     * @param float $rsaFee
     */
    public function logAddTax(StoreOrder $order, float $fee, float $rsaFee)
    {
        $data         = new RsaTaxHistoryData();
        $data->fee    = $fee;
        $data->rsaFee = $rsaFee;

        $this->log($order, StoreOrderHistory::RSA_TAX, $data, 'Add order tax');
    }

    /**
     * @param StoreOrder $order
     * @param StoreOrderItem $orderItem
     */
    public function logItemAdded(StoreOrder $order, StoreOrderItem $orderItem)
    {
        $data              = new ItemAddHistoryData();
        $data->orderItemId = $orderItem->id;

        $this->log($order, StoreOrderHistory::ITEM_ADDED, $data, 'Add order item');
    }

    /**
     * @param StoreOrder $order
     * @param string $status
     * @param string $previousStatus
     * @param string $comment
     */
    public function logPaymentStatus(StoreOrder $order, string $status, string $previousStatus = null, $comment = 'Change payment status')
    {
        $data            = new ChangePaymentStatusHistoryData();
        $data->oldStatus = $previousStatus;
        $data->newStatus = $status;

        $this->log($order, StoreOrderHistory::PAYMENT_STATUS, $data, $comment);
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param string $oldTime
     * @param string $time
     * @param string $reason
     */
    public function logRequestMoreTime(StoreOrderAttemp $attempt, string $oldTime, string $time, string $reason)
    {
        $order           = $attempt->order;
        $data            = new RequestMoreTimeHistoryData();
        $data->attemptId = $attempt->id;
        $data->oldTime   = $oldTime;
        $data->time      = $time;
        $data->reason    = $reason;
        $this->log($order, StoreOrderHistory::REQUEST_MORE_TIME, $data, 'Request more time');
    }

    public function logScheduledToSentAtTime(StoreOrderAttemp $attempt, string $oldTime, string $time, string $reason)
    {
        $order           = $attempt->order;
        $data            = new RequestMoreTimeHistoryData();
        $data->attemptId = $attempt->id;
        $data->oldTime   = $oldTime;
        $data->time      = $time;
        $data->reason    = $reason;
        $this->log($order, StoreOrderHistory::SCHEDULED_TO_SENT_TIME, $data, 'Scheduled to sent at time');
    }

    /**
     * @param StoreOrderPosition $orderPosition
     * @param string $reason
     * @internal param StoreOrder $order
     */
    public function logDeclinePosition(StoreOrderPosition $orderPosition, string $reason)
    {
        $order = $orderPosition->order;
        $this->log($order, StoreOrderHistory::POSITION_DECLINE, ['position_id' => $orderPosition->id], $reason);
    }

    public function logChangeDeliveryType(StoreOrder $order, string $oldType, string $newType)
    {
        $this->log($order, StoreOrderHistory::MODERATOR_DELIVERY_TYPE, ['oldType' => $oldType, 'newType' => $newType], 'Change delivery type');
    }

    /**
     * @param StoreOrder $storeOrder
     * @param string $newCarrier
     * @throws \common\components\exceptions\InvalidModelException
     * @internal param StoreOrder $order
     */
    public function logChangeCarrier(StoreOrder $order, string $oldCarrier, string $newCarrier)
    {
        $this->log($order, StoreOrderHistory::MODERATOR_CHANGE_CARRIER, ['oldCarrier' => $oldCarrier, 'newCarrier' => $newCarrier], 'Change delivery carrier');
    }

    /**
     * @param StoreOrder $order
     * @param string $message
     */
    public function logPositionPaid(StoreOrder $order, string $message = '')
    {
        $this->log($order, StoreOrderHistory::POSITION_PAID, [], $message);
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param string $status
     * @param string $previousStatus
     * @param string $comment
     */
    protected function logAttemptChangeStatus(StoreOrderAttemp $attempt, string $status, string $previousStatus, string $comment)
    {
        $data            = new ChangeAttemptStatusHistoryData();
        $data->attemptId = $attempt->id;
        $data->oldStatus = $previousStatus;
        $data->newStatus = $status;

        $this->log($attempt->order, StoreOrderHistory::ORDER_ATTEMPT_CHANGE_STATUS, $data, $comment);
    }

    public function logModeratorRejectPrinting(StoreOrderAttemp $attempt, string $rejectText)
    {
        $this->log($attempt->order, StoreOrderHistory::ORDER_ATTEMPT_MODERATOR_PRINT_REJECT, ['reason' => $rejectText], 'Print results rejected');
    }

    /**
     * @param StoreOrder $order
     * @param string $action
     * @param mixed $data
     * @param string $comment
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function log(StoreOrder $order, $action, $data, string $comment = null)
    {
        $item             = new StoreOrderHistory();
        $item->action_id  = $action;
        $item->order_id   = $order->id;
        $item->created_at = dbexpr('NOW()');
        $item->comment    = $comment;
        $item->data       = Json::encode($data);


        if (\Yii::$app instanceof Application && $userIdentity = \Yii::$app->user->identity) {
            if ($userIdentity instanceof FrontUser) {
                $item->user_id = $userIdentity->id;
            } else {
                if ($userIdentity instanceof \backend\models\user\UserAdmin) {
                    $item->admin_user_id = $userIdentity->id;
                }
            }
        }
        AssertHelper::assertSave($item);
    }

    public function logAction(StoreOrder $order, $action, $comment = null)
    {
        return $this->log($order, $action, [], $comment);
    }


    public function logRefund(StoreOrder $storeOrder = null, PaymentTransaction $transaction, Money $amount, $comment, User $user = null)
    {
        if ($storeOrder) {
            $this->log($storeOrder, 'refund', ['transaction_id' => $transaction->id, 'amount' => $amount->__toString()], $comment);
        } else {
            $ptHistory                 = new PaymentTransactionHistory();
            $ptHistory->transaction_id = $transaction->id;
            $ptHistory->comment        = ['comment' => $comment];
            $ptHistory->action_id      = 'refund';
            if ($user) {
                $ptHistory->user_id = $user->id;
            }
            $ptHistory->safeSave();
        }
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param $oldShipper
     * @param $oldNumber
     * @param $newShipper
     * @param $newNumber
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function logChangeTracking(StoreOrderAttemp $attempt, $oldShipper, $oldNumber, $newShipper, $newNumber)
    {
        $data = new ChangeTrackingHistoryData();

        $data->attemptId  = $attempt->id;
        $data->oldShipper = $oldShipper;
        $data->oldNumber  = $oldNumber;
        $data->newShipper = $newShipper;
        $data->newNumber  = $newNumber;

        $this->log($attempt->order, StoreOrderHistory::CHANGE_TRACKING, $data, 'Tracking number changed');
    }
}