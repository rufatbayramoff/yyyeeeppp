<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\order\notifyer;


use common\components\exceptions\AssertHelper;
use common\models\StoreOrder;
use yii\helpers\Json;

/**
 * Class OrderNotifyAdapter
 * @package common\components\order\notifyer
 */
class OrderNotifyAdapter
{
    /**
     * @var StoreOrder
     */
    private $order;

    /**
     * OrderNotifyAdapter constructor.
     * @param StoreOrder $order
     */
    public function __construct(StoreOrder $order)
    {
        $this->order = $order;
    }

    /**
     * @param $template
     * @return bool
     */
    public function isAlreadySended($template)
    {
        $data = $this->getData();
        return isset($data[$template]);
    }

    /**
     * @param $template
     */
    public function logSend($template)
    {
        $data = $this->getData();
        $data[$template] = date('Y-m-d H:i:s');
        $this->saveData($data);

    }
    
    /**
     * @return array|mixed
     */
    private function getData()
    {
        return $this->order->messages ? Json::decode($this->order->messages) : [];
    }

    /**
     * @param array $data
     * @throws \common\components\exceptions\InvalidModelException
     */
    private function saveData(array $data)
    {
        $this->order->messages = Json::encode($data);
        AssertHelper::assertSave($this->order);
    }
}