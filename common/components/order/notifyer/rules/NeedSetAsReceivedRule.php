<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\order\notifyer\rules;


use common\components\order\notifyer\OrderNotifyAdapter;
use common\components\order\notifyer\RuleInterface;
use common\models\base\User;
use common\models\query\StoreOrderAttempQuery;
use common\models\StoreOrder;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrderAttemp;

class NeedSetAsReceivedRule implements RuleInterface
{
    /**
     * @param StoreOrderAttempQuery $query
     */
    public function prepareQuery(StoreOrderAttempQuery $query)
    {
        $query
            ->inStatus([StoreOrderAttemp::STATUS_PRINTED, StoreOrderAttemp::STATUS_SENT, StoreOrderAttemp::STATUS_DELIVERED])
            ->joinWith('dates', false)
            ->andWhere(['<', 'finish_print_at', date('Y-m-d H:i:s', time() - 86400 * 5)])
            ->joinWith(['order' => function (StoreOrderQuery $query) {
                $query
                    ->inProcess()
                    ->notTest();
            }]);
        return $query;
    }

    /**
     * Return template for notify
     * @return string
     */
    public function getTemplate()
    {
        return 'needSetAsReceived';
    }

    /**
     * Resolve notify params for template
     * @param StoreOrder $order
     * @return array
     */
    public function resolveParams(StoreOrderAttemp $attemp)
    {
        $order = $attemp->order;
        return [
            'username'     => $order->user->username,
            'orderId'      => $order->id,
            'myOrdersLink' => param('server') . '/workbench/orders'
        ];
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @return User
     */
    public function toUser(StoreOrderAttemp $attemp)
    {
        return $attemp->order->user;
    }

    /**
     * Filter
     * @param StoreOrder $order
     * @param OrderNotifyAdapter $notifyAdapter
     * @return bool
     */
    public function filter(StoreOrder $order, OrderNotifyAdapter $notifyAdapter)
    {
        return true;
    }
}