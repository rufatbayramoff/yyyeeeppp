<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\order\notifyer\rules;


use common\components\order\notifyer\OrderNotifyAdapter;
use common\components\order\notifyer\RuleInterface;
use common\models\base\User;
use common\models\query\StoreOrderAttempQuery;
use common\models\StoreOrder;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrderAttemp;
use frontend\modules\workbench\components\OrderServiceUrlHelper;

class Ps2DayAfterAcceptRule implements RuleInterface
{
    /**
     * @param StoreOrderAttempQuery $query
     */
    public function prepareQuery(StoreOrderAttempQuery $query)
    {
        $query
            ->inStatus(StoreOrderAttemp::STATUS_ACCEPTED)
            ->joinWith('dates', false)
            ->andWhere(['<', 'store_order_attemp_dates.accepted_date', date('Y-m-d H:i:s', time() - 86400 * 2)])
            ->joinWith(['order' => function (StoreOrderQuery $query) {
                $query
                    ->inProcess()
                    ->notTest();
            }]);
        return $query;
    }

    /**
     * Filter
     * @param StoreOrder $order
     * @param OrderNotifyAdapter $notifyAdapter
     * @return bool
     */
    public function filter(StoreOrder $order, OrderNotifyAdapter $notifyAdapter)
    {
        return true;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @return User
     */
    public function toUser(StoreOrderAttemp $attemp)
    {
        return $attemp->ps->user;
    }

    /**
     * Return template for notify
     * @return string
     */
    public function getTemplate()
    {
        return 'ps2DayAfterAccept';
    }

    /**
     * Resolve notify params for template
     * @param StoreOrder $order
     * @return array
     */
    public function resolveParams(StoreOrderAttemp $attemp)
    {
        $order = $attemp->order;
        return [
            'orderId'            => $order->id,
            'psName'             => $attemp->ps->title,
            'acceptedOrdersLink' => OrderServiceUrlHelper::viewList(StoreOrderAttemp::STATUS_ACCEPTED, '', true)
        ];
    }
}