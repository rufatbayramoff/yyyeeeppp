<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\order\notifyer\rules;


use common\components\order\notifyer\OrderNotifyAdapter;
use common\components\order\notifyer\RuleInterface;
use common\models\base\User;
use common\models\query\StoreOrderAttempQuery;
use common\models\StoreOrder;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrderAttemp;

class Ps19HoursAfterPrinted implements RuleInterface
{

    /**
     * @param StoreOrderAttempQuery $query
     */
    public function prepareQuery(StoreOrderAttempQuery $query)
    {
        $query
            ->inStatus(StoreOrderAttemp::STATUS_PRINTED)
            ->joinWith('dates', false)
            ->andWhere(['<', 'finish_print_at', date('Y-m-d H:i:s', time() - 3600 * 19)])
            ->joinWith(['order' => function (StoreOrderQuery $query) {
                $query
                    ->inProcess()
                    ->notTest();
            }]);
        return $query;
    }

    /**
     * Filter
     * @param StoreOrder $order
     * @param OrderNotifyAdapter $notifyAdapter
     * @return bool
     */
    public function filter(StoreOrder $order, OrderNotifyAdapter $notifyAdapter)
    {
        return true;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @return User
     */
    public function toUser(StoreOrderAttemp $attemp)
    {
        return $attemp->ps->user;
    }

    /**
     * Return template for notify
     * @return string
     */
    public function getTemplate()
    {
        return 'ps19HoursAfterPrinted';
    }

    /**
     * Resolve notify params for template
     * @param StoreOrder $order
     * @return array
     */
    public function resolveParams(StoreOrderAttemp $attemp)
    {
        $order = $attemp->order;
        return [
            'psName'  => $attemp->ps->title,
            'orderId' => $order->id
        ];
    }
}