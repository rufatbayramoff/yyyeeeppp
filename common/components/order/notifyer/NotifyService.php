<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\order\notifyer;


use common\models\query\StoreOrderAttempQuery;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;

class NotifyService
{
    /**
     * @var array
     */
    public static $postponeRulesClasses = [
        rules\NeedSetAsReceivedRule::class,
        rules\Ps2DayAfterAcceptRule::class,
        rules\PsNewOrder3HourBeforeExpiredRule::class,
        rules\PsNewOrder12HourRule::class,
        rules\Ps19HoursAfterPrinted::class,
    ];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function sendPostponed()
    {
        /** @var RuleInterface[] $postponedRules */
        $postponedRules = [];
        foreach (self::$postponeRulesClasses as $class) {
            $postponedRules[] = \Yii::createObject($class);
        }

        foreach ($postponedRules as $rule) {
            $query = StoreOrderAttemp::find()
                ->joinWith(['order' => function (StoreOrderQuery $query) {
                    $query->notDispute();
                }]);
            $rule->prepareQuery($query);

            /** @var StoreOrderAttemp[] $attempts */
            $attempts = $query->all();
            foreach ($attempts as $attemp) {
                $order = $attemp->order;

                $notifyAdapter = new OrderNotifyAdapter($order);
                if ($notifyAdapter->isAlreadySended($rule->getTemplate())) {
                    continue;
                }

                if (!$rule->filter($order, $notifyAdapter)) {
                    continue;
                }

                $user = $rule->toUser($attemp);
                \Yii::$app->message->send($user, $rule->getTemplate(), $rule->resolveParams($attemp));
                $notifyAdapter->logSend($rule->getTemplate());
            }
        }
    }
}
