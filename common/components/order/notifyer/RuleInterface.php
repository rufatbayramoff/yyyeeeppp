<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components\order\notifyer;


use common\models\base\User;
use common\models\query\StoreOrderAttempQuery;
use common\models\StoreOrder;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrderAttemp;


/**
 * Interface RuleInterface
 * @package common\components\order\notifyer
 */
interface RuleInterface
{
    /**
     * Prepare query for selecting orders for whith we need send notify for this rule
     * @param StoreOrderAttempQuery $query
     */
    public function prepareQuery(StoreOrderAttempQuery $query);

    /**
     * Filter selected orders (using prepareQuery)
     * Not all checks we can do in SQL query. This method allow make additional filtering orders for this rules
     * @param StoreOrder         $order
     * @param OrderNotifyAdapter $notifyAdapter
     * @return bool true if need send notify, else - false
     */
    public function filter(StoreOrder $order, OrderNotifyAdapter $notifyAdapter);

    /**
     * @param StoreOrderAttemp $attemp
     * @return User
     */
    public function toUser(StoreOrderAttemp $attemp);

    /**
     * Return template for notify
     * @return string
     */
    public function getTemplate();

    /**
     * Resolve notify params for template
     * @param StoreOrderAttemp $attemp
     * @return array
     */
    public function resolveParams(StoreOrderAttemp $attemp);
}