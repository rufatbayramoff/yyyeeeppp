<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\builder;


use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\order\history\OrderHistoryService;
use common\models\StoreOrder;
use common\models\StoreOrderItem;

/**
 * Class OrderItemAdder
 * @package common\components\order\builder
 */
class OrderItemAdder
{
    /**
     * @var OrderHistoryService
     */
    protected $history;

    /**
     * @var StoreOrder
     */
    private $order;

    /**
     * OrderItemAdder constructor.
     * @param StoreOrder $order
     */
    public function __construct(StoreOrder $order)
    {
        $this->order = $order;
    }

    /**
     * @param OrderHistoryService $history
     */
    public function injectDependencies(OrderHistoryService $history)
    {
        $this->history = $history;
    }

    /**
     * @param StoreOrderItem $item
     * @return StoreOrderItem
     */
    public function add(StoreOrderItem $item) : StoreOrderItem
    {
        $item->order_id = $this->order->id;
        $item->created_at = DateHelper::now();
        $this->history->logItemAdded($this->order, $item);

        AssertHelper::assertSave($item);
        return $item;
    }

}