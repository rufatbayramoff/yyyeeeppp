<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\builder;


use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\models\User;
use common\models\UserLoginLog;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\Security;

/**
 * Class UserResolver
 * @package common\components\order\builder\items
 */
class UserResolver
{
    /**
     * @var Emailer
     */
    protected $emailer;

    /**
     * @var Security
     */
    protected $security;

    /**
     * @param Emailer $emailer
     */
    public function injectDependencies(Emailer $emailer)
    {
        $this->emailer = $emailer;
        $this->security = \Yii::$app->security;
    }

    /**
     * Resolve order user.
     * If it real user - return him
     * If it anonim user (for example, when make order without registration) - will be created new user
     *
     * @param User $user
     * @param string $email
     * @return User
     */
    public function resolve(User $user, string $email = null): User
    {
        // if normal logined user
        if (!$user->isAnonimUser()) {
            return $user;
        }

        AssertHelper::assert($email, 'Email is empty');

        if ($userExist = User::findOne(['email' => $email])) {
            return $userExist;
        }

        // if new user
        $password = $this->security->generateRandomString(8);
        $user = UserFacade::createUser([
            'email'    => $email,
            'password' => $password,
        ]);

        UserFacade::logLogin([
            'user_id'    => $user->id,
            'result'     => 'ok',
            'login_type' => UserLoginLog::LOGIN_TYPE_AUTO_CREATE
        ]);

        $this->emailer->sendSignupEmail($user, $password);
        $userSession = Yii::$app->user;
        AssertHelper::assert($userSession->login($user));

        return $user;
    }
}