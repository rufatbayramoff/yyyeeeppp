<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\builder\items;


use common\components\order\builder\OrderItemAdder;
use common\models\StoreOrder;

/**
 * Interface OrderItemsFactory
 * @package common\components\order\builder
 */
interface OrderItemsFactory
{
    /**
     * Create order items
     * @param StoreOrder $order
     * @param OrderItemAdder $adder
     */
    public function createItems(StoreOrder $order, OrderItemAdder $adder) : void;
}