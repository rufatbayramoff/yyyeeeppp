<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\builder\items;


use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\order\builder\OrderItemAdder;
use common\components\order\TestOrderFactory;
use common\interfaces\Model3dBasePartInterface;
use common\models\Cart;
use common\models\Model3dReplica;
use common\models\Model3dReplicaPart;
use common\models\StoreOrder;
use common\models\StoreOrderItem;
use common\models\StoreUnit;
use common\models\User;
use common\services\Model3dService;
use common\services\StoreOrderService;
use Yii;
use yii\base\UserException;

/**
 * Class CartOrderItemsBuilder
 * @package common\components\order\builder
 */
class CartOrderItemsFactory implements OrderItemsFactory
{
    /**
     * @var StoreOrderService
     */
    protected $orderService;

    /**
     * @var Cart
     */
    private $cart;

    /**
     * CartOrderItemsBuilder constructor.
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @param StoreOrderService $orderService
     */
    public function injectDependencies(StoreOrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Create order items
     *
     * @param StoreOrder $order
     * @param OrderItemAdder $adder
     * @throws UserException
     */
    public function createItems(StoreOrder $order, OrderItemAdder $adder): void
    {
        $customer   = $order->user;
        $cartItems  = $this->cart->cartItems;
        $orderItems = [];

        if (count($cartItems) === 0) {
            throw new BusinessException('No items found to add');
        }

        foreach ($cartItems as $cartItem) {

            $model3dReplica = $cartItem->model3dReplica;
            $storeUnit      = $model3dReplica->storeUnit;

            $this->clearEmptyParts($model3dReplica);
            $model3dReplica->user_id = $customer->id;
            AssertHelper::assertSave($model3dReplica);

            $item                     = new StoreOrderItem();
            $item->unit_id            = $storeUnit->id;
            $item->model3d_replica_id = $model3dReplica->id;
            $item->qty                = $cartItem->qty;

            $adder->add($item);

            $orderItems[] = $item;
        }

        if (!$orderItems) {
            throw new BusinessException('No items added');
        }

        $this->createOrderAttempt($order);
    }

    /**
     * @param StoreOrder $order
     */
    private function createOrderAttempt(StoreOrder $order): void
    {
        $machine     = $this->cart->getMachine();
        $orderAttemp = $this->orderService->createAttempForMachine($order, $machine, false);
        $order->link('currentAttemp', $orderAttemp);
    }

    /**
     * Mark parts with zero qty as inactive
     *
     * @param Model3dReplica $replica
     */
    private function clearEmptyParts(Model3dReplica $replica): void
    {
        /** @var Model3dBasePartInterface|Model3dReplicaPart $parts */
        $parts = $replica->model3dParts;
        foreach ($parts as $part) {
            if (!$part->qty) {
                $part->user_status = Model3dBasePartInterface::STATUS_INACTIVE;
                AssertHelper::assertSave($part);
            }
        }
    }

    /**
     * Validate store unit before adding to order
     *
     * @param StoreUnit $storeUnit
     * @param User $user
     * @return bool
     * @throws \HttpException
     * @throws \yii\base\InvalidConfigException
     */
    protected function validateStoreUnit(StoreUnit $storeUnit, User $user): bool
    {
        /** @var Model3dService $model3dService */
        $model3dService = Yii::createObject(['class' => Model3dService::class, 'user' => $user]);

        $allowOrder = $model3dService->isAvailableForCurrentUser($storeUnit->model3d);

        if (!$allowOrder) {
            $msg = 'Store unit is not published, cannot be added to order. ' . $storeUnit->id;
            \Yii::warning($msg, 'store');
            return false;
        }
        return true;
    }
}