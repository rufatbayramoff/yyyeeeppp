<?php

namespace common\components\order\builder\items;


use common\components\exceptions\AssertHelper;
use common\components\order\builder\OrderItemAdder;
use common\components\order\TestOrderFactory;
use common\interfaces\Model3dBasePartInterface;
use common\models\Cart;
use common\models\CuttingMachine;
use common\models\CuttingPack;
use common\models\Model3dReplica;
use common\models\Model3dReplicaPart;
use common\models\StoreOrder;
use common\models\StoreOrderItem;
use common\models\StoreUnit;
use common\models\User;
use common\services\Model3dService;
use common\services\StoreOrderService;
use Yii;
use yii\base\UserException;

/**
 * Class CartOrderItemsBuilder
 * @package common\components\order\builder
 */
class CuttingPackItemsFactory implements OrderItemsFactory
{
    /**
     * @var StoreOrderService
     */
    protected $orderService;

    /**
     * @var CuttingPack
     */
    protected $cuttingPack;

    /**
     * @var CuttingMachine
     */
    protected $cuttingMachine;

    /**
     * CuttingPackItemsFactory constructor.
     */
    public function __construct($item)
    {
        $this->cuttingPack = $item->cuttingPack;
        $this->cuttingMachine = $item->cuttingMachine;
    }

    /**
     * @param StoreOrderService $orderService
     */
    public function injectDependencies(StoreOrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Create order items
     *
     * @param StoreOrder $order
     * @param OrderItemAdder $adder
     * @throws UserException
     */
    public function createItems(StoreOrder $order, OrderItemAdder $adder): void
    {
        $customer = $order->user;

        $item                     = new StoreOrderItem();
        $item->cutting_pack_uuid  = $this->cuttingPack->uuid;
        $item->qty                = 1;
        $adder->add($item);
        $orderItems[] = $item;

        $machine = null;
        $orderAttemp = $this->orderService->createAttempForMachine($order, $this->cuttingMachine->companyService, false);
        $order->link('currentAttemp', $orderAttemp);
    }
}