<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\builder;


use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\InvalidModelException;
use common\components\order\builder\items\CartOrderItemsFactory;
use common\components\order\builder\items\CuttingPackItemsFactory;
use common\components\order\builder\items\OrderItemsFactory;
use common\components\TextHelper;
use common\interfaces\DiscountsResolverInterface;
use common\models\AffiliateSource;
use common\models\Cart;
use common\models\CuttingMachine;
use common\models\CuttingPack;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceAnalytic;
use common\models\Preorder;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\models\UserAddress;
use common\models\UserSession;
use common\modules\cutting\models\CuttingItem;
use common\modules\payment\factories\PaymentInvoiceFactory;
use common\modules\payment\factories\PaymentInvoiceListFactory;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\modules\payment\services\PaymentInvoiceService;
use common\modules\product\models\ProductCart;
use common\modules\product\models\ProductCartItemsFactory;
use common\modules\promocode\components\PromocodeDiscountService;
use common\services\StoreOrderService;
use frontend\models\user\UserFacade;
use lib\delivery\delivery\models\DeliveryRate;
use lib\money\Money;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Security;
use yii\base\UserException;
use yii\web\NotFoundHttpException;

/**
 * Class OrderBuilder
 *
 * @package common\components\order\builder
 */
class OrderBuilder
{
    /**
     * @var UserSession
     */
    protected $userSession;

    /**
     * @var Security
     */
    protected $security;

    /**
     * @var UserResolver
     */
    protected $userResolver;

    /**
     * @var OrderDeliveryFactory
     */
    protected $deliveryFactory;

    /**
     * @var PromocodeDiscountService
     */
    protected $discountService;

    /**
     * @var PaymentInvoiceFactory
     */
    protected $paymentInvoiceFactory;

    /**
     * @var PaymentInvoiceRepository
     */
    protected $paymentInvoiceRepository;

    /**
     * @var PaymentInvoiceListFactory
     */
    protected $paymentInvoiceListFactory;

    /**
     * @var PaymentInvoiceService
     */
    protected $paymentInvoiceService;

    /**
     * @var StoreOrderService
     */
    protected $orderService;

    /**
     * @var User
     */
    protected $customer;

    /**
     * @var string
     */
    protected $source;

    /**
     * @var int
     */
    protected $manualPsId;

    /**
     * Customer email
     *
     * @var string|null
     */
    protected $customerEmail;

    /**
     * @var UserAddress
     */
    protected $billAddress;

    /**
     * @var Money
     */
    protected $amount;

    /**
     * @var bool
     */
    protected $canChangeSupplier = false;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var DeliveryRate
     */
    protected $deliveryRate;

    /**
     * @var CuttingMachine
     */
    protected $cuttingMachine;

    /**
     * @var CuttingPack
     */
    protected $cuttingPack;

    /**
     * List of objects who transfor to order items.
     * It can be Cart, Preorder e.t.c
     *
     * @var mixed[]
     */
    protected $itemsObjects = [];

    /**
     * @var AffiliateSource|null
     */
    protected $affiliateSource = null;

    protected $sortMode;

    /**
     * @var array
     */
    private $itemsFactoryConfig = [
        Cart::class        => CartOrderItemsFactory::class,
        ProductCart::class => ProductCartItemsFactory::class,
        CuttingItem::class => CuttingPackItemsFactory::class,
    ];


    /**
     * @return OrderBuilder
     * @throws InvalidConfigException
     */
    public static function create(): OrderBuilder
    {
        return Yii::createObject(static::class);
    }

    /**
     * @param UserSession $userSession
     * @param UserResolver $userResolver
     * @param OrderDeliveryFactory $deliveryFactory
     * @param PromocodeDiscountService $discountService
     * @param PaymentInvoiceFactory $paymentInvoiceFactory
     * @param PaymentInvoiceRepository $paymentInvoiceRepository
     * @param StoreOrderService $orderService
     * @param PaymentInvoiceListFactory $paymentInvoiceListFactory
     * @param PaymentInvoiceService $paymentInvoiceService
     */
    public function injectDependencies(
        UserSession $userSession,
        UserResolver $userResolver,
        OrderDeliveryFactory $deliveryFactory,
        PromocodeDiscountService $discountService,
        PaymentInvoiceFactory $paymentInvoiceFactory,
        PaymentInvoiceRepository $paymentInvoiceRepository,
        StoreOrderService $orderService,
        PaymentInvoiceListFactory $paymentInvoiceListFactory,
        PaymentInvoiceService $paymentInvoiceService
    ): void
    {
        $this->security                  = Yii::$app->security;
        $this->userSession               = UserFacade::getUserSession();
        $this->userSession               = $this->userSession ?: ($userSession ?: UserFacade::getUserSession());
        $this->userResolver              = $userResolver;
        $this->deliveryFactory           = $deliveryFactory;
        $this->discountService           = $discountService;
        $this->paymentInvoiceFactory     = $paymentInvoiceFactory;
        $this->paymentInvoiceRepository  = $paymentInvoiceRepository;
        $this->orderService              = $orderService;
        $this->paymentInvoiceListFactory = $paymentInvoiceListFactory;
        $this->paymentInvoiceService     = $paymentInvoiceService;
    }

    /**
     * @param User $customer
     * @param string|null $email
     * @return OrderBuilder
     */
    public function customer(User $customer, string $email = null): OrderBuilder
    {
        $this->customer      = $customer;
        $this->customerEmail = $email;
        return $this;
    }

    public function source($source): OrderBuilder
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @param UserAddress $billAddress
     * @return OrderBuilder
     */
    public function billAddress(UserAddress $billAddress): OrderBuilder
    {
        $this->billAddress = $billAddress;
        return $this;
    }

    /**
     * @param Money $amount
     * @return OrderBuilder
     */
    public function amount(Money $amount): OrderBuilder
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @param string $comment
     * @return OrderBuilder
     */
    public function comment(string $comment = null): OrderBuilder
    {
        $this->comment = TextHelper::filterUtf8($comment);
        return $this;
    }

    /**
     * @param bool $canChangeSupplier
     * @return OrderBuilder
     */
    public function canChangeSupplier(bool $canChangeSupplier): OrderBuilder
    {
        $this->canChangeSupplier = $canChangeSupplier;
        return $this;
    }

    /**
     * @param DeliveryRate $rate
     * @return OrderBuilder
     */
    public function delivery(DeliveryRate $rate): OrderBuilder
    {
        $this->deliveryRate = $rate;
        return $this;
    }

    public function affiliateSource(?AffiliateSource $affiliateSource)
    {
        $this->affiliateSource = $affiliateSource;
        return $this;
    }

    public function sortMode(string $sortMode)
    {
        $this->sortMode = $sortMode;
        return $this;
    }

    /**
     * @param ProductCart $productCart
     * @return OrderBuilder
     */
    public function productCart(ProductCart $productCart): OrderBuilder
    {
        $this->itemsObjects[] = $productCart;
        return $this;
    }

    public function cuttingMachine(CuttingMachine $cuttingMachine)
    {
        if (!array_key_exists('item', $this->itemsObjects)) {
            $this->itemsObjects['item'] = new CuttingItem();
        }
        $this->itemsObjects['item']->cuttingMachine = $cuttingMachine;
        return $this;
    }

    public function cuttingPack(CuttingPack $cuttingPack)
    {
        if (!array_key_exists('item', $this->itemsObjects)) {
            $this->itemsObjects['item'] = new CuttingItem();
        }
        $this->itemsObjects['item']->cuttingPack = $cuttingPack;
        return $this;
    }

    /**
     * @param Cart $cart
     * @return OrderBuilder
     */
    public function cart(Cart $cart): OrderBuilder
    {
        $this->itemsObjects[] = $cart;
        return $this;
    }

    public function manualPs($psId): OrderBuilder
    {
        $this->manualPsId = $psId;
        return $this;
    }

    public function orderType($orderType): OrderBuilder
    {
        $this->orderType = $orderType;
        return $this;
    }

    /**
     * @param DiscountsResolverInterface $discountsResolver
     * @return OrderBuilder
     */
    public function setDiscountResolver(DiscountsResolverInterface $discountsResolver): OrderBuilder
    {
        $this->discountService->setDiscountResolver($discountsResolver);
        return $this;
    }

    /**
     * Check required params,
     * if they not setted - throws exception
     */
    private function checkRequiredParams(): void
    {
        AssertHelper::assert($this->userSession, 'Need specify user session', InvalidConfigException::class);
        AssertHelper::assert($this->customer, 'Need specify customer', InvalidConfigException::class);
        AssertHelper::assert($this->itemsObjects, 'Need specify items object', InvalidConfigException::class);
    }

    /**
     * @return StoreOrder
     * @throws \Exception
     */
    public function buildByItems(): StoreOrder
    {
        $this->checkRequiredParams();

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $this->customer = $this->userResolver->resolve($this->customer, $this->customerEmail);

            $order = $this->createOrder();
            $this->createDelivery($order);
            $this->createItems($order);

            $invoices = $this->paymentInvoiceListFactory->createWithPaymentMethodsByOrder($order, $this->affiliateSource);
            $this->paymentInvoiceRepository->saveInvoicesArray($invoices);
            $invoice = reset($invoices);
            $this->createPaymentInvoiceAnalytic($invoice);
            $order->primary_payment_invoice_uuid = $invoice->uuid;
            $order->safeSave();

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $order;
    }

    /**
     * @param Preorder $preorder
     *
     * @return StoreOrder
     * @throws \yii\db\Exception
     */
    public function buildByPreorder(Preorder $preorder): StoreOrder
    {
        $this->customer($preorder->user);
        $this->canChangeSupplier(false);

        $attempt = $preorder->storeOrderAttempt;

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $order                               = $preorder->primaryPaymentInvoice->storeOrder ?: $this->createOrder();
            $order->preorder_id                  = $preorder->id;
            $order->primary_payment_invoice_uuid = $preorder->primaryPaymentInvoice->uuid;
            $order->ship_address_id              = $preorder->ship_to_id;
            $order->current_attemp_id            = $attempt->id;

            // link order id for invoice
            PaymentInvoice::updateAll(['store_order_id' => $order->id], ['preorder_id' => $preorder->id]);

            AssertHelper::assertSave($order);
            AssertHelper::assertSave($preorder->primaryPaymentInvoice);

            /** @var StoreOrderAttemp $attempt */
            $attempt->order_id = $order->id;
            $attempt->status   = StoreOrderAttemp::STATUS_NEW;
            $attempt->safeSave();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $order;
    }

    /**
     * Create order
     *
     * @return StoreOrder
     * @throws InvalidModelException
     * @throws Exception
     */
    protected function createOrder(): StoreOrder
    {
        $order = new StoreOrder();

        $order->created_at          = DateHelper::now();
        $order->updated_at          = $order->created_at;
        $order->user_id             = $this->customer->id;
        $order->user_session_id     = $this->userSession->id;
        $order->bill_address_id     = $this->billAddress->id ?? null;
        $order->can_change_ps       = $this->canChangeSupplier ? 1 : 0;
        $order->order_state         = StoreOrder::STATE_PROCESSING;
        $order->confirm_hash        = $this->customer->isAnonimUser() ? $this->security->generateRandomString() : null;
        $order->comment             = $this->comment;
        $order->source              = $this->source;
        $order->deliveryRate        = $this->deliveryRate;
        $order->manual_setted_ps_id = $this->manualPsId ? $this->manualPsId : null;

        AssertHelper::assertSave($order);
        $order->refresh();
        return $order;
    }

    /**
     * @param StoreOrder $order
     *
     * @throws UserException
     * @throws NotFoundHttpException
     */
    protected function createDelivery(StoreOrder $order): void
    {
        if (!$this->deliveryRate || !$this->deliveryRate->deliveryParams) {
            return;
        }
        $this->deliveryFactory->createDelivery($order, $this->deliveryRate->deliveryParams, $this->deliveryRate->deliveryForm, $this->deliveryRate);
    }

    /**
     * Create store order items
     *
     * @param StoreOrder $order
     * @throws InvalidConfigException
     */
    protected function createItems(StoreOrder $order): void
    {
        /** @var OrderItemAdder $itemAdder */
        $itemAdder = Yii::createObject(OrderItemAdder::class, [$order]);
        foreach ($this->itemsObjects as $itemsObject) {
            $this->resolveItemFactory($itemsObject)->createItems($order, $itemAdder);
        }
        $order->refresh();
    }

    /**
     * @param Cart $itemsObject
     * @return OrderItemsFactory
     * @throws InvalidConfigException
     */
    private function resolveItemFactory($itemsObject): OrderItemsFactory
    {
        $itemFactory = $this->itemsFactoryConfig[get_class($itemsObject)];
        if (is_string($itemFactory)) {
            $itemFactory = Yii::createObject($itemFactory, [$itemsObject]);
        }
        return $itemFactory;
    }

    /**
     * @param mixed $invoice
     * @throws BusinessException
     */
    protected function createPaymentInvoiceAnalytic(mixed $invoice): void
    {
        if($this->sortMode) {
            $paymentInvoiceAnalytic = PaymentInvoiceAnalytic::create(
                $this->sortMode,
                $invoice->uuid
            );
            $paymentInvoiceAnalytic->safeSave();
        }
    }
}
