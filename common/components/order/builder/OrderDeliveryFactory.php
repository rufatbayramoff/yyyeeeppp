<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\builder;


use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\interfaces\machine\DeliveryParamsInterface;
use common\models\DeliveryType;
use common\models\PsMachineDelivery;
use common\models\repositories\PsMachineDeliveryCountryRepository;
use common\models\StoreOrder;
use common\models\StoreOrderDelivery;
use common\models\User;
use common\models\UserAddress;
use frontend\models\delivery\DeliveryForm;
use lib\delivery\delivery\models\DeliveryRate;
use yii\base\UserException;
use yii\helpers\Html;

/**
 * Class OrderDeliveryFactory
 *
 * @package common\components\order\builder
 */
class OrderDeliveryFactory
{
    /**
     * @var PsMachineDeliveryCountryRepository
     */
    protected $psMachineDeliveryCountryRepository;

    /**
     * @param PsMachineDeliveryCountryRepository $psMachineDeliveryCountryRepository
     */
    public function injectDependencies(PsMachineDeliveryCountryRepository $psMachineDeliveryCountryRepository)
    {
        $this->psMachineDeliveryCountryRepository = $psMachineDeliveryCountryRepository;
    }

    /**
     * Create delivery
     *
     * @param StoreOrder $order
     * @param DeliveryParamsInterface $deliveryParams
     * @param DeliveryForm $deliveryForm
     * @param DeliveryRate $deliveryRate
     * @throws \yii\web\NotFoundHttpException
     * @throws UserException
     */
    public function createDelivery(
        StoreOrder $order,
        DeliveryParamsInterface $deliveryParams,
        DeliveryForm $deliveryForm,
        DeliveryRate $deliveryRate
    ): void
    {
        $deliveryType = DeliveryType::tryFind(['code' => $deliveryForm->deliveryType], _t('site.error', 'Delivery type not found'));
        $shipAddress  = $this->getDeliveryShipAddress($order->user, $deliveryForm, $deliveryParams);

        $order->delivery_type_id = $deliveryType->id;
        $order->ship_address_id  = $shipAddress->id;

        if (!$order->bill_address_id) {
            $order->bill_address_id = $shipAddress->id;
        }

        AssertHelper::assertSave($order);

        $delivery                      = new StoreOrderDelivery();
        $delivery->order_id            = $order->id;
        $delivery->delivery_type_id    = $deliveryType->id;
        $delivery->is_express          = (int)$this->isExpress($deliveryForm, $deliveryParams);
        $delivery->ps_delivery_details = $this->getDeliveryDetails($deliveryParams, $deliveryType, $deliveryForm);
        AssertHelper::assertSave($delivery);

        $order->refresh();
    }

    /**
     * @param $deliveryForm
     * @param $deliveryParams
     * @return bool
     */
    protected function isExpress(DeliveryForm $deliveryForm, $deliveryParams): bool
    {
        return $deliveryForm->canExpress() && $deliveryParams->getCurrentLocation()->country->isUsa();
    }

    /**
     * @param DeliveryParamsInterface $params
     * @param DeliveryType $deliveryType
     * @param DeliveryForm $deliveryForm
     * @return array
     */
    private function getDeliveryDetails(DeliveryParamsInterface $params, DeliveryType $deliveryType, DeliveryForm $deliveryForm)
    {
        $psDelivery = $params->getPsDeliveryTypeByCode($deliveryType->code);
        $details = $psDelivery ? $psDelivery->toArray(PsMachineDelivery::DELIVERY_DETAILS) : null;
        if (!$details) {
            return null;
        }
        if ($psDelivery->canDeliveryToCountry() && $deliveryForm->country) {
            $deliveryCountry = $this->psMachineDeliveryCountryRepository->findDeliveryByIso($deliveryForm->country, $psDelivery->id);
            if ($deliveryCountry && $deliveryCountry->price) {
                $details['country_title']     = $deliveryCountry->geoCountry->title;
                $details['country_price']     = $deliveryCountry->price;
            }
        }
        return $details;
    }

    /**
     * detect shipping address,
     * get shipping address based on delivery form
     * or based on pickup delivery form data
     *
     * @param User $user
     * @param DeliveryForm $deliveryForm
     * @param DeliveryParamsInterface $deliveryParams
     * @return UserAddress|null
     * @throws UserException
     */
    private function getDeliveryShipAddress(User $user, DeliveryForm $deliveryForm, DeliveryParamsInterface $deliveryParams): UserAddress
    {
        if ($deliveryForm->deliveryType != DeliveryType::PICKUP) {
            $toAddress            = $deliveryForm->getUserAddress();
            $toAddress['user_id'] = $user->id;
            $toAddress->safeSave();
            return $toAddress;
        }

        if ($deliveryForm->validate()) {

            $location = $deliveryParams->getCurrentLocation();

            $data        = [
                'first_name'        => $deliveryForm->first_name,
                'last_name'         => $deliveryForm->last_name,
                'email'             => $deliveryForm->email,
                'phone'             => $deliveryForm->phone,
                'address'           => $location->address ?: ' ',
                'extended_address'  => $location->address2 ?: '',
                'city'              => $location->city->title ?: ' ',
                'region'            => $location->region->title ?? ' ',
                'zip_code'          => $location->zip_code,
                'is_forced_invalid' => (int)$deliveryForm->forceInvalidAddress,
                'country_id'        => $location->country_id,
                'user_id'           => $user->id,
                'type'              => DeliveryType::PICKUP,
                'lat'               => $location->lat,
                'lon'               => $location->lon
            ];
            $sameAddress = UserAddress::findOne($data);
            if ($sameAddress) {
                $shipAddress = $sameAddress;
            } else {
                $shipAddress        = new UserAddress();
                $data['created_at'] = dbexpr('NOW()');
                $shipAddress->setAttributes($data);
                $shipAddress->safeSave();
            }
        } else {
            throw new BusinessException(Html::errorSummary($deliveryForm));
        }
        return $shipAddress;
    }
}