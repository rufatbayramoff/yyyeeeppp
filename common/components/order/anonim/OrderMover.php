<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\anonim;


use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\base\StoreOrder;
use common\models\User;
use yii\db\ActiveRecordInterface;

class OrderMover
{
    /**
     * @param StoreOrder $order
     * @param User $fromUser
     * @param User $toUser
     * @throws \Exception
     */
    public function move(StoreOrder $order, User $fromUser, User $toUser)
    {
        $fromUserId = $fromUser->id;
        $toUserId = $toUser->id;

        AssertHelper::assert($order->user_id == $fromUserId);

        $transaction = \Yii::$app->db->beginTransaction();
        try{

            $order->user_id = $toUserId;
            AssertHelper::assertSave($order);

            foreach ($this->getMovedRelations() as $relation) {
                $this->fixRelation($order, $relation, $fromUserId, $toUserId);
            }

            // move delivery addresses
            $order->shipAddress->user_id = $toUser->id;
            AssertHelper::assertSave($order->shipAddress);

            $transaction->commit();
        }
        catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        $order->refresh();
    }

    /**
     * @param ActiveRecordInterface $order
     * @param string $relation
     * @param int $fromUserId
     * @param int $toUserId
     */
    private function fixRelation(ActiveRecordInterface $order, string $relation, int $fromUserId, int $toUserId)
    {
        if($relatedObjects = ArrayHelper::getValue($order, $relation)) {
            $relatedObjects = is_array($relatedObjects)  ? $relatedObjects : [$relatedObjects];
            foreach ($relatedObjects as $relatedObject) {

                if ($relatedObject->user_id == $fromUserId){
                    $relatedObject->user_id = $toUserId;
                    AssertHelper::assertSave($relatedObject);
                }
            }
        }
    }

    /**
     * @return array
     */
    private function getMovedRelations()
    {
        $payment = [
            'paymentInvoices',
            'paymentTransactions',
            'paymentTransactions.paymentTransactionHistories',
            'paymentReceipts',
            'paymentDetails.paymentDetailTaxRates',
        ];
        $models = [
            'storeOrderItems.model3dReplica.originalModel3d.model3dParts',
            'storeOrderItems.model3dReplica.originalModel3d.model3dParts.file',
            'storeOrderItems.model3dReplica.originalModel3d.model3dImgs',
            'storeOrderItems.model3dReplica.originalModel3d.model3dImgs.file',
            'storeOrderItems.model3dReplica.originalModel3d.model3dHistories',
            'storeOrderItems.model3dReplica',
            'storeOrderItems.model3dReplica.model3dReplicaImgs'
        ];

        $addresses = [
            'shipAddress',
            'billAddress',
        ];

        return array_merge($payment, $models, $addresses);
    }
}