<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\anonim;


use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\models\StoreOrder;
use common\models\User;
use frontend\models\user\UserFacade;
use yii\base\UserException;
use yii\helpers\Url;

class AnonimOrderHelper
{
    /**
     * @param StoreOrder $order
     * @throws UserException
     */
    public static function checkOrderAccess(StoreOrder $order)
    {
        if (!UserFacade::isObjectOwner($order)) {
            throw new BusinessException(_t('front.store', 'Order {order} not found', ['order' => $order->id]));
        }
    }

    /**
     * @todo Move to trait
     * @return User
     */
    public static function resolveOrderUser()
    {
        return UserFacade::getCurrentUser() ?: UserFacade::getAnonimUser();
    }


    /**
     * @param StoreOrder $storeOrder
     * @return string
     */
    public static function createConfirmLink(StoreOrder $storeOrder)
    {
        AssertHelper::assert($storeOrder->isAnonim());
        return Url::toRoute(['/workbench/order/confirm-anonim-order', 'confirmHash' => $storeOrder->confirm_hash], true);
    }
}