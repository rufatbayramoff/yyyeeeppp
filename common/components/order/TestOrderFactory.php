<?php
/**
 * Created by mitaichik
 */

namespace common\components\order;


use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\components\order\builder\OrderBuilder;
use common\interfaces\machine\DeliveryParamsInterface;
use common\models\Cart;
use common\models\DeliveryType;
use common\models\factories\Model3dReplicaFactory;
use common\models\Model3dReplica;
use common\models\PaymentTransaction;
use common\models\PsPrinter;
use common\models\repositories\Model3dReplicaRepository;
use common\models\StoreOrder;
use common\models\StoreUnit;
use common\models\User;
use common\modules\payment\components\PaymentCheckout;
use frontend\components\cart\CartFactory;
use frontend\models\delivery\DeliveryForm;
use lib\delivery\delivery\DeliveryFacade;
use yii\base\Exception;

/**
 * Class TestOrderFactory
 * @package common\components\order
 */
class TestOrderFactory
{
    /**
     * @var CartFactory
     */
    private $cartFactory;

    /**
     * @var Model3dReplicaFactory
     */
    private $replicaFactory;

    /**
     * @var Model3dReplicaRepository
     */
    private $replicaRepository;

    /**
     * Return test order store unit id
     *
     * @return int
     * @throws Exception
     */
    public static function getTestOrderStoreUnitId(): int
    {
        return (int)app('setting')->get('printer.testorder.storeunit', -1);
    }

    /**
     * Return test order user
     *
     * @return int
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public static function getTestOrderUserId(): int
    {
        return (int)app('setting')->get('printer.testorder.userid', -1);
    }

    /**
     * @param CartFactory $cartFactory
     * @param Model3dReplicaFactory $replicaFactory
     * @param Model3dReplicaRepository $replicaRepository
     * @internal param OrderFactory $orderFactory
     */
    public function injectDependencies(CartFactory $cartFactory, Model3dReplicaFactory $replicaFactory, Model3dReplicaRepository $replicaRepository) : void
    {
        $this->cartFactory = $cartFactory;
        $this->replicaFactory = $replicaFactory;
        $this->replicaRepository = $replicaRepository;
    }

    /**
     * @param PsPrinter $printer
     *
     * @return StoreOrder
     * @throws Exception
     * @throws \ErrorException
     * @throws \frontend\components\cart\exceptions\CartEmptyException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function create(PsPrinter $printer) : StoreOrder
    {
        $customer = User::tryFindByPk(self::getTestOrderUserId());
        $replica = $this->createReplica($printer);
        $cart = $this->createCart($replica, $printer);
        $deliveryFrom = $this->createDeliveryForm($printer->companyService->asDeliveryParams(), $cart, $customer);

        $rate = DeliveryFacade::getPickupRate();
        $rate->deliveryForm = $deliveryFrom;
        $rate->deliveryParams = $printer->companyService->asDeliveryParams();

        BaseActiveQuery::$staticCacheGlobalEnable = false;

        $order = OrderBuilder::create()
            ->customer($customer)
            ->canChangeSupplier(false)
            ->cart($cart)
            ->delivery($rate)
            ->buildByItems();

        $this->payOrder($order);

        return $order;
    }

    /**
     * @param PsPrinter $printer
     *
     * @return Model3dReplica
     * @throws Exception
     * @throws \ErrorException
     * @throws \yii\web\NotFoundHttpException
     */
    private function createReplica(PsPrinter $printer) : Model3dReplica
    {
        $testStoreUnit = StoreUnit::tryFindByPk(self::getTestOrderStoreUnitId());

        $model3dReplica = Model3dReplicaFactory::createModel3dReplica($testStoreUnit->model3d);
        $model3dReplica->setStoreUnit($testStoreUnit);
        $model3dReplica->setKitTexture($printer->getMostCheapestTexture());
        Model3dReplicaRepository::save($model3dReplica);

        return $model3dReplica;
    }

    /**
     * @param Model3dReplica $replica
     * @param PsPrinter $printer
     *
     * @return Cart
     * @throws \Exception
     */
    private function createCart(Model3dReplica $replica, PsPrinter $printer) : Cart
    {
        $cart = CartFactory::createCart();

        $cart->addPosition(CartFactory::createCartItem($replica, $printer->companyService, 1));
        $cart->setCanChangePs(false);

        return $cart;
    }

    /**
     * @param DeliveryParamsInterface $params
     * @param Cart $cart
     * @param User $customer
     *
     * @return DeliveryForm
     * @throws \frontend\components\cart\exceptions\CartEmptyException
     */
    private function createDeliveryForm(DeliveryParamsInterface $params, Cart $cart, User $customer) : DeliveryForm
    {
        $deliveryType = $params->hasDeliveryPickup()
            ? DeliveryType::PICKUP
            : ($params->hasDomesticDelivery() ? DeliveryType::STANDARD : DeliveryType::INTERNATIONAL);

        $formScenario = $deliveryType == DeliveryType::PICKUP
            ? DeliveryForm::SCENARIO_PICKUP
            : DeliveryForm::SCENARIO_DELIVERY;

        $form = new DeliveryForm($params, $cart->getModel3d());
        $form->setScenario($formScenario);
        $form->userId = $customer->id;
        $form->deliveryType = $deliveryType;
        $form->first_name = 'Treatstock';
        $form->last_name = 'company';
        $form->phone = "+13025258060";
        $form->email = "support@treatstock.com";

        if ($deliveryType != DeliveryType::PICKUP) {
            $form->loadUserAddress(ArrayHelper::first($customer->userAddresses));
        }

        return $form;
    }

    /**
     * @param StoreOrder $order
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    private function payOrder(StoreOrder $order) : void
    {
        $paymentFacade = new PaymentCheckout(PaymentTransaction::VENDOR_TS);
        $paymentFacade->pay($order->getPrimaryInvoice(), '');
    }
}