<?php
/**
 * Created by mitaichik
 */

namespace common\components\order;

use common\components\order\predicates\NeedCertificateForDownloadModelPredicate;
use common\components\orderOffer\OrderOffer;
use common\components\orderOffer\OrderOfferLocatorService;
use common\components\PaymentExchangeRateConverter;
use common\components\PaymentExchangeRateFacade;
use common\components\ps\locator\materials\MaterialColorItem;
use common\interfaces\Model3dBaseInterface;
use common\models\Cart;
use common\models\CartItem;
use common\models\CatalogCost;
use common\models\DeliveryType;
use common\models\factories\Model3dTextureFactory;
use common\models\GeoLocation;
use common\models\model3d\Model3dCostInfo;
use common\models\Model3dTexture;
use common\models\orders\events\DeliveryInfo;
use common\models\Printer;
use common\models\PrinterColor;
use common\models\PrinterMaterialGroup;
use common\models\PsPrinter;
use common\models\repositories\CatalogCostRepository;
use common\models\repositories\PrinterColorRepository;
use common\models\StoreOrder;
use common\models\StoreUnit;
use common\modules\printersList\models\RequestInfo;
use common\services\CatalogCostService;
use common\services\PrinterMaterialService;
use frontend\models\serialize\Model3dTextureSerializer;
use frontend\models\user\UserFacade;
use lib\delivery\delivery\DeliveryService;
use lib\delivery\parcel\Parcel;
use lib\delivery\parcel\ParcelFactory;
use lib\geo\models\Location;
use lib\money\Currency;
use lib\money\Money;
use common\modules\payment\fee\FeeHelper;
use common\modules\payment\fee\FeeHelperInterface;
use lib\money\MoneyMath;
use Yii;

class PriceCalculator
{
    /**
     * @param Model3dBaseInterface $model3d
     * @param GeoLocation $currentLocation
     * @param PrinterMaterialGroup $printerMaterialGroup
     * @param PrinterColor $printerColor
     * @return Model3dCostInfo[]
     * @throws \yii\base\InvalidConfigException
     */
    public static function calculateModel3dPrintingCosts(Model3dBaseInterface $model3d, GeoLocation $currentLocation, $printerMaterialGroup = null, $printerColor = null)
    {
        $deliveryService = \Yii::createObject(DeliveryService::class);
        $costColors      = [];

        $predicateOnlyCertificated = new NeedCertificateForDownloadModelPredicate();
        $onlyCertificated          = $predicateOnlyCertificated->test($model3d);

        $location          = new Location();
        $location->country = $currentLocation->country->iso_code;
        $location->lat     = $currentLocation->lat;
        $location->lon     = $currentLocation->lon;


        $offersLocator = \Yii::createObject(OrderOfferLocatorService::class);
        $offersLocator->initAnonymousBundle($location);
        $offersLocator->setSortMode(RequestInfo::SORT_STRATEGY_DEFAULT);
        $offersLocator->onlyCertificated = $onlyCertificated;
        $offersLocator->currencies[]     = Currency::USD;

        $savedTextureSerialized = PrinterMaterialService::serializeModel3dTexture($model3d);
        $texture                = Model3dTextureFactory::createModel3dTexture(true);
        $texture->setPrinterMaterial(PrinterMaterialService::getDefaultMaterial()); // PLA is not availble in multicolor
        $model3d->setKitTexture($texture);
        $model3d->setAnyTextureAllowed(true);
        $offersLocator->formOffersListForModel($model3d);
        if (!$offersLocator->availableColorGroupsForCurrentPrintersList) {
            $offersLocator->onlyInternational = true;
            $offersLocator->formOffersListForModel($model3d);
        }

        /** @var MaterialColorItem $groupsColor */
        $groupsColors = [];

        // Group For material color groups
        foreach ($offersLocator->availableColorGroupsForCurrentPrintersList as $materialColorItem) {
            if ($printerMaterialGroup && $materialColorItem->getGroup() && $printerMaterialGroup->code !== $materialColorItem->getGroup()->code) {
                continue;
            }
            if ($printerColor && $printerColor->code !== $materialColorItem->getColor()->code) {
                continue;
            }
            $groupId = $materialColorItem->groupId;
            if (!$groupId) {
                continue;
            }
            $groupsColors[$groupId . '_' . $materialColorItem->colorId] = $materialColorItem;
        }

        $parcel = ParcelFactory::createFromModel($model3d);
        PrinterMaterialService::unSerializeModel3dTexture($model3d, $savedTextureSerialized);
        foreach ($groupsColors as $groupColor) {
            // Save infill. if it setted. Change only material group and part
            PrinterMaterialService::setMaterialGroupAndColor($model3d, $groupColor->getGroup(), $groupColor->getColor());
            $model3d->setAnyTextureAllowed(false);
            $offersLocator->formOffersListForModel($model3d);

            $allOffersList = $offersLocator->getAllOffersList();
            if (!$allOffersList) {
                continue;
            }
            /** @var OrderOffer $cheapestOffer */
            $cheapestOffer  = reset($allOffersList);
            $costModel3dUsd = PriceCalculator::calculateModel3dPriceUsd($model3d, true);
            $costUsd        = $costModel3dUsd->getAmount() + $cheapestOffer->anonymousPrintPrice->getAmount();

            [$estimateDeliveryCost, $cheapestOffer->deliveryTypes] = $deliveryService->resolveDeliveryRateAndLabels(
                $cheapestOffer->getPrinter()->companyService,
                $cheapestOffer->anonymousPrintPrice,
                $location,
                $parcel
            );

            $oneModel3dCostInfo                       = Yii::createObject(Model3dCostInfo::class);
            $oneModel3dCostInfo->printerMaterialGroup = $groupColor->getGroup();
            $oneModel3dCostInfo->printerColor         = $groupColor->getColor();
            $oneModel3dCostInfo->psPrinter            = $cheapestOffer->getPrinter();
            $oneModel3dCostInfo->price                = $costUsd;
            $oneModel3dCostInfo->estimateDeliveryCost = $estimateDeliveryCost;
            $costColors[]                             = $oneModel3dCostInfo;
        }
        return $costColors;
    }

    /**
     * @param StoreUnit $storeUnit
     * @param GeoLocation $currentLocation
     * @return CatalogCost|null
     */
    public static function getModel3dCatalogCostObject(StoreUnit $storeUnit, GeoLocation $currentLocation)
    {
        $currentCountry = $currentLocation->country;
        $calcCost       = CatalogCostRepository::findCost($storeUnit, $currentCountry);
        if (!$calcCost) {
            return null;
        }
        if ($calcCost->isExpired()) {
            return null;
        }
        return $calcCost;
    }

    /**
     * @param StoreUnit $storeUnit
     * @param GeoLocation|Location $currentLocation
     * @return CatalogCost
     * @throws \yii\base\Exception
     */
    public static function calculateModel3dCatalogCostObject(StoreUnit $storeUnit, GeoLocation $currentLocation)
    {
        $currentCountry = $currentLocation->country;
        $calcCost       = CatalogCostRepository::findCost($storeUnit, $currentCountry);
        if (!$calcCost) {
            $calcCosts = CatalogCostService::createCatalogCostForModel($storeUnit, $currentCountry);
            $calcCost  = $calcCosts[$currentCountry->id];
        }
        if ($calcCost->isExpired()) {
            CatalogCostService::reCalculateCost($calcCost);
        }
        return $calcCost;
    }

    /**
     * @param StoreUnit $storeUnit
     * @param GeoLocation|Location $currentLocation
     * @param string $currencyIsoCode
     * @return null|string
     */
    public static function calculateModel3dCatalogCost(StoreUnit $storeUnit, GeoLocation $currentLocation, $currencyIsoCode)
    {
        $calcCost = self::calculateModel3dCatalogCostObject($storeUnit, $currentLocation);
        $costUsd  = $calcCost->cost_usd;
        if ($costUsd !== null) {
            $costUsd = (float)$costUsd;
            $cost    = PaymentExchangeRateFacade::convert($costUsd, Currency::USD, $currencyIsoCode);
        } else {
            $cost = null;
        }
        return $cost;
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param string $currency
     * @param bool $anonymousMode
     * @return mixed
     */
    public static function calculateModel3dPrice(Model3dBaseInterface $model3d, $currency, $anonymousMode = false): Money
    {
        if (UserFacade::isObjectOwner($model3d->getSourceModel3d()) && (!$anonymousMode)) {
            return Money::zero($currency);
        }

        $count = 1;

        foreach ($model3d->getActiveModel3dParts() as $part) {
            $partKitCount = $part->getOriginalQty() == 0 ? 0 : $part->qty / $part->getOriginalQty();
            $count        = max($count, $partKitCount);
        }

        $price = $model3d->getPriceMoneyByQty($count);
        return $price->convertTo($currency);
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param PsPrinter $printer
     * @param $useMinOrderPrintPrice
     * @param bool $anonymousMode
     * @param int $quantity
     * @return Money|null
     */
    public static function calculateModel3dAndStoreUnitPrice(
        Model3dBaseInterface $model3d,
        PsPrinter $printer,
        $useMinOrderPrintPrice,
        $anonymousMode = false,
        $quantity = 1
    )
    {
        $printPrice   = self::calculateModel3dPrintPriceWithPackageFee($model3d, $printer, $useMinOrderPrintPrice, $anonymousMode);
        $model3dPrice = self::calculateModel3dPrice($model3d, $printPrice->getCurrency(), $anonymousMode);
        if ($printPrice === null) {
            return null;
        }
        return MoneyMath::sum($printPrice, $model3dPrice);
    }

    /**
     * @param Cart $cart
     * @return int
     */
    public static function calculateCartPrice(Cart $cart)
    {
        $printPriceTotal = 0;
        foreach ($cart->cartItems as $cartItem) {
            $printPriceTotal += self::calculateCartItemPrice($cartItem);
        }
        return $printPriceTotal;
    }

    /**
     * @param CartItem $cartItem
     * @return float|int|null
     */
    public static function calculateCartItemPrice(CartItem $cartItem)
    {
        $modelPrice = self::calculateModel3dPriceUsd($cartItem->model3dReplica) * $cartItem->qty;
        $printPrice = self::calculateModel3dPrintPriceUsd($cartItem->model3dReplica, $cartItem->machine->asPrinter(), true, false, $cartItem->qty);
        if ($printPrice === null) {
            return null;
        }
        return $modelPrice + $printPrice;
    }

    /**
     * @param Money $price
     * @param PsPrinter $printer
     * @return mixed
     */
    public static function minPrintPrice(Money $price, PsPrinter $printer)
    {
        $minOrderPrice = $printer->getMinPrintPrice();
        // price is 9.66, minOrderPrice is 15 - now 15, we don't need to add %
        return Money::create(max($price->getAmount(), $minOrderPrice->getAmount()), $price->getCurrency());
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param bool $anonymousMode
     * @return float|int
     */
    public static function calculateModel3dPriceUsd(Model3dBaseInterface $model3d, $anonymousMode = false): Money
    {
        $priceInUsd = self::calculateModel3dPrice($model3d->getSourceModel3d(), Currency::USD, $anonymousMode);
        return $priceInUsd->convertTo(Currency::USD);

    }


    public static function calculateModel3dPrintPriceWithPackageFee(
        Model3dBaseInterface $model3d,
        PsPrinter $printer,
        $useMinOrderPrintPrice = true,
        $anonymousMode = false,
        $internationalDelivery = false
    ): ?Money
    {
        $model3dParts = $model3d->getActiveModel3dParts();
        $totalCost    = 0;
        foreach ($model3dParts as $model3dPart) {
            $texture   = $model3dPart->getCalculatedTexture();
            $priceInfo = $printer->getPrintPriceInfoForTexture($texture, $anonymousMode);
            if ($priceInfo === null) {
                return null;
            }

            $volume    = $model3dPart->getVolume();
            $totalCost += $volume * round($priceInfo['price_ml']->getAmount(), 2);
        }
        $totalCost = Money::create($totalCost, $printer->company->currency);
        if ($useMinOrderPrintPrice) {
            $totalCost = Money::create(max($totalCost->getAmount(), $printer->min_order_price), $totalCost->getCurrency());
        }

        if($internationalDelivery === false) {
            $totalCost = MoneyMath::sum($totalCost, $printer->domesticPackagePrice());
        } else {
            $totalCost = MoneyMath::sum($totalCost, $printer->internationalPackagePrice());
        }

        $feeHelper = new FeeHelper();
        $totalCost = MoneyMath::sum($totalCost, $feeHelper->getTsCommonFee($totalCost));
        if ($totalCost->getAmount() > 0 && $totalCost->getAmount() < 0.01) {
            $totalCost = Money::create(0.01, $totalCost->getCurrency());
        }
        $totalCost = $totalCost->round();
        return $totalCost;
    }
}