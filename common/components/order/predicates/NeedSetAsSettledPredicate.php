<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\predicates;


use common\components\DateHelper;
use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\modules\payment\gateways\PaymentGatewayTransaction;

/**
 * Class NeedSetAsSettledPredicate
 * @package common\components\order\predicates
 */
class NeedSetAsSettledPredicate
{
    /**
     * @param StoreOrder $order
     * @return bool
     */
    public function test(StoreOrder $order)
    {
        foreach ($order->paymentTransactions as $transaction){

            if ($transaction->status != PaymentGatewayTransaction::STATUS_AUTHORIZED){
                return false;
            }

            if (in_array($transaction->vendor, [PaymentTransaction::VENDOR_TS, PaymentTransaction::VENDOR_BONUS, PaymentTransaction::VENDOR_THINGIVERSE])) {
                return false;
            }

            $isNeedSettle = (time() - DateHelper::strtotimeUtc($transaction->created_at)) > 86400 * 4;

            if ($isNeedSettle) {
                return true;
            }
        }

        return false;
    }

}