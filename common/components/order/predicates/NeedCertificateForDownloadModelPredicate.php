<?php
/**
 * Created by mitaichik
 */

namespace common\components\order\predicates;


use common\interfaces\Model3dBaseInterface;
use common\models\ApiPrintablePack;
use common\models\Model3dReplica;
use common\models\User;

/**
 * Class NeedCertificateForDownloadModelPredicate
 *
 * @package common\components\order\predicates
 */
class NeedCertificateForDownloadModelPredicate
{
    /**
     * @param Model3dBaseInterface $model
     * @return bool
     */
    public function test(Model3dBaseInterface $model)
    {
        $sourceModel = $model->getSourceModel3d();
        $modelId = $sourceModel->id;
        $modelUserId = $sourceModel->user_id;

        if ($model->source === Model3dBaseInterface::SOURCE_THINGIVERSE) {
            return false;
        }

        if ($modelUserId === User::USER_THINGIVERSE) {
            return false;
        }

        if ($model->source === Model3dBaseInterface::SOURCE_API && $modelUserId !== User::USER_THINGIVERSE) {
            $printablePack = ApiPrintablePack::findOne(['model3d_id' => $modelId]);

            if (!$printablePack) {
                return false;
            }

            return (bool)$printablePack->apiExternalSystem->only_certificated_printers;
        }

        if ($model->getSourceModel3d()->isPublished()) {
            return true;
        }

        return false;
    }
}