<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\components;

use yii\db\ActiveRecord;

/**
 * Class ArrayHelper
 *
 * @package common\components
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{
    /**
     * Group array by field
     *
     * @param array $array
     * @param string $name
     * @return array
     */
    public static function group($array, $name)
    {
        $result = [];
        foreach ($array as $item) {
            $result[self::getValue($item, $name)][] = $item;
        }
        return $result;
    }

    public static function mapByName($array, $name)
    {
        $result = [];
        foreach ($array as $item) {
            $result[self::getValue($item, $name)] = $item;
        }
        return $result;
    }

    /**
     * Remove values form array.
     * It return new array withou deleted valuse
     *
     * @param $array
     * @param $values
     * @return mixed
     */
    public static function removeValues($array, $values)
    {
        $values = is_array($values) ? $values : [$values];

        foreach ($values as $value) {
            if (($key = array_search($value, $array)) !== false) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    /**
     * Return first element for.
     * analog underscore _.first
     *
     * @param array $array
     * @param callable $filterFn
     * @return mixed
     */
    public static function first(array $array, $filterFn = null)
    {
        if ($filterFn === null) {
            if (!$array) {
                return null;
            }
            return array_values($array)[0];
        }

        foreach ($array as $value) {
            if ($filterFn($value)) {
                return $value;
            }
        }
        return null;
    }

    public static function arrayKeyFirst(array $array)
    {
        if (function_exists('array_key_first')) {
            return array_key_first($array);
        }
        foreach ($array as $key => $unused) {
            return $key;
        }
        return null;
    }

    /**
     * @param mixed[] ...$obj
     * @return mixed|null
     */
    public static function firstNotEmpty(...$obj)
    {
        foreach ($obj as $value) {
            if ($value) {
                return $value;
            }
        }
        return null;
    }

    /**
     * Return last element for array
     *
     * @param array $array
     * @return mixed
     */
    public static function last(array $array)
    {
        if (!$array) {
            return null;
        }
        return end($array);
    }

    /**
     * Return new array who contains part of $array
     *
     * @param array $array
     * @param array $keys Keys, who will copy
     * @param bool $keepKeys
     * @return array
     */
    public static function sub(array $array, array $keys, $keepKeys = true)
    {
        $result = [];

        foreach ($keys as $key) {
            if (array_key_exists($key, $array)) {
                if ($keepKeys) {
                    $result[$key] = $array[$key];
                } else {
                    $result[] = $array[$key];
                }
            }
        }

        return $result;
    }

    /**
     * Case-insensitive array_unique
     *
     * @param string[] $array
     * @return string[]
     */
    public static function uniqueCaseInsensitive(array $array)
    {
        return array_intersect_key($array, array_unique(array_map("strtolower", $array)));
    }


    public static function findAR($array, $conditions)
    {
        foreach ($array as $item) {
            $conditionsPerformed = true;
            foreach ($conditions as $key => $val) {
                if (is_object($item)) {
                    if ($item->$key !== $val) {
                        $conditionsPerformed = false;
                        break;
                    }
                } elseif (is_array($item)) {
                    if ($item[$key] !== $val) {
                        $conditionsPerformed = false;
                        break;
                    }
                }
            }
            if ($conditionsPerformed) {
                return $item;
            }
        }
        return null;
    }

    public static function findARById($array, $id)
    {
        foreach ($array as $item) {
            if ($item->id == $id) {
                return $item;
            }
        }
        return null;
    }

    public static function setObjectProperties($object, $data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value) && is_object($object->$key)) {
                self::setObjectProperties($object->$key, $value);
            } else {
                $object->$key = $value;
            }
        }
    }


    /**
     * Check that array comtains model use equals method
     *
     *
     * @param ActiveRecord[] $array
     * @param ActiveRecord $model
     * @return bool
     */
    public static function containsEqual(array $array, ActiveRecord $model): bool
    {
        foreach ($array as $item) {
            if ($item->equals($model)) {
                return true;
            }
        }
        return false;
    }
}