<?php

 
namespace common\components;

/** 
 *
 * @author DeFacto
 */
class GoogleOAuth extends \yii\authclient\clients\Google{
    
    /**
     * @inheritdoc
     */
    protected function defaultViewOptions()
    {
        return [
            'popupWidth' => 480,
            'popupHeight' => 500,
        ];
    }
}
