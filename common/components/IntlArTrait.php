<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.05.17
 * Time: 8:40
 */

namespace common\components;

use Yii;
use yii\db\ActiveRecord;

/**
 * Trait IntlArTrait
 *
 * @package common\components
 * @mixin ActiveRecord
 */
trait IntlArTrait
{
    protected $translations;

    protected function getCurrentLang()
    {
        return Yii::$app->language;
    }


    public static function getTranslateFieldsList()
    {
        return [
            'title'
        ];
    }

    public function getTranslations($langIso = '')
    {
        if ($this->translations) {
            return $this->translations;
        }
        if (!$langIso) {
            $langIso = $this->getCurrentLang();
        }
        $sql           = 'SELECT * FROM `' . $this->tableName() . '_intl` WHERE `model_id`=\'' . $this->primaryKey . '\' AND `lang_iso`=\'' . $langIso . '\' AND `is_active`=1';
        $uniqueCacheId = 'intsr:' . md5($sql);
        $cacheResult   = app('cache')->get($uniqueCacheId);

        if ($cacheResult !== false) {
            $this->translations = $cacheResult;
        } else {
            $this->translations = Yii::$app->getDb()->createCommand($sql)->queryOne();
            if ($this->translations) {
                unset($this->translations['id']);
                unset($this->translations['model_id']);
                unset($this->translations['lang_iso']);
                unset($this->translations['is_active']);
                foreach ($this->translations as $key => $translation) {
                    if (!$translation) {
                        unset($this->translations[$key]);
                    }
                }
            }
            app('cache')->set($uniqueCacheId, $this->translations ? $this->translations : null, 60 * 30);

        }
        return $this->translations;
    }


    public function isActiveTranslations()
    {
        if ($this->getCurrentLang() == 'en-US') {
            return false;
        }
        return true;
    }

    /**
     * @param $name
     */
    public function __get($name)
    {
        if (!$this->isActiveTranslations()) {
            return parent::__get($name);
        }
        if (in_array($name, $this->getTranslateFieldsList())) {
            if ((!$translations = $this->getTranslations()) || (!array_key_exists($name, $translations))) {
                return parent::__get($name);
            }
            if ($translations[$name] !== '') {
                // if empty return  eng translation
                return $translations[$name];
            }

        }
        return parent::__get($name);
    }
}