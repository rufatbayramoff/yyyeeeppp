<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.10.16
 * Time: 10:52
 */

namespace common\components;

use common\models\PaymentExchangeRate;
use lib\money\Money;
use yii\base\Component;

class PaymentExchangeRateConverter extends Component
{
    private static $_lastPaymentExchangeRate = null;

    public function setLastPaymentExchangeRate($lastPaymentExchangeRate)
    {
        self::$_lastPaymentExchangeRate = $lastPaymentExchangeRate;
    }

    public function getLastPaymentExchangeRate()
    {
        self::$_lastPaymentExchangeRate = self::$_lastPaymentExchangeRate ? : \Yii::$app->cache->get('pxrate');
        if (self::$_lastPaymentExchangeRate && self::$_lastPaymentExchangeRate->id) {
            return self::$_lastPaymentExchangeRate;
        }
        self::$_lastPaymentExchangeRate = PaymentExchangeRate::find()->orderBy('id desc')->one();
        if(!self::$_lastPaymentExchangeRate){
            throw new \yii\console\Exception('Please update payment_exchange_rate. Run [php yii payment/update-rates]');
        }
        \Yii::$app->cache->set('pxrate', self::$_lastPaymentExchangeRate, 1800);
        return self::$_lastPaymentExchangeRate;
    }

    /**
     * @param $price
     * @param $fromCurrency
     * @param $toCurrency
     * @param bool $round
     * @param PaymentExchangeRate|null $paymentExchangeRate
     * @return float|int|string
     * @throws \Exception
     */
    public function convert($price, $fromCurrency, $toCurrency, $round = false, $paymentExchangeRate = null)
    {
        $fromCurrency = strtoupper($fromCurrency);
        if (!$paymentExchangeRate) {
            $paymentExchangeRate = $this->getLastPaymentExchangeRate();
        }

        $fromCurrency = $paymentExchangeRate->validateCurrency($fromCurrency);
        $toCurrency = $paymentExchangeRate->validateCurrency($toCurrency);

        if (!is_float($price) && !is_numeric($price)) {
            throw new \Exception(sprintf("%s not valid price", $price));
        }
        if ($fromCurrency == $toCurrency) {
            $convertedPrice = $price;
        } else {
            $base = $paymentExchangeRate->base; // USD
            $rates = $paymentExchangeRate->getRatesData();
            $fromRate = $rates[$fromCurrency];
            $toRate = $rates[$toCurrency];
            if ($fromCurrency == $base) {
                $convertedPrice = $price * $toRate;
            } else if ($toCurrency == $base) {
                $convertedPrice = $price / $fromRate;
            } else {
                $basePrice = $price / $fromRate; // in usd
                $convertedPrice = $basePrice * $toRate;
            }

            \Yii::info(sprintf("In: %s %s Out %s %s", $price, $fromCurrency, $convertedPrice, $toCurrency), 'tsdebug');
        }
        if ($round) {
            $convertedPrice = self::round($convertedPrice);
        }
        return $convertedPrice;
    }

    /**
     * @param Money $money
     * @param $toCurrency
     * @param bool $round
     * @param null $paymentExchangeRate
     * @return Money
     */
    public function convertMoney(Money $money, $toCurrency, $round = false, $paymentExchangeRate = null)
    {
        $newAmount = $this->convert($money->getAmount(), $money->getCurrency(), $toCurrency, $round, $paymentExchangeRate);
        return Money::create($newAmount, $toCurrency);
    }

    /**
     * @param $amount
     * @return float
     */
    public static function round($amount)
    {
        return round($amount, 2);
    }

    /**
     * @param $amount
     * @return float
     */
    public static function roundUp($amount)
    {
        return round($amount, 2, PHP_ROUND_HALF_UP);
    }
}