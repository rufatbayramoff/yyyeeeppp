<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace common\events;


use common\models\base\Model3d;
use common\models\model3d\events\DeleteEvent;
use frontend\models\model3d\CollectionFacade;
use yii\base\Event;

Event::on(Model3d::class, DeleteEvent::class, function(DeleteEvent $event)
{
    // Delete models from all collection all users
    CollectionFacade::removeModel($event->getModel3d());
});