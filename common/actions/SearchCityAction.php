<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.11.17
 * Time: 11:54
 */

namespace common\actions;

use common\components\ArrayHelper;
use common\models\GeoCity;
use Yii;
use yii\base\Action;
use yii\web\Response;

class SearchCityAction extends Action
{
    public function run()
    {
        $q = Yii::$app->request->get('q');
        $out['results'] = [];
        foreach (GeoCity::find()->where(['like', 'title', $q])->all() as $geoCity) {
            $geoCityArray = ['id' => $geoCity->id, 'text' => $geoCity->getFullTitle()];
            $out['results'][] = $geoCityArray;
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $out;
    }
}