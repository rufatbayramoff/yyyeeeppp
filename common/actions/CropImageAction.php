<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.08.17
 * Time: 15:47
 */

namespace common\actions;

use backend\components\AdminAccess;
use backend\components\NoAccessException;
use common\components\BaseController;
use common\models\Model3dImg;
use common\services\Model3dImageService;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\Action;
use yii\base\UserException;

/**
 * Class CropImageAction
 *
 * @property BaseController $controller
 * @package common\actions
 */
class CropImageAction extends Action
{
    /**
     * @var Model3dImageService
     */
    protected $model3dImageService;

    public function injectDependencies(
        Model3dImageService $model3dImageService
    ) {
        $this->model3dImageService = $model3dImageService;
    }

    protected function checkAccess(Model3dImg $model3dImg)
    {
        if (UserFacade::isObjectOwner($model3dImg)) {
            return true;
        }
        $currentUser = UserFacade::getCurrentUser();
        if ($currentUser && $currentUser->id < 1000) {
            AdminAccess::validateAccess('storeunit.view');
            return true;
        }
        throw new NoAccessException('You can`t crop this image');
    }


    protected function clearResizeImageCache(Model3dImg $model3dImg)
    {
        $cacheFilesMaskFront = Yii::getAlias('@frontend/runtime/resizeImgCache/') . $model3dImg->file->id . '_*';
        $cacheFilesMaskBack = Yii::getAlias('@backend/runtime/resizeImgCache/') . $model3dImg->file->id . '_*';
        $cacheFilesPath = glob($cacheFilesMaskFront) + glob($cacheFilesMaskBack);
        foreach ($cacheFilesPath as $cacheFilePath) {
            unlink($cacheFilePath);
        }
    }

    /**
     * crop submited image and save orig
     *
     * @param int $id
     * @return array
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */

    public function run($id)
    {
        if (!app('request')->isPost) {
            throw new UserException('Invalid request method. Need post method.');
        }
        $model3dImg = Model3dImg::tryFindByPk($id);

        $this->checkAccess($model3dImg);
        $this->model3dImageService->createOriginalCopyIfNotExists($model3dImg);
        $this->model3dImageService->createCropTempFile($model3dImg, Yii::$app->request->post());
        $this->clearResizeImageCache($model3dImg);

        if (Yii::$app->request->isAjax) {
            return $this->controller->jsonSuccess(['url' => $model3dImg->file->getFileUrl()]);
        } else {
            echo "<script>window.opener.location.reload();self.close()</script>";
        }
    }
}