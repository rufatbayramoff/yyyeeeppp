<?php

namespace common\modules\equipments\repositories;

use yii\helpers\FileHelper;

/**
 * User: nabi
 */

class PrinterImageRepository extends \yii\base\BaseObject
{
    private $defaultFilter = ['*.jpg', '*.png', '*.jpeg', '*.gif', '*.JPG', '*.PNG', '*.GIF', '*.JPEG'];

    /**
     * @param $path
     * @param $only
     * @return array
     */
    public function findAll($path, $only = [])
    {
        if(!$only){
            $only = $this->defaultFilter;
        }
        $files = FileHelper::findFiles($path, ['only'=>$only]);
        return $files;
    }

    public function scanDir($path)
    {
        $files = scandir($path);
        return $files;
    }

}