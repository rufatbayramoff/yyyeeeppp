<?php
/**
 * User: nabi
 */

namespace common\modules\equipments\repositories;

use common\modules\catalogPs\repositories\AbstractArRepository;

class PrinterRepository extends AbstractArRepository
{

    public $tableName = 'printer';

    public function fillItems()
    {
        if (!empty($this->items)) {
            return;
        }
        $obj = new $this->modelClass;
        $items = $obj::find()->where(['is_active'=>1])->asArray()->all();
        $this->setItems($items);
    }

}