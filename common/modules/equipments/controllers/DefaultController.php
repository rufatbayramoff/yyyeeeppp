<?php
/**
 * User: nabi
 */

namespace common\modules\equipments\controllers;

use common\components\ActiveQuery;
use common\components\ArrayHelper;
use common\components\BaseController;

use common\models\EquipmentCategory;
use common\models\Printer;
use common\models\WikiMachine;
use common\modules\catalogPs\repositories\PrinterTechnologyRepository;
use common\modules\equipments\models\CatalogSearchForm;
use common\modules\equipments\models\CatalogWikiMachineSearchForm;
use yii\data\ActiveDataProvider;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;

class DefaultController extends BaseController
{

    private $printersRepository;
    private $technologyRepository;
    /**
     * @var CatalogSearchForm
     */
    private $searchForm;

    /**
     * @var CatalogWikiMachineSearchForm
     */
    protected $searchFormWiki;

    public function injectDependencies(CatalogSearchForm $searchForm, CatalogWikiMachineSearchForm $searchFormWiki, PrinterTechnologyRepository $technologyRepository)
    {
        $this->searchForm = $searchForm;
        $this->printersRepository = null;
        $this->technologyRepository = $technologyRepository;
        $this->searchFormWiki = $searchFormWiki;
    }

    public function findEquipmentCategories($fullCategory)
    {
        $categoryNames = explode('_', $fullCategory);
        $categories = [];
        $currentCategory = EquipmentCategory::find()->where(['depth' => 1])->one(); // Get root
        foreach ($categoryNames as $categoryName) {
            $currentCategory = $currentCategory->children()->andWhere(['slug' => mb_strtolower($categoryName)])->one();
            if (!$currentCategory) {
                return null;
            }
            $categories[] = $currentCategory;
        };
        return $categories;
    }

    public function actionIndex($filter = null)
    {
        $searchForm = $this->searchForm;
        $searchForm->perPage = 15;
        $filters = $searchForm->parseRequestParams($filter, \Yii::$app->request->getQueryParams());
        $searchForm->load($filters);

        if (!$searchForm->validate()) {
            return $this->redirect($searchForm->getValidUrl(), 301);
        }

        $dataProvider = $searchForm->getDataProvider();

        return $this->render(
            'index.php',
            [
                'category'     => $searchForm->getCategory(),
                'searchForm'   => $searchForm,
                'dataProvider' => $dataProvider
            ]
        );
    }

    /**
     * @param $category
     * @param null $filter
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionWiki($category, $filter = null)
    {
        $searchForm = $this->searchFormWiki;
        $searchForm->categories = $this->findEquipmentCategories($category);

        $searchForm->perPage = 15;
        $searchForm->changeCategory($this->findCategory($category));
        $filters = $searchForm->parseRequestParams($filter, \Yii::$app->request->getQueryParams());
        $searchForm->load($filters);

        if (!$searchForm->validate()) {
            return $this->redirect($searchForm->getValidUrl(), 301);
        }

        $dataProvider = $searchForm->getDataProvider();

        return $this->render('wikiMachines', [
            'searchForm' => $searchForm,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionItem($slug)
    {
        $item = Printer::findBySlug($slug);
        if (!$item) {
            throw new NotFoundHttpException('Printer not found');
        }
        $this->view->title = $item->title . ' - 3D Printers - Treatstock';
        $this->view->params['meta_description'] = StringHelper::truncateWords(strip_tags($item->description), 25);
        return $this->render('item.php', ['slug' => $slug, 'item' => $item]);
    }

    public function actionWikiItem($code)
    {
        $item = WikiMachine::find()->where(['code' => $code])->one();
        if (!$item) {
            throw new NotFoundHttpException('Wiki machine not found');
        }
        return $this->render('itemWikiMachine.php', ['item' => $item]);
    }

    /**
     * @param string $slug
     * @return array|mixed|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findCategory(string $slug)
    {
        $category = EquipmentCategory::find()->with(['wikiMachineAvailableProperties' => static function(ActiveQuery $activeQuery){
            $activeQuery->with(['wikiMachineProperties' => static function(ActiveQuery $query){
                $query->orderBy(['value' => SORT_ASC]);
                return $query;
            }]);
            return $activeQuery;
        }])->where(['slug' => $slug])->one();
        if($category) {
            return $category;
        }
        throw new NotFoundHttpException('Category not found');
    }
}