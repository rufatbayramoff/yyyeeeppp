<?php
/**
 * User: nabi
 */

namespace common\modules\equipments;

use Yii;
use yii\web\UrlRuleInterface;

class EquipmentUrlRule implements UrlRuleInterface
{

    const CATALOG_ROUTE = 'machines/default/index';
    const PATH_START    = 'machines';

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        if (!$this->checkPathRoute($pathInfo)) {
            return false;
        }
        $route              = self::CATALOG_ROUTE;
        $params             = [];
        $parameters         = explode('/', $pathInfo);
        $params['category'] = $parameters[1];
        if (count($parameters) > 2) {
            $categories = $parameters;
            unset($categories[count($parameters) - 1]);
            unset($categories[0]);
            $params['category'] = implode('_', $categories);
            $params['filter']   = $parameters[count($parameters) - 1];
        }
        Yii::trace("Request parsed with URL rule: " . $route, __METHOD__);
        return [$route, $params];
    }

    public function createUrl($manager, $route, $params)
    {
        if ($route !== self::CATALOG_ROUTE) {
            return false;
        }
        $url = self::PATH_START . '/' . $params['category'] . '/';

        if (array_key_exists('filter', $params) && !empty($params['filter'])) {
            $url .= $params['filter'];
        }
        if (array_key_exists('page', $params) && !empty($params['page'])) {
            $url .= '?page=' . (int)$params['page'];
            if (array_key_exists('sort', $params) && !empty($params['sort'])) {
                $url .= '&sort=' . $params['sort'];
            }
            if (array_key_exists('search', $params) && !empty($params['search'])) {
                $url .= '&search=' . $params['search'];
            }
        }
        return $url;
    }

    /**
     * check if given path should be parsed with this url rule
     *
     * @param $pathInfo
     * @return bool
     */
    private function checkPathRoute($pathInfo)
    {
        if (strpos($pathInfo, self::PATH_START . '/') === 0) {
            return true;
        }
        return false;
    }

}