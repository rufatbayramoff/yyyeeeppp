<?php
/**
 * User: nabi
 */
namespace common\modules\equipments\helpers;

class SimpleFile {

    public $filename;
    public $size;
    public $pathInfo;
    public $extension;
    public $imageInfo;
    public $filemTime;
    public $url;

    private function __construct()
    {
    }

    /**
     * @param array $urls
     * @return SimpleFile[]
     */
    public static function createByUrls(array $urls)
    {
        $result = [];
        foreach($urls as $url){
           $result[] = self::createByUrl($url);
        }
        return array_filter($result);
    }

    /**
     * @param $url
     * @return SimpleFile
     */
    public static function createByUrl(string $url)
    {
        $sm = new SimpleFile();
        $pathInfo = pathinfo($url);
        if(empty($pathInfo['extension'])){
            return null;
        }
        $sm->url = $url;
        $sm->extension = isset($pathInfo['extension']) ? strtolower($pathInfo['extension']) : '';
        $sm->filename = $pathInfo['basename'];
        $result[] = $sm;
        return $sm;
    }

    public function getStaticUrl()
    {
        return $this->url;
    }
}