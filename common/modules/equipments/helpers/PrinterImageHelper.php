<?php
/**
 * User: nabi
 */

namespace common\modules\equipments\helpers;


use common\modules\equipments\repositories\PrinterImageRepository;
use yii\base\BaseObject;
use yii\helpers\Inflector;

class PrinterImageHelper extends BaseObject
{
    /**
     * where to scan for images
     * default path frontend/web/static/uploads
     *
     * @var string
     */
    public $scanPath;

    /**
     * @var PrinterImageRepository
     */
    private $imageRepository;

    /**
     * @var array
     */
    public $images;
    /**
     * @var array
     */
    private $filesMap;

    public function injectDependencies(PrinterImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;

    }
    public function init()
    {
        parent::init();
        if(empty($this->scanPath))
            $this->scanPath = \Yii::getAlias('@frontend/web/static/uploads');
    }

    public function getScanPath()
    {
        return $this->scanPath;
    }

    private function getFilesMap()
    {
        if(!empty($this->filesMap)){
            return $this->filesMap;
        }
        $this->filesMap = $this->formatImagesMapGroupByPrinter($this->imageRepository->scanDir($this->scanPath));
        return $this->filesMap;
    }
    /**
     * @param string $printerTitle
     * @return mixed|null
     */
    public function findLogoImageByTitle(string $printerTitle)
    {
        $f = $this->findImagesByTitle($printerTitle);

        if(!empty($f)){
            $filePath = $f[0];
            if(strpos($filePath, "_0.")!==false){
                return $filePath;
            }
            return null;
        }
        return $f;
    }



    public function findImagesByTitle(string $printerTitle = null)
    {
        if (!$printerTitle) {
            return null;
        }
        $printerTitle = str_ireplace(':', '-', $printerTitle);
        $printerTitle = str_replace('+', 'Plus', $printerTitle);
        $printerTitle = str_replace([' ', ')', '(', '.', '/', '\\', '#', '?'], '-', $printerTitle);
        $filesMap = $this->getFilesMap();
        $slug = strtolower(Inflector::slug($printerTitle));
        if(!empty($filesMap[$slug])){
            $files = $filesMap[$slug];
            sort($files);
            return $files;
        }
        return null;
    }

    /**
     * get url by filepath, replace alias by param server
     *
     * @param $filePath
     * @return mixed
     */
    private function getUrlByFilePath($filePath)
    {
        $filePath = str_replace(\Yii::getAlias('@frontend/web'), param('server'), $filePath);
        return $filePath;
    }

    private function getSlugByFilename($filename)
    {
        $filename = str_replace('-3d-printer', '', $filename);
        $filename = strtolower($filename);
        $urlKey = explode("_", $filename);
        if(count($urlKey)===0){
            return false;
        }
        array_pop($urlKey);
        return end($urlKey);
    }

    private function formatImagesMapGroupByPrinter($images)
    {
        $result = [];
        foreach($images as $k=>$v){
            $filePath = \Yii::getAlias($v);
            $url =  '/static/uploads/'. $filePath;
            $printerSlug = $this->getSlugByFilename($filePath);
            if(!$printerSlug){ continue; }
            if(!array_key_exists($printerSlug, $result)){
                $result[$printerSlug] = [];
            }

            $result[$printerSlug][] = $url;
        }
        return $result;
    }

    public function formatImagesAsMap($files)
    {
        $result = [];
        foreach($files as $k=>$v){
            $filePath = \Yii::getAlias($v);
            if(!file_exists($filePath)){
                continue;
            }
            $url = str_replace(\Yii::getAlias('@frontend/web'), param('server'), $filePath);
            $result[$k] = [
                'filename'  => $filePath,
                'size'      => filesize($filePath),
                'pathinfo'  => pathinfo($filePath),
                'imageinfo' => getimagesize($filePath),
                'filemtime' => filemtime($filePath),
                'url'       => $url
            ];
        }
        return $result;
    }
}