<?php

namespace common\modules\equipments\helpers;

use common\models\WikiMachine;
use common\modules\equipments\EquipmentsModule;

class WikiMachineUrlHelper
{
    public static function viewWikiMachine(WikiMachine $wikiMachine)
    {
        return  EquipmentsModule::URL_PREFIX . '/wiki-item/' . $wikiMachine->code;
    }
}