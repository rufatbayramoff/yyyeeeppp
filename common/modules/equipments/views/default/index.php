<?php

use common\components\ArrayHelper;
use common\models\EquipmentCategory;
use common\modules\equipments\EquipmentsModule;
use yii\helpers\Url;

/** @var $this \yii\web\View */
/** @var $searchForm \common\modules\equipments\models\CatalogSearchForm */

$description = '';
$header      = $this->title;
if ($this->context->seo) {
    $this->title = $this->context->seo->title;
    $header      = $this->context->seo->header;
    $description = $this->context->seo->description ?? '';
}
if (empty($this->title)) {
    if (!empty($category)) {
        $this->title = $category->title;
    } else {
        $this->title = "Comparison Guide for 3D Printers";
    }
}
if (!$header) {
    $header = $this->title;
}

$ogTags = [
    'og:url'         => param('server') . app('request')->url,
    'og:title'       => "Comparison Guide for 3D Printers",
    'og:description' => \H(strip_tags($description)),
    'og:image'       => 'https://static.treatstock.com/static/uploads/3Dprinting-guide_800x600.png',
    'og:type'        => 'website'
];
$this->registerLinkTag([
    'rel'  => 'image_src',
    'href' => 'https://static.treatstock.com/static/uploads/3Dprinting-guide_800x600.png'
], true);

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to('/machines/3d-printers/', true)]);

$this->params['ogtags'] = $ogTags;

/** @var EquipmentCategory[] $parentCategories */
$parentCategories = [];
if ($searchForm->getCategory()) {
    $parentCategories = $searchForm->getCategory()->parents()->andWhere(['>=', 'depth', 2])->orderBy('lft')->all();
    array_push($parentCategories, $searchForm->getCategory());
}

$breadcrumps                                                        = [];
$breadcrumps[_t('catalog.machine', 'Manufacturing Machines Guide')] = EquipmentsModule::URL_PREFIX;
$fullLink                                                           = EquipmentsModule::URL_PREFIX;
foreach ($parentCategories as $category) {
    $fullLink                      .= '/' . mb_strtolower($category->slug);
    $breadcrumps[$category->title] = $fullLink;
}
?>
<div class="over-nav-tabs-header">
    <div class="container container--wide">
        <div class="row">
            <div class="col-sm-9">
                <ol class="breadcrumb m-t10 m-b0">
                    <?php
                    foreach ($breadcrumps as $breadcrumpText => $breadcrumpUrl) {
                        echo '<li>';
                        echo $breadcrumpUrl ? '<a href="' . HL($breadcrumpUrl) . '">' : '';
                        echo H($breadcrumpText);
                        echo $breadcrumpUrl ? '</a>' : '';
                        echo '</li>';
                    }
                    ?>
                </ol>
                <?php
                echo \frontend\widgets\BreadcrumbsSchemaWidget::widget([
                'breadcrumpsItems' => $breadcrumps
                ]);
                ?>
                <h1 class="m-t10"><?= H($header); ?></h1>
            </div>
            <div class="col-sm-3 p-l0 p-r0">
                <div class="machines-share">
                    <h4 class="machines-share__title"><?= _t('public.store', 'Share') ?></h4>
                    <?php
                    echo \frontend\widgets\SocialShareWidget::widget(['showHtmlcode' => false]); ?>
                </div>
            </div>
        </div>

    </div>
</div>
<?php echo $this->render('_searchTopbar.php', ['searchForm' => $searchForm]); ?>
<?php if (!false): ?>

<?php endif; ?>
<div class="container container--wide">
    <div class="row">
        <div class="col-xs-12 col-sm-9 col-lg-10">

            <?php
            $listView = Yii::createObject(
                [
                    'class'        => \yii\widgets\ListView::class,
                    'dataProvider' => $dataProvider,
                    'itemOptions'  => ['tag' => null],
                    'itemView'     => 'listItem',
                    'viewParams'   => [
                    ]
                ]
            );

            ?>
            <?php if ($listView->dataProvider->getCount() > 0): ?>
                <div class="responsive-container-list responsive-container-list--3">
                    <?= $listView->renderItems() ?>
                </div>
            <?php else: ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?= _t('catalog.machine', 'Not found'); ?>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-sm-12 machines-paginations">
                    <?= $listView->renderPager() ?>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-sm-3 col-lg-2 p-l0">
            <div class="tsadelement">
                <div id="amzn-assoc-ad-a62ceb71-6f15-49a6-93a9-810eeb3bdbc2"></div>
                <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=a62ceb71-6f15-49a6-93a9-810eeb3bdbc2"></script>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-10">
            <div class="p-l0 p-r0 p-0">
                <div class="tsadelement tsadelement--horizontal">
                    <div id="amzn-assoc-ad-36ecd97a-d7c3-42bd-b069-67da508d2ac4"></div>
                    <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=36ecd97a-d7c3-42bd-b069-67da508d2ac4"></script>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <?php
            echo H($category ? $category->description : ''); ?>
        </div>
    </div>
</div>

<?= $this->render('initSwiper'); ?>

