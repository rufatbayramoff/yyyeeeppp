<?php

use common\models\EquipmentCategory;
use yii\bootstrap\ActiveForm;

/** @var $searchForm \common\modules\equipments\models\CatalogSearchForm */

$hideSort = $hideSort ?? false;

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->controller([
        'wiki/MachinesCatalogController'
    ])
    ->controllerParams([
        'category'   => $searchForm->category,
        'technology' => $searchForm->technology,
        'brand'      => $searchForm->brand,
        'material'   => $searchForm->material ? (is_string($searchForm->material) ? $searchForm->material : implode('_', $searchForm->material)) : '',
        'search'     => $searchForm->search
    ]);

?>
<div class="nav-tabs__container" id="catalog" ng-controller="MachinesCatalogController">
    <div class="container container--wide">
        <div class="nav-filter nav-filter--many">
            <?php $form = ActiveForm::begin(
                [
                    'layout'      => 'inline',
                    'options'     => [
                        'onSubmit' => 'return false;',
                    ],
                    'fieldConfig' => [
                        'template'     => "{label} {input} ",
                        'inputOptions' => [
                            'class' => 'form-control input-sm nav-filter__input',
                        ],
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ],
                ]
            );
            ?>
            <input type="checkbox" id="showhideNavFilter">
            <label class="nav-filter__mobile-label" for="showhideNavFilter">
                <?= _t('site.ps', 'Filters'); ?>
                <span class="tsi tsi-down"></span>
            </label>
            <div class="nav-filter__mobile-container">
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Machine Type'); ?></label>
                    <?= $form->field($searchForm, 'category')
                        ->dropDownList($searchForm->getCategoriesList(),
                            [
                                'ng-model'  => 'category',
                                'ng-change' => 'onChangedFilter()'
                            ])
                        ->label(false); ?>
                </div>
                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'technology')
                        ->dropDownList($searchForm->getComboTechnologies(), [
                            'prompt' => _t('site.catalogps', 'Any'),

                            'ng-model'  => 'technology',
                            'ng-change' => 'onChangedFilter()'
                        ]) ?>
                </div>
                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'brand')
                        ->dropDownList($searchForm->getComboBrand(), [
                            'prompt'    => _t('site.catalogps', 'Any'),
                            'ng-model'  => 'brand',
                            'ng-change' => 'onChangedFilter()'
                        ]) ?>
                </div>
                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'material')
                        ->dropDownList($searchForm->getComboMaterials(), [
                            'prompt'    => _t('site.catalogps', 'Any'),
                            'ng-model'  => 'material',
                            'ng-change' => 'onChangedFilter()'
                        ]) ?>
                </div>
                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'search')->textInput([
                        'ng-model'  => 'search',
                        'ng-change' => 'onChangedFilter()'
                    ]); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
