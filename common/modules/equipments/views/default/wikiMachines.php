<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.12.17
 * Time: 16:52
 */

/** @var \common\modules\equipments\models\CatalogWikiMachineSearchForm $searchForm */
/** @var \frontend\components\FrontendWebView $this */

/** @var \yii\data\ArrayDataProvider $dataProvider */

use common\modules\equipments\EquipmentsModule;
use yii\helpers\Url;

if ($this->context->seo) {
    $this->title = $this->context->seo->title;
}
if (empty($this->title)) {
    if (!empty($category)) {
        $this->title = $category->title;
    } else {
        $this->title = _t('catalog.machine', 'Manufacturing Machines Guide');
    }
}

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to('/machines/3d-printers/', true)]);

$categories = $searchForm->categories;

$breadcrumps                                                        = [];
$breadcrumps[_t('catalog.machine', 'Manufacturing Machines Guide')] = EquipmentsModule::URL_PREFIX;
$fullLink                                                           = EquipmentsModule::URL_PREFIX;
foreach ($categories as $category) {
    $fullLink                      .= '/' . mb_strtolower($category->slug);
    $breadcrumps[$category->title] = $fullLink;
}

?>
<div class="over-nav-tabs-header">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <ol class="breadcrumb m-t10 m-b0">
                    <?php
                    foreach ($breadcrumps as $breadcrumpText => $breadcrumpUrl) {
                        echo '<li>';
                        echo $breadcrumpUrl ? '<a href="' . HL($breadcrumpUrl) . '">' : '';
                        echo H($breadcrumpText);
                        echo $breadcrumpUrl ? '</a>' : '';
                        echo '</li>';
                    }
                    ?>
                </ol>
                <?php
                echo \frontend\widgets\BreadcrumbsSchemaWidget::widget([
                    'breadcrumpsItems' => $breadcrumps
                ]);
                ?>
                <h1 class="m-t10"><?php echo $this->title; ?></h1>
            </div>
            <div class="col-sm-3 p-l0 p-r0">
                <div class="machines-share">
                    <h4 class="machines-share__title"><?= _t('public.store', 'Share') ?></h4>
                    <?php
                    echo \frontend\widgets\SocialShareWidget::widget(['showHtmlcode' => false]); ?>
                </div>
            </div>
        </div>

    </div>
</div>
<?= $this->render('_searchTopbarWikiMachines.php', ['searchForm' => $searchForm]); ?>

<div class="container">
    <?php
    $listView = Yii::createObject(
        [
            'class'        => \yii\widgets\ListView::class,
            'dataProvider' => $dataProvider,
            'itemOptions'  => ['tag' => null],
            'itemView'     => 'listItemWikiMachine',
            'viewParams'   => [
            ]
        ]
    );
    ?>
    <?php if ($listView->dataProvider->getCount() > 0): ?>
        <div class="responsive-container-list responsive-container-list--3">
            <?= $listView->renderItems() ?>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-lg-12">
                <?= _t('catalog.machine', 'Not found'); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $listView->renderPager() ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <?php
            echo H($category ? $category->description : ''); ?>
        </div>
    </div>
</div>
<?= $this->render('initSwiper'); ?>
