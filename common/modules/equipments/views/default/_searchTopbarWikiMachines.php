<?php

use common\models\EquipmentCategory;
use common\models\WikiMachineAvailableProperty;
use common\models\WikiMachineProperty;
use yii\bootstrap\ActiveForm;

/** @var $searchForm \common\modules\equipments\models\CatalogWikiMachineSearchForm */
/** @var $mainCategory EquipmentCategory */

$hideSort = $hideSort ?? false;

$categories = EquipmentCategory::find()->withoutRoot()->orderBy('lft')->all();

function printOption($model, $data)
{
    $levelUrl = [];
    foreach ($data as $k => $equipmentCategory) {
        $level            = $equipmentCategory->depth - 2;
        $levelUrl         = array_splice($levelUrl, 0, $level + 1); // Cat end
        $levelUrl[$level] = $equipmentCategory->slug;

        $space    = str_repeat('&nbsp; ', $level * 2);
        $selected = 'selected="selected"';
        if ($equipmentCategory->id != $model->id) {
            $selected = '';
        }
        $url = implode('/', $levelUrl);
        echo sprintf("<option value='%s' %s>%s</option>", HL('/machines/' . $url ), $selected, $space . $equipmentCategory->title);
    }
}

?>
<div class="nav-tabs__container">
    <div class="container">
        <div class="nav-filter nav-filter--many">
            <?php $form = ActiveForm::begin(
                [
                    'layout'      => 'inline',
                    'options'     => [
                        'onSubmit' => new \yii\web\JsExpression('TsEquipmentSearch.changeLocation(this.form)'),
                    ],
                    'fieldConfig' => [
                        'template'     => "{label} {input} ",
                        'inputOptions' => [
                            'class'    => 'form-control input-sm nav-filter__input',
                            'onchange' => new \yii\web\JsExpression('TsEquipmentSearch.changeLocation(this.form)')
                        ],
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ],
                ]
            );
            ?>
            <input type="checkbox" id="showhideNavFilter">
            <label class="nav-filter__mobile-label" for="showhideNavFilter">
                <?= _t('site.ps', 'Filters'); ?>
                <span class="tsi tsi-down"></span>
            </label>
            <div class="nav-filter__mobile-container">
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Machine Type'); ?></label>
                    <select class="form-control input-sm nav-filter__input nav-filter__input--wide" name="category"
                            onchange="TsEquipmentSearch.changeCategoryLocation(this.value)">
                        <?= printOption($searchForm->getLastCategory(), $categories); ?>
                    </select>
                </div>
                <?php /** @var WikiMachineAvailableProperty $option */
                foreach ($searchForm->options as $option):?>
                    <div class="nav-filter__group">
                        <?= $form->field($searchForm, $option->slugCode)
                            ->dropDownList(\yii\helpers\ArrayHelper::map($option->wikiMachineProperties, 'filterValue', 'value'), ['prompt' => _t('site.catalogps', 'Any')])->label($option->title) ?>
                    </div>
                <?php endforeach; ?>
                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'search')->textInput(); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    TsEquipmentSearch = {
        formFilterElement: 'category',
        changeLocation: function (form) {

            // get form elements data
            var inputs = form.getElementsByTagName("select");
            var formData = {};
            var sort = '';
            for (var i = 0; i < inputs.length; i++) {
                if (this.formFilterElement !== inputs[i].name && inputs[i].value !== '') {
                    formData[inputs[i].name] = inputs[i].value.replace('/', '_').toLowerCase();
                }

                if (inputs[i].name === 'sort') {
                    sort = inputs[i].value.toLowerCase();
                }
            }
            serialize = function (obj) {
                var str = [];
                for (var p in obj)
                    if (obj.hasOwnProperty(p)) {
                        if (obj[p])
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    }
                return str.join("&");
            }
            var searchVal = document.getElementById('search').value;
            query = serialize({sort: sort, search: searchVal});
            if (query) query = '?' + query;
            var filter = this.convertToFilterUrl(formData);
            var url = this.updateUrlFilter(filter) + query;
            url = url.replace(/ /g, "+").replace(/%20/g, "+");
            window.location.href = url;
        },

        convertToFilterUrl: function (formData) {
            ;
            var result = [];
            for (var i in formData) {
                result.push([i, formData[i]].join('-'));
            }
            return result.join('--');
        },

        updateUrlFilter: function (filter) {
            var newHref = window.location.href;
            newHref = newHref.split('?');
            newHref = newHref[0];
            newHref = newHref.split('/');
            var p = newHref.length - 1;
            if (p < 5) {
                p = 5;
            }
            newHref[p] = filter;
            return newHref.join('/');
        },

        changeCategoryLocation: function (newCatergoryPath) {
            var newHref = window.location.href;
            newHref = newHref.split('?');
            var currentUrlFilter = '';

            if (newHref[1]) {
                currentUrlFilter = '?' + newHref[1];
            }

            window.location.href = newCatergoryPath + currentUrlFilter;
        }
    };
</script>
