<?php
/** @var $item \common\models\WikiMachine */

use common\models\Category;
use common\models\EquipmentCategory;
use common\modules\equipments\EquipmentsModule;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\ps\PsFacade;

$this->title = $item->title;

if ($item->equipmentCategory) {
    $this->title = $item->title . ' - ' . $item->equipmentCategory->title;

    if ($item->equipmentCategory->parentCategory) {
        $this->title = $item->title . ' - ' . $item->equipmentCategory->title . ' - ' . $item->equipmentCategory->parentCategory->title;
    }
}

$categories = [];
if ($item->equipmentCategory) {
    $parentCategories = $item->equipmentCategory->parents(2)->withoutRoot()->orWhere(['id' => $item->equipmentCategory->id])->orderBy('lft')->all();
}

$breadcrumps                                                        = [];
$breadcrumps[_t('catalog.machine', 'Manufacturing Machines Guide')] = EquipmentsModule::URL_PREFIX;
$fullLink                                                           = EquipmentsModule::URL_PREFIX;
foreach ($parentCategories as $category) {
    $fullLink                      .= '/' . mb_strtolower($category->slug);
    $breadcrumps[$category->title] = $fullLink;
}
$breadcrumps[$item->title] = \Yii::$app->request->url;

?>
    <div class="over-nav-tabs-header">
        <div class="container">
            <ol class="breadcrumb m-t10 m-b0">
                <?php
                foreach ($breadcrumps as $breadcrumpText => $breadcrumpUrl) {
                    echo '<li>';
                    echo $breadcrumpUrl ? '<a href="' . HL($breadcrumpUrl) . '">' : '';
                    echo H($breadcrumpText);
                    echo $breadcrumpUrl ? '</a>' : '';
                    echo '</li>';
                }
                ?>
            </ol>
            <?php
            echo \frontend\widgets\BreadcrumbsSchemaWidget::widget([
                'breadcrumpsItems' => $breadcrumps
            ]);
            ?>
            <h1 class="m-t10"><?= $item->title; ?></h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="designer-card p-t0 p-l0 p-r0 p-b0 m-b30">
                    <div class="fotorama"
                         data-width="100%"
                         data-height="auto"
                         data-arrows="true"
                         data-click="true"
                         data-nav="thumbs"
                         data-thumbmargin="10"
                         data-thumbwidth="80px"
                         data-thumbheight="60px"
                         data-allowfullscreen="true">
                        <?php
                        if ($item->mainPhotoFile) {
                            echo sprintf(
                                '<img src="%s" alt="%s">',
                                HL($item->mainPhotoFile->getFileUrl()),
                                \H($item->title . ' #' . $item->mainPhotoFile->getFileName())
                            );
                        }
                        foreach ($item->photoFiles as $k => $photoFile):
                            $img = $photoFile->getFileUrl();
                            $alt = $item->title . ' #' . $photoFile->getFileName();
                            echo sprintf(
                                '<img src="%s" alt="%s">',
                                HL($img),
                                \H($alt)
                            );
                            ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="machines-share p-l0 p-r0 m-b30">
                    <h4 class="machines-share__title"><?= _t('public.store', 'Share') ?></h4>
                    <?php
                    echo \frontend\widgets\SocialShareWidget::widget(['showHtmlcode' => false, 'imgUrl' => $item->mainPhotoFile ? $item->mainPhotoFile->getFileUrl() : '']); ?>
                </div>
            </div>
            <div class="col-sm-8 wide-padding wide-padding--left">
                <div class="m-b20">
                    <?php
                    foreach ($item->wikiMachineProperties as $wikiMachineProperty) {
                        if (!$wikiMachineProperty->value) {
                            continue;
                        }
                        ?>
                        <span class="m-r10">
                         <strong><?= $wikiMachineProperty->getTitle(); ?></strong>
                        </span>
                        <?= $wikiMachineProperty->value ?> <br>
                    <?php } ?>
                </div>

                <?php
                if (count($item->files) > 0) {
                    ?>
                    <p class="m-b0"><strong><?= _t('catalog.machine', 'Documents'); ?></strong></p>
                    <ul class="list-unstyled m-b30">
                        <?php
                        foreach ($item->files as $file):
                            $fileSize = $file->size;
                            $printerFileUrl = $file->getFileUrl();
                            $title = $file->getFileName();
                            ?>
                            <li>
                                <a href="<?= $printerFileUrl; ?>"><span class="tsi tsi-doc m-r10"></span><?= $title; ?> <?php if ($fileSize) {
                                        echo "($fileSize)";
                                    } ?></a>
                            </li>
                        <?php endforeach; ?>

                    </ul>
                <?php } ?>


                <h2><?php echo _t('catalog.machine', 'About {title}', ['title' => $item->title]); ?></h2>
                <p>
                    <?php echo $item->description; ?>
                </p>
                <br/>
                <?= \common\modules\comments\widgets\Comments::widget(['model' => 'wikiMachine', 'model_id' => $item->id]); ?>
            </div>
        </div>
    </div>

<?= $this->render('initSwiper.php'); ?>