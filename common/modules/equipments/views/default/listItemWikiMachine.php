<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.12.17
 * Time: 16:05
 */

/* @var $model \common\models\WikiMachine */

/* @var $wikiMachineProperty WikiMachineProperty */

use common\models\WikiMachineProperty;
use common\modules\equipments\EquipmentsModule;
use frontend\components\image\ImageHtmlHelper;

$wikiMachine = $model;
$category = $wikiMachine->equipmentCategory;
$categorySlug = $category->slug;
$urlItem = \common\modules\equipments\helpers\WikiMachineUrlHelper::viewWikiMachine($wikiMachine);
?>
<div class="responsive-container">
    <div class="designer-card designer-card--ps-cat">
        <div class="designer-card__mach">
            <?php
            if ($wikiMachine->mainPhotoFile) {
                $mainPhotoFile = ImageHtmlHelper::getThumbUrlForFile($wikiMachine->mainPhotoFile,120, 80);
                ?>
                <a class="designer-card__mach-pic" href="<?= $urlItem; ?>" target="_blank">
                    <img src="<?= $mainPhotoFile ?>" alt="<?= H($wikiMachine->title); ?>" align="left"/>
                </a>
                <?php
            }
            ?>
            <?php
            ?>
            <div class="designer-card__mach-data">
                <h4 class="designer-card__mach-title">
                    <a href="<?= $urlItem; ?>">
                        <?= $model->title; ?>
                    </a>
                </h4>
                <?php
                foreach ($wikiMachine->getWikiMachineProperties()->onlyTop()->all() as $wikiMachineProperty) {
                    ?>
                    <div class="designer-card__data">
                        <span class="designer-card__data-label"><?= $wikiMachineProperty->title ?>:</span>
                        <?= $wikiMachineProperty->value; ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="designer-card__ps-pics">
            <?php if ($wikiMachine->photoFiles) { ?>
                <div class="designer-card__ps-portfolio swiper-container swiper-container-horizontal" style="cursor: -webkit-grab;">
                    <div class="swiper-wrapper">
                        <?php
                        foreach ($wikiMachine->photoFiles as $k => $photoFile) {
                            $img = $photoFile->getFileUrl();
                            $alt = $wikiMachine->title . ' #' . $photoFile->getFileName();
                            $imgThumb = ImageHtmlHelper::getThumbUrlForFile($photoFile, 160, 90);
                            echo sprintf(
                                '<a class="designer-card__ps-portfolio-item swiper-slide" href="%s" data-lightbox="%s"><img src="%s" alt="%s"></a>',
                                $img,
                                'm' . $wikiMachine->id,
                                $imgThumb,
                                H($alt)
                            );
                        }
                        ?>
                    </div>
                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;">
                        <div class="swiper-scrollbar-drag" style="width: 0px;"></div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="designer-card__ps-pics-empty">
                    <?= _t('site.catalog', 'No images'); ?>
                </div>
            <?php } ?>
        </div>

        <?php
        foreach ($wikiMachine->getWikiMachineProperties()->onlyBottom()->all() as $wikiMachineProperty) {
            ?>
            <div class="designer-card__data">
                <span class="designer-card__data-label"><?= $wikiMachineProperty->title ?>:</span>
                <?= $wikiMachineProperty->value; ?>
            </div>
            <?php
        }
        ?>

        <div class="designer-card__about">
            <?= yii\helpers\StringHelper::truncateWords(strip_tags($wikiMachine->description), 10); ?>
            <?php if (!empty($wikiMachine->description)): ?>
                <?= \yii\helpers\Html::a(_t('site.ps', 'Read more'), $urlItem); ?>
            <?php endif; ?>
        </div>

    </div>
</div>
