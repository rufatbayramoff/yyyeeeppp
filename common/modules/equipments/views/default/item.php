<?php
/** @var $item \common\models\Printer */

use common\modules\equipments\EquipmentsModule;
use frontend\components\image\ImageHtmlHelper;
use frontend\modules\mybusiness\components\PrinterReviewService;
use frontend\widgets\SwipeGalleryWidget;

if (empty($this->title)) {
    $this->title = $item->title;
}
if (!empty($item->category)) {
    $this->title = $item->title . ' - ' . $item->category->title;
    if (!empty($item->category->parent)) {
        $this->title = $item->title . ' - ' . $item->category->title . ' - ' . $item->category->parent->title;
    }
}
$headerTitle = $item->title;
if ($this->context->seo) {
    if (!empty($this->context->seo->header)) {
        $headerTitle = $this->context->seo->header;
    }

}
$categories = [];
if ($item->equipmentCategory) {
    $parentCategories = $item->equipmentCategory->parents(2)->withoutRoot()->orWhere(['id' => $item->equipmentCategory->id])->orderBy('lft')->all();
}
$printedPictures = $item->getPrintedImages();

$breadcrumps                                                        = [];
$breadcrumps[_t('catalog.machine', 'Manufacturing Machines Guide')] = EquipmentsModule::URL_PREFIX;
$fullLink                                                           = EquipmentsModule::URL_PREFIX;
foreach ($parentCategories as $category) {
    $fullLink                      .= '/' . mb_strtolower($category->slug);
    $breadcrumps[$category->title] = $fullLink;
}
$breadcrumps[$item->title] = \Yii::$app->request->url;
?>

<div class="over-nav-tabs-header">
    <div class="container container--wide">
        <ol class="breadcrumb m-t10 m-b0">
            <?php
            foreach ($breadcrumps as $breadcrumpText => $breadcrumpUrl) {
                echo '<li>';
                echo $breadcrumpUrl ? '<a href="' . HL($breadcrumpUrl) . '">' : '';
                echo H($breadcrumpText);
                echo $breadcrumpUrl ? '</a>' : '';
                echo '</li>';
            }
            ?>
        </ol>
        <?php
        echo \frontend\widgets\BreadcrumbsSchemaWidget::widget([
            'breadcrumpsItems' => $breadcrumps
        ]);
        ?>
        <h1 class="m-t10"><?= $headerTitle; ?></h1>
        <?php
        if (!empty($this->context->seo->header_text)):
            echo '<p>' . $this->context->seo->header_text . '</p>';
        endif; ?>
    </div>
</div>

<div class="container container--wide">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-10">

            <div class="row">
                <div class="col-sm-5">
                    <div class="designer-card p-t0 p-l0 p-r0 p-b0 m-b30" style="overflow: hidden;">
                        <div class="fotorama"
                             data-width="100%"
                             data-height="auto"
                             data-arrows="true"
                             data-click="true"
                             data-nav="thumbs"
                             data-thumbmargin="10"
                             data-thumbwidth="80px"
                             data-thumbheight="60px"
                             data-allowfullscreen="true">
                            <?php
                            foreach ($item->getImages() as $k => $picture):
                                $img      = $picture->url;
                                $alt      = $item->title . ' #' . $k;
                                $imgThumb = ImageHtmlHelper::getThumbUrl($img, 160, 90);
                                echo sprintf(
                                    '<img src="%s" alt="%s">',
                                    htmlentities($img),
                                    \H($alt)
                                );

                                ?>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <div class="designer-card product-page-info">
                        <h3 class="designer-card__title"><?= _t('catalog.machine', 'Properties'); ?></h3>
                        <table class="form-table form-table--top-label m-b0">
                            <tbody>
                            <?php
                            $properties = \frontend\models\ps\PsFacade::properties($item->id)->getProperties();
                            foreach ($properties as $propertyId => $propertyValue) {
                                if (empty($propertyValue['value'])) {
                                    continue;
                                }
                                ?>
                                <tr class="form-table__row">
                                    <td class="form-table__label">
                                        <b><?php echo $propertyValue['title']; ?></b>
                                    </td>
                                    <td class="form-table__data">
                                        <?php echo (isset($propertyValue['value'])) ? $propertyValue['value'] : ''; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                        <?php
                        $printerFiles = $item->getPrinterFiles();
                        if (count($printerFiles) > 0): ?>
                            <div class="row">
                                <hr class="m-t10 m-b10">
                            </div>
                            <h4 class="m-t0 m-b5"><?= _t('catalog.machine', 'Documents'); ?></h4>
                            <ul class="list-unstyled m-b0">
                                <?php
                                foreach ($printerFiles as $k => $printerFile):
                                    $fileSize = null;
                                    $printerFileUrl = $printerFile->getStaticUrl();
                                    $title = $printerFile->filename;
                                    ?>
                                    <li>
                                        <a style="display:flex;align-items:baseline;" href="<?= $printerFileUrl; ?>">
                                            <span class="tsi tsi-doc m-r10"></span>
                                            <?= $title; ?> <?php if ($fileSize) {
                                                echo "($fileSize)";
                                            } ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                    </div>

                    <!-- Reviews block -->
                    <div class="mach-guide-reviews hidden-xs">
                        <?php
                        $printerReview     = new \common\models\PrinterReview();
                        $printerReviewCols = $printerReview->getReviewLabels();
                        $printerReviews    = PrinterReviewService::getPrinterReviews($item);
                        $reviewsAvgs       = PrinterReviewService::calculateAvg($printerReviews);
                        if (count($printerReviews) > 0):
                            ?>
                            <h3>
                                <?= _t('site.printer', 'Reviews'); ?>

                                <?= \frontend\widgets\ReviewStarsWidget::widget(
                                    [
                                        'withSchema'     => true,
                                        'rating'         => $reviewsAvgs['summary'],
                                        'count'          => count($printerReviews),
                                        'itemTitle'   => $item->firm,
                                        'itemLogoUrl' => $item->getMainImageUrl()
                                    ]); ?>

                                <span style="display:inline-block;vertical-align:middle;border-radius:5px;padding:4px 10px;color:white;margin:0 10px;background:<?= PrinterReviewService::getColorByRating($reviewsAvgs['summary']); ?>">
                                <?= sprintf("%01.1f", $reviewsAvgs['summary']); ?>
                            </span>

                                <small style="display:inline-block;vertical-align:middle;">
                                    <?= _t('site.printer', '{num} reviews', ['num' => count($printerReviews)]); ?>
                                </small>
                            </h3>

                            <div class="row m-t15 m-b15">
                                <div class="col-xs-6 col-lg-4">
                                    <?php $i = 0;
                                    foreach ($printerReviewCols as $code => $title): $i++; ?>
                                        <div class="m-b10">
                                            <b><?= (H($title)) ?></b>

                                            <br/>

                                            <?= \frontend\widgets\ReviewStarsWidget::widget(
                                                [
                                                    'rating' => $reviewsAvgs[$code] ?? 5,
                                                ]
                                            ); ?>

                                        </div>
                                        <?php
                                        if ($i > 0 && $i % 1 === 0) {
                                            echo "</div><div class='col-xs-6 col-lg-4'>";
                                        }
                                    endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($printerReviews): ?>
                            <a href="#collapseReviews1" class="ps-profile-model__show-parts m-b15 collapsed" role="button"
                               data-toggle="collapse" aria-expanded="false"
                               aria-controls="collapseReviews1">
                            <span class="if-collapsed">
                                <?= _t('site.model3dViewedState', 'Show reviews') ?>
                            </span>
                                <span class="if-not-collapsed">
                                <?= _t('site.model3dViewedState', 'Hide reviews') ?>
                            </span>
                            </a>
                            <div class="collapse" id="collapseReviews1">
                                <?php foreach ($printerReviews as $review):
                                    /** @var \common\models\PrinterReview $review */
                                    ?>
                                    <div class="ps-profile-user-review">
                                        <div class="ps-profile-user-review__user-bar">
                                            <div class="ps-profile-user-review__user-info">
                                                <?php
                                                if ($review->user->allowViewPublicProfile() && $review->user->company) {
                                                    ?>
                                                    <a href="<?= $review->user->company->getPublicCompanyLink(); ?>"
                                                       class="ps-profile-user-review__user-avatar">
                                                        <img src="<?= $review->user->company->getCompanyLogo(false, 40) ?: '/static/images/defaultPrinter.png'; ?>"/>
                                                    </a>
                                                    <a href="<?= $review->user->company->getPublicCompanyLink() ?>"
                                                       class="ps-profile-user-review__user-name">
                                                        <?= \H($review->user->company->title); ?>
                                                    </a>
                                                    <?php
                                                } elseif ($review->user->isDeleted()) {
                                                    ?>
                                                    <div class="ps-profile-user-review__user-name">
                                                        <?= _t('site.common', 'Deleted'); ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="ps-profile-user-review__user-date">
                                                <?= Yii::$app->formatter->asDate($review->created_at) ?>
                                            </div>
                                            <div class="ps-profile-user-review__user-rate">
                                                <?= \frontend\widgets\ReviewStarsWidget::widget(
                                                    [
                                                        'rating' => $review->getRating(),
                                                    ]
                                                ); ?>
                                            </div>
                                        </div>
                                        <?php if ($review->comments): ?>
                                            <div class="ps-profile-user-review__text">
                                                <?= nl2br(H($review->comments)) ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <!-- // Reviews block -->

                </div>

                <div class="col-sm-7 wide-padding wide-padding--left">

                    <?php /*
                        <img src="<?php echo $item->getMainImageUrl();?>" alt="<?=H($item->title);?>" class="img-responsive m-b20" style="width: 100px; border-radius:5px;">
                        */ ?>

                    <div class="designer-card product-page-info">
                        <table class="form-table form-table--top-label m-b0">
                            <tbody>
                            <tr class="form-table__row">
                                <td class="form-table__label">
                                    <b><?= _t('catalog.machine', 'Brand'); ?></b>
                                </td>
                                <td class="form-table__data">
                                    <a href="/machines/3d-printers/brand-<?= HL($item->firm); ?>"
                                       target="_blank"><?= $item->firm; ?></a>
                                </td>
                            </tr>
                            <tr class="form-table__row">
                                <td class="form-table__label">
                                    <b><?= _t('catalog.machine', 'Machine type'); ?></b>
                                </td>
                                <td class="form-table__data">
                                    3D Printer
                                </td>
                            </tr>
                            <tr class="form-table__row">
                                <td class="form-table__label">
                                    <b><a href="/guide" target="_blank"><?= _t('catalog.machine', 'Technology'); ?></a></b>
                                </td>
                                <td class="form-table__data">
                                    <?= $item->technology->title; ?>
                                </td>
                            </tr>
                            <tr class="form-table__row">
                                <td class="form-table__label">
                                    <b><a href="/materials" target="_blank"><?= _t('catalog.machine', 'Materials'); ?></a></b>
                                </td>
                                <td class="form-table__data">
                                    <?php
                                    $materialsAll = \yii\helpers\ArrayHelper::getColumn($item->materials, 'title');
                                    echo H(implode(', ', $materialsAll));
                                    ?>
                                </td>
                            </tr>
                            <tr class="form-table__row">
                                <td class="form-table__label">
                                    <b><?= _t('catalog.machine', 'Website'); ?></b>
                                </td>
                                <td class="form-table__data">
                                    <noindex>
                                        <a rel="nofollow" href="<?= $item->website; ?>"
                                           target="_blank"><?= $item->website; ?></a>
                                    </noindex>
                                </td>
                            </tr>
                            <tr class="form-table__row">
                                <td class="form-table__label">
                                    <b><?= _t('catalog.machine', 'Average Price'); ?></b>
                                </td>
                                <td class="form-table__data">
                                    <?php if (!empty($item->price)): ?>
                                        <?= displayAsCurrency($item->price, 'USD'); ?>
                                        <span data-toggle="tooltip" data-placement="top"
                                              data-original-title="<?= _t('catalog.machine', 'Prices may vary depending on the retailer and the country'); ?>"
                                              class="tsi tsi-warning-c"></span>
                                    <?php else: ?>
                                        <?= _t('catalog.machine', 'Contact manufacturer'); ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <!-- descr -->
                    <h2><?php echo _t('catalog.machine', 'About {title}', ['title' => $item->title]); ?></h2>
                    <p>
                        <?php echo $item->description; ?>
                    </p>
                    <?php if (!empty($printedPictures)): ?>
                        <div>
                            <h3 class="m-b0"><?= _t('site.catalog', '3D printed products made using the {title}', ['title' => $item->title]); ?></h3>
                            <div class="row">
                                <?=
                                SwipeGalleryWidget::widget([
                                    'files'            => $printedPictures,
                                    'thumbSize'        => [230, 180],
                                    'containerOptions' => ['class' => 'ps-pub-portfolio__slider'],
                                    'itemOptions'      => ['class' => 'ps-pub-portfolio__item'],
                                    'scrollbarOptions' => ['class' => 'ps-pub-portfolio__scrollbar'],
                                ]);
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- Reviews block -->
                    <div class="mach-guide-reviews visible-xs">
                        <?php
                        $printerReview     = new \common\models\PrinterReview();
                        $printerReviewCols = $printerReview->getReviewLabels();
                        $printerReviews    = PrinterReviewService::getPrinterReviews($item);
                        $reviewsAvgs       = PrinterReviewService::calculateAvg($printerReviews);
                        if (count($printerReviews) > 0):
                            ?>
                            <h3>
                                <?= _t('site.printer', 'Reviews'); ?>

                                <span style="display:inline-block;vertical-align:middle;border-radius:5px;padding:4px 10px;color:white;margin:0 10px;background:<?= PrinterReviewService::getColorByRating($reviewsAvgs['summary']); ?>">
                                <?= sprintf("%01.1f", $reviewsAvgs['summary']); ?>
                            </span>

                                <small style="display:inline-block;vertical-align:middle;">
                                    <?= _t('site.printer', '{num} reviews', ['num' => count($printerReviews)]); ?>
                                </small>
                            </h3>

                            <div class="row m-t15 m-b15">
                                <div class="col-xs-6 col-lg-4">
                                    <?php $i = 0;
                                    foreach ($printerReviewCols as $code => $title): $i++; ?>
                                        <div class="m-b10">
                                            <b><?= (H($title)) ?></b>

                                            <br/>

                                            <?= \frontend\widgets\ReviewStarsWidget::widget(
                                                [
                                                    'rating' => $reviewsAvgs[$code] ?? 5,
                                                ]
                                            ); ?>

                                        </div>
                                        <?php
                                        if ($i > 0 && $i % 1 === 0) {
                                            echo "</div><div class='col-xs-6 col-lg-4'>";
                                        }
                                    endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($printerReviews): ?>
                            <a href="#collapseReviews2" class="ps-profile-model__show-parts m-b15 collapsed" role="button"
                               data-toggle="collapse" aria-expanded="false"
                               aria-controls="collapseReviews2">
                            <span class="if-collapsed">
                                <?= _t('site.model3dViewedState', 'Show reviews') ?>
                            </span>
                                <span class="if-not-collapsed">
                                <?= _t('site.model3dViewedState', 'Hide reviews') ?>
                            </span>
                            </a>
                            <div class="collapse" id="collapseReviews2">
                                <?php foreach ($printerReviews as $review):
                                    /** @var \common\models\PrinterReview $review */
                                    ?>
                                    <div class="ps-profile-user-review">
                                        <div class="ps-profile-user-review__user-bar">
                                            <div class="ps-profile-user-review__user-info">
                                                <?php
                                                if ($review->user->allowViewPublicProfile() && $review->user->company) {
                                                    ?>
                                                    <a href="<?= $review->user->company->getPublicCompanyLink(); ?>"
                                                       class="ps-profile-user-review__user-avatar">
                                                        <img src="<?= $review->user->company->getCompanyLogo(false, 40) ?: '/static/images/defaultPrinter.png'; ?>"/>
                                                    </a>
                                                    <a href="<?= $review->user->company->getPublicCompanyLink() ?>"
                                                       class="ps-profile-user-review__user-name">
                                                        <?= \H($review->user->company->title); ?>
                                                    </a>
                                                    <?php
                                                } elseif ($review->user->isDeleted()) {
                                                    ?>
                                                    <div class="ps-profile-user-review__user-name">
                                                        <?= _t('site.common', 'Deleted'); ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="ps-profile-user-review__user-date">
                                                <?= Yii::$app->formatter->asDate($review->created_at) ?>
                                            </div>
                                            <div class="ps-profile-user-review__user-rate">
                                                <?= \frontend\widgets\ReviewStarsWidget::widget(
                                                    [
                                                        'rating' => $review->getRating(),
                                                    ]
                                                ); ?>
                                            </div>
                                        </div>
                                        <?php if ($review->comments): ?>
                                            <div class="ps-profile-user-review__text">
                                                <?= nl2br(H($review->comments)) ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <!-- // Reviews block -->

                    <div class="p-l0 p-r0 p-0">
                        <div class="tsadelement tsadelement--horizontal">
                            <div id="amzn-assoc-ad-36ecd97a-d7c3-42bd-b069-67da508d2ac4"></div>
                            <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=36ecd97a-d7c3-42bd-b069-67da508d2ac4"></script>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-lg-2 p-l0">
            <div class="tsadelement">
                <div id="amzn-assoc-ad-a62ceb71-6f15-49a6-93a9-810eeb3bdbc2"></div>
                <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=a62ceb71-6f15-49a6-93a9-810eeb3bdbc2"></script>
            </div>
        </div>

    </div>

</div>

<?= $this->render('initSwiper.php'); ?>
