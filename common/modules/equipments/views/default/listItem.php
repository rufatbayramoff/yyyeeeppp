<?php
/* @var $model \common\models\Printer */

use common\modules\equipments\EquipmentsModule;
use frontend\components\image\ImageHtmlHelper;
$categorySlug = !empty($model->category_id)?$model->category->getSlugUrl():'all';
$urlItem =  $model->getPublicPageUrl();


?>
<div class="responsive-container">
    <div class="designer-card designer-card--ps-cat">
        <div class="designer-card--mach-label label label-default">
            <?= _t('site.ps', '3D Printer'); ?>
        </div>

        <div class="designer-card__mach">
            <a class="designer-card__mach-pic" href="<?=$urlItem; ?>" target="_blank">
                <img src="<?php echo $model->getMainImageUrl();?>" alt="<?=H($model->title);?>" align="left">
            </a>
            <div class="designer-card__mach-data">
                <h4 class="designer-card__mach-title">
                    <a href="<?=$urlItem;?>">
                        <?=H($model->title);?>
                    </a>
                </h4>
                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Technology'); ?>:</span> <?php
                        echo H($model->technology->title);
                    ?>
                </div>
                <div class="designer-card__data m-b0">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Materials'); ?>:</span>
                    <?php
                    $materialsAll = \yii\helpers\ArrayHelper::getColumn($model->materials, 'title');
                    $materialsDisplay = array_splice($materialsAll, 0, min(count($materialsAll), 3));
                    echo implode(', ', $materialsDisplay);
                    if(count($materialsAll) > count($materialsDisplay)){
                        echo _t("catalog.machine", ' and +{num} materials', ['num'=>count($materialsAll) - count($materialsDisplay)]);
                    }
                    ?>

                </div>
            </div>
        </div>

        <div class="designer-card__ps-pics">
            <?php if($model->getImages()): ?>
            <div class="designer-card__ps-portfolio swiper-container swiper-container-horizontal" style="cursor: -webkit-grab;">
                <div class="swiper-wrapper">
                    <?php
                    foreach ($model->getImages() as $k=>$picture) {
                        $img = $picture->url;
                        $alt = $model->title . ' #' . $k;
                        $imgThumb = ImageHtmlHelper::getThumbUrl($img, 160, 90);
                        echo sprintf(
                            '<a class="designer-card__ps-portfolio-item swiper-slide" href="%s" data-lightbox="%s"><img src="%s" alt="%s"></a>',
                            $img, 'm'.$model->id, $imgThumb, \H($alt)
                        );
                    }
                    ?>
                </div>
                <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;"><div class="swiper-scrollbar-drag" style="width: 0px;"></div></div>
            </div>
            <?php else: ?>
                <div class="designer-card__ps-pics-empty">
                    <?=_t('site.catalog', 'No images'); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="designer-card__data">
            <span class="designer-card__data-label"><?= _t('site.ps', 'Brand'); ?>:</span> <?=H($model->firm);?>
        </div>

        <div class="designer-card__about">
            <?= H(yii\helpers\StringHelper::truncateWords(strip_tags(html_entity_decode($model->description)), 10)); ?>
            <?php if(!empty($model->description)): ?>
            <?=\yii\helpers\Html::a(_t('site.ps', 'Read more'), $urlItem);?>
            <?php endif; ?>
        </div>

        <div class="row">
            <div class="col-sm-12">

                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Average price'); ?>:</span>
                    <?php if(!empty($model->price)): ?>
                    <?=displayAsCurrency($model->price, 'USD');?>
                        <span data-toggle="tooltip" data-placement="top" data-original-title="<?=_t('catalog.machine', 'Prices may vary depending on the retailer and the country');?>" class="tsi tsi-warning-c"></span>
                        <?php else:?><?=_t('catalog.machine', 'Contact manufacturer');?>
                    <?php endif; ?>
                </div>

                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Build volume'); ?>:</span>
                    <?=H($model->build_volume); ?>
                </div>
            </div>
        </div>

    </div>
</div>