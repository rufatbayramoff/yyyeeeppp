<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.12.17
 * Time: 11:29
 */

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

?>

<script>
    <?php $this->beginBlock('js1', false); ?>
    //init PS card portfolio pics
    var swiperMachCards = new Swiper('.designer-card__ps-portfolio', {
        scrollbar: '.designer-card__ps-portfolio-scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });
    var swiperPubPortfolio = new Swiper('.ps-pub-portfolio__slider', {
        scrollbar: '.ps-pub-portfolio__scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });
    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>
