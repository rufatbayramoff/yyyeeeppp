<?php
/**
 * User: nabi
 */

namespace common\modules\equipments\services;


use common\models\PrinterMaterial;
use common\models\PrinterTechnology;
use common\models\PrinterToMaterial;
use common\modules\catalogPs\repositories\PrinterMaterialGroupRepository;
use common\modules\catalogPs\repositories\PrinterMaterialRepository;
use common\modules\catalogPs\repositories\PrinterTechnologyRepository;
use common\modules\catalogPs\repositories\PrinterToMaterialRepository;
use common\modules\catalogPs\repositories\PsMachineDeliveryRepository;
use common\modules\catalogPs\repositories\PsPrinterMaterialRepository;
use common\modules\catalogPs\repositories\PsRepository;
use common\modules\catalogPs\repositories\UsageGroupsRepository;

class EquipmentSearchService
{

    /**
     * @var PsPrinterMaterialRepository
     */
    public static $psMaterialsRepo;

    /**
     * @var PsMachineDeliveryRepository
     */
    public static $deliveryRepo;

    /**
     * @var PsRepository
     */
    public static $psRepoStatic;

    /**
     * @var PrinterTechnologyRepository
     */
    public $techRepo;

    /**
     * @var UsageGroupsRepository
     */
    public $usageRepo;

    /**
     * @var PsRepository
     */
    public $psRepo;

    /**
     * @var PrinterMaterialRepository
     */
    public $materialRepo;

    /**
     * @var PrinterMaterialGroupRepository
     */
    public $materialGroupRepo;

    public $printerToMaterialRepo;

    public function injectDependencies(
        PrinterTechnologyRepository $techRepo,
        UsageGroupsRepository $usageRepo,
        PrinterMaterialRepository $materialRepo,
        PrinterToMaterialRepository $printerToMaterialRepo,
        PrinterMaterialGroupRepository $materialGroupRepo
    )
    {
        $this->techRepo = $techRepo;
        $this->usageRepo = $usageRepo;
        $this->materialRepo = $materialRepo;
        $this->materialGroupRepo = $materialGroupRepo;
        $this->printerToMaterialRepo = $printerToMaterialRepo;
    }

    /**
     * @param array $array
     * @return string
     */
    public function convertToFilterUrl(array $array)
    {
        $array = array_filter($array);
        $result = [];
        foreach($array as $k=>$v){
            $result[] = $k."-".$v;
        }
        return join("--", $result);
    }

    /**
     * @param $technologyCode
     * @return \common\models\PrinterTechnology|null
     */
    public function getTechnology($technologyCode)
    {
        $technology = $this->techRepo->getByCode($technologyCode);
        if (!$technology) {
            return null;
        }
        return $technology;
    }

    /**
     * @param $technologyId
     * @return \common\models\PrinterTechnology|null
     */
    public function getTechnologyById($technologyId)
    {
        $technology = $this->techRepo->getModelById($technologyId);
        if (!$technology) {
            return null;
        }
        return $technology;
    }
    /**
     * @param $usageCode
     * @return array
     */
    public function getUsage($usageCode)
    {
        return $this->usageRepo->getByCode($usageCode);
    }

    /**
     * @param $technologyCode
     * @return array
     */
    public function getUsageTypes($technologyCode)
    {
        if (!empty($technologyCode)) {
            $technology = $this->getTechnology($technologyCode);
            $usageTypes = $this->usageRepo->getAllByTechnology($technology->id);
        } else {
            $usageTypes = $this->usageRepo->getAll();
        }
        return $usageTypes;
    }

    /**
     * @param $technologyCode
     * @return array
     */
    public function getUsagesByTechnology($technologyCode)
    {
        $technology = $this->techRepo->getByCode($technologyCode);
        $usageTypes = $this->usageRepo->getAllByTechnology($technology->id);
        return $usageTypes;
    }

    /**
     * @return PrinterTechnology[]
     */
    public function getTechnologies()
    {
        return $this->techRepo->getAllModels();
    }


    /**
     * @param $materialSlug
     * @return PrinterMaterial
     */
    public function getMaterial($materialSlug)
    {
        return $this->materialRepo->getBySlug($materialSlug);
    }

    /**
     * @param $usageCode
     * @param $technologyCode
     *
     * @return PrinterMaterial[]
     */
    public function getMaterials($usageCode = null, $technologyCode = null)
    {
        if (!empty($usageCode)) {
            $usage = $this->getUsage($usageCode);
            $materials = $this->getMaterialsByUsage($usage);
        } else {
            if (!empty($technologyCode)) {
                $technology = $this->getTechnology($technologyCode);
                $materials = $this->getMaterialsByTechnology($technology);
            } else {
                $materials = $this->materialRepo->getAllModels();
            }
        }
        return $materials;
    }

    public function getMaterialsByUsage($usage)
    {
        $materialGroups = [];
        foreach($usage['groups'] as $groupCode){
            $materialGroups[] = $this->materialGroupRepo->getByCode($groupCode);
        }
        if(empty($materialGroups)){
            return [];
        }
        $materials = $this->materialRepo->getModelsByGroups($materialGroups);

        return $materials;
    }

    public function getMaterialsByTechnology(PrinterTechnology $technology)
    {
        $materialIds = PrinterToMaterial::find()->select('material_id')
            ->joinWith('printer')
            ->andWhere(['printer.technology_id' => $technology->id, 'printer.is_active'=>1])
            ->groupBy('printer_to_material.material_id')->column();

        return $this->materialRepo->getModelsById($materialIds);
    }

    /**
     * @param $filter
     * @param $getQueryParams
     * @return array
     */
    public function parseRequestParams($filter = null, $getQueryParams): array
    {
        if(empty($filter)) {
            return $getQueryParams;
        }
        return array_merge($this->parseFilterUrl($filter), $getQueryParams);
    }

    /**
     * converts string <key1-val1--key2-val2--...> to array [k1=>v1,k2=>v2..]
     *
     * @param string|null $filter
     * @return array
     */
    public function parseFilterUrl(?string $filter){
        $result = [];
        $parts = explode("--", $filter);
        $parts = array_filter($parts);
        if(count($parts)===0){
            return $result;
        }
        foreach($parts as $part){
            $partRow = explode('-', $part, 2);
            if(count($partRow)===1){
                return []; // not valid filter
            }
            $result[$partRow[0]] = $partRow[1];
        }
        return $result;
    }
}