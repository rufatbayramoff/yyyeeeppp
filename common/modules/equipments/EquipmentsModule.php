<?php
/**
 * User: nabi
 */

namespace common\modules\equipments;


use yii\base\Module;

class EquipmentsModule extends Module
{
    const URL_PREFIX = '/machines';
    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\equipments\controllers';


}