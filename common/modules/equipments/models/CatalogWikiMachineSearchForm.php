<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.12.17
 * Time: 13:14
 */

namespace common\modules\equipments\models;

use common\components\ActiveQuery;
use common\components\ArrayHelper;
use common\components\BaseForm;
use common\models\EquipmentCategory;
use common\models\WikiMachine;
use common\models\WikiMachineAvailableProperty;
use common\modules\equipments\services\EquipmentSearchService;
use yii\data\ActiveDataProvider;
use yii\db\Expression;


/**
 * @property WikiMachineAvailableProperty[] $options
 * */
class CatalogWikiMachineSearchForm extends BaseForm
{
    public $search;

    /**
     * @var EquipmentSearchService
     */
    protected $searchService;

    /**
     * @var int
     */
    public $perPage = 15;

    /**
     * @var string
     */
    public $filter;


    /**
     * @var EquipmentCategory[]
     */
    public $categories;

    /**
     * @var EquipmentCategory
     */
    public $category;

    public $fields = [];


    public function rules()
    {
        return [
            [['categories', 'search'], 'safe'],
            [['perPage'], 'number'],
        ];
    }

    public function injectDependencies(EquipmentSearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    public function formName()
    {
        return '';
    }

    public function attributeLabels()
    {
        return [
            'perPage' => _t('site.ps', 'Per page'),
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);
        $errors = $this->validateSearch();
        foreach ($errors as $field => $error) {
            $this->{$field} = null;
            $this->addError($field, $error);
        }
        return !$this->hasErrors();
    }

    /**
     * @return array
     */
    private function validateSearch()
    {
        $errors = [];
        if (!$this->categories) {
            $errors['categories'] = 'Not valid';
            return $errors;
        }
        return $errors;
    }

    /**
     * @param null $filter
     * @param $getQueryParams
     * @return array
     */
    public function parseRequestParams($filter = null, $getQueryParams): array
    {
        return $this->searchService->parseRequestParams($filter, $getQueryParams);
    }

    /**
     * @return string
     */
    public function getValidUrl()
    {
        $validUrlParts = array_merge(['machines'], ArrayHelper::getColumn($this->categories, 'slug'));
        return implode('/', $validUrlParts);
    }

    /**
     * @return ActiveDataProvider
     */
    public function getDataProvider(): ActiveDataProvider
    {
        $query = WikiMachine::find();
        $this->filteredDynamicFields($query);

        if ($lastCategory = $this->getLastCategory()) {
            $equipmentIds = ArrayHelper::getColumn(array_merge([$lastCategory], $lastCategory->children()->all()), 'id');
            $query->andWhere([WikiMachine::column('equipment_category_id') => $equipmentIds]);
        }
        if ($this->search) {
            $query->andWhere(
                [
                    'OR',
                    ['like', 'title', $this->search],
                ]
            );
        }

        return new ActiveDataProvider(
            [
                'query'      => $query,
                'pagination' => [
                    'defaultPageSize' => $this->perPage,
                    'pageSizeLimit'   => [1, 500]
                ],
            ]
        );
    }

    /**
     * @param ActiveQuery $activeQuery
     * @return void
     */
    protected function filteredDynamicFields(ActiveQuery $activeQuery): void
    {
        $filteredFields =  array_filter($this->fields, static function ($field){
            return $field['value'];
        });
        if(count($filteredFields) === 0) {
            return;
        }
        /** @var array{id: string, value: string} $property */
        foreach ($filteredFields as $tableName => $property) {
            $activeQuery->innerJoin([$tableName => 'wiki_machine_property'],"{$tableName}.wiki_machine_id=wiki_machine.id");
            $activeQuery->andWhere([
                'AND',
                ["{$tableName}.wiki_machine_available_property_id" => $property['id']],
                ['=',new Expression("REPLACE({$tableName}.value,'/','')"), $property['value']],
            ]);
        }
        $activeQuery->groupBy('wiki_machine.id');
    }

    /**
     * @return EquipmentCategory|null
     */
    public function getLastCategory()
    {
        if (!$this->categories) {
            return null;
        }
        return end($this->categories);
    }

    public function changeCategory(EquipmentCategory $category): void
    {
        $this->category = $category;
        foreach ($this->options as $option) {
            $this->fields[$option->slugCode] = ['id' => $option->id, 'value' => null];
        }

    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->category->wikiMachineAvailableProperties;
    }

    /**
     * @return array
     */
    public function safeAttributes()
    {
        return ArrayHelper::merge(parent::safeAttributes(), array_keys($this->fields));
    }

    public function __get($name)
    {
        if(array_key_exists($name,$this->fields)){
            return $this->fields[$name]['value'];
        }
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->fields)) {
            $this->fields[$name]['value'] = $value;
        } else {
            parent::__set($name, $value);
        }
    }


    public function __isset($name)
    {
        return isset($this->fields[$name]) || parent::__isset($name);
    }
}