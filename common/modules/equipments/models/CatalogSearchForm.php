<?php
/**
 * User: nabi
 */

namespace common\modules\equipments\models;


use common\components\ArrayHelper;
use common\components\BaseForm;
use common\models\EquipmentCategory;
use common\models\Printer;
use common\modules\catalogPs\components\CatalogFilter;
use common\modules\catalogPs\repositories\PsPrinterEntityRepository;
use common\modules\equipments\services\EquipmentSearchService;
use lib\geo\models\Location;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

class CatalogSearchForm extends BaseForm
{
    const SORT_PRICE          = 'price';
    const SORT_ID             = 'id';
    const SORT_TITLE          = 'title';
    const DEFAULT_CATEGORY    = '3d-printers';
    const DEFAULT_CATEGORY_ID = 2;

    public $category;
    public $technology;
    public $material;
    public $brand;
    public $search;

    /**
     * @var string
     */
    public $sort = 'id';

    /**
     * @var int
     */
    public $perPage = 15;

    /**
     * @var string
     */
    public $filter;

    /**
     * @var EquipmentSearchService
     */
    public $searchService;

    public function rules()
    {
        return [
            ['category', 'default', 'value' => self::DEFAULT_CATEGORY],
            [['category', 'technology', 'brand', 'search', 'filter', 'material', 'sort'], 'safe'],
            [['perPage'], 'number'],
            [['sort'], 'in', 'range' => [self::SORT_ID, self::SORT_PRICE, self::SORT_TITLE]]
        ];
    }

    public function formName()
    {
        return '';
    }

    public function injectDependencies(EquipmentSearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    public function attributeLabels()
    {
        return [
            'technology' => _t('site.ps', 'Technology'),
            'material'   => _t('site.ps', 'Material'),
            'brand'      => _t('site.ps', 'Brand'),
            'sort'       => _t('site.ps', 'Sort by'),
            'perPage'    => _t('site.ps', 'Per page'),
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);
        $errors = $this->validateSearch();
        foreach ($errors as $field => $error) {
            $this->{$field} = null;
            $this->addError($field, $error);
        }
        return !$this->hasErrors();
    }

    /**
     * @param $technologyCode
     * @param $materialCode
     *
     * @return array
     */
    private function validateSearch()
    {
        $errors         = [];
        $technologyCode = $this->technology;
        $materialCode   = $this->material;
        if (!empty($technologyCode)) {
            $technology = $this->searchService->getTechnology($technologyCode);
            if (!$technology) {
                $errors['technology'] = 'Not valid';
                return $errors;
            }
            if (!empty($this->brand)) {
                $brands = $this->getComboBrand();
                if (!in_array($this->brand, array_keys($brands))) {
                    $this->brand     = null;
                    $errors['brand'] = 'Not valid';
                    return $errors;
                }
            }
        }
        if (!empty($materialCode)) {
            $material = $this->searchService->getMaterial($materialCode);
            if (!$material) {
                $errors['material'] = 'Not valid';
                return $errors;
            }
        }
        if (empty($this->category)) {
            $errors['category'] = 'Not valid';
            return $errors;
        }
        if (!empty($materialCode) && (!empty($technologyCode))) {
            $materials = $this->getComboMaterials();
            if (!array_key_exists($materialCode, $materials)) {
                $errors['material'] = 'Not valid';
                return $errors;
            }
        }
        return $errors;
    }

    /**
     * @param $filter
     * @param $getQueryParams
     * @return array
     */
    public function parseRequestParams($filter = null, $getQueryParams): array
    {
        return $this->searchService->parseRequestParams($filter, $getQueryParams);
    }

    /**
     * @return string
     */
    public function getValidUrl()
    {
        if (empty($this->category)) {
            $this->category = self::DEFAULT_CATEGORY;
        }
        $validUrlParts = [
            '/machines',
            $this->category
        ];

        $filterUrl = $this->searchService->convertToFilterUrl(
            [
                'technology' => $this->technology,
                'material'   => $this->material,
                'brand'      => $this->brand
            ]
        );
        if ($filterUrl) {
            $validUrlParts[] = $filterUrl;
        }
        return join('/', $validUrlParts);
    }

    public function getTechnology()
    {
        $technology = $this->searchService->getTechnology($this->technology);
        return $technology;
    }

    public function getMaterial()
    {
        return $this->searchService->getMaterial($this->material);
    }


    /**
     * @return array
     */
    public function getComboSort()
    {
        return [
            ''               => _t('site.catalogps', 'default'), // default
            self::SORT_PRICE => _t('site.catalogps', 'Lowest Price'),
            self::SORT_TITLE => _t('site.catalogps', 'Title'),
        ];
    }

    public function getComboBrand()
    {
        $brands = Printer::find()->select('firm')->orderBy('firm ASC')->distinct();
        if (!empty($this->technology)) {
            $brands->andWhere(['printer.technology_id' => $this->getTechnology()->id]);
        }
        $brands = $brands->all();
        $result = [];
        foreach ($brands as $k => $v) {
            $firm             = trim($v->firm);
            $result[mb_strtolower($firm)] = $firm;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getComboTechnologies()
    {
        $technologyRecords = $this->searchService->getTechnologies(); // if filter by usage pass $this->usage
        $techs             = [];
        foreach ($technologyRecords as $tech) {
            $techs[mb_strtolower($tech->getTitleCode())] = $tech->getTitleCode();
        }
        return $techs;
    }

    /**
     * @return array
     */
    public function getComboUsage()
    {
        $usageTypes = $this->searchService->getUsageTypes($this->technology);
        $result     = [];
        foreach ($usageTypes as $usageType):
            $result[mb_strtolower($usageType['id'])] = $usageType['title'];
        endforeach;
        return $result;
    }

    /**
     * @return array
     */
    public function getComboMaterials()
    {
        $materials = $this->searchService->getMaterials(null, $this->technology);
        usort(
            $materials,
            function ($item, $item2) {
                return strnatcmp($item->title, $item2->title);
            }
        );

        $result = [];
        foreach ($materials as $material):
            if (!$material->is_active) {
                continue;
            }
            $result[mb_strtolower($material->getSlugifyFilament())] = $material->title;
        endforeach;
        return $result;
    }

    /**
     * @return ArrayDataProvider
     */
    public function getDataProvider()
    {
        $where = ['printer.is_active' => 1];
        if ($this->category) {
            $where['equipment_category_id'] = $this->getCategory()->id;
        }
        $items      = Printer::find()->joinWith(['printerToMaterials', 'technology'])
            ->with('materials.printerToMaterials')->where($where)->groupBy('printer.id');
        $excludeIds = \Yii::$app->setting->get('site.excludeMachinePrinter', "0");
        if ($excludeIds) {
            $items->andWhere(['NOT IN', 'printer.id', explode(",", $excludeIds)]);
        }
        if (!empty($this->technology)) {
            $items->andWhere(['printer.technology_id' => $this->getTechnology()->id]);
        }
        if (!empty($this->material)) {
            $items->andWhere(['printer_to_material.material_id' => $this->getMaterial()->id]);
        }
        if (!empty($this->brand)) {
            $items->andWhere(['printer.firm' => $this->brand]);
        }
        if (!empty($this->search)) {
            $items->andFilterWhere([
                'OR',
                ['like', 'printer.title', $this->search],
                ['like', 'printer.description', $this->search],
            ]);
        }
        if (!empty($this->sort)) {
            $items->orderBy('printer.`' . $this->sort . '` ASC');
        }
        $dataProvider = new ActiveDataProvider(
            [
                'query'      => $items,
                'pagination' => [
                    'defaultPageSize' => $this->perPage,
                    'pageSizeLimit'   => [1, 500]
                ],
            ]
        );

        return $dataProvider;
    }

    public function getCategoriesList()
    {
        $categories = EquipmentCategory::find()->withoutRoot()->orderBy('lft')->all();
        $categories = ArrayHelper::map($categories, 'slug', 'title');
        return $categories;
    }

    public function getCategory()
    {
        if (empty($this->category)) {
            return null;
        }
        $categoriesPath = explode('_', $this->category);
        // TODO: make full path search
        $lastCategory    = end($categoriesPath);
        $defaultCategory = EquipmentCategory::findByPk(self::DEFAULT_CATEGORY_ID);
        if ($lastCategory == self::DEFAULT_CATEGORY) {
            return $defaultCategory;
        }
        return $defaultCategory->children()->andWhere(['slug' => $lastCategory])->one();
    }
}