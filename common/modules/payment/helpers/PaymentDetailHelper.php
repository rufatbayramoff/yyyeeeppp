<?php

namespace common\modules\payment\helpers;

use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentTransaction;
use common\modules\payment\gateways\vendors\BankPayoutGateway;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use common\modules\payment\services\PaymentStoreOrderService;
use frontend\models\user\UserFacade;
use lib\money\MoneyMath;
use Yii;

/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 12.11.18
 * Time: 11:32
 */
class PaymentDetailHelper extends \yii\base\BaseObject
{
    public static function getPriceInfo(PaymentDetail $model)
    {
        $invoice = $model->paymentDetailOperation->payment->paymentInvoice;

        if ($invoice && $model->isTypeTo()) {
            if ($invoice->getLinkedAffiliateAward() && UserFacade::isObjectOwner($invoice->getLinkedAffiliateAward()->affiliateSource)) {
                // Affiliate reward
                return _t('site.ps', 'Total order price {price1}. Service fee is {price2}.', [
                    'price1' => displayAsMoney($invoice->getAmountTotal()),
                    'price2' => displayAsMoney($invoice->getAmountPsFee()),
                ]);
            }
            $modelAward = $invoice->storeOrderAmount->getAmountModel3dWithRefund();
            if ($modelAward && $modelAward->getAmount() && $invoice->storeOrder->getFirstReplicaItem()) {
                $model3d = $invoice->storeOrder->getFirstReplicaItem()->getSourceModel3d();
                if (UserFacade::isObjectOwner($model3d)) {
                    $fee = $invoice->storeOrderAmount->getAmountModel3dFee();
                    if ($fee && $fee->getAmount()) {
                        return _t('site.ps', 'Total order model price {price1}. Treatstock fee is {price2}.', [
                            'price1' => displayAsMoney($modelAward),
                            'price2' => displayAsMoney($fee),
                        ]);
                    } else {
                        return _t('site.ps', 'Total order model price {price1}.', [
                            'price1' => displayAsMoney($modelAward)
                        ]);
                    }
                }
            }

            $psFee             = $invoice->getCompanyFee();
            $manufacturerMoney = $invoice->getManufacturerAward();
            $bonusFromTs       = $invoice->getAmountBonusAccruedFromTs();
            $bonusFromPs       = $invoice->getAmountBonusAccruedFromPs();
            $bonusText         = '';

            if ($bonusFromTs && $bonusFromTs->getAmount()) {
                $bonusText .= _t('site.ps', ' Bonus from Treatstock {price}.', ['price' => $bonusFromTs]);
            }
            if ($bonusFromPs && $bonusFromPs->getAmount()) {
                $bonusText .= _t('site.ps', ' Bonus from manufacture {price}.', ['price' => $bonusFromPs]);
            }

            if ($manufacturerMoney && $psFee) {
                /** @var PaymentStoreOrderService $paymentStoreOrderService */
                $paymentStoreOrderService = Yii::createObject(PaymentStoreOrderService::class);
                $psShippingFeeDetail      = $paymentStoreOrderService->getPsShippingFeePaymentDetail($invoice); // If shipping by Ps

                if ($psShippingFeeDetail) {
                    return _t('site.ps', 'Manufacturing fee for the customer is {price1}, including packaging fee. Service fee is {price2}. Shipping price {price4}.Therefore, your manufacturing income is {price3}', [
                            'price1' => displayAsMoney($invoice->getAmountTotal()),
                            'price2' => displayAsMoney($psFee),
                            'price3' => displayAsMoney($manufacturerMoney),
                            'price4' => displayAsMoney($psShippingFeeDetail->getMoneyAmount())
                        ]) . $bonusText;
                }

                return _t('site.ps', 'Manufacturing fee for the customer is {price1}, including packaging fee. Service fee is {price2}. Therefore, your manufacturing income is {price3}', [
                        'price1' => displayAsMoney($invoice->getAmountTotal()),
                        'price2' => displayAsMoney($psFee),
                        'price3' => displayAsMoney($manufacturerMoney)
                    ]) . $bonusText;
            }
        }
        return null;
    }

    /**
     * @param PaymentDetail $model
     *
     * @return string
     */
    public static function getFullDescription(PaymentDetail $model): string
    {
        $desc = H($model->description);

        if ($model->type === PaymentDetail::TYPE_AWARD_PRINT) {
            $desc = _t('site.payments', 'Award for printing.') . ' ' . H($model->description);
        }

        if ($model->type === PaymentDetail::TYPE_AWARD_MODEL) {
            $desc = _t('site.payments', 'Award for model order.') . ' ' . H($model->description);
        }

        if ($model->type === PaymentDetail::TYPE_PAYOUT) {
            if ($model->paymentTransaction) {
                if ($model->paymentTransaction->vendor === PaymentTransaction::VENDOR_BANK_PAYOUT) {
                    $desc = _t('site.payments', 'Amount credited to Bank account. ');
                    $desc .= $model->paymentTransaction->details[BankPayoutGateway::ACCOUNT_DETAILS] ?
                        '<br/>' . _t('site.payments', 'Account details') . ': ' . H($model->paymentTransaction->details[BankPayoutGateway::ACCOUNT_DETAILS]) :
                        '';
                } else {
                    $desc = _t('site.payments', 'Amount credited to PayPal. ');
                }
                if ($model->paymentTransaction->transaction_id !== '-') {
                    $desc = $desc . "<br />" . sprintf(
                            "<b>%s</b>: %s <b>%s</b>: %s",
                            _t('site.payments', 'Status'),
                            H($model->paymentTransaction->status),
                            _t('site.payments', "Transaction Id"),
                            H($model->paymentTransaction->transaction_id)
                        );
                } else {
                    $desc = $desc . "\n" . sprintf(
                            "%s: %s",
                            _t('site.payments', "Status"),
                            H($model->paymentTransaction->status)
                        );
                }
            }
        }

        return $desc;
    }
}