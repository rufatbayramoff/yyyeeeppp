<?php namespace lib\payment\exception;

/**
 * Description of PaymentManagerUserException
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentManagerUserException extends PaymentManagerException
{
}
