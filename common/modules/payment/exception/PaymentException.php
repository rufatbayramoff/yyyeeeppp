<?php
namespace common\modules\payment\exception;
/**
 * PaymentException
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentException extends \Exception
{
}
