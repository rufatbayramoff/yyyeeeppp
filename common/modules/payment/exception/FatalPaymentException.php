<?php
namespace common\modules\payment\exception;
/**
 * PaymentException
 *
 */
class FatalPaymentException extends \Exception
{
}
