<?php namespace lib\payment\exception;

/**
 * Description of PaymentManagerException
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentManagerException extends \Exception
{
}
