<?php namespace lib\payment\exception;

/**
 * Description of PaymentManagerSystemException
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentManagerSystemException extends PaymentManagerException
{
}
