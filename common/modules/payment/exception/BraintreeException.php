<?php

namespace common\modules\payment\exception;

use yii\base\UserException;

class BraintreeException extends UserException
{
}

