<?php
namespace common\modules\payment\exception;

use yii\base\UserException;

class TransactionNotFoundException extends UserException
{
    public function __construct($message = "Transaction not found")
    {
        parent::__construct($message);
    }
}
