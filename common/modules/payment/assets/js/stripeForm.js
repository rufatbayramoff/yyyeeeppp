var stripeFormClass = {
    checkoutSession: null,
    config: {
        publicKey: null,
        clientToken: null,
        invoiceUuid: null,
        logUuid: null,
        address: null,
        successUrl: null,

        stripePaymentBlockId: 'stripe-payment-block',
        stripePaymentLoadingMsgId: 'stripe-payment-loading-msg',
        stripePaymentSubmitId: 'stripe-payment-submit',
        stripeProcessingMsgId: 'stripe-processing-msg',
        stripePaymentFormId: 'stripe-payment-form'

    },

    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        self.initForm();
    },

    initForm: function () {
        var self = this;
        $.ajaxSetup({cache: true});

        if (window.stripeJs) {
            window.stripeInitStep2 = self;
        } else {
            window.stripeJs = jQuery.getScript("https://js.stripe.com/v3/", function () {
                self.initStripeDefaultConfig();
            });
        }
    },

    stopLoading: function () {
        var self = this;
        $('#'+self.config.stripePaymentBlockId).removeClass('hidden');
        $('#'+self.config.stripePaymentLoadingMsgId).addClass('hidden');
        $('#'+self.config.stripePaymentSubmitId).removeClass('hidden');
    },

    submitting: function() {
        var self = this;
        $('#'+self.config.stripeProcessingMsgId).removeClass('hidden');
        $('#'+self.config.stripePaymentSubmitId).prop('disabled', true);
    },

    stopSubmitting: function() {
        var self = this;
        $('#'+self.config.stripeProcessingMsgId).addClass('hidden');
        $('#'+self.config.stripePaymentSubmitId).prop('disabled', false);
    },

    initStripeDefaultConfig: function () {
        var self = this;
        const stripe = Stripe(self.config.publicKey);
        let elements = stripe.elements({appearance: {theme: 'stripe'}, clientSecret: self.config.clientToken, locale:'en'});
        let elementOptions = {
            fields: {
                billingDetails: {
                    name: self.config.clientName,
                    email: self.config.clientEmail,
                    phone: self.config.clientPhone,
                }
            }
        };
        if (self.config.address) {
            elementOptions = {fields: {billingDetails: {address: 'never'}}};
        }

        let paymentElement = elements.create("payment", elementOptions);
        paymentElement.mount("#"+self.config.stripePaymentBlockId);

        paymentElement.on('ready', function(){
            self.stopLoading();
            if (window.stripeInitStep2) {
                setTimeout(function () {
                    let step2 = window.stripeInitStep2;
                    window.stripeInitStep2 = null;
                    step2.initStripeDefaultConfig();
                }, 100);
            }
        });

        document
            .querySelector("#" + self.config.stripePaymentFormId)
            .addEventListener("submit", function (event) {
                event.preventDefault();
                self.submitting();

                stripe.confirmPayment({
                    elements,
                    confirmParams: {
                        return_url: self.config.successUrl,
                        payment_method_data: {
                            billing_details: {
                                name: self.config.clientName,
                                email: self.config.clientEmail,
                                phone: self.config.clientPhone,
                                address: self.config.address
                            }
                        }
                    },

                }).then(function (result) {
                    if (result.error) {
                        let error = result.error;
                        // Inform the customer that there was an error.
                        if (error.type === "card_error" || error.type === "validation_error") {
                            errorText = error.message;
                        } else {
                            errorText = "Payment failed please contact support@treatstock.com";
                        }
                        $.post('/my/payments/log-payment-process-status?uuid=' + self.config.logUuid, {
                            'vendor': 'stripe',
                            payload: '',
                            err: JSON.stringify(result.error),
                            errorText: errorText
                        });
                        new TS.Notify({
                            type: 'error',
                            text: errorText,
                            target: '.messageBox',
                            automaticClose: false
                        });
                        self.stopSubmitting();
                    }
                });
            });

    },
};
