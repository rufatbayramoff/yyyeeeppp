var braintreeFormClass = {
    config: {
        clientToken: null,
        amount: null,
        currency: '',
        enableCard: true,
        threeDSecureEnabled: true,
        threeDSecureParams: {
            mobilePhoneNumber: '',  // 8101234567
            email: '',              // test@example.com
            shippingMethod: '',     // 01
            billingAddress: {
                firstName: '',      // Jill
                lastName: '',       // Doe
                streetAddress: '',  // 555 Smith St.
                extendedAddress: '',    // #5
                locality: '',       // Oakland
                region: '',         // CA
                postalCode: '',     // 12345
                countryCodeAlpha2: '' // US
            }
        }
    },

    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        self.initForm();
    },

    sameError: function (err, template) {
        for (var key in template) {
            if (!template.hasOwnProperty(key)) continue;
            var value = template[key];
            if (err[key] !== value) {
                return false;
            }
        }
        return true;
    },

    processErrorMessage: function (err) {
        if (!err) {
            return '';
        }
        let self = this;
        let errorText = err.message ? err.message : '';
        self.logError('Request Payment Method Error: ' + JSON.stringify(err));
        if (self.sameError(err, {
            "name": "DropinError",
            "message": "No payment method is available."
        }) && $('.braintree-options').not('.braintree-options-initial').not('.braintree-hidden').length
        ) {
            errorText = _t('site.store', 'No payment method selected');
        }
        return errorText;
    },

    initBraintree: function (braintreeConfig) {
        var self = this;
        var form = document.querySelector('#checkout');
        $(braintreeConfig['selector']).addClass('hidden');
        braintree.dropin.create(braintreeConfig, function (createErr, instance) {
            if (createErr) {
                if (createErr['_braintreeWebError'] && createErr['_braintreeWebError']['code'] === 'THREEDS_NOT_ENABLED') {
                    self.logError('Create Error: ' + JSON.stringify(createErr));
                    delete braintreeConfig['threeDSecure'];
                    $(braintreeConfig['selector']).html();
                    braintreeConfig['selector'] = '#payment-form';
                    self.initBraintree(braintreeConfig);
                    return;
                }

                self.logError('Create Error: ' + JSON.stringify(createErr));
                $('#paymentLoadingMsg').html('Payment failed');
                return;
            }
            $(braintreeConfig['selector']).removeClass('hidden');

            $('#paymentLoadingMsg').addClass('hidden');
            $('#paymentSubmit').removeClass('hidden');

            form.addEventListener('submit', function (event) {
                $.post('/my/payments/log-payment-button-press?uuid=' + self.config.logUuid, {
                    invoiceUuid: self.config.invoiceUuid,
                    'vendor': 'braintree'
                });
                $('#paymentSubmit').attr('disabled', true);
                event.preventDefault();
                let paymentMethodCallBack = function (err, payload) {
                    let errorText = '';
                    if (payload &&
                        payload.type == 'CreditCard' &&
                        payload.details.cardType &&
                        payload.details.cardType !== 'Visa' &&
                        payload.details.cardType !== 'Mastercard' &&
                        payload.details.cardType !== 'MasterCard' &&
                        payload.details.cardType !== 'JCB' &&
                        payload.details.cardType !== 'American Express'
                    ) {
                        errorText = _t('site.store', 'Credit card type is not allowed. Please try  Visa, MasterCard, or JCB.');
                        err = true;
                    } else {
                        errorText = self.processErrorMessage(err);
                    }

                    $.post('/my/payments/log-payment-process-status?uuid=' + self.config.logUuid, {
                        'vendor': 'braintree',
                        payload: JSON.stringify(payload),
                        err: JSON.stringify(err),
                        errorText: errorText
                    });

                    if (err) {
                        if (errorText) {
                            new TS.Notify({
                                type: 'error',
                                text: errorText,
                                target: '.messageBox',
                                automaticClose: false
                            });
                        }
                        $('#paymentSubmit').attr('disabled', false);
                        return;
                    }

                    function submitResults() {
                        document.querySelector('#payment_method_nonce').value = payload.nonce;
                        $('#paymentProcessingMsg').removeClass('hidden');
                        $('#paymentSubmit').addClass('hidden');
                        $('.braintree-toggle').addClass('hidden');
                        setTimeout(function () { // For finish ajax log-payment-process-status requests
                            form.submit();
                        }, 300);
                    }

                    if (self.config.threeDSecureEnabled) {
                        // User 3dsecure
                        if (payload.liabilityShifted || payload.type !== 'CreditCard') {
                            submitResults();
                        } else {
                            // Decide if you will force the user to
                            // enter a different
                            // payment method if liablity was not shifted
                            self.logError('3d secure failed');
                            new TS.Notify({
                                type: 'error',
                                text: _t('site.store', '3d secure checking failed'),
                                target: '.messageBox',
                                automaticClose: false
                            });
                            instance.clearSelectedPaymentMethod();
                            $('#paymentSubmit').attr('disabled', false);
                        }
                    } else {
                        // without 3dsecure
                        submitResults();
                    }
                };
                if (self.config.threeDSecureParams) {
                    instance.requestPaymentMethod({threeDSecure: self.config.threeDSecureParams}, paymentMethodCallBack);
                } else {
                    instance.requestPaymentMethod(paymentMethodCallBack);
                }
            });
        });
    },

    initBraintreeDefaultConfig: function () {
        var self = this;
        var braintreeConfig = {
            authorization: self.config.clientToken,
            selector: '#payment-form-3ds',
        };
        if (!self.config.enableCard) {
            braintreeConfig.card = false;
        }
        if (self.config.currency == 'usd') {
            braintreeConfig.paypal = {
                flow: 'vault'
            };
        }
        if (self.config.threeDSecureEnabled) {
            braintreeConfig.threeDSecure = {
                amount: self.config.amount,
                currency: self.config.currency
            };
        }
        self.initBraintree(braintreeConfig);
    },

    initForm: function () {
        var self = this;
        $.ajaxSetup({cache: true});

        if (window.dropinMinJs) {
            self.initBraintreeDefaultConfig();
        } else {
            window.dropinMinJs = jQuery.getScript("https://js.braintreegateway.com/web/dropin/1.27.0/js/dropin.min.js", function () {
                self.initBraintreeDefaultConfig();
            });
        }
    },

    logError: function (string) {
        console.log(string);
    },
};
