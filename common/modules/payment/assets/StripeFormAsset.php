<?php

namespace common\modules\payment\assets;

use yii\web\AssetBundle;

class StripeFormAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = __DIR__ ;

    public $js = [
        'js/stripeForm.js',
    ];

    public $css = [
        'css/stripeForm.css',
    ];
}