<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.07.18
 * Time: 13:59
 */

namespace common\modules\payment\assets;

use yii\web\AssetBundle;

class BraintreeFormAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = __DIR__ ;

    public $js = [
        'js/braintreeForm.js',
    ];

    public $css = [
        'css/braintreeForm.css',
    ];
}