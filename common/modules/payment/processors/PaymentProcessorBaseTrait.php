<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.08.18
 * Time: 10:30
 */

namespace common\modules\payment\processors;

trait PaymentProcessorBaseTrait
{
    /**
     * log all request|response operations from braintree
     *
     * @param string $operation
     * @param array $data
     * @param string $sep
     */
    public function log($operation, $data, $sep = "\n\r")
    {
        $message = '[' . $operation . ']' . var_export($data, true);
        $message = $message . $sep;
        \Yii::info($message, $this->code);
    }

    /**
     * start to log API requests
     *
     * @param string $operation
     * @param mixed $data
     */
    public function logStart($operation, $data)
    {
        $sep = PHP_EOL . str_repeat('-', 80) . PHP_EOL;
        $this->log($operation . '_START', $data, $sep);
    }

    /**
     * log API response to request
     *
     * @param string $operation
     * @param mixed $data
     */
    public function logFinish($operation, $data)
    {
        $sep = PHP_EOL . str_repeat('=', 80) . PHP_EOL;
        $this->log($operation . '_FINISH', $data, $sep);
    }
}