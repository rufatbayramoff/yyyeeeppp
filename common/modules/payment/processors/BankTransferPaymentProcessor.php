<?php
/**
 * Date: 01.10.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\payment\processors;

use common\components\exceptions\BusinessException;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\models\PaymentTransactionRefund;
use common\models\StoreOrder;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\factories\PaymentFactory;
use common\modules\payment\factories\PaymentTransactionFactory;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\gateways\vendors\BankInvoiceGateway;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use frontend\models\user\UserFacade;
use lib\money\Money;
use yii\base\InvalidArgumentException;

/**
 * Class BankTransferProcessor
 * @package common\modules\payment\processors
 *
 * @property BankInvoiceGateway $bankInvoiceGateway
 * @property PaymentFactory $paymentFactory
 * @property PaymentService $paymentService
 * @property PaymentAccountService $paymentAccountService
 * @property PaymentTransactionFactory $paymentTransactionFactory
 */
class BankTransferPaymentProcessor implements PaymentProcessorInterface
{
    use PaymentProcessorBaseTrait {
    }

    public $bankInvoiceGateway;

    public $paymentFactory;

    public $paymentTransactionFactory;

    public $paymentService;

    public $paymentAccountService;

    public $code = 'BankTransfer';

    public function injectDependencies(
        BankInvoiceGateway $bankInvoiceGateway,
        PaymentFactory $paymentFactory,
        PaymentTransactionFactory $paymentTransactionFactory,
        PaymentAccountService $paymentAccountService,
        PaymentService $paymentService
    ): void
    {
        $this->bankInvoiceGateway        = $bankInvoiceGateway;
        $this->paymentFactory            = $paymentFactory;
        $this->paymentTransactionFactory = $paymentTransactionFactory;
        $this->paymentAccountService     = $paymentAccountService;
        $this->paymentService            = $paymentService;
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @return string
     */
    public function generateClientToken(PaymentInvoice $paymentInvoice, string $logUuid = ''): string
    {
        return '0000';
    }

    /**
     * Cancel new order payment
     *
     * @param PaymentDetailOperation $paymentOperation
     * @param string $comment
     * @return int
     *
     */
    public function cancel(PaymentDetailOperation $paymentOperation, $comment)
    {
        //throw new PaymentException('Bank invoice cancel not available');
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @param $paymentToken
     *
     * @return PaymentDetailOperation
     * @throws PaymentException
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function pay(PaymentInvoice $paymentInvoice, $paymentToken, $amount = null): PaymentDetailOperation
    {
        if (!UserFacade::isCurrentUserAdmin()) {
            throw new PaymentException('No access.');
        }

        if ($paymentInvoice->getAmountTotal()->getAmount() < $amount) {
            throw new BusinessException('Amount is more than ' . $paymentInvoice->getAmountTotal()->getAmount());
        }

        $payment = $this->paymentFactory->createByInvoice($paymentInvoice);
        $payment->safeSave();

        $amountTotal = $paymentInvoice->getAmountTotalWithRefund();
        $isPartPayed = false;
        if ($amount && $amountTotal->getAmount() > $amount) {
            $amountTotal = Money::create($amount, $paymentInvoice->currency);
            $isPartPayed = true;
        }

        $this->logStart(
            'pay',
            [
                'paymentInvoiceUuid' => $paymentInvoice->uuid,
                'pmNonce'            => $paymentToken,
                'amount'             => $amountTotal,
            ]
        );

        $bankTransferAccount     = $this->paymentAccountService->getBankTransferAccount($paymentInvoice->currency);
        $paymentAuthorizeAccount = $this->paymentAccountService->getUserPaymentAccount($paymentInvoice->user, PaymentAccount::ACCOUNT_TYPE_RESERVED, $paymentInvoice->currency);

        $paymentGatewayResult = $this->bankInvoiceGateway->authorize($amountTotal);

        /** @var $detailFrom PaymentDetail */
        [$detailFrom, $detailTo] = $this->paymentService->transferMoney($payment, $bankTransferAccount, $paymentAuthorizeAccount, $amountTotal,
            PaymentDetail::TYPE_PAYMENT,
            'Payment via Bank Transfer for invoice:' . $paymentInvoice->uuid);

        /** @var PaymentTransaction $paymentTransaction */
        $paymentGatewayTransaction = $paymentGatewayResult->getTransaction();

        /** @var PaymentTransactionHistory $paymentTransactionHistory */
        [$paymentTransaction, $paymentTransactionHistory] = $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayTransaction, $detailFrom);

        $paymentTransaction->safeSave();
        $paymentTransactionHistory->transaction_id = $paymentTransaction->id;
        $paymentTransactionHistory->safeSave();

        $paymentInvoice->paymentBankInvoice->transaction_id = $paymentTransaction->id;
        $paymentInvoice->paymentBankInvoice->safeSave(['transaction_id']);

        $invoiceStatus = $this->transactionStatusToInvoiceStatus($paymentGatewayTransaction->status);
        if (($invoiceStatus === PaymentInvoice::STATUS_PAID) && $isPartPayed) {
            $invoiceStatus = PaymentInvoice::STATUS_PART_PAID;
        }
        $this->paymentService->setPayedInvoice($paymentInvoice, $detailFrom->paymentDetailOperation, $invoiceStatus);

        $this->submitForSettle($detailFrom->paymentDetailOperation);

        $payment->status = Payment::STATUS_PAID;
        $payment->safeSave();

        $this->logFinish('pay', \yii\helpers\VarDumper::dumpAsString($paymentGatewayResult));
        return $detailFrom->paymentDetailOperation;
    }

    /**
     * Convert vendor transaction status into invoice status
     *
     * @param string $paymentTransactionStatus
     * @return mixed
     */
    public function transactionStatusToInvoiceStatus($paymentTransactionStatus)
    {
        $map = [
            PaymentTransaction::STATUS_REQUIRES_CAPTURE => PaymentInvoice::STATUS_PAID,
            PaymentTransaction::STATUS_AUTHORIZED => PaymentInvoice::STATUS_PAID,
            PaymentTransaction::STATUS_SETTLED    => PaymentInvoice::STATUS_PAID,
            PaymentTransaction::STATUS_VOIDED     => PaymentInvoice::STATUS_VOID,
        ];
        if (array_key_exists($paymentTransactionStatus, $map)) {
            return $map[$paymentTransactionStatus];
        }
        throw new InvalidArgumentException('Not found gateway transaction status: ' . $paymentTransactionStatus . '.');
    }

    /**
     * Settle payemnt operation
     *
     * @param PaymentDetailOperation $paymentOperation
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function submitForSettle(PaymentDetailOperation $paymentOperation)
    {
        $paymentTransaction = $paymentOperation->paymentTransaction;
        $transactionId      = $paymentTransaction->transaction_id;
        $this->logStart('settle', [$transactionId]);
        $paymentGatewayResult = $this->bankInvoiceGateway->settle($transactionId);
        $this->paymentService->updateTransactionStatus($paymentTransaction, PaymentTransaction::STATUS_SETTLED);
        $this->logFinish('settle', [$paymentGatewayResult]);
        return true;
    }

    /**
     * Payment gateway transaction
     *
     * @param $id
     * @return PaymentGatewayTransaction
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTransaction($id): PaymentGatewayTransaction
    {
        return $this->bankInvoiceGateway->getTransaction($id);
    }

    /**
     * @param array $ids
     * @return PaymentGatewayTransaction[]
     */
    public function getTransactions(array $ids): array
    {
        return $this->bankInvoiceGateway->getTransactions($ids);
    }

    /**
     * Request for refund
     *
     * @param PaymentTransactionRefund $paymentTransactionRefund
     *
     * @param PaymentAccount $fromPaymentAccount
     * @return mixed|void
     * @throws PaymentException
     */
    public function refund(PaymentTransactionRefund $paymentTransactionRefund, PaymentAccount $fromPaymentAccount): ?PaymentDetailOperation
    {
        throw new PaymentException('Bank invoice refund not available');
    }

    public function checkAllowPartitalRefund()
    {

    }

    public function getGateway(): PaymentGateway
    {
        return $this->bankInvoiceGateway;
    }
}