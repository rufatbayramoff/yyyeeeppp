<?php
/**
 * Date: 01.10.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\payment\processors;

use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\models\User;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\factories\PaymentFactory;
use common\modules\payment\factories\PaymentTransactionFactory;
use common\modules\payment\gateways\vendors\BankPayoutGateway;
use common\modules\payment\gateways\vendors\PayPalPayoutGateway;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use lib\money\Money;
use yii\base\UserException;

/**
 * Class BankPayoutProcessor
 *
 * @package common\modules\payment\processors
 *
 * @property PaymentAccountService $paymentAccountService
 * @property PaymentFactory $paymentFactory
 * @property BankPayoutGateway $bankPayoutGateway
 * @property PaymentService $paymentService
 * @property PaymentTransactionFactory $paymentTransactionFactory
 */
class BankPayoutProcessor
{
    public const MINIMUM_PAYOUT = 400;

    protected $paymentAccountService;

    protected $paymentFactory;

    protected $payPalPayoutGateway;

    protected $paymentService;

    protected $paymentTransactionFactory;

    public function injectDependencies(
        PaymentAccountService $paymentAccountService,
        PaymentFactory $paymentFactory,
        BankPayoutGateway $bankPayoutGateway,
        PaymentService $paymentService,
        PaymentTransactionFactory $paymentTransactionFactory
    ): void {
        $this->paymentAccountService     = $paymentAccountService;
        $this->paymentFactory            = $paymentFactory;
        $this->bankPayoutGateway         = $bankPayoutGateway;
        $this->paymentService            = $paymentService;
        $this->paymentTransactionFactory = $paymentTransactionFactory;
    }

    /**
     * @param User $user
     * @param string $bankAccountDetails
     * @param Money $withdrawMoney
     *
     * @return PaymentDetailOperation
     * @throws PaymentException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     */
    public function payOut(User $user, $bankAccountDetails, Money $withdrawMoney): PaymentDetailOperation
    {
        $tax = PaymentInfoHelper::getTaxRequired($user);
        $taxAmountMoney = null;
        $requestWithdrawMoney = $withdrawMoney;

        // add taxes before paypal payout
        // we do it here, because if user fill's in his taxes
        // anyway old taxes with 30% will be already saved.
        // but if user fills in after, and trys to payout, no problems, he will have 0% taxes
        if (!$tax->isEmpty()) {
            $taxAmountMoney = Money::create($tax->getTaxAmount($withdrawMoney->getAmount()), $withdrawMoney->getCurrency());
            $withdrawMoney = Money::create($tax->getCleanAmount($withdrawMoney->getAmount()), $withdrawMoney->getCurrency());
        }
        // ---

        if (strlen($bankAccountDetails)<1) {
            throw new PaymentException(_t(
                'site.payment',
                'Please fill bank account details'
            ));
        }

        if ($requestWithdrawMoney->getAmount() < self::MINIMUM_PAYOUT) {
            throw new PaymentException(_t(
                'site.payment',
                'The minimum payout amount is '.$requestWithdrawMoney->getCurrency().'{minimun}',
                ['minimun' => self::MINIMUM_PAYOUT]
            ));
        }

        $amountNetIncome = $this->paymentAccountService->getUserMainAmount($user, $requestWithdrawMoney->getCurrency());

        if (round($amountNetIncome->getAmount(), 2) < round($withdrawMoney->getAmount(), 2) || $withdrawMoney->getCurrency() !== $amountNetIncome->getCurrency()) {
            throw new PaymentException(_t(
                'site.payment',
                'Sorry, you cannot request payout more than {hasAmount}. Your request {outAmount}',
                ['hasAmount' => displayAsMoney($amountNetIncome), 'outAmount' => displayAsMoney($withdrawMoney)]
            ));
        }

        $payment = $this->paymentFactory->createPayment('Bank payout');
        $payment->safeSave();

        $paymentMainAccount = $this->paymentAccountService->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_MAIN, $requestWithdrawMoney->getCurrency());
        $paymentPayoutAccount = $this->paymentAccountService->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_PAYOUT, $requestWithdrawMoney->getCurrency());
        $paymentTaxAccount = $this->paymentAccountService->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_TAX, $requestWithdrawMoney->getCurrency());

        // tax ---
        if ($taxAmountMoney) {
            $this->paymentService->transferMoney($payment, $paymentMainAccount, $paymentTaxAccount, $taxAmountMoney,
                PaymentDetail::TYPE_TAX,
                'Tax withholding'
            );
        }
        // ---
        $this->bankPayoutGateway->details = [BankPayoutGateway::ACCOUNT_DETAILS => $bankAccountDetails];

        $paymentGatewayResult = $this->bankPayoutGateway->authorize($withdrawMoney);

        /** @var $detailFrom PaymentDetail */
        [$detailFrom, $detailTo] = $this->paymentService->transferMoney($payment, $paymentMainAccount, $paymentPayoutAccount, $withdrawMoney,
            PaymentDetail::TYPE_PAYOUT,
            'Withdrawal ' . displayAsMoney($withdrawMoney) . ' of funds from the balance');

        /** @var PaymentTransaction $paymentTransaction */
        $paymentGatewayTransaction = $paymentGatewayResult->getTransaction();

        /** @var PaymentTransactionHistory $paymentTransactionHistory */
        [$paymentTransaction, $paymentTransactionHistory] = $this->paymentTransactionFactory->createByGatewayTransaction(
            $paymentGatewayTransaction,
            $detailFrom,
            PaymentTransaction::TYPE_MINUS
        );

        $paymentTransaction->safeSave();
        $paymentTransactionHistory->transaction_id = $paymentTransaction->id;
        $paymentTransactionHistory->safeSave();

        return $detailFrom->paymentDetailOperation;
    }

    /**
     * @param PaymentTransaction $pt
     * @throws PaymentException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function updateToSuccess(PaymentTransaction $pt)
    {
        if (!$pt->isBankPayoutAuthorized()) {
            throw new  UserException('Payment transaction status already changed');
        }
        $paymentTransactionHistory = $this->paymentService->updateTransactionStatus($pt, PaymentTransaction::STATUS_UP_SUCCESS);
        $payment = $pt->firstPaymentDetail->paymentDetailOperation->payment;
        $detailFrom = $pt->firstPaymentDetail->paymentDetailOperation->toPaymentDetail()->paymentAccount;
        $detailTo = $this->paymentAccountService->getPaypalAccount();
        $amount = $pt->getMoneyAmount();
        [$paymentDetailFrom, $paymentDetailTo] = $this->paymentService->transferMoney($payment, $detailFrom, $detailTo, $amount, PaymentDetail::TYPE_PAYOUT,
            'Payout success completed');
        $paymentTransactionHistory->payment_detail_id = $paymentDetailTo->id;
        $paymentTransactionHistory->safeSave();


    }

    /**
     * @param PaymentTransaction $pt
     * @throws PaymentException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     */
    public function updateToFailed(PaymentTransaction $pt)
    {
        if (!$pt->isBankPayoutAuthorized()) {
            throw new  UserException('Payment transaction status already changed');
        }
        $paymentTransactionHistory = $this->paymentService->updateTransactionStatus($pt, PaymentTransaction::STATUS_FAILED);
        $payment = $pt->firstPaymentDetail->paymentDetailOperation->payment;
        $detailFrom = $pt->firstPaymentDetail->paymentDetailOperation->toPaymentDetail()->paymentAccount;
        $detailTo = $pt->firstPaymentDetail->paymentDetailOperation->fromPaymentDetail()->paymentAccount;
        $amount = $pt->getMoneyAmount();
        [$paymentDetailFrom, $paymentDetailTo] = $this->paymentService->transferMoney($payment, $detailFrom, $detailTo, $amount, PaymentDetail::TYPE_PAYOUT,
            'Payout failed');
        $paymentTransactionHistory->payment_detail_id = $paymentDetailTo->id;
        $paymentTransactionHistory->safeSave();
    }
}