<?php

namespace common\modules\payment\processors;

use common\components\PaymentExchangeRateConverter;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentBankInvoice;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionRefund;
use common\models\StoreOrder;
use common\models\StoreOrderPosition;
use common\models\User;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\services\RefundService;
use lib\money\Money;
use lib\payment\PaymentManager;
use Yii;

/**
 * PaymentGateway
 *
 * all vendors (api for payment) should extends this gateway
 * All API requests MUST go only through Gateway class
 * For each API vendor, create Gateway class  to use
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
interface PaymentProcessorInterface
{
    /**
     * when order first pay, we reserve (authorize) required amount
     *
     * @param PaymentInvoice $paymentInvoice
     * @param $paymentToken
     * @return
     */
    public function pay(PaymentInvoice $paymentInvoice, $paymentToken, $amount = null): PaymentDetailOperation;

    /**
     * default client token not required
     *
     * @param PaymentInvoice $paymentInvoice
     * @return string
     */
    public function generateClientToken(PaymentInvoice $paymentInvoice, string $logUuid = ''): string;

    /**
     * Cancel new order payment
     *
     * @param PaymentDetailOperation $paymentOperation
     * @param string $comment
     * @return int
     */
    public function cancel(PaymentDetailOperation $paymentOperation, $comment);


    /**
     * Settle payment operation
     *
     * @param PaymentDetailOperation $paymentOperation
     * @return mixed
     */
    public function submitForSettle(PaymentDetailOperation $paymentOperation);

    /**
     * Check is allowed partital refund
     *
     * @return mixed
     */
    public function checkAllowPartitalRefund();

    /**
     * Request for refund
     *
     * @param PaymentTransactionRefund $paymentTransactionRefund
     * @param PaymentAccount $fromPaymentAccount
     * @return mixed
     */
    public function refund(PaymentTransactionRefund $paymentTransactionRefund, PaymentAccount $fromPaymentAccount): ?PaymentDetailOperation;

    /**
     * Convert vendor status into invoice status
     *
     * @param $paymentTransactionStatus
     * @return mixed
     */
    public function transactionStatusToInvoiceStatus($paymentTransactionStatus);


    /**
     * Payment gateway transaction
     *
     * @param $id
     * @return PaymentGatewayTransaction
     */
    public function getTransaction($id): PaymentGatewayTransaction;

    /**
     * Payment gateway transactions
     *
     * @param array $ids
     * @return PaymentGatewayTransaction[]
     */
    public function getTransactions(array $ids): array;

    /**
     * @return PaymentGateway
     */
    public function getGateway(): PaymentGateway;
}
