<?php

namespace common\modules\payment\processors;

use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\models\PaymentTransactionRefund;
use common\modules\payment\components\PaymentLogger;
use common\modules\payment\exception\FatalPaymentException;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\factories\PaymentFactory;
use common\modules\payment\factories\PaymentTransactionFactory;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\gateways\vendors\BonusBalanceGateway;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use yii\base\InvalidArgumentException;

/**
 * Class BonusBalanceProcessor
 *
 * @package common\modules\payment\processors
 *
 * @property BonusBalanceGateway $bonusBalanceGateway
 * @property PaymentService $paymentService
 * @property PaymentFactory $paymentFactory
 * @property PaymentAccountService $paymentAccountService
 * @property PaymentTransactionFactory $paymentTransactionFactory
 */
class BonusBalanceProcessor implements PaymentProcessorInterface
{
    use PaymentProcessorBaseTrait {
    }

    public $bonusBalanceGateway;

    public $paymentFactory;

    public $paymentTransactionFactory;

    public $paymentService;

    public $paymentAccountService;

    public $code = 'BonusBalance';

    /** @var PaymentLogger */
    public $paymentLogger;

    /**
     * @param BonusBalanceGateway $bonusBalanceGateway
     * @param PaymentFactory $paymentFactory
     * @param PaymentTransactionFactory $paymentTransactionFactory
     * @param PaymentAccountService $paymentAccountService
     * @param PaymentService $paymentService
     * @param PaymentLogger $paymentLogger
     */
    public function injectDependencies(
        BonusBalanceGateway       $bonusBalanceGateway,
        PaymentFactory            $paymentFactory,
        PaymentTransactionFactory $paymentTransactionFactory,
        PaymentAccountService     $paymentAccountService,
        PaymentService            $paymentService,
        PaymentLogger             $paymentLogger
    ): void
    {
        $this->bonusBalanceGateway       = $bonusBalanceGateway;
        $this->paymentFactory            = $paymentFactory;
        $this->paymentTransactionFactory = $paymentTransactionFactory;
        $this->paymentAccountService     = $paymentAccountService;
        $this->paymentService            = $paymentService;
        $this->paymentLogger             = $paymentLogger;
    }

    public function generateClientToken(PaymentInvoice $paymentInvoice, string $logUuid = ''): string
    {
        return '0000';
    }

    /**
     * Cancel new order payment
     *
     * @param PaymentDetailOperation $paymentOperation
     * @param string $comment
     * @return void
     * @throws PaymentException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function cancel(PaymentDetailOperation $paymentOperation, $comment)
    {
        $payment            = $paymentOperation->payment;
        $paymentTransaction = $paymentOperation->paymentTransaction;
        $fromAccount        = $paymentOperation->fromPaymentDetail()->paymentAccount;
        $toAccount          = $paymentOperation->toPaymentDetail()->paymentAccount;
        $transactionId      = $paymentTransaction->transaction_id;
        $isReturned         = 0;

        if (
            $paymentTransaction->status === PaymentTransaction::STATUS_AUTHORIZED ||
            $paymentTransaction->status === PaymentTransaction::STATUS_SUBMITTED_FOR_SETTLEMENT ||
            $paymentTransaction->status === PaymentTransaction::STATUS_SETTLING ||
            $paymentTransaction->status === PaymentTransaction::STATUS_SETTLED
        ) {
            // Transaction was transfered to reserved account
            // Payment was transferred to reserved or etc...
            $reservedAccount = $this->paymentAccountService->getUserPaymentAccount($toAccount->user, PaymentAccount::ACCOUNT_TYPE_RESERVED, Currency::BNS);
            $this->bonusBalanceGateway->refund($transactionId);
            [$paymentDetailFrom, $paymentDetailTo] = $this->paymentService->transferMoney(
                $payment,
                $reservedAccount,
                $fromAccount,
                $paymentTransaction->getMoneyAmount(), PaymentDetail::TYPE_REFUND);

            $isReturned                = 1;
            $paymentTransactionHistory = $this->paymentService->updateTransactionStatus($paymentTransaction, PaymentTransaction::STATUS_REFUNDED);
            if ($paymentTransactionHistory) {
                $paymentTransactionHistory->payment_detail_id = $paymentDetailFrom->id;
                $paymentTransactionHistory->safeSave();
            }
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_VOIDED) {
            // Already voided
            throw new PaymentException('Cancel voided transaction is not enabled');
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_FAILED) {
            // Was failed
            throw new PaymentException('Cancel failed transaction is not enabled');
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_REFUNDED) {
            // Already refunded
            throw new PaymentException('Cancel refunded transaction is not enabled');
        }

        // Current pay position
        if ($isReturned) {
            $paymentOperation->payment->status                 = Payment::STATUS_CANCELLED;
            $paymentOperation->payment->paymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
        } else {
            $paymentOperation->payment->status                 = Payment::STATUS_FAILED;
            $paymentOperation->payment->paymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
        }
        $paymentOperation->payment->safeSave();
        $paymentOperation->payment->paymentInvoice->safeSave();

    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @param $paymentToken
     *
     * @return PaymentDetailOperation
     * @throws PaymentException
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function pay(PaymentInvoice $paymentInvoice, $paymentToken, $amount = null): PaymentDetailOperation
    {
        $payment = $this->paymentFactory->createByInvoice($paymentInvoice);
        $payment->safeSave();

        if (!$paymentInvoice->getAmountBonus()?->getAmount()) {
            throw new FatalPaymentException(_t('site.payments', 'Invalid payment invoice type.'));
        }
        $amountTotal = Money::create($paymentInvoice->getAmountBonus()->getAmount(), Currency::BNS);

        $this->logStart(
            'pay',
            [
                'paymentInvoiceUuid' => $paymentInvoice->uuid,
                'pmNonce'            => $paymentToken,
                'amount'             => $paymentInvoice->getAmountTotal(),
                'bns'                => $amountTotal
            ]
        );

        $amountNetIncome = $this->paymentAccountService->getUserMainAmount($paymentInvoice->user, Currency::BNS);

        if (MoneyMath::less($amountNetIncome, $amountTotal)) {
            throw new PaymentException(_t('site.payments', 'Your bonus balance has insufficient funds.'));
        }

        $paymentMainAccount      = $this->paymentAccountService->getUserPaymentAccount($paymentInvoice->user, PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);
        $paymentAuthorizeAccount = $this->paymentAccountService->getUserPaymentAccount($paymentInvoice->user, PaymentAccount::ACCOUNT_TYPE_RESERVED, Currency::BNS);

        $this->bonusBalanceGateway->init([
            'user' => $paymentInvoice->user
        ]);

        $paymentGatewayResult = $this->bonusBalanceGateway->authorize($amountTotal);

        /** @var $detailFrom PaymentDetail */
        [$detailFrom, $detailTo] = $this->paymentService->transferMoney($payment, $paymentMainAccount, $paymentAuthorizeAccount, $amountTotal,
            PaymentDetail::TYPE_PAYMENT,
            'Payment via bonus balance for invoice:' . $paymentInvoice->uuid);

        /** @var PaymentTransaction $paymentTransaction */
        $paymentGatewayTransaction = $paymentGatewayResult->getTransaction();

        /** @var PaymentTransactionHistory $paymentTransactionHistory */
        [$paymentTransaction, $paymentTransactionHistory] = $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayTransaction, $detailFrom);

        $paymentTransaction->safeSave();
        $paymentTransactionHistory->transaction_id = $paymentTransaction->id;
        $paymentTransactionHistory->safeSave();

        $this->paymentService->setPayedInvoice($paymentInvoice, $detailFrom->paymentDetailOperation, $this->transactionStatusToInvoiceStatus($paymentGatewayTransaction->status));
        $payment->status = Payment::STATUS_PAID;
        $payment->safeSave();

        $this->logFinish('pay', \yii\helpers\VarDumper::dumpAsString($paymentGatewayResult));
        return $detailFrom->paymentDetailOperation;
    }

    /**
     * Convert vendor transaction status into invoice status
     *
     * @param string $paymentTransactionStatus
     * @return mixed|void
     */
    public function transactionStatusToInvoiceStatus($paymentTransactionStatus)
    {
        $map = [
            PaymentTransaction::STATUS_SETTLED  => PaymentInvoice::STATUS_PAID,
            PaymentTransaction::STATUS_FAILED   => PaymentInvoice::STATUS_NEW,
            PaymentTransaction::STATUS_REFUNDED => PaymentInvoice::STATUS_REFUND,
            PaymentTransaction::STATUS_VOIDED   => PaymentInvoice::STATUS_NEW,
        ];
        if (array_key_exists($paymentTransactionStatus, $map)) {
            return $map[$paymentTransactionStatus];
        }
        throw new InvalidArgumentException('Not found gateway transaction status: ' . $paymentTransactionStatus . '.');
    }

    /**
     * Settle payemnt operation
     *
     * @param PaymentDetailOperation $paymentOperation
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function submitForSettle(PaymentDetailOperation $paymentOperation)
    {
        return true;
    }

    /**
     * Payment gateway transaction
     *
     * @param $id
     * @return PaymentGatewayTransaction
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTransaction($id): PaymentGatewayTransaction
    {
        return $this->bonusBalanceGateway->getTransaction($id);
    }

    /**
     * @param array $ids
     * @return PaymentGatewayTransaction[]
     */
    public function getTransactions(array $ids): array
    {
        return $this->bonusBalanceGateway->getTransactions($ids);
    }

    /**
     * Request for refund
     *
     * @param PaymentTransactionRefund $paymentTransactionRefund
     * @param PaymentAccount $fromPaymentAccount
     * @return mixed
     * @throws PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function refund(PaymentTransactionRefund $paymentTransactionRefund, PaymentAccount $fromPaymentAccount): ?PaymentDetailOperation
    {
        $paymentGatewayResult = $this->bonusBalanceGateway->refund($paymentTransactionRefund->transaction->transaction_id, $paymentTransactionRefund->amount);
        if ($paymentGatewayResult->isSuccess()) {

            $fromPaymentAccountMain = $this->paymentAccountService->getUserPaymentAccount($fromPaymentAccount->user, PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);
            $payment                = $paymentTransactionRefund->transaction->firstPaymentDetail->paymentDetailOperation->payment;
            $amountTotal            = Money::create($paymentTransactionRefund->amount, Currency::BNS);

            /** @var $detailFrom PaymentDetail */
            [$detailFrom, $detailTo] = $this->paymentService->transferMoney($payment, $fromPaymentAccount, $fromPaymentAccountMain,
                $amountTotal,
                PaymentDetail::TYPE_REFUND,
                'Refund via transaction:' . $paymentTransactionRefund->id);

            /* @var PaymentTransaction $paymentTransaction */
            /* @var PaymentTransactionHistory $paymentTransactionHistory */
            [$paymentTransaction, $paymentTransactionHistory] =
                $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayResult->getTransaction(), $detailTo);
            $paymentTransaction->type = PaymentTransaction::TYPE_REFUND;
            $paymentTransaction->safeSave();
            $paymentTransactionHistory->transaction_id = $paymentTransaction->id;
            $paymentTransactionHistory->safeSave();
            $paymentTransactionRefund->setApproved();
            $paymentTransactionRefund->transaction_refund_id = $paymentTransaction->id;
            $paymentTransactionRefund->safeSave();

            return $detailFrom->paymentDetailOperation;
        } elseif (strpos($paymentGatewayResult->getMessage(), 'Transaction has already been completely refunded') === 0) {
            $this->paymentLogger->log(
                PaymentLogger::TYPE_PAYMENT_DETAIL,
                'Already voided: ' . $paymentTransactionRefund->transaction->transaction_id,
                '',
                [
                    PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $paymentTransactionRefund->transaction->transaction_id,
                ],
                PaymentLogger::LEVEL_INFO
            );
        }
        return null;
    }

    /**
     * Check is allowed partital refund
     *
     * @return mixed
     */
    public function checkAllowPartitalRefund()
    {
        // TODO: Implement checkAllowPartitalRefund() method.
    }

    public function getGateway(): PaymentGateway
    {
        return $this->bonusBalanceGateway;
    }
}