<?php

namespace common\modules\payment\processors;

use common\components\exceptions\InvalidModelException;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentLog;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\models\PaymentTransactionRefund;
use common\models\StoreOrder;
use common\modules\payment\components\PaymentLogger;
use common\modules\payment\exception\BraintreeException;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\exception\TransactionNotFoundException;
use common\modules\payment\factories\PaymentFactory;
use common\modules\payment\factories\PaymentOperationFactory;
use common\modules\payment\factories\PaymentTransactionFactory;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\gateways\vendors\BraintreeGateway;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use lib\money\Money;
use yii\base\InvalidArgumentException;

/**
 * BraintreeGateway - helps to init config, and log requests to BrainTree api
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class BraintreeProcessor implements PaymentProcessorInterface
{
    use PaymentProcessorBaseTrait {

    }

    /** @var BraintreeGateway */
    public $braintreeGateway;

    /** @var PaymentFactory */
    public $paymentFactory;

    /** @var PaymentTransactionFactory */
    public $paymentTransactionFactory;

    /** @var PaymentOperationFactory */
    public $paymentOperationFactory;

    /** @var PaymentService */
    public $paymentService;

    /** @var PaymentAccountService */
    public $paymentAccountService;

    /** @var PaymentLogger */
    public $paymentLogger;

    /** @var string */
    public $code = 'BraintreeProcessor';

    /**
     * @param BraintreeGateway $braintreeGateway
     * @param PaymentFactory $paymentFactory
     * @param PaymentOperationFactory $paymentOperationFactory
     * @param PaymentService $paymentService
     * @param PaymentTransactionFactory $paymentTransactionFactory
     * @param PaymentAccountService $paymentAccountService
     */
    public function injectDependencies(
        BraintreeGateway $braintreeGateway,
        PaymentFactory $paymentFactory,
        PaymentOperationFactory $paymentOperationFactory,
        PaymentService $paymentService,
        PaymentTransactionFactory $paymentTransactionFactory,
        PaymentAccountService $paymentAccountService
    ): void
    {
        $this->braintreeGateway          = $braintreeGateway;
        $this->paymentFactory            = $paymentFactory;
        $this->paymentOperationFactory   = $paymentOperationFactory;
        $this->paymentService            = $paymentService;
        $this->paymentTransactionFactory = $paymentTransactionFactory;
        $this->paymentAccountService     = $paymentAccountService;
    }

    public function generateClientToken(PaymentInvoice $paymentInvoice, string $logUuid = ''): string
    {
        return $this->braintreeGateway->generatePaymentToken('', '', '', '', Money::zero($paymentInvoice->currency));
    }

    /**
     * Cancel new order payment
     *
     * @param PaymentDetailOperation $paymentOperation
     * @param string $comment
     * @return void
     * @throws PaymentException
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     */
    public function cancel(PaymentDetailOperation $paymentOperation, $comment)
    {
        $payment            = $paymentOperation->payment;
        $invoice            = $payment->paymentInvoice;
        $paymentTransaction = $paymentOperation->paymentTransaction;
        $fromAccount        = $paymentOperation->fromPaymentDetail()->paymentAccount;
        $toAccount          = $paymentOperation->toPaymentDetail()->paymentAccount;
        $transactionId      = $paymentTransaction->transaction_id;
        $vendorTransaction  = $this->getTransaction($transactionId);
        $isReturned         = 0;

        if (
            $paymentTransaction->status === PaymentTransaction::STATUS_AUTHORIZED ||
            $paymentTransaction->status === PaymentTransaction::STATUS_SUBMITTED_FOR_SETTLEMENT ||
            $paymentTransaction->status === PaymentTransaction::STATUS_SETTLING
        ) {
            // Its on authorized payment account
            if ($vendorTransaction->status === PaymentTransaction::STATUS_AUTHORIZED) {
                // Same status, just void transaction
                $gatewayResult = $this->braintreeGateway->void($transactionId);
                if (!$gatewayResult->isSuccess()) {
                    throw new PaymentException('Failed void transaction');
                }
                $isReturned = 1;
            } elseif ($vendorTransaction->status === PaymentTransaction::STATUS_SETTLED) {
                // Treastock doesn`t move money to reserved, buy braintree move to settled
                $paymentGatewayResult = $this->braintreeGateway->refund($transactionId);
                if (!$paymentGatewayResult->isSuccess()) {
                    throw new InvalidModelException('Failed refund transaction');
                }

                [$detailFrom, $detailTo] = $this->paymentService->transferMoney(
                    $payment,
                    $toAccount,
                    $fromAccount,
                    $paymentTransaction->getMoneyAmount(),
                    PaymentDetail::TYPE_REFUND,
                    'Full refund for invoice: ' . $invoice->uuid);

                /* @var PaymentTransaction $paymentTransactionRefund */
                /* @var PaymentTransactionHistory $paymentTransactionRefundHistory */
                [$paymentTransactionRefunded, $paymentTransactionRefundedHistory] =
                    $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayResult->getTransaction(), $detailTo);
                $paymentTransactionRefunded->type = PaymentTransaction::TYPE_REFUND;
                $paymentTransactionRefunded->safeSave();
                $paymentTransactionRefundedHistory->transaction_id = $paymentTransactionRefunded->id;
                $paymentTransactionRefundedHistory->safeSave();

                $isReturned = 1;
            } elseif ($vendorTransaction->status === PaymentTransaction::STATUS_REFUNDED) {
                throw new PaymentException('Payment transaction already refunded');
            } elseif ($vendorTransaction->status === PaymentTransaction::STATUS_FAILED) {
                // Transaction is failed
                throw new PaymentException('Payment transaction status transfered from AUTHORIZED to FAILED');
            }
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_VOIDED) {
            // Already voided
            throw new PaymentException('Cancel voided transaction is not enabled');
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_FAILED) {
            // Was failed
            throw new PaymentException('Cancel failed transaction is not enabled');
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_REFUNDED) {
            // Already refunded
            throw new PaymentException('Cancel refunded transaction is not enabled');
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_SETTLED) {
            // Transaction was transfered to reserved account
            // Payment was transferred to reserved or etc...
            if ($vendorTransaction->status !== PaymentTransaction::STATUS_SETTLED) {
                throw new InvalidModelException('Invalid vendor transaction status: ' . $vendorTransaction->status);
            }

            $paymentGatewayResult = $this->braintreeGateway->refund($transactionId);
            if (!$paymentGatewayResult->isSuccess()) {
                throw new InvalidModelException('Failed refund transaction');
            }

            $reservedAccount = $this->paymentAccountService->getUserPaymentAccount($toAccount->user, PaymentAccount::ACCOUNT_TYPE_RESERVED);

            [$detailFrom, $detailTo] = $this->paymentService->transferMoney(
                $payment,
                $reservedAccount,
                $fromAccount,
                $paymentTransaction->getMoneyAmount(),
                PaymentDetail::TYPE_REFUND,
                'Full refund from reserved');

            /* @var PaymentTransaction $paymentTransactionRefund */
            /* @var PaymentTransactionHistory $paymentTransactionRefundHistory */
            [$paymentTransactionRefunded, $paymentTransactionRefundedHistory] =
                $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayResult->getTransaction(), $detailTo);
            $paymentTransactionRefunded->type = PaymentTransaction::TYPE_REFUND;
            $paymentTransactionRefunded->safeSave();
            $paymentTransactionRefundedHistory->transaction_id = $paymentTransactionRefunded->id;
            $paymentTransactionRefundedHistory->safeSave();
            $isReturned = 1;
        }

        // Current pay position
        if ($isReturned) {
            $paymentOperation->payment->status                 = Payment::STATUS_CANCELLED;
            $paymentOperation->payment->paymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
        } else {
            $paymentOperation->payment->status                 = Payment::STATUS_FAILED;
            $paymentOperation->payment->paymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
        }
        $paymentOperation->payment->safeSave();
        $paymentOperation->payment->paymentInvoice->safeSave();
        return $isReturned;
    }


    /**
     * Refund accepted, make transactions
     *
     * @param PaymentTransactionRefund $paymentTransactionRefund
     * @param PaymentAccount $fromPaymentAccount
     * @return mixed
     * @throws PaymentException
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function refund(PaymentTransactionRefund $paymentTransactionRefund, PaymentAccount $fromPaymentAccount): ?PaymentDetailOperation
    {
        $paymentGatewayResult = $this->braintreeGateway->refund($paymentTransactionRefund->transaction->transaction_id, $paymentTransactionRefund->amount);
        if ($paymentGatewayResult->isSuccess()) {
            $braintreeAccount = PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_BRAINTREE);
            $payment          = $paymentTransactionRefund->transaction->firstPaymentDetail->paymentDetailOperation->payment;
            /** @var $detailFrom PaymentDetail */
            [$detailFrom, $detailTo] = $this->paymentService->transferMoney($payment, $fromPaymentAccount, $braintreeAccount, $paymentTransactionRefund->getMoneyAmount(),
                PaymentDetail::TYPE_REFUND,
                'Refund via transaction:' . $paymentTransactionRefund->id);

            /* @var PaymentTransaction $paymentTransaction */
            /* @var PaymentTransactionHistory $paymentTransactionHistory */
            [$paymentTransaction, $paymentTransactionHistory] =
                $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayResult->getTransaction(), $detailTo);
            $paymentTransaction->type = PaymentTransaction::TYPE_REFUND;
            $paymentTransaction->safeSave();
            $paymentTransactionHistory->transaction_id = $paymentTransaction->id;
            $paymentTransactionHistory->safeSave();
            $paymentTransactionRefund->setApproved();
            $paymentTransactionRefund->transaction_refund_id = $paymentTransaction->id;
            $paymentTransactionRefund->safeSave();

            return $detailFrom->paymentDetailOperation;
        } elseif (strpos($paymentGatewayResult->getMessage(), 'Transaction has already beeen completely refunded') === 0) {
            $this->paymentLogger->log(
                PaymentLogger::TYPE_BRAINTRE,
                'Already voided: ' . $paymentTransactionRefund->transaction->transaction_id,
                '',
                [
                    PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $paymentTransactionRefund->transaction->transaction_id,
                ],
                PaymentLogger::LEVEL_INFO
            );
        }
        return null;
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @param $paymentToken
     *
     * @return PaymentDetailOperation
     * @throws PaymentException
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function pay(PaymentInvoice $paymentInvoice, $paymentToken, $amount = null): PaymentDetailOperation
    {
        $payment = $this->paymentFactory->createByInvoice($paymentInvoice);
        $payment->safeSave();

        $this->logStart(
            'pay',
            [
                'paymentInvoiceUuid' => $paymentInvoice->uuid,
                'pmNonce'            => $paymentToken,
                'amount'             => $paymentInvoice->getAmountTotalWithRefund(),
            ]
        );

        if ($paymentInvoice->isSettleSubmit()) {
            $paymentGatewayResult = $this->braintreeGateway->authorizeWithSubmit($paymentInvoice->getAmountTotalWithRefund(), $paymentToken);
        } else {
            $paymentGatewayResult = $this->braintreeGateway->authorize($paymentInvoice->getAmountTotalWithRefund(), $paymentToken);
        }
        try {
            $paymentGatewayTransaction = $paymentGatewayResult->getTransaction();
        } catch (TransactionNotFoundException $e) {
            $paymentGatewayTransaction = null;
        }

        if (!$paymentGatewayTransaction) {
            $message = _t('site.payments', 'Your payment authorization failed. Please contact your financial institution for more information, or try another payment method.');
            throw new BraintreeException($message);
        }

        $braintreeAccount        = PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_BRAINTREE);
        $paymentAuthorizeAccount = $this->paymentAccountService->getUserPaymentAccount($paymentInvoice->user, PaymentAccount::ACCOUNT_TYPE_AUTHORIZE);
        /** @var $detailFrom PaymentDetail */
        [$detailFrom, $detailTo] = $this->paymentService->transferMoney($payment, $braintreeAccount, $paymentAuthorizeAccount, $paymentInvoice->getAmountTotalWithRefund(),
            PaymentDetail::TYPE_PAYMENT,
            'Payment via braintree for invoice:' . $paymentInvoice->uuid);

        /** @var PaymentTransaction $paymentTransaction */
        /** @var PaymentTransactionHistory $paymentTransactionHistory */
        [$paymentTransaction, $paymentTransactionHistory] = $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayTransaction, $detailFrom);

        $paymentTransaction->safeSave();
        $paymentTransactionHistory->transaction_id = $paymentTransaction->id;
        $paymentTransactionHistory->safeSave();

        $this->paymentService->setPayedInvoice($paymentInvoice, $detailFrom->paymentDetailOperation, $this->transactionStatusToInvoiceStatus($paymentGatewayTransaction->status));
        $payment->status = Payment::STATUS_PAID;
        $payment->safeSave();

        $this->logFinish('pay', \yii\helpers\VarDumper::dumpAsString($paymentGatewayResult));
        return $detailFrom->paymentDetailOperation;

    }

    /**
     * Convert vendor transaction status into invoice status
     *
     * @param string $paymentTransactionStatus
     * @return mixed|void
     */
    public function transactionStatusToInvoiceStatus($paymentTransactionStatus)
    {
        $map = [
            PaymentTransaction::STATUS_AUTHORIZED_EXPIRED       => PaymentInvoice::STATUS_EXPIRED,
            PaymentTransaction::STATUS_AUTHORIZED               => PaymentInvoice::STATUS_AUTHORIZED,
            PaymentTransaction::STATUS_FAILED                   => PaymentInvoice::STATUS_NEW,
            PaymentTransaction::STATUS_REFUNDED                 => PaymentInvoice::STATUS_REFUND,
            PaymentTransaction::STATUS_SETTLED                  => PaymentInvoice::STATUS_PAID,
            PaymentTransaction::STATUS_SUBMITTED_FOR_SETTLEMENT => PaymentInvoice::STATUS_AUTHORIZED,
            PaymentTransaction::STATUS_SETTLE_DECLINE           => PaymentInvoice::STATUS_NEW,
            PaymentTransaction::STATUS_VOIDED                   => PaymentInvoice::STATUS_VOID,
            PaymentTransaction::STATUS_SETTLING                 => PaymentInvoice::STATUS_AUTHORIZED,
        ];
        if (array_key_exists($paymentTransactionStatus, $map)) {
            return $map[$paymentTransactionStatus];
        }
        throw new InvalidArgumentException('Not found gateway transaction status: ' . $paymentTransactionStatus . '.');
    }

    /**
     * Settle payemnt operation
     *
     * @param PaymentDetailOperation $paymentOperation
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function submitForSettle(PaymentDetailOperation $paymentOperation)
    {
        $paymentTransaction = $paymentOperation->paymentTransaction;
        $transactionId      = $paymentTransaction->transaction_id;
        $this->logStart('settle', [$transactionId]);
        $paymentGatewayResult = $this->braintreeGateway->settle($transactionId);
        $this->paymentService->updateTransactionStatus($paymentTransaction, PaymentTransaction::STATUS_SUBMITTED_FOR_SETTLEMENT);
        $this->logFinish('settle', [$paymentGatewayResult]);
        return true;
    }

    /**
     * Payment gateway transaction
     *
     * @param $id
     * @return PaymentGatewayTransaction
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     */
    public function getTransaction($id): PaymentGatewayTransaction
    {
        return $this->braintreeGateway->getTransaction($id);
    }

    /**
     * @param array $ids
     * @return PaymentGatewayTransaction[]
     */
    public function getTransactions(array $ids): array
    {
        return $this->braintreeGateway->getTransactions($ids);
    }

    /**
     * Check is allowed partital refund
     *
     * @return mixed
     */
    public function checkAllowPartitalRefund()
    {
        // TODO: Implement checkAllowPartitalRefund() method.
    }

    public function getGateway(): PaymentGateway
    {
        return $this->braintreeGateway;
    }
}
