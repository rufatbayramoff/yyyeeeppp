<?php

namespace common\modules\payment\processors;

use common\components\exceptions\InvalidModelException;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\models\PaymentTransactionRefund;
use common\modules\payment\components\PaymentLogger;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\factories\PaymentFactory;
use common\modules\payment\factories\PaymentOperationFactory;
use common\modules\payment\factories\PaymentTransactionFactory;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\gateways\vendors\StripeGateway;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use InvalidArgumentException;

class StripeProcessor implements PaymentProcessorInterface
{
    use PaymentProcessorBaseTrait {

    }

    /** @var StripeGateway */
    public $stripeGateway;

    /** @var PaymentFactory */
    public $paymentFactory;

    /** @var PaymentTransactionFactory */
    public $paymentTransactionFactory;

    /** @var PaymentOperationFactory */
    public $paymentOperationFactory;

    /** @var PaymentService */
    public $paymentService;

    /** @var PaymentAccountService */
    public $paymentAccountService;

    /** @var PaymentLogger */
    public $paymentLogger;

    /** @var string */
    public $code = 'StripeProcessor';

    /**
     * @param StripeGateway $stripeGateway
     * @param PaymentFactory $paymentFactory
     * @param PaymentOperationFactory $paymentOperationFactory
     * @param PaymentService $paymentService
     * @param PaymentTransactionFactory $paymentTransactionFactory
     * @param PaymentAccountService $paymentAccountService
     */
    public function injectDependencies(
        StripeGateway $stripeGateway,
        PaymentFactory $paymentFactory,
        PaymentOperationFactory $paymentOperationFactory,
        PaymentService $paymentService,
        PaymentTransactionFactory $paymentTransactionFactory,
        PaymentAccountService $paymentAccountService
    ): void
    {
        $this->stripeGateway             = $stripeGateway;
        $this->paymentFactory            = $paymentFactory;
        $this->paymentOperationFactory   = $paymentOperationFactory;
        $this->paymentService            = $paymentService;
        $this->paymentTransactionFactory = $paymentTransactionFactory;
        $this->paymentAccountService     = $paymentAccountService;
    }


    public function generateClientToken(PaymentInvoice $paymentInvoice, string $logUuid = ''): string
    {
        $paymentTransaction = \Yii::createObject(PaymentTransaction::class);
        return $this->stripeGateway->generatePaymentToken(
            'Treatstock invoice ' . $paymentInvoice->uuid,
            '',
            self::getSuccessUrl($paymentInvoice, $logUuid),
            self::getCancelUrl($paymentInvoice, $logUuid),
            $paymentInvoice->getAmountTotal()
        );
    }

    public static function getSuccessUrl(PaymentInvoice $paymentInvoice, string $logUuid = '')
    {
        return param('server') . '/store/payment/checkout-success?vendor=stripe&paymentInvoiceUuid=' . $paymentInvoice->uuid . '&logUuid=' . $logUuid;
    }

    public static function getCancelUrl(PaymentInvoice $paymentInvoice, string $logUuid = '')
    {
        return param('server') . '/store/payment/checkout-failed?vendor=stripe&paymentInvoiceUuid=' . $paymentInvoice->uuid . '&logUuid=' . $logUuid;
    }

    /**
     * Cancel new order payment
     *
     * @param PaymentDetailOperation $paymentOperation
     * @param string $comment
     * @return void
     * @throws PaymentException
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     */
    public function cancel(PaymentDetailOperation $paymentOperation, $comment)
    {
        $payment            = $paymentOperation->payment;
        $paymentTransaction = $paymentOperation->paymentTransaction;
        $fromAccount        = $paymentOperation->fromPaymentDetail()->paymentAccount;
        $toAccount          = $paymentOperation->toPaymentDetail()->paymentAccount;
        $transactionId      = $paymentTransaction->transaction_id;
        $vendorTransaction  = $this->getTransaction($transactionId);
        $isReturned         = 0;

        if ($paymentTransaction->status === PaymentTransaction::STATUS_FAILED) {
            // Was failed
            throw new PaymentException('Cancel failed transaction is not enabled');
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_REFUNDED) {
            // Already refunded
            throw new PaymentException('Cancel refunded transaction is not enabled');
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_REQUIRES_CAPTURE) {
            $paymentGatewayResult = $this->stripeGateway->void($transactionId);
            if (!$paymentGatewayResult->isSuccess()) {
                throw new PaymentException('Failed void transaction');
            }
            $isReturned = 1;
        } elseif ($paymentTransaction->status === PaymentTransaction::STATUS_SUCCEEDED) {
            // Transaction was transfered to reserved account
            // Payment was transferred to reserved or etc...
            if ($vendorTransaction->status !== PaymentTransaction::STATUS_SUCCEEDED) {
                throw new InvalidModelException('Invalid vendor transaction status: ' . $vendorTransaction->status);
            }

            $paymentGatewayResult = $this->stripeGateway->refund($transactionId);
            if (!$paymentGatewayResult->isSuccess()) {
                throw new InvalidModelException('Failed refund transaction');
            }
            $paymentTransaction->refund_id = $paymentGatewayResult->getTransaction()->transactionId;
            $paymentTransaction->safeSave();

            $reservedAccount = $this->paymentAccountService->getUserPaymentAccount($toAccount->user, PaymentAccount::ACCOUNT_TYPE_RESERVED);

            [$detailFrom, $detailTo] = $this->paymentService->transferMoney(
                $payment,
                $reservedAccount,
                $fromAccount,
                $paymentTransaction->getMoneyAmount(),
                PaymentDetail::TYPE_REFUND,
                'Full refund from reserved');
            /* @var PaymentTransaction $paymentTransactionRefund */
            /* @var PaymentTransactionHistory $paymentTransactionRefundHistory */
            [$paymentTransactionRefunded, $paymentTransactionRefundedHistory] =
                $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayResult->getTransaction(), $detailTo);
            $paymentTransactionRefunded->type = PaymentTransaction::TYPE_REFUND;
            $paymentTransactionRefunded->safeSave();
            $paymentTransactionRefundedHistory->transaction_id = $paymentTransactionRefunded->id;
            $paymentTransactionRefundedHistory->safeSave();
            $isReturned = 1;
        }

        // Current pay position
        if ($isReturned) {
            $paymentOperation->payment->status                 = Payment::STATUS_CANCELLED;
            $paymentOperation->payment->paymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
        } else {
            $paymentOperation->payment->status                 = Payment::STATUS_FAILED;
            $paymentOperation->payment->paymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
        }
        $paymentOperation->payment->safeSave();
        $paymentOperation->payment->paymentInvoice->safeSave();
        return $isReturned;
    }


    /**
     * Refund accepted, make transactions
     *
     * @param PaymentTransactionRefund $paymentTransactionRefund
     * @param PaymentAccount $fromPaymentAccount
     * @return mixed
     * @throws PaymentException
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function refund(PaymentTransactionRefund $paymentTransactionRefund, PaymentAccount $fromPaymentAccount): ?PaymentDetailOperation
    {
        $paymentGatewayResult = $this->stripeGateway->refund($paymentTransactionRefund->transaction->transaction_id, $paymentTransactionRefund->amount);
        if ($paymentGatewayResult->isSuccess()) {
            $paymentTransactionRefund->transaction->refund_id = $paymentGatewayResult->getTransaction()->transactionId;
            $paymentTransactionRefund->transaction->safeSave();

            $stripeAccount = PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_STRIPE);
            $payment       = $paymentTransactionRefund->transaction->firstPaymentDetail->paymentDetailOperation->payment;
            /** @var $detailFrom PaymentDetail */

            [$detailFrom, $detailTo] = $this->paymentService->transferMoney($payment, $fromPaymentAccount, $stripeAccount, $paymentTransactionRefund->getMoneyAmount(),
                PaymentDetail::TYPE_REFUND,
                'Refund via transaction:' . $paymentTransactionRefund->id);

            /* @var PaymentTransaction $paymentTransaction */
            /* @var PaymentTransactionHistory $paymentTransactionHistory */
            [$paymentTransaction, $paymentTransactionHistory] =
                $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayResult->getTransaction(), $detailTo);
            $paymentTransaction->type = PaymentTransaction::TYPE_REFUND;
            $paymentTransaction->safeSave();
            $paymentTransactionHistory->transaction_id = $paymentTransaction->id;
            $paymentTransactionHistory->safeSave();
            $paymentTransactionRefund->setApproved();
            $paymentTransactionRefund->transaction_refund_id = $paymentTransaction->id;
            $paymentTransactionRefund->safeSave();

            return $detailFrom->paymentDetailOperation;
        } elseif (strpos($paymentGatewayResult->getMessage(), 'Transaction has already beeen completely refunded') === 0) {
            $this->paymentLogger->log(
                PaymentLogger::TYPE_STRIPE,
                'Already voided: ' . $paymentTransactionRefund->transaction->transaction_id,
                '',
                [
                    PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $paymentTransactionRefund->transaction->transaction_id,
                ],
                PaymentLogger::LEVEL_INFO
            );
        }
        return null;
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @param $paymentToken
     *
     * @return PaymentDetailOperation
     * @throws PaymentException
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function pay(PaymentInvoice $paymentInvoice, $paymentToken, $amount =null): PaymentDetailOperation
    {
        $payment = $this->paymentFactory->createByInvoice($paymentInvoice);
        $payment->safeSave();

        $this->logStart(
            'pay',
            [
                'paymentInvoiceUuid' => $paymentInvoice->uuid,
                'pmNonce'            => $paymentToken,
                'amount'             => $paymentInvoice->getAmountTotalWithRefund(),
            ]
        );

        $paymentGatewayResult      = $this->stripeGateway->authorize($paymentInvoice->getAmountTotalWithRefund(), $paymentToken);
        $paymentGatewayTransaction = $paymentGatewayResult->getTransaction();

        if (!$paymentGatewayTransaction) {
            throw new PaymentException('Stripe payment: ' . $paymentGatewayResult->getMessage());
        }

        $stripeAccount           = PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_STRIPE);
        $paymentAuthorizeAccount = $this->paymentAccountService->getUserPaymentAccount($paymentInvoice->user, PaymentAccount::ACCOUNT_TYPE_RESERVED);

        /** @var $detailFrom PaymentDetail */

        [$detailFrom, $detailTo] = $this->paymentService->transferMoney($payment, $stripeAccount, $paymentAuthorizeAccount, $paymentInvoice->getAmountTotalWithRefund(),
            PaymentDetail::TYPE_PAYMENT,
            'Payment via stripe for invoice:' . $paymentInvoice->uuid);

        /** @var PaymentTransaction $paymentTransaction */
        /** @var PaymentTransactionHistory $paymentTransactionHistory */
        [$paymentTransaction, $paymentTransactionHistory] = $this->paymentTransactionFactory->createByGatewayTransaction($paymentGatewayTransaction, $detailFrom);

        $paymentTransaction->safeSave();
        $paymentTransactionHistory->transaction_id = $paymentTransaction->id;
        $paymentTransactionHistory->safeSave();

        $this->paymentService->setPayedInvoice($paymentInvoice, $detailFrom->paymentDetailOperation, $this->transactionStatusToInvoiceStatus($paymentGatewayTransaction->status));
        $payment->status = Payment::STATUS_PAID;
        $payment->safeSave();

        $this->logFinish('pay', \yii\helpers\VarDumper::dumpAsString($paymentGatewayResult));
        return $detailFrom->paymentDetailOperation;
    }

    /**
     * Convert vendor transaction status into invoice status
     *
     * @param string $paymentTransactionStatus
     * @return mixed|void
     */
    public function transactionStatusToInvoiceStatus($paymentTransactionStatus)
    {
        $map = [
            PaymentTransaction::STATUS_REQUIRES_CAPTURE         => PaymentInvoice::STATUS_AUTHORIZED,
            PaymentTransaction::STATUS_SUCCEEDED                => PaymentInvoice::STATUS_PAID,
            PaymentTransaction::STATUS_VOIDED                   => PaymentInvoice::STATUS_VOID,
        ];
        if (array_key_exists($paymentTransactionStatus, $map)) {
            return $map[$paymentTransactionStatus];
        }
        throw new InvalidArgumentException('Not found gateway transaction status: ' . $paymentTransactionStatus . '.');
    }

    /**
     * Settle payemnt operation
     *
     * @param PaymentDetailOperation $paymentOperation
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function submitForSettle(PaymentDetailOperation $paymentOperation)
    {
        $paymentTransaction = $paymentOperation->paymentTransaction;
        $transactionId      = $paymentTransaction->transaction_id;
        $this->logStart('settle', [$transactionId]);
        $paymentGatewayResult = $this->stripeGateway->settle($transactionId);
        $this->paymentService->updateTransactionStatus($paymentTransaction, PaymentTransaction::STATUS_SUCCEEDED);
        $paymentOperation->payment->paymentInvoice->status = PaymentInvoice::STATUS_PAID;
        $paymentOperation->payment->paymentInvoice->safeSave();
        $this->logFinish('settle', [$paymentGatewayResult]);
        return true;
    }

    /**
     * Payment gateway transaction
     *
     * @param $id
     * @return PaymentGatewayTransaction
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     */
    public function getTransaction($id): PaymentGatewayTransaction
    {
        return $this->stripeGateway->getTransaction($id);

    }

    /**
     * @param array $ids
     * @return PaymentGatewayTransaction[]
     */
    public function getTransactions(array $ids): array
    {

        return $this->stripeGateway->getTransactions($ids);
    }

    /**
     * Check is allowed partital refund
     *
     * @return mixed
     */
    public function checkAllowPartitalRefund()
    {
        return false;
    }

    public function getGateway(): PaymentGateway
    {
        return $this->stripeGateway;
    }
}
