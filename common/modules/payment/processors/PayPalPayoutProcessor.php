<?php
/**
 * Date: 01.10.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\payment\processors;

use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\models\User;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\factories\PaymentFactory;
use common\modules\payment\factories\PaymentTransactionFactory;
use common\modules\payment\gateways\vendors\PayPalPayoutGateway;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use lib\money\Money;

/**
 * Class PayPalPayoutProcessor
 *
 * @package common\modules\payment\processors
 *
 * @property PaymentAccountService $paymentAccountService
 * @property PaymentFactory $paymentFactory
 * @property PayPalPayoutGateway $payPalPayoutGateway
 * @property PaymentService $paymentService
 * @property PaymentTransactionFactory $paymentTransactionFactory
 */
class PayPalPayoutProcessor
{
    public const MINIMUM_PAYOUT = 10;

    protected $paymentAccountService;

    protected $paymentFactory;

    protected $payPalPayoutGateway;

    protected $paymentService;

    protected $paymentTransactionFactory;

    public function injectDependencies(
        PaymentAccountService $paymentAccountService,
        PaymentFactory $paymentFactory,
        PayPalPayoutGateway $payPalPayoutGateway,
        PaymentService $paymentService,
        PaymentTransactionFactory $paymentTransactionFactory
    ): void
    {
        $this->paymentAccountService     = $paymentAccountService;
        $this->paymentFactory            = $paymentFactory;
        $this->payPalPayoutGateway       = $payPalPayoutGateway;
        $this->paymentService            = $paymentService;
        $this->paymentTransactionFactory = $paymentTransactionFactory;
    }

    /**
     * @param User $user
     * @param Money $withdrawMoney
     *
     * @return PaymentDetailOperation
     * @throws PaymentException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     */
    public function payOut(User $user, Money $withdrawMoney): PaymentDetailOperation
    {
        $tax            = PaymentInfoHelper::getTaxRequired($user);
        $taxAmountMoney = null;

        // add taxes before paypal payout
        // we do it here, because if user fill's in his taxes
        // anyway old taxes with 30% will be already saved.
        // but if user fills in after, and trys to payout, no problems, he will have 0% taxes
        if (!$tax->isEmpty()) {
            $taxAmountMoney = Money::create($tax->getTaxAmount($withdrawMoney->getAmount()), $withdrawMoney->getCurrency());
            $withdrawMoney  = Money::create($tax->getCleanAmount($withdrawMoney->getAmount()), $withdrawMoney->getCurrency());
        }
        // ---

        if ($withdrawMoney->getAmount() < self::MINIMUM_PAYOUT) {
            throw new PaymentException(_t(
                'site.payment',
                'The minimum payout amount is '.$withdrawMoney->getCurrency().'{minimun}',
                ['minimun' => self::MINIMUM_PAYOUT]
            ));
        }

        $amountNetIncome = $this->paymentAccountService->getUserMainAmount($user, $withdrawMoney->getCurrency());

        if (round($amountNetIncome->getAmount(), 2) < round($withdrawMoney->getAmount(), 2) || $withdrawMoney->getCurrency() !== $amountNetIncome->getCurrency()) {
            throw new PaymentException(_t(
                'site.payment',
                'Sorry, you cannot request payout more than {hasAmount}. Your request {outAmount}',
                ['hasAmount' => displayAsMoney($amountNetIncome), 'outAmount' => displayAsMoney($withdrawMoney)]
            ));
        }

        $payment = $this->paymentFactory->createPayment('Paypal payout');
        $payment->safeSave();

        $paymentMainAccount   = $this->paymentAccountService->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_MAIN,  $withdrawMoney->getCurrency());
        $paymentPayoutAccount = $this->paymentAccountService->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_PAYOUT,  $withdrawMoney->getCurrency());
        $paymentTaxAccount    = $this->paymentAccountService->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_TAX,  $withdrawMoney->getCurrency());

        // tax ---
        if ($taxAmountMoney) {
            $this->paymentService->transferMoney($payment, $paymentMainAccount, $paymentTaxAccount, $taxAmountMoney,
                PaymentDetail::TYPE_TAX,
                'Tax withholding'
            );
        }
        // ---

        $this->payPalPayoutGateway->init([
            'user' => $user
        ]);

        $paymentGatewayResult = $this->payPalPayoutGateway->authorize($withdrawMoney);

        /** @var $detailFrom PaymentDetail */
        [$detailFrom, $detailTo] = $this->paymentService->transferMoney($payment, $paymentMainAccount, $paymentPayoutAccount, $withdrawMoney,
            PaymentDetail::TYPE_PAYOUT,
            'Withdrawal ' . displayAsMoney($withdrawMoney) . ' of funds from the balance');

        /** @var PaymentTransaction $paymentTransaction */
        $paymentGatewayTransaction = $paymentGatewayResult->getTransaction();

        /** @var PaymentTransactionHistory $paymentTransactionHistory */
        [$paymentTransaction, $paymentTransactionHistory] = $this->paymentTransactionFactory->createByGatewayTransaction(
            $paymentGatewayTransaction,
            $detailFrom,
            PaymentTransaction::TYPE_MINUS
        );

        $paymentTransaction->safeSave();
        $paymentTransactionHistory->transaction_id = $paymentTransaction->id;
        $paymentTransactionHistory->safeSave();

        return $detailFrom->paymentDetailOperation;
    }


    /**
     * Update payout status from payout server
     *
     * @param $id
     * @param $transactionId
     * @param $transactionStatus
     * @throws PaymentException
     * @throws \yii\base\UserException
     */
    public function updateStatus($id, $transactionId, $transactionStatus): void
    {
        $pt = PaymentTransaction::find()->where(['id' => $id, 'type' => 'minus'])->one();
        if (!$pt) {
            throw new PaymentException('Payout transaction not found: ' . $id);
        }
        if ($pt->vendor != PaymentTransaction::VENDOR_PP_PAYOUT) {
            // Skip invalid vendor
            return;
        }

        if ($transactionId) {
            $pt->transaction_id = $transactionId;
        }
        if (!$transactionStatus) {
            return;
        }
        $pt->safeSave();

        $oldStatus                 = $pt->status;
        $paymentTransactionHistory = $this->paymentService->updateTransactionStatus($pt, $transactionStatus);
        // This flag is finish status
        $inEndStatus = in_array($oldStatus, [
            PaymentTransaction::STATUS_UP_SUCCESS,
            PaymentTransaction::STATUS_UP_RETURNED,
            PaymentTransaction::STATUS_UP_FAILED,
        ]);

        if (!$inEndStatus && ($transactionStatus === PaymentTransaction::STATUS_UP_SUCCESS)) {
            $payment    = $pt->firstPaymentDetail->paymentDetailOperation->payment;
            $detailFrom = $pt->firstPaymentDetail->paymentDetailOperation->toPaymentDetail()->paymentAccount;
            $detailTo   = $this->paymentAccountService->getPaypalAccount();
            $amount     = $pt->getMoneyAmount();
            [$paymentDetailFrom, $paymentDetailTo] = $this->paymentService->transferMoney($payment, $detailFrom, $detailTo, $amount, PaymentDetail::TYPE_PAYOUT,
                'Payout success completed');
            $paymentTransactionHistory->payment_detail_id = $paymentDetailTo->id;
            $paymentTransactionHistory->safeSave();
        } elseif (!$inEndStatus && (in_array($transactionStatus, [PaymentTransaction::STATUS_UP_RETURNED, PaymentTransaction::STATUS_UP_FAILED]))) {
            $payment    = $pt->firstPaymentDetail->paymentDetailOperation->payment;
            $detailFrom = $pt->firstPaymentDetail->paymentDetailOperation->toPaymentDetail()->paymentAccount;
            $detailTo   = $pt->firstPaymentDetail->paymentDetailOperation->fromPaymentDetail()->paymentAccount;
            $amount     = $pt->getMoneyAmount();
            [$paymentDetailFrom, $paymentDetailTo] = $this->paymentService->transferMoney($payment, $detailFrom, $detailTo, $amount, PaymentDetail::TYPE_PAYOUT,
                'Payout failed');
            $paymentTransactionHistory->payment_detail_id = $paymentDetailTo->id;
            $paymentTransactionHistory->safeSave();
        }
    }
}