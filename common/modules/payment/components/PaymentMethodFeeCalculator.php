<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 10.12.18
 * Time: 15:20
 */

namespace common\modules\payment\components;

use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use lib\money\Money;

/**
 * Class PaymentMethodFeeCalculator
 */
class PaymentMethodFeeCalculator extends \yii\base\BaseObject
{
    public function calcFee(PaymentInvoice $paymentInvoice, $paymentMethod): ?Money
    {
        if ($paymentMethod === PaymentTransaction::VENDOR_BRAINTREE || $paymentMethod === PaymentTransaction::VENDOR_STRIPE) {
            return Money::create(round($paymentInvoice->getAmountTotal()->getAmount() / 100 * 3.1, 2), $paymentInvoice->currency);
        }

        return null;
    }
}