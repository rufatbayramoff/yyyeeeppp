<?php

namespace common\modules\payment\components;

use common\models\Company;
use common\models\PaymentAccount;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionRefund;
use common\models\Preorder;
use common\models\User;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\processors\BankTransferPaymentProcessor;
use common\modules\payment\processors\BonusBalanceProcessor;
use common\modules\payment\processors\BraintreeProcessor;
use common\modules\payment\processors\PaymentProcessorInterface;
use common\modules\payment\processors\StripeProcessor;
use common\modules\payment\processors\TreatstockBalanceProcessor;
use yii\base\InvalidConfigException;

/**
 * BonusCalculator
 */
class BonusCalculator
{
    public const DISALLOW_USERS = [
        27463
    ];

    public static function isAllowBonusForUser(?User $user): bool
    {
        if (!$user) {
            return true;
        }
        if (in_array($user->id, self::DISALLOW_USERS)) {
            return false;
        }
        return true;
    }

    public static function calcInstantOrder(float $percent, float $feePrice, ?User $user = null): float
    {
        if (!self::isAllowBonusForUser($user)) {
            return 0;
        }
        return round($percent * $feePrice / 100, 2);
    }

    public static function calcInstantOrderDirectService(Company $company, float $feePrice, ?User $user = null): float
    {
        if (!self::isAllowBonusForUser($user)) {
            return 0;
        }
        return round($company->getCashbackPercentPlus() * $feePrice / 100, 2);
    }

    public static function calcQuote(Company $company, float $feePrice, ?User $user = null): float
    {
        if (!self::isAllowBonusForUser($user)) {
            return 0;
        }
        return round($company->cashback_percent * $feePrice / 100, 2);
    }

    public static function calcInstantPayment(Company $company, float $feePrice, ?User $user = null): float
    {
        if (!self::isAllowBonusForUser($user)) {
            return 0;
        }
        return round($company->getCashbackPercentPlus() * $feePrice / 100, 2);
    }

    public static function calcAdditionalService(Company $company, float $feePrice, ?User $user = null): float
    {
        if (!self::isAllowBonusForUser($user)) {
            return 0;
        }
        return round($company->getCashbackPercentPlus() * $feePrice / 100, 2);
    }

    public static function calcProductSales(Company $company, float $feePrice, ?User $user = null): float
    {
        if (!self::isAllowBonusForUser($user)) {
            return 0;
        }
        return round($company->getCashbackPercentPlus() * $feePrice / 100, 2);
    }
}