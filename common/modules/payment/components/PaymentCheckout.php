<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.08.18
 * Time: 10:54
 */

namespace common\modules\payment\components;

use common\models\PaymentAccount;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionRefund;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\processors\BankTransferPaymentProcessor;
use common\modules\payment\processors\BonusBalanceProcessor;
use common\modules\payment\processors\BraintreeProcessor;
use common\modules\payment\processors\PaymentProcessorInterface;
use common\modules\payment\processors\StripeProcessor;
use common\modules\payment\processors\TreatstockBalanceProcessor;
use yii\base\InvalidConfigException;

/**
 * PaymentCheckout - decorator for payment processor with specified vendor
 */
class PaymentCheckout implements PaymentProcessorInterface
{
    /** @var PaymentProcessorInterface PaymentProcessorInterface */
    public $paymentProcessor;

    /** @var string */
    public $vendor;

    /**
     * created facade with specified adapter
     *
     * @param string $vendor
     * @param PaymentProcessorInterface|null $paymentProcessor
     * @throws InvalidConfigException
     */
    public function __construct(string $vendor, PaymentProcessorInterface $paymentProcessor = null)
    {
        $this->vendor           = $vendor;
        $this->paymentProcessor = $paymentProcessor ?: $this->getPaymentVendor($vendor);
    }


    /**
     * @param $vendor
     * @return PaymentProcessorInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getPaymentVendor($vendor): PaymentProcessorInterface
    {
        $vendorMap = [
            PaymentTransaction::VENDOR_TS            => TreatstockBalanceProcessor::class,
            PaymentTransaction::VENDOR_BONUS         => BonusBalanceProcessor::class,
            PaymentTransaction::VENDOR_STRIPE        => StripeProcessor::class,
            PaymentTransaction::VENDOR_BRAINTREE     => BraintreeProcessor::class,
            PaymentTransaction::VENDOR_BANK_TRANSFER => BankTransferPaymentProcessor::class,
        ];
        if (!array_key_exists($vendor, $vendorMap)) {
            throw new InvalidConfigException('Can`t create payment processor: ' . $vendor);
        }
        return \Yii::createObject($vendorMap[$vendor]);
    }


    public function pay(PaymentInvoice $paymentInvoice, $paymentToken, $amount=null): PaymentDetailOperation
    {
        return $this->paymentProcessor->pay($paymentInvoice, $paymentToken, $amount);
    }

    /**
     * Get raw payment gateway transaction
     *
     * @param $id
     * @return PaymentGatewayTransaction
     */
    public function getTransaction($id): PaymentGatewayTransaction
    {
        return $this->paymentProcessor->getTransaction($id);
    }

    public function getTransactions(array $transactionsList): array
    {
        return $this->paymentProcessor->getTransactions($transactionsList);
    }

    public function generateClientToken(PaymentInvoice $paymentInvoice, string $logUuid = ''): string
    {
        return $this->paymentProcessor->generateClientToken($paymentInvoice, $logUuid);
    }

    /**
     * get vendor code
     *
     * @return string
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param PaymentDetailOperation $paymentOperation
     * @param string $comment
     * @return int|void
     */
    public function cancel(PaymentDetailOperation $paymentOperation, $comment)
    {
        return $this->paymentProcessor->cancel($paymentOperation, $comment);
    }

    public function submitForSettle(PaymentDetailOperation $paymentOperation)
    {
        return $this->paymentProcessor->submitForSettle($paymentOperation);
    }

    public function refund(PaymentTransactionRefund $paymentTransactionRefund, PaymentAccount $fromPaymentAccount): ?PaymentDetailOperation
    {
        return $this->paymentProcessor->refund($paymentTransactionRefund, $fromPaymentAccount);
    }

    public function checkAllowPartitalRefund()
    {
        return $this->paymentProcessor->checkAllowPartitalRefund();
    }

    public function transactionStatusToInvoiceStatus($status)
    {
        return $this->paymentProcessor->transactionStatusToInvoiceStatus($status);
    }

    public function getGateway(): PaymentGateway
    {
        return $this->paymentProcessor->getGateway();
    }
}