<?php

namespace common\modules\payment\components;


class PaymentUrlHelper
{
    public static function payInvoice($invoice, $schema = false)
    {
        return ($schema ? param('server') : '') . '/store/payment/pay-invoice?invoiceUuid=' . $invoice->uuid;
    }
}
