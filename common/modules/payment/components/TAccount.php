<?php
/**
 * User: nabi
 */

namespace common\modules\payment\components;

use common\components\PaymentExchangeRateFacade;
use common\models\PaymentDetail;
use common\models\PaymentExchangeRate;
use lib\money\Money;

/**
 * Class TAccount
 * TAccount for double entry logic
 *
 * value object, once created cannot be changed.
 *
 * @package common\modules\payment\components
 */
class TAccount
{
    /**
     * user account id
     *
     * @var int
     */
    private $userId;

    /**
     * description for given t account transaction
     *
     * @var string
     */
    private $description;

    /**
     * type, based on PaymentDetail::TYPE_PAYMENT
     *
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $rateId;

    /**
     * payment transaction id
     *
     * @var int
     */
    private $transactionId;

    /**
     * @param $userId
     * @param string $description
     * @param string $type
     * @param PaymentExchangeRate|null $rate
     * @return TAccount
     * @internal param null $rateId
     */
    public static function create($userId, $description = '', $type = PaymentDetail::TYPE_PAYMENT, PaymentExchangeRate $rate = null)
    {
        $rate = is_null($rate) ? PaymentExchangeRateFacade::getCurrentRate() : $rate;
        return new TAccount($userId, $description, $type, $rate);
    }

    /**
     * TAccount constructor.
     *
     * @param $userId
     * @param string $description - description
     * @param string $type
     * @param PaymentExchangeRate $rate
     * @internal param string $description
     */
    private function __construct($userId, $description, $type, PaymentExchangeRate $rate)
    {
        $this->userId = $userId;
        $this->description = $description;
        $this->type = $type;
        $this->rateId = $rate->id;
    }

    /**
     * get details for payment_detail table
     * used in PaymentDubleEntry
     *
     * @see PaymentDoubleEntry::transaction()
     * @return array
     */
    public function getDetails()
    {
        return [
            'user_id'           => $this->userId,
            'rate_id'           => $this->rateId,
            "description"       => $this->description,
            'type'              => $this->type,
            'transaction_id'    => $this->transactionId
        ];
    }
}