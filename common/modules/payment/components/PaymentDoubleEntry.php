<?php
/**
 * User: nabi
 */

namespace common\modules\payment\components;

use common\components\PaymentExchangeRateFacade;
use common\models\Payment;
use common\models\PaymentDetail;
use common\models\PaymentExchangeRate;
use common\models\PaymentTransaction;
use lib\money\Currency;
use lib\money\Money;
use yii\base\UserException;
use \yii\helpers\ArrayHelper;
use lib\payment\exception\PaymentManagerException;

/**
 * Class PaymentDoubleEntry
 *
 * Usage example with new payment:
 * <code>
 *  $payment = PaymentDoubleEntry::init()->addPayment([
 *      'created_at'  => dbexpr("NOW()"),
 *      'description' => 'Order #' . $invoice->order_id,
 *      'status'      => StoreOrder::PAYMENT_NEW
 *  ])->transaction(
 *      $from = TAccount::create(1001),
 *      $to = TAccount::create(1001),
 *      Money::usd(100),
 *      null
 *  )->submit();
 * </code>
 *
 * If payment already exists (payment table)
 * use:
 * <code>
 *   PaymentDoubleEntry::init()->withPayment($payment)->transaction(...)->submit()
 * </code>
 *
 * @package common\modules\payment\components
 */
class PaymentDoubleEntry
{
    /**
     *
     * @var PaymentExchangeRate
     */
    private $rate;
    /**
     *
     * @var \yii\db\Transaction
     */
    private $dbTransaction;
    private $status = "new";
    private $parentId;

    /**
     * payment row to add
     *
     * @var array
     */
    private $paymentRow;

    /**
     *
     * @var Payment
     */
    private $paymentObj;

    /**
     * payment details to be added with current payment transaction
     *
     * @var array
     */
    private $paymentDetails = [];

    /**
     * should we check that amount is not equals to zero?
     * @var boolean
     */
    private $checkZeroAmount = true;

    /**
     * PaymentDoubleEntry constructor.
     *
     * @param PaymentExchangeRate $rate
     * @param bool $checkZeroAmount
     */
    private function __construct(PaymentExchangeRate $rate, $checkZeroAmount = true)
    {
        $this->rate = $rate;
        $this->checkZeroAmount = $checkZeroAmount;
    }

    /**
     *
     * @param PaymentExchangeRate $rate
     * @param bool $checkZero
     * @return PaymentDoubleEntry
     */
    public static function init(PaymentExchangeRate $rate = null, $checkZero = true)
    {
        $rate = is_null($rate) ? PaymentExchangeRateFacade::getCurrentRate() : $rate;
        return new PaymentDoubleEntry($rate, $checkZero);
    }

    /**
     * move money from one account to another
     * if payment transaction specified, transaction is bined
     * to gateway transaction(braintree,paypal,thingiverse and etc.)
     * currently transaction id is for payouts (paypal)
     *
     * @param TAccount $from
     * @param TAccount $to
     * @param Money $amount
     * @param PaymentTransaction|null $paymentTransaction
     * @return $this
     * @throws PaymentManagerException
     * @throws \yii\web\NotFoundHttpException
     */
    public function transaction(
        TAccount $from,
        TAccount $to,
        Money $amount,
        PaymentTransaction $paymentTransaction = null
    ) {
        $fromDetails = $from->getDetails();
        $toDetails = $to->getDetails();

        $fromDetails['original_amount'] = -1 * $amount->getAmount();
        $fromDetails['original_currency'] = $amount->getCurrency();

        $toDetails['original_amount'] =  $amount->getAmount();
        $toDetails['original_currency'] = $amount->getCurrency();

        if ($paymentTransaction) {
            $fromDetails['transaction_id'] = $paymentTransaction->id;
            $toDetails['transaction_id'] = $paymentTransaction->id;
        }

        $this->addDetails($fromDetails)
             ->addDetails($toDetails);
        return $this;
    }

    /**
     * Add payment info and later write double entry for that payment
     *
     * @param  array $data
     * @return PaymentDoubleEntry
     */
    public function addPayment($data)
    {
        $paymentRow =  ArrayHelper::merge(
            [
                "created_at" => dbexpr("NOW()"),
                "status" => $this->status,
                "parent_id" => $this->parentId
            ], $data
        );
        $this->paymentRow = $paymentRow;
        return $this;
    }

    /**
     * write double entry to existing payment
     *
     * @param Payment $payment
     * @return PaymentDoubleEntry
     */
    public function withPayment(Payment $payment)
    {
        $this->paymentObj = $payment;
        return $this;
    }

    /**
     * add payment details
     *
     * @param  array $dataIn
     * @return PaymentDoubleEntry
     * @throws PaymentManagerException
     * @throws \yii\web\NotFoundHttpException
     */
    public function addDetails($dataIn)
    {
        $data = $this->validateAmount($dataIn);
        $data['rate_id'] = isset($data['rate_id']) ? $data['rate_id'] : $this->rate->id;
        $data['created_at'] = dbexpr("NOW()");
        $data['description'] = !empty($data['description']) ? $data['description'] : "";
        $this->paymentDetails[] = $data;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getDetails()
    {
        return $this->paymentDetails;
    }

    /**
     * submit collected data to database
     *
     * @param  string $status
     * @return Payment
     * @throws UserException
     * @throws \yii\db\Exception
     * @throws PaymentManagerException
     */
    public function submit($status = '')
    {
        if (count($this->paymentDetails) === 0) {
            return null;
        }
        $this->preValidatePayment();
        $this->dbTransaction = app("db")->beginTransaction();
        try {
            $payment = $this->paymentObj ? $this->paymentObj : Payment::addRecord($this->paymentRow);
            foreach ($this->paymentDetails as $k => $detail) {
                $detail['payment_id'] = $payment->id;
                $this->paymentDetails[$k] = PaymentDetail::addRecord($detail);
            }
            if (!empty($status)) {
                $payment->status = $status;
                $payment->safeSave();
            }
            $this->validatePayment($payment->id);
            $this->dbTransaction->commit();
            $this->paymentObj = $payment;
            return $payment;
        } catch (\Exception $ex) {
            $this->dbTransaction->rollBack();
            $data = var_export($this->paymentDetails, true);
            \Yii::warning($data, "payment_manager");
            logException($ex, "payment_manager");
            throw new UserException("Payment exception. Please try again.");
        }
    }

    /**
     * validate before working with db
     *
     * @return boolean
     * @throws PaymentManagerException
     */
    private function preValidatePayment()
    {
        $sum = 0;
        if (empty($this->paymentDetails)) {
            throw new PaymentManagerException("No payment details specified");
        }
        foreach ($this->paymentDetails as $k => $v) {
            $sum = $sum + floatval($v['amount']);
        }
        if ($sum != 0) {
            throw new PaymentManagerException("Payment pre-validate error, sum must be zero, got " . $sum);
        }
        return true;
    }

    /**
     * validate amount in payments, if original not set, assume that USD is used as default
     *
     * @param  array $data
     * @return array
     * @throws PaymentManagerException
     * @throws \yii\web\NotFoundHttpException
     */
    private function validateAmount($data)
    {
        if (!isset($data['original_amount'])) {
            $data['original_amount'] = $data['amount'];
            $data['original_currency'] = "USD";
        }
        if ($data['original_currency'] == "USD") {
            $amount = $data["original_amount"];
        } else {
            $rateObj = $this->rate;
            if (!empty($data['rate_id'])) {
                $rateObj = PaymentExchangeRate::tryFindByPk($data['rate_id'], "xRate not found " . $data['rate_id']);
            }
            $amount = $rateObj->convert($data['original_amount'], $data['original_currency'], Currency::USD);
        }
        if (empty($amount) && $this->checkZeroAmount) {
            throw new PaymentManagerException("Zero amount not allowed for payment");
        }
        $data['amount'] = $amount;
        return $data;
    }

    /**
     * validate payment details, total sum must be always = 0
     *
     * @param  int $paymentId
     * @return bool
     * @throws PaymentManagerException
     * @throws \yii\db\Exception
     */
    private function validatePayment($paymentId)
    {
        $command = app("db")->createCommand("SELECT sum(amount) FROM payment_detail WHERE payment_id=$paymentId");
        $sum = (int)$command->queryScalar();
        if ($sum!=0) {
            throw new PaymentManagerException("Payment validate error! Payment details sum must be zero, got " . $sum);
        }
        return true;
    }
}
