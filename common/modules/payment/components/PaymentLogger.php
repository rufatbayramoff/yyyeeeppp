<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.10.18
 * Time: 14:25
 */

namespace common\modules\payment\components;

use common\components\DateHelper;
use common\models\PaymentInvoice;
use common\models\PaymentLog;
use Yii;

class PaymentLogger
{
    public $payPageLogUuid ='';

    public const TYPE_TREATSTOCK     = 'treatstock';
    public const TYPE_BRAINTRE       = 'braintree';
    public const TYPE_STRIPE         = 'stripe';
    public const TYPE_PAYMENT_DETAIL = 'paymentDetail';
    public const TYPE_PAYPAL         = 'paypal';
    public const TYPE_STORE_ORDER    = 'storeOrder';

    public const LEVEL_DEBUG   = 'debug';
    public const LEVEL_INFO    = 'info';
    public const LEVEL_WARNING = 'warning';
    public const LEVEL_ERROR   = 'error';

    /**
     * Search markers list
     */
    public const MARKER_VENDOR_TRANSACTION_IDS       = 'vendor_transaction_ids';
    public const MARKER_VENDOR_TRANSACTION_ID        = 'vendor_transaction_id';
    public const MARKER_VENDOR_REFUND_TRANSACTION_ID = 'vendor_refund_transaction_id';


    public const MARKER_PAGE_LOG_UUID       = 'page_log_uuid';
    public const MARKER_AMOUNT              = 'amount';
    public const MARKER_PM_NONCE            = 'payment_method_nonce';
    public const MARKER_MERCHANT_ACCOUNT_ID = 'merchant_account_id';
    public const MARKER_PAYMENT_DETAIL_ID   = 'payment_detail_id';
    public const MARKER_PAYMENT_ID          = 'payment_id';


    /**
     * Log payment info
     *
     * @param $type
     * @param $commentString
     * @param $info
     * @param $markers
     * @param $level
     * @throws \yii\base\UserException
     */
    public function log($type, $commentString, $info, $markers, $level)
    {
        if (!is_array($markers)) {
            $markers = [];
        }
        if ($this->payPageLogUuid) {
            $markers[self::MARKER_PAGE_LOG_UUID] = $this->payPageLogUuid;
        }

        $paymentLog                 = new PaymentLog();
        $paymentLog->created_at     = DateHelper::now();
        $paymentLog->level          = $level;
        $paymentLog->type           = $type;
        $paymentLog->comment        = $commentString;
        $paymentLog->info           = $info;
        $paymentLog->search_markers = $markers;
        $paymentLog->safeSave();

        Yii::info(json_encode([
            'comment' => $commentString,
            'info'    => $info,
            'markers' => $markers,
            'level'   => $level
        ]), 'payment_' . $type);
    }
}