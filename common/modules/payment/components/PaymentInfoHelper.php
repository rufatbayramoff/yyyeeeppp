<?php

namespace common\modules\payment\components;

use common\models\PaymentAccount;
use common\models\CompanyService;
use common\models\StoreOrder;
use common\models\User;
use common\models\UserAddress;
use common\models\UserTaxInfo;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\taxes\Tax;

/**
 * Date: 28.11.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class PaymentInfoHelper
{
    /**
     * is paypal paid order?
     *
     * @param StoreOrder $storeOrder
     * @return bool
     */
    public static function isPaypalPaid(StoreOrder $storeOrder): bool
    {
        $transactions = $storeOrder->paymentTransactions;

        if (!$transactions) {
            return false;
        }

        $billInfo = $transactions[0]->findBillInfo();

        return strpos($billInfo, 'PayPal') !== false;
    }

    /**
     * get required tax for user based on his tax information and country location
     *
     * @param \common\models\base\User $user
     * @return Tax
     */
    public static function getTaxRequired(User $user)
    {
        // validate user tax info is ok
        $userTax = $user->userTaxInfo;

        if ($userTax) {
            if ($userTax->status === UserTaxInfo::STATUS_CONFIRMED) { // we have 0 taxes if confirmed
                $tax = new Tax(0);
            } else { // not confirmed, we set default rates
                $userCountry = UserAddress::findUserCountry($user->id);
                $tax         = $userCountry === 'US' ? new Tax(Tax::BACKUP_TAX) : new Tax(Tax::NRA_TAX);
            }
            // if we have personal tax rate, we will override all above
            if (!empty($userTax->rsa_tax_rate)) {
                $tax = new Tax($userTax->rsa_tax_rate);
            }
            $tax->userTaxStatus = $userTax->status;
        } else {
            $userCountry = UserAddress::findUserCountry($user->id);
            $tax         = $userCountry === 'US' ? new Tax(Tax::BACKUP_TAX) : new Tax(Tax::NRA_TAX);
        }

        return $tax;
    }

    /**
     *
     * @param int $orderId
     * @return string
     */
    public static function getAwardModelDescription($orderId)
    {
        return 'award for model #' . $orderId;
    }

    /**
     *
     * @param int $orderId
     * @return string
     */
    public static function getAwardPrintDescription($orderId)
    {
        return 'award for printing order #' . $orderId;
    }


    /**
     * Detect which vendor  to use
     *
     * @param StoreOrder $storeOrder
     * @return string
     */
    public static function detectVendor(StoreOrder $storeOrder)
    {   // only braintree processor
        return 'braintree';
        //        $billUsa = !$storeOrder->billAddress || $storeOrder->billAddress->country->iso_code == 'US';
        //        if ($billUsa) {
        //            return 'braintree';
        //        }
        //        return 'stripe';
    }


    public static function isPaymentPendingWarningNeed(StoreOrder $storeOrder, CompanyService $psMachine)
    {
        $transactionStatus = $storeOrder->primaryPaymentInvoice->getActivePaymentFirstTransaction() ? $storeOrder->primaryPaymentInvoice->getActivePaymentFirstTransaction()->status : '';
        return self::isPaypalPaid($storeOrder) && in_array(
                $transactionStatus,
                [
                    PaymentGatewayTransaction::STATUS_SETTLING,
                    PaymentGatewayTransaction::STATUS_SUBMITED_SETTLE,
                    PaymentGatewayTransaction::STATUS_SETTLING,
                ]
            ) && $psMachine->location->country->iso_code === 'IT';
    }


    /**
     * @param PaymentAccount $paymentAccount
     * @param null $fromDate
     * @param null $toDate
     * @return string
     */
    public static function getDetailsUrl($paymentAccount = null, $fromDate = null, $toDate = null, $sign = null): string
    {
        return '/payment/balance-report/details?user_id=' . ($paymentAccount->user->id ?? '') . '&dateFrom=' . $fromDate . '&dateTo=' . $toDate . '&sign=' .
            str_replace('+', '%2B', $sign);
    }
}