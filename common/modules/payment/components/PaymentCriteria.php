<?php
/**
 * User: nabi
 */

namespace common\modules\payment\components;

use common\modules\payment\services\InvoiceBankService;
use lib\money\Money;

/**
 * Class PaymentCriteria
 *
 * Usage example:
 *
 * <code>
 *  $criteria = PaymentCriteria::create()->setCountry('USA')->setIsThingiverse(false);
 *  $criteria->setPaymentAmount(Money::create(400, 'USD'));
 *  $criteria->setTreatstockBalance(Money::create(100, 'USD'));
 * </code>
 * Now we can find gateways for payment using:
 * <code>
 *  $gatewaysNames = new PaymentGatewayProvider()->getGatewaysNamesByCriteria($criteria);
 * </code>
 *
 * @package common\modules\payment
 */
class PaymentCriteria
{
    /**
     * @var string
     */
    private $country;

    /**
     * @var Money
     */
    private $treatstockBalance;

    /**
     * @var boolean
     */
    private $isThingiverse;
    /**
     * @var Money
     */
    private $paymentAmount;

    /**
     * @return PaymentCriteria
     */
    public static function create()
    {
        $self = new PaymentCriteria();
        return $self;
    }

    /**
     * @return Money|null
     */
    public function getPaymentAmount()
    {
        return $this->paymentAmount;
    }

    /**
     * @param Money $paymentAmount
     * @return $this
     */
    public function setPaymentAmount(Money $paymentAmount)
    {
        $this->paymentAmount = $paymentAmount;
        return $this;
    }

    /**
     * is order from thingiverse
     *
     * @return bool
     */
    public function isThingiverse(): bool
    {
        return $this->isThingiverse;
    }

    /**
     * is order from thingiverse
     *
     * @param bool $isThingiverse
     * @return $this
     */
    public function setIsThingiverse(bool $isThingiverse)
    {
        $this->isThingiverse = $isThingiverse;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * get set balance for criteria
     *
     * @return Money
     */
    public function getTreatstockBalance(): Money
    {
        return $this->treatstockBalance??Money::create(0, 'USD');
    }

    /**
     * set current user balance
     *
     * @param Money $treatstockBalance
     * @return $this
     */
    public function setTreatstockBalance(Money $treatstockBalance)
    {
        $this->treatstockBalance = $treatstockBalance;
        return $this;
    }

    /**
     * check if current payment amount can be paid by invoice
     * payment amount can be in any currency
     *
     * @return bool
     */
    public function canPayByInvoice()
    {
        if (!$this->paymentAmount) {
            return false;
        }
        if ($this->getPaymentAmount()->convertTo('USD')->getAmount() >= InvoiceBankService::getInvoiceFromUsd()) {
            return true;
        }
        return false;
    }

    /**
     * check if current payment amount can be paid by treatstock balance amount
     * can accept different currencies
     *
     * @return bool
     */
    public function canPayByTreatstockBalance()
    {
        if(!$this->treatstockBalance){
            return false;
        }
        if($this->getTreatstockBalance()->asUsd()->getAmount() >= $this->getPaymentAmount()->asUsd()->getAmount())
            return true;
        return false;
    }
}