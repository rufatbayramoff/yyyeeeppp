<?php
/**
 * User: nabi
 */

namespace common\modules\payment\repositories;


use common\components\exceptions\AssertHelper;
use common\models\PaymentBankInvoice;

/**
 * Class InvoiceRepository
 * save created invoice with invoice items
 *
 * @package common\modules\payment\repositories
 */
class InvoiceBankRepository
{
    /**
     * @param PaymentBankInvoice $bankInvoice
     * @throws \Exception
     */
    public function save(PaymentBankInvoice $bankInvoice): void
    {
        $bankInvoice->safeSave();
    }
}