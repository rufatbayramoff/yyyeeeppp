<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.08.18
 * Time: 11:28
 */

namespace common\modules\payment\repositories;

use common\components\exceptions\AssertHelper;
use common\models\PaymentAccount;
use common\models\PaymentInvoice;

class PaymentAccountRepository
{
    public static $accountUserIdAndType = [];

    public function getAccountByUserIdAndType($userId, $type, $currency)
    {
        $key = $userId.'_'.$type.'_'.$currency;

        if (isset(self::$accountUserIdAndType[$key])) {
            return self::$accountUserIdAndType[$key];
        }

        $paymentAccount = PaymentAccount::find()
            ->where([
                'user_id'  => $userId,
                'type'     => $type,
                'currency' => $currency
            ])->one();

        if ($paymentAccount) {
            $this->putAccountByUserIdAndType($paymentAccount);
        }

        return $paymentAccount;
    }

    public function putAccountByUserIdAndType(PaymentAccount $account)
    {
        self::$accountUserIdAndType[$account->user_id . '_' . $account->type . '_' . $account->currency] = $account;
    }
}