<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.08.18
 * Time: 11:28
 */


namespace common\modules\payment\repositories;

use common\components\exceptions\AssertHelper;
use common\models\InstantPayment;
use common\models\PaymentInvoice;
use common\models\PaymentInvoicePaymentMethod;
use common\models\PaymentTransaction;
use common\models\Preorder;
use common\models\StoreOrder;
use common\models\StoreOrderPosition;
use common\models\StoreOrderPromocode;
use common\models\TsInternalPurchase;

class PaymentInvoiceRepository
{
    /**
     * @param PaymentInvoice $paymentInvoice
     * @throws \yii\base\Exception
     */
    public function save(PaymentInvoice $paymentInvoice): void
    {
        foreach ($paymentInvoice->paymentInvoiceItems as $invoiceItem) {
            AssertHelper::assertValidate($invoiceItem);
        }

        $paymentInvoice->safeSave();

        foreach ($paymentInvoice->paymentInvoiceItems as $invoiceItem) {
            $invoiceItem->safeSave();
        }

        foreach ($paymentInvoice->paymentInvoicePaymentMethods as $paymentMethod) {
            $paymentMethod->safeSave();
        }

        if ($paymentInvoice->affiliateAward) {
            $paymentInvoice->affiliateAward->safeSave();
        }
    }

    /**
     * @param PaymentInvoice[] $paymentInvoices
     *
     * @throws \yii\base\Exception
     */
    public function saveInvoicesArray(array $paymentInvoices): void
    {
        foreach ($paymentInvoices as $paymentInvoice) {
            $this->save($paymentInvoice);
        }
    }

    /**
     * Search for invoice in a group, with permissible payment method
     *
     * @param $invoiceGroupUuid
     * @param $paymentVendor
     *
     * @return PaymentInvoice|null
     */
    public function getNewInvoiceByGroupUuidPaymentMethod($invoiceGroupUuid, $paymentVendor): ?PaymentInvoice
    {
        if ($paymentVendor === PaymentTransaction::VENDOR_STRIPE_ALIPAY) {
            $paymentVendor = PaymentTransaction::VENDOR_STRIPE;
        }
        $query = PaymentInvoice::find()
            ->from(['pi' => PaymentInvoice::tableName()])
            ->innerJoin(['pipm' => PaymentInvoicePaymentMethod::tableName()], 'pipm.payment_invoice_uuid = pi.uuid')
            ->where([
                'and',
                ['=', 'pi.invoice_group_uuid', $invoiceGroupUuid],
                ['=', 'pi.status', PaymentInvoice::STATUS_NEW],
                ['=', 'pipm.vendor', $paymentVendor]
            ])
            ->groupBy('pi.uuid')
            ->orderBy(['pi.created_at' => SORT_DESC]);
        return $query->one();
    }

    /**
     * @param StoreOrder $storeOrder
     *
     * @return PaymentInvoice[]
     */
    public function getNewInvoiceByStoreOrder(StoreOrder $storeOrder): array
    {
        return PaymentInvoice::find()
            ->from(['pi' => PaymentInvoice::tableName()])
            ->where([
                'and',
                ['=', 'pi.store_order_id', $storeOrder->id],
                ['=', 'pi.status', PaymentInvoice::STATUS_NEW],
            ])
            ->orderBy(['pi.created_at' => SORT_DESC])
            ->all();
    }

    /**
     * @param StoreOrder $storeOrder
     *
     * @return PaymentInvoice[]
     */
    public function getNewInvoicePromoCodeByStoreOrder(StoreOrder $storeOrder): array
    {
        return PaymentInvoice::find()
            ->from(['pi' => PaymentInvoice::tableName()])
            ->innerJoin(['sop' => StoreOrderPromocode::tableName()], 'pi.uuid = sop.invoice_uuid')
            ->where([
                'and',
                ['=', 'pi.store_order_id', $storeOrder->id],
                ['=', 'pi.status', PaymentInvoice::STATUS_NEW],
            ])
            ->orderBy(['pi.created_at' => SORT_DESC])
            ->all();
    }

    /**
     * @param InstantPayment $instantPayment
     * @param $paymentMethod
     *
     * @return PaymentInvoice|null
     */
    public function getNewInvoiceByInstantPaymentAndPaymentMethod(InstantPayment $instantPayment, $paymentMethod): ?PaymentInvoice
    {
        return PaymentInvoice::find()
            ->from(['pi' => PaymentInvoice::tableName()])
            ->innerJoin(['pipm' => PaymentInvoicePaymentMethod::tableName()], 'pipm.payment_invoice_uuid = pi.uuid')
            ->where([
                'and',
                ['=', 'pi.instant_payment_uuid', $instantPayment->uuid],
                ['=', 'pi.status', PaymentInvoice::STATUS_NEW],
                ['=', 'pipm.vendor', $paymentMethod]
            ])
            ->orderBy(['pi.created_at' => SORT_DESC])
            ->one();
    }

    public function getNewInvoiceByTsInternalPurchaseAndPaymentMethod(TsInternalPurchase $tsInternalPurchase, $paymentMethod): ?PaymentInvoice
    {
        return PaymentInvoice::find()
            ->from(['pi' => PaymentInvoice::tableName()])
            ->innerJoin(['pipm' => PaymentInvoicePaymentMethod::tableName()], 'pipm.payment_invoice_uuid = pi.uuid')
            ->where([
                'and',
                ['=', 'pi.ts_internal_purchase_uid', $tsInternalPurchase->uid],
                ['=', 'pi.status', PaymentInvoice::STATUS_NEW],
                ['=', 'pipm.vendor', $paymentMethod]
            ])
            ->orderBy(['pi.created_at' => SORT_DESC])
            ->one();
    }

    /**
     * @param Preorder $preorder
     * @param $paymentMethod
     *
     * @return PaymentInvoice|null
     */
    public function getNewInvoiceByPreorderAndPaymentMethod(Preorder $preorder, $paymentMethod): ?PaymentInvoice
    {
        return PaymentInvoice::find()
            ->from(['pi' => PaymentInvoice::tableName()])
            ->innerJoin(['pipm' => PaymentInvoicePaymentMethod::tableName()], 'pipm.payment_invoice_uuid = pi.uuid')
            ->where([
                'and',
                ['=', 'pi.preorder_id', $preorder->id],
                ['=', 'pi.status', PaymentInvoice::STATUS_NEW],
                ['=', 'pipm.vendor', $paymentMethod]
            ])
            ->orderBy(['pi.created_at' => SORT_DESC])
            ->one();
    }

    /**
     * @param StoreOrderPosition $additionalService
     * @param $paymentMethod
     *
     * @return PaymentInvoice|null
     */
    public function getNewInvoiceByAdditionalServiceAndPaymentMethod(StoreOrderPosition $additionalService, $paymentMethod): ?PaymentInvoice
    {
        return PaymentInvoice::find()
            ->from(['pi' => PaymentInvoice::tableName()])
            ->innerJoin(['pipm' => PaymentInvoicePaymentMethod::tableName()], 'pipm.payment_invoice_uuid = pi.uuid')
            ->where([
                'and',
                ['=', 'pi.order_position_id', $additionalService->id],
                ['=', 'pi.status', PaymentInvoice::STATUS_NEW],
                ['=', 'pipm.vendor', $paymentMethod]
            ])
            ->orderBy(['pi.created_at' => SORT_DESC])
            ->one();
    }
}