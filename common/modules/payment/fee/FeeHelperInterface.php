<?php
/**
 * Created by mitaichik
 */

namespace common\modules\payment\fee;


use lib\money\Money;

interface FeeHelperInterface
{
    /**
     * Caclulate model fee for order
     * @param \common\models\StoreOrder $order
     * @return int
     */
    public function getModelFeeByOrder(\common\models\StoreOrder $order);

    /**
     * @param Money $printPrice
     * @return mixed
     */
    public function getPrintFeeFromPrice(Money $printPrice);

    /**
     * @param Money $printPrice
     * @return mixed
     */
    public function getTsCommonFee(Money $printPrice);

    /**
     * @return mixed
     */
    public function getPsMinFee();

    /**
     * @param Money $price
     * @param bool $considerMinFee
     * @return mixed
     */
    public function getPrintFeeUsd(Money $price, bool $considerMinFee = true);

    public function getManufacturingFeePercent();
}