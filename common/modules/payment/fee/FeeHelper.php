<?php

namespace common\modules\payment\fee;

use common\components\PaymentExchangeRateConverter;
use common\components\PaymentExchangeRateFacade;
use common\models\Company;
use common\models\Preorder;
use common\models\Ps;
use common\models\StoreOrder;
use \common\models\TaxCountry;
use common\models\UserAddress;
use lib\money\Currency;
use lib\money\Money;

/**
 *
 * helper with fee
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class FeeHelper implements FeeHelperInterface
{
    /**
     * ps print fee percent from printing price
     * for example if printing price 10, we give 11
     *
     * @var float
     */
    public $psFeePercent = 0.1;

    /**
     * min print fee we add to printing price
     *
     * @var float|int
     */
    public $psMinFee = 1;

    /**
     * Treatstock model3d fee
     * @var float
     */
    public $model3dFee = 0.30;

    public static $feeHelper;

    public function __construct()
    {
        $this->psFeePercent = floatval(param('ps_fee', 0));
        $this->psMinFee     = floatval(param('ps_min_fee_usd', 0));
        $this->psFeePercent = (float)app('setting')->get('store.ps_fee', $this->psFeePercent);
        $this->psMinFee     = (float)app('setting')->get('store.ps_min_fee_usd', $this->psMinFee);
    }

    /**
     * @param Money $printPrice
     * @return mixed
     */
    public static function printFee(Money $printPrice)
    {
        return (new self)->getTsCommonFee($printPrice);
    }

    /**
     * @return FeeHelper
     */
    public static function init()
    {
        if (self::$feeHelper) {
            return self::$feeHelper;
        }
        self::$feeHelper = new FeeHelper();
        return self::$feeHelper;
    }

    /**
     * @param Money $printPrice
     * @param bool $considerMinFee
     */
    public function getPrintFeeUsd(Money $printPrice, bool $considerMinFee = true): Money
    {
        $fee = $this->getTsCommonFee($printPrice);
        return $fee->convertTo(Currency::USD);
    }

    /**
     * calculate print fee to add for printing in order to minus it later
     *
     * @param Money $printPrice
     * @return mixed
     */
    public function getTsCommonFee(Money $printPrice, $considerMinFee = true): Money
    {
        if (empty($this->psFeePercent)) {
            return $this->getPsMinFee();
        }
        $printMinFeeUsd = $this->psMinFee;
        $psFeePercent   = $this->psFeePercent;
        if ($printPrice->getCurrency() === Currency::EUR) {
            $psFeePercent+=0.05;
            $printMinFeeUsd+= $printMinFeeUsd*0.05;
        }
        $printPriceFull = $printPrice->getAmount() * $psFeePercent;
        $printFee       = $considerMinFee
            ? max($printMinFeeUsd, $printPriceFull)
            : $printPrice;
        return Money::create($printFee, $printPrice->getCurrency());
    }

    public function getModel3dFee(Money $price): Money
    {
        return Money::create($price->getAmount() * $this->model3dFee, $price->getCurrency());
    }

    /**
     * @param Money $price
     * @return Money
     */
    public function getTsFee(Money $price): Money
    {
        if (empty($this->tsFeePercent)) {
            return Money::zero($price->getCurrency());
        }
        $feePercent = $this->tsFeePercent;
        $fee        = $price->getAmount() * $feePercent;
        return Money::create($fee, $price->getCurrency());
    }

    /**
     * get fee amount from print price
     *
     * @param Money $printPrice - print price with fee inside it.
     * @return float
     */
    public function getPrintFeeFromPrice(Money $printPrice): Money
    {
        if (empty($this->psFeePercent)) {
            return 0;
        }
        $amount = $printPrice->getAmount();
        $feePercent = $this->psFeePercent;
        $minPsFee = $this->psMinFee;
        if ($printPrice->getCurrency() === Currency::EUR) {
            $feePercent+=0.05;
            $minPsFee+=$minPsFee*0.05;
        }
        $r      = $feePercent * 100 * $amount / (100 + $feePercent * 100);
        $amount = round(max($r, $minPsFee), 2);

        return Money::create($amount, $printPrice->getCurrency());
    }

    /**
     *
     * @param StoreOrder $order
     * @return int
     */
    public function getModelFeeByOrder(StoreOrder $order)
    {
        $model3d = $order->getFirstItem()->model3dReplica->getSourceModel3d();
        if (!$model3d->getAuthor()) {
            return 0;
        }
        $modelAuthorId         = $model3d->getAuthor()->id;
        $modelAuthorCountryIso = UserAddress::findUserCountry($modelAuthorId);

        $fee = $this->getCountryFee($modelAuthorCountryIso);
        if ($fee == 0) {
            return 0;
        }
        return $fee / 100;
    }

    /**
     *
     * @param string $countryIso
     * @return int
     */
    private function getCountryFee($countryIso)
    {
        $taxRate = TaxCountry::findOne(['country' => $countryIso]);
        if ($taxRate) {
            $rate = $taxRate->rate_model;
        } else {
            $rate = TaxCountry::getDefaultRate(TaxCountry::RATE_MODEL);
        }
        return $rate;
    }

    public function getPsMinFee()
    {
        return (float)$this->psMinFee;
    }

    /**
     * has ps fee defined.
     *
     * @return bool
     */
    public function hasPsFee()
    {
        return !empty($this->psFeePercent) || !empty($this->psMinFee);
    }

    /**
     *
     * @param Money $totalPrice
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function getManufacturingFee(Money $totalPrice): Money
    {
        return $this->getTsCommonFee($totalPrice);
    }

    public function getQuoteFee(Money $totalPrice)
    {
        return $this->getTsCommonFee($totalPrice);
    }

    public function getInstantPaymentFee(Money $totalPrice)
    {
        return $this->getTsCommonFee($totalPrice);
    }

    /**
     * @return mixed
     */
    public function getManufacturingFeePercent()
    {
        return param('manufacturing_fee', 0.02);
    }

    /**
     * https://www.braintreepayments.com/en-fr/braintree-pricing
     *
     * @param $total
     * @return mixed
     */
    public static function getThingiverseFeeAmount($total)
    {
        return PaymentExchangeRateConverter::roundUp(($total * 0.039) + 0.3) +
            PaymentExchangeRateConverter::roundUp($total * 0.05);
    }
}
