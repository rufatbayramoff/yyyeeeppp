<?php
/**
 * Created by mitaichik
 */

namespace common\modules\payment\fee;

use common\models\StoreOrder;
use lib\money\Money;

class FeeHelperStub implements FeeHelperInterface
{
    /**
     * @inheritdoc
     */
    public function getModelFeeByOrder(StoreOrder $order)
    {
        return 0;
    }

    public function getPrintFeeFromPrice(Money $printPrice)
    {
        return 0;
    }

    public function getTsCommonFee(Money $printPrice)
    {
        return 0;
    }

    public function getPsMinFee()
    {
        return 0;
    }

    public function getPrintFeeUsd($price, bool $considerMinFee = true)
    {
        return 0;
    }

    public function getManufacturingFeePercent()
    {
        return 0;
    }
}