<?php


namespace common\modules\payment\widgets;


use common\models\PaymentInvoice;
use yii\base\Widget;

class FeeViewWidget extends Widget
{
    /**
     * @var PaymentInvoice[]
     */
    public $paidInvoices;

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        return $this->render('feeView', [
            'inlineCss'    => file_get_contents(__DIR__ . '/css/receipt.css'),
            'paidInvoices' => array_filter($this->paidInvoices, [$this, 'filterPaymentInvoice']),
        ]);
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @return bool
     */
    protected function filterPaymentInvoice(PaymentInvoice $paymentInvoice): bool
    {
        return $paymentInvoice->getCompanyFee() !== null;
    }
}