<?php

namespace common\modules\payment\widgets;

use common\models\PaymentInvoice;
use common\modules\payment\services\RefundService;
use yii\base\Widget;

class ReceiptViewWidget extends Widget
{
    /**
     * @var PaymentInvoice[]
     */
    public $paidInvoices;

    /**
     * Path to injected details view
     *
     * @var string
     */
    public $injectedAdditionalBuyerDetailsView = '';

    /**
     * Path to injected comment view
     *
     * @var string
     */
    public $injectedCommentView = '';


    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        return $this->render('receiptView', [
            'inlineCss' => file_get_contents(__DIR__ . '/css/receipt.css'),
            'refundService' => \Yii::createObject(RefundService::class),
            'paidInvoices' => $this->paidInvoices,
            'injectedAdditionalBuyerDetailsView' => $this->injectedAdditionalBuyerDetailsView,
            'injectedCommentView' => $this->injectedCommentView
        ]);
    }
}