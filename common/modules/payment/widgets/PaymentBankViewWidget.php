<?php


namespace common\modules\payment\widgets;


use Yii;
use yii\base\Widget;

class PaymentBankViewWidget extends Widget
{
    /**
     * @var \common\models\PaymentBankInvoice
     */
    public $bankInvoice;

    public $isPdf = false;

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $cssInline = $this->needCssPdf();
        return $this->render('baknkPaymentView', [
            'inlineCss'   => $cssInline,
            'bankInvoice' => $this->bankInvoice,
            'isPdf'       => $this->isPdf
        ]);
    }

    public function needCssPdf()
    {
        $bankInvoice = file_get_contents(__DIR__ . '/css/bankInvoice.css');
        if(!$this->isPdf) {
            return $bankInvoice;
        }
        $invoiceCss = file_get_contents(Yii::getAlias('@frontend') . '/web/css/invoice.css');
        return $bankInvoice.$invoiceCss;
    }
}