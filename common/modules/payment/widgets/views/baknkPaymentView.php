<?php
/**
 * @var $bankInvoice \common\models\PaymentBankInvoice
 * @var $isPdf boolean
 */

use common\models\PaymentInvoiceItem;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$paymentInvoice = $bankInvoice->paymentInvoice;
$items          = $paymentInvoice->getSortedPaymentInvoiceItems();
?>
<style>
    <?php echo $inlineCss?>
</style>

<div class="container invoice">
    <div class="row">
        <div class="col-sm-12">
            <div class="invoice__header">
                <img src="https://static.treatstock.com/static/images/ts-logo.svg" alt="Tratstock" class="invoice__logo" width="190px" height="24px">
                Invoice #<?php echo $bankInvoice->uuid; ?>
            </div>
        </div>
    </div>
    <div class="row m-b20">
        <?php if ($bankInvoice->ship_to): ?>
            <div class="col-xs-4">
                <h4 class="m-b0">Ship To:</h4>
                <p>
                    <?php echo $bankInvoice->ship_to; ?>
                </p>
            </div>
        <?php endif; ?>
        <div class="col-xs-4">
            <div class="table-responsive">
                <table class="table--invoice-data">
                    <tbody>
                    <tr>
                        <th>Issued Date:</th>
                        <td><?php echo Yii::$app->formatter->asDate($bankInvoice->created_at); ?></td>
                    </tr>
                    <tr>
                        <th>Due Date:</th>
                        <td><?php echo Yii::$app->formatter->asDate($bankInvoice->date_due); ?></td>
                    </tr>
                    <?php if ($paymentInvoice->storeOrder): ?>
                        <tr>
                            <th>PO #:</th>
                            <td><?php echo $paymentInvoice->storeOrder->id; ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <th>Currency:</th>
                        <td><?php echo $paymentInvoice->currency; ?></td>
                    </tr>
                    <tr>
                        <th>Customer #:</th>
                        <td><?php echo $paymentInvoice->user_id; ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="invoice__table">
                    <tbody>
                    <tr>
                        <th> #</th>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Subtotal</th>
                    </tr>
                    <?php foreach ($items as $k => $item): ?>
                        <tr>
                            <td><?php echo $item->pos; ?>.</td>
                            <td>
                                <b><?php echo $item->title; ?></b><br/>
                                <?php echo $item->description; ?>
                            </td>
                            <td><?php echo $item->qty ? app('formatter')->asDecimal($item->qty) : 1; ?></td>
                            <td><?php echo displayAsCurrency($item->total_line, $paymentInvoice->currency); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3">
                            <strong>Total</strong>
                        </td>
                        <th>
                            <?php echo displayAsMoney($paymentInvoice->getAmountTotalWithRefund()); ?>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <p>Please pay amount (within 10 days) to:</p>
            <h4 class="m-b0">Bank Details</h4>
            <?php echo nl2br($bankInvoice->bank_details); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <hr>
            <h4 class="m-b0">Company Address</h4>
            <?php echo nl2br($bankInvoice->payment_details); ?>
        </div>
    </div>
    <?php if (!$isPdf): ?>
        <?php if (!empty($bankInvoice->comment)): ?>
            <div class="receipt__payment"><p><?php echo \H($bankInvoice->comment); ?></p></div>
        <?php else: ?>
            <div class="receipt__payment no-print">
                <?php $form = ActiveForm::begin([
                    'action' => \yii\helpers\Url::toRoute(['/payment/invoice-bank/save-comment', 'uuid' => $bankInvoice->uuid])
                ]); ?>
                <div class="receipt__payment-details border-0 p-t0">
                    <?php echo $form->field($bankInvoice, 'comment')->textarea(['rows' => 2, 'class' => 'payment-details', 'placeholder' => 'Add details'])->label('') ?>

                    <div class="receipt__payment-btns">
                        <button class="receipt-btn receipt-btn--cancel" type="reset" name="paymentCancel"><?= _t('payment.receipt', 'Cancel'); ?></button>
                        <button class="receipt-btn receipt-btn--submit" type="submit" name="paymentCancel"><?= _t('payment.receipt', 'Submit'); ?></button>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        <?php endif; ?>
        <div class="no-print">
            <hr/>
            <a href="javascript:;" class="receipt-btn" type="button" name="Print" onclick="window.print()">
                <span class="ico ico--print"></span>
                <?= _t('payment.receipt', 'Print'); ?>

            </a>
            <a href="<?php echo Url::to(['download', 'uuid' => $bankInvoice->uuid]) ?>" class="receipt-btn" type="button"">
            <span class="ico"></span>
            <?= _t('payment.receipt', 'Download'); ?>

            </a>
            <?php
            if ($bankInvoice->storeOrderId) {
                ?>
                <a href="<?php
                echo param('server') . '/workbench/order/view/' . $bankInvoice->storeOrderId;
                ?>" class="receipt-btn" type="button""> <span class="ico"></span> <?= _t('payment.receipt', 'Back to order'); ?>
                </a>
            <?php } ?>
            <br/>
            <br/>
        </div>
    <?php endif; ?>
</div>