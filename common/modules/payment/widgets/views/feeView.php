<?php

use common\models\UserAddress;
use frontend\models\user\UserFacade;

/** @var $inlineCss string */
/** @var $paidInvoices \common\models\PaymentInvoice[] */
/** @var $this \yii\web\View */

$user = UserFacade::getCurrentUser();
?>

<style>
    <?=$inlineCss?>
</style>

<div class="receipt__header">
    <a href="https://www.treatstock.com/" class="receipt__logo" target="_blank">
        <img src="https://static.treatstock.com/static/images/logo_2x.png" width="190px" height="40px">
    </a>
</div>

<?php foreach ($paidInvoices as $invoice):?>
    <?php
        if(!$invoice->checkAccessForUser($user)) {
            continue;
        }
        $storeOrder = $invoice->storeOrder;
        $fee = $invoice->getCompanyFee();
    ?>
    <div class="receipt__body">
        <h2><?php echo _t('payment.receipt', 'Invoice'); ?>: <?php echo \H($invoice->feeUuid); ?></h2>

        <div class="receipt__common-data">
            <strong><?php echo _t('payment.receipt', 'Invoice Date'); ?>
                :</strong> <?php echo app('formatter')->asDate($invoice->created_at); ?>
            <br>
            <strong><?php echo _t('payment.receipt', 'Order Total'); ?>
                :</strong> <?php echo displayAsMoney($invoice->getAmountTotalWithRefund()); ?>
            <?php if ($transactionsFindBillInfo = $invoice->transactionsFindBillInfo()): ?>
                <br>
                <strong><?php echo _t('payment.receipt', 'Billed To'); ?>
                    :</strong> <?php echo $transactionsFindBillInfo; ?>
            <?php endif; ?>
        </div>

        <table class="receipt-table receipt__info">
            <tr>
                <td class="receipt__info-company">
                    <h3>Treatstock Inc.</h3>
                    40 E Main St Suite 900 Newark DE 17911
                    <br>
                    <strong>EIN</strong> 30-0875412
                </td>

                <td class="receipt__info-ps">
                    <h3><?php echo _t('payment.receipt', 'Supplier'); ?></h3>
                    Treatstock Inc.
                    <br>
                </td>

                <td class="receipt__info-buyer">
                    <h3><?php echo _t('payment.receipt', 'Buyer'); ?></h3>
                    <?php if ($storeOrder && $storeOrder->hasDelivery() && !$storeOrder->isPickup() && $storeOrder->hasBillAddress()): ?>
                        <?php echo UserAddress::formatAddress($storeOrder->billAddress); ?> <br>
                    <?php endif; ?>
                    <?php if($storeOrder->currentAttemp && $storeOrder->currentAttemp->ps):?>
                        <strong>Company name</strong>: <?php echo \H($storeOrder->currentAttemp->ps->title); ?>
                        <strong>Company id</strong>: <?php echo \H($storeOrder->currentAttemp->ps->id); ?>
                    <?php endif;?>
                </td>
            </tr>
        </table>

        <table class="receipt-table receipt-items">
            <tbody>
            <tr>
                <th class="receipt-items__model"><?= _t('payment.receipt', 'Item'); ?></th>
                <th class="receipt-items__qty"><?= _t('payment.receipt', 'Qty'); ?></th>
                <th class="receipt-items__subtotal"><?= _t('payment.receipt', 'Subtotal'); ?></th>
            </tr>

            <tr>
                <td class="receipt-items__model">
                    Treatstock Service Fee
                </td>
                <td class="receipt-items__qty">
                </td>
                <td class="receipt-items__subtotal">
                    <?php echo displayAsMoney($fee); ?>
                </td>
            </tr>

            <tr>
                <td colspan="1"></td>
                <td colspan="2" class="receipt-items__total">
                    <table class="receipt-table receipt-total__table">
                        <tbody>
                        <tr class="receipt-total__total-row">
                            <td class="receipt-total__label"><?= _t('payment.receipt', 'Total'); ?></td>
                            <td class="receipt-total__value"><?php echo displayAsMoney($invoice->getAmountTotalWithRefund()); ?></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php endforeach; ?>