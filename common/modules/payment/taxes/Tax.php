<?php

namespace common\modules\payment\taxes;

use common\models\PaymentCurrency;
use lib\money\Currency;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Tax
{
    public $value = 0;

    public $userTaxStatus = 'new';
    /**
     * for foreigners
     */
    public const NRA_TAX = 30;

    /**
     * for usa residents
     */
    public const BACKUP_TAX = 28;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * no tax required
     *
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->value);
    }

    /**
     * get tax value
     *
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * get tax amount calculated from given amount
     *
     * @param int $amount
     * @return int
     * @throws \yii\base\Exception
     */
    public function getTaxAmount($amount): int
    {
        $taxRate = $this->getValue();
        $taxAmountOrig = $amount * $taxRate / 100;
        $taxAmount = PaymentCurrency::getCurrencyByIso(Currency::USD)->doRound($taxAmountOrig);
        return $taxAmount;
    }

    /**
     * get amount minus taxes
     *
     * @param int $amount
     * @return int
     * @throws \yii\base\Exception
     */
    public function getCleanAmount($amount): int
    {
        $taxAmount = $this->getTaxAmount($amount);
        return $amount - $taxAmount;
    }
}
