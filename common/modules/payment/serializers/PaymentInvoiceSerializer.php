<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 11.12.18
 * Time: 13:38
 */

namespace common\modules\payment\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\PaymentInvoicePaymentMethod;
use common\models\StoreOrderPromocode;
use lib\money\Money;

class PaymentInvoiceSerializer extends AbstractProperties
{
    public function getProperties()
    {
        return [
            PaymentInvoice::class => [
                'uuid',
                'currency',
                'amountTotal' => function (PaymentInvoice $invoice) {
                    return $invoice->getAmountTotalWithRefund()->getCurrency().' '.$invoice->getAmountTotalWithRefund()->getAmount();
                },
                'paymentFor' => function (PaymentInvoice $invoice) {
                    return $invoice->paymentFor();
                },
                'invoiceItems' => function (PaymentInvoice $invoice) {
                    if ($invoice->baseByPrintOrder()) {
                        $amountModel3d = $invoice->storeOrderAmount->getAmountModel3d();
                        $amountTax = $invoice->storeOrderAmount->getAmountTax();
                        $amountFee = $invoice->storeOrderAmount->getAmountFee();
                        $amountPrint = $invoice->storeOrderAmount->getAmountPrint();
                        $amountShipping = $invoice->storeOrderAmount->getAmountShipping();
                        $amountPostPrint = $invoice->storeOrderAmount->getAmountPostPrint();
                        $amountAmountAddon = $invoice->storeOrderAmount->getAmountAddon();
                        $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();
                        $amountAmountDiscount = $invoice->storeOrderAmount->getAmountDiscount();
                        $amountBonus  =$invoice->getAmountBonus();

                        $rows = [];

                        if ($amountModel3d && $amountModel3d->getAmount()) {
                            $rows[] = [
                                'type'       => 'model3d',
                                'title'      => _t('payment.modelPrice', 'Model Price'),
                                'total_line' => displayAsMoney($amountModel3d)
                            ];
                        }

                        if ($amountTax) {
                            $rows[] = [
                                'type'       => 'tax',
                                'title'      => _t('payment.modelPrice', 'Tax'),
                                'total_line' => displayAsMoney($amountTax)
                            ];
                        }

                        if ($amountFee) {
                            $rows[] = [
                                'type'       => 'fee',
                                'title'      => _t('payment.modelPrice', 'Service Fee'),
                                'total_line' => displayAsMoney($amountFee)
                            ];
                        }

                        if ($amountPrint) {
                            $rows[] = [
                                'type'       => 'print',
                                'title'      => _t('payment.modelPrice', 'Print Fee'),
                                'total_line' => displayAsMoney($amountPrint)
                            ];
                        }

                        if ($amountShipping) {

                            $rows[] = [
                                'type'       => 'shipping',
                                'title'      => _t('payment.modelPrice', 'Shipping Fee'),
                                'total_line' => displayAsMoney($amountShipping)
                            ];
                        }

                        if ($amountPostPrint) {
                            $rows[] = [
                                'type'       => 'postPrint',
                                'title'      => _t('payment.modelPrice', 'PS after print work fee'),
                                'total_line' => displayAsMoney($amountPostPrint)
                            ];
                        }

                        if ($amountAmountAddon) {
                            $rows[] = [
                                'type'       => 'amountAddon',
                                'title'      => _t('payment.modelPrice', 'Additional Services'),
                                'total_line' => displayAsMoney($amountAmountAddon)
                            ];
                        }

                        if ($amountPaymentMethodFee) {
                            $rows[] = [
                                'type'       => 'paymentMethodFee',
                                'title'      => _t('payment.modelPrice', 'Payment method fee'),
                                'total_line' => displayAsMoney($amountPaymentMethodFee)
                            ];
                        }

                        if ($amountAmountDiscount) {
                            $rows[] = [
                                'type'       => 'discount',
                                'title'      => _t('payment.modelPrice', 'Discount'),
                                'total_line' => displayAsMoney($amountAmountDiscount)
                            ];
                        }

                        if ($amountBonus) {
                            $rows[] = [
                                'type'       => 'bonus',
                                'title'      => _t('payment.modelPrice', 'Discount (Bonus)'),
                                'total_line' => displayAsMoney(Money::create(-$amountBonus->getAmount(), $invoice->currency))
                            ];
                        }
                        return $rows;
                    }

                    $items = $invoice->paymentInvoiceItems;
                    $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();

                    if ($amountPaymentMethodFee) {
                        $items[] = [
                            'type'       => 'paymentMethodFee',
                            'title'      => _t('payment.modelPrice', 'Payment method fee'),
                            'total_line' => displayAsMoney($amountPaymentMethodFee)
                        ];
                    }

                    return $items;
                },
                'amountBonusAccruedFromPs' => function (PaymentInvoice $invoice) {
                    return $invoice->getAmountBonusAccruedFromPs();
                },
                'amountBonusAccruedFromTs' => function (PaymentInvoice $invoice) {
                    return $invoice->getAmountBonusAccruedFromTs();
                },
                'accruedBonus' => function (PaymentInvoice $invoice) {
                    return $invoice->getAmountBonusAccrued();
                },
                'allowPromoCode',
                'storeOrderPromocode',
                'paymentInvoicePaymentMethods' => function (PaymentInvoice $invoice) {
                    $methods = $invoice->paymentInvoicePaymentMethods;
                    return $methods;
                }
            ],
            PaymentInvoicePaymentMethod::class => [
                'vendor'
            ],
            StoreOrderPromocode::class => [
                'amountTotal' => function (StoreOrderPromocode $storeOrderPromocode) {
                    return displayAsMoney($storeOrderPromocode->getAmountTotal());
                },
                'promocode' => function (StoreOrderPromocode $storeOrderPromocode) {
                    return $storeOrderPromocode->promocode->code;
                },
            ],
            PaymentInvoiceItem::class => [
                'type' => function (PaymentInvoiceItem $item) {
                    return 'item';
                },
                'title',
                'total_line' => function (PaymentInvoiceItem $item) {
                    return displayAsMoney($item->getLineTotal());
                }
            ]
        ];
    }
}