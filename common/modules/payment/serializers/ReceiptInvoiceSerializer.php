<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.05.19
 * Time: 10:26
 */

namespace common\modules\payment\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\PaymentInvoice;

/**
 *
 * @property array $properties
 */
class ReceiptInvoiceSerializer extends AbstractProperties
{
    public function getProperties()
    {
        return [
            PaymentInvoice::class => [
                'uuid',
                'additionalBuyerDetails' => 'paymentReceiptInvoiceComment.additional_buyer_details',
                'comment'                => 'paymentReceiptInvoiceComment.comment'
            ]
        ];
    }
}