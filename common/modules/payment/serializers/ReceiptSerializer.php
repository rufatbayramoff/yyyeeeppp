<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.05.19
 * Time: 11:29
 */
namespace common\modules\payment\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\PaymentReceipt;

class ReceiptSerializer  extends AbstractProperties
{
    public function getProperties()
    {
        return [
            PaymentReceipt::class => [
                'id',
                'orderId' => 'order_id'
            ]
        ];
    }
}