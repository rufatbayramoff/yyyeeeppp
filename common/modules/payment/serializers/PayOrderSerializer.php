<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 11.12.18
 * Time: 13:38
 */

namespace common\modules\payment\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\Model3dReplica;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\PaymentInvoicePaymentMethod;
use common\models\StoreOrder;
use common\models\StoreOrderPromocode;
use common\models\UserAddress;
use common\services\StoreOrderService;
use frontend\controllers\catalog\actions\serializers\Model3dSerializer;
use frontend\models\ps\PsFacade;
use lib\delivery\delivery\DeliveryFacade;
use Yii;

class PayOrderSerializer extends AbstractProperties
{
    public function getProperties()
    {
        return [
            StoreOrder::class                  => [
                'id'                      => 'id',
                'isPickupDelivery'        => function (StoreOrder $storeOrder) {
                    return DeliveryFacade::isPickupDelivery($storeOrder);
                },
                'primaryPaymentInvoice',
                'shipAddress',
                'model3d'                 => function (StoreOrder $storeOrder) {
                    return $storeOrder->getFirstReplicaItem();
                },
                'hasDelivery'             => function (StoreOrder $storeOrder) {
                    return $storeOrder->hasDelivery();
                },
                'machine'                 => function (StoreOrder $storeOrder) {

                    $machine = $storeOrder->currentAttemp->machine;

                    if ($machine) {
                        return [
                            'layerResolution' => PsFacade::getPrinterLayerResolution($machine)
                        ];
                    }

                    return null;
                },
            ],
            PaymentInvoice::class              => [
                'uuid',
                'currency',
                'accruedBonus' => function (PaymentInvoice $invoice) {
                    return $invoice->getAmountBonusAccrued()?$invoice->getAmountBonusAccrued()->getAmount():null;
                },
                'amountTotal'  => function (PaymentInvoice $invoice) {
                    return $invoice->getAmountTotalWithRefund()->getCurrency() . ' ' . $invoice->getAmountTotalWithRefund()->getAmount();
                },
                'invoiceItems' => function (PaymentInvoice $invoice) {

                    if ($invoice->baseByPrintOrder()) {
                        $amountModel3d          = $invoice->storeOrderAmount->getAmountModel3d();
                        $amountTax              = $invoice->storeOrderAmount->getAmountTax();
                        $amountFee              = $invoice->storeOrderAmount->getAmountFee();
                        $amountPrint            = $invoice->storeOrderAmount->getAmountPrint();
                        $amountShipping         = $invoice->storeOrderAmount->getAmountShipping();
                        $amountPostPrint        = $invoice->storeOrderAmount->getAmountPostPrint();
                        $amountAmountAddon      = $invoice->storeOrderAmount->getAmountAddon();
                        $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();
                        $amountAmountDiscount   = $invoice->storeOrderAmount->getAmountDiscount();

                        $rows = [];

                        if ($amountModel3d && $amountModel3d->getAmount()) {
                            $rows[] = [
                                'type'       => 'model3d',
                                'title'      => _t('payment.modelPrice', 'Model Price'),
                                'total_line' => displayAsMoney($amountModel3d)
                            ];
                        }

                        if ($amountTax) {
                            $rows[] = [
                                'type'       => 'tax',
                                'title'      => _t('payment.modelPrice', 'Tax'),
                                'total_line' => displayAsMoney($amountTax)
                            ];
                        }

                        if ($amountFee) {
                            $rows[] = [
                                'type'       => 'fee',
                                'title'      => _t('payment.modelPrice', 'Service Fee'),
                                'total_line' => displayAsMoney($amountFee)
                            ];
                        }

                        if ($amountPrint) {
                            $rows[] = [
                                'type'       => 'print',
                                'title'      => _t('payment.modelPrice', 'Print Fee'),
                                'total_line' => displayAsMoney($amountPrint)
                            ];
                        }

                        if ($amountShipping) {
                            $rows[] = [
                                'type'       => 'shipping',
                                'title'      => _t('payment.modelPrice', 'Shipping Fee'),
                                'total_line' => displayAsMoney($amountShipping)
                            ];
                        }

                        if ($amountPostPrint) {
                            $rows[] = [
                                'type'       => 'postPrint',
                                'title'      => _t('payment.modelPrice', 'PS after print work fee'),
                                'total_line' => displayAsMoney($amountPostPrint)
                            ];
                        }

                        if ($amountAmountAddon) {
                            $rows[] = [
                                'type'       => 'amountAddon',
                                'title'      => _t('payment.modelPrice', 'Additional Services'),
                                'total_line' => displayAsMoney($amountAmountAddon)
                            ];
                        }

                        if ($amountPaymentMethodFee) {
                            $rows[] = [
                                'type'       => 'paymentMethodFee',
                                'title'      => _t('payment.modelPrice', 'Payment method fee'),
                                'total_line' => displayAsMoney($amountPaymentMethodFee)
                            ];
                        }

                        if ($amountAmountDiscount) {
                            $rows[] = [
                                'type'       => 'discount',
                                'title'      => _t('payment.modelPrice', 'Discount'),
                                'total_line' => displayAsMoney($amountAmountDiscount)
                            ];
                        }

                        return $rows;
                    } else {
                        $items                  = $invoice->paymentInvoiceItems;
                        $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();

                        if ($amountPaymentMethodFee) {
                            $items[] = [
                                'type'       => 'paymentMethodFee',
                                'title'      => _t('payment.modelPrice', 'Payment method fee'),
                                'total_line' => displayAsMoney($amountPaymentMethodFee)
                            ];
                        }

                        return $items;
                    }
                },
                'storeOrderPromocode',
                'paymentInvoicePaymentMethods'
            ],
            UserAddress::class                 => [
                'companyName' => function (UserAddress $userAddress) {
                    return $userAddress->company;
                },
                'contactName' => function (UserAddress $userAddress) {
                    return $userAddress->contact_name;
                },
                'address'     => function (UserAddress $userAddress) {
                    return UserAddress::formatAddress($userAddress, true, false);
                }
            ],
            PaymentInvoicePaymentMethod::class => [
                'vendor'
            ],
            StoreOrderPromocode::class         => [
                'amountTotal' => function (StoreOrderPromocode $storeOrderPromocode) {
                    return displayAsMoney($storeOrderPromocode->getAmountTotal());
                },
                'promocode'   => function (StoreOrderPromocode $storeOrderPromocode) {
                    return $storeOrderPromocode->promocode->code;
                },
            ],
            PaymentInvoiceItem::class          => [
                'type'       => function (PaymentInvoiceItem $item) {
                    return 'item';
                },
                'title',
                'total_line' => function (PaymentInvoiceItem $item) {
                    return displayAsMoney($item->getLineTotal());
                }
            ],
            Model3dReplica::class => Model3dSerializer::class,
        ];
    }
}