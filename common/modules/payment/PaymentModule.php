<?php
/**
 * Date: 28.11.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\payment;

use Yii;
use yii\base\Module;


class PaymentModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\payment\controllers';

    public $maxBraintreePrice = 1200;


    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();
    }
}
