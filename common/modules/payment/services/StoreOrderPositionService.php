<?php

namespace common\modules\payment\services;

use common\components\Emailer;
use common\components\order\history\OrderHistoryService;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use common\modules\informer\InformerModule;
use common\modules\payment\factories\PaymentInvoiceListFactory;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\services\StoreOrderService;
use common\services\UserEmailLoginService;
use yii\base\BaseObject;
use yii\web\HttpException;

/**
 * User: nabi
 *
 * @property PaymentInvoiceListFactory $paymentInvoiceListFactory
 * @property PaymentInvoiceRepository $paymentInvoiceRepository
 */
class StoreOrderPositionService extends BaseObject
{
    /**
     * @var OrderHistoryService
     */
    protected $orderHistoryService;

    /**
     * @var StoreOrderService
     */
    protected $orderService;

    /**
     * @var Emailer
     */
    protected $emailer;

    /**
     * @var UserEmailLoginService
     */
    protected $emailLoginService;

    protected $paymentInvoiceListFactory;

    protected $paymentInvoiceRepository;

    /** @var PaymentService */
    protected $paymentService;

    public function injectDependencies(
        Emailer $emailer,
        StoreOrderService $orderService,
        OrderHistoryService $orderHistoryService,
        UserEmailLoginService $emailLoginService,
        PaymentInvoiceListFactory $paymentInvoiceListFactory,
        PaymentInvoiceRepository $paymentInvoiceRepository,
        PaymentService $paymentService
    ) {
        $this->emailer = $emailer;
        $this->orderHistoryService = $orderHistoryService;
        $this->orderService = $orderService;
        $this->emailLoginService = $emailLoginService;
        $this->paymentInvoiceListFactory = $paymentInvoiceListFactory;
        $this->paymentInvoiceRepository = $paymentInvoiceRepository;
        $this->paymentService = $paymentService;
    }

    /**
     * @param StoreOrderPosition $position
     * @param bool $isPs
     *
     * @throws HttpException
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     */
    public function addOrderPosition(StoreOrderPosition $position, $isPs = true)
    {
        if ($position->amount <= 0) {
            throw new HttpException(400, _t('site.ps', 'Need positive amount.'));
        }

        $position->safeSave();

        $paymentInvoices = $this->paymentInvoiceListFactory->createWithPaymentMethodsByAdditionalService($position);
        $this->paymentInvoiceRepository->saveInvoicesArray($paymentInvoices);

        /** @var PaymentInvoice $primaryInvoice */
        $primaryInvoice = $this->paymentInvoiceRepository->getNewInvoiceByAdditionalServiceAndPaymentMethod(
            $position,
            PaymentTransaction::VENDOR_BRAINTREE
        );

        $position->primary_payment_invoice_uuid = $primaryInvoice->uuid;
        $position->safeSave(['primary_payment_invoice_uuid']);

        if (!$isPs) {
            // send email to PS
            $this->emailer->sendPositionNewPs($position);
        }
        $client = $position->order->user;
        $clientName = $client->getFullNameOrUsername();

        // send email to User
        $email = $client->getEmail();
        if ($position->order->isThingiverseOrder()) {
            $email = $position->order->user->thingiverseUser->email;
            if (strpos($clientName, '_tg')) {
                $userNameParts = explode("_tg", $clientName, 2);
                $clientName = $userNameParts[0];
            }
        }
        $userEmailLogin = $this->emailLoginService->requestLoginByEmail(
            $client,
            $email,
            param('server') . '/workbench/order/view/' . $position->order_id,
            false
        );

        $this->emailer->sendPositionNewClient($position, $clientName, $userEmailLogin);
        InformerModule::addCustomerOrderInformer($position->order);
    }

    public function cancelOrderPosition(StoreOrderPosition $position)
    {
        $position->status = StoreOrderPosition::STATUS_CANCELED;
        $position->safeSave();
        $this->paymentService->setCanceledInvoice($position->primaryPaymentInvoice);
    }


    /**
     * user accepts additional service position for given order
     *
     * @param StoreOrderPosition $position
     *
     * @throws \common\components\exceptions\BusinessException
     */
    public function acceptOrderPosition(StoreOrderPosition $position)
    {
        // ins
        $order = $position->order;

        // update position
        $position->updated_at = dbexpr('NOW()');
        $position->status = StoreOrderPosition::STATUS_PAID;
        $position->safeSave();

        // write history
        $this->orderHistoryService->logPositionPaid($order, $position->title);

        // send email to PS
        $this->emailer->sendPositionPaid($position);
        /*
        $this->emailer->sendSupportMessage(
            "Additional service for order #" . $position->order_id . " paid",
            "Additional service for order #" . $position->order_id . " paid"
        );*/
    }

    /**
     * users declindes additional service position for given order
     * all business logic is here
     * - send email
     * - notify PS
     * - update position status
     * - write history
     *
     * @param StoreOrder $order
     * @param StoreOrderPosition $position
     * @param string $message
     * @throws \yii\base\Exception
     */
    public function declineOrderPosition(StoreOrder $order, StoreOrderPosition $position, string $message)
    {
        // update position
        $position->updated_at = dbexpr('NOW()');
        $position->status = StoreOrderPosition::STATUS_DECLINED;
        $position->safeSave();

        // write history
        $this->orderHistoryService->logDeclinePosition($position, $message);

        // send email
        $this->emailer->sendPositionDeclined($position, $message);

        /*
        $this->emailer->sendSupportMessage(
            "Additional service for order #" . $position->order_id . " declined",
            "Additional service for order #" . $position->order_id . " declined"
        );*/
    }

    /**
     * check if position (additional service) can be added to given attempt
     *
     * @param StoreOrderAttemp $attempt
     * @return bool
     */
    public function canAddPositionToAttempt(StoreOrderAttemp $attempt)
    {
        if ($attempt->isCancelled() ||
            $attempt->status == StoreOrderAttemp::STATUS_RECEIVED ||
            $attempt->status == StoreOrderAttemp::STATUS_NEW) {
            return false;
        }
        return true;
    }
}
