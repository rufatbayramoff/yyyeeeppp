<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.10.18
 * Time: 14:37
 */

namespace common\modules\payment\services;

use common\components\ArrayHelper;
use common\models\Company;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\StoreOrderAttemp;
use common\models\User;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use yii\data\ActiveDataProvider;

class PaymentAccountStatistics
{
    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /**
     * @param PaymentAccountService $paymentAccountService
     */
    public function injectDependencies(
        PaymentAccountService $paymentAccountService
    ) {
        $this->paymentAccountService = $paymentAccountService;
    }

    /**
     * @param User $user
     * @return array
     *
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function getStatisticsForCompany(User $user)
    {
        $paymentAccountQuery = PaymentAccount::find()->where(['user_id' => $user->id, 'type' => PaymentAccount::ACCOUNT_TYPE_MAIN]);
        $balancesInfo = $this->paymentAccountService->calculateAccountsBalanceEx($paymentAccountQuery);

        $totalIncome = [];
        foreach ($balancesInfo as $balanceInfo) {
            $currency = $balanceInfo->balanceCurrent->getCurrency();
            $totalPayout = $this->paymentAccountService->getTotalPayout($user, $currency);
            $totalIncome[$currency] = MoneyMath::sum($totalPayout, $balanceInfo->balanceCurrent);
        }

        $ordersReceived = StoreOrderAttemp::find()
            ->forPsUser($user)
            ->inStatus(StoreOrderAttemp::STATUS_RECEIVED)
            ->notTest()
            ->count();

        $paymentAccountAward = $this->paymentAccountService->getUserPaymentAccounts($user, PaymentAccount::ACCOUNT_TYPE_AWARD);
        $accountIds = ArrayHelper::getColumn($paymentAccountAward, 'id');
        $paymentDetailModel3dAwardQuery = PaymentDetail::find()->where(['payment_account_id' => $accountIds, 'type' => PaymentDetail::TYPE_AWARD_MODEL]);
        //debugSql($paymentDetailModel3dAwardQuery);exit;
        $modelsPrinted = $paymentDetailModel3dAwardQuery->sum('amount');
        $modelsAwarded = $paymentDetailModel3dAwardQuery->count();

        $result = [
            'totalIncome'    => $totalIncome,
            'ordersResolved' => $ordersReceived,
            'modelsPrinted'  => (int)$modelsPrinted,
            'modelsSold'     => $modelsAwarded,
        ];
        return $result;
    }
}