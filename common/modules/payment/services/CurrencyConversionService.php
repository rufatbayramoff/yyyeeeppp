<?php

namespace common\modules\payment\services;

use backend\models\search\AccountBalanceSearch;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\modules\payment\models\AccountBalance;
use common\modules\payment\models\ConversationBalance;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use yii\base\BaseObject;
use yii\data\ArrayDataProvider;
use yii\data\BaseDataProvider;

/**
 * Class BonusService
 * @package common\modules\payment\services
 *
 */
class CurrencyConversionService extends BaseObject
{
    /** @var PaymentService */
    protected PaymentService $paymentService;

    /** @var PaymentAccountService */
    protected PaymentAccountService $paymentAccountService;

    public function injectDependencies(
        PaymentService $paymentService,
        PaymentAccountService $paymentAccountService,
    )
    {
        $this->paymentService        = $paymentService;
        $this->paymentAccountService = $paymentAccountService;
    }

    public function getCurrencyBalancesArray(string $accountType)
    {
        $amountsQuery = PaymentDetail::find()
            ->joinWith('paymentAccount.user')
            ->andWhere(['payment_account.type' => $accountType]);
        $amountsQuery
            ->select("sum(`amount`) as amount, payment_account_id")
            ->groupBy('payment_account_id')
            ->orderBy('payment_account_id')
            ->asArray();
        $amounts         = $amountsQuery->all();
        $accountBalances = [];
        foreach ($amounts as $oneAmount) {
            $paymentAccount = PaymentAccount::findByPk($oneAmount['payment_account_id']);
            $accountBalances[$paymentAccount->currency]  = Money::create($oneAmount['amount'], $paymentAccount->currency);
        }
        $conversationBalance = Yii::createObject(ConversationBalance::class);
        $conversationBalance->type = $accountType;
        $conversationBalance->balances = $accountBalances;
        return $conversationBalance;
    }

    public function getConversionBalancesArray()
    {
        $conversions[PaymentAccount::ACCOUNT_TYPE_CONVERT_BNS_EUR] = $this->getCurrencyBalancesArray(PaymentAccount::ACCOUNT_TYPE_CONVERT_BNS_EUR);
        $conversions[PaymentAccount::ACCOUNT_TYPE_CONVERT_BNS_USD] = $this->getCurrencyBalancesArray(PaymentAccount::ACCOUNT_TYPE_CONVERT_BNS_USD);
        $conversions[PaymentAccount::ACCOUNT_TYPE_CONVERT_EUR_USD] = $this->getCurrencyBalancesArray(PaymentAccount::ACCOUNT_TYPE_CONVERT_EUR_USD);
        return $conversions;
    }

    public function getConversionBalancesProvider(): BaseDataProvider
    {
        $accountBalances = $this->getConversionBalancesArray();
        $dataProvider    = new ArrayDataProvider([
            'allModels' => $accountBalances,
        ]);
        return $dataProvider;
    }
}
