<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 09.11.2017
 * Time: 15:54
 */

namespace common\modules\payment\services;

use common\components\exceptions\DeprecatedException;
use common\components\PaymentExchangeRateConverter;
use common\models\Payment;
use common\models\PaymentDetail;
use common\models\PaymentTransaction;
use common\models\User;
use common\models\UserTaxInfo;
use lib\money\Currency;
use lib\payment\exception\PaymentManagerUserException;
use lib\payment\PaymentManager;
use common\modules\payment\taxes\Tax;

/**
 * Class PayoutService
 *
 * payout service to create paypal payout requests.
 *
 * @package common\modules\payment\services
 * @deprecated
 */
class PayoutService
{
    /**
     * add paypal payout
     *
     * @param \common\models\base\User|User $user
     * @param  int $usdAmountRounded - in USD
     * @param  Tax $taxObj
     * @param PaymentTransaction $transaction
     * @return int
     * @throws PaymentManagerUserException
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     *
     * @deprecated
     */
    public static function addPaypalPayout(User $user, $usdAmountRounded, Tax $taxObj, PaymentTransaction $transaction)
    {
        throw new DeprecatedException('deprecated PayoutService:addPaypalPayout()');
        $paymentService = \Yii::createObject(PaymentService::class);

        $userId = $user->id;
        $hasAmountUsd = $paymentAccountService->getUserMainAmount($user);
        $taxRate = $taxObj->getValue();
        if ($usdAmountRounded > $hasAmountUsd) {
            $hasAmountUsdTxt = displayAsCurrency($hasAmountUsd, Currency::USD);
            $usdAmountRoundedTxt = displayAsCurrency($usdAmountRounded, Currency::USD);
            throw new PaymentManagerUserException(
                _t(
                    "site.payment",
                    "Sorry, you cannot request payout more than {hasAmount}. Your request {outAmount}",
                    ['hasAmount' => $hasAmountUsdTxt, 'outAmount' => $usdAmountRoundedTxt]
                )
            );
        }
        $taxAmount = $taxObj->getTaxAmount($usdAmountRounded);
        $usdAmountClean = $taxObj->getCleanAmount($usdAmountRounded);

        $description = 'Paypal payout';
        // add payout to paypal account
        /** @var PaymentExchangeRateConverter $converter */
        $converter = \Yii::createObject(PaymentExchangeRateConverter::class);
        $xrate = $converter->getLastPaymentExchangeRate();
        $paymentManager = PaymentManager::init($xrate)
            ->addPayment(["description" => $description])
            ->addDetails(
                [
                    "user_id"        => $userId,
                    "amount"         => -$usdAmountClean,
                    'created_at'     => dbexpr("NOW()"),
                    'transaction_id' => $transaction->id,
                    "type"           => PaymentDetail::TYPE_PAYOUT
                ]
            )->addDetails(
                [
                    "user_id"    => User::USER_PAYPAL,
                    "amount"     => $usdAmountClean,
                    'created_at' => dbexpr("NOW()"),
                    "type"       => PaymentDetail::TYPE_PAYOUT
                ]
            );

        if (!empty($taxAmount)) {
            $paymentManager->addDetails(
                [ // minus from
                    "user_id"           => $userId,
                    "original_amount"   => -$taxAmount,
                    "original_currency" => "USD",
                    'description'       => "Tax withholding $taxRate%",
                    "type"              => PaymentDetail::TYPE_TAX
                ]
            )->addDetails(
                [
                    'user_id'           => User::USER_TAXAGENT,
                    "original_amount"   => $taxAmount,
                    "original_currency" => "USD",
                    "type"              => PaymentDetail::TYPE_TAX
                ]
            );
        }
        // now procceed
        $paymentId = $paymentManager->submit();
        UserTaxInfo::taxNotifyOff($user);
        return $paymentId;
    }
}
