<?php

namespace common\modules\payment\services;

use backend\models\search\AccountBalanceSearch;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\modules\payment\models\AccountBalance;
use lib\money\Currency;
use lib\money\Money;
use yii\base\BaseObject;
use yii\data\ArrayDataProvider;
use yii\data\BaseDataProvider;

/**
 * Class BonusService
 * @package common\modules\payment\services
 *
 */
class BonusService extends BaseObject
{
    /** @var PaymentService */
    protected PaymentService $paymentService;

    /** @var PaymentAccountService */
    protected PaymentAccountService $paymentAccountService;

    public function injectDependencies(
        PaymentService $paymentService,
        PaymentAccountService $paymentAccountService,
    )
    {
        $this->paymentService        = $paymentService;
        $this->paymentAccountService = $paymentAccountService;
    }

    public function getBonusUsersArray(AccountBalanceSearch $search)
    {
        $amountsQuery = PaymentDetail::find()
            ->joinWith('paymentAccount.user')
            ->andWhere(['payment_account.currency' => Currency::BNS]);
        if ($search->user) {
            $amountsQuery->andWhere(['or',
                ['like', 'user.username', $search->user],
                ['user.id' => $search->user],
                ['like', 'user.email', $search->user],
            ]);
        }
        $amountsQuery
            ->select("sum(`amount`) as amount, payment_account_id")
            ->groupBy('payment_account_id')
            ->asArray();
        $amounts         = $amountsQuery->all();
        $accountBalances = [];
        foreach ($amounts as $oneAmount) {
            $accountBalance                 = new AccountBalance();
            $accountBalance->paymentAccount = PaymentAccount::findByPk($oneAmount['payment_account_id']);
            $accountBalance->money          = Money::create($oneAmount['amount'], $accountBalance->paymentAccount->currency);
            $accountBalances[]              = $accountBalance;
        }
        return $accountBalances;
    }

    public function getBonusUsersProvider(AccountBalanceSearch $search): BaseDataProvider
    {
        $accountBalances = $this->getBonusUsersArray($search);
        $dataProvider    = new ArrayDataProvider([
            'allModels' => $accountBalances,
        ]);
        return $dataProvider;
    }

    public function addBalance(AccountBalance $accountBalance, $paymentDescription = 'Add bonus balance')
    {
        $payment                     = Payment::tryFindByPk(['id' => Payment::PAYMENT_CORRECTION_ID]);
        $treatstockCorrectionAccount = $this->paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_CORRECTION, Currency::BNS);

        $this->paymentService->transferMoney(
            $payment,
            $treatstockCorrectionAccount,
            $accountBalance->paymentAccount,
            $accountBalance->money,
            PaymentDetail::TYPE_CORRECTION,
            $paymentDescription
        );


    }
}
