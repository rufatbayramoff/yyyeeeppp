<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.11.18
 * Time: 14:47
 */

namespace common\modules\payment\services;

use backend\modules\payment\models\PaymentAlarm;
use common\components\ArrayHelper;
use common\components\DateHelper;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentInvoice;
use common\models\PaymentWarning;

class PaymentWarningService
{

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /** @var PaymentService */
    protected $paymentService;

    public const MAX_DIFF_DETAILS_DATES    = 40; // 30 days max details
    public const DAYS_CLOSE_INVOICE_PERIOD = 40; // Days number

    /**
     * @param PaymentAccountService $paymentAccountService
     * @param PaymentService $paymentService
     */
    public function injectDependencies(
        PaymentAccountService $paymentAccountService,
        PaymentService $paymentService
    )
    {
        $this->paymentAccountService = $paymentAccountService;
        $this->paymentService        = $paymentService;
    }

    protected function isWarningChecked(PaymentAlarm $paymentAlarm)
    {
        return PaymentWarning::findOne(['uuid' => $paymentAlarm->uuid]);
    }

    protected function viewPaymentUrl(PaymentDetail $paymentDetail)
    {
        return '<a href="/store/payment/view?id='.$paymentDetail->payment->id.'#paymentDetail'.$paymentDetail->id.'">'.$paymentDetail->id.'</a>';
    }

    /**
     * Check invoice incoming and outcome sum is same
     *
     * @param $invoice
     * @param $alarms
     */
    public function formAlarmIncomeOutcomeSum($invoice, &$alarms): void
    {
        $incomingAmount  = $this->paymentService->incomingAmount($invoice);
        $outcomingAmount = $this->paymentService->outcomingAmount($invoice);
        if ($incomingAmount < $outcomingAmount) {
            $orderId              = $invoice->store_order_id;
            $alarm                = new PaymentAlarm();
            $alarm->date          = $invoice->created_at;
            $alarm->description   = 'Order: <a href="/store/store-order/view?id=' . $orderId . '" target="_blank">' . $orderId . '</a>. Invoice ' . $invoice->uuid . ' has minus amount. Incoming: ' . $incomingAmount . ' Outcoming: ' . $outcomingAmount;
            $alarm->uuid          = 'incomeOutcome:' . $invoice->uuid;
            $alarms[$alarm->uuid] = $alarm;
        }
    }

    /**
     * return PaymentAlarm[]
     *
     * @param $dateBegin
     * @param $dateEnd
     * @return array
     */
    public function formAlarmsInvoices($dateBegin, $dateEnd): array
    {
        $alarms   = [];
        $invoices = PaymentInvoice::find()->where(['and', ['>=', 'created_at', $dateBegin], ['<=', 'created_at', $dateEnd]])->orderBy('created_at desc')->limit(30)->all();
        foreach ($invoices as $invoice) {
            $this->formAlarmIncomeOutcomeSum($invoice, $alarms);
        }
        return $alarms;
    }

    /**
     * Check details inserted after invoice was closed
     *
     * @param $dateBegin
     * @param $dateEnd
     * @return array
     */
    public function formAlarmsDetailsInvoiceDate($dateBegin, $dateEnd): array
    {
        if (substr($dateBegin, 0, 10) < '2018-12-13') {
            $dateBegin = '2018-12-13 00:00:00';
        }
        $paymentDetailQuery = PaymentDetail::find()
            ->andWhere(['>=', 'payment_detail.updated_at', $dateBegin])
            ->andWhere(['<=', 'payment_detail.updated_at', $dateEnd])
            ->leftJoin('payment_detail_operation', 'payment_detail.payment_detail_operation_uuid = payment_detail_operation.uuid')
            ->leftJoin('payment', 'payment_detail_operation.payment_id = payment.id')
            ->leftJoin('payment_invoice', 'payment_invoice.uuid=payment.payment_invoice_uuid')
            ->andWhere('payment_invoice.created_at < DATE_SUB(payment_detail.updated_at, INTERVAL ' . self::MAX_DIFF_DETAILS_DATES . ' DAY)')
            ->groupBy('payment_detail.id')
            ->limit(30);
        $paymentDetails     = $paymentDetailQuery->all();

        $alarms = [];

        foreach ($paymentDetails as $paymentDetail) {
            if ($paymentDetail->paymentWarnings) {
                // Skip already checked
                continue;
            }

            $alarm       = new PaymentAlarm();
            $alarm->date = $paymentDetail->updated_at;
            $invoice     = $paymentDetail->paymentDetailOperation->payment->paymentInvoice;
            if (!$invoice) {
                continue;
                // Skip payouts
            }
            $orderId          = $invoice->storeOrder->id ?? null;
            $instantPaymentId = $invoice->instant_payment_uuid ?? null;

            $alarm->description     = 'Was changed with expire date. ' .
                ($orderId ? ' Order: <a href="/store/store-order/view?id=' . $orderId . '" target="_blank">' . $orderId . '</a>.' : '') .
                ($instantPaymentId ? ' Instant payment: <a href="/store/instant-payment/update?id=' . $instantPaymentId . '">' . $instantPaymentId . '</a>.' : '') .
                'Invoice ' . $invoice->uuid . ' date: ' . $invoice->created_at . '. Detail Id: ' . $this->viewPaymentUrl($paymentDetail);
            $alarm->uuid            = 'canceledByDate:' . $invoice->uuid;
            $alarm->paymentDetailId = $paymentDetail->id;
            if ($this->isWarningChecked($alarm)) {
                continue;
            }
            $alarms[$alarm->uuid] = $alarm;
        }
        return $alarms;
    }

    /**
     * Payout payments, should be without incoming payment details
     *
     * @param $dateBegin
     * @param $dateEnd
     * @return array
     */
    public function formAlarmsIncomeWithoutInvoice($dateBegin, $dateEnd): array
    {
        $payoutPayments = Payment::find()->where('payment.payment_invoice_uuid is null')
            ->andWhere(['>=', 'payment.created_at', $dateBegin])
            ->andWhere(['<=', 'payment.created_at', $dateEnd])
            ->leftJoin('payment_detail_operation', 'payment_detail_operation.payment_id=payment.id')
            ->leftJoin('payment_detail', 'payment_detail.payment_detail_operation_uuid=payment_detail_operation.uuid')
            ->leftJoin('payment_account', 'payment_account.id=payment_detail.payment_account_id')
            ->andWhere("payment_account.type='main' and payment_account.user_id>999")
            ->andWhere('payment_detail.amount>0')
            ->groupBy('payment.id')
            ->all();
        $alarms         = [];

        foreach ($payoutPayments as $payoutPayment) {
            $alarm         = new PaymentAlarm();
            $alarm->date   = $payoutPayment->created_at;
            $paymentDetail = $payoutPayment->getPaymentDetails()->joinAccount()->andWhere("payment_account.type='main' and payment_account.user_id>999")
                ->andWhere('payment_detail.amount>0')->one();
            if ($paymentDetail->paymentWarnings) {
                // Skip already checked
                continue;
            }
            $alarm->description     = 'Incoming money in payout payment: <a href="/store/payment/view?id=' . $payoutPayment->id . '">' . $payoutPayment->id . '</a>. Detail Id: ' . $this->viewPaymentUrl($paymentDetail);
            $alarm->uuid            = 'payoutIn:' . $payoutPayment->id;
            $alarm->paymentDetailId = $paymentDetail->id;
            if ($this->isWarningChecked($alarm)) {
                continue;
            }
            $alarms[$alarm->uuid] = $alarm;
        }
        return $alarms;
    }

    /**
     * @param $dateBegin
     * @param $dateEnd
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function formAlarmsReservedOutWithoutIn($dateBegin, $dateEnd): array
    {
        $alarms = [];

        // Select all payments where payment details
        $sql                    = 'SELECT payment_detail.id
FROM payment
  LEFT JOIN payment_detail_operation ON payment_detail_operation.payment_id = payment.id
  LEFT JOIN payment_detail ON payment_detail.payment_detail_operation_uuid = payment_detail_operation.uuid
  LEFT JOIN payment_account ON payment_account.id = payment_detail.payment_account_id
WHERE payment_account.type = \'reserved\' AND payment_detail.amount < 0 AND payment_detail.updated_at <= \'' . $dateEnd . '\' AND payment_detail.updated_at >= \'' . $dateBegin . '\' AND
      payment.id NOT IN (SELECT payment.id
                         FROM payment
                           LEFT JOIN payment_detail_operation ON payment_detail_operation.payment_id = payment.id
                           LEFT JOIN payment_detail ON payment_detail.payment_detail_operation_uuid = payment_detail_operation.uuid
                           LEFT JOIN payment_account ON payment_account.id = payment_detail.payment_account_id
                         WHERE payment_account.type = \'reserved\' AND payment_detail.amount > 0)';
        $invalidReservedDetails = \Yii::$app->db->createCommand($sql)->queryAll();
        $invalidReservedDetails = ArrayHelper::map($invalidReservedDetails, 'id', 'id');
        foreach ($invalidReservedDetails as $paymentDetailId) {
            $paymentDetail = PaymentDetail::tryFindByPk($paymentDetailId);
            $paymentId     = $paymentDetail->paymentDetailOperation->payment_id;
            $orderId       = $paymentDetail->paymentDetailOperation->payment->paymentInvoice ?
                $paymentDetail->paymentDetailOperation->payment->paymentInvoice->store_order_id : null;
            $alarm         = new PaymentAlarm();
            $alarm->date   = $paymentDetail->updated_at;
            if ($paymentDetail->paymentWarnings) {
                // Skip already checked
                continue;
            }
            $alarm->description       = 'Reserv payment without incoming to reserv.';
            if ($orderId) {
                $alarm->description .= ' Order: <a href="/store/store-order/view?id=' . $orderId . '">' . $orderId . '</a>';
            }
            $alarm->description       .= ' Detail Id: ' . $this->viewPaymentUrl($paymentDetail);
            $alarm->uuid              = 'reservedMinus:' . $paymentId;
            $alarm->paymentDetailId   = $paymentDetail->id;
            $alarm->allowPressChecked = false;
            if ($this->isWarningChecked($alarm)) {
                continue;
            }
            $alarms[$alarm->uuid] = $alarm;
        }

        // Select all payments where payment details
        $sql                    = 'SELECT payment_detail.id
FROM payment
  LEFT JOIN payment_detail_operation ON payment_detail_operation.payment_id = payment.id
  LEFT JOIN payment_detail ON payment_detail.payment_detail_operation_uuid = payment_detail_operation.uuid
  LEFT JOIN payment_account ON payment_account.id = payment_detail.payment_account_id
WHERE payment_account.type = \'authorize\' AND payment_detail.amount < 0 AND payment_detail.updated_at <= \'' . $dateEnd . '\' AND payment_detail.updated_at >= \'' . $dateBegin . '\' AND
      payment.id NOT IN (SELECT payment.id
                         FROM payment
                           LEFT JOIN payment_detail_operation ON payment_detail_operation.payment_id = payment.id
                           LEFT JOIN payment_detail ON payment_detail.payment_detail_operation_uuid = payment_detail_operation.uuid
                           LEFT JOIN payment_account ON payment_account.id = payment_detail.payment_account_id
                         WHERE payment_account.type = \'authorize\' AND payment_detail.amount > 0)';
        $invalidReservedDetails = \Yii::$app->db->createCommand($sql)->queryAll();
        $invalidReservedDetails = ArrayHelper::map($invalidReservedDetails, 'id', 'id');
        foreach ($invalidReservedDetails as $paymentDetailId) {
            $paymentDetail = PaymentDetail::tryFindByPk($paymentDetailId);
            $paymentId     = $paymentDetail->paymentDetailOperation->payment_id;
            $orderId       = $paymentDetail->paymentDetailOperation->payment->paymentInvoice ?
                $paymentDetail->paymentDetailOperation->payment->paymentInvoice->store_order_id : null;

            $alarm       = new PaymentAlarm();
            $alarm->date = $paymentDetail->updated_at;
            if ($paymentDetail->paymentWarnings) {
                // Skip already checked
                continue;
            }
            $alarm->description       = 'Authorize payment without incoming to authorize.';
            if ($orderId) {
                $alarm->description .= ' Order: <a href="/store/store-order/view?id=' . $orderId . '">' . $orderId . '</a>';
            }
            $alarm->description       .= ' Detail Id: ' . $this->viewPaymentUrl($paymentDetail);
            $alarm->uuid              = 'authorizeMinus:' . $paymentId;
            $alarm->paymentDetailId   = $paymentDetail->id;
            $alarm->allowPressChecked = false;
            if ($this->isWarningChecked($alarm)) {
                continue;
            }
            $alarms[$alarm->uuid] = $alarm;
        }
        return $alarms;
    }

    /**
     * @param $dateBegin
     * @param $dateEnd
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function formAlarmsNotZeroTechnicalAccounts($dateBegin, $dateEnd): array
    {
        if (substr($dateEnd, 0, 10) > DateHelper::subNow('P' . self::DAYS_CLOSE_INVOICE_PERIOD . 'D')) {
            $dateEnd = DateHelper::subNow('P' . self::DAYS_CLOSE_INVOICE_PERIOD . 'D');
        }

        $alarms                  = [];
        $technicalAccountsList   = "'" . implode("', '", PaymentAccount::getTechnicalZeroAccounts()) . "'";
        $sql                     = 'SELECT
  CONCAT(payment.id, \'_\', payment_account.type),
  ANY_VALUE(payment_detail.id),
  sum(payment_detail.amount)
FROM payment
  LEFT JOIN payment_detail_operation ON payment_detail_operation.payment_id = payment.id
  LEFT JOIN payment_detail ON payment_detail.payment_detail_operation_uuid = payment_detail_operation.uuid
  LEFT JOIN payment_account ON payment_account.id = payment_detail.payment_account_id
WHERE payment_account.type IN (' . $technicalAccountsList . ') and
  payment.created_at>=\'' . $dateBegin . '\' and
  payment.created_at<=\'' . $dateEnd . '\'
GROUP BY CONCAT(payment.id, \'_\', payment_account.type)
HAVING sum(payment_detail.amount) != 0 LIMIT 15';
        $notZeroTechnicalDetails = \Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($notZeroTechnicalDetails as $paymentInfo) {
            $paymentInfo = array_values($paymentInfo);
            list($paymentId, $accountType) = explode('_', $paymentInfo[0]);
            $paymentDetailId = $paymentInfo[1];
            $sum             = $paymentInfo[2];

            if ((round($sum, 2) > -0.01) && (round($sum, 2) < 0.01)) {
                continue;
            }

            $paymentDetail    = PaymentDetail::tryFindByPk($paymentDetailId);
            $orderId          = $paymentDetail->paymentDetailOperation->payment->paymentInvoice->store_order_id ?? null;
            $instantPaymentId = $paymentDetail->paymentDetailOperation->payment->paymentInvoice->instant_payment_uuid ?? null;

            $alarm       = new PaymentAlarm();
            $alarm->date = $paymentDetail->updated_at;
            if ($paymentDetail->paymentWarnings) {
                // Skip already checked
                continue;
            }
            $alarm->description     = 'Not zero technical account. Payment: ' . $paymentId .
                '. Type: ' . $accountType . ' Summ: ' . round($sum, 2) .
                ($orderId ? ' Order: <a href="/store/store-order/view?id=' . $orderId . '" target="_blank">' . $orderId . '</a>.' : '') .
                ($instantPaymentId ? ' Instant payment: <a href="/store/instant-payment/update?id=' . $instantPaymentId . '">' . $instantPaymentId . '</a>.' : '') .
                ' Detail Id: ' . $paymentDetail->id;
            $alarm->uuid            = 'notZeroTechnical:' . $paymentId;
            $alarm->paymentDetailId = $paymentDetail->id;
            if ($this->isWarningChecked($alarm)) {
                continue;
            }
            $alarms[$alarm->uuid] = $alarm;

        }
        return $alarms;
    }

    public function formAlarmsDifferentCurrency($dateBegin, $dateEnd): array
    {
        if (substr($dateEnd, 0, 10) > DateHelper::subNow('P' . self::DAYS_CLOSE_INVOICE_PERIOD . 'D')) {
            $dateEnd = DateHelper::subNow('P' . self::DAYS_CLOSE_INVOICE_PERIOD . 'D');
        }

        $alarms                  = [];
        $technicalAccountsList   = "'" . implode("', '", PaymentAccount::getTechnicalZeroAccounts()) . "'";
        $sql                     = 'SELECT
  CONCAT(payment.id, \'_\', payment_account.type),
  ANY_VALUE(payment_detail.id),
  sum(payment_detail.amount)
FROM payment
  LEFT JOIN payment_detail_operation ON payment_detail_operation.payment_id = payment.id
  LEFT JOIN payment_detail ON payment_detail.payment_detail_operation_uuid = payment_detail_operation.uuid
  LEFT JOIN payment_account ON payment_account.id = payment_detail.payment_account_id
WHERE payment_account.type IN (' . $technicalAccountsList . ') and
  payment.created_at>=\'' . $dateBegin . '\' and
  payment.created_at<=\'' . $dateEnd . '\'
GROUP BY CONCAT(payment.id, \'_\', payment_account.type)
HAVING sum(payment_detail.amount) != 0 LIMIT 15';
        $notZeroTechnicalDetails = \Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($notZeroTechnicalDetails as $paymentInfo) {
            $paymentInfo = array_values($paymentInfo);
            [$paymentId, $accountType] = explode('_', $paymentInfo[0]);
            $paymentDetailId = $paymentInfo[1];
            $sum             = $paymentInfo[2];

            if ((round($sum, 2) > -0.01) && (round($sum, 2) < 0.01)) {
                continue;
            }

            $paymentDetail    = PaymentDetail::tryFindByPk($paymentDetailId);
            $orderId          = $paymentDetail->paymentDetailOperation->payment->paymentInvoice->store_order_id ?? null;
            $instantPaymentId = $paymentDetail->paymentDetailOperation->payment->paymentInvoice->instant_payment_uuid ?? null;

            $alarm       = new PaymentAlarm();
            $alarm->date = $paymentDetail->updated_at;
            if ($paymentDetail->paymentWarnings) {
                // Skip already checked
                continue;
            }
            $alarm->description     = 'Not zero technical account. Payment: ' . $paymentId .
                '. Type: ' . $accountType . ' Summ: ' . round($sum, 2) .
                ($orderId ? ' Order: <a href="/store/store-order/view?id=' . $orderId . '" target="_blank">' . $orderId . '</a>.' : '') .
                ($instantPaymentId ? ' Instant payment: <a href="/store/instant-payment/update?id=' . $instantPaymentId . '">' . $instantPaymentId . '</a>.' : '') .
                ' Detail Id: ' . $this->viewPaymentUrl($paymentDetail);
            $alarm->uuid            = 'notZeroTechnical:' . $paymentId;
            $alarm->paymentDetailId = $paymentDetail->id;
            if ($this->isWarningChecked($alarm)) {
                continue;
            }
            $alarms[$alarm->uuid] = $alarm;

        }
        return $alarms;
    }
}