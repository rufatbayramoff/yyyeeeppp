<?php
/**
 * User: nabi
 */

namespace common\modules\payment\services;

use common\components\ArrayHelper;
use common\components\Emailer;
use common\components\exceptions\BusinessException;
use common\components\order\history\OrderHistoryService;
use common\components\PaymentExchangeRateConverter;
use common\models\DeliveryType;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionRefund;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderHistory;
use common\models\StoreOrderPosition;
use common\models\StorePricer;
use common\models\TsInternalPurchase;
use common\models\User;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\components\PaymentDoubleEntry;
use common\modules\payment\components\TAccount;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayProvider;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use common\modules\payment\models\RefundRequestForm;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use yii\base\UserException;
use yii\helpers\Html;

/**
 * Class RefundService
 * to work with refunds
 *
 * @package common\modules\payment\services
 */
class RefundService
{
    /**
     * if error message is required
     *
     * @var string
     */
    public $lastErrorMessage;

    /**
     * @var OrderHistoryService
     */
    private $orderHistoryService;

    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @var StoreOrder
     */
    private $order;

    /** @var PaymentService */
    protected $paymentService;

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /** @var PaymentStoreOrderService */
    protected $paymentStoreOrderService;

    public function injectDependencies(
        OrderHistoryService $orderHistoryService,
        Emailer $emailer,
        PaymentStoreOrderService $paymentStoreOrderService,
        PaymentService $paymentService,
        PaymentAccountService $paymentAccountService
    )
    {
        $this->orderHistoryService      = $orderHistoryService;
        $this->emailer                  = $emailer;
        $this->paymentStoreOrderService = $paymentStoreOrderService;
        $this->paymentService           = $paymentService;
        $this->paymentAccountService    = $paymentAccountService;
    }

    /**
     * @param PaymentTransactionRefund $paymentTransactionRefund
     * @return void
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function processRefund(PaymentTransactionRefund $paymentTransactionRefund): void
    {
        $paymentTransactionRefund->tryValidate();
        $paymentTransaction    = $paymentTransactionRefund->transaction;
        $paymentDetail         = $paymentTransaction->firstPaymentDetail;
        $refundToUser          = $paymentDetail->paymentDetailOperation->toPaymentDetail()->paymentAccount->user;
        $payment               = $paymentDetail->paymentDetailOperation->payment;
        $invoice               = $payment->paymentInvoice;
        $storeOrder            = $invoice->storeOrder;
        $refundToAccount       = $this->paymentAccountService->getUserPaymentAccount($refundToUser, PaymentAccount::ACCOUNT_TYPE_REFUND);
        $refundDetailOperation = null;

        // Only if the refund comes from the manufacturer
        if ($paymentTransactionRefund->isRefundFromManufacturer()) {
            $manufacturerAwardWithRefund = $invoice->getAmountManufacturerAwardWithRefund();

            if (
                $manufacturerAwardWithRefund &&
                round($paymentTransactionRefund->getMoneyAmount()->getAmount(), 2) >= round($manufacturerAwardWithRefund->getAmount(), 2)
            ) {
                $fullRefundAmount = $this->availableTransactionRefund($paymentTransaction);

                $paymentTransactionRefund->amount       = $fullRefundAmount->getAmount();
                $paymentTransactionRefund->currency     = $fullRefundAmount->getCurrency();
                $paymentTransactionRefund->isFullRefund = true;
                $paymentTransactionRefund->safeSave();

            }
        }

        $this->orderHistoryService->logRefund(
            $storeOrder,
            $paymentTransactionRefund->transaction,
            $paymentTransactionRefund->getMoneyAmount(),
            $paymentTransactionRefund->comment,
            $paymentTransactionRefund->user
        );

        $paymentCheckout = new PaymentCheckout($paymentTransactionRefund->transaction->vendor);

        if ($this->paymentService->isMoneyOnReservedAccount($paymentTransaction)) {
            /** @var PaymentDetail $reservedDetail */
            $reservedDetail  = $payment->getPaymentDetails()->toReserved()->one();
            $reservedAccount = $reservedDetail->paymentDetailOperation->toPaymentDetail()->paymentAccount; // Money always send to reserve by payment
            $amountMoney = Money::create($paymentTransactionRefund->getMoneyAmount()->getAmount(), $reservedAccount->currency);

            // If money reserved and full refund
            if ($paymentTransactionRefund->isFullRefund) {
                $this->paymentService->transferMoney(
                    $payment,
                    $reservedAccount,
                    $refundToAccount,
                    $amountMoney,
                    PaymentDetail::TYPE_REFUND_REQUEST,
                    'Full refund transaction: ' . $paymentTransactionRefund->id
                );

                $refundDetailOperation = $paymentCheckout->refund($paymentTransactionRefund, $refundToAccount);

                $payment->status = Payment::STATUS_RETURNED;
                $invoice->status = PaymentInvoice::STATUS_REFUNDED;
                $payment->safeSave();
                $invoice->safeSave();

                if ($storeOrder && ($storeOrder->getPrimaryInvoice()->uuid == $invoice->uuid)) {
                    // Main invoice;
                    $storeOrder->order_state = StoreOrder::STATE_CANCELED;
                    $storeOrder->safeSave();

                    if ($storeOrder->currentAttemp) {
                        $storeOrder->currentAttemp->status = StoreOrderAttemp::STATUS_CANCELED;
                        $storeOrder->currentAttemp->safeSave(['status']);
                    }
                }

                if ($invoice->baseByAdditionalService()) {
                    $invoice->storeOrderPosition->status = StoreOrderPosition::STATUS_REFUNDED;
                    $invoice->storeOrderPosition->safeSave(['status']);
                }

                if ($invoice->baseByTsInternalPurchase()) {
                    $invoice->tsInternalPurchase->status = TsInternalPurchase::STATUS_CANCELED;
                    $invoice->tsInternalPurchase->safeSave(['status']);
                }
            } else {
                // Partial refund. Form new invoice
                // Do nothing all calculations will be in invoice output
                // Every output will use also possible refunds
                $this->paymentService->transferMoney($payment, $reservedAccount, $refundToAccount, $amountMoney, PaymentDetail::TYPE_REFUND_REQUEST,
                    'Partital refund transaction: ' . $paymentTransactionRefund->id);

                $refundDetailOperation = $paymentCheckout->refund($paymentTransactionRefund, $refundToAccount);

                if (($bonusAccured = $invoice->getAmountBonusAccrued()) && ($paymentTransactionRefund->refund_type!==PaymentTransactionRefund::REFUND_TYPE_SHIPPING)) {
                    $invoice->resetAccountingBonusAccuredAndSave();
                    if ($storeOrder && $storeOrder->currentAttemp) {
                        $this->orderHistoryService->log($storeOrder, StoreOrderHistory::ORDER_ATTEMPT_CHANGE_STATUS, null, 'Remove bonus accured: '.$bonusAccured->getAmount());
                    }
                }
            }
        } else {
            // Payment was closed. Refund money to user
            if ($paymentTransactionRefund->isFullRefund) {

                $treatstockAccount      = $this->paymentAccountService->getTreatstockAccount();
                $moneyFromTreatstock    = $paymentTransactionRefund->getMoneyAmount();
                $manufacturerAwardMoney = $invoice->getAmountManufacturerAwardWithRefund();

                // We cannot transfer all money back, it may be already send to paypal payout
                // Transfer money back from ps, other summ will be payed by treatstock
                // Make user minus balance for print
                if ($manufacturerAwardMoney && $manufacturerAwardMoney->getAmount() > 0) {
                    $awardMainAccount = $this->paymentAccountService->getUserPaymentAccount($invoice->storeOrder->currentAttemp->ps->user, PaymentAccount::ACCOUNT_TYPE_MAIN);

                    // Write off money from the manufacturer
                    $this->paymentService->transferMoney(
                        $payment,
                        $awardMainAccount,
                        $refundToAccount,
                        $manufacturerAwardMoney,
                        PaymentDetail::TYPE_REFUND_REQUEST,
                        'Full refund for manufacture'
                    );

                    $moneyFromTreatstock = MoneyMath::minus($moneyFromTreatstock, $manufacturerAwardMoney);
                }

                $this->paymentService->transferMoney($payment, $treatstockAccount, $refundToAccount, $moneyFromTreatstock, PaymentDetail::TYPE_REFUND_REQUEST,
                    'Full refund treatstock compensation');

                $refundDetailOperation = $paymentCheckout->refund($paymentTransactionRefund, $refundToAccount);

                $payment->status = Payment::STATUS_RETURNED;
                $invoice->status = PaymentInvoice::STATUS_REFUNDED;
                $payment->safeSave();
                $invoice->safeSave();

                if ($storeOrder->getPrimaryInvoice()->uuid === $invoice->uuid) {
                    // Main invoice;
                    $storeOrder->order_state = StoreOrder::STATE_CANCELED;
                    $storeOrder->safeSave();

                    if ($storeOrder->currentAttemp) {
                        $storeOrder->currentAttemp->status = StoreOrderAttemp::STATUS_CANCELED;
                        $storeOrder->currentAttemp->safeSave(['status']);
                    }
                }

                if ($invoice->baseByAdditionalService()) {
                    $invoice->storeOrderPosition->status = StoreOrderPosition::STATUS_REFUNDED;
                    $invoice->storeOrderPosition->safeSave(['status']);
                }
            } else {
                $paymentCheckout->checkAllowPartitalRefund();

                // Partial refund
                $fromAccount = $this->paymentAccountService->getTreatstockAccount();

                if ($paymentTransactionRefund->isRefundFromManufacturer()) {
                    $fromAccount = $this->paymentAccountService->getUserPaymentAccount($storeOrder->currentAttemp->ps->user);
                }

                $this->paymentService->transferMoney(
                    $payment,
                    $fromAccount,
                    $refundToAccount,
                    $paymentTransactionRefund->getMoneyAmount(),
                    PaymentDetail::TYPE_REFUND_REQUEST,
                    'Partial refund transaction: ' . $paymentTransactionRefund->id
                );

                $refundDetailOperation = $paymentCheckout->refund($paymentTransactionRefund, $refundToAccount);
            }
        }
    }


    /**
     * create pre request form in new status
     * later moderator should approve this refund request
     *
     * @param PaymentInvoice $paymentInvoice
     * @param PaymentTransactionRefund $refundModel
     *
     * @return PaymentTransactionRefund
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     */
    public function addRequestRefund(PaymentInvoice $paymentInvoice, PaymentTransactionRefund $refundModel): PaymentTransactionRefund
    {
        $refundModel->status      = PaymentTransactionRefund::STATUS_NEW;
        $totalPsFee               = $paymentInvoice->getAmountManufacturerAwardWithRefund();
        if($paymentInvoice->storeOrder->hasCuttingPack()){
            $refundModel->refund_type = PaymentTransactionRefund::REFUND_TYPE_PS_CUTTING;
        } else {
            $refundModel->refund_type = PaymentTransactionRefund::REFUND_TYPE_PS;
        }

        if (!$refundModel->validate()) {
            throw new BusinessException(Html::errorSummary($refundModel));
        }

        if (!$this->canRefundOrder($paymentInvoice->storeOrder)) {
            throw new BusinessException(_t('site.order', 'Partial refund unavailable.'));
        }

        if ((float)$refundModel->amount <= 0) {
            throw new BusinessException(_t('site.order', 'Valid amount required.'));
        }

        if (!$totalPsFee || ($totalPsFee && $refundModel->amount > $totalPsFee->getAmount())) {
            throw new BusinessException(_t('site.order', 'Partial refund amount cannot exceed your fee for this order.'));
        }

        $refundModel->save();
        $this->emailer->sendPreRefundRequested($paymentInvoice->storeOrder, $refundModel);

        return $refundModel;
    }

    /**
     * @param StoreOrder $order
     * @return bool
     */
    public function canRefundOrder(StoreOrder $order)
    {
        if ($order->isThingiverseOrder()) {
            $this->lastErrorMessage = _t('site.order', 'Partial refund not available for API partner.');
            return false;
        }
        if ($order->currentAttemp && $order->currentAttemp->status == StoreOrderAttemp::STATUS_NEW) {
            $this->lastErrorMessage = _t('site.order', 'Order is not accepted.');
            return false;
        }
        if ($order->isResolved()) {
            $this->lastErrorMessage = _t('site.order', 'Order is completed.');
            return false;
        }
        if ($order->isCancelled()) {
            $this->lastErrorMessage = _t('site.order', 'Order is canceled.');
            return false;
        }
        return true;
    }

    /**
     * test if order can be refunded.
     * currently just check first payment transaction
     *
     * @param StoreOrder $order
     * @return bool
     * @throws \common\modules\payment\exception\PaymentException
     */
    public function canRefundOrderTransaction(StoreOrder $order)
    {
        if (!$this->canRefundOrder($order)) {
            return false;
        }

        $paymentTransaction = $this->paymentStoreOrderService->findOrderPaymentTransaction($order);
        if (!$paymentTransaction) {
            $this->lastErrorMessage = _t('site.order', 'No transactions to refund.');
            return false;
        }
        $ptr = PaymentTransactionRefund::findOne(['transaction_id' => $paymentTransaction->id]);
        if ($ptr) {
            $this->lastErrorMessage = _t('site.order', 'No valid payments for this order');
            return false;
        }
        return $this->canRefund($paymentTransaction);
    }

    /**
     * returns all refund requests filtered by PS refund types
     *
     * @param StoreOrder $order
     * @return PaymentTransactionRefund[]|null
     * @throws \common\modules\payment\exception\PaymentException
     */
    public function getRefundRequestByOrder(StoreOrder $order)
    {
        $paymentTransaction = $this->paymentStoreOrderService->findOrderPaymentTransaction($order);
        if (!$paymentTransaction) {
            return null;
        }
        $ptrs = PaymentTransactionRefund::find()->where(
            [
                'transaction_id' => $paymentTransaction->id,
                'refund_type'    => [
                    PaymentTransactionRefund::REFUND_TYPE_SHIPPING,
                    PaymentTransactionRefund::REFUND_TYPE_PS_FEE,
                    PaymentTransactionRefund::REFUND_TYPE_MANUFACTURER_WITH_PACKAGE,
                    PaymentTransactionRefund::REFUND_TYPE_PS,
                    PaymentTransactionRefund::REFUND_TYPE_ALL,
                    PaymentTransactionRefund::REFUND_TYPE_PS_CUTTING
                ]
            ]
        )->all();
        return $ptrs;
    }

    /**
     * Check if can request refund
     *
     * @param PaymentTransaction $transaction
     * @param Money|null $amount
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function canRefund(PaymentTransaction $transaction, Money $amount = null)
    {
        $paymentTransactionRefund = new PaymentTransactionRefund();
        $paymentTransactionRefund->validateTransaction($transaction, $amount);
        if ($paymentTransactionRefund->getErrors()) {
            return false;
        }
        return true;
    }

    /**
     * @param PaymentTransaction $transaction
     * @return Money
     */
    public function availableTransactionRefund(PaymentTransaction $transaction): Money
    {
        $refundAmount = $transaction->getPaymentTransactionRefunds()->approved()->sum('amount');
        $amount       = $transaction->amount - $refundAmount;
        return Money::create($amount, $transaction->currency);
    }


    /**
     * check if moderator can do more refund.
     * refund is checked for total amount by given transaction
     *
     * @param PaymentTransaction $transaction
     * @param Money|null $amount
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function canRefundByModerator(PaymentTransaction $transaction, Money $amount = null): bool
    {
        if ($transaction->firstPaymentDetail->paymentDetailOperation->payment->paymentInvoice->instantPayment) {
            return false;
        }
        if ($transaction->vendor === PaymentTransaction::VENDOR_BANK_TRANSFER) {
            return false;
        }

        $maxRefundedAmount = $this->availableTransactionRefund($transaction);

        if (!$amount) {
            $amount = $maxRefundedAmount;
        }

        if ($maxRefundedAmount->getAmount() > $amount->getAmount()) {
            return false;
        }

        return $this->canRefund($transaction, $amount);
    }


    /**
     * get total refunded amount for given transaction
     *
     * @param PaymentTransaction $transaction
     * @return Money
     */
    private function getTotalRefundAmount(PaymentTransaction $transaction)
    {
        $refundTransactions  = $transaction->paymentTransactionRefunds;
        $totalRefundedAmount = 0;
        foreach ($refundTransactions as $refundTransaction) {
            if ($refundTransaction->status !== PaymentTransactionRefund::STATUS_APPROVED) {
                continue;
            }
            $totalRefundedAmount = $totalRefundedAmount + $refundTransaction->amount;
        }
        return Money::create($totalRefundedAmount, $transaction->currency);
    }

    /**
     * @param StoreOrder $order
     * @return PaymentDetail[]
     */
    public function getApprovedRefundsByOrder(StoreOrder $order)
    {
        // Refund from user
        $paymentDetails = $order->getPaymentDetails()->type(PaymentDetail::TYPE_REFUND)->andWhere('payment_detail.amount<0')->all();
        return $paymentDetails;
    }

    /**
     * @param PaymentInvoice $invoice
     *
     * @return PaymentDetail[]
     */
    public function getApprovedRefundsByInvoice(PaymentInvoice $invoice): array
    {
        return $invoice->storeOrder->getPaymentDetails()->type(PaymentDetail::TYPE_REFUND)->andWhere('payment_detail.amount<0')->all();
    }
}
