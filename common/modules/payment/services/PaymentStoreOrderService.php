<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.10.18
 * Time: 10:31
 */

namespace common\modules\payment\services;

use common\components\Emailer;
use common\components\exceptions\BusinessException;
use common\components\exceptions\InvalidModelException;
use common\components\order\history\OrderHistoryService;
use common\models\Company;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\modules\affiliate\services\AffiliateService;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\components\PaymentLogger;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use Yii;
use yii\base\BaseObject;
use yii\base\UserException;
use yii\console\Exception;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;

class PaymentStoreOrderService extends BaseObject
{

    /** @var PaymentService */
    protected $paymentService;

    /** @var PaymentLogger */
    protected $paymentLogger;

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /** @var PaymentTaxService */
    protected $paymentTaxService;

    /** @var AffiliateService */
    protected $affiliateService;

    /** @var Emailer */
    protected $emailer;

    /** @var OrderHistoryService */
    protected $history;

    /**
     * @param PaymentService $paymentService
     * @param PaymentLogger $paymentLogger
     * @param PaymentAccountService $paymentAccountService
     * @param PaymentTaxService $paymentTaxService
     * @param AffiliateService $affiliateService
     * @param OrderHistoryService $history
     */
    public function injectDependencies(
        PaymentService        $paymentService,
        PaymentLogger         $paymentLogger,
        PaymentAccountService $paymentAccountService,
        PaymentTaxService     $paymentTaxService,
        AffiliateService      $affiliateService,
        Emailer               $emailer,
        OrderHistoryService   $history
    )
    {
        $this->paymentService        = $paymentService;
        $this->paymentLogger         = $paymentLogger;
        $this->paymentAccountService = $paymentAccountService;
        $this->paymentTaxService     = $paymentTaxService;
        $this->affiliateService      = $affiliateService;
        $this->emailer               = $emailer;
        $this->history               = $history;
    }


    public function submitForSettleStoreOrderPayment(StoreOrder $storeOrder)
    {
        try {
            $invoice = $storeOrder->getPrimaryInvoice();
            $this->paymentService->submitForSettleInvoice($invoice);
        } catch (\Exception $exception) {
            $this->history->logFailedPayment($storeOrder, 'submitForSettleStoreOrderPayment', $exception->getMessage());
        }
    }

    /**
     * @param PaymentDetail $fromPaymentDetail
     * @param PaymentTransactionHistory $paymentTransactionHistory
     * @param string $oldTransactionStatus
     * @param $newTransactionStatus
     *
     * @throws InvalidModelException
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     */
    protected function transferFromAuthorizedToReserved(
        PaymentDetail             $fromPaymentDetail,
        PaymentTransactionHistory $paymentTransactionHistory,
                                  $oldTransactionStatus,
                                  $newTransactionStatus
    )
    {
        if (!$fromPaymentDetail->isTypeFrom()) {
            throw new PaymentException('First payment detail should be to set transaction invoice.');
        }
        if ($toPaymentDetail = $fromPaymentDetail->paymentDetailOperation->toPaymentDetail()) {
            if ($toPaymentDetail->paymentAccount->type === PaymentAccount::ACCOUNT_TYPE_AUTHORIZE && \in_array($oldTransactionStatus,
                    [PaymentTransaction::STATUS_REQUIRES_CAPTURE, PaymentTransaction::STATUS_AUTHORIZED, PaymentTransaction::STATUS_SUBMITTED_FOR_SETTLEMENT, PaymentTransaction::STATUS_SETTLING], false)
            ) {
                if ($newTransactionStatus === PaymentTransaction::STATUS_SETTLED ||
                    $newTransactionStatus === PaymentTransaction::STATUS_PAID ||
                    $newTransactionStatus === PaymentTransaction::STATUS_SUCCEEDED
                ) { // Money recived from client
                    // Check is money already transferred to invoice directly from authorized?
                    $payment             = $fromPaymentDetail->paymentDetailOperation->payment;
                    $fromAutorizedDetail = $payment->getPaymentDetails()->fromAuthorize()->one();
                    if (!$fromAutorizedDetail) {
                        /** @var PaymentDetail $paymentDetailFrom */
                        /** @var PaymentDetail $paymentDetailTo */
                        [$paymentDetailFrom, $paymentDetailTo] = $this->paymentService->transferMoney(
                            $fromPaymentDetail->paymentDetailOperation->payment,
                            $toPaymentDetail->paymentAccount,
                            $this->paymentAccountService->getUserPaymentAccount($toPaymentDetail->paymentAccount->user, PaymentAccount::ACCOUNT_TYPE_RESERVED, $toPaymentDetail->paymentAccount->currency),
                            $toPaymentDetail->getMoneyAmount());
                        $paymentTransactionHistory->payment_detail_id = $paymentDetailFrom->id;
                    } else {
                        $paymentTransactionHistory->payment_detail_id = $fromAutorizedDetail->id;
                    }
                    $paymentTransactionHistory->safeSave();
                }
                if ($newTransactionStatus === PaymentTransaction::STATUS_AUTHORIZED_EXPIRED ||
                    $newTransactionStatus === PaymentTransaction::STATUS_VOIDED ||
                    $newTransactionStatus === PaymentTransaction::STATUS_SETTLE_DECLINE ||
                    $newTransactionStatus === PaymentTransaction::STATUS_UP_FAILED ||
                    $newTransactionStatus === PaymentTransaction::STATUS_FAILED
                ) {
                    // Make transfer back!
                    /** @var PaymentDetail $paymentDetailFrom */
                    /** @var PaymentDetail $paymentDetailTo */
                    [$paymentDetailFrom, $paymentDetailTo] = $this->paymentService->transferMoney(
                        $fromPaymentDetail->paymentDetailOperation->payment,
                        $toPaymentDetail->paymentAccount,
                        $fromPaymentDetail->paymentAccount,
                        $toPaymentDetail->getMoneyAmount()
                    );
                    $paymentTransactionHistory->payment_detail_id = $paymentDetailFrom->id;
                    $paymentTransactionHistory->safeSave();
                    $msg = 'Payment detail(' . $paymentDetailFrom->id . ') transaction settle failed: ' . $newTransactionStatus . '. Order: ' . ($paymentDetailFrom->paymentDetailOperation->payment->paymentInvoice->store_order_id ?? '');
                    $this->emailer->sendSupportMessage('Payment detail(' . $paymentDetailFrom->id . ') transaction settle failed: ' . $newTransactionStatus . '.', $msg);
                }
            }
        }
    }

    public function updatePaymentTransactionStatus(PaymentTransaction $paymentTransaction, PaymentGatewayTransaction $gatewayVendorTransaction, PaymentCheckout $checkoutObj)
    {
        $oldStatus                 = $paymentTransaction->status;
        $paymentTransactionHistory = $this->paymentService->updateTransactionStatus($paymentTransaction, $gatewayVendorTransaction->status);
        if ($paymentTransactionHistory) { // Status was changed
            $paymentInvoiceStatus = $checkoutObj->transactionStatusToInvoiceStatus($paymentTransaction->status);

            $this->paymentLogger->log(PaymentLogger::TYPE_PAYMENT_DETAIL, 'Payment status changed',
                [
                    'paymentTransactionHistory' => $paymentTransactionHistory->attributes,
                    'invoiceStatus'             => $paymentInvoiceStatus,
                    'gatewayTransaction'        => json_decode(json_encode($gatewayVendorTransaction), true)
                ],
                [
                    PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $paymentTransaction->transaction_id,
                    PaymentLogger::MARKER_PAYMENT_DETAIL_ID     => $paymentTransaction->firstPaymentDetail->id,
                    PaymentLogger::MARKER_PAYMENT_ID            => $paymentTransaction->firstPaymentDetail->paymentDetailOperation->payment->id,
                ], PaymentLogger::LEVEL_INFO);
            if ($paymentTransaction->type === PaymentTransaction::TYPE_PAYMENT) {
                $paymentInvoice = $paymentTransaction->firstPaymentDetail->paymentDetailOperation->payment->paymentInvoice;
                $paymentInvoice->setNewStatus($paymentInvoiceStatus);
                $paymentInvoice->safeSave();

                // Fix payment transfer
                $this->transferFromAuthorizedToReserved($paymentTransaction->firstPaymentDetail, $paymentTransactionHistory, $oldStatus, $paymentTransaction->status);
            }
            return true;
        }
        return false;
    }

    /**
     *
     * @param StoreOrder $storeOrder
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function refreshPaymentStatus(StoreOrder $storeOrder)
    {
        /** @var PaymentTransaction $paymentTransaction */
        $paymentTransaction = $this->findOrderPaymentTransaction($storeOrder);

        if (!$paymentTransaction) {
            return false;
        }

        $checkoutObj              = new PaymentCheckout($paymentTransaction->vendor);
        $gatewayVendorTransaction = $checkoutObj->getTransaction($paymentTransaction->transaction_id);

        if ($gatewayVendorTransaction) {
            $this->updatePaymentTransactionStatus($paymentTransaction, $gatewayVendorTransaction, $checkoutObj);
            return true;
        }
        return false;
    }

    public function receivedOrderCuttingPack(StoreOrderAttemp $attemp)
    {
        $order   = $attemp->order;
        $invoice = $order->getPrimaryInvoice();

        if (!$invoice) {
            throw new PaymentException('Null invoice is impossible');
        }

        if ($attemp->status !== StoreOrderAttemp::STATUS_RECEIVED && $attemp->status !== StoreOrderAttemp::STATUS_SENT) {
            throw new PaymentException(
                _t("site.payment", 'Store order #{orderId} status is wrong', ['orderId' => $order->id])
            );
        }
        \Yii::info('Receive order process: ' . $order->id, 'order');

        $vendorTransaction = $this->findOrderPaymentTransaction($order);
        if (!$vendorTransaction) {
            throw new PaymentException('No transaction found');
        }
        $firstPaymentDetail    = $vendorTransaction->firstPaymentDetail;
        $firstPaymentOperation = $firstPaymentDetail->paymentDetailOperation;
        $payment               = $firstPaymentOperation->payment;

        $clientUser = $firstPaymentOperation->payment->paymentInvoice->user;

        if (
            !$order->getIsTestOrder() &&
            ($clientUser->id !== $firstPaymentOperation->toPaymentDetail()->paymentAccount->user_id) &&
            ($firstPaymentOperation->toPaymentDetail()->paymentAccount->user_id !== User::USER_ANONIM)
        ) {
            \Yii::info('Not same user. Invoice and transaction diff. ' . $order->id, 'order');
        }

        $totalAmount = $invoice->getAmountTotalWithRefund();

        $reservedOperation = $this->paymentService->findReservedDetail($payment);

        // Not reserved!!! We transfer money directly from autorized to invoice
        $accountReservedType    = $reservedOperation ? PaymentAccount::ACCOUNT_TYPE_RESERVED : PaymentAccount::ACCOUNT_TYPE_AUTHORIZE;
        $reservedCurrency       = $reservedOperation ? $reservedOperation->fromPaymentDetail()->original_currency : $invoice->currency;
        $clientUserBonusAccount = $this->paymentAccountService->getUserPaymentAccount($clientUser, PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);
        $accountReserved        = $this->paymentAccountService->getUserPaymentAccount($clientUser, $accountReservedType, $reservedCurrency);
        $accountInvoice         = $this->paymentAccountService->getUserPaymentAccount($clientUser, PaymentAccount::ACCOUNT_TYPE_INVOICE, $invoice->currency);

        /** @var Company $awardCompany */
        $awardCompany = $attemp->ps;

        $accountManufactureAward   = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_MANUFACTURE_AWARD, $invoice->currency);
        $accountCompanyAward       = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_AWARD, $invoice->currency);
        $accountTreatstockEasyPost = $this->paymentAccountService->getTreatsockEasyPostAccount($invoice->currency);
        $accountTreatstockFee      = $this->paymentAccountService->getTreatsockFeeAccount($invoice->currency);
        $accountTreatstock         = $this->paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_MAIN, $invoice->currency);
        $accountTreatstockBonus    = $this->paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);
        $accountCompanyMain        = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_MAIN, $invoice->currency);


        if ($bonusAmount = $invoice->getAmountBonusWithRefund()) {
            $convertAccountType     = PaymentAccount::getConvertAccountType(Currency::BNS, $invoice->currency);
            $convertAccountBNS      = $this->paymentAccountService->getConvertAccount(Currency::BNS, $convertAccountType);
            $convertAccountCurrency = $this->paymentAccountService->getConvertAccount($invoice->currency, $convertAccountType);

            $this->paymentService->transferMoney(
                $payment,
                $accountReserved,
                $convertAccountBNS,
                Money::create($bonusAmount->getAmount(), Currency::BNS),
                'Convert bonus to money: ' . $invoice->uuid);
            $this->paymentService->transferMoney(
                $payment,
                $convertAccountCurrency,
                $accountInvoice,
                Money::create($bonusAmount->getAmount(), $invoice->currency),
                PaymentDetail::TYPE_PAYMENT,
                'Invoice total payment with conversation: ' . $invoice->uuid
            );
        }

        // Transfer money to client:invoice payment account
        if ($totalAmount->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountReserved, $accountInvoice, $totalAmount, 'Invoice total payment');
        }

        // Payment Method Fee
        $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();

        if ($amountPaymentMethodFee) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountTreatstockFee, $amountPaymentMethodFee, PaymentDetail::TYPE_PAYMENT,
                'Payment method fee'
            );
        }
        // ---

        // Transfer from invoice account to destinations
        $manufactureAwardAmount = $invoice->storeOrderAmount->getAmountCuttingWithRefund();

        if ($manufactureAwardAmount) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountManufactureAward, $manufactureAwardAmount, PaymentDetail::TYPE_PAYMENT,
                'Manufacture award'
            );
        }

        $treatstockFee = $invoice->storeOrderAmount->getAmountPsFeeWithRefund();
        if ($treatstockFee && $treatstockFee->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountTreatstockFee, $treatstockFee, PaymentDetail::TYPE_PAYMENT,
                'Payment fee to treatstock');
        }

        $cuttingAward = $invoice->storeOrderAmount->getAmountCuttingForPsWithRefund();
        $invoice->storeOrderAmount->getAmountPrintForPsWithRefund();

        if ($cuttingAward) {
            $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountCompanyAward, $cuttingAward, PaymentDetail::TYPE_AWARD_PRINT,
                'Reward for cutting'
            );
        }

        $shippingFee = $invoice->storeOrderAmount->getAmountShippingWithRefund();
        // If shipping by treatstock

        if ($shippingFee) {
            if ($order->isDeliveryByTreatstock()) {
                $this->paymentService->transferMoney($payment, $accountInvoice, $accountTreatstockEasyPost, $shippingFee, PaymentDetail::TYPE_PAYMENT,
                    'Payment for easypost to treatstock');
            } else {
                $this->paymentService->transferMoney($payment, $accountInvoice, $accountCompanyAward, $shippingFee, PaymentDetail::TYPE_PAYMENT,
                    'Payment for shipping to company');
            }
        } else {
            $deliveryParams = $order->hasDelivery() && $order->currentAttemp->machine ? $order->currentAttemp->machine->asDeliveryParams() : null;
            if ($deliveryParams && $deliveryParams->isFreeDelivery($order)) {
                $paymentToEasyPost = $payment->getPaymentDetails()->toEasyPost()->one();
                if ($paymentToEasyPost) {
                    $shippingFeeForTreatstock = MoneyMath::abs($paymentToEasyPost->getMoneyAmount());
                    // Get shipping from company award
                    $this->paymentService->transferMoney($payment, $accountCompanyAward, $accountTreatstockEasyPost, $shippingFeeForTreatstock, PaymentDetail::TYPE_PAYMENT,
                        'Payment for easypost to treatstock');
                }
            }
        }

        $packageFee = $invoice->storeOrderAmount->getAmountPackageWithRefund();
        if ($packageFee) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountCompanyAward, $packageFee, PaymentDetail::TYPE_PAYMENT, 'Package fee');
        }

        $transactionAward = $invoice->storeOrderAmount->getAmountTransaction();
        if ($transactionAward) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountTreatstock, $transactionAward, PaymentDetail::TYPE_PAYMENT,
                'Transaction additional');
        }

        $bonusAccruedFromPs = $invoice->getAmountBonusAccruedFromPs();
        $bonusAccruedFromTs = $invoice->getAmountBonusAccruedFromTs();
        $bonusFromDescr     = " for order " . $order->id;
        if ($bonusAccruedFromTs?->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountTreatstockBonus, $clientUserBonusAccount, $bonusAccruedFromTs, PaymentDetail::TYPE_PAYMENT,
                'Bonus from Treatstock' . $bonusFromDescr);
        }
        if ($bonusAccruedFromPs?->getAmount()) {
            $convertAccountType     = PaymentAccount::getConvertAccountType($invoice->currency, Currency::BNS);
            $convertAccountBNS      = $this->paymentAccountService->getConvertAccount(Currency::BNS, $convertAccountType);
            $convertAccountCurrency = $this->paymentAccountService->getConvertAccount($invoice->currency, $convertAccountType);
            $this->paymentService->transferMoney($payment, $accountCompanyMain, $convertAccountCurrency, Money::create($bonusAccruedFromPs->getAmount(), $invoice->currency), PaymentDetail::TYPE_PAYMENT, 'Bonus' . $bonusFromDescr);
            $this->paymentService->transferMoney($payment, $convertAccountBNS, $clientUserBonusAccount, $bonusAccruedFromPs, PaymentDetail::TYPE_PAYMENT, 'Bonus from "' . mb_ucfirst($awardCompany->title) . '"' . $bonusFromDescr);
        }


        $calcTaxesForUsers[$accountCompanyAward->user->id] = $accountCompanyAward->user;

        $totalAward = $this->paymentAccountService->calculateAccountAmount($accountCompanyAward, $payment);
        if ($totalAward->getAmount() < 0) {
            $totalAward = Money::zero($totalAward->getCurrency());
        }
        $this->paymentService->transferMoney($payment, $accountCompanyAward, $accountCompanyMain, $totalAward, PaymentDetail::TYPE_PAYMENT, 'Transfer award money');

        $this->paymentTaxService->sendTaxNotifications($calcTaxesForUsers);

        $this->receivedOrderAdditionalServices($attemp);

        if ($order->isThingiverseOrder()) {
            $this->transferThingiverseCommission($order, $payment);
        }

        \Yii::info("FINISHED Order process " . $order->id . ". PaymentID: $payment->id\n\n\n", 'order');
    }

    /**
     * The order status was set as “Received” -
     * We get money from TS account and put them to author and print service
     *
     * @param StoreOrderAttemp $attemp
     *
     * @return mixed
     * @throws Exception
     * @throws InvalidModelException
     * @throws PaymentException
     * @throws PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function receivedOrderPrint3dPayment(StoreOrderAttemp $attemp)
    {
        $order   = $attemp->order;
        $invoice = $order->getPrimaryInvoice();

        if (!$invoice) {
            throw new PaymentException('Null invoice is impossible');
        }

        if ($attemp->status !== StoreOrderAttemp::STATUS_RECEIVED && $attemp->status !== StoreOrderAttemp::STATUS_SENT) {
            throw new PaymentException(
                _t("site.payment", 'Store order #{orderId} status is wrong', ['orderId' => $order->id])
            );
        }
        \Yii::info('Receive order process: ' . $order->id, 'order');

        $vendorTransaction = $this->findOrderPaymentTransaction($order);
        if (!$vendorTransaction) {
            throw new PaymentException('No transaction found');
        }
        $firstPaymentDetail    = $vendorTransaction->firstPaymentDetail;
        $firstPaymentOperation = $firstPaymentDetail->paymentDetailOperation;
        $payment               = $firstPaymentOperation->payment;

        $clientUser = $firstPaymentOperation->payment->paymentInvoice->user;

        if ($order->getIsTestOrder()) {
            return;
        }

        if (
            ($clientUser->id !== $firstPaymentOperation->toPaymentDetail()->paymentAccount->user_id) &&
            ($firstPaymentOperation->toPaymentDetail()->paymentAccount->user_id !== User::USER_ANONIM)
        ) {
            \Yii::info('Not same user. Invoice and transaction diff. ' . $order->id, 'order');
        }

        $totalAmount = $invoice->getAmountTotalWithRefund();

        $reservedOperation = $this->paymentService->findReservedDetail($payment);

        // Not reserved!!! We transfer money directly from autorized to invoice
        $accountReservedType    = $reservedOperation ? PaymentAccount::ACCOUNT_TYPE_RESERVED : PaymentAccount::ACCOUNT_TYPE_AUTHORIZE;
        $reservedCurrency       = $reservedOperation ? $reservedOperation->fromPaymentDetail()->original_currency : $invoice->currency;
        $clientUserBonusAccount = $this->paymentAccountService->getUserPaymentAccount($clientUser, PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);
        $accountReserved        = $this->paymentAccountService->getUserPaymentAccount($clientUser, $accountReservedType, $reservedCurrency);
        $accountInvoice         = $this->paymentAccountService->getUserPaymentAccount($clientUser, PaymentAccount::ACCOUNT_TYPE_INVOICE, $invoice->currency);

        if ($invoice->hasPromoCode()) {
            $this->paymentService->transferMoney(
                $payment,
                $this->paymentAccountService->getTreatstockDiscountAccount($invoice->currency),
                $accountInvoice,
                $invoice->storeOrderPromocode->getAmountTotal(),
                PaymentDetail::TYPE_PAYMENT,
                'Treatstock promo code for invoice: ' . $invoice->uuid
            );
        }

        if ($bonusAmount = $invoice->getAmountBonusWithRefund()) {
            $convertAccountType     = PaymentAccount::getConvertAccountType(Currency::BNS, $invoice->currency);
            $convertAccountBNS      = $this->paymentAccountService->getConvertAccount(Currency::BNS, $convertAccountType);
            $convertAccountCurrency = $this->paymentAccountService->getConvertAccount($invoice->currency, $convertAccountType);

            $this->paymentService->transferMoney(
                $payment,
                $accountReserved,
                $convertAccountBNS,
                Money::create($bonusAmount->getAmount(), Currency::BNS),
                'Convert bonus to money: ' . $invoice->uuid);
            $this->paymentService->transferMoney(
                $payment,
                $convertAccountCurrency,
                $accountInvoice,
                Money::create($bonusAmount->getAmount(), $invoice->currency),
                PaymentDetail::TYPE_PAYMENT,
                'Invoice total payment with conversation: ' . $invoice->uuid
            );
        }

        // Transfer money to client:invoice payment account
        if ($totalAmount->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountReserved, $accountInvoice, $totalAmount, 'Invoice total payment');
        }

        /** @var Company $awardCompany */
        $model3dReplica   = $attemp->order->getFirstReplicaItem();
        $model3dUserAward = $model3dReplica->getAuthor() ? $model3dReplica->getAuthor() : null;
        $awardCompany     = $attemp->ps;

        $accountModel3dAward       = $model3dUserAward ? $this->paymentAccountService->getUserPaymentAccount($model3dUserAward, PaymentAccount::ACCOUNT_TYPE_AWARD, $invoice->currency) : null;
        $accountModel3dMain        = $model3dUserAward ? $this->paymentAccountService->getUserPaymentAccount($model3dUserAward, PaymentAccount::ACCOUNT_TYPE_MAIN, $invoice->currency) : null;
        $accountManufactureAward   = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_MANUFACTURE_AWARD, $invoice->currency);
        $accountCompanyAward       = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_AWARD, $invoice->currency);
        $accountTreatstockEasyPost = $this->paymentAccountService->getTreatsockEasyPostAccount($invoice->currency);
        $accountTreatstockFee      = $this->paymentAccountService->getTreatsockFeeAccount($invoice->currency);
        $accountTreatstock         = $this->paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_MAIN, $invoice->currency);
        $accountTreatstockBonus    = $this->paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);
        $affiliatePaymentAward     = $invoice->getLinkedAffiliateAward();
        if ($affiliatePaymentAward) {
            $accountAffiliate = $this->paymentAccountService->getUserPaymentAccount($affiliatePaymentAward->affiliateSource->user, PaymentAccount::ACCOUNT_TYPE_MAIN, $invoice->currency) ?? null;
        } else {
            $accountAffiliate = null;
        }
        $accountCompanyMain = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_MAIN, $invoice->currency);

        // Payment Method Fee
        $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();

        if ($amountPaymentMethodFee) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountTreatstockFee, $amountPaymentMethodFee, PaymentDetail::TYPE_PAYMENT,
                'Payment method fee'
            );
        }
        // ---

        // Transfer from invoice account to destinations
        $manufactureAwardAmount = $invoice->storeOrderAmount->getAmountPrintWithRefund();

        if ($manufactureAwardAmount) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountManufactureAward, $manufactureAwardAmount, PaymentDetail::TYPE_PAYMENT,
                'Manufacture award'
            );
        }

        $treatstockFee        = $invoice->storeOrderAmount->getAmountPsFeeWithRefund();
        $affiliateFeeInMoney  = $invoice->getAffiliateFeeIn();
        $affiliateFeeOutMoney = $invoice->getAffiliateExclusive();
        $affiliateFeeMoney    = null;

        if ($affiliateFeeInMoney) {
            $treatstockFee       = MoneyMath::minus($treatstockFee, $affiliateFeeInMoney);
            $affiliateFeeInMoney = $affiliateFeeInMoney;
        }


        if ($affiliateFeeInMoney && $affiliateFeeInMoney->getAmount() && $accountAffiliate && $affiliatePaymentAward) {
            [$paymentDetailFrom, $paymentDetailTo] = $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountAffiliate, $affiliateFeeInMoney, PaymentDetail::TYPE_AFFILIATE,
                'Affiliate payment award: ' . $affiliatePaymentAward->id);
            $affiliatePaymentAward->accrued_payment_detail_id = $paymentDetailTo->id;
            $affiliatePaymentAward->safeSave();
        }

        if ($treatstockFee && $treatstockFee->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountTreatstockFee, $treatstockFee, PaymentDetail::TYPE_PAYMENT,
                'Payment fee to treatstock');
        }

        $printAward = $invoice->storeOrderAmount->getAmountPrintForPsWithRefund();

        if ($affiliateFeeOutMoney) {
            $printAward = MoneyMath::minus($printAward, $affiliateFeeOutMoney);
        }

        if ($affiliateFeeOutMoney && $affiliateFeeOutMoney->getAmount() && $accountAffiliate && $affiliatePaymentAward) {
            [$paymentDetailFrom, $paymentDetailTo] = $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountAffiliate, $affiliateFeeOutMoney, PaymentDetail::TYPE_AFFILIATE,
                'Affiliate payment award: ' . $affiliatePaymentAward->id);
            $affiliatePaymentAward->accrued_payment_detail_id = $paymentDetailTo->id;
            $affiliatePaymentAward->safeSave();
        }

        if ($printAward) {
            $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountCompanyAward, $printAward, PaymentDetail::TYPE_AWARD_PRINT,
                'Reward for printing'
            );
        }

        $shippingFee = $invoice->storeOrderAmount->getAmountShippingWithRefund();
        // If shipping by treatstock

        if ($shippingFee) {
            if ($order->isDeliveryByTreatstock()) {
                $this->paymentService->transferMoney($payment, $accountInvoice, $accountTreatstockEasyPost, $shippingFee, PaymentDetail::TYPE_PAYMENT,
                    'Payment for easypost to treatstock');
            } else {
                $this->paymentService->transferMoney($payment, $accountInvoice, $accountCompanyAward, $shippingFee, PaymentDetail::TYPE_PAYMENT,
                    'Payment for shipping to company');
            }
        } else {
            $deliveryParams = $order->hasDelivery() && $order->currentAttemp->machine ? $order->currentAttemp->machine->asDeliveryParams() : null;
            if ($deliveryParams && $deliveryParams->isFreeDelivery($order)) {
                $paymentToEasyPost = $payment->getPaymentDetails()->toEasyPost()->one();
                if ($paymentToEasyPost) {
                    $shippingFeeForTreatstock = MoneyMath::abs($paymentToEasyPost->getMoneyAmount());
                    // Get shipping from company award
                    $this->paymentService->transferMoney($payment, $accountCompanyAward, $accountTreatstockEasyPost, $shippingFeeForTreatstock, PaymentDetail::TYPE_PAYMENT,
                        'Payment for easypost to treatstock');
                }
            }
        }

        $packageFee = $invoice->storeOrderAmount->getAmountPackageWithRefund();
        if ($packageFee) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountCompanyAward, $packageFee, PaymentDetail::TYPE_PAYMENT, 'Package fee');
        }

        $modelAward = $invoice->storeOrderAmount->getAmountModel3dWithRefund();
        if ($modelAward && $modelAward->getAmount() && $accountModel3dAward) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountModel3dAward, $modelAward, PaymentDetail::TYPE_AWARD_MODEL, 'Model3d designer award');
            $calcTaxesForUsers[$model3dReplica->getAuthor()->id] = $model3dReplica->getAuthor();
            $this->emailer->sendModelAward($order);
            $modelAwardFee = $invoice->storeOrderAmount->getAmountModel3dFee();
            $this->paymentService->transferMoney($payment, $accountModel3dAward, $accountTreatstockFee, $modelAwardFee, PaymentDetail::TYPE_PAYMENT, 'Transfer model 3d fee');
            $totalModelAward = $this->paymentAccountService->calculateAccountAmount($accountModel3dAward, $payment);
            $this->paymentService->transferMoney($payment, $accountModel3dAward, $accountModel3dMain, $totalModelAward, PaymentDetail::TYPE_PAYMENT, "Transfer award model3d money.\n{$model3dReplica->title} ({$model3dReplica->original_model3d_id})");
        }

        $transactionAward = $invoice->storeOrderAmount->getAmountTransaction();
        if ($transactionAward) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountTreatstock, $transactionAward, PaymentDetail::TYPE_PAYMENT,
                'Transaction additional');
        }

        $bonusAccruedFromPs = $invoice->getAmountBonusAccruedFromPs();
        $bonusAccruedFromTs = $invoice->getAmountBonusAccruedFromTs();
        $bonusFromDescr     = " for order " . $order->id;
        if ($bonusAccruedFromTs?->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountTreatstockBonus, $clientUserBonusAccount, $bonusAccruedFromTs, PaymentDetail::TYPE_PAYMENT,
                'Bonus from Treatstock' . $bonusFromDescr);
        }
        if ($bonusAccruedFromPs?->getAmount()) {
            $convertAccountType     = PaymentAccount::getConvertAccountType($invoice->currency, Currency::BNS);
            $convertAccountBNS      = $this->paymentAccountService->getConvertAccount(Currency::BNS, $convertAccountType);
            $convertAccountCurrency = $this->paymentAccountService->getConvertAccount($invoice->currency, $convertAccountType);
            $this->paymentService->transferMoney($payment, $accountCompanyMain, $convertAccountCurrency, Money::create($bonusAccruedFromPs->getAmount(), $invoice->currency), PaymentDetail::TYPE_PAYMENT, 'Money' . $bonusFromDescr);
            $this->paymentService->transferMoney($payment, $convertAccountBNS, $clientUserBonusAccount, $bonusAccruedFromPs, PaymentDetail::TYPE_PAYMENT, 'Bonus from "' . mb_ucfirst($awardCompany->title) . '"' . $bonusFromDescr);
        }

        $calcTaxesForUsers[$accountCompanyAward->user->id] = $accountCompanyAward->user;

        $totalAward = $this->paymentAccountService->calculateAccountAmount($accountCompanyAward, $payment);
        if ($totalAward->getAmount() < 0) {
            $totalAward = Money::zero($totalAward->getCurrency());
        }
        $this->paymentService->transferMoney($payment, $accountCompanyAward, $accountCompanyMain, $totalAward, PaymentDetail::TYPE_PAYMENT, 'Transfer award money');

        $this->paymentTaxService->sendTaxNotifications($calcTaxesForUsers);

        $this->receivedOrderAdditionalServices($attemp);

        if ($order->isThingiverseOrder()) {
            $this->transferThingiverseCommission($order, $payment);
        }

        \Yii::info("FINISHED Order process " . $order->id . ". PaymentID: $payment->id\n\n\n", 'order');
    }

    public function getPsShippingFeePaymentDetail(PaymentInvoice $invoice): ?PaymentDetail
    {
        if (!$invoice->storeOrder || !$invoice->storeOrder->currentAttemp) {
            return null;
        }
        $awardCompany              = $invoice->storeOrder->currentAttemp->ps;
        $accountCompanyAward       = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_AWARD, $invoice->currency);
        $accountTreatstockEasyPost = $this->paymentAccountService->getTreatsockEasyPostAccount($invoice->currency);

        $shippingFee = $this->paymentService->findPaymentDetail($invoice, $accountCompanyAward, $accountTreatstockEasyPost, PaymentDetail::TYPE_PAYMENT);
        return $shippingFee;
    }

    /**
     * @param StoreOrderAttemp $attemp
     *
     * @throws Exception
     * @throws InvalidModelException
     * @throws NotFoundHttpException
     * @throws PaymentException
     * @throws PaymentException
     * @throws \yii\base\InvalidConfigException
     */
    public function receivedOrderByPreorderPayment(StoreOrderAttemp $attemp)
    {
        $order   = $attemp->order;
        $invoice = $order->getPrimaryInvoice();

        if (!$invoice) {
            throw new PaymentException('Null invoice is impossible');
        }

        if ($attemp->status !== StoreOrderAttemp::STATUS_RECEIVED && $attemp->status !== StoreOrderAttemp::STATUS_SENT) {
            throw new PaymentException(
                _t("site.payment", 'Store order #{orderId} status is wrong', ['orderId' => $order->id])
            );
        }

        Yii::info('Receive order process: ' . $order->id, 'order');

        $vendorTransaction = $this->findOrderPaymentTransaction($order);
        if (!$vendorTransaction) {
            throw new PaymentException('No transaction found');
        }
        $firstPaymentDetail    = $vendorTransaction->firstPaymentDetail;
        $firstPaymentOperation = $firstPaymentDetail->paymentDetailOperation;
        $payment               = $firstPaymentOperation->payment;
        $clientUser            = $firstPaymentOperation->payment->paymentInvoice->user;

        if ($clientUser->id !== $firstPaymentOperation->toPaymentDetail()->paymentAccount->user_id) {
            \Yii::info('Not same user. Invoice and transaction diff. ' . $order->id, 'order');
        }

        /** @var Company $awardCompany */
        $awardCompany = $attemp->interceptionCompany();

        $totalAmount            = $invoice->getAmountTotalWithRefund();
        $treatstockFee          = $invoice->preorderAmount->getAmountFeeWithRefund();
        $manufactureAwardAmount = $invoice->preorderAmount->getAmountManufacturerWithRefund();

        // Not reserved!!! We transfer money directly from autorized to invoice
        $accountReservedType = $this->paymentService->findReservedDetail($payment) ? PaymentAccount::ACCOUNT_TYPE_RESERVED : PaymentAccount::ACCOUNT_TYPE_AUTHORIZE;

        $accountReserved         = $this->paymentAccountService->getUserPaymentAccount($clientUser, $accountReservedType, $invoice->currency);
        $accountInvoice          = $this->paymentAccountService->getUserPaymentAccount($clientUser, PaymentAccount::ACCOUNT_TYPE_INVOICE, $invoice->currency);
        $accountCompanyAward     = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_AWARD, $invoice->currency);
        $accountCompanyMain      = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_MAIN, $invoice->currency);
        $accountManufactureAward = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_MANUFACTURE_AWARD, $invoice->currency);
        $accountTreatstockFee    = $this->paymentAccountService->getTreatsockFeeAccount($invoice->currency);
        $clientUserBonusAccount  = $this->paymentAccountService->getUserPaymentAccount($clientUser, PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);
        $accountTreatstockBonus  = $this->paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);

        if ($invoice->hasPromoCode()) {
            $this->paymentService->transferMoney(
                $payment,
                $this->paymentAccountService->getTreatstockDiscountAccount(),
                $accountInvoice,
                $invoice->storeOrderPromocode->getAmountTotal(),
                PaymentDetail::TYPE_PAYMENT,
                'Treatstock promo code for invoice: ' . $invoice->uuid
            );
        }

        if ($bonusAmount = $invoice->getAmountBonusWithRefund()) {
            $convertAccountType     = PaymentAccount::getConvertAccountType(Currency::BNS, $invoice->currency);
            $convertAccountBNS      = $this->paymentAccountService->getConvertAccount(Currency::BNS, $convertAccountType);
            $convertAccountCurrency = $this->paymentAccountService->getConvertAccount($invoice->currency, $convertAccountType);

            $this->paymentService->transferMoney(
                $payment,
                $accountReserved,
                $convertAccountBNS,
                Money::create($bonusAmount->getAmount(), Currency::BNS),
                'Convert bonus to money: ' . $invoice->uuid);
            $this->paymentService->transferMoney(
                $payment,
                $convertAccountCurrency,
                $accountInvoice,
                Money::create($bonusAmount->getAmount(), $invoice->currency),
                PaymentDetail::TYPE_PAYMENT,
                'Invoice total payment with conversation: ' . $invoice->uuid
            );
        }

        // Transfer money to client:invoice payment account
        if ($totalAmount->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountReserved, $accountInvoice, $totalAmount, PaymentDetail::TYPE_PAYMENT,
                'Invoice total payment');
        }

        // Payment Method Fee
        $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();

        if ($amountPaymentMethodFee) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountTreatstockFee, $amountPaymentMethodFee, PaymentDetail::TYPE_PAYMENT,
                'Payment method fee'
            );
        }
        // ---
        $manufactureAward = MoneyMath::sum($treatstockFee, $manufactureAwardAmount);
        $this->paymentService->transferMoney($payment, $accountInvoice, $accountManufactureAward, $manufactureAward,
            PaymentDetail::TYPE_PAYMENT,
            'Money transfer to manufacture account'
        );
        if ($treatstockFee) {
            $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountTreatstockFee, $treatstockFee, PaymentDetail::TYPE_PAYMENT,
                'Payment fee to treatstock'
            );
        }
        $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountCompanyAward, $manufactureAwardAmount, PaymentDetail::TYPE_AWARD_MANUFACTURER,
            'Manufacture award'
        );

        $this->paymentService->transferMoney($payment, $accountCompanyAward, $accountCompanyMain, $manufactureAwardAmount, PaymentDetail::TYPE_PAYMENT,
            'Money for withdrawal from the company account'
        );
        $bonusAccruedFromPs = $invoice->getAmountBonusAccruedFromPs();
        $bonusAccruedFromTs = $invoice->getAmountBonusAccruedFromTs();
        $bonusFromDescr     = " for order " . $order->id;
        if ($bonusAccruedFromTs?->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountTreatstockBonus, $clientUserBonusAccount, $bonusAccruedFromTs, PaymentDetail::TYPE_PAYMENT,
                'Bonus from Treatstock' . $bonusFromDescr);
        }
        if ($bonusAccruedFromPs?->getAmount()) {
            $convertAccountType     = PaymentAccount::getConvertAccountType($invoice->currency, Currency::BNS);
            $convertAccountBNS      = $this->paymentAccountService->getConvertAccount(Currency::BNS, $convertAccountType);
            $convertAccountCurrency = $this->paymentAccountService->getConvertAccount($invoice->currency, $convertAccountType);
            $this->paymentService->transferMoney($payment, $accountCompanyMain, $convertAccountCurrency, Money::create($bonusAccruedFromPs->getAmount(), $invoice->currency), PaymentDetail::TYPE_PAYMENT, 'Bonus' . $bonusFromDescr);
            $this->paymentService->transferMoney($payment, $convertAccountBNS, $clientUserBonusAccount, $bonusAccruedFromPs, PaymentDetail::TYPE_PAYMENT, 'Bonus from "' . mb_ucfirst($awardCompany->title) . '"' . $bonusFromDescr);
        }

        $this->receivedOrderAdditionalServices($attemp);

        \Yii::info("FINISHED Order process " . $order->id . ". PaymentID: $payment->id\n\n\n", 'order');
    }

    /**
     * Transfer money from additional services of the order
     *
     * @param StoreOrderAttemp $attemp
     *
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws PaymentException
     * @throws PaymentException
     * @throws \yii\base\InvalidConfigException
     */
    protected function receivedOrderAdditionalServices(StoreOrderAttemp $attemp): void
    {
        /** @var ActiveQuery $additionalService */
        $additionalServices = $attemp->order->getPayedAdditionalServices();

        foreach ($additionalServices as $additionalService) {
            $invoice = $additionalService->primaryPaymentInvoice;

            if (!$invoice) {
                throw new PaymentException('Null invoice is impossible');
            }

            /** @var Company $awardCompany */
            $awardCompany = $attemp->ps;

            $totalAmount            = $invoice->getAmountTotalWithRefund();
            $treatstockFee          = $invoice->additionalServiceAmount->getAmountFeeWithRefund();
            $manufactureAwardAmount = $invoice->additionalServiceAmount->getAmountManufacturerWithRefund();
            $clientUser             = $invoice->user;
            $payment                = $invoice->getActivePayment();

            // Not reserved!!! We transfer money directly from autorized to invoice
            $accountReservedType = $this->paymentService->findReservedDetail($payment) ? PaymentAccount::ACCOUNT_TYPE_RESERVED : PaymentAccount::ACCOUNT_TYPE_AUTHORIZE;

            $accountReserved         = $this->paymentAccountService->getUserPaymentAccount($clientUser, $accountReservedType, $invoice->currency);
            $accountInvoice          = $this->paymentAccountService->getUserPaymentAccount($clientUser, PaymentAccount::ACCOUNT_TYPE_INVOICE, $invoice->currency);
            $accountCompanyAward     = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_AWARD, $invoice->currency);
            $accountCompanyMain      = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_MAIN, $invoice->currency);
            $accountManufactureAward = $this->paymentAccountService->getUserPaymentAccount($awardCompany->user, PaymentAccount::ACCOUNT_TYPE_MANUFACTURE_AWARD, $invoice->currency);
            $accountTreatstockFee    = $this->paymentAccountService->getTreatsockFeeAccount($invoice->currency);

            // Transfer money to client:invoice payment account
            if ($totalAmount->getAmount()) {
                $this->paymentService->transferMoney($payment, $accountReserved, $accountInvoice, $totalAmount,
                    PaymentDetail::TYPE_PAYMENT,
                    'Invoice total payment');
            }

            // Payment Method Fee
            $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();

            if ($amountPaymentMethodFee) {
                $this->paymentService->transferMoney($payment, $accountInvoice, $accountTreatstockFee, $amountPaymentMethodFee, PaymentDetail::TYPE_PAYMENT,
                    'Payment method fee'
                );
            }
            // ---
            $manufactureAward = MoneyMath::sum($treatstockFee, $manufactureAwardAmount);
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountManufactureAward, $manufactureAward,
                PaymentDetail::TYPE_PAYMENT,
                'money transfer to manufacture account'
            );
            $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountTreatstockFee, $treatstockFee, PaymentDetail::TYPE_PAYMENT,
                'Payment fee to treatstock'
            );
            $this->paymentService->transferMoney($payment, $accountManufactureAward, $accountCompanyAward, $manufactureAwardAmount, PaymentDetail::TYPE_AWARD_MANUFACTURER,
                'Manufacture award'
            );
            $this->paymentService->transferMoney($payment, $accountCompanyAward, $accountCompanyMain, $manufactureAwardAmount, PaymentDetail::TYPE_PAYMENT,
                'Money for withdrawal from the company account'
            );
        }
    }

    public function transferThingiverseCommission(StoreOrder $order, $payment)
    {
        $thingiverseTotalFee   = Money::usd($order->thingiverseOrder->getTotalThingiverseFee());
        $treatstockMainAccount = $this->paymentAccountService->getTreatstockAccount();
        $thingiverseFeeAccount = $this->paymentAccountService->getThingiverseAccount(PaymentAccount::ACCOUNT_TYPE_FEE);

        $this->paymentService->transferMoney(
            $payment,
            $treatstockMainAccount,
            $thingiverseFeeAccount,
            $thingiverseTotalFee,
            PaymentDetail::TYPE_PAYMENT,
            'Back thingiverse fee'
        );
    }

    /**
     * Try to find payment transaction based on order and status
     *
     * @param StoreOrder $storeOrder
     * @param array $status
     *
     * @return PaymentTransaction
     *
     * @throws PaymentException
     */
    public function findOrderPaymentTransaction(StoreOrder $storeOrder, $status = []): ?PaymentTransaction
    {
        if (empty($status)) {
            $status = [
                PaymentTransaction::STATUS_REQUIRES_CAPTURE,
                PaymentTransaction::STATUS_AUTHORIZED,
                PaymentTransaction::STATUS_AUTHORIZED_EXPIRED,
                PaymentTransaction::STATUS_SUCCEEDED,
                PaymentTransaction::STATUS_SETTLING,
                PaymentTransaction::STATUS_SUBMITTED_FOR_SETTLEMENT,
                PaymentTransaction::STATUS_SETTLEMENT_PENDING,
                PaymentTransaction::STATUS_SETTLED,
                PaymentTransaction::STATUS_REFUNDED,
            ];
        }

        $paymentTransaction = $storeOrder->getPaymentTransactions()->andWhere(['status' => $status, 'type' => PaymentTransaction::TYPE_PAYMENT])->one();
        return $paymentTransaction;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param Money $amount
     *
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws PaymentException
     */
    public function payEasypostForDelivery(StoreOrderAttemp $attemp, Money $amount)
    {
        $order   = $attemp->order;
        $invoice = $order->getPrimaryInvoice();
        if (!$invoice) {
            throw new BusinessException(_t('site.payment', 'No payment found for order {orderId}', ['orderId' => $order->id]));
        }
        $paymentTransaction = $this->findOrderPaymentTransaction($order);
        if (!$paymentTransaction) {
            throw new PaymentException('No transaction found');
        }
        $payment = $paymentTransaction->firstPaymentDetail->paymentDetailOperation->payment;
        if ($attemp->order->isExpressDelivery()) {
            $message = "Payment to EasyPost(express delivery) for the order number #{$order->id}";
        } else {
            $message = "Payment to EasyPost for the order number #{$order->id}";
        }
        $this->paymentService->transferMoney(
            $payment,
            $this->paymentAccountService->getTreatsockEasyPostAccount($amount->getCurrency()),
            $this->paymentAccountService->getEasypostAccount($amount->getCurrency()),
            $amount,
            PaymentDetail::TYPE_PAYMENT,
            $message
        );
    }

    /**
     *
     * order status is new, means no one started to work with this order,
     * user or moderator can cancel this order
     * we will return funds to user
     *
     * @param StoreOrder $storeOrder
     *
     * @param string $comment
     * @return bool|string
     * @throws PaymentException
     * @throws \yii\base\InvalidConfigException
     */
    public function cancelNewOrderPayment(StoreOrder $storeOrder, $comment = '')
    {
        $paymentTransaction = $this->findOrderPaymentTransaction($storeOrder);
        if (!$paymentTransaction || !((float)$paymentTransaction->amount)) {
            // \Yii::info('Payment not found for order #' . $storeOrder->id, 'payment_manager');
            return false;
        }
        return $this->cancelPaymentTransaction($paymentTransaction, $comment);
    }


    /**
     * @param PaymentTransaction $paymentTransaction
     *
     * @param string $comment
     * @return bool
     * @throws BusinessException
     * @throws \yii\base\InvalidConfigException
     */
    public function cancelPaymentTransaction(PaymentTransaction $paymentTransaction, $comment = ''): bool
    {
        $invoiceTransaction = $paymentTransaction->firstPaymentDetail->paymentDetailOperation->payment->paymentInvoice;
        if ($this->paymentService->cancelInvoiceTransaction($invoiceTransaction, $paymentTransaction, $comment)) {
            if ($invoiceTransaction->baseByAdditionalService()) {
                $invoiceTransaction->storeOrderPosition->status = $invoiceTransaction->status;
                $invoiceTransaction->storeOrderPosition->safeSave();
            } elseif ($invoiceTransaction->storeOrder) {
                $invoiceTransaction->storeOrder->order_state = StoreOrder::STATE_CANCELED;
                $invoiceTransaction->storeOrder->safeSave();
                if ($invoiceTransaction->storeOrder->currentAttemp) {
                    $invoiceTransaction->storeOrder->currentAttemp->status = StoreOrderAttemp::STATUS_CANCELED;
                    $invoiceTransaction->storeOrder->currentAttemp->safeSave();
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @return Money
     * @throws InvalidModelException
     */
    public function getProductionInvoicePsProfit(StoreOrderAttemp $attemp)
    {
        $primaryInvoice = $attemp->order->getPrimaryInvoice();

        /** @var Payment $payment */
        $payment = $primaryInvoice->getActivePayment();
        if (!$payment) {
            return Money::zero($primaryInvoice->currency);
        }
        $order      = $primaryInvoice->storeOrder;
        $moneyAward = Money::zero($primaryInvoice->currency);

        // Transfer from invoice account to destinations
        $printAwardAmountPs   = $primaryInvoice->storeOrderAmount->getAmountPrintForPsWithRefund();
        $cuttingAwardAmountPs = $primaryInvoice->storeOrderAmount->getAmountCuttingForPsWithRefund();

        $moneyAward = MoneyMath::sum($moneyAward, $printAwardAmountPs, $cuttingAwardAmountPs);

        $shippingFee = $primaryInvoice->storeOrderAmount->getAmountShippingWithRefund();
        // If shipping by treatstock
        if ($shippingFee) {
            if ($order->isDeliveryByTreatstock()) {
            } else {
                $moneyAward = MoneyMath::sum($moneyAward, $shippingFee);
            }
        } else {
            $deliveryParams = $order->hasDelivery() && $order->currentAttemp->machine ? $order->currentAttemp->machine->asDeliveryParams() : null;
            if ($deliveryParams && $deliveryParams->isFreeDelivery($order)) {
                $paymentToEasyPost = $payment->getPaymentDetails()->toEasyPost()->one();
                if ($paymentToEasyPost) {
                    $shippingFeeForTreatstock = MoneyMath::abs($paymentToEasyPost->getMoneyAmount());
                    $moneyAward               = MoneyMath::minus($moneyAward, $shippingFeeForTreatstock);
                }
            }
        }

        $packageFee = $primaryInvoice->storeOrderAmount->getAmountPackageWithRefund();
        $moneyAward = MoneyMath::sum($moneyAward, $packageFee);

        /** @var ActiveQuery $additionalService */
        $additionalServices = $attemp->order->getPayedAdditionalServices();

        foreach ($additionalServices as $additionalService) {
            $invoice = $additionalService->primaryPaymentInvoice;
            if (!$invoice) {
                continue;
            }
            $manufactureAwardAmount = $invoice->additionalServiceAmount->getAmountManufacturerWithRefund();
            $moneyAward             = MoneyMath::sum($moneyAward, $manufactureAwardAmount);
        }
        $manufacturePreorderAmount = $primaryInvoice->preorderAmount->getAmountManufacturerWithRefund();
        $moneyAward                = MoneyMath::sum($moneyAward, $manufacturePreorderAmount);

        return $moneyAward;
    }
}