<?php
/**
 * User: DeFacto
 * Date: 09.11.2017
 * Time: 17:27
 */

namespace common\modules\payment\services;

use common\modules\payment\gateways\PaymentGateway;
use lib\money\Money;

class PaymentCheckoutService
{
    /**
     * @var PaymentGateway
     */
    public $paymentGateway;

    public function pay(Money $create)
    {
        return true;
    }

    public function setGateway(PaymentGateway $gateway)
    {
        $this->paymentGateway = $gateway;
    }

    public function setPaymentTransaction($paymentTransaction)
    {

    }
}