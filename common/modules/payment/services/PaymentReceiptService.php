<?php

namespace common\modules\payment\services;

use common\components\Emailer;
use common\models\factories\FileFactory;
use common\models\PaymentInvoice;
use common\models\PaymentReceipt;
use common\models\PaymentReceiptInvoiceComment;
use common\models\repositories\FileRepository;
use common\models\StoreOrder;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\modules\payment\widgets\ReceiptViewWidget;
use common\repositories\BaseActiveRecordRepository;
use frontend\models\user\UserFacade;
use lib\pdf\HtmlToPdf;
use Yii;
use yii\base\BaseObject;
use yii\base\ErrorException;
use yii\base\InvalidArgumentException;
use yii\base\UserException;
use yii\web\Response;

/**
 * Class PaymentReceiptService
 *
 * @package common\modules\payment\services
 */
class PaymentReceiptService extends BaseObject
{
    /**
     * @var UserIdentityProvider
     */
    public $userIdentityProvider;

    public function injectDependencies(UserIdentityProvider $userIdentityProvider)
    {
        $this->userIdentityProvider = $userIdentityProvider;
    }

    /**
     * @param StoreOrder $storeOrder
     *
     * @return PaymentReceipt
     */
    public function getReceiptByStoreOrder(StoreOrder $storeOrder): PaymentReceipt
    {
        $receipt = PaymentReceipt::findOne(['order_id' => $storeOrder->id]);

        if (!$receipt) {
            $receipt = PaymentReceipt::addRecord([
                'user_id'      => $storeOrder->user_id,
                'order_id'     => $storeOrder->id,
                'receipt_date' => $storeOrder->billed_at
            ]);
        }

        return $receipt;
    }

    public function getReceiptByInvoiceForCurrentUser(PaymentInvoice $paymentInvoice): PaymentReceipt
    {
        $currentUser = $this->userIdentityProvider->getUser();
        return $this->getReceiptByInvoiceForUser($paymentInvoice, $currentUser);
    }

    public function getReceiptByInvoiceForUser(PaymentInvoice $paymentInvoice, User $user): PaymentReceipt
    {
        $receipt = $paymentInvoice->getPaymentReceipts()->forUser($user)->one();
        if ($receipt) {
            return $receipt;
        }
        $receipt = PaymentReceipt::addRecord([
            'user_id'      => $user->id,
            'invoice_uuid' => $paymentInvoice->uuid,
            'receipt_date' => $paymentInvoice->start_pay_progress_date
        ]);

        return $receipt;
    }


    /**
     * @param $receipt
     *
     * @return string Filepath
     * @throws ErrorException
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function getPdfReceipt(PaymentReceipt $receipt): string
    {
        if (!$receipt->htmlFile) {
            /** @var FileFactory $fileFactory */
            $fileFactory    = Yii::createObject(FileFactory::class);
            $fileRepository = Yii::createObject(FileRepository::class);
            $file           = $fileFactory->createFileFromContent('receipt-' . $receipt->id . '.txt', ' ');
            $fileRepository->save($file);
            $receipt->html_file_id = $file->id;
            $receipt->safeSave();
        }
        $result          = ReceiptViewWidget::widget(['paidInvoices' => $receipt->getBindInvoices()]);
        $fileContentPath = $receipt->htmlFile->getLocalTmpFilePath();
        $receipt->htmlFile->setFileContentAndSave($result);
        copy($fileContentPath, $fileContentPath . '.html');
        $filePdf = HtmlToPdf::convert($fileContentPath . '.html');
        unlink($fileContentPath . '.html');
        return $filePdf;
    }

    public function mailPdfReceipt(PaymentReceipt $receipt): bool
    {
        $emailer = new Emailer();
        $filePdf = $this->getPdfReceipt($receipt);
        if ($filePdf) {
            if ($receipt->order) {
                $subject = _t('payment.receipt', 'Payment Receipt. Order #' . $receipt->order_id);
                $content = _t('payment.receipt', "Hello, You've received a payment receipt. Please see the attached file for payment receipt. Order #" . $receipt->order_id);
            }
            if ($receipt->invoice) {
                $subject = _t('payment.receipt', 'Payment Receipt. Invoice #' . $receipt->invoice->uuid);
                $content = _t('payment.receipt', "Hello, You've received a payment receipt. Please see the attached file for payment receipt. Invoice #" . $receipt->invoice->uuid);
            }
            $user = $receipt->user;
            $emailer->attachFile($receipt->getCombinedId() . '.pdf', $filePdf)
                ->sendMessage($user->email, $subject, $content);
            return true;
        }
        return false;
    }

    public function saveBuyerDetails(PaymentReceipt $receipt, PaymentInvoice $invoice, $buyerDetails): void
    {
        if (!$this->inInvoice($invoice, $receipt->getBindInvoices())) {
            throw new InvalidArgumentException('Invoice not contain receipt');
        }
        $receiptInvoiceComment = $invoice->paymentReceiptInvoiceComment;

        if ($receiptInvoiceComment) {
            $receiptInvoiceComment->additional_buyer_details = $buyerDetails;
            $receiptInvoiceComment->safeSave();
            return;
        }

        PaymentReceiptInvoiceComment::addRecord([
            'receipt_id'               => $receipt->id,
            'payment_invoice_uuid'     => $invoice->uuid,
            'additional_buyer_details' => $buyerDetails
        ]);
    }

    /**
     *
     * @param PaymentReceipt $receipt
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\db\StaleObjectException
     */
    public function deleteHtmlFile(PaymentReceipt $receipt)
    {
        if ($receipt->htmlFile) {
            $htmlFile              = $receipt->htmlFile;
            $receipt->html_file_id = null;
            $receipt->safeSave();
            $fileRepository = \Yii::createObject(FileRepository::class);
            $fileRepository->delete($htmlFile);
        }
    }


    /**
     * @param PaymentReceipt $receipt
     * @param PaymentInvoice $invoice
     * @param $comment
     *
     * @return bool|PaymentReceiptInvoiceComment
     * @throws \yii\base\UserException
     */
    public function saveComment(PaymentReceipt $receipt, PaymentInvoice $invoice, $comment)
    {
        if (!$this->inInvoice($invoice, $receipt->getBindInvoices())) {
            throw new InvalidArgumentException('Invoice not contain receipt');
        }

        $receiptInvoiceComment = $invoice->paymentReceiptInvoiceComment;

        if ($receiptInvoiceComment) {
            $receiptInvoiceComment->comment = $comment;
            $receiptInvoiceComment->safeSave(['comment']);
            return $receiptInvoiceComment;
        }

        return PaymentReceiptInvoiceComment::addRecord([
            'receipt_id'           => $receipt->id,
            'payment_invoice_uuid' => $invoice->uuid,
            'comment'              => $comment
        ]);
    }

    public function checkAccessForCurrentUser(PaymentInvoice $paymentInvoice): bool
    {
        if (UserFacade::isObjectOwner($paymentInvoice)) {
            return true;
        }
        $currentUser = UserFacade::getCurrentUser();
        if ($paymentInvoice->instantPayment->to_user_id === $currentUser->id) {
            return true;
        }
        return false;
    }

    public function checkAccessForCurrentUserOrFail(PaymentInvoice $paymentInvoice): void
    {
        if (!$this->checkAccessForCurrentUser($paymentInvoice)) {
            throw new UserException('No Access');
        }
    }

    /**
     * @param PaymentInvoice $invoice
     * @param PaymentInvoice[] $orderInvoices
     *
     * @return bool
     */
    protected function inInvoice($invoice, $orderInvoices): bool
    {
        foreach ($orderInvoices as $orderInvoice) {
            if ($orderInvoice->uuid === $invoice->uuid) {
                return true;
            }
        }

        return false;
    }
}