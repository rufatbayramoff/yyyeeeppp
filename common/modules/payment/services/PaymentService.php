<?php
/**
 * User: nabi
 */

namespace common\modules\payment\services;

use common\components\DateHelper;
use common\components\PaymentExchangeRateConverter;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\models\StoreOrder;
use common\models\StoreOrderPosition;
use common\models\TsInternalPurchase;
use common\models\User;
use common\modules\instantPayment\services\InstantPaymentService;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\exception\FatalPaymentException;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\factories\PaymentOperationFactory;
use common\modules\payment\models\BalanceEx;
use common\modules\payment\models\UserBalances;
use common\modules\tsInternalPurchase\services\TsInternalPurchaseService;
use common\services\StoreOrderService;
use frontend\models\user\UserFacade;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use Yii;
use yii\base\BaseObject;
use yii\db\Query;

/**
 * Class PaymentService
 *
 * @package common\modules\payment\services
 */
class PaymentService extends BaseObject
{

    /** @var PaymentOperationFactory */
    protected $paymentOperationFactory;

    /** @var PaymentExchangeRateConverter */
    protected $paymentExchangeRateConverter;

    /** @var PaymentReceiptService */
    protected $receiptService;

    /**
     * @param PaymentOperationFactory $paymentOperationFactory
     * @param PaymentExchangeRateConverter $paymentExchangeRateConverter
     * @param StoreOrderService $storeOrderService
     * @param PaymentReceiptService $receiptService
     */
    public function injectDependencies(
        PaymentOperationFactory      $paymentOperationFactory,
        PaymentExchangeRateConverter $paymentExchangeRateConverter,
        PaymentReceiptService        $receiptService
    )
    {
        $this->paymentOperationFactory      = $paymentOperationFactory;
        $this->paymentExchangeRateConverter = $paymentExchangeRateConverter;
        $this->receiptService               = $receiptService;
    }

    public function calculateUserBalance(User $user)
    {
        $balance                = new UserBalances();
        $balance->user          = $user;
        $balance->main          = $this->getUserMainAmount();
        $balance->authorize     = $this->calculateAccountBalance($this->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_AUTHORIZE));
        $balance->reserved      = $this->calculateAccountBalance($this->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_RESERVED));
        $balance->tax           = $this->calculateAccountBalance($this->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_TAX));
        $balance->refundRequest = $this->calculateAccountBalance($this->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_REFUND_REQUEST));
        return $balance;
    }

    public function userHasPayments(User $user)
    {
        $paymentDetailsCount = PaymentDetail::find()
            ->toReserved()
            ->andWhere(['payment_account.user_id' => $user->id])
            ->count();
        return $paymentDetailsCount > 0;
    }

    public function fixSummAsMoney($rows)
    {
        $returnValue = [];
        foreach ($rows as $row) {
            $returnValue[$row['type']] = Money::create($row['sum'], Currency::USD);
        }
        return $returnValue;
    }

    /**
     * Calculate payment detail balance result
     *
     * @param PaymentDetail $paymentDetail
     * @return Money
     * @throws \yii\db\Exception
     */
    public function calculatePaymentDetailBalanceResult(PaymentDetail $paymentDetail): Money
    {
        $query         = 'SELECT sum(payment_detail.amount) as sum FROM payment_detail LEFT JOIN payment_detail_operation on payment_detail.payment_detail_operation_uuid = payment_detail_operation.uuid WHERE ((payment_detail.updated_at<\'' . $paymentDetail->updated_at . '\') or (payment_detail.updated_at=\'' . $paymentDetail->updated_at . '\'  and payment_detail.id<=\'' . $paymentDetail->id . '\')) and payment_detail.payment_account_id=\'' . $paymentDetail->payment_account_id . '\'';
        $resultBalance = \Yii::$app->db->createCommand($query)->queryScalar();
        return Money::create($resultBalance, $paymentDetail->original_currency);
    }

    /**
     * Select client users
     *
     * @param $periodBegin
     * @param $periodEnd
     *
     * @return BalanceEx
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function calculateTotalUsersBalanceEx($periodBegin, $periodEnd): BalanceEx
    {
        $periodBegin = DateHelper::filterDate($periodBegin);
        if ($periodEnd) {
            $periodEnd = DateHelper::filterDate($periodEnd);
        } else {
            // Date in feature
            $periodEnd = '9999-01-01';
        }

        $balanceEx = \Yii::createObject(BalanceEx::class);

        $query                   = "SELECT payment_account.type as type, sum(payment_detail.amount) as sum FROM payment_detail LEFT JOIN payment_account on payment_account.id=payment_detail.payment_account_id LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_account.user_id>=1000 AND payment_detail_operation.created_at<'" . $periodBegin . "' GROUP by payment_account.type";
        $balanceEx->balanceStart = $this->fixSummAsMoney(\Yii::$app->db->createCommand($query)->queryAll());

        $query                     = "SELECT payment_account.type as type, sum(payment_detail.amount) as sum FROM payment_detail LEFT JOIN payment_account on payment_account.id=payment_detail.payment_account_id LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_account.user_id>=1000 AND payment_detail_operation.created_at<='" . $periodEnd . "' GROUP by payment_account.type";
        $balanceEx->balanceCurrent = $this->fixSummAsMoney(\Yii::$app->db->createCommand($query)->queryAll());

        $query                  = "SELECT payment_account.type as type, sum(payment_detail.amount) as sum FROM payment_detail LEFT JOIN payment_account on payment_account.id=payment_detail.payment_account_id LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_account.user_id>=1000 AND payment_detail_operation.created_at>='" . $periodBegin . "' AND payment_detail.amount>0 AND payment_detail_operation.created_at<='" . $periodEnd . "' GROUP by payment_account.type";
        $balanceEx->balancePlus = $this->fixSummAsMoney(\Yii::$app->db->createCommand($query)->queryAll());

        $query                   = "SELECT payment_account.type as type, sum(payment_detail.amount) as sum FROM payment_detail LEFT JOIN payment_account on payment_account.id=payment_detail.payment_account_id LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_account.user_id>=1000 AND payment_detail_operation.created_at>='" . $periodBegin . "' AND payment_detail.amount<0 AND payment_detail_operation.created_at<='" . $periodEnd . "' GROUP by payment_account.type";
        $balanceEx->balanceMinus = $this->fixSummAsMoney(\Yii::$app->db->createCommand($query)->queryAll());

        return $balanceEx;
    }

    public function calculateTotalUserBalanceEx(User $user, $periodBegin, $periodEnd): BalanceEx
    {
        $periodBegin = DateHelper::filterDate($periodBegin);
        if ($periodEnd) {
            $periodEnd = DateHelper::filterDate($periodEnd);
        } else {
            // Date in feature
            $periodEnd = '9999-01-01';
        }

        $balanceEx = \Yii::createObject(BalanceEx::class);

        $query                   = "SELECT payment_account.type as type, sum(payment_detail.amount) as sum FROM payment_detail LEFT JOIN payment_account on payment_account.id=payment_detail.payment_account_id LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_account.user_id=" . $user->id . " AND payment_detail_operation.created_at<'" . $periodBegin . "' GROUP by payment_account.type";
        $balanceEx->balanceStart = $this->fixSummAsMoney(\Yii::$app->db->createCommand($query)->queryAll());

        $query                     = "SELECT payment_account.type as type, sum(payment_detail.amount) as sum FROM payment_detail LEFT JOIN payment_account on payment_account.id=payment_detail.payment_account_id LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_account.user_id=" . $user->id . " AND payment_detail_operation.created_at<='" . $periodEnd . "' GROUP by payment_account.type";
        $balanceEx->balanceCurrent = $this->fixSummAsMoney(\Yii::$app->db->createCommand($query)->queryAll());

        $query                  = "SELECT payment_account.type as type, sum(payment_detail.amount) as sum FROM payment_detail LEFT JOIN payment_account on payment_account.id=payment_detail.payment_account_id LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_account.user_id=" . $user->id . " AND payment_detail_operation.created_at>='" . $periodBegin . "' AND payment_detail.amount>0 AND payment_detail_operation.created_at<='" . $periodEnd . "' GROUP by payment_account.type";
        $balanceEx->balancePlus = $this->fixSummAsMoney(\Yii::$app->db->createCommand($query)->queryAll());

        $query                   = "SELECT payment_account.type as type, sum(payment_detail.amount) as sum FROM payment_detail LEFT JOIN payment_account on payment_account.id=payment_detail.payment_account_id LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_account.user_id=" . $user->id . " AND payment_detail_operation.created_at>='" . $periodBegin . "' AND payment_detail.amount<0 AND payment_detail_operation.created_at<='" . $periodEnd . "' GROUP by payment_account.type";
        $balanceEx->balanceMinus = $this->fixSummAsMoney(\Yii::$app->db->createCommand($query)->queryAll());

        return $balanceEx;
    }

    public function getUserPaymentDetailOperations($user, $accountType, $periodBegin, $periodEnd, $sign)
    {
        $periodBegin                  = $periodBegin ? DateHelper::filterDate($periodBegin) : '';
        $periodEnd                    = $periodEnd ? DateHelper::filterDate($periodEnd) : '';
        $paymentDetailOperationsQuery = PaymentDetailOperation::find()
            ->leftJoin('payment_detail payment_detail_condit', 'payment_detail_condit.payment_detail_operation_uuid=payment_detail_operation.uuid')
            ->leftJoin('payment_account', 'payment_account.id=payment_detail_condit.payment_account_id');
        if ($user) {
            $paymentDetailOperationsQuery->andWhere(['payment_account.user_id' => $user['id']]);
        }
        if ($sign === '+') {
            $paymentDetailOperationsQuery->andWhere('payment_detail_condit.amount>0');
        }
        if ($sign === '-') {
            $paymentDetailOperationsQuery->andWhere('payment_detail_condit.amount<0');
        }
        if ($periodBegin) {
            $paymentDetailOperationsQuery->andWhere("DATE(payment_detail_operation.created_at)>='" . $periodBegin . "'");
        }
        if ($periodEnd) {
            $paymentDetailOperationsQuery->andWhere("DATE(payment_detail_operation.created_at)<='" . $periodEnd . "'");
        }
        if ($accountType) {
            $paymentDetailOperationsQuery->andWhere(['payment_account.type' => $accountType]);
        }

        $paymentDetailOperationsQuery->orderBy('payment_detail_operation.payment_id, payment_detail_operation.created_at');
        $paymentDetailOperationsQuery->limit(2000);
        return $paymentDetailOperationsQuery->all();
    }

    /**
     * @param PaymentAccount|null $paymentAccount
     *
     * @return PaymentDetailOperation[]
     */
    public function getAccountPaymentDetailOperations($paymentAccount, $periodBegin, $periodEnd, $sign)
    {
        $periodBegin                  = $periodBegin ? DateHelper::filterDate($periodBegin) : '';
        $periodEnd                    = $periodEnd ? DateHelper::filterDate($periodEnd) : '';
        $paymentDetailOperationsQuery = PaymentDetailOperation::find()
            ->leftJoin('payment_detail payment_detail_condit', 'payment_detail_condit.payment_detail_operation_uuid=payment_detail_operation.uuid');
        if ($paymentAccount) {
            $paymentDetailOperationsQuery->andWhere(['payment_detail.payment_account_id' => $paymentAccount->id]);
        }
        if ($sign === '+') {
            $paymentDetailOperationsQuery->andWhere('payment_detail_condit.amount>0');
        }
        if ($sign === '-') {
            $paymentDetailOperationsQuery->andWhere('payment_detail_condit.amount<0');
        }
        if ($periodBegin) {
            $paymentDetailOperationsQuery->andWhere("DATE(payment_detail_operation.created_at)>='" . $periodBegin . "'");
        }
        if ($periodEnd) {
            $paymentDetailOperationsQuery->andWhere("DATE(payment_detail_operation.created_at)<='" . $periodEnd . "'");
        }
        $paymentDetailOperationsQuery->orderBy('payment_detail_operation.payment_id, payment_detail_operation.created_at');
        $paymentDetailOperationsQuery->limit(1000);
        return $paymentDetailOperationsQuery->all();
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getUsedPaymentTypesByUser(User $user): array
    {
        return (new Query())
            ->select(['pa.type as type'])
            ->from([
                'pd' => PaymentDetail::tableName()
            ])
            ->innerJoin(['pa' => PaymentAccount::tableName()], 'pa.id = pd.payment_account_id')
            ->where([
                'pa.user_id' => $user->id
            ])
            ->groupBy('pa.type')
            ->column();
    }

    /**
     * @return array
     */
    public function getUsedPaymentTypesAll(): array
    {
        return (new Query())
            ->select(['pa.type as type'])
            ->from([
                'pd' => PaymentDetail::tableName()
            ])
            ->innerJoin(['pa' => PaymentAccount::tableName()], 'pa.id = pd.payment_account_id')
            ->groupBy('pa.type')
            ->column();
    }

    /**
     * Transfer money from one account to other
     *
     * @param Payment $payment
     * @param PaymentAccount $paymentFrom
     * @param PaymentAccount $paymentTo
     * @param Money $amount
     * @param string $paymentDetailType
     * @param string $paymentDetailDescription
     *
     * @return array
     * @throws PaymentException
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\console\Exception
     */
    public function transferMoney(
        Payment        $payment,
        PaymentAccount $paymentFrom,
        PaymentAccount $paymentTo,
        Money          $amount,
                       $paymentDetailType = PaymentDetail::TYPE_PAYMENT,
                       $paymentDetailDescription = ''
    )
    {
        $paymentOperation                                 = $this->paymentOperationFactory->createByPayment($payment);
        $paymentDetailFrom                                = new PaymentDetail();
        $paymentDetailFrom->payment_detail_operation_uuid = $paymentOperation->uuid;
        $paymentDetailFrom->payment_account_id            = $paymentFrom->id;
        $paymentDetailFrom->updated_at                    = DateHelper::now();
        $paymentDetailFrom->amount                        = -$amount->getAmount();
        $paymentDetailFrom->original_currency             = $amount->getCurrency();
        $paymentDetailFrom->rate_id                       = $this->paymentExchangeRateConverter->getLastPaymentExchangeRate()->id;
        $paymentDetailFrom->description                   = $paymentDetailDescription;
        $paymentDetailFrom->type                          = $paymentDetailType;

        $paymentDetailTo                                = new PaymentDetail();
        $paymentDetailTo->payment_detail_operation_uuid = $paymentDetailFrom->payment_detail_operation_uuid;
        $paymentDetailTo->payment_account_id            = $paymentTo->id;
        $paymentDetailTo->updated_at                    = $paymentDetailFrom->updated_at;
        $paymentDetailTo->amount                        = $amount->getAmount();
        $paymentDetailTo->original_currency             = $amount->getCurrency();
        $paymentDetailTo->rate_id                       = $paymentDetailFrom->rate_id;
        $paymentDetailTo->description                   = $paymentDetailDescription;
        $paymentDetailTo->type                          = $paymentDetailFrom->type;

        if (!$payment->validate()) {
            throw new PaymentException('Invalid payment:' . json_encode($payment->getErrors()));
        }
        if (!$paymentOperation->validate()) {
            throw new PaymentException('Invalid payment operation:' . json_encode($paymentOperation->getErrors()));
        }
        if (!$paymentDetailFrom->validate()) {
            throw new PaymentException('I' . $paymentDetailFrom->rate_id . 'Invalid payment detail from:' . json_encode($paymentDetailFrom->getErrors()));
        }
        if (!$paymentDetailTo->validate()) {
            throw new PaymentException('Invalid payment detail to:' . json_encode($paymentDetailTo->getErrors()));
        }

        $paymentOperation->safeSave();
        $paymentDetailFrom->safeSave();
        $paymentDetailTo->safeSave();
        return [$paymentDetailFrom, $paymentDetailTo];
    }

    /**
     * @param PaymentDetailOperation $paymentOperation
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function submitForSettlePaymentOperation(PaymentDetailOperation $paymentOperation)
    {
        $paymentTransaction = $paymentOperation->paymentTransaction;
        $checkoutObj        = new PaymentCheckout($paymentTransaction->vendor);
        $checkoutObj->submitForSettle($paymentOperation);
    }

    /**
     * Set invoice payment status: settle
     * We can get money from payment system
     *
     * @param PaymentInvoice $paymentInvoice
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function submitForSettleInvoice(PaymentInvoice $paymentInvoice)
    {
        $payments = $paymentInvoice->payments;
        foreach ($payments as $payment) {
            foreach ($payment->paymentDetailOperations as $paymentDetailOperation) {
                if ($paymentTransaction = $paymentDetailOperation->paymentTransaction) {
                    if (in_array($paymentTransaction->status, [PaymentTransaction::STATUS_AUTHORIZED, PaymentTransaction::STATUS_REQUIRES_CAPTURE])) {
                        $this->submitForSettlePaymentOperation($paymentDetailOperation);
                    }
                }
            }
        }
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @param PaymentTransaction $paymentTransaction
     * @param string $comment
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function cancelInvoiceTransaction(PaymentInvoice $paymentInvoice, PaymentTransaction $paymentTransaction, $comment = '')
    {
        /**
         * @var $paymentTransaction PaymentTransaction
         */
        $payment = $paymentInvoice->getActivePayment();
        if (!$payment) {
            \Yii::warning('payment not found for order', 'payment_manager');
            return false;
        }

        \Yii::info('cancelInvoicePayment #' . $paymentInvoice->uuid, 'payment_manager');
        $paymentCheckout = new PaymentCheckout($paymentTransaction->vendor);
        return $paymentCheckout->cancel($paymentTransaction->firstPaymentDetail->paymentDetailOperation, $comment);
    }

    /**
     * Used for payment transaction update status, Technical method.
     *
     * @param PaymentTransaction $paymentTransaction
     * @param string $newStatus
     *
     * @return PaymentTransactionHistory|null
     * @throws \yii\base\UserException
     */
    public function updateTransactionStatus(PaymentTransaction $paymentTransaction, $newStatus): ?PaymentTransactionHistory
    {
        if ($paymentTransaction->status !== $newStatus) {
            $paymentTransactionHistory                    = new PaymentTransactionHistory();
            $paymentTransactionHistory->transaction_id    = $paymentTransaction->id;
            $paymentTransactionHistory->payment_detail_id = null;
            $paymentTransactionHistory->created_at        = DateHelper::now();
            $paymentTransactionHistory->action_id         = PaymentTransactionHistory::ACTION_UPDATE;
            $paymentTransactionHistory->comment           = ['status' => ['from' => $paymentTransaction->status, 'to' => $newStatus]];
            $paymentTransactionHistory->user_id           = UserFacade::getCurrentUserId() ?? 1;
            $paymentTransactionHistory->safeSave();
            $paymentTransaction->status = $newStatus;
            $paymentTransaction->safeSave();
            return $paymentTransactionHistory;
        }
        return null;
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     *
     * @return Payment
     */
    public function findActivePayment(PaymentInvoice $paymentInvoice)
    {
        return $paymentInvoice->getPayments()->inPayProcess()->one();
    }

    public function findInvoiceReservedDetail(PaymentInvoice $paymentInvoice): ?PaymentDetail
    {
        foreach ($paymentInvoice->payments as $payment) {
            return $payment->getPaymentDetailOperations()->inReservedStatus()->one();
        }
        return null;
    }

    public function findReservedDetail(Payment $payment): ?PaymentDetailOperation
    {
        return $payment->getPaymentDetailOperations()->inReservedStatus()->one();
    }

    /**
     * @param PaymentInvoice $invoice
     * @param PaymentAccount $fromAccount
     * @param PaymentAccount $toAccount
     * @param string $type
     */
    public function findPaymentDetail(PaymentInvoice $invoice, PaymentAccount $fromAccount, PaymentAccount $toAccount, $type): ?PaymentDetail
    {
        foreach ($invoice->payments as $payment) {
            foreach ($payment->paymentDetailOperations as $paymentDetailOperation) {
                if (count($paymentDetailOperation->paymentDetails) !== 2) {
                    continue;
                }
                try {
                    $from = $paymentDetailOperation->fromPaymentDetail();
                    $to   = $paymentDetailOperation->toPaymentDetail();
                } catch (FatalPaymentException $paymentException) {
                    continue;
                }
                if ($from && $to && ($from->payment_account_id === $fromAccount->id) &&
                    ($to->payment_account_id === $toAccount->id) &&
                    ($from->type === $type)
                ) {
                    return $to;
                }
            }
        }
        return null;
    }

    /**
     * Set invoice as paid
     *
     * @param PaymentInvoice $paymentInvoice
     * @param $paymentDetailOperation
     *
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function setPayedInvoice(PaymentInvoice $paymentInvoice, PaymentDetailOperation $paymentDetailOperation, $invoiceStatus): void
    {
        /** @var StoreOrderService $storeOrderService */
        $storeOrderService = Yii::createObject(StoreOrderService::class);

        /** @var StoreOrderPositionService $orderPositionService */
        $orderPositionService   = Yii::createObject(StoreOrderPositionService::class);
        $paymentInvoice->status = $invoiceStatus;
        $paymentInvoice->safeSave(['status']);

        if ($paymentInvoice->storeOrder) {
            if (!$paymentInvoice->storeOrder->billed_at) {
                $storeOrderService->setPayed($paymentInvoice->storeOrder, $paymentDetailOperation);
            }
        }

        if ($paymentInvoice->orderPosition && $paymentInvoice->orderPosition->status === StoreOrderPosition::STATUS_NEW) {
            $orderPositionService->acceptOrderPosition($paymentInvoice->orderPosition);
        }

        if ($paymentInvoice->instantPayment && $paymentInvoice->instantPayment->isNotPayed()) {
            /** @var InstantPaymentService $instantPaymentService */
            $instantPaymentService = Yii::createObject(InstantPaymentService::class);
            $instantPaymentService->setPayed($paymentInvoice);
        }

        if ($paymentInvoice->tsInternalPurchase && $paymentInvoice->tsInternalPurchase->isNotPayed()) {
            /** @var InstantPaymentService $instantPaymentService */
            $tsInternalPurchaseService = Yii::createObject(TsInternalPurchaseService::class);
            $tsInternalPurchaseService->setPayed($paymentInvoice);
        }

        if ($paymentInvoice->invoice_group_uuid) {
            $this->unpaidCanceledNewInvoiceGroupInvoices($paymentInvoice->invoice_group_uuid);
        }

        if ($paymentInvoice->user && !$paymentInvoice->user->isAnonimUser() && !$paymentInvoice->user->getIsSystem() && !$paymentInvoice->storeOrder) {
            $receipt = $this->receiptService->getReceiptByInvoiceForUser($paymentInvoice, $paymentInvoice->user);
            $this->receiptService->mailPdfReceipt($receipt);
        }
    }

    /**
     * @param $invoiceGroupUuid
     */
    protected function unpaidCanceledNewInvoiceGroupInvoices($invoiceGroupUuid): void
    {
        PaymentInvoice::updateAll(['status' => PaymentInvoice::STATUS_CANCELED], [
            'and',
            ['=', 'invoice_group_uuid', $invoiceGroupUuid],
            ['=', 'status', PaymentInvoice::STATUS_NEW],
        ]);
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     *
     * @throws \yii\base\UserException
     */
    public function setCanceledInvoice(PaymentInvoice $paymentInvoice): void
    {
        $paymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
        $paymentInvoice->safeSave(['status']);
    }

    /**
     * @param $price
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public static function isCanBePayedByBraintree(PaymentInvoice $paymentInvoice, $defaultPayCardVendor = '')
    {
        if ($defaultPayCardVendor !== PaymentTransaction::VENDOR_BRAINTREE && $paymentInvoice->currency !== Currency::USD) {
            return false;
        }
        $price = $paymentInvoice->getAmountTotal();

        if ($price->getAmount() > 0 && $price->getAmount() < self::getMaxBraintreePrice()) {
            return true;
        }
        return false;
    }

    public static function isCanBePayedByBonus(PaymentInvoice $paymentInvoice)
    {
        $paymentAccountService = \Yii::createObject(PaymentAccountService::class);
        $amountNetIncome       = $paymentAccountService->getUserMainAmount($paymentInvoice->user, Currency::BNS);

        if ($paymentInvoice->getAmountBonus() && $paymentInvoice->getAmountBonus()->getAmount() < $amountNetIncome->getAmount()) {
            return true;
        }
        return false;
    }

    public static function isCanBePayedByTs(PaymentInvoice $paymentInvoice)
    {
        $paymentAccountService = \Yii::createObject(PaymentAccountService::class);
        $amountNetIncome       = $paymentAccountService->getUserMainAmount($paymentInvoice->user, $paymentInvoice->currency);

        if ($paymentInvoice->getAmountTotal()->getAmount() < $amountNetIncome->getAmount()) {
            return true;
        }
        return false;
    }

    public static function isCanBePayedByStripe(PaymentInvoice $paymentInvoice, $defaultPayCardVendor)
    {
        $price = $paymentInvoice->getAmountTotal()->getAmount();
        if ($price > 0 && $price < self::getMaxBraintreePrice()) {
            return true;
        }
        return false;
    }

    public static function isCanBePayedByAlipay(PaymentInvoice $paymentInvoice, $defaultPayCardVendor)
    {
        if ($paymentInvoice->currency !== Currency::USD) {
            return false;
        }
        $price = $paymentInvoice->getAmountTotal()->getAmount();
        if ($price > 0 && $price < self::getMaxBraintreePrice()) {
            return true;
        }
        return false;
    }

    public function getAccountingsList(PaymentInvoice $paymentInvoice)
    {
        $list    = [];
        $amounts = [];
        if (array_key_exists(PaymentInvoice::ACCOUNTING_TYPE_ITEMS, $paymentInvoice->accounting)) {
            foreach ($paymentInvoice->accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS] as $itemKey => $item) {
                foreach ($item as $key => $accounting) {
                    if (!is_array($accounting) || !array_key_exists('price', $accounting)) {
                        continue;
                    }
                    if ($accounting['price'] == 0) {
                        continue;
                    }
                    $money = $paymentInvoice->getItemAmountByTypeWithRefund($itemKey, $accounting['type']);
                    $money = $money ? round($money->getAmount(), 2) : 0;
                    if ($money <= 0.001) {
                        continue;
                    }
                    $listKey           = $itemKey . '-' . $key;
                    $list[$listKey]    = 'Accounting item: ' . $itemKey . ' ' . $accounting['type'];
                    $amounts[$listKey] = [
                        'price'    => $money,
                        'currency' => $paymentInvoice->currency
                    ];

                }
            }
        } else {
            foreach ($paymentInvoice->accounting as $key => $accounting) {
                if (($accounting['price'] == 0) || (in_array($accounting['type'], [
                        PaymentInvoice::ACCOUNTING_TYPE_BONUS,
                        PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS,
                        PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS,
                    ]))) {
                    continue;
                }

                if (in_array($accounting['type'], [PaymentInvoice::ACCOUNTING_TYPE_PRINT, PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE, PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2])) {
                    $amountPrintForPsWithRefund = $paymentInvoice->storeOrderAmount->getAmountPrintForPsWithRefund();
                    $money                      = $amountPrintForPsWithRefund ? round($amountPrintForPsWithRefund->getAmount(), 2) : 0;
                } elseif ($accounting['type'] === PaymentInvoice::ACCOUNTING_TYPE_PS_FEE) {
                    $amountPrintForPsWithRefund = $paymentInvoice->storeOrderAmount->getAmountPsFeeWithRefund();
                    $money                      = $amountPrintForPsWithRefund ? round($amountPrintForPsWithRefund->getAmount(), 2) : 0;
                } else {
                    $money = $paymentInvoice->getAmountByTypeWithRefund($accounting['type']) ?
                        round($paymentInvoice->getAmountByTypeWithRefund($accounting['type'])->getAmount(), 2) : 0;
                }

                if ($money <= 0.001) {
                    continue;
                }

                $list[$key] = 'Accounting item: ' . $accounting['type'];

                $amounts[$key] = [
                    'price'    => $money,
                    'currency' => $accounting['currency']
                ];
            }
        }
        return [$list, $amounts];
    }

    /**
     * check if order can be paid by invoice
     *
     * @param StoreOrder $storeOrder
     *
     * @return bool
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public static function canPayByBankInvoice(StoreOrder $storeOrder)
    {
        if ($storeOrder->getPrimaryInvoice() && $storeOrder->getPrimaryInvoice()->getAmountTotalWithRefund()->getAmount() >= InvoiceBankService::getInvoiceFromUsd()) {
            return true;
        }
        return false;
    }

    /**
     * @param PaymentInvoice $invoice
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public static function canPayInvoiceByBankInvoice(PaymentInvoice $invoice)
    {
        return $invoice->getAmountTotalWithRefund()->getAmount() >= InvoiceBankService::getInvoiceFromUsd();
    }

    public function isMoneyOnReservedAccount(PaymentTransaction $paymentTransaction)
    {
        $paymentDetail = $paymentTransaction->firstPaymentDetail;
        $payment       = $paymentDetail->paymentDetailOperation->payment;
        $toInvoice     = $payment->getPaymentDetails()->toInvoice()->one();
        if ($toInvoice) {
            return false;
        }
        return true;
    }


    /**
     * @return int
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    private static function getMaxBraintreePrice()
    {
        return (int)app('setting')->get('store.braintree_to', 1300);
    }

    /**
     * Does the current user have access to the invoice?
     *
     * @param PaymentInvoice $invoice
     *
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function checkAccessInvoiceForCurrentUser(PaymentInvoice $invoice): bool
    {
        $user = UserFacade::getCurrentUser();

        if ($user) {
            return $invoice->checkAccessForUser($user);
        }

        return false;
    }

    public function getTotalPayed(PaymentInvoice $invoice)
    {
        $amount = 0;
        foreach ($invoice->payments as $payment) {
            foreach ($payment->paymentDetails as $paymentDetail) {
                if ($paymentDetail?->paymentTransaction?->isPayed()) {
                    $amount += $paymentDetail?->paymentTransaction->amount;
                }
            }
        }
        return $amount;
    }

    /**
     * Total incoming amount
     *
     * @param PaymentInvoice $invoice
     * @return float
     */
    public function incomingAmount(PaymentInvoice $invoice)
    {
        $amount = 0;
        foreach ($invoice->payments as $payment) {
            $paymentDetailsQuery = $payment->getPaymentDetails();
            $amount              += $paymentDetailsQuery->incoming()->sum('amount');
        }
        return -$amount;
    }

    /**
     * Total outcoming amount
     *
     * @param PaymentInvoice $invoice
     * @return float
     */
    public function outcomingAmount(PaymentInvoice $invoice)
    {
        $amount = 0;
        foreach ($invoice->payments as $payment) {
            $paymentDetailsQuery = $payment->getPaymentDetails();
            $amount              += $paymentDetailsQuery->outcoming()->sum('amount');
        }
        return $amount;
    }

    public function allowCancelPayout(PaymentTransaction $paymentTransaction)
    {
        return (($paymentTransaction->vendor == PaymentTransaction::VENDOR_PP_PAYOUT) &&
            ($paymentTransaction->status == PaymentTransaction::STATUS_AUTHORIZED) &&
            ($paymentTransaction->type === PaymentTransaction::TYPE_MINUS)
//            ($paymentTransaction->transaction_id === 0)
        );
    }

    /**
     *
     * @param PaymentInvoice $paymentInvoice
     * @return PaymentTransaction
     */
    public function getPaymentTransaction(PaymentInvoice $paymentInvoice): ?PaymentTransaction
    {
        foreach ($paymentInvoice->payments as $payment) {
            /** @var PaymentTransaction $transaction */
            foreach ($payment->vendorTransactions as $transaction) {
                if ($transaction->type === PaymentTransaction::TYPE_PAYMENT) {
                    return $transaction;
                }
            }
        }
        return null;
    }
}