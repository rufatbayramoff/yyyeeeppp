<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.10.18
 * Time: 9:46
 */
namespace common\modules\payment\services;

use common\models\PaymentAccount;
use common\models\User;

class PaymentTaxService
{

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /**
     * @param PaymentAccountService $paymentAccountService
     */
    public function injectDependencies(
        PaymentAccountService $paymentAccountService
    ) {
        $this->paymentAccountService = $paymentAccountService;
    }

    /**
     * @param User[] $calcTaxesForUsers
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\db\Exception
     */
    public function sendTaxNotifications($calcTaxesForUsers)
    {
        foreach ($calcTaxesForUsers as $user) {
            // turned off login by ticket #1519:
            $hasTaxInfo = \common\models\PaymentDetailTaxRate::hasUserTaxes($user);
            if ($hasTaxInfo === false) { // taxes not added, we need to send email
                $totalCount =  $this->paymentAccountService->calculateAccountDetailsCount($this->paymentAccountService->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_AWARD));
                if ($totalCount == 1 || $totalCount == 2) {
                    $emailer = new \common\components\Emailer();
                    $emailer->sendFirstTaxMessage($user, 30); // first time
                }
            }
        }
    }
}