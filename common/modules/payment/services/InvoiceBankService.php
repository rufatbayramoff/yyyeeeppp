<?php
/**
 * User: nabi
 */

namespace common\modules\payment\services;

use common\components\DateHelper;
use common\components\Emailer;
use common\models\PaymentBankInvoice;
use common\models\PaymentInvoice;
use common\modules\payment\factories\InvoiceBankFactory;
use common\modules\payment\gateways\vendors\BankInvoiceGateway;
use common\modules\payment\repositories\InvoiceBankRepository;
use common\modules\payment\widgets\PaymentBankViewWidget;
use frontend\models\user\UserFacade;
use common\services\GeneratorPdf;
use yii\base\BaseObject;
use yii\web\ForbiddenHttpException;

/**
 * Class InvoiceBankService
 * @package common\modules\payment\services
 *
 * @property InvoiceBankFactory $factory
 * @property Emailer $emailer
 * @property InvoiceBankRepository $invoiceRepository
 * @property BankInvoiceGateway $gateway
 */
class InvoiceBankService extends BaseObject
{
    protected $factory;

    protected $emailer;

    protected $invoiceRepository;

    protected $gateway;

    /**
     * @var GeneratorPdf
     * */
    protected $invoiceReportPdf;

    /**
     *  from which price to show get invoice
     */
    public const INVOICE_FROM_USD = 400;

    /**
     * @param InvoiceBankFactory $factory
     * @param Emailer $emailer
     * @param InvoiceBankRepository $invoiceRepository
     * @param BankInvoiceGateway $gateway
     * @param GeneratorPdf $invoiceReportPdf
     */
    public function injectDependencies(
        InvoiceBankFactory $factory,
        Emailer $emailer,
        InvoiceBankRepository $invoiceRepository,
        BankInvoiceGateway $gateway,
        GeneratorPdf $invoiceReportPdf
    )
    {
        $this->factory           = $factory;
        $this->emailer           = $emailer;
        $this->invoiceRepository = $invoiceRepository;
        $this->gateway           = $gateway;
        $this->invoiceReportPdf  = $invoiceReportPdf;
    }

    /**
     * @return int
     */
    /**
     * @return int
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public static function getInvoiceFromUsd(): int
    {
        $fromUsd = (int)app('setting')->get('store.invoice_from', self::INVOICE_FROM_USD);
        return $fromUsd;
    }

    /**
     * @param PaymentInvoice $invoice
     *
     * @return PaymentBankInvoice
     * @throws \yii\base\UserException
     */
    public function createBankInvoiceByInvoice(PaymentInvoice $invoice): PaymentBankInvoice
    {
        $bankInvoice          = $this->factory->createByInvoice($invoice);
        $invoice->date_expire = DateHelper::addNowSec(PaymentInvoice::EXPIRE_TIME_BANK_INVOICE);
        $invoice->safeSave();
        $this->invoiceRepository->save($bankInvoice);

        return $bankInvoice;
    }

    public function sendEmailBankInvoice(PaymentBankInvoice $bankInvoice,string $pathToPdf)
    {
        // email to ceo
        $msg = 'Bank Invoice created for invoice ' . $bankInvoice->payment_invoice_uuid;
        $this->emailer->sendSupportMessage($msg, $msg);
        $this->emailer->sendMessage('ceo@treatstock.com', $msg, $msg);
        if(file_exists($pathToPdf)) {
            $this->emailer->sendBankInvoicePayment($bankInvoice, $pathToPdf);
        }
    }

    /**
     * @param PaymentInvoice $invoice
     *
     * @throws ForbiddenHttpException
     */
    public function checkAccessToInvoice(PaymentInvoice $invoice): void
    {
        $user        = UserFacade::getCurrentUser();
        $userSession = UserFacade::getUserSession();

        if (!UserFacade::isObjectOwner($invoice, $user, $userSession)) {
            throw new ForbiddenHttpException(_t('site.order', 'Access denied for current invoice'));
        }
    }

    /**
     * @param PaymentBankInvoice $bankInvoice
     *
     * @throws \yii\base\UserException
     */
    public function settle(PaymentBankInvoice $bankInvoice): void
    {
        $bankInvoice->status = PaymentBankInvoice::STATUS_PAID;
        $bankInvoice->safeSave(['status']);
    }

    /**
     * @param PaymentBankInvoice $bankInvoice
     *
     * @throws \yii\base\UserException
     */
    public function extend(PaymentBankInvoice $bankInvoice): void
    {
        $bankInvoice->status   = PaymentBankInvoice::STATUS_NEW;
        $bankInvoice->date_due = DateHelper::addNowSec(60 * 60 * 24 * 10); // 10 days
        $bankInvoice->safeSave(['status', 'date_due']);
    }

    /**
     * @param PaymentBankInvoice $bankInvoice
     *
     * @throws \yii\base\UserException
     */
    public function void(PaymentBankInvoice $bankInvoice): void
    {
        $bankInvoice->status = PaymentBankInvoice::STATUS_VOID;
        $bankInvoice->safeSave(['status']);
    }

    /**
     * expire invoice, current logic only change status.
     *
     * @param PaymentBankInvoice $invoice
     *
     * @throws \yii\base\UserException
     */
    public function expireInvoice(PaymentBankInvoice $invoice): void
    {
        $invoice->status = PaymentBankInvoice::STATUS_EXPIRED;
        $invoice->safeSave(['status']);

        // what with order?
        if (!empty($invoice->order)) {

        }
    }

    /**
     * @param PaymentBankInvoice $paymentBankInvoice
     * @return string
     * @throws \Exception
     */
    public function paymentBankInvoicePdf(PaymentBankInvoice $paymentBankInvoice): string
    {
        $content = PaymentBankViewWidget::widget([
            'bankInvoice' => $paymentBankInvoice,
            'isPdf'       => true
        ]);
        return $this->pdf($content);
    }

    /**
     * @param string $content
     * @return string
     */
    public function pdf(string $content): string
    {
        return $this->invoiceReportPdf->write($content,'paymentBankInvoicepdf.html');
    }
}