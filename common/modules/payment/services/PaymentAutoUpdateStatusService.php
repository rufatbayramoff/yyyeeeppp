<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.10.18
 * Time: 17:46
 */

namespace common\modules\payment\services;

use Braintree\Transaction;
use common\components\ArrayHelper;
use common\components\DateHelper;
use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use yii\base\BaseObject;

class PaymentAutoUpdateStatusService extends BaseObject
{
    public $isDebug = true;

    /** @var PaymentStoreOrderService */
    protected $paymentStoreOrderService;

    /** @var PaymentService */
    protected $paymentService;

    public function injectDependencies(
        PaymentStoreOrderService $paymentStoreOrderService,
        PaymentService $paymentService
    )
    {
        $this->paymentStoreOrderService = $paymentStoreOrderService;
        $this->paymentService           = $paymentService;
    }


    protected function println($out)
    {
        if (\Yii::$app instanceof \yii\console\Application) {
            echo DateHelper::now() . ': ' . $out . "\n";
        } else {
            echo DateHelper::now() . ': ' . $out . '<br/>';
        }
    }

    protected function getTransactionsForUpdateBraitree()
    {
        $sqlPaymentTransactions = PaymentTransaction::find()
            ->where(['vendor' => 'braintree'])
            ->andWhere(['!=', 'transaction_id', '-'])
            ->andWhere([
                'not in',
                'status',
                [
                    PaymentTransaction::STATUS_SETTLED,
                    PaymentTransaction::STATUS_AUTHORIZED_EXPIRED,
                    PaymentTransaction::STATUS_VOIDED,
                    PaymentTransaction::STATUS_REFUNDED,
                ]
            ])
            ->andWhere(['<', PaymentTransaction::column('updated_at'), dbexpr('NOW() - INTERVAL 3 MINUTE')])
            ->limit(200);
        if ($this->isDebug) {
            $this->println(debugSql($sqlPaymentTransactions, true));
        }
        $paymentTransactions = $sqlPaymentTransactions->all();

        return $paymentTransactions;
    }

    public function updateTransactions(StoreOrder $storeOrder)
    {
        foreach ($storeOrder->paymentInvoices as $invoice) {
            $transaction = $invoice->getActivePaymentFirstTransaction();
            if ($transaction) {
                $this->updateVendorTransaction($transaction);
            }
        }
    }

    public function updateVendorTransaction(PaymentTransaction $paymentTransaction)
    {
        $paymentCheckout    = new PaymentCheckout($paymentTransaction->vendor);
        $gatewayTransaction = $paymentCheckout->getTransaction($paymentTransaction->transaction_id);
        if ($paymentTransaction->vendor === PaymentTransaction::VENDOR_BRAINTREE) {
            $this->paymentStoreOrderService->updatePaymentTransactionStatus($paymentTransaction, $gatewayTransaction, $paymentCheckout);
        } else {
            $this->paymentService->updateTransactionStatus($paymentTransaction, $gatewayTransaction->status);
        }
    }

    /**
     * @param PaymentTransaction $paymentTransactionList
     * @throws \yii\base\InvalidConfigException
     */
    public function updateBraintreeTransaction($paymentTransactionList)
    {
        if (!$paymentTransactionList) {
            return;
        }
        $transactionIds          = ArrayHelper::getColumn($paymentTransactionList, 'transaction_id');
        $paymentTransactionsList = ArrayHelper::index($paymentTransactionList, 'transaction_id');
        $paymentCheckout         = new PaymentCheckout(PaymentTransaction::VENDOR_BRAINTREE);
        $gatewayTransactions     = $paymentCheckout->getTransactions($transactionIds);
        foreach ($gatewayTransactions as $gatewayTransaction) {
            /** @var $braintreeTransaction Transaction */
            if (!array_key_exists($gatewayTransaction->transactionId, $paymentTransactionsList)) {
                continue;
            }
            $paymentTransaction = $paymentTransactionsList[$gatewayTransaction->transactionId];
            $this->paymentStoreOrderService->updatePaymentTransactionStatus($paymentTransaction, $gatewayTransaction, $paymentCheckout);
        }
    }

    /**
     *
     */
    public function updateStatusesBraintree()
    {
        $paymentTransactions = $this->getTransactionsForUpdateBraitree();
        $this->println('Found transactions to update: ' . count($paymentTransactions));
        $this->updateBraintreeTransaction($paymentTransactions);
    }

}