<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 23.10.18
 * Time: 14:41
 */

namespace common\modules\payment\services;

use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\PreorderWork;
use yii\base\BaseObject;

class PaymentPreorderService extends BaseObject
{
    /**
     * @param PaymentInvoiceItem $invoiceItem
     *
     * @return PreorderWork|null
     */
    public function getPreorderWorkByInvoiceItem(PaymentInvoiceItem $invoiceItem): ?PreorderWork
    {
        $items = $invoiceItem->paymentInvoice->accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS] ?? [];

        $preorderWorkId = !empty($items[$invoiceItem->uuid]) && $items[$invoiceItem->uuid]['preorderWorkId'] ? (int) $items[$invoiceItem->uuid]['preorderWorkId'] : null;

        foreach ($invoiceItem->paymentInvoice->preorder->preorderWorks as $work) {
            if ($preorderWorkId === $work->id) {
                return $work;
            }
        }

        return null;
    }
}