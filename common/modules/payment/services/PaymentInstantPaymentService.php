<?php

namespace common\modules\payment\services;

use common\components\exceptions\BusinessException;
use common\models\InstantPayment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\exception\FatalPaymentException;
use common\modules\payment\exception\PaymentException;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use yii\base\BaseObject;

class PaymentInstantPaymentService extends BaseObject
{
    /** @var PaymentService */
    protected $paymentService;

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    public function injectDependencies(PaymentService $paymentService, PaymentAccountService $paymentAccountService)
    {
        $this->paymentService        = $paymentService;
        $this->paymentAccountService = $paymentAccountService;
    }

    public function cancelPayment(InstantPayment $instantPayment)
    {
        $invoice = $instantPayment->primaryInvoice;

        $payTransaction = $this->paymentService->getPaymentTransaction($invoice);

        if (!$payTransaction) {
            throw new BusinessException('Is not payed');
        }

        $payment         = $payTransaction->firstPaymentDetail->payment;
        $refundToAccount = $this->paymentAccountService->getUserMainAmount($instantPayment->fromUser);

        $paymentCheckout = new PaymentCheckout($payTransaction->vendor);
        $paymentCheckout->cancel($payTransaction->firstPaymentDetail->paymentDetailOperation, 'Instant payment was canceled');
    }

    public function approvePayment(InstantPayment $instantPayment)
    {
        $invoice    = $instantPayment->primaryInvoice;
        $clientUser = $instantPayment->fromUser;

        if (!$invoice) {
            throw new FatalPaymentException('Null invoice is impossible');
        }

        // Submit to payment
        $this->paymentService->submitForSettleInvoice($invoice);

        $totalAmount            = $invoice->getAmountTotal();
        $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();
        $awardAmount            = $amountPaymentMethodFee ? MoneyMath::minus($totalAmount, $amountPaymentMethodFee) : $totalAmount;
        $feeInAmount            = $invoice->getAmountInstantPaymentFeeIn();
        $companyAwardAmount     = $invoice->getAmountInstantPaymentAward();

        $payments = $invoice->payments;
        $payment  = reset($payments);

        $reservedOperation = $this->paymentService->findReservedDetail($payment);

        // Not reserved!!! We transfer money directly from authorized to invoice
        $accountReservedType = $reservedOperation ? PaymentAccount::ACCOUNT_TYPE_RESERVED : PaymentAccount::ACCOUNT_TYPE_AUTHORIZE;

        $accountReserved        = $this->paymentAccountService->getUserPaymentAccount($clientUser, $accountReservedType, $invoice->currency);
        $accountInvoice         = $this->paymentAccountService->getUserPaymentAccount($clientUser, PaymentAccount::ACCOUNT_TYPE_INVOICE, $invoice->currency);
        $accountCompanyAward    = $this->paymentAccountService->getUserPaymentAccount($instantPayment->toUser, PaymentAccount::ACCOUNT_TYPE_AWARD, $invoice->currency);
        $accountCompanyMain     = $this->paymentAccountService->getUserPaymentAccount($instantPayment->toUser, PaymentAccount::ACCOUNT_TYPE_MAIN, $invoice->currency);
        $accountTreatstockFee   = $this->paymentAccountService->getTreatsockFeeAccount($invoice->currency);
        $accountTreatstockBonus = $this->paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);
        $clientUserBonusAccount = $this->paymentAccountService->getUserPaymentAccount($clientUser, PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);

        // Transfer money to client:invoice payment account
        if ($totalAmount->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountReserved, $accountInvoice, $totalAmount, 'Invoice total payment');
        }
        $this->paymentService->transferMoney($payment, $accountInvoice, $accountCompanyAward, $awardAmount, 'Instant payment total award');
        if ($feeInAmount) {
            $this->paymentService->transferMoney($payment, $accountCompanyAward, $accountTreatstockFee, $feeInAmount, 'Instant payment fee include');
        }
        $this->paymentService->transferMoney($payment, $accountCompanyAward, $accountCompanyMain, $companyAwardAmount, PaymentDetail::TYPE_PAYMENT, 'Award for instant payment ' . $instantPayment->uuid);

        // Payment Method Fee
        $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee();
        if ($amountPaymentMethodFee) {
            $this->paymentService->transferMoney($payment, $accountInvoice, $accountTreatstockFee, $amountPaymentMethodFee, PaymentDetail::TYPE_PAYMENT,
                'Payment method fee'
            );
        }

        $bonusAccruedFromPs = $invoice->getAmountBonusAccruedFromPs();
        $bonusAccruedFromTs = $invoice->getAmountBonusAccruedFromTs();
        $bonusFromDescr = " for instant payment ".$instantPayment->uuid;
        if ($bonusAccruedFromTs?->getAmount()) {
            $this->paymentService->transferMoney($payment, $accountTreatstockBonus, $clientUserBonusAccount, $bonusAccruedFromTs, PaymentDetail::TYPE_PAYMENT,
                'Bonus from Treatstock'.$bonusFromDescr);
        }
        if ($bonusAccruedFromPs?->getAmount()) {
            $convertAccountType     = PaymentAccount::getConvertAccountType($invoice->currency, Currency::BNS);
            $convertAccountBNS      = $this->paymentAccountService->getConvertAccount(Currency::BNS, $convertAccountType);
            $convertAccountCurrency = $this->paymentAccountService->getConvertAccount($invoice->currency, $convertAccountType);
            $this->paymentService->transferMoney($payment, $accountCompanyMain, $convertAccountCurrency, Money::create($bonusAccruedFromPs->getAmount(), $invoice->currency), PaymentDetail::TYPE_PAYMENT, 'Bonus' .$bonusFromDescr);
            $companyTitle = mb_ucfirst($instantPayment->toUser->company ? $instantPayment->toUser->company->title : $instantPayment->toUser->username);
            $this->paymentService->transferMoney($payment, $convertAccountBNS, $clientUserBonusAccount, $bonusAccruedFromPs, PaymentDetail::TYPE_PAYMENT, 'Bonus from "' . $companyTitle . '"'.$bonusFromDescr);
        }

    }
}