<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.10.18
 * Time: 11:39
 */

namespace common\modules\payment\services;

use common\components\DateHelper;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentTransaction;
use common\models\query\PaymentAccountQuery;
use common\models\User;
use common\modules\payment\models\PaymentAccountBalancePeriod;
use common\modules\payment\repositories\PaymentAccountRepository;
use lib\money\Currency;
use lib\money\Money;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;
use yii\data\ActiveDataProvider;

/**
 * Class PaymentAccountService
 *
 * @package common\modules\payment\services
 *
 * @property PaymentAccountRepository $paymentAccountRepository
 */
class PaymentAccountService extends BaseObject
{
    protected $paymentAccountRepository;

    public function injectDependencies(
        PaymentAccountRepository $paymentAccountRepository
    )
    {
        $this->paymentAccountRepository = $paymentAccountRepository;
    }

    public function getConvertAccount($currency, $type)
    {
        return PaymentAccount::tryFind(['user_id' => User::USER_TS, 'type' => $type, 'currency' => $currency]);
    }

    /**
     * @param User $user
     * @param string $type
     * @param string $currency
     * @return PaymentAccount
     *
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\InvalidConfigException
     */
    public function getUserPaymentAccount(User $user, $type = PaymentAccount::ACCOUNT_TYPE_MAIN, $currency = Currency::USD)
    {
        $paymentAccount = $this->paymentAccountRepository->getAccountByUserIdAndType($user->id, $type, $currency);

        if (!$paymentAccount && !$user->getIsSystem()) {
            // Not system user, create payment account
            /** @var PaymentAccount $paymentAccount */
            $paymentAccount           = \Yii::createObject(PaymentAccount::class);
            $paymentAccount->user_id  = $user->id;
            $paymentAccount->type     = $type;
            $paymentAccount->currency = $currency;
            $paymentAccount->safeSave();
            $this->paymentAccountRepository->putAccountByUserIdAndType($paymentAccount);
        }

        return $paymentAccount;
    }

    public function getUserPaymentAccounts(User $user, $type= PaymentAccount::ACCOUNT_TYPE_MAIN)
    {
        $paymentAccounts = [];
        foreach (Currency::ALL_CURRENCIES as $currency) {
            $paymentAccount = $this->getUserPaymentAccount($user, $type, $currency);
            if ($paymentAccount) {
                $paymentAccounts[$currency] = $paymentAccount;
            }
        }
        return $paymentAccounts;
    }

    public function getTotalPayout(User $user, $currency): Money
    {
        $query = "SELECT sum(payment_detail.amount) as summ FROM payment_detail LEFT JOIN payment_account on payment_account.id=payment_detail.payment_account_id  " .
            "WHERE payment_account.user_id=" . $user->id . " and (payment_account.type='main' or payment_account.type='refund') and payment_account.currency='".$currency."' and payment_detail.type='payout' ";

        $amount = Money::create(-\Yii::$app->db->createCommand($query)->queryScalar(), $currency);
        return $amount;
    }

    /**
     * @param $currency
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTreatsockEasyPostAccount($currency)
    {
        $accounts = [
            Currency::EUR => PaymentAccount::ACCOUNT_TREATSTOCK_EASYPOST_EUR,
            Currency::USD => PaymentAccount::ACCOUNT_TREATSTOCK_EASYPOST
        ];
        return PaymentAccount::tryFindByPk($accounts[$currency]);
    }

    /**
     * @param $currency
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTreatsockFeeAccount($currency)
    {
        $accounts = [
            Currency::EUR => PaymentAccount::ACCOUNT_TREATSTOCK_EUR,
            Currency::USD => PaymentAccount::ACCOUNT_TREATSTOCK_USD
        ];
        return PaymentAccount::tryFindByPk($accounts[$currency]);
    }

    /**
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getSupportAccount()
    {
        return PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_SUPPORT);
    }

    /**
     * @param string $type
     * @param $currency
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTreatstockAccount($type = PaymentAccount::ACCOUNT_TYPE_MAIN, $currency = Currency::USD): PaymentAccount
    {
        return PaymentAccount::tryFind(['user_id' => User::USER_TS, 'type' => $type, 'currency'=>$currency]);
    }

    /**
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getThingiverseAccount($type = PaymentAccount::ACCOUNT_TYPE_MAIN)
    {
        return PaymentAccount::tryFind(['user_id' => PaymentAccount::ACCOUNT_THINGIVERSE, 'type' => $type]);
    }

    /**
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getEasypostAccount()
    {
        return PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_EASYPOST);
    }

    /**
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTaxagentAccount()
    {
        return PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_TAXAGENT);
    }

    /**
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getPaypalAccount()
    {
        return PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_PAYPAL);
    }

    /**
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getBraintreeAccount()
    {
        return PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_BRAINTREE);
    }

    /**
     * @param string $currency
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getBankTransferAccount($currency = Currency::USD)
    {
        if ($currency == Currency::USD) {
            $id = PaymentAccount::ACCOUNT_BANK_TRANSFER;
        } elseif ($currency == Currency::EUR) {
            $id = PaymentAccount::ACCOUNT_BANK_TRANSFER_EUR;
        } else {
            throw new InvalidArgumentException('Invalid currency: ' . $currency);
        }
        return PaymentAccount::tryFindByPk($id);
    }

    /**
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getAnonimAccount()
    {
        return PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_ANONIM);
    }

    /**
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTreatstockDiscountAccount()
    {
        return PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_TREATSTOCK_DISCOUNT);
    }

    /**
     * @return PaymentAccount
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTreatstockEasypostAccount()
    {
        return PaymentAccount::tryFindByPk(PaymentAccount::ACCOUNT_TREATSTOCK_EASYPOST);
    }

    public function getUserMainAmounts(User $user)
    {
        $amounts = [];
        foreach (Currency::ALL_CURRENCIES as $currency) {
            $amount = $this->getUserMainAmount($user,$currency);
            if ($amount->getAmount()) {
                $amounts[$currency] = $amount;
            }
        }
        if (empty($amounts[Currency::USD]) && empty($amounts[Currency::EUR])) {
            $amounts[Currency::USD] = Money::zero(Currency::USD);
        }
        return $amounts;
    }

    /**
     * @param User $user
     * @param string $currency
     * @return mixed
     *
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\InvalidConfigException
     */
    public function getUserMainAmount(User $user, $currency = Currency::USD): Money
    {
        return $this->calculateAccountBalance($this->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_MAIN, $currency));
    }

    public function calculateAccountBalance(PaymentAccount $paymentAccount)
    {
        $sum = PaymentDetail::find()->withoutStaticCache()->where(['payment_account_id' => $paymentAccount->id])->sum('amount');
        return Money::create($sum, $paymentAccount->currency);
    }

    /**
     * Calculate total account amount
     *
     * @param PaymentAccount $paymentAccount
     * @param Payment $payment May be filtered py payment
     * @param null $periodBegin
     * @param null $periodEnd
     * @return Money
     * @throws \yii\db\Exception
     */
    public function calculateAccountAmount(PaymentAccount $paymentAccount, Payment $payment, $periodBegin = null, $periodEnd = null)
    {
        $query = "
SELECT sum(payment_detail.amount) as summ FROM payment_detail
 LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid
WHERE payment_detail.payment_account_id=" . $paymentAccount->id . "
and payment_detail_operation.payment_id = " . $payment->id . "
";
        if ($periodBegin) {
            $query .= " AND payment_detail_operation.created_at>='" . $periodBegin . "'";
        }
        if ($periodEnd) {
            $query .= " AND payment_detail_operation.created_at<='" . $periodEnd . "'";
        }
        $amount = Money::create(\Yii::$app->db->createCommand($query)->queryScalar(), $paymentAccount->currency);
        return $amount;
    }

    /**
     * @param PaymentAccount $paymentAccount
     * @param null $periodBegin
     * @param null $periodEnd
     * @return Money
     * @throws \yii\db\Exception
     */
    public function calculateAccountDetailsCount(PaymentAccount $paymentAccount, $periodBegin = null, $periodEnd = null)
    {
        $query = "SELECT count(payment_detail.amount) as cnt FROM payment_detail LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_detail.payment_account_id=" . $paymentAccount->id;
        if ($periodBegin) {
            $query .= " AND payment_detail_operation.created_at>='" . $periodBegin . "'";
        }
        if ($periodEnd) {
            $query .= " AND payment_detail_operation.created_at<='" . $periodEnd . "'";
        }
        $count = \Yii::$app->db->createCommand($query)->queryScalar();
        return (int)$count;
    }

    /**
     * @param PaymentAccountQuery $paymentAccountQuery
     * @param $periodBegin
     * @param $periodEnd
     * @return PaymentAccountBalancePeriod[]
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function calculateAccountsBalanceEx(PaymentAccountQuery $paymentAccountQuery, $periodBegin = null, $periodEnd = null): array
    {
        $balances = [];
        if ($periodBegin) {
            $periodBegin = DateHelper::filterDate($periodBegin);
        } else {
            $periodBegin = '0001-01-01';
        }
        if ($periodEnd) {
            $periodEnd = DateHelper::filterDate($periodEnd);
        } else {
            // Date in feature
            $periodEnd = '9999-01-01';
        }

        $paymentAccounts = $paymentAccountQuery->all();
        foreach ($paymentAccounts as $paymentAccount) {
            $userBalanceEx                 = \Yii::createObject(PaymentAccountBalancePeriod::class);
            $userBalanceEx->paymentAccount = $paymentAccount;

            $query                       = "SELECT sum(payment_detail.amount) as summ FROM payment_detail LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_detail.payment_account_id=" . $paymentAccount['id'] . " AND payment_detail_operation.created_at<'" . $periodBegin . "'";
            $userBalanceEx->balanceStart = Money::create(\Yii::$app->db->createCommand($query)->queryScalar(), $paymentAccount->currency);

            $query                         = "SELECT sum(payment_detail.amount) as summ FROM payment_detail LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_detail.payment_account_id=" . $paymentAccount['id'] . " AND payment_detail_operation.created_at<='" . $periodEnd . "'";
            $userBalanceEx->balanceCurrent = Money::create(\Yii::$app->db->createCommand($query)->queryScalar(),$paymentAccount->currency);

            $query                      = "SELECT sum(payment_detail.amount) as summ FROM payment_detail LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_detail.payment_account_id=" . $paymentAccount['id'] . " AND payment_detail_operation.created_at>='" . $periodBegin . "' AND payment_detail.amount>0 AND payment_detail_operation.created_at<='" . $periodEnd . "'";
            $userBalanceEx->balancePlus = Money::create(\Yii::$app->db->createCommand($query)->queryScalar(), $paymentAccount->currency);

            $query                       = "SELECT sum(payment_detail.amount) as summ FROM payment_detail LEFT JOIN payment_detail_operation on payment_detail_operation.uuid=payment_detail.payment_detail_operation_uuid WHERE payment_detail.payment_account_id=" . $paymentAccount['id'] . " AND payment_detail_operation.created_at>='" . $periodBegin . "' AND payment_detail.amount<0 AND payment_detail_operation.created_at<='" . $periodEnd . "'";
            $userBalanceEx->balanceMinus = Money::create(\Yii::$app->db->createCommand($query)->queryScalar(), $paymentAccount->currency);
            if (!$userBalanceEx->isEmpty()) {
                $balances[] = $userBalanceEx;
            }
        };
        return $balances;
    }

    /**
     * @param PaymentAccount $paymentAccount
     * @param string $type
     * @param null $from
     * @param null $to
     * @param int $pageSize
     *
     * @return ActiveDataProvider
     */
    public function getPaymentDetailsProvider(User $user, $type = 'all', $from = null, $to = null, $pageSize = 20): ActiveDataProvider
    {
        $query = PaymentDetail::find()
            ->joinWith('paymentAccount')
            ->where([
                'and',
                ['payment_account.user_id' => $user->id],
                ['payment_account.type' => 'main'],
                ['!=', 'amount', 0],
            ])
            ->with([
                'paymentDetailOperation.payment.paymentInvoice',
                'paymentTransaction'
            ]);

        if ($type === 'payout') {
            $query->andWhere(['<', 'amount', 0]);
        } elseif ($type === 'income') {
            $query->andWhere(['>', 'amount', 0]);
        }

        if ($from && $to) {
            $fromDate = date('Y-m-d 00:00:01', strtotime($from));
            $toDate   = date('Y-m-d 23:59:59', strtotime($to));
            $query->andWhere(['between', 'updated_at', $fromDate, $toDate]);
        }

        $activeDataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ]
        ]);

        $activeDataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];

        return $activeDataProvider;
    }

    public function lastDetailsBankPayout(User $user): ?PaymentTransaction
    {
        return PaymentTransaction::find()
            ->forUser($user)
            ->andWhere(['vendor'=>PaymentTransaction::VENDOR_BANK_PAYOUT])
            ->andWhere(['is not','details', NULL])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }

}