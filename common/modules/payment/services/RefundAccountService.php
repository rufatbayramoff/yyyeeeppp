<?php

namespace common\modules\payment\services;

use common\models\DeliveryType;
use common\models\PaymentAccount;
use common\models\PaymentTransactionRefund;
use common\models\User;
use common\modules\payment\models\RefundRequestForm;
use yii\base\BaseObject;

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.11.18
 * Time: 17:51
 */
class RefundAccountService extends BaseObject
{
    /** @var PaymentAccountService */
    public $paymentAccountService;

    /** @var PaymentService */
    public $paymentService;

    public function injectDependencies(PaymentAccountService $paymentAccountService, PaymentService $paymentService)
    {
        $this->paymentAccountService = $paymentAccountService;
        $this->paymentService = $paymentService;
    }


    /**
     * detect from which user to refund
     * if $this->order is bined, it used to detect by refund type
     * correct user. for example for shipping, model author and etc.
     *
     * @param PaymentTransactionRefund $transactionRefund
     * @return PaymentAccount
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function detectRefundFromPaymentAccount(PaymentTransactionRefund $transactionRefund): PaymentAccount
    {
        $paymentTransaction = $transactionRefund->transaction;
        $paymentDetail = $paymentTransaction->firstPaymentDetail;
        $order = $transactionRefund->paymentInvoice->storeOrder;
        if ($this->paymentService->isMoneyOnReservedAccount($paymentTransaction)) {
            $toPaymentAccount = $paymentDetail->paymentDetailOperation->toPaymentDetail()->getPaymentAccount();
            $reserved = $this->paymentAccountService->getUserPaymentAccount($toPaymentAccount->user, PaymentAccount::ACCOUNT_TYPE_RESERVED);
            return $reserved;
        }


        $paymentAccount = $this->paymentAccountService->getTreatstockAccount();

        // we get money from refund requester by refund_type
        $paymentRefund = $transactionRefund->paymentTransactionRefund;
        if ($paymentRefund->from_user_id) {
            $paymentAccount = $this->paymentAccountService->getUserPaymentAccount($paymentRefund->from_user_id, PaymentAccount::ACCOUNT_TYPE_MAIN);
        }


        // Nabi: grbg logic :(
        // Analitic: A am agree!
        // refund from PS
        $userId = null;
        if ($transactionRefund->refundType == PaymentTransactionRefund::REFUND_TYPE_PS ||
            $transactionRefund->refundType == PaymentTransactionRefund::REFUND_TYPE_PS) {
            $userId = $this->order->currentAttemp->ps->user_id;
        } elseif ($transactionRefund->refundType == PaymentTransactionRefund::REFUND_TYPE_PS) {
            $deliveryDetails = $this->order->getOrderDeliveryDetails();
            if ($deliveryDetails['carrier'] === DeliveryType::CARRIER_MYSELF) {
                $userId = $this->order->currentAttemp->ps->user_id;
            } else {
                $userId = User::USER_EASYPOST;
            }
        } elseif ($transactionRefund->refundType == PaymentTransactionRefund::REFUND_TYPE_PS) {
            $userId = $this->order->getFirstItem()->unit->model3d->user_id; // model author
        }
        if ($userId) {
            $paymentAccount = $this->paymentAccountService->getUserPaymentAccount($userId, PaymentAccount::ACCOUNT_TYPE_MAIN);
        }

        return $paymentAccount;
    }
}