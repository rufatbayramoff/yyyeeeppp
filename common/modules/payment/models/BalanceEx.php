<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.09.18
 * Time: 14:35
 */

namespace common\modules\payment\models;

use common\models\User;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;

class BalanceEx
{
    /** @var Money[] */
    public $balanceCurrent;

    /** @var Money[] */
    public $balanceStart;

    /** @var Money[] */
    public $balancePlus;

    /** @var Money[] */
    public $balanceMinus;

    public function getDiff()
    {
        $returnValue = [];
        foreach ($this->balancePlus as $type => $balance) {
            $returnValue[$type] = Money::create($balance->getAmount() + (isset($this->balanceMinus[$type]) ? $this->balanceMinus[$type]->getAmount() : 0), Currency::USD);
        }
        return $returnValue;
    }

    /**
     * @return float
     */
    public function getCurrentTotalAmount()
    {
        $total = 0;
        foreach ($this->balanceCurrent as $balance) {
            $total += $balance->getAmount();
        }
        return $total;
    }
}