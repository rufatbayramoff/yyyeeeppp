<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.09.18
 * Time: 14:35
 */

namespace common\modules\payment\models;

use backend\modules\payment\models\AccountBalanceForm;
use common\models\PaymentAccount;
use common\models\User;
use common\modules\payment\services\PaymentAccountService;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;

class AccountBalance
{
    /** @var PaymentAccount */
    public ?PaymentAccount $paymentAccount;

    /** @var Money */
    public ?Money $money;

    public function loadByForm(AccountBalanceForm $form)
    {
        $paymentAccountService = \Yii::createObject(PaymentAccountService::class);
        if ($form->userId) {
            $user                 = User::tryFindByPk($form->userId);
            $this->paymentAccount = $paymentAccountService->getUserPaymentAccount($user, PaymentAccount::ACCOUNT_TYPE_MAIN, Currency::BNS);
        }
        if ($form->userId) {
            $this->money = Money::create($form->amount, Currency::BNS);
        }
    }
}
