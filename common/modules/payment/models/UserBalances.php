<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.09.18
 * Time: 9:40
 */

namespace common\modules\payment\models;

use common\models\User;
use lib\money\Money;

class UserBalances {

    /** @var User */
    public $user;

    /** @var Money */
    public $main;

    /** @var Money */
    public $authorize;

    /** @var Money */
    public $reserved;

    /** @var Money */
    public $tax;

    /** @var Money */
    public $refundRequest;

}