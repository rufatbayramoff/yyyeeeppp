<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.10.18
 * Time: 15:23
 */
namespace common\modules\payment\models;

use common\models\PaymentInvoice;
use lib\money\Money;
use lib\money\MoneyMath;

/**
 * Class PaymentInvoiceAdditionalServiceAmount
 * @package common\modules\payment\models
 *
 * @property PaymentInvoice $paymentInvoice
 */
class PaymentInvoiceAdditionalServiceAmount
{
    /** @var PaymentInvoice */
    public $paymentInvoice;

    public function __construct(PaymentInvoice $paymentInvoice)
    {
        $this->paymentInvoice = $paymentInvoice;
    }

    /**
     * @return Money|null
     */
    public function getAmountFee(): ?Money
    {
        return $this->paymentInvoice->getItemsSubTotalByType(PaymentInvoice::ACCOUNTING_TYPE_FEE);
    }

    /**
     * @return Money|null
     */
    public function getAmountFeeWithRefund(): ?Money
    {
        $money = $this->paymentInvoice->getItemsSubTotalByType(PaymentInvoice::ACCOUNTING_TYPE_FEE);
        $refund = $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_FEE, false);

        if ($money && $refund) {
            return MoneyMath::minus($money, $refund);
        }

        return $money;
    }

    /**
     * @return Money|null
     */
    public function getAmountManufacturer(): ?Money
    {
        return $this->paymentInvoice->getItemsSubTotalByType(PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER);
    }

    public function getAmountManufacturerWithRefund(): ?Money
    {
        $money = $this->paymentInvoice->getItemsSubTotalByType(PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER);
        $refund = $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER, false);

        if ($money && $refund) {
            return MoneyMath::minus($money, $refund);
        }

        return $money;
    }
}