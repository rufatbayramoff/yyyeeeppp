<?php
/**
 * User: nabi
 */

namespace common\modules\payment\models;

use common\models\PaymentTransaction;
use common\models\PaymentTransactionRefund;
use common\models\UserAdmin;
use lib\money\Money;
use yii\base\Model;

/**
 * Class RefundRequestForm
 *
 * @package common\modules\payment\models
 */
class RefundRequestForm extends Model
{

    /**
     * @var PaymentTransaction
     */
    public $transaction;

    /**
     * @var Money
     */
    public $amountMoney;

    /**
     * @var UserAdmin
     */
    public $user;

    /**
     * @var string
     */
    public $refundType;

    /**
     * @var User
     */
    public $fromUser;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var PaymentTransactionRefund
     */
    public $paymentTransactionRefund;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment', 'transaction', 'amountMoney', 'user'], 'required'],
        ];
    }

}
