<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.10.18
 * Time: 15:23
 */

namespace common\modules\payment\models;

use common\models\PaymentInvoice;
use lib\money\Money;
use lib\money\MoneyMath;

/**
 * Class PaymentInvoicePreorderAmount
 * @package common\modules\payment\models
 *
 * @property PaymentInvoice $paymentInvoice
 */
class PaymentInvoicePreorderAmount
{
    /** @var PaymentInvoice */
    public $paymentInvoice;

    public function __construct(PaymentInvoice $paymentInvoice)
    {
        $this->paymentInvoice = $paymentInvoice;
    }

    /**
     * @return Money|null
     */
    public function getAmountFee(): ?Money
    {
        return $this->paymentInvoice->getItemsSubTotalByType(PaymentInvoice::ACCOUNTING_TYPE_FEE);
    }

    public function getAmountFeeWithRefund(): ?Money
    {
        $money = $this->paymentInvoice->getItemsSubTotalByType(PaymentInvoice::ACCOUNTING_TYPE_FEE);
        $refund = $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_FEE, false);

        if ($money && $refund) {
            return MoneyMath::minus($money, $refund);
        }

        return $money;
    }

    /**
     * @return Money|null
     */
    public function getAmountManufacturer(): ?Money
    {
        return $this->paymentInvoice->getItemsSubTotalByType(PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER);
    }

    /**
     * @return Money|null
     */
    public function getAmountManufacturerWithRefund(): ?Money
    {
        $money = $this->paymentInvoice->getItemsSubTotalByType(PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER);
        $refund = $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER, false);

        if ($money && $refund) {
            return MoneyMath::minus($money, $refund);
        }

        return $money;
    }

    /**
     * @param $invoiceItemUuid
     *
     * @return Money|null
     */
    public function getAmountManufacturerByItemUuid($invoiceItemUuid): ?Money
    {
        return $this->paymentInvoice->getItemsSubTotalByTypeAndUuid(PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER, $invoiceItemUuid);
    }

    /**
     * @param $invoiceItemUuid
     *
     * @return Money|null
     */
    public function getAmountFeeByItemUuid($invoiceItemUuid): ?Money
    {
        return $this->paymentInvoice->getItemsSubTotalByTypeAndUuid(PaymentInvoice::ACCOUNTING_TYPE_FEE, $invoiceItemUuid);
    }

    /**
     * @param $invoiceItemUuid
     *
     * @return Money
     */
    public function getAmountTotalByItemUuid($invoiceItemUuid): ?Money
    {
        $fee = $this->getAmountFeeByItemUuid($invoiceItemUuid);
        $manufacturer = $this->getAmountManufacturerByItemUuid($invoiceItemUuid);

        if (!$fee && !$manufacturer) {
            return null;
        }

        $money = Money::zero($this->paymentInvoice->currency);

        foreach ([$fee, $manufacturer] as $item) {
            if ($item) {
                $money = MoneyMath::sum($money, $item);
            }
        }

        return $money;
    }
}