<?php
/**
 * User: nabi
 */

namespace common\modules\payment\models;

use lib\money\Money;
use common\modules\payment\taxes\Tax;

/**
 * Class InvoiceItem
 *
 * Information about a single line item.
 *
 */
class InvoiceItem
{
    private $name;
    private $description;
    private $discount;
    private $measure;
    private $date;
    private $tax;
    private $unitPrice;
    private $currency;
    private $quantity;

    /**
     * Name of the item. 200 characters max.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Name of the item. 200 characters max.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Description of the item. 1000 characters max.
     *
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Description of the item. 1000 characters max.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Quantity of the item. Range of -10000 to 10000.
     *
     * @param string|double $quantity
     *
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * Quantity of the item. Range of -10000 to 10000.
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     *
     * @param Money $price
     * @return $this
     */
    public function setPrice(Money $price)
    {
        $this->unitPrice = $price;
        return $this;
    }

    /**
     *
     * @return \PayPal\Api\Currency
     */
    public function getPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Tax associated with the item.
     *
     * @param Tax $tax
     * @return $this
     */
    public function setTax(Tax $tax)
    {
        $this->tax = $tax;
        return $this;
    }

    /**
     * Tax associated with the item.
     *
     * @return
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param string $date
     *
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * The item discount, as a percent or an amount value.
     *
     * @param \PayPal\Api\Cost $discount
     *
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * The item discount, as a percent or an amount value.
     *
     * @return \PayPal\Api\Cost
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * The unit of measure of the item being invoiced.
     * Valid Values: ["QUANTITY", "HOURS", "AMOUNT"]
     *
     * @return $this
     */
    public function setMeasure($measure)
    {
        $this->measure = $measure;
        return $this;
    }

    /**
     * The unit of measure of the item being invoiced.
     *
     * @return string
     */
    public function getMeasure()
    {
        return $this->measure;
    }

}
