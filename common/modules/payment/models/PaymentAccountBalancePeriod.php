<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.09.18
 * Time: 14:35
 */

namespace common\modules\payment\models;

use common\models\PaymentAccount;
use lib\money\Money;
use lib\money\MoneyMath;

class PaymentAccountBalancePeriod
{
    /** @var PaymentAccount */
    public $paymentAccount;

    /** @var Money */
    public $balanceCurrent;

    /** @var Money */
    public $balanceStart;

    /** @var Money */
    public $balancePlus;

    /** @var Money */
    public $balanceMinus;

    public function getDiff(): Money
    {
        return MoneyMath::sum($this->balanceMinus, $this->balancePlus);
    }

    public function isEmpty(): bool
    {
        return !($this->balanceStart->getAmount() || $this->balanceCurrent || $this->balancePlus->getAmount() || $this->balanceMinus->getAmount());
    }
}