<?php
/**
 * User: nabi
 */

namespace common\modules\payment\models;

use lib\money\Money;

/**
 * Class Fee
 *
 * @package common\modules\payment\models
 */
class Fee
{
    const TYPE_MODEL3D = 'model3d';
    const TYPE_PRINT_CLIENT = 'print_client';
    const TYPE_PRINT_PS = 'print_ps';

    private $type;
    private $amount;

    private function __construct()
    {
    }

    /**
     * @param $type
     * @param Money $amount
     * @return Fee
     */
    public static function create($type, Money $amount)
    {
        $self = new Fee();
        $self->type = $type;
        $self->amount = $amount;
        return $self;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }
}