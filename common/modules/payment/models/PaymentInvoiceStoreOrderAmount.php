<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.10.18
 * Time: 15:23
 */

namespace common\modules\payment\models;

use common\models\PaymentInvoice;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;

class PaymentInvoiceStoreOrderAmount
{
    /** @var PaymentInvoice */
    public $paymentInvoice;

    public function __construct(PaymentInvoice $paymentInvoice)
    {
        $this->paymentInvoice = $paymentInvoice;
    }

    public function getAmountTax()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_TAX);
    }

    public function getAmountFee()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_FEE);
    }

    public function getAmountPackage()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_PACKAGE);
    }

    public function getAmountPackageRefund()
    {
        return $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_PACKAGE);
    }

    public function getAmountPackageWithRefund()
    {
        $amountPrint = $this->getAmountPackage();
        $amountPrintRefund = $this->getAmountPackageRefund();
        return $amountPrint ? MoneyMath::minus($amountPrint, $amountPrintRefund) : null;
    }

    public function getAmountCutting()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_CUTTING);
    }


    public function getAmountPrint()
    {
        if ($this->paymentInvoice->hasAmountType(PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2)) {
            return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2);
        }
        if ($this->paymentInvoice->hasAmountType(PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE)) {
            return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE);
        }
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_PRINT);
    }

    public function getAmountCuttingRefund()
    {
        return $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_CUTTING);
    }

    public function getAmountPrintRefund()
    {
        $amount1 = $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2);
        $amount2 = $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE);
        $amount3= $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_PRINT);
        return MoneyMath::sum($amount1, $amount2, $amount3);
    }

    public function getAmountCuttingWithRefund(): ?Money
    {
        $amountCutting = $this->getAmountCutting();
        $amountCuttingRefund = $this->getAmountCuttingRefund();
        $amountCuttingFeeRefund = $this->getAmountPrintFeeRefund();
        if (!$amountCutting) {
            return null;
        }
        $resultMoney = MoneyMath::minus($amountCutting, $amountCuttingRefund);
        $resultMoney = MoneyMath::minus($resultMoney, $amountCuttingFeeRefund);
        return $resultMoney;
    }

    public function getAmountPrintWithRefund(): ?Money
    {
        $amountPrint = $this->getAmountPrint();
        $amountPrintRefund = $this->getAmountPrintRefund();
        $amountPrintFeeRefund = $this->getAmountPrintFeeRefund();
        if (!$amountPrint) {
            return null;
        }
        $resultMoney = MoneyMath::minus($amountPrint, $amountPrintRefund);
        $resultMoney = MoneyMath::minus($resultMoney, $amountPrintFeeRefund);
        return $resultMoney;
    }

    public function getAmountPrintFeeRefund(): Money
    {
        return $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_PS_FEE);
    }

    public function getAmountPsFeeWithRefund(): ?Money
    {
        $amountPrintFee = $this->getAmountPsFee();

        if (!$amountPrintFee) {
            return null;
        }

        $amountPrintFeeRefund = $this->getAmountPrintFeeRefund();

        if (MoneyMath::less($amountPrintFee, $amountPrintFeeRefund)) {
            return Money::zero($this->paymentInvoice->currency);
        }

        return $amountPrintFee ? MoneyMath::minus($amountPrintFee, $amountPrintFeeRefund) : null;
    }

    public function getAmountCuttingForPs()
    {
        $priceForCustomer = $this->getAmountCutting();
        if ($priceForCustomer === null) {
            return null;
        }
        $printFee = $this->getAmountPsFee();
        if ($printFee === null) {
            return $priceForCustomer;
        }
        return $priceForCustomer ? MoneyMath::minus($priceForCustomer, $printFee) : null;
    }

    public function getAmountPrintForPs()
    {
        $priceForCustomer = $this->getAmountPrint();
        if ($priceForCustomer === null) {
            return null;
        }
        $printFee = $this->getAmountPsFee();
        if ($printFee === null) {
            return $priceForCustomer;
        }
        return $priceForCustomer ? MoneyMath::minus($priceForCustomer, $printFee) : null;
    }

    public function getAmountCuttingForPsWithRefund(): ?Money
    {
        $priceForCustomer = $this->getAmountCuttingWithRefund();

        if ($priceForCustomer === null) {
            return null;
        }

        $psFee = $this->getAmountPsFeeWithRefund();

        if ($psFee === null) {
            return $priceForCustomer;
        }

        if (MoneyMath::less($priceForCustomer, $psFee)) {
            return Money::zero($this->paymentInvoice->currency);
        }

        return $priceForCustomer ? MoneyMath::minus($priceForCustomer, $psFee) : null;
    }

    public function getAmountPrintForPsWithRefund(): ?Money
    {
        $priceForCustomer = $this->getAmountPrintWithRefund();

        if ($priceForCustomer === null) {
            return null;
        }

        $psFee = $this->getAmountPsFeeWithRefund();

        if ($psFee === null) {
            return $priceForCustomer;
        }

        if (MoneyMath::less($priceForCustomer, $psFee)) {
            return Money::zero($this->paymentInvoice->currency);
        }

        return $priceForCustomer ? MoneyMath::minus($priceForCustomer, $psFee) : null;
    }

    public function getAmountPsFee()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_PS_FEE);
    }

    public function getAmountShipping()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_SHIPPING);
    }

    public function getAmountShippingRefund()
    {
        return $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_SHIPPING);
    }

    public function getAmountShippingWithRefund(): ?Money
    {
        $shipping = $this->getAmountShipping();
        $refund = $this->getAmountShippingRefund();
        return $shipping ? MoneyMath::minus($shipping, $refund) : null;
    }

    public function getAmountPostPrint()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_POSTPRINT);
    }

    public function getAmountAddon()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_ADDON);
    }

    public function getAmountDiscount()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_DISCOUNT);
    }

    public function getAmountModel3d()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_MODEL);
    }

    public function getAmountModel3dRefund()
    {
        return $this->paymentInvoice->getAmountRefundByTypeAndItem(PaymentInvoice::ACCOUNTING_TYPE_MODEL);
    }

    public function getAmountModel3dWithRefund(): ?Money
    {
        $model3d = $this->getAmountModel3d();
        $refund = $this->getAmountModel3dRefund();
        return $model3d ? MoneyMath::minus($model3d, $refund) : null;
    }

    public function getAmountModel3dFee(): ?Money
    {
        $award = $this->getAmountModel3dWithRefund();
        $totalFee = $fee = $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_MODEL_FEE);
        if (!$award || !$totalFee || ($award->getAmount() < $fee->getAmount())) {
            return Money::zero();
        };
        return $totalFee;
    }

    public function getAmountTransaction():?Money
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_TRANSACTION);
    }

    public function getAmountProduct()
    {
        return $this->paymentInvoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_PS_PRODUCT);
    }
}