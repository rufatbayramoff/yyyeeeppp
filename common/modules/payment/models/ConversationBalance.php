<?php
namespace common\modules\payment\models;

use common\models\User;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;

class ConversationBalance
{
    public string $type;
    public array $balances;
}