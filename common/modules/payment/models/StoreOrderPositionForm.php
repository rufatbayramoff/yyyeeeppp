<?php

namespace common\modules\payment\models;

use Cassandra\Date;
use common\components\BaseForm;
use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use common\modules\payment\services\StoreOrderPositionService;
use lib\money\Currency;
use lib\money\Money;
use common\modules\payment\fee\FeeHelper;
use yii\web\UploadedFile;

/**
 * User: nabi
 */
class StoreOrderPositionForm extends BaseForm
{

    /**
     * @var StoreOrderPosition
     */
    public $orderPosition;
    /**
     * @var UploadedFile[]
     */
    public $files ;
    public $title;
    public $user_id;
    public $order_id;
    public $amount;
    public $currency;
    private $file_ids;

    /**
     * @var StoreOrderPositionService
     */
    private $positionService;

    public function injectDependencies(StoreOrderPositionService $positionService)
    {
        $this->positionService = $positionService;
    }

    public function init()
    {
        parent::init();
        if(empty($this->positionService)){
            $this->positionService = \Yii::createObject(StoreOrderPositionService::class);
        }
        $this->orderPosition = new StoreOrderPosition();
        $this->user_id = \Yii::$app->user->id;
    }

    public function rules()
    {
        return [
            [['title', 'amount', 'order_id', 'user_id'], 'required'],
            [['user_id', 'order_id'], 'integer'],
            [['title'], 'string', 'max' => 245],
            [['amount'], 'number'],
            [['files'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 15],
        ];
    }
    public function attributeLabels()
    {
        return [
            'title' => \Yii::t('site.order', 'Title'),
            'files' => \Yii::t('site.order', 'Files'),
            'amount' => \Yii::t('site.order', 'Amount'),
        ];
    }
    public function createPosition()
    {
        $this->files = UploadedFile::getInstances($this, 'files');
        $this->processFiles();

        $this->orderPosition->load(
            array_filter(
                [
                    'user_id'    => $this->user_id,
                    'order_id'   => $this->order_id,
                    'title'      => $this->title,
                    'file_ids'   => $this->file_ids,
                    'created_at' => DateHelper::now(),
                    'updated_at' => DateHelper::now(),
                    'status'     => StoreOrderPosition::STATUS_NEW,
                    'amount'     => $this->amount,
                    'currency'   => $this->currency
                ]
            ),
            ''
        );
        return $this->orderPosition;
    }

    private function processFiles()
    {
        $fileRepository = \Yii::createObject(FileRepository::class);
        $fileFactory = \Yii::createObject(FileFactory::class);
        if (empty(array_filter($this->files))) {
            return;
        }
        $fileIds = [];
        foreach ($this->files as $uploadedFile) {
            $file = $fileFactory->createFileFromUploadedFile($uploadedFile);
            $file->setPublicMode(true);
            $fileRepository->save($file);
            $fileIds[] = $file->id;
        }
        $this->file_ids = $fileIds;
    }

    /**
     * @param $amount
     * @return mixed
     */
    public function calculateFeeByAmount($amount)
    {
        // $trasactionFee = $this->amount * 0.03 + 0.30; // this fee for braintree
        $ourFee = $amount * 0.15;
        $fee = max($ourFee, 0.75);
        return $fee;
    }
    /**
     * rule
     * 15% fee, or transaction fee + 0.50 if higher
     *
     * @return mixed
     */
    private function calculateFee()
    {
        return $this->calculateFeeByAmount($this->amount);
    }

    public function calculateIncludeFee()
    {
        $feeHelper = \Yii::createObject(FeeHelper::class);
        $feePrice = $feeHelper->getManufacturingFee(Money::create($this->amount, $this->currency));
        return $feePrice->getAmount();
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @return bool
     */
    public function canAdd(StoreOrderAttemp $attempt)
    {
        return $this->positionService->canAddPositionToAttempt($attempt);
    }
}