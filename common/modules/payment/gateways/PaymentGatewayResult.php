<?php
/**
 * User: nabi
 */

namespace common\modules\payment\gateways;

use common\modules\payment\exception\TransactionNotFoundException;

/**
 * Class PaymentGatewayResult
 * wrapper for gateway result.
 * returned by void, settle,refund and etc. functions
 *
 * @package common\modules\payment\gateways
 */
class PaymentGatewayResult
{
    /**
     * @var PaymentGatewayTransaction
     */
    private $transaction;

    /**
     * @var boolean
     */
    private $success;

    /**
     * @var string
     */
    private $message;

    /**
     * PaymentGatewayResult constructor.
     *
     * @param PaymentGatewayTransaction $transaction
     * @param bool $success
     * @param string $message
     */
    public function __construct(PaymentGatewayTransaction $transaction = null, $success = true, $message = '')
    {
        $this->transaction = $transaction;
        $this->success = $success;
        $this->message = $message;
    }

    /**
     * is result successful,
     * if so, transaction object should not be null
     *
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * can hold message about error
     * if ok, usually empty
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getResultAsArray()
    {
        return [
            'success'     => $this->success,
            'message'     => $this->message,
            'transaction' => $this->transaction
        ];
    }

    /**
     * @return PaymentGatewayTransaction
     * @throws TransactionNotFoundException
     */
    public function getTransaction(): PaymentGatewayTransaction
    {
        if(!$this->transaction) {
            throw new TransactionNotFoundException();
        }
        return $this->transaction;
    }
}