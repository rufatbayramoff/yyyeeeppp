<?php
/**
 * User: nabi
 */

namespace common\modules\payment\gateways;


use common\models\PaymentTransaction;
use lib\money\Money;

/**
 * Class PaymentGatewayTransaction
 *
 * wrapper  - standard transaction returned by any vendor gateway.
 * $originalTransaction - can be used as reference to original transaction from gateway
 *
 * @package common\modules\payment\gateways
 */
class PaymentGatewayTransaction
{
    /**
     * transaction vendor
     *
     * @var string
     */
    public $vendor;
    /**
     *
     * @var string
     */
    public $transactionId;

    /**
     * transaction status
     *
     * @var string
     */
    public $status;

    /**
     * @var Money
     */
    public $amount;

    /**
     * @var array|null
     */
    public $details;

    /**
     *
     * @var string
     */
    public $createdAt;

    /**
     * @var string
     */
    public $updatedAt;

    /**
     * is there refund for this transaction?
     *
     * @var string
     */
    public $refundId;

    /**
     * @var PaymentTransaction
     */
    public $refundTransaction;

    /**
     * original transaction object (braintree, stripe and etc.)
     *
     * @var object
     */
    public $originalTransaction;

    /**
     * payment details.
     * can contain credit card last 4 digits, payment method (paypal, credit and etc.)
     * also can include order details (like order id if payed by invoice and Paypal)
     * @see https://developers.braintreepayments.com/reference/request/transaction/sale/php#order_id
     *
     * @var array
     */
    public $paymentDetails;

    /**
     * customer details
     * can contain customer_id
     *
     * @var array
     */
    public $customerDetails;

    const STATUS_AUTHORIZED = 'authorized';
    const STATUS_AUTHORIZATION_EXPIRED = 'authorization_expired';
    const STATUS_SUCCEEDED = 'succeeded';
    const STATUS_PAID = 'paid';
    const STATUS_VOIDED = 'voided';
    const STATUS_SETTLED = 'settled';
    const STATUS_REFUNDED = 'refunded';
    const STATUS_REFUND_REQUESTED = 'refund_requested'; // our status to track
    const STATUS_SUBMITED_SETTLE = 'submitted_for_settlement';
    const STATUS_SETTLING = 'settling';

}