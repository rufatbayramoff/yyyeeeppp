<?php
/**
 * User: nabi
 */

namespace common\modules\payment\gateways;

use common\modules\payment\components\PaymentCriteria;
use common\modules\payment\gateways\vendors\BraintreeGateway;
use common\modules\payment\gateways\vendors\BankInvoiceGateway;
use common\modules\payment\gateways\vendors\StripeGateway;
use common\modules\payment\gateways\vendors\ThingiversePaymentGateway;
use common\modules\payment\gateways\vendors\TreatstockBalanceGateway;

/**
 * Class PaymentGatewayProvider
 *
 * set and get providers available for payment
 *
 *
 * @package common\modules\payment\gateways
 */
class PaymentGatewayProvider
{

    public $gateways = [
        PaymentGateway::GATEWAY_TS          => TreatstockBalanceGateway::class,
        PaymentGateway::GATEWAY_THINGIVERSE => ThingiversePaymentGateway::class,
        PaymentGateway::GATEWAY_BRAINTREE   => BraintreeGateway::class,
        PaymentGateway::GATEWAY_INVOICE     => BankInvoiceGateway::class,
        PaymentGateway::GATEWAY_STRIPE      => StripeGateway::class,
    ];

    /**
     * maybe move to gateway itself as isLocal for InternalGateway?
     *
     * @var array
     */
    public $updateRequired = [PaymentGateway::GATEWAY_BRAINTREE, PaymentGateway::GATEWAY_STRIPE];

    /**
     * PaymentGatewayProvider constructor.
     */
    public function __construct()
    {
    }

    public function isPaymentTransactionUpdateRequired(PaymentGateway $gateway)
    {
        if (in_array($gateway->getVendorCode(), $this->updateRequired)) {
            return true;
        }
        return false;
    }

    /**
     * get list of gateways
     *
     * @return array
     */
    public function getGatewaysNames()
    {
        return array_keys($this->gateways);
    }

    /**
     * get gateway by code (referred as vendor)
     *
     * @param $code
     * @return PaymentGateway|null
     */
    public function getGatewayByCode($code)
    {
        if (in_array($code, $this->getGatewaysNames())) {
            return new $this->gateways[$code];
        }
        return null;
    }

    /**
     * alias function
     *
     * @see self::getGatewayByCode
     * @param $vendor
     * @return PaymentGateway|null
     */
    public function getGatewayByVendor($vendor)
    {
        return $this->getGatewayByCode($vendor);
    }

    /**
     * Get available payment gateways by criteria
     *
     * @param PaymentCriteria $criteria
     * @return array
     */
    public function getGatewaysNamesByCriteria(PaymentCriteria $criteria): array
    {
        $result = $this->getGatewaysNames();

        if ($criteria->isThingiverse()) {
            $result = [PaymentGateway::GATEWAY_THINGIVERSE];
        }
        if (!$criteria->canPayByInvoice()) {
            unset($result[array_search(PaymentGateway::GATEWAY_INVOICE, $result)]);
        }
        $balance = $criteria->getTreatstockBalance();
        $paymentAmount = $criteria->getPaymentAmount();
        if ($paymentAmount && $balance && $balance->getAmount() < $paymentAmount->convertTo('USD')->getAmount()) {
            unset($result[array_search(PaymentGateway::GATEWAY_TS, $result)]);
        }
        return $result;
    }
}