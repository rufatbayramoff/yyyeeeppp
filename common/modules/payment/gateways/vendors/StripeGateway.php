<?php

namespace common\modules\payment\gateways\vendors;

use common\components\DateHelper;
use common\models\PaymentLog;
use common\models\PaymentTransaction;
use common\models\User;
use common\modules\payment\components\PaymentLogger;
use common\modules\payment\exception\FatalPaymentException;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayResult;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use lib\money\Currency;
use lib\money\Money;
use lib\payment\exception\PaymentManagerException;
use Yii;
use yii\base\BaseObject;

/**
 * Class StripeGateway
 *
 * @package common\modules\payment\gateways\vendors
 */
class StripeGateway extends BaseObject implements PaymentGateway
{
    public $code = 'stripe';
    public static $inited = false;

    /** @var PaymentLogger */
    public $paymentLogger;

    /** @var \Stripe\StripeClient */
    protected $stripe;

    public const METHOD_TYPE_ALIPAY = 'alipay';
    public const METHOD_TYPE_CARD   = 'card';

    public const CAPTURE_METHOD_AUTOMATIC = 'automatic';
    public const CAPTURE_METHOD_MANUAL    = 'manual';

    /** @var array */
    public $paymentMethods = [self::METHOD_TYPE_CARD];

    /** @var string */
    public $captureMethod = self::CAPTURE_METHOD_MANUAL;

     /**
     * init config
     *
     * @param array $params
     */
    public function init(array $params = [])
    {
        parent::init();
        $this->paymentLogger = Yii::createObject(PaymentLogger::class);
        $this->stripe = new \Stripe\StripeClient(
            app()->params['stripe_private_key']
        );
        self::$inited = true;
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $successUrl
     * @param string $cancelUrl
     * @param Money $amount
     * @return string
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function generatePaymentToken(string $title, string $description, string $successUrl, string $cancelUrl, Money $amount): string
    {
        $paymentIntent = $this->stripe->paymentIntents->create(
            [
                'amount'               => round($amount->getAmount() * 100),
                'currency'             => $amount->getCurrency(),
                'payment_method_types' => $this->paymentMethods,
                'capture_method'       => $this->captureMethod,
            ]
        );
        $this->paymentLogger->log(PaymentLogger::TYPE_STRIPE, 'GeneratePaymentToken', $paymentIntent,
            [
                PaymentLogger::MARKER_AMOUNT                => $amount->getAmount(),
                PaymentLogger::MARKER_PM_NONCE              => $this->paymentMethods,
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $paymentIntent->id,
            ], PaymentLogger::LEVEL_INFO);
        return $paymentIntent->client_secret;
    }

    /**
     * create transaction - pay
     *
     * @param Money $money
     * @param string $paymentToken
     * @return PaymentGatewayResult
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function authorize(Money $money, $paymentToken = ''): PaymentGatewayResult
    {
        $paymentIntentId = $paymentToken['paymentIntent'];
        $paymentIntent   = $this->stripe->paymentIntents->retrieve($paymentIntentId);
        if ($paymentIntent->amount != round($money->getAmount() * 100)) {
            throw  new FatalPaymentException('Invalid payment session sum');
        }
        if (!in_array($paymentIntent->status, [\Stripe\PaymentIntent::STATUS_SUCCEEDED, \Stripe\PaymentIntent::STATUS_REQUIRES_CAPTURE])) {
            throw  new FatalPaymentException('Invalid payment status:' . $paymentIntent->status);
        }
        $email = $paymentIntent->customer->email ?? '';
        $this->paymentLogger->log(PaymentLogger::TYPE_STRIPE, 'Authorize', $paymentIntent,
            [
                PaymentLogger::MARKER_AMOUNT                => $money->getAmount(),
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $paymentIntentId,
            ], PaymentLogger::LEVEL_INFO);

        return $this->adaptResult($paymentIntent);
    }


    /**
     * void transaction
     *
     * @param string $transactionId
     * @return PaymentGatewayResult
     * @throws \yii\base\Exception
     */
    public function void($transactionId): PaymentGatewayResult
    {
        $paymentIdent = $this->stripe->paymentIntents->retrieve($transactionId);
        if (!$paymentIdent) {
            throw new  FatalPaymentException('Invalid payment ident');
        }
        $paymentIdent->cancel();
        $this->paymentLogger->log(PaymentLogger::TYPE_STRIPE, 'Void', $paymentIdent,
            [
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $transactionId,
            ], PaymentLogger::LEVEL_INFO
        );
        return $this->adaptResult($paymentIdent);
    }

    /**
     * @param string $transactionId
     * @param int $amount
     * @return PaymentGatewayResult
     * @throws \yii\base\Exception
     */
    public function refund($transactionId, $amount = null): PaymentGatewayResult
    {
        $params = [
            'payment_intent' => $transactionId
        ];
        if ($amount) {
            $params['amount'] = $amount * 100;
        }
        $paymentLogger = \Yii::createObject(PaymentLogger::class);
        $resp          = $this->stripe->refunds->create($params);
        $paymentLogger->log(PaymentLogger::TYPE_BRAINTRE, 'Refund', $resp,
            [
                PaymentLogger::MARKER_AMOUNT                => $amount,
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $transactionId,
            ], PaymentLogger::LEVEL_INFO
        );

        return $this->adaptResult($resp);
    }

    /**
     * settle payment
     *
     * @param string $transactionId
     * @param float $amount
     * @return PaymentGatewayResult
     * @throws \yii\base\Exception
     */
    public function settle($transactionId, $amount = null): PaymentGatewayResult
    {
        $paymentIntent = $this->stripe->paymentIntents->retrieve($transactionId);
        if ($paymentIntent->status != \Stripe\PaymentIntent::STATUS_REQUIRES_CAPTURE) {
            throw  new FatalPaymentException('Invalid payment status:' . $paymentIntent->status);
        }
        $paymentIntent->capture(['amount_to_capture' => $paymentIntent->amount]);
        $email = $paymentIntent->customer->email ?? '';


        $this->paymentLogger->log(PaymentLogger::TYPE_STRIPE, 'Settle', $paymentIntent,
            [
                PaymentLogger::MARKER_AMOUNT                => $amount,
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $transactionId,
            ], PaymentLogger::LEVEL_INFO);

        return $this->adaptResult($paymentIntent);
    }


    /**
     * get vendor code
     *
     * @return string
     */
    public function getVendorCode()
    {
        return $this->code;
    }

    /**
     * get transaction by id
     *
     * @param int $id
     * @return PaymentGatewayTransaction
     * @throws PaymentManagerException
     * @throws \yii\base\Exception
     */
    public function getTransaction($transactionId): PaymentGatewayTransaction
    {
        if ((strpos($transactionId, 'pyr_') === 0) || (strpos($transactionId, 're_') === 0)) {
            $paymentIdent = $this->stripe->refunds->retrieve($transactionId);
        } else {
            $paymentIdent = $this->stripe->paymentIntents->retrieve($transactionId);
        }
        return $this->adaptTransaction($paymentIdent);
    }

    /**
     * @param PaymentGatewayTransaction[]
     */
    public function getTransactions($transactionIds)
    {
        $gatewayTransactions = [];
        foreach ($transactionIds as $transactionId) {
            $gatewayTransactions[] = $this->getTransaction($transactionId);
        }
        return $gatewayTransactions;
    }

    /**
     * @param \Stripe\PaymentIntent|\Stripe\Refund $paymentIntent
     * @return PaymentGatewayTransaction
     */
    protected function adaptTransaction($paymentIntent): PaymentGatewayTransaction
    {
        $gatewayTransaction                      = new PaymentGatewayTransaction();
        $gatewayTransaction->vendor              = PaymentTransaction::VENDOR_STRIPE;
        $gatewayTransaction->transactionId       = $paymentIntent->id;
        $gatewayTransaction->status              = $paymentIntent->status;
        $gatewayTransaction->amount              = Money::create($paymentIntent->amount / 100, $paymentIntent->currency ?: Currency::USD);
        $gatewayTransaction->createdAt           = date('Y-m-d H:i:s', $paymentIntent->created);
        $gatewayTransaction->updatedAt           = $gatewayTransaction->createdAt;
        $gatewayTransaction->originalTransaction = $paymentIntent;
        $gatewayTransaction->refundId            = false;
        return $gatewayTransaction;
    }

    /**
     * adapt braintree result to PaymentGatewayResult
     *
     * @param \Stripe\PaymentIntent|\Stripe\Refund|null $result
     * @return PaymentGatewayResult
     */
    private function adaptResult($result = null)
    {
        $success     = false;
        $transaction = null;
        $message     = '';
        if ($result) {
            $success     = true;
            $transaction = $this->adaptTransaction($result);
        }
        return new PaymentGatewayResult($transaction, $success, $message);
    }

    /**
     * is gateway internal? for invoice, treatstock balance, thingiverse gateways
     * for braintree,stripe - false
     *
     * @return mixed
     */
    public function isInternalGateway()
    {
        return false;
    }


    public function getLogger(): PaymentLogger
    {
        return $this->paymentLogger;
    }
}
