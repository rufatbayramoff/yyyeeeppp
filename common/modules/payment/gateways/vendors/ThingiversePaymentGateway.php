<?php
/**
 * User: nabi
 */

namespace common\modules\payment\gateways\vendors;


use common\models\User;
use common\modules\payment\gateways\PaymentGatewayResult;
use common\modules\thingPrint\components\ThingiverseFacade;
use yii\helpers\Html;

class ThingiversePaymentGateway extends InternalGateway
{
    public $code = 'thingiverse';
}