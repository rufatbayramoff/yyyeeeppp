<?php
/**
 * User: nabi
 */

namespace common\modules\payment\gateways\vendors;

use common\models\PaymentTransaction;
use common\models\User;

class BankPayoutGateway extends InternalGateway
{
    public $code = PaymentTransaction::VENDOR_BANK_PAYOUT;


    public const ACCOUNT_DETAILS = 'accountDetails';

}