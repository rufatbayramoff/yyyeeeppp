<?php
/**
 *
 * @author Nabi <n.ibatulin@treatstock.com>
 */
namespace common\modules\payment\gateways\vendors;

use common\modules\payment\gateways\PaymentGateway;

/**
 * Class BankInvoiceGateway
 * helps to pay using invoice
 *
 * @package common\modules\payment\gateways
 */
class BankInvoiceGateway extends InternalGateway
{
    public $code = PaymentGateway::GATEWAY_INVOICE;
}