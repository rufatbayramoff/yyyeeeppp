<?php
/**
 * User: nabi
 */

namespace common\modules\payment\gateways\vendors;

use common\models\User;

class PayPalPayoutGateway extends InternalGateway
{
    public $code = 'pp_payout';

    /**
     * init gateway params to access
     *
     * @param array $args
     */
    public function init(array $args = [])
    {
        parent::init();

    }
}