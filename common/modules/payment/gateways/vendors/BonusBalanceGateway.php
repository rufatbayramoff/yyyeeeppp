<?php
namespace common\modules\payment\gateways\vendors;

use common\components\DateHelper;
use common\models\PaymentTransaction;
use common\models\User;
use common\modules\payment\gateways\PaymentGatewayResult;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use lib\money\Money;

class BonusBalanceGateway extends InternalGateway
{
    public $code = PaymentTransaction::VENDOR_BONUS;

    /**
     * @param Money $money
     * @param string $paymentToken
     * @return mixed
     */
    public function authorize(Money $money, $paymentToken = ''): PaymentGatewayResult
    {
        $result  = true;
        $payment = [
            'amount'         => $money,
            'status'         => PaymentGatewayTransaction::STATUS_SETTLED,
            'details'        => $this->details,
            'transaction_id' => (string)time(),
            'created_at'     => DateHelper::now()
        ];
        $pt      = new PaymentGatewayResult($this->adaptTransaction($payment), $result, '');
        return $pt;
    }
}