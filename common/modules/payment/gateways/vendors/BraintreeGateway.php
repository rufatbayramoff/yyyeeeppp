<?php

namespace common\modules\payment\gateways\vendors;

use Braintree\Test\Transaction;
use common\models\PaymentLog;
use common\models\PaymentTransaction;
use common\models\User;
use common\modules\payment\components\PaymentLogger;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayResult;
use common\modules\payment\gateways\PaymentGatewayTransaction;

use lib\money\Currency;
use lib\money\Money;
use lib\payment\exception\PaymentManagerException;
use Yii;
use yii\base\BaseObject;
use yii\helpers\VarDumper;

/**
 * Class BraintreeGateway
 *
 * @package common\modules\payment\gateways\vendors
 */
class BraintreeGateway extends BaseObject implements PaymentGateway
{
    public $code = 'braintree';

    /** @var PaymentLogger */
    public $paymentLogger;

    /** @var \Braintree\Gateway */
    protected $gateway;


    /**
     * init config
     *
     * @param array $params
     */
    public function init(array $params = [])
    {
        parent::init();;
        $this->paymentLogger = Yii::createObject(PaymentLogger::class);
        if (empty($params)) {
            $params = [
                'braintree_env'      => app()->params['braintree_env'],
                'braintree_merchant' => app()->params['braintree_merchant'],
                'braintree_public'   => app()->params['braintree_public'],
                'braintree_private'  => app()->params['braintree_private'],
            ];
        }
        // Instantiate a Braintree Gateway either like this:
        $this->gateway = new \Braintree\Gateway([
            'environment' => $params['braintree_env'],
            'merchantId'  => $params['braintree_merchant'],
            'publicKey'   => $params['braintree_public'],
            'privateKey'  => $params['braintree_private']
        ]);
    }


    /**
     * @param string $title
     * @param string $description
     * @param string $successUrl
     * @param string $cancelUrl
     * @param Money $amount
     * @return string
     */
    public function generatePaymentToken(string $title, string $description, string $successUrl, string $cancelUrl, Money $amount): string
    {
        $merchantAccountId = $this->getAccountByCurrency($amount->getCurrency());
        return $this->gateway->clientToken()->generate(['merchantAccountId' => $merchantAccountId]);
    }

    /**
     * create transaction - pay
     *
     * @param Money $money
     * @param string $paymentToken
     * @return PaymentGatewayResult
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     */
    public function authorize(Money $money, $paymentToken): PaymentGatewayResult
    {
        $accountId = self::getAccountByCurrency($money->getCurrency());
        $result    = $this->gateway->transaction()->sale(
            [
                'amount'             => $money->getAmount(),
                //'channel'            => 'TSMarketPlace',
                'merchantAccountId'  => $accountId,
                'paymentMethodNonce' => $paymentToken,
            ]
        );

        $this->paymentLogger->log(PaymentLogger::TYPE_BRAINTRE, 'Authorize', $result,
            [
                PaymentLogger::MARKER_AMOUNT                => $money->getAmount(),
                PaymentLogger::MARKER_MERCHANT_ACCOUNT_ID   => $accountId,
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $paymentToken,
            ], PaymentLogger::LEVEL_INFO);

        return $this->adaptResult($result);
    }

    /**
     * @param Money $money
     * @param string $transactionId
     * @return PaymentGatewayResult
     * @throws \yii\base\Exception
     */
    public function authorizeWithSubmit(Money $money, $transactionId = '')
    {
        $accountId = self::getAccountByCurrency($money->getCurrency());
        $result    = $this->gateway->transaction()->sale(
            [
                'amount'             => $money->getAmount(),
                'channel'            => 'TSMarketPlace',
                'merchantAccountId'  => $accountId,
                'paymentMethodNonce' => $transactionId,
                'options'            => [
                    'submitForSettlement' => true
                ]
            ]
        );
        $this->paymentLogger->log(PaymentLogger::TYPE_BRAINTRE, 'Authorize with submit', $result,
            [
                PaymentLogger::MARKER_AMOUNT                => $money->getAmount(),
                PaymentLogger::MARKER_MERCHANT_ACCOUNT_ID   => $accountId,
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $transactionId,
            ], PaymentLogger::LEVEL_INFO);

        return $this->adaptResult($result);
    }

    /**
     * void transaction
     *
     * @param string $transactionId
     * @return PaymentGatewayResult
     * @throws \yii\base\Exception
     */
    public function void($transactionId): PaymentGatewayResult
    {
        $resp = $this->gateway->transaction()->void($transactionId);
        $this->paymentLogger->log(PaymentLogger::TYPE_BRAINTRE, 'Void', $resp,
            [
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $transactionId,
            ], PaymentLogger::LEVEL_INFO);

        return $this->adaptResult($resp);
    }

    /**
     * @param string $transactionId
     * @param int $amount
     * @return PaymentGatewayResult
     * @throws \yii\base\Exception
     */
    public function refund($transactionId, $amount = null): PaymentGatewayResult
    {
        $paymentLogger = \Yii::createObject(PaymentLogger::class);
        $resp          = $this->gateway->transaction()->refund($transactionId, $amount);
        $paymentLogger->log(PaymentLogger::TYPE_BRAINTRE, 'Refund', $resp,
            [
                PaymentLogger::MARKER_AMOUNT                => $amount,
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $transactionId,
            ], PaymentLogger::LEVEL_INFO);

        return $this->adaptResult($resp);
    }

    /**
     * settle payment
     *
     * @param string $transactionId
     * @param float $amount
     * @return PaymentGatewayResult
     * @throws \yii\base\Exception
     */
    public function settle($transactionId, $amount = null): PaymentGatewayResult
    {
        $resp = $this->gateway->transaction()->submitForSettlement($transactionId, $amount);

        $this->paymentLogger->log(PaymentLogger::TYPE_BRAINTRE, 'Submit for settle', $resp,
            [
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $transactionId,
                PaymentLogger::MARKER_AMOUNT                => $amount,
            ], PaymentLogger::LEVEL_INFO);

        return $this->adaptResult($resp);
    }


    /**
     * get merchant account by currency
     *
     * @param string $currency
     * @return string
     */
    private function getAccountByCurrency($currency)
    {
        $account = param('braintree_default_merchant_' . strtolower($currency), '');
        if (!$account && $currency == Currency::EUR) {
            $account = 'treatstockinc_EUR';
        }
        return $account;
    }


    /**
     * get vendor code
     *
     * @return string
     */
    public function getVendorCode()
    {
        return $this->code;
    }

    /**
     * get transaction by id
     *
     * @param int $id
     * @return PaymentGatewayTransaction
     * @throws PaymentManagerException
     * @throws \yii\base\Exception
     */
    public function getTransaction($id): PaymentGatewayTransaction
    {
        $resp = $this->gateway->transaction()->find($id);
        $this->paymentLogger->log(PaymentLogger::TYPE_BRAINTRE, 'Get transaction', $resp,
            [
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID => $id,
            ], PaymentLogger::LEVEL_INFO);

        if (!$resp->status) {
            throw new PaymentManagerException('Get Transaction Error. ' . $resp->message);
        }
        return $this->adaptTransaction($resp);
    }

    /**
     * @param PaymentGatewayTransaction[]
     */
    public function getTransactions($transactionIds)
    {
        $gatewayTransactions = [];
        $collection          = $this->gateway->transaction()->search([\Braintree\TransactionSearch::ids()->in($transactionIds)]);
        $collectionArray     = json_decode(json_encode($collection), true);
        $this->paymentLogger->log(PaymentLogger::TYPE_BRAINTRE, 'Get transactions', $collectionArray,
            [
                PaymentLogger::MARKER_VENDOR_TRANSACTION_IDS => $transactionIds,
            ], PaymentLogger::LEVEL_INFO);
        foreach ($collection as $braintreeTransaction) {
            $transaction           = $this->adaptTransaction($braintreeTransaction);
            $gatewayTransactions[] = $transaction;
        }
        return $gatewayTransactions;
    }

    /**
     * @param $transaction \Braintree\Transaction
     * @return PaymentGatewayTransaction
     */
    protected function adaptTransaction($transaction): PaymentGatewayTransaction
    {
        $gatewayTransaction                      = new PaymentGatewayTransaction();
        $gatewayTransaction->vendor              = PaymentTransaction::VENDOR_BRAINTREE;
        $gatewayTransaction->transactionId       = $transaction->id;
        $gatewayTransaction->status              = $transaction->status;
        $gatewayTransaction->amount              = Money::create($transaction->amount, $transaction->currencyIsoCode ?: Currency::USD);
        $gatewayTransaction->createdAt           = is_string($transaction->createdAt) ? $transaction->createdAt : $transaction->createdAt->format('Y-m-d H:i:s');
        $gatewayTransaction->updatedAt           = is_string($transaction->updatedAt) ? $transaction->updatedAt : $transaction->updatedAt->format('Y-m-d H:i:s');
        $gatewayTransaction->originalTransaction = $transaction;

        $gatewayTransaction->refundId = $transaction->refundId ? $transaction->refundId : false;
        if ($transaction->refundedTransactionId) {
            $gatewayTransaction->refundId = $transaction->refundedTransactionId;
        }
        return $gatewayTransaction;
    }

    /**
     * adapt braintree result to PaymentGatewayResult
     *
     * @param $result \Braintree\Result\Successful|\Braintree\Result\Error
     * @return PaymentGatewayResult
     * @throws \yii\base\Exception
     */
    private function adaptResult($result)
    {
        $success     = false;
        $transaction = null;
        $message     = '';
        if ($result instanceof \Braintree\Result\Error) {
            $message = !empty($result->message) ? $result->message : (string)$result;
        } else {
            if ($result instanceof \Braintree\Result\Successful) {
                $success = true;
                if (!empty($result->transaction)) {
                    $transaction = $this->adaptTransaction($result->transaction);
                }
            }
        }
        return new PaymentGatewayResult($transaction, $success, $message);
    }

    /**
     * is gateway internal? for invoice, treatstock balance, thingiverse gateways
     * for braintree,stripe - false
     *
     * @return mixed
     */
    public function isInternalGateway()
    {
        return false;
    }

    public function testSettle($transactionId)
    {
        $gate = $this->gateway;
        return $gate->testing()->settle($transactionId);
    }

    public function getLogger(): PaymentLogger
    {
        return $this->paymentLogger;
    }
}