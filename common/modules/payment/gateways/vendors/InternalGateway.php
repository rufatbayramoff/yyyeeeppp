<?php
/**
 * User: nabi
 */

namespace common\modules\payment\gateways\vendors;

use common\components\DateHelper;
use common\models\PaymentLog;
use common\models\PaymentTransaction;
use common\models\User;
use common\modules\payment\components\PaymentLogger;
use common\modules\payment\gateways\PaymentGateway;
use common\modules\payment\gateways\PaymentGatewayResult;
use common\modules\payment\gateways\PaymentGatewayTransaction;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use yii\base\BaseObject;

/**
 * Class InternalGateway
 *
 * superclass for internal vendors, like  treatstock balance, or thingiverse, invoice
 * when we don't call remote API (like braintree, stripe)
 *
 * @package common\modules\payment\gateways\vendors
 */
class InternalGateway extends BaseObject implements PaymentGateway
{
    public $code = 'internal';

    /** @var PaymentLogger */
    public $paymentLogger;

    public $details;

    public function init(array $args = [])
    {
        parent::init();
        $this->paymentLogger = Yii::createObject(PaymentLogger::class);
    }

    /**
     * @param Money $money
     * @param string $paymentToken
     * @return mixed
     */
    public function authorize(Money $money, $paymentToken = ''): PaymentGatewayResult
    {
        $result  = true;
        $payment = [
            'amount'         => $money,
            'status'         => PaymentGatewayTransaction::STATUS_AUTHORIZED,
            'details'        => $this->details,
            'transaction_id' => (string)time(),
            'created_at'     => DateHelper::now()
        ];
        $pt      = new PaymentGatewayResult($this->adaptTransaction($payment), $result, '');
        return $pt;
    }

    /**
     * when order is printed, we settle money to TS account
     *
     * @param $transactionId
     * @return PaymentGatewayResult
     * @throws \yii\web\NotFoundHttpException
     */
    public function settle($transactionId): PaymentGatewayResult
    {
        $inf = $this->getTransaction($transactionId);
        $pt  = new PaymentGatewayResult($inf, true, '');
        return $pt;
    }

    /**
     * if order cancelled before status is printed
     *
     * @param $transactionId
     * @return PaymentGatewayResult
     * @throws \yii\web\NotFoundHttpException
     */
    public function void($transactionId): PaymentGatewayResult
    {
        $inf = $this->getTransaction($transactionId);
        $pt  = new PaymentGatewayResult($inf, true, '');
        return $pt;
    }

    /**
     * if order cancelled after status is printed, and we return just part of money
     * also, check if transaction status allows to refund
     *
     * @param $transactionId
     * @param null $amount
     * @param null $currency
     * @return PaymentGatewayResult
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function refund($transactionId, $amount = null): PaymentGatewayResult
    {
        $transaction      = $this->getTransaction($transactionId);
        $newTransactionId = (string)time();
        $result           = [
            'success'        => true,
            'amount'         => Money::create($amount, $transaction->amount->getCurrency()),
            'details'        => $this->details,
            'status'         => PaymentTransaction::STATUS_SETTLED,
            'transaction_id' => $newTransactionId
        ];
        $inf              = $this->adaptTransaction($result);
        $this->paymentLogger->log(PaymentLogger::TYPE_TREATSTOCK, 'Refund', [],
            [
                PaymentLogger::MARKER_AMOUNT                       => $amount,
                PaymentLogger::MARKER_VENDOR_TRANSACTION_ID        => $transactionId,
                PaymentLogger::MARKER_VENDOR_REFUND_TRANSACTION_ID => $newTransactionId
            ], PaymentLogger::LEVEL_INFO);
        $pt = new PaymentGatewayResult($inf, true, '');
        return $pt;
    }

    /**
     * get current transaction status from vendor using db request
     *
     * @param $transactionId
     * @return PaymentGatewayTransaction
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTransaction($transactionId): PaymentGatewayTransaction
    {
        $paymentTransaction = PaymentTransaction::tryFind(
            [
                'transaction_id' => $transactionId,
                'vendor'         => $this->getVendorCode()
            ],
            sprintf('Transaction %s [%s] not found', $transactionId, $this->code)
        );
        $result             = [
            'success'        => true,
            'amount'         => Money::create($paymentTransaction->amount, $paymentTransaction->currency),
            'status'         => $paymentTransaction->status,
            'payment_id'     => $paymentTransaction->id,
            'transaction_id' => $transactionId,
            'details'        => null,
        ];
        return $this->adaptTransaction($result);
    }

    /**
     * @param $transactions
     * @return PaymentGatewayTransaction[]
     */
    public function getTransactions($transactions)
    {
        return [];
    }

    /**
     * @param $data
     * @return PaymentGatewayTransaction
     */
    protected function adaptTransaction($data)
    {
        $pgt                      = new PaymentGatewayTransaction();
        $pgt->vendor              = $this->getVendorCode();
        $pgt->amount              = $data['amount'];
        $pgt->transactionId       = $data['transaction_id'];
        $pgt->details             = $data['details'];
        $pgt->status              = $data['status'];
        $pgt->originalTransaction = [];
        $pgt->updatedAt           = $pgt->createdAt = $data['created_at'] ?? DateHelper::now();
        return $pgt;
    }

    /**
     * get vendor code
     *
     * @return string
     */
    public function getVendorCode(): string
    {
        return $this->code;
    }

    /**
     * is gateway internal? for invoice, treatstock balance, thingiverse gateways
     * for braintree,stripe - false
     *
     * @return mixed
     */
    public function isInternalGateway()
    {
        return true;
    }

    /**
     * Stub for generate token
     *
     * @param string $title
     * @param string $description
     * @param string $successUrl
     * @param string $cancelUrl
     * @param Money $amount
     * @return string
     */
    public function generatePaymentToken(string $title, string $description, string $successUrl, string $cancelUrl, Money $amount): string
    {
        return '';
    }

    public function getLogger(): PaymentLogger
    {
        return $this->paymentLogger;
    }
}