<?php

namespace common\modules\payment\gateways;

use common\models\PaymentInvoice;
use common\models\PaymentLog;
use common\models\User;
use common\modules\payment\components\PaymentLogger;
use lib\money\Money;

/**
 * User: nabi
 *
 * PaymentGateway
 * all vendors (api for payment) should implement this gateway
 *
 * All API requests MUST go only through Gateway class
 * For each API vendor, create Gateway class  to use
 *
 * @package payment
 */
interface PaymentGateway
{
    const GATEWAY_BRAINTREE = 'braintree';

    /**
     *
     */
    const GATEWAY_STRIPE = 'stripe';

    /**
     * usually with type minus
     */
    const GATEWAY_PAYPAL = 'paypal';

    /**
     * usually with type plus - ts - treatstock
     */
    const GATEWAY_TS = 'ts';

    /**
     * thingiverse
     */
    const GATEWAY_THINGIVERSE = 'thingiverse';

    /**
     * bank invoice payment gateway
     */
    const GATEWAY_INVOICE = 'invoice';


    /**
     * init gateway params to access internal gateway lib
     *
     * @param array $args
     */
    public function init(array $args = []);

    /**
     * @param string $title
     * @param string $description
     * @param string $successUrl
     * @param string $cancelUrl
     * @param Money $amount
     * @return mixed
     */
    public function generatePaymentToken(string $title, string $description, string $successUrl, string $cancelUrl, Money $amount): string;


    /**
     * @param Money $money
     * @param $paymentToken
     * @return PaymentGatewayResult
     */
    public function authorize(Money $money, $paymentToken): PaymentGatewayResult;

    /**
     * when order is printed, we settle money to TS account
     *
     * @param $transactionId
     * @return PaymentGatewayResult
     */
    public function settle($transactionId): PaymentGatewayResult;

    /**
     * if order cancelled before status is printed
     *
     * @param $transactionId
     * @return PaymentGatewayResult
     */
    public function void($transactionId): PaymentGatewayResult;

    /**
     * if order cancelled after status is printed, and we return just part of money
     * also, check if transaction status allows to refund
     *
     * @param $transactionId
     * @param null $amount
     * @return PaymentGatewayResult
     */
    public function refund($transactionId, $amount = null): PaymentGatewayResult;

    /**
     * get current transaction from vendor using API request
     *
     * @param $transactionId
     * @return PaymentGatewayTransaction
     */
    public function getTransaction($transactionId): PaymentGatewayTransaction;

    /**
     * get vendor code
     *
     * @return string
     */
    public function getVendorCode();

    /**
     * is gateway internal? for invoice, treatstock balance, thingiverse gateways
     * for braintree,stripe - false
     *
     * @return mixed
     */
    public function isInternalGateway();

    /**
     * @return PaymentLogger
     */
    public function getLogger(): PaymentLogger;
}