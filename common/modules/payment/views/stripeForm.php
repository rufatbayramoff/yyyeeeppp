<?php

use common\components\JsObjectFactory;
use common\models\PaymentInvoice;
use common\models\PaymentPayPageLog;
use common\models\PaymentPayPageLogStatus;
use common\models\PaymentTransaction;
use common\modules\payment\assets\StripeFormAsset;
use common\modules\payment\services\InvoiceBankService;
use frontend\components\UserSessionFacade;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 06.11.18
 * Time: 16:20
 *
 * @var string $postUrl
 * @var string $clientToken
 * @var string $logUuid
 * @var PaymentInvoice $paymentInvoice
 */

$this->registerAssetBundle(StripeFormAsset::class);

$amountTotal = $paymentInvoice->getAmountTotalWithRefund();
$stripe      = '';
$address     = [];

if ($paymentInvoice->storeOrder && $paymentInvoice->storeOrder->shipAddress) {
    $address = [
        'country'     => $paymentInvoice->storeOrder->shipAddress->country->iso_code,
        'postal_code' => $paymentInvoice->storeOrder->shipAddress->zip_code,
        'state'       => $paymentInvoice->storeOrder->shipAddress->getShortRegionName(),
        'city'        => $paymentInvoice->storeOrder->shipAddress->city,
        'line1'       => mb_substr($paymentInvoice->storeOrder->shipAddress->address, 0, 50),
        'line2'       => mb_substr($paymentInvoice->storeOrder->shipAddress->extended_address, 0, 50)
    ];
} else {
    $addressList = $paymentInvoice->user->userAddresses;
    $userAddress = reset($addressList);
    if ($userAddress && is_object($userAddress)) {
        $address = [
            'country'     => $userAddress->country?->iso_code ?? 'US',
            'postal_code' => $userAddress->zip_code ?? '',
            'state'       => $userAddress->getShortRegionName() ?? 'CA',
            'city'        => $userAddress->city,
            'line1'       => $userAddress->address,
            'line2'       => $userAddress->extended_address
        ];
    } else {
        $location = UserSessionFacade::getLocation();
        $address = [
            'country'     => $location?->country?? 'US',
            'postal_code' => '',
            'state'       => $location->region ?? 'CA',
            'city'        => $location->city,
            'line1'       => '',
            'line2'       => ''
        ];
    }
}

PaymentPayPageLog::register($logUuid, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $paymentInvoice->uuid);

$clientName = $paymentInvoice->user->userProfile->full_name;
if (!$clientName) {
    $clientName = $paymentInvoice?->storeOrder?->shipAddress?->first_name;
    if ($clientName && $paymentInvoice?->storeOrder?->shipAddress?->last_name) {
        $clientName .= ' ' . $paymentInvoice?->storeOrder?->shipAddress?->last_name;
    }
}

$clientEmail = $paymentInvoice->user->email;
if (!$clientEmail && $paymentInvoice?->storeOrder?->shipAddress?->email) {
    $clientEmail = $paymentInvoice->storeOrder->shipAddress->email;
}
$clientPhone = $paymentInvoice->user->userProfile->phone;
if (!$clientPhone && $paymentInvoice?->storeOrder?->shipAddress?->phone) {
    $clientPhone = $paymentInvoice->storeOrder->shipAddress->phone;
}

JsObjectFactory::createJsObject(
    'stripeFormClass',
    'stripeFormObj',
    [
        'publicKey'   => app()->params['stripe_pub_key'],
        'clientToken' => $clientToken,
        'invoiceUuid' => $paymentInvoice->uuid,
        'logUuid'     => $logUuid,
        'address'     => $address,
        'clientName'  => $clientName,
        'clientEmail' => $clientEmail,
        'clientPhone' => $clientPhone,
        'successUrl'  => \common\modules\payment\processors\StripeProcessor::getSuccessUrl($paymentInvoice, $logUuid),
        'failed'      => \common\modules\payment\processors\StripeProcessor::getCancelUrl($paymentInvoice, $logUuid)
    ]
    ,
    $this
);
?>

<form id="stripe-payment-form" method="post" action="<?php echo \yii\helpers\Url::toRoute([$postUrl]); ?>" style="">
    <input type="hidden" name="vendor" value="<?= \common\models\PaymentTransaction::VENDOR_STRIPE; ?>"/>
    <input type="hidden" name="payment_invoice" value="<?= $paymentInvoice->uuid; ?>"/>
    <input type="hidden" name="payment_method_nonce" id="payment_method_nonce">
    <input type="hidden" name="logUuid" value="<?= $logUuid ?>">
    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

    <div id="stripe-payment-form-3ds"></div>
    <div id="stripe-payment-block" class="hidden"></div>
    <div class="row" style="padding-top: 15px">
        <div id="stripe-payment-loading-msg" style="text-align: center;margin: 0 0 30px;"><img
                    src="/static/images/loading.gif"> <?= _t('site.payment', 'loading...'); ?></div>
        <div id="stripe-processing-msg" class="hidden" style="text-align: center;margin: 15px 0 30px;"><img
                    src="/static/images/loading.gif"> <?= _t('site.payment', 'processing...'); ?></div>
        <div class="col-md-6">
            <input id="stripe-payment-submit" type="submit" value="<?= _t('site.store', 'Pay') ?>"
                   class="btn btn-primary btn-block hidden m-b10">

        </div>
    </div>
</form>

