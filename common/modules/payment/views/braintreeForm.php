<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.07.18
 * Time: 10:34
 */

use common\components\JsObjectFactory;
use lib\money\Money;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \common\models\PaymentInvoice $paymentInvoice */
/** @var string $postUrl */
/** @var string $clientToken */
/** @var string $logUuid */
/** @var string $defaultPayCardVendor */
/** @var Money $amount */

$this->registerAssetBundle(\common\modules\payment\assets\BraintreeFormAsset::class);
$amount = $paymentInvoice->getAmountTotalWithRefund();

$threeDSecureParams = null;
if (param('braintree_threedsecure_enable')) {
    $threeDSecureEnabled = true;
    if ($paymentInvoice->storeOrder && $paymentInvoice->storeOrder->shipAddress) {
        $threeDSecureParams = [
            'mobilePhoneNumber' => $paymentInvoice->storeOrder->shipAddress->phone,  // 8101234567
            'email'             => $paymentInvoice->storeOrder->shipAddress->email,  // test@example.com
            'shippingMethod'    => '01',     // 01
            'billingAddress'    => [
                'firstName'         => mb_substr($paymentInvoice->storeOrder->shipAddress->first_name, 0, 50),      // Jill
                'lastName'          => mb_substr($paymentInvoice->storeOrder->shipAddress->last_name, 0, 50),       // Doe
                'streetAddress'     => mb_substr($paymentInvoice->storeOrder->shipAddress->address, 0, 50),  // 555 Smith St.
                'extendedAddress'   => mb_substr($paymentInvoice->storeOrder->shipAddress->extended_address, 0, 50),    // #5
                'locality'          => $paymentInvoice->storeOrder->shipAddress->city,       // Oakland
                'region'            => $paymentInvoice->storeOrder->shipAddress->getShortRegionName(),         // CA
                'postalCode'        => $paymentInvoice->storeOrder->shipAddress->zip_code,     // 12345
                'countryCodeAlpha2' => $paymentInvoice->storeOrder->shipAddress->country->iso_code // US
            ]
        ];
    } else {
        $threeDSecureParams = [
            'email'          => $paymentInvoice->user->email,
            'shippingMethod' => '01'
        ];
    }
} else {
    $threeDSecureEnabled = false;
}

$json = json_encode($threeDSecureParams);

JsObjectFactory::createJsObject(
    'braintreeFormClass',
    'braintreeFormObj',
    [
        'clientToken'         => $clientToken,
        'amount'              => $amount->round()->getAmount(),
        'currency'            => strtolower($amount->getCurrency()),
        'invoiceUuid'         => $paymentInvoice->uuid,
        'logUuid'             => $logUuid,
        'threeDSecureEnabled' => $threeDSecureEnabled,
        'threeDSecureParams'  => $threeDSecureParams,
        'enableCard'          => $defaultPayCardVendor===\common\models\PaymentTransaction::VENDOR_BRAINTREE
    ]
    ,
    $this
);

?>
<form id="checkout" method="post" action="<?php echo \yii\helpers\Url::toRoute([$postUrl]); ?>"
>
    <input type="hidden" name="vendor" value="<?= \common\models\PaymentTransaction::VENDOR_BRAINTREE; ?>"/>
    <input type="hidden" name="payment_invoice" value="<?= $paymentInvoice->uuid; ?>"/>
    <input type="hidden" name="payment_method_nonce" id="payment_method_nonce">
    <input type="hidden" name="logUuid" value="<?= $logUuid ?>">
    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

    <div id="payment-form-3ds"></div>
    <div id="payment-form"></div>
    <div class="row">
        <div id="paymentLoadingMsg" style="text-align: center;margin: 0 0 30px;"><img src="/static/images/loading.gif"> <?= _t('site.payment', 'loading...'); ?></div>
        <div id="paymentProcessingMsg" class="hidden" style="text-align: center;margin: 15px 0 30px;"><img src="/static/images/loading.gif"> <?= _t('site.payment', 'processing...'); ?></div>
        <div class="col-md-6">
            <input id="paymentSubmit" type="submit" value="<?= _t('site.store', 'Pay') ?>"
                   class="btn btn-primary btn-block js-braintree-pay hidden m-b10">

        </div>
    </div>
</form>

<style>
    .braintree-toggle {
        margin-bottom: 15px;
        background: #f0f4f8;
        color: #505458;
    }

    .braintree-toggle:hover {
        background: #e0e4e8;
        color: #404448;
    }
</style>



