<?php
/**
 * @var $bankInvoice PaymentBankInvoice
 */

use common\models\PaymentBankInvoice;
use common\modules\payment\widgets\PaymentBankViewWidget;


echo PaymentBankViewWidget::widget(['bankInvoice' => $bankInvoice]);
