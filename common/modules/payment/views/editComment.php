<?php
/** @var $invoice \common\models\PaymentInvoice */
?>
<div ng-if="editComment" ng-cloak>
   <textarea id='editComment_<?= $invoice->uuid ?>'
             ng-model="invoices['<?= $invoice->uuid ?>'].comment"
             class="payment-details"
             ng-blur="saveComment('<?= $invoice->uuid ?>')"
   ></textarea>
</div>
<div ng-if="!editComment" ng-cloak>
    <div>
        {{invoices['<?= $invoice->uuid ?>'].comment}}
    </div>
    <a href="#" ng-click="startEditComment('<?= $invoice->uuid ?>')" class="no-print">
        <p ng-if="invoices['<?= $invoice->uuid ?>'].comment"><?= _t('payment.receipt', 'Change comment') ?></p>
        <p ng-if="!invoices['<?= $invoice->uuid ?>'].comment"><?= _t('payment.receipt', 'Add comment') ?></p>
    </a>
</div>



