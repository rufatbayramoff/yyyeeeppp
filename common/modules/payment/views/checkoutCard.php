<?php
/**
 * @var \common\models\PaymentInvoice $invoiceBraintree
 * @var \common\models\PaymentInvoice $invoiceTs
 * @var \common\models\PaymentInvoice $invoiceBonus
 * @var \common\models\PaymentInvoice $invoiceBankTransfer
 * @var string $clientToken
 * @var string $widget
 * @var string $posUid
 * @var Money $amountNetIncome
 * @var StoreOrder $storeOrder
 * @var string $stripeClientToken
 * @var string $stripeAlipayClientToken
 * @var string $braintreeClientToken
 * @var string $defaultPayCardVendor
 */

use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\modules\payment\services\PaymentService;
use lib\money\Money;
use yii\helpers\Url;

$postUrl = '/store/payment/checkout?';

if ($widget) {
    $postUrl .= 'widget=1&';
}

if ($posUid) {
    $postUrl .= 'posUid=' . $posUid . '&';
}

$postUrl     = substr($postUrl, 0, -1);
$currentUser = \frontend\models\user\UserFacade::getCurrentUser();

$showBraintreeForm            = $invoiceBraintree && PaymentService::isCanBePayedByBraintree($invoiceBraintree, $defaultPayCardVendor);
$showStripeForm               = $invoiceBraintree && PaymentService::isCanBePayedByStripe($invoiceBraintree, $defaultPayCardVendor);
$showAlipayForm               = $invoiceBraintree && PaymentService::isCanBePayedByAlipay($invoiceBraintree, $defaultPayCardVendor);
$showBankTransferForm         = $invoiceBankTransfer && PaymentService::canPayInvoiceByBankInvoice($invoiceBankTransfer);
$showBankTransferFormDiscount = ($invoiceBankTransfer && $invoiceBankTransfer->preorder) ? true : false;
$showTsForm                   = $currentUser && $invoiceTs && PaymentService::isCanBePayedByTs($invoiceTs);
$showBonusForm                = $currentUser && $invoiceBonus && PaymentService::isCanBePayedByBonus($invoiceBonus);

$paymentTypesCount         = (int)$showBraintreeForm + (int)$showStripeForm + (int)$showBankTransferForm + (int)$showTsForm + (int)$showBonusForm;
$enablePaymentTypeSelector = $paymentTypesCount > 1;

if ($defaultPayCardVendor === PaymentTransaction::VENDOR_BRAINTREE) {
    $selectedMethod = $showBraintreeForm ? PaymentTransaction::VENDOR_BRAINTREE : (
    $showStripeForm ? PaymentTransaction::VENDOR_STRIPE : '');
} else {
    $selectedMethod = $showStripeForm ? PaymentTransaction::VENDOR_STRIPE : (
    $showBraintreeForm ? PaymentTransaction::VENDOR_BRAINTREE : '');
}
$selectedMethod = $selectedMethod ? $selectedMethod : (
    $showBankTransferForm ? PaymentTransaction::VENDOR_BANK_TRANSFER : (
    $showTsForm ? PaymentTransaction::VENDOR_TS : (
    $showBonusForm ? PaymentTransaction::VENDOR_BONUS : ''
    )));

?>
<span ng-cloak class="ng-cloak-loader wide-padding--right">
    <img src="/static/images/preloader.gif" width="60" height="60">
</span>
<span ng-cloak="">
    <?php if ($enablePaymentTypeSelector) {
        function braintreeSelector($text)
        { ?>
            <div class="radio radio-primary">
            <input type="radio"
                   name="payment_method"
                   id="payment_method_b"
                   ng-change="choosePaymentMethod('<?php echo PaymentTransaction::VENDOR_BRAINTREE; ?>')"
                   ng-model="paymentData.selectedMethod"
                   value="<?php echo PaymentTransaction::VENDOR_BRAINTREE; ?>"
            >
            <label for="payment_method_b">
                <?= $text ?>
            </label>
        </div>
            <?php
        }

        function stripeSelector($text)
        { ?>
            <div class="radio radio-primary">
            <input type="radio"
                   name="payment_method"
                   id="payment_method_stripe"
                   ng-change="choosePaymentMethod('<?php echo PaymentTransaction::VENDOR_STRIPE; ?>')"
                   ng-model="paymentData.selectedMethod"
                   value="<?php echo PaymentTransaction::VENDOR_STRIPE; ?>"
            >
            <label for="payment_method_stripe">
                <?= $text ?>
            </label>
            </div>
            <?php
        }

        ?>
        <?php
        if ($defaultPayCardVendor === PaymentTransaction::VENDOR_STRIPE) {
            if ($showStripeForm) {
                echo stripeSelector(_t('site.store', 'Pay with a bank card'));
            }
            if ($showBraintreeForm) {
                echo braintreeSelector(_t('site.store', 'Paypal'));
            }
        } else {
            if ($showBraintreeForm) {
                echo braintreeSelector(_t('site.store', 'Pay with a bank card or paypal'));
            }
        }
        ?>

    <?php if ($showAlipayForm): ?>
    <div class="radio radio-primary">
            <input type="radio"
                   name="payment_method"
                   id="payment_method_stripe_alipay"
                   ng-change="choosePaymentMethod('<?php echo PaymentTransaction::VENDOR_STRIPE_ALIPAY; ?>')"
                   ng-model="paymentData.selectedMethod"
                   value="<?php echo PaymentTransaction::VENDOR_STRIPE_ALIPAY; ?>"
            >
            <label for="payment_method_stripe_alipay">
                <?=_t('site.store', 'Alipay payment')?>
            </label>
    </div>
    <?php endif; ?>

    <?php if ($showBankTransferForm): ?>
            <div class="radio radio-primary">
            <input type="radio"
                   name="payment_method"
                   id="payment_method_bt"
                   ng-change="choosePaymentMethod('<?php echo PaymentTransaction::VENDOR_BANK_TRANSFER; ?>')"
                   ng-model="paymentData.selectedMethod"
                   value="<?php echo PaymentTransaction::VENDOR_BANK_TRANSFER; ?>"
            >
            <label for="payment_method_bt">
                <?php echo _t('site.store', 'Bank transfer'); ?>
                <?php
                if ($showBankTransferFormDiscount) {
                    echo _t('site.store', ' (Payment method discount)');
                }
                ?>
            </label>
        </div>
    <?php endif; ?>

    <?php if ($showTsForm): ?>
            <div class="radio radio-primary">
            <input type="radio"
                   name="payment_method"
                   id="payment_method_ts"
                   ng-change="choosePaymentMethod('<?php echo PaymentTransaction::VENDOR_TS; ?>')"
                   ng-model="paymentData.selectedMethod"
                   value="<?php echo PaymentTransaction::VENDOR_TS; ?>"
            >
            <label for="payment_method_ts">
                <?php echo _t('site.store', 'Pay Using Treatstock Balance'); ?>
            </label>
        </div>
        <?php endif; ?>

    <?php if ($showBonusForm): ?>
            <div class="radio radio-primary">
            <input type="radio"
                   name="payment_method"
                   id="payment_method_bonus"
                   ng-change="choosePaymentMethod('<?php echo PaymentTransaction::VENDOR_BONUS; ?>')"
                   ng-model="paymentData.selectedMethod"
                   value="<?php echo PaymentTransaction::VENDOR_BONUS; ?>"
            >
            <label for="payment_method_bonus">
                <?php echo _t('site.store', 'Pay Using Treatstock Bonus'); ?>
            </label>
        </div>
        <?php endif; ?>
    <?php } ?>
    <div ng-cloak>{{!paymentData.selectedMethod?choosePaymentMethod('<?= $selectedMethod ?>'):''}}</div>

    <div class="panel panel-default m-t30 m-b30">
        <div class="panel-body">
            <?php if ($showBraintreeForm): ?>
                <div ng-class="{'hide' : !isSelectedPaymentMethod('<?php echo PaymentTransaction::VENDOR_BRAINTREE; ?>')}">
                    <?php echo $this->render('@common/modules/payment/views/braintreeForm.php', [
                        'logUuid'              => $logUuid,
                        'postUrl'              => $postUrl,
                        'paymentInvoice'       => $invoiceBraintree,
                        'clientToken'          => $braintreeClientToken,
                        'defaultPayCardVendor' => $defaultPayCardVendor
                    ]) ?>
                </div>
            <?php endif; ?>
            <?php if ($showStripeForm): ?>
                <div ng-class="{'hide' : !isSelectedPaymentMethod('<?php echo PaymentTransaction::VENDOR_STRIPE; ?>')}">
                <?php echo $this->render('@common/modules/payment/views/stripeForm.php', [
                    'logUuid'              => $logUuid,
                    'postUrl'              => $postUrl,
                    'paymentInvoice'       => $invoiceBraintree,
                    'clientToken'          => $stripeClientToken,
                    'defaultPayCardVendor' => $defaultPayCardVendor
                ]) ?>
            </div>
            <?php endif; ?>
            <?php if ($showAlipayForm): ?>
                <div ng-class="{'hide' : !isSelectedPaymentMethod('<?php echo PaymentTransaction::VENDOR_STRIPE_ALIPAY; ?>')}">
                <?php echo $this->render('@common/modules/payment/views/stripeAlipayForm.php', [
                    'logUuid'              => $logUuid,
                    'postUrl'              => $postUrl,
                    'paymentInvoice'       => $invoiceBraintree,
                    'clientToken'          => $stripeAlipayClientToken,
                    'defaultPayCardVendor' => $defaultPayCardVendor
                ]) ?>
            </div>
            <?php endif; ?>
            <?php if ($showBankTransferForm): ?>
                <div ng-class="{'hide' : !isSelectedPaymentMethod('<?php echo PaymentTransaction::VENDOR_BANK_TRANSFER; ?>')}">
                <?php echo $this->render('@common/modules/payment/views/bankTransferForm.php', [
                    'logUuid'              => $logUuid,
                    'postUrl'        => Url::toRoute(['store/payment/checkout-invoice']),
                    'paymentInvoice' => $invoiceBankTransfer
                ]) ?>
            </div>
            <?php endif; ?>
            <?php if ($showTsForm): ?>
                <div ng-class="{'hide' : !isSelectedPaymentMethod('<?php echo PaymentTransaction::VENDOR_TS; ?>')}">
                <?php echo $this->render('@common/modules/payment/views/treatstockForm.php', [
                    'logUuid'              => $logUuid,
                    'postUrl'        => $postUrl,
                    'paymentInvoice' => $invoiceTs,
                ]) ?>
            </div>
            <?php endif; ?>
            <?php if ($showBonusForm): ?>
                <div ng-class="{'hide' : !isSelectedPaymentMethod('<?php echo PaymentTransaction::VENDOR_BONUS; ?>')}">
                <?php echo $this->render('@common/modules/payment/views/bonusForm.php', [
                    'logUuid'              => $logUuid,
                    'postUrl'        => $postUrl,
                    'paymentInvoice' => $invoiceBonus,
                ]) ?>
            </div>
            <?php endif; ?>
            <?php if (!$showTsForm && !$showBankTransferForm && !$showStripeForm && !$showBraintreeForm): ?>
                <div>
               Sorry, we can't accept payment at the moment. Please, try again next day.
            </div>
            <?php endif; ?>
        </div>
    </div>
</span>

