<?php

/**
 * @var \common\models\PaymentInvoice[] $paidInvoices
 * @var \common\models\PaymentReceipt $receipt
 */

use common\models\UserAddress;
use common\modules\payment\serializers\ReceiptInvoiceSerializer;
use common\modules\payment\serializers\ReceiptSerializer;
use common\modules\payment\widgets\ReceiptViewWidget;
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

Yii::$app->angular
    ->controller(
        [
            'reciept/controllers/ReceiptController',
        ]
    )->service(['notify', 'user', 'router', 'modal']);

$invoicesSerialized = ReceiptInvoiceSerializer::serialize($paidInvoices);
$invoicesSerialized = \yii\helpers\ArrayHelper::index($invoicesSerialized, 'uuid');
$receiptSerialized = ReceiptSerializer::serialize($receipt);

Yii::$app->angular->controllerParam('receipt', $receiptSerialized);
Yii::$app->angular->controllerParam('invoices', $invoicesSerialized);
?>
<div class="receipt" ng-controller="ReceiptController">
    <?= Alert::widget(['options' => ['class' => 'alertpanel no-print']]); ?>
    <?= ReceiptViewWidget::widget([
        'paidInvoices' => $paidInvoices,
        'injectedAdditionalBuyerDetailsView' => __DIR__.'/additionalBuyerDetails.php',
        'injectedCommentView' => __DIR__.'/editComment.php'
    ]);
    ?>

    <div class="receipt__footer no-print">
        <div class="receipt__share">
            <a href="javascript:" class="receipt-btn" type="button" name="Print" onclick="window.print()">
                <span class="ico ico--print"></span>
                <?php echo _t('payment.receipt', 'Print'); ?>
            </a>
            <a href="/payment/receipt/email/?id=<?php echo $receipt->id; ?>" class="receipt-btn" type="button"
               name="Send">
                <span class="ico ico--mail"></span>
                <?php echo _t('payment.receipt', 'Send by email'); ?>
            </a>
            <a href="/payment/receipt/download/?id=<?php echo $receipt->id; ?>" class="receipt-btn" type="button"
               name="Download">
                <span class="ico ico--download"></span>
                <?php echo _t('payment.receipt', 'Download'); ?>
            </a>
        </div>

        <strong><?php echo _t('payment.receipt', 'Return Policy'); ?></strong>: <a
                href="https://www.treatstock.com/site/return-policy">https://www.treatstock.com/site/return-policy</a>
        <br>
        <strong><?php echo _t('payment.receipt', 'Terms and Conditions'); ?></strong>: <a
                href="https://www.treatstock.com/site/terms">https://www.treatstock.com/site/terms</a>
        <br>
        <strong><?php echo _t('payment.receipt', 'Contact Us'); ?></strong>: <a
                href="https://www.treatstock.com/site/contact">https://www.treatstock.com/site/contact</a>
    </div>
</div>

