<?php

use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use lib\money\Money;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 06.11.18
 * Time: 16:20
 *
 * @var Money $userMainAmount
 * @var string $postUrl
 * @var PaymentInvoice $paymentInvoice
 */
$paymentAccountService = Yii::createObject(\common\modules\payment\services\PaymentAccountService::class);
$amountTotal = $paymentInvoice->getAmountTotalWithRefund();
$userMainAmount = $paymentAccountService->getUserMainAmount($paymentInvoice->user, $paymentInvoice->currency);
?>
<h2 class="m-t10 m-b30"><?php echo _t('site.store', 'Pay Using Treatstock Balance'); ?></h2>

<?php if ($userMainAmount->getAmount() > $amountTotal->getAmount() && $amountTotal->getCurrency() === $userMainAmount->getCurrency()): ?>
    <?php ActiveForm::begin([
        'action' => $postUrl
    ]); ?>
        <?php echo Html::hiddenInput('vendor', PaymentTransaction::VENDOR_TS); ?>
        <?php echo Html::hiddenInput('payment_invoice', $paymentInvoice->uuid); ?>
        <?php echo Html::hiddenInput('logUuid', $logUuid); ?>
        <div class="row">
            <div class="col-md-6">
                <input type="submit" value="<?php echo _t('site.store', 'Pay Now') ?>" class="btn btn-primary btn-block">
            </div>
            <div class="col-md-6">
                <p class="m-t10"><?php echo _t('site.store', 'Your balance: {balance}', ['balance' => displayAsMoney($userMainAmount)]); ?></p>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
<?php else: ?>
    <p><?php echo _t('site.store', 'Your Treatstock balance has insufficient funds.'); ?></p>
<?php endif; ?>
