<?php

use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\modules\payment\services\InvoiceBankService;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 06.11.18
 * Time: 16:20
 *
 * @var string $postUrl
 * @var PaymentInvoice $paymentInvoice
 */

$amountTotal = $paymentInvoice->getAmountTotal();
?>

<h2 class="m-t10 m-b30"><?php echo _t('site.store', 'Bank transfer'); ?></h2>

<?php ActiveForm::begin([
    'action' => $postUrl
]); ?>
    <?php echo Html::hiddenInput('vendor', PaymentTransaction::VENDOR_BANK_TRANSFER); ?>
    <?php echo Html::hiddenInput('payment_invoice', $paymentInvoice->uuid); ?>
    <?php echo Html::hiddenInput('logUuid', $logUuid); ?>

    <div class="row">
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary btn-block m-b10">
                <span class="tsi tsi-doc m-r10"></span>
                <?php echo _t('site.order', 'Issue invoice') ?>
            </button>
        </div>
        <div class="col-md-6">
            <p>
                <?php echo _t('site.store', 'Invoice can only be issued for orders where the total cost exceeds {currency}{limit}',
                    ['currency'=>$paymentInvoice->paymentCurrency->title_original ,'limit' => InvoiceBankService::getInvoiceFromUsd()]
                ); ?>
            </p>
        </div>
    </div>
<?php ActiveForm::end(); ?>