<?php

use common\components\JsObjectFactory;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\modules\payment\assets\StripeFormAsset;
use common\modules\payment\services\InvoiceBankService;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 06.11.18
 * Time: 16:20
 *
 * @var string $postUrl
 * @var string $clientToken
 * @var string $logUuid
 * @var PaymentInvoice $paymentInvoice
 */

$this->registerAssetBundle(StripeFormAsset::class);

$amountTotal = $paymentInvoice->getAmountTotalWithRefund();
$stripe      = '';

JsObjectFactory::createJsObject(
    'stripeFormClass',
    'stripeAlipayFormObj',
    [
        'publicKey'   => app()->params['stripe_pub_key'],
        'clientToken' => $clientToken,
        'invoiceUuid' => $paymentInvoice->uuid,
        'logUuid'     => $logUuid,
        'successUrl'  => \common\modules\payment\processors\StripeProcessor::getSuccessUrl($paymentInvoice, $logUuid),
        'failed'      => \common\modules\payment\processors\StripeProcessor::getCancelUrl($paymentInvoice, $logUuid),

        'stripePaymentBlockId'      => 'stripe-alipay-payment-block',
        'stripePaymentLoadingMsgId' => 'stripe-alipay-payment-loading-msg',
        'stripePaymentSubmitId'     => 'stripe-alipay-payment-submit',
        'stripeProcessingMsgId'     => 'stripe-alipay-processing-msg',
        'stripePaymentFormId'       => 'stripe-alipay-payment-form'
    ]
    ,
    $this
);
?>
<form id="stripe-alipay-payment-form" method="post" action="<?php echo \yii\helpers\Url::toRoute([$postUrl]); ?>" style="">
    <input type="hidden" name="vendor" value="<?= \common\models\PaymentTransaction::VENDOR_STRIPE; ?>"/>
    <input type="hidden" name="payment_invoice" value="<?= $paymentInvoice->uuid; ?>"/>
    <input type="hidden" name="payment_method_nonce" id="payment_method_nonce">
    <input type="hidden" name="logUuid" value="<?= $logUuid ?>">
    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

    <div id="stripe-alipay-payment-form-3ds"></div>
    <div id="stripe-alipay-payment-block" class="hidden"></div>
    <div class="row" style="padding-top: 15px">
        <div id="stripe-alipay-payment-loading-msg" style="text-align: center;margin: 0 0 30px;"><img
                    src="/static/images/loading.gif"> <?= _t('site.payment', 'loading...'); ?></div>
        <div id="stripe-alipay-processing-msg" class="hidden" style="text-align: center;margin: 15px 0 30px;"><img
                    src="/static/images/loading.gif"> <?= _t('site.payment', 'processing...'); ?></div>
        <div class="col-md-6">
            <input id="stripe-alipay-payment-submit" type="submit" value="<?= _t('site.store', 'Pay') ?>"
                   class="btn btn-primary btn-block hidden m-b10">

        </div>
    </div>
</form>

