<?php
/** @var $invoice \common\models\PaymentInvoice */
?>
<div ng-if="editAdditionalBuyerDetails" ng-cloak>
  <textarea id='editAdditionalBuyerDetails_<?= $invoice->uuid ?>'
      ng-model="invoices['<?= $invoice->uuid ?>'].additionalBuyerDetails"
      ng-blur="saveBuyerDetails('<?= $invoice->uuid ?>')"></textarea>
</div>
<div ng-if="!editAdditionalBuyerDetails" ng-cloak>
    <div>
        {{invoices['<?= $invoice->uuid ?>'].additionalBuyerDetails}}
    </div>
    <a href="#" ng-click="startEditBuyerDetails('<?= $invoice->uuid ?>')"
       class="no-print"><?= _t('payment.receipt', 'Additional details') ?></a>
</div>

