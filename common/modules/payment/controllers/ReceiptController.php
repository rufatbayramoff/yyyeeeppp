<?php

namespace common\modules\payment\controllers;

use common\components\BaseController;
use common\components\Emailer;
use common\components\exceptions\BusinessException;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\PaymentInvoice;
use common\models\PaymentReceipt;
use common\models\repositories\FileRepository;
use common\models\StoreOrder;
use common\modules\payment\services\PaymentReceiptService;
use common\modules\payment\services\RefundService;
use frontend\models\user\UserFacade;
use lib\pdf\HtmlToPdf;
use Stripe\Refund;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\UserException;
use yii\helpers\Html;

/**
 * Date: 28.11.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 *
 * @property RefundService $refundService
 * @property PaymentReceiptService $receiptService
 */
class ReceiptController extends BaseController
{
    public $viewPath = '@common/modules/payment/views';

    public $layout = '@frontend/views/layouts/plain.php';

    protected $refundService;

    protected $receiptService;

    /** @var FileRepository */
    protected $fileRepository;

    public function injectDependencies(
        RefundService $refundService,
        PaymentReceiptService $receiptService,
        FileRepository $fileRepository
    )
    {
        $this->refundService  = $refundService;
        $this->receiptService = $receiptService;
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param $orderId
     * @param bool $toFile
     *
     * @return string
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionOrder($orderId, $toFile = false)
    {
        $storeOrder  = StoreOrder::tryFindByPk($orderId, 'Order not found');
        $currentUser = UserFacade::getCurrentUser();

        if (!$currentUser) {
            throw new BusinessException('No Access');
        }
        $orderOwner       = UserFacade::isObjectOwner($storeOrder);
        $orderPrintedByPs = $storeOrder->currentAttemp && UserFacade::isObjectOwner($storeOrder->currentAttemp->ps);
        if (!$orderOwner && !$orderPrintedByPs) {
            throw new UserException('No Access');
        }

        if ($storeOrder->getPaymentStatus() === PaymentInvoice::STATUS_NEW) {
            throw new UserException(_t('payment.receipt', 'No payment receipt information for this order'));
        }

        $this->view->title = _t('payment.receipt', 'Payment Receipt for Order #{orderId}', ['orderId' => $orderId]);
        $receipt           = $this->receiptService->getReceiptByStoreOrder($storeOrder);
        $paidInvoices      = $storeOrder->getPaidInvoices();

        return $this->render('receipt', [
            'paidInvoices' => $paidInvoices,
            'receipt'      => $receipt,
            'toFile'       => $toFile
        ]);
    }

    /**
     * @param $uuid
     * @param bool $toFile
     * @return string
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionInvoice($uuid, $toFile = false)
    {
        $invoice = PaymentInvoice::tryFind(['uuid' => $uuid]);
        $this->receiptService->checkAccessForCurrentUserOrFail($invoice);
        $receipt      = $this->receiptService->getReceiptByInvoiceForCurrentUser($invoice);
        $paidInvoices = [
            $invoice
        ];
        return $this->render('receipt', [
            'paidInvoices' => $paidInvoices,
            'receipt'      => $receipt,
            'toFile'       => $toFile
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws ErrorException
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionEmail($id)
    {
        $receipt = $this->getReceipt($id);
        $mailed = $this->receiptService->mailPdfReceipt($receipt);
        if ($mailed) {
            $this->setFlashMsg(true, _t('payment.receipt', 'Email sent'));
        } else {
            $this->setFlashMsg(false, _t('payment.receipt', 'No files to email for this document.'));
        }

        if ($receipt->order) {
            $backUrl = '/payment/receipt/order/?orderId=' . $receipt->order_id;
        } else {
            $backUrl = '/payment/receipt/invoice/?uuid=' . $receipt->invoice_uuid;
        }
        return $this->redirect($backUrl);
    }

    public function actionSaveBuyerDetails($invoiceUuid, $receiptId)
    {
        $receipt = $this->getReceipt($receiptId);
        $invoice = PaymentInvoice::tryFind($invoiceUuid);
        $this->receiptService->saveBuyerDetails($receipt, $invoice, app('request')->post('buyerDetails'));
        $this->receiptService->deleteHtmlFile($receipt);
        return $this->jsonReturn(['success' => true]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws UserException
     * @throws \Exception
     */
    public function actionDownload($id)
    {
        $receipt = $this->getReceipt($id);
        try {
            $filePdf = $this->receiptService->getPdfReceipt($receipt);
            if (empty($filePdf)) {
                throw new Exception(_t('receipt.download', 'No download available for this document.'));
            }
        } catch (\Exception $e) {
            logException($e, 'app');
            throw new \Exception(_t('receipt.download', 'Error generating receipt document'), 0, $e);
        }
        app('response')->setDownloadHeaders($receipt->getCombinedId() . '.pdf', 'application/pdf', false);
        return app('response')->sendFile($filePdf);
    }

    /**
     * @param $id
     * @param $invoiceUuid
     *
     * @return \yii\web\Response
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSaveComment($invoiceUuid, $receiptId)
    {
        $receipt = $this->getReceipt($receiptId);
        $invoice = PaymentInvoice::tryFind($invoiceUuid);
        $this->receiptService->saveComment($receipt, $invoice, app('request')->post('comment'));
        $this->receiptService->deleteHtmlFile($receipt);
        return $this->jsonReturn(['success' => true]);
    }

    /**
     * @param $id
     *
     * @return PaymentReceipt
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    private function getReceipt($id)
    {
        $receipt = PaymentReceipt::tryFindByPk($id);

        if (!UserFacade::isObjectOwner($receipt)) {
            throw new BusinessException('No Access');
        }
        return $receipt;
    }
}