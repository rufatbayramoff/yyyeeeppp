<?php
/**
 * User: nabi
 */

namespace common\modules\payment\controllers;

use common\components\BaseController;
use common\models\PaymentBankInvoice;
use common\modules\payment\services\InvoiceBankService;
use yii\helpers\Html;

/**
 * Class InvoiceBankController
 * @package common\modules\payment\controllers
 *
 * @property InvoiceBankService $invoiceService
 */
class InvoiceBankController extends BaseController
{
    public $viewPath = '@common/modules/payment/views';

    public $layout = '@frontend/views/layouts/plain.php';

    protected $invoiceService;

    public function injectDependencies(InvoiceBankService $invoiceService){
        $this->invoiceService = $invoiceService;
    }

    /**
     * @param $uuid PaymentBankInvoice uuid
     *
     * @return \yii\web\Response
     * @throws \yii\base\UserException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSaveComment($uuid)
    {
        $paymentBankInvoice = PaymentBankInvoice::tryFind([
            'uuid' => $uuid
        ]);

        $this->invoiceService->checkAccessToInvoice($paymentBankInvoice->paymentInvoice);

        $paymentBankInvoice->load(app('request')->post());

        if ($paymentBankInvoice->validate(['comment'])) {
            $paymentBankInvoice->safeSave(['comment']);
        } else {
            $this->setFlashMsg(false, Html::errorSummary($paymentBankInvoice));
        }

        return $this->redirect( ['/store/payment/invoice', 'uuid' => $paymentBankInvoice->uuid]);
    }
}