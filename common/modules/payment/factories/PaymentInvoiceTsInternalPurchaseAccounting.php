<?php

namespace common\modules\payment\factories;

use common\models\InstantPayment;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\StoreOrderPosition;
use common\models\TsInternalPurchase;
use common\modules\payment\fee\FeeHelper;
use lib\money\Currency;
use lib\money\Money;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

class PaymentInvoiceTsInternalPurchaseAccounting extends BaseObject
{
    protected $feeHelper;

    public function injectDependencies(
        FeeHelper $feeHelper
    )
    {
        $this->feeHelper = $feeHelper;
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @param InstantPayment $instantPayment
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function formInvoiceItemsAndAccounting(PaymentInvoice $paymentInvoice, TsInternalPurchase $tsInternalPurchase): array
    {
        if ($tsInternalPurchase->tsInternalPurchaseCertification) {
            $certClass =  $tsInternalPurchase->tsInternalPurchaseCertification->companyService->getTsCertificationClass();
            $paymentInvoiceItem = $this->formInvoiceItem(
                $paymentInvoice,
                'Payment for certification: ' . $tsInternalPurchase->uid,
                $certClass->description,
                1,
                $certClass->price,
                1,
                $measure = ''
            );
        }

        $items = [ $paymentInvoiceItem];

        $accounting[PaymentInvoice::ACCOUNTING_TYPE_TS_INTERNAL_PURCHASE_CERTIFICATE] = [
            'type'  => PaymentInvoice::ACCOUNTING_TYPE_TS_INTERNAL_PURCHASE_CERTIFICATE,
            'price' => $paymentInvoiceItem->total_line,
            'currency' => Currency::USD
        ];

        return [
            'items'      => $items,
            'accounting' => $accounting
        ];
    }

    public function formInvoiceItemsAndAccountingForBalance(PaymentInvoice $paymentInvoice, TsInternalPurchase $tsInternalPurchase, Money $money)
    {
        $paymentInvoiceItem = $this->formInvoiceItem(
            $paymentInvoice,
            'Balance refill: ' . $tsInternalPurchase->uid,
            'Fill up your account balance',
            1,
            $money->getAmount(),
            1,
            $measure = ''
        );

        $items = [ $paymentInvoiceItem];

        $accounting[PaymentInvoice::ACCOUNTING_TYPE_TS_INTERNAL_PURCHASE_DEPOSIT] = [
            'type'  => PaymentInvoice::ACCOUNTING_TYPE_TS_INTERNAL_PURCHASE_DEPOSIT,
            'price' => $paymentInvoiceItem->total_line,
            'currency' => $money->getCurrency()
        ];

        return [$items, $accounting];
    }

    /**
     * @throws \Exception
     */
    public function formInvoiceItem($paymentInvoice, $title, $description, $qty, $cost, $pos, $measure = '')
    {
        $paymentInvoiceItem                       = new PaymentInvoiceItem();
        $paymentInvoiceItem->uuid                 = PaymentInvoiceItem::generateUuid();
        $paymentInvoiceItem->payment_invoice_uuid = $paymentInvoice->uuid;
        $paymentInvoiceItem->pos                  = $pos;
        $paymentInvoiceItem->title                = $title;
        $paymentInvoiceItem->description          = $description;
        $paymentInvoiceItem->measure              = $measure;
        $paymentInvoiceItem->qty                  = $qty;
        $paymentInvoiceItem->total_line           = $cost;
        return $paymentInvoiceItem;
    }

    /**
     * @param $accounting
     * @param string $type
     * @return float
     */
    public function formTotalAmountWithType(array $accounting, string $type): float
    {
        return $accounting[$type]['price'];
    }
}