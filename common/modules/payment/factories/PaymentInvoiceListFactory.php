<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 10.12.18
 * Time: 11:55
 */

namespace common\modules\payment\factories;

use common\models\AffiliateSource;
use common\models\InstantPayment;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\Preorder;
use common\models\StoreOrder;
use common\models\StoreOrderPosition;
use yii\base\BaseObject;

/**
 * Class PaymentInvoiceListFactory
 * @package common\modules\payment\factories
 *
 * @property PaymentInvoiceFactory $paymentInvoiceFactory
 * @property PaymentInvoicePaymentMethodFactory $paymentInvoicePaymentMethodFactory
 */
class PaymentInvoiceListFactory extends BaseObject
{
    /**
     * @var PaymentInvoiceFactory
     */
    protected $paymentInvoiceFactory;

    /**
     * @var PaymentInvoicePaymentMethodFactory
     */
    protected $paymentInvoicePaymentMethodFactory;

    public function injectDependencies(
        PaymentInvoiceFactory $paymentInvoiceFactory,
        PaymentInvoicePaymentMethodFactory $paymentInvoicePaymentMethodFactory
    )
    {
        $this->paymentInvoiceFactory              = $paymentInvoiceFactory;
        $this->paymentInvoicePaymentMethodFactory = $paymentInvoicePaymentMethodFactory;
    }

    public static function generateGroupUuid(): string
    {
        do {
            $groupUuid = \common\components\UuidHelper::generateRandCode(10);
            $checkUuid = PaymentInvoice::find()->where(['invoice_group_uuid' => $groupUuid])->asArray()->withoutStaticCache()->one();

        } while ($checkUuid);

        return $groupUuid;
    }

    public function createWithPaymentMethodsByOrder(StoreOrder $storeOrder, ?AffiliateSource $affiliateSource = null): array
    {
        $groupUuid = self::generateGroupUuid();

        $baseInvoice                     = $this->paymentInvoiceFactory->createByOrder($storeOrder, $affiliateSource);
        $baseInvoice->invoice_group_uuid = $groupUuid;

        $baseInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($baseInvoice, [
                PaymentTransaction::VENDOR_TS,
                PaymentTransaction::VENDOR_BANK_TRANSFER,
                PaymentTransaction::VENDOR_THINGIVERSE,
                PaymentTransaction::VENDOR_BRAINTREE,
                PaymentTransaction::VENDOR_STRIPE
            ])
        );

        $bonusInvoice                   = $this->paymentInvoiceFactory->createByPaymentInvoiceBonus($baseInvoice);
        $bonusInvoice->is_settle_submit = 1;
        $bonusInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($bonusInvoice, [
                PaymentTransaction::VENDOR_BONUS
            ])
        );

        return [
            $baseInvoice,
            $bonusInvoice
        ];
    }

    /**
     * @param InstantPayment $instantPayment
     *
     * @return array
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     */
    public function createWithPaymentMethodsByInstantPayment(InstantPayment $instantPayment): array
    {
        $groupUuid = self::generateGroupUuid();

        $baseInvoice                     = $this->paymentInvoiceFactory->createByInstantPayment($instantPayment);
        $baseInvoice->invoice_group_uuid = $groupUuid;

        $baseInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($baseInvoice, [
                PaymentTransaction::VENDOR_TS,
                PaymentTransaction::VENDOR_BONUS,
                PaymentTransaction::VENDOR_STRIPE,
                PaymentTransaction::VENDOR_BANK_TRANSFER,
                PaymentTransaction::VENDOR_THINGIVERSE
            ])
        );

        $baseInvoiceBraintree                     = $this->paymentInvoiceFactory->createByInstantPaymentWithPaymentMethodFee($instantPayment, PaymentTransaction::VENDOR_BRAINTREE);
        $baseInvoiceBraintree->invoice_group_uuid = $groupUuid;

        return [
            $baseInvoice,
            $baseInvoiceBraintree
        ];
    }

    /**
     * @param Preorder $preorder
     *
     * @return array
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     */
    public function createWithPaymentMethodsByPreorder(Preorder $preorder): array
    {
        $groupUuid = self::generateGroupUuid();

        $baseInvoice                     = $this->paymentInvoiceFactory->createByPreorder($preorder);
        $baseInvoice->invoice_group_uuid = $groupUuid;

        $baseInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($baseInvoice, [
                PaymentTransaction::VENDOR_TS,
                PaymentTransaction::VENDOR_STRIPE,
                PaymentTransaction::VENDOR_BANK_TRANSFER,
                PaymentTransaction::VENDOR_THINGIVERSE
            ])
        );

        $baseInvoiceBraintree                     = $this->paymentInvoiceFactory->createByPreorderWithPaymentMethodFee($preorder, PaymentTransaction::VENDOR_BRAINTREE);
        $baseInvoiceBraintree->invoice_group_uuid = $groupUuid;
        $invoiceStripe                            = $this->paymentInvoiceFactory->createByPreorderWithPaymentMethodFee($preorder, PaymentTransaction::VENDOR_STRIPE);
        $invoiceStripe->invoice_group_uuid        = $groupUuid;
        $bonusInvoice                             = $this->paymentInvoiceFactory->createByPaymentInvoiceBonus($baseInvoice);

        return [
            $baseInvoice,
            $baseInvoiceBraintree,
            $invoiceStripe,
            $bonusInvoice
        ];
    }

    /**
     * @param StoreOrderPosition $additionalService
     *
     * @return array
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     */
    public function createWithPaymentMethodsByAdditionalService(StoreOrderPosition $additionalService): array
    {
        $groupUuid = self::generateGroupUuid();

        $baseInvoice                     = $this->paymentInvoiceFactory->createByAdditionalService($additionalService);
        $baseInvoice->invoice_group_uuid = $groupUuid;

        $baseInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($baseInvoice, [
                PaymentTransaction::VENDOR_TS,
                PaymentTransaction::VENDOR_BONUS,
                PaymentTransaction::VENDOR_STRIPE,
                PaymentTransaction::VENDOR_BANK_TRANSFER,
                PaymentTransaction::VENDOR_THINGIVERSE
            ])
        );

        $baseInvoiceBraintree                     = $this->paymentInvoiceFactory->createByAdditionalServiceWithPaymentMethodFee($additionalService, PaymentTransaction::VENDOR_BRAINTREE);
        $baseInvoiceBraintree->invoice_group_uuid = $groupUuid;

        return [
            $baseInvoice,
            $baseInvoiceBraintree
        ];
    }
}