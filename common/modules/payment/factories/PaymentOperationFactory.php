<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.10.18
 * Time: 10:25
 */

namespace common\modules\payment\factories;

use common\components\DateHelper;
use common\components\order\PriceCalculator;
use common\components\PaymentExchangeRateConverter;
use common\components\UuidHelper;
use common\interfaces\machine\DeliveryParamsInterface;
use common\models\DeliveryType;
use common\models\Payment;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\StoreOrder;
use common\modules\payment\fee\FeeHelper;
use common\modules\payment\fee\FeeHelperInterface;
use frontend\models\delivery\DeliveryForm;
use lib\delivery\delivery\models\DeliveryRate;
use lib\money\Currency;
use lib\money\Money;
use PayPal\Api\PaymentOptions;
use yii\base\BaseObject;

class PaymentOperationFactory extends BaseObject
{
    /** @var PriceCalculator */
    protected $priceCalculator;

    /** @var FeeHelper */
    protected $feeHelper;

    /** @var PaymentExchangeRateConverter */
    protected $paymentExchangeRateConverter;

    public function injectDependencies(
        PriceCalculator $priceCalculator,
        FeeHelper $feeHelper,
        PaymentExchangeRateConverter $paymentExchangeRateConverter
    ) {
        $this->priceCalculator = $priceCalculator;
        $this->feeHelper = $feeHelper;
        $this->paymentExchangeRateConverter = $paymentExchangeRateConverter;
    }

    /**
     * @param Payment $payment
     * @param PaymentAccount $paymentFrom
     * @param PaymentAccount $paymentTo
     */
    public function createByPayment(Payment $payment)
    {
        $paymentOperation = new PaymentDetailOperation();
        $paymentOperation->payment_id = $payment->id;
        $paymentOperation->populateRelation('payment', $payment);
        $paymentOperation->uuid = PaymentDetailOperation::generateUuid();
        $paymentOperation->created_at = DateHelper::now();
        return $paymentOperation;
    }

}