<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.08.18
 * Time: 10:48
 */


namespace common\modules\payment\factories;

use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\Preorder;
use common\modules\payment\components\BonusCalculator;
use common\modules\payment\fee\FeeHelper;
use lib\money\Currency;
use Yii;
use yii\base\BaseObject;

/**
 * Class PaymentInvoiceFactory
 * @package common\modules\payment\factories
 */
class PaymentInvoicePreorderAccounting extends BaseObject
{
    /**
     * @param PaymentInvoice $paymentInvoice
     * @param Preorder $preorder
     *
     * @return array
     * @throws \Exception
     */
    public function formInvoiceItemsAndAccounting(PaymentInvoice $paymentInvoice, Preorder $preorder): array
    {
        $items                                             = [];
        $accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS] = [];

        $pos        = 1;
        $totalPrice = 0;
        $totalFee   = 0;

        foreach ($preorder->preorderWorks as $work) {
            $paymentInvoiceItem                       = new PaymentInvoiceItem();
            $paymentInvoiceItem->uuid                 = PaymentInvoiceItem::generateUuid();
            $paymentInvoiceItem->payment_invoice_uuid = $paymentInvoice->uuid;
            $paymentInvoiceItem->pos                  = $pos++;
            $paymentInvoiceItem->title                = $work->getWorkTitle();
            $paymentInvoiceItem->description          = _t('store.order', 'Invoice item for preorder: {preorderId}', ['preorderId' => $work->preorder_id]);

            $measure = $work->getUnitTypeLabel();

            $paymentInvoiceItem->measure    = $measure !== '' ? $measure : 'item';
            $paymentInvoiceItem->qty        = $work->qty;
            $paymentInvoiceItem->total_line = $work->cost;
            $items[]                        = $paymentInvoiceItem;
            $totalPrice                     += $work->cost;
            $totalFee                       += $work->getCalcCostPsFee()->getAmount();

            $accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS][$paymentInvoiceItem->uuid] = [
                'preorderWorkId'                             => $work->id,
                'paymentInvoiceItemUuid'                     => $paymentInvoiceItem->uuid,
                PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER => [
                    'type'  => PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER,
                    'price' => $work->getCalcCostMoney()->getAmount(),
                ],
                PaymentInvoice::ACCOUNTING_TYPE_FEE          => [
                    'type'  => PaymentInvoice::ACCOUNTING_TYPE_FEE,
                    'price' => $work->getCalcCostPsFee()->getAmount(),
                ]
            ];
        }

        $feeHelper = Yii::createObject(FeeHelper::class);
        $company   = $preorder->company;
        if ($company->cashback_percent && ($paymentInvoice->currency !== Currency::BNS)) {
            $tsBonusBalance = BonusCalculator::calcQuote($company, $totalPrice, $preorder->user);

            $accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS,
                'price'    => $tsBonusBalance,
                'currency' => Currency::BNS,
            ];
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS,
                'price'    => $tsBonusBalance,
                'currency' => Currency::BNS,
            ];
        }

        return [
            'items'      => $items,
            'accounting' => $accounting
        ];
    }

    /**
     * @param $accounting
     *
     * @return float
     */
    public function formTotalAmount($accounting): float
    {
        $totalAmount = 0;

        foreach ($accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS] as $item) {
            $totalAmount += ($item[PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER]['price'] ?? 0)
                + ($item[PaymentInvoice::ACCOUNTING_TYPE_FEE]['price'] ?? 0);
        }

        $totalAmount -= ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PROMO_CODE]['price'] ?? 0);
        $totalAmount -= ($accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS]['price'] ?? 0);

        $totalAmount += ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE]['price'] ?? 0);

        return $totalAmount;
    }

}