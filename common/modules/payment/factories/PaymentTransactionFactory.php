<?php
/**
 * User: nabi
 */

namespace common\modules\payment\factories;

use common\models\PaymentDetail;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionHistory;
use common\modules\payment\gateways\PaymentGatewayTransaction;

class PaymentTransactionFactory
{
    /**
     * @param PaymentGatewayTransaction $paymentGatewayTransaction
     * @param PaymentDetail $paymentDetail
     * @param string $type
     *
     * @return array
     */
    public function createByGatewayTransaction(
        PaymentGatewayTransaction $paymentGatewayTransaction,
        PaymentDetail $paymentDetail,
        $type = PaymentTransaction::TYPE_PAYMENT
    )
    {
        $paymentTransaction = new PaymentTransaction();
        $paymentTransaction->user_id = $paymentDetail->paymentAccount->user_id;
        $paymentTransaction->transaction_id = $paymentGatewayTransaction->transactionId;
        $paymentTransaction->first_payment_detail_id = $paymentDetail->id;
        $paymentTransaction->vendor = $paymentGatewayTransaction->vendor;
        $paymentTransaction->created_at = $paymentGatewayTransaction->createdAt;
        $paymentTransaction->updated_at = $paymentGatewayTransaction->updatedAt;
        $paymentTransaction->status = $paymentGatewayTransaction->status;
        $paymentTransaction->type = $type;
        $paymentTransaction->amount = $paymentGatewayTransaction->amount->getAmount();
        $paymentTransaction->details = $paymentGatewayTransaction->details;
        $paymentTransaction->currency = $paymentGatewayTransaction->amount->getCurrency();
        $paymentTransaction->last_original_transaction = serialize($paymentGatewayTransaction->originalTransaction);

        $paymentTransactionHistory = new PaymentTransactionHistory();
        $paymentTransactionHistory->payment_detail_id = $paymentDetail->id;
        $paymentTransactionHistory->created_at = $paymentTransaction->created_at;
        $paymentTransactionHistory->action_id = PaymentTransactionHistory::ACTION_CREATE;
        $paymentTransactionHistory->comment = ['status' => $paymentTransaction->status];
        $paymentTransactionHistory->user_id = $paymentDetail->paymentAccount->user_id;
        $paymentTransactionHistory->populateRelation('transaction', $paymentTransaction);

        return [$paymentTransaction, $paymentTransactionHistory];
    }
}