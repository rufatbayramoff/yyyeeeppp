<?php
/**
 *
 * @author Nabi <n.ibatulin@treatstock.com>
 */

namespace common\modules\payment\factories;

use common\components\DateHelper;
use common\models\Model3dPart;
use common\models\PaymentBankInvoice;
use common\models\PaymentBankInvoiceItem;
use common\models\PaymentInvoice;
use common\models\Preorder;
use common\models\StoreOrder;
use common\models\StoreOrderItem;
use common\models\StorePricer;
use common\models\StorePricerType;
use common\models\UserAddress;

/**
 * Class InvoiceBankFactory
 *
 * @package common\modules\payment\factories
 */
class InvoiceBankFactory
{
    /**
     * @param PaymentInvoice $invoice
     *
     * @return PaymentBankInvoice
     * @throws \yii\base\UserException
     */
    public function createByInvoice(PaymentInvoice $invoice): PaymentBankInvoice
    {
        $bankInvoice                       = new PaymentBankInvoice();
        $bankInvoice->uuid                 = PaymentBankInvoice::generateUuid();
        $bankInvoice->payment_invoice_uuid = $invoice->uuid;
        $bankInvoice->ship_to              = $invoice->storeOrder && $invoice->storeOrder->shipAddress ? UserAddress::formatAddress($invoice->storeOrder->shipAddress) : null;
        $bankInvoice->bill_to              = $invoice->storeOrder && $invoice->storeOrder->billAddress ? UserAddress::formatAddress($invoice->storeOrder->billAddress) : null;
        $bankInvoice->created_at           = DateHelper::now();
        $bankInvoice->date_due             = DateHelper::addNowSec(60 * 60 * 24 * 10); // 10 days
        $bankInvoice->status               = PaymentBankInvoice::STATUS_NEW;
        $paymentDetails                    = \Yii::$app->setting->get('store.payment_details');
        $bankDetails                       = \Yii::$app->setting->get('store.bank_details');
        $bankInvoice->payment_details      = $paymentDetails[$invoice->currency];
        $bankInvoice->bank_details         = $bankDetails[$invoice->currency];

        return $bankInvoice;
    }
}