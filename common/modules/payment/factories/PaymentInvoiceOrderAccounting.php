<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.08.18
 * Time: 10:48
 */


namespace common\modules\payment\factories;

use common\components\order\PriceCalculator;
use common\components\PaymentExchangeRateConverter;
use common\models\AffiliateAward;
use common\models\DeliveryType;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\StoreOrder;
use common\models\StoreOrderItem;
use common\modules\affiliate\components\AffiliateAwardCalculator;
use common\modules\affiliate\services\AffiliateService;
use common\modules\cutting\services\CuttingOfferPriceCalculator;
use common\modules\payment\components\BonusCalculator;
use common\modules\payment\fee\FeeHelper;
use common\modules\payment\fee\FeeHelperInterface;
use lib\delivery\delivery\models\DeliveryRate;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

/**
 * Class PaymentInvoiceFactory
 *
 * @package common\modules\payment\factories
 *
 * @property PaymentExchangeRateConverter $paymentExchangeRateConverter
 */
class PaymentInvoiceOrderAccounting extends BaseObject
{
    /** @var  PaymentExchangeRateConverter $paymentExchangeRateConverter */
    protected $paymentExchangeRateConverter;

    /** @var PriceCalculator */
    protected $priceCalculator;

    /** @var CuttingOfferPriceCalculator */
    protected $cuttingCalculator;

    /** @var FeeHelper */
    protected $feeHelper;

    /**
     * @var AffiliateService
     */
    protected $affiliateService;

    /** @var AffiliateAwardCalculator */
    protected $affiliateAwardCalculator;

    public function injectDependencies(
        PaymentExchangeRateConverter $paymentExchangeRateConverter,
        PriceCalculator $priceCalculator,
        FeeHelper $feeHelper,
        AffiliateService $affiliateService,
        CuttingOfferPriceCalculator $cuttingCalculator
    )
    {
        $this->paymentExchangeRateConverter = $paymentExchangeRateConverter;
        $this->priceCalculator              = $priceCalculator;
        $this->feeHelper                    = $feeHelper;
        $this->affiliateService             = $affiliateService;
        $this->cuttingCalculator            = $cuttingCalculator;
    }

    /**
     * @throws \Exception
     */
    public function formInvoiceItem($paymentInvoice, $title, $description, $qty, $cost, $pos, $measure = '')
    {
        $paymentInvoiceItem                       = new PaymentInvoiceItem();
        $paymentInvoiceItem->uuid                 = PaymentInvoiceItem::generateUuid();
        $paymentInvoiceItem->payment_invoice_uuid = $paymentInvoice->uuid;
        $paymentInvoiceItem->pos                  = $pos;
        $paymentInvoiceItem->title                = $title;
        $paymentInvoiceItem->description          = $description;
        $paymentInvoiceItem->measure              = $measure;
        $paymentInvoiceItem->qty                  = $qty;
        $paymentInvoiceItem->total_line           = $cost;
        return $paymentInvoiceItem;
    }

    /**
     * Model3d Or Cutting
     *
     * @param PaymentInvoice $paymentInvoice
     * @param StoreOrder $order
     * @param $items
     * @param $accounting
     * @param $totalProducePrice
     * @throws \Exception
     */
    protected function formInvoiceItemProduce(PaymentInvoice $paymentInvoice, StoreOrder $order, &$items, &$accounting, &$totalProducePrice)
    {
        $printer      = $order->currentAttemp->machine->psPrinter;
        $modelReplica = $order->getFirstReplicaItem();
        if (!$modelReplica) {
            throw new InvalidArgumentException('Invalid order state');
        }

        $manufacturePrice  = $this->priceCalculator->calculateModel3dPrintPriceWithPackageFee($modelReplica, $printer, true, false);
        $totalModelPrice   = $this->priceCalculator->calculateModel3dPrice($modelReplica, $manufacturePrice->getCurrency());
        $totalProducePrice = MoneyMath::sum($totalProducePrice, $manufacturePrice);

        if ($manufacturePrice) {
            $item                                                                 = $this->formInvoiceItem($paymentInvoice,
                _t('site.payment', 'Manufacturing Fee'),
                _t('site.payment', 'Manufacturing payment to company'),
                '',
                $manufacturePrice->getAmount(),
                1);
            $items[1]                                                             = $item;
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2,
                'price'    => $order->getIsTestOrder() ? 0 : $manufacturePrice->getAmount(),
                'currency' => $manufacturePrice->getCurrency(),
                'item'     => $item
            ];
            $company                                                              = $order->currentAttemp->company;
            if ($order->manualSettedPs) {
                $tsBonusBalance       = BonusCalculator::calcInstantOrder($company->cashback_percent, $manufacturePrice->getAmount(), $paymentInvoice->user);
                $tsBonusBalanceFromPs = $tsBonusBalance;
                $tsBonusBalanceFromTs = $tsBonusBalance;
            } else {
                $tsBonusBalance       = BonusCalculator::calcInstantOrder($company->cashback_percent, $manufacturePrice->getAmount(), $paymentInvoice->user);
                $tsBonusBalanceFromPs = $tsBonusBalance;
                $tsBonusBalanceFromTs = 0;
            }

            if ($tsBonusBalance && ($paymentInvoice->currency !== Currency::BNS)) {
                $accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS] = [
                    'type'     => PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS,
                    'price'    => $tsBonusBalanceFromPs,
                    'currency' => Currency::BNS,
                ];
                $accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS] = [
                    'type'     => PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS,
                    'price'    => $tsBonusBalanceFromTs,
                    'currency' => Currency::BNS,
                ];
            }
        }
        if ($totalModelPrice) {
            $totalModelPrice                                   = $totalModelPrice->round();
            $items[6]                                          = $this->formInvoiceItem($paymentInvoice,
                _t('site.payment', 'Model price'),
                _t('site.payment', 'Payment to 3d model designer'),
                '',
                $totalModelPrice->getAmount(),
                6);
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_MODEL] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_MODEL,
                'price'    => $order->getIsTestOrder() ? 0 : $totalModelPrice->getAmount(),
                'currency' => $totalModelPrice->getCurrency(),
            ];
            // Model3d fee
            $model3dFee = $this->feeHelper->getModel3dFee($totalModelPrice);
            if ($model3dFee && $model3dFee->getAmount()) {
                $accounting[PaymentInvoice::ACCOUNTING_TYPE_MODEL_FEE] = [
                    'type'     => PaymentInvoice::ACCOUNTING_TYPE_MODEL_FEE,
                    'price'    => $order->getIsTestOrder() ? 0 : $model3dFee->getAmount(),
                    'currency' => $model3dFee->getCurrency(),
                ];
            }
        }
    }

    protected function formCuttingPackItem(PaymentInvoice $paymentInvoice, StoreOrderItem $orderItem, array &$items, array &$accounting, Money &$totalProducePrice)
    {
        $cuttingPack    = $orderItem->cuttingPack;
        $order          = $orderItem->order;
        $cuttingMachine = $order->currentAttemp->machine->cuttingMachine;

        $cuttingPrice = $this->cuttingCalculator->calculatePrice($cuttingMachine, $cuttingPack);
        if ($cuttingPrice) {

            $accounting[PaymentInvoice::ACCOUNTING_TYPE_CUTTING] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_CUTTING,
                'price'    => $cuttingPrice->getAmount(),
                'currency' => $cuttingPrice->getCurrency(),
            ];
            $item                                                = $this->formInvoiceItem($paymentInvoice,
                _t('site.payment', 'Fabrication cost'),
                _t('site.payment', 'Cutting manufacturing payment to company'),
                '',
                $cuttingPrice->getAmount(),
                1);

            $items[1]          = $item;
            $totalProducePrice = MoneyMath::sum($totalProducePrice, $cuttingPrice);

            $company = $order->currentAttemp->company;
            if ($order->manualSettedPs) {
                $tsBonusBalance       = BonusCalculator::calcInstantOrder($company->cashback_percent, $totalProducePrice->getAmount(), $paymentInvoice->user);
                $tsBonusBalanceFromPs = $tsBonusBalance;
                $tsBonusBalanceFromTs = $tsBonusBalance;
            } else {
                $tsBonusBalance       = BonusCalculator::calcInstantOrder($company->cashback_percent, $totalProducePrice->getAmount(), $paymentInvoice->user);
                $tsBonusBalanceFromPs = $tsBonusBalance;
                $tsBonusBalanceFromTs = 0;
            }

            if ($tsBonusBalance) {
                $accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS] = [
                    'type'     => PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS,
                    'price'    => $tsBonusBalanceFromPs,
                    'currency' => Currency::BNS,
                ];
                $accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS] = [
                    'type'     => PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS,
                    'price'    => $tsBonusBalanceFromTs,
                    'currency' => Currency::BNS,
                ];
            }
        }
    }

    protected function formOrderItemAccounting(PaymentInvoice $paymentInvoice, StoreOrderItem $orderItem, array &$items, array &$accounting, Money &$totalProducePrice)
    {
        if ($orderItem->cuttingPack) {
            $this->formCuttingPackItem($paymentInvoice, $orderItem, $items, $accounting, $totalProducePrice);
        }
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @param StoreOrder $order
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function formInvoiceItemsAndAccounting(PaymentInvoice $paymentInvoice, StoreOrder $order)
    {
        $items         = [];
        $accounting    = [];
        $deliveryPrice = $order->deliveryRate->delveiryCost->round();

        // Delivery
        if ($deliveryPrice->notZero()) {
            $item                                                 = $this->formInvoiceItem($paymentInvoice,
                _t('site.payment', 'Shipping Fee'),
                _t('site.payment', 'Payment for the delivery of your goods'),
                '',
                $order->getIsTestOrder() ? 0 : $deliveryPrice->getAmount(),
                2
            );
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_SHIPPING] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_SHIPPING,
                'price'    => $order->getIsTestOrder() ? 0 : $deliveryPrice->getAmount(),
                'currency' => $paymentInvoice->currency,
                'item'     => $item
            ];
            $items[2]                                             = $item;
        }

        // Print And Model Price
        $totalProducePrice = Money::zero($paymentInvoice->currency);
        if ($order->hasModel()) {
            $this->formInvoiceItemProduce($paymentInvoice, $order, $items, $accounting, $totalProducePrice);
        }

        foreach ($order->storeOrderItems as $orderItem) {
            $this->formOrderItemAccounting($paymentInvoice, $orderItem, $items, $accounting, $totalProducePrice);
        }

        // Get ps fee
        $psFee = $this->feeHelper->getPrintFeeFromPrice($totalProducePrice);
        if ($psFee) {
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_PS_FEE] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_PS_FEE,
                'price'    => $order->getIsTestOrder() ? 0 : $psFee->getAmount(),
                'currency' => $psFee->getCurrency(),
            ];
        }

        if (!$order->manualSettedPs && $paymentInvoice->getLinkedAffiliateAward()) {
            $this->affiliateService->invoiceAccounting($paymentInvoice, $paymentInvoice->getLinkedAffiliateAward(), $items, $accounting);
        }

        ksort($items);
        foreach ($accounting as $key => $value) {
            if (array_key_exists('item', $accounting[$key])) {
                unset($accounting[$key]['item']);
            }
        }

        return ['items' => $items, 'accounting' => $accounting];
    }

    public function formTotalStoreOrderAmount($accounting)
    {
        return ($accounting[PaymentInvoice::ACCOUNTING_TYPE_SHIPPING]['price'] ?? 0)
            + ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PACKAGE]['price'] ?? 0)
            + ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PRINT]['price'] ?? 0)
            + ($accounting[PaymentInvoice::ACCOUNTING_TYPE_MODEL]['price'] ?? 0)
            + ($accounting[PaymentInvoice::ACCOUNTING_TYPE_FEE]['price'] ?? 0)
            + ($accounting[PaymentInvoice::ACCOUNTING_TYPE_TRANSACTION]['price'] ?? 0)
            - ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PROMO_CODE]['price'] ?? 0)
            - ($accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS]['price'] ?? 0)
            + ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE]['price'] ?? 0)
            + ($accounting[PaymentInvoice::ACCOUNTING_TYPE_CUTTING]['price'] ?? 0)
            + ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE]['price'] ?? 0)
            + ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2]['price'] ?? 0);

    }

    /**
     * @param DeliveryRate $rate
     * @return bool
     */
    protected function isNeedProcessPackingFee(DeliveryRate $rate): bool
    {
        if (!$rate->packingCost || $rate->packingCost->isZero()) {
            return false;
        }

        $psDeliveryType = $rate->deliveryParams->getPsDeliveryTypeByCode($rate->deliveryForm->deliveryType);

        if ($psDeliveryType->carrier !== DeliveryType::CARRIER_TS) {
            return false;
        }

        return true;
    }

    /**
     * @param StoreOrder $order
     * @param $accounting
     * @return mixed
     */
    protected function packageInfo(StoreOrder $order, $accounting)
    {
        $packagePrice                                             = $order->getIsTestOrder() ? 0 : $order->deliveryRate->packingCost->round();
        $accounting[PaymentInvoice::ACCOUNTING_TYPE_PACKAGE_INFO] = [
            'type'     => PaymentInvoice::ACCOUNTING_TYPE_PACKAGE_INFO,
            'price'    => $packagePrice->getAmount(),
            'currency' => $packagePrice->getCurrency(),
        ];
        return [$accounting, $packagePrice];
    }
}