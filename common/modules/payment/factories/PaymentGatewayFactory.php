<?php
/**
 * User: nabi
 */

namespace common\modules\payment\factories;

use common\modules\payment\gateways\PaymentGateway;

class PaymentGatewayFactory
{
    /**
     * @param string $gatewayCode
     * @see PaymentGateway
     */
    public static function create(string $gatewayCode)
    {
    }
}
