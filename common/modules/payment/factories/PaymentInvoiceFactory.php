<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.08.18
 * Time: 10:48
 */


namespace common\modules\payment\factories;

use common\components\DateHelper;
use common\components\order\PriceCalculator;
use common\components\PaymentExchangeRateConverter;
use common\models\AffiliateSource;
use common\models\InstantPayment;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\PaymentInvoicePaymentMethod;
use common\models\PaymentTransaction;
use common\models\Preorder;
use common\models\Promocode;
use common\models\StoreOrder;
use common\models\StoreOrderPosition;
use common\models\TsInternalPurchase;
use common\models\User;
use common\modules\affiliate\factories\AffiliateAwardFactory;
use common\modules\payment\components\PaymentMethodFeeCalculator;
use common\modules\payment\exception\FatalPaymentException;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\fee\FeeHelper;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\modules\promocode\components\PromocodeDiscountCalc;
use frontend\models\user\UserFacade;
use lib\money\Currency;
use lib\money\Money;
use yii\base\BaseObject;

/**
 * Class PaymentInvoiceFactory
 *
 * @package common\modules\payment\factories
 *
 * @property PriceCalculator $priceCalculator
 * @property FeeHelper $feeHelper
 * @property PaymentExchangeRateConverter $paymentExchangeRateConverter
 * @property PaymentInvoicePreorderAccounting $preorderAccounting
 * @property PaymentInvoiceOrderAccounting $orderAccounting
 * @property PaymentInvoiceAdditionalServiceAccounting $additionalServiceAccounting
 * @property PaymentMethodFeeCalculator $paymentMethodFeeCalculator
 * @property PaymentInvoicePaymentMethodFactory $paymentInvoicePaymentMethodFactory
 */
class PaymentInvoiceFactory extends BaseObject
{
    /** @var PriceCalculator */
    protected $priceCalculator;

    /** @var FeeHelper */
    protected $feeHelper;

    /** @var PaymentExchangeRateConverter */
    protected $paymentExchangeRateConverter;

    protected $preorderAccounting;

    /** @var PaymentInvoiceInstantPaymentAccounting */
    protected $instantPaymentAccounting;

    protected $orderAccounting;

    protected $additionalServiceAccounting;

    protected $paymentMethodFeeCalculator;

    protected $paymentInvoicePaymentMethodFactory;

    /** @var PaymentInvoiceTsInternalPurchaseAccounting */
    protected $tsInternalAccounting;

    /** @var AffiliateAwardFactory */
    protected $affiliateAwardFactory;

    /** @var PaymentInvoiceRepository */
    protected $paymentInvoiceRepository;

    public function injectDependencies(
        PriceCalculator $priceCalculator,
        FeeHelper $feeHelper,
        PaymentExchangeRateConverter $paymentExchangeRateConverter,
        PaymentInvoicePreorderAccounting $preorderAccounting,
        PaymentInvoiceOrderAccounting $orderAccounting,
        PaymentInvoiceAdditionalServiceAccounting $additionalServiceAccounting,
        PaymentMethodFeeCalculator $paymentMethodFeeCalculator,
        PaymentInvoicePaymentMethodFactory $paymentInvoicePaymentMethodFactory,
        AffiliateAwardFactory $affiliateAwardFactory,
        PaymentInvoiceInstantPaymentAccounting $instantPaymentAccounting,
        PaymentInvoiceTsInternalPurchaseAccounting $tsInternalAccounting,
        PaymentInvoiceRepository $paymentInvoiceRepository
    )
    {
        $this->priceCalculator                    = $priceCalculator;
        $this->feeHelper                          = $feeHelper;
        $this->paymentExchangeRateConverter       = $paymentExchangeRateConverter;
        $this->preorderAccounting                 = $preorderAccounting;
        $this->orderAccounting                    = $orderAccounting;
        $this->additionalServiceAccounting        = $additionalServiceAccounting;
        $this->paymentMethodFeeCalculator         = $paymentMethodFeeCalculator;
        $this->paymentInvoicePaymentMethodFactory = $paymentInvoicePaymentMethodFactory;
        $this->affiliateAwardFactory              = $affiliateAwardFactory;
        $this->instantPaymentAccounting           = $instantPaymentAccounting;
        $this->tsInternalAccounting               = $tsInternalAccounting;
        $this->paymentInvoiceRepository           = $paymentInvoiceRepository;
    }

    /**
     * @param StoreOrder $order
     * @return PaymentInvoice
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function createByOrder(StoreOrder $order, ?AffiliateSource $affiliateSource = null): PaymentInvoice
    {
        /** @var PaymentInvoice $paymentInvoice */
        $paymentInvoice                  = \Yii::createObject(PaymentInvoice::class);
        $paymentInvoice->uuid            = PaymentInvoice::generateUuid();
        $paymentInvoice->created_at      = DateHelper::now();
        $paymentInvoice->date_expire     = DateHelper::addNowSec(PaymentInvoice::EXPIRE_TIME);
        $paymentInvoice->currency        = $order->currentAttemp->company->currency;
        $paymentInvoice->status          = PaymentInvoice::STATUS_NEW;
        $paymentInvoice->user_id         = $order->user_id;
        $paymentInvoice->user_session_id = $order->user_session_id;
        $paymentInvoice->store_order_id  = $order->id;
        $paymentInvoice->populateRelation('storeOrder', $order);
        if ($affiliateSource) {
            $affiliateAward = $this->affiliateAwardFactory->create($affiliateSource, $paymentInvoice);
            $paymentInvoice->populateRelation('affiliateAward', $affiliateAward);
        }

        $invoiceItemsAndAccounting = $this->orderAccounting->formInvoiceItemsAndAccounting($paymentInvoice, $order);

        $paymentInvoice->populateRelation('paymentInvoiceItems', $invoiceItemsAndAccounting['items']);
        $paymentInvoice->accounting   = $invoiceItemsAndAccounting['accounting'];
        $paymentInvoice->total_amount = $this->orderAccounting->formTotalStoreOrderAmount($paymentInvoice->accounting);
        $paymentInvoice->details      = _t('store.order', 'Invoice for order') . ': ' . $order->getTitle();
        $paymentInvoice->rate_id      = $this->paymentExchangeRateConverter->getLastPaymentExchangeRate()->id;
        $paymentInvoice->company_id   = null;

        return $paymentInvoice;
    }

    /**
     * @param StoreOrder $storeOrder
     *
     * @param AffiliateSource|null $affiliateSource
     * @return PaymentInvoice
     * @throws \yii\base\InvalidConfigException
     */
    public function createWithPaymentMethodsByOrder(StoreOrder $storeOrder, ?AffiliateSource $affiliateSource = null): PaymentInvoice
    {
        $baseInvoice = $this->createByOrder($storeOrder, $affiliateSource);

        $baseInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($baseInvoice, [
                PaymentTransaction::VENDOR_TS,
                PaymentTransaction::VENDOR_BONUS,
                PaymentTransaction::VENDOR_BANK_TRANSFER,
                PaymentTransaction::VENDOR_THINGIVERSE,
                PaymentTransaction::VENDOR_BRAINTREE,
                PaymentTransaction::VENDOR_STRIPE
            ])
        );

        return $baseInvoice;
    }

    /**
     * @param Preorder $preorder
     *
     * @return PaymentInvoice
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     * @throws \Exception
     */
    public function createByPreorder(Preorder $preorder): PaymentInvoice
    {
        /** @var PaymentInvoice $paymentInvoice */
        $paymentInvoice                  = \Yii::createObject(PaymentInvoice::class);
        $paymentInvoice->uuid            = PaymentInvoice::generateUuid();
        $paymentInvoice->created_at      = DateHelper::now();
        $paymentInvoice->date_expire     = DateHelper::addNowSec(PaymentInvoice::EXPIRE_TIME);
        $paymentInvoice->currency        = $preorder->currency;
        $paymentInvoice->status          = PaymentInvoice::STATUS_NEW;
        $paymentInvoice->user_id         = $preorder->user_id;
        $paymentInvoice->user_session_id = UserFacade::getUserSession()->id ?? 1;
        $paymentInvoice->preorder_id     = $preorder->id;

        $invoiceItemsAndAccounting = $this->preorderAccounting->formInvoiceItemsAndAccounting($paymentInvoice, $preorder);

        $paymentInvoice->populateRelation('paymentInvoiceItems', $invoiceItemsAndAccounting['items']);
        $paymentInvoice->accounting       = $invoiceItemsAndAccounting['accounting'];
        $paymentInvoice->total_amount     = $this->preorderAccounting->formTotalAmount($paymentInvoice->accounting);
        $paymentInvoice->details          = _t('store.order', 'Invoice for preorder: {preorderId}', ['preorderId' => $preorder->id]);
        $paymentInvoice->rate_id          = $this->paymentExchangeRateConverter->getLastPaymentExchangeRate()->id;
        $paymentInvoice->company_id       = $preorder->company->id;
        $paymentInvoice->is_settle_submit = 1;

        return $paymentInvoice;
    }

    /**
     * @param InstantPayment $instantPayment
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     */
    public function createByInstantPayment(InstantPayment $instantPayment)
    {
        /** @var PaymentInvoice $paymentInvoice */
        $paymentInvoice                       = \Yii::createObject(PaymentInvoice::class);
        $paymentInvoice->uuid                 = PaymentInvoice::generateUuid();
        $paymentInvoice->created_at           = DateHelper::now();
        $paymentInvoice->date_expire          = DateHelper::addNowSec(PaymentInvoice::EXPIRE_TIME);
        $paymentInvoice->currency             = $instantPayment->currency;
        $paymentInvoice->status               = PaymentInvoice::STATUS_NEW;
        $paymentInvoice->user_id              = $instantPayment->from_user_id;
        $paymentInvoice->user_session_id      = 1;
        $paymentInvoice->instant_payment_uuid = $instantPayment->uuid;
        $paymentInvoice->user_session_id      = 1;

        $invoiceItemsAndAccounting = $this->instantPaymentAccounting->formInvoiceItemsAndAccounting($paymentInvoice, $instantPayment);

        $paymentInvoice->populateRelation('paymentInvoiceItems', $invoiceItemsAndAccounting['items']);
        $paymentInvoice->accounting       = $invoiceItemsAndAccounting['accounting'];
        $paymentInvoice->total_amount     = $this->instantPaymentAccounting->formTotalAmount($paymentInvoice->accounting);
        $paymentInvoice->details          = _t('store.order', 'Invoice for instant payment: {uuid}', ['uuid' => $instantPayment->uuid]);
        $paymentInvoice->rate_id          = $this->paymentExchangeRateConverter->getLastPaymentExchangeRate()->id;
        $paymentInvoice->company_id       = $instantPayment->toUser->company->id;
        $paymentInvoice->is_settle_submit = 0;  // Authorization on approve payment

        return $paymentInvoice;
    }

    public function createByInstantPaymentWithPaymentMethodFee(InstantPayment $instantPayment, string $paymentMethod): PaymentInvoice
    {
        $paymentInvoice = $this->createByInstantPayment($instantPayment);

        $paymentInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($paymentInvoice, [$paymentMethod])
        );

        if ($paymentInvoice->getAmountTotal()->getAmount() >= 100) {
            $paymentMethodFee = $this->paymentMethodFeeCalculator->calcFee($paymentInvoice, $paymentMethod);

            if ($paymentMethodFee) {
                $this->addAccountingPaymentMethodFee($paymentInvoice, $paymentMethodFee);
            }
        }

        return $paymentInvoice;
    }

    /**
     * @param Preorder $preorder
     * @param string $paymentMethod
     *
     * @return PaymentInvoice
     * @throws PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     */
    public function createByPreorderWithPaymentMethodFee(Preorder $preorder, string $paymentMethod): PaymentInvoice
    {
        $paymentInvoice = $this->createByPreorder($preorder);

        $paymentInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($paymentInvoice, [$paymentMethod])
        );

        if ($paymentInvoice->getAmountTotal()->getAmount() >= 100) {
            $paymentMethodFee = $this->paymentMethodFeeCalculator->calcFee($paymentInvoice, $paymentMethod);

            if ($paymentMethodFee) {
                $this->addAccountingPaymentMethodFee($paymentInvoice, $paymentMethodFee);
            }
        }

        return $paymentInvoice;
    }

    /**
     * @param StoreOrderPosition $additionalService
     *
     * @return PaymentInvoice
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     * @throws \Exception
     */
    public function createByAdditionalService(StoreOrderPosition $additionalService): PaymentInvoice
    {
        /** @var PaymentInvoice $paymentInvoice */
        $paymentInvoice                    = \Yii::createObject(PaymentInvoice::class);
        $paymentInvoice->uuid              = PaymentInvoice::generateUuid();
        $paymentInvoice->created_at        = DateHelper::now();
        $paymentInvoice->date_expire       = DateHelper::addNowSec(PaymentInvoice::EXPIRE_TIME);
        $paymentInvoice->currency          = $additionalService->order->getCurrency();
        $paymentInvoice->status            = PaymentInvoice::STATUS_NEW;
        $paymentInvoice->user_id           = $additionalService->order->user_id;
        $paymentInvoice->user_session_id   = $additionalService->order->user_session_id;
        $paymentInvoice->store_order_id    = $additionalService->order->id;
        $paymentInvoice->order_position_id = $additionalService->id;

        $invoiceItemsAndAccounting = $this->additionalServiceAccounting->formInvoiceItemsAndAccounting($paymentInvoice, $additionalService);

        $paymentInvoice->populateRelation('paymentInvoiceItems', $invoiceItemsAndAccounting['items']);
        $paymentInvoice->accounting       = $invoiceItemsAndAccounting['accounting'];
        $paymentInvoice->total_amount     = $this->additionalServiceAccounting->formTotalAmount($paymentInvoice->accounting);
        $paymentInvoice->details          = _t('store.order', 'Additional service invoice for order: #{orderId}', ['orderId' => $additionalService->order->id]);
        $paymentInvoice->rate_id          = $this->paymentExchangeRateConverter->getLastPaymentExchangeRate()->id;
        $paymentInvoice->company_id       = $additionalService->user->company->id ?? null;
        $paymentInvoice->is_settle_submit = 1;

        return $paymentInvoice;
    }

    /**
     * @param StoreOrderPosition $additionalService
     *
     * @param string $paymentMethod
     *
     * @return PaymentInvoice
     * @throws PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     */
    public function createByAdditionalServiceWithPaymentMethodFee(StoreOrderPosition $additionalService, string $paymentMethod): PaymentInvoice
    {
        $paymentInvoice = $this->createByAdditionalService($additionalService);

        $paymentInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($paymentInvoice, [$paymentMethod])
        );

        if ($paymentInvoice->getAmountTotal()->getAmount() >= 100) {
            $paymentMethodFee = $this->paymentMethodFeeCalculator->calcFee($paymentInvoice, $paymentMethod);

            if ($paymentMethodFee) {
                $this->addAccountingPaymentMethodFee($paymentInvoice, $paymentMethodFee);
            }
        }

        return $paymentInvoice;
    }

    /**
     * @param PaymentInvoice $invoice
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    protected function createByPaymentInvoice(PaymentInvoice $invoice): PaymentInvoice
    {
        /** @var PaymentInvoice $paymentInvoice */
        $newInvoice = \Yii::createObject(PaymentInvoice::class);

        $newInvoice->setAttributes($invoice->getAttributes());

        $newInvoice->uuid                = PaymentInvoice::generateUuid();
        $newInvoice->created_at          = DateHelper::now();
        $newInvoice->date_expire         = DateHelper::addNowSec(PaymentInvoice::EXPIRE_TIME);
        $newInvoice->currency            = $invoice->currency;
        $newInvoice->status              = PaymentInvoice::STATUS_NEW;
        $newInvoice->parent_invoice_uuid = $invoice->uuid;

        $newItems           = [];
        $relinkInvoiceItems = [];

        foreach ($invoice->paymentInvoiceItems as $k => $item) {
            $newItem = new PaymentInvoiceItem();
            $newItem->setAttributes($item->getAttributes());
            $newUiid                            = PaymentInvoiceItem::generateUuid();
            $relinkInvoiceItems[$item->uuid] = $newUiid;
            $newItem->uuid                      = $newUiid;
            $newItem->payment_invoice_uuid      = $newInvoice->uuid;
            $newItems[]                         = $newItem;
        }

        $newMethods = [];

        foreach ($invoice->paymentInvoicePaymentMethods as $method) {
            $newMethod = new PaymentInvoicePaymentMethod();
            $newMethod->setAttributes($method->getAttributes());
            $newMethod->payment_invoice_uuid = $newInvoice->uuid;
            $newMethods[]                    = $newMethod;
        }

        if ($newInvoice->baseByPreorder() || $newInvoice->baseByAdditionalService()) {
            $newInvoice->accounting = $this->relinkAccountingWithInvoiceItems($newInvoice->accounting, $relinkInvoiceItems);
        }

        $newInvoice->populateRelation('paymentInvoiceItems', $newItems);
        $newInvoice->populateRelation('paymentInvoicePaymentMethods', $newMethods);

        return $newInvoice;
    }

    /**
     * @param TsInternalPurchase $tsInternalPurchase
     */
    public function createByTsInternalPurchase(TsInternalPurchase $tsInternalPurchase): PaymentInvoice
    {
        $paymentInvoice = $this->createTsInternalPurchase($tsInternalPurchase, Currency::USD);

        $invoiceItemsAndAccounting = $this->tsInternalAccounting->formInvoiceItemsAndAccounting($paymentInvoice, $tsInternalPurchase);

        $paymentInvoice->populateRelation('paymentInvoiceItems', $invoiceItemsAndAccounting['items']);
        $paymentInvoice->accounting       = $invoiceItemsAndAccounting['accounting'];
        $paymentInvoice->total_amount     = $this->tsInternalAccounting->formTotalAmountWithType($paymentInvoice->accounting, PaymentInvoice::ACCOUNTING_TYPE_TS_INTERNAL_PURCHASE_CERTIFICATE);
        $paymentInvoice->details          = _t('store.order', 'Invoice for internal treatstock purchase: {uid}', ['uid' => $tsInternalPurchase->uid]);
        $paymentInvoice->rate_id          = $this->paymentExchangeRateConverter->getLastPaymentExchangeRate()->id;
        $paymentInvoice->is_settle_submit = 1;

        $tsInternalPurchase->populateRelation('primaryInvoice', $paymentInvoice);
        return $paymentInvoice;
    }

    /**
     * @param TsInternalPurchase $tsInternalPurchase
     * @return PaymentInvoice
     */
    public function createWithPaymentMethodsByTsInternalPurchase(TsInternalPurchase $tsInternalPurchase): PaymentInvoice
    {
        $baseInvoice = $this->createByTsInternalPurchase($tsInternalPurchase);

        $baseInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($baseInvoice, [
                PaymentTransaction::VENDOR_TS,
                PaymentTransaction::VENDOR_BONUS,
                PaymentTransaction::VENDOR_BANK_TRANSFER,
                PaymentTransaction::VENDOR_THINGIVERSE,
                PaymentTransaction::VENDOR_BRAINTREE,
                PaymentTransaction::VENDOR_STRIPE
            ])
        );

        return $baseInvoice;
    }

    /**
     * @param PaymentInvoice $invoice
     *
     * @return PaymentInvoice
     * @throws PaymentException
     * @throws \yii\base\InvalidConfigException
     */
    public function createByPaymentInvoiceWithoutPaymentMethodFee(PaymentInvoice $invoice): PaymentInvoice
    {
        /**
         * @var PaymentInvoice $newInvoice
         * @var array $relinkInvoiceItems
         */
        $newInvoice = $this->createByPaymentInvoice($invoice);

        if (!isset($newInvoice->accounting[PaymentInvoice::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE])) {
            return $newInvoice;
        }

        $accounting = $newInvoice->accounting;

        unset($accounting[PaymentInvoice::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE]);

        $newInvoice->accounting   = $accounting;
        $newInvoice->total_amount = $this->calcTotalAmount($newInvoice);

        return $newInvoice;
    }

    /**
     * @param PaymentInvoice $invoice
     * @param Promocode $promoCode
     *
     * @return PaymentInvoice
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function createByPaymentInvoicePromoCode(PaymentInvoice $invoice, Promocode $promoCode): PaymentInvoice
    {
        /**
         * @var PaymentInvoice $newInvoice
         * @var array $relinkInvoiceItems
         */
        $newInvoice = $this->createByPaymentInvoice($invoice);
        $this->createAndAddItemByPromoCode($newInvoice, $promoCode);

        return $newInvoice;
    }

    /**
     * @param $accounting
     * @param $relinkData
     *
     * @return array
     */
    protected function relinkAccountingWithInvoiceItems($accounting, $relinkData): array
    {
        $items = $accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS];

        foreach ($items as $k => $item) {
            if (array_key_exists($k, $relinkData)) {
                $item['paymentInvoiceItemUuid'] = $relinkData[$k];
                $items[$relinkData[$k]]         = $item;
                unset($items[$k]);
            }
        }

        $accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS] = $items;

        return $accounting;
    }

    /**
     * Calculate and get the total amount
     *
     * @param PaymentInvoice $invoice
     *
     * @return float
     * @throws PaymentException
     */
    protected function calcTotalAmount(PaymentInvoice $invoice): float
    {
        if ($invoice->baseByPrintOrder() || $invoice->baseByCuttingOrder()) {
            return $this->orderAccounting->formTotalStoreOrderAmount($invoice->accounting);
        } elseif ($invoice->baseByPreorder()) {
            return $this->preorderAccounting->formTotalAmount($invoice->accounting);
        } elseif ($invoice->baseByAdditionalService()) {
            return $this->additionalServiceAccounting->formTotalAmount($invoice->accounting);
        } elseif ($invoice->baseByInstantPayment()) {
            return $this->instantPaymentAccounting->formTotalAmount($invoice->accounting);
        }

        throw new FatalPaymentException(_t('site.payment', 'Error when calculating the total amount.'));
    }

    /**
     * @param PaymentInvoice $invoice
     * @param Promocode $promoCode
     *
     * @return PaymentInvoice
     * @throws \Exception
     */
    protected function createAndAddItemByPromoCode(PaymentInvoice $invoice, Promocode $promoCode): PaymentInvoice
    {
        if ($invoice->currency !== $promoCode->discount_currency) {
            throw new PaymentException('Invalid promocode currency');
        }

        $discountsTotal = PromocodeDiscountCalc::getDiscountsTotal($promoCode, $invoice);

        $newItems = $invoice->paymentInvoiceItems;

        $promocodeItem                       = new PaymentInvoiceItem();
        $promocodeItem->uuid                 = PaymentInvoiceItem::generateUuid();
        $promocodeItem->payment_invoice_uuid = $invoice->uuid;
        $promocodeItem->pos                  = \count($newItems) + 11;
        $promocodeItem->title                = $promoCode->code;
        $promocodeItem->description          = $promoCode->description;
        $promocodeItem->measure              = '';
        $promocodeItem->qty                  = 1;
        $promocodeItem->total_line           = -$discountsTotal->getAmount();

        $newItems   = $invoice->paymentInvoiceItems;
        $newItems[] = $promocodeItem;

        $invoice->populateRelation('paymentInvoiceItems', $newItems);

        $accounting = $invoice->accounting;

        $accounting[PaymentInvoice::ACCOUNTING_TYPE_PROMO_CODE] = [
            'fromUserId' => User::USER_TS,
            'type'       => PaymentInvoice::ACCOUNTING_TYPE_PROMO_CODE,
            'price'      => $discountsTotal->getAmount(),
            'currency'   => $discountsTotal->getCurrency(),
        ];

        $invoice->accounting = $accounting;

        $invoice->total_amount = $this->calcTotalAmount($invoice);

        return $invoice;
    }


    public function createByPaymentInvoiceBonus(PaymentInvoice $invoice): PaymentInvoice
    {
        /**
         * @var PaymentInvoice $newInvoice
         * @var array $relinkInvoiceItems
         */
        $newInvoice = $this->createByPaymentInvoice($invoice);
        $this->createAndAddItemByBonus($newInvoice);
        $newInvoice->resetAccountingBonusAccured();


        $newInvoice->is_settle_submit = 1;
        $newInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($newInvoice, [
                PaymentTransaction::VENDOR_BONUS
            ])
        );

        return $newInvoice;
    }

    protected function createAndAddItemByBonus(PaymentInvoice $invoice): PaymentInvoice
    {
        $newItems = $invoice->paymentInvoiceItems;

        $totalAmount = $invoice->getAmountTotal();

        $invoiceItem                       = new PaymentInvoiceItem();
        $invoiceItem->uuid                 = PaymentInvoiceItem::generateUuid();
        $invoiceItem->payment_invoice_uuid = $invoice->uuid;
        $invoiceItem->pos                  = \count($newItems) + 10;
        $invoiceItem->title                = 'Discount (bonus)';
        $invoiceItem->description          = 'Discount (treatstock bonus)';
        $invoiceItem->measure              = '';
        $invoiceItem->qty                  = 1;
        $invoiceItem->total_line           = -$totalAmount->getAmount();

        $newItems   = $invoice->paymentInvoiceItems;
        $newItems[] = $invoiceItem;

        $invoice->populateRelation('paymentInvoiceItems', $newItems);

        $accounting = $invoice->accounting;

        $accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS] = [
            'type'     => PaymentInvoice::ACCOUNTING_TYPE_BONUS,
            'price'    => $totalAmount->getAmount(),
            'currency' => $invoice->currency
        ];

        $invoice->accounting = $accounting;

        $invoice->total_amount = $this->calcTotalAmount($invoice);

        return $invoice;
    }

    /**
     * @param PaymentInvoice $invoice
     * @param Money $paymentMethodFee
     *
     * @return PaymentInvoice
     * @throws PaymentException
     */
    protected function addAccountingPaymentMethodFee(PaymentInvoice $invoice, Money $paymentMethodFee): PaymentInvoice
    {
        return $invoice;
        if (isset($invoice->accounting[PaymentInvoice::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE])) {
            return $invoice;
        }

        $accounting = $invoice->accounting;

        $accounting[PaymentInvoice::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE] = [
            'type'     => PaymentInvoice::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE,
            'price'    => $paymentMethodFee->getAmount(),
            'currency' => $invoice->currency,
        ];

        $invoice->accounting   = $accounting;
        $invoice->total_amount = $this->calcTotalAmount($invoice);

        return $invoice;
    }

    protected function createByTsInternalForDeposit(TsInternalPurchase $tsInternalPurchase, Money $money)
    {
        $paymentInvoice = $this->createTsInternalPurchase($tsInternalPurchase, $money->getCurrency());
        [$paymentInvoiceItem, $accounting] = $this->tsInternalAccounting->formInvoiceItemsAndAccountingForBalance($paymentInvoice, $tsInternalPurchase, $money);

        $paymentInvoice->populateRelation('paymentInvoiceItems', $paymentInvoiceItem);
        $paymentInvoice->accounting       = $accounting;
        $paymentInvoice->total_amount     = $this->tsInternalAccounting->formTotalAmountWithType($paymentInvoice->accounting, PaymentInvoice::ACCOUNTING_TYPE_TS_INTERNAL_PURCHASE_DEPOSIT);
        $paymentInvoice->details          = _t('store.order', 'Invoice for internal treatstock purchase: {uid}', ['uid' => $tsInternalPurchase->uid]);
        $paymentInvoice->rate_id          = $this->paymentExchangeRateConverter->getLastPaymentExchangeRate()->id;
        $paymentInvoice->is_settle_submit = 1;

        $tsInternalPurchase->populateRelation('primaryInvoice', $paymentInvoice);
        return $paymentInvoice;
    }

    public function createWithPaymentMethodsByForDeposit(TsInternalPurchase $tsInternalPurchase, Money $money)
    {
        $baseInvoice                     = $this->createByTsInternalForDeposit($tsInternalPurchase, $money);
        $baseInvoice->invoice_group_uuid = $baseInvoice->uuid;
        $baseInvoice->populateRelation(
            'paymentInvoicePaymentMethods',
            $this->paymentInvoicePaymentMethodFactory->createMethods($baseInvoice, [
                PaymentTransaction::VENDOR_BANK_TRANSFER
            ])
        );

        return $baseInvoice;
    }

    /**
     * @param TsInternalPurchase $tsInternalPurchase
     * @return PaymentInvoice
     * @throws \yii\base\InvalidConfigException
     */
    protected function createTsInternalPurchase(TsInternalPurchase $tsInternalPurchase, string $currency): PaymentInvoice
    {
        /** @var PaymentInvoice $paymentInvoice */
        $paymentInvoice = \Yii::createObject(PaymentInvoice::class);

        $paymentInvoice->uuid                     = PaymentInvoice::generateUuid();
        $paymentInvoice->created_at               = DateHelper::now();
        $paymentInvoice->date_expire              = DateHelper::addNowSec(PaymentInvoice::EXPIRE_TIME);
        $paymentInvoice->currency                 = $currency;
        $paymentInvoice->status                   = PaymentInvoice::STATUS_NEW;
        $paymentInvoice->company_id               = User::findByPk(1)->company->id;
        $paymentInvoice->user_id                  = $tsInternalPurchase->user_id;
        $paymentInvoice->user_session_id          = 1;
        $paymentInvoice->ts_internal_purchase_uid = $tsInternalPurchase->uid;
        $paymentInvoice->user_session_id          = 1;
        return $paymentInvoice;
    }
}