<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 10.12.18
 * Time: 11:55
 */

namespace common\modules\payment\factories;

use common\models\PaymentInvoice;
use common\models\PaymentInvoicePaymentMethod;
use yii\base\BaseObject;

/**
 * Class PaymentInvoicePaymentMethodFactory
 * @package common\modules\payment\factories
 */
class PaymentInvoicePaymentMethodFactory extends BaseObject
{
    /**
     * @param PaymentInvoice $paymentInvoice
     * @param array $methods
     *
     * @return PaymentInvoicePaymentMethod[]
     */
    public function createMethods(PaymentInvoice $paymentInvoice, array $methods): array
    {
        $paymentMethods = [];

        foreach ($methods as $method) {
            $paymentMethod = new PaymentInvoicePaymentMethod();
            $paymentMethod->payment_invoice_uuid = $paymentInvoice->uuid;
            $paymentMethod->vendor = $method;
            $paymentMethods[] = $paymentMethod;
        }

        return $paymentMethods;
    }
}