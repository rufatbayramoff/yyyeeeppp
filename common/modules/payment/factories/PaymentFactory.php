<?php

namespace common\modules\payment\factories;

use common\components\DateHelper;
use common\models\Payment;

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.10.18
 * Time: 10:17
 */
class PaymentFactory
{
    /**
     * @param \common\models\PaymentInvoice $invoice
     *
     * @return Payment
     */
    public function createByInvoice(\common\models\PaymentInvoice $invoice): Payment
    {
        $payment = new Payment();
        $payment->payment_invoice_uuid = $invoice->uuid;
        $payment->created_at = DateHelper::now();
        $payment->description = 'Payment for invoice: ' . $invoice->uuid;
        $payment->status = Payment::STATUS_NEW;
        return $payment;
    }

    /**
     * Create payment without invoice
     *
     * @param string|null $description
     *
     * @return Payment
     */
    public function createPayment($description = null): Payment
    {
        $payment = new Payment();
        $payment->payment_invoice_uuid = null;
        $payment->created_at = DateHelper::now();
        $payment->description = $description;
        $payment->status = Payment::STATUS_NEW;
        return $payment;
    }
}