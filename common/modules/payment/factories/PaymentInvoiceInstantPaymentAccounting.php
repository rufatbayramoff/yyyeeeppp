<?php

namespace common\modules\payment\factories;

use common\models\InstantPayment;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\StoreOrderPosition;
use common\modules\payment\components\BonusCalculator;
use common\modules\payment\fee\FeeHelper;
use lib\money\Currency;
use lib\money\Money;
use yii\base\BaseObject;

class PaymentInvoiceInstantPaymentAccounting extends BaseObject
{
    protected $feeHelper;

    public function injectDependencies(
        FeeHelper $feeHelper
    )
    {
        $this->feeHelper = $feeHelper;
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @param InstantPayment $instantPayment
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function formInvoiceItemsAndAccounting(PaymentInvoice $paymentInvoice, InstantPayment $instantPayment): array
    {
        $calculateFee = $this->calculateFee($instantPayment->getAmount());

        $paymentInvoiceItem                       = new PaymentInvoiceItem();
        $paymentInvoiceItem->uuid                 = PaymentInvoiceItem::generateUuid();
        $paymentInvoiceItem->payment_invoice_uuid = $paymentInvoice->uuid;
        $paymentInvoiceItem->pos                  = 1;
        $paymentInvoiceItem->title                = 'Instant payment: ' . $instantPayment->uuid;
        $paymentInvoiceItem->description          = $instantPayment->descr;
        $paymentInvoiceItem->measure              = '';
        $paymentInvoiceItem->qty                  = 1;
        $paymentInvoiceItem->total_line           = $instantPayment->sum;

        $items = [
            $this->formInvoiceItem($paymentInvoice,
                _t('site.payment', 'Instant payment'),
                _t('site.payment', $instantPayment->descr),
                '',
                $instantPayment->sum,
                1)
        ];

        $accounting[PaymentInvoice::ACCOUNTING_TYPE_INSTANT_PAYMENT] = [
            'type'  => PaymentInvoice::ACCOUNTING_TYPE_INSTANT_PAYMENT,
            'price' => $instantPayment->sum,
        ];
        $accounting[PaymentInvoice::ACCOUNTING_TYPE_INSTANT_PAYMENT_FEE_IN]             = [
            'type'  => PaymentInvoice::ACCOUNTING_TYPE_INSTANT_PAYMENT_FEE_IN,
            'price' => $calculateFee,
        ];

        $company   = $instantPayment->toUser->company;
        if ($company?->cashback_percent  && ($paymentInvoice->currency!==Currency::BNS)) {
            $tsBonusBalance = BonusCalculator::calcQuote($company, $instantPayment->sum, $paymentInvoice->user);

            $accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_PS,
                'price'    => $tsBonusBalance,
                'currency' => Currency::BNS,
            ];
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_BONUS_ACCRUED_FROM_TS,
                'price'    => $tsBonusBalance,
                'currency' => Currency::BNS,
            ];
        }



        return [
            'items'      => $items,
            'accounting' => $accounting
        ];
    }

    /**
     * @throws \Exception
     */
    public function formInvoiceItem($paymentInvoice, $title, $description, $qty, $cost, $pos, $measure = '')
    {
        $paymentInvoiceItem                       = new PaymentInvoiceItem();
        $paymentInvoiceItem->uuid                 = PaymentInvoiceItem::generateUuid();
        $paymentInvoiceItem->payment_invoice_uuid = $paymentInvoice->uuid;
        $paymentInvoiceItem->pos                  = $pos;
        $paymentInvoiceItem->title                = $title;
        $paymentInvoiceItem->description          = $description;
        $paymentInvoiceItem->measure              = $measure;
        $paymentInvoiceItem->qty                  = $qty;
        $paymentInvoiceItem->total_line           = $cost;
        return $paymentInvoiceItem;
    }

    /**
     * @param $accounting
     *
     * @return float
     */
    public function formTotalAmount($accounting): float
    {
        $totalAmount = $accounting[PaymentInvoice::ACCOUNTING_TYPE_INSTANT_PAYMENT]['price'];
        $totalAmount += ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE]['price'] ?? 0);

        return $totalAmount;
    }

    /**
     * @param $amount
     *
     * @return float
     * @throws \yii\base\InvalidConfigException
     */
    protected function calculateFee($amount): float
    {
        $feePrice = $this->feeHelper->getInstantPaymentFee($amount);
        return $feePrice->getAmount();
    }
}