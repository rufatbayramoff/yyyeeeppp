<?php

namespace common\modules\payment\factories;

use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\StoreOrderPosition;
use common\modules\payment\fee\FeeHelper;
use lib\money\Money;
use yii\base\BaseObject;

/**
 * Class PaymentInvoiceFactory
 * @package common\modules\payment\factories
 *
 * @property FeeHelper $feeHelper
 */
class PaymentInvoiceAdditionalServiceAccounting extends BaseObject
{
    protected $feeHelper;

    public function injectDependencies(
        FeeHelper $feeHelper
    ) {
        $this->feeHelper = $feeHelper;
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @param StoreOrderPosition $additionalService
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function formInvoiceItemsAndAccounting(PaymentInvoice $paymentInvoice, StoreOrderPosition $additionalService): array
    {
        $items = [];
        $accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS] = [];

        $paymentInvoiceItem = new PaymentInvoiceItem();
        $paymentInvoiceItem->uuid = PaymentInvoiceItem::generateUuid();
        $paymentInvoiceItem->payment_invoice_uuid = $paymentInvoice->uuid;
        $paymentInvoiceItem->pos =1;
        $paymentInvoiceItem->title = $additionalService->title;
        $paymentInvoiceItem->description =  _t('store.order', 'Additional service invoice for order: #{orderId}', ['orderId' => $additionalService->order->id]);
        $paymentInvoiceItem->measure = '';
        $paymentInvoiceItem->qty = 1;
        $paymentInvoiceItem->total_line = $additionalService->amount;
        $items[] = $paymentInvoiceItem;

        $calculateFee = $this->calculateFee($paymentInvoiceItem->total_line);

        $accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS][$paymentInvoiceItem->uuid] = [
            'additionalServiceId'                        => $additionalService->id,
            'paymentInvoiceItemUuid'                     => $paymentInvoiceItem->uuid,
            PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER => [
                'type'  => PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER,
                'price' => $paymentInvoiceItem->total_line - $calculateFee,
            ],
            PaymentInvoice::ACCOUNTING_TYPE_FEE          => [
                'type'  => PaymentInvoice::ACCOUNTING_TYPE_FEE,
                'price' => $calculateFee,
            ]
        ];

        return [
            'items'      => $items,
            'accounting' => $accounting
        ];
    }

    /**
     * @param $accounting
     *
     * @return float
     */
    public function formTotalAmount($accounting): float
    {
        $totalAmount = 0;

        foreach ($accounting[PaymentInvoice::ACCOUNTING_TYPE_ITEMS] as $item) {
            $totalAmount += ($item[PaymentInvoice::ACCOUNTING_TYPE_MANUFACTURER]['price'] ?? 0)
                +($item[PaymentInvoice::ACCOUNTING_TYPE_FEE]['price'] ?? 0);
        }

        $totalAmount -= ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PROMO_CODE]['price'] ?? 0);
        $totalAmount -= ($accounting[PaymentInvoice::ACCOUNTING_TYPE_BONUS]['price'] ?? 0);

        $totalAmount += ($accounting[PaymentInvoice::ACCOUNTING_TYPE_PAYMENT_METHOD_FEE]['price'] ?? 0);

        return $totalAmount;
    }

    /**
     * @param $amount
     *
     * @return float
     * @throws \yii\base\InvalidConfigException
     */
    protected function calculateFee($amount): float
    {
        $feePrice = $this->feeHelper->getManufacturingFee(Money::usd($amount));
        return $feePrice->getAmount();
    }
}