<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.06.17
 * Time: 15:01
 */

namespace common\modules\intlDomains;

use common\components\BaseModule;
use common\components\UrlHelper;
use common\modules\intlDomains\components\DomainManager;

/**
 * Class IntlDomainsModule
 *
 * @property DomainManager domainManager
 * @package common\modules\intlDomains
 */
class IntlDomainsModule extends BaseModule
{
    public $mainDomain = '';
    public $allowedDomains = [];
    public $langDomainsMap = [];
    public $defaultLocationsMap = [];
    public $allowedHeaders = '';
    public $detectLanguage = true;

    protected function getComponentsList()
    {
        return [
            'domainManager' => DomainManager::class
        ];
    }


    /**
     * @throws \yii\base\InvalidParamException
     */
    protected function initConfig()
    {

    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();

        $this->initConfig();
        $componentsList = $this->getComponentsList();
        $this->setComponents($componentsList);
        $this->initComponentsModule($componentsList);
    }
}