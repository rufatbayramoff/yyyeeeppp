<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.06.17
 * Time: 17:01
 */

namespace common\modules\intlDomains\components;

use common\components\UrlHelper;
use common\modules\intlDomains\IntlDomainsModule;
use Yii;
use yii\base\Component;
use yii\base\Module;

class DomainManager extends Component
{
    /**
     * @var IntlDomainsModule
     */
    protected $module;

    public function setModule(Module $module)
    {
        $this->module = $module;
    }


    public function getDomainForLang($langIso)
    {
        return $this->module->langDomainsMap[$langIso]??$this->module->mainDomain;
    }

    public function getUrlWithLangDomain($url, $langIso, $autoRestoreSession=true): string
    {
        $langDomainMap = $this->module->langDomainsMap;
        $addRestoreSession = false;
        if (array_key_exists($langIso, $langDomainMap)) {
            $langDomain = $langDomainMap[$langIso];
            if ($langIso !== 'en-US') {
                $addRestoreSession = true;
            }
        } else {
            $langDomain = Yii::$app->getModule('intlDomains')->mainDomain;
        }

        $redirectUrl = UrlHelper::replaceDomain($url, $langDomain);
        if ($addRestoreSession && $autoRestoreSession) {
            // Add save session parameter
            $redirectUrl = Yii::$app->sessionManager->addRestoreSessionFlagToUrl($redirectUrl);
        }
        return $redirectUrl;
    }

    public function getDefaultLangIsoForDomain($domain)
    {
        $domainLangMap = array_flip($this->module->langDomainsMap);
        return $domainLangMap[$domain]??'';
    }

    public function getDefaultIpLocationForDomain($domain)
    {
        if (array_key_exists($domain, $this->module->defaultLocationsMap)) {
            return $this->module->defaultLocationsMap[$domain];
        }
        return null;
    }

    public function isMainDomainNow()
    {
        if ($_SERVER['HTTP_HOST'] === $this->module->mainDomain) {
            return true;
        }
        return false;
    }

    public function sendAllowAjaxIntlDomainHeaders()
    {
        if (array_key_exists('HTTP_ORIGIN', $_SERVER) && $_SERVER['HTTP_ORIGIN']) {
            $originDomain = UrlHelper::getDomainFromUrl($_SERVER['HTTP_ORIGIN']);
            if ($originDomain && in_array($originDomain, $this->module->allowedDomains)) {
                header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            }
        }
        header('Access-Control-Allow-Methods: DELETE, HEAD, GET, OPTIONS, POST, PUT');
        header('Access-Control-Allow-Headers: ' . $this->module->allowedHeaders);
        header('Access-Control-Max-Age: 1728000');
    }

    public function isTreatstockDomain($referrer)
    {
        $domain = UrlHelper::getDomainFromUrl($referrer);
        $isAllowedDomain = in_array($domain, $this->module->allowedDomains);
        return $isAllowedDomain;
    }
}