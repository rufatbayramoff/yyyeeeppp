<?php

namespace common\modules\api\v2\models;

use common\interfaces\Model3dBaseInterface;
use common\models\Model3d;

/**
 * Date: 14.11.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class ApiExternalSystemConfig
{

    /**
     * @var
     */
    public $model3d_cae = Model3dBaseInterface::CAE_CAD;
    public $affiliate_price = 0;
    public $affiliate_currency = 'USD';
    public $config = [];

    /**
     * ApiExternalSystemConfig constructor.
     *
     * @param $json
     * @throws \yii\base\InvalidParamException
     */
    public function __construct($json)
    {
        if (empty($json) || !is_array($json)) {
            return;
        }
        foreach ($json as $k => $v) {
            $this->$k = $v;
        }
    }
}