<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.11.16
 * Time: 9:43
 */

namespace common\modules\api\v2\models;


use common\models\PrinterMaterialGroup;

class ApiPrinterMaterialGroupColor extends PrinterMaterialGroup
{
    public function fields()
    {
        return [
            'code'        => 'code',
            'description' => 'short_description',
            'infill'      => 'infillArray',
            'colors'      => 'colorsList'
        ];
    }

    public function getColorsList()
    {
        $colorsList = [];
        $materials = $this->printerMaterials;
        foreach ($materials as $material) {
            if (!$material->is_active) {
                continue;
            }
            $toColors = $material->printerMaterialToColors;
            foreach ($toColors as $toColor) {
                if (!$toColor->is_active) {
                    continue;
                }
                $color = $toColor->color;
                if ($color->is_active) {
                    $colorsList[$color->render_color] = [
                        'code' => $color->render_color,
                        'rgb'  => $color->rgb
                    ];
                }
            }
        }

        return array_values($colorsList);
    }
}