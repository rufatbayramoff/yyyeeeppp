<?php
/**
 * User: nabi
 */

namespace common\modules\api\v2\serializers;


use common\components\helpers\ColorHelper;
use common\components\ps\locator\materials\MaterialColorsItem;
use common\components\serizaliators\AbstractProperties;

class AllowedMaterialsSerializer extends AbstractProperties
{

    public $shortListSize = 8;
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            MaterialColorsItem::class => [
                'materialGroups' => function (MaterialColorsItem $item) {
                    $materials = [];
                    foreach ($item->materialGroup->printerMaterials as $pr) {
                        $materials[] = ['id'=>$pr->id, 'code'=>$pr->filament_title, 'title'=>$pr->title];
                    }
                    return [['id' => $item->materialGroup->id, 'code' => $item->materialGroup->code, 'title' => $item->materialGroup->title]];
                },
                'colors'       => function (MaterialColorsItem $item) {
                    $colors = [];
                    foreach ($item->colors as $color) {
                        $colors[] = ['id' => $color->id, 'code' => $color->render_color, 'title' => $color->title, 'rgb' => $color->rgb];
                    }
                    return $colors;
                }
            ],
        ];
    }
}