<?php
/**
 * @author Nabi Defacto
 */
namespace common\modules\api\v2\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterialGroup;
use frontend\controllers\catalog\actions\serializers\PrinterColorSerializer;

class Model3dTextureSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {

        return [
            PrinterColor::class         => PrinterColorSerializer::class,
            PrinterMaterialGroup::class => [
                'id',
                'code',
                'title'
            ],
            Model3dTexture::class       => [
                'printerColor',
                'printerMaterialGroup' => function (Model3dTexture $model3dTexture) {
                    return $model3dTexture->calculatePrinterMaterialGroup();
                }
            ]
        ];
    }
}