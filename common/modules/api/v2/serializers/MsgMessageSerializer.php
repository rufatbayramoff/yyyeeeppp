<?php
namespace common\modules\api\v2\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\MsgFile;
use common\models\MsgMessage;

class MsgMessageSerializer extends AbstractProperties
{

    /**
     * @return array
     * @throws \yii\base\InvalidParamException
     *
     *
     */
    public function getProperties()
    {
        return [
            MsgMessage::class => [
                'id',
                'userUid'   => 'user.uid',
                'userTitle' => 'user.fullNameOrUsername',
                'createdAt' => 'created_at',
                'text',
                'topicId'   => 'topic_id',
                'files'     => function (MsgMessage $message) {
                    return $message->msgFiles;
                }
            ],
            MsgFile::class    => [
                'name' => function (MsgFile $file) {
                    return $file->file->getFileName();
                },
                'url'  => function (MsgFile $file) {
                    return $file->file->getFileUrl();
                }
            ]
        ];
    }
}