<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.06.19
 * Time: 15:02
 */

namespace common\modules\api\v2\serializers;


use common\components\orderOffer\OrderOffer;
use common\components\serizaliators\AbstractProperties;
use common\modules\printersList\models\OfferItemInfo;

class ApiOffersBundleSerializer extends AbstractProperties
{
    public function getProperties()
    {
        return [
            OrderOffer::class => [
                'printPrice' => function (OrderOffer $offerItemInfo) {
                    return ceil($offerItemInfo->anonymousPrintPrice->getAmount() * 100) / 100;
                },
                'printPriceCurrency' => function (OrderOffer $offerItemInfo) {
                    return $offerItemInfo->anonymousPrintPrice->getCurrency();
                },
                'providerId' => function (OrderOffer $offerItemInfo) {
                    return $offerItemInfo->getPrinterId();
                }
            ]
        ];
    }
}
