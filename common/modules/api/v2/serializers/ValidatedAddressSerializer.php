<?php

namespace common\modules\api\v2\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\UserAddress;

class ValidatedAddressSerializer extends AbstractProperties
{

    /**
     * @return array
     * @throws \yii\base\InvalidParamException
     *
     */
    public function getProperties()
    {
        return [
            UserAddress::class => [
                'country' => 'country.iso_code',
                'state'   => 'region',
                'city',
                'street'  => 'address',
                'street2' => 'extended_address',
                'zip'     => 'zip_code',
            ]
        ];
    }
}
