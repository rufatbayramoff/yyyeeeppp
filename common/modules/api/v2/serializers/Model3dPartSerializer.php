<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.11.18
 * Time: 14:33
 */

namespace common\modules\api\v2\serializers;

use common\components\serizaliators\AbstractProperties;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\Model3dPart;
use common\models\Model3dReplicaPart;
use common\models\PrinterColor;

class Model3dPartSerializer extends AbstractProperties
{
    public function getProperties()
    {
        $part = [
            'uid'     => function (Model3dBasePartInterface $part) {
                return $part->getUid();
            },
            'name',
            'qty'     => function (Model3dBasePartInterface $part) {
                return (int)$part->qty;
            },
            'hash'    => function (Model3dBasePartInterface $part) {
                return $part->file->md5sum;
            },
            'size',
            'originalSize',
            'weight'  => function (Model3dBasePartInterface $part) {
                return round($part->getWeight(), 2);
            },
            'texture' => function (Model3dBasePartInterface $part) {
                $texture = $part->getCalculatedTexture();
                return [
                    'color'         => $texture->printerColor->render_color,
                    'materialGroup' => $texture->calculatePrinterMaterialGroup()->code
                ];
            }
        ];
        return [
            Model3dPart::class        => $part,
            Model3dReplicaPart::class => $part
        ];
    }
}