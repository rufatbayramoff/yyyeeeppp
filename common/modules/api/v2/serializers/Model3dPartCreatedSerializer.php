<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.11.18
 * Time: 14:33
 */
namespace common\modules\api\v2\serializers;

use common\components\serizaliators\AbstractProperties;
use common\interfaces\Model3dBasePartInterface;
use common\models\Model3dPart;
use common\models\Model3dReplicaPart;

class Model3dPartCreatedSerializer extends AbstractProperties
{
    public function getProperties()
    {
        $part = [
            'uid'                   => function (Model3dBasePartInterface $part) {
                return $part->getUid();
            },
            'name',
            'qty'                   => function (Model3dBasePartInterface $part) {
                return (int)$part->qty;
            },
            'hash'                  => function (Model3dBasePartInterface $part) {
                return $part->file->md5sum;
            },
        ];
        return [
            Model3dPart::class        => $part,
            Model3dReplicaPart::class => $part
        ];
    }
}