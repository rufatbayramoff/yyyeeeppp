<?php

namespace common\modules\api\v2\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\MsgFile;
use common\models\MsgMessage;
use common\models\MsgTopic;

class MsgTopicMessagesSerializer extends AbstractProperties
{

    /**
     * @return array
     * @throws \yii\base\InvalidParamException
     *
     *
     */
    public function getProperties()
    {
        return [
            MsgTopic::class =>[
                'id',
                'creatorUid'      => 'creator.uid',
                'creatorTitle'    => 'creator.fullNameOrUsername',
                'createdAt'       => 'created_at',
                'title',
                'lastMessageTime' => 'last_message_time',
                'orderId'         => function (MsgTopic $msgTopic) {
                    return $msgTopic->bind_to === MsgTopic::BIND_OBJECT_ORDER ? $msgTopic->bind_id : null;
                },
                'messages' => function (MsgTopic $msgTopic) {
                    return $msgTopic->messages;
                }
            ],
            MsgMessage::class => [
                'id',
                'userUid'   => 'user.uid',
                'userTitle' => 'user.fullNameOrUsername',
                'createdAt' => 'created_at',
                'text',
                'files'     => function (MsgMessage $message) {
                    return $message->msgFiles;
                }
            ],
            MsgFile::class    => [
                'name' =>  function (MsgFile $file) {
                    return $file->file->getFileName();
                },
                'url' => function (MsgFile $file) {
                    return $file->file->getFileUrl();
                }
            ]
        ];
    }
}

