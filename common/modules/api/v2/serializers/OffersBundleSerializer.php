<?php
/**
 * User: nabi
 */

namespace common\modules\api\v2\serializers;

use common\components\orderOffer\OrderOffer;
use common\components\serizaliators\AbstractProperties;
use common\models\Model3dViewedState;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\UserAddress;
use common\services\ReviewService;
use frontend\controllers\catalog\actions\models\Rating;
use frontend\models\ps\PsFacade;
use lib\geo\models\Location;

class OffersBundleSerializer extends AbstractProperties
{

    /**
     * @return array
     * @throws \yii\base\InvalidParamException
     *
     *
     */
    public function getProperties()
    {
        return [
            OrderOffer::class => [
                'psPrinter'    => function (OrderOffer $orderOffer) {
                    $printer =  $orderOffer->getPrinter();
                    return [
                        'id'                  => $printer->id,
                        'title'               => $printer->title,
                        'positional_accuracy' => $printer->positional_accuracy,
                    ];
                },
                'userPrice',
                'estimateDeliveryCost',
                'deliveryTypes'
            ]
        ];
    }
}

