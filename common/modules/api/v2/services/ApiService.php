<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.03.18
 * Time: 14:27
 */

namespace common\modules\api\v2\services;

use common\models\AffiliateResource;
use common\models\ApiExternalSystem;
use common\models\StoreOrder;
use common\modules\affiliate\repositories\AffiliateResourceRepository;
use common\modules\api\v2\components\ApiExternalSystemAuthManager;
use common\services\Model3dViewedStateService;
use common\services\StoreUnitShoppingCandidateService;
use common\models\ApiPrintablePack;
use common\models\StoreUnitShoppingCandidate;
use common\services\Model3dService;
use frontend\components\UserSessionFacade;
use frontend\models\model3d\Model3dItemForm;
use frontend\models\user\UserFacade;
use InvalidArgumentException;
use treatstock\api\v2\exceptions\UnSuccessException;
use yii\base\BaseObject;
use lib\geo\models\Location;
use yii\web\NotAcceptableHttpException;

class ApiService extends BaseObject
{
    /** @var Model3dService */
    public $model3dService;

    /** @var StoreUnitShoppingCandidateService */
    public $shoppingCandidateService;

    /** @var Model3dViewedStateService */
    public $model3dViewedStateService;

    public function injectDependencies(StoreUnitShoppingCandidateService $shoppingCandidateService, Model3dService $model3dService, Model3dViewedStateService $model3dViewedStateService): void
    {
        $this->model3dService = $model3dService;
        $this->shoppingCandidateService = $shoppingCandidateService;
        $this->model3dViewedStateService = $model3dViewedStateService;
    }

    /**
     * @param ApiPrintablePack $printablePack
     * @throws \yii\base\UserException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidCallException
     * @throws \yii\web\NotFoundHttpException
     */
    public function prepareForPrint(ApiPrintablePack $printablePack)
    {
        $model3d = $printablePack->model3d;

        if (!$model3d->user && ($printablePack->apiExternalSystem->model3d_owner === ApiExternalSystem::MODEL3D_OWNER_UPLOAD_USER)) {
            $this->model3dService->fixCurrentUserSession($model3d);
            if (UserFacade::getCurrentUser()) {
                $this->model3dService->fixCurrentUser($model3d);
            }
        }

        if ($printablePack->geoLocation) {
            $currentLocation = UserSessionFacade::getLocation();
            $country = $printablePack->geoLocation->country;
            if ($country->capital) {
                $location = new Location();
                $location->country = $country->iso_code;
                $location->timezone = $currentLocation->timezone;
                $location->city = $country->capital->title;
                $location->lat = $country->capital->lat;
                $location->lon = $country->capital->lon;
            }
            UserSessionFacade::setLocationToSession($location);
        }

        $this->shoppingCandidateService->formShoppingCandidate($model3d, StoreUnitShoppingCandidate::TYPE_API);

        $model3dItemForm = Model3dItemForm::create($model3d);
        $model3dViewedState = $this->model3dViewedStateService->registerModel3dViewForm($model3dItemForm);
    }

    public function getPayedStoreOrder(ApiPrintablePack $apiPrintablePack): ?StoreOrder
    {
        foreach ($apiPrintablePack->model3d->model3dReplicas as $model3dReplica) {
            $storeOrder = $model3dReplica->getStoreOrder();
            if ($storeOrder && $storeOrder->isPayed()) {
                return $storeOrder;
            }
        }
        return null;
    }

    /**
     * Store order
     *
     * @param $bodyParams
     * @return StoreOrder
     * @throws NotAcceptableHttpException
     * @throws UnSuccessException
     * @throws \yii\web\NotFoundHttpException
     */
    public function getOrderByFilters($bodyParams): StoreOrder
    {
        if (!array_key_exists('orderId', $bodyParams) && !array_key_exists('printablePackId', $bodyParams)) {
            throw new InvalidArgumentException('You should set filter parameter "orderId" or "printablePackId".');
        }

        if (array_key_exists('printablePackId', $bodyParams)) {
            $apiPrintablePackId = $bodyParams['printablePackId'];
            $apiPrintablePack = ApiPrintablePack::tryFindByPk($apiPrintablePackId);
            $this->checkAccess('view', $apiPrintablePack);

            $storeOrder = $this->apiService->getPayedStoreOrder($apiPrintablePack);
            if (!$storeOrder) {
                throw new UnSuccessException('No payed orders exists for printable pack', ['no_order_exists']);
            }
        } else {
            $externalSystem = ApiExternalSystemAuthManager::getCurrentApiExternalSystem();
            $storeOrder = StoreOrder::tryFindByPk($bodyParams['orderId']);
            if ($storeOrder->user_id !== $externalSystem->binded_user_id) {
                throw new NotAcceptableHttpException('Invalid order access');
            }
            if (!$storeOrder->isPayed()) {
                throw new UnSuccessException('Not payed order. Send messages allowed only on payed orders.', 'not_payed_order');
            }

        }
        return $storeOrder;
    }
}