<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.10.16
 * Time: 16:10
 */

namespace common\modules\api\v2\components;

use common\models\ApiPrintablePack;

class ApiPrintablePackRepository
{
    public function save(ApiPrintablePack $apiPrintablePack)
    {
        $apiPrintablePack->safeSave();
    }
}