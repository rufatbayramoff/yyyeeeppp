<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.10.16
 * Time: 16:09
 */

namespace common\modules\api\v2\components;

use common\components\UserSession;
use common\interfaces\Model3dBaseInterface;
use common\models\ApiExternalSystem;
use common\models\ApiPrintablePack;
use common\models\base\PaymentCurrency;
use common\models\factories\GeoLocationFactory;
use common\models\factories\Model3dTextureFactory;
use common\models\GeoLocation;
use common\models\Model3d;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\modules\api\v2\models\ApiExternalSystemConfig;
use common\modules\product\interfaces\ProductInterface;
use common\services\Model3dService;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use Yii;
use yii\web\UploadedFile;

class ApiPrintablePackFactory
{
    /**
     * @var FrontUser
     */
    public $user;

    /**
     * @var UserSession
     */
    public $userSession;

    /**
     * @var ApiExternalSystem
     */
    public $apiExternalSystem;

    /**
     * ApiPrintablePackFactory constructor.
     *
     * @param ApiExternalSystem $apiExternalSystem
     * @param bool|FrontUser $user
     * @param bool $userSession
     */
    public function __construct(ApiExternalSystem $apiExternalSystem = null, $user = false, $userSession = false)
    {
        if ($user !== false) {
            $this->user = $user;
        } else {
            $this->user = UserFacade::getCurrentUser();
        }
        if ($userSession !== false) {
            $this->userSession = $userSession;
        } else {
            $this->userSession = UserFacade::getUserSession();
        }

        $this->apiExternalSystem = $apiExternalSystem;
    }

    public function generateTokien()
    {
        return substr(md5(mt_rand(0, 9999999)), 0, 7) . '-' . substr(md5(mt_rand(0, 9999999)), 1, 7) . '-' . substr(md5(mt_rand(0, 9999999)), 3, 7);
    }

    /**
     * @param UploadedFile[] $uploadFiles
     */
    public function checkStepFilesExists($uploadFiles)
    {
        foreach ($uploadFiles as $uploadFile) {
            $lowExt = strtolower($uploadFile->extension);
            if ($lowExt === 'step' || $lowExt === 'stp') {
                throw new \InvalidArgumentException('Sorry, but we doesn`t support support STEP files by api.');
            }
        }
    }

    /**
     * @param array $packParams
     * @param $uploadFiles
     * @return ApiPrintablePack
     * @throws \Exception
     */
    public function create($packParams, $uploadFiles)
    {
        $affiliateCurrency = 'USD';
        if (array_keys($packParams, 'affiliate_currency') && $packParams['affiliate_currency']) {
            $affiliateCurrency = (string)$packParams['affiliate_currency'];
        }
        $affiliatePrice = 0;
        if (array_keys($packParams, 'affiliate_price') && $packParams['affiliate_price']) {
            $affiliatePrice = (int)$packParams['affiliate_price'];
        }

        $this->checkStepFilesExists($uploadFiles);

        $apiPrintablePack = new ApiPrintablePack();
        /** @var GeoLocation $geoLocation */
        $geoLocation = null;
        if (array_key_exists('location', $packParams)) {
            $locationArr = $packParams['location'];
            $geoLocation = GeoLocationFactory::create($locationArr);
            $geoLocation->safeSave();
            $apiPrintablePack->geo_location_id = $geoLocation->id;
        }

        $paymentCurrency = PaymentCurrency::tryFind(['currency_iso' => $affiliateCurrency]);
        $apiPrintablePack->affiliate_price = $affiliatePrice;
        $apiPrintablePack->affiliate_currency_id = $paymentCurrency->id;
        /** @var Model3dService $model3dService */
        if ($this->apiExternalSystem->model3d_owner === ApiExternalSystem::MODEL3D_OWNER_UPLOAD_USER) {
            $model3dUser = null;
        } else {
            $model3dUser = $this->user;
        }
        $model3dProperties = [];

        if ($this->apiExternalSystem->name === ApiExternalSystem::SYSTEM_TINKERCAD) {
            $model3dProperties['source_details'] = [
                'vendor' => ApiExternalSystem::SYSTEM_TINKERCAD,
                'link'   => app('request')->referrer
            ];
        }
        $model3dProperties['source'] = Model3d::SOURCE_API;
        $model3dProperties['cae'] = !empty($packParams['model3d_cae']) ? $packParams['model3d_cae'] : Model3dBaseInterface::CAE_CAD;

        if (array_key_exists('description', $packParams)) {
            $model3dProperties['description'] = $packParams['description'];
        }
        if (array_key_exists('print_instructions', $packParams)) {
            $model3dProperties['print_instructions'] = $packParams['print_instructions'];
        }
        if (array_key_exists('title', $packParams)) {
            $model3dProperties['title'] = $packParams['title'];
        }
        $model3dService = Yii::createObject(Model3dService::class, [$model3dUser]);
        $model3d = $model3dService->createModel3dByUploadFilesList(
            $model3dProperties,
            $uploadFiles
        );

        $configObj = $this->apiExternalSystem->getConfig();
        $config = $configObj->config;
        if (!empty($config['texture'])) {
            try {
                $material = PrinterMaterial::findOne(['filament_title' => $config['texture']['material']]);
                $color = PrinterColor::findOne(['render_color' => $config['texture']['color']]);
                $texture = Model3dTextureFactory::createTexture($material->group, $material, $color);
                $texture->safeSave();
                $model3d->populateRelation('model3dTexture', $texture);
                $model3d->model3d_texture_id = $texture->id;
            } catch (\Exception $e) {
                \Yii::error($e);
            }
        }
        $model3d->productCommon->single_price = $affiliatePrice;
        $model3d->price_currency = $affiliateCurrency;
        $model3d->safeSave();
        $apiPrintablePack->model3d_id = $model3d->id;
        $apiPrintablePack->public_token = $this->generateTokien();
        return $apiPrintablePack;
    }
}