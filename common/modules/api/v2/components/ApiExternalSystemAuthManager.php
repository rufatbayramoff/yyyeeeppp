<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.10.16
 * Time: 8:51
 */


namespace common\modules\api\v2\components;

use common\components\HttpRequestLogger;
use common\models\ApiExternalSystem;
use common\models\base\ApiPrintablePack;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use Yii;
use yii\filters\auth\AuthMethod;
use yii\web\ForbiddenHttpException;
use yii\web\UnauthorizedHttpException;

class ApiExternalSystemAuthManager extends AuthMethod
{
    const KEY_AUTH_PUBLIC_UPLOAD = 'public-upload-key';
    const KEY_AUTH_PRIVATE = 'private-key';

    /** @var ApiExternalSystem */
    public static $apiExternalSystem = null;

    public static $authType = null;

    /**
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\UnauthorizedHttpException
     */
    public function authenticate($user, $request, $response)
    {
        if (Yii::$app->request->get(self::KEY_AUTH_PRIVATE) || Yii::$app->request->post(self::KEY_AUTH_PRIVATE)) {
            $privateKey = Yii::$app->request->post(self::KEY_AUTH_PRIVATE, Yii::$app->request->get(self::KEY_AUTH_PRIVATE));
            $apiExternalSystem = ApiExternalSystemAuthManager::auth($privateKey);
        } elseif (Yii::$app->request->get(self::KEY_AUTH_PUBLIC_UPLOAD) || Yii::$app->request->post(self::KEY_AUTH_PUBLIC_UPLOAD)) {
            $publicUploadKey = Yii::$app->request->post(self::KEY_AUTH_PUBLIC_UPLOAD, Yii::$app->request->get(self::KEY_AUTH_PUBLIC_UPLOAD));
            $apiExternalSystem = ApiExternalSystemAuthManager::auth($publicUploadKey, self::KEY_AUTH_PUBLIC_UPLOAD);
        } else {
            throw new UnauthorizedHttpException('You should set correct public or private key parameter.');
        }

        return $apiExternalSystem->bindedUser;
    }

    /**
     * @param $apiExternalSystemKey
     * @param string $keyType
     * @return ApiExternalSystem|null
     * @throws \yii\base\InvalidParamException
     * @throws ForbiddenHttpException
     * @internal param string $apiExternalSystemPrivateKey
     */
    public static function auth($apiExternalSystemKey, $keyType = self::KEY_AUTH_PRIVATE)
    {
        if (!$apiExternalSystemKey) {
            app()->httpRequestLogger->logRequestInfo('Login failed', ['type'=>'login', 'success'=>false, 'reason'=>'keyNotSet']);
            throw new ForbiddenHttpException('Invalid api external system key.');
        }
        if ($keyType === self::KEY_AUTH_PUBLIC_UPLOAD) {
            $externalSystem = self::$apiExternalSystem = ApiExternalSystem::findOne(['public_upload_key' => $apiExternalSystemKey]);
        } else {
            $externalSystem = self::$apiExternalSystem = ApiExternalSystem::findOne(['private_key' => $apiExternalSystemKey]);
        }
        /** @var ApiExternalSystem $externalSystem */
        if (!$externalSystem) {
            app()->httpRequestLogger->logRequestInfo('Login failed', ['type'=>'login', 'success'=>false, 'reason'=>'invalid key']);
            throw new ForbiddenHttpException('Invalid external system private key.');
        }
        $frontUser = FrontUser::tryFindByPk($externalSystem->binded_user_id);
        $externalSystem->populateRelation('bindedUser', $frontUser);
        UserFacade::loginByOsn($frontUser, false);
        Yii::$app->getSession()->destroy();
        self::$authType = $keyType;
        app()->httpRequestLogger->logRequestInfo('Login success', ['type'=>'login', 'success'=>true, 'extSystemId'=>$externalSystem->id, 'userId'=>$frontUser->id]);
        return $externalSystem;
    }

    public static function getCurrentApiExternalSystem()
    {
        return self::$apiExternalSystem;
    }


    /**
     * @param ApiPrintablePack $printablePack
     * @throws ForbiddenHttpException
     */
    public static function checkPrintablePackAccess($printablePack)
    {
        if (!self::$apiExternalSystem) {
            throw new ForbiddenHttpException('Invalid api external system.');
        }
        if ((self::$authType !== self::KEY_AUTH_PRIVATE) && $printablePack) {
            throw new ForbiddenHttpException('Can`t access to this printable pack with public upload key.');
        }
        if ($printablePack && ($printablePack->api_external_system_id !== self::$apiExternalSystem->id)) {
            throw new ForbiddenHttpException('Can`t access to this printable pack.');
        }
    }
}