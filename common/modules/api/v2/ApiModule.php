<?php
namespace common\modules\api\v2;

use common\models\ApiRequest;
use common\modules\api\v2\controllers\BaseApiController;
use Yii;
use yii\base\Module;
use yii\web\Response;


/**
 * Date: 30.06.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class ApiModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\api\v2\controllers';

    /**
     * folder where to store API uploaded files
     * @var string
     */
    public $uploadFolder;

    /**
     * init API module
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        $this->setComponents([
            'apiRequestLog'  => ApiRequest::class,
            'baseApi' => [
                'class' => BaseApiController::class,
                'tokenParam' => 'api-token'
            ]
        ]);
        if(!$this->uploadFolder){
            $this->uploadFolder =  '/api'; // in @storage folder
        }
        Yii::$app->response->getHeaders()->set('Access-Control-Allow-Origin', '*');
        Yii::$app->response->format = Response::FORMAT_JSON;
        parent::init();
    }
}
