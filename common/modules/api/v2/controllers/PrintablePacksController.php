<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.10.16
 * Time: 11:31
 */

namespace common\modules\api\v2\controllers;

use common\components\ArrayHelper;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\ApiExternalSystem;
use common\models\ApiPrintablePack;
use common\models\factories\FileFactory;
use common\models\factories\GeoLocationFactory;
use common\models\factories\UploadedFileListFactory;
use common\models\repositories\Model3dRepository;
use common\modules\api\v2\components\ApiExternalSystemAuthManager;
use common\modules\api\v2\components\ApiPrintablePackFactory;
use common\modules\api\v2\components\ApiPrintablePackRepository;
use common\modules\api\v2\models\ApiExternalSystemConfig;
use common\modules\api\v2\serializers\Model3dPartCreatedSerializer;
use common\services\Model3dService;
use common\services\PrinterMaterialService;
use common\services\WidgetService;
use lib\MeasurementUtil;
use Psr\Log\InvalidArgumentException;
use Yii;
use yii\base\InvalidParamException;
use yii\base\UserException;
use yii\helpers\Html;
use yii\web\UploadedFile;

class PrintablePacksController extends BaseApiController
{
    public $modelClass = ApiPrintablePack::class;

    /** @var  ApiPrintablePackRepository */
    public $diApiPrintablePackRepository;

    /** @var WidgetService */
    public $widgetService;

    /** @var Model3dRepository */
    public $model3dRepository;

    public function injectDependencies(ApiPrintablePackRepository $diApiPrintablePackRepository, WidgetService $widgetService, Model3dRepository $model3dRepository)
    {
        $this->diApiPrintablePackRepository = $diApiPrintablePackRepository;
        $this->widgetService                = $widgetService;
        $this->model3dRepository            = $model3dRepository;
    }

    public function behaviors()
    {
        $behaviors                  = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => ApiExternalSystemAuthManager::class,
        ];

        return $behaviors;
    }

    /**
     * User only default view action
     *
     * @return array
     */
    public function actions()
    {
        $actions       = parent::actions();
        $returnActions = [
            'view' => [
                'class'       => 'yii\rest\ViewAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ]
        ];

        return $returnActions;
    }

    /**
     * Check access before any action
     *
     * @param \yii\base\Action $action
     * @return bool
     * @throws UserException
     */
    public function beforeAction($action)
    {
        $returnValue = parent::beforeAction($action);
        $this->checkAccess('create');
        return $returnValue;
    }

    /**
     * @return array
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $response   = Yii::$app->getResponse();
        $bodyParams = Yii::$app->getRequest()->getBodyParams();

        $externalSystem       = ApiExternalSystemAuthManager::getCurrentApiExternalSystem();
        $externalSystemConfig = new ApiExternalSystemConfig($externalSystem->json_config);

        if ($externalSystem->model3d_owner === ApiExternalSystem::MODEL3D_OWNER_UPLOAD_USER) {
            // Fix files owner
            $fileFactory       = Yii::createObject(FileFactory::class);
            $fileFactory->user = null;
            Yii::$container->set(FileFactory::class, $fileFactory);
        }
        try {
            $files = UploadedFile::getInstancesByName('files');

            if (!$files) {
                if (array_key_exists('file-urls', $bodyParams)) {
                    $files = UploadedFileListFactory::createUploadFilesList($bodyParams['file-urls']);
                }
            }
            if (!$files) {
                throw new InvalidArgumentException('Empty files list');
            }
            $bodyParams['model3d_cae'] = $externalSystemConfig->model3d_cae;
            /** @var ApiPrintablePackFactory $apiPrintablePackFactory */
            $apiPrintablePackFactory = Yii::createObject(ApiPrintablePackFactory::class, [$externalSystem]);
            $apiPrintablePack        = $apiPrintablePackFactory->create($bodyParams, $files);
        } catch (UserException $exception) {
            return [
                'success' => 'false',
                'message' => $exception->getMessage()
            ];
        }
        $apiPrintablePack->api_external_system_id = $externalSystem->id;
        if ($apiPrintablePack->hasErrors()) {
            $response->setStatusCode(422);
            $errors = $apiPrintablePack->getErrors();
            return ['success' => false, 'errors' => $errors];
        }
        $apiPrintablePack->safeSave();
        $this->diApiPrintablePackRepository->save($apiPrintablePack);

        $redirUrl        = $this->getModel3dUrl($apiPrintablePack);
        $widgetUrl       = $this->getWidgetIframeUrl($apiPrintablePack);
        $widgetHtml      = $this->getWidgetHtml($apiPrintablePack);
        $partsSerialized = $this->getPartsSerialized($apiPrintablePack);

        $data = [
            'success'    => true,
            'id'         => $apiPrintablePack->id,
            'redir'      => $redirUrl,
            'widgetUrl'  => $widgetUrl,
            'widgetHtml' => $widgetHtml,
            'parts'      => $partsSerialized
        ];
        return $data;
    }

    protected function updateScale(Model3dBaseInterface $model3d, array $bodyParams)
    {
        $scaleUnit = $bodyParams['scaleUnit'] ?? null;
        $scale     = $bodyParams['scale'] ?? null;
        if ($scaleUnit) {
            if ($scaleUnit === MeasurementUtil::MM) {
                $scale                = 1;
                $model3d->model_units = MeasurementUtil::MM;
            }
            if ($scaleUnit === MeasurementUtil::IN) {
                $scale                = MeasurementUtil::MILLIMETERS_TO_INCHES;
                $model3d->model_units = MeasurementUtil::IN;
            } elseif ($scaleUnit === MeasurementUtil::CM) {
                $scale                = MeasurementUtil::MM_TO_CM;
                $model3d->model_units = MeasurementUtil::CM;
            }
        }
        if ($scale) {
            Model3dService::resetScale($model3d);
            Model3dService::scaleModel3d($model3d, $scale);
            $this->model3dRepository->save($model3d);
        }
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param array $bodyParams
     */
    protected function updateCount(Model3dBaseInterface $model3d, array $bodyParams)
    {
        if (!array_key_exists('qty', $bodyParams)) {
            return;
        }
        $qtyArray = $bodyParams['qty'];
        /** @var Model3dBasePartInterface $model3dPart */
        foreach ($model3d->model3dParts as $model3dPart) {
            if (array_key_exists($model3dPart->getUid(), $qtyArray)) {
                $qty              = $qtyArray[$model3dPart->getUid()];
                $model3dPart->qty = (int)$qty;
                $model3dPart->safeSave();
            }
        }
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param array $bodyParams
     * @throws \ErrorException
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    protected function updateColors(Model3dBaseInterface $model3d, array $bodyParams)
    {
        if (!array_key_exists('modelTextureInfo', $bodyParams)) {
            return;
        }
        $modelTextureState = $bodyParams['modelTextureInfo'];
        PrinterMaterialService::unSerializeModel3dTexture($model3d, $modelTextureState, true);
        $this->model3dRepository->save($model3d);
    }

    /**
     * @param ApiPrintablePack $apiPrintablePack
     * @param array $bodyParams
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     */
    protected function updateLocation(ApiPrintablePack $apiPrintablePack, array $bodyParams): void
    {
        if (!array_key_exists('location', $bodyParams) || empty($bodyParams['location'])) {
            return;
        }
        $locationArr = $bodyParams['location'];
        $geoLocation = GeoLocationFactory::create($locationArr);
        $geoLocation->safeSave();
        $apiPrintablePack->geo_location_id = $geoLocation->id;
        $this->diApiPrintablePackRepository->save($apiPrintablePack);
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getBodyParams()
    {
        $bodyParams = \Yii::$app->getRequest()->getBodyParams();
        if (!is_array($bodyParams)) {
            throw new InvalidArgumentException('Invalid data format was send.');
        }
        $bodyParamReset = reset($bodyParams);
        if (is_string($bodyParamReset) && strpos($bodyParamReset, "\r\nContent-Disposition: form-data;")) {
            throw new InvalidArgumentException('Invalid data format was send. Use url encoded params.');
        }
        if (!is_array($bodyParams)) {
            throw new InvalidArgumentException('Invalid data format was send.');
        }
        if (!$bodyParams) {
            throw new InvalidArgumentException('No data was send.');
        }
        return $bodyParams;
    }

    /**
     * Change printable pack info
     *
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws UserException
     */
    public function actionUpdate($id)
    {
        $apiPrintablePack = ApiPrintablePack::tryFindByPk($id);

        $this->checkAccess($apiPrintablePack);

        $bodyParams = $this->getBodyParams();

        $model3d = $apiPrintablePack->model3d;

        if (!$model3d->getActiveModel3dParts()) {
            return [
                'success' => true,
            ];
        };
        try {
            $this->updateScale($model3d, $bodyParams);
            $this->updateCount($model3d, $bodyParams);
            $this->updateColors($model3d, $bodyParams);
            $this->updateLocation($apiPrintablePack, $bodyParams);
        } catch (UserException $exception) {
            return ['success' => false, 'message' => $exception->getMessage()];
        }

        return [
            'success' => true,
        ];
    }

    /**
     * http://ts.vcap.me/api/v2/printable-packs/create-by-url?file-urls[]=http://ts.vcap.me/static/tests/common-test.stl&private-key=9c261b3b809cf48
     *
     * @return array
     * @throws \Exception
     */
    public function actionCreateByUrl()
    {
        $response   = Yii::$app->getResponse();
        $bodyParams = Yii::$app->getRequest()->get();
        try {
            if (empty($bodyParams['file-urls'])) {
                throw new \Exception('No file-urls specified');
            }
            $externalSystem       = ApiExternalSystemAuthManager::getCurrentApiExternalSystem();
            $externalSystemConfig = new ApiExternalSystemConfig($externalSystem->json_config);

            if ($externalSystem->model3d_owner === ApiExternalSystem::MODEL3D_OWNER_UPLOAD_USER) {
                // Fix files owner
                $fileFactory          = Yii::createObject(FileFactory::class);
                $fileFactory->user    = null;
                Yii::$container->set(FileFactory::class, $fileFactory);
            }

            $files = UploadedFileListFactory::createUploadFilesList($bodyParams['file-urls']);

            if (!empty($externalSystemConfig->config['description'])) {
                $bodyParams['description'] = $externalSystemConfig->config['description'];
            }
            if (empty($bodyParams['affiliate_currency'])) {
                $bodyParams['affiliate_currency'] = $externalSystemConfig->affiliate_currency;
                $bodyParams['affiliate_price']    = $externalSystemConfig->affiliate_price;
            }
            $bodyParams['model3d_cae'] = $externalSystemConfig->model3d_cae;
            $apiPrintablePackFactory   = Yii::createObject(ApiPrintablePackFactory::class, [$externalSystem]);
            $apiPrintablePack          = $apiPrintablePackFactory->create($bodyParams, $files);
        } catch (\Exception $e) {
            $response->setStatusCode(422);
            Yii::warning($e, 'app');
            if (!Yii::$app->request->isAjax) {
                Yii::$app->getSession()->setFlash('danger', $e->getMessage(), false);
                return $this->redirect('/upload?utm_source=api_upload');
            }
            return ['success' => false, 'message' => $e->getMessage()];
        }
        $apiPrintablePack->api_external_system_id = $externalSystem->id;
        if ($apiPrintablePack->hasErrors()) {
            $response->setStatusCode(422);
            if (!Yii::$app->request->isAjax) {
                Yii::$app->getSession()->setFlash('danger', Html::errorSummary($apiPrintablePack), false);
                return $this->redirect('/upload?utm_source=api_upload');
            }
            $errors = $apiPrintablePack->getErrors();
            return ['success' => false, 'errors' => $errors];
        }
        $apiPrintablePack->safeSave();
        $this->diApiPrintablePackRepository->save($apiPrintablePack);

        $redirUrl = $this->getModel3dUrl($apiPrintablePack);
        if (!Yii::$app->request->isAjax) {
            return $this->redirect($redirUrl);
        }
        $data = [
            'success' => true,
            'id'      => $apiPrintablePack->id,
            'redir'   => $redirUrl
        ];
        return $data;
    }

    public function getWidgetIframeUrl($apiPrintablePack)
    {
        $url = \Yii::$app->params['siteUrl'] . '/api/v2/printable-pack-widget/?apiPrintablePackToken=' . $apiPrintablePack->public_token;
        return $url;
    }

    public function getWidgetHtml(ApiPrintablePack $apiPrintablePack)
    {
        $comment   = '<!-- ApiWidget: ' . $apiPrintablePack->public_token . ' -->';
        $iframeUrl = $this->getWidgetIframeUrl($apiPrintablePack);
        $style     = $this->widgetService->getUserWidgetHtmlStyle();
        return $this->widgetService->getFormWidgetHtmlCode($comment, $style, '', $iframeUrl);
    }

    public function getPartsSerialized(ApiPrintablePack $apiPrintablePack)
    {
        $parts           = $apiPrintablePack->model3d->getActiveModel3dParts();
        $partsSerialized = Model3dPartCreatedSerializer::serialize($parts);
        $partsSerialized = ArrayHelper::index($partsSerialized, 'uid');
        return $partsSerialized;
    }

    /**
     * @param ApiPrintablePack $apiPrintablePack
     * @return null
     * @throws \yii\base\InvalidParamException
     */
    public function getModel3dUrl(ApiPrintablePack $apiPrintablePack)
    {
        $url = \Yii::$app->params['siteUrl'] . '/catalog/model3d/preload-printable-pack?packPublicToken=' . $apiPrintablePack->public_token;
        return $url;
    }
}
