<?php
/**
 * @author DeFacto
 * @date 04.10.2018
 */

namespace common\modules\api\v2\controllers;

use common\components\ErrorListHelper;
use common\components\order\builder\OrderBuilder;
use common\components\order\PriceCalculator;
use common\models\ApiPrintablePack;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\factories\DeliveryFormFactory;
use common\models\factories\Model3dReplicaFactory;
use common\models\factories\Model3dTextureFactory;
use common\models\PsPrinter;
use common\models\repositories\Model3dReplicaRepository;
use common\models\repositories\Model3dRepository;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\models\StoreOrder;
use common\modules\api\v2\components\ApiExternalSystemAuthManager;
use common\modules\api\v2\components\ApiPrintablePackRepository;
use common\modules\api\v2\serializers\ValidatedAddressSerializer;
use common\modules\api\v2\services\ApiService;
use common\services\Model3dService;
use common\services\PrinterMaterialService;
use frontend\components\cart\CartFactory;
use frontend\components\UserSessionFacade;
use frontend\controllers\store\serializers\DeliveryAddressSerializer;
use frontend\models\delivery\DeliveryForm;
use frontend\models\model3d\Model3dFacade;
use lib\delivery\parcel\Parcel;
use lib\delivery\parcel\ParcelFactory;
use lib\money\Money;
use treatstock\api\v2\exceptions\UnSuccessException;
use Yii;
use yii\base\ErrorException;
use yii\base\InvalidArgumentException;
use yii\base\UserException;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

class PlaceOrderController extends BaseApiController
{
    public $modelClass = ApiPrintablePack::class;

    /** @var  ApiPrintablePackRepository */
    public $diApiPrintablePackRepository;

    /** @var PrinterMaterialGroupRepository */
    public $printerMaterialGroupRepository;

    /** @var PrinterColorRepository */
    public $printerColorRepository;

    /** @var DeliveryFormFactory */
    private $deliveryFormFactory;

    /** @var ApiService */
    public $apiService;

    /** @var PriceCalculator */
    public $priceCalculator;
    /** @var  Model3dRepository */
    public $model3dRepository;

    public function injectDependencies(
        ApiPrintablePackRepository $diApiPrintablePackRepository,
        PrinterMaterialGroupRepository $printerMaterialGroupRepository,
        PrinterColorRepository $printerColorRepository,
        DeliveryFormFactory $deliveryFormFactory,
        PriceCalculator $priceCalculator,
        Model3dRepository $model3dRepository,
        ApiService $apiService
    )
    {
        $this->diApiPrintablePackRepository = $diApiPrintablePackRepository;
        $this->printerMaterialGroupRepository = $printerMaterialGroupRepository;
        $this->printerColorRepository = $printerColorRepository;
        $this->deliveryFormFactory = $deliveryFormFactory;
        $this->apiService = $apiService;
        $this->priceCalculator = $priceCalculator;
        $this->model3dRepository = $model3dRepository;
    }

    /**
     * User only default view action
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => ApiExternalSystemAuthManager::class,
        ];

        return $behaviors;
    }

    /**
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {

        $bodyParams = Yii::$app->getRequest()->getBodyParams();
        try {
            if (!array_key_exists('printablePackId', $bodyParams)) {
                throw new InvalidArgumentException(_t('site.api', 'Please specify printablePackId.'));
            }
            if (!array_key_exists('providerId', $bodyParams)) {
                throw new InvalidArgumentException(_t('site.api', 'Please specify providerId.'));
            }
            if (!array_key_exists('shippingAddress', $bodyParams)) {
                throw new InvalidArgumentException(_t('site.api', 'Please specify shipping address.'));
            }
            if (!array_key_exists('printerMaterialGroup', $bodyParams) && !array_key_exists('modelTextureInfo', $bodyParams)) {
                throw new InvalidArgumentException(_t('site.api', 'Please specify material.'));
            }
            if (!array_key_exists('printerColor', $bodyParams) && !array_key_exists('modelTextureInfo', $bodyParams)) {
                throw new InvalidArgumentException(_t('site.api', 'Please specify color.'));
            }
        } catch (InvalidArgumentException $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
        $providerId = $bodyParams['providerId'];
        $apiPrintablePackId = $bodyParams['printablePackId'];
        $apiPrintablePack = ApiPrintablePack::tryFindByPk($apiPrintablePackId);
        $this->checkAccess('view', $apiPrintablePack);
        $psPrinter = PsPrinter::tryFindByPk($providerId);
        if (!$psPrinter->isCanPrintInStore()) {
            return [
                'success' => false,
                'message' => _t('site.api', 'Specified provider cannot print with given details')
            ];
        };

        $printerMaterialGroup = null;
        $printerColor = null;
        if (array_key_exists('modelTextureInfo', $bodyParams)) {
            $modelTextureState = $bodyParams['modelTextureInfo'];
            PrinterMaterialService::unSerializeModel3dTexture($apiPrintablePack->model3d, $modelTextureState, true);
            $this->model3dRepository->save($apiPrintablePack->model3d);
        } else {
            if (array_key_exists('printerMaterialGroup', $bodyParams)) {
                $printerMaterialGroup = $this->printerMaterialGroupRepository->getByCode($bodyParams['printerMaterialGroup']);
            }
            if (array_key_exists('materialGroup', $bodyParams)) {
                $printerMaterialGroup = $this->printerMaterialGroupRepository->getByCode($bodyParams['materialGroup']);
            }
            if (array_key_exists('printerColor', $bodyParams)) {
                $printerColor = $this->printerColorRepository->getByCode($bodyParams['printerColor']);
            }
            if ($printerMaterialGroup && $printerColor) {
                $texture = Model3dTextureFactory::createTexture($printerMaterialGroup, null, $printerColor);
                $apiPrintablePack->model3d->setKitTexture($texture);
                $this->model3dRepository->save($apiPrintablePack->model3d);
            }
        }

        if (!Model3dFacade::isCanPrintModelByTextureAndSize($psPrinter, $apiPrintablePack->model3d)) {
            return [
                'success' => false,
                'message' => _t('site.api', 'Printer cannot print size/color/material')
            ];
        }

        $psMachine = $psPrinter->companyService;
        $bodyParams['shippingAddress']['countryIso'] = $bodyParams['shippingAddress']['country'] ?? $bodyParams['location']['country'];
        $bodyParams['shippingAddress']['comment'] = $bodyParams['comment'];
        $order = $this->makeOrder($psMachine, $apiPrintablePack, $bodyParams['shippingAddress']);

        if (is_array($order)) {
            return $order;
        }
        return [
            'success' => true,
            'orderId' => $order->id,
            'total'   => $order->getTotalPrice(),
            'url'     => Url::to('/workbench/order/view/' . $order->id, true)
        ];
    }

    /**
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionState()
    {
        $bodyParams = Yii::$app->getRequest()->queryParams;
        if (!array_key_exists('printablePackId', $bodyParams) && !array_key_exists('orderId', $bodyParams)) {
            throw new InvalidArgumentException('You should set filter parameter printablePackId or orderId.');
        }
        if (array_key_exists('printablePackId', $bodyParams)) {
            $apiPrintablePackId = $bodyParams['printablePackId'];
            $apiPrintablePack = ApiPrintablePack::tryFindByPk($apiPrintablePackId);
            $this->checkAccess('view', $apiPrintablePack);
            $storeOrder = $this->apiService->getPayedStoreOrder($apiPrintablePack);
        } elseif (array_key_exists('orderId', $bodyParams)) {
            $storeOrder = StoreOrder::tryFindByPk($bodyParams['orderId']);
            $this->checkAccessStoreOrder($storeOrder);
        } else {
            throw new InvalidArgumentException('You should set filter parameter printablePackId or orderId.');
        }
        $deliveryDetails = $storeOrder->getOrderDeliveryDetails();

        return [
            'success'      => true,
            'orderId'      => $storeOrder->id,
            'total'        => $storeOrder->getTotalPrice(),
            'url'          => Url::to('/workbench/order/view/' . $storeOrder->id, true),
            'orderState'   => $storeOrder->order_state,
            'paymentState' => $storeOrder->getPaymentStatus(),
            'attemptState' => $storeOrder->currentAttemp ? $storeOrder->currentAttemp->status : 'none',
            'shipping'     => [
                'carrier'         => $deliveryDetails['carrier'] ?? '',
                'trackingNumber'  => $deliveryDetails['tracking_number'] ?? '',
                'trackingShipper' => $deliveryDetails['tracking_shipper'] ?? ''
            ]
        ];
    }

    /**
     * @param CompanyService $psMachine
     * @param $model3dReplica
     * @return StoreOrder
     * @throws ErrorException
     * @throws UserException
     * @throws \lib\geo\GeoNamesException
     */
    protected function makeOrder(CompanyService $companyService, ApiPrintablePack $apiPrintablePack, $deliveryInfo)
    {
        // if make order, always create order and model binded to API owner
        $apiOwnerUser = $apiPrintablePack->apiExternalSystem->bindedUser;
        $apiPrintablePack->model3d->populateRelation('user', $apiOwnerUser);
        $apiPrintablePack->model3d->user_id = $apiOwnerUser->id;
        $apiPrintablePack->model3d->safeSave();

        $this->apiService->prepareForPrint($apiPrintablePack);

        $model3d = $apiPrintablePack->model3d;
        $posUid = 'PP' . $apiPrintablePack->public_token; // Same token for same printable token if reload page

        $model3dReplica = Model3dReplicaFactory::createModel3dReplica($apiPrintablePack->model3d);
        $model3dReplica->setStoreUnit($model3d->storeUnit);
        Model3dService::setCheapestMaterialInPrinter($model3dReplica, $companyService->psPrinter);
        Model3dReplicaRepository::save($model3dReplica);

        $cart = CartFactory::createCart($posUid);
        $cartItem = CartFactory::createCartItem($model3dReplica, $companyService, 1, true);
        $cart->addPosition($cartItem);
        if (!$cart->safeSave()) {
            throw new ErrorException('Save cart error: ' . json_encode($cart->getErrors()));
        };
        /** @var DeliveryForm $deliveryForm */
        $deliveryForm = $this->deliveryFormFactory->createApiByPsMachine($companyService);
        $deliveryForm->loadInfo($deliveryInfo);

        if ($deliveryForm->isInternational()) {
            $deliveryForm->deliveryType = DeliveryType::INTERNATIONAL;
        }
        $deliveryForm->validate();
        $errors = $deliveryForm->getErrors();
        if ($errors) {
            $validateErrors = ErrorListHelper::addErrorsFormPrefix($errors, 'deliveryform');
            throw new UnSuccessException('Delivery form invalid', $validateErrors);
        }
        if ($deliveryForm->getValidatedAddress() && $deliveryForm->getValidatedAddress()->isEasyPost()) {
            return [
                'validatedAddress' => ValidatedAddressSerializer::serialize($deliveryForm->getValidatedAddress())
            ];
        }
        $manufacturePrice = $this->priceCalculator->calculateModel3dPrintPriceWithPackageFee(
            model3d: $cart->getModel3d(),
            printer: $cart->getMachine()->asPrinter(),
            internationalDelivery: $deliveryForm->isInternational()
        );

        $rate = $this->getShippingRate(
            $companyService,
            $deliveryForm,
            $manufacturePrice,
            ParcelFactory::createFromCart($cart)
        );
        $rate->deliveryForm = $deliveryForm;
        $rate->deliveryParams = $companyService->asDeliveryParams();

        $user = $model3d->user; // model owner
        $order = OrderBuilder::create()
            ->customer($user, $deliveryForm->email)
            ->delivery($rate)
            ->comment($deliveryForm->comment)
            ->canChangeSupplier($cart->canChangePs())
            ->cart($cart)
            ->buildByItems();

        return $order;
    }

    protected function getShippingRate(CompanyService $machine, DeliveryForm $deliveryForm, Money $manufacturePrice, Parcel $parcel)
    {
        $userCurrency = UserSessionFacade::getCurrency();
        $formKey = serialize($deliveryForm->getAttributes(['deliveryType', 'contact_name', 'company', 'street', 'street2', 'city', 'state', 'zip', 'country']));
        $key = md5('g1' . $machine->id . $machine->getDeliveryOptionsHash() . $formKey . serialize($parcel->attributes) . $userCurrency);
        if ($shippingCache = app('cache')->get($key)) {
            return $shippingCache;
        }

        $rate = Yii::$app->deliveryService->getOrderRate($machine->asDeliveryParams(), $deliveryForm, $manufacturePrice, $parcel, $userCurrency);

        app('cache')->set($key, $rate);
        return $rate;
    }

    protected function checkAccessStoreOrder(StoreOrder $storeOrder)
    {
        if (!ApiExternalSystemAuthManager::getCurrentApiExternalSystem()) {
            throw new ForbiddenHttpException('Invalid api external system.');
        }
        if ($storeOrder->user_id !== ApiExternalSystemAuthManager::getCurrentApiExternalSystem()->binded_user_id) {
            throw new ForbiddenHttpException('Can`t access to this order.');
        }
    }

}