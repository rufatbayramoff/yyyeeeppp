<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 07.11.16
 * Time: 16:27
 */

namespace common\modules\api\v2\controllers;

use common\components\exceptions\BusinessException;
use common\components\order\PriceCalculator;
use common\components\orderOffer\OrderOfferLocatorService;
use common\components\ps\locator\Size;
use common\models\ApiPrintablePack;
use common\models\factories\GeoLocationFactory;
use common\models\factories\Model3dTextureFactory;
use common\models\model3d\Model3dCostInfo;
use common\models\model3d\StubModel3d;
use common\models\model3d\StubModel3dPart;
use common\models\Model3dPartProperties;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\models\repositories\PrinterMaterialRepository;
use common\modules\api\v2\components\ApiExternalSystemAuthManager;
use common\modules\api\v2\components\ApiPrintablePackRepository;
use common\modules\api\v2\models\ApiModelBluePrint;
use common\modules\api\v2\serializers\AllowedMaterialsSerializer;
use common\modules\api\v2\serializers\Model3dTextureSerializer;
use common\modules\api\v2\serializers\OffersBundleSerializer;
use common\modules\printersList\models\RequestInfo;
use common\services\ReviewService;
use InvalidArgumentException;
use lib\geo\models\Location;
use Yii;
use yii\helpers\Url;

class PrintablePackCostsController extends BaseApiController
{
    public $modelClass = ApiPrintablePack::class;

    /** @var  ApiPrintablePackRepository */
    public $diApiPrintablePackRepository;

    /** @var PrinterMaterialGroupRepository */
    public $printerMaterialGroupRepository;

    /** @var PrinterColorRepository */
    public $printerColorRepository;

    /** @var ReviewService */
    public $reviewService;

    public function injectDependencies(
        ApiPrintablePackRepository $diApiPrintablePackRepository,
        PrinterMaterialGroupRepository $printerMaterialGroupRepository,
        PrinterColorRepository $printerColorRepository,
        ReviewService $reviewService
    )
    {
        $this->diApiPrintablePackRepository   = $diApiPrintablePackRepository;
        $this->printerMaterialGroupRepository = $printerMaterialGroupRepository;
        $this->printerColorRepository         = $printerColorRepository;
        $this->reviewService                  = $reviewService;
    }

    public function behaviors()
    {
        $behaviors                  = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => ApiExternalSystemAuthManager::class,
        ];

        return $behaviors;
    }

    /**
     * User only default view action
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        $bodyParams = Yii::$app->getRequest()->queryParams;
        if (!array_key_exists('printablePackId', $bodyParams)) {
            throw new InvalidArgumentException('You should set filter parameter printablePackId.');
        }
        $apiPrintablePackId = $bodyParams['printablePackId'];
        $apiPrintablePack   = ApiPrintablePack::tryFindByPk($apiPrintablePackId);
        $this->checkAccess('view', $apiPrintablePack);

        if (array_key_exists('location', $bodyParams)) {
            if ($apiPrintablePack->geoLocation) {
                $apiPrintablePack->geoLocation->setLocationArr($bodyParams['location']);
                $apiPrintablePack->geoLocation->safeSave();
            } else {
                $locationArr = $bodyParams['location'];
                $geoLocation = GeoLocationFactory::create($locationArr);
                $geoLocation->safeSave();
                $apiPrintablePack->geo_location_id = $geoLocation->id;
                $apiPrintablePack->safeSave();
            }
        }

        if (!$apiPrintablePack->geoLocation) {
            throw new InvalidArgumentException(_t('site.api', 'Printable pack must contain client location information to get prices.'));
        }
        if (!$apiPrintablePack->model3d->isCalculatedProperties()) {
            return [
                'success' => false,
                'reason'  => ApiPrintablePack::REASON_NOT_CALCULATED_YET
            ];
        }

        $printerMaterialGroup = null;
        $printerColor         = null;
        if (array_key_exists('printerMaterialGroup', $bodyParams)) {
            $printerMaterialGroup = $this->printerMaterialGroupRepository->getByCode($bodyParams['printerMaterialGroup']);

        }
        if (array_key_exists('printerColor', $bodyParams)) {
            $printerColor = $this->printerColorRepository->getByCode($bodyParams['printerColor']);
        }

        $printingCosts = PriceCalculator::calculateModel3dPrintingCosts(
            $apiPrintablePack->model3d,
            $apiPrintablePack->geoLocation,
            $printerMaterialGroup,
            $printerColor
        );
        $returnValue   = [];
        $psRatings     = [];
        foreach ($printingCosts as $printingCost) {
            $psId = $printingCost->psPrinter->companyService->ps_id;
            if (!array_key_exists($psId, $psRatings)) {
                $reviewsCount     = $this->reviewService->getCompanyServiceShowedReviewsCount($printingCost->psPrinter->companyService);
                $rating           = $this->reviewService->calculateCompanyServiceRating($printingCost->psPrinter->companyService);
                $psRating         = [
                    'rating'       => $rating,
                    'reviewsCount' => $reviewsCount
                ];
                $psRatings[$psId] = $psRating;
            } else {
                $psRating = $psRatings[$psId];
            }
            $oneRetVal     = [
                'printablePackId' => $apiPrintablePack->id,
                'materialGroup'   => $printingCost->printerMaterialGroup->code,
                'printer'         => $printingCost->psPrinter->ps->title . ': ' . $printingCost->psPrinter->title,
                'providerId'      => $printingCost->psPrinter->id,
                'rating'          => $psRating['rating'],
                'reviewsCount'    => $psRating['reviewsCount'],
                'color'           => $printingCost->printerColor->render_color,
                'price'           => round($printingCost->price, 2),
                'deliveryPrice'   => $printingCost->estimateDeliveryCost ? $printingCost->estimateDeliveryCost->getAmount() : 0,
                'url'             => $this->getModel3dUrl($apiPrintablePack, $printingCost)
            ];
            $returnValue[] = $oneRetVal;
        }
        return $returnValue;
    }

    public function actionPriceInitial()
    {
        $printerMaterialRepository = Yii::createObject(PrinterMaterialRepository::class);
        $printerColorRepository    = Yii::createObject(PrinterColorRepository::class);
        $bodyParams                = Yii::$app->getRequest()->getBodyParams();
        if (!$bodyParams) {
            $bodyParams = app('request')->queryParams;
        }
        $bluePrint = new ApiModelBluePrint();
        $bluePrint->load($bodyParams, '');

        try {
            if (!$bluePrint->validate()) {
                $msg = implode(". ", array_values($bluePrint->errors)[0]);
                return ['success' => false, 'message' => $msg, 'errors' => $bluePrint->errors];
            }
            $model3d           = $this->createStubModel3dByBlueprint($bluePrint, $printerMaterialRepository, $printerColorRepository);
            $offersLocator     = \Yii::createObject(OrderOfferLocatorService::class);
            $location          = new Location();
            $location->country = $bluePrint->country;
            $offersLocator->initPrintersBundle(null, $location);
            $materials = $printerMaterialRepository->getByCode($bluePrint->materialCode);
            if (!$materials) {
                throw new BusinessException('Material not found');
            }
            $offersLocator->setAllowedMaterials([$materials]);
            $offersLocator->setSortMode(RequestInfo::SORT_STRATEGY_DEFAULT);
            $offersLocator->formOffersListForModel($model3d);

            $paginationOffersList = $offersLocator->getPaginationList(0, 10);
            $offersLocator->formCosts($paginationOffersList, $model3d);
            $allowedMaterials = $offersLocator->availableColorGroupsForCurrentPrintersList;

            $obs               = new OffersBundleSerializer();
            $offers            = $obs->serialize($paginationOffersList);
            $textureSerializer = new Model3dTextureSerializer();
            $model3dSerialized = $textureSerializer->serialize($model3d->getKitTexture());

            $materialsSerializer        = new AllowedMaterialsSerializer();
            $allowedMaterialsSerialized = $materialsSerializer->serialize(array_values($allowedMaterials));
        } catch (\Exception $e) {
            Yii::error($e);
            return ['success' => false, 'message' => 'Error in price calculation.', 'code' => $e->getLine()];
        }
        return ['success' => true, 'offers' => $offers, 'model3d' => $model3dSerialized, 'availableMaterialsColors' => $allowedMaterialsSerialized];
    }


    private function createStubModel3dByBlueprint(ApiModelBluePrint $bluePrint, $printerMaterialRepository, $printerColorRepository)
    {
        $stubPart                                = new StubModel3dPart();
        $stubPart->qty                           = 1;
        $stubPart->size                          = Size::create(
            $bluePrint->width,
            $bluePrint->height,
            $bluePrint->length,
            $bluePrint->metric
        );
        $stubPart->model3dPartProperties         = new Model3dPartProperties();
        $stubPart->model3dPartProperties->volume = $bluePrint->volume;
        $stubPart->model3dPartProperties->area   = $bluePrint->area;

        $stubModel  = new StubModel3d();
        $kitTexture = Model3dTextureFactory::createModel3dTexture(false);

        $material = $printerMaterialRepository->getByCode($bluePrint->materialCode);
        if ($material) {
            $kitTexture->setPrinterMaterial($material);
        }
        $printerColor = $printerColorRepository->getByCode($bluePrint->color);
        if ($printerColor) {
            $kitTexture->setPrinterColor($printerColor);
        }
        $stubModel->setKitTexture($kitTexture);

        $stubModel->model3dParts = [$stubPart];
        $stubPart->model3d       = $stubModel;
        $stubPart->populateRelation('model3d', $stubModel);
        return $stubModel;
    }

    /**
     * @param ApiPrintablePack $apiPrintablePack
     * @param Model3dCostInfo $printingCost
     * @return null
     * @throws \yii\base\InvalidParamException
     */
    public function getModel3dUrl(ApiPrintablePack $apiPrintablePack, Model3dCostInfo $printingCost)
    {
        $url = Url::toRoute(
            [
                '/catalog/model3d/preload-printable-pack',
                'packPublicToken'        => $apiPrintablePack->public_token,
                'printerMaterialGroupId' => $printingCost->printerMaterialGroup->id,
                'printerColorId'         => $printingCost->printerColor->id
            ],
            true
        );
        return $url;
    }

}