<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 07.11.16
 * Time: 16:27
 */

namespace common\modules\api\v2\controllers;

use common\components\order\predicates\NeedCertificateForDownloadModelPredicate;
use common\components\orderOffer\OrderOfferLocatorService;
use common\models\ApiPrintablePack;
use common\models\factories\LocationFactory;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\modules\api\v2\components\ApiExternalSystemAuthManager;
use common\modules\api\v2\components\ApiPrintablePackRepository;
use common\modules\api\v2\serializers\ApiOffersBundleSerializer;
use common\services\PrinterMaterialService;
use common\services\ReviewService;
use InvalidArgumentException;
use lib\money\Currency;
use Yii;

class PrintablePackOffersController extends BaseApiController
{
    public $modelClass = ApiPrintablePack::class;

    /** @var  ApiPrintablePackRepository */
    public $diApiPrintablePackRepository;

    /** @var PrinterMaterialGroupRepository */
    public $printerMaterialGroupRepository;

    /** @var PrinterColorRepository */
    public $printerColorRepository;

    /** @var ReviewService */
    public $reviewService;

    public function injectDependencies(
        ApiPrintablePackRepository $diApiPrintablePackRepository,
        PrinterMaterialGroupRepository $printerMaterialGroupRepository,
        PrinterColorRepository $printerColorRepository,
        ReviewService $reviewService
    ) {
        $this->diApiPrintablePackRepository = $diApiPrintablePackRepository;
        $this->printerMaterialGroupRepository = $printerMaterialGroupRepository;
        $this->printerColorRepository = $printerColorRepository;
        $this->reviewService = $reviewService;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => ApiExternalSystemAuthManager::class,
        ];

        return $behaviors;
    }

    /**
     * User only default view action
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        $bodyParams = Yii::$app->getRequest()->queryParams;
        if (!array_key_exists('printablePackId', $bodyParams)) {
            throw new InvalidArgumentException('You should set filter parameter printablePackId.');
        }
        $apiPrintablePackId = $bodyParams['printablePackId'];
        $apiPrintablePack = ApiPrintablePack::tryFindByPk($apiPrintablePackId);
        $this->checkAccess('view', $apiPrintablePack);

        if (!$apiPrintablePack->model3d->isCalculatedProperties()) {
            return [
                'success' => false,
                'reason'  => ApiPrintablePack::REASON_NOT_CALCULATED_YET
            ];
        }

        $model3d = $apiPrintablePack->model3d;
        $model3d->isAnyTextureAllowed = false;

        $offersLocator = \Yii::createObject(OrderOfferLocatorService::class);

        if (array_key_exists('intlOnly', $bodyParams) && $bodyParams['intlOnly']) {
            $offersLocator->onlyInternational = true;
        }
        $predicateOnlyCertificated = new NeedCertificateForDownloadModelPredicate();
        $onlyCertificated = $predicateOnlyCertificated->test($model3d);
        $location = LocationFactory::createFromGeoLocation($apiPrintablePack->geoLocation);
        $offersLocator->initAnonymousBundle($location, $onlyCertificated);
        $offersLocator->currencies[] = Currency::USD;
        $offersLocator->formOffersListForModel($model3d);
        $allOffersList = $offersLocator->getAllOffersList();

        $offersList = ApiOffersBundleSerializer::serialize($allOffersList);
        $modelTextureInfo = PrinterMaterialService::serializeModel3dTexture($apiPrintablePack->model3d);

        return [
            'success'    => true,
            'modelTextureInfo' => $modelTextureInfo,
            'offersList' => $offersList,
        ];
    }
}