<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.03.18
 * Time: 13:53
 */

namespace common\modules\api\v2\controllers;

use common\models\ApiPrintablePack;
use common\modules\api\v2\services\ApiService;
use frontend\models\model3d\PlaceOrderState;
use Yii;
use yii\web\Controller;

class PrintablePackWidgetController extends Controller
{
    /** @var ApiService */
    public $apiService;

    /**
     * @param ApiService $apiService
     */
    public function injectDependencies(
        ApiService $apiService
    ) {
        $this->apiService = $apiService;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        $apiPrintablePackToken = \Yii::$app->request->get('apiPrintablePackToken');
        $printablePack = ApiPrintablePack::tryFind(['public_token' => $apiPrintablePackToken]);

        $this->apiService->prepareForPrint($printablePack);

        $placeOrderState = Yii::createObject(PlaceOrderState::class);
        $posUid = 'PPW' . $printablePack->public_token;
        $placeOrderState->setStateUid($posUid); // Same token for same printable token if reload page
        $placeOrderState->setModel3d($printablePack->model3d);

        $redirectUrl = '/my/print-model3d/widget?posUid=' . $posUid;
        $this->redirect($redirectUrl);
    }
}