<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.06.19
 * Time: 13:53
 */

namespace common\modules\api\v2\controllers;

use common\components\BaseActiveQuery;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\modules\api\v2\components\ApiExternalSystemAuthManager;
use common\modules\api\v2\components\ApiPrintablePackRepository;
use common\modules\api\v2\services\ApiService;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\services\PaymentReceiptService;
use InvalidArgumentException;
use lib\money\Money;
use treatstock\api\v2\exceptions\UnSuccessException;
use Yii;
use yii\base\UserException;
use yii\web\NotAcceptableHttpException;
use yii\web\Response;

class PaymentController extends BaseApiController
{
    public $modelClass = '';

    /** @var  ApiPrintablePackRepository */
    public $diApiPrintablePackRepository;

    /** @var ApiService */
    public $apiService;

    /** @var PaymentReceiptService */
    public $paymentReceiptService;

    public function injectDependencies(ApiPrintablePackRepository $diApiPrintablePackRepository, ApiService $apiService, PaymentReceiptService $paymentReceiptService)
    {
        $this->diApiPrintablePackRepository = $diApiPrintablePackRepository;
        $this->apiService = $apiService;
        $this->paymentReceiptService = $paymentReceiptService;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => ApiExternalSystemAuthManager::class,
        ];

        return $behaviors;
    }

    /**
     * Disable default
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }

    /**
     * @return Response
     * @throws UnSuccessException
     * @throws UserException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPay()
    {
        $bodyParams = Yii::$app->getRequest()->getBodyParams();
        if (!array_key_exists('orderId', $bodyParams)) {
            throw new InvalidArgumentException('You should set parameter "orderId".');
        }
        if (!array_key_exists('total', $bodyParams)) {
            throw new InvalidArgumentException('You should set parameter "total".');
        }
        $totalSumCheck = $bodyParams['total'];
        $externalSystem = ApiExternalSystemAuthManager::getCurrentApiExternalSystem();
        $storeOrder = StoreOrder::tryFindByPk($bodyParams['orderId']);
        if ($storeOrder->user_id !== $externalSystem->binded_user_id) {
            throw new NotAcceptableHttpException('Invalid order access');
        }

        $printablePacks = $storeOrder->getFirstReplicaItem()->originalModel3d->apiPrintablePacks;
        $printablePack = reset($printablePacks);

        $this->checkAccess('view', $printablePack);

        $payedStoreOrder = $this->apiService->getPayedStoreOrder($printablePack);
        if ($payedStoreOrder) {
            throw new UnSuccessException('It is already payed.', ['already_payed']);
        }

        /** @var PaymentInvoice $paymentInvoice */
        $paymentInvoice = $storeOrder->primaryPaymentInvoice;
        BaseActiveQuery::$staticCacheGlobalEnable = false;
        $vendor = PaymentTransaction::VENDOR_TS;
        $paymentToken = '';

        $paymentInvoice->canBePaidOrFail();

        if (!$paymentInvoice->canPaidByMethodType($vendor, $externalSystem->bindedUser)) {
            throw new UnSuccessException('Unable to pay by current payment method.', 'can_not_be_paid_by_method');
        }

        if (!$paymentInvoice->getAmountTotal()->equal(Money::usd($totalSumCheck))) {
            throw new UnSuccessException('Total price is invalid. Should be: '.$paymentInvoice->getAmountTotal()->getAmount().'.', 'invalid_total');
        }

        $paymentInvoice->setPaymentInProgress();
        $paymentInvoice->setPrimaryInvoiceForRelatedObjects();

        $paymentCheckout = new PaymentCheckout($vendor);
        try {
            $paymentCheckout->pay($paymentInvoice, $paymentToken);
        } catch (\Exception $exception) {
            throw new UnSuccessException('Payment failed: ' . $exception->getMessage(), ['payment_failed']);
        }
        return [
            'success'      => true,
            'orderId'     => $storeOrder->id,
            'invoiceUuid' => $paymentInvoice->uuid,
            'total'       => $paymentInvoice->getAmountTotal()->getAmount(),
        ];
    }
}
