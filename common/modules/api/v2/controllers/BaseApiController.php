<?php
/**
 * Date: 30.06.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\api\v2\controllers;


use common\models\HttpRequestLog;
use common\modules\api\v2\components\ApiExternalSystemAuthManager;
use frontend\models\user\FrontUser;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class BaseApiController extends Controller
{
    /**
     * api token param
     *
     * @var string
     */
    public $tokenParam = 'api-token';


    /**
     * {@inheritdoc}
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    public function init()
    {
        parent::init();
        Yii::$app->getSession()->destroy();
    }


    public function beforeAction($action)
    {
        app()->httpRequestLogger->initRequestLog(HttpRequestLog::REQUEST_TYPE_API);
        return parent::beforeAction($action);
    }



    public function runAction($id, $params = [])
    {
        app()->httpRequestLogger->initRequestLog(HttpRequestLog::REQUEST_TYPE_API);

        $result = parent::runAction($id, $params);
        app()->httpRequestLogger->closeRequest($result);
        return $result;
    }

    /**
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws ForbiddenHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        ApiExternalSystemAuthManager::checkPrintablePackAccess($model);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class'   => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
                'application/jsonp' => Response::FORMAT_JSONP,
                //'application/xml'  => Response::FORMAT_XML,
            ],
        ];
        $behaviors['authenticator'] = [
            'class'       => CompositeAuth::className(),
            'authMethods' => [
                [
                    'class' => HttpBasicAuth::className(),
                    'auth'  => function ($username, $password) {
                        $user = FrontUser::findOne(
                            [
                                'username' => $username,
                            ]
                        );
                        if ($user && $user->validatePassword($password)) {
                            return $user;
                        }
                        return null;
                    }
                ],
                [
                    'class'      => QueryParamAuth::className(),
                    'tokenParam' => $this->tokenParam
                ]
            ],
        ];
        return $behaviors;
    }
}