<?php

namespace common\modules\api\v2\controllers;

use common\models\message\builders\TopicBuilder;
use common\models\message\forms\MessageForm;
use common\models\MsgTopic;
use common\modules\api\v2\components\ApiExternalSystemAuthManager;
use common\modules\api\v2\components\ApiPrintablePackRepository;
use common\modules\api\v2\serializers\MsgMessageSerializer;
use common\modules\api\v2\serializers\MsgTopicMessagesSerializer;
use common\modules\api\v2\services\ApiService;
use frontend\models\community\MessageFacade;
use treatstock\api\v2\exceptions\UnSuccessException;
use Yii;

class MsgTopicMessagesController extends BaseApiController
{
    public $modelClass = '';

    /** @var ApiPrintablePackRepository */
    public $apiPrintablePackRepository;

    /** @var ApiService */
    public $apiService;

    public function injectDependencies(ApiPrintablePackRepository $apiPrintablePackRepository, ApiService $apiService)
    {
        $this->apiPrintablePackRepository = $apiPrintablePackRepository;
        $this->apiService = $apiService;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => ApiExternalSystemAuthManager::class,
        ];

        return $behaviors;
    }

    /**
     * Disable default
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }

    public function actionIndex()
    {
        $queryParams = Yii::$app->getRequest()->queryParams;
        $storeOrder = $this->apiService->getOrderByFilters($queryParams);
        $orderId = $storeOrder->id;

        $topics = MsgTopic::findAll(['bind_to' => MsgTopic::BIND_OBJECT_ORDER, 'bind_id' => $orderId]);
        $topicsSerialized = MsgTopicMessagesSerializer::serialize($topics);
        return $topicsSerialized;
    }

    /**
     * Add message to last topic
     *
     * @throws UnSuccessException
     * @throws \yii\base\Exception
     * @throws \yii\web\NotAcceptableHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function actionSendMessage()
    {
        $queryParams = Yii::$app->getRequest()->queryParams;
        $bodyParams = Yii::$app->getRequest()->bodyParams;
        $params = $queryParams + $bodyParams;

        $storeOrder = $this->apiService->getOrderByFilters($params);
        $orderId = $storeOrder->id;
        $externalSystem = ApiExternalSystemAuthManager::getCurrentApiExternalSystem();

        if (!array_key_exists('messageText', $bodyParams)) {
            throw new UnSuccessException('Empty message Text', ['empty_message']);
        }
        $messageText = $bodyParams['messageText'];

        if (!$storeOrder->currentAttemp) {
            throw new UnSuccessException('No current printing attempt', 'no_current_attempt');
        }

        // Find exists topic
        $lastTopic = MsgTopic::find()
            ->where(['bind_to' => MsgTopic::BIND_OBJECT_ORDER, 'bind_id' => $orderId])
            ->forUser($storeOrder->currentAttemp->ps->user)
            ->orderBy('created_at desc')->one();

        if (!$lastTopic) {
            $lastTopic = TopicBuilder::builder()
                ->setInitialUser($externalSystem->bindedUser)
                ->setTitle('Order #' . $storeOrder->id)
                ->to($storeOrder->currentAttemp->ps->user)
                ->bindObject($storeOrder)
                ->create();
        }

        $messageForm = new MessageForm(['message' => $messageText]);
        $message = MessageFacade::addMessage($lastTopic, $externalSystem->bindedUser, $messageForm);

        return MsgMessageSerializer::serialize($message);
    }
}