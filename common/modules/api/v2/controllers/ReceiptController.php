<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.06.19
 * Time: 13:53
 */

namespace common\modules\api\v2\controllers;

use common\models\ApiPrintablePack;
use common\modules\api\v2\components\ApiExternalSystemAuthManager;
use common\modules\api\v2\components\ApiPrintablePackRepository;
use common\modules\api\v2\services\ApiService;
use common\modules\payment\services\PaymentReceiptService;
use InvalidArgumentException;
use treatstock\api\v2\exceptions\UnSuccessException;
use Yii;
use yii\base\UserException;
use yii\web\Response;

class ReceiptController extends BaseApiController
{
    public $modelClass = '';

    /** @var  ApiPrintablePackRepository */
    public $diApiPrintablePackRepository;

    /** @var ApiService */
    public $apiService;

    /** @var PaymentReceiptService */
    public $paymentReceiptService;

    public function injectDependencies(ApiPrintablePackRepository $diApiPrintablePackRepository, ApiService $apiService, PaymentReceiptService $paymentReceiptService)
    {
        $this->diApiPrintablePackRepository = $diApiPrintablePackRepository;
        $this->apiService = $apiService;
        $this->paymentReceiptService = $paymentReceiptService;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => ApiExternalSystemAuthManager::class,
        ];

        return $behaviors;
    }

    /**
     * Disable default
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }

    /**
     * @return Response
     * @throws UnSuccessException
     * @throws UserException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDownload()
    {
        $response = Yii::$app->getResponse();

        $queryParams = Yii::$app->getRequest()->queryParams;
        $storeOrder = $this->apiService->getOrderByFilters($queryParams);
        if (!$storeOrder) {
            throw new UnSuccessException('No payed orders exists', ['no_order_exists']);
        }
        $receipt = $this->paymentReceiptService->getReceiptByStoreOrder($storeOrder);
        $receiptPdfPath = $this->paymentReceiptService->getPdfReceipt($receipt);
        $response->setDownloadHeaders('receipt-' . $receipt->id . '.pdf', 'application/pdf', false);
        return $response->sendFile($receiptPdfPath);
    }
}
