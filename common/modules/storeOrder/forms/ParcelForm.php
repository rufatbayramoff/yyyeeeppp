<?php


namespace common\modules\storeOrder\forms;


use common\models\StoreOrderDelivery;
use lib\delivery\parcel\Parcel;
use lib\MeasurementUtil;
use yii\base\Model;

/**
 * @property float $widthMM
 * @property float $heightMM
 * @property float $lengthMM
 * @property float $weightGr
 * */
class ParcelForm extends Model
{
    public $width;
    public $height;
    public $length;
    public $weight;
    public $measure = MeasurementUtil::IN;

    public $rate;
    public $currency;

    public function __construct(StoreOrderDelivery $storeOrderDelivery = null, $config = [])
    {
        parent::__construct($config);
        if ($storeOrderDelivery) {
            if ($storeOrderDelivery->calculated_parcel_measure === MeasurementUtil::MM) {
                $this->width = round(MeasurementUtil::convertMillimetersToInches($storeOrderDelivery->calculated_parcel_width), 2);
                $this->height = round(MeasurementUtil::convertMillimetersToInches($storeOrderDelivery->calculated_parcel_height), 2);
                $this->length = round(MeasurementUtil::convertMillimetersToInches($storeOrderDelivery->calculated_parcel_length), 2);
                $this->weight = round(MeasurementUtil::convertGramToOunce($storeOrderDelivery->calculated_parcel_weight), 2);
            } else {
                $this->width = $storeOrderDelivery->calculated_parcel_width;
                $this->height = $storeOrderDelivery->calculated_parcel_height;
                $this->length = $storeOrderDelivery->calculated_parcel_length;
                $this->weight = $storeOrderDelivery->calculated_parcel_weight;
            }
        }
    }

    public function getWidthMM()
    {
        return round(MeasurementUtil::convertInchesToMm($this->width), 2);
    }

    public function getHeightMM()
    {
        return round(MeasurementUtil::convertInchesToMm($this->height), 2);
    }


    public function getLengthMM()
    {
        return round(MeasurementUtil::convertInchesToMm($this->length), 2);
    }

    public function getWeightGr()
    {
        return round(MeasurementUtil::convertOunceToGram($this->weight), 2);
    }

    public function rules(): array
    {
        return [
            [['weight', 'height', 'length', 'width'], 'required'],
            [['weight', 'height', 'length', 'width'], 'number']
        ];
    }

    public function parcel(): Parcel
    {
        return new Parcel(
            $this->width,
            $this->height,
            $this->length,
            $this->weight,
            MeasurementUtil::IN
        );
    }
}