<?php


namespace common\modules\storeOrder\helpers;


use lib\delivery\parcel\Parcel;
use lib\MeasurementUtil;

class ParcelHelper
{
    public static function formatMeasure(Parcel $calculatedParcel): string
    {
        if ($calculatedParcel->measure === MeasurementUtil::MM) {
            $oz = MeasurementUtil::convertGramToOunce($calculatedParcel->weight);
            $gr = $calculatedParcel->weight;
        } else {
            $oz = $calculatedParcel->weight;
            $gr = MeasurementUtil::convertOunceToGram($calculatedParcel->weight);
        }
        if (round($gr, 2) >= 456.93){
            return self::infinity();
        }


        [$widthInch, $heightInch, $lengthInch] = $calculatedParcel->getInchFormat();
        [$widthMM, $heightMM, $lengthMM] = $calculatedParcel->getMMFormat();
        return self::print([
            $oz,
            $gr,
            $lengthInch,
            $lengthMM,
            $widthInch,
            $widthMM,
            $heightInch,
            $heightMM,
        ]);
    }

    public static function format(array $data): string
    {
        [
            $weight,
            $length,
            $width,
            $height,
            $measure
        ] = $data;

        if ($measure === MeasurementUtil::MM) {
            return self::print([
                MeasurementUtil::convertGramToOunce($weight),
                $weight,
                MeasurementUtil::convertMillimetersToInches($length),
                $length,
                MeasurementUtil::convertMillimetersToInches($width),
                $width,
                MeasurementUtil::convertMillimetersToInches($height),
                $height,
            ]);
        }

        return self::print([
            $weight,
            MeasurementUtil::convertOunceToGram($weight),
            $length,
            MeasurementUtil::convertInchesToMm($length),
            $width,
            MeasurementUtil::convertInchesToMm($width),
            $height,
            MeasurementUtil::convertInchesToMm($height),
        ]);
    }

    public static function print(array $data): string
    {
        $data = array_map(function ($value) {
            return round($value, 2);
        }, $data);

        [
            $oz,
            $gr,
            $lengthInch,
            $lengthMM,
            $widthInch,
            $widthMM,
            $heightInc,
            $heightMM,
        ] = $data;

        return "Weight: {$oz} oz({$gr} gr)<br>Size: {$lengthInch}in x {$widthInch}in x {$heightInc}in ({$lengthMM}mm x {$widthMM}mm x {$heightMM}mm)";
    }

    public static function infinity(): string
    {
        return "Infinity box";

    }
}