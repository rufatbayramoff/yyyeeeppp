<?php


namespace common\modules\storeOrder\services;


use common\interfaces\DeliveryExpensiveInterface;
use lib\delivery\carrier\models\CarrierRate;
use lib\money\Money;

class DummyExpensive implements DeliveryExpensiveInterface
{

    public function check(CarrierRate $rate, Money $money): bool
    {
        return false;
    }
}