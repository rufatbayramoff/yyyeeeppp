<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.03.19
 * Time: 12:31
 */

namespace common\modules\storeOrder\exceptions;

use yii\base\UserException;

class StoreOrderException extends UserException
{

}