<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.06.18
 * Time: 18:10
 */

namespace common\modules\dynamicField\asset;

use yii\web\AssetBundle;

class IonRangeSliderAsset extends AssetBundle
{
    public $sourcePath = '@bower/ionrangeslider';

    public $css = [
        'css/ion.rangeSlider.css',
        'css/ion.rangeSlider.skinModern.css'
    ];

    public $js = [
        'js/ion.rangeSlider.min.js',
    ];
}