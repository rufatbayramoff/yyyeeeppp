<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.06.18
 * Time: 17:36
 */

namespace common\modules\dynamicField\forms;

use common\models\DynamicField;
use common\modules\dynamicField\models\DynamicFieldValue;

class DynamicFieldStringForm implements DynamicFieldFormInterface
{
    /** @var DynamicFieldValue */
    public $dynamicFieldValue;

    public function __construct(DynamicFieldValue $dynamicFieldValue)
    {
        $this->dynamicFieldValue = $dynamicFieldValue;
    }

    /**
     * @param string $formName
     * @return string
     */
    public function getInput($formName): string
    {
        if ($this->dynamicFieldValue->dynamicField->type !== DynamicField::TYPE_STRING) {
            throw new \LogicException('Invalid dynamic field type "' . $this->dynamicFieldValue->dynamicField->type . '" should be String');
        }
        $maxLength = $this->dynamicFieldValue->dynamicField->getTypeParams()['maxLength'] ?? 0;
        $maxLengthParam = $maxLength ? 'maxlength="' . $maxLength . '"' : '';
        $regular = $this->dynamicFieldValue->dynamicField->getTypeParams()['regularExpression'] ?? '';
        $multiValue = $this->dynamicFieldValue->dynamicField->getTypeParams()['multiValue'] ?? '';

        $id = strtolower($formName) . '-dynamicField_' . $this->dynamicFieldValue->dynamicField->code;
        $name = $formName . '[dynamicFields][' . $this->dynamicFieldValue->dynamicField->code . ']';
        $value = $this->dynamicFieldValue->value;

        if ($multiValue) {
            return '<textarea id="' . $id . '" ' . $maxLengthParam . ' name="' . $name . '" class="form-control">' . htmlspecialchars($value) . '</textarea>';
        } else {
            $pattern = $regular ? 'pattern="' . $regular . '"' : '';
            return '<input id="' . $id . '" class="form-control" name="' . $name . '" type="string" ' . $pattern . ' ' . $maxLengthParam . ' value=\'' . htmlspecialchars($value) . '\' >';
        }
    }
}