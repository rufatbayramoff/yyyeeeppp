<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.06.18
 * Time: 18:03
 */

namespace common\modules\dynamicField\forms;

use common\models\DynamicField;
use common\modules\dynamicField\models\DynamicFieldValue;

class DynamicFieldBooleanForm implements DynamicFieldFormInterface
{
    public $dynamicFieldValue;

    public function __construct(DynamicFieldValue $dynamicFieldValue)
    {
        $this->dynamicFieldValue = $dynamicFieldValue;
    }

    public function getInput($formName): string
    {
        $df = $this->dynamicFieldValue->dynamicField;
        if ($df->type !== DynamicField::TYPE_BOOLEAN) {
            throw new \LogicException('Invalid dynamic field type "' . $df->type . '" should be Boolean');
        }
        $id = strtolower($formName) . '-dynamicField_' . $df->code;
        $name = $formName . '[dynamicFields][' . $df->code . ']';
        $value = $this->dynamicFieldValue->value;


        $input = '<input type=hidden name="' . $name . '" value="0">';
        $input .= '<label class="checkbox-switch checkbox-switch--xs" style="margin: -2px 0 0 10px;">' . '<input type=checkbox id="' . $id . '" name="' . $name . '" ' . ($value ? 'checked' : '') . " value=\"1\">\n" . '<span class="slider"></span></label>';
        return $input;
    }
}
