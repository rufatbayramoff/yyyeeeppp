<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.06.18
 * Time: 17:37
 */

namespace common\modules\dynamicField\forms;

interface DynamicFieldFormInterface
{
    public function getInput($formName): string;
}