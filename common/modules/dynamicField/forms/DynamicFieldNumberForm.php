<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.06.18
 * Time: 17:36
 */

namespace common\modules\dynamicField\forms;

use common\models\DynamicField;
use common\modules\dynamicField\models\DynamicFieldValue;

class DynamicFieldNumberForm implements DynamicFieldFormInterface
{
    /** @var DynamicFieldValue */
    public $dynamicFieldValue;

    public function __construct(DynamicFieldValue $dynamicFieldValue)
    {
        $this->dynamicFieldValue = $dynamicFieldValue;
    }

    /**
     * @param string $formName
     * @return string
     */
    public function getInput($formName): string
    {
        if ($this->dynamicFieldValue->dynamicField->type !== DynamicField::TYPE_NUMBER) {
            throw new \LogicException('Invalid dynamic field type "' . $this->dynamicFieldValue->dynamicField->type . '" should be Number');
        }
        $decimalPrecision = $this->dynamicFieldValue->dynamicField->getTypeParams()['decimalPrecision'] ?? 0;
        $step = '';
        if ($decimalPrecision) {
            $step = '0.' . str_repeat('0', $decimalPrecision - 1) . '1';
        };
        $id = strtolower($formName) . '-dynamicField_' . $this->dynamicFieldValue->dynamicField->code;
        $name = $formName . '[dynamicFields][' . $this->dynamicFieldValue->dynamicField->code . ']';
        $value = $this->dynamicFieldValue->value;
        return '<input id="' . $id . '"  class="form-control" name="' . $name . '" type="number" ' . $step . ' value=\'' . $value . '\'>';
    }
}