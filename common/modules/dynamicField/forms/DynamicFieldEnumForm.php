<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.06.18
 * Time: 17:36
 */

namespace common\modules\dynamicField\forms;

use common\models\DynamicField;
use common\modules\dynamicField\models\DynamicFieldValue;
use common\modules\dynamicField\models\json\DynamicFieldTypeEnum;

class DynamicFieldEnumForm implements DynamicFieldFormInterface
{
    public $dynamicFieldValue;

    public function __construct(DynamicFieldValue $dynamicFieldValue)
    {
        $this->dynamicFieldValue = $dynamicFieldValue;
    }

    /**
     * @param $formName
     * @return string
     */
    public function getInput($formName): string
    {
        $df = $this->dynamicFieldValue->dynamicField;
        if ($df->type !== DynamicField::TYPE_ENUM) {
            throw new \LogicException('Invalid dynamic field type "' . $df->type . '" should be Enum');
        }
        $multiValue = $df->getTypeParams()['multiValue'] ?? '';

        $id = strtolower($formName) . '-dynamicField_' . $df->code;
        $name = $formName . '[dynamicFields][' . $df->code . ']' ;
        $value = $this->dynamicFieldValue->value;

        $allowValues = DynamicFieldTypeEnum::clearValuesList($df->getTypeParams());

        $input = '<select id="' . $id . '"  class="form-control" name="' . $name . '" ' . ($multiValue ? 'multiple="multiple"' : '') . ">\n";
        foreach ($allowValues as $allowValue => $allowValueLabel) {
            if ($multiValue) {
                $selected = '';
                if ((!is_array($value) && $allowValue == $value) || (is_array($value) && in_array($allowValue, $value ?: []))) {
                    $selected = 'selected';
                }

            } else {
                $selected = $allowValue == $value ? 'selected' : '';
            }
            $input .= '<option value="' . $allowValue . '" ' . $selected . '>' . $allowValueLabel . "</option>\n";
        }
        $input .= "</select>\n";
        return $input;
    }
}