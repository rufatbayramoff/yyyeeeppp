<?php

namespace common\modules\dynamicField;

use yii\base\Module;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class DynamicFieldModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\dynamicField\controllers';


    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();
    }
}
