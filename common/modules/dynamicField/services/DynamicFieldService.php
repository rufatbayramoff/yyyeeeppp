<?php
/**
 * User: nabi
 */

namespace common\modules\dynamicField\services;

use common\components\ArrayHelper;
use common\models\DynamicField;
use common\models\DynamicFieldCategory;
use common\models\Product;
use common\models\ProductCategory;
use common\modules\dynamicField\forms\DynamicFieldFormInterface;
use common\modules\dynamicField\models\DynamicFieldValue;
use common\modules\dynamicField\render\DynamicFieldRenderInterface;

class DynamicFieldService
{
    /**
     * @param $class
     * @return DynamicField[]
     */
    public function getModelFields($class)
    {
        // @TODO replace with repository
        return DynamicField::find()->where(['is_active' => 1, 'binded_model' => $class])->all();
    }

    /**
     * @return DynamicField[]
     * @param ProductCategory $category
     */
    public function getModelFieldsInCategory(ProductCategory $category)
    {
        /** @var DynamicFieldCategory[][] $dFCategoriesMap */
        $dFCategoriesMap = [];
        do {
            $raws = DynamicFieldCategory::find()
                ->where(
                    [
                        DynamicFieldCategory::column('category_id') => $category->id
                    ]
                )
                ->all();
            $dFCategoriesMap[$category->id] = $raws;
            $category = $category->parent;
        } while ($category);

        $dFCategoriesMap = array_reverse($dFCategoriesMap);
        $dynamicFieldsMaps = [];
        foreach ($dFCategoriesMap as $categoryId => $dfCategories) {
            foreach ($dfCategories as $dfCategory) {
                $df = $dfCategory->dynamicField;
                if (!$df) {
                    continue;
                }
                if (!$dfCategory->is_active || !$df->is_active) {
                    unset($dynamicFieldsMaps[$df->code]);
                } else {
                    $df->bindDynamicFieldCategory = $dfCategory;
                    $dynamicFieldsMaps[$df->code] = $df;
                }
            }
        }
        uasort($dynamicFieldsMaps, function (DynamicField $a, DynamicField $b) {
            return $a->bindDynamicFieldCategory->position > $b->bindDynamicFieldCategory->position;
        });
        return $dynamicFieldsMaps;
    }

    /**
     * @param ProductCategory $category
     * @param $values
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getDynamicFieldValues(ProductCategory $category, $values)
    {
        if (!$values) {
            $values = [];
        }
        $fields = $this->getModelFieldsInCategory($category);
        $dynamicValues = [];
        foreach ($fields as $dynamicField) {
            $dynamicValue = \Yii::createObject(DynamicFieldValue::class);
            $dynamicValue->dynamicField = $dynamicField;
            if (array_key_exists($dynamicField->code, $values)) {
                $dynamicValue->value = $values[$dynamicField->code];
            } else {
                $dynamicValue->value = $dynamicField->getDefaultValue();
            }
            $dynamicValues[] = $dynamicValue;
        }
        return $dynamicValues;
    }

    /**
     * @param DynamicFieldValue $dynamicValue
     * @return DynamicFieldFormInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getElementForm(DynamicFieldValue $dynamicValue): DynamicFieldFormInterface
    {
        /** @var DynamicFieldFormInterface $dynamicFieldForm */
        $dynamicFieldForm = \Yii::createObject([
            'class'             => 'common\modules\dynamicField\forms\DynamicField' . ucfirst($dynamicValue->dynamicField->type) . 'Form',
            'dynamicFieldValue' => $dynamicValue
        ]);
        return $dynamicFieldForm;
    }

    /**
     * @param DynamicFieldValue $dynamicValue
     * @return DynamicFieldRenderInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getElementRender(DynamicFieldValue $dynamicValue): DynamicFieldRenderInterface
    {
        /** @var DynamicFieldRenderInterface $dynamicFieldRender */
        $dynamicFieldRender = \Yii::createObject([
            'class'             => 'common\modules\dynamicField\render\DynamicField' . ucfirst($dynamicValue->dynamicField->type) . 'Render',
            'dynamicFieldValue' => $dynamicValue
        ]);
        return $dynamicFieldRender;
    }

    public function displayValue(DynamicFieldValue $dynamicValue)
    {
        return $this->getElementRender($dynamicValue)->render();
    }

    /**
     * @param DynamicFieldValue[] $dynamicValues
     */
    public function formDbValueJson($dynamicValues)
    {
        $json = [];
        foreach ($dynamicValues as $dynamicValue) {
            $json[$dynamicValue->dynamicField->code] = $dynamicValue->value;
        }
        return $json;
    }


    /**
     * @TODO change to
     * DynamicField::find()->forCategory($category)->hasFilter()->active()->forModel(Product::class);
     *
     * @param $class
     * @return array|DynamicField[]|\common\models\Product[]|mixed|\yii\db\ActiveRecord[]
     */
    public function getModelFieldsWithFilter($class)
    {
        return DynamicField::find()
            ->joinWith('dynamicFieldCategories')
            ->where(
                [
                    'binded_model'                             => $class,
                    DynamicField::column('is_active')          => 1,
                    DynamicFieldCategory::column('is_active')  => 1,
                    DynamicFieldCategory::column('has_filter') => 1
                ]
            )
            ->all();
    }
}