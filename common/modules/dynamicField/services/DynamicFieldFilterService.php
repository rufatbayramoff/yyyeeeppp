<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.06.18
 * Time: 15:32
 */

namespace common\modules\dynamicField\services;

use common\components\exceptions\InvalidModelException;
use common\models\DynamicField;
use common\models\ProductCategory;
use common\modules\dynamicField\filterTemplates\DfBooleanCheckboxFilter;
use common\modules\dynamicField\filterTemplates\DfBooleanDropdownFilter;
use common\modules\dynamicField\filterTemplates\DfEnumCheckboxFilter;
use common\modules\dynamicField\filterTemplates\DfEnumSelect2Filter;
use common\modules\dynamicField\filterTemplates\DfNumberInputFilter;
use common\modules\dynamicField\filterTemplates\DfNumberSliderFilter;
use common\modules\dynamicField\filterTemplates\DfStringInputFilter;
use common\modules\dynamicField\filterTemplates\DynamicFieldFilterInterface;
use yii\base\BaseObject;

class DynamicFieldFilterService extends BaseObject
{
    /**
     * @var DynamicFieldService
     */
    public $dynamicFieldService;

    /**
     * @param DynamicFieldService $dynamicFieldService
     */
    public function injectDependencies(DynamicFieldService $dynamicFieldService)
    {
        $this->dynamicFieldService = $dynamicFieldService;
    }


    public function getDfFilters()
    {
        return [
            DynamicField::TYPE_NUMBER => [
                DfNumberSliderFilter::class,
                DfNumberInputFilter::class
            ],
            DynamicField::TYPE_STRING => [
                DfStringInputFilter::class
            ],
            DynamicField::TYPE_ENUM => [
                DfEnumCheckboxFilter::class,
                DfEnumSelect2Filter::class,
            ],
            DynamicField::TYPE_BOOLEAN => [
                DfBooleanCheckboxFilter::class,
                DfBooleanDropdownFilter::class
            ]
        ];
    }

    /**
     * @param DynamicField $dynamicField
     * @return DynamicFieldFilterInterface[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getFiltersListForField(DynamicField $dynamicField): array
    {
        $dfFields = $this->getDfFilters();
        $classList = $dfFields[$dynamicField->type];
        $objects = [];
        foreach ($classList as $class) {
            $objects[$class] = \Yii::createObject(['class' => $class, 'dynamicField' => $dynamicField]);
        }
        return $objects;
    }


    /**
     * @param $dynamicField
     * @return array
     */
    public function getFilterNamesListForField($dynamicField)
    {
        $dfFields = $this->getDfFilters();
        if (!array_key_exists($dynamicField->type, $dfFields)) {
            return [];
        }
        $classList = $dfFields[$dynamicField->type];
        $titles = [];
        foreach ($classList as $class) {
            $titles[$class] = substr($class, strrpos($class, '\\') + 1, 1024);
        }
        return $titles;
    }

    /**
     * @param ProductCategory $category
     * @return DynamicFieldFilterInterface[]
     * @throws InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function getFiltersListForCategory(ProductCategory $category): array
    {
        $filters = [];
        $fields = $this->dynamicFieldService->getModelFieldsInCategory($category);
        foreach ($fields as $field) {
            if ($field->bindDynamicFieldCategory->has_filter && $field->filter_class) {
                $filter = \Yii::createObject(['class' => $field->filter_class, 'dynamicField' => $field]);
                if (!($filter instanceof DynamicFieldFilterInterface)) {
                    throw new InvalidModelException('Dynamic field filter interface not realised');
                }
                $filters[$field->code] = $filter;
            }
        }
        return $filters;
    }
}