<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.06.18
 * Time: 14:11
 */

namespace common\modules\dynamicField\render;

class DynamicFieldBooleanRender extends DynamicFieldStringRender
{
    public function render(): string
    {
        return $this->dynamicFieldValue->value ? _t('site.dynamicField', 'yes') : _t('site.dynamicField', 'no');
    }
}