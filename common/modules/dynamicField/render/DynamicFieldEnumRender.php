<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.06.18
 * Time: 18:04
 */

namespace common\modules\dynamicField\render;

use common\modules\dynamicField\models\DynamicFieldValue;

class DynamicFieldEnumRender implements DynamicFieldRenderInterface
{
    /** @var DynamicFieldValue */
    public $dynamicFieldValue;

    public function __construct(DynamicFieldValue $dynamicFieldValue)
    {
        $this->dynamicFieldValue = $dynamicFieldValue;
    }

    public function render(): string
    {
        if (is_array($this->dynamicFieldValue->value)) {
            return implode(', ', $this->dynamicFieldValue->value);
        } else {
            return $this->dynamicFieldValue->value;
        }
    }
}