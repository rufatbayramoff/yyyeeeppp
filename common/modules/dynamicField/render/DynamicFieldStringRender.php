<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.06.18
 * Time: 18:04
 */

namespace common\modules\dynamicField\render;

use common\modules\dynamicField\models\DynamicFieldValue;

class DynamicFieldStringRender implements DynamicFieldRenderInterface
{
    /** @var DynamicFieldValue */
    public $dynamicFieldValue;

    public function __construct(DynamicFieldValue $dynamicFieldValue)
    {
        $this->dynamicFieldValue = $dynamicFieldValue;
    }

    public function render(): string
    {
        return $this->dynamicFieldValue->value;
    }
}