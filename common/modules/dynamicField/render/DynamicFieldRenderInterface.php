<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.06.18
 * Time: 18:03
 */

namespace common\modules\dynamicField\render;

interface DynamicFieldRenderInterface
{
    public function render(): string;
}

