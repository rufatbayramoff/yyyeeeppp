<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.06.18
 * Time: 16:16
 */

namespace common\modules\dynamicField\filterTemplates;

use common\models\DynamicField;
use common\modules\dynamicField\models\json\DynamicFieldTypeEnum;
use yii\web\View;

class DfEnumSelect2Filter implements DynamicFieldFilterInterface
{
    /** @var DynamicField $dynamicField */
    public $dynamicField;

    /** @var array */
    protected $formData = [];

    public function __construct(DynamicField $dynamicField)
    {
        $this->dynamicField = $dynamicField;
    }

    public function getElementForm(View $view)
    {
        if ($this->dynamicField->type !== DynamicField::TYPE_ENUM) {
            throw new \LogicException('Invalid dynamic field type "' . $this->dynamicField->type . '" should be Enum');
        }
        $dfCode = $this->dynamicField->code;
        $id = 'dynamicFields_' . $dfCode;
        $name = 'dynamicFields[' . $dfCode . ']';
        $multiValue = $this->dynamicField->getTypeParams()['multiValue'] ?? '';
        $values = $this->formDataArray();

        $allowValues = DynamicFieldTypeEnum::clearValuesList($this->dynamicField->getTypeParams());
        if (!$multiValue) {
            // Radio button - any
            $allowValues = array_merge(
                ['any' => _t('site.dynamicFields', 'any')],
                $allowValues
            );
        }
        $input = \kartik\select2\Select2::widget([
            'id'           => $id,
            'name'         => $name,
            'value'        => $values,
            'data'         => $allowValues,
            'options'      => ['multiple' => $multiValue, 'placeholder' => 'Select value...'],
            'pluginEvents' => [
                'change' => 'function(t){ $("#searchPanel").trigger("submitSearch", [{"df_' . $this->dynamicField->code . '": $("#' . $id . '").val()' .
                    ($multiValue ? '.join(\',\')' : '') . '}]); }'
            ],
        ]);
        return $input;

    }

    public function formDataArray()
    {
        return ($this->formData && $this->formData['value']) ? explode(',', $this->formData['value']) : [''];
    }

    public function formDataArrayClean()
    {
        $values = $this->formDataArray();
        $allowValues = DynamicFieldTypeEnum::clearValuesList($this->dynamicField->getTypeParams());
        return array_intersect($values, $allowValues);
    }

    public function load($formData)
    {
        $this->formData = ['value' => $formData];
    }

    public function buildQuery(\SolrQuery $solrQuery)
    {
        if (!$this->formData || $this->formData['value'] === '') {
            return false;
        }
        $values = $this->formDataArray();
        $queryText = '';
        foreach ($values as $value) {
            if ($value === 'any') {
                continue;
            }
            $queryText .= 'df_' . $this->dynamicField->code . ':"s' . $value . '" or ';
        }
        if ($queryText) {
            $queryText = substr($queryText, 0, -4);
            $solrQuery->addFilterQuery($queryText);
        }
        return true;
    }
}