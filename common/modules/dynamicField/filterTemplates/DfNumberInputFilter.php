<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.06.18
 * Time: 14:43
 */

namespace common\modules\dynamicField\filterTemplates;

use common\models\DynamicField;
use common\modules\dynamicField\models\json\DynamicFieldTypeNumber;
use yii\web\View;

class DfNumberInputFilter implements DynamicFieldFilterInterface
{
    /** @var DynamicField $dynamicField */
    public $dynamicField;

    /** @var array */
    public $formData = [];

    public function __construct(DynamicField $dynamicField)
    {
        $this->dynamicField = $dynamicField;
    }

    /**
     * @return string
     * @throws \LogicException
     */
    public function getElementForm(View $view)
    {
        if ($this->dynamicField->type !== DynamicField::TYPE_NUMBER) {
            throw new \LogicException('Invalid dynamic field type "' . $this->dynamicField->type . '" should be Number');
        }
        $id = 'dynamicFields_' . $this->dynamicField->code;
        $name = 'dynamicFields[' . $this->dynamicField->code . ']';
        $value = $this->formData['value'] ?? '';
        $input = '<input id="' . $id . '"  class="form-control" name="' . $name . '" type="number" value=\'' . $value .
            '\' onchange=\'$("#searchPanel").trigger("submitSearch", [{"df_' . $this->dynamicField->code . '": $("#' . $id . '").val()}]);\'>';
        return $input;

    }

    public function load($formData)
    {
        $this->formData = ['value' => $formData];
    }

    public function buildQuery(\SolrQuery $solrQuery)
    {
        if (!$this->formData || $this->formData['value'] === '') {
            return false;
        }
        $value = $this->formData['value'];
        $queryText = 'df_' . $this->dynamicField->code . ':' . $value;
        $solrQuery->addFilterQuery($queryText);
        return true;
    }
}