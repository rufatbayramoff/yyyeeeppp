<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.06.18
 * Time: 14:43
 */

namespace common\modules\dynamicField\filterTemplates;

use common\models\DynamicField;
use common\modules\dynamicField\asset\IonRangeSliderAsset;
use common\modules\dynamicField\models\json\DynamicFieldTypeNumber;
use yii\web\View;

class DfNumberSliderFilter implements DynamicFieldFilterInterface
{
    /** @var DynamicField $dynamicField */
    public $dynamicField;

    /** @var array */
    public $formData = [];

    public function __construct(DynamicField $dynamicField)
    {
        $this->dynamicField = $dynamicField;
    }

    /**
     * @param View $view
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getElementForm(View $view)
    {
        if ($this->dynamicField->type !== DynamicField::TYPE_NUMBER) {
            throw new \LogicException('Invalid dynamic field type "' . $this->dynamicField->type . '" should be Number');
        }
        $id = 'dynamicFields_' . $this->dynamicField->code;
        $name = 'dynamicFields[' . $this->dynamicField->code . ']';
        $value = $this->formData['value'] ?? '';
        $input = '<div class="dynamic-field-filter__range"><input id="' . $id . '"  name="' . $name . '" type="hidden" value=\'' . $value . '\'></div>';
        $typeParams = $this->dynamicField->getTypeParams();
        $min = $typeParams['min'] ?? 0;
        $max = $typeParams['max'] ?? 100;
        $decimalPrec = $typeParams['decimalPrecision'] ?? 0;
        $sliderConfig = [
            'type'     => '"double"',
            'grid'     => true,
            'min'      => $min,
            'max'      => $max,
            'step'      => 1 / intval('1'.str_repeat(0, $decimalPrec)),
            'onFinish' => 'function (data) {$("#searchPanel").trigger("submitSearch", [{"df_' . $this->dynamicField->code . '": $("#' . $id . '").val()}]);}'
        ];
        $sliderConfigStr = '{';
        foreach ($sliderConfig as $configKey => $configItem) {
            $sliderConfigStr .= $configKey . ': ' . $configItem . ', ';
        }
        $sliderConfigStr .= '}';
        $script = '$(function() {$("#' . $id . '").ionRangeSlider(' . $sliderConfigStr . ');});';
        $view->registerJs($script);
        $view->registerAssetBundle(IonRangeSliderAsset::class);
        return $input;

    }

    public function load($formData)
    {
        $this->formData['value'] = $formData;
    }

    public function buildQuery(\SolrQuery $solrQuery)
    {
        if (!$this->formData) {
            return false;
        }
        list($min, $max) = explode(';', $this->formData['value']);
        $queryText = 'df_' . $this->dynamicField->code . ':[' . $min . ' TO ' . $max . ']';
        $solrQuery->addFilterQuery($queryText);
        return true;
    }
}