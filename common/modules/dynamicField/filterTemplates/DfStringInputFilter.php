<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.06.18
 * Time: 15:41
 */

namespace common\modules\dynamicField\filterTemplates;

use common\models\DynamicField;
use yii\web\View;

class DfStringInputFilter implements DynamicFieldFilterInterface
{
    /** @var DynamicField $dynamicField */
    public $dynamicField;

    /** @var array */
    protected $formData = [];

    public function __construct(DynamicField $dynamicField)
    {
        $this->dynamicField = $dynamicField;
    }

    public function getElementForm(View $view)
    {
        if ($this->dynamicField->type !== DynamicField::TYPE_STRING) {
            throw new \LogicException('Invalid dynamic field type "' . $this->dynamicField->type . '" should be String');
        }
        $id = 'dynamicFields_' . $this->dynamicField->code;
        $name = 'dynamicFields[' . $this->dynamicField->code . ']';
        $value = $this->formData['value'] ?? '';
        $input = '<input id="' . $id . '"  class="form-control " name="' . $name . '" type="text" value=\'' . $value .
            '\' onchange=\'$("#searchPanel").trigger("submitSearch", [{"df_' . $this->dynamicField->code . '": $("#' . $id . '").val()}]);\'>';
        return $input;

    }

    public function load($formData)
    {
        $this->formData = ['value' => $formData];
    }

    public function buildQuery(\SolrQuery $solrQuery)
    {
        if (!$this->formData || $this->formData['value'] === '') {
            return false;
        }
        $value = $this->formData['value'];
        $queryText = 'df_' . $this->dynamicField->code . ':*' . $value.'*';
        $solrQuery->addFilterQuery($queryText);
        return true;
    }
}