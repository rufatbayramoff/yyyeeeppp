<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.06.18
 * Time: 16:16
 */

namespace common\modules\dynamicField\filterTemplates;

use common\models\DynamicField;
use common\modules\dynamicField\models\json\DynamicFieldTypeEnum;
use yii\web\View;

class DfBooleanCheckboxFilter implements DynamicFieldFilterInterface
{
    /** @var DynamicField $dynamicField */
    public $dynamicField;

    /** @var array */
    protected $formData = [];

    public function __construct(DynamicField $dynamicField)
    {
        $this->dynamicField = $dynamicField;
    }

    public function getElementForm(View $view)
    {
        if ($this->dynamicField->type !== DynamicField::TYPE_BOOLEAN) {
            throw new \LogicException('Invalid dynamic field type "' . $this->dynamicField->type . '" should be Boolean');
        }
        $dfCode = $this->dynamicField->code;
        $id = 'dynamicFields_' . $dfCode;
        $name = 'dynamicFields[' . $dfCode . ']';

        $checked = ($this->formData && $this->formData['value']) ? 'checked' : '';
        $input = '<label class="checkbox-switch checkbox-switch--xs" style="margin: -2px 0 0 10px;">' . '<input id="' . $id . '" type="checkbox" name="' . $name . '" ' . $checked .
            ' value=1 onchange="$(\'#searchPanel\').trigger(\'submitSearch\', [{\'df_' . $dfCode . '\': $(this).is(\':checked\')?1:0}])"/>' . '<span class="slider"></span></label>';
        return $input;

    }

    public function load($formData)
    {
        $this->formData = ['value' => $formData];
    }

    public function buildQuery(\SolrQuery $solrQuery)
    {
        if (!$this->formData || !$this->formData['value']) {
            return false;
        }
        $value = (int)$this->formData['value'];
        $queryText = 'df_' . $this->dynamicField->code . ':' . $value;
        $solrQuery->addFilterQuery($queryText);
        return true;
    }
}