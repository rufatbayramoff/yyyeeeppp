<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.06.18
 * Time: 15:08
 */

namespace common\modules\dynamicField\filterTemplates;

use common\models\DynamicField;
use common\modules\dynamicField\models\json\DynamicFieldTypeEnum;
use yii\web\View;

class DfBooleanDropdownFilter implements DynamicFieldFilterInterface
{
    /** @var DynamicField $dynamicField */
    public $dynamicField;

    /** @var array */
    protected $formData = [];

    public function __construct(DynamicField $dynamicField)
    {
        $this->dynamicField = $dynamicField;
    }

    public function getElementForm(View $view)
    {
        if ($this->dynamicField->type !== DynamicField::TYPE_BOOLEAN) {
            throw new \LogicException('Invalid dynamic field type "' . $this->dynamicField->type . '" should be Boolean');
        }
        $dfCode = $this->dynamicField->code;
        $id = 'dynamicFields_' . $dfCode;
        $name = 'dynamicFields[' . $dfCode . ']';
        $value = $this->formData['value'] ?? '';

        $input = '<select id="' . $id . '" name="' . $name . '" class="form-control" ' .
            'onchange="$(\'#searchPanel\').trigger(\'submitSearch\', [{\'df_' . $dfCode . '\': $(\'#' . $id . '\').val()}])">' .
            '<option value="">' . _t('site.dynamicFields', 'any') . '</option>' .
            '<option value="1" ' . ($value === '1' ? 'selected' : '') . '>' . _t('site.dynamicFields', 'yes') . '</option>' .
            '<option value="0" ' . ($value === '0' ? 'selected' : '') . '>' . _t('site.dynamicFields', 'no') . '</option>' .
            '</select>';
        return $input;

    }

    public function load($formData)
    {
        $this->formData = ['value' => $formData];
    }

    public function buildQuery(\SolrQuery $solrQuery)
    {
        if (!$this->formData || $this->formData['value'] === '') {
            return false;
        }
        $value = (int)$this->formData['value'];
        $queryText = 'df_' . $this->dynamicField->code . ':' . $value;
        $solrQuery->addFilterQuery($queryText);
        return true;
    }
}