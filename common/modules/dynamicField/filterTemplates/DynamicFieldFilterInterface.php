<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.06.18
 * Time: 14:48
 */

namespace common\modules\dynamicField\filterTemplates;

use common\models\DynamicField;
use yii\web\View;

/**
 * Interface DynamicFieldFilterInterface
 *
 * @property DynamicField $dynamicField
 * @package common\modules\dynamicField\filterTemplates
 */
interface DynamicFieldFilterInterface
{
    public function getElementForm(View $view);
    public function load($formData);
    public function buildQuery(\SolrQuery $solrQuery);
}