<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.06.18
 * Time: 16:16
 */

namespace common\modules\dynamicField\filterTemplates;

use common\models\DynamicField;
use common\modules\dynamicField\models\json\DynamicFieldTypeEnum;
use yii\web\View;

class DfEnumCheckboxFilter implements DynamicFieldFilterInterface
{
    /** @var DynamicField $dynamicField */
    public $dynamicField;

    /** @var array */
    protected $formData = [];

    public function __construct(DynamicField $dynamicField)
    {
        $this->dynamicField = $dynamicField;
    }

    public function getElementForm(View $view)
    {
        if ($this->dynamicField->type !== DynamicField::TYPE_ENUM) {
            throw new \LogicException('Invalid dynamic field type "' . $this->dynamicField->type . '" should be Enum');
        }
        $dfCode = $this->dynamicField->code;
        $id = 'dynamicFields_' . $dfCode;
        $name = 'dynamicFields[' . $dfCode . ']';
        $multiValue = $this->dynamicField->getTypeParams()['multiValue'] ?? '';
        $values = $this->formDataArray();

        $i = 1;
        $allowValues = DynamicFieldTypeEnum::clearValuesList($this->dynamicField->getTypeParams());
        $input = '<div class="dynamic-field-filter__checks">';
        if (!$multiValue) {
            // Radio button - any
            $allowValues = ['' => _t('site.dynamicFields', 'All')] + $allowValues;
        }
        foreach ($allowValues as $allowValue => $allowValueLabel) {
            $inputId = $id . $i;
            $inputType = $multiValue ? 'checkbox' : 'radio';
            $checked = in_array($allowValue, $values) ? 'checked' : '';
            $input .= '<div class="' . $inputType . '"><input id="' . $inputId . '" type="' . $inputType . '" name="' . $name . '" ' . $checked . ' value="' . $allowValue .
                '" label="' . $allowValueLabel . '" onchange="$(\'#searchPanel\').trigger(\'dfEnum\', [\'' . $dfCode . '\'])"/>' .
                '<label for="' . $inputId . '" style="font-weight:normal"> &nbsp;' . $allowValueLabel . '</label></div>';
            $i++;
        }
        $input .= '</div>';
        return $input;

    }

    public function formDataArray()
    {
        return ($this->formData && $this->formData['value']) ? explode(',', $this->formData['value']) : [''];
    }

    public function formDataArrayClean()
    {
        $values = $this->formDataArray();
        $allowValues = DynamicFieldTypeEnum::clearValuesList($this->dynamicField->getTypeParams());
        return array_intersect($values, $allowValues);
    }

    public function load($formData)
    {
        $this->formData = ['value' => $formData];
    }

    public function buildQuery(\SolrQuery $solrQuery)
    {
        if (!$this->formData || $this->formData['value'] === '') {
            return false;
        }
        $values = $this->formDataArray();
        $queryText = '';
        foreach ($values as $value) {
            $queryText .= 'df_' . $this->dynamicField->code . ':"s' . $value . '" or ';
        }
        $queryText = substr($queryText, 0, -4);

        $solrQuery->addFilterQuery($queryText);
        return true;
    }
}

