<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.06.18
 * Time: 14:22
 */

namespace common\modules\dynamicField\models\repositories;


use common\models\DynamicFieldCategory;
use yii\base\BaseObject;

class DynamicFieldCategoryRepository  extends BaseObject
{

    /**
     * @param DynamicFieldCategory $dynamicFieldCategory
     * @throws \yii\base\Exception
     */
    public function saveDynamicFieldCategory(DynamicFieldCategory $dynamicFieldCategory)
    {
        $dynamicFieldCategory->safeSave();
    }

    /**
     * @param DynamicFieldCategory $dynamicFieldCategory
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function deleteDynamicField(DynamicFieldCategory $dynamicFieldCategory)
    {
        $dynamicFieldCategory->delete();
    }
}