<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.06.18
 * Time: 11:18
 */

namespace common\modules\dynamicField\models\repositories;


use common\models\DynamicField;
use yii\base\BaseObject;

class DynamicFieldRepository extends BaseObject
{

    public function saveDynamicField(DynamicField $dynamicField)
    {
        if (empty($dynamicField->type_params)) {
            $dynamicField->type_params = null;
        }
        $dynamicField->safeSave();
    }

    public function deleteDynamicField(DynamicField $dynamicField)
    {
        $dynamicField->delete();
    }
}