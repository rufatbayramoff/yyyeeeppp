<?php

namespace common\modules\dynamicField\models\populators;

use common\components\AbstractPopulator;
use common\models\DynamicFieldCategory;

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.06.18
 * Time: 14:30
 */
class DynamicFieldCategoryPopulator extends AbstractPopulator
{
    /**
     * @param DynamicFieldCategory $dynamicFieldCategory
     * @param $data
     * @throws \yii\base\InvalidConfigException
     */
    public function populate(\common\models\DynamicFieldCategory $dynamicFieldCategory, $data)
    {
        $formName = $dynamicFieldCategory->formName();
        if (array_key_exists($formName, $data)) {
            $dfForm = $data[$formName];
            if ($dfForm) {
                $this->loadAttributes($dynamicFieldCategory, $dfForm);
                $this->loadTypeParams($dynamicFieldCategory, $dfForm);
            }
        }
    }

    /**
     * @param DynamicFieldCategory $dynamicFieldCategory
     * @param $data
     */
    public function loadAttributes(DynamicFieldCategory $dynamicFieldCategory, $data)
    {
        if (array_key_exists('is_required', $data) && $data['is_required'] === '') {
            $data['is_required'] = null;
        }
        $this->populateAttributes($dynamicFieldCategory, $data, ['category_id', 'is_required', 'default_value', 'is_active', 'has_filter', 'position']);
    }

    /**
     * @param DynamicFieldCategory $dynamicFieldCategory
     * @param $data
     */
    public function loadTypeParams(DynamicFieldCategory $dynamicFieldCategory, $data)
    {
        if (array_key_exists('hasTypeParams', $data)) {
            if (!$data['hasTypeParams']) {
                $dynamicFieldCategory->type_params = [];
                return;
            }
        }

        if (array_key_exists('typeParams', $data)) {
            $typeParams = json_decode($data['typeParams'] ?? '{}', true);
        } elseif (array_key_exists('typeParamsString', $data)) {
            $typeParams = json_decode($data['typeParamsString'] ?? '{}', true);
        } else {
            $typeParams = [];
        }
        $dynamicFieldCategory->type_params = $typeParams;
    }
}