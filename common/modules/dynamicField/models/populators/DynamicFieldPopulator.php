<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.06.18
 * Time: 11:18
 */

namespace common\modules\dynamicField\models\populators;

use common\components\AbstractPopulator;
use common\models\DynamicField;
use common\modules\dynamicField\models\json\DynamicFieldTypeManager;
use common\modules\dynamicField\services\DynamicFieldFilterService;
use yii\base\BaseObject;

class DynamicFieldPopulator extends AbstractPopulator
{
    public function populateAdminForm(DynamicField $dynamicField, $data)
    {
        $formName = $dynamicField->formName();
        if (array_key_exists($formName, $data)) {
            $dfForm = $data[$formName];
            if ($dfForm) {
                $this->loadAttributes($dynamicField, $dfForm);
                $this->loadTypeParams($dynamicField, $dfForm);
                $this->fixFilterClass($dynamicField);
            }
        }
    }

    /**
     * @param DynamicField $dynamicField
     * @param $data
     */
    public function loadAttributes(DynamicField $dynamicField, $data)
    {
        $this->populateAttributes($dynamicField, $data, ['code', 'binded_model', 'title', 'type', 'default_value', 'description', 'is_required', 'is_active', 'filter_class']);
    }

    /**
     * @param DynamicField $dynamicField
     * @param $data
     * @throws \yii\base\InvalidConfigException
     */
    public function loadTypeParams(DynamicField $dynamicField, $data)
    {
        if ($dynamicField->type === DynamicField::TYPE_BOOLEAN) {
            $typeParams = null;
        } else {
            $typeParams = json_decode($data['type_params'] ?? '{}', true);
        }
        $dynamicField->type_params = $typeParams;
        /*  May be used with custom populators
        $classes = DynamicFieldTypeManager::getDynamicFieldTypeClasses();
        if (array_key_exists($dynamicField->type, $classes)) {
            $class = $classes[$dynamicField->type];
            $typeObj = \Yii::createObject($class);
            $typeParams = json_decode($data['type_params'] ?? '{}', true);
            $typeObj->populateValue($dynamicField, $typeParams);
        }
        */
    }

    /**
     * @param DynamicField $dynamicField
     * @throws \yii\base\InvalidConfigException
     */
    public function fixFilterClass(DynamicField $dynamicField)
    {
        if ($dynamicField->type) {
            $dynamicFieldFilterService = \Yii::createObject(DynamicFieldFilterService::class);
            $allowedDfFilters = $dynamicFieldFilterService->getDfFilters();
            if (!in_array($dynamicField->filter_class, $allowedDfFilters[$dynamicField->type])) {
                $dynamicField->filter_class = reset($allowedDfFilters[$dynamicField->type]);
            }
        }
    }
}