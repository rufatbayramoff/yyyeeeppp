<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.06.18
 * Time: 11:57
 */
namespace common\modules\dynamicField\models\populators;

use common\modules\dynamicField\models\DynamicFieldValue;

class DynamicFieldValuePopulator
{
    /**
     * @param DynamicFieldValue[] $dynamicFieldValues
     * @param $data
     */
    public function populate($dynamicFieldValues, $data)
    {
        foreach ($dynamicFieldValues as $dynamicFieldValue) {
            if (array_key_exists($dynamicFieldValue->dynamicField->code, $data)) {
                $dynamicFieldValue->value = $data[$dynamicFieldValue->dynamicField->code];
            }
        }
    }
}