<?php
/**
 * User: nabi
 */

namespace common\modules\dynamicField\models;

use common\components\exceptions\InvalidModelException;
use common\models\DynamicField;
use common\modules\dynamicField\models\json\DynamicFieldTypeManager;
use yii\base\Model;


/**
 * @property integer $isCertified
 * @property string $palletColor
 * @property string $packMaterial
 * @property boolean $palletHolders
 * @property string $tubeMaterial
 *
 * @package common\modules\dynamicField\models
 */
class DynamicFieldValue extends Model
{

    /**
     * @var DynamicField
     */
    public $dynamicField;

    public $value;

    public function rules()
    {
        return [
            [['value'], 'validateValue', 'skipOnEmpty' => false]
        ];
    }

    /**
     * @return bool|void
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function validateValue()
    {
        $typeObj = DynamicFieldTypeManager::getDynamicFieldTypeObj($this->dynamicField->type);
        if (!$typeObj) {
            throw new InvalidModelException('Dynamic field has invalid type');
        }

        if ($this->dynamicField->getIsRequared() && !$this->value) {
            $this->addError('value', $this->dynamicField->title.' '._t('site.dynamicField', 'cannot be left blank'));
            return;
        }

        $error = $typeObj->validateValue($this->value, $this->dynamicField->getTypeParams());
        if ($error) {
            $this->addError('value', $error);
        }
    }

    public function getValueErrors()
    {
        return $this->getErrors('value');
    }
}