<?php
/**
 * User: nabi
 */

namespace common\modules\dynamicField\models;


class DynamicFieldFacets
{

    public $facetsValues = [];

    public function getValueByFieldCodeKey($fieldCode)
    {
        foreach ($this->facetsValues as $k => $facetsValue) {
            if ($k == $fieldCode) {
                return $facetsValue;
            }
        }
    }

    public function add($fieldKey, $values)
    {
        $this->facetsValues[$fieldKey] = $values;
    }
}