<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.06.18
 * Time: 16:48
 */

namespace common\modules\dynamicField\models\json;

use backend\models\site\siteSettingsTemplates\JsonSchemeInterface;
use common\models\DynamicField;

class DynamicFieldTypeManager
{
    public static function getDynamicFieldTypeClasses()
    {
        return [
            DynamicField::TYPE_ENUM   => DynamicFieldTypeEnum::class,
            DynamicField::TYPE_NUMBER => DynamicFieldTypeNumber::class,
            DynamicField::TYPE_STRING => DynamicFieldTypeString::class,
            DynamicField::TYPE_BOOLEAN => DynamicFieldTypeBoolean::class,
        ];
    }

    /**
     * @param $type
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function getDynamicFieldTypeObj($type)
    {
        $classes = DynamicFieldTypeManager::getDynamicFieldTypeClasses();
        if (array_key_exists($type, $classes)) {
            $class = $classes[$type];
            $typeObj = \Yii::createObject($class);
            return $typeObj;
        }
        return null;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getSchemes()
    {
        $classes = self::getDynamicFieldTypeClasses();
        $returnValue = [];
        foreach ($classes as $type => $dfTypeClass) {
            /** @var JsonSchemeInterface $dfTypeObject */
            $dfTypeObject = \Yii::createObject($dfTypeClass);
            $returnValue[$type] = $dfTypeObject->getScheme();
        }
        return $returnValue;
    }
}