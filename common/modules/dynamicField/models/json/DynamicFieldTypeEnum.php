<?php
/**
 * User: nabi
 */

namespace common\modules\dynamicField\models\json;

use backend\models\site\siteSettingsTemplates\JsonSchemeInterface;
use common\components\ArrayHelper;
use common\models\DynamicField;

class DynamicFieldTypeEnum implements JsonSchemeInterface
{
    /**
     * @return array
     */
    public function getScheme(): array
    {
        return [
            'title'      => 'Type params',
            'type'       => 'object',
            'properties' => [
                'values'     => [
                    'type'   => 'array',
                    'format' => 'table',
                    'items'  => [
                        'type'       => 'object',
                        'title'      => 'Enum element',
                        'properties' => [
                            'element' => [
                                'type' => 'string',
                            ],
                        ],

                    ],
                ],
                'multiValue' => [
                    'type' => 'boolean'
                ]
            ],

        ];
    }

    public static function clearValuesList(array $config)
    {
        $returnValue = [];
        if (empty($config['values'])) {
            return $returnValue;
        }
        foreach ($config['values'] as $value) {
            $returnValue[$value['element']] = $value['element'];
        };
        return $returnValue;
    }

    /**
     * @param array $conf
     * @return string
     */
    public function validate(array $conf)
    {
        $conf = self::clearValuesList($conf);
        // Check for duplicates
        $duplicates = array_diff_key($conf, array_unique($conf));
        if ($duplicates) {
            return _t('site.product', 'Please remove duplicates from enum values: ' . implode(', ', $duplicates));
        }
    }

    /**
     * @param $value
     * @param $conf
     * @return string
     */
    public function validateValue($value, $conf)
    {
        $confValues = self::clearValuesList($conf);
        if ($conf['multiValue']) {
            $intersect = array_diff($value?:[], $confValues);
            if ($intersect) {
                return 'Value not in allowed list: ' . H(json_encode($intersect));
            }
        } else {
            if ($value && !in_array($value, $confValues)) {
                return 'Value not in allowed list: ' . $value;
            }
        }
    }
}