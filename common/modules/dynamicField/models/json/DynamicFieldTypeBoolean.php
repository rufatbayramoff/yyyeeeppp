<?php
/**
 * User: nabi
 */

namespace common\modules\dynamicField\models\json;

use backend\models\site\siteSettingsTemplates\JsonSchemeInterface;
use common\components\ArrayHelper;
use common\models\DynamicField;

class DynamicFieldTypeBoolean implements JsonSchemeInterface
{
    /**
     * @return array
     */
    public function getScheme(): array
    {
        return [
        ];
    }

    /**
     * @param array $conf
     * @return string
     */
    public function validate(array $conf)
    {
    }

    /**
     * @param $value
     * @param $conf
     * @return string
     */
    public function validateValue($value, $conf)
    {
    }
}