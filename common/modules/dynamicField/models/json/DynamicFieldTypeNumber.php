<?php
/**
 * User: nabi
 */

namespace common\modules\dynamicField\models\json;

use backend\models\site\siteSettingsTemplates\JsonSchemeInterface;
use common\models\DynamicField;

/**
 * Class DynamicFieldTypeNumber
 * This is static class description for Dynamic Field type of NUMBER
 *
 * @package common\modules\dynamicField\models\json
 */
class DynamicFieldTypeNumber implements JsonSchemeInterface
{
    /**
     * @return array
     */
    public function getScheme(): array
    {
        return
            [
                'title'      => 'Type params',
                'type'       => 'object',
                'properties' => [
                    'decimalPrecision' => [
                        'type' => 'number',
                    ],
                    'min'        => [
                        'type' => ['null', 'number'],
                    ],
                    'max'        => [
                        'type' => ['null', 'number'],
                    ]
                ]
            ];
    }

    public function validate(array $conf)
    {
        if ($conf['decimalPrecision'] < 0) {
            return _t('site.product', 'Decimal format is required.');
        }
        // Check for duplicates
        if ($conf['min'] > $conf['max']) {
            return _t('site.product', 'Max. must be greater than or equal to min.');
        }
    }

    public function validateValue($value, $conf)
    {
        $valueRound = round($value, $conf['decimalPrecision']);
        if ($valueRound != $value) {
            return _t('site.product', 'Please enter rounded value: {value}', ['value'=> $valueRound]);
        }
        if (($conf['max'] !== null) && ($value > $conf['max'])) {
            return _t('site.product', 'Please enter less than or equal to {max}', ['max'=>$conf['max']]);
        }
        if (($conf['min'] !== null) && ($value < $conf['min'])) {
            return _t('site.product', 'Please enter greater or equal to {min} ', ['min' => $conf['min']]);
        }
    }
}