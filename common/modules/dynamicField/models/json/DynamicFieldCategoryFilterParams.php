<?php
/**
 * User: nabi
 */

namespace common\modules\dynamicField\models\json;


use backend\models\site\siteSettingsTemplates\JsonSchemeInterface;
use common\models\DynamicFieldCategory;

class DynamicFieldCategoryFilterParams implements JsonSchemeInterface
{

    public $model;

    /**
     * @param DynamicFieldCategory $dfCategory
     * @return DynamicFieldCategoryFilterParams
     */
    public static function init(DynamicFieldCategory $dfCategory) : DynamicFieldCategoryFilterParams
    {
        $obj = new DynamicFieldCategoryFilterParams();
        $obj->model = $dfCategory;
        return $obj;
    }

    /**
     * @return array
     */
    public function getScheme(): array
    {
        return [];
    }
}