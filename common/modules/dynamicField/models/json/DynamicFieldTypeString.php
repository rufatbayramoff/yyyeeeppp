<?php
/**
 * User: nabi
 */

namespace common\modules\dynamicField\models\json;

use backend\models\site\siteSettingsTemplates\JsonSchemeInterface;
use common\models\DynamicField;
use yii\base\ErrorException;

class DynamicFieldTypeString implements JsonSchemeInterface
{
    /**
     * @return array
     */
    public function getScheme(): array
    {
        return [
            'title'      => 'Type params',
            'type'       => 'object',
            'properties' => [
                'maxLength'         => [
                    'type' => 'integer',
                ],
                'regularExpression' => [
                    'type' => 'string',
                ],
                'multiValue'        => [
                    'type' => 'boolean'
                ]
            ]
        ];
    }

    /**
     * @param $value
     * @param $regularExpression
     * @return string|bool
     */
    protected function validateRegular($value, $regularExpression)
    {
        $rgx = '/^' . $regularExpression . '$/';
        $rows = explode("\n", $value);
        foreach ($rows as $val) {
            if (empty($val)) {
                continue;
            }
            $regRes = preg_match($rgx, trim($val), $matches);
            if (!$regRes) {
                return $val;
            }
        }
        return false;
    }

    public function validate(array $conf)
    {
        if ($conf['regularExpression']) {
            try {
                $this->validateRegular('Test subj', $conf['regularExpression']);
            } catch (ErrorException $e) {
                return _t('site.product', 'Regular expression invalid') . $conf['regularExpression'];
            }
        }
        if ($conf['maxLength'] < 0) {
            return _t('site.product', 'Max length should be more than zero');
        }
    }

    public function validateValue($value, $conf)
    {
        if ($conf['regularExpression']) {
            $matches = [];
            try {
                if ($val = $this->validateRegular($value, $conf['regularExpression'])) {
                    return _t('site.product', 'Value format invalid: ' . $val);
                }
            } catch (ErrorException $e) {
                return _t('site.product', 'Regular expression invalid');
            }
        }
        if ($conf['maxLength'] && (mb_strlen($value) > $conf['maxLength'])) {
            return _t('site.product', 'Length should be less than: ') . $conf['maxLength'];
        }
    }
}