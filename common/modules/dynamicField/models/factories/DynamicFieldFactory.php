<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.06.18
 * Time: 15:00
 */

namespace common\modules\dynamicField\models\factories;

use common\models\DynamicField;
use common\models\Product;

class DynamicFieldFactory extends \yii\base\BaseObject
{
    public function create()
    {
        $dynamicField = new DynamicField();
        $dynamicField->binded_model = Product::class;
        $dynamicField->type = DynamicField::TYPE_NUMBER;
        return $dynamicField;
    }
}