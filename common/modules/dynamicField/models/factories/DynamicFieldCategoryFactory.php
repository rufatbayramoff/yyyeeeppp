<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.06.18
 * Time: 14:34
 */
namespace common\modules\dynamicField\models\factories;

use common\models\DynamicField;
use common\models\DynamicFieldCategory;

class DynamicFieldCategoryFactory extends \yii\base\BaseObject
{
    public function create(DynamicField $dynamicField)
    {
        $dynamicFieldCategory = new DynamicFieldCategory();
        $dynamicFieldCategory->field_code = $dynamicField->code;
        return $dynamicFieldCategory;
    }
}