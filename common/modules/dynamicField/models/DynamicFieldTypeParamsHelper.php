<?php
/**
 * User: nabi
 */

namespace common\modules\dynamicField\models;

use backend\models\site\siteSettingsTemplates\JsonSchemeInterface;
use common\modules\dynamicField\models\json\DynamicFieldTypeNumber;
use Yii;
class DynamicFieldTypeParamsHelper
{

    public static function getSchemaByType($type)
    {
        $camelCaseKey = str_replace('_', '', ucwords($type, '_'));
        $className = 'common\modules\dynamicField\models\json\DynamicFieldType' . $camelCaseKey;
        $templateObj = null;
        try {
            /**
             * @var $templateObj JsonSchemeInterface
             */
            $templateObj = Yii::createObject($className);
        } catch (\Exception $e) {
        }
        if ($templateObj) {
            return $templateObj->getScheme();
        } else {
            return [];
        }
    }
}