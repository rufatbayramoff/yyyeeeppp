<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.05.18
 * Time: 13:56
 */

namespace common\modules\dynamicField\models\serializer;

use common\components\serizaliators\AbstractProperties;
use common\models\DynamicField;

class DynamicFieldSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            DynamicField::class => [
                'id',
                'code',
                'bindedModel'  => 'binded_model',
                'title',
                'type',
                'typeParams'   => 'type_params',
                'defaultValue' => 'default_value',
                'description',
                'isActive'     => 'is_active',
            ]
        ];
    }
}