<?php

namespace common\modules\dynamicField\widgets;

use common\components\ArrayHelper;
use common\models\DynamicField;
use common\modules\dynamicField\models\DynamicFieldFacets;
use common\modules\dynamicField\services\DynamicFieldService;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;

class DynamicFieldsFilterWidget extends \yii\base\Widget
{
    public $model = null;
    public $classModel = null;
    /**
     * @var ActiveForm
     */
    public $form;
    /**
     * @var DynamicFieldService
     */
    private $dynamicFieldService;

    /**
     * @var DynamicFieldFacets
     */
    public $dynamicFieldFacets;

    public function injectDependencies(DynamicFieldService $dynamicFieldService)
    {
        $this->dynamicFieldService = $dynamicFieldService;
    }

    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        if (empty($this->classModel)) {
            $this->classModel = get_class($this->model);
        }
        $form = $this->form; //ActiveForm::begin(['layout' => 'default']);
        $dynamicFields = $this->dynamicFieldService->getModelFieldsWithFilter($this->classModel);
        foreach ($dynamicFields as $field) {
            $typeParams = json_decode($field->type_params);
            switch ($field->type) {
                case DynamicField::TYPE_BOOLEAN:
                    echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']', ['template' => '{input} {label}'])->checkbox([], false)->label($field->title);
                    break;
                case DynamicField::TYPE_ENUM:
                    $enums = array_combine($typeParams, $typeParams);
                    $enums = array_map('trim', $enums);
                    echo '<h3>'.$field->title.'</h3>';

                    foreach($enums as $k=>$enum){
                        $enumLabel = $enum;
                        if($this->dynamicFieldFacets){
                            $qty = $this->dynamicFieldFacets->getValueByFieldCodeKey('df_'.$field->code . $enum);
                            $enumLabel = $enum . ($qty ? " ($qty)" : '');
                        }
                        echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']['.$enum.']', ['template' => '{input} {label}'])->checkbox([], false)->label($enumLabel);
                    }
                    echo '<hr />';
                    break;
                case DynamicField::TYPE_STRING:
                    if (!empty($typeParams->is_multiline)) {
                        echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->textarea()->label($field->title);
                    } else {
                        echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->textInput()->label($field->title);
                    }
                    break;
                case DynamicField::TYPE_NUMBER:
                    echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->input('number')->label($field->title);
                    break;
                default:
                    echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->input('text')->label($field->title);
            }
        }
        $content = ob_get_clean();
        return $content;
    }

}
