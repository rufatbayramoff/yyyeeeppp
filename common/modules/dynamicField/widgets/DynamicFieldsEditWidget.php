<?php

namespace common\modules\dynamicField\widgets;

use common\components\ArrayHelper;
use common\models\DynamicField;
use common\modules\dynamicField\services\DynamicFieldService;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;

class DynamicFieldsEditWidget extends \yii\base\Widget
{
    public $model = null;

    /**
     * @var ActiveForm
     */
    public $form;
    /**
     * @var DynamicFieldService
     */
    private $dynamicFieldService;

    public function injectDependencies(DynamicFieldService $dynamicFieldService)
    {
        $this->dynamicFieldService = $dynamicFieldService;
    }

    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        $form = $this->form; //ActiveForm::begin(['layout' => 'default']);
        $dynamicFields = $this->dynamicFieldService->getModelFields(get_class($this->model));
        $dynamicFieldsValues = Json::decode($this->model->dynamic_fields_values);
        $this->model->dynamic_fields_values =$dynamicFieldsValues;
        foreach ($dynamicFields as $field) {
            $typeParams = $field->type_params;
            switch($field->type){
                case DynamicField::TYPE_BOOLEAN:
                    echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->checkbox()->label($field->title);
                break;
                case DynamicField::TYPE_ENUM:
                    $enums = array_combine($typeParams, $typeParams);
                    echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->dropDownList($enums)->label($field->title);
                break;
                case DynamicField::TYPE_STRING:
                    if(!empty($typeParams->is_multiline)){
                        echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->textarea()->label($field->title);
                    }else{
                        echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->textInput()->label($field->title);
                    }
                    break;
                case DynamicField::TYPE_NUMBER:
                    echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->input('number')->label($field->title);
                    break;
                default:
                    echo $form->field($this->model, 'dynamic_fields_values[' . $field->code . ']')->input('text')->label($field->title);
            }
        }
        echo \yii\bootstrap\Html::submitButton('Save');
        $content = ob_get_clean();
        return $content;
    }

}
