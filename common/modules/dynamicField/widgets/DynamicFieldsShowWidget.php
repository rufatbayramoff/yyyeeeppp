<?php

namespace common\modules\dynamicField\widgets;

use common\components\ArrayHelper;
use common\models\DynamicField;
use common\modules\dynamicField\services\DynamicFieldService;
use yii\base\DynamicModel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;
use yii\widgets\DetailView;

class DynamicFieldsShowWidget extends \yii\base\Widget
{
    public $model = null;

    /**
     * @var ActiveForm
     */
    public $form;
    /**
     * @var DynamicFieldService
     */
    private $dynamicFieldService;

    public function injectDependencies(DynamicFieldService $dynamicFieldService)
    {
        $this->dynamicFieldService = $dynamicFieldService;
    }

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $form = $this->form; //ActiveForm::begin(['layout' => 'default']);
        $dynamicFields = $this->dynamicFieldService->getModelFields(get_class($this->model));
        $dynamicFieldsValues = Json::decode($this->model->dynamic_fields_values);
        $fieldKeys = array_keys($dynamicFieldsValues);
        $this->model->dynamic_fields_values = $dynamicFieldsValues;
        $resultTable = [];
        #dd($dynamicFieldsValues);

        foreach ($dynamicFields as $field) {
            $typeParams = json_decode($field->type_params);
            if (!array_key_exists($field->code, $dynamicFieldsValues)) {
                continue;
            }
            if ($dynamicFieldsValues[$field->code] === '') {
                continue;
            }
            $resultTable[$field->code] = $dynamicFieldsValues[$field->code];
        }
        $dynamicModel = new DynamicModel(array_keys($resultTable));
        $dynamicModelAttributes = [];
        foreach ($dynamicFields as $field) {
            if (!in_array($field->code, array_keys($resultTable))) {
                continue;
            }
            if (in_array($field->type, ['integer', 'boolean'])) {
                $dynamicModelAttributes[] = $field->code . ':' . $field->type;
            } else {
                $dynamicModelAttributes[] = $field->code;
            }
        }
        $dynamicModel->setAttributes($resultTable, false);
        $content = DetailView::widget(
            [
                'model'      => $dynamicModel,
                'attributes' => $dynamicModelAttributes
            ]
        );
        return $content;
    }

}
