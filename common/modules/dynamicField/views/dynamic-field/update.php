<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dynamicField common\models\DynamicField */

$this->title = 'Update Dynamic Field: ' . $dynamicField->title;
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $dynamicField->title, 'url' => ['view', 'id' => $dynamicField->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dynamic-field-update">

    <?= $this->render('_form', [
        'dynamicField' => $dynamicField,
    ]) ?>

</div>
