<?php

use backend\assets\AngularAppAsset;
use common\modules\dynamicField\models\json\DynamicFieldTypeEnum;
use common\modules\dynamicField\models\json\DynamicFieldTypeManager;
use common\modules\dynamicField\models\serializer\DynamicFieldSerializer;
use frontend\assets\JsonEditorAssetJsTemplate;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $dynamicField common\models\DynamicField */
/* @var $form yii\widgets\ActiveForm */

$dynamicFieldInfo = DynamicFieldSerializer::serialize($dynamicField);

$dynamicFieldSchema = DynamicFieldTypeManager::getSchemes();
$dynamicFieldFilterService = Yii::createObject(\common\modules\dynamicField\services\DynamicFieldFilterService::class);

$dynamicField->is_active = $dynamicField->isNewRecord ? 1 : $dynamicField->is_active;

AngularAppAsset::register($this);
JsonEditorAssetJsTemplate::register($this);

Yii::$app->angular
    ->service(['router', 'user', 'camelizer', 'jsonEditorHelper'])
    ->controller(
        [
            'dynamicField/dynamicField'
        ])
    ->controllerBackend('product/dynamicField/dynamic-field-form-controller')
    ->controllerParam('dynamicField', $dynamicFieldInfo)
    ->controllerParam('dynamicFieldSchema', $dynamicFieldSchema)
    ->registerScripts($this);
?>
<div ng-app="app">
    <div class="dynamic-field-form" ng-controller="DynamicFieldFormController">
        <div class="row">
            <div class="col-lg-6">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($dynamicField, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($dynamicField, 'type')->dropDownList($dynamicField->getTypeList(),
                    [
                        'ng-model'  => "dynamicField.type",
                        'ng-change' => 'onChangeType()'
                    ]

                ) ?>
                <div id="typeParamsJsonContainer" class="form-group typeParamsJsonContainer">
                </div>
                <input
                        id='typeParamsJson'
                        name="DynamicField[type_params]"
                        ng-model="dynamicField.typeParams"
                        type="text"
                        class="form-control hidden"
                        autocomplete="off"
                        value=''
                >
                <div class="form-group <?= $dynamicField->hasErrors('type_params') ? 'has-error' : '' ?>">
                    <p class="has-error help-block help-block-error">
                        <?php
                        $errors = $dynamicField->getErrors('type_params');
                        echo implode(" <br>\n", $errors);
                        ?>
                    </p>
                </div>
                <?= $form->field($dynamicField, 'is_required')->checkbox() ?>
                <?= $form->field($dynamicField, 'default_value')->textInput(['maxlength' => true]) ?>
                <?= $form->field($dynamicField, 'description')->textarea() ?>
                <?= $form->field($dynamicField, 'code')->textInput(['maxlength' => true, 'readonly' => !$dynamicField->isNewRecord]) ?>
                <?= $dynamicField->isNewRecord ? '' :
                    $form->field($dynamicField, 'filter_class')->dropDownList($dynamicFieldFilterService->getFilterNamesListForField($dynamicField)) ?>
                <?= $form->field($dynamicField, 'is_active')->checkbox() ?>
                <?php /* $form->field($dynamicField, 'filter_')*/ ?>
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?= Html::submitButton($dynamicField->isNewRecord ? 'Create' : 'Update',
                            ['class' => $dynamicField->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-lg-6">
                <?php if (!$dynamicField->isNewRecord): ?>
                    <?= \yii\grid\GridView::widget([
                        'dataProvider' => \common\models\DynamicFieldCategory::getDataProvider(['field_code' => $dynamicField->code]),
                        'columns'      => [
                            [
                                'attribute'      => 'id',
                                'value'          => 'id',
                                'contentOptions' => ['style' => 'width: 80px;']
                            ],
                            [
                                'attribute' => 'category_id',
                                'label'     => 'Category',
                                'value'     => 'category.title',
                                'filter'    => Html::activeDropDownList(
                                    new \backend\models\search\DynamicFieldCategorySearch(),
                                    'category_id',
                                    \common\models\ProductCategory::getActiveSelectCategories(),
                                    ['class' => 'form-control', 'prompt' => 'All']),
                            ],
                            'is_active:boolean',
                            'has_filter:boolean',
                            'is_required:boolean',
                            [
                                'attribute' => 'type_params',
                                'label'     => 'Type params',
                                'value'     => function ($model) {
                                    return json_encode($model->type_params);
                                }

                            ],
                            'default_value',
                            'position',
                            [
                                'class'          => ActionColumn::class,
                                'controller'     => 'dynamic-field-category',
                                'buttons'        => [
                                    'update' => function ($url, $topModel, $key) {
                                        $editButton = Html::button('Edit',
                                            [
                                                'title'       => 'Update category',
                                                'class'       => 'btn btn-primary btn-ajax-modal',
                                                'data-pjax'   => 0,
                                                'value'       => $url,
                                                'data-target' => '#edit_df_category'
                                            ]
                                        );
                                        return $editButton;
                                        //return '<a href="'.$url.'" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>';
                                    }
                                ],
                                'template'       => '{update} &nbsp; {delete} ',
                                'contentOptions' => ['style' => 'width: 150px;']
                            ]
                        ],
                    ]); ?>

                    <hr/>


                    <div class="dynamic-field-category-form">
                        <h3>Add Field To Category</h3>
                        <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'action' => ['dynamic-field-category/create']]); ?>

                        <?php
                        $dfCategory = new \common\models\DynamicFieldCategory();
                        $dfCategory->field_code = $dynamicField->code;
                        $dfCategory->position = 1;
                        $dfCategory->is_active = 1;
                        ?>
                        <?php echo $form->field($dfCategory, 'category_id')->dropDownList(\common\models\ProductCategory::getActiveSelectCategories()); ?>


                        <?= $form->field($dfCategory, 'field_code')->hiddenInput()->label(false) ?>
                        <?= $form->field($dfCategory, 'is_active')->checkbox() ?>
                        <?= $form->field($dfCategory, 'has_filter')->checkbox() ?>
                        <input type="hidden" name="redirect_refferer" value="1"/>

                        <div class="text-center">
                            <?= Html::submitButton('Add to category', ['class' => $dynamicField->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>


                <?php endif; ?>
            </div>
        </div>
    </div>
</div>