<?php

use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DynamicFieldSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dynamic Fields';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dynamic-field-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dynamic Field', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
//        'caption'      => \backend\components\GridAttributeHelper::getExportImportCaption($dataProvider, $searchModel),
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            'code',
            'title',
            'type',
            [
                'label'     => 'Type params',
                'attribute' => 'type_params',
                'value'     => function (\common\models\DynamicField $dynamicField) {
                    return json_encode($dynamicField->type_params);
                }
            ],
            'default_value',
            'is_active:boolean',
            'binded_model',

            [
                'class'    => ActionColumn::class,
                'template' => '{update} &nbsp; {delete} ',
                'contentOptions' => ['style' => 'width: 70px;']
            ]

        ],
    ]); ?>
</div>
