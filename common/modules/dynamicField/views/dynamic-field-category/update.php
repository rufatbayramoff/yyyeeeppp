<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dynamicFieldCategory common\models\DynamicFieldCategory */

$this->title = 'Update Dynamic Field Category: ' . $dynamicFieldCategory->id;
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Field Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $dynamicFieldCategory->id, 'url' => ['view', 'id' => $dynamicFieldCategory->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dynamic-field-category-update">

    <?= $this->render('_form', [
        'dynamicFieldCategory' => $dynamicFieldCategory,
        'referrer'             => $referrer
    ]) ?>

</div>
