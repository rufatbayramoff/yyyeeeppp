<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $dynamicFieldCategory common\models\DynamicFieldCategory */

$this->title = $dynamicFieldCategory->id;
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Field Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dynamic-field-category-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $dynamicFieldCategory->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $dynamicFieldCategory->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $dynamicFieldCategory,
        'attributes' => [
            'id',
            'field_code',
            'category_id',
            'is_required',
        ],
    ]) ?>

</div>
