<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DynamicFieldCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dynamic Field Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dynamic-field-category-index">
    <?= GridView::widget([
        //        'caption' => \backend\components\GridAttributeHelper::getExportImportCaption($dataProvider, $searchModel),
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute'      => 'id',
                'value'          => 'id',
                'contentOptions' => ['style' => 'width: 80px;']
            ],
            'field_code',
            [
                'attribute' => 'category_id',
                'label'     => 'Category',
                'value'     => 'category.title',
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'category_id',
                    \common\models\ProductCategory::getActiveSelectCategories(),
                    ['class' => 'form-control', 'prompt' => 'All']),
            ],
            'is_active:boolean',
            'has_filter:boolean',
            'is_required:boolean',
            [
                'attribute' => 'type_params',
                'label'     => 'Type params',
                'value'     => function ($model) {
                    return json_encode($model);
                }
            ],
            'default_value',
            'position',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
