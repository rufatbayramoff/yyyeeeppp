<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DynamicFieldCategory */

$this->title = 'Create Dynamic Field Category';
$this->params['breadcrumbs'][] = ['label' => 'Dynamic Field Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dynamic-field-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
