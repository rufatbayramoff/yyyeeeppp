<?php

use common\modules\dynamicField\models\json\DynamicFieldTypeManager;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $dynamicFieldCategory common\models\DynamicFieldCategory */
/* @var $form yii\widgets\ActiveForm */

$dynamicFieldCategory->is_active = $dynamicFieldCategory->isNewRecord ? 1 : $dynamicFieldCategory->is_active;
$typeObj = DynamicFieldTypeManager::getDynamicFieldTypeObj($dynamicFieldCategory->dynamicField->type);
$jsonSchema = [];
if ($typeObj) {
    $jsonSchema = $typeObj->getScheme();
}
?>

<div class="dynamic-field-category-form" style="max-width: 1200px;margin: 0 auto;padding:20px">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
    <div class="form-group">
        <?= DetailView::widget([
            'model'      => $dynamicFieldCategory,
            'attributes' => [
                'id',
                [
                    'label' => 'Field',
                    'value' => $dynamicFieldCategory->dynamicField->title . ' (Code: ' . $dynamicFieldCategory->dynamicField->code . ')'
                ]
            ]
        ]) ?>
    </div>
    <?= $form->field($dynamicFieldCategory, 'default_value')->textInput(['maxlength' => true]) ?>
    <?= $form->field($dynamicFieldCategory, 'position')->input('number') ?>
    <?= $form->field($dynamicFieldCategory, 'category_id')->dropDownList(\common\models\ProductCategory::getActiveSelectCategories(true)); ?>
    <?= $form->field($dynamicFieldCategory, 'is_required')->dropDownList([null => 'default', 1 => 'Yes', 0 => 'No']); ?>
    <?= $form->field($dynamicFieldCategory, 'is_active')->checkbox() ?>
    <?= $form->field($dynamicFieldCategory, 'has_filter')->checkbox() ?>

    <?= $form->field($dynamicFieldCategory, 'hasTypeParams')->checkbox([
        'onclick' => 'changeHasTypeParams();'
    ]) ?>
    <div id='category_type_params'>
        <?php
        $dynamicFieldCategory->typeParamsString = json_encode($dynamicFieldCategory->type_params);
        echo $form->field($dynamicFieldCategory, 'typeParamsString', ['options' => ['class' => 'form-group jsonEditorField']])->widget(
            \beowulfenator\JsonEditor\JsonEditorWidget::className(),
            [
                'schema'          => $jsonSchema,
                'enableSelectize' => true,
                'clientOptions'   => [
                    'theme'                         => 'bootstrap3',
                    'iconlib'                       => 'bootstrap3',
                    'disable_collapse'              => true,
                    'disable_edit_json'             => true,
                    'disable_properties'            => true,
                    'disable_array_delete_all_rows' => true,
                    'disable_array_delete_last_row' => true,
                    'no_additional_properties'      => true,
                ]
            ]
        )->label('') ?>
    </div>
    <input type="hidden" name="referrer" value="<?= H($referrer) ?>">

    <div class="text-center">
        <?= Html::submitButton($dynamicFieldCategory->isNewRecord ? 'Create' : 'Update',
            ['class' => $dynamicFieldCategory->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<script>
    function changeHasTypeParams() {
        if ($("#dynamicfieldcategory-hastypeparams").prop("checked")) {
            $("#category_type_params").removeClass("hidden");
        }
        else {
            $("#category_type_params").addClass("hidden");
        }
    }
    changeHasTypeParams();
</script>
