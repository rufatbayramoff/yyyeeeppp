<?php

namespace common\modules\dynamicField\controllers\backend;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\DynamicFieldCategorySearch;
use common\models\DynamicField;
use common\models\DynamicFieldCategory;
use common\modules\dynamicField\models\factories\DynamicFieldCategoryFactory;
use common\modules\dynamicField\models\populators\DynamicFieldCategoryPopulator;
use common\modules\dynamicField\models\repositories\DynamicFieldCategoryRepository;
use common\modules\dynamicField\services\DynamicFieldService;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class DynamicFieldCategoryController extends AdminController
{

    /** @var DynamicFieldCategoryRepository */
    public $dynamicFieldCategoryRepository;

    /** @var DynamicFieldCategoryPopulator */
    public $dynamicFieldCategoryPopulator;

    /** @var DynamicFieldService */
    public $dynamicFieldService;

    /** @var DynamicFieldCategoryFactory */
    public $dynamicFieldCategoryFactory;

    public function injectDependencies(
        DynamicFieldCategoryRepository $dynamicFieldCategoryRepository,
        DynamicFieldCategoryPopulator $dynamicFieldCategoryPopulator,
        DynamicFieldService $dynamicFieldService,
        DynamicFieldCategoryFactory $dynamicFieldCategoryFactory
    ) {
        $this->dynamicFieldCategoryRepository = $dynamicFieldCategoryRepository;
        $this->dynamicFieldCategoryPopulator = $dynamicFieldCategoryPopulator;
        $this->dynamicFieldService = $dynamicFieldService;
        $this->dynamicFieldCategoryFactory = $dynamicFieldCategoryFactory;
    }


    /**
     * @param $action
     * @return bool
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('dynamic_field.view');
        return parent::beforeAction($action);
    }

    /**
     * @param DynamicFieldCategory $dynamicFieldCategory
     * @param array $post
     * @return bool
     * @throws \yii\base\Exception
     */
    public function saveDynamicFieldCategory(DynamicFieldCategory $dynamicFieldCategory, array $post)
    {
        $this->dynamicFieldCategoryPopulator->populate($dynamicFieldCategory, $post);

        if ($dynamicFieldCategory->validate()) {
            $this->dynamicFieldCategoryRepository->saveDynamicFieldCategory($dynamicFieldCategory);
            return true;
        }
        return false;
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'create' => ['POST'],
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        $searchModel = new DynamicFieldCategorySearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionView($id)
    {
        $dynamicFieldCategory = DynamicFieldCategory::tryFindByPk($id);
        return $this->render(
            'view',
            [
                'dynamicFieldCategory' => $dynamicFieldCategory
            ]
        );
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $post = \Yii::$app->request->post();
        $formData = $post['DynamicFieldCategory'];
        $dynamicField = DynamicField::tryFind(['code' => $formData['field_code']]);
        $dynamicFieldCategory = $this->dynamicFieldCategoryFactory->create($dynamicField);
        $this->dynamicFieldCategoryPopulator->populate($dynamicFieldCategory, $post);
        $this->dynamicFieldCategoryRepository->saveDynamicFieldCategory($dynamicFieldCategory);
        return $this->redirect(app('request')->referrer);
    }

    /**
     * @param string $id
     * @return mixed
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $dynamicFieldCategory = DynamicFieldCategory::tryFindByPk($id);

        if (\Yii::$app->request->isPost) {
            if ($this->saveDynamicFieldCategory($dynamicFieldCategory, \Yii::$app->request->post())) {
                \Yii::$app->getSession()->setFlash('success', 'Dynamic category field saved', false);
                return $this->redirect(\Yii::$app->request->post('referrer'));
            }
        }
        return $this->renderAdaptive(
            'update',
            [
                'dynamicFieldCategory' => $dynamicFieldCategory,
                'referrer'             => app('request')->referrer
            ]
        );
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $dynamicFieldCategory = DynamicFieldCategory::tryFindByPk($id);
        $this->dynamicFieldCategoryRepository->deleteDynamicField($dynamicFieldCategory);
        return $this->redirect(app('request')->referrer);
    }
}
