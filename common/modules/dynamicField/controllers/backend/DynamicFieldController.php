<?php

namespace common\modules\dynamicField\controllers\backend;

use backend\components\AdminAccess;
use backend\components\AdminController;
use backend\models\search\DynamicFieldSearch;
use common\models\DynamicField;
use common\modules\dynamicField\models\factories\DynamicFieldFactory;
use common\modules\dynamicField\models\populators\DynamicFieldPopulator;
use common\modules\dynamicField\models\repositories\DynamicFieldRepository;
use common\modules\dynamicField\services\DynamicFieldService;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class DynamicFieldController extends AdminController
{

    /** @var DynamicFieldRepository */
    public $dynamicFieldRepository;

    /** @var DynamicFieldPopulator */
    public $dynamicFieldPopulator;

    /** @var DynamicFieldService */
    public $dynamicFieldService;

    /** @var DynamicFieldFactory */
    public $dynamicFieldFactory;

    public function injectDependencies(
        DynamicFieldRepository $dynamicFieldRepository,
        DynamicFieldPopulator $dynamicFieldPopulator,
        DynamicFieldService $dynamicFieldService,
        DynamicFieldFactory $dynamicFieldFactory
    ) {
        $this->dynamicFieldRepository = $dynamicFieldRepository;
        $this->dynamicFieldPopulator = $dynamicFieldPopulator;
        $this->dynamicFieldService = $dynamicFieldService;
        $this->dynamicFieldFactory = $dynamicFieldFactory;
    }


    /**
     * @param $action
     * @return bool
     * @throws \yii\base\UserException
     */
    public function beforeAction($action)
    {
        AdminAccess::validateAccess('dynamic_field.view');
        return parent::beforeAction($action);
    }

    /**
     * @param DynamicField $dynamicField
     * @param array $post
     */
    public function saveDynamicField(DynamicField $dynamicField, array $post)
    {
        $this->dynamicFieldPopulator->populateAdminForm($dynamicField, $post);

        if ($dynamicField->validate()) {
            $this->dynamicFieldRepository->saveDynamicField($dynamicField);
            return true;
        }
        return false;
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     *
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        $searchModel = new DynamicFieldSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $dynamicField = $this->dynamicFieldFactory->create();
        if (Yii::$app->request->isPost) {
            if ($this->saveDynamicField($dynamicField, Yii::$app->request->post())) {
                Yii::$app->getSession()->setFlash('success', 'Dynamic field created', false);
                return $this->redirect(['/product/fields/dynamic-field/update?id=' . $dynamicField->id]);
            }
        }
        return $this->render(
            'create',
            [
                'dynamicField' => $dynamicField,
            ]
        );

    }

    /**
     * @param string $id
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $dynamicField = DynamicField::tryFindByPk($id);

        if (Yii::$app->request->isPost) {
            if ($this->saveDynamicField($dynamicField, Yii::$app->request->post())) {
                Yii::$app->getSession()->setFlash('success', 'Dynamic field saved', false);
                return $this->redirect(['/product/fields/dynamic-field']);
            }
        }
        return $this->render(
            'update',
            [
                'dynamicField' => $dynamicField,
            ]
        );
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $dynamicField = DynamicField::tryFindByPk($id);
        $this->dynamicFieldRepository->deleteDynamicField($dynamicField);
        return $this->redirect(['/product/fields/dynamic-field']);
    }
}
