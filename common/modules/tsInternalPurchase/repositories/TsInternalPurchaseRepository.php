<?php

namespace common\modules\tsInternalPurchase\repositories;

use common\models\TsInternalPurchase;
use common\models\CompanyService;
use yii\base\BaseObject;

class TsInternalPurchaseRepository extends BaseObject
{
    public function save(TsInternalPurchase $tsInternalPurchase)
    {
        $tsInternalPurchase->safeSave();
        if ($tsInternalPurchase->tsInternalPurchaseCertification) {
            $tsInternalPurchase->tsInternalPurchaseCertification->safeSave();
        }
    }

    public function getLastPayedCertForService(CompanyService $companyService): ?TsInternalPurchase
    {
        $tsInternalPurchase = TsInternalPurchase::find()
            ->joinWith('tsInternalPurchaseCertification')
            ->andWhere(['ts_internal_purchase_certification.company_service_id' => $companyService->id])
            ->andWhere(['ts_internal_purchase.status' => TsInternalPurchase::STATUS_PAYED])
            ->andWhere(['ts_internal_purchase.type'=> TsInternalPurchase::TYPE_CERTIFICATION])
            ->orderBy('ts_internal_purchase.created_at desc')
            ->one();
        return $tsInternalPurchase;
    }
}