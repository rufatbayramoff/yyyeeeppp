<?php

namespace common\modules\tsInternalPurchase\factories;

use common\components\DateHelper;
use common\components\UuidHelper;
use common\models\CompanyService;
use common\models\TsInternalPurchase;
use common\models\TsInternalPurchaseCertification;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use Yii;
use yii\base\BaseObject;
use yii\db\Query;
use yii\web\IdentityInterface;

class TsInternalPurchaseFactory extends BaseObject
{
    /** @var UserIdentityProvider */
    public $userIdentityProvider;

    public function injectDependencies(
        UserIdentityProvider $userIdentityProvider
    )
    {
        $this->userIdentityProvider = $userIdentityProvider;
    }

    public function generateUuid()
    {
        do {
            $uid = strtoupper(UuidHelper::generateRandCode(6));
            $checkUuid = (new Query())->select('*')->from('ts_internal_purchase')->where(['uid' => $uid])->one();
        } while ($checkUuid);
        return $uid;
    }

    public function createCertificationForCompanyService(CompanyService $companyService): TsInternalPurchase
    {
        $tsInternalPurchase                                        = Yii::createObject(TsInternalPurchase::class);
        $tsInternalPurchase->uid                                   = $this->generateUuid();
        $tsInternalPurchase->created_at                            = DateHelper::now();
        $tsInternalPurchase->user_id                               = $this->userIdentityProvider->getUser()->id ?? null;
        $tsInternalPurchase->type                                  = TsInternalPurchase::TYPE_CERTIFICATION;
        $tsInternalPurchaseCertification                           = Yii::createObject(TsInternalPurchaseCertification::class);
        $tsInternalPurchaseCertification->ts_internal_purchase_uid = $tsInternalPurchase->uid;
        $tsInternalPurchaseCertification->company_service_id       = $companyService->id;
        $tsInternalPurchaseCertification->type                     = CompanyService::CERT_TYPE_VERIFIED;
        $tsInternalPurchaseCertification->expire_date              = DateHelper::addNow('P' . CompanyService::CERT_EXPIRE_PERIOD_DAYS . 'D');
        $tsInternalPurchaseCertification->ts_certification_class_id = $companyService->psPrinter->printer->ts_certification_class_id;
        $tsInternalPurchase->populateRelation('tsInternalPurchaseCertification', $tsInternalPurchaseCertification);
        return $tsInternalPurchase;
    }

    /**
     * @param IdentityInterface $user
     * @return TsInternalPurchase
     * @throws \yii\base\InvalidConfigException
     */
    public function makeDeposit(User $user): TsInternalPurchase
    {
        $tsInternalPurchase                                        = Yii::createObject(TsInternalPurchase::class);
        $tsInternalPurchase->uid                                   = $this->generateUuid();
        $tsInternalPurchase->created_at                            = DateHelper::now();
        $tsInternalPurchase->user_id                               = $user->id;
        $tsInternalPurchase->type                                  = TsInternalPurchase::TYPE_DEPOSIT;
        return $tsInternalPurchase;
    }


}