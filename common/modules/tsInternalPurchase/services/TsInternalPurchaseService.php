<?php

namespace common\modules\tsInternalPurchase\services;

use common\components\DateHelper;
use common\components\exceptions\ValidationException;
use common\models\CompanyService;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\TsInternalPurchase;
use common\models\TsInternalPurchaseCertification;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\factories\PaymentInvoiceFactory;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use common\modules\tsInternalPurchase\factories\TsInternalPurchaseFactory;
use common\modules\tsInternalPurchase\repositories\TsInternalPurchaseRepository;
use http\Exception\InvalidArgumentException;
use lib\money\Money;
use yii\base\BaseObject;

class TsInternalPurchaseService extends BaseObject
{

    /** @var TsInternalPurchaseFactory */
    public $tsInternalPurchaseFactory;

    /** @var TsInternalPurchaseRepository */
    public $tsInternalPurchaseRepository;

    /** @var PaymentInvoiceFactory */
    public $invoiceFactory;

    /** @var PaymentInvoiceRepository */
    public $paymentInvoiceRepository;

    /** @var PaymentAccountService */
    public $paymentAccountService;

    /** @var PaymentService */
    public $paymentService;

    public function injectDependencies(
        TsInternalPurchaseFactory $tsInternalPurchaseFactory,
        TsInternalPurchaseRepository $tsInternalPurchaseRepository,
        PaymentInvoiceFactory $invoiceFactory,
        PaymentInvoiceRepository $paymentInvoiceRepository,
        PaymentAccountService $paymentAccountService,
        PaymentService $paymentService
    )
    {
        $this->tsInternalPurchaseFactory    = $tsInternalPurchaseFactory;
        $this->tsInternalPurchaseRepository = $tsInternalPurchaseRepository;
        $this->invoiceFactory               = $invoiceFactory;
        $this->paymentInvoiceRepository     = $paymentInvoiceRepository;
        $this->paymentAccountService        = $paymentAccountService;
        $this->paymentService               = $paymentService;
    }

    public function cancel(TsInternalPurchase $tsInternalPurchase)
    {

    }

    public function processCertification(TsInternalPurchaseCertification $certification)
    {
        $certification->start_date = DateHelper::now();
        $certification->safeSave();
        $certification->companyService->certification = CompanyService::CERT_TYPE_PROGRESS;
        $certification->companyService->safeSave();
    }

    public function setPayed(PaymentInvoice $paymentInvoice)
    {
        $internalPurchase =$paymentInvoice->tsInternalPurchase;
        $internalPurchase->status = TsInternalPurchase::STATUS_PAYED;
        $internalPurchase->safeSave();
        if ($internalPurchase->tsInternalPurchaseCertification ) {
            $this->processCertification($internalPurchase->tsInternalPurchaseCertification);
        }
        if ($internalPurchase->isDeposit()) {
            $this->transferDeposit($paymentInvoice);
        }
    }

    protected function fixPrimaryInvoice($tsInternalPurchase)
    {
        /** @var PaymentInvoice $primaryInvoice */
        $primaryInvoice                           = $this->paymentInvoiceRepository->getNewInvoiceByTsInternalPurchaseAndPaymentMethod(
            $tsInternalPurchase,
            PaymentTransaction::VENDOR_BRAINTREE
        );
        $tsInternalPurchase->primary_invoice_uuid = $primaryInvoice->uuid;
        $tsInternalPurchase->safeSave();
    }

    public function createOrder(CompanyService $companyService)
    {
        if ($companyService->isCertificated()) {
            throw new ValidationException($companyService, _t('site.tsInternal', 'Already certificated'));
        }
        $tsInternalPurchase = $this->tsInternalPurchaseFactory->createCertificationForCompanyService($companyService);
        $paymentInvoice    = $this->invoiceFactory->createWithPaymentMethodsByTsInternalPurchase($tsInternalPurchase);

        $this->tsInternalPurchaseRepository->save($tsInternalPurchase);
        $this->paymentInvoiceRepository->save($paymentInvoice);
        $this->fixPrimaryInvoice($tsInternalPurchase);


        return $tsInternalPurchase;
    }

    /**
     * @param PaymentInvoice $paymentInvoice
     * @return void
     * @throws PaymentException
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     */
    protected function transferDeposit(PaymentInvoice $paymentInvoice):void
    {
        $payments = $paymentInvoice->payments;
        $payment = reset($payments);
        $paymentReservedAccount = $this->paymentAccountService->getUserPaymentAccount($paymentInvoice->user, PaymentAccount::ACCOUNT_TYPE_RESERVED, $paymentInvoice->currency);
        $paymentMainAccount = $this->paymentAccountService->getUserPaymentAccount($paymentInvoice->user, PaymentAccount::ACCOUNT_TYPE_MAIN, $paymentInvoice->currency);
        $reservedOperation = $this->paymentService->findReservedDetail($payment);
        if (!$reservedOperation) {
            throw new InvalidArgumentException('Not found reserved operation');
        }
        $amountTotal = Money::create($reservedOperation->toPaymentDetail()->amount, $paymentInvoice->currency);
        $this->paymentService->transferMoney(
            $payment,
            $paymentReservedAccount,
            $paymentMainAccount,
            $amountTotal,
            PaymentDetail::TYPE_PAYMENT,
            'Balance refill'
        );
    }

    public function transferFromReservedToTreatstock(TsInternalPurchase $tsInternalPurchase)
    {
        $paymentInvoice = $tsInternalPurchase->primaryInvoice;
        $payments = $paymentInvoice->payments;
        $payment = reset($payments);
        $paymentReservedAccount = $this->paymentAccountService->getUserPaymentAccount($paymentInvoice->user, PaymentAccount::ACCOUNT_TYPE_RESERVED);
        $treatstockAccount = $this->paymentAccountService->getTreatstockAccount(PaymentAccount::ACCOUNT_TYPE_MAIN, $paymentInvoice->currency);
        $this->paymentService->transferMoney(
            $payment,
            $paymentReservedAccount,
            $treatstockAccount,
            $paymentInvoice->getAmountTotalWithRefund(),
            PaymentDetail::TYPE_PAYMENT,
            'Finish payment'
        );
    }
}