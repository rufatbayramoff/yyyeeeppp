<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.03.17
 * Time: 9:48
 */

namespace common\modules\rabbitMq\components;


use AMQPChannel;
use AMQPConnection;
use AMQPExchange;
use AMQPQueue;

class RabbitMq
{
    public $host;
    public $port;
    public $user;
    public $password;
    public $handlerHost;

    /** @var AMQPConnection */
    public $connection = null;

    const TREATSTOCK_EXCHANGE = 'treatstock';

    public function connect()
    {
        $this->connection = new AMQPConnection(
            [
                'host'     => $this->host,
                'port'     => $this->port,
                'login'     => $this->user,
                'password' => $this->password,
                'vhost'    => '/'
            ]
        );
        $this->connection->connect();
    }

    /**
     * @return AMQPConnection
     */
    public function getConnection()
    {
        if (!$this->connection) {
            $this->connect();
        }
        return $this->connection;
    }

    public function closeConnection()
    {
        if (!$this->connection) {
            $this->connection->disconnect();
            $this->connection = null;
        }
    }

    public function getExchange($channel)
    {
        $exchange = new AMQPExchange($channel);
        $exchange->setName(self::TREATSTOCK_EXCHANGE);
        $exchange->setType(AMQP_EX_TYPE_DIRECT);
        $exchange->setFlags(AMQP_DURABLE);
        $exchange->declareExchange();
        return $exchange;
    }

    public function getQueue($channel, $queueName)
    {
        $queue = new AMQPQueue($channel);
        $queue->setName($queueName);
        $queue->setFlags(AMQP_DURABLE);
        $queue->declareQueue();
        return $queue;
    }

    public function listenQueue($queueName, $callback)
    {
        $connection = $this->getConnection();
        $connection->connect();
        $channel = new AMQPChannel($connection);
        $exchange = $this->getExchange($channel);
        $queue = $this->getQueue($channel, $queueName);
        $queue->bind($exchange->getName(), $queueName);
        while (true) {
            $queue->consume($callback, AMQP_AUTOACK);
        }
    }

    public function sendMessage($queueName, $message)
    {
        $connection = $this->getConnection();
        $channel = new AMQPChannel($connection);
        $exchange = $this->getExchange($channel);
        $this->getQueue($channel, $queueName);
        $exchange->publish(json_encode($message), $queueName);
    }
}