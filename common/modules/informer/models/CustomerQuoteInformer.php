<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


use common\models\InformerCustomerPreorder;

class CustomerQuoteInformer extends BaseInformer
{
    public $type = 'customer_quote';

    public $paths = [];

    public const STORAGE_CLASS = InformerCustomerPreorder::class;
}