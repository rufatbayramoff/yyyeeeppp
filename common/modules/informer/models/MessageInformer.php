<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


class MessageInformer extends BaseInformer
{
    public $type = 'message';

    public $paths = [
        '/workbench/messages'
    ];

}