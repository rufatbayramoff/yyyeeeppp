<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


/*
 * New orders informer
 * Received orders informer
 *
 * Params:
 * orderId
 * storeOrderAttempt - main parameter
 */

use common\models\InformerStoreOrderAttempt;

class CompanyOrderInformer extends BaseInformer
{
    public $type = 'company_order';

    public $paths = [];

    public const STORAGE_CLASS = InformerStoreOrderAttempt::class;
}