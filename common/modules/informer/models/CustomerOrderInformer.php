<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


use common\models\InformerCustomerOrder;

class CustomerOrderInformer extends BaseInformer
{
    public $type = 'customer_order';

    public $paths = [];

    public const STORAGE_CLASS = InformerCustomerOrder::class;
}