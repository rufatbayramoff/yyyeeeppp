<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


class CompanyInformer extends BaseInformer
{
    public $type = 'company';

    public $paths = [
        '/mybusiness/company/edit-ps'
    ];
}