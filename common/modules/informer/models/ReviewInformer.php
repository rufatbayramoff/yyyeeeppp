<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


class ReviewInformer extends BaseInformer
{
    public $type = 'review';

    public $paths = [
        '/workbench/reviews'
    ];
}