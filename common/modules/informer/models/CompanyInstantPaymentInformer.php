<?php

namespace common\modules\informer\models;


/**
 * Class CompanyInstantPaymentInformer
 *
 * @package common\modules\informer\models
 */
class CompanyInstantPaymentInformer extends BaseInformer
{
    public $type = 'company_instant_payment';

    public $paths = [
        '/workbench/instant-payments/recv-list'
    ];
}