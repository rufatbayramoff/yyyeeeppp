<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


use yii\redis\ActiveRecord;

/**
 * Class BaseInformerRedis
 *
 * @property string $key
 * @property string $type
 * @property string $user_id
 * @property string $created_at
 * @property string $count
 * @property array $paths
 * @property array $parts
 *
 * @package common\modules\informer\models
 */
class BaseInformerRedis extends ActiveRecord
{
    public function attributes()
    {
        return [
            'key',
            'type',
            'user_id',
            'created_at',
            'count',
            'paths',
            'parts',
        ];
    }

    public static function primaryKey()
    {
        return ['key'];
    }

    public function getInformerCLass()
    {
        return BaseInformer::class;
    }

    public function saveInformer()
    {
        $origPaths   = $this->paths;
        $origParts   = $this->parts;
        $this->paths = $origPaths?implode(',', $origPaths):'';
        $this->parts = $origParts?json_encode($origParts):null;
        $this->save();
        $this->paths = $origPaths;
        $this->parts = $origParts;
    }

    public static function deleteByKey($key)
    {
        return self::deleteAll(['key' => $key]);
    }

    public static function getByKey($key)
    {
        $item        = self::findOne(['key' => $key]);
        if ($item && $item->paths) {
            $item->paths = explode(',', $item->paths);
        }
        return $item;
    }
}