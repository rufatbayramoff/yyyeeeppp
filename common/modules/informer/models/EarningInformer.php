<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


class EarningInformer extends BaseInformer
{
    public $type = 'earning';

    public $paths = [
        '/workbench/payments'
    ];

}