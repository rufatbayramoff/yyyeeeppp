<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


class TaxInformer extends BaseInformer
{
    public $type = 'tax';

    public $paths = [
        '/mybusiness/taxes'
    ];
}