<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


class ProductInformer extends BaseInformer
{
    public $type = 'product';

    public $paths = [
        '/mybusiness/products'
    ];
}