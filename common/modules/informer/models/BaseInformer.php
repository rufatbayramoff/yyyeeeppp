<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;

use frontend\components\image\ImageHtmlHelper;
use yii\redis\ActiveRecord;

class BaseInformer
{
    public $key;
    public $type;
    public $user_id;
    public $created_at;
    public $count = 1;

    public const STORAGE_CLASS = BaseInformerRedis::class;

    /**
     * One informer may contain information about sevral entities. If not empty, count will be calculated by informerParts count.
     *
     * @var array
     */
    public $parts;

    /**
     * List of urls where informer will be deleted
     *
     * @var string[]
     */
    public $paths = [];

    public function attributes()
    {
        return [
            'key',
            'type',
            'user_id',
            'created_at',
            'count',
            'parts',
            'paths'
        ];
    }
    public function attributeValues()
    {
        $result = [];
        foreach ($this->attributes() as $v) {
            $result[$v] = $this->{$v};
        }
        return $result;
    }

    public function getKey()
    {
        return $this->user_id . ':' . $this->getType();
    }

    public function getType()
    {
        return $this->type;
    }

    public function getCount()
    {
        if ($this->parts) {
            return count($this->parts);
        }
        return $this->count;
    }

    public function getAsRedDot($count = null)
    {
        $showCount = $this->getCount();
        if($count && $count < $this->getCount()){
            $showCount = $count;
        }
        return ImageHtmlHelper::getRedDot($showCount);
    }
}
