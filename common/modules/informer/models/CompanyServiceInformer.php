<?php
/**
 * User: nabi
 */

namespace common\modules\informer\models;


class CompanyServiceInformer extends BaseInformer
{
    public $type = 'service';

    public $paths = [
        '/mybusiness/services'
    ];
}