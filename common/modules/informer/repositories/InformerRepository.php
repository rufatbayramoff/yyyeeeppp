<?php
/**
 * User: nabi
 */

namespace common\modules\informer\repositories;

use common\modules\informer\interfaces\InformerStorageInterface;
use common\modules\informer\models\BaseInformer;
use common\modules\informer\models\BaseInformerRedis;
use yii\caching\ApcCache;

/**
 * Class InformerRepository
 *
 * @package common\modules\informer\repositories
 */
class InformerRepository
{
    /**
     * @var BaseInformer[]
     */
    protected static $cache = [];

    public function save(BaseInformer $informer)
    {
        $storageClass = $informer::STORAGE_CLASS;
        /** @var InformerStorageInterface $baseInformerStored */
        $baseInformerStored = new $storageClass();
        $baseInformerStored->key = $informer->key;
        $baseInformerStored->type = $informer->type;
        $baseInformerStored->user_id = $informer->user_id;
        $baseInformerStored->created_at = $informer->created_at;
        $baseInformerStored->count = $informer->count;
        $baseInformerStored->paths = $informer->paths;
        $baseInformerStored->parts = $informer->parts;
        $baseInformerStored->saveInformer();
        self::$cache[$informer->key] = $informer;
    }

    public function remove(BaseInformer $informer)
    {
        $idKey = $informer->getKey();
        unset(self::$cache[$idKey]);
        $storageClass = $informer::STORAGE_CLASS;
        $storageClass::deleteByKey($idKey);
    }

    /**
     * @param $idKey
     * @return BaseInformer
     */
    public function getByKey($idKey, $informerClass)
    {
        if (array_key_exists($idKey, self::$cache)) {
            return self::$cache[$idKey];
        }

        $storageClass = $informerClass::STORAGE_CLASS;
        /** @var  InformerStorageInterface $baseInformerStored */
        $baseInformerStored =  $storageClass::getByKey($idKey);
        if (!$baseInformerStored) {
            self::$cache[$idKey] = null;
            return null;
        }
        $informerClass            = $baseInformerStored->getInformerCLass();
        $baseInformer             = new $informerClass();
        $baseInformer->key        = $baseInformerStored->key;
        $baseInformer->type       = $baseInformerStored->type;
        $baseInformer->user_id    = $baseInformerStored->user_id;
        $baseInformer->created_at = $baseInformerStored->created_at;
        $baseInformer->count      = $baseInformerStored->count;
        $baseInformer->paths      = $baseInformerStored->paths;
        $baseInformer->parts      = $baseInformerStored->parts;
        self::$cache[$idKey] = $baseInformer;
        return $baseInformer;
    }

}