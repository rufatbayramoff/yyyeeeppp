<?php
/**
 * User: nabi
 */

namespace common\modules\informer\services;

use common\components\DateHelper;
use common\models\InformerCustomerOrder;
use common\models\InformerCustomerPreorder;
use common\models\InformerStoreOrderAttempt;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\modules\informer\models\BaseInformer;
use common\modules\informer\models\CompanyInformer;
use common\modules\informer\models\CompanyInstantPaymentInformer;
use common\modules\informer\models\CompanyOrderInformer;
use common\modules\informer\models\CompanyServiceInformer;
use common\modules\informer\models\CustomerOrderInformer;
use common\modules\informer\models\CustomerQuoteInformer;
use common\modules\informer\models\EarningInformer;
use common\modules\informer\models\MessageInformer;
use common\modules\informer\models\ProductInformer;
use common\modules\informer\models\ReviewInformer;
use common\modules\informer\models\TaxInformer;
use common\modules\informer\repositories\InformerRepository;
use yii\base\InvalidArgumentException;
use yii\web\Request;

/**
 * Class InformerService
 *
 * @package common\modules\informer\services
 */
class InformerService
{
    /**
     * @var BaseInformer[]
     */
    public $informers = [
        CustomerOrderInformer::class,
        CustomerQuoteInformer::class,

        CompanyInformer::class,
        CompanyOrderInformer::class,
        CompanyServiceInformer::class,
        CompanyInstantPaymentInformer::class,

        EarningInformer::class,
        MessageInformer::class,
        ProductInformer::class,
        ReviewInformer::class,
        TaxInformer::class,
    ];

    /**
     * informer for given user
     *
     * @var User
     */
    private $user;

    /**
     * @var InformerRepository
     */
    private $informerRepository;

    public function injectDependencies(InformerRepository $informerRepository, UserIdentityProvider $userProvider)
    {
        $this->user = $userProvider->getUser();
        $this->informerRepository = $informerRepository;
    }

    /**
     * @param User $user
     * @param string $informerClass
     * @param null|array $informerParts
     * @return BaseInformer
     */
    public function createInformerForUser(User $user, $informerClass, $informerParts = null)
    {
        $oldInformer = $this->getInformerForUser($user, $informerClass, $informerParts);
        /** @var BaseInformer $informer */
        $informer = new $informerClass;
        $informer->user_id = $user->id;
        $informer->created_at = DateHelper::now();
        $informer->count = 1;
        $informer->key = $informer->getKey();
        if ($informerParts) {
            $informer->parts = $informerParts;
        }
        if ($oldInformer) {
            if ($informerParts) {
                if ($oldInformer->parts) { // Protect from old format
                    $informer->parts = $informer->parts + $oldInformer->parts;
                }
            } else {
                $informer->count = 1 + $oldInformer->count;
            }
        }
        $this->saveInformer($informer);
        return $informer;
    }


    /**
     * @param User $user
     * @param $informerClass
     * @return BaseInformer
     */
    public function getInformerForUser(User $user, $informerClass)
    {
        return $this->informerRepository->getByKey($this->generateKey($user, $informerClass), $informerClass);
    }

    /**
     * @param BaseInformer $informer
     * @return bool
     */
    public function saveInformer(BaseInformer $informer)
    {
        return $this->informerRepository->save($informer);
    }

    /**
     * @param User $user
     * @param $type
     * @return bool
     */
    public function removeUserInformer(User $user, $informerClass)
    {
        $informer = $this->getInformerForUser($user, $informerClass);
        if ($informer) {
            return $this->informerRepository->remove($informer);
        }
    }

    public function markAsViewedInformer(BaseInformer $informer, $informerPartKey)
    {
        if (!$informerPartKey) {
            throw new InvalidArgumentException('Informer part key required');
        }
        if (!array_key_exists($informerPartKey, $informer->parts)) {
            return ;
        }
        unset($informer->parts[$informerPartKey]);
        if (count($informer->parts)<1) {
            $this->informerRepository->remove($informer);
        } else {
            $this->informerRepository->save($informer);
        }
    }

    /**
     * @param $type
     * @return BaseInformer
     */
    public function createInformer($informerClass)
    {
        $informer = $this->createInformerForUser($this->user, $informerClass);
        return $informer;
    }

    /**
     * @param $informerClass
     * @return BaseInformer
     */
    public function getInformer($informerClass)
    {
        return $this->getInformerForUser($this->user, $informerClass);
    }


    /**
     * @param User $user
     * @return BaseInformer[]
     */
    public function getUserInformers(User $user)
    {
        $informers = [];
        foreach ($this->informers as $informerClass) {
            $informerId = $this->generateKey($user, $informerClass);
            $informer = $this->informerRepository->getByKey($informerId, $informerClass);
            if (!$informer) {
                continue;
            }
            $informers[] = $informer;
        }
        return $informers;
    }

    /**
     * @return array
     */
    public function getInformerTypes()
    {
        static $types = [];
        foreach ($this->informers as $informer) {
            /** @var BaseInformer $inf */
            $inf = new $informer();
            $types[] = $inf->getType();
        }
        return $types;
    }

    /**
     * @param User $user
     * @param $type
     * @return string
     */
    public function generateKey(User $user, $informerClass)
    {
        /** @var BaseInformer $typeObj */
        $typeObj = new $informerClass;
        $typeObj->user_id = $user->id;
        $type = $typeObj->getType();
        if (!in_array($type, $this->getInformerTypes())) {
            throw new \Exception('[Informer] Not supported informer.');
        }
        return $typeObj->getKey();
    }

    /**
     * @param User $user
     * @param Request $request
     */
    public function deleteUserInformersByRequest(User $user, Request $request)
    {
        $informerItems = $this->getUserInformers($user);
        foreach ($informerItems as $informer) {
            /** @var BaseInformer $inf */
            if (empty($informer->paths)) {
                continue;
            }
            foreach ($informer->paths as $path) {
                if (empty($path)) {
                    continue;
                }
                if (strpos($request->url, $path) === 0) {
                    $this->informerRepository->remove($informer);
                }
            }
        }
    }

    public function removeOldInformer()
    {
        InformerStoreOrderAttempt::deleteAll(['<','created_at',DateHelper::subNow('P1M')]);
        InformerCustomerPreorder::deleteAll(['<','created_at',DateHelper::subNow('P1M')]);
        InformerCustomerOrder::deleteAll(['<','created_at',DateHelper::subNow('P1M')]);
    }
}