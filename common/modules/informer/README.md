### Fast and easy usage:
```
// easy fast way to add informer
InformerModule::addInformer($user, ServiceInformer::class);

// easy way to get informer for given user
InformerModule::getInformer($user, CompanyOrderInformer::class)
```



## Usage

``` 
$informerService = new InformerService();

// create informer for given user with specific informer
$informerService->createInformer(User $user, TaxInformer::class)

// get informers and use them to show red dots in correct places
$informerService->getInformer(TaxInformer::class)

// remove informer after visit
$informerService->removeInformer($user, TaxInformer::class);

// in BaseController
// visit of given page, deletes informers binded to that page.
InformerModule::deleteUserInformersByRequest($user, app('request'));

```
