<?php
/**
 * User: nabi
 */

namespace common\modules\informer;

use common\models\Preorder;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\modules\informer\models\CompanyOrderInformer;
use common\modules\informer\models\CustomerOrderInformer;
use common\modules\informer\models\CustomerQuoteInformer;
use common\modules\informer\services\InformerService;
use Yii;
use yii\base\Module;
use yii\web\Request;


class InformerModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\informer\controllers';

    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param User $user
     * @param $informerClass
     * @param array $informerParts
     * @return models\BaseInformer
     * @throws \yii\base\InvalidConfigException
     */
    public static function addInformer(User $user, $informerClass, $informerParts = [])
    {
        /**
         * @var InformerService $informerService
         */
        $informerService = \Yii::createObject(InformerService::class);
        return $informerService->createInformerForUser($user, $informerClass, $informerParts);
    }

    public static function addCompanyOrderInformer(StoreOrderAttemp $attempt)
    {
        self::addInformer($attempt->interceptionCompany()->user, CompanyOrderInformer::class, [
            $attempt->id => [
                'storeOrderAttemptId' => $attempt->id
            ]
        ]);
    }

    public static function addCustomerOrderInformer(StoreOrder $storeOrder)
    {
        self::addInformer($storeOrder->user, CustomerOrderInformer::class, [
            $storeOrder->id => [
                'storeOrderId' => $storeOrder->id
            ]
        ]);
    }

    public static function addCustomerQuoteInformer(Preorder $preorder)
    {
        self::addInformer($preorder->user, CustomerQuoteInformer::class, [
            $preorder->id => [
                'preorderId' => $preorder->id
            ]
        ]);
    }


    /**
     * @param User $user
     * @param $informerClass
     * @return models\BaseInformer
     * @throws \yii\base\InvalidConfigException
     */
    public static function getInformer(User $user, $informerClass)
    {
        $informerService = \Yii::createObject(InformerService::class);
        return $informerService->getInformerForUser($user, $informerClass);
    }

    public static function deleteInformers(User $user, Request $request)
    {
        /** @var InformerService $informerService */
        $informerService = \Yii::createObject(InformerService::class);
        return $informerService->deleteUserInformersByRequest($user, $request);
    }

    public static function markAsViewed(User $user, $informerClass, $partKey)
    {
        $informer = self::getInformer($user, $informerClass);
        if ($informer) {
            $informerService = \Yii::createObject(InformerService::class);
            $informerService->markAsViewedInformer($informer, $partKey);
        }
    }

    public static function markAsViewedCompanyOrderInformer(StoreOrderAttemp $attempt)
    {
        self::markAsViewed($attempt->interceptionCompany()->user, CompanyOrderInformer::class, $attempt->id);
    }

    public static function markAsViewedCustomerOrderInformer(StoreOrder $storeOrder)
    {
        self::markAsViewed($storeOrder->user, CustomerOrderInformer::class, $storeOrder->id);
    }

    public static function markAsViewedCustomerQuoteInformer(Preorder $preorder)
    {
        self::markAsViewed($preorder->user, CustomerQuoteInformer::class, $preorder->id);
    }
}
