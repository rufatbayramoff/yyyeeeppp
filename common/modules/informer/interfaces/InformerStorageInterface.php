<?php

namespace common\modules\informer\interfaces;

/**
 * Interface InformerStorageInterface
 * @package common\modules\informer\interfaces
 * @property string $key;
 * @property string type
 * @property int $user_id
 * @property string $created_at
 * @property int $count
 * @property string[] paths
 * @property array $parts
 */
interface InformerStorageInterface
{
    public function getInformerCLass();

    public function saveInformer();

    public static function deleteByKey($key);

    public static function getByKey($key);
}