<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.10.16
 * Time: 14:46
 */

namespace common\modules\contentAutoBlocker\repositories;

use common\components\ArrayHelper;
use common\models\ContentFilterBannedPhrase;
use common\models\SystemLang;

class BannedPhraseRepository
{
    public function getBannedPhrasesList()
    {
        $listObject = ContentFilterBannedPhrase::find()->all();
        return ArrayHelper::map($listObject, 'id', 'phrase');
    }

    public static function import($list)
    {
        $lang = SystemLang::tryFind(['iso_code' =>'en-US']);
        foreach ($list as $listElement) {
            $listElement = trim($listElement);
            if ($listElement && !ContentFilterBannedPhrase::findOne(['phrase' => $listElement])) {
                $bannedPhrase = new ContentFilterBannedPhrase();
                $bannedPhrase->phrase = $listElement;
                $bannedPhrase->lang_id = $lang->id;
                $bannedPhrase->safeSave();
            }
        }
    }
}

