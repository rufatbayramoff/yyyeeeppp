<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 12:15
 */

namespace common\modules\contentAutoBlocker;

use common\modules\contentAutoBlocker\components\checkObjects\CheckMessage;
use common\modules\contentAutoBlocker\components\checkObjects\CheckMessageTopic;
use common\modules\contentAutoBlocker\components\ContentBlocker;
use common\modules\contentAutoBlocker\components\contentFilters\BannedPhraseFilter;
use common\modules\contentAutoBlocker\components\contentFilters\EmailFilter;
use common\modules\contentAutoBlocker\components\contentFilters\PhoneFilter;
use common\modules\contentAutoBlocker\components\contentFilters\SkypeFilter;
use common\modules\contentAutoBlocker\components\contentFilters\WebSiteFilter;
use common\modules\contentAutoBlocker\components\Statistics;
use yii\base\Module;

/**
 * Class ContentAutoBlockerModule
 *
 * @property ContentBlocker $contentBlocker
 * @property Statistics $statistics
 *
 * @package common\modules\contentAutoBlocker
 */
class ContentAutoBlockerModule extends Module
{
    const CHECK_MESSAGE = 'message';
    const CHECK_MESSAGE_TOPIC = 'messageTopic';

    public $checkObjects = [
        self::CHECK_MESSAGE => CheckMessage::class,
        self::CHECK_MESSAGE_TOPIC => CheckMessageTopic::class,
    ];

    public $enabledFilters = [
        BannedPhraseFilter::class,
        EmailFilter::class,
        PhoneFilter::class,
        SkypeFilter::class,
        WebSiteFilter::class
    ];

    protected function getComponentsList()
    {
        return [
            'contentBlocker' => ContentBlocker::class,
            'statistics'     => Statistics::class,
        ];
    }

    protected function initComponentsModule($componentsList)
    {
        foreach ($componentsList as $componentName => $componentClass) {
            $this->$componentName->setModule($this);
        }
    }

    /**
     * Инициализация и расшифровка конфига модуля
     *
     * @throws \yii\base\InvalidParamException
     */
    protected function initConfig()
    {

    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();

        $this->initConfig();
        $componentsList = $this->getComponentsList();
        $this->setComponents($componentsList);
        $this->initComponentsModule($componentsList);
    }
}