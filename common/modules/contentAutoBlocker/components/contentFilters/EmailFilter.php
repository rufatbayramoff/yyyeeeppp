<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 15:20
 */

namespace common\modules\contentAutoBlocker\components\contentFilters;

class EmailFilter implements ContentFilterInterface
{
    const REGULAR_EXPRESSION = '/\A[^@]+@([^@\.]+\.)+[^@\.]+\z/';

    public function isAlarmText($textData)
    {
        $result = preg_match(self::REGULAR_EXPRESSION, $textData, $matches);
        if ($result) {
            return true;
        }
        return false;
    }
}