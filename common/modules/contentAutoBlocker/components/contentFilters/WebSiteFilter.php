<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 15:23
 */

namespace common\modules\contentAutoBlocker\components\contentFilters;

class WebSiteFilter implements ContentFilterInterface
{
    const REGULAR_EXPRESSION1 = '/(www\.)([a-z0-9\-.]+)(.[a-z]{2,3})(\s|$)/i';
    const REGULAR_EXPRESSION2 = '/([a-z0-9\-.]+)(.com|.net|.ru|.org)(\s|$)/i';
    const REGULAR_EXPRESSION3 = '/(http:\/\/|https:\/\/(.*))/i';

    const ALLOWED_SITES = ['www.treatstock.com', 'treatstock.com'];

    public function isAlarmText($textData)
    {
        $result = preg_match(self::REGULAR_EXPRESSION1, $textData, $matches);
        $result2 = preg_match(self::REGULAR_EXPRESSION2, $textData, $matches2);
        $result3 = preg_match(self::REGULAR_EXPRESSION3, $textData, $matches3);
        if ($matches2 && in_array($matches2[0], self::ALLOWED_SITES, true)) {
            return false;
        }
        if ($matches3 && array_key_exists(2, $matches3)) {
            foreach (self::ALLOWED_SITES as $site) {
                if (mb_substr($matches3[2],0,strlen($site)) === $site) {
                    return false;
                }
            }
        }
        if ($result || $result2 || $result3) {
            return true;
        }
        return false;
    }
}