<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 15:23
 */

namespace common\modules\contentAutoBlocker\components\contentFilters;

class PhoneFilter implements ContentFilterInterface
{
    const REGULAR_EXPRESSION = '/([\s\-\.\?\!\)\(\,\:\+]{1,}(?:(?:9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)|\((?:9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\))[0-9. -]{4,14})/';

    const MAIL_TRACK_EXPRESSION = [
        'FedEx'         => '/((96\d\d\d\d\d ?\d\d\d\d|96\d\d) ?\d\d\d\d ?d\d\d\d( ?\d\d\d)?)/i',
        'Usps'          => '/(91\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d|91\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d)/i',
        'MyCustomUsps2' => '/[\d]{4}[\ \-\_]{1,3}[\d]{4}[\ \-\_]{1,3}[\d]{4}[\ \-\_]{1,3}[\d]{4}[\ \-\_]{1,3}[\d]{4}[\ \-\_]{1,3}[\d]{2}/',
    ];

    protected function digitsCount($phone)
    {
        $cnt = 0;
        for ($i = 0, $iMax = strlen($phone); $i < $iMax; $i++) {
            $charOrd = ord($phone[$i]);
            if ($charOrd > 47 && $charOrd < 58) {
                $cnt++;
            }
        }
        return $cnt;
    }

    protected function prefilterTextData($textData)
    {
        $allMatches = [];
        foreach (self::MAIL_TRACK_EXPRESSION as $trackExpression) {
            $result = preg_match($trackExpression, $textData, $matches);
            foreach ($matches as $oneMatch) {
                $allMatches[$oneMatch] = $oneMatch;
            }
        }
        usort(
            $allMatches,
            function ($a, $b) {
                return strlen($b) <=> strlen($a);
            }
        );
        $textData = str_replace($allMatches, [], $textData);
        return $textData;
    }

    public function isAlarmText($textData)
    {
        $textData = $this->prefilterTextData($textData);

        $result = preg_match(self::REGULAR_EXPRESSION, $textData, $matches);
        if ($result) {
            // Check phone lenght
            foreach ($matches as $oneMatch) {
                $digitsCount = $this->digitsCount($oneMatch);
                if (($digitsCount > 6) && ($digitsCount < 13)) {
                    return true;
                }
            }
        }
        return false;
    }
}