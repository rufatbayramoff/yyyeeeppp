<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 15:24
 */

namespace common\modules\contentAutoBlocker\components\contentFilters;


use common\modules\contentAutoBlocker\repositories\BannedPhraseRepository;

class BannedPhraseFilter implements ContentFilterInterface
{
    /** @var  BannedPhraseRepository */
    public $bannedPhraseRepository;

    public $matches = [];

    public $checkMode = self::MODEL_CHECK_TEXT;

    const MODEL_CHECK_TEXT = 'checkText';
    const MODEL_CHECK_WORD = 'checkWord';

    /**
     * BannedWordsFilter constructor.
     *
     * @param BannedPhraseRepository $bannedWordsRepository
     */
    public function __construct(BannedPhraseRepository $bannedWordsRepository)
    {
        $this->bannedPhraseRepository = $bannedWordsRepository;
    }

    public function getFullRegularExpression($bannedPhrase)
    {
        return '/(\s|^|[[:punct:]])(' . $bannedPhrase . ')(\s|$|[[:punct:]])/i';
    }

    public function setCheckMode($checkMode)
    {
        $this->checkMode = $checkMode;
    }

    public function checkOnePhrase($textData, $bannedPhrase)
    {
        if ($this->checkMode === self::MODEL_CHECK_TEXT) {
            $regularExpression = $this->getFullRegularExpression($bannedPhrase);
            $result = preg_match($regularExpression, $textData, $matches);
            if (!empty($matches)) {
                $this->matches[] = $matches;
            }
            return $result;
        }
        if ($this->checkMode === self::MODEL_CHECK_WORD) {
            return strpos(strtolower($textData), strtolower($bannedPhrase))!==FALSE;
        }
    }

    public function isAlarmText($textData)
    {
        $bannedWords = $this->bannedPhraseRepository->getBannedPhrasesList();
        foreach ($bannedWords as $bannedPhrase) {
            $result = $this->checkOnePhrase($textData, $bannedPhrase);
            if ($result) {
                return true;
            }
        }
        return false;
    }
}