<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 15:33
 */

namespace common\modules\contentAutoBlocker\components\contentFilters;

interface ContentFilterInterface
{
    /**
     * Return true if filter alert
     *
     * @param $textData
     * @return bool
     */
    public function isAlarmText($textData);
}