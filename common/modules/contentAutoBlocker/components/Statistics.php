<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 15:26
 */


namespace common\modules\contentAutoBlocker\components;

use common\components\Emailer;
use common\models\ContentFilterStatistics;
use common\models\User;
use common\modules\contentAutoBlocker\ContentAutoBlockerModule;
use common\modules\contentAutoBlocker\models\UserStatistics;
use frontend\components\UserUtils;
use Yii;
use yii\base\Component;
use yii\base\Module;
use yii\db\Query;

class Statistics extends Component
{
    const DIRECTION_SEND = 'send';
    const DIRECTION_RECIVE = 'recive';

    const TYPE_MESSAGE_ALERT = 'message';
    const TYPE_MESSAGE_TOPIC_ALERT = 'messageTopic';
    const NOTIFIER_PERIOD = 10;

    /**
     * @var ContentAutoBlockerModule
     */
    protected $module;

    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    protected function checkAndSendAlarm($userId)
    {
        $count = ContentFilterStatistics::find()->select('count(id) as cnt')->where(['user_id' => $userId, 'direction' => 'send', 'alert_type' => 'message'])->scalar();
        if (($count % self::NOTIFIER_PERIOD) === 0) {
            $user = User::findOne(['id' => $userId]);
            //Every 10 messages
            $emailer = new Emailer();
            $emailer->sendSupportMessage(
                'Alert. User "' . $user->username . '" has ' . $count . ' blocked messages.',
                'User "'.$user->username.'" has '.$count." blocked messages.\n" .
                'User profile: ' . UserUtils::getUserPublicProfileUrl($user)
            );
        }
    }

    public function createAlert($userId, $alertType, $direction, $alertInfo)
    {
        if ($userId < 1000) {
            // Dont log system users
            return;
        }

        /** @var ContentFilterStatistics $statElement */
        $statElement = Yii::createObject(ContentFilterStatistics::class);
        $statElement->user_id = $userId;
        $statElement->alert_type = $alertType;
        $statElement->direction = $direction;
        $statElement->alert_info = (string)$alertInfo;
        $statElement->safeSave();

        if ($direction === self::DIRECTION_SEND) {
            $this->checkAndSendAlarm($userId);
        }
    }

    /**
     * @param User $user
     * @return UserStatistics
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function formStatisticForUser(User $user)
    {
        $userId = $user->id;
        /** @var UserStatistics $userStatistics */
        $userStatistics = Yii::createObject(UserStatistics::class);
        $userStatistics->trySendBannedMessageCount = ContentFilterStatistics::find()->select('count(id) as cnt')->where(
            ['user_id' => $userId, 'direction' => self::DIRECTION_SEND]
        )->scalar();
        $userStatistics->tryReceiveBannedMessageCount = ContentFilterStatistics::find()->select('count(id) as cnt')->where(
            ['user_id' => $userId, 'direction' => self::DIRECTION_RECIVE]
        )->scalar();
        $userStatistics->trySendBannedMessageToInterlocutorCount = (int)Yii::$app->getDb()->createCommand(
            'SELECT count(DISTINCT b.user_id) FROM content_filter_statistics a, content_filter_statistics b 
WHERE a.direction="' . self::DIRECTION_SEND . '" AND a.user_id=' . $userId . ' AND  a.alert_type=b.alert_type AND a.alert_info=b.alert_info AND b.direction="' . self::DIRECTION_RECIVE . '"'
        )->queryScalar();
        $userStatistics->tryReceiveBannedMessageFromInterlocutorCount = (int)Yii::$app->getDb()->createCommand(
            'SELECT count(DISTINCT b.user_id) FROM content_filter_statistics a, content_filter_statistics b 
WHERE a.direction="' . self::DIRECTION_RECIVE . '" AND a.user_id=' . $userId . ' AND  a.alert_type=b.alert_type AND a.alert_info=b.alert_info AND b.direction="' . self::DIRECTION_SEND . '"'
        )->queryScalar();
        return $userStatistics;
    }
}