<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.10.16
 * Time: 14:59
 */

namespace common\modules\contentAutoBlocker\components;

use common\models\ContentFilterCheckedMessages;
use common\models\base\MsgMessage;
use common\models\ContentFilterCheckedTopics;
use common\models\MsgTopic;
use common\modules\contentAutoBlocker\components\checkObjects\CheckObject;
use common\modules\contentAutoBlocker\ContentAutoBlockerModule;
use Yii;
use yii\base\Component;
use yii\base\Module;

class ContentBlocker extends Component
{
    /**
     * @var ContentAutoBlockerModule
     */
    protected $module;

    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    public function checkNewContent()
    {
        $enabledFilters = $this->module->enabledFilters;
        /** @var CheckObject $checkObject */
        foreach ($this->module->checkObjects as $checkObjectClass) {
            $checkObject = Yii::createObject($checkObjectClass);
            $checkObject->checkNewElements($enabledFilters);
        }
    }

    public function isObjectChecked($name, $checkObjectElement)
    {
        $checkObjectClass = $this->module->checkObjects[$name];
        $checkObject = Yii::createObject($checkObjectClass);
        return $checkObject->isObjectChecked($checkObjectElement);
    }

    public function isObjectBanned($name, $checkObjectElement, $checkImmediately)
    {
        $checkObjectClass = $this->module->checkObjects[$name];
        $checkObject = Yii::createObject($checkObjectClass);
        if ($checkImmediately) {
            $enabledFilters = $this->module->enabledFilters;
            return $checkObject->checkIsBanned($checkObjectElement, $enabledFilters);
        } else {
            return $checkObject->isObjectBanned($checkObjectElement);
        }

    }

}