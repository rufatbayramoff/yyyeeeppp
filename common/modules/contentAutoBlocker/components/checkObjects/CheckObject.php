<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.10.16
 * Time: 14:02
 */

namespace common\modules\contentAutoBlocker\components\checkObjects;

use common\modules\contentAutoBlocker\components\Statistics;
use Yii;
use yii\base\Component;

abstract class CheckObject extends Component {

    /**
     * @var Statistics
     */
    public $statistics;

    public function init()
    {
        $this->statistics = Yii::$app->getModule('contentAutoBlocker')->statistics;
    }

    public function isObjectChecked($checkObjectElement)
    {
        $relationName = $this->getCheckObjectsRelationName();
        if ($checkObjectElement->$relationName) {
            return true;
        } else {
            return false;
        }
    }

    public function isObjectBanned($checkObjectElement)
    {
        $relationName = $this->getCheckObjectsRelationName();
        $checkedMessages = $checkObjectElement->$relationName;
        $checkedMessage = reset($checkedMessages);
        if ($checkedMessage && $checkedMessage->isBanned) {
            return true;
        } else {
            return false;
        }
    }

    abstract public function checkNewElements($enabledFilters);
    abstract public function getCheckObjectsRelationName();
    abstract public function checkIsBanned($element, $enabledFilters);

    public function processFilters($text, $enabledFilters)
    {
        foreach ($enabledFilters as $oneFilterClassName) {
            $filter = Yii::createObject($oneFilterClassName);
            if ($filter->isAlarmText($text)) {
                return true;
            }
        }
        return false;
    }

}