<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.10.16
 * Time: 12:11
 */

namespace common\modules\contentAutoBlocker\components\checkObjects;

use common\models\ContentFilterCheckedMessages;
use common\models\MsgMessage;
use common\modules\contentAutoBlocker\components\Statistics;
use frontend\models\community\MessageFacade;
use Yii;

class CheckMessage extends CheckObject
{

    public function getCheckObjectsRelationName()
    {
        return 'contentFilterCheckedMessages';
    }

    public function checkNewElements($enabledFilters)
    {
        /** @var MsgMessage[] $newMessages */
        $newMessages = MsgMessage::find()->leftJoin('content_filter_checked_messages', 'msg_message.id=content_filter_checked_messages.message_id')->where(
            'content_filter_checked_messages.message_id is null'
        )->all();
        foreach ($newMessages as $message) {
            $processResult = $this->checkIsBanned($message, $enabledFilters);
            /** @var ContentFilterCheckedMessages $checkMessage */
            $checkMessage             = Yii::createObject(ContentFilterCheckedMessages::class);
            $checkMessage->message_id = $message->id;
            $checkMessage->isBanned   = (integer)$processResult;
            $checkMessage->safeSave();

            if (!$processResult) {
                // Not banned
                foreach ($message->topic->msgMembers as $member) {
                    if ($member->user_id != $message->user_id) {
                        MessageFacade::markTopicAsUnreaded($message->topic, $member->user);
                    }
                }
            }
        }
    }

    /**
     * @param MsgMessage $element
     * @param $enabledFilters
     * @return bool
     */
    public function checkIsBanned($element, $enabledFilters)
    {
        if ($element->user->shadow_message_ban_from && $element->created_at > $element->user->shadow_message_ban_from) {
            return true;
        }
        return false;
    }
}