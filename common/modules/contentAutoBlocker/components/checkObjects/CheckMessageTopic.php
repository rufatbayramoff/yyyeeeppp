<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.10.16
 * Time: 12:11
 */

namespace common\modules\contentAutoBlocker\components\checkObjects;

use common\models\ContentFilterCheckedTopics;
use common\models\MsgTopic;
use common\modules\contentAutoBlocker\components\Statistics;
use frontend\models\community\MessageFacade;
use Yii;

class CheckMessageTopic extends CheckObject
{
    public function getCheckObjectsRelationName()
    {
        return 'contentFilterCheckedTopics';
    }

    public function checkNewElements($enabledFilters)
    {
        /** @var MsgTopic[] $newTopics */
        $newTopics = MsgTopic::find()->leftJoin('content_filter_checked_topics', 'msg_topic.id=content_filter_checked_topics.topic_id')->where(
            'content_filter_checked_topics.topic_id is null'
        )->all();
        foreach ($newTopics as $topic) {
            $processResult = $this->checkIsBanned($topic, $enabledFilters);
            /** @var ContentFilterCheckedTopics $checkTopic */
            $checkTopic = Yii::createObject(ContentFilterCheckedTopics::class);
            $checkTopic->topic_id = $topic->id;
            $checkTopic->isBanned = (integer)$processResult;
            $checkTopic->safeSave();

            if (!$processResult) {
                // Not banned mark as unreaded
                foreach ($topic->msgMembers as $member) {
                    if ($member->user_id != $topic->creator_id) {
                        if(!empty($topic->messages)){ // no need to mark topic unread if it's empty
                            MessageFacade::markTopicAsUnreaded($topic, $member->user);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param MsgTopic $element
     * @param $enabledFilters
     * @return bool
     */
    public function checkIsBanned($element, $enabledFilters)
    {
        if ($element->creator->shadow_message_ban_from && $element->created_at > $element->creator->shadow_message_ban_from) {
            return true;
        }
    }
}