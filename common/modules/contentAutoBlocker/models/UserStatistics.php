<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.10.16
 * Time: 16:43
 */

namespace common\modules\contentAutoBlocker\models;

class UserStatistics
{
    public $trySendBannedMessageCount;
    public $trySendBannedMessageToInterlocutorCount;
    public $tryReceiveBannedMessageCount;
    public $tryReceiveBannedMessageFromInterlocutorCount;
}