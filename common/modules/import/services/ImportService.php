<?php
namespace common\modules\import\services;

use common\components\order\TestOrderFactory;
use common\services\StoreOrderService;

class ImportService
{
    /** @var ThingiverseImportService */
    protected $thingiverseImportService;

    public function injectDependencies(ThingiverseImportService $importService): void
    {
        $this->thingiverseImportService = $importService;
    }
}
