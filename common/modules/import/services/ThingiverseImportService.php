<?php

namespace common\modules\import\services;

use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\exceptions\BusinessException;
use common\components\FileDirHelper;
use common\components\FileTypesHelper;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\factories\FileFactory;
use common\models\Model3d;
use common\models\repositories\Model3dRepository;
use common\models\ThingiverseImport;
use common\services\Model3dService;
use common\services\OrderUploadService;
use frontend\models\model3d\PlaceOrderState;
use PhpParser\Node\Expr\Yield_;
use Yii;
use yii\base\InvalidArgumentException;

class ThingiverseImportService
{

    public const TIMEOUT = 30;

    /** @var  Model3dService */
    protected $model3dService;

    /** @var FileFactory */
    protected $fileFactory;

    /** @var Model3dRepository */
    protected $model3dRepository;

    public function injectDependencies(Model3dService $model3dService, FileFactory $fileFactory, Model3dRepository $model3dRepository)
    {
        $this->model3dService    = $model3dService;
        $this->fileFactory       = $fileFactory;
        $this->model3dRepository = $model3dRepository;
    }

    public function download(string $url, bool $returnFile = false, array $headers = [])
    {
        $parsedUrl = parse_url($url);
        if (empty($parsedUrl['scheme']) || !in_array($parsedUrl['scheme'], ['http', 'https', 'zip'], true)) {
            throw new InvalidArgumentException('Invalid file URL');
        }
        if (empty($parsedUrl['path'])) {
            throw new InvalidArgumentException('File path not found in URL');
        }
        $dir = Yii::getAlias('@storage') . '/thingiverse_import';
        FileDirHelper::createDir($dir);
        if ($returnFile) {
            $filePath = $dir . '/import-' . hash('sha512', $url);
            $fp       = fopen($filePath, 'w+');
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if ($returnFile) {
            curl_setopt($ch, CURLOPT_FILE, $fp);
        }
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT);
        $result   = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($returnFile) {
            return $filePath;
        } else {
            return $result;
        }
    }

    public function importThingDescription($thingId)
    {
        $thingiverseHeaders = [
            'authorization: Bearer 56edfc79ecf25922b98202dd79a291aa',
            'origin: https://www.thingiverse.com',
            'referer: https://www.thingiverse.com/',
            'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36'
        ];
        $url                = 'https://api.thingiverse.com/things/' . $thingId;
        $result             = $this->download(url: $url, headers: $thingiverseHeaders);
        if (!$result) {
            throw new BusinessException('Unable to download thingiverse model: ' . $thingId);
        }
        $thingInfo               = json_decode($result, true);
        $thingInfo['categories'] = [];
        $thingInfo['images']     = [];
        $thingInfo['files']      = [];

        $result = $this->download(url: 'https://api.thingiverse.com/things/' . $thingId . '/categories', headers: $thingiverseHeaders);
        if ($result) {
            $thingInfo['categories'] = json_decode($result, true);
        }
        $result = $this->download(url: 'https://api.thingiverse.com/things/' . $thingId . '/images', headers: $thingiverseHeaders);
        if ($result) {
            $thingInfo['images'] = json_decode($result, true);
            foreach ($thingInfo['images'] as $key => $imageInfo) {
                $bestSize = null;
                foreach ($imageInfo['sizes'] as $size) {
                    $bestSize = $size;
                    if ($size['type'] == 'display' && $size['size'] == 'large') {
                        $bestSize = $size;
                        break;
                    }
                }
                if ($bestSize) {
                    $thingInfo['images'][$key]['filePath'] = $this->download(url: $bestSize['url'], returnFile: true, headers: $thingiverseHeaders);
                }
            }
        }
        $result = $this->download(url: 'https://api.thingiverse.com/things/' . $thingId . '/files', headers: $thingiverseHeaders);
        if ($result) {
            $thingInfo['files'] = json_decode($result, true);
            foreach ($thingInfo['files'] as $key => $fileInfo) {
                $thingInfo['files'][$key]['filePath'] = $this->download(url: $fileInfo['download_url'], returnFile: true, headers: $thingiverseHeaders);
            }
        }
        return $thingInfo;
    }

    public function importThing($thingId)
    {
        $thingiverseImport = ThingiverseImport::find()->where(['thing_id' => $thingId])->one();
        if ($thingiverseImport) {
            $thingInfo = $thingiverseImport->info;
        } else {
            $thingInfo = $this->importThingDescription($thingId);
            if (!$thingInfo['id']) {
                throw new BusinessException('Unable to download thingiverse model: ' . $thingId);
            }
            $thingiverseImport             = new ThingiverseImport();
            $thingiverseImport->thing_id   = $thingId;
            $thingiverseImport->created_at = DateHelper::now();
            $thingiverseImport->info       = $thingInfo;
            $thingiverseImport->safeSave();
        }

        $files = [];
        foreach ($thingInfo['images'] as $thingImageInfo) {
            $pathinfo = pathinfo($thingImageInfo['name']);
            if (empty($pathinfo['extension']) || !in_array(mb_strtolower($pathinfo['extension']), Model3dBaseImgInterface::ALLOWED_FORMATS)) {
                // Skip not image files
                continue;
            }
            $file    = $this->fileFactory->createFileFromPath($thingImageInfo['filePath'], [
                'name'      => $pathinfo['filename'],
                'extension' => $pathinfo['extension'] ?? 'jpg'
            ]);
            $files[] = $file;
        }
        foreach ($thingInfo['files'] as $thingFileInfo) {
            $pathinfo = pathinfo($thingFileInfo['name']);
            if (empty($pathinfo['extension']) || !in_array(mb_strtolower($pathinfo['extension']), Model3dBasePartInterface::ALLOWED_FORMATS)) {
                // Skip not model files
                continue;
            }
            $file    = $this->fileFactory->createFileFromPath($thingFileInfo['filePath'], [
                'name'      => $pathinfo['filename'],
                'extension' => $pathinfo['extension']
            ]);
            $files[] = $file;
        }
        $tagsInfo = ArrayHelper::getColumn($thingInfo['tags'], 'name');

        $model3d  = $this->model3dService->createModel3dByFilesList([
            'source'      => Model3d::SOURCE_THINGIVERSE,
            'title'       => $thingInfo['name'],
            'description' => $thingInfo['description'],
            'tags'        => $tagsInfo
        ], $files);
        $siteTags = $model3d->tags;
        $this->model3dRepository->save($model3d);
        $model3d->setSiteTags($siteTags);
        Model3dService::saveTags($model3d);
        $posUid          = 'MU' . $model3d->id;
        $placeOrderState = Yii::createObject(PlaceOrderState::class);
        $placeOrderState->setStateUid($posUid);
        $placeOrderState->setModel3d($model3d);
        return $model3d;
    }

    public function detectThingIdByUrl($url): ?int
    {
        $parsedUrl = parse_url($url);
        if ($parsedUrl['host'] === 'www.thingiverse.com') {
            if (strpos($parsedUrl['path'], '/thing:') !== 0) {
                throw new InvalidArgumentException('Invalid thingiverse url. Allowed only example: https://www.thingiverse.com/thing:5158955');
            }
            $thindId = (int)substr($parsedUrl['path'], 7, 200);
            return $thindId;
        }
        return null;
    }
}