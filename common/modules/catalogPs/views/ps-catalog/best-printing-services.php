<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * */

use common\models\PsPrinter;

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

?>
<div class="best-3dprint-service__cover">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 m-t30 m-b30">
                <h1 class="text-center">
                    <?= _t('site.bestservice', 'Best 3D Printing Services of the Month'); ?>
                </h1>
            </div>
        </div>
    </div>
</div>
<?php
$listView = Yii::createObject([
    'class' => \yii\widgets\ListView::class,
    'dataProvider' => $dataProvider,
    'itemView' => 'listItem',
    'viewParams' => [
        'needIndex' => true
    ]
]);
?>
<div class="container container--wide">
    <div class="responsive-container-list catalog-listview">
        <?=$listView->renderItems()?>
    </div>
</div>
