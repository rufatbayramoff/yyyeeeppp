<?php

use common\models\PsPrinter;
use common\models\SeoPage;
use common\modules\catalogPs\components\CatalogPsSeoDetector;
use common\modules\catalogPs\models\CatalogSearchForm;
use frontend\assets\DropzoneAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;
use yii\widgets\Menu;

/** @var $seo CatalogPsSeoDetector */
/** @var $seoPage SeoPage */
/** @var $searchForm CatalogSearchForm */
/** @var string $canonicalUrl */
/** @var View $this */

$this->registerAssetBundle(DropzoneAsset::class);

$this->title                      = $seoPage->title;
$this->params['meta_description'] = $seoPage->meta_description;
$this->params['meta_title']       = $seoPage->title;
$this->params['meta_keywords']    = $seoPage->meta_keywords;

$this->registerLinkTag(['rel' => 'canonical', 'href' => $canonicalUrl]);

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->directive('dropzone-button')
    ->controllerParams([
        'z'            => 'y',
        'categories'   => $searchForm->categories(),
        'categorySlug' => '3d-printing-services'
    ])
    ->controller([
        'ps/PsCatalogController',
        'ps/CatalogController',
        'product/productForm',
        'product/productModels',
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service'
    ]);

echo $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-modal.php');

?>

<div class="ps-cat-head">
    <div class="container container--wide">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="h2 ps-cat-head__title"><?= ($seoPage->header); ?></h1>
                <p class="m-b30"><?= ($seoPage->header_text); ?></p>
            </div>
        </div>
    </div>
</div>
<?= $this->render('_searchTopbar.php', ['searchForm' => $searchForm]); ?>

<div class="ps-cat-upload hidden-xs">
    <div class="container container--wide">
        <div class="ps-cat-upload__body">
            <h2 class="ps-cat-upload__text m-r10"><?= _t('front.catalog', 'Upload files to compare instant quotes from local 3D printing services'); ?></h2>
            <a href="/my/print-model3d?utm_source=top_upload" class="btn btn-danger btn-sm"><?= _t('front.catalog', 'Upload files'); ?></a>
            <h2 class="ps-cat-upload__text">
                <?php
                $contactLink = yii\helpers\Html::a("contact us", ['/site/contact'], ['target' => '_blank']);
                echo _t('front.catalog', 'or {contact} directly to get a volume discount.', ['contact' => $contactLink]);
                ?>
            </h2>
        </div>
    </div>
</div>

<div class="container container--wide" ng-controller="PsCatalogController">
    <?php
    $listView = Yii::createObject([
        'class'        => ListView::class,
        'dataProvider' => $dataProvider,
        'itemView'     => 'listItem',
        'viewParams'   => [
            'searchForm' => $searchForm,
        ]
    ]);
    ?>
    <?php if ($listView->dataProvider->getCount() > 0): ?>
        <!-- Items -->
        <div class="responsive-container-list catalog-listview">
            <?= $listView->renderItems() ?>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $listView->renderPager() ?>
            </div>
        </div>
    <?php else: ?>
        <div class="panel box-shadow m-b30" style="display: inline-block">
            <div class="panel-body">
                <?php
                if (app('request')->get('international', false) || $searchForm->text) {
                    echo _t('front.catalog', 'Sorry, no matches were found for that search query. Please try changing the parameters of your search.');
                } else {
                    $internationalUrl = \common\components\UrlHelper::addGetParameter($searchForm->getValidUrl(), 'international', 1);
                    echo _t('front.catalog', 'Sorry, no matches were found for that search query in your location.');
                    echo '<br>';
                    echo '<strong>';
                    echo Html::a(_t('front.catalog', 'Show international services. '), $internationalUrl);
                    echo '</strong>';
                }
                ?>
            </div>
        </div>
    <?php endif; ?>

</div>

<div class="container container--wide">
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <?= ($seoPage->footer_text); ?>
        </div>

    </div>
</div>

<div class="page-link-blocks">
    <div class="container container--wide">
        <div class="row">
            <?php
            $locationLinks = $seo->getLinksTopCities();
            if (!empty($locationLinks)): ?>
                <div class="col-sm-3">
                    <h3 class="h4"><?= _t('front.catalog', 'Printing locations'); ?></h3>
                    <?php
                    echo Menu::widget(['items' => $locationLinks]);
                    ?>
                </div>
            <?php endif; ?>
            <div class="col-sm-3">
                <h3 class="h4"><?= _t('front.catalog', 'Product Application'); ?></h3>
                <?php
                echo Menu::widget(['items' => $seo->getLinksProductTypes()]);
                ?>
            </div>
            <div class="col-sm-3">
                <h3 class="h4"><?= _t('front.catalog', 'Materials'); ?></h3>
                <?php
                echo Menu::widget(['items' => $seo->getLinksMaterials()]);
                ?>
            </div>
            <div class="col-sm-3">
                <h3 class="h4"><?= _t('front.catalog', 'Technology'); ?></h3>
                <?php
                echo Menu::widget(['items' => $seo->getLinksTechnologies()]);
                ?>
            </div>
        </div>
    </div>
</div>