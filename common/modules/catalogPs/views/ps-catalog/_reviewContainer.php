<?php
/** @var $dataProvider \yii\data\ActiveDataProvider */
?>
<div id="reviewList" class="container container--wide">
    <div class="row reviews-cat-list">
        <?php echo \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['tag'=>null],
            'itemView' => '_reviewItem'
        ]);?>
    </div>
</div>