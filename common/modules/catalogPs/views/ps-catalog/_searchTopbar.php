<?php

use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use common\modules\catalogPs\models\CatalogSearchForm;
use kartik\select2\Select2KrajeeAsset;
use kartik\select2\ThemeKrajeeAsset;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use yii\web\View;

/** @var $searchForm CatalogSearchForm */

$hideSort = $hideSort ?? false;
Select2KrajeeAsset::register($this);
ThemeKrajeeAsset::register($this);
Yii::$app->angular
    ->controllerParams([
        'locationString' => CatalogPrintingUrlHelper::getLocationString($searchForm->location),
        'international'  => (int)$searchForm->international,
        'technology'     => $searchForm->technology,
        'usage'          => $searchForm->usage,
        'material'       => $searchForm->material ? (is_string($searchForm->material)?$searchForm->material:implode('_', $searchForm->material)) : '',
        'sort'           => $searchForm->sort,
        'text'           => $searchForm->text
    ]);

?>
<div class="nav-tabs__container" id="catalog" ng-controller="CatalogController">
    <div class="container container--wide">
        <div class="nav-filter nav-filter--many nav-filter--ps-cat">
            <?php $form = ActiveForm::begin(
                [
                    'options' => [
                        'onsubmit' => "return false; ",
                    ],
                ]
            );
            ?>
            <?php if (Yii::$app->mobileDetect->isMobile() && !Yii::$app->mobileDetect->isTablet()): ?>
                <div class="nav-filter__group">
                    <label style="display: inline-block;width: auto; min-width:80px;margin-right: 10px;"><?= _t('site.ps', 'Locaton'); ?></label>
                    <?php
                    echo frontend\widgets\UserLocationWidget::widget(
                        [
                            'location'       => $searchForm->location,
                            'locationFormat' => $searchForm->location->country ? '%city%, %country%' : '%city%',
                            'jsCallback'     => 'function(btn, result, modalId) {if (result) { $("#catalog").trigger("changeFilter", {"location":result.location}); } }'
                        ]
                    );
                    ?>
                </div>


                <div class="nav-filter__group visible-xs p-r0">
                    <?= $form->field($searchForm, 'text', ['options' => ['style' => 'display:flex; align-items:center;']
                        , 'labelOptions'                             => ['style' => 'display: inline-block;width: auto; min-width:80px;margin-right: 10px;']])
                        ->textInput(['class' => 'form-control nav-filter__input input-sm']) ?>
                </div>
            <?php endif; ?>

            <input type="checkbox" id="showhideNavFilter">
            <label class="nav-filter__mobile-label" for="showhideNavFilter">
                <?= _t('site.ps', 'Filters'); ?>
                <span class="tsi tsi-down"></span>
            </label>
            <?php if (!$hideSort): ?>
                <div class="nav-filter__mobile-right-block">
                    <?= $form->field($searchForm, 'sort')
                        ->dropDownList($searchForm->getComboSort()) ?>
                </div>
            <?php endif; ?>
            <div class="nav-filter__mobile-container">

                <div class="nav-filter__group">
                    <div class="form-group field-category">
                        <label for=""><?= _t('site.ps', 'Category') ?></label>
                        <select class="form-control js-category-select input-sm" ng-model="categorySlug" ng-init="initCategories()">
                        </select>
                    </div>
                </div>

                <?php if (!Yii::$app->mobileDetect->isMobile() || Yii::$app->mobileDetect->isTablet()) : ?>
                    <div class="nav-filter__group">
                        <label for=""><?= _t('site.ps', 'Location'); ?></label>
                        <?php
                        echo frontend\widgets\UserLocationWidget::widget(
                            [
                                'location'       => $searchForm->location,
                                'locationFormat' => $searchForm->location->country ? '%city%, %country%' : '%city%',
                                'jsCallback'     => 'function(btn, result, modalId) { if (result) { $("#catalog").trigger("changeFilter", {"location":result.location}); } }'
                            ]
                        );
                        ?>
                    </div>
                <?php endif; ?>
                <div class="nav-filter__group">
                    <?php
                    echo $form->field(
                        $searchForm,
                        'international',
                        [
                            'inputTemplate' => '<label class="checkbox-switch checkbox-switch--xs" for="international">{input}<span class="slider"></span></label>',
                        ]
                    )
                        ->checkbox(
                            [
                                'ng-model'  => 'international',
                                'ng-change' => 'onChangedFilter()'
                            ],
                            false
                        )
                        ->label(_t('site.catalog', 'International'));
                    ?>
                </div>
                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'technology')->widget(
                        Select2::class,
                        [
                            'data'         => array_merge([_t('site.catalogps', 'Any')], $searchForm->getComboTechnologies()),
                            'options'      => [
                                'placeholder' => _t('site.catalogps', 'Any'),
                                'ng-model'    => 'technology',
                                'ng-change'   => 'onChangedFilter()'
                            ],
                            'size'         => kartik\select2\Select2::SMALL,
                            'pluginEvents' => [
                            ]
                        ]
                    ) ?>
                </div>

                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'usage')->widget(
                        Select2::class,
                        [
                            'data'         => array_merge([_t('site.catalogps', 'Any')], $searchForm->getComboUsage()),
                            'options'      => [
                                'placeholder' => _t('site.catalogps', 'Any'),
                                'ng-model'    => 'usage',
                                'ng-change'   => 'onChangedFilter()'
                            ],
                            'size'         => kartik\select2\Select2::SMALL,
                            'pluginEvents' => [
                            ]
                        ]
                    ) ?>
                </div>

                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'material')->widget(
                        Select2::class,
                        [
                            'data'          => $searchForm->getComboMaterials(),
                            'options'       => [
                                'placeholder' => _t('site.catalogps', 'Any'),
                            ],
                            'size'          => kartik\select2\Select2::SMALL,
                            'pluginOptions' => [
                                'multiple' => true,
                                'width'    => 'resolve'
                            ],
                            'pluginEvents'  => [
                                'select2:select'   => 'function(e) { $("#catalog").trigger("changeFilter", {"material": e.params.data}); }',
                                'select2:unselect' => 'function(e) { $("#catalog").trigger("changeFilter", {"material": e.params.data}); }',
                            ]
                        ]
                    ) ?>
                </div>

                <?php if (!$hideSort && !Yii::$app->mobileDetect->isMobile() || Yii::$app->mobileDetect->isTablet()) : ?>
                    <div class="nav-filter__group  hidden-xs">
                        <?= $form->field($searchForm, 'sort')->widget(
                            Select2::class,
                            [
                                'data'          => $searchForm->getComboSort(),
                                'options'       => [
                                    'id'          => 'sort2',
                                    'placeholder' => _t('site.catalogps', 'Any'),
                                    'ng-model'    => 'sort',
                                    'ng-change'   => 'onChangedFilter()'
                                ],
                                'size'          => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ],
                                'pluginEvents'  => [
//                                    'select2:select' => "function() { TsCatalogSearch.changeLocation(this.form); }",
                                ]
                            ]
                        ) ?>
                    </div>
                <?php endif; ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    <?php
    /** @var View $this */
    $this->registerJs("
$(function () {
    
    var changeBubble=$.support.changeBubbles;

    $(\"#text\")
        .on(\"change\", function(e) {
            $.data(this, \"value\", this.value);
        })
        .on(\"keyup\", function(e) {
        if (e.which === 13 && this.value !== $.data(this, \"value\") && !changeBubble) {
            e.preventDefault();
            $(this).trigger('blur');
        }
    })
});
");

    ?>


</script>
