<?php

use common\modules\catalogPs\models\StoreOrderReviewSearchForm;
use kartik\select2\Select2KrajeeAsset;
use kartik\select2\ThemeKrajeeAsset;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;

/** @var $searchForm StoreOrderReviewSearchForm */
Select2KrajeeAsset::register($this);
ThemeKrajeeAsset::register($this);
?>
<div class="nav-tabs__container" ng-controller="ReviewController" ng-init="init()">
    <div class="container container--wide">
        <div class="nav-filter nav-filter--many nav-filter--ps-cat">
            <?php $form = ActiveForm::begin(
                [
                    'layout'      => 'inline',
                    'fieldConfig' => [
                        'template'     => "{label} {input} ",
                        'options'      => [
                            'class' => 'form-group field-material'
                        ],
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
            <div class="nav-filter__group">
                <?php echo $form->field($searchForm, 'category', [
                    'options' => ['id' => 'category-filter']
                ])->widget(
                    Select2::class,
                    [
                        'data'          => array_merge([_t('site.catalogps', 'All')], $searchForm->categories),
                        'options'       => [
                            'placeholder' => _t('site.catalogps', 'All'),
                        ],
                        'size'          => kartik\select2\Select2::SMALL,
                        'pluginOptions' => [
                            'width' => 'resolve'
                        ],
                    ]
                ) ?>
            </div>
            <div class="nav-filter__group">
                <?php echo $form->field($searchForm, 'material', [
                    'options' => ['id' => 'material-filter']
                ])->widget(
                    Select2::class,
                    [
                        'data'          => array_merge([_t('site.catalogps', 'All')], $searchForm->comboMaterials),
                        'options'       => [
                            'placeholder' => _t('site.catalogps', 'All'),
                        ],
                        'size'          => kartik\select2\Select2::SMALL,
                        'pluginOptions' => [
                            'width' => 'resolve'
                        ],
                    ]
                ) ?>
            </div>
            <div class="nav-filter__group">
                <?php echo $form->field($searchForm, 'technology', [
                    'options' => ['id' => 'technology-filter']
                ])->widget(
                    Select2::class,
                    [
                        'data'          => array_merge([_t('site.catalogps', 'All')], $searchForm->comboTechnologies),
                        'options'       => [
                            'placeholder' => _t('site.catalogps', 'All'),
                        ],
                        'size'          => kartik\select2\Select2::SMALL,
                        'pluginOptions' => [
                            'width' => 'resolve'
                        ],
                    ]
                ) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>