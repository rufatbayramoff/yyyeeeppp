<?php
/* @var SeoPage $seo */

use common\models\factories\LocationFactory;
use common\models\Ps;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\modules\catalogPs\repositories\PrintedFileRepository;
use frontend\components\UserSessionFacade;

$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);
$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);

$this->title = $seo && $seo->title ? $seo->title : _t('site.ps', 'Print services');

$listView = Yii::createObject([
    'class'        => \yii\widgets\ListView::class,
    'dataProvider' => $dataProvider,
    'itemView'     => 'listItem',
    'viewParams'   => [
    ]
]);
Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->directive('dropzone-button')
    ->controllerParams([
        'z'            => 'y',
        'categories'   => $searchForm->categories(),
        'categorySlug' => '3d-printing-services'
    ])
    ->controller([
        'ps/PsCatalogController',
        'ps/CatalogController',
        'product/productForm',
        'product/productModels',
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service'
    ]);
?>

<div class="ps-cat-head">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="h2 ps-cat-head__title"><?= $seo && $seo->header ? $seo->header : _t('site.ps', 'Print Services'); ?></h1>
            </div>
        </div>
    </div>
</div>
<?php if (isset($topSearchForm)): ?>
    <?= $this->render('_searchTopbar.php', ['searchForm' => $topSearchForm, 'hideSort' => true]); ?>
<?php endif; ?>
<?php if ($listView->dataProvider->getCount() > 0): ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?= $seo && $seo->header_text ? $seo->header_text : ''; ?>
                <h2 class="promo-page-title m-t30"><?= _t('site.ps', 'Featured 3D Print Services'); ?></h2>
            </div>
        </div>
        <div class="responsive-container-list responsive-container-list--3 catalog-listview">
            <?= $listView->renderItems() ?>
        </div>
    </div>
<?php endif; ?>
<div class="ps-cat-links ps-cat-links--bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="promo-page-title ps-cat-links__title"><?= _t('site.ps', 'Featured Locations'); ?></h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php
            $featuredLocations = $searchForm->getFeaturedLocations();
            $cityChunks        = array_chunk($featuredLocations, count($featuredLocations) / min(3, count($featuredLocations))); ?>
            <?php foreach ($cityChunks as $cityChunk): ?>
                <div class="col-xs-6 col-sm-4">
                    <ul>
                        <?php foreach ($cityChunk as $city): $cityUrl = '';
                            $location = str_replace(', ', '--', trim($city));
                            $location = str_replace(' ', '-', $location);
                            $location = trim($location);
                            $locationObj = LocationFactory::createFromString($location);
                            if (!$locationObj) continue;
                            $locationStr = UserSessionFacade::getLocationAsString($locationObj);
                            ?>
                            <li><a href="<?= \yii\helpers\Url::toRoute([
                                    '/catalogps/ps-catalog/index',
                                    'type'     => '3d-printing-services',
                                    'location' => $locationStr,
                                ]); ?>"><?php echo H($city); ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div class="ps-cat-links">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="promo-page-title ps-cat-links__title"><?= _t('site.ps', 'Top Technologies'); ?></h2>
                <div class="row">
                    <?php
                    /** @var CatalogSearchForm $searchForm */
                    $technologies       = $searchForm->getTechnologies();
                    $technologiesChunks = array_chunk($technologies, ceil(count($technologies) / min(2, count($technologies)))); ?>
                    <?php foreach ($technologiesChunks as $techChunk): ?>
                        <div class="col-xs-6">
                            <ul>
                                <?php foreach ($techChunk as $tech): ?>
                                    <li><a href="<?= \yii\helpers\Url::toRoute([
                                            '/catalogps/ps-catalog/index',
                                            'type'     => '3d-printing-services',
                                            'location' => CatalogSearchForm::LOCATION_EMPTY,
                                            'filter'   => 'technology-' . $tech->getTitleCode()
                                        ]); ?>"><?= H($tech->title); ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <h2 class="promo-page-title ps-cat-links__title"><?= _t('site.ps', 'Featured Materials'); ?></h2>
                <div class="row">
                    <?php
                    $materials = $searchForm->getMaterials();
                    if ($materials) {
                        $materialsChunks = array_chunk($materials, ceil(count($materials) / min(2, count($materials))));
                        foreach ($materialsChunks as $materialsChunk) {
                            ?>
                            <div class="col-xs-6">
                                <ul>
                                    <?php foreach ($materialsChunk as $material): ?>
                                        <li><a href="<?= \yii\helpers\Url::toRoute([
                                                '/catalogps/ps-catalog/index',
                                                'type'     => '3d-printing-services',
                                                'location' => CatalogSearchForm::LOCATION_EMPTY,
                                                'filter'   => 'material-' . $material->getSlugifyFilament()
                                            ]); ?>"><?= H($material->title); ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>


