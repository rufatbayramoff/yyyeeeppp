<?php

use common\models\Model3dReplica;
use common\models\Ps;
use common\models\StoreOrderReview;
use frontend\widgets\SwipeGalleryWidget;

/** @var StoreOrderReview $model */
/** @var StoreOrderReview $review */
/** @var Ps $ps */

$review = $model;
$ps = $review->ps;
/** @var Model3dReplica|null $model3dReplica */

$materials = \common\services\PrinterMaterialService::orderMaterials($review->order);
$model3dReplica = $review->order->getFirstItem() && $review->order->getFirstItem()->hasModel() ? $review->order->getFirstItem()->model3dReplica : null;
if ($model3dReplica && $model3dReplica->isOneTextureForKit()) {
    $color = $model3dReplica->model3dTexture->printerColor;
}
?>
<div class="col-xs-12 col-sm-6 col-md-4 reviews-cat-list__cell">
    <div class="reviews-cat">
        <div class="reviews-cat__service">
            <?php if($ps):?>
                <a href="<?php echo $ps->getPublicCompanyLink()?>" target="_blank" class="reviews-cat__service-data" title="<?php echo H($ps->title)?>">
                    <img src="<?php echo $ps->getCompanyLogoOrDefault()?>"
                         alt="<?php echo H($ps->title)?>" title="<?php echo H($ps->title)?>">
                    <div>
                        <?php echo H($ps->title)?>
                    </div>
                </a>
            <?php endif;?>
            <div class="reviews-cat__materials">
                <?php if($materials):?>
                    <span class="text-muted m-r10"><?= _t('ps.profile', 'Material &amp; Color') ?></span>
                    <strong>
                        <span class="m-r10"><?php echo $materials ?></span>
                        <?php if(!empty($color)):?>
                            <div title="<?php echo $materials;?>" class="material-item">
                                <div class="material-item__color" style="background-color: rgb(<?php echo $color->rgb?>)"></div>
                                <div class="material-item__label"><?php echo H($color->title) ?></div>
                            </div>
                        <?php endif;?>
                    </strong>
                <?php endif;?>
            </div>
        </div>
        <div class="reviews-cat__body" id="user-review-<?php echo $review->id; ?>">
            <div class="reviews-cat__rate">
                <?php echo $this->render('@frontend/views/c/review/rate',['review' => $review])?>
            </div>
            <div class="ps-profile-user-review__data">
                <?php echo SwipeGalleryWidget::widget(
                    [
                        'files'            => $review->reviewFilesSort,
                        'thumbSize'        => [120, 120],
                        'assetsByClass'    => true,
                        'containerOptions' => ['class' => 'ps-profile-user-review__user-models'],
                        'itemOptions'      => ['class' => 'ps-profile-user-review__user-models-pic'],
                        'scrollbarOptions' => ['class' => 'ps-profile-user-review__user-models-scrollbar'],
                        'alt'              => _t('ps.public', 'Review image #{id}')
                    ]
                )
                ?>
            </div>
            <?php echo $this->render('@frontend/views/c/review/comment',['review' => $review])?>
        </div>
        <div class="reviews-cat__user">
            <?php echo $this->render('@frontend/views/c/review/info',['review' => $review])?>
        </div>
    </div>
</div>