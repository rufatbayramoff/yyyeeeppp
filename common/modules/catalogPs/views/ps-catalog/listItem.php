<?php

use common\models\CompanyService;
use common\models\Ps;
use common\models\PsPrinter;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\modules\catalogPs\repositories\PrintedFileRepository;
use common\modules\payment\fee\FeeHelper;
use frontend\models\ps\PsFacade;
use frontend\widgets\PsPrintServiceReviewStarsWidget;
use frontend\widgets\SwipeGalleryWidget;
use lib\money\Money;

/**
 * @var $model CompanyService
 * @var $printer PsPrinter
 * @var $searchForm CatalogSearchForm
 * @var $printedFilesRepo PrintedFileRepository
 * @var $index int
 */
$printer = $model->getPsPrinter();
$ps      = $printer->company;

$materials        = [];
$materialsDisplay = [];
if ($printer) {
    $certLevelLabel     = $ps->printersCertificationData();
    $moreMaterialsCount = 0;
    foreach ($printer->materials as $material) {
        if (count($materialsDisplay) < 3) {
            $materialsDisplay[] = $material->material->title;
        } else {
            $moreMaterialsCount++;
        }
    }
}
$getShippingPrice = function (PsPrinter $printer) {
    $shippingPrice = _t('ps.shipping', 'Standard Flat Rate');
    foreach ($printer->psMachineDeliveries as $psDelivery) {
        $price = $psDelivery->carrier_price
            ? displayAsCurrency($psDelivery->carrier_price, $printer->companyService->getCurrency()) : '';
        if (!empty($price) && $psDelivery->carrier != 'treatstock') {
            $shippingPrice = $price;
            break;
        }
    }
    return $shippingPrice;
};

$psLink    = PsFacade::getPsLink($ps);
$psPicture = Ps::getCircleImageByPs($ps);
?>
<div class="responsive-container">
    <?php if (isset($needIndex) && $needIndex === true): ?>
        <div class="best-3dprint-service__number">
            <?php echo($index + 1); ?>
        </div>
    <?php endif; ?>
    <div class="designer-card designer-card--ps-cat">
        <div class="designer-card__ps-rating">
            <?php
            $starsWidget = PsPrintServiceReviewStarsWidget::widget([
                'ps' => $ps
            ]);
            if ($starsWidget) {
                echo $starsWidget;
            } else {
                if ($ps->allowShowBeFirstCommentator()) {
                    echo "<span class='text-muted'>" . _t('site.catalog', 'Be the first to leave a review') . "</span>";
                }
            }
            ?>
        </div>

        <?php if ($printer && $certLevelLabel): ?>
            <div class="cert-label <?= $certLevelLabel['label']; ?>" data-toggle="tooltip" data-placement="bottom"
                 title=""
                 data-original-title="<?= $certLevelLabel['description']; ?>">
                <?= $certLevelLabel['labelTxt']; ?>
            </div>
        <?php endif; ?>

        <div class="designer-card__userinfo">
            <a class="designer-card__avatar" href="<?= $psLink; ?>" target="_blank"><img src="<?= $psPicture; ?>"
                                                                                         alt="<?= H($ps->title); ?>"
                                                                                         title="<?= H($ps->title); ?>"/></a>
            <h3 class="designer-card__username">
                <a href="<?= $psLink; ?>" target="_blank" title="<?= H($ps->title); ?>">
                    <?= H($ps->title); ?>
                </a>
            </h3>
            <?php /*
            Hide block
            <div class="designer-card__ps-visit">
                <?php
                $onlineStatus = \frontend\models\user\UserFacade::getUserLastOnlineByDate($printer->lastonline_at);
                echo $onlineStatus;
                ?>
            </div>
            */ ?>
        </div>

        <div class="designer-card__ps-pics">
            <?php
            echo SwipeGalleryWidget::widget([
                'files'            => $ps->gallery(),
                'maxCount'         => 3,
                'thumbSize'        => [160, 90],
                'assetsByClass'    => true,
                'containerOptions' => ['class' => 'designer-card__ps-portfolio'],
                'itemOptions'      => ['class' => 'designer-card__ps-portfolio-item'],
                'scrollbarOptions' => ['class' => 'designer-card__ps-portfolio-scrollbar'],
                'emptyOptions'     => ['class' => 'designer-card__ps-pics-empty'],
                'emptyText'        => _t('site.catalog', 'Images not uploaded'),
                'alt'              => H($ps->title) . _t('site.catalog', ' 3D printing photo'),
                'title'            => H($ps->title) . _t('site.catalog', ' 3D printing photo')
            ]);
            ?>
        </div>

        <?php if ($printer):
            $printerLocation = ($printer->companyService->location->city->title ? $printer->companyService->location->city->title . ', ' : '') .
                ($printer->companyService->location->region && $printer->companyService->location->region->title ? $printer->companyService->location->region->title . ', ' : '') .
                ($printer->companyService->location->country->iso_code);
            ?>
            <div class="designer-card__ps-loc" title="<?= $printerLocation; ?>">
                <span class="tsi tsi-map-marker"></span>
                <?= $printerLocation; ?>
            </div>
        <?php else: ?>

        <?php endif; ?>

        <div class="designer-card__about">
            <?= H(yii\helpers\StringHelper::truncateWords($ps->description, 14)); ?>
            <?php if (!empty($ps->description) && strlen($ps->description) > 50): ?>
                <a class="ts-ga" data-categoryga="PublicPs" data-actionga="ShowMoreLink"
                   href="<?= $psLink; ?>"><?= _t('site.catalog', 'read more'); ?></a>
            <?php endif; ?>
        </div>

        <div class="row" style="min-height: 75px">
            <div class="col-sm-12">
                <div class="designer-card__data m-b10">
                    <h4 class="m-b0 m-t0"><?php echo _t('site.catalog', 'Manufacturing Service') ?></h4>
                    <?php foreach ($ps->serviceCategories() as $categoryTitle): ?>
                        <span class="badge designer-card__data-badge"><?php echo H($categoryTitle) ?></span>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Materials:'); ?></span>
                    <?= H(implode(', ', $materialsDisplay)); ?>
                    <?php if ($moreMaterialsCount > 0):
                        echo _t('public.ps', '+{n, plural, =1{1 material} other{# materials}}', ['n' => $moreMaterialsCount]);
                    endif; ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="designer-card__data">
                    <?php
                    $minOrderPrice = $printer->getMinPrintPrice();
                    if ($minOrderPrice->getAmount()): ?>
                        <span class="designer-card__data-label"><?= _t('site.ps', 'Minimum Charge:'); ?></span>
                        <?= displayAsMoney($minOrderPrice); ?>
                    <?php endif; ?>
                </div>
                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Shipping:'); ?></span>
                    <?= $getShippingPrice($printer); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="designer-card__btn-block">
                    <a class="btn btn-danger btn-sm"
                       href="/my/print-model3d?utm_source=public_ps&posUid=fixedPs&psId=<?= $ps->id ?>"><?= _t('site.ps', 'Instant order'); ?>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>