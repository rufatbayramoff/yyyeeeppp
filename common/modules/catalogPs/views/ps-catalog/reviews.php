<?php
/** @var $form StoreOrderReviewSearchForm */
/** @var $dataProvider \yii\data\ActiveDataProvider */

/** @var $seo \common\models\SeoPage */

use common\modules\catalogPs\models\StoreOrderReviewSearchForm;

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->controllerParams([
        'category'   => $form->category,
        'material'   => $form->material,
        'technology' => $form->technology,
    ])
    ->controller([
        'ps/ReviewController',
    ]);

$this->title = $seo->title;
$header = $seo->header;
$headerText = $seo->header_text;
$description = $seo->meta_description ?? '';

?>
<div class="over-nav-tabs-header">
    <div class="container container--wide">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="animated m-b20">
                    <?= _t('site.page', 'Reviews'); ?>
                </h1>
                <?= $headerText; ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->render('_reviewSearchTopBar', ['searchForm' => $form]) ?>
<?php echo $this->render('_reviewContainer', ['dataProvider' => $dataProvider]) ?>
