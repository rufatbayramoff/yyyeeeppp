<?php

use common\models\Ps;
use frontend\models\ps\PsFacade;
use frontend\widgets\SwipeGalleryWidget;
use yii\helpers\Url;

/**
 * @var $printedFilesRepo \common\modules\catalogPs\repositories\PrintedFileRepository
 * @var $printer \common\modules\catalogPs\models\PsPrinterEntity
 * @var $searchForm \common\modules\catalogPs\models\CatalogSearchForm
 */
$printer = $model;
/** @var $ps Ps */
$ps = $printer->company;


$materials        = [];
$materialsDisplay = [];
if ($printer) {
    $certLevel = $printer->companyService->getCertificationLabel();

    $materials          = $searchForm->material ? $printer->getMaterials() : $printer->getAllPsMaterials();
    $moreMaterialsCount = 0;
    foreach ($materials as $material) {
        if (count($materialsDisplay) < 3) {
            $materialsDisplay[] = $material->material->title;
        } else {
            $moreMaterialsCount++;
        }
    }
}
$getShippingPrice = function (\common\modules\catalogPs\models\PsPrinterEntity $printer) {
    $shippingPrice   = _t('ps.shipping', 'Standard Flat Rate');
    $printerDelivery = $printer->getDeliveryTypes();
    foreach ($printerDelivery as $psDelivery) {
        $price = $psDelivery->carrier_price
            ? displayAsCurrency($psDelivery->carrier_price, $printer->getCompanyService()->getCurrency()) : '';
        if (!empty($price) && $psDelivery->carrier != 'treatstock') {
            $shippingPrice = $price;
            break;
        }
    }
    return $shippingPrice;
};

$psLink    = Url::toRoute(array_merge(['/c/public/profile', 'username' => $ps->url]));
$psPicture = Ps::getCircleImageByPs($ps);
?>
<div class="responsive-container">
    <div class="designer-card designer-card--ps-cat">
        <div class="designer-card__ps-rating">
            <?php if (intval($printer->ps_rating_count) > 0): ?>

                <?php
                echo \frontend\widgets\PsPrintServiceReviewStarsWidget::widget([
                    'ps'              => $ps,
                    'reviewsCount'    => (int)$printer->ps_rating_count,
                    'fromReviewCount' => 0,
                    'reviewsRating'   => (float)$printer->ps_rating
                ]);
            else:
                if ($ps->allowShowBeFirstCommentator()) {
                    echo "<span class='text-muted'>" . _t('site.catalog', 'Be the first to leave a review') . "</span>";
                }
            endif;
            ?>
        </div>

        <?php if ($printer): ?>
            <div class="cert-label" data-toggle="tooltip" data-placement="bottom" title="">
                <?= $certLevel; ?>
            </div>
        <?php endif; ?>

        <div class="designer-card__userinfo">
            <a class="designer-card__avatar" href="<?= $psLink; ?>" target="_blank"><img src="<?= $psPicture; ?>"
                                                                                         alt="<?= H($ps->title); ?>"
                                                                                         title="<?= H($ps->title); ?>"/></a>
            <h3 class="designer-card__username">
                <a href="<?= $psLink; ?>" target="_blank" title="<?= H($ps->title); ?>">
                    <?= H($ps->title); ?>
                </a>
            </h3>
            <?php /*
            Hide block
            <div class="designer-card__ps-visit">
                <?php
                $onlineStatus = \frontend\models\user\UserFacade::getUserLastOnlineByDate($printer->lastonline_at);
                echo $onlineStatus;
                ?>
            </div>
            */ ?>
        </div>
        <div class="designer-card__ps-pics">
            <?php
            $picturesPs      = $ps->getPicturesFiles(4);
            $picturesPrinted = (array)$printedFilesRepo->getById($printer->ps_id);
            $pictures        = array_merge($picturesPs, $picturesPrinted);

            echo SwipeGalleryWidget::widget([
                'files'            => $pictures,
                'maxCount'         => 3,
                'thumbSize'        => [160, 90],
                'assetsByClass'    => true,
                'containerOptions' => ['class' => 'designer-card__ps-portfolio'],
                'itemOptions'      => ['class' => 'designer-card__ps-portfolio-item'],
                'scrollbarOptions' => ['class' => 'designer-card__ps-portfolio-scrollbar'],
                'emptyOptions'     => ['class' => 'designer-card__ps-pics-empty'],
                'emptyText'        => _t('site.catalog', 'Images not uploaded')
            ]);
            ?>
        </div>
        <?php if ($printer):
            $printerLocation = sprintf("%s, %s, %s", $printer->city, $printer->region, $printer->country_iso);
            ?>
            <div class="designer-card__ps-loc" title="<?= $printerLocation; ?>">
                <span class="tsi tsi-map-marker"></span>
                <?= $printerLocation; ?>
            </div>
        <?php else: ?>
        <?php endif; ?>

        <div class="designer-card__about">
            <?= yii\helpers\StringHelper::truncateWords($ps->description, 14); ?>
            <?php if (!empty($ps->description) && strlen($ps->description) > 50): ?>
                <a class="ts-ga" data-categoryga="PublicPs" data-actionga="ShowMoreLink"
                   href="<?= $psLink; ?>"><?= _t('site.catalog', 'read more'); ?></a>
            <?php endif; ?>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="designer-card__data">
                    <?php if ((float)$printer->min_order_price > 0): ?>
                        <span class="designer-card__data-label"><?= _t('site.ps', 'Minimum Charge:'); ?></span>
                        <?= displayAsCurrency($printer->min_order_price, $printer->companyService->company->currency); ?>
                    <?php endif; ?>
                </div>
                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Shipping:'); ?></span>
                    <?= $getShippingPrice($printer); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="designer-card__btn-block">

                    <button class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicPs" data-actionga="GetAQuote"
                            loader-click="openCreatePreorder(<?= $model->ps_id ?>, '', '', <?= $model->id ?>)">
                        <?= _t('site.ps', 'Get a Quote'); ?>
                    </button>

                </div>
            </div>
        </div>

    </div>
</div>