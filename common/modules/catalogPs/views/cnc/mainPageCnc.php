<?php
/* @var SeoPage $seo */

use common\modules\catalogPs\repositories\PrintedFileRepository;

$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);
$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);

$this->registerAssetBundle(\frontend\assets\DropzoneAsset::class);


Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->directive('dropzone-button')
    ->controllerParams(['z' => 'y'])
    ->controller([
        'ps/PsCatalogController',
        'product/productForm',
        'product/productModels',
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service'
    ]);

echo $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-modal.php');



$this->title = $seo && $seo->title?$seo->title:_t('site.ps','CNC Services');

$listView = Yii::createObject([
    'class' => \yii\widgets\ListView::class,
    'dataProvider' => $dataProvider,
    'itemView' => 'listItem',
    'viewParams' => [
        'searchForm' => $searchForm,
        'printedFilesRepo' => new PrintedFileRepository($dataProvider->getModels()),
    ]
]);
?>

<div class="ps-cat-head">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="h2 ps-cat-head__title"><?=($seo&&$seo->header?$seo->header:_t('site.ps', 'CNC Services')); ?></h1>
                <p class="m-b30"><?=($seo?$seo->header_text:'');?></p>
            </div>
        </div>
    </div>
</div>
<?php if($listView->dataProvider->getCount() > 0):?>
<div class="container" ng-controller="PsCatalogController">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="promo-page-title m-t30"><?= _t('site.ps', 'CNC Services'); ?></h2>
        </div>
    </div>
    <div class="responsive-container-list responsive-container-list--3 catalog-listview">
    <?=$listView->renderItems()?>
    </div>
</div>
<?php endif; ?>


    <script>
        <?php $this->beginBlock('js1', false); ?>

        //Hide header nav
        //$('.header-bar__findps').addClass('header-bar__findps--short');

        //Init slider for PS portfolio
        var swiperPromo = new Swiper('.designer-card__ps-portfolio', {
            scrollbar: '.designer-card__ps-portfolio-scrollbar',
            scrollbarHide: true,
            slidesPerView: 'auto',
            grabCursor: true
        });

        <?php $this->endBlock(); ?>
    </script>

<?php $this->registerJs($this->blocks['js1']); ?>