<?php

use common\modules\catalogPs\models\CatalogForm;
use kartik\select2\Select2Asset;
use kartik\select2\ThemeKrajeeAsset;
use yii\bootstrap\ActiveForm;

/** @var $searchForm CatalogForm */

$hideSort = $hideSort ?? false;
Select2Asset::register($this);
ThemeKrajeeAsset::register($this);
?>
<div class="nav-tabs__container" ng-controller="CatalogController">
    <div class="container container--wide">
        <div class="nav-filter nav-filter--many nav-filter--ps-cat">
            <?php $form = ActiveForm::begin(
                [
                    'layout'      => 'inline',
                    'options'     => [
                        'onsubmit' => "return false; ",
                    ],
                    'fieldConfig' => [
                        'template'     => "{label} {input} ",
                        'inputOptions' => [
                            'class' => 'form-control input-sm nav-filter__input'
                        ],
                        'labelOptions' => [
                            'class' => '',
                        ],
                    ],
                ]
            );
            ?>
            <input type="checkbox" id="showhideNavFilter">
            <label class="nav-filter__mobile-label" for="showhideNavFilter">
                <?= _t('site.ps', 'Filters'); ?>
                <span class="tsi tsi-down"></span>
            </label>
            <div class="nav-filter__mobile-container">

                <div class="nav-filter__group">
                    <div class="form-group field-category">
                        <label for=""><?= _t('site.ps', 'Category'); ?></label>
                        <select class="form-control js-category-select input-sm" ng-model="categorySlug" ng-init="initCategories()">
                        </select>
                    </div>
                </div>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
