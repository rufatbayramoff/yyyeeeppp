<?php
/** @var $searchForm \common\modules\catalogPs\models\CatalogForm */
if ($searchForm->uploadedFiles) {
    ?>
    <div class="ps-cat-uploaded-files">
        <div class="container container--wide">
            <div class="row">
                <div class="col-sm-6">
                    <label>
                        <?= _t('site.common', 'Uploaded files') ?>
                    </label>
                    <div class="m-b10">
                        <?php
                        foreach ($searchForm->uploadedFiles as $file) {
                            ?>
                            <div class="simple-file-link" title="<?= H($file->getFileName()); ?>">
                                <div class="simple-file-link__link">
                                    <span><?= H($file->getFileName()); ?></span>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <?php
                    if ($searchForm->orderDescription) {
                        ?>
                        <label>
                            <?= _t('site.common', 'Project Description') ?>
                        </label>
                        <div class="ps-cat-uploaded-files__descr-text">
                            <?= $searchForm->orderDescription ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-sm-3">
                    <?php
                    if ($searchForm->productCategory) {
                        ?>
                        <label>
                            <?= _t('site.common', 'Product category') ?>
                        </label>
                        <div class="ps-cat-uploaded-files__descr-text">
                            <?= H($searchForm->productCategory->title) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}

?>

