<?php

use common\models\PsPrinter;
use common\models\SeoPage;
use common\modules\catalogPs\components\CatalogPsSeoDetector;
use common\modules\catalogPs\models\CatalogForm;
use frontend\assets\DropzoneAsset;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;

/** @var $seo CatalogPsSeoDetector */
/** @var $seoPage SeoPage */
/** @var $form CatalogForm */
/** @var string $canonicalUrl */
/** @var View $this */
/** @var ActiveDataProvider $dataProvider */
/** @var $searchForm CatalogForm */

$this->registerAssetBundle(DropzoneAsset::class);

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->directive('dropzone-button')
    ->controllerParams([
        'uploadedFilesUuids' => $form->uploadedFilesUuids,
        'description'        => $form->orderDescription,
        'categories'         => $form->categories(),
        'categorySlug'       => $form->category->slug ?? CatalogForm::DEFAULT_CATEGORY
    ])
    ->controller([
        'ps/PsCatalogController',
        'ps/CatalogController',
        'product/productForm',
        'product/productModels',
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service'
    ]);

echo $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-modal.php');
if ($this->context->seo) {
    $this->title = $this->context->seo->title;
    $header      = $this->context->seo->header;
    $description = $this->context->seo->header_text ?? '';
}
$this->registerLinkTag(['rel' => 'canonical', 'href' => \yii\helpers\Url::canonical()]);

?>
<div class="ps-cat-head">
    <div class="container container--wide">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="h2 ps-cat-head__title"><?php echo isset($header) ? $header : '' ?></h1>
                <p class="m-b30"><?php echo isset($description) ? $description : '' ?></h1></p>
            </div>
        </div>
    </div>
</div>
<?= $this->render('uploadedFiles', ['searchForm' => $form]) ?>
<?php
if (!$form->uploadedFiles) {
    echo $this->render('_searchTopbar.php', ['searchForm' => $form]);
    ?>
    <div class="ps-cat-upload hidden-xs">
        <div class="container container--wide">

            <?php if ($form->isCutting()): ?>
                <div class="ps-cat-upload__body">
                    <h2 class="ps-cat-upload__text m-r10"><?= _t('front.catalog', 'Upload files to compare instant quotes from local Cutting services'); ?></h2>
                    <a href="/my/order-laser-cutting/upload-file?utm_source=top_upload" class="btn btn-danger btn-sm"><?= _t('front.catalog', 'Upload files'); ?></a>
                    <h2 class="ps-cat-upload__text">
                        <?php
                        $contactLink = yii\helpers\Html::a("contact us", ['/site/contact'], ['target' => '_blank']);
                        echo _t('front.catalog', 'or {contact} directly to get a volume discount.', ['contact' => $contactLink]);
                        ?>
                    </h2>
                </div>
            <?php endif; ?>

        </div>
    </div>
    <?php
}
?>

<div class="container container--wide" ng-controller="PsCatalogController">
    <?php
    $listView = Yii::createObject([
        'class'        => ListView::class,
        'dataProvider' => $dataProvider,
        'itemView'     => '_listItem',
        'viewParams'   => [
            'form' => $form
        ]
    ]);
    ?>
    <?php if ($dataProvider->getCount() > 0): ?>
        <!-- Items -->
        <div class="responsive-container-list catalog-listview">
            <?= $listView->renderItems() ?>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $listView->renderPager() ?>
            </div>
        </div>
    <?php else: ?>
        <div class="panel box-shadow m-b30" style="display: inline-block">
            <div class="panel-body">
                <?php
                   echo _t('front.catalog', 'Sorry, no matches were found for that search query. Please try changing the parameters of your search.');
                ?>
            </div>
        </div>
    <?php endif; ?>

</div>
