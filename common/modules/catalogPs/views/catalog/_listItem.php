<?php
/** @var \common\models\Company $model */

/** @var \common\modules\catalogPs\models\CatalogForm $form */

use common\models\Ps;
use frontend\models\ps\PsFacade;
use frontend\widgets\PsPrintServiceReviewStarsWidget;
use frontend\widgets\SwipeGalleryWidget;
use yii\bootstrap\Html;

$ps       = $model;
$canOrder = $form->canInstantOrder() && $ps->isCanCuttingInStore() && !$form->isCncQuote();

$psLink    = PsFacade::getPsLink($ps);
$psPicture = Ps::getCircleImageByPs($ps);
?>
<div class="responsive-container">
    <div class="designer-card designer-card--ps-cat">
        <div class="designer-card__ps-rating">
            <?php
            $starsWidget = PsPrintServiceReviewStarsWidget::widget([
                'ps' => $ps
            ]);
            if ($starsWidget) {
                echo $starsWidget;
            } else {
                if ($ps->allowShowBeFirstCommentator()) {
                    echo "<span class='text-muted'>" . _t('site.catalog', 'Be the first to leave a review') . "</span>";
                }
            }
            ?>
        </div>

        <?php if ($certLevel = $ps->printersCertificationData()): ?>
            <div class="cert-label <?php echo H($certLevel['label']); ?>" data-toggle="tooltip" data-placement="bottom"
                 title=""
                 data-original-title="<?php echo H($certLevel['description']) ?>">
                <?php echo $certLevel['labelTxt'] ?>
            </div>
        <?php endif; ?>

        <div class="designer-card__userinfo">
            <a class="designer-card__avatar" href="<?= $psLink; ?>" target="_blank"><img src="<?= $psPicture ?>"
                                                                                         alt="<?= H($ps->title) ?>"
                                                                                         title="<?= H($ps->title) ?>"/></a>
            <h3 class="designer-card__username">
                <a href="<?= $psLink ?>" target="_blank" title="<?= H($ps->title) ?>">
                    <?= H($ps->title) ?>
                </a>
            </h3>
        </div>

        <div class="designer-card__ps-pics">
            <?php
            echo SwipeGalleryWidget::widget([
                'files'            => $ps->gallery(),
                'maxCount'         => 3,
                'thumbSize'        => [160, 90],
                'assetsByClass'    => true,
                'containerOptions' => ['class' => 'designer-card__ps-portfolio'],
                'itemOptions'      => ['class' => 'designer-card__ps-portfolio-item'],
                'scrollbarOptions' => ['class' => 'designer-card__ps-portfolio-scrollbar'],
                'emptyOptions'     => ['class' => 'designer-card__ps-pics-empty'],
                'emptyText'        => _t('site.catalog', 'Images not uploaded'),
                'alt'              => H($ps->title) . _t('site.catalog', ' 3D printing photo'),
                'title'            => H($ps->title) . _t('site.catalog', ' 3D printing photo')
            ]);
            ?>
        </div>

        <?php if ($location = $ps->fullLocation()): ?>
            <div class="designer-card__ps-loc" title="<?php echo H($location); ?>">
                <span class="tsi tsi-map-marker"></span>
                <?php echo H($location); ?>
            </div>
        <?php endif; ?>

        <div class="designer-card__about">
            <?php echo H(yii\helpers\StringHelper::truncateWords($ps->description, 14)); ?>
            <?php if (!empty($ps->description) && strlen($ps->description) > 50): ?>
                <a class="ts-ga" data-categoryga="PublicPs" data-actionga="ShowMoreLink"
                   href="<?php echo $psLink ?>"><?= _t('site.catalog', 'read more'); ?></a>
            <?php endif; ?>
        </div>

        <div class="row" style="min-height: 75px">
            <?php
            if ($ps->serviceCategories()) {
                ?>
                <div class="col-sm-12">
                    <div class="designer-card__data">
                        <h4 class="m-b0 m-t0"><?php echo _t('site.catalog', 'Manufacturing Service') ?></h4>
                        <?php foreach ($ps->serviceCategories() as $categoryTitle): ?>
                            <span class="badge designer-card__data-badge"><?php echo H($categoryTitle); ?></span>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <?php $materials = $ps->materials(); ?>
                    <?php if (!empty($materials)): ?>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Materials:') ?></span>
                            <?php echo H(implode(', ', array_slice($materials, 0, 3))) ?>
                            <?php if (count($materials) > 3):
                                echo _t('public.ps', '+{n, plural, =1{1 material} other{# materials}}', ['n' => count($materials)]);
                            endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php
            }
            if ($ps->siteTags) {
                ?>
                <div class="col-sm-12">
                    <div class="designer-card__data">
                        <h4 class="m-b0 m-t0"><?php echo _t('site.catalog', 'Tags') ?></h4>
                        <?php foreach ($ps->siteTags as $tag): ?>
                            <span class="badge designer-card__data-badge"><?php echo H($tag->text); ?></span>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="col-sm-12">
                <?php $minimumCharge = $ps->minimumCharge() ?>
                <?php if ($minimumCharge && $canOrder): ?>
                    <div class="designer-card__data">
                        <span class="designer-card__data-label"><?= _t('site.ps', 'Minimum Charge:') ?></span>
                        <?php echo $minimumCharge; ?>
                    </div>
                <?php endif; ?>
                <?php if ($canOrder): ?>
                    <div class="designer-card__data">
                        <span class="designer-card__data-label"><?= _t('site.ps', 'Shipping:') ?></span>
                        <?php echo $ps->minimumShippingPrice() ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="designer-card__btn-block">
                    <?php if ($canOrder): ?>
                        <?php echo Html::a(_t('site.ps', 'Instant order'), ['/my/order-laser-cutting/upload-file', 'utm_source' => 'from_catalog', 'psId' => $ps->id, 'posUid' => 'fixedPs'], ['class' => 'btn btn-danger btn-sm']) ?>
                    <?php endif; ?>
                    <?php if ($form->isCncQuote()): ?>
                        <?php echo Html::a(
                            _t('site.ps', 'Get a Quote'),
                            '/c/' . $ps->url . '/cnc',
                            [
                                'class' => 'btn btn-ghost btn-danger btn-sm ts-ga',
                                'data'  => [
                                    'actionga'   => 'GetAQuote',
                                    'categoryga' => 'PublicPs'
                                ]
                            ]
                        ) ?>
                    <?php else: ?>
                        <button class="btn btn-ghost btn-danger btn-sm ts-ga" data-categoryga="PublicPs"
                                data-actionga="GetAQuote" loader-click="openCreatePreorder(<?= $ps->id ?>)">
                            <?php echo _t('site.ps', 'Get a Quote'); ?>
                        </button>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
</div>
