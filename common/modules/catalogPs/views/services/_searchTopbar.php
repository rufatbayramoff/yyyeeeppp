<?php

use common\models\CompanyServiceCategory;
use common\models\EquipmentCategory;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use frontend\components\UserSessionFacade;
use yii\bootstrap\ActiveForm;

/** @var $searchForm \common\modules\equipments\models\CatalogSearchForm */

$hideSort = $hideSort ?? false;

$categories = CompanyServiceCategory::find()->withoutRoot()->orderBy('lft')->all();

function printOption($model, $data)
{
    if (!$model) {
        $model = new CompanyServiceCategory();
    }
    $result = $model->getCategoryTree($data);
    foreach ($result as $k => $equipmentCategory) {
        $url = '/company-services/'.HL($equipmentCategory['url']);
        if ($equipmentCategory['url'] ==='3d-printing') {
            $url = CatalogPrintingUrlHelper::printing3dCatalog(UserSessionFacade::getLocation());
        }

        echo sprintf(
            "<option value='%s' %s>%s</option>",
            $url,
            $equipmentCategory['selected'],
            $equipmentCategory['space'] . $equipmentCategory['title']
        );
    }
}

?>
<div class="nav-tabs__container">
    <div class="container container--wide">
        <div class="nav-filter nav-filter--many1">
            <?php $form = ActiveForm::begin(
                [
                    'method'      => 'get',
                    'layout'      => 'inline',
                    'options'     => [
                        'onSubmit' => new \yii\web\JsExpression('TsServiceSearch.changeForm(this);return false;'),
                    ],
                    'fieldConfig' => [
                        'template'     => "{label} {input} ",
                        'labelOptions' => [
                            'class' => '',
                        ],
                        'inputOptions' => [
                            'class'    => 'form-control input-sm nav-filter__input nav-filter__input--wide',
                            'onchange' => new \yii\web\JsExpression('TsServiceSearch.changeForm(this.form)')
                        ],
                    ],
                ]
            );
            ?>

            <input type="checkbox" id="showhideNavFilter">
            <label class="nav-filter__mobile-label" for="showhideNavFilter">
                <?= _t('site.ps', 'Filters'); ?>
                <span class="tsi tsi-down"></span>
            </label>

            <div class="nav-filter__mobile-container">
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Category'); ?></label>
                    <select class="form-control input-sm nav-filter__input nav-filter__input--wide" name="category"
                            onchange="TsServiceSearch.changeCategory(this.value)">
                        <option value="/company-services/">All</option>
                        <?= printOption($searchForm->getCategory(), $categories); ?>
                    </select>
                </div>
                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'search')->textInput(); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    TsServiceSearch = {
        formFilterElements: [],
        changeForm: function (form) {
            // get form elements data
            var inputs = form.getElementsByTagName("select");
            var formData = {};
            var sort = '';
            for (var i = 0; i < inputs.length; i++) {
                if (this.formFilterElements.indexOf(inputs[i].name) >= 0) {
                    if (inputs[i].value != '')
                        formData[inputs[i].name] = inputs[i].value;
                }
                if (inputs[i].name == 'sort') {
                    sort = inputs[i].value;
                }
            }
            serialize = function (obj) {
                var str = [];
                for (var p in obj)
                    if (obj.hasOwnProperty(p)) {
                        if (obj[p])
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    }
                return str.join("&");
            }
            var searchVal = document.getElementById('search').value;
            query = serialize({sort: sort, search: searchVal});
            if (query) query = '?' + query;
            var filter = this.convertToFilterUrl(formData);
            var url = this.updateUrlFilter(filter) + query;

            window.location.href = url;
        },

        convertToFilterUrl: function (formData) {
            var result = [];
            for (var i in formData) {
                result.push([i, formData[i]].join('-'));
            }
            return result.join('--');
        },

        updateUrlFilter: function (filter) {
            var newHref = window.location.href;
            newHref = newHref.split('?');
            newHref = newHref[0];
            newHref = newHref.split('/');
            newHref[5] = filter;
            return newHref.join('/');
        },

        changeCategory: function (newCatergoryPath) {
            var currentUrl = window.location.href;
            var currentUrlFilter = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
            window.location.href = newCatergoryPath + currentUrlFilter;
        }
    };

</script>
