<?php
/**
 * User: nabi
 */


/* @var $model \common\models\CompanyService */
?>
<div class="responsive-container" itemscope itemtype="http://schema.org/Service">
    <div class="designer-card designer-card--service-card"><?php if($model->category_id): ?>
            <span class="designer-card__tech-label label label-info" itemprop="serviceType"><?=H($model->category->title);?></span>
        <?php endif; ?>

        <div class="designer-card__userinfo">
            <a class="designer-card__avatar" href="<?=$model->getPublicServiceUrl();?>">
                <img src="<?=$model->getLogoImage();?>" />
            </a>
            <div class="designer-card__username" itemprop="name">
                <a href="<?=$model->getPublicServiceUrl();?>"><?=H($model->getTitleLabel());?></a>
            </div>
            <div class="designer-card__author"><a href="/c/<?=HL($model->ps->url);?>"><?=H($model->ps->title); ?></a></div>
        </div>

        <div class="designer-card__models-list">
            <?php
            $images = $model->getImages(100, true);
            ?>
            <?=
            \frontend\widgets\SwipeGalleryWidget::widget([
                'files' => $images,
                'assetsByClass' => true,
                'thumbSize' => [320, 180],
                'containerOptions' => ['class' => 'ps-pub-portfolio__slider'],
                'itemOptions' => ['class' => 'ps-pub-portfolio__item', 'itemprop' => 'image'],
                'scrollbarOptions' => ['class' => 'ps-pub-portfolio__scrollbar'],
                'emptyOptions' => ['class' => 'designer-card__ps-pics-empty'],
                'emptyText' => _t('site.catalog', 'Images not uploaded')
            ]);
            ?>
        </div>
        <?php
        $company = $model->ps;
        if ($company->location):

            $printerLocation = \common\models\UserLocation::formatLocation($company->location, '%city%, %region%, %country%');

            ?>
            <div class="designer-card__ps-loc" title="<?=$printerLocation;?>" style="font-size: 14px;">
                <span class="tsi tsi-map-marker"></span>
                <?= $printerLocation; ?>
            </div>
        <?php else: ?>
            <div class="designer-card__ps-loc" title="" style="font-size: 14px; min-height: 20px;">

            </div>
        <?php endif; ?>
        <div class="designer-card__about" itemprop="description">
            <?=H(\yii\helpers\StringHelper::truncate(strip_tags($model->description), 90));?>
        </div>

        <div class="designer-card__btn-block">
            <button class="btn btn-danger btn-ghost"
                    loader-click="openCreatePreorder(<?= $model->ps_id; ?>, {description:'<?= H($model->getTitleLabel()); ?>'}, '', <?= $model->id ?>)">
                <?= _t('site.ps', 'Get a Quote'); ?>
            </button>
        </div>
    </div>
</div>