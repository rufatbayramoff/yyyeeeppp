<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\controllers;


use common\components\BaseController;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\modules\catalogPs\repositories\PrinterMaterialRepository;
use common\modules\catalogPs\repositories\PrinterTechnologyRepository;
use common\modules\catalogPs\repositories\PsPrinterEntityRepository;
use frontend\components\UserSessionFacade;

class CncController extends BaseController
{

    /**
     * @var CatalogSearchForm
     */
    private $searchForm;
    /**
     * @var PrinterMaterialRepository
     */
    private $materialRepo;

    /**
     * @var PrinterTechnologyRepository
     */
    private $technologyRepository;


    public function injectDependencies(CatalogSearchForm $searchForm, PrinterMaterialRepository $materialRepo, PrinterTechnologyRepository $technologyRepository)
    {
        $this->searchForm = $searchForm;
        $this->materialRepo = $materialRepo;
        $this->technologyRepository = $technologyRepository;
    }


    public function actionIndex()
    {
        if (substr(app('request')->pathInfo, -1) != '/') {
            return $this->redirect(app('request')->pathInfo . '/', 301);
        }
        $searchForm = $this->searchForm;
        $searchForm->sort = CatalogSearchForm::SORT_MANUAL;
        $searchForm->perPage = 12;
        $searchForm->psLinks = \Yii::$app->setting->get(
            'settings.cncServices',
            [
                'ps1',
            ]
        );

        /* @var $psPrintersRepo PsPrinterEntityRepository */
        $psPrintersRepo = \Yii::createObject(
            [
                'class' => PsPrinterEntityRepository::class,
                'sort'  => $searchForm->sort
            ]
        );
        $dataProvider = $searchForm->getDataProvider($psPrintersRepo);


        return $this->render(
            'mainPageCnc.php',
            [
                'searchForm'    => $searchForm,
                'dataProvider'  => $dataProvider,
                'seoPage'           => $this->seo,
                'seo'           => $this->seo,
            ]
        );
    }

}