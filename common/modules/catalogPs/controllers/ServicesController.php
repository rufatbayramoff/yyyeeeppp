<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\controllers;

use common\components\BaseController;
use common\modules\catalogPs\models\ServicesSearchForm;
use common\modules\company\components\CompanyServiceInterface;
use common\modules\company\repositories\CompanyServiceRepository;
use yii\data\ActiveDataProvider;

class ServicesController extends BaseController
{

    /**
     * @var CompanyServiceRepository
     */
    private $companyServiceRepository;


    public function injectDependencies(CompanyServiceRepository $companyServiceRepository)
    {
        $this->companyServiceRepository = $companyServiceRepository;
    }

    public function actionIndex($filter = '')
    {
        $searchForm = new ServicesSearchForm();
        $filters = $searchForm->parseRequestParams($filter, \Yii::$app->request->getQueryParams());
        $searchForm->load($filters);

        $servicesQuery = $this->companyServiceRepository->getPublicServicesBySearch($searchForm);

        $servicesDataProvider = new ActiveDataProvider(
            [
                'query'      => $servicesQuery,
                'pagination' => [
                    'defaultPageSize' => 16,
                    'pageSizeLimit'   => [1, 64]
                ],
            ]
        );
        return $this->render('servicesList', ['servicesDataProvider' =>$servicesDataProvider, 'searchForm'=>$searchForm]);
    }
}
