<?php

namespace common\modules\catalogPs\controllers;

use common\components\BaseController;
use common\models\CompanyServiceCategory;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use common\modules\catalogPs\models\CatalogForm;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;

class CatalogController extends BaseController
{

    /**
     * @return string
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionIndex(): string
    {
        return $this->commonAction(null);
    }

    /**
     * @param string $category
     * @return string
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionCategory(string $category): string
    {
        return $this->commonAction($category);
    }

    /**
     * @param string|null $category
     * @return string
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function commonAction(?string $category): string
    {
        if ($category === '3d-printing') {
            $this->redirect(CatalogPrintingUrlHelper::printing3dCatalog(location:UserSessionFacade::getLocation()));
            return '';
        }
        $form = Yii::createObject([
            'class'    => CatalogForm::class,
            'category' => $category ? CompanyServiceCategory::tryFind(['slug' => $category]) : null

        ]);
        $form->load(Yii::$app->request->queryParams);
        $dataProvider = $form->search();

        return $this->render('index', [
            'form'         => $form,
            'dataProvider' => $dataProvider,
        ]);
    }
}