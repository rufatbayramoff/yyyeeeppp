<?php

namespace common\modules\catalogPs\controllers;

use common\components\BaseController;
use common\models\factories\LocationFactory;
use common\models\PrinterTechnology;
use common\models\SeoPage;
use common\modules\catalogPs\components\CatalogPsSeoDetector;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\modules\catalogPs\models\StoreOrderReviewSearchForm;
use common\modules\catalogPs\repositories\criterias\BestPrintingServiceCriteria;
use common\modules\catalogPs\repositories\criterias\PrinterTechnologyTypeCriteria;
use common\modules\catalogPs\repositories\PrinterMaterialRepository;
use common\modules\catalogPs\repositories\PrinterTechnologyRepository;
use common\modules\catalogPs\repositories\PsPrinterCatalogRepository;
use common\modules\catalogPs\repositories\PsPrinterEntityRepository;
use frontend\components\UserSessionFacade;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\Response;

/**
 *
 *
 * User: nabi
 */
class PsCatalogController extends BaseController
{

    /**
     * @var CatalogSearchForm
     */
    private $searchForm;
    /**
     * @var PrinterMaterialRepository
     */
    private $materialRepo;

    /**
     * @var PrinterTechnologyRepository
     */
    private $technologyRepository;


    public function injectDependencies(CatalogSearchForm $searchForm, PrinterMaterialRepository $materialRepo, PrinterTechnologyRepository $technologyRepository)
    {
        $this->searchForm           = $searchForm;
        $this->materialRepo         = $materialRepo;
        $this->technologyRepository = $technologyRepository;
    }

    /**
     * default page for catalog /3d-printing-services/
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function actionDefault()
    {
        $topSearchForm = clone $this->searchForm;
        $topSearchForm->setLocation(UserSessionFacade::getLocation());
        $this->searchForm->perPage = 6;
        $this->searchForm->psLinks = \Yii::$app->setting->get('settings.psCatalogPrintServiceLinks', CatalogSearchForm::PS_CATALOG_PRINT_SERVICE_LINKS);
        $this->searchForm->sort = CatalogSearchForm::SORT_RATING;

        /* @var $psPrintersRepo PsPrinterEntityRepository */
        $psPrintersRepo = \Yii::createObject(
            [
                'class' => PsPrinterEntityRepository::class,
            ]
        );
        return $this->render(
            'mainPage.php',
            [
                'topSearchForm'     => $topSearchForm,
                'searchForm'        => $this->searchForm,
                'dataProvider'      => $this->searchForm->getDataProvider($psPrintersRepo),
                'seo'               => $this->seo
            ]
        );
    }


    public function actionProfessionalServices(): string
    {
        $this->searchForm->perPage = 50;
        $this->searchForm->sort = CatalogSearchForm::SORT_RATING;
        /* @var $psPrintersRepo PsPrinterEntityRepository */
        $psPrintersRepo = Yii::createObject(
            [
                'class'    => PsPrinterEntityRepository::class,
                'criteria' => new PrinterTechnologyTypeCriteria(PrinterTechnology::professionalCodeTypes())
            ]
        );

        return $this->render(
            'mainPage.php',
            [
                'searchForm'        => $this->searchForm,
                'dataProvider'      => $this->searchForm->getDataProvider($psPrintersRepo),
                'seo'               => $this->seo
            ]
        );
    }

    public function actionBestPrintingServices()
    {
        $this->searchForm->perPage = 10;
        $this->searchForm->sort = CatalogSearchForm::SORT_RATING;
        /* @var $psPrintersRepo PsPrinterEntityRepository */
        $psPrintersRepo = Yii::createObject(
            [
                'class'    => PsPrinterEntityRepository::class,
                'criteria' => new BestPrintingServiceCriteria()
            ]
        );

        return $this->render(
            'best-printing-services',
            [
                'searchForm'        => $this->searchForm,
                'dataProvider'      => $this->searchForm->getDataProvider($psPrintersRepo),
                'seo'               => $this->seo
            ]
        );
    }

    /**
     * @param string|null $category
     * @param string|null $material
     * @param string|null $technology
     * @return string
     * @throws InvalidConfigException
     */
    public function actionReviews(?string $category = null,?string $material = null, ?string $technology = null)
    {
        $form = Yii::createObject([
            'class' => StoreOrderReviewSearchForm::class,
            'material' => $material,
            'technology' => $technology,
            'category' => $category,
        ]);
        $dataProvider = $form->search();
        if(\Yii::$app->request->isAjax) {
            $htmlBlock = $this->renderAjax('_reviewContainer', ['dataProvider' => $dataProvider]);
            return $this->jsonReturn([
                'htmlBlock' => $htmlBlock
            ]);
        }
        if(!$this->seo) {
            $seo = new SeoPage();
            $seo->title = '3D Printed Parts with Reviews';
            $seo->meta_keywords = '3D printed parts, 3D printed car parts, RC parts, custom parts';
            $seo->meta_description = 'Newcomer to 3D printing? See the latest reviews for 3D printed parts. People share how their custom 3D designs turned out.';
            $this->seo = $seo;
        }
        return $this->render('reviews', [
            'dataProvider' => $dataProvider,
            'form'         => $form,
            'seo'          => $this->seo
        ]);
    }

    public function actionPrintingServiceOnlineOrder()
    {
        return $this->redirect('my/print-model3d', 301);
    }

    /**
     * page with <location>/<filters>
     *
     * @param $location
     * @param string $filter
     * @return string|Response
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        $searchForm = $this->searchForm;

        $searchForm->loadParams(Yii::$app->request->getQueryParams());

        if (!$searchForm->validate()) {
            if ($searchForm->isInvalidLocation()) {
                $this->setFlashMsg(true, _t('site.catalogps', 'This location cannot be found.'));
            }
            return $this->response->redirect($searchForm->getValidUrl(), 301);
        }
        if (Yii::$app->request->url !== $searchForm->getValidUrl()) {
            return $this->response->redirect($searchForm->getValidUrl());
        }

        /* @var $psPrintersRepo PsPrinterCatalogRepository */
        $psPrintersRepo = Yii::createObject(
            [
                'class'      => PsPrinterCatalogRepository::class,
                'searchForm' => $searchForm
            ]
        );

        $dataProvider = $psPrintersRepo->getDataProvider();
        $allowedTechnologies = $psPrintersRepo->getAllowedTechnologies();
        $searchForm->setFilterTechnologies($allowedTechnologies);

        /** @var CatalogPsSeoDetector $seo */
        $seoService = Yii::createObject(
            [
                'class'      => CatalogPsSeoDetector::class,
                'language'   => Yii::$app->language,
                'totalFound' => $dataProvider->getTotalCount(),
                'location'   => $searchForm->location,
                'material'   => $searchForm->material,
                'usage'      => $searchForm->usage,
                'technology' => $searchForm->technology,
                'seoPage'    => $this->seo
            ]
        );
        $seoPage                = $seoService->getRenderedSeoPage();
        $this->seo              = clone $seoPage;
        $this->seo->footer_text = '';

        $canonicalUrl = strpos(Yii::$app->request->absoluteUrl, '?') !== false
            ? substr(Yii::$app->request->absoluteUrl, 0, strpos(Yii::$app->request->absoluteUrl, '?'))
            : Yii::$app->request->absoluteUrl;
        $canonicalUrl.='?location=all-locations';

        return $this->render(
            'index',
            [
                'seo'              => $seoService,
                'seoPage'          => $seoPage,
                'searchForm'       => $searchForm,
                'dataProvider'     => $dataProvider,
                'canonicalUrl'     => $canonicalUrl
            ]
        );
    }

}