<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


use common\components\ArrayHelper;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\helpers\Inflector;

abstract class AbstractArRepository extends BaseObject
{
    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var array
     */
    protected $models = [];

    /**
     * @var string
     */
    protected $modelClass;

    /**
     * @var string
     */
    public $tableName;

    public function init()
    {
        if (empty($this->tableName)) {
            throw new InvalidConfigException('Please specify tableName');
        }
        $this->modelClass = 'common\models\\' . Inflector::id2camel($this->tableName, '_');
    }

    /**
     * used for lazy loading, fill items only if items are requested from given repository
     */
    public function fillItems()
    {
        if (!empty($this->items)) {
            return;
        }
        $items = $this->modelClass::find()->asArray()->all();
        $this->setItems($items);
    }

    /**
     * @param array $items
     */
    public function setItems(array $items)
    {
        $this->items = ArrayHelper::index($items, 'id');
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $this->fillItems(); // lazy loading
        return $this->items;
    }

    public function getAllModels()
    {
        $this->fillItems();
        foreach ($this->items as $k => $v) {
            if (array_key_exists($v['id'], $this->models)) {
                continue;
            }
            $this->models[$v['id']] = $this->getModelById($v['id']);
        }
        return $this->models;
    }

    /**
     * @param $id
     * @return array
     */
    public function getRowById($id)
    {
        $this->fillItems(); // lazy loading
        if (!array_key_exists($id, $this->items)) {
            return null;
        }
        return $this->items[$id];
    }

    /**
     * @param $id
     * @return BaseAR
     */
    public function getModelById($id)
    {
        $this->fillItems(); // lazy loading
        if (array_key_exists($id, $this->models)) {
            return $this->models[$id];
        }
        $row = $this->getRowById($id);
        if ($row === null) {
            return null;
        }
        if(is_object($row)){ // @TODO - hack, need to trace, why row is object here???
            $this->models[$id] = $row;
            return $row;
        }
        $model = $this->modelClass::instantiate($row);
        $this->modelClass::populateRecord($model, $row);
        $this->models[$id] = $model;
        return $model;
    }

    public function getModelsById(array $ids)
    {
        $result = [];
        foreach ($ids as $id) {
            $result[] = $this->getModelById($id);
        }
        $result = array_filter($result);
        return $result;
    }
}