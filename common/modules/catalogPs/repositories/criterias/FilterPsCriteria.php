<?php


namespace common\modules\catalogPs\repositories\criterias;


use common\models\Ps;
use yii\db\Query;

class FilterPsCriteria implements CriteriaInterface
{
    /**
     * @var Ps
     */
    protected $ps;

    public function __construct(Ps $ps)
    {
        $this->ps = $ps;
    }

    /**
     * @param Query $query
     * @return Query
     */
    public function filter(Query $query): Query
    {
        $query->andWhere(['ps.id' => $this->ps->id]);
        return $query;
    }
}