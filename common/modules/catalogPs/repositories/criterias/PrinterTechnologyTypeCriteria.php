<?php


namespace common\modules\catalogPs\repositories\criterias;


use yii\db\Query;

class PrinterTechnologyTypeCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    private $types;

    /**
     * PrinterTechnologyTypeCriteria constructor.
     *
     * @param array $types
     */
    public function __construct(array $types)
    {
        $this->types = $types;
    }

    /**
     * @param Query $query
     * @return Query
     */
    public function filter(Query $query): Query
    {
        $query->innerJoin(['printer_technology'],'printer_technology.id = printer.technology_id');
        $query->andWhere(['printer_technology.code' => $this->types]);
        return $query;
    }
}