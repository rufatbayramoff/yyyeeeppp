<?php

namespace common\modules\catalogPs\repositories\criterias;

use common\components\DateHelper;
use common\models\StoreOrder;
use yii\db\Expression;
use yii\db\Query;

class BestPrintingServiceCriteria implements CriteriaInterface
{

    /**
     * @param Query $query
     * @return Query
     */
    public function filter(Query $query): Query
    {
        $query->innerJoin(['store_order_review_cnt' => $this->storeOrderAttemp()], 'store_order_review_cnt.machine_id = company_service.id');
        $query->addOrderBy(new Expression('max(store_order_review_cnt.cnt) DESC'));
        return $query;
    }

    /**
     * @return Query
     */
    protected function storeOrderAttemp()
    {
        $query = new Query();
        $query->select(['cnt' => 'count(*)', 'machine_id']);
        $query->from('store_order_attemp');
        $query->innerJoin('store_order','store_order.id = store_order_attemp.order_id');
        $query->where(['between', 'store_order_attemp.created_at', DateHelper::subNow('P30D'), DateHelper::now()]);
        $query->andWhere(['store_order.order_state' => StoreOrder::STATE_COMPLETED]);
        $query->groupBy('store_order_attemp.machine_id');
        return $query;
    }

}