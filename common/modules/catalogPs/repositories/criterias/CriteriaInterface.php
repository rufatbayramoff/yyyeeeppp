<?php


namespace common\modules\catalogPs\repositories\criterias;


use yii\db\Query;

interface CriteriaInterface
{
    public function filter(Query $query): Query;
}