<?php


namespace common\modules\catalogPs\repositories;


use common\models\CompanyServiceCategory;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use frontend\components\UserSessionFacade;
use yii\helpers\Url;

class CompanyServiceCategoryRepository
{
    /**
     * @param array $exclude
     * @return array
     */
    public function categoriesMenu($exclude = []): array
    {
        $menu   = [];
        $menu[] = [
            'text' => _t('site.main', 'Any'),
            'id'   => _t('site.main', 'Any'),
            'url'  => '/company-services'
        ];
        /** @var CompanyServiceCategory $serviceCategory */
        foreach (CompanyServiceCategory::find()->leaves()->all() as $serviceCategory) {
            if (array_key_exists($serviceCategory->slug, $exclude)) {
                continue;
            }
            $url = '/company-service/'.mb_strtolower($serviceCategory->slug);
            if ($serviceCategory->slug==='3d-printing') {
                $url = CatalogPrintingUrlHelper::printing3dCatalog(UserSessionFacade::getLocation());
            }
            $menu[] = [
                'text' => _t('site.main', $serviceCategory->title),
                'id'   => $serviceCategory->slug,
                'url'  => $url
            ];
        }
        return $menu;
    }
}