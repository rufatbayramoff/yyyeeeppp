<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


use common\components\ArrayHelper;
use common\models\Company;
use common\models\CompanyService;
use common\models\PrinterToMaterial;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\StoreOrderReview;
use common\models\User;
use common\modules\catalogPs\models\PsPrinterEntity;
use common\modules\catalogPs\repositories\criterias\CriteriaInterface;
use yii\base\BaseObject;
use yii\caching\DbDependency;
use yii\db\Query;

/**
 * Class PsPrinterEntityRepository
 *
 * @package common\modules\catalogPs\repositories
 */
class PsPrinterEntityRepository extends BaseObject
{
    public $items;
    public $sort;

    /**
     * @var CriteriaInterface
     */
    public $criteria;

    /**
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        $this->items = ArrayHelper::index($this->getItems(), 'id');
    }

    public function getById($id)
    {
        if (!array_key_exists($id, $this->items)) {
            return null;
        }
        return $this->items[$id];
    }


    /**
     * @return array|\yii\db\DataReader
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function getItems()
    {
        $query = new Query();
        $query->select([
            'ps_printer.id',
            'ps_id'                => 'MIN(company_service.ps_id)',
            'ps_printer.title',
            'printer_id'           => 'ps_printer.id',
            'ps_printer.description',
            'min_order_price'      => 'IFNULL(ps_printer.min_order_price,0)',
            'min_order_price_gr'   => 'MIN(pscolor.price)',
            'ps_rating'            => '(AVG(review.rating_speed) + AVG(review.rating_quality) + AVG(review.rating_communication)) / 3',
            'ps_rating_count'      => 'COUNT(DISTINCT review.id)',
            'location_id'          => 'MIN(company_service.location_id)',
            'location_lat'         => 'MIN(user_location.lat)',
            'location_lon'         => 'MIN(user_location.lon)',
            'printer.technology_id',
            'ps_material_ids'      => 'GROUP_CONCAT(DISTINCT ps_printer_material.id)',
            'material_ids'         => 'GROUP_CONCAT(DISTINCT printer_material.id)',
            'material_group_codes' => 'GROUP_CONCAT(DISTINCT mgroup.code)',
            'ps_delivery_ids'      => 'GROUP_CONCAT(DISTINCT d.id)',
            'delivery_type_ids'    => 'GROUP_CONCAT(DISTINCT d.delivery_type_id)',
            'country_id'           => 'MIN(user_location.country_id)',
            'country_iso'          => 'MIN(geo_country.iso_code)',
            'city'                 => 'MIN(geo_city.title)',
            'region'               => 'MIN(geo_region.title)',
            'lastonline_at'        => 'MAX(user_statistics.lastonline_at)'
        ]);
        $query->from('ps_printer');
        $query->leftJoin('company_service', "company_service.ps_printer_id=ps_printer.id AND company_service.type='" . CompanyService::TYPE_PRINTER . "'");
        $query->leftJoin(['d' => 'ps_machine_delivery'], "d.ps_machine_id = company_service.id");
        $query->leftJoin('ps', "ps.id = company_service.ps_id");
        $query->leftJoin(['review' => 'store_order_review'], "review.ps_id = company_service.ps_id AND review.status = '" . StoreOrderReview::ANSWER_STATUS_MODERATED . "'");
        $query->leftJoin('ps_printer_material', "ps_printer_material.ps_printer_id = ps_printer.id");
        $query->leftJoin('printer_material', "printer_material.id=ps_printer_material.material_id");
        $query->leftJoin('printer_to_material',
            "printer_to_material.printer_id = ps_printer.printer_id AND printer_to_material.material_id = ps_printer_material.material_id");
        $query->leftJoin(['mgroup' => 'printer_material_group'], "mgroup.id=printer_material.group_id");
        $query->leftJoin(['pscolor' => 'ps_printer_color'], "pscolor.ps_material_id=ps_printer_material.id");
        $query->leftJoin('printer', "ps_printer.printer_id = printer.id");
        $query->leftJoin('user_location', "company_service.location_id = user_location.id");
        $query->leftJoin('geo_country', "geo_country.id=user_location.country_id");
        $query->leftJoin('geo_city', "geo_city.id=user_location.city_id");
        $query->leftJoin('geo_region', "geo_region.id=user_location.region_id");
        $query->leftJoin('user', "ps.user_id = user.id");
        $query->leftJoin('user_statistics', "user.id = user_statistics.user_id");
        $query->where([
            'ps.moderator_status'           => [Ps::MSTATUS_CHECKED, Ps::MSTATUS_UPDATED],
            'ps.is_deleted'                 => Company::IS_NOT_DELETED_FLAG,
            'company_service.moderator_status'   => CompanyService::moderatedStatusList(),
            'company_service.visibility'        => CompanyService::VISIBILITY_EVERYWHERE,
            'company_service.is_deleted'    => CompanyService::IS_NOT_DELETED_FLAG,
            'user.status'                   => User::STATUS_ACTIVE,
            'printer_to_material.is_active' => PrinterToMaterial::STATUS_ACTIVE
        ]);
        $query->groupBy('ps_printer.id');
        if($this->criteria) {
            $query = $this->criteria->filter($query);
        }
        return $query->createCommand()->cache(7200, $this->getCacheDependency())->queryAll([\PDO::FETCH_CLASS, PsPrinterEntity::class]);
    }


    /**
     * @return DbDependency
     * @throws \yii\base\InvalidConfigException
     */
    private function getCacheDependency(): DbDependency
    {
        return \Yii::createObject([
            'class' => DbDependency::class,
            'sql'   => "SELECT 
                (SELECT max(updated_at)  FROM `company_service` WHERE type='printer' LIMIT 1),
                (SELECT max(updated_at)  FROM `printer_to_material` LIMIT 1)",
        ]);
    }

}