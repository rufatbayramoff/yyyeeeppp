<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


use common\models\PrinterMaterial;
use common\models\PrinterTechnology;

class PrinterTechnologyRepository extends AbstractArRepository
{
    public $tableName = 'printer_technology';

    public $modelsByCode = [];

    public function fillItems()
    {
        if(!empty($this->items)){
            return;
        }
        $obj = new $this->modelClass;
        $items = $obj::find()->where(['is_active' => 1])->asArray()->all();
        $this->setItems($items);
        foreach($items as $item){
            $this->models[$item['id']] = $this->getModelById($item['id']);
        }
    }

    /**
     * @param $code
     * @return null|PrinterTechnology
     */
    public function getByCode($code)
    {
        $this->fillItems();
        $code = strtolower($code);
        if(empty($this->modelsByCode)){
            foreach($this->models as $model){
                /* @var $model PrinterTechnology */
                $this->modelsByCode[strtolower($model->getTitleCode())] = $model;
            }
        }
        if (array_key_exists($code, $this->modelsByCode)) {
            return $this->modelsByCode[$code];
        }
        return null;
    }






    /**
     * @TODO - delete? not used for now
     *
     * @param array $materialIds
     * @return array
     */
    private function getAllByMaterials(array $materialIds)
    {
        // @TODO - move to service?

        $technologies = PrinterTechnology::find()
            ->joinWith('printers.printerToMaterials')
            ->where(['printer_technology.is_active' => 1])
            ->andWhere(['printer_to_material.material_id' => $materialIds])
            ->groupBy('printer_technology.id')
            ->column();

        return $technologies;
    }

    /**
     * @TODO - delete? not used for now
     *
     * @param $usage
     * @return array
     */
    private function getAllByUsage($usage)
    {
        $materialIds = PrinterMaterial::find()
            ->joinWith('group')
            ->where(['printer_material.is_active' => 1])
            ->andWhere(['printer_material_group.code' => $usage['groups'], 'printer_material_group.is_active' => 1])
            ->column();

        $printerTechnologiesIds = $this->getAllByMaterials($materialIds);
        $result = [];
        foreach ($printerTechnologiesIds as $technologyId) {
            $technology = $this->getModelById($technologyId);
            $result[$technology->getTitleCode()] = $technology;
        }

        return $result;
    }
}