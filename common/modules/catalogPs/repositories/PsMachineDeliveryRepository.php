<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


class PsMachineDeliveryRepository extends AbstractArRepository
{
    public $tableName = 'ps_machine_delivery';

    public function fillItems()
    {
        if(!empty($this->items)){
            return;
        }
        $obj = new $this->modelClass;
        $items = $obj::find()->asArray()->all();
        $this->setItems($items);
    }
}