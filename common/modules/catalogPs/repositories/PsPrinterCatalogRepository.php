<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


use common\components\ArrayHelper;
use common\models\query\PsQuery;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\modules\catalogPs\models\PsPrinterCatalogEntity;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;

/**
 * Class PsPrinterEntityRepository
 *
 * @package common\modules\catalogPs\repositories
 */
class PsPrinterCatalogRepository extends BaseObject
{
    public $items;

    /** @var CatalogSearchForm */
    public $searchForm;

    /** @var integer */
    public $limit;

    public function getBaseQuery()
    {
        $query = PsPrinterCatalogEntity::find()
            ->select('ANY_VALUE (ps_printer.id) as id, ps.id as ps_id')
            ->joinWith(
                [
                    'ps' => function (PsQuery $psQuery) {
                        $psQuery
                            ->joinWith('psCatalog', false)
                            ->active();
                    }
                ],
                false
            )
            ->visible()
            ->moderated();

        if ($this->limit) {
            $query->limit($this->limit);
        }

        if ($this->searchForm->location && ($this->searchForm->location->city !== CatalogSearchForm::LOCATION_EMPTY)) {
            $query->canPickupOrDeliveryToCountry($this->searchForm->location->country, !$this->searchForm->international);
        }

        return $query;
    }

    public function getAllowedTechnologies()
    {
        $query = $this->getBaseQuery();
        $query->joinWith(
            'printer'
        );
        $query->select('printer.technology_id as technology_id');
        $query->groupBy('technology_id');
        $printersInfo = $query->all();
        $technologies = ArrayHelper::getColumn($printersInfo, 'technology_id');
        return $technologies;
    }

    public function getPsIdsFilterByText(string $text): array
    {
        $psIds = (new \yii\sphinx\Query())
            ->select('ps_id')
            ->from('pscatalog')
            ->match(trim($text))
            ->groupBy('ps_id')
            ->showMeta(true)
            ->column();

        $psIds = array_map(function ($val) {
            return (int)$val;
        }, $psIds);
        return $psIds;
    }

    /**
     * @return ActiveDataProvider
     */
    public function getDataProvider()
    {
        $query = $this->getBaseQuery();

        if ($this->searchForm->technology) {
            $query->technology($this->searchForm->getTechnology());
        }

        if ($this->searchForm->usage) {
            $query->usage($this->searchForm->getUsage());
        }

        if ($this->searchForm->material) {
            $query->materials($this->searchForm->getMaterial());
        }

        if ($this->searchForm->text) {
            $query->leftJoin('company_service_tag', 'company_service_tag.company_service_id=company_service.id');
            $query->leftJoin('site_tag', 'site_tag.id=company_service_tag.tag_id');
            $query->andWhere(['or',
                ['like', 'ps.title', $this->searchForm->text],
                ['like', 'company_service.title', $this->searchForm->text],
                ['site_tag.text' => $this->searchForm->text]
            ]);
        }

        switch ($this->searchForm->sort) {
            case CatalogSearchForm::SORT_DISTANCE:
                if ($this->searchForm->location->lat && $this->searchForm->location->lon) {
                    $selectCmd = 'ANY_VALUE (ps_printer.id) as id, ps.id as ps_id, ' .
                        'min( 6371 * acos( cos( radians(' . $this->searchForm->location->lat . ') ) * cos( radians( l.lat ) )' .
                        '* cos( radians(l.lon) - radians(' . $this->searchForm->location->lon . ')) + sin(radians(' . $this->searchForm->location->lat . '))' .
                        '* sin( radians(l.lat)))) as distance';
                    $query->select($selectCmd);
                    $query->orderBy('distance');
                }
                break;
            case CatalogSearchForm::SORT_RATING:
                $query->orderBy('ps_catalog.rating_med desc');
                break;
            case CatalogSearchForm::SORT_PRICE:
                $query->orderBy('ps_catalog.price_low asc');
                break;
        }
        $query->groupBy('ps.id');
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);
        return $dataProvider;
    }
}