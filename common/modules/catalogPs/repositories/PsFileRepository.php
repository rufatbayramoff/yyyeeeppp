<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


use common\components\ArrayHelper;
use common\models\File;

class PsFileRepository
{

    public $items = [];

    public function __construct($fileIds)
    {
        $fileIds = array_unique($fileIds);
        $this->items = ArrayHelper::index(File::findAll(['id'=>$fileIds]), 'id');
    }

    public function getById($id)
    {
        if(empty($this->items[$id])){
            return null;
        }
        return $this->items[$id];
    }
    public function getAll(){
        return $this->items;
    }
}

