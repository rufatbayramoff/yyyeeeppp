<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


use common\components\BaseAR;

class PrinterToMaterialRepository extends AbstractArRepository
{
    public $tableName = 'printer_to_material';

    public function fillItems()
    {
        if (!empty($this->items)) {
            return;
        }
        /**
         * @var $obj BaseAR
         */
        $obj = new $this->modelClass;
        $items = $obj::find()->joinWith('printer')->joinWith('material')
            ->where(['printer_material.is_active'=>1, 'printer.is_active'=>1, 'printer_to_material.is_active'=>1])
            ->asArray()->all();
        $this->setItems($items);
    }
}
