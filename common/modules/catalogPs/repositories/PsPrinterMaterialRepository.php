<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


class PsPrinterMaterialRepository extends AbstractArRepository
{
    public $tableName = 'ps_printer_material';

    public function fillItems()
    {
        if(!empty($this->items)){
            return;
        }
        $obj = new $this->modelClass;
        $items = $obj::find()->where(['is_active'=>1])->asArray()->all();
        $this->setItems($items);
    }
}