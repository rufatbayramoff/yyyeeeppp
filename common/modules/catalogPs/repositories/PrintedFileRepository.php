<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;

use common\components\ActiveQuery;
use common\components\FileTypesHelper;
use common\models\CompanyService;
use common\models\Printer;
use common\models\repositories\FileRepository;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderReview;
use yii\db\Query;

class PrintedFileRepository
{

    /**
     * grouped by ps.id
     *
     * @var array
     */
    public $itemsByPs = [];

    /**
     * grouped by ps_printer.id
     *
     * @var array
     */
    public $itemsByPrinter = [];
    /**
     * @var int|null
     */
    private $limit;


    /**
     * PrintedFileRepository constructor.
     *
     * @param $printers
     * @param int|null $limit
     */
    public function __construct($printers, ?int $limit = null)
    {
        $ids = array_column($printers,'ps_id');
        $this->limit = $limit;
        $this->initByPs($ids);
    }

    /**
     * @param Printer $printer
     * @param int $limit
     * @return array|null
     */
    public static function getByPrinterMachineId(Printer $printer, int $limit = 20): ?array
    {
        if ($companyService = $printer->companyService) {
            return (new self([$companyService], $limit))->getByPrinterId($printer->id);
        }
        return null;
    }

    private function initByPs($psIds)
    {
        $query = StoreOrderReview::find();
        $query->joinWith(['files'], false);
        $query->joinWith(['storeOrderCurrentAttemp' => function(ActiveQuery $activeQuery){
            $activeQuery->andWhere(['store_order_attemp.status' => StoreOrderAttemp::STATUS_RECEIVED]);
        }]);
        $query->andWhere(['store_order_attemp.ps_id' => $this->findModerateOrderByPsList($psIds)]);
        $query->andWhere(['store_order_review.status' => 'moderated']);
        $query->orderBy(['store_order_review.id' => SORT_DESC]);
        $query->distinct();
        if ($this->limit) {
            $query->limit($this->limit);
        }
        $orderReviews = $query->all();
        $filerepository = \Yii::CreateObject(FileRepository::class);
        /** @var StoreOrderReview $review */
        foreach ($orderReviews as $review) {
            $files = $review->files;
            foreach ($files as $file) {
                if (!$file->is_public && $file->isActiveStatus() && FileTypesHelper::isImage($file)) {
                    // Fix: Move TestOrder Images to public mode
                    $file->setPublicMode(true);
                    $filerepository->save($file);
                }
                /** @var CompanyService $companyService */
                $companyService = $review->storeOrderCurrentAttemp->machine ?? null;
                if ($companyService) {
                    $this->itemsByPs[$companyService->ps_id][$file->id] = $file;
                    $this->itemsByPrinter[$companyService->ps_printer_id][$file->id] = $file;
                }
            }
        }
    }

    /**
     * @param array $ids
     * @return array
     */
    protected function findModerateOrderByPsList(array $ids): array
    {
        $query = new Query();
        $query->from('store_order');
        $query->innerJoin('store_order_attemp', 'store_order_attemp.id = store_order.current_attemp_id');
        $query->innerJoin('company_service', 'company_service.id = store_order_attemp.machine_id');
        $query->innerJoin('store_order_attempt_moderation',
            "store_order_attempt_moderation.attempt_id = store_order.current_attemp_id and store_order_attempt_moderation.status = 'accepted'");
        $query->innerJoin('store_order_attempt_moderation_file', 'store_order_attempt_moderation_file.attempt_id = store_order.current_attemp_id');
        $query->andWhere(['company_service.ps_id' => $ids]);
        $query->andWhere(['store_order_attemp.status' => StoreOrderAttemp::STATUS_RECEIVED]);
        $query->select('company_service.ps_id');
        $query->distinct();
        return $query->column();
    }

    /**
     * @param $psPrinterId
     * @return array
     */
    public function getByPrinterId($psPrinterId)
    {
        if (empty($this->itemsByPrinter[$psPrinterId])) {
            return null;
        }
        return $this->itemsByPrinter[$psPrinterId];
    }

    /**
     * @param $psId
     * @return mixed|null
     */
    public function getById($psId)
    {
        if (empty($this->itemsByPs[$psId])) {
            return null;
        }
        return $this->itemsByPs[$psId];
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->itemsByPs;
    }

}