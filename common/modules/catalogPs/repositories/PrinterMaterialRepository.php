<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


use common\components\ArrayHelper;
use common\models\PrinterMaterial;

class PrinterMaterialRepository extends AbstractArRepository
{

    public $tableName = 'printer_material';

    /**
     * @var array
     */
    private $itemsByCode = [];

    /**
     * @var array
     */
    private $itemsBySlug = [];

    protected $items = [];

    public function fillItems()
    {
        if (!empty($this->items)) {
            return;
        }
        $obj = new $this->modelClass;
        $items = $obj::find()->asArray()->all();

        foreach($items as $k=>$item){
            $item['filament_title'] = str_replace('+', 'plus', $item['filament_title']);
            $items[$k]['slug'] = PrinterMaterial::slugifyFilament($item['filament_title']);
        }
        $this->items = ArrayHelper::index($items, 'id');
        $this->itemsByCode = ArrayHelper::index($items, 'filament_title');
        $this->itemsBySlug = ArrayHelper::index($items, 'slug');
    }

    public function getBySlug($slug)
    {
        if(is_null($slug)){
            return null;
        }
        $this->fillItems();
        if (array_key_exists($slug, $this->itemsBySlug)) {
            return $this->getModelById($this->itemsBySlug[$slug]['id']);
        }
        return null;
    }
    /**
     * @param $code
     * @return PrinterMaterial|null
     */
    public function getByCode($code)
    {
        if(is_null($code)){
            return null;
        }
        $this->fillItems();
        if (array_key_exists($code, $this->itemsByCode)) {
            return $this->getModelById($this->itemsByCode[$code]['id']);
        }
        return null;
    }

    /**
     * get materials by material group
     *
     * @param array $groups
     * @return PrinterMaterial[]
     * @internal param PrinterMaterialGroup $groups
     */
    public function getModelsByGroups(array $groups)
    {
        $this->fillItems();
        $result = [];
        $groupIds = array_keys(ArrayHelper::index($groups, 'id'));

        foreach($this->items as $k=>$item){
            if(in_array($item['group_id'], $groupIds)){
                $result[] = $this->getModelById($item['id']);
            }
        }
        return $result;
    }

}