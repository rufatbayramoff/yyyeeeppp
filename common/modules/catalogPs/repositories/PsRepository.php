<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


class PsRepository extends AbstractArRepository
{
    public $tableName = 'ps';

    public function fillItems()
    {
        if(!empty($this->items)){
            return;
        }
        $obj = new $this->modelClass;
        $items = $obj::find()->asArray()->all();
        $this->setItems($items);
    }
}