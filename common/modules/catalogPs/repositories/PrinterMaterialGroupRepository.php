<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


use common\components\ArrayHelper;

class PrinterMaterialGroupRepository extends AbstractArRepository
{

    public $tableName = 'printer_material_group';

    private $itemsByCode = null;

    public function fillItems()
    {
        if (!empty($this->items)) {
            return;
        }
        $obj = new $this->modelClass;
        $items = $obj::find()->where(['is_active'=>1])->asArray()->all();
        $this->setItems($items);
    }

    /**
     * @param $code
     * @return BaseAR|null
     */
    public function getByCode($code)
    {
        $this->fillItems();
        $this->itemsByCode = $this->itemsByCode ?: ArrayHelper::index($this->items, 'code');
        if (array_key_exists($code, $this->itemsByCode)) {
            return $this->getModelById($this->itemsByCode[$code]['id']);
        }
        return null;
    }

}