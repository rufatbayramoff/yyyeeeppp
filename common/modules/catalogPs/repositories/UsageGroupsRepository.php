<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\repositories;


use common\models\Printer;
use common\models\PrinterMaterialGroup;
use common\models\repositories\PrinterColorRepository;
use yii\base\BaseObject;

class UsageGroupsRepository extends BaseObject
{
    protected $items;

    public function init()
    {
        if ($this->items === null) {
            $usageGroups = self::getUsageGroups();
            $result      = [];
            foreach ($usageGroups as $usageType):
                if (empty($usageType['id'])) {
                    continue;
                }
                $result[$usageType['id']] = $usageType;
            endforeach;
            $this->items = $result;
        }
    }

    public static function getUsageGroups($materialsGroupItem = [])
    {
        $groups   = \Yii::$app->setting->get('store.usage_groups');
        $result   = [];
        $result[] = [
            'id'          => '0',
            'title'       => _t('filepage.usage', 'Any'),
            'image'       => 'https://static.treatstock.com/static/images/usage-groups/all.svg',
            'description' => _t('filepage.usage', 'Show all available materials'),
            'groups'      => []
        ];
        if (empty($groups) || !is_array($groups)) {
            return $result;
        }
        $printerMaterialGroupRepository = \Yii::createObject(PrinterMaterialGroupRepository::class);
        $materialsGroupCodes            = [];
        foreach ($materialsGroupItem as $colorsItem) {
            $materialsGroupCodes[$colorsItem->materialGroup->code] = $colorsItem->materialGroup->code;
        }
        foreach ($groups as $k => $group) {

            if (!is_array($group['groups'])) {
                continue;
            }
            if (!empty($materialsGroupCodes) && count(array_intersect($materialsGroupCodes, $group['groups'])) === 0 &&
                count(array_intersect($group['groups'], $materialsGroupCodes)) === 0
            ) {
                continue;
            }

            $title       = $k;
            $description = $group['description'] !== '-' ? $group['description'] : implode(", ", $group['groups']);
            if (!empty($group['title'])) {
                $title = $group['title'];
            }
            if (!empty($group['title_' . \Yii::$app->language])) {
                $title = $group['title_' . \Yii::$app->language];
            }
            if (!empty($group['description_' . \Yii::$app->language])) {
                $description = $group['description_' . \Yii::$app->language];
            }

            $printerMaterialGroups = [];
            foreach ($group['groups'] as $groupCode) {
                $printerMaterialGroup    = $printerMaterialGroupRepository->getByCode($groupCode);
                $printerMaterialGroups[] = $printerMaterialGroup;
            }

            $result[] = [
                'id'                    => $k,
                'title'                 => $title,
                'image'                 => $group['image'] !== '-' ? $group['image'] : '',
                'description'           => $description,
                'groups'                => $group['groups'],
                'printerMaterialGroups' => $printerMaterialGroups
            ];
        }
        return $result;
    }

    public function getById($id)
    {
        if (array_key_exists($id, $this->items)) {
            return $this->items[$id];
        }
        return null;
    }

    public function getAll()
    {
        return $this->items;
    }

    public function getByCode($code)
    {
        return $this->getById($code);
    }

    public function getAllByTechnology($technologyId)
    {
        // @TODO to service
        $printerIds          = Printer::find()->active()->where(['technology_id' => $technologyId])->column();
        $materialsGroupCodes = PrinterMaterialGroup::find()->select('code')
            ->joinWith('printerMaterials.printerToMaterials')
            ->where(['printer_to_material.printer_id' => $printerIds])->groupBy('printer_material_group.code')
            ->column();

        $result = [];
        foreach ($this->items as $k => $usageGroup) {
            if (!empty($materialsGroupCodes) && count(array_intersect($materialsGroupCodes, $usageGroup['groups'])) === 0 &&
                count(array_intersect($usageGroup['groups'], $materialsGroupCodes)) === 0
            ) {
                continue;
            }
            $result[$k] = $usageGroup;
        }
        return $result;
    }

}