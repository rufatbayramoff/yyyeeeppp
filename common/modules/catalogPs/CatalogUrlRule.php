<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs;

use frontend\components\UserSessionFacade;
use Yii;
use yii\helpers\Url;
use yii\web\UrlRuleInterface;

class CatalogUrlRule implements UrlRuleInterface{

    const CATALOG_PS_ROUTE = 'catalogps/ps-catalog/index';
    const CATALOG_CNC_ROUTE = 'catalogps/cnc/index';
    const CATALOG_SERVICE_ROUTE = 'catalogps/services/index';
    const PATH_START = ['catalogps/', '3d-printing-services/', 'cnc-machine-shops/', 'cnc-services/'];

    const TYPE_PS = 'ps';
    const TYPE_CNC = 'cnc';
    const TYPE_SERVICES = 'services';

    /**
     * get ps catalog url with current location
     *
     * @param array $array
     * @return string
     */
    public static function getPsCatalogUrl($array = [])
    {
        $url = Url::toRoute(
            array_merge([
                '/catalogps/ps-catalog/index',
                'type'     => '3d-printing-services',
                'location' => UserSessionFacade::getLocationAsString(UserSessionFacade::getLocation())
            ], $array)
        );
        $url = mb_strtolower($url);
        $url = str_replace('/?', '?', $url);
        $url = rtrim($url, '/');
        return $url;
    }
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if (!$this->checkPathRoute($pathInfo)) {
            return false;
        }
        $route = self::CATALOG_PS_ROUTE;
        $params = [];
        $parameters = explode('/', $pathInfo);
        $params['type'] =  $parameters[0];
        if($this->isCncType($params['type'])){
            $route = self::CATALOG_CNC_ROUTE;
        }
        if (count($parameters) > 1) {
            $params['location'] = $parameters[1];
        }
        if (count($parameters) > 2 ) {
            $params['filter'] = $parameters[2];
        }

        Yii::trace("Request parsed with URL rule: " . $route, __METHOD__);
        return [$route, $params];
    }

    public function createUrl($manager, $route, $params)
    {
        if (!in_array($route, [self::CATALOG_PS_ROUTE, self::CATALOG_CNC_ROUTE, self::CATALOG_SERVICE_ROUTE])) {
            return false;
        }
        $url = ($params['type']??'') . '/';

        if (array_key_exists('location', $params) && !empty($params['location'])) {
            $params['location'] = str_replace('----', '', $params['location']);
            $url .= $params['location'] . '/';
        }
        if (array_key_exists('filter', $params) && !empty($params['filter'])) {
            $url .=   $params['filter'];
        }
        if (array_key_exists('page', $params) && !empty($params['page'])) {
            $url = trim($url, '?');
            $url .= '?page=' . (int)$params['page'];
            if (array_key_exists('sort', $params) && !empty($params['sort'])) {
                $url .= '&sort=' . $params['sort'];
            }
            if (array_key_exists('international', $params) && !empty($params['international'])) {
                $url .= '&international=' . (int)$params['international'];
            }
            if (array_key_exists('printer', $params) && !empty($params['printer'])) {
                $url .= '&printer=' . (int)$params['printer'];
            }
        }
        return $url;
    }

    /**
     * check if given path should be parsed with this url rule
     * @param $pathInfo
     * @return bool
     */
    private function checkPathRoute($pathInfo)
    {
        foreach(self::PATH_START as $path){
            if(strpos($pathInfo, $path) === 0){
                return true;
            }
        }
        return false;
    }

    private function isCncType($type)
    {
        if($type=='cnc-machine-shops' or $type=='cnc-services'){
            return true;
        }
        return false;
    }

}