<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\components;


use common\components\ArrayHelper;
use common\models\factories\LocationFactory;
use common\models\GeoTopCity;
use common\models\SeoPage;
use common\models\SeoPageAutofillTemplate;
use common\models\UserAddress;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use common\modules\seo\placeholders\PrintServiceCatalogPlaceholder;
use common\modules\seo\services\SeoAutofillService;
use lib\geo\models\Location;
use lib\message\MessageRenderHelper;
use Yii;
use yii\base\BaseObject;

class CatalogPsSeoDetector extends BaseObject
{
    const TPL_LOCATION            = 'location';
    const TPL_LOCATION_TECHNOLOGY = 'location-technology';
    const TPL_LOCATION_MATERIAL   = 'location-material';
    const TPL_LOCATION_USAGE      = 'location-usage';

    const TPL_LOCATION_TECHNOLOGY_USAGE    = 'location-technology-usage';
    const TPL_LOCATION_TECHNOLOGY_MATERIAL = 'location-technology-material';
    const TPL_LOCATION_MATERIAL_USAGE      = 'location-material-usage';
    const TPL_LOCATION_FILTER_ALL          = 'location-technology-material-usage';

    /**
     * @var
     */
    public $language;

    /**
     * @var Location
     */
    public $location;
    /**
     * @var string
     */
    public $technology;
    /**
     * @var string
     */
    public $usage;

    /**
     * @var string
     */
    public $material;

    /**
     * @var integer
     */
    public $totalFound;

    /**
     * total all ps without filtering
     *
     * @var integer
     */
    public $psTotal;

    /**
     * @var SeoPage
     */
    public $seoPage;

    /**
     * if false, %psTotal% will stay
     *
     * @var bool
     */
    public $emptyRender = true;

    /**
     * @var CatalogSearchService
     */
    protected $searchService;

    /**
     * @var SeoPage
     */
    protected $renderedSeoPage;

    public function injectDependencies(CatalogSearchService $searchService)
    {
        $this->searchService = $searchService;
        if (!empty($this->location)) {
            $this->initByTemplate();
        }
    }

    public function getRenderedSeoPage()
    {
        return $this->renderedSeoPage ?: new SeoPage();
    }


    /**
     * init seo data by template
     */
    private function initByTemplate()
    {
        $this->renderedSeoPage = new SeoPage();
        $seoTpl                = $this->detectSeoTemplate() ?: new SeoPageAutofillTemplate();
        $placeHolder           = $this->getFilledSeoPlaceholder();
        $tplVars               = $placeHolder->getFilledPlaceholders();

        $refillAttributes = ['title', 'header', 'header_text', 'footer_text', 'meta_description', 'meta_keywords'];
        foreach ($refillAttributes as $refillAttribute) {
            $seoTpl->{$refillAttribute} = $this->seoPage && !empty($this->seoPage->{$refillAttribute})
                ? $this->seoPage->{$refillAttribute}
                : $seoTpl->{$refillAttribute};
            if (!$this->emptyRender) {
                $tplVars = array_filter($tplVars);
            }
            $renderedField                             = MessageRenderHelper::renderTemplate($seoTpl->{$refillAttribute}, $tplVars);
            $this->renderedSeoPage->{$refillAttribute} = $renderedField;
        }
        if (!empty($this->seoPage)) {
            $this->seoPage->setTranslateFieldList(['title', 'header', 'meta_description', 'meta_keywords', 'header_text']);
            $this->seoPage->footer_text = '';
        }

        if ($seoTpl->is_apply_created) {
            $this->renderedSeoPage->need_review = true;
            $this->renderedSeoPage->is_active   = true;
        }
    }

    /**
     * @return string
     */
    public function getValidUrl()
    {
        $url           =  CatalogPrintingUrlHelper::printing3dCatalog(location: $this->location, technologyCode: $this->technology, usageCode: $this->usage, materialCode: $this->material);
        return $url;
    }

    /**
     * @return SeoPage
     */
    public function reinit()
    {
        $this->initByTemplate();
    }

    /**
     * get template based on set of attributes [location,technology,material,usage]
     *
     * @return SeoPageAutofillTemplate
     */
    public function detectSeoTemplate()
    {
        $tplParts = [];
        if (!empty($this->location)) {
            $tplParts[] = 'location';
        }
        if (!empty($this->technology)) {
            $tplParts[] = 'technology';
        }
        if (!empty($this->material)) {
            $tplParts[] = 'material';
        }
        if (!empty($this->usage)) {
            $tplParts[] = 'usage';
        }
        $tplName   = join('-', $tplParts);
        $searchKey = [
            'type'          => SeoAutofillService::TYPE_PSCATALOG,
            'template_name' => $tplName,
            'lang_iso'      => $this->language
        ];
        $seoTpl    = SeoPageAutofillTemplate::findOne($searchKey);

        return $seoTpl;
    }

    /**
     * @return PrintServiceCatalogPlaceholder
     */
    public function getFilledSeoPlaceholder()
    {
        $placeHolder = new PrintServiceCatalogPlaceholder();
        $technology  = $this->searchService->getTechnology($this->technology);
        $usageObj    = $this->searchService->getUsage($this->usage);
        $materials   = $this->searchService->getMaterialsByCodes($this->material);
        $placeHolder->setData([
            'found'                         => $this->totalFound,
            'psTotal'                       => $this->psTotal,
            'location'                      => $this->location,
            'technology'                    => $technology? $technology->title: $this->technology,
            'usage'                         => $usageObj ? $usageObj['title'] : $this->usage,
            'productApplication'            => $usageObj ? $usageObj['title'] : $this->usage,
            'technologyDescription'         => $technology ? $technology->description : '',
            'productApplicationDescription' => $usageObj ? $usageObj['description'] : '',
            'materialDescription'           => $materials ? implode(', ', ArrayHelper::getColumn($materials, 'long_description')) : '',
            'material'                      => $materials ? implode(', ', ArrayHelper::getColumn($materials, 'title')) : '',
            'city'                          => $this->location->city,
            'region'                        => $this->location->region,
            'country'                       => $this->location->country
        ]);
        return $placeHolder;
    }

    /***************************/
    /*  ['label' => 'Home', 'url' => ['site/index']], */
    // move to another class?

    /**
     * @return array
     */
    public function getLinksTopCities()
    {
        $result    = [];
        $countryId = $this->location->country ? $this->location->getGeoCountry()->id : UserAddress::COUNTRY_USA_ID;
        $topCities = GeoTopCity::find()->with('city')->where(['country_id' => $countryId])->all();

        foreach ($topCities as $tp) {
            $city     = $tp->city;
            $location = LocationFactory::createFromGeoCity($city);
            $result[] = [
                'label' => $city->title,
                'url'   => CatalogPrintingUrlHelper::printing3dCatalog($location)
            ];
        }
        return $result;
    }

    public function getLinksProductTypes()
    {
        $usages = $this->searchService->getUsageTypes('');
        $result = [];
        foreach ($usages as $usage) {
            $result[] = [
                'label' => $usage['title'],
                'url'   => CatalogPrintingUrlHelper::printing3dCatalog(location: $this->location, usageCode: $usage['id'])
            ];
        }
        return $result;
    }

    public function getLinksMaterials()
    {
        $materials       = $this->searchService->getMaterials();
        $materialsFilter = Yii::$app->setting->get('settings.catalogFooterLinks', []);

        $result = [];
        foreach ($materials as $material) {
            if (!in_array($material->filament_title, $materialsFilter['materials'])) {
                continue;
            }
            $result[] = [
                'label' => $material->title,
                'url'   => CatalogPrintingUrlHelper::printing3dCatalog(location: $this->location, materialCode: $material->getSlugifyFilament())
            ];
        }
        return $result;
    }

    public function getLinksTechnologies()
    {
        $techs           = $this->searchService->getTechnologies();
        $materialsFilter = Yii::$app->setting->get('settings.catalogFooterLinks', []);

        $result = [];
        foreach ($techs as $tech) {
            if (!in_array($tech->getTitleCode(), $materialsFilter['technologies'])) {
                continue;
            }
            $result[] = [
                'label' => $tech->title,
                'url'   => CatalogPrintingUrlHelper::printing3dCatalog(location: $this->location, technologyCode: $tech->getTitleCode())
            ];
        }
        return $result;
    }

    public function changeLanguage(string $lang)
    {
        $this->language = $lang;
        return $this;
    }
}