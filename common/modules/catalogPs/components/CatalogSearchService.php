<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\components;


use common\models\GeoCity;
use common\models\PrinterMaterial;
use common\models\PrinterTechnology;
use common\models\PrinterToMaterial;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\modules\catalogPs\repositories\PrinterMaterialGroupRepository;
use common\modules\catalogPs\repositories\PrinterMaterialRepository;
use common\modules\catalogPs\repositories\PrinterTechnologyRepository;
use common\modules\catalogPs\repositories\PrinterToMaterialRepository;
use common\modules\catalogPs\repositories\PsMachineDeliveryRepository;
use common\modules\catalogPs\repositories\PsPrinterMaterialRepository;
use common\modules\catalogPs\repositories\PsRepository;
use common\modules\catalogPs\repositories\UsageGroupsRepository;
use frontend\components\UserSessionFacade;
use lib\geo\models\Location;

class CatalogSearchService
{

    /**
     * @var PsPrinterMaterialRepository
     */
    public static $psMaterialsRepo;

    /**
     * @var PsMachineDeliveryRepository
     */
    public static $deliveryRepo;

    /**
     * @var PsRepository
     */
    public static $psRepoStatic;

    /**
     * @var PrinterTechnologyRepository
     */
    public $techRepo;

    /**
     * @var UsageGroupsRepository
     */
    public $usageRepo;

    /**
     * @var PsRepository
     */
    public $psRepo;

    /**
     * @var PrinterMaterialRepository
     */
    public $materialRepo;

    /**
     * @var PrinterMaterialGroupRepository
     */
    public $materialGroupRepo;

    public $printerToMaterialRepo;

    public function injectDependencies(
            PrinterTechnologyRepository $techRepo,
            UsageGroupsRepository $usageRepo,
            PrinterMaterialRepository $materialRepo,
            PrinterToMaterialRepository $printerToMaterialRepo,
            PrinterMaterialGroupRepository $materialGroupRepo,
            PsRepository $psRepo
    )
    {
        $this->techRepo = $techRepo;
        $this->usageRepo = $usageRepo;
        $this->materialRepo = $materialRepo;
        $this->materialGroupRepo = $materialGroupRepo;
        $this->printerToMaterialRepo = $printerToMaterialRepo;
        $this->psRepo = $psRepo;
    }

    /**
     * @param array $array
     * @return string
     */
    public function convertToFilterUrl(array $array)
    {
        $array = array_filter($array);
        $result = [];
        foreach($array as $k=>$v){
            if (is_array($v)) {
                $str = $k . '_' . implode('_', $v);
            } else {
                $str = $k . '-' . $v;
            }
            $result[] = $str;
        }
        return join("--", $result);
    }

    /**
     * @param $technologyCode
     * @return \common\models\PrinterTechnology|null
     */
    public function getTechnology($technologyCode)
    {
        $technology = $this->techRepo->getByCode($technologyCode);
        if (!$technology) {
            return null;
        }
        return $technology;
    }

    /**
     * @param $technologyId
     * @return \common\models\PrinterTechnology|null
     */
    public function getTechnologyById($technologyId)
    {
        $technology = $this->techRepo->getModelById($technologyId);
        if (!$technology) {
            return null;
        }
        return $technology;
    }
    /**
     * @param $usageCode
     * @return array
     */
    public function getUsage($usageCode)
    {
        return $this->usageRepo->getByCode($usageCode);
    }

    /**
     * @param $technologyCode
     * @return array
     */
    public function getUsageTypes($technologyCode)
    {
        if (!empty($technologyCode)) {
            $technology = $this->getTechnology($technologyCode);
            $usageTypes = $this->usageRepo->getAllByTechnology($technology->id);
        } else {
            $usageTypes = $this->usageRepo->getAll();
        }
        return $usageTypes;
    }

    /**
     * @param $technologyCode
     * @return array
     */
    public function getUsagesByTechnology($technologyCode)
    {
        $technology = $this->techRepo->getByCode($technologyCode);
        $usageTypes = $this->usageRepo->getAllByTechnology($technology->id);
        return $usageTypes;
    }

    /**
     * @return PrinterTechnology[]
     */
    public function getTechnologies()
    {
        return $this->techRepo->getAllModels();
    }


    /**
     * @param $materialSlug
     * @return PrinterMaterial
     */
    public function getMaterial($materialSlug)
    {
        return $this->materialRepo->getBySlug($materialSlug);
    }

    public function getMaterialByCode($code)
    {
        return $this->materialRepo->getByCode($code);
    }

    /**
     * @param $materialsSlug
     * @return PrinterMaterial
     */
    public function getMaterialsByCodes($materialsSlug)
    {
        $materials = [];
        if (is_array($materialsSlug)) {
            foreach ($materialsSlug as $materialCode) {
                $materials[]= $this->getMaterial($materialCode);
            }

        } else {
            $materials[] = $this->getMaterial($materialsSlug);
        }
        return $materials;
    }

    /**
     * @param $usageCode
     * @param $technologyCode
     *
     * @return PrinterMaterial[]
     */
    public function getMaterials($usageCode = null, $technologyCode = null)
    {
        if (!empty($usageCode)) {
            $usage = $this->getUsage($usageCode);
            $materials = $this->getMaterialsByUsage($usage);
        } else {
            if (!empty($technologyCode)) {
                $technology = $this->getTechnology($technologyCode);
                $materials = $this->getMaterialsByTechnology($technology);
            } else {
                $materials = $this->materialRepo->getAllModels();
            }
        }
        return $materials;
    }

    public function getActiveMaterials($usageCode = null, $technologyCode = null)
    {
        $materials = $this->getMaterials($usageCode, $technologyCode);
        usort(
            $materials,
            function ($item, $item2) {
                return strnatcmp($item->title, $item2->title);
            }
        );

        $result = [];
        foreach ($materials as $material){
            if (!$material->is_active) {
                continue;
            }
            $result[mb_strtolower($material->getSlugifyFilament())] = $material->title;
        }
        return $result;
    }

    public function getMaterialsByUsage($usage)
    {
        $materialGroups = [];
        foreach($usage['groups'] as $groupCode){
            $materialGroups[] = $this->materialGroupRepo->getByCode($groupCode);
        }
        if(empty($materialGroups)){
            return [];
        }
        $materials = $this->materialRepo->getModelsByGroups($materialGroups);

        return $materials;
    }

    public function getMaterialsByTechnology(PrinterTechnology $technology)
    {
        $materialIds = PrinterToMaterial::find()->select('material_id')
            ->joinWith('printer')
            ->andWhere(['printer.technology_id' => $technology->id, 'printer.is_active'=>1])
            ->groupBy('printer_to_material.material_id')->column();

        return $this->materialRepo->getModelsById($materialIds);
    }

    /**
     * @return PsPrinterMaterialRepository
     */
    public static function getPsMaterialRepo()
    {
        self::$psMaterialsRepo = self::$psMaterialsRepo ? : \Yii::createObject(PsPrinterMaterialRepository::class);
        return self::$psMaterialsRepo;
    }

    /**
     * @return PsMachineDeliveryRepository
     */
    public static function getDeliveryRepo()
    {
        static::$deliveryRepo = static::$deliveryRepo?:\Yii::createObject(PsMachineDeliveryRepository::class);
        return static::$deliveryRepo;
    }

    /**
     * @return PsRepository
     */
    public static function getPsRepo()
    {
        static::$psRepoStatic = static::$psRepoStatic ?: \Yii::createObject(PsRepository::class);
        return static::$psRepoStatic;
    }

    public function filteredTechnologies(?array $filterTechnologies = []): array
    {
        $technologyRecords = $this->getTechnologies();
        $techs = [];
        foreach ($technologyRecords as $tech) {
            if ($filterTechnologies && !in_array($tech->id, $filterTechnologies)) {
                continue;
            }
            $techs[mb_strtolower($tech->getTitleCode())] = $tech->getTitleCode();
        }
        return $techs;
    }
}