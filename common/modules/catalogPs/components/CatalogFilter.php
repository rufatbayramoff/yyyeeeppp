<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\components;


use common\models\PrinterMaterial;
use common\models\PrinterTechnology;
use common\models\Ps;
use common\modules\catalogPs\models\PsPrinterEntity;
use lib\geo\LocationUtils;
use lib\geo\models\Location;

/**
 * Class PsPrinterCatalogFilter
 *
 * works with items of PsPrinterEntity
 * - helps to filter by technology, location, material,
 * - helps to sort by price, distance
 *
 * @package common\modules\catalogPs\components
 */
class CatalogFilter
{
    /**
     * @var PsPrinterEntity[]
     */
    private $items;
    private $withInternational = false;

    public function __construct(array $items)
    {
        $this->items = $items;
    }


    /**
     * @param PrinterTechnology $technology
     */
    public function filterTechnology(PrinterTechnology $technology)
    {
        $this->items = array_filter(
            $this->items,
            function (PsPrinterEntity $item) use ($technology) {
                return ($item->technology_id == $technology->id);
            }
        );
    }

    /**
     * @param $usage
     * @return $this
     */
    public function filterUsage($usage)
    {
        $usageGroups = $usage['groups'];
        $this->items = array_filter(
            $this->items,
            function (PsPrinterEntity $item) use ($usageGroups) {
                foreach ($usageGroups as $group) {
                    if (in_array($group, $item->getMaterialGroupCodes())) {
                        return true;
                    }
                }
                return false;
            }
        );
    }

    /**
     * @param PrinterMaterial[] $material
     */
    public function filterMaterial($materials)
    {
        $this->items = array_filter(
            $this->items,
            function (PsPrinterEntity $item) use ($materials) {
                foreach ($materials as $material) {
                    if ($material && in_array($material->id, $item->getMaterialIds())) {
                        return true;
                    }
                }
                return false;
            }
        );
    }

    /**
     * with international delivery
     *
     * @param bool $flag
     */
    public function withInternational($flag = true)
    {
        $this->withInternational = $flag;
    }

    /**
     * @param Location $location
     */
    public function filterLocationCountry(Location $location)
    {
        if(!$location->country){
            return;
        }
        $this->items = array_filter(
            $this->items,
            function (PsPrinterEntity $item) use ($location) {
                $flag = ($item->country_iso == $location->country);
                if($this->withInternational){
                    $flag  = $flag || $item->hasInternationalDelivery();
                }
                return $flag;
            }
        );
    }

    public function getItems()
    {
        return $this->items;
    }

    /**
     * group by ps. only 1 printer from PS should be present.
     *
     * @param string $type
     */
    public function groupBy($type = 'ps_id')
    {
        $result = [];
        $has = [];
        foreach ($this->items as $item) {
            if (array_key_exists($item->{$type}, $has)) {
                $oldItem = $result[$has[$item->{$type}]];
                $oldItem->psAllMaterials = array_merge($oldItem->psAllMaterials, $item->getPsMaterialIds());
                $result[$oldItem->id] = $oldItem;
                continue;
            }
            $item->psAllMaterials = array_merge($item->psAllMaterials, $item->getPsMaterialIds());
            $has[$item->{$type}] = $item->id;
            $result[$item->id] = $item;
        }
        $this->items = $result;
    }

    /**
     * sort by distance for given location
     *
     * @param Location $location
     */
    public function sortByDistance(Location $location)
    {
        usort(
            $this->items,
            function ($item, $item2) use ($location) {
                $d1 = LocationUtils::getDistance($location->lat, $location->lon, $item->location_lat, $item->location_lon);
                $d2 = LocationUtils::getDistance($location->lat, $location->lon, $item2->location_lat, $item2->location_lon);
                return $d1 <=> $d2;
            }
        );
    }

    /**
     *  sort by price
     */
    public function sortByPrice()
    {
        usort(
            $this->items,
            function ($item, $item2) {
                $min1 = $item->min_order_price_gr;
                $min2 = $item2->min_order_price_gr;
                if (floatval($item->min_order_price_gr) == 0) {
                    $min1 = 99999;
                }
                if (floatval($item2->min_order_price_gr) == 0) {
                    $min2 = 99999;
                }
                return $min1 <=> $min2;
            }
        );
    }

    /**
     * sort by rating
     */
    public function sortByRating()
    {
        usort(
            $this->items,
            function ($item, $item2) {
                // S = R/(R+n)*A + n/(R+n)*a
                $R = $item->ps_rating_count ;
                $A = $item->ps_rating;
                $R2 = $item2->ps_rating_count ;
                $A2 = $item2->ps_rating;
                $n = 5;
                $a = 2;
                $rating1 = $R / ($R + $n) * $A + $n / ($R + $n) * $a;
                $rating2 = $R2 / ($R2 + $n) * $A2 + $n / ($R2 + $n) * $a;
                return $rating1 == $rating2 ? $item->ps_rating_count < $item2->ps_rating_count : $rating2 > $rating1;

                /*
                $rating1 = $item->ps_rating_count < 3 ? 0 : $item->ps_rating;
                $rating2 = $item2->ps_rating_count < 3 ? 0 : $item2->ps_rating;
                return $rating1 == $rating2 ? $item->ps_rating_count < $item2->ps_rating_count : $rating2 > $rating1; */
            }
        );
    }

    /**
     * sort by defined list, if ps links [ps1,ps2,ps3] display as it is.
     *
     * @param $psLinks
     */
    public function sortByManual($psLinks)
    {
        $psIndex = array_flip($psLinks);
        usort(
            $this->items,
            function ($item, $item2) use ($psIndex) {
                $ps = $item->getPs();
                $ps2 = $item2->getPs();
                $d1 = $psIndex[$ps->url];
                $d2 = $psIndex[$ps2->url];
                return $d1 <=> $d2;
            }
        );
    }

    public function filterActive()
    {
        $this->items = array_filter(
            $this->items,
            function (PsPrinterEntity $item) {
                /** @var Ps $ps */
                $ps = $item->getPs();
                $isActive = $ps->isActive();
                return $isActive;
            }
        );
    }

    /**
     * filter by given ps links
     *
     * @param $psLinks
     */
    public function filterByPsLinks($psLinks)
    {
        $this->items = array_filter(
            $this->items,
            function (PsPrinterEntity $item) use ($psLinks) {
                $ps = $item->getPs();
                return in_array($ps->url, $psLinks);
            }
        );
    }

    public function filterByPrinterId($printerId)
    {
        $this->items = array_filter(
            $this->items,
            function (PsPrinterEntity $item) use ($printerId) {
                return in_array($item->printer_id, (array)$printerId);
            }
        );
    }

    public function psExist()
    {
        $this->items = array_filter(
            $this->items,
            static function (PsPrinterEntity $item) {
                $psPrinter = $item->getPsPrinter();
                return $psPrinter && $psPrinter->ps && !$psPrinter->ps->getIsRejected();
            }
        );
    }

    /**
     * Filter by text (use sphinx search)
     * @param string $text
     */
    public function filterByText(string $text) : void
    {
        $psIds = (new \yii\sphinx\Query())
            ->select('ps_id')
            ->from('pscatalog')
            ->match(trim($text))
            ->groupBy('ps_id')
            ->showMeta(true)
            ->column();

        $psIds = array_map(function($val) {return (int) $val; }, $psIds);

        $this->items = array_filter($this->items, function (PsPrinterEntity $item) use ($psIds) {
            return in_array($item->ps_id, $psIds);
        });
    }
}