<?php


namespace common\modules\catalogPs\models;

use common\components\ArrayHelper;
use common\components\BaseForm;
use common\models\Company;
use common\models\CompanyService;
use common\models\CompanyServiceCategory;
use common\models\CuttingMachineProcessing;
use common\models\File;
use common\models\ProductCategory;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\query\CompanyServiceQuery;
use common\models\query\ProductCommonQuery;
use common\models\SiteTag;
use common\models\StoreOrderReview;
use common\modules\catalogPs\repositories\CompanyServiceCategoryRepository;
use common\modules\product\repositories\ProductCategoryRepository;
use common\services\OrderUploadService;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

class CatalogForm extends BaseForm
{

    /**
     * @var CompanyServiceCategory|null
     */
    public $category;
    public $location;

    public $search;

    /**
     * @var ProductCategory|null
     */
    public $productCategory;

    /**
     * @var string
     */
    public $orderDescription;

    /** @var File[] */
    public $uploadedFiles = [];

    /** @var string */
    public $uploadedFilesUuids;

    public $allowedOfferType = self::ALLOW_OFFER_ANY;

    public const DEFAULT_CATEGORY = 'any';

    public const ALLOW_OFFER_INSTANT_ONLY = 'instant';
    public const ALLOW_OFFER_QUOTE_ONLY   = 'quote';
    public const ALLOW_OFFER_ANY          = 'any';
    /**
     * @var CompanyServiceCategoryRepository
     */
    protected $companyServiceCategoryRepository;

    public function injectDependencies(CompanyServiceCategoryRepository $companyServiceCategoryRepository): void
    {
        $this->companyServiceCategoryRepository = $companyServiceCategoryRepository;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            ['search', 'trim']
        ];
    }

    public function load($data, $formName = '')
    {
        if (array_key_exists('search', $data)) {
            $this->search = (string)$data['search'];
        }
        if (array_key_exists('text', $data)) {
            $this->search = (string)$data['text'];
            $this->search = strip_tags($this->search);
            $this->search = trim($this->search);
        }
        if (array_key_exists('description', $data)) {
            $this->orderDescription = $data['description'];
        }

        if (array_key_exists('productCategory', $data)) {
            $this->productCategory = $data['productCategory'];
            $this->productCategory = ProductCategory::find()->active()->andWhere(['code' => $this->productCategory])->one();
        }

        if (array_key_exists('uploadedFilesUuids', $data)) {
            $this->uploadedFilesUuids = (string)$data['uploadedFilesUuids'];
            $uploadedFilesUuids       = explode('-', $this->uploadedFilesUuids);
            $orderUploadService       = \Yii::createObject(OrderUploadService::class);
            foreach ($uploadedFilesUuids as $fileUuid) {
                $this->uploadedFiles[] = $orderUploadService->getFileByUuid($fileUuid);
            }
            $this->allowedOfferType = self:: ALLOW_OFFER_QUOTE_ONLY;
        }
    }

    public function canInstantOrder()
    {
        return $this->allowedOfferType === self::ALLOW_OFFER_ANY || $this->allowedOfferType === self::ALLOW_OFFER_INSTANT_ONLY;
    }

    /**
     * @param array|null $params
     * @return ActiveDataProvider
     */
    public function search(): ActiveDataProvider
    {
        $query = Company::find()->active();
        if ($this->productCategory) {
            $productCategoryRepository = Yii::createObject(ProductCategoryRepository::class);
            $productSubCategories      = $productCategoryRepository->subCategories($this->productCategory);
            $productSubCategoriesIds   = ArrayHelper::getColumn($productSubCategories, 'id');
            $productSubCategoriesIds[] = $this->productCategory->id;
            $query->joinWith(['productCommons' => function (ProductCommonQuery $productCommonQuery) use ($productSubCategoriesIds) {
                $productCommonQuery->active()->statusPublished()
                    ->andWhere(['product_common.category_id' => $productSubCategoriesIds]);
            }]);
        } else {
            $query->addSelect(['ps.*', 'cut_price' => new Expression('max(cutting_machine_processing.cutting_price >' . CuttingMachineProcessing::MIN_CUTTING_LEN . ')')]);
            $query->joinWith([
                'companyServices' => function (CompanyServiceQuery $companyServiceQuery) {
                    $companyServiceQuery->visibleEverywhere()->moderated()->notDeleted();
                    $companyServiceQuery->joinWith(['cuttingMachineProcessings']);
                    $companyServiceQuery->joinWith(['location', 'category'], false);
                    $companyServiceQuery->joinWith(['psPrinter', 'psCncMachine'], false);
                    return $companyServiceQuery;
                }
            ]);
            if ($this->category) {
                $query->andWhere(['company_service_category.id' => $this->category->id]);
            }
        }

        $query->leftJoin(['review' => 'store_order_review'], "review.ps_id = ps.id AND review.status = '" . StoreOrderReview::ANSWER_STATUS_MODERATED . "'");

        $query->orderBy(new Expression('(AVG(review.rating_speed) + AVG(review.rating_quality) + AVG(review.rating_communication)) / 3 DESC'));
        $query->groupBy(['ps.id']);

        if ($this->search) {
            $query->leftJoin('company_service_tag', 'company_service_tag.company_service_id=company_service.id');
            $query->leftJoin('site_tag', 'site_tag.id=company_service_tag.tag_id');

            $tags   = SiteTag::find()
                ->leftJoin('site_tag_link', 'site_tag_link.site_tag_one=site_tag.id or site_tag_link.site_tag_two=site_tag.id')
                ->leftJoin('site_tag as site_tag_linked', 'site_tag_link.site_tag_one=site_tag_linked.id or site_tag_link.site_tag_two=site_tag_linked.id')
                ->andWhere(['or', ['site_tag.text' => $this->search], ['site_tag_linked.text' => $this->search]])
                ->andWhere(['site_tag.status' => SiteTag::STATUS_NEW])
                ->all();
            $tagsId = ArrayHelper::getColumn($tags, 'id');

            $query->andWhere([
                'or',
                ['like', 'ps.title', $this->search],
                ['site_tag.id' => $tagsId],
            ]);
        }

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }

    public function isCutting(): bool
    {
        if ($this->category) {
            return $this->category->isCutting();
        }
        return false;
    }

    public function isCncQuote(): bool
    {
        if ($this->category) {
            return $this->category->isCnc() && !$this->uploadedFilesUuids;
        }
        return false;
    }

    /**
     * @return array
     */
    public function categories(): array
    {
        return $this->companyServiceCategoryRepository->categoriesMenu();
    }

    protected function needRatingOrder()
    {
//        if ($this->category) {
//            return $this->category->isCnc() && !$this->uploadedFilesUuids;
//        }
//        return false;
    }
}