<?php

namespace common\modules\catalogPs\models;

use common\models\PsPrinter;

class PsPrinterCatalogEntity extends PsPrinter
{
    protected $psPrinter;
    public $ps_id;
    public $technology_id;

    public function getPsPrinter()
    {
        if (!$this->psPrinter) {
            $this->psPrinter = PsPrinter::tryFindByPk($this->id);
        }
        return $this->psPrinter;
    }
}