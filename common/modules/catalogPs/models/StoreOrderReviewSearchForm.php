<?php


namespace common\modules\catalogPs\models;


use common\components\BaseForm;
use common\models\CompanyServiceCategory;
use common\models\query\CompanyServiceQuery;
use common\models\query\PsQuery;
use common\models\query\StoreOrderQuery;
use common\models\StoreOrderReview;
use common\modules\catalogPs\components\CatalogSearchService;
use common\modules\catalogPs\repositories\PrinterMaterialRepository;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * @property $comboMaterials array
 * @property $comboTechnologies array
 * @property $categories array
 * */
class StoreOrderReviewSearchForm extends BaseForm
{

    public $material;
    public $technology;
    public $category;
    /**
     * @var CatalogSearchService
     */
    protected $catalogSearchService;


    public function injectDependencies(CatalogSearchService $catalogSearchService)
    {
        $this->catalogSearchService = $catalogSearchService;
    }

    public function getComboMaterials(): array
    {
        return $this->catalogSearchService->getActiveMaterials();
    }

    /**
     * @return array
     */
    public function getComboTechnologies(): array
    {
        return $this->catalogSearchService->filteredTechnologies();
    }

    /**
     * @return ActiveDataProvider
     */
    public function search(): ActiveDataProvider
    {
        $query = StoreOrderReview::find()
            ->joinWith(['ps' => function (PsQuery $psQuery) {
                $psQuery->active();
                return $psQuery;
            }])
            ->with(['order.user'])
            ->where([
                'store_order_review.status' => StoreOrderReview::STATUS_MODERATED
            ])
            ->orderBy(['id' => SORT_DESC]);
        if ($this->material || $this->technology || $this->category) {
            if ($this->category && !$this->is3dPrinting()) {
                $query->joinWith(['order.currentAttemp.machine.category']);
                $query->andWhere(['company_service_category.code' => $this->category]);
            }
            if ($this->technology || $this->is3dPrinting()) {
                $query->joinWith(['order.currentAttemp.machine.psPrinter']);
                $query->andWhere('ps_printer.id is not null');
                if ($this->technology) {
                    $query->joinWith(['order.currentAttemp.machine.psPrinter.printer.technology']);
                    $query->andWhere(['printer_technology.code' => $this->technology]);
                }
            }
            if ($this->material) {
                $query->joinWith(['order.storeOrderItems.model3dReplica.model3dTexture.printerMaterial']);
                $query->andWhere(['or', ['printer_material.title' => $this->material], ['printer_material.filament_title' => $this->material]]);
            }
        }
        //debugSql($query);exit;
        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 21
            ]
        ]);
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return [
            '3d-printing'                              => _t('site.main', '3D Printing'),
            CompanyServiceCategory::CODE_CNC           => _t('site.main', 'CNC Machining'),
            CompanyServiceCategory::CODE_LASER_CUTTING => _t('site.main', 'Cutting'),
        ];
    }

    protected function is3dPrinting(): bool
    {
        return $this->category === '3d-printing';
    }

    protected function filerQuery()
    {
        $query = (new Query())->from('store_order');
        $query->innerJoin('store_order_item','store_order_item.order_id=store_order.id');
        $query->innerJoin('store_order_attemp','store_order_attemp.id=store_order.current_attemp_id');
        $query->innerJoin('company_service','company_service.id=store_order_attemp.machine_id');
        if($this->category && !$this->is3dPrinting()) {
            $query->innerJoin('company_service_category','company_service_category.id=company_service.category_id');
            $query->andWhere(['company_service_category.code' => $this->category]);
        }
        if($this->technology || $this->is3dPrinting()) {
            $query->innerJoin('ps_printer','ps_printer.id=company_service.ps_printer_id');
            if($this->technology) {
                $query->innerJoin('printer', 'printer.id=ps_printer.printer_id');
                $query->innerJoin('printer_technology', 'printer_technology.id=printer.technology_id');
                $query->andWhere(['printer_technology.code' => $this->technology]);
            }

        }
        if($this->material) {
            $query->innerJoin('model3d_replica','model3d_replica.id=store_order_item.model3d_replica_id');
            $query->innerJoin('model3d_texture','model3d_texture.id=model3d_replica.model3d_texture_id');
            $query->innerJoin('printer_material','printer_material.id=model3d_texture.printer_material_id');
            $query->innerJoin('printer_color','printer_color.id=model3d_texture.printer_material_id');
            $query->andWhere(['printer_material.title' => $this->material]);
        }
        $query->select('store_order.id');
        return $query;

    }
}