<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\models;


use common\components\ArrayHelper;
use common\models\File;
use common\models\PrinterTechnology;
use common\models\PsPrinter;
use common\models\PsPrinterTest;
use common\models\repositories\FileRepository;
use common\modules\catalogPs\components\CatalogSearchService;
use yii\db\Exception;

/**
 * Class PsPrinterEntityMeta
 * additional meta information based on ps printer
 * - like layer resolution, test order, certification and etc.
 *
 * @package common\modules\catalogPs\models
 */
class PsPrinterEntityMeta
{
    /**
     * @var PrinterTechnology
     */
    public $technology;

    /**
     * @var PsPrinterEntity
     */
    private $entity;
    /**
     * @var CatalogSearchService
     */
    private $catalogService;

    /**
     * @var PsPrinter
     */
    private $psPrinter;

    /**
     * PsPrinterEntityMeta constructor.
     *
     * @param PsPrinterEntity $entity
     * @param CatalogSearchService $catalogService
     */
    public function __construct(PsPrinterEntity $entity, CatalogSearchService $catalogService)
    {
        $this->entity = $entity;
        $this->psPrinter = PsPrinter::findByPk($this->entity->id);
        $this->catalogService = $catalogService;

    }

    /**
     * @return PrinterTechnology|null
     */
    public function getTechnology()
    {
        if ($this->technology) {
            return $this->technology;
        }
        $this->technology = $this->catalogService->getTechnologyById($this->entity->technology_id);
        return $this->technology;
    }

    /**
     * @return float[]
     */
    public function getBuildVolume()
    {
        return $this->psPrinter->getBuildVolume();
    }

    /**
     * @param string $type
     * @return string
     */
    public function getLayerResolution($type = 'high')
    {
        $res = ArrayHelper::map(
            $this->psPrinter->getProperties(
                [
                    'code' => [PsPrinter::PROPERTY_LAYER_RESOLUTION_LOW, PsPrinter::PROPERTY_LAYER_RESOLUTION_HIGH]
                ]
            ),
            'code',
            'value'
        );
        if ($type == 'high') {
            return $res[PsPrinter::PROPERTY_LAYER_RESOLUTION_HIGH] ?? '-';
        }

        return sprintf(
            "High: %s <br/>Low: %s",
            $res[PsPrinter::PROPERTY_LAYER_RESOLUTION_HIGH] ?? '-',
            $res[PsPrinter::PROPERTY_LAYER_RESOLUTION_LOW] ?? '-'
        );
    }

    public function getDeliveryDetails()
    {

    }

    /**
     * @return File[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getTestOrderImages()
    {
        $printer = $this->psPrinter;

        if ($printer->test) {

            return array_map(function (File $file) {
                if (!$file->is_public) {
                    $file->setPublicMode(true);
                    $filerepository = \Yii::CreateObject(FileRepository::class);
                    $filerepository->save($file);
                }
                return $file;
            }, $printer->test->getFiles());
        }

        if ($printer->is_test_order_resolved) {
            $sql = "select mf.file_id from  store_order
                    left join store_order_attemp attempt ON attempt.id=store_order.current_attemp_id
                    left join store_order_attempt_moderation_file mf on mf.attempt_id=store_order.current_attemp_id
                    left join company_service on company_service.id=attempt.machine_id
                    where company_service.ps_printer_id=:printer_id and store_order.user_id=:user_id";
            $sqlStm = \Yii::$app->db->pdo->prepare($sql);
            $sqlStm->execute(['printer_id' => $printer->id, 'user_id' => 1]);
            $fileId = $sqlStm->fetchColumn(0);
            if ($fileId) {
                $file = File::findByPk($fileId);
                if (!$file->is_public) {
                    // Fix: Move TestOrder Images to public mode
                    $file->setPublicMode(true);
                    $filerepository = \Yii::CreateObject(FileRepository::class);
                    $filerepository->save($file);
                }
                return [$file];
            }
        }
        return [];
    }

    public function getSlug()
    {
        return $this->psPrinter->printer->getSlug();
    }
}