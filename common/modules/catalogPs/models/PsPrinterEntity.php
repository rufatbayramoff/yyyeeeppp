<?php
/**
 * User: nabi
 */

namespace common\modules\catalogPs\models;


use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\factories\LocationFactory;
use common\models\PrinterMaterial;
use common\models\PsPrinter;
use common\models\PsPrinterMaterial;
use common\models\UserLocation;
use common\modules\catalogPs\components\CatalogFilter;
use common\modules\catalogPs\components\CatalogSearchService;
use common\modules\catalogPs\repositories\PsPrinterEntityRepository;
use common\modules\company\components\CompanyServiceInterface;
use lib\geo\models\Location;
use yii\base\BaseObject;

/**
 * Class PsPrinterEntity
 *
 * @see PsPrinterEntityRepository
 *
 * @package common\modules\catalogPs\models
 */
class PsPrinterEntity extends BaseObject implements CompanyServiceInterface
{
    public $id;
    public $ps_id;
    public $ps_rating;
    public $ps_rating_count;
    public $title;
    public $description;
    public $min_order_price;
    public $min_order_price_gr;
    public $location_id;
    public $test_status;
    public $currency_iso;
    public $location_lat;
    public $location_lon;
    public $technology_id;
    public $printer_id;
    public $ps_material_ids;
    public $material_ids;
    public $material_group_codes;
    public $ps_delivery_ids;
    public $delivery_type_ids;
    public $lastonline_at;
    public $country_id;
    public $country_iso;
    public $region;
    public $city;

    /**
     * any addiontional info for other views,
     * like tech, certification, and etc,
     * @var PsPrinterEntityMeta
     */
    public $meta;
    /**
     * set while grouping by ps
     * @see CatalogFilter::groupByPs()
     * @var array
     */
    public $psAllMaterials = [];

    public function getMaterialIds()
    {
        return array_filter(array_map('intval', explode(',', $this->material_ids)));
    }

    public function getPsMaterialIds()
    {
        return array_filter(array_map('intval', explode(',', $this->ps_material_ids)));
    }

    public function getDeliveryTypeIds()
    {
        return array_unique(array_filter(array_map('intval', explode(',', $this->delivery_type_ids))));
    }

    public function getPsDeliveryIds()
    {
        return array_filter(array_map('intval', explode(',', $this->ps_delivery_ids)));
    }

    public function getMaterialGroupCodes()
    {
        return array_unique(array_filter(explode(',', $this->material_group_codes)));
    }

    public function getPs()
    {
        $psRepo = CatalogSearchService::getPsRepo();
        return $psRepo->getModelById($this->ps_id);
    }

    /**
     * @return PsPrinterMaterial[]
     */
    public function getMaterials()
    {
        $ids           = $this->getPsMaterialIds();
        $materialsRepo = CatalogSearchService::getPsMaterialRepo();
        $result        = [];
        foreach ($ids as $id) {
            $result[] = $materialsRepo->getModelById($id);
        }
        $result = array_filter($result);
        return $result;
    }

    public function getAllPsMaterials()
    {
        $materialsRepo = CatalogSearchService::getPsMaterialRepo();
        $result        = [];
        $ids           = $this->getPsMaterialIds() + $this->psAllMaterials;
        $ids           = array_unique($ids);
        foreach ($ids as $id) {
            $printerMaterial = $materialsRepo->getModelById($id);
            if (!$printerMaterial) {
                continue;
            }
            $result[$printerMaterial->material_id] = $printerMaterial;
        }

        $result = array_filter($result);
        return $result;
    }

    public function getDeliveryTypes()
    {
        $ids    = $this->getPsDeliveryIds();
        $repo   = CatalogSearchService::getDeliveryRepo();
        $result = [];
        foreach ($ids as $id) {
            $result[] = $repo->getModelById($id);
        }
        $result = array_filter($result);
        return $result;
    }

    public function hasInternationalDelivery()
    {
        $deliveryTypeIds = $this->getDeliveryTypeIds();
        return in_array(DeliveryType::INTERNATIONAL_STANDART_ID, $deliveryTypeIds);
    }

    /**
     * @return string
     */
    public function getShippingPrice()
    {
        $shippingPrice   = _t('ps.shipping', 'Standard Flat Rate');
        $printerDelivery = $this->getDeliveryTypes();
        foreach ($printerDelivery as $psDelivery) {
            $price = $psDelivery->carrier_price
                ? displayAsCurrency($psDelivery->carrier_price, $this->currency_iso) : '';
            if (!empty($price) && $psDelivery->carrier != 'treatstock') {
                $shippingPrice = $price;
                break;
            }
        }
        return $shippingPrice;
    }

    public function getTitleLabel()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getOwner()
    {
        return $this->getPs()->user;
    }

    public function getCompanyService(): ?CompanyService
    {
        return $this->getPsPrinter()->companyService;
    }

    public function getImages()
    {
        return [];
    }

    public function getType()
    {
        return CompanyServiceInterface::TYPE_PRINTER;
    }

    public function getCategory()
    {
        return null;
    }

    public function getLogoImage()
    {
        return null;
    }

    public function getPsPrinter()
    {
        return PsPrinter::findOne($this->printer_id);
    }

    public function getTsCertificationClass()
    {
        /** @var PsPrinter $psPrinter */
        $psPrinter = $this->getPsPrinter();
        if (!$psPrinter) {
            return false;
        }
        return $psPrinter->printer->tsCertificationClass;
    }

    public function getLocation(): ?Location
    {
        if (!$this->location_id) {
            return null;
        }
        $location = UserLocation::findOne(['id' => $this->location_id]);
        return LocationFactory::createFromUserLocation($location);
    }
}