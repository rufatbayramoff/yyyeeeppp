<?php

namespace common\modules\catalogPs;

use yii\base\Module;

/**
 * Date: 03.07.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class CatalogPsModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\catalogPs\controllers';


    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();
    }
}
