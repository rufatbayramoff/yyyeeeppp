<?php

namespace common\modules\catalogPs\helpers;

use common\models\GeoCity;
use frontend\components\UserSessionFacade;
use lib\geo\models\Location;

class CatalogPrintingUrlHelper
{
    public static function landingPage()
    {
        return '/3d-printing-services';
    }

    public static function getLocationString(?Location $location = null)
    {
        $locationString = 'all-locations';
        if ($location && is_object($location) && $location->city !== 'all-locations' && $location->city !== 'all-locations----') {
            $locationString = mb_strtolower(UserSessionFacade::getLocationAsString($location));
        }
        return $locationString;
    }

    public static function printing3dCatalog(?Location $location = null, ?string $text = '', ?string $usageCode = '', ?string $technologyCode = '', ?string $materialCode = '', ?string $sort = '', ?int $international = 0, ?int $page=0)
    {
        $locationString = self::getLocationString($location);
        $resultUrl      = '/3d-printing-services?';
        $searchParams   = self::convertToFilterUrl([
            'location'      => $locationString,
            'usage'         => $usageCode,
            'technology'    => $technologyCode,
            'material'      => $materialCode,
            'sort'          => $sort,
            'international' => $international,
            'text'          => $text,
            'page'          => $page,
        ]);

        $resultUrl = $resultUrl . $searchParams;
        $resultUrl = substr($resultUrl, 0, -1);
        return $resultUrl;
    }

    public static function convertToFilterUrl($params)
    {
        $url = '';
        foreach ($params as $key => $value) {
            if ($value) {
                if ($key !== 'text') {
                    $value = mb_strtolower($value);
                }
                $url .= $key . '=' . urlencode($value) . '&';
            }
        }
        return $url;

    }
}