<?php


namespace common\modules\tsInternalDeposit\services;


use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\repositories\UserRepository;
use common\models\TsInternalPurchase;
use common\models\user\UserIdentityProvider;
use common\modules\payment\factories\PaymentInvoiceFactory;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\modules\tsInternalPurchase\factories\TsInternalPurchaseFactory;
use common\modules\tsInternalPurchase\repositories\TsInternalPurchaseRepository;
use frontend\modules\tsInternalPurchase\forms\DepositForm;

class TsInternalDepositService
{
    /**
     * @var TsInternalPurchaseFactory
     */
    private $factory;
    /**
     * @var PaymentInvoiceFactory
     */
    private $invoiceFactory;
    /**
     * @var TsInternalPurchaseRepository
     */
    private $tsInternalPurchaseRepository;
    /**
     * @var PaymentInvoiceRepository
     */
    private $paymentInvoiceRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserIdentityProvider
     */
    private $identityProvider;

    public function __construct(
        TsInternalPurchaseFactory $factory,
        PaymentInvoiceFactory $invoiceFactory,
        TsInternalPurchaseRepository $tsInternalPurchaseRepository,
        PaymentInvoiceRepository $paymentInvoiceRepository,
        UserIdentityProvider $identityProvider

    ) {
        $this->factory = $factory;
        $this->invoiceFactory = $invoiceFactory;
        $this->tsInternalPurchaseRepository = $tsInternalPurchaseRepository;
        $this->paymentInvoiceRepository = $paymentInvoiceRepository;
        $this->identityProvider = $identityProvider;
    }

    /**
     * @param DepositForm $form
     * @return TsInternalPurchase
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function createOrder(DepositForm $form): TsInternalPurchase
    {
        $tsInternalPurchase = $this->factory->makeDeposit($form->user);
        $paymentInvoice = $this->invoiceFactory->createWithPaymentMethodsByForDeposit($tsInternalPurchase, $form->money());
        $this->tsInternalPurchaseRepository->save($tsInternalPurchase);
        $this->paymentInvoiceRepository->save($paymentInvoice);
        $this->fixPrimaryInvoice($tsInternalPurchase);

        return $tsInternalPurchase;
    }

    /**
     * @param TsInternalPurchase $tsInternalPurchase
     * @throws \common\components\exceptions\BusinessException
     */
    protected function fixPrimaryInvoice(TsInternalPurchase $tsInternalPurchase): void
    {
        /** @var PaymentInvoice $primaryInvoice */
        $primaryInvoice = $this->paymentInvoiceRepository->getNewInvoiceByTsInternalPurchaseAndPaymentMethod(
            $tsInternalPurchase,
            PaymentTransaction::VENDOR_BANK_TRANSFER
        );
        $tsInternalPurchase->primary_invoice_uuid = $primaryInvoice->uuid;
        $tsInternalPurchase->populateRelation('primaryInvoice', $primaryInvoice);
        $tsInternalPurchase->safeSave();
    }
}