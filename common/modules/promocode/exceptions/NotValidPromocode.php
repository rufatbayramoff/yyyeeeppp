<?php
namespace common\modules\promocode\exceptions;

use yii\base\UserException;

/**
 * Date: 05.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class NotValidPromocode extends UserException
{
    public function getName()
    {
        return _t('site.promo', 'Invalid Promo Code');
    }
}