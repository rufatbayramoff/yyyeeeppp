<?php
/**
 * User: nabi
 */

namespace common\modules\promocode\components;

use common\interfaces\DiscountsResolverInterface;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\Promocode;
use common\models\StoreOrder;
use common\modules\payment\factories\PaymentInvoiceFactory;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use Yii;
use yii\base\BaseObject;

/**
 * Class PromocodeDiscountService
 * @package common\modules\promocode\components
 *
 * @property PaymentInvoiceFactory $invoiceFactory
 * @property PaymentInvoiceRepository $invoiceRepository
 * @property PromocodeProcessor $discountProcessor
 */
class PromocodeDiscountService extends BaseObject
{
    protected $invoiceFactory;

    protected $invoiceRepository;

    protected $discountProcessor;

    public function injectDependencies(
        PaymentInvoiceFactory $invoiceFactory,
        PaymentInvoiceRepository $invoiceRepository,
        PromocodeProcessor $discountProcessor
    ) {
        $this->invoiceFactory = $invoiceFactory;
        $this->invoiceRepository = $invoiceRepository;
        $this->discountProcessor = $discountProcessor;
    }

    /**
     * @param StoreOrder $order
     * @param Promocode $promocode
     *
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \Exception
     */
    public function applyPromocodeDiscounts(StoreOrder $order, Promocode $promocode): void
    {
        /** @var PaymentInvoiceRepository $paymentInvoiceRepository */
        $paymentInvoiceRepository = Yii::createObject(PaymentInvoiceRepository::class);

        $invoices = $paymentInvoiceRepository->getNewInvoiceByStoreOrder($order);
        $primaryInvoice = $order->getPrimaryInvoice();

        foreach ($invoices as $invoice) {
            $newInvoice = $this->invoiceFactory->createByPaymentInvoicePromoCode($invoice, $promocode);
            $this->invoiceRepository->save($newInvoice);

            $this->discountProcessor->bindToInvoice($newInvoice, $promocode);

            if ($newInvoice->parent_invoice_uuid === $primaryInvoice->uuid) {
                $order->primary_payment_invoice_uuid = $newInvoice->uuid;
                $order->safeSave();
            }
        }
    }

    /**
     * @param StoreOrder $order
     *
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\UserException
     * @throws \yii\db\StaleObjectException
     */
    public function removePromocodeDiscounts(StoreOrder $order): void
    {
        /** @var PaymentInvoiceRepository $paymentInvoiceRepository */
        $paymentInvoiceRepository = Yii::createObject(PaymentInvoiceRepository::class);

        $invoices = $paymentInvoiceRepository->getNewInvoicePromoCodeByStoreOrder($order);

        $order->primary_payment_invoice_uuid = $order->getPrimaryInvoice()->parentInvoice->uuid;
        $order->safeSave(['primary_payment_invoice_uuid']);

        foreach ($invoices as $invoice) {
            $invoice->status = PaymentInvoice::STATUS_CANCELED;
            $invoice->safeSave(['status']);

            $invoice->storeOrderPromocode->delete();
        }
    }
}