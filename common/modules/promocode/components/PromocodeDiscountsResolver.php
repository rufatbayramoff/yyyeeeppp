<?php
/**
 * Date: 02.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\promocode\components;


use common\interfaces\DiscountsResolverInterface;

class PromocodeDiscountsResolver implements DiscountsResolverInterface
{

    /**
     * @return \common\models\Promocode[]
     */
    public function getDiscounts()
    {
        $service = new PromocodeApplyService();
        $discounts = $service->getAppliedPromos();
        return $discounts;
    }
}