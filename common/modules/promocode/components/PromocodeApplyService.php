<?php

namespace common\modules\promocode\components;


use common\components\exceptions\BusinessException;
use common\models\PaymentInvoice;
use common\models\Promocode;
use frontend\models\user\UserFacade;
use yii\base\BaseObject;
use yii\base\UserException;

/**
 * Date: 01.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class PromocodeApplyService extends BaseObject
{
    const REPO_SESSION = 'promocode_apply_session';

    /**
     * @var PromocodeAppliedRepository
     */
    private $repoSession;

    /**
     * @var PromocodeUsageValidator
     */
    private $validator;

    public function init()
    {
        parent::init();
        // if no repoSession specified directly, try to find in session
        if (!$this->repoSession) {
            $repoSession = $this->getSession()->get(self::REPO_SESSION, false);
            $this->repoSession = $repoSession ? unserialize($repoSession) : new PromocodeAppliedRepository(); // @TODO - SecurityFix required, remove unserialize
        }
        if(!$this->validator){
            $this->validator = new PromocodeUsageValidator();
            $this->validator->setCurrentUser(is_guest() ? null : UserFacade::getCurrentUser());
        }
    }

    /**
     * save repo to session
     */
    public function saveRepoSession()
    {
        $this->getSession()->set(self::REPO_SESSION, serialize($this->repoSession));
    }

    /**
     * remove promo
     *
     * @param Promocode $promocode
     * @return bool
     */
    public function removePromo(Promocode $promocode)
    {
        return $this->repoSession->remove($promocode);
    }

    /**
     * get applied promos
     * @return Promocode[]
     */
    public function getAppliedPromos()
    {
        return $this->repoSession->getAll();
    }

    public function validatePromo(Promocode $promocode)
    {
        return $this->validator->tryValidate($promocode);
    }

    /**
     * @return mixed|\yii\web\Session
     */
    public function getSession()
    {
        return \Yii::$app->session;
    }

    /**
     * @param PromocodeAppliedRepository $repo
     */
    public function setRepoSession(PromocodeAppliedRepository $repo)
    {
        $this->repoSession = $repo;
    }

    /**
     *
     */
    public function clearRepo()
    {
        $this->repoSession = new PromocodeAppliedRepository();
        $this->saveRepoSession();
    }

    /**
     * @param Promocode $promo
     * @param PaymentInvoice $invoice
     *
     * @return bool
     */
    public function validatePromoByPrices(Promocode $promo, PaymentInvoice $invoice): bool
    {
        $result = false;

        switch($promo->discount_for){
            case Promocode::DISCOUNT_FOR_MODEL:
                $model = $invoice->storeOrderAmount->getAmountModel3d();

                if($model && $model->getAmount() > 0){
                    $result = true;
                }

                break;
            case Promocode::DISCOUNT_FOR_PRINT:
                $print = $invoice->storeOrderAmount->getAmountPrint();

                if($print && $print->getAmount() > 0){
                    $result = true;
                }
                break;
            case Promocode::DISCOUNT_FOR_DELIVERY:
                $delivery = $invoice->storeOrderAmount->getAmountShipping();

                if($delivery && $delivery->getAmount() > 0){
                    $result = true;
                }
                break;
            case Promocode::DISCOUNT_FOR_SERVICE_FEE:
                $fee = $invoice->storeOrderAmount->getAmountFee();

                if($fee && $fee->getAmount() > 0){
                    $result = true;
                }
                break;
            case Promocode::DISCOUNT_FOR_ALL:
                $all = $invoice->getAmountTotal();

                if($all && $all->getAmount() > 0){
                    $result = true;
                }

                break;
        }

        return $result;
    }

    /**
     * validate by price type from promo
     * if type discount for print, checks minimum print price (in promocode.order_total_from)
     *
     * @param Promocode $promo
     * @param PaymentInvoice $invoice
     * @return bool
     * @throws UserException
     */
    public function validateByTotalPrice(Promocode $promo, PaymentInvoice $invoice): bool
    {
        $promocodePriceFrom = (float) $promo->order_total_from;

        if(empty($promocodePriceFrom)){
            return true;
        }

        $orderPrice = $invoice->getAmountTotal()->getAmount();

        $feeType = $promo->discount_for;

        switch($promo->discount_for){
            case Promocode::DISCOUNT_FOR_ALL:
                $orderPrice = $invoice->getAmountTotal()->getAmount();
                $feeType = _t('site.payment', 'total price');
                break;
            case Promocode::DISCOUNT_FOR_MODEL:
                $model = $invoice->storeOrderAmount->getAmountModel3d();
                $orderPrice = $model ? $model->getAmount() : null;
                $feeType = _t('site.payment', 'Model Fee');
                break;
            case Promocode::DISCOUNT_FOR_PRINT:
                $print = $invoice->storeOrderAmount->getAmountPrint();
                $orderPrice = $print ? $print->getAmount() : null;
                $feeType = _t('site.payment', 'Manufacturing Fee');
                break;
            case Promocode::DISCOUNT_FOR_DELIVERY:
                $delivery = $invoice->storeOrderAmount->getAmountShipping();
                $orderPrice = $delivery ? $delivery->getAmount() : null;
                $feeType = _t('site.payment', 'Delivery Fee');
                break;
            case Promocode::DISCOUNT_FOR_SERVICE_FEE:
                $fee = $invoice->storeOrderAmount->getAmountFee();
                $orderPrice = $fee ? $fee->getAmount() : null;
                $feeType = _t('site.payment', 'Service Fee');
                break;
        }

        if ($promocodePriceFrom > $orderPrice || $orderPrice === null) {
            throw new BusinessException(
                _t(
                    'site.order',
                    'This coupon code is only redeemable for orders with a minimum {feeType} of {price}',
                    ['feeType'=> $feeType, 'price'=>displayAsCurrency($promocodePriceFrom, $promo->discount_currency)]
                )
            );
        }

        return true;
    }

    /**
     * @param PaymentInvoice $invoice
     *
     * @throws UserException
     */
    public function validateBeforeRemove(PaymentInvoice $invoice): void
    {
        if (!$invoice->parentInvoice || !$invoice->storeOrderPromocode) {
            throw new BusinessException(
                _t(
                    'site.order',
                    'No active promotional codes'
                )
            );
        }
    }
}