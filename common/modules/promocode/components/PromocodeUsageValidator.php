<?php
/**
 * Date: 01.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\promocode\components;


use common\models\Promocode;
use common\models\PromocodeUsage;
use common\models\User;
use common\modules\promocode\exceptions\NotValidPromocode;

class PromocodeUsageValidator
{

    /**
     * @var User
     */
    protected $currentUser;

    /**
     * try to validate
     *
     * @param Promocode $promoCode
     * @return bool
     * @throws \yii\base\Exception
     * @throws NotValidPromocode
     */
    public function tryValidate(Promocode $promoCode)
    {
        $errorMsg = _t('site.promo', 'Promo code is invalid or already used');
        $isValid = $this->validate($promoCode);
        if (!$isValid) {
            throw new NotValidPromocode($errorMsg);
        }
        return true;
    }

    /**
     * @param Promocode $promoCode
     * @return bool
     */
    public function validate(Promocode $promoCode)
    {
        $isValid  = $this->validateFlags($promoCode);
        $isValid = $isValid  && $this->validateType($promoCode);
        $isValid = $isValid  && $this->validateDate($promoCode);
        return $isValid;
    }

    /**
     * @param Promocode $promoCode
     * @return bool
     */
    protected function validateFlags(Promocode $promoCode)
    {
        return $promoCode->is_active && $promoCode->is_valid;
    }

    /**
     * @param Promocode $promoCode
     * @return bool
     */
    protected function validateType(Promocode $promoCode)
    {
        $result = true;
        switch ($promoCode->usage_type) {
            case Promocode::USAGE_TYPE_ONE_PER_USER:
                if (!$this->currentUser) {
                    $result = false;
                } else {
                    $count = (int)PromocodeUsage::find()->where(['promocode_id' => $promoCode->id, 'user_id' => $this->currentUser->id])->count();
                    $result = $count === 0;
                }
                break;
            case Promocode::USAGE_TYPE_ONE:
                $count = (int)PromocodeUsage::find()->where(['promocode_id' => $promoCode->id])->count();
                $result = $count === 0;
                break;
            case Promocode::USAGE_TYPE_MANY:
                $result = true;
                break;

        }
        return $result;
    }

    /**
     * @param Promocode $promoCode
     * @return bool
     */
    protected function validateDate(Promocode $promoCode)
    {
        $now = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $validFrom = (new \DateTime($promoCode->valid_from))->setTimezone(new \DateTimeZone('UTC'));
        $validTo = (new \DateTime($promoCode->valid_to))->setTimezone(new \DateTimeZone('UTC'));
        return $now->getTimestamp() >= $validFrom->getTimestamp() && $now->getTimestamp() <= $validTo->getTimestamp();
    }

    public function setCurrentUser(User $user = null)
    {
        $this->currentUser = $user;
    }
}