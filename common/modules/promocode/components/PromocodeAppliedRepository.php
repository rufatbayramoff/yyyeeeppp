<?php
/**
 * Date: 01.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\promocode\components;
use common\interfaces\RepositoryInterface;
use common\interfaces\StatePersistenInterface;
use common\models\Promocode;

/**
 *
 * Repository of applied promo codes
 * uses session to store applied promos
 *
 * @package common\modules\promocode\components
 */
class PromocodeAppliedRepository implements RepositoryInterface, StatePersistenInterface
{

    public $promocodes = [];
    public $promocodeKeys = [];

    /**
     * @param Promocode $promocode
     * @return bool
     */
    public function put($promocode)
    {
        $this->promocodes[$promocode->code] = $promocode;
        return true;
    }

    public function remove($promocode)
    {
        if(array_key_exists($promocode->code, $this->promocodes)){
            unset($this->promocodes[$promocode->code]);
            return true;
        }
        return false;
    }

    public function get($code)
    {
        if(array_key_exists($code, $this->promocodes)){
            return $this->promocodes[$code];
        }
        return null;
    }

    public function getAll()
    {
        return $this->promocodes;
    }

    private function refreshPromocodeKeys()
    {
        $this->promocodeKeys = [];
        foreach($this->promocodes as $k=>$v){
            $this->promocodeKeys[] = $k;
        }
    }

    private function loadPromocodes(array $codes)
    {
        if($codes){
            $promocodes = Promocode::findAll(['code'=>$codes]);
            foreach($promocodes as $promo){
                $this->promocodes[$promo->code] = $promo;
            }
        }
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        $this->refreshPromocodeKeys();
        return ['promocodeKeys'];
    }

    /**
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function __wakeup()
    {
        $this->loadPromocodes($this->promocodeKeys);
    }
}