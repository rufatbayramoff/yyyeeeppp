<?php
/**
 * Date: 02.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\promocode\components;

use common\components\DateHelper;
use common\models\PaymentInvoice;
use common\models\Promocode;
use common\models\PromocodeUsage;
use common\models\StoreOrderPromocode;

class PromocodeProcessor
{
    /**
     * @param PaymentInvoice $invoice
     * @param Promocode $promoCode
     */
    public function bindToInvoice(PaymentInvoice $invoice, Promocode $promoCode): void
    {
        $discount = $invoice->getAmountByType(PaymentInvoice::ACCOUNTING_TYPE_PROMO_CODE);

        if ($discount) {
            StoreOrderPromocode::addRecord([
                'invoice_uuid'    => $invoice->uuid,
                'promocode_id'    => $promoCode->id,
                'created_at'      => DateHelper::now(),
                'amount'          => $discount->getAmount(),
                'amount_currency' => $discount->getCurrency(),
            ]);

            $invoice->refresh();
        }
    }

    /**
     * @param PaymentInvoice $invoice
     */
    public function markAsUsed(PaymentInvoice $invoice): void
    {
        if ($invoice->storeOrderPromocode) {
            PromocodeUsage::addRecord([
                'user_id'         => $invoice->storeOrder->user_id,
                'promocode_id'    => $invoice->storeOrderPromocode->promocode_id,
                'used_at'         => $invoice->storeOrder->created_at,
                'invoice_uuid'    => $invoice->uuid,
                'amount'          => $invoice->storeOrderPromocode->amount,
                'amount_currency' => $invoice->storeOrderPromocode->amount_currency
            ]);
        }
    }
}