<?php
/**
 * Date: 02.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\promocode\components;

use common\components\PaymentExchangeRateFacade;
use common\models\PaymentInvoice;
use common\models\Promocode;
use lib\money\Money;

class PromocodeDiscountCalc
{
    /**
     * @param Promocode $code
     * @param Money $price
     * @return Money
     */
    public static function getDiscount(Promocode $code, Money $price): Money
    {
        $resultPrice = 0;
        if ($code->isFixedDiscount()) {
            $resultPrice = self::normalizeAmountCurrency($code, $price->getCurrency());
        } else if ($code->isPercentDiscount()) {
            $resultPrice = $price->getAmount() * $code->discount_amount / 100;
        }
        // if discount goes negative
        if($resultPrice > $price->getAmount()){
            $resultPrice  = $price->getAmount();
        }
        return Money::create($resultPrice, $price->getCurrency());
    }

    /**
     * @param Promocode[] $promoCodes
     * @param $userCurrency
     * @return array
     * @internal param array $orderPrices
     */
    public static function getDiscountSettings(array $promoCodes, $userCurrency): array
    {
        $result = [];
        foreach($promoCodes as $promoCode){
            $discountAmount = self::normalizeAmountCurrency($promoCode, $userCurrency);
            $result[] = [
                'discount_for' => $promoCode->discount_for,
                'discount_type' => $promoCode->discount_type,
                'discount_amount' => $discountAmount
            ];
        }
        return $result;
    }
    /**
     * @param Promocode $promocode
     * @param Money $price
     * @return bool
     */
    private static function isSameCurrency(Promocode $promocode, Money $price): bool
    {
        return $promocode->discount_currency === $price->getCurrency();
    }

    /**
     * if promocode currency is different from user currency and price is fixed
     * we exchange to user currency
     *
     * @param Promocode $promoCode
     * @param $userCurrency
     * @return mixed
     */
    private static function normalizeAmountCurrency(Promocode $promoCode, $userCurrency)
    {
        $result = $promoCode->discount_amount;
        if($promoCode->isFixedDiscount() && !self::isSameCurrency($promoCode, Money::create(0, $userCurrency))){
            $result = PaymentExchangeRateFacade::convert($promoCode->discount_amount, $promoCode->discount_currency, $userCurrency);
        }
        return $result;
    }

    /**
     * @param Promocode $promoCode
     * @param PaymentInvoice $paymentInvoice
     *
     * @return Money
     */
    public static function getDiscountsTotal(Promocode $promoCode, PaymentInvoice $paymentInvoice): Money
    {
        $invoiceMoney = null;

        switch($promoCode->discount_for) {
            case Promocode::DISCOUNT_FOR_MODEL:
                $invoiceMoney = $paymentInvoice->storeOrderAmount->getAmountModel3d();
                break;
            case Promocode::DISCOUNT_FOR_PRINT:
                $invoiceMoney = $paymentInvoice->storeOrderAmount->getAmountPrint();
                break;
            case Promocode::DISCOUNT_FOR_DELIVERY:
                $invoiceMoney = $paymentInvoice->storeOrderAmount->getAmountShipping();
                break;
            case Promocode::DISCOUNT_FOR_SERVICE_FEE:
                $invoiceMoney = $paymentInvoice->storeOrderAmount->getAmountFee();
                break;
            case Promocode::DISCOUNT_FOR_ALL:
                $invoiceMoney = $paymentInvoice->getAmountTotal();
                break;
        }

        if ($invoiceMoney) {
            $discount = self::getDiscount($promoCode, $invoiceMoney);

            if ($discount->getAmount() > $invoiceMoney->getAmount()) {
                return $invoiceMoney;
            }

            return $discount;
        }

        return Money::zero($paymentInvoice->currency);
    }
}