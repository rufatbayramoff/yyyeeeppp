<?php
/**
 * Created by mitaichik
 */

namespace common\modules\promocode\components;


use common\interfaces\DiscountsResolverInterface;

/**
 * Class DiscountsResolverStub
 * @package common\modules\promocode\components
 */
class DiscountsResolverStub implements DiscountsResolverInterface
{
    /**
     * @return array
     */
    public function getDiscounts()
    {
        return [];
    }
}