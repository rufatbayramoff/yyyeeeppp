<?php

namespace common\modules\promocode;

use yii\base\Module;

/**
 * Date: 30.11.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class PromocodeModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\promoсode\controllers';


    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();
    }
}
