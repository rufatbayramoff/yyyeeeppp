
Comments module 
=====

 Copy from [https://github.com/yeesoft/yii2-comments]
 
 Copy because:
  1. not supported on github, and 
  2. easier to dev for own use (not in vendor).
  3. clean up from not required features
  4. skin CSS styles and etc.
 

Configuration
------

- In your config file

```php
'bootstrap' => ['comments'],
'modules'=>[
	'comments' => [
		'class' => 'yeesoft\comments\Comments',
	],
],
```

- In you model [optional]

```php
public function behaviors()
{
  return [
    'comments' => [
      'class' => 'yeesoft\comments\behaviors\CommentsBehavior'
    ]
  ];
}
```

- How to enable anti-spam protection (using Akismet) [optional]

```php
'modules' => [
	'comments' => [
		...
		 'enableSpamProtection' => true,
		 ...
	],
],
'components' => [
	'akismet' => [
            'class' => 'yeesoft\comments\components\Akismet',
            'apiKey' => '*******', //you can get your apiKey here: https://akismet.com/
	],
],
```

Usage
---

- Widget namespace
```php
use yeesoft\comments\widgets\Comments;
```

- Add comment widget in model view using (string) page key :

```php
echo Comments::widget(['model' => $pageKey]); 
```

- Or display comments using model name and id:

```php
echo Comments::widget(['model' => 'post', 'model_id' => 1]); 
```

- Or display comments using model behavior:

```php
echo Post::findOne(10)->displayComments(); 
```

Module Options
-------

Use this options to configurate comments module:
 
- `userModel` - User model class name.

- `maxNestedLevel` - Maximum allowed nested level for comment's replies.

- `onlyRegistered` - Indicates whether not registered users can leave a comment.

- `orderDirection` - Comments order direction.

- `nestedOrderDirection` - Replies order direction.

- `userAvatar` - The field for displaying user avatars.

  Is this field is NULL default avatar image will be displayed. Also it can specify path to image or use callable type.
  
  If this property is specified as a callback, it should have the following signature: `function ($user_id)`

  Example of module settings:
  ```php
    'comments' => [
      'class' => 'yeesoft\comments\Comments',
      'userAvatar' => function($user_id){
        return User::getUserAvatarByID($user_id);
      }
    ]
  ```
   