<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\components;


use common\components\exceptions\AssertHelper;
use common\interfaces\Model3dBasePartInterface;
use frontend\models\model3d\Model3dFacade;
use yii\base\Exception;

/**
 * Class FileConvertor
 *
 * Component for converting files from IGES and STEP formats to STL
 *
 * @package common\modules\cnc\components
 */
class FileConvertor
{
    const QULAITY_HIGHT = '0.1';
    const QULAITY_LOW = '0.4';

    private static $command = "/usr/bin/drawf.sh";

    private static $stepCovertFileTemplate = "pload ALL
{readType} {srcFilepath} a *
incmesh a_1 {quality}
writestl a_1 {dstFilepatch}";

    private static $igesCovertFileTemplate = "pload ALL
{readType} {srcFilepath} a *
incmesh a {quality}
writestl a {dstFilepatch}
";

    /**
     * Convert IGES or STEP file to STL file
     * @param string $srcFilepath
     * @param string $dstFilepatch
     * @param string $quality
     * @throws Exception
     */
    public function convert(string $srcFilepath, string $dstFilepatch, string $quality)
    {
        AssertHelper::assert(file_exists($srcFilepath), "File {$srcFilepath} not exist");

        $srcExt = $this->getFileExt($srcFilepath);

        AssertHelper::assert(Model3dFacade::isIgesOrStep($srcExt), "Unsupported file ext {$srcExt} for convert");

        AssertHelper::assert(
            Model3dBasePartInterface::STL_FORMAT == $this->getFileExt($dstFilepatch),
            "Convertor can convert only to STL format, not to $dstFilepatch"
        );

        $convertFile = $this->generateConvertFilename();
        $convertFileContent = $this->generateConvertFileContent($srcFilepath, $dstFilepatch, $quality);
        file_put_contents($convertFile, $convertFileContent);

        $output = $status = 0;
        exec(self:: $command." ".$convertFile, $output, $status);
        $status = (int)$status;
        if (is_array($output)) {
            $output = implode(" ", $output);
        }

        @unlink($convertFile);

        if (filesize($dstFilepatch) == 84 || $status > 0) {
            @unlink($dstFilepatch);
            throw new Exception("Cannot convert {$srcFilepath} to {$dstFilepatch} with quality {$quality} : " . $output);
        }
    }


    /**
     * Generate content for convert command who will place to temporary file
     * @param string $srcFilepath
     * @param string $dstFilepatch
     * @param string $quality
     * @return string
     * @throws Exception
     */
    private function generateConvertFileContent(string $srcFilepath, string $dstFilepatch, string $quality) : string
    {
        $ext = $this->getFileExt($srcFilepath);

        switch ($ext) {
            case Model3dBasePartInterface::STEP_FORMAT:
            case Model3dBasePartInterface::STEP_FORMAT_2:
                $readType = "stepread";
                $template = self::$stepCovertFileTemplate;
                break;

            case Model3dBasePartInterface::IGES_FORMAT:
            case Model3dBasePartInterface::IGES_FORMAT_2:
                $readType = "igesread";
                $template = self::$igesCovertFileTemplate;
                break;

            default:
                throw new Exception("Unsupported file ext {$ext} for convert");
        }

        return strtr($template, [
            '{readType}' => $readType,
            '{srcFilepath}' => $srcFilepath,
            '{quality}' => $quality,
            '{dstFilepatch}' => $dstFilepatch,
        ]);
    }


    /**
     * Generate random filename for temporary data
     * @return string
     */
    private function generateConvertFilename() : string
    {
        return \Yii::getAlias("@runtime/" . \Yii::$app->security->generateRandomString());
    }

    /**
     * Get extension of path in lower case
     * @param string $filepath
     * @return string
     */
    public static function getFileExt(string $filepath) : string
    {
        return strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
    }
}