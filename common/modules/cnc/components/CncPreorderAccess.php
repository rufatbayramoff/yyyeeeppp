<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\components;


use common\models\Preorder;
use common\models\User;
use yii\base\UserException;
use yii\web\ForbiddenHttpException;

class CncPreorderAccess
{
    /**
     * @param Preorder $preorder
     * @param User $user
     * @return bool
     */
    public function isCustomer(Preorder $preorder, User $user) : bool
    {
        return $preorder->user->equals($user);
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     * @return bool
     */
    private function isPs(Preorder $preorder, User $user) : bool
    {
        return $preorder->machine->ps->user->equals($user);
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     * @throws ForbiddenHttpException
     */
    private function tryIsCustomer(Preorder $preorder, User $user)
    {
        if(!$this->isCustomer($preorder, $user)){
            $this->throwForrobin();
        }
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     * @throws ForbiddenHttpException
     */
    private function tryIsPs(Preorder $preorder, User $user)
    {
        if (!$this->isPs($preorder, $user)){
            $this->throwForrobin();
        }
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     * @throws ForbiddenHttpException
     */
    public function tryCanView(Preorder $preorder, User $user)
    {
        if (!$this->isCustomer($preorder, $user) && !$this->isPs($preorder, $user)) {
            $this->throwForrobin();
        }
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     * @throws ForbiddenHttpException
     */
    public function tryCanEdit(Preorder $preorder, User $user)
    {
        $this->tryIsPs($preorder, $user);

        if (!$preorder->isEditable()) {
            $this->throwIllegalState();
        }
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     */
    public function tryCanMakeOffer(Preorder $preorder, User $user)
    {
        $this->tryIsPs($preorder, $user);
        if(!$preorder->isCanMakeOffer()) {
            $this->throwIllegalState();
        }
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     * @throws UserException
     */
    public function tryCanDecline(Preorder $preorder, User $user)
    {
        $this->tryCanView($preorder, $user);
        if(!$preorder->isCanDecline()) {
          $this->throwIllegalState();
        }
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     */
    public function tryCanAccept(Preorder $preorder, User $user)
    {
        $this->tryIsCustomer($preorder, $user);
        if(!$preorder->isCanAccept()) {
            $this->throwIllegalState();
        }
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     */
    public function tryCanReturnToService(Preorder $preorder, User $user)
    {
        $this->tryIsCustomer($preorder, $user);
        if(!$preorder->isCanReturnToPs()) {
            $this->throwIllegalState();
        }
    }

    /**
     * @throws UserException
     */
    private function throwIllegalState()
    {
        throw new UserException("Illegal state of CNC Request   ");
    }

    /**
     * @throws UserException
     */
    private function throwForrobin()
    {
        throw new ForbiddenHttpException();
    }
}