<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.07.17
 * Time: 17:03
 */

namespace common\modules\cnc\components;

use common\components\ArrayHelper;
use common\models\CncMaterialGroup;
use common\modules\cnc\CncModule;
use common\modules\cnc\models\PsCnc;
use yii\base\Component;

class PsCncJsonGenerator extends Component
{
    /**
     * @var CncModule
     */
    protected $module;

    CONST JSON_VERSION = 1;

    public function setModule(CncModule $module)
    {
        $this->module = $module;
    }

    /**
     * @param PsCnc $psCnc
     * @return array
     */
    public function generateJsonServiceInfo(PsCnc $psCnc)
    {
        $schema    = $psCnc;
        $jsonArray = [
            'version'            => self::JSON_VERSION,
            'company'            => $schema->cncCompany,
            'materials'          => $schema->materials,
            'stocks'             => [
                'freeform' => $schema->stocksFreeForm,
                'slabs'    => $schema->stocksSlabs
            ],
            'settings'           => $schema->settings,
            'millingMachines'    => $schema->millingMachines,
            'millingInstruments' => $schema->millingInstruments,
            'cuttingMachines'    => $schema->cuttingMachines,
            'postprocessing'     => $schema->postProcessing
        ];
        return $jsonArray;
    }
}

