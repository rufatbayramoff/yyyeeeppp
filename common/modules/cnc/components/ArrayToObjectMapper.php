<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\components;
use \ReflectionClass;

/**
 * Class ArrayToObjectMapper
 * Хелпер для маппинга массива в объекты
 */
class ArrayToObjectMapper
{
    /**
     * Хранит схемы уже распарсенных классов
     * @var array
     */
    private static $schemes = [];

    /**
     * Маппит массив в объекты
     * @param $object
     * @param $data
     */
    public static function map($object, $data)
    {
        $className = get_class($object);
        $scheme = self::resolveScheme($className);
        foreach($scheme as $property) {

            if(!isset($data[$property->getName()])) {
                continue;
            }

            $value = $data[$property->getName()];

            if($property->isArray()) {
                $object->{$property->getName()} = [];
                foreach($value as $key => $val) {
                    $object->{$property->getName()}[$key] = self::resolveValue($property->getClass(), $val);
                }
            }
            else {
                $object->{$property->getName()} = self::resolveValue($property->getClass(), $value);
            }
        }
    }

    /**
     * Приводит значение к необходимому классу.
     * Если клясс - объект, то создает его и маппит
     * @param $className
     * @param $value
     * @return mixed
     */
    private static function resolveValue($className, $value)
    {
        $result = null;
        switch($className)
        {
            case 'int':
                $result = (int)$value;
                break;

            case 'string':
                $result = (string)$value;
                break;

            case 'bool':
            case 'boolean':
                $result = (bool)$value;
                break;

            case 'mixed':
                $result = $value;
                break;

            default:
                $result = new $className;
                self::map($result, $value);
        }
        return $result;
    }

    /**
     * Возвращает схему класса
     * @param string $className имя класса
     * @return PropertyDescription[]
     */
    public static function resolveScheme($className)
    {
        if(isset(self::$schemes[$className])) {
            return self::$schemes[$className];
        }
        else {
            $class = new ReflectionClass($className);
            $scheme = array();
            foreach($class->getProperties() as $property) {
                $name = $property->getName();
                if($property->isPublic() && !$property->isStatic() && ($docCommnet = $property->getDocComment()) != '') {
                    preg_match_all('~@var ([A-Za-z0-9_\\\\]+)(\[\])*~', $docCommnet, $matches, PREG_SET_ORDER);
                    $item = new PropertyDescription($name, $matches[0][1], isset($matches[0][2]));
                    $scheme[] = $item;
                }
            }
            self::$schemes[$className] = $scheme;
            return $scheme;
        }
    }
}

/**
 * Class SchemeItem
 * Схема для одного поля класса
 */
class PropertyDescription
{
    /**
     * Имя свойства
     * @var string
     */
    private $name;

    /**
     * Класс свойства
     * @var string
     */
    private $class;

    /**
     * Является ли свойство массивом
     * @var bool
     */
    private $isArray;

    /**
     * @param string $name
     * @param string $class
     * @param bool $isArray
     */
    public function __construct(string $name, string $class, bool $isArray)
    {
        $this->name = $name;
        $this->class = $class;
        $this->isArray = $isArray;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return bool
     */
    public function isArray()
    {
        return $this->isArray;
    }
}