<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.07.17
 * Time: 16:16
 */

namespace common\modules\cnc\components;

use common\components\ArrayHelper;
use common\modules\cnc\CncModule;
use common\modules\cnc\models\CncCuttingMachine;
use common\modules\cnc\models\CncCuttingMachineSpeed;
use common\modules\cnc\models\CncMaterial;
use common\modules\cnc\models\CncMillingMachine;
use common\modules\cnc\models\CncPostprocessing;
use common\modules\cnc\models\CncStocksFreeForm;
use common\modules\cnc\models\CncStocksSlab;
use common\modules\cnc\models\PsCnc;

class PsCncPopulator
{
    /**
     * @var CncModule
     */
    protected $module;

    public function setModule(CncModule $module)
    {
        $this->module = $module;
    }


    public function populate(PsCnc $psCnc, $data)
    {
        $version = $data['version'] ?? 1;
        unset($data['company']);
        unset($data['version']);
        if (array_key_exists('materials', $data)) {
            $materials = [];
            foreach ($data['materials'] as $key => $materialInfo) {
                $materialItem = new CncMaterial();
                ArrayHelper::setObjectProperties($materialItem, $materialInfo);
                $materials[] = $materialItem;
            }
            $psCnc->materials = $materials;
            unset($data['materials']);
        }

        if (array_key_exists('stocks', $data)) {
            if (array_key_exists('freeform', $data['stocks'])) {
                $newStocksForms = [];
                foreach ($data['stocks']['freeform'] as $key => $freeForm) {
                    $stocksFreeForm = new CncStocksFreeForm();
                    ArrayHelper::setObjectProperties($stocksFreeForm, $freeForm);
                    $newStocksForms[] = $stocksFreeForm;
                }
                $psCnc->stocksFreeForm = $newStocksForms;
            }

            if (array_key_exists('slabs', $data['stocks'])) {
                $newStocksSlabs = [];
                foreach ($data['stocks']['slabs'] as $key => $slabs) {
                    $stocksSlabs = new CncStocksSlab();
                    ArrayHelper::setObjectProperties($stocksSlabs, $slabs);
                    $newStocksSlabs[] = $stocksSlabs;
                }
                $psCnc->stocksSlabs = $newStocksSlabs;
            }
            unset($data['stocks']);
        }

        if (array_key_exists('millingMachines', $data)) {
            $millingMachines = [];
            foreach ($data['millingMachines'] as $key => $millingMachineInfo) {
                $millingMachine = new CncMillingMachine();
                ArrayHelper::setObjectProperties($millingMachine, $millingMachineInfo);
                $millingMachines[] = $millingMachine;
            }
            unset($data['millingMachines']);
            $psCnc->millingMachines = $millingMachines;
        }

        if (array_key_exists('millingInstruments', $data)) {
            $millingInstruments = [];
            foreach ($data['millingInstruments'] as $key => $millingInstrumentInfo) {
                $millingInstrument = new CncMillingMachine();
                ArrayHelper::setObjectProperties($millingInstrument, $millingInstrumentInfo);
                $millingInstruments[] = $millingInstrument;
            }
            unset($data['millingInstruments']);
            $psCnc->millingInstruments = $millingInstruments;
        }

        if (array_key_exists('cuttingMachines', $data)) {
            $cuttingMachines = [];
            foreach ($data['cuttingMachines'] as $key => $cuttingMachineInfo) {
                $cuttingMachine = new CncCuttingMachine();
                $speeds = [];
                if (array_key_exists('speeds', $cuttingMachineInfo)) {
                    foreach ($cuttingMachineInfo['speeds'] as $cuttingMachineSpeedInfo) {
                        $cuttingMachineSpeed = new CncCuttingMachineSpeed();
                        ArrayHelper::setObjectProperties($cuttingMachineSpeed, $cuttingMachineSpeedInfo);
                        $speeds[] = $cuttingMachineSpeed;
                    }
                }
                $cuttingMachine->speeds = $speeds;
                ArrayHelper::setObjectProperties($cuttingMachine, $cuttingMachineInfo);
                $cuttingMachines[] = $cuttingMachine;
            }
            unset($data['cuttingMachines']);
            $psCnc->cuttingMachines = $cuttingMachines;
        }

        if (array_key_exists('postprocessing', $data)) {
            $postprocessing = [];
            foreach ($data['postprocessing'] as $key => $postprocessingInfo) {
                $postprocessingElement = new CncPostprocessing();
                ArrayHelper::setObjectProperties($postprocessingElement, $postprocessingInfo);
                $postprocessing[] = $postprocessingElement;
            }
            unset($data['postprocessing']);
            $psCnc->postProcessing = $postprocessing;
        }

        ArrayHelper::setObjectProperties($psCnc, $data);
    }
}