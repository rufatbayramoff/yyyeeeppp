<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.07.17
 * Time: 14:13
 */

namespace common\modules\cnc\components;

use common\components\ArrayHelper;
use common\models\CncMaterialGroup;
use common\models\CncMaterialSame;
use common\modules\cnc\CncModule;
use lib\money\Currency;

class SchemaManager extends \yii\base\Component
{
    /**
     * @var CncModule
     */
    protected $module;

    public function setModule(CncModule $module)
    {
        $this->module = $module;
    }

    /**
     * @return array
     */
    public function generateMaterialsList()
    {
        $cncMaterials = CncMaterialSame::findAll(['is_active' => 1]);
        return ArrayHelper::toArray(
            $cncMaterials,
            [
                CncMaterialSame::class => [
                    'value' => 'code',
                    'title' => 'title',
                    'group' => function (CncMaterialSame $material) {
                        return $material->cncMaterialGroup->code;
                    },
                ],
            ]
        );
    }

    /**
     * @return array
     */
    public function generateMaterialsGroupList()
    {
        $cncMaterialGroups = CncMaterialGroup::find()->all();
        return ArrayHelper::toArray(
            $cncMaterialGroups,
            [
                CncMaterialGroup::class => [
                    'value' => 'code',
                    'title' => 'title'
                ],
            ]
        );
    }

    protected function getVarsValues($vars)
    {
        $defaultVars = [
            '__SITE_ROOT__' => param('staticUrl') . '/',
            '__CURRENCY__'  => Currency::USD
        ];
        $vars        = ArrayHelper::merge($defaultVars, $vars);
        return $vars;
    }

    protected function processVars(&$cncSchema, $vars)
    {
        foreach ($cncSchema as $key => &$value) {
            if (is_array($value)) {
                $this->processVars($value, $vars);
                continue;
            }
            foreach ($this->getVarsValues($vars) as $varName => $varValue) {

                if ($key !== 'propertyOrder') {
                    $newValue        = str_replace($varName, $varValue, $value);
                    $cncSchema[$key] = $newValue;
                }
            }
        }
    }

    public function getInputFormSchema($vars = [])
    {
        $rawCncSchema = $this->module->rawCncSchema;
        unset($rawCncSchema['properties']['company']);
        $materials                                                                                               = $this->generateMaterialsList();
        $materialGroups                                                                                          = $this->generateMaterialsGroupList();
        $rawCncSchema['properties']['materials']['items']['properties']['material']['enumSource']['0']['source'] = $materialGroups;
        $rawCncSchema['properties']['materials']['items']['properties']['material']['default']                   = 'structural-steel';
        $rawCncSchema['properties']['materials']['items']['properties']['sourceMaterial']                        = [
            'type'          => 'string',
            'title'         => 'Similar Material',
            'propertyOrder' => 1,
            'watch'         => [
                'materialGroupItem' => 'material_item.material'
            ],
            'enumSource'    => [
                '0' =>
                    [
                        'source' => $materials,
                        'title'  => '{{item.title}}',
                        'value'  => '{{item.value}}',
                        'filter' => 'js: function(itemInfo) {if (!itemInfo.watched.materialGroupItem || itemInfo.watched.materialGroupItem == itemInfo.item.group) return 1;}',
                    ],

            ],
            'default'       => 'structural-steel-A36'
        ];

        $this->processVars($rawCncSchema, $vars);

        return $rawCncSchema;
    }

    public function getInputFormSchemaShortMaterials($vars =[])
    {
        $schema = $this->getInputFormSchema($vars);
        unset($schema['properties']['postprocessing']);
        unset($schema['properties']['settings']);
        unset($schema['properties']['stocks']);
        unset($schema['properties']['millingInstruments']);
        unset($schema['properties']['millingInstruments']);
        unset($schema['properties']['cuttingMachines']);
        unset($schema['properties']['millingMachines']);
        $schema['properties']['materials']['items']['properties']['density']['options']['hidden'] = true;
        unset($schema['properties']['materials']['items']['properties']['density']['minimum']);
        $schema['properties']['materials']['items']['properties']['defaultTolerance']['options']['hidden'] = true;
        $schema['properties']['materials']['items']['properties']['maxTolerance']['options']['hidden']     = true;

        $schema['properties']['materials']['title']          = '-';
        $schema['properties']['materials']['info']           = '';
        $schema['properties']['materials']['items']['title'] = _t('site.ps', 'Add Material');
        $schema['title']                                     = '-';
        return $schema;
    }
}