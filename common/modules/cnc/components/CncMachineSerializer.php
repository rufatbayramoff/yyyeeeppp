<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\components;


use common\components\serizaliators\AbstractProperties;
use common\modules\cnc\models\CncSettings;
use common\modules\cnc\models\CncMaterial;
use common\modules\cnc\models\CncPostprocessing;
use common\modules\cnc\models\PsCnc;

class CncMachineSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            PsCnc::class => [
                'materials',
                'postProcessing',
                'settings',
                'currency' => function (PsCnc $psCnc) {
            return $psCnc->cncCompany->currency;
    }
            ],
            CncMaterial::class => [
                'title',
                'code' => 'title'
            ],
            CncPostprocessing::class => [
                'title',
                'code' => 'title'
            ],
            CncSettings::class => [
                'defaultMaterial'
            ]
        ];
    }
}