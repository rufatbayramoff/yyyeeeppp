<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Class Preset
 * @package common\modules\cnc\api
 */
class Preset extends Model
{
    /**
     * Quantity
     * @var int
     */
    public $count;

    /**
     * Material
     * @var string
     */
    public $material;

    /**
     * Json data for postprocessing
     * @var string[]
     */
    public $postprocessing;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['count', 'integer', 'min' => 1],
            ['material', 'string'],
            ['postprocessing', 'safe'],
            [['count', 'material'], 'required']
        ];
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        $data = $this->getAttributes(null, ['postprocessing']);
        $data['postprocessing'] =  Json::encode($this->postprocessing);
        return Json::encode($data);
    }
}