<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api;


use common\components\exceptions\OutExceptionInterface;
use yii\base\Exception;

class ApiException extends Exception implements OutExceptionInterface
{

}