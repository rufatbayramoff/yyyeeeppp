<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api;


use common\components\exceptions\AssertHelper;
use common\interfaces\Model3dBasePartInterface;
use common\models\File;
use common\models\PsCncMachine;
use common\modules\cnc\api\responses\AnalyzeResponse;
use common\modules\cnc\api\responses\costing\CostingResponse;
use common\modules\cnc\api\responses\costing\Instrument;
use common\modules\cnc\api\responses\costing\PriceElement;
use common\modules\cnc\api\responses\ModelingResponse;
use common\modules\cnc\api\responses\RecognizeResponse;
use common\modules\cnc\api\responses\UploadResponse;
use common\modules\cnc\api\serializers\CostingSerializer;
use common\modules\cnc\components\FileConvertor;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Yii;
use yii\base\BaseObject;
use yii\helpers\Json;

class Api extends BaseObject
{
    /**
     * @var string
     */
    public $apiUrl;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var FileConvertor
     */
    private $fileConvertor;

    /**
     * @var string
     */
    public $machineParamsPath = '@frontend/../tools/node/www/cloudcam/services/data';

    /**
     * @var string
     */
    public $storagePath = '@frontend/../tools/node/';

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->apiUrl = $this->apiUrl ?? param('watermarkServer', 'http://192.168.66.10:5858');
        $this->client = new Client(['base_uri' => $this->apiUrl]);
    }

    /**
     * @param FileConvertor $fileConvertor
     */
    public function injectDependencies(FileConvertor $fileConvertor)
    {
        $this->fileConvertor = $fileConvertor;
    }

    /**
     * Upload file to api
     * @param File $file
     * @return UploadResponse
     * @throws ApiException
     */
    public function upload(File $file): UploadResponse
    {
        try {
            $response = $this->client
                ->post("/jsapi/private/cloudcam/upload", [
                    'multipart' => [
                        [
                            'name'     => 'file',
                            'contents' => fopen($file->getLocalTmpFilePath(), 'r'),
                            'filename' => $file->getFileName()
                        ]
                    ]
                ])
                ->getBody()
                ->getContents();
            //Yii::error(Json::encode($response), 'tsdebug');
        } catch (RequestException $e) {
            Yii::error($e, 'tsdebug');
            throw $this->parseApiException($e);
        }

        $response = Json::decode($response);

        AssertHelper::assert($response['success'], "Unsuccess upload", ApiException::class);
        return new UploadResponse($response['filepath']);
    }

    /**
     * Recognize file
     * @param string $filepath Filepath from upload response
     * @return RecognizeResponse
     * @throws ApiException
     */
    public function recognize(string $filepath): RecognizeResponse
    {
        // if it stl file - we dont need make recognize
        if ($this->fileConvertor->getFileExt($filepath) == Model3dBasePartInterface::STL_FORMAT) {
            return new RecognizeResponse($filepath, $filepath);
        }


        $filepath    = Yii::getAlias($this->storagePath . $filepath);
        $hightDstUri = "data/" . $this->generateGuid() . ".stl";
        $lowDstUri   = "data/" . $this->generateGuid() . ".stl";

        $this->fileConvertor->convert($filepath, Yii::getAlias($this->storagePath . $lowDstUri), FileConvertor::QULAITY_LOW);
        $this->fileConvertor->convert($filepath, Yii::getAlias($this->storagePath . $hightDstUri), FileConvertor::QULAITY_HIGHT);

        return new RecognizeResponse($hightDstUri, $lowDstUri);
    }

    /**
     * Analyze file
     * @param string $firstfile
     * @param string $roughfile
     * @param string $finefile
     * @return AnalyzeResponse
     * @throws ApiException
     */
    public function analyze(string $firstfile, string $roughfile, string $finefile): AnalyzeResponse
    {
        try {
            $response = $this->client
                ->post("/jsapi/private/cloudcam/processing/analyze", [
                    'timeout'     => 600,
                    'form_params' => [
                        'firstfile' => $firstfile,
                        'roughfile' => $roughfile,
                        'finefile'  => $finefile,
                    ]
                ])
                ->getBody()
                ->getContents();
        } catch (RequestException $e) {
            throw $this->parseApiException($e);
        }

        $response = Json::decode($response);

        AssertHelper::assert($response['result'] == 'ok', "Unsuccess analyze", ApiException::class);
        return new AnalyzeResponse(Json::encode($response['analyzeResult']));
    }

    /**
     * Calculate cost for file
     * @param string $firstfile
     * @param string $roughfile
     * @param string $finefile
     * @param string $service
     * @param string $analyze
     * @param Preset $preset
     * @return CostingResponse
     * @throws ApiException
     */
    public function costing(string $firstfile, string $roughfile, string $finefile, string $service, string $analyze, Preset $preset): CostingResponse
    {
        try {
            $response = $this->client
                ->post("/jsapi/private/cloudcam/processing/costing", [
                    'form_params' => [
                        'firstfile' => $firstfile,
                        'roughfile' => $roughfile,
                        'finefile'  => $finefile,
                        'service'   => $service,
                        'analyze'   => $analyze,
                        'preset'    => Json::encode($this->resolvePresetData($preset)),
                    ]
                ])
                ->getBody()
                ->getContents();
        } catch (RequestException $e) {
            throw $this->parseApiException($e);
        }

        $response = Json::decode($response);
        AssertHelper::assert($response['result'] == 'ok', "Unsuccess costing", ApiException::class);
        return $this->createCostingResponse($response);
    }

    /**
     * @param Model3dBasePartInterface $part
     * @param PsCncMachine $machine
     * @param string $preset
     * @return CostingResponse|null
     * @throws ApiException
     */
    public function budget(Model3dBasePartInterface $part, PsCncMachine $machine, string $preset): ?CostingResponse
    {
        $params                = $part->model3dPartCncParams;
        $formParams            = $params->toArray(['filepath', 'roughfile', 'finefile', 'analyze']);
        $formParams['service'] = $machine->resolveMachineUri();
        $formParams['preset']  = $preset;

        try {
            $response = $this->client
                ->post("/jsapi/private/cloudcam/processing/costing", [
                    'form_params' => $formParams
                ])
                ->getBody()
                ->getContents();
            $response = Json::decode($response);
            AssertHelper::assert($response['result'] == 'ok', "Unsuccess costing", ApiException::class);
            return $this->createCostingResponse($response);
        } catch (RequestException $e) {
            throw $this->parseApiException($e);
        }
    }

    /**
     * Load modeling model from api
     * @param string $firstfile
     * @param string $roughfile
     * @param string $finefile
     * @param string $service
     * @param string $analyze
     * @param Preset $preset
     * @param CostingResponse $costing
     * @return ModelingResponse
     * @throws ApiException
     */
    public function modeling(string $firstfile, string $roughfile, string $finefile, string $service, string $analyze, Preset $preset, CostingResponse $costing): ModelingResponse
    {
        try {
            $response = $this->client
                ->post("/jsapi/private/cloudcam/processing/modelling", [
                    'form_params' => [
                        'firstfile' => $firstfile,
                        'roughfile' => $roughfile,
                        'finefile'  => $finefile,
                        'service'   => $service,
                        'analyze'   => $analyze,
                        'costing'   => Json::encode(CostingSerializer::serialize($costing)),
                        'preset'    => Json::encode($this->resolvePresetData($preset)),
                    ]
                ])
                ->getBody()
                ->getContents();
        } catch (RequestException $e) {
            throw $this->parseApiException($e);
        }

        $response = Json::decode($response);

        AssertHelper::assert($response['result'] == 'ok', "Unsuccess modeling", ApiException::class);
        return new ModelingResponse($response['plyColored']);
    }

    /**
     * Load STL from api
     * @param $uri
     * @return string
     */
    public function loadFile($uri): string
    {
        return file_get_contents($this->apiUrl . '/' . $uri);
    }

    /**
     * Upload cnc mashine params to API
     * @param PsCncMachine $machine
     * @todo implement this method in api and remove this stub
     */
    public function upadteMachine(PsCncMachine $machine)
    {
        $filepath = $this->resolveMashineFile($machine);
        file_put_contents($filepath, json_encode($machine->description));
    }

    /**
     * @param PsCncMachine $machine
     * @return bool|string
     */
    public function resolveMashineFile(PsCncMachine $machine): string
    {
        return Yii::getAlias($this->machineParamsPath . '/cnc_machine_' . $machine->companyService->id . '.json');
    }

    /**
     * Resolve preset data for sending to api
     * @param Preset $preset
     * @return array
     */
    private function resolvePresetData(Preset $preset): array
    {
        $data                   = $preset->getAttributes(null, ['postprocessing']);
        $data['postprocessing'] = Json::encode($preset->postprocessing);
        return $data;
    }

    /**
     * @param array $rawResponse
     * @return CostingResponse
     */
    public function createCostingResponse(array $rawResponse): CostingResponse
    {
        $priceElements = [];
        foreach ($rawResponse['costingResult']['priceElements'] as $data) {
            if ($data['price'] === null) {
                continue;
            }
            $priceElements[] = new PriceElement($data['price'], $data['title'], $data['isPerPart']);
        }

        $data       = $rawResponse['costingResult']['instrument'];
        $instrument = new Instrument($data['d'], $data['delta'], $data['fr'], $data['l'], $data['material'], $data['quality'], $data['s']);

        $data = $rawResponse['costingResult'];
        if ($data['total'] === null) {
            throw new ApiException(_t('site.cnc', "Can`t calculate result"), 0);
        }
        return new CostingResponse($data['count'], $data['currency'], $instrument, $priceElements, $data['total']);
    }

    /**
     * @param RequestException $exception
     * @return ApiException
     */
    private function parseApiException(RequestException $exception): ApiException
    {
        $response = $exception->getResponse();

        if (!$response) {
            return new ApiException("Unknown API exception", 0, $exception);
        }

        $data = $response->getBody()->getContents();
        $data = Json::decode($data);

        if (!$data || !isset($data['message'])) {
            return new ApiException("Unknown API exception", 0, $exception);
        }

        return new ApiException($data['message'], 0, $exception);
    }

    /**
     * Generate GUID
     * @return string
     */
    private function generateGuid(): string
    {
        mt_srand((double)microtime() * 10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));

        $guid = substr($charid, 0, 8) . '-' .
            substr($charid, 8, 4) . '-' .
            substr($charid, 12, 4) . '-' .
            substr($charid, 16, 4) . '-' .
            substr($charid, 20, 12);
        return strtolower($guid);
    }
}