<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses;

/**
 * Class AnalyzeResponse
 * @package common\modules\cnc\api
 */
class AnalyzeResponse
{
    /**
     * @var string
     */
    private $result;

    /**
     * AnalyzeResponse constructor.
     * @param string $result
     */
    public function __construct(string $result)
    {
        $this->result = $result;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }
}