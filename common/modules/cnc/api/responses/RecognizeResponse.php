<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses;


/**
 * Class RecognizeResponse
 * @package common\modules\cnc\api
 */
class RecognizeResponse
{
    /**
     * @var string
     */
    private $fine;

    /**
     * @var string
     */
    private $rough;

    /**
     * RecognizeResponse constructor.
     * @param string $fine
     * @param string $rough
     */
    public function __construct(string $fine, string $rough)
    {
        $this->fine = $fine;
        $this->rough = $rough;
    }

    /**
     * @return string
     */
    public function getFine(): string
    {
        return $this->fine;
    }

    /**
     * @return string
     */
    public function getRough(): string
    {
        return $this->rough;
    }
}