<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses\analyze;

/**
 * Class AnalyzeResponse
 * @package common\modules\cnc\models\analyze
 */
class AnalyzeResponse
{
    /**
     * @var string
     */
    public $result;


    /**
     * @var Result
     */
    public $analyzeResult;

}