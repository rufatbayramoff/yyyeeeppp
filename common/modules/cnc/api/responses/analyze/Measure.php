<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses\analyze;

/**
 * Class Measure
 * @package common\modules\cnc\models\analyze
 */
class Measure
{
    /**
     * @var float
     */
    public $area;

    /**
     * @var float
     */
    public $volume;

    /**
     * @var float
     */
    public $weight;

}