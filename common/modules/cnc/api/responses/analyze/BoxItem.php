<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses\analyze;

/**
 * Class BoxItem
 * @package common\modules\cnc\models\analyze
 */
class BoxItem
{
    /**
     * @var float
     */
    public $x;

    /**
     * @var float
     */
    public $y;

    /**
     * @var float
     */
    public $z;

}