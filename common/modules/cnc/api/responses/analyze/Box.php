<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses\analyze;

/**
 * Class Box
 * @package common\modules\cnc\models\analyze
 */
class Box
{
    /**
     * @var BoxItem
     */
    public $min;

    /**
     * @var BoxItem
     */
    public $max;

}