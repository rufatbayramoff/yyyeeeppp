<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses\analyze;

/**
 * Class Result
 * @package common\modules\cnc\models\analyze
 */
class Result
{
    /**
     * @var string
     */
    public $result;

    /**
     * @var float
     */
    public $minInstrumentD;

    /**
     * @var Measure
     */
    public $measures;

    /**
     * @var Box
     */
    public $box;

}