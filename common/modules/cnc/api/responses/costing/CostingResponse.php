<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses\costing;
use common\components\exceptions\AssertHelper;
use yii\base\BaseObject;

/**
 * Class CostingResponse
 * @package common\modules\cnc\api
 */
class CostingResponse extends BaseObject
{
    /**
     * @var int
     */
    private $count;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var Instrument
     */
    private $instrument;

    /**
     * @var PriceElement[]
     */
    private $priceElements;

    /**
     * @var float
     */
    private $total;

    /**
     * CostingResponse constructor.
     * @param int $count
     * @param string $currency
     * @param Instrument $instrument
     * @param array $priceElements
     * @param float $total
     */
    public function __construct(int $count, string $currency, Instrument $instrument, array $priceElements, float $total)
    {
        parent::__construct();
        AssertHelper::assertInstances($priceElements, PriceElement::class);

        $this->count = $count;
        $this->currency = $currency;
        $this->instrument = $instrument;
        $this->priceElements = $priceElements;
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return Instrument
     */
    public function getInstrument(): Instrument
    {
        return $this->instrument;
    }

    /**
     * @return PriceElement[]
     */
    public function getPriceElements(): array
    {
        return $this->priceElements;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }
}