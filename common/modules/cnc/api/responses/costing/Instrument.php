<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses\costing;
use yii\base\BaseObject;

/**
 * Class Instrument
 * @package common\modules\cnc\api\responses\costing
 */
class Instrument extends BaseObject
{
    /**
     * @var float
     */
    private $d;

    /**
     * @var float
     */
    private $delta;

    /**
     * @var float
     */
    private $fr;

    /**
     * @var float
     */
    private $l;

    /**
     * @var string
     */
    private $material;

    /**
     * @var string
     */
    private $quality;

    /**
     * @var float
     */
    private $s;

    /**
     * Instrument constructor.
     * @param float $d
     * @param float $delta
     * @param float $fr
     * @param float $l
     * @param string $material
     * @param string $quality
     * @param float $s
     */
    public function __construct(float $d, float $delta, float $fr, float $l, string $material, string $quality, float $s)
    {
        parent::__construct();
        $this->d = $d;
        $this->delta = $delta;
        $this->fr = $fr;
        $this->l = $l;
        $this->material = $material;
        $this->quality = $quality;
        $this->s = $s;
    }

    /**
     * @return float
     */
    public function getD(): float
    {
        return $this->d;
    }

    /**
     * @return float
     */
    public function getDelta(): float
    {
        return $this->delta;
    }

    /**
     * @return float
     */
    public function getFr(): float
    {
        return $this->fr;
    }

    /**
     * @return float
     */
    public function getL(): float
    {
        return $this->l;
    }

    /**
     * @return string
     */
    public function getMaterial(): string
    {
        return $this->material;
    }

    /**
     * @return string
     */
    public function getQuality(): string
    {
        return $this->quality;
    }

    /**
     * @return float
     */
    public function getS(): float
    {
        return $this->s;
    }
}