<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses\costing;
use yii\base\BaseObject;

/**
 * Class PriceElement
 * @package common\modules\cnc\api\responses\costing
 */
class PriceElement extends BaseObject
{
    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $title;

    /**
     * @var bool
     */
    private $isPerPart;

    /**
     * Instrument constructor.
     * @param float $price
     * @param string $title
     * @param bool $isPerPart
     */
    public function __construct(float $price, string $title, bool $isPerPart)
    {
        parent::__construct();
        $this->price = $price;
        $this->title = $title;
        $this->isPerPart = $isPerPart;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function getIsPerPart() : bool
    {
        return $this->isPerPart;
    }
}