<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses;

/**
 * Class UploadResponse
 * @package common\modules\cnc\api
 */
class UploadResponse
{
    /**
     * @var string
     */
    private $filepath;

    /**
     * UploadResponse constructor.
     * @param string $filepath
     */
    public function __construct(string $filepath)
    {
        $this->filepath = $filepath;
    }

    /**
     * @return string
     */
    public function getFilepath(): string
    {
        return $this->filepath;
    }
}