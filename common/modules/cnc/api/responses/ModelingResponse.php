<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\responses;
use yii\base\BaseObject;

/**
 * Class ModelingResponse
 * @package common\modules\cnc\api\responses
 */
class ModelingResponse extends BaseObject
{
    /**
     * @var string
     */
    private $plyColored;

    /**
     * ModelingResponse constructor.
     * @param string $plyColored
     */
    public function __construct(string $plyColored)
    {
        parent::__construct();
        $this->plyColored = $plyColored;
    }

    /**
     * @return string
     */
    public function getPlyColored(): string
    {
        return $this->plyColored;
    }
}