<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\serializers;


use common\components\serizaliators\AbstractProperties;
use common\modules\cnc\api\responses\costing\CostingResponse;
use common\modules\cnc\api\responses\costing\Instrument;
use common\modules\cnc\api\responses\costing\PriceElement;
use common\modules\cnc\api\responses\ModelingResponse;

class ModelingSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            ModelingResponse::class => [
                'plyColored',
            ],
        ];
    }
}