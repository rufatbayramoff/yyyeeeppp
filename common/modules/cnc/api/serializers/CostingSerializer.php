<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\api\serializers;


use common\components\serizaliators\AbstractProperties;
use common\modules\cnc\api\responses\costing\CostingResponse;
use common\modules\cnc\api\responses\costing\Instrument;
use common\modules\cnc\api\responses\costing\PriceElement;

class CostingSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CostingResponse::class => [
                'count',
                'currency',
                'priceElements',
                'total',
                'instrument'
            ],
            PriceElement::class => [
                'price',
                'title',
                'isPerPart'
            ],
            Instrument::class => [
                'd',
                'delta',
                'fr',
                'l',
                'material',
                'quality',
                's'
            ],
        ];
    }
}