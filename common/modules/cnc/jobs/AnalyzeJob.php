<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\jobs;


use common\components\exceptions\AssertHelper;
use common\models\FileJob;
use common\models\Model3dPart;
use common\models\Model3dPartCncParams;
use common\modules\cnc\api\Api;
use console\jobs\FileQueueJob;
use console\jobs\RabbitJob;
use yii\base\UserException;

class AnalyzeJob extends FileQueueJob implements RabbitJob
{
    /**
     * @var string
     */
    protected $code = FileJob::JOB_CNC_ANALYZE;

    /**
     * @var Api
     */
    private $api;

    /**
     * @param Api $api
     */
    public function injectDependencies(Api $api)
    {
        $this->api = $api;
    }

    /**
     * Execute job.
     * Put job code here
     *
     * @return mixed
     */
    public function doJob()
    {
        $file = $this->getFile();
        $upload = null;
        $recognize = null;
        $analyze = null;
        /** @var Model3dPart $model3dPart */
        foreach ($file->model3dParts as $model3dPart) {
            $api = $this->api;
            $upload = $api->upload($file);
            $recognize = $api->recognize($upload->getFilepath());
            $analyze = $api->analyze($upload->getFilepath(), $recognize->getRough(), $recognize->getFine());
            $params = new Model3dPartCncParams();
            $params->filepath = $upload->getFilepath();
            $params->finefile = $recognize->getFine();
            $params->roughfile = $recognize->getRough();
            $params->analyze = $analyze->getResult();
            AssertHelper::assertSave($params);

            $model3dPart->setModel3dPartCncParams($params);
            $model3dPart->model3d_part_cnc_params_id = $params->id;
            $model3dPart->safeSave();

            foreach ($model3dPart->model3dReplicaParts as $model3dReplicaPart) {
                if (!$model3dReplicaPart->model3dPartCncParams) {
                    $paramsClone = new Model3dPartCncParams();
                    $paramsClone->attributes = $params;
                    $paramsClone->safeSave();
                    $model3dReplicaPart->setModel3dPartCncParams($params);
                    $model3dReplicaPart->model3d_part_cnc_params_id = $params->id;
                    $model3dReplicaPart->safeSave();
                }
            }
        }

        if (!$upload) {
            throw new UserException('Stl analyze failed');
        }

        return ['upload' => $upload, 'recognize' => $recognize, 'analyze' => $analyze];
    }
}