<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.08.17
 * Time: 17:58
 */

namespace common\modules\cnc\loggers;

use common\components\DateHelper;
use common\models\loggers\ModelArLogger;
use common\models\PsCncMachine;
use common\models\PsCncMachineHistory;
use frontend\models\user\UserFacade;

class PsCncMachineLogger extends ModelArLogger
{
    public function log(PsCncMachine $psCncMachine, $isNewRecord = false)
    {
        if ($psCncMachine->oldAttributes['description'] != $psCncMachine->description || $isNewRecord) {
            $psCncMachineHistory = new PsCncMachineHistory();
            $this->fillBaseAttributes($psCncMachineHistory, $psCncMachine);
            if ($isNewRecord) {
                $psCncMachineHistory->source = [];
            } else {
                $psCncMachineHistory->source = $psCncMachine->oldAttributes['description'] ?? [];
            }
            $psCncMachineHistory->result = $psCncMachine->description;
            $psCncMachineHistory->safeSave();
        }
    }
}