<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.07.17
 * Time: 17:30
 */

namespace common\modules\cnc\repositories;

use common\components\ArrayHelper;
use common\components\DateHelper;
use common\models\CompanyService;
use common\models\Ps;
use common\models\PsCncMachine;
use common\models\query\CompanyServiceQuery;
use common\modules\cnc\api\Api;
use common\modules\cnc\factories\PsCncFactory;
use common\modules\cnc\loggers\PsCncMachineLogger;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\BaseObject;

class PsCncMachineRepository extends BaseObject
{
    /**
     * @var PsCncFactory
     */
    public $psCncFactory;

    /**
     * @var PsCncMachineLogger
     */
    public $psCncMachineLogger;


    /**
     * @var Api
     */
    private $cncApi;

    /**
     * @param PsCncFactory $psCncFactory
     * @param PsCncMachineLogger $psCncMachineLogger
     * @param Api $cncApi
     */
    public function injectDependencies(PsCncFactory $psCncFactory, PsCncMachineLogger $psCncMachineLogger)
    {
        $this->psCncFactory = $psCncFactory;
        $this->psCncMachineLogger = $psCncMachineLogger;
    }

    public function getByPsMachine(CompanyService $psMachine)
    {
        if ($psMachine->type !== CompanyService::TYPE_CNC) {
            return null;
        }
        $data = $psMachine->psCncMachine->description;
        $psMachine->psCncMachine->psCnc = $this->psCncFactory->createPsCnc($psMachine->ps, $data);
        return $psMachine->psCncMachine;
    }

    /**
     * @param $id
     * @return PsCncMachine|null
     */
    public function getById($id)
    {
        $psCncMachine = PsCncMachine::tryFindByPk($id);
        return $this->getByPsMachine($psCncMachine->companyService);
    }

    protected function mapPsMachines($psMachines)
    {
        return ArrayHelper::map($psMachines, 'ps_cnc_machine_id', 'psCncMachine');
    }

    public function getAllByPs(Ps $ps)
    {
        $psMachines = $ps->getCompanyServices()->byType(CompanyService::TYPE_CNC)->all();
        return $this->mapPsMachines($psMachines);
    }

    /**
     * @param Ps $ps
     * @return PsCncMachine[]
     */
    public function getNotDeletedByPs(Ps $ps)
    {
        /** @var CompanyService[] $psMachines */
        $psMachines = $ps->getNotDeletedPsMachines()->byType(CompanyService::TYPE_CNC)->all();
        foreach ($psMachines as $psMachine) {
            $psMachine->setPsCncMachine($this->getByPsMachine($psMachine));
        }
        return $this->mapPsMachines($psMachines);
    }

    /**
     * @param Ps $ps
     * @param string $status
     * @return array
     */
    public function getByAvailability(Ps $ps, string $status)
    {
        $psCncMachines = $this->getNotDeletedByPs($ps);
        $result = [];
        $user = UserFacade::getCurrentUser();
        $forWidget =  $status==CompanyService::VISIBILITY_WIDGETONLY || $status==CompanyService::VISIBILITY_EVERYWHERE;
        foreach ($psCncMachines as $psCncMachine) {
            if ($psCncMachine->companyService->isVisibleForUser($user, $forWidget)) {
                $result[] = $psCncMachine;
                continue;
            }
        }
        return $result;
    }

    public function save(PsCncMachine $psCncMachine)
    {
        $json = Yii::$app->getModule('cnc')->psCncJsonGenerator->generateJsonServiceInfo($psCncMachine->psCnc);
        $psCncMachine->description = $json;
        $companyService = $psCncMachine->companyService;
        if ($psCncMachine->isNewRecord) {
            $psCncMachine->safeSave();
            $this->psCncMachineLogger->log($psCncMachine, true);
        } else {
            $this->psCncMachineLogger->log($psCncMachine);
            $psCncMachine->safeSave();
        }
        $companyService->updated_at = DateHelper::now();
        $companyService->safeSave();

        $cncApi = Yii::createObject(Api::class);
        $cncApi->upadteMachine($psCncMachine);
    }


    /**
     * @param int $id
     * @return PsCncMachine
     * @throws \yii\web\NotFoundHttpException
     */
    public function getMachineByCompanyService(int $id): PsCncMachine
    {
        $query = PsCncMachine::find()->innerJoinWith([
            'companyService' => function(CompanyServiceQuery $query) use($id) {
                $query->onlyCnc();
                $query->notDeleted();
                $query->andWhere([CompanyService::column('id') => $id]);
            }
        ]);return $query->tryOne();
    }
}