<?php

use frontend\widgets\Model3dUploadWidget;
use frontend\widgets\PrintByPsTitleWidget;

/** @var \common\models\Ps $ps */
?>
<div class="item-rendering-external-widget__header--upload">
    <?= $showPsTitle ? PrintByPsTitleWidget::widget(['ps' => $ps]) : ''; ?>
</div>
<?php
$additionalParams = [
    'psId'             => $ps->id,
    'machineId'        => $cncMachine-> id,
    'utmSource'        => 'cnc',
    'psPrinterIdOnly'  => true,
];

echo Model3dUploadWidget::widget(
    [
        'isPlaceOrderState' => true,
        'additionalParams'  => $additionalParams,
    ]
);
?>
