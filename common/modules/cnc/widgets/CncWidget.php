<?php
namespace common\modules\cnc\widgets;

use common\interfaces\Model3dBaseInterface;
use common\models\PsCncMachine;
use yii\base\Widget;

class CncWidget extends Widget
{
    /**
     * @var PsCncMachine
     */
    public $cncMachine;

    /**
     * @var bool
     */
    public $showPsTitle = true;

    /**
     *
     */
    public function run()
    {
        $viewVars = [
            'ps' => $this->cncMachine->companyService->company,
            'cncMachine' => $this->cncMachine,
            'showPsTitle' => $this->showPsTitle
        ];
        return $this->render(
            'cncUploadPage',
            $viewVars
        );

    }
}