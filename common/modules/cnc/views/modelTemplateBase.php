namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class Cnc<?= ucfirst($className) ?>

{
<?php
foreach ($properties as $propertyName => $propertyInfo) {
    echo '  /**' . "\n";
    echo '   * ' . $propertyInfo['title'] . "\n";
    echo '   * @var ' . $propertyInfo['type'] . "\n";
    echo '   */' . "\n";
    echo '  public $' . $propertyName . ";\n\n";
}
?>
}
