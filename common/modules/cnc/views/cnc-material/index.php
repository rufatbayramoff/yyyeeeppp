<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CncMaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cnc Materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-material-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cnc Material', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'cnc_material_group_id' => [
                    'attribute' => 'cnc_material_group',
                    'label'     => 'Cnc material group',
                    'value'     => function (\common\models\CncMaterialSame $model) {
                        return $model->cncMaterialGroup->title??'';
                    }
                ],
                'title',
                'density',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]
    ); ?>
</div>
