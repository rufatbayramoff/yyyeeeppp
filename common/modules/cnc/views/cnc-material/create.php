<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CncMaterial */

$this->title = 'Create Cnc Material';
$this->params['breadcrumbs'][] = ['label' => 'Cnc Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-material-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
