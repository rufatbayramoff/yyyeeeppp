<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CncMaterial */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cnc Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-material-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(
            'Delete',
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ]
        ) ?>
    </p>

    <?= DetailView::widget(
        [
            'model'      => $model,
            'attributes' => [
                'id',
                'cnc_material_group_id' => [
                    'label'     => 'Cnc materail group',
                    'attribute' => 'cnc_material_group_id',
                    'value'     => $model->cncMaterialGroup->title??'',
                ],
                'title',
                'density',
            ],
        ]
    ) ?>

</div>
