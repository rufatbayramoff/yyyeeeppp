<?php

use common\components\ArrayHelper;
use common\models\CncMaterialGroup;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CncMaterial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cnc-material-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'cnc_material_group_id')->widget(
        \kartik\select2\Select2::classname(),
        [
            'data'          => ArrayHelper::map(CncMaterialGroup::find()->asArray()->all(), 'id', 'title'),
            'options'       => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => false
            ],
        ]
    );
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'density')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
