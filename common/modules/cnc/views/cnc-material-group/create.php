<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CncMaterialGroup */

$this->title = 'Create Cnc Material Group';
$this->params['breadcrumbs'][] = ['label' => 'Cnc Material Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnc-material-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
