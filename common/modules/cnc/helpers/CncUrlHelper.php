<?php
namespace common\modules\cnc\helpers;

use common\models\PsCncMachine;
use common\modules\cnc\models\PsCnc;

class CncUrlHelper
{
    public static function tryCncUrl(PsCncMachine $cncMachine)
    {
        return '/workbench/cncm/filepage/try-cnc?machineId='.$cncMachine->id;
    }
}