<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.07.17
 * Time: 11:25
 */

namespace common\modules\cnc;

use common\modules\cnc\components\PsCncJsonGenerator;
use common\modules\cnc\components\PsCncPopulator;
use common\modules\cnc\components\SchemaManager;
use common\modules\cnc\components\SchemaParser;
use yii\base\Module;

/**
 * Class CncModule
 *
 * @property SchemaParser $schemaParser
 * @property SchemaManager $schemaManager
 * @property PsCncPopulator $psCncPopulator
 * @property PsCncJsonGenerator $psCncJsonGenerator
 * @package common\modules\cnc
 */
class CncModule extends Module
{
    public $rawCncSchema;

    protected function getComponentsList()
    {
        return [
            'psCncJsonGenerator' => PsCncJsonGenerator::class,
            'psCncPopulator'     => PsCncPopulator::class,
            'schemaParser'       => SchemaParser::class,
            'schemaManager'      => SchemaManager::class
        ];
    }

    protected function initComponentsModule($componentsList)
    {
        foreach ($componentsList as $componentName => $componentClass) {
            $this->$componentName->setModule($this);
        }
    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    protected function initConfig()
    {

    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();

        $this->initConfig();
        $componentsList = $this->getComponentsList();
        $this->setComponents($componentsList);
        $this->initComponentsModule($componentsList);
    }
}