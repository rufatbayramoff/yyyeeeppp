<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.07.17
 * Time: 15:41
 */

namespace common\modules\cnc\factories;

use common\models\CompanyService;
use common\models\CompanyServiceCategory;
use common\models\factories\PcMachineFactory;
use common\models\Ps;
use common\models\PsCncMachine;
use common\modules\cnc\components\PsCncPopulator;
use frontend\modules\companyServices\controllers\ServicesController;

class PsCncMachineFactory
{
    /**
     * @var PsCncFactory
     */
    public $psCncFactory;

    /**
     * @var PcMachineFactory
     */
    public $psMachineFactory;

    /**
     * @var PsCncPopulator
     */
    public $psCncPopulator;

    /**
     * @var PcMachineFactory
     * */
    protected $companyServiceFactory;


    /**
     * @param PsCncFactory $psCncFactory
     * @param PcMachineFactory $psMachineFactory
     * @param PsCncPopulator|null $psCncPopulator
     */
    public function injectDependencies(PsCncFactory $psCncFactory, PcMachineFactory $psMachineFactory, PsCncPopulator $psCncPopulator = null)
    {
        $this->psCncFactory = $psCncFactory;
        $this->companyServiceFactory = $psMachineFactory;
        $this->psCncPopulator = $psCncPopulator ?? \Yii::$app->getModule('cnc')->psCncPopulator;
    }

    public function createCncMachine(Ps $ps, $data): \common\models\PsCncMachine
    {

        $cncCategory = CompanyServiceCategory::findOne(['code'=>CompanyServiceCategory::CODE_CNC]);
        $psCncMachine = new PsCncMachine();
        $psCncMachine->code = 'Cnc' . random_int(0, 999) . '_' . str_replace(['.', ' '], '', microtime(false));
        $psCncMachine->psCnc = $this->psCncFactory->createPsCnc($ps, $data['psCnc'] ?? []);
        $psMachine = $this->companyServiceFactory->createPsMachine($ps, $data['psMachine'] ?? []);
        $psMachine->moderator_status = CompanyService::MODERATOR_STATUS_PENDING;
        $psMachine->type = CompanyService::TYPE_CNC;
        $psMachine->setPendingModeration();
        if ($cncCategory) {
            $psMachine->category_id = $cncCategory->id;
        }
        $psMachine->setPsCncMachine($psCncMachine);
        $psCncMachine->setPsMachine($psMachine);
        return $psCncMachine;
    }

}