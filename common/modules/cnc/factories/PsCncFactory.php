<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.07.17
 * Time: 11:52
 */

namespace common\modules\cnc\factories;

use common\models\Ps;
use common\modules\cnc\components\PsCncPopulator;
use common\modules\cnc\models\CncCompany;
use common\modules\cnc\models\CncMaterial;
use common\modules\cnc\models\CncMillingInstrument;
use common\modules\cnc\models\CncMillingMachine;
use common\modules\cnc\models\CncSettings;
use common\modules\cnc\models\CncStocksFreeForm;
use common\modules\cnc\models\CncStocksSlab;
use common\modules\cnc\models\PsCnc;
use yii\base\Component;

class PsCncFactory extends Component
{

    /**
     * @var PsCncPopulator
     */
    public $psCncPopulator;

    /**
     * @param PsCncPopulator $psCncPopulator
     */
    public function injectDependencies(PsCncPopulator $psCncPopulator)
    {
        $this->psCncPopulator = $psCncPopulator;
    }

    /**
     * @param Ps $ps
     * @param array $data
     * @return PsCnc
     */
    public function createPsCnc(Ps $ps, array $data)
    {
        $psCnc = new PsCnc();
        $psCnc->cncCompany = new CncCompany();
        $psCnc->settings = new CncSettings();
        $psCnc->millingMachines = [];
        $psCnc->millingInstruments = [];
        $psCnc->cuttingMachines = [];
        $psCnc->stocksFreeForm = [];
        $psCnc->stocksSlabs = [];
        $psCnc->postProcessing = [];

        $psCnc->materials = [];
        $psCnc->cncCompany->name = $ps->title;
        $psCnc->cncCompany->currency = $ps->currency;


        $psCnc->materials[] = new CncMaterial();
        $psCnc->millingMachines[] = new CncMillingMachine();
        $psCnc->millingInstruments[] = new CncMillingInstrument();
        $psCnc->stocksFreeForm[] = new CncStocksFreeForm();
        $psCnc->stocksSlabs[] = new CncStocksSlab();

        $this->psCncPopulator->populate($psCnc, $data);
        return $psCnc;
    }
}