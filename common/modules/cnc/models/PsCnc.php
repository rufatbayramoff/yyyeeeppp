<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.07.17
 * Time: 17:05
 */

namespace common\modules\cnc\models;

class PsCnc
{
    public $code;

    /**
     * @var CncCompany
     */
    public $cncCompany;

    /**
     * @var CncMaterial[]
     */
    public $materials;

    /**
     * @var CncSettings
     */
    public $settings;

    /**
     * @var CncMillingMachine[]
     */
    public $millingMachines;

    /**
     * @var CncMillingInstrument[]
     */
    public $millingInstruments;

    /**
     * @var CncCuttingMachine[]
     */
    public $cuttingMachines;

    /**
     * @var CncStocksFreeForm[]
     */
    public $stocksFreeForm;

    /**
     * @var CncStocksSlab[]
     */
    public $stocksSlabs;

    /**
     * @var CncPostprocessing[]
     */
    public $postProcessing;

    public function isAllowedWidget()
    {
        if (!$this->millingMachines || !$this->materials) {
            return false;
        }

        $hasWorkMachine = false;
        foreach ($this->millingMachines as $millingMachine) {
            if ($millingMachine->maxRPM && $millingMachine->power && $millingMachine->volumeW && $millingMachine->volumeH && $millingMachine->volumeL) {
                $hasWorkMachine = true;
                break;
            }
        }
        if (!$hasWorkMachine) {
            return false;
        }

        $hasWorkMaterial = false;
        foreach ($this->materials as $material) {
            if ($material->density) {
                $hasWorkMaterial = true;
                break;
            }
        }
        if (!$hasWorkMaterial) {
            return false;
        }
        return true;
    }
}
