<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncMillingMachine
{
  /**
   * Machine Title
   * @var string
   */
  public $title;

  /**
   * Max. Speed (rot/min)
   * @var number
   */
  public $maxRPM;

  /**
   * Main Spindle Power (watts)
   * @var number
   */
  public $power;

  /**
   * Work Volume Width (mm)
   * @var number
   */
  public $volumeW = 100;

  /**
   * Work Volume Height (mm)
   * @var number
   */
  public $volumeH = 100;

  /**
   * Work Volume Length (mm)
   * @var number
   */
  public $volumeL = 100;

  /**
   * Machining Cost ($/hr)
   * @var number
   */
  public $priceMachining;

  /**
   * Labor Cost of Operator ($/hr)
   * @var number
   */
  public $priceLabourOperator;

  /**
   * Labor Cost of Programmer ($/h)
   * @var number
   */
  public $priceLabourProgrammer;

  /**
   * Preparation Time for Stock (sec)
   * @var number
   */
  public $placetime;

  /**
   * Preparation Time for CNC program (min)
   * @var number
   */
  public $programmingtime;

}
