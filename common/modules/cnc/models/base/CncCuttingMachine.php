<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncCuttingMachine
{
  /**
   * Machine Title
   * @var string
   */
  public $title;

  /**
   * Machining Price ($/h)
   * @var number
   */
  public $priceMachining;

  /**
   * Cutting Modes
   * @var array
   */
  public $speeds;

}
