<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncMaterial
{
  /**
   * Material Type
   * @var string
   */
  public $material;

  /**
   * Material Title
   * @var string
   */
  public $title;

  /**
   * Density (kg/m3)
   * @var number
   */
  public $density;

  /**
   * Default Tolerance (mm)
   * @var number
   */
  public $defaultTolerance;

  /**
   * Best Tolerance (mm)
   * @var number
   */
  public $maxTolerance;

}
