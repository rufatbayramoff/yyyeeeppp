<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncPostprocessing
{
  /**
   * Describe Post Process
   * @var string
   */
  public $title;

  /**
   * Price per Part
   * @var number
   */
  public $pricePerPart;

  /**
   * Price per dm2 of Area
   * @var number
   */
  public $pricePerArea;

  /**
   * Price per dm3 of Volume
   * @var number
   */
  public $pricePerVolume;

}
