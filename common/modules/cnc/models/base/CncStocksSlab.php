<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncStocksSlab
{
  /**
   * Material
   * @var string
   */
  public $material;

  /**
   * Thickness (mm)
   * @var number
   */
  public $thickness;

  /**
   * Price ($/kg)
   * @var number
   */
  public $price;

}
