<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncCuttingMode
{
  /**
   * Material
   * @var string
   */
  public $material;

  /**
   * Max. Thickness (mm)
   * @var number
   */
  public $maxThickness;

  /**
   * Max. Speed (mm/sec)
   * @var number
   */
  public $cuttingSpeed;

}
