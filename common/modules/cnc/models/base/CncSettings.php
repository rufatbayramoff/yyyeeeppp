<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncSettings
{
  /**
   * Minimum Order Charge
   * @var number
   */
  public $minCostPrice;

  /**
   * Mark up Rate for Machining & Labor (%)
   * @var number
   */
  public $chargeJob;

  /**
   * Mark up Rate for Materials (%)
   * @var number
   */
  public $chargeMaterial;

  /**
   * Total Allowance (mm)
   * @var number
   */
  public $stockSpacers;

  /**
   * Set Default Material
   * @var string
   */
  public $defaultMaterial;

}
