<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncStocksFreeForm
{
  /**
   * Material
   * @var string
   */
  public $material;

  /**
   * Price ($/kg)
   * @var number
   */
  public $price;

}
