<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncCompany
{
  /**
   * Company Name
   * @var string
   */
  public $name;

  /**
   * Company Phone
   * @var string
   */
  public $phone;

  /**
   * Company Website
   * @var string
   */
  public $website;

    /**
     * Currency
     * @var string
     */
  public $currency;

}
