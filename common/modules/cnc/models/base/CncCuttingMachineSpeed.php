<?php
namespace common\modules\cnc\models\base;

use Yii;

/**
*
*/
class CncCuttingMachineSpeed
{
  /**
   * Material
   * @var string
   */
  public $material;

  /**
   * Max. Thickness (mm)
   * @var number
   */
  public $maxThickness;

  /**
   * Max. Speed (mm/sec)
   * @var number
   */
  public $cuttingSpeed;

}
