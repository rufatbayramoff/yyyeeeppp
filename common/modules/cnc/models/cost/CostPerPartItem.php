<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\models\cost;


use common\interfaces\Model3dBasePartInterface;

/**
 * Class CostPerPartItem
 *
 * List item of cost for concrete part
 *
 * @package common\modules\cnc\models\cost
 */
class CostPerPartItem
{
    /**
     * @var Model3dBasePartInterface
     */
    public $part;

    /**
     * @var CostItem[]
     */
    public $costs;
}