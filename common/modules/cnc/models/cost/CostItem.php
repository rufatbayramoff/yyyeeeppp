<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\models\cost;


use lib\money\Money;

/**
 * Class CostItem
 * @package common\modules\cnc\models\cost
 */
class CostItem
{
    /**
     * Type of work part
     * @var string|null
     */
    public $type;

    /**
     * Cost of work part
     * @var Money
     */
    public $cost;

    /**
     * Description of work part
     * @var string
     */
    public $description;
}