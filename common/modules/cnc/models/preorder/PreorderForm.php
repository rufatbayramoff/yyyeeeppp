<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\models\preorder;


use yii\base\Model;

class PreorderForm extends Model
{
    /**
     * @var int
     */
    public $machineId;

    /**
     * @var string
     */
    public $modelId;

    /**
     * @var \common\modules\cnc\models\preorder\PreorderDescription
     */
    public $preorder;

    /**
     * @var \common\modules\cnc\api\Preset[]
     */
    public $presets;

    /**
     * @var string
     */
    public $email;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['machineId', 'modelId'], 'required'],
            [['machineId'], 'integer'],
            [['email', 'modelId'], 'string'],
            [['email'], 'safe']
        ];
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        $isValid = parent::validate($attributeNames, $clearErrors);

        if (!$this->preorder->validate()) {
            $this->addErrors(['preorder' => $this->preorder->errors]);
            $isValid = false;
        }
        $this->email = $this->preorder->email;

        foreach ($this->presets as $preset) {
            if (!$preset->validate()) {
                $this->addErrors(['presets' => $preset->errors]);
                $isValid = false;
            }
        }

        return $isValid;
    }
}