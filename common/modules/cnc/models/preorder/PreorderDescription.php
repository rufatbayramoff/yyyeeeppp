<?php
/**
 * Created by mitaichik
 */

namespace common\modules\cnc\models\preorder;


use yii\base\Model;

class PreorderDescription extends Model
{
    /**
     * Name of customer
     * @var string
     */
    public $name;

    /**
     * Description of models an preorder
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $budget;

    /**
     * Estimate time for order
     * @var string
     */
    public $estimateTime;

    /**
     * @var string
     */
    public $email;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description', 'estimateTime'], 'string'],
            [['budget'], 'number'],
            [['email'], 'string'],
            [['email'], 'safe']
        ];
    }
}