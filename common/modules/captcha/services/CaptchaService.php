<?php

namespace common\modules\captcha\services;

use common\components\exceptions\BusinessException;
use common\models\user\UserIdentityProvider;
use common\modules\payment\services\PaymentService;
use ReCaptcha\ReCaptcha;
use Yii;

class CaptchaService extends \yii\base\BaseObject
{
    /** @var UserIdentityProvider */
    protected $userIdentityProvider;

    /** @var PaymentService */
    protected $paymentService;

    public function injectDependencies(UserIdentityProvider $userIdentityProvider, PaymentService $paymentService)
    {
        $this->userIdentityProvider = $userIdentityProvider;
        $this->paymentService = $paymentService;
    }


    public function isCaptchaRequired()
    {
        if (Yii::$app->getModule('captcha')->isDisabled) {
            return false;
        }
        $currentUser = $this->userIdentityProvider->getUser();
        if ($currentUser && $this->paymentService->userHasPayments($currentUser)) {
            return false;
        }
        return true;

    }

    public function checkGoogleRecaptcha2Response()
    {
        if (!$this->isCaptchaRequired()) {
            return;
        }

        if (!$gRecaptchaResponse = \Yii::$app->request->post('g-recaptcha-response')) {
            throw new BusinessException('Please, fill recaptcha.');
        }

        $recaptchaSecret = Yii::$app->getModule('captcha')->googlePrivateKey;

        $recaptcha = new ReCaptcha($recaptchaSecret);
        $resp      = $recaptcha->verify($gRecaptchaResponse, $_SERVER['REMOTE_ADDR']);
        if (!$resp->isSuccess()) {
            throw new BusinessException('Invalid captcha');
        }
    }
}