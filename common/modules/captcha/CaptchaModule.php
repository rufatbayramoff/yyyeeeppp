<?php

namespace common\modules\captcha;

use yii\base\Module;

class CaptchaModule extends Module
{
    /**
     * @var bool
     */
    public $isDisabled = false;

    /**
     * @var string
     */
    public $googlePrivateKey;

    /**
     * @var string
     */
    public $googlePublicKey;
}