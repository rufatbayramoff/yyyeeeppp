<?php
namespace common\modules\captcha\widgets;

use common\modules\captcha\services\CaptchaService;
use Yii;
use yii\base\Widget;

class GoogleRecaptcha2 extends Widget
{


    public function run()
    {
        $captchaService = Yii::createObject(CaptchaService::class);
        if (!$captchaService->isCaptchaRequired()) {
            return '';
        }

        $publicKey = Yii::$app->getModule('captcha')->googlePublicKey;
        return '<script src="https://www.google.com/recaptcha/api.js"></script>'.
               '<div class="g-recaptcha" data-sitekey="'.$publicKey.'"></div>';
    }
}
