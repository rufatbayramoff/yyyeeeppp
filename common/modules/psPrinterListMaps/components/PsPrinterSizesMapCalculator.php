<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.04.17
 * Time: 14:40
 */

namespace common\modules\psPrinterListMaps\components;

use common\components\ps\locator\PrinterRepository;
use common\models\PrinterMaterialGroup;
use common\models\PsPrinter;
use common\models\StoreOrder;
use lib\MeasurementUtil;
use Nette\Caching\Storages\SQLiteStorage;
use Yii;
use yii\base\Component;
use yii\base\Module;

class PsPrinterSizesMapCalculator extends BaseMapCalculator
{
    protected $module;

    const MAX_STEP_LEN_MINUS = 31;
    const MAX_STEP_LEN_PLUS = 31;
    const MAX_TOP_PROCENT = 0.10;
    const MAX_SIZES_IN_GROUP = 20;

    /**
     * Orders count by sizes
     *
     * @var array
     */
    protected $ordersCount = [

    ];

    /**
     * Order sizes
     *
     * @var array
     */
    protected $orderSizes = [

    ];

    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    protected function formVolumeSize($volumeSize)
    {
        $resultSize = '';
        sort($volumeSize);
        foreach ($volumeSize as $oneSizeElement) {
            $oneSizeElement = (int)$oneSizeElement;
            $resultSize .= str_pad($oneSizeElement, 8, '0', STR_PAD_LEFT) . 'x';
        }
        $resultSize = substr($resultSize, 0, -1);
        return $resultSize;
    }

    protected function reduceSizesToCount($sizes)
    {
        $sizesByCount = [];
        foreach ($sizes as $key => $value) {
            $sizesByCount[$key] = count($value['psPrinters']);
        }
        return $sizesByCount;
    }

    protected function reduceSizesToCountWithOrders($sizes)
    {
        $sizesByCount = [];
        foreach ($sizes as $key => $value) {
            $count = 0;
            foreach ($value['psPrinters'] as $psPrinter) {
                if (array_key_exists($psPrinter, $this->ordersCount)) {
                    $count += $this->ordersCount[$psPrinter];
                }
            }

            if ($count || 1) {
                $sizesByCount[$key] = [
                    'printersCount' =>
                        count($value['psPrinters']),
                    'ordersCount'   => $count
                ];
            }
        }
        return $sizesByCount;
    }

    protected function getTopPrintersCountStep($sizes)
    {
        $sizesByCount = $this->reduceSizesToCount($sizes);
        sort($sizesByCount);
        $tops = count($sizesByCount) - (count($sizesByCount) * self::MAX_TOP_PROCENT);
        return $sizesByCount[(int)$tops];
    }

    protected function getTopSizes($sizes, $topStep)
    {
        $topSizes = [];
        foreach ($sizes as $key => $value) {
            if (count($value['psPrinters']) >= $topStep) {
                $topSizes[$key] = $value;
            }
        }
        uasort(
            $topSizes,
            function ($a, $b) {
                if (count($a['psPrinters']) < count($b['psPrinters'])) {
                    return 1;
                }
            }
        );
        return $topSizes;
    }

    public function addToGroupedSizes(&$groupedSizes, $groupSize, $sizeElement)
    {
        $sizeKey = $this->formVolumeSize($groupSize['size']);
        if (array_key_exists($sizeKey, $groupedSizes)) {
            foreach ($sizeElement['psPrinters'] as $psPrinter) {
                if (!in_array($psPrinter, $groupedSizes[$sizeKey]['psPrinters'])) {
                    $groupedSizes[$sizeKey]['psPrinters'][] = $psPrinter;
                }
            }
        } else {
            $groupedSizes[$sizeKey] = $sizeElement;
        }
    }

    protected function isMoreSize($largeSize, $smallSize)
    {
        if ($largeSize[0] > $smallSize[0]) {
            return true;
        }
        if ($largeSize[0] == $smallSize[0]) {
            if ($largeSize[1] > $smallSize[1]) {
                return true;
            }
            if ($largeSize[1] == $smallSize[1]) {
                if ($largeSize[2] > $smallSize[2]) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function getSizeMaxDistance($sizeA, $sizeB)
    {
        $distance = [
            abs($sizeA[0] - $sizeB[0]),
            abs($sizeA[1] - $sizeB[1]),
            abs($sizeA[2] - $sizeB[2])
        ];
        return max($distance);
    }

    protected function minimizeTopSizes($tops)
    {
        $ordersSizes = $this->orderSizes;
        foreach ($tops as $sizeKey => $value) {
            $minSize = $value['size'];
            if (array_key_exists('minSizes', $value)) {
                foreach ($value['minSizes'] as $minKey => $subMinValue) {
                    $minSize[0] = min($minSize[0], $subMinValue[0]);
                    $minSize[1] = min($minSize[1], $subMinValue[1]);
                    $minSize[2] = min($minSize[2], $subMinValue[2]);
                }
                $value['size'] = $minSize;
                $newSizeKey = $this->formVolumeSize($minSize);
                unset($tops[$sizeKey]);
                $tops[$newSizeKey] = $value;
            }
        }
        return $tops;
    }

    protected function groupSizes($sizes, $tops)
    {
        // Group tops if possible
        $i = 0;
        foreach ($tops as $sizeKey => $value) {
            $j = 0;
            foreach ($tops as $sizeKey2 => $value2) {
                if ($j <= $i) {
                    $j++;
                    continue;
                }

                if ($this->isMoreSize($value2['size'], $value['size'])) {
                    if ($this->getSizeMaxDistance($value['size'], $value2['size']) < self::MAX_STEP_LEN_PLUS) {
                        $this->addToGroupedSizes($tops, $value, $value2);
                        $tops[$sizeKey]['maxSizes'][$this->formVolumeSize($value2['size'])] = $value2['size'];
                        unset($tops[$sizeKey2]);
                    }
                } else {
                    if ($this->getSizeMaxDistance($value['size'], $value2['size']) < self::MAX_STEP_LEN_MINUS) {
                        $this->addToGroupedSizes($tops, $value, $value2);
                        $tops[$sizeKey]['minSizes'][$this->formVolumeSize($value2['size'])] = $value2['size'];
                        unset($tops[$sizeKey2]);
                    }
                }
            }
            $i++;
        }

        // Group other sizes
        foreach ($tops as $sizeKey => $value) {
            foreach ($sizes as $sizeKey2 => $value2) {
                if ($sizeKey === $sizeKey2) {
                    unset($sizes[$sizeKey2]);
                    continue;
                }
                if ($this->isMoreSize($value2['size'], $value['size'])) {
                    if ($this->getSizeMaxDistance($value['size'], $value2['size']) < self::MAX_STEP_LEN_PLUS) {
                        $this->addToGroupedSizes($tops, $value, $value2);
                        $tops[$sizeKey]['maxSizes'][$this->formVolumeSize($value2['size'])] = $value2['size'];
                        unset($sizes[$sizeKey2]);
                    }
                } else {
                    if ($this->getSizeMaxDistance($value['size'], $value2['size']) < self::MAX_STEP_LEN_MINUS) {
                        $this->addToGroupedSizes($tops, $value, $value2);
                        $tops[$sizeKey]['minSizes'][$this->formVolumeSize($value2['size'])] = $value2['size'];
                        unset($sizes[$sizeKey2]);
                    }
                }
            }
        }

        return [
            'tops'  => $tops,
            'sizes' => $sizes
        ];
    }

    public function caclPrintersCount($sizes)
    {
        $printersCount = 0;
        foreach ($sizes as $sizeKey => $sizeElement) {
            $printersCount = $printersCount + count($sizeElement['psPrinters']);
        }
        return $printersCount;
    }

    public function formOrdersSizes()
    {
        $sizes = [];
        /** @var StoreOrder[] $completedOrders */
        $completedOrders = StoreOrder::findAll(['order_state' => 'completed']);
        foreach ($completedOrders as $order) {
            foreach ($order->storeOrderItems as $storeOrderItem) {
                $size = $storeOrderItem->model3dReplica->getPrintCameraSize();
                $sizeMM = $size->convertToMeasurement(MeasurementUtil::MM);
                $sizeArr = [$size->height, $size->width, $size->length];
                $sizeStr = $this->formVolumeSize($sizeArr);
                $printerId = $order->currentAttemp->machine->psPrinter->id??'';
                if (!$printerId) {
                    continue;
                }
                $sizes[$sizeStr] = [
                    'size'      => $sizeArr,
                    'storeUnit' => $storeOrderItem->model3dReplica->storeUnit->id,
                    'order'     => $order->id,
                    'printer'   => $printerId
                ];
                if (!array_key_exists($printerId, $this->ordersCount)) {
                    $this->ordersCount[$printerId] = 1;
                }
                {
                    $this->ordersCount[$printerId]++;
                }
            }
        }
        $this->orderSizes = $sizes;
    }

    protected function sortSizesBySize($sizes)
    {
        uasort(
            $sizes,
            function ($a, $b) {
                $aSize = $a['size'];
                sort($aSize);
                $bSize = $b['size'];
                sort($bSize);
                if ($aSize[0] > $bSize[0]) {
                    return 1;
                }
                if ($aSize[0] === $bSize[0]) {
                    if ($aSize[1] > $bSize[1]) {
                        return 1;
                    }
                    if ($aSize[1] == $bSize[1]) {
                        if ($aSize[2] > $bSize[2]) {
                            return 1;
                        }
                        if ($aSize[2] == $bSize[2]) {
                            return 0;
                        }
                    }

                }
                return -1;
            }
        );
        return $sizes;
    }

    protected function restoreIndexes($sizes)
    {
        $restoredKeys = [];
        foreach ($sizes as $size) {
            $volumeSize = $this->formVolumeSize($size['size']);
            $restoredKeys[$volumeSize] = $size;
        }
        return $restoredKeys;
    }

    protected function getOthersSizes($sizes, $maxCount)
    {
        $sizes = $this->sortSizesBySize($sizes);

        $sizesWithIndex = array_values($sizes);

        $sizesCount = count($sizes);
        $step = ceil($sizesCount / $maxCount);
        $stepsSizes = [];
        for ($i = 0; $i < $sizesCount; $i += $step) {
            $stepsSizes[$i] = $sizesWithIndex[$i];
            $sizesWithIndex[$i];
        }
        // Если нет последнего элемента - дополняем
        if ((count($stepsSizes) < $maxCount) && (array_key_exists($sizesCount - 1, $sizesWithIndex))) {
            $stepsSizes[$sizesCount - 1] = $sizesWithIndex[$sizesCount - 1];
            unset($sizesWithIndex[$sizesCount - 1]);
        }
        return $this->restoreIndexes($stepsSizes);

    }

    protected function checkIsBackCombinations($sizes)
    {
        $prevSize = [0, 0, 0];
        foreach ($sizes as $size) {
            $eachSize = $size['size'];
            if ($eachSize[0] > $prevSize[0]) {
                $prevSize = $eachSize;
                continue;
            }
            if ($eachSize[0] == $prevSize[0]) {
                if (($eachSize[1] > $prevSize[1]) && ($eachSize[2] < $prevSize[2])) {
                    echo "\nAlert combinations multiplay!";
                    var_dump($eachSize);
                }
            }

        }
    }

    /**
     * @param PsPrinter[] $psPrinters
     * @return array
     */
    protected function processPrintersGroup($psPrinters)
    {
        $sizes = [];
        foreach ($psPrinters as $psPrinter) {
            $buildSize = $psPrinter->getBuildVolume();
            $buildSize[0] = (int)$buildSize[0];
            $buildSize[1] = (int)$buildSize[1];
            $buildSize[2] = (int)$buildSize[2];
            $volumeSize = $this->formVolumeSize($buildSize);
            if (array_key_exists($volumeSize, $sizes)) {
                $sizes[$volumeSize]['psPrinters'][] = $psPrinter->id;
            } else {
                $sizes[$volumeSize] = [
                    'size'       => $buildSize,
                    'psPrinters' => [
                        $psPrinter->id
                    ]
                ];
            }
        }
        if ($sizes < self::MAX_SIZES_IN_GROUP) {
            return $sizes;
        }
        $topStep = $this->getTopPrintersCountStep($sizes);
        $tops = $this->getTopSizes($sizes, $topStep);
        $groupedSizes = $this->groupSizes($sizes, $tops);
        $minimizedTops = $this->minimizeTopSizes($groupedSizes['tops']);
        $otherSizesCount = self::MAX_SIZES_IN_GROUP - count($minimizedTops);
        $minimizedOthers = $this->getOthersSizes($groupedSizes['sizes'], $otherSizesCount);
        $othersGroupedSizes = $this->groupSizes($groupedSizes['sizes'], $minimizedOthers);
        $minimizedOthersSizes = $this->minimizeTopSizes($othersGroupedSizes['tops']);
        $mergedSizes = array_merge(array_values($minimizedTops), array_values($minimizedOthersSizes));
        $resultSizes = $this->sortSizesBySize($mergedSizes);
        $resultSizes = $this->restoreIndexes($resultSizes);
        $this->checkIsBackCombinations($resultSizes);
        return $resultSizes;
    }

    protected function formLastPrintersSizesMap($sizesByGroups)
    {
        $sortedLastPrintersSizesMap = [];
        foreach ($sizesByGroups as $groupKey => $sizeMap) {
            $sizes = [];
            foreach ($sizeMap as $sizeKey => $sizeElement) {
                sort($sizeElement['size']);
                $sortedLastPrintersSizesMap[$sizeKey] = $sizeElement['size'];
            }
        }
        asort($sortedLastPrintersSizesMap);

        $resultLastPrintersSizesMap = [];
        // Merge in to small sizes, big sizes
        // $size->width . 'x' . $size->height . 'x' . $size->length;
        $i=0;
        foreach ($sortedLastPrintersSizesMap as $sizeKey => $sizeElement)
        {
            $resultLastPrintersSizesMap[$sizeKey] = $sizeElement;
            $i++;
            $j = 0;
            foreach ($sortedLastPrintersSizesMap as $sizeElement2) {
                $j++;
                if ($i>=$j) {
                    continue;
                }
                $sizeElementMerged = $sizeElement;
                $sizeElementMerged[1] = min($sizeElement[1], $sizeElement2[1]);
                $sizeElementMerged[2] = min($sizeElement[2], $sizeElement2[2]);
                if ($sizeElementMerged != $sizeElement) {
                    $sizeElementMergedKey = $this->formVolumeSize($sizeElementMerged);
                    $resultLastPrintersSizesMap[$sizeElementMergedKey] = $sizeElementMerged;
                }
            }
        }
        $resultLastPrintersSizesMap = array_values($resultLastPrintersSizesMap);
        return $resultLastPrintersSizesMap;
    }

    public function getMinimizedPrinterSizesList()
    {
        $this->formOrdersSizes();

        /** @var PsPrinter[] $psPrinters */
        $sizesByGroups = [];
        $printersByGroups = $this->getPrintersByGroups();
        foreach ($printersByGroups as $groupName => $psPrinters) {
            $sizesByGroups[$groupName] = $resultSizes = $this->processPrintersGroup($psPrinters);
            echo "\nResult sizes for group: " . $groupName . ' - ' . count($resultSizes) . " \n";
            echo json_encode($this->reduceSizesToCountWithOrders($resultSizes), JSON_PRETTY_PRINT);
        }
        $lastPrintersSizesMap = $this->formLastPrintersSizesMap($sizesByGroups);
        file_put_contents(
            Yii::getAlias('@common/config/filepageMaps/last-sizes-map.php'),
            '<?php return ' . var_export($lastPrintersSizesMap, true) . ';?>'
        );
        return $sizesByGroups;
    }
}