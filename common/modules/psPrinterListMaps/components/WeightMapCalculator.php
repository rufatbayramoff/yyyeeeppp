<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.04.17
 * Time: 17:12
 */

namespace common\modules\psPrinterListMaps\components;

use common\models\Printer;
use common\models\PsPrinter;
use common\models\StoreOrder;
use Yii;
use yii\base\Component;
use yii\base\Module;

class WeightMapCalculator extends BaseMapCalculator
{
    protected $module;

    protected $ordersWeight;

    const MIN_WEIGHT = 3;
    const MAX_WEIGHT = 150;
    const STEP_COUNT = 30;

    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    public function getOrdersWeight()
    {
        $ordersWeight = [];
        /** @var StoreOrder[] $completedOrders */
        $completedOrders = StoreOrder::findAll(['order_state' => 'completed']);
        foreach ($completedOrders as $order) {
            foreach ($order->storeOrderItems as $storeOrderItem) {
                $model3d = $storeOrderItem->model3dReplica;
                $weight = 0;
                $ordersWeight[$weight][$order->id] = $order->getTitle();
                foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
                    $weight += $model3dPart->getWeight() * $model3dPart->qty;
                }
                $weight *= $storeOrderItem->qty;
                $weight = round($weight);
                $ordersWeight[$weight][$order->id] = $order->getTitle() . ' Cost: ' . $order->price_total;
            }
        }
        ksort($ordersWeight);
        echo "\nOrders weight:\n";
        var_dump($ordersWeight);
        $this->ordersWeight = $ordersWeight;
        return $ordersWeight;
    }

    public function showMinPrintPriceNotSetted()
    {
        $nullMinPrice = 0;
        $printersByGroups = $this->getPrintersByGroups();
        foreach ($printersByGroups as $groupMaterialName => $printerGroup) {
            foreach ($printerGroup as $psPrinter) {
                /** @var PsPrinter $psPrinter */
                if ($psPrinter->min_order_price < 1) {
                    $nullMinPrice++;
                    echo "\nMin print price: ";
                    echo $psPrinter->id . ' ' . $psPrinter->title . ' ' . $psPrinter->companyService->location->country->title;
                }
            }
        }
        echo "\nTotal min price printers count: " . $nullMinPrice;
    }

    public function getWeightRanges()
    {
        $iMin = 1;
        $iMax = 4;
        $lenx = $iMax - $iMin;
        $leny = ($iMax * $iMax * 10 - 10) - ($iMin * $iMin * 10 - 10);
        $step = $lenx / self::STEP_COUNT;

        $realLen = self::MAX_WEIGHT - self::MIN_WEIGHT;
        $costs = [];

        for ($i = $iMin; $i < $iMax; $i += $step) {
            $val = $i * $i * 10 - 10;
            $t = self::MIN_WEIGHT + $realLen / $leny * $val;
            $t = round($t);
            $costs[] = $t;
        }
        if (count($costs) < self::STEP_COUNT) {
            $costs[] = self::MAX_WEIGHT;
        }
        return $costs;
    }

    public function getMinimizedList()
    {
        $reducedWeight = $this->getWeightRanges();

        var_dump($reducedWeight);

        file_put_contents(
            Yii::getAlias('@common/config/filepageMaps/last-weight-map.php'),
            '<?php return ' . var_export($reducedWeight, true) . ';?>'
        );
        return $reducedWeight;
    }
}