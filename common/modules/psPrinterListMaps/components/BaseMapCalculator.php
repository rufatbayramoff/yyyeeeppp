<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.04.17
 * Time: 17:27
 */

namespace common\modules\psPrinterListMaps\components;

use common\components\ps\locator\PrinterRepository;
use common\models\PrinterMaterialGroup;
use yii\base\Component;

class BaseMapCalculator extends  Component
{
    protected function getPrintersByMaterialGroup($materialGroup)
    {
        $printers = PrinterRepository::getCommonQuery()->joinWith('psPrinterMaterials')->andWhere(['ps_printer_material.material_id' => $materialGroup])->all();
        return $printers;
    }

    protected function getPrintersByGroups()
    {
        $materialByGroups = $this->formGroups();
        $allActive = PrinterRepository::getCommonQuery()->all();
        echo "\nTotal active printers: " . count($allActive) . "\n";

        foreach ($materialByGroups as $materialGroupCode => $materialGroup) {
            $printers = $this->getPrintersByMaterialGroup($materialGroup);
            echo 'Printers in group "' . $materialGroupCode . '": ' . count($printers) . "\n";
            if (count($printers)) {
                $printersByGroups[$materialGroupCode] = $printers;
            }
        }
        return $printersByGroups;
    }

    public function formGroups()
    {
        $materialByGroups = [];
        /** @var PrinterMaterialGroup[] $materialGroups */
        $materialGroups = PrinterMaterialGroup::find()->where('is_active = 1')->all();
        foreach ($materialGroups as $materialGroup) {
            foreach ($materialGroup->printerMaterials as $printerMaterial) {
                $materialByGroups[$materialGroup->code][$printerMaterial->filament_title] = $printerMaterial->id;
            }
        }
        return $materialByGroups;
    }
}