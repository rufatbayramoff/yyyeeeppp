<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.04.17
 * Time: 17:12
 */

namespace common\modules\psPrinterListMaps\components;

use common\models\Printer;
use common\models\PsPrinter;
use common\models\StoreOrder;
use Yii;
use yii\base\Component;
use yii\base\Module;

class ColorsMapCalculator extends BaseMapCalculator
{
    protected $module;
    protected $orderColors;

    const MAX_COLORS_COUNT = 30;

    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    public function filterColorsByPrintersCount($printersGroups)
    {
        $filtered = [];
        foreach ($printersGroups as $groupCode => $colors) {
            if (count($colors) < self::MAX_COLORS_COUNT) {
                $filtered[$groupCode] = $colors;
                unset($printersGroups[$groupCode]);
            }
        }
        $maxPrintersInGroup = [];
        foreach ($printersGroups as $groupCode => $colors) {
            uasort(
                $colors,
                function ($colorA, $colorB) {
                    if (count($colorA['printers']) > count($colorB['printers'])) {
                        return -1;
                    }
                    if (count($colorA['printers']) == count($colorB['printers'])) {
                        return 0;
                    }
                    return 1;

                }
            );
            $i = 0;
            $filtered[$groupCode] = [];
            foreach ($colors as $colorCode => $colorInfo) {
                $i++;
                if ($i > self::MAX_COLORS_COUNT) {
                    break;
                }
                $filtered[$groupCode][$colorCode] = $colorInfo;
            }

        }
        return $filtered;
    }

    public function formOrdersColors()
    {
        $orderColors = [];
        $multicolorOrders = [];
        /** @var StoreOrder[] $completedOrders */
        $completedOrders = StoreOrder::findAll(['order_state' => 'completed']);
        foreach ($completedOrders as $order) {
            foreach ($order->storeOrderItems as $storeOrderItem) {
                $model3d = $storeOrderItem->model3dReplica;
                $printer = $order->currentAttemp->machine->asPrinter();

                if ($model3d->isOneTextureForKit()) {
                    $texture = $model3d->getKitModel3dTexture();


                    $materialGroup = $texture->calculatePrinterMaterialGroup()->code;
                    $orderColors[$materialGroup][$texture->printerColor->render_color]['colorId'] = $texture->printer_color_id;
                    $orderColors[$materialGroup][$texture->printerColor->render_color]['psPrinters'][$printer->id] = [
                        'printerId'      => $printer->printer->id,
                        'psPrinterTitle' => $printer->title
                    ];
                    $orderColors[$materialGroup][$texture->printerColor->render_color]['orders'][$order->id] = [
                        $order->getTitle()
                    ];
                } else {
                    $model3dParts = $model3d->getActiveModel3dParts();
                    $multicolorOrders[$order->id] = [
                        'orderId' => $order->id,
                    ];
                    foreach ($model3dParts as $model3dPart) {
                        $texture = $model3dPart->getCalculatedTexture();
                        $color = $texture->printerColor;
                        $multicolorOrders[$order->id]['partColors'] = [
                            $color->render_color => [
                                'colorId' => $color->id,
                            ]
                        ];

                        $materialGroup = $texture->calculatePrinterMaterialGroup()->code;
                        $orderColors[$materialGroup][$color->render_color]['colorId'] = $color->id;
                        $orderColors[$materialGroup][$color->render_color]['psPrinters'][$printer->id] = [
                            'printerId'      => $printer->printer->id,
                            'psPrinterTitle' => $printer->title
                        ];
                        $orderColors[$materialGroup][$color->render_color]['orders'][$order->id] = [
                            $order->getTitle()
                        ];

                    }
                }
            }
        }
        $this->orderColors = $orderColors;
        return [
            'multicolorOrders' => $multicolorOrders,
            'orderColors'      => $orderColors
        ];
    }

    public function checkIsOrderColorsInFiltered($filteredColors)
    {
        foreach ($this->orderColors as $materialGroup => $colorGroup) {
            foreach ($colorGroup as $colorCode => $colorInfo) {
                if (!isset($filteredColors[$materialGroup][$colorCode])) {
                    echo "\nOrder is not in filtered colors: ";
                    var_dump($colorInfo);
                }
            }
        }
    }

    public function reduceColorsInfo($filtered)
    {
        $reduced = [];
        foreach ($filtered as $materialGroup=>$colorGroup) {
            foreach ($colorGroup as $colorCode=>$colorInfo) {
                $reduced[$materialGroup][]=$colorCode;
            }
        }
        return $reduced;
    }


    public function getMinimizedColorsList()
    {
        $this->formOrdersColors();
        $printersGroups = $this->getPrintersByGroups();
        $groupsColors = [];

        foreach ($printersGroups as $groupCode => $printersInGroup) {
            if (!array_key_exists($groupCode, $groupsColors)) {
                $groupsColors[$groupCode] = [];
            }
            /** @var PsPrinter $printer */
            foreach ($printersInGroup as $printer) {
                foreach ($printer->psPrinterMaterials as $psMaterial) {
                    foreach ($psMaterial->psPrinterColors as $psColor) {
                        $colorCode = $psColor->color->render_color;
                        if (!array_key_exists($colorCode, $groupsColors[$groupCode])) {
                            $groupsColors[$groupCode][$colorCode] = [];
                        }

                        $groupsColors[$groupCode][$colorCode]['colorId'] = $psColor->color->id;
                        $groupsColors[$groupCode][$colorCode]['printers'][$printer->id] = $printer->title;
                    }
                }
            }
        }
        echo "\nMaterials groups count: " . count($printersGroups) . "\n";
        $filtered = $this->filterColorsByPrintersCount($groupsColors);
        $this->checkIsOrderColorsInFiltered($filtered);
        $reduceColors = $this->reduceColorsInfo($filtered);
        file_put_contents(
            Yii::getAlias('@common/config/filepageMaps/last-colors-map.php'),
            '<?php return ' . var_export($reduceColors, true) . ';?>'
        );
        var_dump($reduceColors);

    }
}