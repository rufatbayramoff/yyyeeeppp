<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.04.17
 * Time: 17:12
 */

namespace common\modules\psPrinterListMaps\components;

use common\models\Printer;
use common\models\PsPrinter;
use common\models\StoreOrder;
use Yii;
use yii\base\Component;
use yii\base\Module;

class LocationsMapCalculator extends BaseMapCalculator
{
    protected $module;


    public function setModule(Module $module)
    {
        $this->module = $module;
    }



    public function getMinimizedList()
    {
        $printersGroups = $this->getPrintersByGroups();
        $groupsLocations = [];

        foreach ($printersGroups as $groupCode => $printersInGroup) {
            /** @var PsPrinter $printer */
            $groupsLocations[$groupCode]['international'] = 'international';
            foreach ($printersInGroup as $printer) {
                $printerLocation = $printer->companyService->location->country->iso_code;
                $groupsLocations[$groupCode][$printerLocation] = $printerLocation;
            }
        }
        $reducedLocations = [];
        foreach ($groupsLocations as $groupCode => $printersInGroup) {
            $reducedLocations[$groupCode] = array_values($printersInGroup);
        }

        file_put_contents(
            Yii::getAlias('@common/config/filepageMaps/last-locations-map.php'),
            '<?php return ' . var_export($reducedLocations, true) . ';?>'
        );
        return $reducedLocations;
    }
}