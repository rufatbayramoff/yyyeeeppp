<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.04.17
 * Time: 16:13
 */

namespace common\modules\psPrinterListMaps\components;

use common\components\ps\locator\Size;
use common\models\FilepageSize;
use Yii;
use yii\base\BaseObject;

class FilepageSizeRepository extends BaseObject
{
    protected static $sizes = [];

    /** @var null|array */
    protected static $nearestSizes = [];

    /** @var null|array */
    protected static $nearestPrintersSizes = [];

    /** @var null|array */
    protected static $lastMapSizes;

    /**
     * @param Size $size
     * @return string
     */
    protected function formIndex($size)
    {
        return $size->width . 'x' . $size->height . 'x' . $size->length;
    }

    public function getFilepageSize(Size $size)
    {
        $sizeIndex = $this->formIndex($size);
        if (array_key_exists($sizeIndex, self::$sizes)) {
            return self::$sizes[$sizeIndex];
        }
        $filepageSize = new FilepageSize(
            [
                'width'  => $size->width,
                'height' => $size->height,
                'length' => $size->length
            ]
        );
        self::$sizes[$sizeIndex] = $filepageSize;
        return $filepageSize;
    }

    public function getNearestSize(Size $size)
    {
        $sizeArr = $size->toArray();
        $sizeArr[0] = (int)$sizeArr[0];
        $sizeArr[1] = (int)$sizeArr[1];
        $sizeArr[2] = (int)$sizeArr[2];
        sort($sizeArr);

        $sizeIndex = $sizeArr[0] . 'x' . $sizeArr[1] . 'x' . $sizeArr[2];
        if (array_key_exists($sizeIndex, self::$nearestSizes)) {
            // Apparent overlap with the condition
            return self::$nearestSizes[$sizeIndex];
        }

        // Step 5 mm
        $nearestSize[0] = ceil($sizeArr[0] / 5) * 5;
        $nearestSize[1] = ceil($sizeArr[1] / 5) * 5;
        $nearestSize[2] = ceil($sizeArr[2] / 5) * 5;

        $nearestSize = Size::create($nearestSize[0], $nearestSize[1], $nearestSize[2]);
        $filepageSize = $this->getFilepageSize($nearestSize);
        self::$nearestSizes[$sizeIndex] = $filepageSize;
        return $filepageSize;
    }
}