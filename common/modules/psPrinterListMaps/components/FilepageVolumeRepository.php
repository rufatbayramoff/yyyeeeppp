<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.04.17
 * Time: 15:18
 */

namespace common\modules\psPrinterListMaps\components;

use Yii;

class FilepageVolumeRepository
{
    public function getNearestVolume($volume): ?int
    {
        if ($volume < 1) {
            return 1;
        }
        return round($volume);
    }
}