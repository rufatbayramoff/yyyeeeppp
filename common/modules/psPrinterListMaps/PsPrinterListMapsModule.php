<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.04.17
 * Time: 14:36
 */

namespace common\modules\psPrinterListMaps;

use common\components\BaseModule;
use common\modules\psPrinterListMaps\components\ColorsMapCalculator;
use common\modules\psPrinterListMaps\components\LocationsMapCalculator;
use common\modules\psPrinterListMaps\components\PsPrinterSizesMapCalculator;
use common\modules\psPrinterListMaps\components\WeightMapCalculator;
use yii\base\Module;


/**
 * Class PsPrinterListMapsModule
 *
 * @property PsPrinterSizesMapCalculator $psPrinterSizesMapCalculator
 * @property ColorsMapCalculator $colorsMapCalculator
 * @property LocationsMapCalculator $locationsMapCalculator
 * @property WeightMapCalculator $weightMapCalculator
 *
 * WeightMapCalculator
 *
 * @package common\modules\psPrinterListMaps
 */
class PsPrinterListMapsModule extends BaseModule
{
    /**
     * @return array
     */
    protected function getComponentsList()
    {
        return [
            'psPrinterSizesMapCalculator' => PsPrinterSizesMapCalculator::class,
            'colorsMapCalculator'         => ColorsMapCalculator::class,
            'locationsMapCalculator'      => LocationsMapCalculator::class,
            'weightMapCalculator'         => WeightMapCalculator::class,
        ];
    }
}

