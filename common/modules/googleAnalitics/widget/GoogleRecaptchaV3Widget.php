<?php

namespace common\modules\googleAnalitics\widget;

use Yii;
use yii\base\Widget;

class GoogleRecaptchaV3Widget extends Widget
{
    public $publicCode = '6Ld4i8gUAAAAAG9FHRvx-cD1VKC6jOAqT1LvI1WN';

    public function getCurrentScore()
    {
        $salt = 'VKs=d9apwksAKspowdja';
        if (!array_key_exists('dp', $_COOKIE) || !$_COOKIE['dp']) {
            return 0;
        }
        $dpString   = $_COOKIE['dp'];
        $remoteAddr = $_SERVER['REMOTE_ADDR'];

        if (!strpos($dpString, '_')) {
            return 0;
        }
        [$md5, $timestamp, $randInt, $score] = explode('_', $dpString);
        $calcMd5 = md5($salt . '_' . $remoteAddr . '_' . $timestamp . '_' . $randInt . '_' . $score);
        if ($md5 !== $calcMd5) {
            return 0;
        }
        return (float)$score;
    }


    public function run()
    {
        if (Yii::$app->getModule('captcha')->isDisabled) {
            return false;
        }
        $score = $this->getCurrentScore();
        if ($score) {
            return;
        }
        return $this->render('GoogleRecaptcha', [
            'publicCode' => $this->publicCode
        ]);
    }
}

