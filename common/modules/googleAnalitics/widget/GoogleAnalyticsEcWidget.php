<?php

namespace common\modules\googleAnalitics\widget;

use common\models\PaymentInvoice;
use yii\base\Widget;

class GoogleAnalyticsEcWidget extends Widget
{
    public $currencyCode = '';
    public $productInfo = [];
    public $ecEvent = '';       // Example: 'ec:setAction'
    public $ecActionName = '';  // Example: 'purchase'
    public $ecEventParams = ''; // Example: {'id': 'T12345', ...}

    public static function tryGetAffiliateName(PaymentInvoice $invoice)
    {
        return $invoice->getLinkedAffiliateAward()->affiliateSource->user->username ?? '';
    }

    public static function purchase(PaymentInvoice $invoice)
    {
        $productInfo = [
            'id'       => '000',
            'name'     => 'Product',
            'price'    => $invoice->getAmountTotal(),
            'quantity' => 1,
        ];

        if ($invoice->isInternalOrder()) {
                return '';
        }

        if ($invoice->storeOrder) {
            if ($model3dReplica = $invoice->storeOrder->getFirstReplicaItem()) {
                $productInfo = [
                    'id'       => $model3dReplica->getSourceModel3d()->id,
                    'name'     => 'Model: ' . $model3dReplica->getTitle(),
                    'price'    => $invoice->getAmountTotal()->getAmount(),
                    'quantity' => 1,
                ];
            } elseif ($preorder = $invoice->preorder) {
                $productInfo = [
                    'id'       => $preorder->id,
                    'name'     => 'Preorder: ' . $preorder->name,
                    'price'    => $invoice->getAmountTotal()->getAmount(),
                    'quantity' => 1
                ];
            }
        }

        $params = [
            'id'      => $invoice->uuid,
            'revenue' => $invoice->getAmountTotal()->getAmount()
        ];
        if ($affiliateName = self::tryGetAffiliateName($invoice)) {
            $params['affiliation'] = $affiliateName;
        }

        return self::widget([
            'currencyCode'  => $invoice->getAmountTotal()->getCurrency(),
            'productInfo'   => $productInfo,
            'ecEvent'       => 'ec:setAction',
            'ecActionName'  => 'purchase',
            'ecEventParams' => $params
        ]);
    }

    public function run()
    {
        return $this->render(
            'GoogleAnalyticsEc',
            [
                'currencyCode'  => $this->currencyCode,
                'productInfo'   => $this->productInfo,
                'ecEvent'       => $this->ecEvent,
                'ecActionName'  => $this->ecActionName,
                'ecEventParams' => $this->ecEventParams
            ]
        );
    }
}