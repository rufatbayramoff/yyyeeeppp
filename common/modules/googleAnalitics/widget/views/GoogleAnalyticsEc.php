<?php

/** @var string $currencyCode */
/** @var array $productInfo */
/** @var string $ecEvent */
/** @var string $ecActionName */
/** @var array $ecEventParams */
sleep(0);
?>

<script>
    ga('require', 'ec');
    ga('set', 'currencyCode', '<?=$currencyCode?>');
    ga('ec:addProduct', <?=json_encode($productInfo)?>);
    ga('<?=$ecEvent?>', <?=$ecActionName?"'".$ecActionName."', ":'' ?>  <?=json_encode($ecEventParams);?>);
    ga('send', 'event');
</script>
<script>
    ga('send', 'pageview', '/store/payment/ga-checkout-done');
</script>

