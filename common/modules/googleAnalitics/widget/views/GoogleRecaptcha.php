<?php
/** @var string $publicCode */
?>
<style>
    .grecaptcha-badge {
        visibility: hidden;
    }
</style>
<script src='https://www.google.com/recaptcha/api.js?render=<?= $publicCode ?>'></script>
<script>
    let recaptureTimer = null;
    function recaptcha3() {
        console.log('google recapture');
        grecaptcha.ready(function () {
            grecaptcha.execute('<?=$publicCode?>', {action: 'validation'}).then(function (token) {
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/dp');
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.onload = function () {
                };
                xhr.send(encodeURI('token-v3=' + token));
            });
        });
    }
    document.addEventListener('mousedown', function() {
        if (!recaptureTimer) {
            recaptureTimer = setTimeout(recaptcha3, 1000);
        }
    });
    document.addEventListener('mousemove', function() {
        if (!recaptureTimer) {
            recaptureTimer = setTimeout(recaptcha3, 1000);
        }
    });
    document.addEventListener('scroll', function() {
        if (!recaptureTimer) {
            recaptureTimer = setTimeout(recaptcha3, 1000);
        }
    });
</script>
<div class="recaptcha-v3-policy footer__copyrights">
    This site is protected by reCAPTCHA and the Google
    <a href="https://policies.google.com/privacy">Privacy Policy</a> and
    <a href="https://policies.google.com/terms">Terms of Service</a> apply.
</div>