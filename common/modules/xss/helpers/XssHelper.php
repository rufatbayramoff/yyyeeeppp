<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.12.18
 * Time: 13:22
 */

namespace common\modules\xss\helpers;

use common\components\BaseAR;

class XssHelper
{
    public static function cleanXssProtect($value)
    {

        return str_replace(BaseAR::getXssTestText(), '', $value);
    }

    public static function cleanXssOutput($value)
    {
        $simpleXssText = BaseAR::getXssTestText();
        $jsonXss = substr(json_encode(BaseAR::getXssTestText()), 1, -1);
        $xssEncoded = H($simpleXssText);
        $xssEncodedUrl = urlencode($simpleXssText);
        $returnValue = str_replace([$simpleXssText, $jsonXss, $xssEncoded, $xssEncodedUrl], '', $value);
        return $returnValue;
    }

    public static function twiceEncodedPosition($value)
    {
        $xssEncodedTwice = H(H(BaseAR::getXssTestText()));
        $twiceEncodedPosition = strpos($value, $xssEncodedTwice);
        return $twiceEncodedPosition;
    }


    public static function cleanArrayKeys($array): array
    {
        $filtered = [];
        foreach ($array as $key => $value) {
            if (\is_string($key)) {
                $key = str_replace(BaseAR::getXssTestText(), '', $key);
            }
            $filtered[$key] = $value;
        }
        return $filtered;
    }

    public static function cleanArray($array): array
    {
        $json = json_encode($array);
        $jsonXss = substr(json_encode(BaseAR::getXssTestText()), 1, -1);
        $json = str_replace($jsonXss, '', $json);
        $filteredArray = json_decode($json, true);
        return $filteredArray;
    }
}