<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.12.18
 * Time: 13:05
 */

namespace common\modules\xss\validators;

use common\components\BaseAR;
use common\components\XssArTestTrait;
use common\modules\xss\helpers\XssHelper;

class EmailValidator extends \yii\validators\EmailValidator
{
    protected function validateValue($value)
    {
        return parent::validateValue(XssHelper::cleanXssProtect($value));
    }
}