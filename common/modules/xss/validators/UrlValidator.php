<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.12.18
 * Time: 13:05
 */

namespace common\modules\xss\validators;

use common\components\BaseAR;
use common\components\XssArTestTrait;
use common\modules\xss\helpers\XssHelper;

class UrlValidator extends \yii\validators\UrlValidator
{

    /**
     * {@inheritdoc}
     */
    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        $value =  XssHelper::cleanXssProtect($value);
        if (!$value) {
            return ;
        }

        $result = $this->validateValue($value);
        if (!empty($result)) {
            $this->addError($model, $attribute, $result[0], $result[1]);
        } elseif ($this->defaultScheme !== null && strpos($value, '://') === false) {
            $model->$attribute = $this->defaultScheme . '://' . $value;
        }
    }
}