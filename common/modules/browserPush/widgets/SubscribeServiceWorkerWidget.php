<?php

namespace common\modules\browserPush\widgets;

use Yii;
use yii\base\Widget;

class SubscribeServiceWorkerWidget extends Widget
{
    public $message;
    public $code;

    public function run()
    {
        return $this->render('serviceWorker', [
                'keys'            => Yii::$app->getModule('browser-push')->keys,
                'currentEndpoint' => Yii::$app->getModule('browser-push')->getCurrentEndpoit(),
                'message'         => $this->message,
                'code'            => $this->code
            ]
        );
    }
}