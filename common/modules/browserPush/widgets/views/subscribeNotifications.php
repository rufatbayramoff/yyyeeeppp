<?php
/** @var $notificationsSubscribePopupUrl string */
/** @var $currentEndpoint string */

use common\components\JsObjectFactory;
use common\modules\browserPush\assets\SubscribeNotificationsAsset;

SubscribeNotificationsAsset::register($this);
JsObjectFactory::createJsObject(
    'subscribeNotificationsClass',
    'subscribeNotifications',
    [
        'notificationsSubscribePopupUrl' => $notificationsSubscribePopupUrl,
        'currentEndpoint' => $currentEndpoint
    ],
    $this
);

?>
<div id="get-notifications-button" class="get-notifications-button hidden">
    <i class="tsi tsi-reminder m-r10"></i>
    <?= _t('site.notifications', 'Get Notifications') ?>
</div>
