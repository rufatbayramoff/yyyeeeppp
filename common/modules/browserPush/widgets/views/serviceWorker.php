<?php
/** @var $keys array */
/** @var $currentEndpoint string */

/** @var $message string */

use common\components\JsObjectFactory;
use common\modules\browserPush\assets\ServiceWorkerAsset;
use common\modules\browserPush\BrowserPushModule;

ServiceWorkerAsset::register($this);
JsObjectFactory::createJsObject(
    'serviceWorkerRegisterClass',
    'serviceWorkerRegister',
    [
        'publicKey'       => $keys[BrowserPushModule::PUBLIC_KEY],
        'currentEndpoint' => $currentEndpoint
    ],
    $this
)
?>

<div style="text-align:center;">
    <div id="common-text" class="text-block" style="padding-top: 150px;">
        <img src="/static/images/logo128.png">
        <h4>
            <?= _t('site.notifications', "Click on 'Allow' to confirm notifications"); ?>
        </h4>
        <?= H($message) ?>
    </div>
    <div id="already-subscribed" class="text-block hidden" style="padding-top: 150px;">
        <img src="/static/images/logo128.png">
        <h4>
            <?= _t('site.notifications', 'You are already subscribed'); ?>
        </h4>
    </div>
    <div id="unblock-instruction" class="text-block hidden" style="padding-top: 10px;">
        <img src="/static/images/notify.gif">
        <div>
            <?= _t('site.notifications', 'You seem to have previously blocked/denied push notifications from Treatstock.com. See the image above to know how to unblock them.'); ?>
        </div>
    </div>
</div>