<?php

namespace common\modules\browserPush\widgets;

use common\components\UrlHelper;
use common\modules\browserPush\BrowserPushModule;
use common\modules\browserPush\helpers\PushNotificationUrlHelper;
use Yii;
use yii\base\Widget;

class SubscribeNotificationsWidget extends Widget
{
    /**
     * Message for user to allow notifications
     *
     * @var string $message
     */
    public $message = '';

    /**
     * Notifications code
     *
     * @var string $code
     */
    public $code = '';

    public function run()
    {
        return $this->render('subscribeNotifications', [
                'notificationsSubscribePopupUrl' => PushNotificationUrlHelper::getPopupUrl($this->message, $this->code),
                'currentEndpoint'                => Yii::$app->getModule('browser-push')->getCurrentEndpoit(),
            ]
        );
    }
}