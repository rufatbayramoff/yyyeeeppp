<?php

namespace common\modules\browserPush\services;

use common\components\DateHelper;
use common\components\Emailer;
use common\models\NotifyPopup;
use common\models\StoreOrderAttemp;
use yii\base\BaseObject;
use yii\helpers\Url;

class NotifyPopupService extends BaseObject
{
    /**
     * @var BrowserPushService
     */
    public $browserPushService;


    public function injectDependencies(BrowserPushService $browserPushService)
    {
        $this->browserPushService = $browserPushService;
    }

    public function notifyNewOrder(StoreOrderAttemp $storeOrderAttempt)
    {
        $notifyPopup = new NotifyPopup();
        $notifyPopup->uid = NotifyPopup::generateUid();
        $notifyPopup->created_at = DateHelper::now();
        $notifyPopup->user_id = $storeOrderAttempt->ps->user_id;
        $notifyPopup->title = _t('site.notifications', 'You have a new order #{orderId} on Treatstock', ['orderId' => $storeOrderAttempt->order->id]);
        $notifyPopup->text = _t('site.notifications', 'You have received a new order #{orderId} on Treatstock and have 24 hours to accept.', ['orderId' => $storeOrderAttempt->order->id]);
        $notifyPopup->url = Url::toRoute(['/workbench/service-order/view', 'attemptId' => $storeOrderAttempt->id], true);
        $notifyPopup->code = NotifyPopup::CODE_NEW_ORDER;
        $notifyPopup->expire_at = DateHelper::addNow('P1D');
        $notifyPopup->params = [
            'storeOrderAttempt' => $storeOrderAttempt->id
        ];
        $notifyPopup->safeSave();

        $this->browserPushService->tryPush($notifyPopup);
    }

    public function cleanExpireNotifications()
    {
        return NotifyPopup::deleteAll([
            '<','expire_at', DateHelper::now()
        ]);
    }
}