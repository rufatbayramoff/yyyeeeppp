<?php

namespace common\modules\browserPush\assets;

use backend\assets\CommonAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

class ServiceWorkerAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/browserPush/assets';

    public $js = [
        'js/serviceWorkerRegister.js',
    ];

    public $depends = [
        YiiAsset::class,
        CommonAsset::class
    ];
}
