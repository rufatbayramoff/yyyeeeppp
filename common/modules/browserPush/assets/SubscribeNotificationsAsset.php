<?php

namespace common\modules\browserPush\assets;

use backend\assets\CommonAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

class SubscribeNotificationsAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/browserPush/assets';

    public $js = [
        'js/subscribeNotificationsWidget.js',
    ];

    public $css = [
        'css/subscribeNotificationsWidget.css'
    ];

    public $depends = [
        YiiAsset::class,
        CommonAsset::class
    ];
}
