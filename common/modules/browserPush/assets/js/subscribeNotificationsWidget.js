var subscribeNotificationsClass = {
    config: {
        notificationsSubscribePopupUrl: '',
        currentEndpoint: ''
    },

    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        if ((typeof Notification !== 'undefined') && Notification && (Notification.permission !== 'granted' || !self.config.currentEndpoint)) {
            self.initButton();
        }
    },

    popupCenter: function (url, name, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var systemZoom = width / window.screen.availWidth;
        var left = (width - w) / 2 / systemZoom + dualScreenLeft
        var top = (height - h) / 2 / systemZoom + dualScreenTop
        var newWindow = window.open(url, name, 'scrollbars=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) newWindow.focus();

        let reloadCheckerTimer = setInterval(function () { // Reload parent if child closed
            if (newWindow.closed) {
                clearInterval(reloadCheckerTimer);
                window.location.reload();
            }
        }, 500);
    },

    initButton: function () {
        var self = this;
        var button = $('#get-notifications-button');
        button.removeClass('hidden');
        button.on('click', function () {
            self.popupCenter(self.config.notificationsSubscribePopupUrl, 'subscribeNotifications', 500, 450);
        });
    },
};