var serviceWorkerRegisterClass = {
    config: {
        publicKey: '',
        currentEndpoint: '',
    },

    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        self.register();
    },

    closePopup: function () {
        window.close();
    },

    registerSubscription: function (subscription, autoclose=true) {
        var self = this;
        fetch('/browser-push/browser-push/subscribe', {
            method: 'post',
            headers: {
                'Content-type': 'application/json',
                'X-CSRF-Token': yii.getCsrfToken()
            },
            body: JSON.stringify(subscription)
        }).then(function () {
            if (autoclose) {
                self.closePopup();
            }
        });
    },

    displayAlreadySubscribed: function () {
        $('.text-block').addClass('hidden');
        $('#already-subscribed').removeClass('hidden');
    },

    displayUnblockInstruction: function () {
        $('.text-block').addClass('hidden');
        $('#unblock-instruction').removeClass('hidden');
    },

    register: function () {
        var self = this;
        // Check working with service Worker is possible
        if ('serviceWorker' in navigator) {
            Notification.requestPermission().then(function (notificationAccess) {
                if (notificationAccess === 'granted') {
                    navigator.serviceWorker.register('/js/service-worker.js')
                        .then(function (registration) {
                            registration.pushManager.getSubscription().then(
                                function (subscription) {
                                    // Subscription exists, return it
                                    if (subscription) {
                                        if (self.config.currentEndpoint && (self.config.currentEndpoint === subscription.endpoint)) {
                                            self.displayAlreadySubscribed();
                                            return subscription;
                                        }
                                        self.displayAlreadySubscribed();
                                        self.registerSubscription(subscription, false);
                                        return subscription;
                                    }
                                    var subscribeOptions = {
                                        userVisibleOnly: true,
                                        applicationServerKey: self.urlB64ToUint8Array(
                                            self.config.publicKey
                                        )
                                    };
                                    return registration.pushManager.subscribe(subscribeOptions).then(function (subscription) {
                                        self.registerSubscription(subscription);
                                    });
                                });
                        })
                        .catch((err) => console.log('Service worker not registered:' + err));
                } else {
                    self.displayUnblockInstruction();
                }
            });
        } else {
            if (document.location.protocol !== 'https:') {
                console.log('Enable https protocol, to get browser push notifications.');
            }
            self.closePopup();
        }
    },
    urlB64ToUint8Array: function (base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }

};

