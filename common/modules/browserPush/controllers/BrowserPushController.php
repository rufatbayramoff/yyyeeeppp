<?php


namespace common\modules\browserPush\controllers;

use common\components\BaseController;
use common\components\DateHelper;
use common\models\NotifyPopup;
use common\modules\browserPush\exceptions\NotExistsBrowserPushSubscription;
use common\modules\browserPush\services\BrowserPushService;
use frontend\models\user\UserFacade;
use Yii;

class BrowserPushController extends BaseController
{
    /** @var BrowserPushService */
    public $browserPushService;

    public function injectDependencies(BrowserPushService $browserPushService)
    {
        $this->browserPushService = $browserPushService;
    }

    protected $detectLanguage = false;

    public function actionSubscribe()
    {
        $endpoint = \Yii::$app->request->post('endpoint');
        $keys = \Yii::$app->request->post('keys');

        $browserPush = $this->browserPushService->subscribe($endpoint, $keys['auth'], $keys['p256dh']);
        Yii::$app->getModule('browser-push')->setCurrentEndpoit($endpoint);

        return $this->jsonSuccess([
                'push_uuid' => $browserPush->uid
            ]
        );
    }

    public function actionSubscribePopup()
    {
        $this->layout = '@frontend/views/layouts/plain.php';
        $code = Yii::$app->request->get('code');
        $message = Yii::$app->request->get('message');
        return $this->render('subscribePopup', ['message' => $message, 'code' => $code]);
    }

    public function actionGetNotifications($endPoint)
    {
        $currentUser = UserFacade::getCurrentUser();
        $notifyPopups = $this->browserPushService->getUnreceivedNotifications($currentUser, $endPoint);

        return $this->jsonSuccess([
            'list' => $notifyPopups
        ]);
    }

    public function actionTestPush()
    {
        $currentUser = UserFacade::getCurrentUser();
        if (!$currentUser) {
            return $this->jsonError('Unauthorized user');
        }

        $notifyPopup = new NotifyPopup();
        $notifyPopup->uid = NotifyPopup::generateUid();
        $notifyPopup->created_at = DateHelper::now();
        $notifyPopup->expire_at = DateHelper::addNow('P1D');
        $notifyPopup->user_id = $currentUser->id;
        $notifyPopup->title = 'Test push notification! You have new Order #' . mt_rand(0, 9999);
        $notifyPopup->text = 'Model olive. By Date: ' . DateHelper::now();
        $notifyPopup->url = param('server') . '/workbench/service-order/';
        $notifyPopup->code = '';
        $notifyPopup->params = [];
        $notifyPopup->safeSave();

        try {
            $report = $this->browserPushService->push($currentUser, $notifyPopup);
        } catch (NotExistsBrowserPushSubscription $notExistsSuscription) {
            return $this->jsonError('Push subscription not exists');
        }

        return $this->jsonSuccess([
            'report' => $report
        ]);
    }

}