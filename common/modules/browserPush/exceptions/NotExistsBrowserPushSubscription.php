<?php

namespace common\modules\browserPush\exceptions;

use Throwable;

class NotExistsBrowserPushSubscription extends \yii\base\InvalidArgumentException
{
    public function __construct($message = 'User have not browser push subscription', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
