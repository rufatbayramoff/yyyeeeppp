<?php

namespace common\modules\browserPush\models;

class BrowserPushNotificationInfo
{
    /**
     * Notification body
     *
     * @var BrowserPushNotification
     */
    public $notification;


    /**
     *  Info send notification to
     */

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $pushServerUrl;

    /**
     * @var string[]
     */
    public $registrationIds;
}
