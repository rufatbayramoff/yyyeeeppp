<?php
namespace common\modules\browserPush\models;

class BrowserPushNotification
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $body;

    /**
     * @var string
     */
    public $icon;

    /**
     * @var string
     */
    public $click_action;
}
