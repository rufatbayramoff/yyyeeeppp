<?php

use common\modules\browserPush\widgets\SubscribeServiceWorkerWidget;

/** @var $message string */
$this->title = _t('site.notifications', 'Subscribe notifications from Treatstock.com');
echo SubscribeServiceWorkerWidget::widget(['message' => $message, 'code'=>$code]);
?>
