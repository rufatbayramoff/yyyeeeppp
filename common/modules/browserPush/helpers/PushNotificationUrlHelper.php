<?php

namespace common\modules\browserPush\helpers;

use Yii;

class PushNotificationUrlHelper
{
    public static function getPopupUrl(string $message, string $code)
    {
        $mainDomain = Yii::$app->getModule('intlDomains')->mainDomain;
        return 'https://' . $mainDomain
            . '/browser-push/browser-push/subscribe-popup?message=' . urlencode($message)
            . '&code=' . urlencode($code);

    }
}