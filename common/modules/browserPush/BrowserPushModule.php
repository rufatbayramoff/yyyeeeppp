<?php

namespace common\modules\browserPush;

use common\components\BaseModule;
use Yii;

/**
 * Class BrowserPushModule
 * @package common\modules\browserPush
 */
class BrowserPushModule extends BaseModule
{
    public const PUBLIC_KEY = 'public';
    public const PRIVATE_KEY = 'private';
    public const SESSION_ENDPOINT_PARAMETER_NAME = 'browserCurrentEndpoint';

    public $keys = [
        self::PUBLIC_KEY  => 'BAaX3Zq6SdsOjTwtndxvIzWSEWMSlJQvMJCbF_VDu9qd01KjVqQQFiJ2VyUwP6cGZiuzonuPGFMrBa_0sAnyUSg',
        self::PRIVATE_KEY => 'HtJ3snS6tYBAio58jVJ9gyAN4l9dOf5RTJApAo641r4',
    ];

    public function getComponentsList()
    {
        return [
        ];
    }

    public function getCurrentEndpoit()
    {
        $endPoint = Yii::$app->session->get(self::SESSION_ENDPOINT_PARAMETER_NAME, null);
        return $endPoint;
    }

    public function setCurrentEndpoit($endpoint)
    {
        Yii::$app->session->set(self::SESSION_ENDPOINT_PARAMETER_NAME, $endpoint);
    }

    public function init()
    {
        parent::init();
        $componentsList = $this->getComponentsList();
        $this->setComponents($componentsList);
        $this->initComponentsModule($componentsList);
    }
}
