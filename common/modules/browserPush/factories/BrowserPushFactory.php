<?php

namespace common\modules\browserPush\factories;

use common\components\DateHelper;
use common\components\exceptions\InvalidModelException;
use common\components\UuidHelper;
use common\models\BrowserPushSubscription;
use common\models\BrowserPushSubscriptionUser;
use common\models\User;
use common\models\user\UserIdentityProvider;
use Yii;
use yii\base\InvalidArgumentException;
use yii\db\Query;

class BrowserPushFactory
{
    /**
     * @return string
     * @throws \Exception
     */
    public static function generateUuid($tableName)
    {
        do {
            $uid = UuidHelper::generateRandCode(6);
            $checkUid = (new Query())->select('*')->from($tableName)->where(['uid' => $uid])->one();
        } while ($checkUid);
        return $uid;
    }

    public function createSubscription(string $endPoint, string $key_auth, string $key_p): BrowserPushSubscription
    {
        $browserPushSubscription = \Yii::createObject(BrowserPushSubscription::class);
        $browserPushSubscription->uid = $this->generateUuid(BrowserPushSubscription::tableName());
        $browserPushSubscription->created_at = DateHelper::now();
        $browserPushSubscription->user_agent = Yii::$app->request->userAgent ?? '';
        $browserPushSubscription->endpoint = $endPoint;
        $browserPushSubscription->expiration_time = null;
        $browserPushSubscription->key_auth = $key_auth;
        $browserPushSubscription->key_p = $key_p;
        return $browserPushSubscription;
    }

    public function createSubscriptionUser(BrowserPushSubscription $browserPushSubscription, User $user)
    {
        $browserPushSubscriptionUser = new BrowserPushSubscriptionUser();
        $browserPushSubscriptionUser->uid = $this->generateUuid(BrowserPushSubscriptionUser::tableName());
        $browserPushSubscriptionUser->created_at = DateHelper::now();
        $browserPushSubscriptionUser->user_id = $user->id;
        $browserPushSubscriptionUser->browser_push_subscription_uid = $browserPushSubscription->uid;
        $browserPushSubscriptionUser->ip = $_SERVER['REMOTE_ADDR'];
        return $browserPushSubscriptionUser;
    }
}
