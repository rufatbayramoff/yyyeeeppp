<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.05.18
 * Time: 13:17
 */

namespace common\modules\vue\assets;

use yii\web\AssetBundle;

class VueAssets extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/modules/vue/assets/prod/js';

    /**
     * @var array
     */
    public $css = [
    ];

    /**
     * @var array
     */
    public $js = [
        'vue.js',
    ];
}
