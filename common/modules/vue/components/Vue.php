<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.05.18
 * Time: 12:03
 */

namespace common\modules\vue\components;

use common\modules\vue\assets\VueAssets;
use common\modules\vue\assets\VueDebugAssets;
use yii\base\Component;

class Vue  extends Component {

    public $isRegistered;

    public function register(\yii\web\View $view)
    {
        if (!$this->isRegistered) {
            $this->isRegistered = true;
        }
        if (YII_DEBUG) {
            VueDebugAssets::register($view);
        } else {
            VueAssets::register($view);
        }
    }
}