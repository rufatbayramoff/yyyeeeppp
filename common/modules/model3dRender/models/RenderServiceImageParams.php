<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.06.16
 * Time: 14:37
 */

namespace common\modules\model3dRender\models;


/**
 * Class RenderServiceImageParams
 * Full internal render params
 *
 * @package common\modules\model3dRender\models
 */
class RenderServiceImageParams
{
    public $imgUrl = '';
    public $modelColor = 'fefefe';
    public $modelColor3d = '0';
    public $modelTexture = 'texture_lines4.jpg';
    public $modelTextureTT = '2';
    public $modelRotateSpeedX = '0.4';
    public $modelRotateSpeedY = '0.4';
    public $modelGranular = '5';
    public $pb = 0; // 0 - CAD, 1 - CAM

    public function setModelColor($hexColor)
    {
        $this->modelColor = $hexColor;
    }

    public function setPb($pb)
    {
        $this->pb = $pb;
    }
}