<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.06.16
 * Time: 14:48
 */

namespace common\modules\model3dRender\models;

use common\interfaces\FileBaseInterface;
use common\models\File;
use common\modules\model3dRender\Model3dRenderModule;
use frontend\models\model3d\Model3dFacade;
use yii\base\Model;

/**
 * Class RenderImageUrl
 *
 * @package common\modules\model3dRender\models
 */
class RenderImageUrl extends Model
{
    /** @var  Model3dRenderModule */
    public $module;

    /**
     * @var RenderServiceImageParams
     */
    public $renderImageParams;

    public function isCacheWarmup($renderParams)
    {
        return $renderParams->ax != $this->module->defaultAngleX ||
            $renderParams->ay != $this->module->defaultAngleY ||
            $renderParams->az != $this->module->defaultAngleZ;
    }

    public function getImageUrl(FileBaseInterface $file, RenderFileParams $renderParams)
    {
        $fileUid = $file->md5sum . '_' . RenderImageUrl::getRenderExtension($file->extension);;
        $url = $this->renderImageParams->imgUrl . $fileUid .
            '_color_' . $renderParams->color .
            '_ambient_'. $renderParams->ambient.
            '_ax_'.  $renderParams->ax.
            '_ay_'.$renderParams->ay.
            '_az_'.$renderParams->az.
            '_pb_'.$renderParams->pb.
            '.'.$this->module->defaultRenderImageExtension;
        if ($this->isCacheWarmup($renderParams)) {
            $url .= '?cachewarmup=1';
        }
        return $url;
    }

    /**
     * @param $ext
     * @return string
     */
    public static function getRenderExtension($ext) : string
    {
        $ext = strtolower($ext);
        return Model3dFacade::isIgesOrStep($ext) ? "stl" : $ext;
    }
}