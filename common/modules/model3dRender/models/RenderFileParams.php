<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.03.17
 * Time: 16:39
 */

namespace common\modules\model3dRender\models;

/*
 *  Render params for one file
 *
 */
use Yii;

class RenderFileParams
{
    public $color;
    public $ambient;
    public $ax;
    public $ay;
    public $az;
    public $pb;

    public function __construct($color=null, $ambient=null, $ax=null, $ay=null, $az=null, $pb=null)
    {
        $this->color = $color;
        $this->ambient = $ambient;
        $this->ax = $ax;
        $this->ay = $ay;
        $this->az = $az;
        $this->pb = $pb;

        $module = Yii::$app->getModule('model3dRender');

        if ($this->color===null) {
            $this->color = $module->defaultRenderImageColor;
        }

        if ($this->ax === null) {
            $this->ax = $module->defaultAngleX;
        };
        if ($this->ay === null) {
            $this->ay = $module->defaultAngleY;
        };
        if ($this->az === null) {
            $this->az = $module->defaultAngleZ;
        };
        if ($this->ambient === null) {
            $this->ambient = 0;
        };
        if ($this->pb === null) {
            $this->pb = 0;
        };
    }
}
