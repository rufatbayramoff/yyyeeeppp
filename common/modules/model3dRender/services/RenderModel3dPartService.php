<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.03.17
 * Time: 14:01
 */

namespace common\modules\model3dRender\services;

use common\interfaces\FileBaseInterface;
use common\models\File;
use common\modules\model3dRender\Model3dRenderModule;
use common\modules\model3dRender\models\RenderFileParams;
use Yii;

class RenderModel3dPartService
{

    /**
     * @var Model3dRenderModule
     */
    public $module;

    /**
     * @param FileBaseInterface $file
     * @param RenderFileParams $renderParams
     * @return null
     * @throws \Exception
     */
    public function renderFile(FileBaseInterface $file, RenderFileParams $renderParams)
    {
        $renderService = $this->getRenderService($file);
        if (!$renderService) {
            return null;
        }
        return $renderService->renderModel(
            $file,
            $renderParams
        );
    }

    /**
     * Получить доступный для рендеринга модели сервис
     *
     * @param File $file
     * @return RenderService
     * @throws \yii\base\InvalidConfigException
     * @internal param $fileUid
     */
    public function getRenderService(File $file)
    {
        $address = reset($this->module->renderServerAddress);
        // Logic to create renderService

        /** @var RenderService $renderService */
        $renderService = Yii::createObject(
            [
                'class'     => RenderService::class,
                'imgParams' => $address['renderImageParams'],
                'webdavUrl' => $address['webdavUrl'],
                'module'    => $this->module
            ]
        );
        return $renderService;
    }
}