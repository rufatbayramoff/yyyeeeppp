<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.06.16
 * Time: 11:29
 */

namespace common\modules\model3dRender\services;

use common\components\FileDirHelper;
use common\interfaces\FileBaseInterface;
use common\models\File;
use common\modules\model3dRender\Model3dRenderModule;
use common\modules\model3dRender\models\RenderFileParams;
use common\modules\model3dRender\models\RenderServiceImageParams;
use common\modules\model3dRender\models\RenderImageUrl;
use yii;
use yii\base\Component;

require(Yii::getAlias('@vendor') . '/hakre/SoapTimeout/SoapClientTimeout.class.php');

/**
 * Class RenderService -сервис рендеринга на конкретном сервере
 *
 * @package common\modules\model3dRender\services
 */
class RenderService extends Component
{
    /** @var  Model3dRenderModule */
    public $module;

    /** @var  RenderServiceImageParams */
    public $imgParams;


    public $webdavUrl;

    /**
     * Загрузка файла на сервер, для дальнейшего рендеринга
     *
     * @param File $file
     * @return bool true - success, false - failed
     * @throws \yii\base\InvalidParamException
     * @throws \InvalidArgumentException
     * @internal param $fileData
     */
    public function uploadFileData(File $file)
    {
        $headers = [];

        $ch = curl_init();

        $urlExt = RenderImageUrl::getRenderExtension($file->extension);
        $url    = $this->webdavUrl . substr($file->md5sum, 0, 2) . '/' . substr($file->md5sum, 2, 2) . '/' . $file->md5sum . '.' . $urlExt;
        $this->debugOut('Upload url: ' . $url);
        $filePath = $file->getConvertedStlPath();
        $fhRes    = fopen($filePath, 'r');

        // No sertificate on r1.tsdev.work
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        // Headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // Binary transfer i.e. --data-BINARY
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        // Using a PUT method i.e. -XPUT
        curl_setopt($ch, CURLOPT_PUT, true);
        // Instead of POST fields use these settings
        curl_setopt($ch, CURLOPT_INFILE, $fhRes);
        curl_setopt($ch, CURLOPT_INFILESIZE, filesize($filePath));

        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->debugOut('Upload code: ' . $httpCode);
    }

    protected function tryCurlDownload(FileBaseInterface $file, RenderFileParams $renderParams)
    {
        /** @var RenderImageUrl $renderImageUrl */
        $renderImageUrl                    = Yii::createObject(
            [
                'class'  => RenderImageUrl::class,
                'module' => $this->module
            ]
        );
        $warmCacheUp                       = $renderImageUrl->isCacheWarmup($renderParams);
        $renderImageUrl->renderImageParams = $this->imgParams;
        $url                               = $renderImageUrl->getImageUrl($file, $renderParams);
        $this->debugOut('Download url: ' . $url);
        if (!$warmCacheUp) {
            $filePath = $this->module->renderer->getRenderImagePath($file, $renderParams);
            $this->debugOut('Save as path: ' . $filePath);
            $dirname = dirname($filePath);
            FileDirHelper::createDir($dirname);

            $fp = fopen($filePath, 'w+');
        }
        $ch = curl_init();
        // r1.tsdev.work doesn`t work with sertificate
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_REFERER, param('server'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->module->downloadImagesTimout);
        if (!$warmCacheUp) {
            curl_setopt($ch, CURLOPT_FILE, $fp);
        }
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->module->downloadImagesTimout);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->debugOut('Download code: ' . $httpCode);
        if (!$warmCacheUp) {
            fclose($fp);
            $fileSize = filesize($filePath);
            $this->debugOut('Size: ' . $fileSize);
            if ($fileSize < 500) {
                $this->debugOut('Content < 500: ' . file_get_contents($filePath));
                unlink($filePath);
                return $httpCode;
            }
        }
        return $httpCode;
    }

    /**
     *
     * @param FileBaseInterface $file
     * @param RenderFileParams $renderParams
     * @return string
     */
    public function renderModel(FileBaseInterface $file, RenderFileParams $renderParams)
    {
        $httpCode = $this->tryCurlDownload($file, $renderParams);
        if ($httpCode === 302) {
            // retry action
            $httpCode = $this->tryCurlDownload($file, $renderParams);
            if ($httpCode === 302) {
                sleep(5);
                $httpCode = $this->tryCurlDownload($file, $renderParams);
            }
        }
        return $httpCode == 200;
    }

    public function getRenderImageUrlBase()
    {
        return $this->imgParams->imgUrl;
    }

    public function debugOut($string)
    {
        echo date('Y-m-d H:i:s') . ' - ' . $string . "\n";
    }
}
