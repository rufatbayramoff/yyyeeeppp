<?php

namespace common\modules\model3dRender\garbageCollectors;

use common\components\DateHelper;
use common\models\File;
use common\models\GcRenderModel;
use common\modules\model3dRender\Model3dRenderModule;
use common\modules\model3dRender\services\RenderModel3dPartService;
use common\modules\model3dRender\services\RenderService;
use Yii;
use yii\base\BaseObject;
use yii\base\Module;

class RenderGarbageCollector extends BaseObject
{
    public const MODELS_LIST_EXPIRE_DAYS = 10;

    public $showDebugOutput = true;

    /** @var Model3dRenderModule */
    protected $module;

    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    public function run($forceReload=false)
    {
        $this->downloadFilesList($forceReload);
        $this->processModelsFiles();

    }

    protected function debugOutput($str)
    {
        if ($this->showDebugOutput) {
            echo "\n" . date('Y-m-d H:i:s') . ' ' . $str;
        }
    }

    protected function isNeedDownloadFilesList($serverParams)
    {
        if (!array_key_exists('modelsList', $serverParams)) {
            $this->debugOutput('Models list url not set for: ' . $serverParams['webdavUrl']);
            return false;
        }
        $lastRenderModel = GcRenderModel::find()->where('loaded_at is not null')->orderBy('loaded_at asc')->limit(1)->one();
        if (!$lastRenderModel) {
            return true;
        }
        if ($lastRenderModel->loaded_at > date('Y-m-d H:i:s', time() - 60 * 60 * 24 * static::MODELS_LIST_EXPIRE_DAYS)) {
            $this->debugOutput('Models list already downloaded from: ' . $serverParams['modelsList']);
            return false;
        }
        return true;
    }

    protected function saveFilesListAnswer($webdavUrl, $answer)
    {
        $lines       = explode("\n", $answer);
        $batchInsert = [];
        $rowsNames   = ['webdavUrl', 'path'];
        $pos         = 1;
        foreach ($lines as $line) {
            if (!$line || strpos($line, '.xz')) { // Skip archived copies
                continue;
            }
            if (strpos($line, 'model/')===0) {
                $line = substr($line,6, 1024);
            }
            $batchInsert[] = [
                'webdavUrl' => $webdavUrl,
                'path'      => $line
            ];
            if (($pos % 1000) == 0) {
                Yii::$app->db->createCommand()->batchInsert('gc_render_model', $rowsNames, $batchInsert)->execute();
                $batchInsert = [];
            }
            $pos++;

        }
    }

    protected function downloadFilesListFromServer($serverParams)
    {
        if (!array_key_exists('modelsList', $serverParams)) {
            return false;
        }

        $this->debugOutput('Downloading models list from: ' . $serverParams['modelsList']);
        sleep(0);

        $ch = curl_init();
        // No sertificate on r1.tsdev.work
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_URL, $serverParams['modelsList']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $answer   = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode == 200) {
            $this->saveFilesListAnswer($serverParams['webdavUrl'], $answer);
            $this->debugOutput('Saved.');
        } else {
            $this->debugOutput('Download code: ' . $httpCode);
        }
    }

    protected function downloadFilesList($forceReload)
    {
        // Clean old models
        $needDownload = $forceReload;
        foreach ($this->module->renderServerAddress as $serverAddress => $serverParams) {
            if ($this->isNeedDownloadFilesList($serverParams)) {
                $needDownload = true;
            }
        }
        if (!$needDownload) {
            return;
        }
        //Reload all list
        GcRenderModel::deleteAll();
        foreach ($this->module->renderServerAddress as $serverAddress => $serverParams) {
            $this->downloadFilesListFromServer($serverParams);
        }
        GcRenderModel::updateAll(['loaded_at' => DateHelper::now()]);
    }

    protected function processModelsFiles()
    {
        do {
            $gcRenderModels = GcRenderModel::find()->where('loaded_at is not null')->andWhere(['processed_at' => null])->limit(1000)->all();
            foreach ($gcRenderModels as $gcRenderModel) {
                $this->processModelFile($gcRenderModel);
            }
        } while ($gcRenderModel);
    }

    protected function processModelFile(GcRenderModel $gcRenderModel)
    {
        $path         = $gcRenderModel->path;
        $lastSlashPos = strrpos($path, '/') + 1;
        $md5          = substr($path, $lastSlashPos, strpos($path, '.') - $lastSlashPos);
        if (strlen($md5) !== 32) {
            $this->debugOutput('Invalid file md5');
            return;
        }

        $file = File::find()->andWhere(['md5sum' => $md5])->active()->one();
        if (!$file) {
            $this->debugOutput('Deleting: ' . $gcRenderModel->webdavUrl . $gcRenderModel->path);
            $this->deleteFile($gcRenderModel);
        }
        $gcRenderModel->processed_at = DateHelper::now();
        $gcRenderModel->safeSave();
    }

    protected function deleteFile(GcRenderModel $gcRenderModel)
    {
        $url = $gcRenderModel->webdavUrl . $gcRenderModel->path;

        $ch = curl_init();
        // No sertificate on r1.tsdev.work
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "MOVE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Destination: ' . '/archive/'.$gcRenderModel->path,
        ]);
        $output = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode != 204) {
            $this->debugOutput('Delete code: ' . $httpCode);
            $this->debugOutput($output);
        }
    }

}
