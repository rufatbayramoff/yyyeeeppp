<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.06.16
 * Time: 9:25
 */

namespace common\modules\model3dRender\components;

use common\interfaces\FileBaseInterface;
use common\models\File;
use common\modules\model3dRender\Model3dRenderModule;
use common\modules\model3dRender\models\RenderFileParams;
use common\modules\model3dRender\models\RenderImageUrl;
use common\modules\model3dRender\services\RenderModel3dPartService;
use frontend\models\model3d\Model3dFacade;
use Yii;
use yii\base\Component;
use yii\base\Module;

/**
 * Class Renderer - осовной класс для рендеринга модели через сервисы рендеринга
 *
 * @package common\modules\model3dRender\components
 */
class Renderer extends Component
{
    /**
     * @var Model3dRenderModule
     */
    protected $module;

    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    protected function getShortServerName()
    {
        $server = \Yii::$app->params['server'];
        $server = str_replace(['http://', 'https://', '/', '.'], '', $server);
        return $server;
    }

    protected function getFileExt($name)
    {
        $fileExt = strtolower(substr($name, strrpos($name, '.') + 1, 255));
        if (strlen($fileExt) !== 3) {
            $fileExt = 'stl';
        }
        return $fileExt;
    }

    /**
     * Get unique file identifier
     * Метод публичный т.к. используется в старой реализации рендеринга в библиотеке render
     *
     * @param $md5sum
     * @param $name
     * @return string
     */
    public function getFileUuid($md5sum, $name)
    {
        $serverName = $this->getShortServerName();
        $fileExt = $this->getFileExt($name);
        return $serverName . '_' . $md5sum . '.' . $fileExt;
    }

    /**
     * @param FileBaseInterface $file
     * @param RenderFileParams $renderParams
     * @return string
     */
    public function getRenderImagePath(FileBaseInterface $file, RenderFileParams $renderParams)
    {
        $fullPath = Yii::getAlias($this->module->renderImagesPath) . '/' . substr($file->md5sum, 0, 2) . '/' . substr($file->md5sum, 2, 2) .
            '/' . $file->md5sum . '_' .
            $renderParams->color .
            '_ambient_' . $renderParams->ambient .
            '_ax_' . $renderParams->ax .
            '_ay_' . $renderParams->ay .
            '_az_' . $renderParams->az .
            '_pb_' . $renderParams->pb .
            '.' . $this->module->defaultRenderImageExtension;

        return $fullPath;
    }

    /**
     * @param FileBaseInterface $file
     * @param null|RenderFileParams $renderParams
     * @return string
     */
    public function getRenderImageUrl(FileBaseInterface $file, $renderParams = null)
    {
        if ($renderParams == null) {
            $renderParams = new RenderFileParams();
        }

        $server = Yii::$app->params['staticUrl'];

        $resultUrl = $server . $this->module->renderImagesUrl . '/' . $file->md5sum .
            '_color_' . $renderParams->color .
            '_ambient_' . $renderParams->ambient .
            '_ax_' . $renderParams->ax .
            '_ay_' . $renderParams->ay .
            '_az_' . $renderParams->az .
            '_pb_' . $renderParams->pb .
            '.' . $this->module->defaultRenderImageExtension;
        return $resultUrl;
    }


    public function getRenderImageUrlBase(File $file)
    {
        /** @var RenderModel3dPartService $renderModel3dPartService */
        $renderModel3dPartService = Yii::createObject(
            [
                'class'  => RenderModel3dPartService::class,
                'module' => $this->module
            ]
        );
        $renderService = $renderModel3dPartService->getRenderService($file);
        return $renderService->getRenderImageUrlBase();
    }

    public function getRenderFileExt(File $file)
    {
        return RenderImageUrl::getRenderExtension($file->getFileExtension());
    }

    /**
     * @param File $file
     * @param RenderFileParams $renderParams
     * @throws \yii\base\InvalidConfigException
     */
    public function renderFile($file, $renderParams)
    {
        /** @var RenderModel3dPartService $renderModel3dPartService */
        $renderModel3dPartService = Yii::createObject(
            [
                'class'  => RenderModel3dPartService::class,
                'module' => $this->module
            ]
        );
        $renderModel3dPartService->renderFile($file, $renderParams);
    }
}
