<?php

namespace common\modules\model3dRender\actions;

use common\models\File;
use lib\render\RenderException;
use yii;
use yii\base\Action;

/**
 * Class ActionTranslateSave
 *
 * @property \common\components\BaseController $controller
 */
class ActionDownloadModel3d extends Action
{
    public function run()
    {
        $modelMd5 = Yii::$app->request->get('md5');
        $remoteIp = $_SERVER['REMOTE_ADDR'] ?? null;
        if (!((strpos($remoteIp, '10.102.0.') === 0) ||(strpos($remoteIp, '10.137.') === 0) || (in_array($remoteIp, Yii::$app->getModule('model3dRender')->allowRemoteModel3dDownloadIp)))) {
            throw new yii\web\ForbiddenHttpException('Not allowed');
        }
        $file = File::find()->md5($modelMd5)->orderBy('id desc')->one();
        if (!$file) {
            throw new RenderException('Not found exception');
        }
        $path = $file->getConvertedStlPath();
        $this->controller->response->sendFile($path, $file->getFileName());
    }
}
