<?php
$backConnectUrl          = 'http://ts.h3.tsdev.work/';
$stlStoragePath          = '/vmachinessd2/vgprj/vgtestdev/repo2/tools/renderer2/cache/model';

function tryCurlDownload($url, $filePath, $downloadTimeout = 300)
{
    $ch = curl_init();
    $fp = fopen($filePath, 'w');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, $downloadTimeout);
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $downloadTimeout);
    curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    fclose($fp);

    if ($httpCode != 200 || filesize($filePath) < 500) {
        unlink($filePath);
        return false;
    }
    return true;
}

function redirectToViewModel($comment)
{
    $link = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    if ($redirectPos = strpos($link, '&redirect')) {
        $link = substr($link, 0, $redirectPos);
    }
    $link .= '&redirect' . time();
    header('Location: ' . $link);
    echo 'redirect: ' . date('Y-m-d H:i:s') . ' ' . $comment;
    exit;
}

$modelFilePath = $_GET['filename'];
$fileInfo      = pathinfo($modelFilePath);
$modelDir      = $stlStoragePath . '/' . substr($fileInfo['filename'], 0, 2) . '/' . substr($fileInfo['filename'], 2, 2);

if (!is_dir($modelDir)) {
    mkdir($modelDir, 0777, true);
}
$fullModelPath = $modelDir . '/' . $fileInfo['basename'];

if (is_file($fullModelPath)) {
    redirectToViewModel('model exists');
}


$fullModelPathDownload = $fullModelPath . '.download';
if (is_file($fullModelPathDownload)) {
    $lastTimeDiff = time() - filemtime($fullModelPathDownload);
    if ($lastTimeDiff < 300) {
        sleep(10);
        redirectToViewModel('downloading ' . $lastTimeDiff);
    }
    unlink($fullModelPathDownload);
}

$md5 = $fileInfo['filename'];

if (array_key_exists('HTTP_REFERER', $_SERVER) &&  $referrer = $_SERVER['HTTP_REFERER']) {
    $referrerParsed = parse_url($referrer);
    $backConnectUrl = $referrerParsed['scheme'] . '://' . $referrerParsed['host'];
}

$downloadModelUrl = $backConnectUrl . '/site/download-model3d?md5=' . $md5;
$downloadResult   = tryCurlDownload($downloadModelUrl, $fullModelPathDownload);
if ($downloadResult) {
    rename($fullModelPathDownload, $fullModelPath);
    redirectToViewModel('downloaded');
}
http_response_code(416);
echo "download failed: ".$downloadModelUrl.' to '.$fullModelPathDownload;
