<?php

namespace common\modules\model3dRender;

use common\components\BaseModule;
use common\modules\model3dRender\components\Renderer;
use common\modules\model3dRender\garbageCollectors\RenderGarbageCollector;
use common\modules\model3dRender\models\RenderServiceImageParams;
use yii\base\InvalidParamException;
use yii\base\Module;

/**
 * Class Module - модуль для рендеринга изображений.
 *
 * @property Renderer $renderer
 * @property RenderGarbageCollector $gcCollector
 *
 * @property array $renderServerAddress Конфиг: список адресов серверов для рендиринга модели, и их настроек.
 *
 * @package app\modules\support
 */
class Model3dRenderModule extends BaseModule
{
    public $renderServerAddress = [];
    public $renderImagesPath = '@static/render';
    public $renderImagesUrl = '/render';
    public $defaultRenderImageColor = 'ff0000';
    public $defaultRenderImageExtension = 'png';
    public $defaultAngleX = 0;
    public $defaultAngleY = 30;
    public $defaultAngleZ = 0;
    public $anglePreloadStep = 30;
    public $downloadImagesTimout = 3 * 60; // Download timeout 3 minutes
    public $allowRemoteModel3dDownloadIp = ['10.102.0.90'];


    protected function getComponentsList()
    {
        return [
            'renderer'    => Renderer::class,
            'gcCollector' => RenderGarbageCollector::class
        ];
    }


    /**
     * @throws \yii\base\InvalidParamException
     */
    protected function initConfig()
    {
        if (!$this->renderServerAddress) {
            throw new InvalidParamException('Please set "renderServerAddress" in render3dModule config');
        }

        $renderServerAddress = [];
        foreach ($this->renderServerAddress as $address => $params) {
            if (is_string($params)) {
                $fullParams = [];
                $address    = $params;
            } else {
                $fullParams = $params;
            }

            if (!array_key_exists('webdavUrl', $fullParams)) {
                $fullParams['webdavUrl'] = 'https://' . $address . ':8089/webdav/';
            }
            $renderImageParams         = new RenderServiceImageParams();
            $renderImageParams->imgUrl = 'https://' . $address . '/render/';
            if (array_key_exists('renderImageParams', $fullParams)) {
                foreach ($fullParams['renderImageParams'] as $paramName => $paramValue) {
                    $renderImageParams->$paramName = $paramValue;
                }
            }

            $fullParams['renderImageParams'] = $renderImageParams;
            $renderServerAddress[$address]   = $fullParams;
        }
        $this->renderServerAddress = $renderServerAddress;
    }

    public function getConfigExample()
    {
        return [
            'model3dRender' => [
                'class'                => \common\modules\model3dRender\Model3dRenderModule::class,
                'renderImagesPath'     => '@static/render',
                'renderImagesUrl'      => '/static/render',
                'allowRemoteModel3dDownloadIp' => ['10.102.0.90'],

                // Short form
                'renderServerAddress'  => ['r1.tsdev.work'],
                // Long form
                'renderServerAddress'  => [
                    'r1.tsdev.work' => [ // IP адрес сервера, используется, если не заполнен, какой либо из параметров
                        'webdavUrl'         => 'http://r1.tsdev.work:8089/webdav/',
                        'renderImageParams' => [
                            'imgUrl'            => 'http://r1.tsdev.work:5555/render/', // Начало ссылки на изображение с 3d моделью
                            'modelColor'        => 'ffffff',
                            'modelColor3d'      => '0',
                            'modelTexture'      => 'texture_lines4.jpg',
                            'modelTextureTT'    => '2',
                            'modelRotateSpeedX' => '0.4',                           // Коэфицент скорости вращения по оси X в зависимости от движения мыши
                            'modelRotateSpeedY' => '0.4',                           // Коэфицент скорости вращения по оси Y
                            'modelGranular'     => '5'                              // Минимальный угол разворота модели
                        ]
                    ]
                ],
                'downloadImagesTimout' => 30
            ]
        ];
    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();

        $this->initConfig();
        $componentsList = $this->getComponentsList();
        $this->setComponents($componentsList);
        $this->initComponentsModule($componentsList);
    }
}
