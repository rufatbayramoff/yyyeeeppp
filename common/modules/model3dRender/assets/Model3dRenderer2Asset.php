<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.06.16
 * Time: 17:57
 */

namespace common\modules\model3dRender\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class Model3dRenderer2Asset extends AssetBundle
{
    public $sourcePath = '@common/modules/model3dRender/assets/resources';

    public $js = [
        'js/model3dRenderRoute.js',
        'js/model3dRender2.js'
   ];

    public $css = [
        'css/model3dRender2.css'
    ];
}
