var model3dRenderParams = {
    fileUid: '',
    colorHex: '',
    ax: '',
    ay: '',
    pb: '',
    isMulticolorFormat: '',
    renderUrlBase: ''
};

var model3dRenderRouteClass = {
    config: {
        baseUrl: ''

    },
    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
    },

    getUrl: function (renderParams) {
        var self = this;
        if (!renderParams.isMulticolorFormat && renderParams.colorHex === 'multicolor') {
            renderParams.colorHex = 'ffffff';
        }

        var imgUrl = renderParams.renderUrlBase;
        var fileUid = renderParams.fileUid;
        if (!fileUid) {
            return '/static/images/3dicon.png';
        }

        if (renderParams.ax === 0 && renderParams.ay === 30) {
            imgUrl = self.config.baseUrl;
            // remove ext
            if (fileUid.indexOf('_') > 0) {
                fileUid = fileUid.split('_')[0];
            }
        }

        var ambient = renderParams.colorHex == 'ffffff' ? 40 : 0;

        return imgUrl +
            fileUid +
            '_color_' + renderParams.colorHex +
            '_ambient_' + ambient +
            '_ax_' + renderParams.ax +
            '_ay_' + renderParams.ay +
            '_az_' + 0 +
            '_pb_' + renderParams.pb +
            '.png';
    }
};
