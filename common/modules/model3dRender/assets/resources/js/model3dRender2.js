/**
 * Created by analitic on 14.06.16.
 */

var model3dRenderClass = {
    config: {
        imgElementId: '',   // Image dom element id
        fileUid: '',
        color: '',          // Example: ffffff
        ax: 0, ay: 30, az: 0,
        scaleX: 0.6,
        scaleY: 0.6,
        pb: 0,
        granular: 30,        // Rotate degrees
        granularPrecise: 2.5,
        defaultRenderImageExtension: 'png',
        allowAutoRotate: false,
        isMulticolorFormat: '',
        renderUrlBase: ''
    },
    defaultConfig: {},
    lastMouseStatus: false,
    ax: 0, ay: 30, az: 0, ax0: 0, ay0: 0, az0: 0, pb: 0,
    mouseX: 0, mouseY: 0,
    model3dMainImg: null,
    pluginsInfo: {},        // For additional status save
    isMobileEventsWork: false,
    isMouseDownFlag: false,
    prevImgUrl: '',
    loadedImages: {},
    loadingQueue: [],
    isLoading: false,
    allowAutoRotate: true,
    autoRotateTimer: null,
    stopRenderDownload: false,
    isVisible: true,

    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
        self.defaultConfig = jQuery.extend({}, self.config); // Jquery copy object
        self.allowAutoRotate = self.allowAutoRotate && self.config.allowAutoRotate;

        self.clearListener();
        self.initImageRotate();
        return self;
    },

    initImageRotate: function () {
        var self = this;
        var bindEventsElement = self.getImageContainer();

        bindEventsElement.addClass('model3dRenderLiveImgContainer');

        if (typeof bindEventsElement.get(0) == 'undefined') {
            return;
        }

        $('#modal3dModelView').addClass('model3dRenderLiveModal');


        bindEventsElement.on('pointerdown', (function (event) {
                self.isMouseDownFlag = true;
            }
        ));

        bindEventsElement.on('mousedown', function () {
            //console.log('mouseDown');
            self.isMouseDownFlag = true;
        });

        $(window).on('mouseup', function () {
            self.isMouseDownFlag = false;
        });

        bindEventsElement.on('mouseup', function () {
            self.isMouseDownFlag = false;
        });

        $(window).on('mousemove', {'self': self}, self.onMouseMove);

        // For mobile devices. Example: Ipad, Android
        bindEventsElement.on('touchstart', {'self': self}, self.onTouchStart);
        bindEventsElement.on('touchend', {'self': self}, self.onTouchEnd);

        if (typeof (bindEventsElement.get(0).addEventListener) != 'undefined') {
            bindEventsElement.get(0).addEventListener('touchmove', function (event) {
                self.onTouchMove(event);
            });
        }
        $(window).on('stopRenderAutoRotate', {'self': self}, self.stopRenderAutoRotate);
        $(window).on('disableRenderAutoRotate', {'self': self}, self.disableRenderAutoRotate);
        $(window).on('stopRenderDownload', {'self': self}, self.setStopRenderDownload);

        if (!self.allowAutoRotate) {
            self.initQueue(self.config.granular);
        }
    },

    getImageContainer: function () {
        var self = this;
        var model3dMainImg = typeof self.config.imgElementId == 'string' ? $('#' + self.config.imgElementId) : self.config.imgElementId;
        self.model3dMainImg = model3dMainImg;
        return model3dMainImg.parent();
    },

    clearListener: function () {
        var self = this;
        var image = self.getImageContainer();
        $(window).off('mousemove', self.onMouseMove);
        $(window).off('stopRenderAutoRotate', self.stopRenderAutoRotate);
        $(window).off('disableRenderAutoRotate', self.disableRenderAutoRotate);
        $(window).off('stopRenderDownload', self.setStopRenderDownload);
        if (typeof image.get(0) == 'undefined') {
            return;
        }
        image.off('touchstart', self.onTouchStart);
        image.off('touchend', self.onTouchEnd);
        if (typeof (image.get(0).addEventListener) != 'undefined') {
            image.get(0).removeEventListener('touchmove', function (event) {
                self.onTouchMove(event);
            }, false);
        }
    },

    /**
     * Update color
     * @param color
     */
    updateColor: function (color) {
        this.config.color = color;
        this.loadedImages = [];
        this.loadingQueue = [];
        this.clearListener();
        this.initImageRotate();
        this.placeImg();
    },

    setIsVisible: function (isVisible) {
        var self = this;
        self.isVisible = isVisible;
    },

    formUrl: function (aax, aay) {
        var self = this;
        if (aay > 89) aay = 89;
        if (aay < -89) aay = -89;
        if (aax < -180) aax = -180;
        if (aax > 180) aax = 180;
        if (aax === 180) aax = -180;


        var ambient = 0;
        if (self.config.color == 'ffffff') {
            ambient = 40;
        }

        var renderParams = commonJs.clone(model3dRenderParams);
        renderParams.fileUid = self.config.fileUid;
        renderParams.colorHex = self.config.color;
        renderParams.ax = aax;
        renderParams.ay = aay;
        renderParams.pb = self.config.pb;
        renderParams.isMulticolorFormat = self.config.isMulticolorFormat;
        renderParams.renderUrlBase = self.config.imgUrl || self.config.renderUrlBase;
        var imgUrl = model3dRenderRoute.getUrl(renderParams);
        return imgUrl;
    },

    placeImg: function () {
        var self = this;
        var aax = Math.round(self.ax / self.config.granular) * self.config.granular;
        var aay = Math.round(self.ay / self.config.granular) * self.config.granular;

        var imgUrl = self.formUrl(aax, aay);

        if (self.prevImgUrl === imgUrl) {
            return;
        }
        self.prevImgUrl = imgUrl;

        if (self.loadedImages[imgUrl]) {
            self.model3dMainImg.get(0).src = imgUrl;
            self.model3dMainImg.trigger('renderImageLoad', imgUrl);
            return
        }

        self.addToQueueNearestPositions(aax, aay);
        if (self.isLoading) {
            self.addToQueue(imgUrl);
        } else {
            self.loadImage(imgUrl);
        }
    },

    loadImage: function (imgUrl, onload, onerror) {
        var self = this;
        if (self.stopRenderDownload) {
            if (onerror) {
                onerror();
            }
            return;
        }
        if (self.isLoading) {
            if (onerror) {
                onerror();
            }
            return;
        }

        self.isLoading = true;
        var preloadImage = new Image();
        self.loadedImages[imgUrl] = preloadImage;
        preloadImage.onload = function (event) {
            self.isLoading = false;
            if (onload) {
                onload(imgUrl);
            } else {
                self.onImageLoaded(imgUrl);
            }
        };
        preloadImage.onerror = function (event) {
            self.isLoading = false;
            if (onerror) {
                onerror();
            }
            self.loadedImages[imgUrl] = null;
        };
        preloadImage.src = imgUrl;
    },

    onImageLoaded: function (imgUrl) {
        var self = this;
        if (self.prevImgUrl === imgUrl) {
            // last viewed image loaded
            if (self.model3dMainImg && typeof (self.model3dMainImg.get(0)) !== 'undefined') {
                self.model3dMainImg.get(0).src = imgUrl;
                self.model3dMainImg.trigger('renderImageLoad', imgUrl);
            }
        }
        self.popupQueue();
    },

    initQueue: function (step) {
        var self = this;
        for (ax = -180; ax <= 180; ax += step) {
            for (ay = -90; ay <= 90; ay += step) {
                var url = self.formUrl(ax, ay);
                self.addToQueue(url);
            }
        }
    },

    popupQueue: function () {
        var self = this;
        if (!self.isVisible) {
            return;
        }
        var lastQueueUrl = self.loadingQueue.pop();
        if (lastQueueUrl) {
            self.loadImage(lastQueueUrl);

        } else {
            if (self.config.granular === 30) {
                self.config.granular = 15;
            }

        }
    },

    addToQueueNearestPositions: function (aax, aay) {
        var self = this;
        var granular = self.config.granular;

        for (var i = 3; i > 0; i--) {
            var nextGranular = granular * i;
            self.addToQueue(self.formUrl(aax - nextGranular, aay - nextGranular));
            self.addToQueue(self.formUrl(aax + nextGranular, aay - nextGranular));
            self.addToQueue(self.formUrl(aax - nextGranular, aay + nextGranular));
            self.addToQueue(self.formUrl(aax + nextGranular, aay + nextGranular));
        }
    },

    addToQueue: function (imgUrl) {
        var self = this;
        if (self.loadedImages[imgUrl]) {
            return;
        }
        self.loadingQueue.push(imgUrl);
    },

    setMoveCursor: function () {
        var self = this;
        self.model3dMainImg.parent().addClass('model3dRenderLiveImgMoveCursor');
    },

    setSimpleCursor: function () {
        var self = this;
        self.model3dMainImg.parent().removeClass('model3dRenderLiveImgMoveCursor');
    },

    isMouseDown: function (event) {
        var self = this;
        return self.isMouseDownFlag && (event.buttons && 1)
    },

    onMouseMove: function (event) {
        var self = event.data.self;
        if (self.isMobileEventsWork) {
            // If it`s work on mobile version. Click on change color button is move model;
            return;
        }
        //console.log('mouse move '+self.config.fileUid + ' - ' + event.screenX + '-' + event.screenY);
        self.onModelMove(event.screenX, event.screenY, self.isMouseDown(event));
    },

    onTouchStart: function (event) {
        var self = this;
        //console.log('touch start');
        self.isMobileEventsWork = true;
        self.granular = self.config.granular + 10; // In 10 degrees is more for mobile devices

        if (event.touches) {

            self.mouseX = event.touches[0].screenX;
            self.mouseY = event.touches[0].screenY;
            self.setMoveCursor();
            self.lastMouseStatus = true;
        }

    },

    onTouchEnd: function (event) {
        var self = this;
        //console.log('touch end');
        self.isMobileEventsWork = true;
        self.setSimpleCursor();
        self.lastMouseStatus = false;
    },

    onTouchMove: function (event) {
        var self = this;
        event.preventDefault();
        //console.log('touch move');
        //console.log(event);
        self.isMobileEventsWork = true;
        self.onModelMove(event.touches[0].screenX, event.touches[0].screenY, true);
        return false;
    },

    onModelMove: function (clientX, clientY, isMouseDown) {
        var self = this;
        if (!self.isVisible) {
            return;
        }
        if (isMouseDown) {
            if (!self.lastMouseStatus) {
                self.mouseX = clientX;
                self.mouseY = clientY;
                self.setMoveCursor();
            }
            $(window).trigger('disableRenderAutoRotate');
            self.ax = self.ax + Math.round((self.mouseX - clientX) * self.config.scaleX);
            self.ay = self.ay - Math.round((self.mouseY - clientY) * self.config.scaleY);
            self.mouseX = clientX;
            self.mouseY = clientY;
            if (self.ay > 90) self.ay = 90;
            if (self.ay < -90) self.ay = -90;
            if (self.ax < -180) self.ax = self.ax + 360;
            if (self.ax > 180) self.ax = self.ax - 360;
            if (self.ax != self.ax0 || self.ay != self.ay0 || self.az != self.az0) {
                self.ax0 = self.ax;
                self.ay0 = self.ay;
                self.az0 = self.az;
                self.placeImg();
            }
        } else {
            self.setSimpleCursor();
        }
        self.lastMouseStatus = isMouseDown;
    },

    autoRotateStart: function () {
        var self = this;
        if (!self.allowAutoRotate) {
            return;
        }
        if (self.autoRotateTimer) {
            clearTimeout(self.autoRotateTimer);
        }
        self.autoRotateTimer = setTimeout(function () {
            self.autoRotate();
        }, 1000);
    },

    autoRotate: function () {
        var self = this;
        self.ax = self.ax + 15;
        if (self.ax >= 180) {
            self.ax = -180;
        }

        if (!self.allowAutoRotate) {
            return;
        }

        var imgUrl = self.formUrl(self.ax, self.ay);

        if (self.loadedImages[imgUrl]) {
            self.model3dMainImg.get(0).src = imgUrl;
            self.autoRotateTimer = setTimeout(function () {
                self.autoRotate();
            }, 500);
            return;
        }
        self.loadImage(imgUrl, function () {
            self.model3dMainImg.get(0).src = imgUrl;
            clearTimeout(self.autoRotateTimer);
            self.autoRotateTimer = setTimeout(function () {
                self.autoRotate();
            }, 500);
        }, function () {
            clearTimeout(self.autoRotateTimer);
            self.autoRotateTimer = setTimeout(function () {
                self.autoRotate();
            }, 500);
        });
    },

    stopRenderAutoRotate: function (event) {
        var self = event.data.self;
        clearTimeout(self.autoRotateTimer);
        self.autoRotateTimer = null;
    },

    disableRenderAutoRotate: function (event) {
        var self = event.data.self;
        if (self.allowAutoRotate) {
            self.allowAutoRotate = false;
            self.initQueue(self.config.granular);
        }
    },

    setStopRenderDownload: function (event) {
        var self = event.data.self;
        self.stopRenderDownload = true;
    }

};
