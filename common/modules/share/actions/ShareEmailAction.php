<?php
namespace common\modules\share\actions;


class ShareEmailAction extends \yii\base\Action
{

    public function run($link = false)
    {
        $userId = \frontend\models\user\UserFacade::getCurrentUserId();

        $link = $link ? $link : app('cache')->get("sendemail" . $userId, false);
        if ($link == false) {
            $link = app('request')->referrer;
            app('cache')->set("sendemail" . $userId, $link);
        }

        $sendEmailForm = new \frontend\models\site\SendEmailForm();
        if (app('request')->isPost) {
            $sendEmailForm->shareLink = $link;
            $sendEmailForm->load(app('request')->post());
            if ($sendEmailForm->validate()) {
                $sendEmailForm->sendEmail();
                app('cache')->set("sendemail" . $userId, false);
                return $this->controller->jsonMessage(_t("site.email", "Email sent. Thank you."));
            } else {
                $errMsg = \yii\helpers\Html::errorSummary($sendEmailForm);
                $this->controller->setFlashMsg(false, $errMsg);
                if ($this->controller->isAjax) {
                    return $this->controller->jsonError($errMsg);
                }
            }
        }
        return $this->controller->renderAjax(
            "sendemail.php",
            [
                'model' => $sendEmailForm
            ]
        );
    }
}