<?php

namespace common\modules\instantPayment\helpers;

use common\models\Company;
use common\models\InstantPayment;
use common\models\MsgTopic;
use common\models\PaymentInvoice;
use common\models\StoreOrder;
use common\models\User;

class InstantPaymentUrlHelper
{

    public static function getRecvList($uuidSearch = '')
    {
        return '/workbench/instant-payments/recv-list' . ($uuidSearch ? '?InstantPaymentSearch%5BsearchText%5D=' . $uuidSearch : '');
    }

    public static function getSendList($uuidSearch = '')
    {
        return '/workbench/instant-payments/send-list' . ($uuidSearch ? '?InstantPaymentSearch%5BsearchText%5D=' . $uuidSearch : '');
    }

    public static function getUrlForCompany(Company $company)
    {
        return '/workbench/instant-payments/create?for-ps-id=' . $company->id;
    }

    public static function getView(InstantPayment $instantPayment)
    {
        return '/workbench/instant-payments/view?id=' . $instantPayment->uuid;
    }

    public static function getPaymentUrl(InstantPayment $instantPayment)
    {
        return '/workbench/instant-payments/pay?id=' . $instantPayment->uuid;
    }

    public static function approveUrl(InstantPayment $instantPayment)
    {
        return '/workbench/instant-payments/approve?id=' . $instantPayment->uuid;
    }

    public static function getByTopicBind(?User $user, MsgTopic $topic)
    {
        $listTo = $topic->getOtherUsers($user);
        if (count($listTo) == 1) {
            $toUser = reset($listTo);
            if ($toUser->id !== User::USER_ID_SUPPORT && $toUser->company && $toUser->company->isActive()) {
                return self::getUrlForCompany($toUser->company);
            }
        }
        return '';
    }
}
