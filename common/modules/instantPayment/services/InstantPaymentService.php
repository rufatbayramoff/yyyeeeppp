<?php

namespace common\modules\instantPayment\services;

use backend\models\user\UserAdmin;
use Braintree\Base;
use common\components\DateHelper;
use common\components\Emailer;
use common\models\Company;
use common\models\InstantPayment;
use common\models\PaymentInvoice;
use common\models\Ps;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyInstantPaymentInformer;
use common\modules\informer\models\CompanyQuoteToOrderInformer;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\services\PaymentInstantPaymentService;
use yii\base\BaseObject;
use yii\base\UserException;

class InstantPaymentService extends BaseObject
{
    /** @var UserIdentityProvider */
    protected $userIdentityProvider;

    /** @var Emailer */
    protected $emailer;

    /** @var PaymentInstantPaymentService */
    protected $paymentInstantPaymentService;

    public function injectDependencies(UserIdentityProvider $userIdentityProvider, Emailer $emailer, PaymentInstantPaymentService $paymentInstantService)
    {
        $this->userIdentityProvider         = $userIdentityProvider;
        $this->emailer                      = $emailer;
        $this->paymentInstantPaymentService = $paymentInstantService;
    }

    public function getNewPaymentForCompany(Ps $company)
    {
        $count = InstantPayment::find()->forCompany($company)->waitingForConfirm()->count();
        return $count;
    }

    public function allowCancelForUser(InstantPayment $instantPayment, User $user)
    {
        if (($instantPayment->status === InstantPayment::STATUS_WAIT_FOR_CONFIRM) || ($instantPayment->status === InstantPayment::STATUS_NOT_PAYED)) {
            if ($instantPayment->from_user_id == $user->id || $instantPayment->to_user_id == $user->id) {
                return true;
            }
        };
        return false;

    }

    public function allowCancelForCurrentUser(InstantPayment $instantPayment)
    {
        $user = $this->userIdentityProvider->getUser();
        if ($user instanceof UserAdmin) {
            return true;
        }
        return $this->allowCancelForUser($instantPayment, $user);
    }

    public function isToUser(InstantPayment $instantPayment, User $user)
    {
        return $instantPayment->toUser->id == $user->id;
    }

    public function approve(InstantPayment $instantPayment)
    {
        if ($instantPayment->status !== InstantPayment::STATUS_WAIT_FOR_CONFIRM) {
            throw new PaymentException(
                _t("site.payment", 'Instant payment #{uuid} status is wrong', ['uuid' => $instantPayment->uuid])
            );
        }

        $instantPayment->status      = InstantPayment::STATUS_APPROVED;
        $instantPayment->finished_at = DateHelper::now();
        $instantPayment->safeSave();

        $this->paymentInstantPaymentService->approvePayment($instantPayment);
    }

    public function cancelPayment(InstantPayment $instantPayment)
    {
        $instantPayment->status = InstantPayment::STATUS_CANCELED;
        $instantPayment->safeSave();
        if ($instantPayment->primaryInvoice->isPayed()) {
            $this->paymentInstantPaymentService->cancelPayment($instantPayment);
        }
    }

    public function setPayed(PaymentInvoice $invoice)
    {
        $instantPayment                       = $invoice->instantPayment;
        $instantPayment->status               = InstantPayment::STATUS_WAIT_FOR_CONFIRM;
        $instantPayment->payed_at             = DateHelper::now();
        $instantPayment->primary_invoice_uuid = $invoice->uuid;
        $instantPayment->safeSave();
        InformerModule::addInformer($instantPayment->toUser, CompanyInstantPaymentInformer::class);
        $this->emailer->sendNewInstantPayment($instantPayment);
    }

    public function getWaitingForApproveCount(Company $company)
    {
        $count = InstantPayment::find()->forCompany($company)->waitingForConfirm()->count();
        return $count;
    }
}