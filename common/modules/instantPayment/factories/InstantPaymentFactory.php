<?php

namespace common\modules\instantPayment\factories;

use common\components\DateHelper;
use common\components\UuidHelper;
use common\models\Company;
use common\models\InstantPayment;
use common\models\User;
use lib\money\Currency;
use yii\base\BaseObject;

class InstantPaymentFactory extends BaseObject
{
    public function generateUuid()
    {
        do {
            $uuid      = strtoupper(\common\components\UuidHelper::generateRandCode(6));
            $checkUuid = InstantPayment::find()->where(['uuid' => $uuid])->one();
        } while ($checkUuid);
        return $uuid;
    }

    public function create(Company $forCompany, User $fromUser, array $data)
    {
        /** @var InstantPayment $instantPayment */
        $instantPayment               = \Yii::createObject(InstantPayment::class);
        $instantPayment->uuid         = $this->generateUuid();
        $instantPayment->created_at   = DateHelper::now();
        $instantPayment->descr        = $data['descr'] ?? '';
        $instantPayment->sum          = $data['sum'] ?? '';
        $instantPayment->currency     = $data['currency']?? Currency::USD;
        $instantPayment->status       = InstantPayment::STATUS_NOT_PAYED;
        $instantPayment->from_user_id = $fromUser->id;
        $instantPayment->to_user_id   = $forCompany->user->id;
        return $instantPayment;
    }
}