<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.06.16
 * Time: 17:13
 */

namespace common\modules\translation\actions;

use yii\base\Action;

/**
 * Class ActionTranslateSave
 *
 * @property \common\components\BaseController controller
 */
class ActionSetTranslateMode extends Action
{
    public function run($action)
    {
        //TODO: Заменить \Yii::$app->getModule('translation') на текущий модуль?
        if ($action === 'on') {
            \Yii::$app->getModule('translation')->i18n->setTranslateModeStatus(true);
        } else {
            \Yii::$app->getModule('translation')->i18n->setTranslateModeStatus(false);
        }

        return $this->controller->redirect(\Yii::$app->request->referrer);
    }
}