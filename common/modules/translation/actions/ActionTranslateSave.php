<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.06.16
 * Time: 17:14
 **/

namespace common\modules\translation\actions;

use common\components\DateHelper;
use common\models\SystemLangMessage;
use common\models\user\UserIdentityProvider;
use HttpException;
use yii;
use yii\base\Action;

/**
 * Class ActionTranslateSave
 *
 * @property \common\components\BaseController $controller
 */
class ActionTranslateSave extends Action
{
    public function run()
    {
        if (!\Yii::$app->request->post()) {
            throw new HttpException(400, 'Need post parameter translate');
        }

        $translations = Yii::$app->request->post()['translation'];
        $lang = \Yii::$app->language;

        foreach ($translations as $id => $text) {
            $messageTranslate = SystemLangMessage::tryFind(['id' => $id, 'language' => $lang]);
            $messageTranslate->is_auto_translate = 0;
            $oldText= $messageTranslate->translation;
            $newText= trim($text);
            $messageTranslate->translation = trim($text);
            if ($oldText != $newText) {
                $userIdentityProvider = Yii::createObject(UserIdentityProvider::class);
                $messageTranslate->updated_at   = DateHelper::now();
                $messageTranslate->translated_by = $userIdentityProvider->getUser()->id;
            }
            $messageTranslate->safeSave();
        }
        \Yii::$app->getModule('translation')->jsI18n->formTempAssetsFile($lang);
        return $this->controller->jsonSuccess();
    }
}