<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.06.16
 * Time: 17:14
 **/

namespace common\modules\translation\actions;

use common\models\SystemLangSource;
use yii\base\Action;
use yii\base\UserException;

/**
 * Class ActionTranslateSave
 *
 * @property \common\components\BaseController $controller
 */
class ActionTranslateDialog extends Action
{
    public function run($ids)
    {
        $ids = explode(',', $ids);
        $messagesOrig = SystemLangSource::find()->where([SystemLangSource::column('id')=>$ids])->all();

        if (!$messagesOrig) {
            throw new UserException('Origin not found. Run command [yii translate]');
        }

        return $this->controller->renderPartial('@common/modules/translation/views/translate.php', [ // TODO: Заменить на относительные пути
            'messagesOrig' => $messagesOrig
        ]);
    }
}