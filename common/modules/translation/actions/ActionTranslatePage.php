<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.01.18
 * Time: 17:17
 */

namespace common\modules\translation\actions;

use yii\base\Action;

class ActionTranslatePage extends Action
{
    public function run()
    {
        $url = \Yii::$app->request->post('url');
        $parsedUrl = parse_url($url);
        $getParams = [];
        if (array_key_exists('query', $parsedUrl)) {
            parse_str($parsedUrl['query'], $getParams);
        }

        $cat = \Yii::$app->request->post('category');
        $msg = \Yii::$app->request->post('message');
        $referer = \Yii::$app->request->post('referer');
        $isPost = false;
        $isJs = true;
        $postParams = null;

        \Yii::$app->getModule('translation')->i18n->fixTranslationPageWithParams($cat, $msg, $parsedUrl['path'], $referer, $isPost, $isJs, $getParams, $postParams);

        return $this->controller->asJson([
            'success' => true
        ]);
    }
}