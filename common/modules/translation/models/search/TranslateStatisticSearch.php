<?php

namespace common\modules\translation\models\search;

use common\modules\translation\models\I18nTableElement;
use common\modules\translation\models\TranslateStatistic;
use Yii;
use yii\data\ArrayDataProvider;

class TranslateStatisticSearch extends TranslateStatistic
{
    public $userId = null;
    public $dateFrom;
    public $dateTo;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['dateFrom', 'dateTo'], 'safe']
        ]);
    }


    public function formName()
    {
        return 'i18n_stat';
    }

    public function searchItems($params, $formName = null)
    {
        if ($formName === null) {
            $formName = $this->formName();
        }
        if ($formName) {
            if (array_key_exists($this->formName(), $params)) {
                $this->populateParams($params[$this->formName()]);
            }
        } else {
            $this->populateParams($params);
        }
        $rows = Yii::$app->getModule('translation')->statistics->getStatistics($this);
        return $rows;
    }

    public function search($params, $formName = null)
    {
        $rows         = $this->searchItems($params, $formName);
        $dataProvider = new ArrayDataProvider(
            [
                'allModels' => $rows
            ]
        );
        return $dataProvider;
    }
}
