<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.11.17
 * Time: 14:10
 */

namespace common\modules\translation\models\search;

use common\modules\translation\models\I18nTableElement;
use Yii;
use yii\data\ArrayDataProvider;

class I18nTableElementSearch extends I18nTableElement
{
    public const DEFALUT_TABLE_NAME = 'biz_industry_intl';
    public const EMPTY_FIELD_VALUE  = 'empty_value';

    public $dateFrom =null;
    public $dateTo =null;
    public $emptyFieldValue = false;

    public function formName()
    {
        return 'i18n';
    }

    public function searchItems($params, $formName=null)
    {
        if ($formName === null) {
            $formName = $this->formName();
        }
        if ($formName) {
            if (array_key_exists($this->formName(), $params)) {
                $this->populateParams($params[$this->formName()]);
            }
        } else {
            $this->populateParams($params);
        }
        $rows = Yii::$app->getModule('translation')->dbI18n->getI18nRows($this);
        return $rows;
    }


    public function search($params, $formName=null)
    {
        $rows = $this->searchItems($params, $formName);
        $dataProvider = new ArrayDataProvider(
            [
                'allModels' => $rows
            ]
        );
        return $dataProvider;
    }
}