<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.11.17
 * Time: 14:06
 */

namespace common\modules\translation\models;

use common\components\DateHelper;
use common\models\User;
use common\models\UserAdmin;
use Yii;
use yii\base\Model;
use yii\db\Query;

class I18nTableElement extends Model
{
    public $tableName = null;
    public $modelId = null;
    public $fieldName = null;
    public $fieldValueOrig = FALSE;
    public $fieldValue = null;
    public $langIso = null;
    public $isActive = null;
    public $updatedAt =null;
    public $translatedBy =null;

    public const ID_DELIMETER = ',';

    public static function create(
        $tableName = null,
        $modelId = null,
        $fieldName = null,
        $fieldValueOrig = FALSE,
        $value = null,
        $langIso = null,
        $isActive = null,
        $updatedAt =null,
        $translatedBy =null
    ): I18nTableElement
    {
        $element                 = new self();
        $element->tableName      = $tableName;
        $element->modelId        = $modelId;
        $element->fieldValueOrig = $fieldValueOrig;
        $element->fieldName      = $fieldName;
        $element->fieldValue     = $value;
        $element->langIso        = $langIso;
        $element->isActive       = $isActive;
        $element->updatedAt      = $updatedAt;
        $element->translatedBy   = $translatedBy;
        return $element;
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['tableName', 'modelId', 'fieldName', 'fieldValue', 'langIso'], 'required'],
            [['modelId'], 'checkIsModelIdExists'],
            [['updatedAt', 'translatedBy'], 'safe']
        ]);
    }

    public function checkIsModelIdExists()
    {
        if ($this->tableName && $this->fieldName && $this->modelId) {
            $row = (new Query())->select('*')->from($this->tableNameSrc())->where(['id' => $this->modelId])->one();
            if (!$row) {
                $this->addError('modelId', 'Src model id isn`t exists');
                return;
            }

            $row = (new Query())->select('*')->from($this->tableName)->where(['model_id' => $this->modelId, 'lang_iso' => $this->langIso])->one();
            if ($row) {
                $this->addError('modelId', 'Src model id and lang is not unique');
                return;
            }
        }
    }

    public function getFieldSchema()
    {
        if (!$this->tableName || !$this->fieldName) {
            return null;
        }
        $connection  = Yii::$app->db;//get connection
        $dbSchema    = $connection->schema;
        $tableSchema = $dbSchema->getTableSchema($this->tableName);
        return $tableSchema->getColumn($this->fieldName);
    }

    public function populateParams($params)
    {
        foreach ($params as $paramName => $paramValue) {
            if (property_exists($this, $paramName)) {
                if ($paramValue || $paramValue === 0 || $paramValue === '0') {
                    $this->$paramName = $paramValue;
                } else {
                    $this->$paramName = null;
                }
            }
        }
    }

    /**
     * @param UserAdmin $user
     */
    public function updateTranslator($user)
    {
        $this->updatedAt = DateHelper::now();
        $this->translatedBy = $user->id;
    }

    public function getTranslatedByUser()
    {
        if ($this->translatedBy) {
            return User::find()->where(['id' => $this->translatedBy])->one();
        }
        return null;
    }

    public function tableNameSrc()
    {
        return substr($this->tableName, 0, -5);
    }

    public function getValueOrig()
    {
        if ($this->fieldValueOrig !== FALSE) {
            return $this->fieldValueOrig;
        }
        if (!$this->tableNameSrc() || !$this->modelId || !$this->fieldName) {
            return '';
        }
        $row = (new Query())->select('*')->from($this->tableNameSrc())->where(['id' => $this->modelId])->one();
        if ($row) {
            $this->fieldValueOrig = $row[$this->fieldName];
        }
        return $this->fieldValueOrig;
    }

    public static function getIdByParams($langIso, $tableName, $fieldName, $modelId)
    {
        return  $langIso . static::ID_DELIMETER . $tableName . self::ID_DELIMETER . $fieldName . self::ID_DELIMETER . $modelId;
    }

    public function getId()
    {
        return static::getIdByParams($this->langIso,$this->tableNameSrc(),$this->fieldName, $this->modelId);
    }
}