<?php

namespace common\modules\translation\models;

use common\components\DateHelper;
use common\models\User;
use common\models\UserAdmin;
use Yii;
use yii\base\Model;
use yii\db\Query;

class TranslateStatistic extends Model
{
    public $date = null;
    public $tranlsatedCount = 0;
    public $tranlsatedDbCount = 0;


    public function rules()
    {
        return array_merge(parent::rules(), [
            [['userId', 'tranlsatedCount', 'tranlsatedDbCount'], 'integer'],
            [['dateFrom', 'dateTo'], 'safe']
        ]);
    }

    public function populateParams($params)
    {
        foreach ($params as $paramName => $paramValue) {
            if (property_exists($this, $paramName)) {
                if ($paramValue || $paramValue === 0 || $paramValue === '0') {
                    $this->$paramName = $paramValue;
                } else {
                    $this->$paramName = null;
                }
            }
        }
    }
}