<?php

namespace common\modules\translation\components;

use common\components\ArrayHelper;
use common\components\FileDirHelper;
use common\models\base\SystemLangMessage;
use common\models\SystemLang;
use common\models\SystemLangSource;
use common\models\TranslationDb;
use common\modules\translation\models\search\TranslateStatisticSearch;
use common\modules\translation\models\TranslateStatistic;
use DateTime;
use DateTimeZone;
use PDOException;
use yii;
use yii\base\Component;
use yii\db\Query;

/**
 * Class Statistics
 *
 * @package common\modules\translation\components
 */
class Statistics extends Component
{

    protected function getOriginalLangWordsCount($table, $column, $lineId)
    {
        $query       = (new Query())->select($table . '.' . $column . ' as val')->from($table . '_intl')->leftJoin($table, $table . '.id=' . $table . '_intl.model_id')->andWhere([$table . '_intl.id' => $lineId]);
        try {
            $item = $query->one();
            if(!$item) {
                return 0;
            }
            $value       = $item['val'];
            $value       = strip_tags($value);
            return str_word_count($value);
        } catch (yii\db\Exception  $exception) {
            return 0;
        }
    }

    public function getStatistics(TranslateStatisticSearch $statisticSearch): array
    {
        $statistics = [];

        $query = (new Query())->select('sum(wordcount(system_lang_source.message)) as cnt, DATE(updated_at) as update_date')
            ->from(SystemLangMessage::tableName())
            ->leftJoin(SystemLangSource::tableName(), 'system_lang_source.id=system_lang_message.id');
        if ($statisticSearch->dateFrom) {
            $query->andWhere(['>=', 'updated_at', date('Y-m-d 00:00:01', strtotime($statisticSearch->dateFrom))]);
        }
        if ($statisticSearch->dateTo) {
            $query->andWhere(['<=', 'updated_at', date('Y-m-d 23:59:59', strtotime($statisticSearch->dateTo))]);
        }
        $query->andWhere(['translated_by' => $statisticSearch->userId]);
        $query->groupBy('update_date');
        $translatedByList = $query->all();

        foreach ($translatedByList as $translatedByItem) {
            $translateStatistic                    = Yii::createObject(TranslateStatistic::class);
            $translateStatistic->date              = $translatedByItem['update_date'];
            $translateStatistic->tranlsatedCount   = $translatedByItem['cnt'];
            $statistics[$translateStatistic->date] = $translateStatistic;
        }

        $query = TranslationDb::find()->andWhere(['translated_by' => $statisticSearch->userId]);
        if ($statisticSearch->dateFrom) {
            $query->andWhere(['>=', 'updated_at', date('Y-m-d 00:00:01', strtotime($statisticSearch->dateFrom))]);
        }
        if ($statisticSearch->dateTo) {
            $query->andWhere(['<=', 'updated_at', date('Y-m-d 23:59:59', strtotime($statisticSearch->dateTo))]);
        }
        /** @var TranslationDb[] $translatedDbList */
        $translatedDbList = $query->all();
        foreach ($translatedDbList as $translatedDbItem) {
            $updatedAt = substr($translatedDbItem->updated_at, 0, 10);
            if (array_key_exists($updatedAt, $statistics)) {
                $translateStatistic = $statistics[$updatedAt];
            } else {
                $translateStatistic       = Yii::createObject(TranslateStatistic::class);
                $translateStatistic->date = $updatedAt;
            }
            $origTableName = substr($translatedDbItem->table_name, 0, -5);
            $wordsCount    = $this->getOriginalLangWordsCount($origTableName, $translatedDbItem->column_name, $translatedDbItem->line_id);
            if (!$wordsCount) {
                continue;
            }
            $translateStatistic->tranlsatedDbCount += $wordsCount;
            $statistics[$updatedAt]                = $translateStatistic;
        }

        return $statistics;
    }

    public function getTranslatorUsersList()
    {
        $query            = (new Query())->select('translated_by, user.username')
            ->from(SystemLangMessage::tableName())
            ->leftJoin('user', 'user.id = translated_by')
            ->where('translated_by is not null')
            ->groupBy('translated_by');
        $translatedByList = $query->all();
        $translatedByList = ArrayHelper::map($translatedByList, 'translated_by', 'username');


        $query               = (new Query())->select('translated_by, user.username')
            ->from(TranslationDb::tableName())
            ->leftJoin('user', 'user.id = translated_by')
            ->where('translated_by is not null')
            ->groupBy('translated_by');
        $translatedByDbList  = $query->all();
        $translatedByDbList  = ArrayHelper::map($translatedByDbList, 'translated_by', 'username');
        $translatedByAllList = $translatedByList + $translatedByDbList;
        return $translatedByAllList;
    }

    public static function fixWordscountSqlFunction()
    {
        Yii::$app->db->createCommand('DROP FUNCTION IF EXISTS wordcount;')->execute();
        $sqlCommand = '
CREATE FUNCTION wordcount(str TEXT)
       RETURNS INT
       DETERMINISTIC
       SQL SECURITY INVOKER
       NO SQL
  BEGIN
    DECLARE wordCnt, idx, maxIdx INT DEFAULT 0;
    DECLARE currChar, prevChar BOOL DEFAULT 0;
    SET maxIdx=char_length(str);
    WHILE idx < maxIdx DO
        SET currChar=SUBSTRING(str, idx, 1) RLIKE \'[[:alnum:]]\';
        IF NOT prevChar AND currChar THEN
            SET wordCnt=wordCnt+1;
        END IF;
        SET prevChar=currChar;
        SET idx=idx+1;
    END WHILE;
    RETURN wordCnt;
  END
';

        Yii::$app->db->createCommand($sqlCommand)->execute();
    }
}
