<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.06.16
 * Time: 17:48
 */

namespace common\modules\translation\components;

use common\components\DateHelper;
use common\models\SystemLangPage;
use common\models\SystemLangPages;
use common\models\SystemLangSource;
use frontend\models\user\UserFacade;
use yii\base\Component;

/**
 * Class I18n
 *
 * Функционал класса относиться к управлению языком на сервере (backend)
 *
 * @package common\modules\translation\components
 */
class I18n extends Component
{
    CONST TRANSLATE_MODE_CHAR = '♺';

    public function setTranslateModeStatus($isTurnOn)
    {
        if ($isTurnOn) {
            if (UserFacade::canTranslate()) {
                \Yii::$app->session->set('translateMode', 'on');
            }
        } else {
            \Yii::$app->session->remove('translateMode');
        }
        \Yii::$app->getModule('translation')->jsI18n->formAllTempAssetsFile();
    }

    public function isTranslateMode()
    {
        return \Yii::$app->language !== 'en-US' && \Yii::$app->session->get('translateMode');
    }

    public function getMessageId($cat, $msg)
    {
        $langSource = SystemLangSource::find()->where(
            'category = :category and BINARY message=:message and not_found_in_source=0',
            [':category' => $cat, ':message' => $msg]
        )->one();
        if ($langSource) {
            return $langSource->id;
        } else {
            return null;
        }
    }

    public function isFixTranslateMode()
    {
        return \Yii::$app->setting->get('settings.isFixTranslateMode') ? true : false;
    }

    public function fixTranslationPageWithParams($cat, $msg, $url, $referer, $isPost, $isJs, $getParams, $postParams)
    {
        $langSource = SystemLangSource::find()->where(
            'category = :category and BINARY message=:message and not_found_in_source=0',
            [':category' => $cat, ':message' => $msg]
        )->one();
        if ($langSource) {
            $systemLangPage = SystemLangPage::find()->where(
                [
                    'system_lang_source_id' => $langSource->id,
                    'url'                   => $url
                ]
            )->one();
            if (!$systemLangPage) {
                $systemLangPage = new SystemLangPage();
                $systemLangPage->system_lang_source_id = $langSource->id;
                $systemLangPage->url = $url;
                $systemLangPage->referer = $referer;
                $systemLangPage->created_at = DateHelper::now();
                $systemLangPage->lang_iso = \Yii::$app->language;
                $systemLangPage->user_id = UserFacade::getCurrentUserId();
                $systemLangPage->isPost = $isPost ? 1 : 0;
                $systemLangPage->isJs = $isJs ? 1 : 0;
                $systemLangPage->getParams = $getParams;
                $systemLangPage->postParams =$postParams;
                $systemLangPage->safeSave();
            }
        }
    }

    public function fixTranslationPage($cat, $msg)
    {
        $url = $_SERVER['REQUEST_URI']??'';
        if (!$url) {
            return ;
        }
        $parsedUrl = parse_url($url);
        $referer = \Yii::$app->request->referrer;
        $isPost = \Yii::$app->request->isPost;
        $isJs = 0;
        $getParams = $_GET;
        $postParams = $_POST;
        $this->fixTranslationPageWithParams($cat, $msg, $parsedUrl['path'], $referer, $isPost, $isJs, $getParams, $postParams);
    }

    public function translate($cat, $msg, $params)
    {
        if ($this->isFixTranslateMode()) {
            $this->fixTranslationPage($cat, $msg);
        }

        if ($this->isTranslateMode()) {

            $result = \Yii::t($cat, $msg, $params);
            $langSource = SystemLangSource::find()->where(
                'category = :category and BINARY message=:message and not_found_in_source=0',
                [':category' => $cat, ':message' => $msg]
            )->one();
            if ($langSource) {
                $langMsg = $langSource->getLangMessage(\Yii::$app->language)->one();
                if ((!trim($langMsg->translation)) || ($langMsg && $langMsg->is_auto_translate)) {
                    $result = str_replace(['{{', '}}'], ['{', '}'], $msg);
                } else {
                    $result = $langMsg->translation;
                }
            }
            $result = $result . self::TRANSLATE_MODE_CHAR . $this->getMessageId($cat, $msg) . ' ';
        } else {

            $uniqueCacheId = 'tr2:' . md5($cat . $msg . implode(',', array_values($params))) . app()->language;
            $cacheResult = app('cache')->get($uniqueCacheId);

            if ($cacheResult !== false) {
                $result = $cacheResult;
            } else {
                $result = \Yii::t($cat, $msg, $params);
                app('cache')->set($uniqueCacheId, $result, 60 * 300);
            }
        }
        return $result;
    }
}