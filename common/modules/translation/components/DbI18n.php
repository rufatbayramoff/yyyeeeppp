<?php

namespace common\modules\translation\components;

use common\components\ArrayHelper;
use common\models\SystemLang;
use common\models\TranslationDb;
use common\modules\translation\models\I18nTableElement;
use common\modules\translation\models\search\I18nTableElementSearch;
use Yii;
use yii\base\Component;
use yii\db\Query;
use yii\helpers\Html;

/**
 * Class DbI18n
 *
 * @package common\modules\translation\components
 */
class DbI18n extends Component
{
    protected $i18nTablesCached = [];

    public function getI18nTablesList()
    {
        if ($this->i18nTablesCached) {
            return $this->i18nTablesCached;
        }
        $connection = Yii::$app->db;//get connection
        $dbSchema   = $connection->schema;
        //or $connection->getSchema();
        $tables     = $dbSchema->getTableNames();//returns array of tbl schema's
        $i18nTables = [];
        foreach ($tables as $tableName) {
            if (strpos($tableName, '_intl')) {
                $i18nTables[$tableName] = $tableName;
            }
        }
        $this->i18nTablesCached = $i18nTables;
        return $i18nTables;
    }

    public static function getTechnialColumnsList()
    {
        return ['id', 'model_id', 'lang_iso', 'is_active', 'column_name', 'updated_at', 'translated_by'];
    }


    protected function getTranslateColumns($columns)
    {
        $returnValue = [];
        foreach ($columns as $columnName) {
            if (!in_array($columnName, static::getTechnialColumnsList())) {
                $returnValue[$columnName] = $columnName;
            };
        }
        return $returnValue;
    }

    protected function getTranslateRowColumns($row)
    {
        foreach (static::getTechnialColumnsList() as $columnName) {
            unset($row[$columnName]);
        }
        foreach ($row as $name => $value) {
            if (strpos($name, 'orig_') === 0) {
                unset($row[$name]);
            }
        }
        return $row;
    }

    public function getI18nFields($tableName)
    {
        if (!$tableName) {
            return [];
        };

        $connection = Yii::$app->db;//get connection
        $dbSchema   = $connection->schema;
        //or $connection->getSchema();
        $table   = $dbSchema->getTableSchema($tableName);//returns array of tbl schema's
        $columns = $table->getColumnNames();
        return $this->getTranslateColumns($columns);
    }

    protected function addFilteredElement(&$returnValue, I18nTableElement $i18nElement, I18nTableElementSearch $searchCriteria)
    {
        $id = $i18nElement->getId();
        if (array_key_exists($id, $returnValue)) {
            return;
        }
        //Simple search, may search with linguistic analysis
        if ($searchCriteria->fieldValueOrig) {
            if (mb_stripos($i18nElement->getValueOrig(), $searchCriteria->fieldValueOrig) === false) {
                return;
            }
        }
        if ($searchCriteria->fieldValue) {
            if (mb_stripos($i18nElement->fieldValue, $searchCriteria->fieldValue) === false) {
                return;
            }
        }
        if ($searchCriteria->emptyFieldValue === I18nTableElementSearch::EMPTY_FIELD_VALUE) {
            if ($i18nElement->fieldValue) {
                return;
            }
        }
        $returnValue[$id] = $i18nElement;
    }

    protected function updateElementUpdatedAt(&$returnValue, $lang_iso, $tableName, $columnName, $modelId, $updatedAt, $translatedBy)
    {
        if (strpos($tableName, '_intl')) {
            $tableName = substr($tableName, 0, -5);
        }
        $id = I18nTableElement::getIdByParams($lang_iso, $tableName, $columnName, $modelId);
        if (array_key_exists($id, $returnValue)) {
            /** @var I18nTableElement $i18nTableElement */
            $i18nTableElement               = $returnValue[$id];
            $i18nTableElement->updatedAt    = $updatedAt;
            $i18nTableElement->translatedBy = $translatedBy;
        }
    }

    /**
     * @param I18nTableElement|I18nTableElementSearch $element
     * @param bool $ignoreTranslator
     * @return array
     */
    protected function buildSqlCondition($element, $ignoreTranslator = false)
    {
        $sqlCondition = [
            'and'
        ];
        if ($element->isActive !== null) {
            $sqlCondition[] = ['is_active' => (int)$element->isActive];
        }
        if ($element->modelId !== null) {
            $sqlCondition[] = ['model_id' => $element->modelId];
        }
        if ($element->langIso !== null) {
            $sqlCondition[] = ['lang_iso' => $element->langIso];
        }
        if (!$ignoreTranslator) {
            if ($element->updatedAt !== null) {
                $sqlCondition[] = ['translation_db.updated_at' => $element->updatedAt];
            }
            if (property_exists($element, 'dateFrom') && $element->dateFrom !== null) {
                $sqlCondition[] = ['>', 'translation_db.updated_at', date('Y-m-d 00:00:00', strtotime($element->dateFrom))];
            }
            if (property_exists($element, 'dateFrom') && $element->dateTo !== null) {
                $sqlCondition[] = ['<', 'translation_db.updated_at', date('Y-m-d 24:59:59', strtotime($element->dateTo))];
            }

            if ($element->translatedBy !== null) {
                if (is_numeric($element->translatedBy)) {
                    $sqlCondition[] = [
                        'translated_by' => $element->translatedBy
                    ];
                } else {
                    $sqlCondition[] = [
                        'like', 'user.username', $element->translatedBy
                    ];
                }
            }
        }

        return $sqlCondition;
    }

    protected function buildSqlDefaults($tableName)
    {
        $tableFields = $this->getI18nFields($tableName);
        return array_fill_keys($tableFields, '');
    }

    protected function elementToSqlAttributes(I18nTableElement $element)
    {
        $returnValue = [];
        if ($element->isActive !== null) {
            $returnValue['is_active'] = (int)$element->isActive;
        }
        if ($element->modelId !== null) {
            $returnValue['model_id'] = $element->modelId;
        }
        if ($element->langIso !== null) {
            $returnValue['lang_iso'] = $element->langIso;
        }
        if ($element->fieldValue !== null && $element->fieldName) {
            $returnValue[$element->fieldName] = $element->fieldValue;
        }
        return $returnValue;
    }

    protected function formEmptyTableRows($tableName)
    {
        $tableNameOrig = substr($tableName, 0, -5);
        $query         = (new Query())->select('id')->from($tableNameOrig);
        $rows          = $query->all();
        if (!$rows) {
            return;
        }

        $usedIds        = ArrayHelper::getColumn($rows, 'id');
        $query          = (new Query())->select(["id", "concat(model_id, '_', lower(lang_iso)) as lid"])->from($tableName);
        $translatedRows = $query->all();
        $usedLids       = ArrayHelper::map($translatedRows, 'lid', 'id');

        $langs = SystemLang::find()->all();
        $langs = ArrayHelper::getColumn($langs, 'iso_code');

        $insertRows = [];
        $rowsNames  = ['model_id', 'lang_iso', 'is_active'];
        foreach ($usedIds as $usedId) {
            foreach ($langs as $lang) {
                $key = $usedId . '_' . strtolower($lang);
                if (!array_key_exists($key, $usedLids)) {
                    $insertRow = [
                        'model_id'  => $usedId,
                        'lang_iso'  => $lang,
                        'is_active' => 1,
                    ];

                    $insertRows[] = $insertRow;
                }
            }
        }
        if ($insertRows) {
            Yii::$app->db->createCommand()->batchInsert($tableName, $rowsNames, $insertRows)->execute();
        }

    }

    protected function formFieldNames($tableName)
    {
        $origName    = substr($tableName, 0, -5);
        $fieldNames  = $this->getI18nFields($tableName);
        $returnValue = '';
        foreach ($fieldNames as $fieldName) {
            $returnValue .= '`' . $origName . '`.`' . $fieldName . '` as `orig_' . $fieldName . '`,';
        }
        $returnValue = substr($returnValue, 0, -1);
        return $returnValue;
    }

    protected function getTableRows($tableName, I18nTableElementSearch $searchCriteria)
    {
        $sqlCondition   = $this->buildSqlCondition($searchCriteria);
        $tableOrig      = substr($tableName, 0, -5);
        $orignFieldName = $this->formFieldNames($tableName);
        $query          = (new Query())->select($tableName . '.*, ' . $orignFieldName . ', translation_db.column_name, translation_db.updated_at, translation_db.translated_by ')->from($tableName)
            ->where($sqlCondition)
            ->leftJoin('translation_db', '`translation_db`.`table_name`="' . $tableName . '" and `translation_db`.`line_id`=`' . $tableName . '`.`id`')
            ->leftJoin('user', 'user.id=translation_db.translated_by')
            ->leftJoin($tableOrig, $tableOrig . '.id=' . $tableName . '.model_id');

        $rows        = $query->all();
        $returnValue = [];
        foreach ($rows as $row) {
            if ($searchCriteria->fieldName !== null) {
                $i18nElement = I18nTableElement::create(
                    $tableName,
                    $row['model_id'],
                    $searchCriteria->fieldName,
                    $row['orig_' . $searchCriteria->fieldName],
                    $row[$searchCriteria->fieldName],
                    $row['lang_iso'],
                    $row['is_active'],
                    $row['updated_at'],
                    $row['translated_by']
                );
                $this->addFilteredElement($returnValue, $i18nElement, $searchCriteria);
                continue;
            }
            $i18nElement = I18nTableElement::create($tableName,
                $row['model_id'],
                null,
                null,
                null,
                $row['lang_iso'],
                $row['is_active']
            );
            $fields      = $this->getTranslateRowColumns($row);
            foreach ($fields as $fieldName => $fieldValue) {
                $i18nElementClone                 = clone $i18nElement;
                $i18nElementClone->fieldName      = $fieldName;
                $i18nElementClone->fieldValue     = $fieldValue;
                $i18nElementClone->fieldValueOrig = $row['orig_' . $fieldName];
                $this->addFilteredElement($returnValue, $i18nElementClone, $searchCriteria);
            }
            if ($row['column_name']) {
                $this->updateElementUpdatedAt($returnValue, $row['lang_iso'], $tableName, $row['column_name'], $row['model_id'], $row['updated_at'], $row['translated_by']);
            }

        }
        return $returnValue;

    }

    public function getI18nRows(I18nTableElementSearch $searchCriteria)
    {
        $tables = $this->getI18nTablesList();

        if ($searchCriteria->tableName) {
            if ($tables[$searchCriteria->tableName]) {
                $tables = [
                    $searchCriteria->tableName => $searchCriteria->tableName
                ];
            }
        }

        $allRows = [];
        foreach ($tables as $table) {
            $rows    = $this->getTableRows($table, $searchCriteria);
            $allRows = array_merge($allRows, $rows);
        }
        uasort(
            $allRows,
            function (I18nTableElement $a, I18nTableElement $b) {
                if ($a->tableName === $b->tableName) {
                    if ($a->modelId === $b->modelId) {
                        if ($a->fieldName === $b->fieldName) {
                            return $a->langIso > $b->langIso;
                        } else {
                            return $a->fieldName > $b->fieldName;
                        }
                    } else {
                        return $a->modelId > $b->modelId;
                    }
                } else {
                    return $a->tableName > $b->tableName;
                }
            }
        );
        return $allRows;
    }


    public function fillElementValue(I18nTableElement $element)
    {
        $sqlCondition = $this->buildSqlCondition($element);
        $row          = (new Query())->select('*')->from($element->tableName)->where($sqlCondition)->one();
        if ($row && array_key_exists($element->fieldName, $row)) {
            $element->fieldValue = $row[$element->fieldName];
            $element->isActive   = $row['is_active'];
            $element->modelId    = $row['model_id'];
        } else {
            $element->fieldValue = null;
        }
    }

    /**
     * @param $langIso
     * @param $tableSrc
     * @param $fieldName
     * @param $modelId
     * @param null $valueOrigHash
     * @return I18nTableElement
     */
    public function getI18nElement($langIso, $tableSrc, $fieldName, $modelId, $valueOrigHash = null)
    {
        $element = I18nTableElement::create($tableSrc . '_intl', $modelId, $fieldName, FALSE, null, $langIso);
        $this->fillElementValue($element);
        return $element;
    }

    public function saveI18nElement(I18nTableElement $element)
    {
        $sqlCondition = $this->buildSqlCondition($element, true);
        unset($sqlCondition['is_active']);
        $newValue = [$element->fieldName => $element->fieldValue];
        if ($element->isActive !== null) {
            $newValue['is_active'] = $element->isActive;
        }
        Yii::$app->db->createCommand()->update($element->tableName, $newValue, $sqlCondition)->execute();
        if ($element->updatedAt !== null) {
            // We need translation line id
            $translationLine = (new Query())->select('*')->from($element->tableName)->where($sqlCondition)->one();
            $translationDb   = TranslationDb::find()->where([
                'table_name'  => $element->tableName,
                'column_name' => $element->fieldName,
                'line_id'     => $translationLine['id']
            ])->one();
            if (!$translationDb) {
                $translationDb              = Yii::createObject(TranslationDb::class);
                $translationDb->table_name  = $element->tableName;
                $translationDb->column_name = $element->fieldName;
                $translationDb->line_id     = $translationLine['id'];
            }
            $translationDb->updated_at    = $element->updatedAt;
            $translationDb->translated_by = $element->translatedBy;
            $translationDb->safeSave();
        }
        $sql           = 'SELECT * FROM `' . $element->tableName . '` WHERE `model_id`=\'' . $element->modelId . '\' AND `lang_iso`=\'' . $element->langIso . '\' AND `is_active`=1';
        $uniqueCacheId = 'intsr:' . md5($sql);
        app('cache')->set($uniqueCacheId, false);
    }

    public function deleteI18nElement(I18nTableElement $element)
    {
        $sqlCondition = $this->buildSqlCondition($element);
        unset($sqlCondition['is_active']);
        Yii::$app->db->createCommand()->delete($element->tableName, $sqlCondition)->execute();
    }

    public function addNewI18nElement(I18nTableElement $element)
    {
        $newValue   = $this->buildSqlDefaults($element->tableName);
        $attributes = $this->elementToSqlAttributes($element);
        $newValue   = array_merge($newValue, $attributes);
        Yii::$app->db->createCommand()->insert($element->tableName, $newValue)->execute();
    }

    /**
     * TODO: CREATE list for parameters with wysiwyg
     */
    public function isWysiwygEditor(I18nTableElement $element)
    {
        $fieldSchema = $element->getFieldSchema();
        if ($fieldSchema->size > 0 && $fieldSchema->size < 300) {
            return false;
        }
        return true;
    }

    public function formEmptyDbTranslations()
    {
        $tables = Yii::$app->getModule('translation')->dbI18n->getI18nTablesList();
        foreach ($tables as $table) {
            $this->formEmptyTableRows($table);
        }
    }


    public function getGridButton($tableName)
    {
        return [
            'translate' => function ($url, $model) use ($tableName) {
                $urlRes = Html::a(
                    '<span class="fa fa-language"></span>',
                    Yii::$app->getModule('translation')->dbI18n->getTranslateElementUrl($tableName, $model->id),
                    [
                        'title'     => 'Translate',
                        'data-pjax' => '0',
                    ]
                );
                return $urlRes;
            }
        ];
    }

    public function getTranslateElementUrl($tableNameSrc, $modelId, $shortForm = 1)
    {
        return '/site/lang-db?i18n[tableName]=' . $tableNameSrc . '_intl&i18n[modelId]=' . $modelId . ($shortForm ? '&shortForm=1' : '');
    }
}