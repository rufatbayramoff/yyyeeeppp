<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 10.06.16
 * Time: 9:53
 */

namespace common\modules\translation\components;

use common\components\ArrayHelper;
use common\components\FileDirHelper;
use common\models\base\SystemLangMessage;
use common\models\SystemLang;
use common\models\SystemLangSource;
use DateTime;
use DateTimeZone;
use yii;
use yii\base\Component;

/**
 * Class Scanner
 *
 * Сканирует php и js файлы на предмет содержания функции _t. Если функция найдена, фиксируем ее перевод в базе.
 *
 * search mask: _t('category of language element', 'language element {replace}', ['replace' => 'String']);
 *
 * @package common\modules\translation\components
 */
class Scanner extends Component
{
    /**
     * @ignore
     */
    CONST REGULAR_EXPRESSION = "|[_:]t\\(\s*'([^']*)'\\,\\s*'([^']*)'|";

    /**
     * @ignore
     */
    CONST REGULAR_EXPRESSION_DOUBLE = '|[_:]t\(\s*"([^"]*)"\\,\s*"([^"]*)"|';

    /**
     * @ignore
     */
    CONST REGULAR_EXPRESSION_MIXED = '|[_:]t\(\s*\'([^\']*)\'\\,\s*"([^"]*)"|';
    // Не ищем вариант, когда в категории двойные кавычки


    /**
     * @var SystemLang[]
     */
    protected $languages = [];

    public $baseDir;

    public $excludeFolders ;

    public function init()
    {
        /** @var SystemLang[] $languages */
        $this->languages = SystemLang::find()->all();
        $this->baseDir = Yii::getAlias('@common/..');
        $this->excludeFolders = [
            $this->baseDir . '/backend',
            $this->baseDir . '/frontend/storage',
            $this->baseDir . '/frontend/web/static',
            $this->baseDir . '/frontend/web/assets',
            $this->baseDir . '/frontend/runtime',
            $this->baseDir . '/console/gii',
            $this->baseDir . '/console/runtime',
            $this->baseDir . '/tools',
            $this->baseDir . '/environments',
            $this->baseDir . '/tests',
            $this->baseDir . '/installer',
            $this->baseDir . '/db',
            $this->baseDir . '/vendor',
            $this->baseDir . '/common/models/base',
            $this->baseDir . '/common/modules/comments',
            $this->baseDir . '/common/modules/translation/components',
        ];
    }

    protected function globRecursive($directory, &$directories)
    {
        foreach (glob($directory, GLOB_ONLYDIR | GLOB_NOSORT) as $folder) {
            if(in_array($folder, $this->excludeFolders)){
                $this->log("[SKIP] " .$folder);
                continue;
            }
            $directories[] = $folder;
            $this->globRecursive("{$folder}/*", $directories);
        }
    }

    protected function findFiles($directory, array $extensions)
    {
        $directories = [];
        $this->globRecursive($directory, $directories);
        $files = [];
        $totalFiles = 0;
        foreach ($directories as $oneDirectory) {
            foreach ($extensions as $extension) {
                foreach (glob("{$oneDirectory}/*.{$extension}") as $file) {
                    $files[$extension][] = $file;
                    $totalFiles++;
                }
            }
        }
        $this->log("[INFO] Scanned files " . $totalFiles);
        return $files;
    }

    protected function getTranslationsFromDataWithRegular($data, $regular)
    {
        $returnValue = [];
        $context = str_replace(['\\\'', '\\"'], ['----ShieldedQuote------', '----ShieldedQuote2------'], $data);
        preg_match_all($regular, $context, $matches);
        if ($matches) {
            foreach ($matches[0] as $lineExpression) {
                preg_match_all($regular, $lineExpression, $matches);
                $category = str_replace(['----ShieldedQuote------', '----ShieldedQuote2------'], ["'", '"'], $matches[1][0]);
                $message = str_replace(['----ShieldedQuote------', '----ShieldedQuote2------'], ["'", '"'], $matches[2][0]);
                if ($category == '" . $this->messageCategory . "') {
                    // Skip technical translations
                    continue;
                }
                if ($category) {
                    //echo $category.':'.$message."\n";
                    $returnValue[$category][$message] = true;
                }
            }
        }
        return $returnValue;
    }

    protected function getCachedFileData($data)
    {
        $tmpFileName = Yii::getAlias('@runtime') . '/translateCache/' . md5($data) . '.json';
        if (!file_exists($tmpFileName)) {
            return null;
        } else {
            return json_decode(file_get_contents($tmpFileName), true);
        }
    }

    protected function setCachedFileData($data, $translateData)
    {
        $dir = Yii::getAlias('@runtime') . '/translateCache/';
        FileDirHelper::createDir($dir);
        $tmpFileName = $dir . md5($data) . '.json';
        file_put_contents($tmpFileName, json_encode($translateData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    }

    protected function getDataTranslations($data)
    {
        $cachedData = $this->getCachedFileData($data);
        if ($cachedData !== null) {
            return $cachedData;
        }
        $translateData = array_merge_recursive(
            $this->getTranslationsFromDataWithRegular($data, self::REGULAR_EXPRESSION),
            $this->getTranslationsFromDataWithRegular($data, self::REGULAR_EXPRESSION_DOUBLE),
            $this->getTranslationsFromDataWithRegular($data, self::REGULAR_EXPRESSION_MIXED)
        );
        if (count($translateData)) {
            $this->setCachedFileData($data, $translateData);
        }
        return $translateData;
    }

    protected function getTranslationsFromFiles()
    {
        $dir = $this->baseDir;
        $filesList = $this->findFiles($dir, ['php', 'js']);
        /*
                $filesList = [
                    'php' => [
                        1978 => '/var/www/treatstock/common/../frontend/widgets/views/geo/google/search_address.php'
                    ]
                ];
        */
        $emptyFilesCacheFileName = Yii::getAlias('@runtime') . '/translateCache/emptyList.json';
        if (file_exists($emptyFilesCacheFileName)) {
            $emptyFiles = json_decode(file_get_contents($emptyFilesCacheFileName), true);
        } else {
            $emptyFiles = [];
        }

        $translations = [];
        foreach ($filesList as $extension => $filesGroup) {
            foreach ($filesGroup as $oneFileName) {
                if ($oneFileName) {
                    $fileSize = filesize($oneFileName);
                    if (array_key_exists($oneFileName, $emptyFiles) && $emptyFiles[$oneFileName] === $fileSize) {
                        // if size not changed, and no translations in file. skip it
                        continue;
                    }

                    $data = file_get_contents($oneFileName);
                    $td = $this->getDataTranslations($data);
                    if (count($td) === 0) {
                        $emptyFiles[$oneFileName] = $fileSize;
                    } else {
                        if (array_key_exists($oneFileName, $emptyFiles)) {
                            unset($emptyFiles[$oneFileName]);
                        }
                    }
                    $translations = array_merge_recursive(
                        $translations,
                        $td
                    );
                }
            }
        }
        $this->log("[INFO] Translations extracted");
        file_put_contents($emptyFilesCacheFileName, json_encode($emptyFiles, JSON_PRETTY_PRINT));
        return $translations;
    }

    protected function formLangMessages(SystemLangSource $langSource)
    {
        if (count($langSource->systemLangMessages) !== count($this->languages)) {
            //echo 'Lang count is different: '.$langSource->id;
            // Not full lang list
            foreach ($this->languages as $lang) {
                $langIsFind = false;
                foreach ($langSource->systemLangMessages as $langSourceMessage) {
                    if ($langSourceMessage->language === $lang->iso_code) {
                        $langIsFind = true;
                    }
                }
                if (!$langIsFind) {
                    // Язык не найден добавляем
                    $langSourceMessage = new SystemLangMessage();
                    $langSourceMessage->id = $langSource->id;
                    $langSourceMessage->language = $lang->iso_code;
                    $langSourceMessage->translation = null;
                    $langSourceMessage->safeSave();
                    // echo 'Saved translate: '.$langSourceMessage->id;
                }
            }
        }
    }

    protected function saveTranslations($translations)
    {
        SystemLangSource::updateAll(['not_found_in_source' => 1]);
        $langSourceIds = [];
        $totalNew = 0;
        $totalUpdated = 0;
        foreach ($translations as $category => $messages) {

            $langSources = ArrayHelper::index(
                SystemLangSource::find()
                    ->with('systemLangMessages')
                    ->where('category = :category', [':category' => $category])->all(),
                function($element){
                    return $element['category'].'||'.$element['message'];
                }
            );
            foreach ($messages as $message => $flag) {
                /** @var SystemLangSource $langSource */
                $langSource = $langSources[$category.'||'.$message]??null;
                if ($langSource) {
                    $langSource->not_found_in_source = 0;
                    $langSourceIds[] = $langSource->id;
                    $totalUpdated++;
                } else {
                    $langSource = new SystemLangSource();
                    $langSource->category = $category;
                    $langSource->message = $message;
                    $langSource->not_found_in_source = 0;
                    $langSource->date = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
                    //echo 'New trnaslate : '.$category.':'.$message."<br>\n";
                    $langSource->safeSave();
                    $totalNew++;
                }
                $this->formLangMessages($langSource);
            }
        }
        $chunks = array_chunk($langSourceIds, 500);
        foreach($chunks as $chunk)
            SystemLangSource::updateAll(['not_found_in_source' => 0], ['id'=>$chunk]);
        $this->log("[INFO] Total new found: " . $totalNew);
        $this->log("[INFO] Total updated: " . $totalUpdated);
    }

    /**
     * Сканирует каталог проекта на предмет содержания функции _t и фиксирует переводы в базе
     */
    public function scanForTranslations()
    {
        $translations = $this->getTranslationsFromFiles();
        $this->log("[INFO] Saving Translations...");
        $this->saveTranslations($translations);
        $this->log("[INFO] Saved");
        //echo print_r($translations);
    }

    public function log($msg)
    {
        if(is_a(Yii::$app,'yii\console\Application'))
            echo $msg . "\n";
    }
}
