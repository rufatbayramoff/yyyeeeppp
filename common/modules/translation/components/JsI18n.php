<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.06.16
 * Time: 10:09
 */

namespace common\modules\translation\components;

use common\components\FileDirHelper;
use common\models\SystemLang;
use common\models\SystemLangMessage;
use common\models\SystemLangSource;
use common\modules\translation\assets\JqueryMd5Asset;
use common\modules\translation\assets\TranslateFunctionTAsset;
use yii;
use yii\base\Component;

/**
 * Class JsI18n
 *
 * This component is used to work with js files and translations into them. Founding the function assets. Connects the possibility of the _t function to js.
 *  * In js, allow front.% and site.%
 *
 * @package common\modules\translation\components
 */
class JsI18n extends Component
{
    /**
     * Generate data for translation for one message
     *
     * @param $translation
     * @param $id
     * @return string
     */
    protected function formOneMessageTranslation($translation, $id)
    {
        $resultMessage = $translation;
        $i18n = \Yii::$app->getModule('translation')->i18n;
        if ($i18n->isTranslateMode()) {
            $resultMessage = $resultMessage . I18n::TRANSLATE_MODE_CHAR . $id . ' ';
        }
        return $resultMessage;
    }

    /**
     * Generate data for Js translation for the specified language
     *
     * @param $langIsoCode
     * @return array
     */
    protected function formAssetsData($langIsoCode)
    {
        $resultTranslationArray = [];
        /** @var SystemLangSource[] $allLangSource */
        $allLangSource = SystemLangSource::find()->where('category like "front.%" or category like "site.%"')->all();
        foreach ($allLangSource as $oneLangSource) {
            $messageTranslation = $oneLangSource->getLangMessage($langIsoCode);
            /** @var SystemLangMessage $oneMessageTranslation */
            if ($oneMessageTranslation = $messageTranslation->one()) {
                $translation = $oneMessageTranslation->translation;
                if ($translation) {
                    $indexString = $oneLangSource->category . '.' . $oneLangSource->message;
                    if (strlen(md5($indexString)) < strlen($indexString)) {
                        $indexString = md5($indexString); // Create short form for long text
                    }
                    $resultTranslationArray[$indexString] = $this->formOneMessageTranslation($translation, $oneMessageTranslation->id);
                }
            }
        }
        return $resultTranslationArray;
    }

    /**
     * Get the name of the file containing the temporary data for the translation
     *
     * @param $langIsoCode
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    protected function getTempFilePath($langIsoCode)
    {
        $dir = Yii::getAlias('@frontend/runtime/translate/');
        FileDirHelper::createDir($dir);
        return $dir . '/lang_' . $langIsoCode . '.js';
    }


    /**
     * Record the transfer data in a temporary file
     *
     * @param $tempFilePath
     * @param $assetsData
     */
    protected function writeAssetsFile($tempFilePath, $assetsData)
    {
        file_put_contents($tempFilePath, 'window.systemLangMessage = ' . json_encode($assetsData, JSON_UNESCAPED_UNICODE) . ";\n");
    }


    protected function registerAssetsTranslateDialogObject()
    {
        $file = __DIR__ . '/../assets/translateDialogObject.js';
        $fileTimestamp = @filemtime($file);
        list(, $webPath) = Yii::$app->assetManager->publish($file);
        Yii::$app->view->registerJsFile($webPath . "?v=$fileTimestamp", ['depends' => JqueryMd5Asset::class]);
    }

    /**
     * Generate and save assets js language translation file
     *
     * @param $langIsoCode
     */
    public function formTempAssetsFile($langIsoCode)
    {
        $assetsData = $this->formAssetsData($langIsoCode);
        $tempFilePath = $this->getTempFilePath($langIsoCode);
        $this->writeAssetsFile($tempFilePath, $assetsData);
    }

    /**
     * Create and save assets js files for all languages
     */
    public function formAllTempAssetsFile()
    {
        $allLangs = SystemLang::find()->all();
        foreach ($allLangs as $oneLang) {
            $this->formTempAssetsFile($oneLang->iso_code);
        }
    }

    /**
     *
     * Form a language file for the current language, if it is not formed and connect it
     * Connect the translation function _t based on the language file
     * In the translator mode, connect the js object TranslateDialogObject
     *
     * @param $langIsoCode
     */
    public function assets($langIsoCode)
    {
        $tempFilePath = $this->getTempFilePath($langIsoCode);
        if (!file_exists($tempFilePath)) {
            $this->formTempAssetsFile($langIsoCode);
        }
        list(, $webPath) = Yii::$app->assetManager->publish($tempFilePath);
        $tempFileTimestamp = @filemtime($tempFilePath);
        Yii::$app->view->registerJsFile($webPath . "?v=$tempFileTimestamp");
        if (\Yii::$app->getModule('translation')->i18n->isTranslateMode()) {
            $this->registerAssetsTranslateDialogObject();
        }
    }
}
