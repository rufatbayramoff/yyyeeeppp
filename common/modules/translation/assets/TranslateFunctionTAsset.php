<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.08.16
 * Time: 14:31
 */

namespace common\modules\translation\assets;

use yii\web\AssetBundle;

class TranslateFunctionTAsset extends AssetBundle
{
    public $sourcePath = __DIR__;

    public $js = [
        'translateFunctionT.js'
    ];

    public $depends = [
        JqueryMd5Asset::class
    ];
}
