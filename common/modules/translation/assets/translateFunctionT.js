/**
 * Created by analitic on 30.06.16.
 */

var wasTranslated = {};

/**
 * Translate message
 *
 * @param {string} category
 * @param {string} message
 * @param {*} [$params]
 * @returns {string}
 */
window._t = function _t(category, message, $params) {
    var translateMessage = message;
    var translateKey = category + '.' + message;
    if ($.md5(translateKey).length < translateKey.length) {
        translateKey = $.md5(translateKey);
    }
    if (window.systemLangMessage && (typeof (window.systemLangMessage[translateKey]) !== 'undefined')) {
        translateMessage = window.systemLangMessage[translateKey];
    }
    if (typeof(siteParams) !== 'undefined' && parseInt(siteParams.config.isFixTranslateMode)) {
        var keyTranslated = category + '_' + message + '_' + document.referrer;
        if (!wasTranslated[keyTranslated]) {
            $.ajax({
                type: 'POST',
                url: '/site/translate-page',
                data: {
                    url: window.location.href,
                    category: category,
                    message: message,
                    referer: document.referrer
                }
            });
            wasTranslated[keyTranslated] = 1;
        }
    }

    if (typeof ($params) !== 'undefined') {
        for (var search in $params) {
            translateMessage = translateMessage.replace('{' + search + '}', $params[search]);
        }
    }
    return translateMessage;
};

_t = window._t;