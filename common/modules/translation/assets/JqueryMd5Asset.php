<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.06.16
 * Time: 17:57
 */

namespace common\modules\translation\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class JqueryMd5Asset extends AssetBundle
{
    public $sourcePath = '@vendor/placemarker/jquery-md5';
    public $js = [
        'jquery.md5.js',
    ];

    public $depends = [
        JqueryAsset::class,
        YiiAsset::class
    ];
}
