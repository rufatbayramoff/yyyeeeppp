/**
 * Created by analitic on 06.06.16.
 */
var TranslateDialogObject = {

    allTranslateTextNodes: {},

    getTranslateDialogId: function () {  // TODO: Вынести в конфиг в php
        return '#translatewin';
    },

    getTranslateDialogUrl: function() {
        return '/site/translate-dialog';
    },


    /**
     * Вывести всплывающее диалоговое окно для перевода одного элемента
     *
     * @param translateElement
     * @returns {boolean}
     */
    showTranslateDialog: function (translateElement) {
        var self = this;
        var messagesIds = translateElement.attr('translate-message-id');

        var elm = $(this);
        var target = self.getTranslateDialogId();
        var modalTitle = 'Translate';

        if ($(target).length != 0) {
            $(target).remove();
        }
        $(TS.generateModal(target, modalTitle)).insertAfter('footer');

        $(target).css('zIndex', 100000);

        $(target).modal('show').find('.modal-body').html("<div class='preloader'>Loading...</div>");
        $.get(
            self.getTranslateDialogUrl(),
            {ids: messagesIds}
        ).always(function (data) {
            if (typeof data == 'object') {
                data = data.responseText
            }
            $(target).modal('show').find('.modal-body').html(data);
        });
    },

    /**
     * Контекстное меню
     * TODO: Выделить в отдельный компонент с подпиской для возможного дальнейшего расширения
     *
     * @param event
     */
    showContextMenu: function (event) {
        var self = this;
        var htmlElement = $(event.currentTarget);
        if (event.ctrlKey) {
            return;
        }
        event.preventDefault();
        event.stopPropagation();
        self.showTranslateDialog(htmlElement);
    },

    /**
     * Получить переводы с формы всплывающего окна
     *
     * @returns {{}}
     */
    getTranslateDialogParams: function () {
        var self = this;
        var returnValue = {};
        $(self.getTranslateDialogId()).find('textarea').each(function (index) {
            obj = this;
            var translateId = parseInt(obj.name.substr(("translation[").length, 10));
            returnValue[translateId] = obj.value;
        });
        return returnValue;
    },

    /**
     * Установить перевод для ноды на странице
     *
     * @param id
     * @param value
     */
    setTranslationTextNodeValue: function (id, value) {
        var self = this;
        var nodeDescription = self.allTranslateTextNodes[id];
        if (nodeDescription.type == 'simple') {
            nodeDescription.node.nodeValue = value;
        }
        if (nodeDescription.type == 'inputValue') {
            (nodeDescription.node).val(value);
        }
        if (nodeDescription.type == 'inputPlaceholder') {
            nodeDescription.node.attr('placeholder', value);
        }
        if (nodeDescription.type == 'title') {
            nodeDescription.node.attr('title', value);
        }
        if (nodeDescription.type == 'textareaPlaceholder') {
            nodeDescription.node.attr('placeholder', value);
        }
    },

    /**
     * Применить список переводов для всей страницы
     *
     * @param translations
     */
    applyTranslations: function (translations) {
        var self = this;
        for (id in translations) {
            var translation = translations[id];
            self.setTranslationTextNodeValue(id, translation + '♺' + id+' ');

        }
    },

    /**
     * Зафиксировать перевод из всплывающего окна
     */
    fixTranslate: function () {
        var self = this;
        var translations = self.getTranslateDialogParams();
        self.applyTranslations(translations);
    },

    /**
     * Сохранить информацию о ноде в внутренней памяти
     *
     * @param id
     * @param node
     * @param type
     */
    saveTranslationTextNode: function (id, node, type) {
        var self = this;
        var nodeDescription = {
            node: node,
            type: type
        };
        self.allTranslateTextNodes[id] = nodeDescription;
    },

    /**
     * Инициализация элемента заданного типа
     *
     * @param node
     * @param nodeValue
     * @param htmlElement
     * @param elementType
     */
    initTextNode: function (node, nodeValue, htmlElement, elementType) {
        var self = this;
        var translateMarkerPosition = nodeValue.indexOf('♺'); // TODO: выделить спецсимвол в отдельную константу загружаемую с back через параметр создаваемого объекта
        if (translateMarkerPosition != -1) {
            var translateMarkers = nodeValue.split('♺');
            translateMarkers.shift(); // Remove first text element
            for (var key in translateMarkers) {
                var translateMarker = translateMarkers[key];
                var messageId = parseInt(translateMarker);
                if (messageId) {
                    self.saveTranslationTextNode(messageId, node, elementType);
                    // console.log(messageId);
                    var idList = [];
                    if (htmlElement.attr('translate-message-id') != undefined) {
                        idList = htmlElement.attr('translate-message-id').split(',');
                    } else {
                        // Аттрибут не установлен, делаем подвязку события
                        htmlElement.contextmenu(function (event) {
                            self.showContextMenu(event);
                        });
                    }
                    if (idList.length > 0) {
                        // console.log('Dublicate translate: '+idList);
                    }
                    if (idList.indexOf(messageId.toString()) == -1) {
                        idList.push(messageId);
                    }
                    htmlElement.attr('translate-message-id', idList.join());
                    //htmlElement.css('background-color','red');
                }
            }
        }
    },

    /**
     * Инициализация простых текстовых элементов
     */
    initSimpleTextNodes: function () {
        var self = this;
        var textNodes = $('body :not(iframe)').contents().filter(function () {
            var obj = this;
            var nodeValue = obj.nodeValue;
            var htmlElement = $(obj.parentElement);
            if (obj.parentElement && obj.parentElement.tagName == 'OPTION') {
                htmlElement = $(obj.parentElement.parentElement);
            }
            if (nodeValue) {
                self.initTextNode(obj, nodeValue, htmlElement, 'simple');
            }
        });
    },

    /**
     * Инициализая value у input элементов
     */
    initInputValues: function () {
        var self = this;
        var textNodes = $('input').each(function (index) {
            var obj = this;
            var nodeValue = obj.value;
            var htmlElement = $(obj);
            if (nodeValue) {
                self.initTextNode(htmlElement, nodeValue, htmlElement, 'inputValue');
            }
            nodeValue = obj.placeholder;
            if (nodeValue) {
                self.initTextNode(htmlElement, nodeValue, htmlElement, 'inputPlaceholder');
            }
        });
    },

    /**
     * Инициализация title у всех элементов
     */
    initTitles: function () {
        var self = this;
        var textNodes = $('[title]').each(function (index) {
            var obj = this;
            var nodeValue = obj.title;
            var htmlElement = $(obj);
            if (nodeValue) {
                self.initTextNode(htmlElement, nodeValue, htmlElement, 'title');
            }
        });
    },

    initTextarea: function () {
        var self = this;
        var textNodes = $('textarea').each(function (index) {
            var obj = this;
            var nodeValue = obj.placeholder;
            var htmlElement = $(obj);
            if (nodeValue) {
                self.initTextNode(htmlElement, nodeValue, htmlElement, 'textareaPlaceholder');
            }
        });
    },

    initTranslate: function () {
        var self = this;
        self.initSimpleTextNodes();
        self.initInputValues();
        self.initTitles();
        self.initTextarea();
    }
};

$(function () {
    // DOM Is ready;
    /*
    window.setTimeout(function(){TranslateDialogObject.initTranslate();}, 1000);
    */
    window.setInterval(function () {
        TranslateDialogObject.initTranslate();
    }, 1000);
});
