<?php

namespace common\modules\translation\helpers;

use common\components\UrlHelper;
use yii\base\BaseObject;

class TranslateAdminUrlHelper extends BaseObject
{
    public static function translated(?string $dateFrom, ?string $dateTo, ?int $userId)
    {
        $url = '/site/lang';
        if ($dateFrom) {
            $url = UrlHelper::addGetParameter($url, 'SystemLangMessageSearch[dateFrom]', $dateFrom);
        }
        if ($dateTo) {
            $url = UrlHelper::addGetParameter($url, 'SystemLangMessageSearch[dateTo]', $dateTo);
        }
        if ($userId) {
            $url = UrlHelper::addGetParameter($url, 'SystemLangMessageSearch[translated_by]', $userId);
        }
        return $url;
    }

    public static function translatedDb(?string $dateFrom, ?string $dateTo, ?int $userId)
    {
        $url = '/site/lang-db';
        if ($dateFrom) {
            $url = UrlHelper::addGetParameter($url, 'i18n[dateFrom]', $dateFrom);
        }
        if ($dateTo) {
            $url = UrlHelper::addGetParameter($url, 'i18n[dateTo]', $dateTo);
        }
        if ($userId) {
            $url = UrlHelper::addGetParameter($url, 'i18n[translatedBy]', $userId);
        }
        return $url;
    }
}