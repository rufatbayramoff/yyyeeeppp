<?php
/**
 * @var common\models\SystemLangSource[] $messagesOrig
 */

use common\models\SystemLangMessage;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$lang = \Yii::$app->language;

$form = ActiveForm::begin(
    [
        'action' => ['site/translate-save'] // TODO: Вынести в конфиг php
    ]
);

foreach ($messagesOrig as $message) {
    /** @var SystemLangMessage $translation */
    $translation = $message->getLangMessage($lang)->one();
    if ($translation && !$translation->is_auto_translate) {
        $translationMessage = $translation->translation;
    } else {
        $translationMessage = '';
    }
    ?>
    Message id:
    <?= $message->id ?>
    <br>
    Category: <?php echo $message->category; ?>
    <br>
    Original text:
    <div class="panel">
        <div class="panel-body">
            <p><?= $message->message; ?>
            </p>
        </div>
    </div>
    <b>Translation:</b><br>
    <?= Html::textarea('translation[' . $message->id . ']', $translationMessage, ['class' => 'form-control']); ?>
    <?php
    if ($message !== end($messagesOrig)) {
        echo '<br><hr>';
    }
}
?>
<br>
<div class="form-group">
    <?= Html::submitButton(' Save ', ['class' => 'btn btn-primary ts-ajax-submit', 'onclick'=>'TranslateDialogObject.fixTranslate()']) ?>
</div>
<?php

ActiveForm::end();
?>
 