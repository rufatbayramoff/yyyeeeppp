<?php

namespace common\modules\translation;

use common\modules\translation\components\DbI18n;
use common\modules\translation\components\I18n;
use common\modules\translation\components\JsI18n;
use common\modules\translation\components\Scanner;
use common\modules\translation\components\Statistics;
use yii\base\Module;


/**
 * Class Module
 *
 * @property Scanner $scanner
 * @property I18n $i18n
 * @property JsI18n $jsI18n
 * @property dbI18n $dbI18n
 * @property Statistics $statistics
 *
 *
 * @package app\modules\support
 */
class TranslationModule extends Module
{
    /**
     *
     */
    public function init()
    {
        $this->setComponents(
            [
                'dbI18n'  => DbI18n::class,
                'jsI18n'  => JsI18n::class,
                'i18n'    => I18n::class,
                'scanner' => Scanner::class,
                'statistics' => Statistics::class
            ]
        );
        parent::init();
    }

    public function getTranslatorUsersList()
    {

    }
}
