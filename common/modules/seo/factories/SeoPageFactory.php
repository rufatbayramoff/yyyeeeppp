<?php
/**
 * User: nabi
 */
namespace common\modules\seo\factories;

use common\models\SeoPage;
use common\models\SeoPageAutofillTemplate;
use lib\message\MessageRenderHelper;

class SeoPageFactory
{

    /**
     * @param SeoPageAutofillTemplate $tpl
     * @param array $tplVars
     * @return SeoPage
     */
    public static function createBySeoTemplate(SeoPageAutofillTemplate $tpl, array $tplVars)
    {
        $seoPage = new SeoPage();
        $attrs = ['title', 'meta_keywords', 'meta_description', 'meta_keywords', 'header_text', 'footer_text', 'header'];

        foreach($attrs as $attr){
            if(!empty($tpl->{$attr}))
                $seoPage->{$attr} = MessageRenderHelper::renderTemplate($tpl->{$attr}, $tplVars);
        }
        return $seoPage;
    }
}