<?php

namespace common\modules\seo\services;

use common\models\ProductCategory;
use common\models\Company;
use common\models\CompanyService;
use common\models\CompanyServiceCategory;
use common\models\Model3d;
use common\models\Printer;
use common\models\Product;
use common\models\Ps;
use common\models\SeoPage;
use common\models\SeoPageAutofill;
use common\models\SeoPageAutofillTemplate;
use common\models\SiteTag;
use common\models\SystemLang;
use common\models\WikiMachine;
use common\modules\seo\components\autofill\AbstractAutofill;
use common\modules\seo\components\autofill\CompanyCncAutofill;
use common\modules\seo\components\autofill\CompanyPageAutofill;
use common\modules\seo\components\autofill\CompanyProductsAutofill;
use common\modules\seo\components\autofill\CompanyServiceAutofill;
use common\modules\seo\components\autofill\CompanyServiceCategoryAutofill;
use common\modules\seo\components\autofill\Model3dAutofill;
use common\modules\seo\components\autofill\PrinterAutofill;
use common\modules\seo\components\autofill\PrintServiceAutofill;
use common\modules\seo\components\autofill\PrintServiceCatalogAutofill;
use common\modules\seo\components\autofill\ProductAutofill;
use common\modules\seo\components\autofill\ProductCategoryAutofill;
use common\modules\seo\components\autofill\ProductsCatalogTagAutofill;
use common\modules\seo\components\autofill\WikiMachineAutofill;
use common\modules\seo\components\PlaceholderInterface;
use common\modules\seo\components\SeoPageRepository;
use common\modules\seo\components\Utils;
use common\modules\seo\models\SeoAutofillTemplate;
use common\modules\seo\placeholders\CompanyCncPlaceholder;
use common\modules\seo\placeholders\CompanyPagePlaceholder;
use common\modules\seo\placeholders\CompanyProductsPlaceholder;
use common\modules\seo\placeholders\CompanyServiceCategoryPlaceholder;
use common\modules\seo\placeholders\CompanyServicePlaceholder;
use common\modules\seo\placeholders\Model3dPlaceholder;
use common\modules\seo\placeholders\PrinterPlaceholder;
use common\modules\seo\placeholders\PrintServiceCatalogPlaceholder;
use common\modules\seo\placeholders\PrintServicePlaceholder;
use common\modules\seo\placeholders\ProductCategoryPlaceholder;
use common\modules\seo\placeholders\ProductPlaceholder;
use common\modules\seo\placeholders\ProductsCatalogTagPlaceholder;
use common\modules\seo\placeholders\WikiMachinePlaceholder;
use yii\db\ActiveRecordInterface;

/**
 * Date: 20.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class SeoAutofillService
{

    const TYPE_MODEL3D   = 'model3d';
    const TYPE_CATEGORY  = 'category';
    const TYPE_PS        = 'ps';
    const TYPE_PSCATALOG = 'pscatalog';

    const TYPE_PRODUCT_CATEGORY     = 'product_category';
    const TYPE_PRODUCTS_CATALOG_TAG = 'products_catalog_tag';
    const TYPE_PRODUCT              = 'product';
    const TYPE_PRINTER              = 'printer';
    const TYPE_SERVICE_CATEGORY     = 'company_service_category';
    const TYPE_SERVICE              = 'company_service';
    const TYPE_WIKIMACHINE          = 'wikimachine';
    const TYPE_COMPANYPAGE          = 'company_page';
    const TYPE_COMPANYPRODUCTS      = 'company_products';
    const TYPE_COMPANYCNC           = 'company_cnc';

    public $allowTypes = [
        self::TYPE_PS,
        self::TYPE_PSCATALOG,
        self::TYPE_SERVICE,
        self::TYPE_SERVICE_CATEGORY,
        self::TYPE_PRODUCTS_CATALOG_TAG,
        self::TYPE_COMPANYPAGE,
        self::TYPE_COMPANYPRODUCTS,
        self::TYPE_COMPANYCNC,
    ];

    public $autofillers = [
        [
            'type'            => self::TYPE_MODEL3D,
            'autofill'        => Model3dAutofill::class,
            'placeholder'     => Model3dPlaceholder::class,
            'dataObjectClass' => Model3d::class,
        ],
        [
            'type'            => self::TYPE_PS,
            'autofill'        => PrintServiceAutofill::class,
            'placeholder'     => PrintServicePlaceholder::class,
            'dataObjectClass' => Ps::class,
            'allowAutoFill'   => true,
        ],
        [
            'type'            => self::TYPE_PSCATALOG,
            'autofill'        => PrintServiceCatalogAutofill::class,
            'placeholder'     => PrintServiceCatalogPlaceholder::class,
            'dataObjectClass' => Ps::class,
        ],
        [
            'type'            => self::TYPE_PRODUCT,
            'autofill'        => ProductAutofill::class,
            'placeholder'     => ProductPlaceholder::class,
            'dataObjectClass' => Product::class,
        ],
        [
            'type'            => self::TYPE_PRODUCT_CATEGORY,
            'autofill'        => ProductCategoryAutofill::class,
            'placeholder'     => ProductCategoryPlaceholder::class,
            'dataObjectClass' => ProductCategory::class,
        ],
        [
            'type'            => self::TYPE_SERVICE,
            'autofill'        => CompanyServiceAutofill::class,
            'placeholder'     => CompanyServicePlaceholder::class,
            'dataObjectClass' => CompanyService::class,
            'allowAutoFill'   => true,
        ],
        [
            'type'            => self::TYPE_SERVICE_CATEGORY,
            'autofill'        => CompanyServiceCategoryAutofill::class,
            'placeholder'     => CompanyServiceCategoryPlaceholder::class,
            'dataObjectClass' => CompanyServiceCategory::class,
            'allowAutoFill'   => true,
        ],
        [
            'type'            => self::TYPE_PRINTER,
            'autofill'        => PrinterAutofill::class,
            'placeholder'     => PrinterPlaceholder::class,
            'dataObjectClass' => Printer::class,
        ],
        [
            'type'            => self::TYPE_PRODUCTS_CATALOG_TAG,
            'autofill'        => ProductsCatalogTagAutofill::class,
            'placeholder'     => ProductsCatalogTagPlaceholder::class,
            'dataObjectClass' => SiteTag::class,
            'allowAutoFill'   => true,
        ],
        [
            'type'            => self::TYPE_WIKIMACHINE,
            'autofill'        => WikiMachineAutofill::class,
            'placeholder'     => WikiMachinePlaceholder::class,
            'dataObjectClass' => WikiMachine::class,
        ],
        [
            'type'            => self::TYPE_COMPANYPAGE,
            'autofill'        => CompanyPageAutofill::class,
            'placeholder'     => CompanyPagePlaceholder::class,
            'dataObjectClass' => Company::class,
            'allowAutoFill'   => true,
        ],
        [
            'type'            => self::TYPE_COMPANYPRODUCTS,
            'autofill'        => CompanyProductsAutofill::class,
            'placeholder'     => CompanyProductsPlaceholder::class,
            'dataObjectClass' => Company::class,
            'allowAutoFill'   => true,
        ],
        [
            'type'            => self::TYPE_COMPANYCNC,
            'autofill'        => CompanyCncAutofill::class,
            'placeholder'     => CompanyCncPlaceholder::class,
            'dataObjectClass' => Company::class,
            'allowAutoFill'   => true,
        ],
    ];

    /**
     * @param bool $skipCatalog
     * @return array
     */
    public function autofillAll()
    {
        $result = [];
        $langs  = SystemLang::find()->where(['is_active' => 1])->asArray()->all();
        foreach ($this->autofillers as $providerRel) {
            if (!empty($this->allowTypes) && !in_array($providerRel['type'], $this->allowTypes)) {
                continue;
            }
            $autofiller  = $providerRel['autofill'];
            $placeholder = new $providerRel['placeholder'];
            $dataObject  = new $providerRel['dataObjectClass'];
            // default en
            $tpl = $this->getDefaultTemplateByType($providerRel['type'], 'en-US');
            if (!$tpl) {
                $result[$providerRel['type']] = "No default template for type " . $providerRel['type'];
                continue;
            }
            /** @var AbstractAutofill $provider */
            $provider                     = new $autofiller($placeholder, $dataObject, $tpl);
            $result[$providerRel['type']] = $provider->autofill($langs);
        }
        return $result;
    }

    /**
     *
     * @param $type
     * @param string $langIso
     * @return SeoAutofillTemplate
     */
    private function getDefaultTemplateByType($type, $langIso = 'en-US')
    {
        static $defaultTemplates;
        if (isset($defaultTemplates[$type . $langIso])) {
            return $defaultTemplates[$type . $langIso];
        }
        /** @var SeoPageAutofillTemplate $tplDefault */
        $tplDefault = SeoPageAutofillTemplate::find()->where(['type' => $type, 'is_default' => 1, 'lang_iso' => $langIso])->one();
        if (!$tplDefault) {
            return null;
        }
        $tpl                                = new SeoAutofillTemplate(
            [
                'is_apply_created' => $tplDefault->is_apply_created,
                'template_lang'    => $tplDefault->lang_iso,
                'template_id'      => $tplDefault->id,
                'type'             => $tplDefault->type,
                'title'            => $tplDefault->title,
                'header'           => $tplDefault->header,
                'meta_description' => $tplDefault->meta_description,
                'meta_keywords'    => $tplDefault->meta_keywords,
                'header_text'      => $tplDefault->header_text,
                'footer_text'      => $tplDefault->footer_text,
            ]
        );
        $defaultTemplates[$type . $langIso] = $tpl;
        return $tpl;
    }

    public function getTypes()
    {
        $types = [];
        foreach ($this->autofillers as $filler) {
            $types[] = $filler['type'];
        }
        return $types;
    }


    /**
     * @return array
     */
    public function getPlaceholders()
    {
        $result       = [];
        $placeholders = [];
        foreach ($this->autofillers as $providerClass) {
            $placeholders[] = $providerClass['placeholder'];
        }
        foreach ($placeholders as $placeholderClass) {
            /**
             * @var $placeholder PlaceholderInterface
             */
            $placeholder                = new $placeholderClass();
            $result[$placeholder->type] = $placeholder->getPlaceholders();
        }

        return $result;
    }

    /**
     * @param $type
     * @return mixed|null
     */
    private function getConfigByType($type)
    {
        foreach ($this->autofillers as $filler) {
            if ($filler['type'] === $type) {
                return $filler;
            }
        }
        return null;
    }

    private function getTemplateById($id)
    {
        static $tplTemplates;
        if (isset($tplTemplates[$id])) {
            return $tplTemplates[$id];
        }
        /** @var SeoPageAutofillTemplate $tplDefault */
        $tplDefault = SeoPageAutofillTemplate::findOne($id);
        if (!$tplDefault) {
            return null;
        }
        $tpl               = new SeoAutofillTemplate(
            [
                'is_apply_created' => $tplDefault->is_apply_created,
                'template_lang'    => $tplDefault->lang_iso,
                'template_id'      => $tplDefault->id,
                'type'             => $tplDefault->type,
                'title'            => $tplDefault->title,
                'header'           => $tplDefault->header,
                'meta_description' => $tplDefault->meta_description,
                'meta_keywords'    => $tplDefault->meta_keywords,
            ]
        );
        $tplTemplates[$id] = $tpl;
        return $tpl;
    }

    /**
     * @param SeoPage $seoPage
     * @param SeoPageAutofillTemplate $template
     * @return bool
     */
    public function applyTemplate(SeoPage $seoPage, SeoPageAutofillTemplate $template, $autoSave = true)
    {
        if (empty($seoPage->seoPageAutofill)) {
            throw new \Exception('Template cannot be applied to this seo page, please wait for autofill to generate default template.');
        }
        $providerRel = $this->getConfigByType($template->type);
        $tpl         = $this->getTemplateById($template->id);

        /** @var AbstractAutofill $provider */
        $autofiller = $providerRel['autofill'];

        /** @var PlaceholderInterface $placeholder */
        $placeholder = new $providerRel['placeholder'];

        /** @var ActiveRecordInterface $dataObject */
        $dataObject         = new $providerRel['dataObjectClass'];
        $provider           = new $autofiller($placeholder, $dataObject, $tpl);
        $provider->autosave = $autoSave;

        if ($template->lang_iso != 'en-US') {
            $result = $provider->reautofillPageIntl($seoPage, $template);
        } else {
            $result = $provider->reautofillPage($seoPage, $template);
        }
        return $result;
    }

    public function applyNewTemplate(SeoPage $seoPage, SeoPageAutofillTemplate $template)
    {
        $detectObject = Utils::detectObjectByUrl($seoPage->url);
        if (!$detectObject) {
            return false;
        }
        if ($detectObject['type'] !== $template->type) {
            return false;
        }
        // create default
        $seoPageAutofill = SeoPageAutofill::addRecord(
            [
                'seo_page_id' => $seoPage->id,
                'created_at'  => dbexpr('NOW()'),
                'object_type' => $template->type,
                'object_id'   => $detectObject['id'],
                'template_id' => $template->id
            ]
        );
        $seoPage->populateRelation('seoPageAutofill', $seoPageAutofill);
        $this->applyTemplate($seoPage, $template);
        return $seoPage;
    }
}