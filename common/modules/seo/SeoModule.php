<?php

namespace common\modules\seo;

use common\modules\seo\services\SeoAutofillService;
use yii\base\Module;

/**
 * Date: 14.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class SeoModule extends Module
{
    /**
     *
     */
    public function init()
    {
        parent::init();
    }


    /**
     * @TODO - move to another place?
     * @return array
     */
    public static function getTypesCombo()
    {
        $service = new SeoAutofillService();
        $types = $service->getTypes();
        $types['None'] = 'None';
        return array_combine(array_values($types),array_values($types));
    }
}