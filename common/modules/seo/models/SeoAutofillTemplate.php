<?php
/**
 * Date: 21.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\models;


use yii\base\BaseObject;

class SeoAutofillTemplate extends BaseObject
{
    public $is_apply_created;
    public $template_lang;
    public $template_id;
    public $type;
    public $url;
    public $title;
    public $header;
    public $meta_description;
    public $meta_keywords;
    public $header_text;
    public $footer_text;

    public function attributes()
    {
        return get_object_vars($this);
    }
}