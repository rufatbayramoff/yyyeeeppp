<?php
/**
 * Date: 21.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\models;


class SeoAutofillSettings
{
    const SETTINGS_KEY = 'site.seosettings';

    /**
     * @var array
     */
    protected static $settings = null;

    public function initSettings()
    {
        self::$settings = \Yii::$app->setting->get(self::SETTINGS_KEY);
    }

    /**
     *
     * @param null $type
     * @return array|mixed
     */
    public function getSettings($type = null)
    {
        if (!self::$settings) {
            $this->initSettings();
        }
        $result =  self::$settings;
        if($type){
            foreach(self::$settings as $k=>$v){
                if($v['type']===$type){
                    return $v;
                }
            }
        }
        return $result;
    }
}