<?php

namespace common\modules\seo\components;


use common\components\DateHelper;
use common\models\Model3d;
use common\models\Printer;
use common\models\Product;
use common\models\ProductCategory;
use common\models\SeoPageAutofill;
use common\models\SeoPageAutofillTemplate;
use common\models\SeoPage;
use common\models\WikiMachine;
use common\modules\seo\services\SeoAutofillService;
use lib\message\MessageRenderHelper;
use Yii;
use yii\base\BaseObject;

class SeoPageDetector extends BaseObject
{
    /** @var SeoAutofillService */
    protected $seoAutoService;

    public function injectDependencies(SeoAutofillService $seoAutoService)
    {
        $this->seoAutoService = $seoAutoService;
    }

    protected function detectSeoTemplate($url, $langIso)
    {
        $urlsMap = [
            SeoAutofillService::TYPE_MODEL3D          => '3d-printable-models/',
            SeoAutofillService::TYPE_PRODUCT          => 'product/',
            SeoAutofillService::TYPE_PRODUCT_CATEGORY => 'products/',
            SeoAutofillService::TYPE_PRINTER          => 'machines/item/',
            SeoAutofillService::TYPE_WIKIMACHINE      => 'machines/wiki-item/',
        ];
        $seoType = null;
        foreach ($urlsMap as $type => $urlMask) {
            if (strpos($url, $urlMask) === 0) {
                $seoType = $type;
                break;
            }
        }
        $seoPageTemplate = SeoPageAutofillTemplate::find()
            ->where(['type' => $seoType])
            ->andWhere(['lang_iso' => $langIso])
            ->one();
        return $seoPageTemplate;
    }

    protected function detectObject($url, $type)
    {
        $objectsMatch = [
            SeoAutofillService::TYPE_MODEL3D          => '/3d-printable-models\/(\d+)\-/i',
            SeoAutofillService::TYPE_PRODUCT          => '/product\/(\w+)\-/i',
            SeoAutofillService::TYPE_PRODUCT_CATEGORY => '/products\/([a-z,A-Z,0-9,_,-]+)/i',
            SeoAutofillService::TYPE_PRINTER          => '/machines\/item\/(\d+)\-/i',
            SeoAutofillService::TYPE_WIKIMACHINE      => '/machines\/wiki-item\/([a-z,A-Z,0-9,_,-]+)/i',
        ];

        $urlMask  = $objectsMatch[$type];
        $objectId = null;
        if (preg_match($urlMask, $url, $mathes)) {
            $objectId = $mathes[1];
        }
        if (!$objectId) {
            return null;
        }
        $objectId = strtolower($objectId);
        if ($type === SeoAutofillService::TYPE_MODEL3D) {
            return Model3d::find()->andWhere(['id' => $objectId])->published()->one();
        }
        if ($type === SeoAutofillService::TYPE_PRODUCT) {
            return Product::find()->andWhere(['uuid' => $objectId])->published()->one();
        }
        if ($type === SeoAutofillService::TYPE_PRODUCT_CATEGORY) {
            return ProductCategory::find()->andWhere(['code' => $objectId])->one();
        }
        if ($type === SeoAutofillService::TYPE_PRINTER) {
            return Printer::find()->andWhere(['id' => $objectId])->active()->one();
        }
        if ($type === SeoAutofillService::TYPE_WIKIMACHINE) {
            return WikiMachine::find()->andWhere(['code' => $objectId])->one();
        }
        return null;
    }

    public function detectPage(): ?SeoPage
    {
        $url     = Yii::$app->request->getUrl();
        $langIso = Yii::$app->language;

        if (strlen($url) && $url[0] === '/') {
            $url = substr($url, 1, 1024);
        }

        /** @var SeoPage $seoPage */
        $seoPage = SeoPage::find()->where(['url' => $url, 'is_active' => 1])->one();
        if ($seoPage) {
            return $seoPage;
        }

        $template = $this->detectSeoTemplate($url, $langIso);
        if (!$template) {
            return null;
        }
        $dataObject = $this->detectObject($url, $template->type);
        if (!$dataObject) {
            return null;
        }

        $seoPage                      = new SeoPage();
        $seoPageAutofill              = new SeoPageAutofill();
        $seoPageAutofill->created_at  = DateHelper::now();
        $seoPageAutofill->object_id   = $dataObject->id;
        $seoPageAutofill->object_type = $template->type;
        $seoPageAutofill->template_id = $template->id;
        $seoPageAutofill->dataObject  = $dataObject;
        $seoPage->populateRelation('seoPageAutofill', $seoPageAutofill);

        $applayed = $this->seoAutoService->applyTemplate($seoPage, $template, false);
        if ($applayed) {
            return $seoPage;
        }
        return null;
    }

}