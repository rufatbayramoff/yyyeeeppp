<?php
/**
 * Date: 21.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\components;


use common\components\DateHelper;
use common\models\base\SeoPageAutofillTemplate;
use common\models\SeoPage;
use common\models\SeoPageAutofill;
use common\models\SeoPageIntl;
use yii\helpers\StringHelper;

class SeoPageRepository
{

    /**
     * @param SeoPage $seoPage
     * @param SeoPageAutofill $autofillPage
     * @param int $isActive
     * @return bool
     */
    public static function put(SeoPage $seoPage, SeoPageAutofill $autofillPage)
    {
        try {
            $url = StringHelper::truncate($seoPage->url, 145, '');
            $seoPageOld = SeoPage::find()->where(['url' => $url])->exists();
            if($seoPageOld && !$seoPage->id){
                return false;
            }

            // truncate data
            $seoPage->title =  self::wordTruncate($seoPage->title, 70);
            $seoPage->header = self::wordTruncate($seoPage->header, 145);
            $seoPage->meta_keywords = self::wordTruncate($seoPage->meta_keywords, 150);
            $seoPage->meta_description = self::wordTruncate($seoPage->meta_description, 180);


            if(!$seoPage->id){
                $seoPage->url = $url;
            }
            $seoPage->updated_at = DateHelper::now();
            $seoPage->safeSave();
            $autofillPage->seo_page_id = $seoPage->id;
            $autofillPage->save(false);
            return true;
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
        }
        return false;
    }

    /**
     * @param SeoPageIntl $seoPageIntl
     * @param SeoPage $seoPage
     * @param SeoPageAutofill $autofillPage
     * @return bool
     */
    public static function putIntl(SeoPageIntl $seoPageIntl, SeoPage $seoPage, SeoPageAutofill $autofillPage)
    {
        try {
            // truncate data
            $seoPageIntl->title =  self::wordTruncate($seoPageIntl->title, 100);
            $seoPageIntl->header = self::wordTruncate($seoPageIntl->header, 145);
            $seoPageIntl->meta_keywords = self::wordTruncate($seoPageIntl->meta_keywords, 150);
            $seoPageIntl->meta_description = self::wordTruncate($seoPageIntl->meta_description, 180);

            $seoPage->updated_at = dbexpr('NOW()');
            $seoPage->safeSave();

            $seoPageIntl->safeSave();
            $autofillPage->seo_page_intl_id = $seoPageIntl->id;
            $autofillPage->safeSave();
            return true;
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
        }
        return false;
    }

    public static function wordTruncate($word, $limit)
    {
        if(mb_strpos($word, ' ')===false){
            return mb_substr($word, 0, $limit);
        }
        if(mb_strlen($word) <= $limit){
            return $word;
        }
        return mb_substr($word, 0, mb_strpos(wordwrap($word, $limit), "\n"));
    }
}
