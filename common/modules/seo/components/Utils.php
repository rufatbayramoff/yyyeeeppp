<?php
/**
 * Date: 21.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\components;


use common\components\exceptions\BusinessException;
use common\models\Company;
use common\models\CompanyService;
use common\models\CompanyServiceCategory;
use common\models\Printer;
use common\models\Product;
use common\models\ProductCategory;
use common\models\Ps;
use common\models\SiteTag;
use common\modules\equipments\helpers\WikiMachineUrlHelper;
use common\modules\seo\services\SeoAutofillService;
use frontend\models\model3d\Model3dFacade;
use frontend\models\ps\PsFacade;
use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;
use yii\base\UserException;
use yii\helpers\Url;

class Utils
{
    public static function formatKey($key)
    {
        return '%' . $key . '%';
    }

    /**
     * @param PlaceholderInterface $placeHolder
     * @param $dataObject
     * @return string
     * @throws BusinessException
     */
    public static function getUrl(PlaceholderInterface $placeHolder, $dataObject)
    {
        $type = $placeHolder->getType();
        $result = '';
        switch ($type) {
            case SeoAutofillService::TYPE_MODEL3D:
                $result = Model3dFacade::getStoreUrl($dataObject);
                break;
            case SeoAutofillService::TYPE_CATEGORY:
                $result = Url::toRoute(['product/product/index', 'category' => $dataObject->id, 'slug' => $dataObject->getSlug()]);
                break;
            case SeoAutofillService::TYPE_PS:
                $result = PsFacade::getPsLink($dataObject) . '/3d-printing-service';
                //$result = Url::toRoute(['user/u/printservice', 'id' => $dataObject->id, 'slug'=> Inflector::slug($dataObject->title)]);
                break;
            case 'store':
                // link to model store
                //$result = UserUtils::getUserStoreUrl($user);
                break;
            case SeoAutofillService::TYPE_PRODUCT:
                /** @var  Product $dataObject */
                $result = $dataObject->getPublicPageUrl();
                break;
            case SeoAutofillService::TYPE_PRODUCT_CATEGORY:
                /** @var  ProductCategory $dataObject */
                $result = $dataObject->getViewUrl();
                break;
            case SeoAutofillService::TYPE_SERVICE:
                /** @var  CompanyService $dataObject */
                $result = $dataObject->getPublicPageUrl();
                break;
            case SeoAutofillService::TYPE_SERVICE_CATEGORY:
                /** @var  CompanyServiceCategory $dataObject */
                $result = $dataObject->getPublicUrl();
                break;
            case SeoAutofillService::TYPE_PRINTER:
                /** @var  Printer $dataObject */
                $result = $dataObject->getPublicPageUrl();
                break;
            case SeoAutofillService::TYPE_PRODUCTS_CATALOG_TAG:
                /** @var  SiteTag $dataObject */
                $result = ProductUrlHelper::productsCatalogTag($dataObject);
                break;
            case SeoAutofillService::TYPE_WIKIMACHINE:
                /** @var  SiteTag $dataObject */
                $result = WikiMachineUrlHelper::viewWikiMachine($dataObject);
                break;
            case SeoAutofillService::TYPE_COMPANYPAGE:
                /** @var  Company $dataObject */
                $result = $dataObject->getPublicCompanyLink();
                break;
            case SeoAutofillService::TYPE_COMPANYPRODUCTS:
                /** @var  Company $dataObject */
                $result = $dataObject->getPublicProductsCompanyLink();
                break;
            case SeoAutofillService::TYPE_COMPANYCNC:
                /** @var  Company $dataObject */
                $result = $dataObject->getPublicCncCompanyLink();
                break;
            default;
                throw new BusinessException('Type not defined for getUrl');
                break;
        }
        $result = str_replace(param('server') . '/', "", $result);
        return ltrim($result, '/');
    }

    public static function detectObjectByUrl($url)
    {
        #SELECT * FROM yii2advanced_real.ps where url='hr-print-service';
        $result = [];
        $type = self::getTypeByUrl($url);

        if (!$type) {
            return null;
        }
        $result['type'] = $type;
        switch ($type) {
            case SeoAutofillService::TYPE_PS:
                $psUrl = str_replace('3d-printing-services/', '', $url);
                $ps = Ps::findOne(['url' => $psUrl]);
                if ($ps) {
                    $result['id'] = $ps->id;
                }
                break;
            default;
                $result = null;
                break;
        }
        return $result;
    }

    private static function getTypeByUrl($url)
    {
        $urlPatterns = [
            SeoAutofillService::TYPE_PS       => '3d-printing-services/',
        ];
        foreach ($urlPatterns as $type => $pattern) {
            if (strpos($url, $pattern) !== false) {
                return $type;
            }
        }
    }
}