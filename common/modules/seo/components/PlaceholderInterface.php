<?php

namespace common\modules\seo\components;

use yii\db\ActiveRecordInterface;


/**
 * Date: 20.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
interface PlaceholderInterface
{
    public function setDataObject(ActiveRecordInterface $dataObject);

    public function getPlaceholders();

    public function getFilledPlaceholders();

    public function getType();

    public function clear();

    public function setLang($lang);
}