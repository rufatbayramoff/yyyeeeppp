<?php

namespace common\modules\seo\components\autofill;

use common\models\Model3d;
use common\models\query\PsQuery;
use common\modules\product\interfaces\ProductInterface;
use yii\db\Query;

/**
 * Date: 20.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class Model3dAutofill extends AbstractAutofill
{

    public function getQuery(): Query
    {
        $query = Model3d::find()
            ->published()
            ->activeCompany()
            ->leftJoin('seo_page_autofill', 'seo_page_autofill.object_id=model3d.id AND seo_page_autofill.object_type="model3d"')
            ->andWhere('seo_page_autofill.id is null')
            ->asArray()
            ->limit(10000);
        return $query;
    }
}