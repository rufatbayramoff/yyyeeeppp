<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;


use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class WikiMachineAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = new Query();

        $query->select('wiki_machine.*')
            ->from('wiki_machine')
            ->leftJoin(
                'seo_page_autofill',
                'seo_page_autofill.object_id=wiki_machine.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_WIKIMACHINE . '"'
            )
            ->where(
                [
                    'seo_page_autofill.id' => null,
                ]
            )
            ->groupBy('wiki_machine.id')
            ->limit(10000);

        return $query;
    }
}