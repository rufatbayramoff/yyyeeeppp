<?php
/**
 * Date: 21.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\components\autofill;


use common\components\IntlArTrait;
use common\models\SeoPage;
use common\models\SeoPageAutofill;
use common\models\SeoPageAutofillTemplate;
use common\models\SeoPageIntl;
use common\modules\seo\components\PlaceholderInterface;
use common\modules\seo\components\SeoPageAutofillInterface;
use common\modules\seo\components\SeoPageRepository;
use common\modules\seo\components\Utils;
use common\modules\seo\models\SeoAutofillTemplate;
use lib\collection\CollectionDb;
use lib\message\MessageRenderHelper;
use yii\db\ActiveRecordInterface;

class AbstractAutofill implements SeoPageAutofillInterface
{
    /**
     * @var PlaceholderInterface
     */
    public $placeholder;

    /**
     * @var ActiveQueryInterface
     */
    public $dataObject;

    /**
     * @var SeoAutofillTemplate
     */
    public $templateSettings;

    /**
     * Autosave seopage
     *
     * @var bool
     */
    public $autosave = true;

    public static $seoPages = [];

    public function __construct(PlaceholderInterface $placeholder, ActiveRecordInterface $dataObject, SeoAutofillTemplate $tpl)
    {
        $this->placeholder = $placeholder;
        $this->dataObject = $dataObject;
        $this->templateSettings = $tpl;
        $this->placeholder->setLang($tpl->template_lang);
    }

    /**
     *
     * @param array $langs
     * @return array
     * @throws \yii\base\InvalidParamException
     */
    public function autofill(array $langs = null)
    {
        $query = $this->getQuery();
        $rows = [
            'en-US' => 0
        ];
        $type = $this->placeholder->getType();
        $allItems = $query->all();
        foreach ($allItems as $row) {
            $dataObject = clone $this->dataObject;
            $dataObject::populateRecord($dataObject, $row);
            $dataObject->id = $row['id'];
            $this->placeholder->setDataObject($dataObject);
            $autofillPage = $this->createAutofillPage($dataObject);
            $seoPage = $this->createSeoPage($dataObject);
            $isActive = (int)$this->templateSettings->is_apply_created;
            if ($isActive) {
                $seoPage->need_review = true; // if apply after created, need review flag
            }
            $seoPage->is_active = $isActive;
            $r = SeoPageRepository::put($seoPage, $autofillPage);
            if ($seoPage) {
                $defaultTpl = $this->templateSettings;
                foreach ($langs as $lang) {
                    if ($lang['iso_code'] === 'en-US') {
                        continue;
                    }
                    $tpl = $this->getDefaultTemplateByType($type, $lang['iso_code']);
                    if ($tpl) {
                        $this->templateSettings = $tpl;
                        $pageIntl = new SeoPageIntl();
                        $pageIntl->is_active = $isActive;
                        $pageIntl->lang_iso = $tpl->template_lang;
                        $pageIntl->model_id = $seoPage->id;
                        $autoFillPageIntl = $this->createAutofillPage($dataObject, $tpl);
                        $this->updateSeoPageIntl($pageIntl);
                        $r = SeoPageRepository::putIntl($pageIntl, $seoPage, $autoFillPageIntl);
                        if ($r) {
                            if (!isset($rows[$lang['iso_code']])) {
                                $rows[$lang['iso_code']] = 0;
                            }
                            $rows[$lang['iso_code']]++;
                        }
                    }
                }
                $this->templateSettings = $defaultTpl;
            }
            if ($r) {
                $rows['en-US']++;
            }
            $this->placeholder->clear();
        }
        return $rows;
    }


    /**
     *
     * @param $type
     * @param string $langIso
     * @return SeoAutofillTemplate
     */
    protected function getDefaultTemplateByType($type, $langIso = 'en-US')
    {
        static $defaultTemplates;
        if (isset($defaultTemplates[$type . $langIso])) {
            return $defaultTemplates[$type . $langIso];
        }
        /** @var SeoPageAutofillTemplate $tplDefault */
        $tplDefault = SeoPageAutofillTemplate::find()->where(['type' => $type, 'is_default' => 1, 'lang_iso' => $langIso])->one();
        if (!$tplDefault) {
            return null;
        }
        $tpl = new SeoAutofillTemplate(
            [
                'is_apply_created' => $tplDefault->is_apply_created,
                'template_lang'    => $tplDefault->lang_iso,
                'template_id'      => $tplDefault->id,
                'type'             => $tplDefault->type,
                'title'            => $tplDefault->title,
                'header'           => $tplDefault->header,
                'meta_description' => $tplDefault->meta_description,
                'meta_keywords'    => $tplDefault->meta_keywords,
                'header_text'      => $tplDefault->header_text,
                'footer_text'      => $tplDefault->footer_text
            ]
        );
        $defaultTemplates[$type . $langIso] = $tpl;
        return $tpl;
    }

    public function reautofillPage(SeoPage $page, SeoPageAutofillTemplate $template)
    {
        $autoPage = $page->seoPageAutofill;
        if ($autoPage->dataObject) {
            $dataObject = $autoPage->dataObject;
        } else {
            $dataObject = $this->dataObject->findOne($autoPage->object_id);
            if (!$dataObject) {
                return 0;
            }
        }

        $this->placeholder->setDataObject($dataObject);
        $this->updateSeoPage($page);
        $autoPage->template_id = $template->id;
        $r = true;
        if ($this->autosave) {
            $r = SeoPageRepository::put($page, $autoPage);
        }
        $this->placeholder->clear();
        return $r;
    }

    public function reautofillPageIntl(SeoPage $page, SeoPageAutofillTemplate $template)
    {
        if ($template->lang_iso === 'en-US') {
            return null;
        }
        $autoFillPage = $page->seoPageAutofill;
        $dataObject = $this->dataObject->findOne($autoFillPage->object_id); // we find model3d, ps_printer and etc.
        if (!$dataObject) {
            return 0;
        }

        $this->placeholder->setDataObject($dataObject);

        // get translates
        $pageIntls = CollectionDb::getMap($page->seoPageIntls, 'lang_iso');
        /** @var SeoPageIntl $pageIntl */
        $pageIntl = array_key_exists($template->lang_iso, $pageIntls) ? $pageIntls[$template->lang_iso] : null;

        if ($pageIntl) { // we already have translated page
            $autoFillPageIntl = $pageIntl->seoPageAutofill ?: $this->createAutofillPage($dataObject);
        } else {
            $pageIntl = new SeoPageIntl();
            $pageIntl->lang_iso = $template->lang_iso;
            $pageIntl->model_id = $page->id;
            $autoFillPageIntl = $this->createAutofillPage($dataObject);
        }
        $this->updateSeoPageIntl($pageIntl);
        $autoFillPageIntl->template_id = $template->id;
        $r = true;
        if ($this->autosave) {
            $r = SeoPageRepository::putIntl($pageIntl, $page, $autoFillPageIntl);
        } else {
            $pageIntlAttributes = $pageIntl->getAttributes();
            $pageIntlAttributes = array_intersect_key(
                $pageIntlAttributes,
                array_flip([
                    'lang_iso',
                    'title',
                    'header',
                    'meta_description',
                    'meta_keywords',
                    'header_text',
                    'footer_text'
                ])
            );
            $page->setAttributes($pageIntlAttributes);
        }
        $this->placeholder->clear();
        return $r;

    }

    /**
     * @param $dataObject
     * @param null $tpl
     * @return SeoPageAutofill
     */
    public function createAutofillPage($dataObject, $tpl = null)
    {
        $autofillPage = new SeoPageAutofill();
        $autofillPage->created_at = dbexpr('NOW()');
        $autofillPage->object_id = $dataObject->id;
        $autofillPage->object_type = $this->placeholder->getType();
        $autofillPage->template_id = $tpl ? $tpl->template_id : $this->templateSettings->template_id;
        return $autofillPage;
    }

    /**
     * @param $dataObject
     * @return SeoPage
     * @throws \common\components\exceptions\BusinessException
     */
    public function createSeoPage($dataObject)
    {
        $seoPage = new SeoPage();
        $seoPage->url = Utils::getUrl($this->placeholder, $dataObject);
        return $this->updateSeoPage($seoPage);
    }

    /**
     * @param SeoPage $seoPage
     * @return SeoPage
     */
    public function updateSeoPage(SeoPage $seoPage)
    {
        $seoPage->setAttributes($this->getAutofillAttributes());
        return $seoPage;
    }

    /**
     * @param SeoPageIntl $pageIntl
     * @return SeoPageIntl
     */
    protected function updateSeoPageIntl(SeoPageIntl $pageIntl)
    {
        $pageIntl->setAttributes($this->getAutofillAttributes());
        return $pageIntl;
    }

    /**
     * @return array
     */
    protected function getAutofillAttributes()
    {
        $templateAttributes = $this->templateSettings->attributes();

        $filledData = $this->placeholder->getFilledPlaceholders();
        $attributes = [];
        foreach ($templateAttributes as $k => $v) {
            $attributes[$k] = MessageRenderHelper::renderTemplate($v, $filledData);
        }
        if (empty($attributes['url'])) {
            unset($attributes['url']);
        }

        return $attributes;
    }
}