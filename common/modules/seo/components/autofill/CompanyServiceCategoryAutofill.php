<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;

use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class CompanyServiceCategoryAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = new Query();

        $query->select('company_service_category.*')
            ->from('company_service_category')
            ->leftJoin(
                'seo_page_autofill',
                'seo_page_autofill.object_id=company_service_category.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_SERVICE_CATEGORY . '"'
            )
            ->where(
                [
                    'seo_page_autofill.id'                => null,
                    'company_service_category.is_visible' => 1,
                    'company_service_category.is_active'  => 1,
                ]
            )
            ->limit(10000);

        return $query;
    }
}