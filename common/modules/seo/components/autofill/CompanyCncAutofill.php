<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;


use common\models\Company;
use common\models\Ps;
use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class CompanyCncAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = Company::find()
            ->active()
            ->hasActiveCnc()
            ->leftJoin('seo_page_autofill', 'seo_page_autofill.object_id=ps.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_PS . '"')
            ->andWhere(['seo_page_autofill.id' => null])
            ->select('ps.*')
            ->asArray()
            ->limit(10000)
            ->groupBy('ps.id');

        return $query;
    }
}
