<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;


use common\models\Product;
use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class ProductAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = Product::find()
            ->isAvailableInCatalog()
            ->leftJoin(
                'seo_page_autofill',
                'seo_page_autofill.object_id=product.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_PRODUCT . '"'
            )
            ->andWhere(               [                    'seo_page_autofill.id' => null])
            ->limit(10000)
            ->asArray()
            ->select('product.*')
            ->groupBy('product.uuid');

        return $query;
    }
}