<?php

namespace common\modules\seo\components\autofill;

use common\models\CompanyService;
use common\models\PrinterMaterial;
use common\models\PrinterTechnology;
use common\models\SeoPage;
use common\models\SeoPageAutofill;
use common\models\SeoPageAutofillTemplate;
use common\models\SeoPageIntl;
use common\modules\catalogPs\components\CatalogPsSeoDetector;
use common\modules\seo\components\SeoPageRepository;
use common\modules\seo\models\SeoAutofillTemplate;
use lib\collection\CollectionDb;
use lib\geo\models\Location;
use PDO;
use Yii;

/**
 * User: nabi
 */
class PrintServiceCatalogAutofill extends AbstractAutofill
{

    public function reautofillPage(SeoPage $page, SeoPageAutofillTemplate $template)
    {
        $autoPage              = $page->seoPageAutofill;
        $autoPage->template_id = $template->id;

        $page->setAttributes($template->getAttributes());
        $r = SeoPageRepository::put($page, $autoPage);
        return $r;
    }

    public function reautofillPageIntl(SeoPage $page, SeoPageAutofillTemplate $template)
    {
        if ($template->lang_iso === 'en-US') {
            return null;
        }
        // get translates
        $pageIntls = CollectionDb::getMap($page->seoPageIntls, 'lang_iso');

        /** @var SeoPageIntl $pageIntl */
        $pageIntl = array_key_exists($template->lang_iso, $pageIntls) ? $pageIntls[$template->lang_iso] : null;

        if ($pageIntl) { // we already have translated page
            $autoFillPageIntl = $pageIntl->seoPageAutofill ?: new SeoPageAutofill();
            $pageIntl->setAttributes($template->getAttributes());
            $pageIntl->lang_iso = $template->lang_iso;
            $pageIntl->model_id = $page->id;
        } else {
            $pageIntl = new SeoPageIntl();
            $pageIntl->setAttributes($template->getAttributes());
            $pageIntl->lang_iso = $template->lang_iso;
            $pageIntl->model_id = $page->id;

            $autoFillPageIntl              = new SeoPageAutofill();
            $autoFillPageIntl->created_at  = dbexpr('NOW()');
            $autoFillPageIntl->object_id   = $page->id;
            $autoFillPageIntl->object_type = $this->placeholder->getType();
            $autoFillPageIntl->template_id = $template->id;
        }

        $autoFillPageIntl->template_id = $template->id;
        $r                             = SeoPageRepository::putIntl($pageIntl, $page, $autoFillPageIntl);
        return $r;

    }

    /**
     * @param array|null $langs
     * @return array
     */
    public function autofill(array $langs = null)
    {

        $rows = [
            'en-US' => 0
        ];

        /* @var $seoService CatalogPsSeoDetector */

        $combos     = $this->getPrintservicesLocationMap();
        $seoService = Yii::createObject(
            [
                'class'       => CatalogPsSeoDetector::class,
                'language'    => 'en-US',
                'emptyRender' => false
            ]
        );
        foreach ($combos as $combo) {
            $seoService->location   = $combo['location'];
            $seoService->material   = $combo['material'];
            $seoService->technology = $combo['technology'];
            $seoService->changeLanguage('en-US')->reinit();

            $seoTpl = $seoService->detectSeoTemplate();

            if (!$seoTpl) {
                continue;
            }

            $this->templateSettings = new SeoAutofillTemplate(['template_id' => $seoTpl->id]);
            $seoPage                = $seoService->getRenderedSeoPage();
            $seoPage->is_active     = false;
            $seoPage->url           = $seoService->getValidUrl();
            if (SeoPage::find()->withoutStaticCache()->where(['url' => $seoPage->url])->exists()) {
                continue;
            }
            $r = SeoPageRepository::put($seoPage, $this->createAutofillPage($seoTpl));

            foreach ($langs as $lang) {
                if ($lang['iso_code'] === 'en-US') {
                    continue;
                }
                $seoService->changeLanguage($lang['iso_code'])->reinit();
                $tpl = $seoService->detectSeoTemplate();
                if ($tpl) {
                    $this->templateSettings = new SeoAutofillTemplate(['template_id' => $tpl->id]);
                    $pageIntl               = new SeoPageIntl();
                    $pageIntl->is_active    = $seoPage->is_active;
                    $pageIntl->lang_iso     = $tpl->lang_iso;
                    $pageIntl->model_id     = $seoPage->id;
                    $autoFillPageIntl       = $this->createAutofillPage($seoPage);
                    $this->updateSeoPageIntl($pageIntl);
                    $r = SeoPageRepository::putIntl($pageIntl, $seoPage, $autoFillPageIntl);
                    if ($r) {
                        if (!isset($rows[$lang['iso_code']])) {
                            $rows[$lang['iso_code']] = 0;
                        }
                        $rows[$lang['iso_code']]++;
                    }
                }
            }
            if ($r) {
                $rows['en-US']++;
            }
        }

        return $rows;
    }

    private function getPrintservicesLocationMap()
    {
        $sql = "select min(g1.iso_code) as country, min(g2.title) as region, min(g3.title) as city, 
            group_concat(pm2.filament_title) as materials, min(u.lat) as lat, min(u.lon) as lon,
            tech.title as technology
         from ps_printer a
        left join company_service psm ON (psm.ps_printer_id=a.id)
        left join ps p ON psm.ps_id = p.id
        left join user_location u ON u.id=psm.location_id
        left join geo_country g1 ON g1.id=u.country_id
        left join geo_region g2 ON g2.id=u.region_id
        left join geo_city g3 ON g3.id=u.city_id
        left join ps_printer_material pm ON pm.ps_printer_id=a.id AND pm.is_active=1
        left join printer_material pm2 ON (pm2.id=pm.material_id)
        left join printer_material_group pmg ON (pmg.id=pm2.group_id)
        left join printer ON printer.id=a.printer_id
        left join printer_technology tech ON (tech.id=printer.technology_id)
        where psm.moderator_status in ('" . implode("', '", CompanyService::moderatedStatusList()) . "') 
        group by a.id";
        $rows   = Yii::$app->db->pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $result = [];

        foreach ($rows as $row) {
            $location = Yii::createObject(
                [
                    'class'   => Location::class,
                    'city'    => $row['city'],
                    'region'  => $row['region'],
                    'country' => $row['country'],
                    'lat'     => $row['lat'],
                    'lon'     => $row['lon']
                ]
            );
            /*
             * we'll use what PS inputed.
             *
            if($location->country=='US'){ // convert states to NY, CA and etc.
                $location->region = LocationUtils::findUsaState($location->region, true);
            } */
            $locationStr = (string)$location;
            $materials   = explode(",", $row['materials']);
            $technology  = PrinterTechnology::getCodeByTitle($row['technology']);

            if (empty($result[$locationStr]))
                $result[$locationStr] = ['material' => [], 'technology' => [], 'location' => $location];

            foreach ($materials as $material) {
                $material                                    = PrinterMaterial::slugifyFilament($material);
                $result[$locationStr]['material'][$material] = $material;
            }
            $result[$locationStr]['technology'][$technology] = $technology;
        }

        $finalResult = [];
        foreach ($result as $locationStr => $data) {
            $finalResult[] = ['material' => null, 'technology' => null, 'location' => $data['location']];
            foreach ($data['material'] as $material) {
                $finalResult[] = ['material' => $material, 'technology' => null, 'location' => $data['location']];
            }
            foreach ($data['technology'] as $technology) {
                $finalResult[] = ['material' => null, 'technology' => $technology, 'location' => $data['location']];
            }
        }
        return $finalResult;
    }
}