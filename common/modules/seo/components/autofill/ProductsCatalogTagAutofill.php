<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;


use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class ProductsCatalogTagAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = new Query();

        $query->select('site_tag.*')
            ->from('site_tag')
            ->leftJoin(
                'seo_page_autofill',
                'seo_page_autofill.object_id=site_tag.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_PRODUCTS_CATALOG_TAG . '"'
            )
            ->where(
                [
                    'seo_page_autofill.id' => null,
                ]
            )
            ->groupBy('site_tag.id')
            ->limit(10000);

        return $query;
    }
}