<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;


use common\models\Company;
use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class CompanyPageAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = Company::find()
            ->active()
            ->leftJoin(
                'seo_page_autofill',
                'seo_page_autofill.object_id=ps.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_COMPANYPAGE . '"'
            )
            ->andWhere(
                [
                    'seo_page_autofill.id'        => null,
                ]
            )
            ->groupBy('ps.id');

        return $query;
    }
}
