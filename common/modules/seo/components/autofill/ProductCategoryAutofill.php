<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;


use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class ProductCategoryAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = new Query();

        $query->select('product_category.*')
            ->from('product_category')
            ->leftJoin(
                'seo_page_autofill',
                'seo_page_autofill.object_id=product_category.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_PRODUCT_CATEGORY . '"'
            )
            ->where(
                [
                    'seo_page_autofill.id'        => null,
                    'product_category.is_active'  => 1,
                    'product_category.is_visible' => 1,
                ]
            )
            ->groupBy('product_category.id')
            ->limit(10000);

        return $query;
    }
}