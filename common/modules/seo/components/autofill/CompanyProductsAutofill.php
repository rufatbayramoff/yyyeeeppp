<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;


use common\models\Company;
use common\models\Product;
use common\models\ProductCommon;
use common\modules\product\interfaces\ProductInterface;
use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class CompanyProductsAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = new Query();

        $query->select('ps.*')
            ->from('ps')
            ->leftJoin(
                'seo_page_autofill',
                'seo_page_autofill.object_id=ps.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_COMPANYPRODUCTS . '"'
            )
            ->leftJoin(
                'product_common',
                'product_common.company_id = ps.id'
            )
            ->leftJoin(
                'product',
                'product.product_common_uid = product_common.uid'
            )
            ->where(
                [
                    'seo_page_autofill.id'          => null,
                    'product_common.is_active'      => 1,
                    'product_common.product_status' => ProductInterface::PUBLIC_STATUSES,
                    'ps.moderator_status'           => [Company::MSTATUS_CHECKED, Company::MSTATUS_UPDATED],
                    'ps.is_deleted'                 => Company::IS_NOT_DELETED_FLAG
                ]
            )->andWhere(
                'product.uuid is not null',
            )
            ->groupBy('ps.id')
            ->limit(10000);
        return $query;
    }
}
