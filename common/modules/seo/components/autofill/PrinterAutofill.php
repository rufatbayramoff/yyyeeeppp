<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;


use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class PrinterAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = new Query();

        $query->select('printer.*')
            ->from('printer')
            ->leftJoin(
                'seo_page_autofill',
                'seo_page_autofill.object_id=printer.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_PRINTER . '"'
            )
            ->where(
                [
                    'seo_page_autofill.id' => null,
                    'printer.is_active' => 1,
                ]
            )
            ->limit(10000);
        return $query;
    }
}