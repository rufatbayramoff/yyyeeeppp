<?php
/**
 * User: nabi
 */

namespace common\modules\seo\components\autofill;


use common\models\CompanyService;
use common\modules\seo\services\SeoAutofillService;
use yii\db\Query;

class CompanyServiceAutofill extends AbstractAutofill
{
    public function getQuery(): Query
    {
        $query = CompanyService::find()
            ->inCatalog()
            ->activeCompany()
            ->byType( CompanyService::TYPE_SERVICE)
            ->leftJoin(
                'seo_page_autofill',
                'seo_page_autofill.object_id=company_service.id AND seo_page_autofill.object_type="' . SeoAutofillService::TYPE_SERVICE . '"'
            )
            ->andWhere([
                'seo_page_autofill.id'       => null,
            ])
            ->limit(10000)
            ->asArray()
            ->select('company_service.*')
            ->groupBy('company_service.id');

        return $query;
    }
}