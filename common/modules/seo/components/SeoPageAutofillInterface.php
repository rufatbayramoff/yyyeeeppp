<?php
/**
 * Date: 20.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\components;


interface SeoPageAutofillInterface
{
    public function autofill(array $langs = null);
}