<?php
/**
 * Date: 20.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use common\models\Model3d;
use common\modules\seo\services\SeoAutofillService;
use frontend\models\user\UserFacade;

class Model3dPlaceholder extends AbstractPlaceholder
{
    public $modelUser;
    public $modelTitle;
    public $modelCategory;
    public $modelKeywords;
    public $modelDescription;
    public $modelPrice;

    /**
     * @var Model3d
     */
    protected $dataObject;

    /**
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if(empty($this->modelUser)){
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    private function fillPlaceholders()
    {
        $this->modelUser = UserFacade::getFormattedUserName($this->dataObject->user);
        $this->modelTitle = $this->dataObject->title;
        $this->modelCategory = $this->dataObject->productCategory ? $this->dataObject->productCategory->title : '';
        if($this->templateLang && $this->templateLang!='en-US'){
            // try to set translated
            $categoryIntl = $this->dataObject?->productCategory?->getTranslations($this->templateLang);
            if($categoryIntl){
                $this->modelCategory = $categoryIntl->title;
            }
        }
        $this->modelKeywords = implode(', ', $this->getTags());
        $this->modelDescription = strip_tags($this->dataObject->description);
        if($this->dataObject->storeUnit){
            $this->modelPrice = $this->dataObject->price_per_produce;
        }
    }

    /**
     * @return array
     */
    private function getTags()
    {
        $tags = [];
        foreach ($this->dataObject->tags as $tag) {
            $tags[] = $tag->text;
        }
        return $tags;
    }

    public function getType()
    {
        return SeoAutofillService::TYPE_MODEL3D;
    }
}