<?php
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use backend\models\search\ProductSearch;
use common\models\Printer;
use common\models\ProductCategory;
use common\models\SiteTag;
use common\models\WikiMachine;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\repositories\ProductRepositoryCondition;
use common\modules\seo\services\SeoAutofillService;
use Yii;
use yii\helpers\ArrayHelper;

class WikiMachinePlaceholder extends AbstractPlaceholder
{
    public $machinecategory;
    public $title;
    public $description;

    /**
     * @var WikiMachine
     */
    protected $dataObject;

    public function getType()
    {
        return SeoAutofillService::TYPE_WIKIMACHINE;
    }

    /**
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if (empty($this->title)) {
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    /**
     * fill placeholder with data from $dataObject
     */
    private function fillPlaceholders()
    {
        $this->machinecategory = $this->dataObject->equipmentCategory->title;
        $this->title = $this->dataObject->title;
        $this->description = $this->dataObject->description;
    }

    public function setData(array $data)
    {
        foreach($data as $k=>$v){
            $this->$k = $v;
        }
    }
}