<?php
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use common\models\ProductCategory;
use common\modules\seo\services\SeoAutofillService;

class ProductCategoryPlaceholder extends AbstractPlaceholder
{
    public $title;
    public $description;
    public $full_description;
    public $parentCategoryTitle;
    public $parentCategoryDescription;
    /**
     * @var ProductCategory
     */
    protected $dataObject;

    public function getType()
    {
        return SeoAutofillService::TYPE_PRODUCT_CATEGORY;
    }

    /**
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if (empty($this->title)) {
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    /**
     * fill placeholder with data from $dataObject
     */
    private function fillPlaceholders()
    {
        $this->title = $this->dataObject->title;
        $this->description = strip_tags($this->dataObject->description);
        $this->full_description = strip_tags($this->dataObject->full_description);

        if ($this->dataObject->parent && $this->dataObject->parent->parent_id) {
            $this->parentCategoryTitle = $this->dataObject->parent->title;
            $this->parentCategoryDescription = $this->dataObject->parent->description;
        }

    }

    public function setData(array $data)
    {
        foreach($data as $k=>$v){
            $this->$k = $v;
        }
    }
}