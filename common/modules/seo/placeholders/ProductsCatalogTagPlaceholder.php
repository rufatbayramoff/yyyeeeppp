<?php
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use backend\models\search\ProductSearch;
use common\models\Printer;
use common\models\ProductCategory;
use common\models\SiteTag;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\repositories\ProductRepositoryCondition;
use common\modules\seo\services\SeoAutofillService;
use Yii;
use yii\helpers\ArrayHelper;

class ProductsCatalogTagPlaceholder extends AbstractPlaceholder
{
    public $tag;
    public $found;

    /**
     * @var SiteTag
     */
    protected $dataObject;

    public function getType()
    {
        return SeoAutofillService::TYPE_PRODUCTS_CATALOG_TAG;
    }

    /**
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if (empty($this->tag)) {
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    /**
     * fill placeholder with data from $dataObject
     */
    private function fillPlaceholders()
    {
        $this->tag = $this->dataObject->text;
        $productsRepository = Yii::createObject(ProductRepository::class);
        $condition = Yii::createObject(ProductRepositoryCondition::class);
        $condition->onlyForPublicCatalog();
        $condition->tags[] = $this->tag;
        $query =  $productsRepository->getProductsQuery($condition);
        $foundCount = $query->count();
        $this->found = $foundCount;
    }

    public function setData(array $data)
    {
        foreach($data as $k=>$v){
            $this->$k = $v;
        }
    }
}