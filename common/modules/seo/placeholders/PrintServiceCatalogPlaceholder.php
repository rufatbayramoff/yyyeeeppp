<?php
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use common\components\ArrayHelper;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\UserAddress;
use common\modules\seo\services\SeoAutofillService;
use frontend\models\user\UserFacade;
use yii\db\ActiveRecordInterface;

class PrintServiceCatalogPlaceholder extends AbstractPlaceholder
{

    public $location;
    public $city;
    public $region;
    public $country;

    public $found;
    public $psTotal;

    public $technology;
    public $technologyDescription;

    public $usage;
    public $productApplication;
    public $productApplicationDescription;

    public $material;
    public $materialDescription;



    public function getType()
    {
        return SeoAutofillService::TYPE_PSCATALOG;
    }

    public function setData(array $data)
    {
        foreach($data as $k=>$v){
            $this->$k = $v;
        }
    }
}