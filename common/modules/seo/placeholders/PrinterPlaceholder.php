<?php
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use common\models\Printer;
use common\models\ProductCategory;
use common\modules\seo\services\SeoAutofillService;
use yii\helpers\ArrayHelper;

class PrinterPlaceholder extends AbstractPlaceholder
{
    public $title;
    public $firm;
    public $description;
    public $descriptionPlain;
    public $price;
    public $technologyTitle;
    public $technologyDescription;
    public $materialsAll;
    /**
     * @var Printer
     */
    protected $dataObject;

    public function getType()
    {
        return SeoAutofillService::TYPE_PRINTER;
    }

    /**
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if (empty($this->title)) {
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    /**
     * fill placeholder with data from $dataObject
     */
    private function fillPlaceholders()
    {
        $this->title = $this->dataObject->title;
        $this->firm= $this->dataObject->firm;
        $this->description = $this->dataObject->description;
        $this->descriptionPlain = strip_tags($this->dataObject->description);
        $this->price = $this->dataObject->price ? displayAsCurrency($this->dataObject->price, 'USD') : '';
        $this->technologyTitle = $this->dataObject->technology ? $this->dataObject->technology->title : '';
        $this->technologyDescription = $this->dataObject->technology ? $this->dataObject->technology->description : '';

        $this->materialsAll = implode(", ", ArrayHelper::getColumn($this->dataObject->materials, 'title'));

    }

    public function setData(array $data)
    {
        foreach($data as $k=>$v){
            $this->$k = $v;
        }
    }
}