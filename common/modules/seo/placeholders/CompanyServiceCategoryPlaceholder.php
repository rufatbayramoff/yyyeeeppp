<?php
/**
 * User: nabi
 */

namespace common\modules\seo\placeholders;


use common\models\CompanyServiceCategory;
use common\modules\seo\services\SeoAutofillService;

class CompanyServiceCategoryPlaceholder extends AbstractPlaceholder
{
    /**
     * @var CompanyServiceCategory
     */
    protected $dataObject;
    public $title;
    public $description;
    public $descriptionPlain;
    public $parentTitle;

    public function getType()
    {
        return SeoAutofillService::TYPE_SERVICE_CATEGORY;
    }

    public function getFilledPlaceholders()
    {
        if (empty($this->title)) {
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    private function fillPlaceholders()
    {
        $this->title = $this->dataObject->title;
        $this->description = $this->dataObject->description;
        $this->descriptionPlain = strip_tags($this->description);
        if ($this->dataObject->parentCategory) {
            $this->parentTitle = $this->dataObject->parentCategory->title;
        }
    }


    public function setData(array $data)
    {
        foreach($data as $k=>$v){
            $this->$k = $v;
        }
    }
}