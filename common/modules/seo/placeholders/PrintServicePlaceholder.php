<?php
/**
 * Date: 20.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use common\components\ArrayHelper;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\UserAddress;
use common\modules\seo\services\SeoAutofillService;
use frontend\models\user\UserFacade;
use yii\db\ActiveRecordInterface;

class PrintServicePlaceholder extends AbstractPlaceholder
{


    public $psUser;
    public $psTitle;
    public $psDescription;
    public $psMaterials;
    public $psPrinters;
    public $psLocation;
    public $psId;
    /**
     * @var Ps
     */
    protected $dataObject;


    public function getType()
    {
        return SeoAutofillService::TYPE_PS;
    }

    /**
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if(empty($this->psUser)){
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    private function fillPlaceholders()
    {
        $this->psUser = UserFacade::getFormattedUserName($this->dataObject->user);
        $this->psTitle = $this->dataObject->title;
        $this->psId = $this->dataObject->id;
        $this->psDescription = strip_tags($this->dataObject->description);
        if(count($this->dataObject->psPrinters) > 0){
            $this->psMaterials = implode(', ', $this->getMaterials());
            $this->psLocation = $this->psLocation ? : $this->getLocation();
            $this->psPrinters = implode(', ', $this->getPrinterTitles());
        }
    }

    private function getMaterials()
    {
        $materials = [];
        foreach($this->dataObject->psPrinters as $psPrinter)
        {
            /**
             * @var $psPrinter PsPrinter
             */
            $materialsPs = $psPrinter->getMaterials()->all();
            $materialsDisplay = [];
            foreach ($materialsPs as $material) {
                if(!empty($material->material)){
                    $materialsDisplay[$material->material->filament_title] = $material->material->filament_title;
                }
            }

            $materials = ArrayHelper::merge($materialsDisplay, $materials);
        }
        return array_values($materials);
    }

    /**
     * @return array
     */
    private function getPrinterTitles()
    {
        return array_unique(ArrayHelper::getColumn($this->dataObject->psPrinters, 'title'));
    }

    /**
     * @return string
     */
    private function getLocation()
    {
        foreach($this->dataObject->psPrinters as $psPrinter){
            if($psPrinter->companyService->location){
                return UserAddress::formatPrinterAddress($psPrinter);
            }
        }
        return '';
    }

}