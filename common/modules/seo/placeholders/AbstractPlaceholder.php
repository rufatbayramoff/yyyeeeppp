<?php
/**
 * Date: 20.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use common\modules\seo\components\Utils;
use common\modules\seo\components\PlaceholderInterface;
use yii\base\BaseObject;
use yii\db\ActiveRecordInterface;

class AbstractPlaceholder extends BaseObject  implements PlaceholderInterface
{

    /**
     * where to get data from
     *
     * @var ActiveRecordInterface
     */
    protected $dataObject;

    /**
     * lang iso
     * default could be en-US
     * @var string
     */
    protected $templateLang;

    /**
     * @return array
     */
    public function getPlaceholders()
    {
        $vars = (new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
        $keys = [];
        foreach($vars as $var){
            $keys[] = $var->name;
        }

        $keys = array_map(function($k){
            return Utils::formatKey($k);
        }, $keys);
        return $keys;
    }

    /**
     *
     * @return array
     */
    public function getFilledPlaceholders()
    {
        $vars = (new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
        $result = [];
        foreach($vars as $k=>$v){
            $result[$v->name] = $this->{$v->name};
        }
        return $result;
    }

    /**
     * @param ActiveRecordInterface $dataObject
     */
    public function setDataObject(ActiveRecordInterface $dataObject)
    {
        $this->dataObject = $dataObject;
    }

    public function getType()
    {
        // TODO: Implement getType() method.
    }

    public function clear()
    {
        $vars = (new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach($vars as $key){
            $this->{$key->name} = null;
        }
    }

    public function setLang($lang)
    {
        $this->templateLang = $lang;
    }
}