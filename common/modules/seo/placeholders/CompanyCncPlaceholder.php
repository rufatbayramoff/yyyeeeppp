<?php
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use backend\models\search\ProductSearch;
use common\models\Company;
use common\models\Printer;
use common\models\ProductCategory;
use common\models\SiteTag;
use common\models\UserLocation;
use common\models\WikiMachine;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\repositories\ProductRepositoryCondition;
use common\modules\seo\services\SeoAutofillService;
use Yii;
use yii\helpers\ArrayHelper;

class CompanyCncPlaceholder extends AbstractPlaceholder
{
    public $companyname;
    public $pslocation;
    public $materials;
    public $machines;

    /**
     * @var Company
     */
    protected $dataObject;

    public function getType()
    {
        return SeoAutofillService::TYPE_COMPANYCNC;
    }

    /**
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if (empty($this->title)) {
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    /**
     * fill placeholder with data from $dataObject
     */
    private function fillPlaceholders()
    {
        $this->companyname = $this->dataObject->title;
        $this->pslocation  = $this->dataObject->calculatedLocation ? UserLocation::formatLocation($this->dataObject->calculatedLocation, "%country% %region% %city%") : '';
        $cncMachines       = $this->dataObject->psCncMachines;
        $activeMachines    = [];
        $materials         = [];
        foreach ($cncMachines as $cncMachine) {
            $description = $cncMachine->getPsCncDescription();
            if ($description) {
                foreach ($cncMachine->psCnc->materials as $material) {
                    $materials[] = $material->title;
                }
                foreach ($description->cuttingMachines as $cuttingMachine) {
                    $activeMachines[] = $cuttingMachine->title;
                }
                foreach ($description->millingMachines as $millingMachine) {
                    $activeMachines[] = $millingMachine->title;
                }
            }
        }

        $this->materials = implode(', ', $materials);
        $this->machines  = implode(', ', $activeMachines);
    }

    public function setData(array $data)
    {
        foreach ($data as $k => $v) {
            $this->$k = $v;
        }
    }
}