<?php
/**
 * User: nabi
 */

namespace common\modules\seo\placeholders;


use common\models\CompanyService;
use common\modules\seo\services\SeoAutofillService;
use yii\web\Request;

class CompanyServicePlaceholder extends AbstractPlaceholder
{
    /**
     * @var CompanyService
     */
    protected $dataObject;
    public $title;
    public $companyTitle;
    public $descriptionPlain;
    public $companyTags;
    public $priceFrom;
    public $serviceCategoryTitle;

    public function getType()
    {
        return SeoAutofillService::TYPE_SERVICE;
    }

    /**
     * get filled placeholder data
     *
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if (empty($this->title)) {
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    /**
     * fill placeholder data
     */
    private function fillPlaceholders()
    {
        $this->title = $this->dataObject->title;
        $this->companyTitle = $this->dataObject->company->title;
        $this->descriptionPlain = strip_tags($this->dataObject->description);
        $tags = [];
        foreach ($this->dataObject->tags as $tag) {
            $tags[] = $tag->text;
        }
        $this->companyTags = implode(", ", $tags);
        $this->priceFrom = displayAsCurrency($this->dataObject->single_price, 'USD');
        if ($this->dataObject->category) {
            $this->serviceCategoryTitle = $this->dataObject->category->title;
        }
    }


    public function setData(array $data)
    {
        foreach($data as $k=>$v){
            $this->$k = $v;
        }
    }
}