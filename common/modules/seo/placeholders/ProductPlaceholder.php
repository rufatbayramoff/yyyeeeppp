<?php
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use common\components\ArrayHelper;
use common\models\Product;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\UserAddress;
use common\modules\seo\services\SeoAutofillService;
use frontend\models\user\UserFacade;
use yii\db\ActiveRecordInterface;

class ProductPlaceholder extends AbstractPlaceholder
{

    public $companyTitle;
    public $productTitle;
    public $description;
    public $categoryTitle;
    public $parentCategoryTitle;
    public $shipFrom;
    public $priceFrom;
    public $productTags;
    public $specifications;
    /**
     * @var Product
     */
    protected $dataObject;


    public function getType()
    {
        return SeoAutofillService::TYPE_PRODUCT;
    }

    /**
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if (empty($this->companyTitle)) {
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    private function fillPlaceholders()
    {
        $this->companyTitle = $this->dataObject->company->title;
        $this->productTitle = $this->dataObject->title;
        $this->description = strip_tags($this->dataObject->description);

        $specs = [];
        foreach ($this->dataObject->getCustomProperties() as $k => $v) {
            $specs[] = "$k : $v";
        }
        $this->specifications = implode(", ", $specs);
        $categoryTitle = '';
        if ($this->dataObject->category) {
            $categoryTitle = $this->dataObject->category->title;
            if ($this->dataObject->category->parent) {
                $this->parentCategoryTitle = $this->dataObject->category->parent->title;
            }
        }
        $this->categoryTitle = $categoryTitle;

        $this->shipFrom = $this->dataObject->getShipFromLocation(true)->formatted_address;
        $priceFrom = $this->dataObject->getPriceMoneyByQty(1);
        $this->priceFrom = $priceFrom?displayAsMoney($this->dataObject->getPriceMoneyByQty(1)):null;
        $tags = [];
        foreach ($this->dataObject->siteTags as $tag) {
            $tags[] = $tag->text;
        }
        $this->productTags = implode(", ", $tags);
    }


    public function setData(array $data)
    {
        foreach($data as $k=>$v){
            $this->$k = $v;
        }
    }
}