<?php
/**
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\seo\placeholders;


use backend\models\search\ProductSearch;
use common\models\Company;
use common\models\Printer;
use common\models\ProductCategory;
use common\models\SiteTag;
use common\models\UserLocation;
use common\models\WikiMachine;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\repositories\ProductRepositoryCondition;
use common\modules\seo\services\SeoAutofillService;
use Yii;
use yii\helpers\ArrayHelper;

class CompanyProductsPlaceholder extends AbstractPlaceholder
{
    public $companyname;
    public $pslocation;
    public $productscount;
    public $productslist;

    /**
     * @var Company
     */
    protected $dataObject;

    public function getType()
    {
        return SeoAutofillService::TYPE_COMPANYPRODUCTS;
    }

    /**
     * @return array
     */
    public function getFilledPlaceholders()
    {
        if (empty($this->companyname)) {
            $this->fillPlaceholders();
        }
        return parent::getFilledPlaceholders();
    }

    /**
     * fill placeholder with data from $dataObject
     */
    private function fillPlaceholders()
    {
        $this->companyname  = $this->dataObject->title;
        $this->pslocation = $this->dataObject->calculatedLocation ? UserLocation::formatLocation($this->dataObject->calculatedLocation, "%country% %region% %city%") : '';
        $products = $this->dataObject->getProducts()->isAvailableInCatalog()->all();
        $this->productscount = count($products);
        $this->productslist  = implode(', ', ArrayHelper::getColumn($products, 'title'));
    }

    public function setData(array $data)
    {
        foreach ($data as $k => $v) {
            $this->$k = $v;
        }
    }
}