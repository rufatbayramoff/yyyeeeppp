<?php
namespace common\modules\seo\widgets;


use yii\base\Widget;

class LinletManufactureShow extends Widget
{
    public function run()
    {
        if (\Yii::$app->session->get('dontLinletManufactureShow')) {
            return;
        }
        \Yii::$app->session->set('dontLinletManufactureShow', true);
        return $this->render('linletManufactureShow');
    }
}
