<?php
?>
<script>
    setTimeout(function () {
        $(".popup-manuf-show").modal();
    }, 5000);
</script>
<div class="modal fade popup-manuf-show" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    style="position:absolute;top: 0;right: 0;padding: 10px 15px;color: #ffffff;opacity: 1;text-shadow: 0 1px 0 #000;">
                <span aria-hidden="true">×</span>
            </button>
            <a href="https://linlet.com/b/treatstock-inc/events/15-manufacturing-show-2021" target="_blank">
                <img src="https://static.treatstock.com/static/images/main-page-temp/event-cover-CNC-TS-small.jpg"
                     style="display: block;width: 100%;margin: 0 auto;border-radius: 5px 5px 0 0;">
            </a>
            <div class="modal-body">

                <h3 class="m-t0 m-b0">
                    <a href="https://linlet.com/b/treatstock-inc/events/15-manufacturing-show-2021" target="_blank">
                        <?= _t('site.ps', 'Online Manufacturing Show 2021 ') ?>
                    </a>
                </h3>

                <p class="text-muted">
                    <i class="tsi tsi-calendar m-r10"></i><?= _t('front', 'February 1-6'); ?>
                </p>

                <p>
                    <?= _t('front', 'Broaden your partner network, gain knowledge, share the experience with your peers, find new customers, and transform technologies that impact our lives. Let’s grow the community together!'); ?>
                </p>

                <a href="https://linlet.com/b/treatstock-inc/events/15-manufacturing-show-2021" class="btn btn-block btn-primary p-l0 p-r0" target="_blank"><?= _t('front', 'Learn more'); ?></a>

            </div>
        </div>
    </div>
</div>