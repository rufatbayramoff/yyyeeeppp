<?php


namespace common\modules\company\helpers;


use common\models\CompanyService;
use Yii;
use yii\helpers\Inflector;
use yii\helpers\Url;

class CompanyServiceUrlHelper
{
    public static function add(): string
    {
        return '/mybusiness/company-services/company-service/add';
    }

    /**
     * @param string $type
     * @return string
     */
    public static function addWithType(string $type): string
    {
        return Url::to([self::add(),'type' => $type]);
    }

    public static function getPublicUrl(CompanyService $companyService)
    {
        return Yii::$app->params['siteUrl'] . '/c/' .$companyService->ps->url.'/service/'.$companyService->id.'-'.Inflector::slug($companyService->getTitleLabel());
    }
}