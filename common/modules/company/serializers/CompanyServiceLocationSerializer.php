<?php


namespace common\modules\company\serializers;


use common\components\serizaliators\AbstractProperties;
use common\models\Company;

class CompanyServiceLocationSerializer extends AbstractProperties
{

    public function getProperties(): array
    {
        return [
            Company::class => [
                'locations' => function (Company $company) {
                    $locations = [];
                    $machines = $company->notDeletedPsMachines;
                    foreach($machines as $machine) {
                        if($machine->location) {
                            $locations[] = $machine->location;
                        }
                    }
                    return array_map(function ($location){
                        return [
                            'position' => ['lat' => $location->lat, 'lng' =>$location->lon],
                            'title' => $location->address
                        ];
                    }, $locations);
                }
            ]
        ];
    }
}