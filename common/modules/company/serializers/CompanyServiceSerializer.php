<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.08.18
 * Time: 11:37
 */

namespace common\modules\company\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\CompanyService;
use function foo\func;

class CompanyServiceSerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CompanyService::class => [
                'id',
                'type',
                'title'       => function (CompanyService $companyService) {
                    return $companyService->getTitleLabel();
                },
                'description' => function (CompanyService $companyService) {
                    return $companyService->getDescription();
                },
                'tags'        => function (CompanyService $companyService) {
                    return $companyService->tagsList();
                },
                'properties'  => static function (CompanyService $companyService) {
                    $rate    = displayAsMoney($companyService->getSinglePriceMoney());
                    $rateTxt = sprintf('%s / %s', $rate, $companyService->getUnitTypeTitle(false));
                    return [
                        _t("site.store", 'Service rate')                 => $rateTxt,
                        _t("mybusiness.product", 'Min. order')           => $companyService->getMinOrderQty() . ' ' . $companyService->getUnitTypeTitle(true),
                        _t("mybusiness.product", 'Ship from')            => $companyService->getShipFromLocation(true)->formatted_address,
                        _t("mybusiness.product", 'Service ability')      => $companyService->supply_ability ? sprintf(
                            '%s %s / %s',
                            $companyService->supply_ability,
                            $companyService->getUnitTypeTitle(true),
                            $companyService->supply_ability_time
                        ) : '',
                        _t("mybusiness.product", 'Avg. production time') => $companyService->avg_production ? sprintf(
                            '%s  %s',
                            $companyService->avg_production,
                            $companyService->avg_production > 1 ? $companyService->avg_production_time . 's' : $companyService->avg_production_time
                        ) : '',
                        _t("mybusiness.product", 'Incoterms')            => $companyService->incoterms,
                        _t("mybusiness.product", 'Payment')              => '',

                    ];
                }
            ],
        ];
    }
}