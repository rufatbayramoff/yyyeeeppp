<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.08.18
 * Time: 11:37
 */

namespace common\modules\company\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\Company;

class CompanySerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            Company::class => [
                'id',
                'title',
                'cashbackPercent' => 'cashback_percent',
                'cashbackPercentPlus'
            ]
        ];
    }
}