<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.04.18
 * Time: 11:28
 */

namespace common\modules\company\serializers;

use common\components\FileTypesHelper;
use common\components\serizaliators\AbstractProperties;
use common\interfaces\FileBaseInterface;
use common\models\CompanyBlock;
use common\models\File;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\models\Product;
use common\models\ProductCertification;
use common\models\ProductDelivery;
use frontend\components\image\ImageHtmlHelper;
use common\models\ProductCategory;

/**
 * Class CompanyBlockSerializer
 * @package common\modules\company\serializers
 */
class CompanyBlockSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        $product = [
            'id',
            'title',
            'is_visible',
            'videos',
            'isNew' => function (CompanyBlock $s) {
                return $s->isNewRecord;
            },
        ];

        return [
            CompanyBlock::class => $product,
        ];
    }
}