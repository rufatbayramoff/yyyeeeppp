<?php
/**
 * User: nabi
 */

namespace common\modules\company;

use yii\base\Module;

class CompanyModule extends Module
{
    public $controllerNamespace = 'common\modules\company\controllers';

}