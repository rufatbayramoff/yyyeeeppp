<?php
/**
 * User: nabi
 */

namespace common\modules\company\factories;

use common\components\DateHelper;
use common\models\CompanyService;
use common\models\CompanyServiceBlock;
use common\models\CompanyServiceCategory;
use common\models\factories\LocationFactory;
use common\models\factories\UserLocationFactory;
use common\models\Product;
use common\models\Ps;
use common\models\UserLocation;
use common\modules\company\components\CompanyServiceInterface;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\models\CompanyServiceAddForm;
use lib\geo\GeoNames;

class CompanyServiceFactory
{
    /**
     * creates unsaved object
     *
     * @param Ps $company
     * @param CompanyServiceAddForm $form
     * @return CompanyService
     */
    public function createService(Ps $company, CompanyServiceAddForm $form)
    {
        $companyService              = $this->createEmptyService($company);
        $companyService->title       = $form->title;
        $companyService->category_id = $form->category_id;
        $companyService->description = $form->description;
        return $companyService;
    }

    /**
     * creates draft object
     *
     * @param Ps $company
     * @return CompanyService
     */
    public function createEmptyService(Ps $company): CompanyService
    {
        $companyService                   = $this->create($company);
        $companyService->moderator_status = CompanyService::MODERATOR_STATUS_NEW;
        return $companyService;
    }

    /**
     * creates draft object
     *
     * @param Ps $company
     * @return CompanyService
     */
    public function createDraftService(Ps $company): CompanyService
    {
        $companyService                   =  $this->create($company);
        $companyService->moderator_status = CompanyService::MODERATOR_STATUS_DRAFT;
        return $companyService;
    }

    /**
     * used for company
     *
     * @param Ps $company
     * @return CompanyServiceBlock
     */
    public function createEmptyBlock(Ps $company)
    {
        $productBlock             = new CompanyServiceBlock();
        $productBlock->title      = '';
        $productBlock->created_at = DateHelper::now();
        return $productBlock;

    }

    /**
     * @param Ps $company
     * @return CompanyService
     */
    protected function create(Ps $company): CompanyService
    {
        $companyService = new CompanyService();
        $companyService->unit_type = CompanyService::UNIT_TYPE_HOUR;
        $companyService->ps_id = $company->id;
        $companyService->type = CompanyServiceInterface::TYPE_SERVICE;
        $companyService->visibility = CompanyService::VISIBILITY_EVERYWHERE; // hidden by default
        $companyService->created_at = DateHelper::now();
        $companyService->updated_at = DateHelper::now();
        $companyService->supply_ability_time = Product::TIME_DAY;
        $companyService->avg_production_time = Product::TIME_DAY;
        $mainCategory = CompanyServiceCategory::find()->where(['depth' => 2])->one();
        if ($mainCategory) {
            $companyService->category_id = $mainCategory->id;
        }
        $incoterms = '';
        if ($company->incoterms) {
            $incoterms = explode(",", $company->incoterms)[0];
        }
        $companyService->incoterms = $incoterms;
        if ($company->location_id) {
            $companyService->location_id = $company->location_id;
        }

        return $companyService;
    }
}