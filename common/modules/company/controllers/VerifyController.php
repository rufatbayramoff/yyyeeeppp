<?php
/**
 * User: nabi
 */

namespace common\modules\company\controllers;


use common\components\BaseController;
use common\components\exceptions\BusinessException;
use common\models\repositories\CompanyRepository;
use common\models\user\UserIdentityProvider;
use common\modules\company\models\CompanyVerifyCodeForm;
use common\modules\company\services\CompanyVerifyService;
use frontend\models\user\FrontUser;
use frontend\modules\preorder\components\PreorderUrlHelper;
use yii\base\Exception;
use yii\web\NotFoundHttpException;

class VerifyController extends BaseController
{

    /**
     * @var CompanyRepository
     */
    public $companyRepo;

    public function injectDependencies(CompanyRepository $companyRepository)
    {
        $this->companyRepo = $companyRepository;
    }

    public function actionIndex($id)
    {
        $company = $this->companyRepo->getById($id);
        if ($company->isVerfied()) {
            $this->setFlashMsg(false, 'Not valid action');
            return $this->redirect('/c/' . $company->url);
        }
        $this->view->noindex();
        return $this->render('verify', ['company' => $company]);
    }

    /**
     * @param $id
     * @param CompanyVerifyService $verifyService
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionVerifyByEmail($id, CompanyVerifyService $verifyService)
    {
        $company = $this->companyRepo->getById($id);
        if (!$company) {
            throw new NotFoundHttpException(_t('company.verify', 'Company not found'));
        }
        if ($verifyService->sendVerifyEmail($company)) {
            $this->setFlashMsg(true, _t('company.verify', 'Please check your email'));
        } else {
            $this->setFlashMsg(true, _t('company.verify', 'Please check your email') .'!');
        }
        return $this->redirect(['index', 'id' => $id]);
    }

    /**
     * show verify accept form
     *
     * @param $id
     * @param $code
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAccept($id, $code, CompanyVerifyService $verifyService)
    {
        $company = $this->companyRepo->getById($id);
        if (!$company->needClaim()) {
            throw new NotFoundHttpException(_t('company.verify', 'Company already verified'));
        }
        $verifyForm = new CompanyVerifyCodeForm();
        $verifyForm->code = $code;
        $verifyForm->companyId = $id;
        if (app('request')->isPost) {
            $verifyForm->load(app('request')->post());
            try {
                $companyVerify = $verifyService->verifiedByForm($verifyForm);
                $frontUser = FrontUser::findIdentity($companyVerify->company->user_id);
                if (!is_guest()) {
                    app('user')->switchIdentity($frontUser);
                } else {
                    \Yii::$app->user->login($frontUser, 0);
                }
                $this->setFlashMsg(true, _t('company.verify', 'Company verified.'));
                return $this->redirect(PreorderUrlHelper::viewPsList());
            } catch (BusinessException $e) {
                $this->setFlashMsg(false, $e->getMessage());
            } catch (Exception $e) {
                \Yii::error($e);
                $this->setFlashMsg(false, 'Server error. Please try again later.');
            }
        }
        return $this->render('verify-accept.php', ['company' => $company, 'verifyForm' => $verifyForm]);
    }
    /**
     * @param $id
     * @param CompanyVerifyService $verifyService
     * @throws NotFoundHttpException
     */
    public function actionEmailVerified($id, $code, CompanyVerifyService $verifyService, UserIdentityProvider $identityProvider)
    {
        $company = $this->companyRepo->getById($id);
        if (!$company) {
            throw new NotFoundHttpException('Company not found');
        }
        $currentUser = $identityProvider->getUser();
        if ($company->user_id != $currentUser->id) {
            throw new NotFoundHttpException('No access.');
        }
        $verifyService->verifiedByEmail($company, $code);
        $this->setFlashMsg(true, _t('company.verify', 'Company verified.'));
        return $this->redirect('/mybusiness/company/edit-ps');
    }

    public function actionVerifyBySms($id)
    {

    }

    public function actionVerifyByPhoneCall($id)
    {

    }
}