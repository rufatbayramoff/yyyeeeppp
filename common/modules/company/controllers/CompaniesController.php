<?php
/**
 * User: nabi
 */
namespace common\modules\company\controllers;

use common\components\BaseController;
use common\models\Ps;
use common\modules\company\models\CompaniesSearchForm;
use yii\data\ActiveDataProvider;

class CompaniesController extends BaseController
{


    public function actionIndex($filter = '')
    {
        $searchForm = new CompaniesSearchForm();
        $filters = $searchForm->parseRequestParams($filter, \Yii::$app->request->getQueryParams());
        $searchForm->load($filters);

        $servicesQuery = Ps::find()->moderated()->findBySearchForm($searchForm);
        $dataProvider = new ActiveDataProvider(
            [
                'query'      => $servicesQuery,
                'pagination' => [
                    'defaultPageSize' => 15
                ],
            ]
        );
        return $this->render('companyList', ['searchForm' => $searchForm, 'dataProvider' => $dataProvider]);
    }
}