<?php
/**
 * User: nabi
 */

namespace common\modules\company\components;

use backend\models\company\CompanyServiceRejectForm;
use common\components\Emailer;
use common\models\CompanyService;
use lib\message\MessageServiceInterface;
use yii\helpers\Url;

/**
 * Class CompanyServiceEmailer
 */
class CompanyServiceEmailer
{
    /**
     * @var MessageServiceInterface
     */
    private $sender;

    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @param Emailer $emailer
     */
    public function injectDependencies(Emailer $emailer)
    {
        $this->sender = \Yii::$app->message;
        $this->emailer = $emailer;
    }

    /**
     * @param CompanyService $companyService
     * @param CompanyServiceRejectForm $rejectForm
     */
    public function sendCompanyServiceModerationReject(CompanyService $companyService, CompanyServiceRejectForm $rejectForm): void
    {
        $params = [
            'username'           => $companyService->ps->user->getFullNameOrUsername(),
            'serviceName'        => $companyService->title,
            'psName'             => $companyService->ps->title,
            'companyName'        => $companyService->ps->title,
            'companyServiceLink' => param('server').'/mybusiness/services?serviceId=' . $companyService->id,
            'reason'             => $rejectForm->getReason(),
            'comment'            => $rejectForm->getComment(),
            'rejectReason'       => $rejectForm->getText(),
        ];
        $this->sender->send($companyService->ps->user, 'company.service.moderation.reject', $params);
    }

    /**
     * @param CompanyService $companyService
     */
    public function sendCompanyServiceModerationApprove(CompanyService $companyService): void
    {
        $params = [
            'username'           => $companyService->ps->user->getFullNameOrUsername(),
            'serviceName'        => $companyService->title,
            'companyName'        => $companyService->ps->title,
            'companyServiceLink' => param('siteUrl').'/mybusiness/services?serviceId=' . $companyService->id
        ];
        $this->sender->send($companyService->ps->user, 'company.service.moderation.approved', $params);
    }
}
