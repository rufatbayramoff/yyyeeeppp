<?php
/**
 * User: nabi
 */

namespace common\modules\company\components;

interface CompanyServiceInterface
{
    const TYPE_PRINTER = 'printer';
    const TYPE_CNC = 'cnc';
    const TYPE_CUTTING = 'cutting';
    const TYPE_SERVICE = 'service';
    const TYPE_DESIGNER = 'designer';
    const TYPE_WINDOW = 'window';

    public function getTitleLabel();

    public function getDescription();

    public function getOwner();

    public function getLogoImage();

    public function getImages();

    public function getType();

    public function getCategory();
}