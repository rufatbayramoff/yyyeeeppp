<?php
/**
 * User: nabi
 */

namespace common\modules\company\services;

use backend\models\company\CompanyServiceRejectForm;
use common\components\Emailer;
use common\models\CompanyService;
use common\models\CompanyServiceImage;
use common\models\CompanyServiceType;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\repositories\CompanyServiceCategoryRepository;
use common\models\repositories\FileRepository;
use common\modules\company\components\CompanyServiceEmailer;
use common\modules\company\components\CompanyServiceInterface;
use common\modules\company\factories\CompanyServiceFactory;
use common\modules\company\repositories\CompanyServiceRepository;
use common\modules\company\repositories\CompanyServiceTypesRepository;
use Faker\Provider\Company;
use frontend\models\ps\PsPrinterUpdater;
use frontend\modules\mybusiness\models\CompanyServiceAddForm;
use frontend\modules\mybusiness\modules\CsWindow\services\CsWindowService;
use yii\base\BaseObject;
use yii\web\GoneHttpException;
use yii\web\NotAcceptableHttpException;

/**
 * Class CompanyServicesService
 * @package common\modules\company\services
 *
 * @property CsWindowService $windowService
 */
class CompanyServicesService extends BaseObject
{

    /**
     * @var CompanyServiceTypesRepository
     */
    private $servicesRepository;

    /**
     * @var CompanyServiceFactory
     */
    private $companyServiceFactory;

    /**
     * @var CompanyServiceEmailer
     */
    private $emailer;

    private $windowService;

    /**
     * @var CompanyServiceRepository
     */
    protected $companyServiceRepository;

    /**
     * @var CompanyServiceCategoryRepository
     * */
    protected $companyServiceCategoryRepository;

    public function injectDependencies(
        CompanyServiceTypesRepository $servicesRepository,
        CompanyServiceFactory $companyServiceFactory,
        CompanyServiceEmailer $emailer,
        CsWindowService $windowService,
        CompanyServiceRepository $companyServiceRepository,
        CompanyServiceCategoryRepository $companyServiceCategoryRepository
    )
    {
        $this->servicesRepository       = $servicesRepository;
        $this->companyServiceFactory    = $companyServiceFactory;
        $this->emailer                  = $emailer;
        $this->windowService            = $windowService;
        $this->companyServiceRepository = $companyServiceRepository;
        $this->companyServiceCategoryRepository = $companyServiceCategoryRepository;
    }

    /**
     * @return CompanyServiceType[]
     */
    public function getServicesTypes($asMap = false)
    {
        $types = $this->servicesRepository->getAll();
        if ($asMap) {
            $result = [];
            foreach ($types as $type) {
                $result[$type->name] = $type;
            }
            $types = $result;
        }
        return $types;
    }

    /**
     * @return CompanyServiceType[]
     */
    public function getServicesTypeMachine($asMap = false): array
    {
        $types = CompanyServiceTypesRepository::getMachine();
        if ($asMap) {
            $result = [];
            foreach ($types as $type) {
                $result[$type->name] = $type;
            }
            $types = $result;
        }
        return $types;
    }

    /**
     * get services for owner
     *
     * @param Ps $company
     * @param string $type
     * @return array|CompanyService[]|\yii\db\ActiveRecord[]
     */
    public function getCompanyServicesForOwner(Ps $company, $type = CompanyServiceInterface::TYPE_SERVICE)
    {
        $services = CompanyService::find()->forCompany($company)->notDeleted()->byType($type)->all();
        return $services;
    }

    /**
     * @param Ps $company
     * @param string $type
     * @return array|CompanyService[]|\yii\db\ActiveRecord[]
     */
    public function getCompanyServiceForOwner(Ps $company, $serviceId)
    {
        $services = CompanyService::find()->forCompany($company)->notDeleted()->andWhere(['id' => $serviceId])->all();
        return $services;
    }

    /**
     * @param $machines
     * @param $serviceId
     * @return array
     */
    public function filterByCompanyServiceId($machines, $serviceId): array
    {
        foreach ($machines as $machine) {
            if ($machine->companyService->id == $serviceId) {
                return [$machine];
            }
        }
        return [];
    }

    /**
     * @param Ps $company
     * @param CompanyServiceAddForm $form
     * @throws \yii\base\Exception
     */
    public function createService(Ps $company, CompanyServiceAddForm $form)
    {
        $service = $this->companyServiceFactory->createService($company, $form);
        $service->safeSave();
        $this->bindServiceImages($service, $form->newImageFileIds);
        return $service;
    }

    /**
     * @param CompanyServiceAddForm $serviceForm
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function updateService(CompanyServiceAddForm $serviceForm)
    {

        $serviceForm->moderator_status = $serviceForm->getModeratorStatusAfterUpdate();
        $serviceForm->safeSave();
        $this->unbindServiceImages($serviceForm, $serviceForm->getUnbindImageFiles());
        $this->bindServiceImages($serviceForm, $serviceForm->newImageFileIds);
        $this->setPrimaryImage($serviceForm, max(0, $serviceForm->primaryPictureIndex));
        return $serviceForm;
    }

    /**
     * @param CompanyService $service
     * @throws \yii\base\Exception
     */
    public function deleteService(Ps $company, CompanyService $service)
    {
        if ($company->id != $service->ps_id) {
            throw new NotAcceptableHttpException(_t('mybusiness.services', 'Error deleting service'));
        }
        $this->companyServiceRepository->delete($service);
    }

    /**
     * @param CompanyService $service
     * @param CompanyServiceAddForm $form
     * @throws \yii\base\Exception
     */
    private function bindServiceImages(CompanyService $service, array $newImageFileIds)
    {
        $result = [];
        foreach ($newImageFileIds as $k => $fileId) {
            $result[] = CompanyServiceImage::addRecord(
                [
                    'company_service_id' => $service->id,
                    'file_id'            => $fileId
                ]
            );
        }
        return $result;
    }


    /**
     * @param CompanyServiceAddForm $form
     * @param array $unbindImageFiles
     * @throws \yii\base\InvalidConfigException
     */
    private function unbindServiceImages(CompanyServiceAddForm $form, array $unbindImageFiles)
    {
        $fileRepository = \Yii::createObject(FileRepository::class);
        foreach ($unbindImageFiles as $oldFile) {
            if ($form->primaryImage && $form->primaryImage->file_id == $oldFile->id) {
                $this->resetPrimaryImage($form);
            }
            CompanyServiceImage::deleteAll(['file_id' => $oldFile->id, 'company_service_id' => $form->id]);
            $fileRepository->delete($oldFile);
        }
    }

    /**
     * @param $form
     */
    private function resetPrimaryImage($form)
    {
        $form->primary_image_id = null;
        $form->safeSave();
    }

    /**
     * @param CompanyService $service
     * @param CompanyServiceImage $serviceImage
     * @throws \yii\base\Exception
     */
    private function setPrimaryImage(CompanyService $service, $index)
    {
        $serviceImages = CompanyServiceImage::find()->where(['company_service_id' => $service->id])->all();

        $serviceImage              = array_key_exists($index, $serviceImages) ? $serviceImages[$index] : null;
        $service->primary_image_id = $serviceImage ? $serviceImage->id : null;
        $service->safeSave();
    }

    /**
     * @param CompanyService $model
     * @param CompanyServiceRejectForm $rejectForm
     */
    public function moderatorServiceReject(CompanyService $model, CompanyServiceRejectForm $rejectForm)
    {
        $rejectForm->saveRejectStatus($model);
        $this->emailer->sendCompanyServiceModerationReject($model, $rejectForm);
    }

    /**
     * @param CompanyService $companyService
     * @throws \common\components\exceptions\BusinessException
     */
    public function moderatorServiceApprove(CompanyService $companyService)
    {
        $companyService->moderator_status = CompanyService::MODERATOR_STATUS_APPROVED;
        $companyService->safeSave();
        $this->emailer->sendCompanyServiceModerationApprove($companyService);

        if ($companyService->isTypeWindow()) {
            $this->windowService->createSnapshot($companyService->csWindow);
        }
    }

    /**
     * @param CompanyService $companyService
     *
     * @throws GoneHttpException
     */
    public function tryCanBeOrdered(CompanyService $companyService): void
    {
        if ($companyService->isDeleted()) {
            throw new GoneHttpException(_t('site.store', 'Service is deleted'));
        }

        if ($companyService->isRejected()) {
            throw new GoneHttpException(_t('site.store', 'Service is not published'));
        }

        $companyActive = $companyService->company->isActive() ?? false;
        if($companyActive === false) {
            throw new GoneHttpException(_t('site.store', 'The manufacturer is currently inactive.'));
        }
    }
}
