<?php
/**
 * User: nabi
 */

namespace common\modules\company\services;


use common\components\DateHelper;
use common\components\exceptions\BusinessException;
use common\components\UuidHelper;
use common\models\Company;
use common\models\CompanyVerify;
use common\models\Ps;
use common\models\User;
use common\models\UserEmailLogin;
use common\modules\company\models\CompanyVerifyCodeForm;
use common\services\UserEmailLoginService;
use frontend\models\user\FrontUser;
use yii\base\BaseObject;
use yii\base\UserException;
use yii\helpers\Url;

class CompanyVerifyService extends BaseObject
{
    /**
     * @var UserEmailLoginService
     */
    private $emailLoginService;

    public function injectDependencies(UserEmailLoginService $emailLoginService)
    {
        $this->emailLoginService = $emailLoginService;
    }

    /**
     * @param Ps $company
     * @return CompanyVerify
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function sendVerifyEmail(Ps $company)
    {

        $hasRequested = UserEmailLogin::find()
            ->where(['email' => $company->user->email])
            ->andWhere(['>', 'created_at', date("Y-m-d")])
            ->exists();
        if ($hasRequested) {
            return false;
        }
        $password = '';
        if ($company->user->password_hash === '-') {
            $password = UuidHelper::generateRandCode(7);
            $frontUser = FrontUser::tryFindByPk($company->user_id);
            $frontUser->setPassword($password);
            $frontUser->generateAuthKey();
            $frontUser->safeSave();
        }

        $emailLogin = $this->emailLoginService->generateUserEmailLogin(
            $company->user, $company->user->email, param('server') . '/companies/verify/email-verified?id=' . $company->id
        );

        $this->emailLoginService->sendLoginLink($emailLogin, $emailLogin->requested_url . '&code=' . $emailLogin->hash, $password);
        // verify
        $companyVerify = $this->createCompanyVerify($company, $emailLogin->hash);
        return $companyVerify;
    }

    /**
     * after user registers and verifies email, we verify his company
     *
     * @param Ps $company
     * @throws \yii\base\Exception
     */
    public function verifiedByUserRegister(Ps $company)
    {
        if ($company->needClaim()) {
            $this->verifiedCompany($company);
        }
    }
    /**
     * @param Ps $company
     * @param $code
     * @return bool
     * @throws \yii\base\Exception
     */
    public function verifiedByEmail(Ps $company, $code)
    {
        $verify = CompanyVerify::findOne(['code' => $code, 'company_id' => $company->id]);
        if ($verify) {
            $this->markVerified($verify);
            $this->activateUser($company->user);
            $this->verifiedCompany($company);
            return true;
        }
        return false;
    }

    /**
     * @param Ps $company
     * @param $emailLogin
     * @return CompanyVerify
     * @throws \yii\base\Exception
     */
    public function createCompanyVerify(Ps $company, $verifyCode = null): CompanyVerify
    {
        if (!$verifyCode) {
            $verifyCode = \Yii::$app->getSecurity()->generateRandomString(32);
        }
        $companyVerify = new CompanyVerify();
        $companyVerify->company_id = $company->id;
        $companyVerify->verify_method = CompanyVerify::METHOD_EMAIL;
        $companyVerify->created_at = DateHelper::now();
        $companyVerify->code = $verifyCode;
        $companyVerify->safeSave();
        return $companyVerify;
    }

    /**
     * @param CompanyVerify $verify
     * @return string
     */
    public function getVerifyAndAcceptLink(CompanyVerify $verify)
    {
        return Url::toRoute(['/companies/verify/accept', 'id' => $verify->company_id, 'code' => $verify->code], true);
    }

    /**
     * @param CompanyVerifyCodeForm $verifyForm
     * @return CompanyVerify
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function verifiedByForm(CompanyVerifyCodeForm $verifyForm)
    {
        $verify = CompanyVerify::findOne(['code' => $verifyForm->code, 'company_id' => $verifyForm->companyId]);
        if (!$verify) {
            throw new BusinessException(_t('company.verify', 'Invalid verification code'));
        }
        if (!empty($verify->verified_at)) {
            throw new BusinessException(_t('company.verify', 'This verification code cannot be used'));
        }
        $company = $verify->company;
        $this->markVerified($verify);
        $this->activateUser($company->user);
        $this->verifiedCompany($company);
        return $verify;
    }

    /**
     * set random password and activate user
     *
     * @param Ps $company
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    private function activateUser(User $user): void
    {
        $user = FrontUser::tryFindByPk($user->id);
        $user->status = User::STATUS_ACTIVE;
        $user->safeSave();
    }

    /**
     * @param Ps $company
     * @throws \yii\base\Exception
     */
    private function verifiedCompany(Ps $company): void
    {
        if ($company->is_backend) {
            $company->moderator_status = Company::MSTATUS_CHECKED;
            $company->is_backend = 0;
        }
        $company->safeSave();
    }

    /**
     * @param $verify
     */
    private function markVerified($verify): void
    {
        $verify->verified_at = DateHelper::now();
        $verify->safeSave();
    }

}