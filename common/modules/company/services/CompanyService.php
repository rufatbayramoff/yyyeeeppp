<?php
/**
 * User: nabi
 */

namespace common\modules\company\services;


use common\models\User;
use frontend\models\user\FrontUser;
use frontend\models\user\LoginForm;
use Yii;
use yii\base\BaseObject;

class CompanyService extends BaseObject
{

    public function loginCompanyUser(User $user)
    {

    }

    public function isCompanyCreatedUser(User $user)
    {
        return $user->password_hash === '-' && $user->status === User::STATUS_DRAFT_COMPANY;
    }

    /**
     * @param FrontUser $user
     * @return LoginForm
     * @throws \yii\base\Exception
     */
    public function updateCompanyUserForLogin(FrontUser $user, $password)
    {
    }
}