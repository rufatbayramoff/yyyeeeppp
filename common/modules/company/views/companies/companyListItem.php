<?php
/**
 * User: nabi
 */


/* @var $model \common\models\Ps */
?>
<div class="col-sm-6 col-md-4" itemscope itemtype="http://schema.org/Service">
    <div class="designer-card designer-card--service-card">
        <div class="designer-card__userinfo">
            <a class="designer-card__avatar" href="#">
                <img src="<?= HL($model->getLogoUrl()); ?>"/>
            </a>
            <div class="designer-card__username" itemprop="name">
                <a href="/c/<?= H($model->url); ?>"><?= H($model->title); ?></a>
            </div>
        </div>

        <div class="designer-card__models-list">
            <?php
            $images = $model->getPicturesFiles(100);
            ?>
            <?=
            \frontend\widgets\SwipeGalleryWidget::widget([
                'files'            => $images,
                'assetsByClass'    => true,
                'thumbSize'        => [320, 180],
                'containerOptions' => ['class' => 'ps-pub-portfolio__slider'],
                'itemOptions'      => ['class' => 'ps-pub-portfolio__item', 'itemprop' => 'image'],
                'scrollbarOptions' => ['class' => 'ps-pub-portfolio__scrollbar'],
                'emptyOptions'     => ['class' => 'designer-card__models-list-empty'],
                'emptyText'        => _t('site.catalog', 'Images not uploaded')
            ]);
            ?>
        </div>

        <div class="designer-card__about" itemprop="description">
            <?= H(\yii\helpers\StringHelper::truncate(strip_tags($model->description), 90)); ?>
        </div>

        <div class="designer-card__btn-block">
            <button class="btn btn-danger btn-ghost" loader-click="openCreatePreorder(<?= $model->id; ?>, {description:'<?= H($model->title); ?>'})">
                <?= _t('site.ps', 'Get a Quote'); ?>
            </button>
        </div>
    </div>
</div>