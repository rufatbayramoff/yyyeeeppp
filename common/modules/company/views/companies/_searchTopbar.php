<?php

use common\models\CompanyServiceCategory;
use common\models\EquipmentCategory;
use yii\bootstrap\ActiveForm;

/** @var $searchForm \common\modules\equipments\models\CatalogSearchForm */

$hideSort = $hideSort ?? false;

?>
<div class="nav-tabs__container">
    <div class="container">
        <div class="nav-filter nav-filter--many1">
            <?php $form = ActiveForm::begin(
                [
                    'method'=>'get',
                    'layout'      => 'inline',
                    'options'     => [
                        'onSubmit' => new \yii\web\JsExpression('TsServiceSearch.changeForm(this);return false;'),
                    ],
                    'fieldConfig' => [
                        'template'     => "{label} {input} ",
                        'labelOptions' => [
                            'class' => '',
                        ],
                        'inputOptions' => [
                            'class'    => 'form-control input-sm',
                            'onchange' => new \yii\web\JsExpression('TsServiceSearch.changeForm(this.form)')
                        ],
                    ],
                ]
            );
            ?>

            <input type="checkbox" id="showhideNavFilter">
            <label class="nav-filter__mobile-label" for="showhideNavFilter">
                <?= _t('site.ps', 'Filters'); ?>
                <span class="tsi tsi-down"></span>
            </label>

            <div class="nav-filter__mobile-container">
                <div class="nav-filter__group">
                    <?= $form->field($searchForm, 'search')->textInput(); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    TsServiceSearch = {
        formFilterElements: [],
        changeForm: function (form) {
            // get form elements data
            var inputs = form.getElementsByTagName("select");
            var formData = {};
            var sort = '';
            for (var i = 0; i < inputs.length; i++) {
                if (this.formFilterElements.indexOf(inputs[i].name) >= 0) {
                    if (inputs[i].value != '')
                        formData[inputs[i].name] = inputs[i].value;
                }
                if (inputs[i].name == 'sort') {
                    sort = inputs[i].value;
                }
            }
            serialize = function (obj) {
                var str = [];
                for (var p in obj)
                    if (obj.hasOwnProperty(p)) {
                        if (obj[p])
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    }
                return str.join("&");
            }
            var searchVal = document.getElementById('search').value;
            query = serialize({sort: sort, search: searchVal});
            if (query) query = '?' + query;
            var filter = this.convertToFilterUrl(formData);
            var url = this.updateUrlFilter(filter) + query;

            window.location.href = url;
        },

        convertToFilterUrl: function (formData) {
            var result = [];
            for (var i in formData) {
                result.push([i, formData[i]].join('-'));
            }
            return result.join('--');
        },

        updateUrlFilter: function (filter) {
            var curLocation = window.location.pathname;
            var newHref = curLocation.split('/');
            fullHref = newHref.join('/');
            return fullHref;
        },

        changeCategory: function (newCatergoryPath) {
            var currentUrl = window.location.href;
            var currentUrlFilter = window.location.href.substr(window.location.href.lastIndexOf('/')+1);
            window.location.href = newCatergoryPath + currentUrlFilter;
        }
    };

</script>
