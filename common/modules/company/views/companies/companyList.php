<?php
/**
 * User: nabi
 */


/** @var $ps \common\models\Ps */
/** @var $publicServicePage \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\PsCncMachine[] $psCncMachines */

$this->registerAssetBundle(\frontend\assets\DropzoneAsset::class);

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->directive('dropzone-button')
    ->controllerParams(['z' => 'y'])
    ->controller([
        'ps/PsCatalogController',
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service',
        'product/productForm',
        'product/productModels',
    ]);
$this->title = _t('public.services', 'Companies');
?>

<div class="store-filter__container">
    <div class="container">
        <div class="store-filter">
            <h1 class="store-filter__categories-title js-category-header">
                <?=_t('public.services', 'Companies');?>
            </h1>
        </div>
    </div>
</div>

<?php echo $this->render('_searchTopbar.php', ['searchForm' => $searchForm]); ?>

<div class="container">

    <?php
    $listView = Yii::createObject(
        [
            'class'        => \yii\widgets\ListView::class,
            'dataProvider' => $dataProvider,
            'itemOptions'  => ['tag' => null],
            'itemView'     => function($model, $key, $index, $widget){
                return $this->render('companyListItem', ['model'=>$model]);
            },
            'viewParams'   => [
            ]
        ]
    );
    if ($listView->dataProvider->getCount() > 0): ?>
        <div class="responsive-container-list responsive-container-list--3" ng-controller="PsCatalogController">
            <?= $listView->renderItems() ?>
        </div>
        <div class="row"><div class="col-sm-12"><?= $listView->renderPager() ?></div></div>
    <?php endif; ?>

</div>

<?php
echo $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-modal.php');
?>