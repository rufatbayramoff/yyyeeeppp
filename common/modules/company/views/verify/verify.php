<?php
/**
 * User: nabi
 */
/* @var $this \yii\web\View */
/* @var $company array|\common\models\Ps|null|\yii\db\ActiveRecord */
$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

$this->title = $company->title . ' - ' . _t('company.verify', 'Verification Process');
?>

<div class="container">
    <h2><a href="/c/<?=$company->url;?>"><?=H($company->title); ?></a></h2>
    <h3><?= _t('company.verify', 'Choose a way to verify'); ?></h3>


    <p><?=_t('company.verify', "Select how you'd like to get a verification code.");?></p>



    <?php if (false && !empty($company->phone)): ?>
        <div class="row">
        <div class="col-lg-1 m-t20" style="font-size: 28px;">
            <div class="tsi tsi-call-incoming "></div>
        </div>
        <div class="col-lg-6">
            <h3><?php
                $numberObj = $phoneUtil->parse($company->phone, $company->country->iso_code);
                echo $phoneUtil->format($numberObj, \libphonenumber\PhoneNumberFormat::NATIONAL); ?></h3>
            <?=_t('company.verify', 'Get your code at this number by automated call or text message (standard rates apply)');?>
        </div>
        <div class="col-lg-4">
            <input type="button" class="btn  btn-info" value="Call"/>
            <input type="button" class="btn btn-primary" value="Text"/>
        </div>
    </div>
    <?php endif; ?>
    <hr />
    <div class="row">
        <div class="col-lg-1 m-t20" style="font-size: 28px;">
            <div class="tsi tsi-mail "></div>
        </div>
        <div class="col-lg-6">
            <h3><?= $company->formatPublicEmail($company->user->email); ?></h3>
            <?=_t('front.site', 'Get your code at this email address.');?>
        </div>
        <div class="col-lg-4">
            <a href="#" onClick="document.location=$(this).data('action')" data-action="/companies/verify/verify-by-email?id=<?=$company->id;?>" class="btn btn-primary"><?=_t('front.site', 'Send');?></a>
        </div>
    </div>

    <p class="bar bg-warning">
        <?php
        $terms = _t('front.site', 'Terms');
        $policy = _t('front.site', 'Privacy Policy');
        $cookie = _t('front.site', 'Cookie use');
        echo _t('app.site', 'By using verification you agree to our {terms} and that
                    you have read our {policy}, including our {cookie}', [
                'terms' => sprintf("<a href='%s/site/terms'>$terms</a> ", Yii::getAlias('@web')),
                'policy' => sprintf("<a href='%s/site/policy'>$policy</a>", Yii::getAlias('@web')),
                'cookie' => sprintf("<a href='%s/site/policy#cookie'>$cookie</a>.", Yii::getAlias('@web')),
            ]
        );

        ?>
    </p>
    <hr />
    <div class="row">
        <div class="col-lg-1 m-t20" style="font-size: 28px;">
            <div class="tsi tsi-question-c "></div>
        </div>
        <div class="col-lg-6">
            <h3><?=_t('company.verify', 'Need help?');?></h3>
            <?=_t('company.verify', 'Contact us if you have difficulties with verifying your business.');?>
        </div>
        <div class="col-lg-4">
            <a href="/site/contact?subject=Verify:<?=HL($company->title);?>" class="btn btn-info"><?=_t('front.site', 'Contact Us');?></a>
        </div>
    </div>
</div>
