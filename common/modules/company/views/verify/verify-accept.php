<?php
/**
 * User: nabi
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $company array|\common\models\Ps|null|\yii\db\ActiveRecord */
/* @var $verifyForm \common\modules\company\models\CompanyVerifyCodeForm */
$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

$this->title = $company->title . ' - ' . _t('company.verify', 'Verification process');
?>

<div class="container">
    <h2><a href="/c/<?=$company->url;?>"><?=H($company->title); ?></a></h2>
    <p><?=_t('company.verify', "Please accept our terms to verify your business.");?></p>
    <hr />


    <p>
        <?php
        $terms = _t('front.site', 'Terms');
        $policy = _t('front.site', 'Privacy Policy');
        $cookie = _t('front.site', 'Cookie use');
        echo _t('app.site', 'By accepting you agree to our {terms} and that
                    you have read our {policy}, including our {cookie}', [
                'terms' => sprintf("<a href='%s/site/terms'>$terms</a> ", Yii::getAlias('@web')),
                'policy' => sprintf("<a href='%s/site/policy'>$policy</a>", Yii::getAlias('@web')),
                'cookie' => sprintf("<a href='%s/site/policy#cookie'>$cookie</a>.", Yii::getAlias('@web')),
            ]
        );

        ?>
    </p>
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
    <div class="row">
        <div class="col-lg-8">
            <?= $form->field($verifyForm, 'companyId')->hiddenInput()->label(false) ?>
            <?= $form->field($verifyForm, 'code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-2"> <br />
            <?= Html::submitButton(Yii::t('app', 'Accept'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <hr />
    <br />
    <br />
    <div class="row">
        <div class="col-lg-1 m-t20" style="font-size: 22px;">
            <div class="tsi tsi-question-c "></div>
        </div>
        <div class="col-lg-6">
            <h4><?=_t('company.verify', 'Need help?');?></h4>
            <?=_t('company.verify', 'Contact Us if you have difficulties with verifying your business.');?>
        </div>
        <div class="col-lg-4">
            <a href="/site/contact?subject=VerifyAccept:<?=HL($company->title);?>" class="btn btn-info btn-sm"><?=_t('front.site', 'Contact Us');?></a>
        </div>
    </div>
</div>
