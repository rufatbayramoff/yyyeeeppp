<?php


namespace common\modules\company\repositories;


use common\models\PsPrinter;

class PsPrinterRepository
{
    public function save(PsPrinter $printer): void
    {
        $printer->safeSave();
    }
}