<?php
/**
 * User: nabi
 */

namespace common\modules\company\repositories;


use common\components\ArrayHelper;
use common\components\DateHelper;
use common\models\Company;
use common\models\CompanyService;
use common\models\CompanyServiceBlock;
use common\models\CompanyServiceBlockImage;
use common\models\CompanyServiceCategory;
use common\models\CompanyServiceImage;
use common\models\CompanyServiceTag;
use common\models\Ps;
use common\models\query\CompanyServiceQuery;
use common\models\repositories\FileRepository;
use common\modules\catalogPs\models\ServicesSearchForm;
use yii\web\NotFoundHttpException;

class CompanyServiceRepository
{
    public $perPage = 50;

    /** @var FileRepository */
    public $fileRepository;

    public function injectDependencies(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }
    /**
     * show private sections for current user
     *
     * @param Ps $ps
     * @param null $type
     * @return array|CompanyService[]|\yii\db\ActiveRecord[]
     */
    public function getPrivateServicesByCompany(Ps $ps, $type = null)
    {
        $services = CompanyService::find()->forCompany($ps)->notDeleted()->byType($type)->all();
        return $services;
    }

    public function companyServiceExist(Ps $ps,int $categoryId)
    {
        return CompanyService::find()->forCompany($ps)->andWhere(['category_id' => $categoryId])->exists();
    }

    /**
     * used to show in ps public profile
     *
     * @param Ps $ps
     * @param null $type
     * @return CompanyServiceQuery
     */
    public function getPublicServicesByCompanyQuery(Ps $ps, $type = null)
    {
        $services = CompanyService::find()->forCompany($ps)->notDeleted()->moderatorStatus(CompanyService::moderatedStatusList())->visibleEverywhere();
        if ($type) {
            $services->byType($type);
        }

        return $services;
    }

    /**
     * @param $type
     * @return CompanyServiceQuery
     */
    public function getPublicServicesQueryByType($type)
    {
        $services = CompanyService::find()->notDeleted()->moderatorStatus(CompanyService::moderatedStatusList())->byType($type);
        return $services;
    }

    /**
     * @param ServicesSearchForm $searchForm
     * @return CompanyServiceQuery
     */
    public function getPublicServicesBySearch(ServicesSearchForm $searchForm): CompanyServiceQuery
    {
        $services = CompanyService::find()->notDeleted()->inCatalog();
        $services->joinWith('ps');
        $services->andWhere(['ps.moderator_status'=>[Company::MSTATUS_CHECKED, Company::MSTATUS_UPDATED, Company::MSTATUS_CHECKING]]);
        $services->andWhere(['ps.is_deleted' => Company::IS_NOT_DELETED_FLAG]);
        if (!empty($searchForm->search)) {
            $services->andWhere(['OR', ['like', 'company_service.title', $searchForm->search], ['like', 'company_service.description', $searchForm->search]]);
        }
        if (!empty($searchForm->category) && !empty($searchForm->getCategory())) {
            $leaves = ArrayHelper::getColumn($searchForm->getCategory()->leaves()->all(), 'id');
            $leaves[] = $searchForm->getCategory()->id;
            $services->inCategory($leaves);
        }
        if ($searchForm->type) {
            $services->byType($searchForm->type);
        }
        return $services;
    }

    /**
     * @param int $id
     * @param Company $company
     * @return array|CompanyService|null|\yii\db\ActiveRecord
     */
    public function findByIdAndCompany(int $id, Company $company)
    {
        $service = CompanyService::find()
            ->where(['id'=>$id, 'ps_id'=>$company->id])
            ->one();
        return $service;
    }

    /**
     * @param CompanyService $companyService
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function save(CompanyService $companyService)
    {
        $companyService->updated_at = DateHelper::now();
        $relatedRecords = $companyService->relatedRecords;

        $companyService->safeSave();

        if (array_key_exists('companyServiceImages', $relatedRecords)) {
            /* @var $bindedImage CompanyServiceImage */
            foreach ($relatedRecords['companyServiceImages'] as $bindedImage) {
                if ($bindedImage->file->forDelete) {
                    CompanyServiceImage::deleteAll(['file_id' => $bindedImage->file->id]);
                    $this->fileRepository->delete($bindedImage->file);
                } else {
                    if ($bindedImage->isNewRecord) {
                        $this->fileRepository->save($bindedImage->file);
                    }
                    $bindedImage->company_service_id = $companyService->id;
                    $bindedImage->safeSave();
                }
            }
        }

        if (array_key_exists('tags', $relatedRecords)) {
            $oldTagsIds = ArrayHelper::getColumn($companyService->companyServiceTags, 'tag_id');
            $bindedTags = array_filter(ArrayHelper::getColumn($relatedRecords['tags'], 'id'));
            if (empty($bindedTags)) {
                $bindedTags = [0];
            }
            CompanyServiceTag::deleteAll(['AND', 'company_service_id=:id', ['NOT IN', 'tag_id', $bindedTags]], [':id' => $companyService->id]);
            foreach ($relatedRecords['tags'] as $siteTag) {
                if (in_array($siteTag->id, $oldTagsIds)) {
                    continue;
                }
                $siteTag->safeSave();
                $productSiteTag = new CompanyServiceTag();
                $productSiteTag->company_service_id = $companyService->id;
                $productSiteTag->tag_id = $siteTag->id;
                $productSiteTag->safeSave();
            }
        }

    }

    public function delete(CompanyService $companyService)
    {
        $companyService->is_deleted = 1;
        $companyService->safeSave();
    }

    /**
     * we need to check if such service already exists in company,
     *
     * @param Company $company
     * @param $title
     * @return array|CompanyService|null|\yii\db\ActiveRecord
     */
    public function getServiceByTitle(Company $company, $title)
    {
        $service = CompanyService::find()
            ->where(['title'=>$title, 'ps_id' => $company->id])
            ->one();
        return $service;
    }

    /**
     * @param CompanyServiceBlock $serviceBlock
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \Throwable
     */
    public function saveCompanyServiceBlock(CompanyServiceBlock $serviceBlock)
    {
        $relatedRecords = $serviceBlock->relatedRecords;
        $serviceBlock->safeSave();
        if (array_key_exists('imageFiles', $relatedRecords)) {
            foreach ($relatedRecords['imageFiles'] as $imageFile) {
                if ($imageFile->forDelete) {
                    CompanyServiceBlockImage::deleteAll(['file_uuid' => $imageFile->uuid]);
                    $this->fileRepository->delete($imageFile);
                } else {
                    if ($imageFile->isNewRecord) {
                        $this->fileRepository->save($imageFile);
                        $productImage = new CompanyServiceBlockImage();
                        $productImage->file_uuid = $imageFile->uuid;
                        $productImage->block_id = $serviceBlock->id;
                        $productImage->safeSave();
                    }
                }
            }
        }
    }

    public function findPublicServicesByCategory(CompanyServiceCategory $category)
    {
        $services = CompanyService::find()->notDeleted()->inCatalog();
        $leaves = ArrayHelper::getColumn($category->leaves()->all(), 'id');
        $leaves[] = $category->id;
        $services->inCategory($leaves);
        return $services;
    }

    public function getSeeAlsoByService($service, $limit = 3)
    {
        $services = CompanyService::find()->where(['ps_id' => $service->ps_id])
            ->inCatalog()
            ->byType(CompanyService::TYPE_SERVICE)
            ->andWhere(['NOT', ['id' => $service->id]])->limit($limit)
            ->all();

        return $services;
    }

    /**
     * @param CompanyService $companyService
     *
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function setServiceImagesRotate(CompanyService $companyService): void
    {
        foreach ($companyService->companyServiceImages as $img) {
            $img->setImageRotate();
        }
    }

    /**
     * @param $id
     *
     * @return CompanyService
     * @throws NotFoundHttpException
     */
    public function getTryById($id): CompanyService
    {
        $service = CompanyService::find()->where(['id' => $id])->one();

        if (!$service) {
            throw new NotFoundHttpException(_t('site.store', 'Service not found.'));
        }

        return $service;
    }
}
