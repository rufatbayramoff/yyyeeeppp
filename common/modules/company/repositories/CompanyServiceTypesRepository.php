<?php
/**
 * User: nabi
 */

namespace common\modules\company\repositories;

use common\models\CompanyServiceType;

class CompanyServiceTypesRepository
{
    protected static $companyServiceTypes;
    protected static $machine;

    /**
     * @return CompanyServiceType[]
     */
    public static function getAll()
    {
        if (!self::$companyServiceTypes) {
            self::$companyServiceTypes = CompanyServiceType::find()->withoutWindow()->all();
        }
        return self::$companyServiceTypes;
    }

    /**
     * @return CompanyServiceType[]
     */
    public static function getMachine()
    {
        if (!self::$machine) {
            self::$machine = CompanyServiceType::find()->machine()->all();
        }
        return self::$machine;
    }

    public static function getLabels()
    {
        $types = self::getAll();
        $labels = [];
        foreach ($types as $type) {
            $labels[$type->name] = $type->title;
        }
        return $labels;
    }
}