<?php


namespace common\modules\company\repositories;


use common\components\DateHelper;
use common\models\Company;
use common\models\InstantPayment;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\Payment;
use common\models\PaymentInvoice;
use common\models\Preorder;
use common\models\query\StoreOrderAttempQuery;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use yii\db\Expression;

class OrderRepository
{
    public function preorder(Company $company)
    {
        return Preorder::find()
            ->forPs($company)
            ->innerJoin(['store_order' => 'store_order'], 'store_order.preorder_id=preorder.id')
            ->andWhere(['>', 'store_order.created_at', new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->notDraft()
            ->count();
    }

    public function allPreorder(Company $company)
    {
        return Preorder::find()
            ->forPs($company)
            ->andWhere(['>', 'preorder.created_at', new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->notDraft()
            ->count();
    }

    public function preorderCancel(Company $company)
    {
        return Preorder::find()
            ->forPs($company)
            ->andWhere(['>', 'preorder.created_at', DateHelper::subNow('P12M' )])
            ->andWhere(['preorder.status' => Preorder::STATUS_REJECTED_BY_COMPANY])
            ->count();
    }

    public function payOrder(Company $company)
    {
        return Payment::find()
            ->innerJoinWith(['paymentInvoice.storeOrder.storeOrderAttemps'])
            ->andWhere(['store_order_attemp.ps_id' => $company->id])
            ->andWhere(['payment_invoice.status' => [PaymentInvoice::STATUS_AUTHORIZED, PaymentInvoice::STATUS_VOID, PaymentInvoice::STATUS_REFUNDED, PaymentInvoice::STATUS_CANCELED, PaymentInvoice::STATUS_PAID]])
            ->andWhere(['>', 'store_order.created_at', new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->count();
    }

    public function payApproved(Company $company)
    {
        return InstantPayment::find()
            ->innerJoinWith(['primaryInvoice'])
            ->andWhere(['payment_invoice.company_id' => $company->id])
            ->andWhere(['instant_payment.status' => [InstantPayment::STATUS_APPROVED]])
            ->andWhere(['>', 'payment_invoice.created_at', new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->count();
    }

    public function completedOrder(Company $company)
    {
        return StoreOrder::find()->joinWith(
            [
                'currentAttemp' => function (StoreOrderAttempQuery $query) use ($company) {
                    $query->forCompany($company);
                    $query->inStatus(StoreOrderAttemp::STATUS_RECEIVED);
                }
            ])
            ->andWhere(['>', 'store_order.created_at', new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->count();
    }

    public function canceledPs(Company $company)
    {
        return StoreOrderAttemp::find()
            ->forCompany($company)
            ->andWhere(['status' => StoreOrderAttemp::STATUS_CANCELED, 'cancel_initiator' => ChangeOrderStatusEvent::INITIATOR_PS])
            ->andWhere(['is not', 'cancelled_at', NULL])
            ->andWhere(['>', 'store_order_attemp.created_at', new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->count();
    }

    public function order(Company $company)
    {
        return StoreOrderAttemp::find()
            ->forCompany($company)
            ->andWhere(['>', 'store_order_attemp.created_at', new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->count();
    }

    public function canceledClient(Company $company)
    {
        return Payment::find()
            ->innerJoinWith(['paymentInvoice.storeOrder.storeOrderAttemps'])
            ->andWhere(['store_order_attemp.ps_id' => $company->id])
            ->andWhere(['store_order_attemp.cancel_initiator' => ChangeOrderStatusEvent::INITIATOR_CLIENT])
            ->andWhere(new Expression('DATEDIFF(store_order_attemp.cancelled_at, store_order_attemp.created_at) > 1'))
            ->andWhere(['payment_invoice.status' => [PaymentInvoice::STATUS_AUTHORIZED, PaymentInvoice::STATUS_VOID, PaymentInvoice::STATUS_REFUNDED, PaymentInvoice::STATUS_CANCELED, PaymentInvoice::STATUS_PAID]])
            ->andWhere(['>', 'store_order.created_at', new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->count();
    }
}