<?php


namespace common\modules\company\repositories;


use common\models\User;
use yii\db\Expression;
use yii\db\Query;

class ResponseTimeCalculator
{
    /**
     * @return array
     */
    public function calc(): array
    {
        $query = new Query();
        $query->select(['id' => 'p.id', 'hours' => new Expression($this->mergeHours())]);
        $query->from(['mm' => 'msg_message']);
        $query->innerJoin(['p' => 'ps'], 'p.user_id = mm.user_id');
        $query->andWhere(['mm.topic_id' => $this->filterTopics()]);
        $query->groupBy('p.id');
        return $query->all();
    }

    protected function mergeHours(): string
    {
        return 'GROUP_CONCAT(TIMESTAMPDIFF(HOUR ,(select created_at from msg_message where created_at < mm.created_at
		and user_id <> mm.user_id and topic_id = mm.topic_id order by created_at  limit 1),mm.created_at))';
    }

    /**
     * @return Query
     */
    protected function filterTopics(): Query
    {
        $query = (new Query())->from(['mm' => 'msg_message']);
        $query->select(new Expression('distinct mm.topic_id'));
        $query->innerJoin(['mm2' => 'msg_message'], 'mm2.topic_id = mm.topic_id and mm.user_id <> mm2.user_id');
        $query->andWhere(['<>', 'mm.user_id',  User::USER_ID_SUPPORT]);
        $query->andWhere(['<>', 'mm2.user_id', User::USER_ID_SUPPORT]);
        return $query;
    }
}