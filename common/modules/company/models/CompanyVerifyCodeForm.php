<?php
/**
 * User: nabi
 */

namespace common\modules\company\models;


use yii\base\Model;

class CompanyVerifyCodeForm extends Model
{
    public $companyId;
    public $code;

    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string'],
            [['companyId'], 'number'],
        ];
    }
}