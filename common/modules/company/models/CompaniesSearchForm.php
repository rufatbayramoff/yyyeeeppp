<?php
/**
 * User: nabi
 */

namespace common\modules\company\models;


use common\components\BaseForm;
use common\models\CompanyServiceCategory;

class CompaniesSearchForm extends BaseForm
{
    const SORT_ID = 'id';
    const SORT_TITLE = 'title';
    const DEFAULT_CATEGORY = 'root';
    const DEFAULT_CATEGORY_ID = 1;

    public $category;
    public $search;

    /**
     * @var string
     */
    public $sort = 'id';

    /**
     * @var int
     */
    public $perPage = 15;

    /**
     * @var string
     */
    public $filter;


    public function rules()
    {
        return [
            [['category', 'search', 'filter',  'search', 'sort'], 'safe'],
            [['perPage'], 'number'],
            [['sort'], 'in', 'range' => [self::SORT_ID, self::SORT_TITLE]]
        ];
    }

    public function load($data, $formName = null)
    {
        $result = parent::load($data, $formName);
        if($data && !empty($data['location'])){
            $this->category = $data['location'];
        }
        return $result;
    }

    public function formName()
    {
        return '';
    }
    private function parseFilterUrl($filter)
    {
        $result = [];
        $parts = explode("--", $filter);
        $parts = array_filter($parts);
        if (count($parts) === 0) {
            return $result;
        }
        foreach ($parts as $part) {
            $partRow = explode('-', $part, 2);
            if (count($partRow) === 1) {
                return []; // not valid filter
            }
            $result[$partRow[0]] = $partRow[1];
        }
        return $result;
    }
    public function parseRequestParams($filter, $getQueryParams)
    {
        return array_merge($this->parseFilterUrl($filter), $getQueryParams);
    }

    public function getCategory()
    {
        if (empty($this->category)) {
            return null;
        }
        $categoriesPath = explode('--', $this->category);
        $lastCategory = end($categoriesPath);
        $defaultCategory = CompanyServiceCategory::findByPk(self::DEFAULT_CATEGORY_ID);
        if ($lastCategory == self::DEFAULT_CATEGORY) {
            return $defaultCategory;
        }
        return $defaultCategory->children()->andWhere(['slug'=>$lastCategory])->one();
    }
}