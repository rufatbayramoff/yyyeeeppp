<?php

namespace common\modules\company\populators;

use common\components\AbstractPopulator;
use common\models\CompanyBlock;
use yii\helpers\HtmlPurifier;
use yii\helpers\Json;

class CompanyBlockPopulator extends AbstractPopulator
{

    /**
     * @param CompanyBlock $companyBlock
     * @param $data
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function populate(CompanyBlock $companyBlock, $data)
    {
        $formName = $companyBlock->formName();
        if (array_key_exists($formName, $data)) {
            $productForm = $data[$formName];
            if ($productForm) {
                $this->loadAttributes($companyBlock, $productForm);
                if (array_key_exists('imageFiles', $productForm)) {
                    $this->populateFileAttributes(
                        $productForm['imageFiles'],
                        function ($uuid) use ($companyBlock) {
                            return $companyBlock->getImageFileByUuid($uuid);
                        }
                    );
                }
            }
        }
        return $this;
    }


    public function loadAttributes(CompanyBlock $companyBlock, $data)
    {
        $this->populateAttributes(
            $companyBlock,
            $data,
            [
                'title',
                'is_visible',
                'is_public_profile',
                'position',
                'content',
            ]
        );
        if (!empty($data['videos'])) {
            $companyBlock->setCompanyVideos((array)Json::decode($data['videos']));
        }
        if (!empty($data['content'])) {
            $companyBlock->content = HtmlPurifier::process($data['content']);
        }
    }
}