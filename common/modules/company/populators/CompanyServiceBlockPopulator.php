<?php
/**
 * User: nabi
 */

namespace common\modules\company\populators;


use common\components\AbstractPopulator;
use common\components\DateHelper;
use common\models\Product;
use common\models\CompanyServiceBlock;
use common\models\SiteTag;
use common\modules\dynamicField\models\populators\DynamicFieldValuePopulator;
use common\modules\product\interfaces\ProductInterface;
use yii\helpers\HtmlPurifier;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 * Class CompanyServiceBlockPopulator
 * @package common\modules\company\populators
 */
class CompanyServiceBlockPopulator extends AbstractPopulator
{

    /**
     * @param CompanyServiceBlock $companyServiceBlock
     * @param $data
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function populate(CompanyServiceBlock $companyServiceBlock, $data)
    {
        $formName = $companyServiceBlock->formName();
        if (array_key_exists($formName, $data)) {
            $productForm = $data[$formName];
            if ($productForm) {
                $this->loadAttributes($companyServiceBlock, $productForm);
                if (array_key_exists('imageFiles', $productForm)) {
                    $this->populateFileAttributes(
                        $productForm['imageFiles'],
                        function ($uuid) use ($companyServiceBlock) {
                            return $companyServiceBlock->getImageFileByUuid($uuid);
                        }
                    );
                }
            }
        }
        return $this;
    }

    /**
     * @param CompanyServiceBlock $companyServiceBlock
     * @param $data
     */
    public function loadAttributes(CompanyServiceBlock $companyServiceBlock, $data)
    {
        $this->populateAttributes(
            $companyServiceBlock,
            $data,
            [
                'title',
                'is_visible',
                'position',
                'content',
            ]
        );
        if (!empty($data['content'])) {
            $companyServiceBlock->content = HtmlPurifier::process($data['content']);
        }
    }
}