<?php
/**
 * User: nabi
 */

namespace common\modules\company\populators;


use common\components\AbstractPopulator;
use common\models\CompanyService;
use common\models\SiteTag;
use yii\helpers\HtmlPurifier;
use yii\helpers\Json;

class CompanyServicePopulator extends AbstractPopulator
{

    /**
     * @param $formName
     * @return bool
     */
    private function checkFileUploads($formName)
    {
        if (!$formName) {
            $formName = 'CompanyService';
        }
        return array_key_exists($formName, $_FILES) && array_key_exists('images', $_FILES[$formName]['name']);
    }

    /**
     * @param CompanyService $companyService
     * @param $data
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function populate(CompanyService $companyService, $data, $formName = false)
    {
        $companyServiceForm = $formName ? $data[$formName] : $data;
        if ($companyServiceForm) {
            $this->loadAttributes($companyService, $companyServiceForm);
            if (array_key_exists('tags', $companyServiceForm)) {
                $this->loadSiteTags($companyService, $companyServiceForm['tags']);
            }

            if (!array_key_exists('images', $companyServiceForm) && ($this->checkFileUploads($formName))) {
                $companyServiceForm['images'] = [];
            }
            // we need to specify class name for images
            if (array_key_exists('images', $companyServiceForm)) {
                $this->loadImages($companyService, $companyServiceForm['images']);
            }
            if (array_key_exists($companyService->formName(), $companyServiceForm) &&
                array_key_exists('images', $companyServiceForm[$companyService->formName()])) {
                $this->loadImages($companyService, $companyServiceForm[$companyService->formName()]['images']);
            }

            if (array_key_exists('submitMode', $companyServiceForm)) {
                $submitMode = $companyServiceForm['submitMode'];
                if ($submitMode === 'publish') {
                    $companyService->moderator_status = $companyService->getModeratorStatusAfterUpdate();
                    $companyService->visibility       = CompanyService::VISIBILITY_EVERYWHERE;
                } elseif ($submitMode === 'unpublish') {
                    $companyService->visibility = CompanyService::VISIBILITY_NOWHERE;
                }
            }
        }

        return $this;
    }

    /**
     * @param CompanyService $companyService
     * @param $data
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function loadAttributes(CompanyService $companyService, $data)
    {
        $this->populateAttributes(
            $companyService,
            $data,
            [
                'title',
                'category_id',
                'single_price',
                'supply_ability',
                'supply_ability_time',
                'avg_production',
                'avg_production_time',
                'product_package',
                'outer_package',
                'incoterms',
                'unit_type',
            ]
        );
        if (!empty($data['shipFrom'])) {
            $companyService->setShipFromLocation($data['shipFrom']);
        }
        if (!empty($data['productPrices'])) {
            $companyService->setMoqPrices($data['productPrices']);
        }
        if (array_key_exists('customProperties', $data)) {
            $companyService->setCustomProperties($data['customProperties']);
        }
        if (array_key_exists('videos', $data)) {
            $companyService->setProductVideos($data['videos']);
        }
        if (array_key_exists('description', $data)) {
            $companyService->description = HtmlPurifier::process($data['description']);
        }
    }

    /**
     * @param CompanyService $companyService
     * @param $images
     */
    public function loadImages(CompanyService $companyService, $images)
    {
        $existsImages = $companyService->companyServiceImages;
        foreach ($existsImages as $imageBind) {
            $imageBind->file->forDelete = true;
        }
        foreach ($existsImages as $imageBind) {
            foreach ($images as $imagePos => $image) {
                if (\is_array($image) && array_key_exists('id', $image) && $imageBind->file->id == $image['id']) {
                    $imageBind->pos             = $imagePos;
                    $imageBind->file->forDelete = false;
                    if (array_key_exists('fileOptions', $image) && array_key_exists('rotate', $image['fileOptions'])) {
                        $imageBind->rotate = $image['fileOptions']['rotate'];
                    }
                }
            }
        }
    }

    /**
     * @param CompanyService $companyService
     * @param $tags
     */
    public function loadSiteTags(CompanyService $companyService, $tags)
    {
        if (!$tags) {
            $tags = [];
        }
        $siteTags       = [];
        $productTagsNew = [];
        foreach ($tags as $productTag) {
            $productTagsSep = $productTag;
            if (!is_array($productTagsSep)) {
                $productTagsSep = explode(",", $productTag);
            }
            if ($productTagsSep) {
                foreach ($productTagsSep as $p) {
                    $productTagsNew[] = $p;
                }
            }
        }
        foreach ($productTagsNew as $productTag) {
            $productTag = trim($productTag);
            if (ctype_digit($productTag)) { // WTF: Form send id or text!
                $siteTag = SiteTag::find()->where(['id' => intval($productTag)])->withoutStaticCache()->one();
            } else {
                if (strlen($productTag) < 2) {
                    continue;
                }
                $siteTag = SiteTag::find()->where(['text' => $productTag])->withoutStaticCache()->one();
            }
            if (!$siteTag) {
                $siteTag       = new SiteTag();
                $siteTag->text = $productTag;
            }
            $siteTags[] = $siteTag;
        }
        $companyService->populateRelation('tags', $siteTags);
    }


    /**
     * @param CompanyService $companyService
     * @param $data
     * @param bool $formName
     *
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function populateImagesRotateOptions(CompanyService $companyService, $data, $formName = false)
    {
        $companyServiceForm = $formName ? $data[$formName] : $data;

        if ($companyServiceForm
            && array_key_exists($companyService->formName(), $companyServiceForm)
            && array_key_exists('imagesOptions', $companyServiceForm[$companyService->formName()])
        ) {
            $images        = $companyService->companyServiceImages;
            $imagesOptions = $companyServiceForm[$companyService->formName()]['imagesOptions'];
            foreach ($images as $image) {
                if (!empty($imagesOptions[$image->pos]) && array_key_exists('rotate', $imagesOptions[$image->pos])) {
                    $image->rotate = $imagesOptions[$image->pos]['rotate'];
                }
            }

        }

        return $this;
    }

}