<?php

namespace common\modules\cutting\services;

use Braintree\Base;
use common\models\CuttingMachine;
use common\models\CuttingMachineProcessing;
use common\models\CuttingPack;
use common\models\CuttingPackPart;
use common\models\CuttingWorkpieceMaterial;
use common\models\user\UserIdentityProvider;
use common\modules\cutting\models\CutSize;
use common\modules\cutting\models\CuttingPartMaterialInfo;
use common\modules\payment\fee\FeeHelper;
use lib\money\Money;
use lib\money\MoneyMath;
use yii\base\BaseObject;

class CuttingOfferPriceCalculator extends BaseObject
{
    /**
     * @param UserIdentityProvider $userIdentityProvider
     */
//    public function injectDependencies(UserIdentityProvider $userIdentityProvider, CuttingRepository $cuttingMachineRepository)
//    {
//        $this->userIdentityProvider     = $userIdentityProvider;
//    }
    public const MIN_WORKPIECE_AREA = 0.7;

    /** @var FeeHelper */
    protected $feeHelper;

    /**
     * @param FeeHelper $feeHelper
     */
    public function injectDependencies(
        FeeHelper $feeHelper
    )
    {
        $this->feeHelper = $feeHelper;
    }


    /**
     * @param CuttingMachine $cuttingMachine
     * @param CuttingPartMaterialInfo $cuttingPartMaterialInfo
     * @param CuttingPackPart[] $partsInSameMaterial
     * @return array|null
     */
    public function getWorkpieceMaterialForSameMaterialParts(CuttingMachine $cuttingMachine, CuttingPartMaterialInfo $cuttingPartMaterialInfo, $partsInSameMaterial): ?array
    {
        $cuttingPackArea    = 0;
        $cuttingPackMaxSize = CutSize::zero();

        foreach ($partsInSameMaterial as $cuttingPart) {
            $cuttingPackArea += $cuttingPart->getArea()*$cuttingPart->qty;
            if ($cuttingPart->getSize()->isMoreOrEqualThen($cuttingPackMaxSize)) {
                $cuttingPackMaxSize = $cuttingPart->getSize();
            }
        }

        $workpieceMaterial = null;
        $processing        = null;
        foreach ($cuttingMachine->cuttingMachineProcessings as $processing) {
            $workpieceMaterial = $processing->getMinWorkpieceMaterial($cuttingPackMaxSize, $cuttingPartMaterialInfo);
            if ($workpieceMaterial) {
                break;
            }
        }
        if (!$workpieceMaterial) {
            return null;
        }
        $workpieceMaterialAreaPercent = $cuttingPackArea / $workpieceMaterial->getArea();
        $workpieceMaterialCount       = ceil($workpieceMaterialAreaPercent);
        $info                         = [
            $processing,
            $workpieceMaterial,
            $workpieceMaterialCount * $workpieceMaterial->price,
        ];
        return $info;
    }

    /**
     * @param CuttingPackPart[] $cuttingParts
     */
    protected function calculateLength($cuttingParts)
    {
        $engravingLength = 0;
        $cuttingLength   = 0;
        foreach ($cuttingParts as $cuttingPart) {
            $engravingLength += $cuttingPart->engraving_length*$cuttingPart->qty;
            $cuttingLength   += $cuttingPart->cutting_length*$cuttingPart->qty;
        }
        return [$engravingLength, $cuttingLength];
    }

    public function calculatePrice(CuttingMachine $cuttingMachine, CuttingPack $cuttingPack, $includeTreatstockFee = true)
    {
        // Group parts by material, thickness, color
        $partsInSameMaterial                      = $cuttingPack->getActiveCuttingParts(); // Now all parts in same material
        $cuttingPartMaterialInfo                  = new CuttingPartMaterialInfo();
        $cuttingPartMaterialInfo->cuttingMaterial = $cuttingPack->material;
        $cuttingPartMaterialInfo->thickness       = $cuttingPack->thickness;
        $cuttingPartMaterialInfo->cuttingColor    = $cuttingPack->color;

        /** @var $processing CuttingMachineProcessing */
        /** @var $workpieceMaterial CuttingWorkpieceMaterial */


        $workpieceMaterialInfo = $this->getWorkpieceMaterialForSameMaterialParts($cuttingMachine, $cuttingPartMaterialInfo, $partsInSameMaterial);
        if (!$workpieceMaterialInfo) {
            return null;
        }
        [$processing, $workpieceMaterial, $workpieceMaterialPrice] = $workpieceMaterialInfo;
        [$engravingLength, $cuttingLength] = $this->calculateLength($partsInSameMaterial);
        $engravingPrice = $processing->engraving_price * $engravingLength;
        $cuttingPrice   = $processing->cutting_price * $cuttingLength;
        if (!$cuttingPrice) {
            return null;
        }
        $totalPrice     = $workpieceMaterialPrice + $engravingPrice + $cuttingPrice;

        if ($totalPrice<$cuttingMachine->min_order_price) {
            $totalPrice = $cuttingMachine->min_order_price;
        }

        $totalPrice = Money::create($totalPrice, $cuttingMachine->companyService->company->currency);

        if ($totalPrice && $includeTreatstockFee) {
            $feePrice   = $this->feeHelper->getPrintFeeFromPrice($totalPrice);
            $totalPrice = MoneyMath::sum($totalPrice, $feePrice);
        }

        return $totalPrice;
    }
}