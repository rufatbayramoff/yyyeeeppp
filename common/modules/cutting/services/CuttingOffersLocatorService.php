<?php

namespace common\modules\cutting\services;

use common\components\helpers\PeliabilityHelper;
use common\components\helpers\ResponseTimeHelper;
use common\models\Company;
use common\models\CompanyService;
use common\models\CuttingMachine;
use common\models\CuttingMachineProcessing;
use common\models\CuttingPack;
use common\models\GeoCountry;
use common\models\user\UserIdentityProvider;
use common\models\UserAddress;
use common\modules\cutting\models\CutSize;
use common\modules\cutting\models\OffersBundle;
use common\modules\cutting\repositories\CuttingRepository;
use common\modules\payment\components\BonusCalculator;
use common\modules\payment\fee\FeeHelper;
use common\modules\printersList\models\AnswerInfo;
use common\services\ReviewService;
use frontend\controllers\catalog\actions\models\Rating;
use lib\delivery\delivery\DeliveryService;
use lib\delivery\parcel\ParcelFactory;
use Yii;
use yii\base\BaseObject;

class CuttingOffersLocatorService extends BaseObject
{

    /**
     * @var bool
     */
    public $allowedInternational = false;

    /**
     * @var \lib\geo\models\Location
     */
    public $location;

    /*
     * Filtered by Domestic country id
     */
    public $countryId = GeoCountry::ID_US;


    /**
     * @var UserIdentityProvider
     */
    protected $userIdentityProvider;

    /** @var CuttingRepository */
    protected $cuttingMachineRepository;

    /** @var CuttingOfferPriceCalculator */
    protected $cuttingOfferPriceCalculator;

    /** @var DeliveryService */
    protected $deliveryService;

    /** @var FeeHelper */
    protected $feeHelper;

    /** @var ReviewService */
    protected $reviewService;

    public $manualSettedPsId;

    /**
     * @param UserIdentityProvider $userIdentityProvider
     * @param CuttingRepository $cuttingMachineRepository
     * @param CuttingOfferPriceCalculator $cuttingOfferPriceCalculator
     */
    public function injectDependencies(
        UserIdentityProvider $userIdentityProvider,
        CuttingRepository $cuttingMachineRepository,
        CuttingOfferPriceCalculator $cuttingOfferPriceCalculator,
        DeliveryService $deliveryService,
        FeeHelper $feeHelper,
        ReviewService $reviewService
    )
    {
        $this->userIdentityProvider        = $userIdentityProvider;
        $this->cuttingMachineRepository    = $cuttingMachineRepository;
        $this->cuttingOfferPriceCalculator = $cuttingOfferPriceCalculator;
        $this->deliveryService             = $deliveryService;
        $this->reviewService             = $reviewService;
        $this->feeHelper                   = $feeHelper;
    }


    public function setLocation(\lib\geo\models\Location $location)
    {
        $this->location  = $location;
        $this->countryId = $this->location->geoCountry->id;
    }

    /**
     * @param CuttingMachine[] $cuttingMachines
     * @param CutSize $size
     */
    protected function filterBySize(array $cuttingMachines, CutSize $size)
    {
        $filteredMachines = [];
        foreach ($cuttingMachines as $cuttingMachine) {
            if (!$cuttingMachine->getWorkSize()->isMoreOrEqualThen($size)) {
                continue;
            }
            $workpieceMaterialExists = false;

            foreach ($cuttingMachine->cuttingMachineProcessings as $processing) {
                if ($processing->cutting_price > CuttingMachineProcessing::MIN_CUTTING_LEN && $processing->getMinWorkpieceMaterial($size)) {
                    $workpieceMaterialExists = true;
                    break;
                }
            }
            if (!$workpieceMaterialExists) {
                continue;
            }
            $filteredMachines[] = $cuttingMachine;
        }
        return $filteredMachines;
    }

    /**
     * @param CuttingMachine[] $cuttingMachines
     * @param CutSize $size
     * @return array
     */
    protected function formAvailableMaterials(array $cuttingMachines, CutSize $size): array
    {
        $allowedMaterials = [];
        foreach ($cuttingMachines as $cuttingMachine) {
            foreach ($cuttingMachine->cuttingMachineProcessings as $processing) {
                if (!$processing->getCuttingPricePerM()) {
                    continue;
                }
                foreach ($processing->getWorkpieceMaterials($size) as $workpieceMaterial) {
                    if (!array_key_exists($workpieceMaterial->material->id, $allowedMaterials)) {
                        $allowedMaterials[$workpieceMaterial->material->id] = [
                            'material'  => $workpieceMaterial->material,
                            'thickness' => [
                                $workpieceMaterial->thickness => [
                                    'thickness' => $workpieceMaterial->thickness,
                                    'colors'    => [
                                        $workpieceMaterial->color_id => $workpieceMaterial->color
                                    ]
                                ]
                            ]
                        ];
                    }
                    if (!array_key_exists($workpieceMaterial->thickness, $allowedMaterials[$workpieceMaterial->material->id]['thickness'])) {
                        $allowedMaterials[$workpieceMaterial->material->id]
                        ['thickness']
                        [$workpieceMaterial->thickness] = [
                            'thickness' => $workpieceMaterial->thickness,
                            'colors'    => [
                                $workpieceMaterial->color_id => $workpieceMaterial->color
                            ]
                        ];
                    }
                    $allowedMaterials[$workpieceMaterial->material->id]
                    ['thickness']
                    [$workpieceMaterial->thickness]
                    ['colors']
                    [$workpieceMaterial->color_id] = $workpieceMaterial->color;
                }
            }
        }
        foreach ($allowedMaterials as $materialId=>$materialInfo) {
            asort($materialInfo['thickness']);
            $allowedMaterials[$materialId]['thickness'] = $materialInfo['thickness'];
        }
        return $allowedMaterials;
    }

    /**
     * @param CompanyService $companyService
     * @return string
     */
    protected function getAddressTemplate(CompanyService $companyService): string
    {
        return $this->location->country != $companyService->location->country->iso_code
            ? '%city%, %region%, %country%'
            : '%city%, %region%';
    }

    /**
     * @param CuttingMachine[] $cuttingMachines
     * @param CutSize $size
     * @return array
     */
    protected function formOffers(array $cuttingMachines, CuttingPack $cuttingPack): array
    {
        $parcel = ParcelFactory::createFromCuttingPack($cuttingPack);

        $offers = [];
        foreach ($cuttingMachines as $cuttingMachine) {
            $manufacturePrice = $this->cuttingOfferPriceCalculator->calculatePrice($cuttingMachine, $cuttingPack);

            if (!$manufacturePrice || !$manufacturePrice->getAmount()) {
                continue;
            }

            if ($cuttingMachine->companyService->company->cashback_percent) {
                if ($this->manualSettedPsId) {
                    $tsBonusAmount = BonusCalculator::calcInstantOrderDirectService($cuttingMachine->companyService->company, $manufacturePrice->getAmount(), $cuttingPack->user);
                } else {
                    $tsBonusAmount = BonusCalculator::calcInstantOrder($cuttingMachine->companyService->company->cashback_percent, $manufacturePrice->getAmount(), $cuttingPack->user);
                }
            } else {
                $tsBonusAmount = null;
            }

            [$estimateDeliveryCost, $deliveryTypes, $packingPrice] = $this->deliveryService->resolveDeliveryRateAndLabels(
                $cuttingMachine->companyService,
                $manufacturePrice,
                $this->location,
                $parcel
            );

            // TBC, FREE, deliveryPrice.amount,
            // allowPickup
            $deliveryPrice = $estimateDeliveryCost ?? 'TBC';
            $allowPickup   = false;

            $offer                       = [
                'serviceMachine'   => [
                    'id'             => $cuttingMachine->id,
                    'title'          => $cuttingMachine->companyService->getTitleLabel(),
                    'technology'     => $cuttingMachine->getTechnologyLabel(),
                    'publicLink'     => $cuttingMachine->companyService->getPublicPageUrl(),
                    'companyLogoUrl' => $cuttingMachine->companyService->company->getLogoUrl(),
                    'companyTitle'   => $cuttingMachine->companyService->company->getTitle(),
                    'address'        => UserAddress::formatMachineAddress($cuttingMachine->companyService, $this->getAddressTemplate($cuttingMachine->companyService)),
                    'topService'     => $cuttingMachine?->companyService->company?->psCatalog?->country_rating ?? 0,
                    'peliability'    => PeliabilityHelper::format($cuttingMachine?->companyService->company?->psCatalog->conversion()),
                    'responseTime'   => ResponseTimeHelper::response($cuttingMachine?->companyService?->company?->response_time),
                    'reviewsInfo'    => Rating::create(
                        $this->reviewService->getCompanyServiceShowedReviewsCount($cuttingMachine->companyService),
                        $this->reviewService->calculateCompanyServiceRating($cuttingMachine->companyService)
                    ),

                ],
                'tsBonusAmount'    => $tsBonusAmount,
                'manufacturePrice' => $manufacturePrice,
                'packingPrice'     => $packingPrice,
                'deliveryPrice'    => $deliveryPrice,
                'allowPickup'      => $allowPickup,
            ];
            $offers[$cuttingMachine->id] = $offer;
        }
        return $offers;
    }

    public function formOffersBundle(CuttingPack $cuttingPack)
    {
        $offersBundle = Yii::createObject(OffersBundle::class);

        if (!$cuttingPack->isParsed() || !$cuttingPack->getActiveCuttingParts()) {
            $offersBundle->emptyReason = AnswerInfo::EMPTY_OFFERS_LIST_NO_CUTTING_PARTS;
            return $offersBundle;
        }

        $largestPart = $cuttingPack->getMaxPartSize();

        $cuttingMachineQuery = CuttingMachine::find()->availableForOffers();
        if ($this->allowedInternational) {
            $cuttingMachineQuery->international($this->countryId);
        } else {
            $cuttingMachineQuery->domestic($this->countryId);
        }
        if ($this->manualSettedPsId) {
            $cuttingMachineQuery->company(Company::tryFindByPk($this->manualSettedPsId));
            if (!$cuttingMachineQuery->count()) {
                $offersBundle->emptyReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_NO_DELIVERY;
                return $offersBundle;
            }
        }

        if (!$cuttingMachineQuery->count()) {
            $offersBundle->emptyReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_NO_ACTIVE_MACHINES;
            return $offersBundle;
        }
        $cuttingMachines = $cuttingMachineQuery->all();
        $cuttingMachines = $this->filterBySize($cuttingMachines, $largestPart);
        if (!$cuttingMachines) {
            $offersBundle->emptyReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_LARGE_SIZE;
            return $offersBundle;
        }
        $offersBundle->allowedMaterialsColors = $this->formAvailableMaterials($cuttingMachines, $largestPart);
        $offersBundle->offers                 = $this->formOffers($cuttingMachines, $cuttingPack);
        $offersBundle->totalCount             = count($offersBundle->offers);

        return $offersBundle;
    }


}
