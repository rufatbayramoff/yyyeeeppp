<?php

namespace common\modules\cutting\services;

use common\components\FileTypesHelper;
use common\components\UuidHelper;
use common\models\CuttingMachine;
use common\models\CuttingPack;
use common\models\CuttingPackFile;
use common\models\File;
use common\models\repositories\UserSessionRepository;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\user\UserIdentityProvider;
use common\modules\cutting\factories\CuttingPackFactory;
use common\modules\cutting\models\CuttingConvertJob;
use common\modules\cutting\populators\CuttingPackPopulator;
use common\modules\cutting\repositories\CuttingPackRepository;
use common\modules\cutting\repositories\CuttingRepository;
use frontend\models\delivery\DeliveryForm;
use frontend\models\user\UserFacade;
use lib\delivery\delivery\models\DeliveryRate;
use lib\delivery\parcel\ParcelFactory;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

class CuttingPackService extends BaseObject
{
    /**
     * @var UserIdentityProvider
     */
    protected $userIdentityProvider;

    /** @var UserSessionRepository */
    protected $userSessionRepository;

    /** @var CuttingRepository */
    protected $cuttingMachineRepository;

    /** @var CuttingPackFactory */
    protected $cuttingPackFactory;

    /** @var CuttingPackRepository */
    protected $cuttingPackRepository;

    /** @var CuttingPackPopulator */
    protected $cuttingPackPopulator;

    /** @var CuttingOfferPriceCalculator */
    protected $cuttingOfferPriceCalculator;

    /**
     * @param UserIdentityProvider $userIdentityProvider
     * @param CuttingRepository $cuttingMachineRepository
     * @param CuttingPackFactory $cuttingPackFactory
     * @param CuttingPackRepository $cuttingPackRepository
     * @param CuttingPackPopulator $cuttingPackPopulator
     * @param CuttingOfferPriceCalculator $cuttingOfferPriceCalculator
     */
    public function injectDependencies(
        UserIdentityProvider $userIdentityProvider,
        UserSessionRepository $userSessionRepository,
        CuttingRepository $cuttingMachineRepository,
        CuttingPackFactory $cuttingPackFactory,
        CuttingPackRepository $cuttingPackRepository,
        CuttingPackPopulator $cuttingPackPopulator,
        CuttingOfferPriceCalculator $cuttingOfferPriceCalculator
    )
    {
        $this->userIdentityProvider        = $userIdentityProvider;
        $this->userSessionRepository       = $userSessionRepository;
        $this->cuttingMachineRepository    = $cuttingMachineRepository;
        $this->cuttingPackFactory          = $cuttingPackFactory;
        $this->cuttingPackRepository       = $cuttingPackRepository;
        $this->cuttingPackPopulator        = $cuttingPackPopulator;
        $this->cuttingOfferPriceCalculator = $cuttingOfferPriceCalculator;
    }

    public function validateFile(File $file)
    {
        if (FileTypesHelper::isImage($file)) {
            return true;
        }
        if (FileTypesHelper::isFileInExt($file, CuttingPack::ALLOWED_FORMATS)) {
            return true;
        }
        throw new InvalidArgumentException('Not allowed file format');
    }

    /**
     * @param CuttingPack $cuttingPack
     * @param File $file
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @throws \yii\db\StaleObjectException
     */
    public function addFile(CuttingPack $cuttingPack, File $file, $uuid)
    {
        $cuttingPackFiles   = $cuttingPack->cuttingPackFiles;
        foreach ($cuttingPackFiles as $cuttingPackFile) {
            if ($cuttingPackFile->file->md5sum === $file->md5sum) {
                $cuttingPackFile->is_active = 1;
                $cuttingPackFile->safeSave();
                return;
            }
        }
        $cuttingPackFile    = $this->cuttingPackFactory->createCuttingPackFile($cuttingPack, $file, $uuid);
        $cuttingPackFiles[] = $cuttingPackFile;
        $cuttingPack->populateRelation('cuttingPackFiles', $cuttingPackFiles);
        $this->cuttingPackRepository->save($cuttingPack);
        if ($cuttingPackFile->isModel()) {
            CuttingConvertJob::addConvertJob($cuttingPackFile);
        }
    }

    public function cancelPackFile(CuttingPackFile $cuttingPackFile)
    {
        $cuttingPackFile->is_active = 0;
        $cuttingPackFile->safeSave();
    }

    public function updateFileQty(CuttingPack $cuttingPack, $uid, $qty): ?CuttingPackFile
    {
        foreach ($cuttingPack->cuttingPackFiles as $cuttingPackFile) {
            if ($cuttingPackFile->uuid === $uid) {
                $cuttingPackFile->qty = $qty;
                $cuttingPackFile->safeSave();
                return $cuttingPackFile;
            }
        }
        return null;
    }

    /**
     * @param CuttingPack $cuttingPack
     * @param $uid
     * @param $md5
     */
    public function cancelFileByUidOrMd5(CuttingPack $cuttingPack, $uid, $md5)
    {
        foreach ($cuttingPack->cuttingPackFiles as $cuttingPackFile) {
            if (($cuttingPackFile->uuid == $uid) || ($cuttingPackFile->file->getMd5NameSize() === $md5)) {
                $this->cancelPackFile($cuttingPackFile);
            }
        }
    }


    public function restorePackFile(CuttingPackFile $cuttingPackFile)
    {
        $cuttingPackFile->is_active = 1;
        $cuttingPackFile->safeSave();
    }

    /**
     * @param CuttingPack $cuttingPack
     * @param $uid
     * @param $md5
     */
    public function restoreFileByUidOrMd5(CuttingPack $cuttingPack, $uid, $md5)
    {
        foreach ($cuttingPack->cuttingPackFiles as $cuttingPackFile) {
            if ($cuttingPackFile->uuid === $uid) {
                $this->restorePackFile($cuttingPackFile);
                break;
            }
        }
    }

    public function saveFileParts(CuttingPack $cuttingPack, $cuttingPageInfo)
    {
        $this->cuttingPackPopulator->populatePackPage($cuttingPack, $cuttingPageInfo);
        $this->cuttingPackRepository->save($cuttingPack);
        return $cuttingPack;
    }

    public function saveCuttingPackMaterialInfo(CuttingPack $cuttingPack, $cuttingPackMaterilaInfo)
    {
        $this->cuttingPackPopulator->populateMaterialInfo($cuttingPack, $cuttingPackMaterilaInfo);
        $this->cuttingPackRepository->save($cuttingPack);
        return $cuttingPack;
    }

    public function getOrderRateCached(CuttingMachine $cuttingMachine, CuttingPack $cuttingPack, DeliveryForm $deliveryForm): ?DeliveryRate
    {
        $parcel  = ParcelFactory::createFromCuttingPack($cuttingPack);
        $formKey = $deliveryForm->hashKey();
        $key     = md5('cutting_rate' . $cuttingMachine->id . $cuttingMachine->companyService->getDeliveryOptionsHash() . $formKey . serialize($parcel->attributes));
        if ($shippingCache = app('cache')->get($key)) {
            return $shippingCache;
        }

        $parcel       = ParcelFactory::createFromCuttingPack($cuttingPack);
        $cuttingPrice = $this->cuttingOfferPriceCalculator->calculatePrice($cuttingMachine, $cuttingPack);
        $deliveryRate = Yii::$app->deliveryService->getOrderRate($cuttingMachine->companyService->asDeliveryParams(), $deliveryForm, $cuttingPrice, $parcel);
        app('cache')->set($key, $deliveryRate);
        return $deliveryRate;
    }

    /**
     * @param CuttingPack $cuttingPack
     * @param StoreOrderAttemp $attemp
     * @return bool
     */
    public function isCuttingByAttempt(CuttingPack $cuttingPack, StoreOrderAttemp $attemp)
    {
        foreach ($cuttingPack->storeOrderItems as $storeOrderItem) {
            if ($storeOrderItem->order->currentAttemp->id === $attemp->id) {
                return true;
            }
        }
        return false;
    }


    public function allowViewForCurrentUser(CuttingPack $cuttingPack)
    {
        $user        = $this->userIdentityProvider->getUser();
        $userSession = $this->userSessionRepository->getCurrent();
        if (UserFacade::isObjectOwner($cuttingPack, $user, $userSession)) {
            // You is author
            return true;
        }
        if ($user) {
            foreach ($cuttingPack->storeOrderItems as $storeOrderItem) {
                if ($storeOrderItem->order->currentAttemp->ps->user_id === $user->id) {
                    // You is ps producing order
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param File[] $files
     */
    public function createCuttingPackByFilesList($files): CuttingPack
    {
        $cuttingPack = $this->cuttingPackFactory->createForCurrentUser();
        foreach ($files as $file) {
            $this->addFile($cuttingPack, $file, UuidHelper::generateUuid());
        }
        return $cuttingPack;
    }

    /**
     * @param StoreOrder $order
     * @return CuttingPack
     * @throws \Exception
     */
    public function duplicate(StoreOrder $order): CuttingPack
    {
        $pack = $order->getCuttingPack();
        $cuttingPackFiles = $pack->cuttingPackFiles;
        $files = array_map(function(CuttingPackFile $cuttingPackFile){
            $file = new File();
            $file->attributes = $cuttingPackFile->file->attributes;
            $file->oldAttributes = $cuttingPackFile->file->oldAttributes;
            $file->uuid = UuidHelper::generateUuid();
            $file->isNewRecord = true;
            unset($file->id);
            return $file;
        }, $cuttingPackFiles);
        return $this->createCuttingPackByFilesList($files);
    }
}
