<?php

namespace common\modules\cutting\services;

use backend\components\NoAccessException;
use common\components\UuidHelper;
use common\models\Company;
use common\models\CompanyService;
use common\models\CuttingMachine;
use common\models\CuttingMachineProcessing;
use common\models\CuttingWorkpieceMaterial;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\modules\cutting\repositories\CuttingRepository;
use Yii;
use yii\base\BaseObject;

class CuttingMachineService extends BaseObject
{
    /**
     * @var UserIdentityProvider
     */
    protected $userIdentityProvider;

    /** @var CuttingRepository */
    protected $cuttingMachineRepository;

    /**
     * @param UserIdentityProvider $userIdentityProvider
     */
    public function injectDependencies(UserIdentityProvider $userIdentityProvider, CuttingRepository $cuttingMachineRepository)
    {
        $this->userIdentityProvider     = $userIdentityProvider;
        $this->cuttingMachineRepository = $cuttingMachineRepository;
    }

    /**
     * @param CuttingMachine $cuttingMachine
     * @param User $currentUser
     * @throws NoAccessException
     */
    public function checkIsAllowedEditForUser(CuttingMachine $cuttingMachine, User $currentUser)
    {
        if ($cuttingMachine->companyService->ps->user->id !== $currentUser->id) {
            throw new NoAccessException('No access for cutting machine');
        }
    }

    /**
     * @param CuttingMachine $cuttingMachine
     * @throws NoAccessException
     */
    public function checkIsAllowEditForCurrentUser(CuttingMachine $cuttingMachine)
    {
        $currentUser = $this->userIdentityProvider->getUser();
        return $this->checkIsAllowedEditForUser($cuttingMachine, $currentUser);
    }

    /**
     * @param CuttingMachine $cuttingMachine
     * @throws NoAccessException
     */
    public function delete(CuttingMachine $cuttingMachine)
    {
        $this->checkIsAllowEditForCurrentUser($cuttingMachine);
        $this->cuttingMachineRepository->delete($cuttingMachine);
    }

    public function addEmptyWorkpieceMaterial(Company $company)
    {
        $materials   = $company->cuttingWorkpieceMaterials;
        $cuttingWorkpieceMaterial = Yii::createObject(CuttingWorkpieceMaterial::class);
        $cuttingWorkpieceMaterial->populateRelation('company', $company);
        $materials[] = $cuttingWorkpieceMaterial;
        $company->populateRelation('cuttingWorkpieceMaterials', $materials);
    }

    public function addEmptyProcessing(CuttingMachine $cuttingMachine): CuttingMachineProcessing
    {
        $processing                            = $cuttingMachine->cuttingMachineProcessings;
        $cuttingProcessing                     = Yii::createObject(CuttingMachineProcessing::class);
        $cuttingProcessing->uuid               = UuidHelper::generateUuid();
        $cuttingProcessing->cutting_machine_id = $cuttingMachine->id;
        $processing[]                          = $cuttingProcessing;
        $cuttingMachine->populateRelation('cuttingMachineProcessings', $processing);
        return $cuttingProcessing;
    }

    public function addEmptyProcessingsForNewMaterials(Company $company)
    {
        foreach ($company->companyServices as $companyService) {
            if ($companyService->isCutting()) {
                $cuttingMachine = $companyService->cuttingMachine;
                if (!$cuttingMachine->cuttingMachineProcessings) {
                    foreach ($company->cuttingWorkpieceMaterials as $workpieceMaterial) {
                        if (!$cuttingMachine->getProcessing($workpieceMaterial->material, $workpieceMaterial->thickness)) {
                            $processing                      = $this->addEmptyProcessing($companyService->cuttingMachine);
                            $processing->cutting_material_id = $workpieceMaterial->material_id;
                            $processing->thickness           = $workpieceMaterial->thickness;
                        }
                    }
                } else {
                    foreach ($company->cuttingWorkpieceMaterials as $workpieceMaterial) {
                        if ($workpieceMaterial->isNewRecord) {
                            if (!$cuttingMachine->getProcessing($workpieceMaterial->material, $workpieceMaterial->thickness)) {
                                $processing                      = $this->addEmptyProcessing($companyService->cuttingMachine);
                                $processing->cutting_material_id = $workpieceMaterial->material_id;
                                $processing->thickness           = $workpieceMaterial->thickness;

                            }
                        }
                    }
                }
            }
        }



    }

    public function isAvailableForOffers(CuttingMachine $cuttingMachine): bool
    {
        return $cuttingMachine->companyService->isAvailableForOffers();
    }
}