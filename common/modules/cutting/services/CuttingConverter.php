<?php

namespace common\modules\cutting\services;

use backend\components\NoAccessException;
use common\components\FileDirHelper;
use common\components\FileTypesHelper;
use common\components\UuidHelper;
use common\models\Company;
use common\models\CuttingMachine;
use common\models\CuttingMachineProcessing;
use common\models\CuttingPack;
use common\models\CuttingPackFile;
use common\models\CuttingPackPageImage;
use common\models\CuttingWorkpieceMaterial;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\repositories\FileRepository;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\modules\cutting\factories\CuttingPackFactory;
use common\modules\cutting\models\CuttingConvertJob;
use common\modules\cutting\repositories\CuttingPackRepository;
use common\modules\cutting\repositories\CuttingRepository;
use common\services\Model3dImageService;
use console\jobs\model3d\RenderJob;
use console\jobs\QueueGateway;
use CURLFile;
use http\Exception\RuntimeException;
use lib\file\UploadException;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

class CuttingConverter extends BaseObject
{
    /** @var FileFactory */
    protected $fileFactory;

    /** @var FileRepository */
    protected $fileRepository;

    /** @var CuttingPackService */
    protected $cuttingService;

    /** @var CuttingPackFactory */
    protected $cuttingFactory;

    /** @var CuttingPackRepository */
    protected $cuttingPackRepository;

    public $showDebugOutput = true;


    public function injectDependencies(
        FileFactory $fileFactory,
        FileRepository $fileRepository,
        CuttingPackService $cuttingService,
        CuttingPackFactory $cuttingFactory,
        CuttingPackRepository $cuttingPackRepository
    )
    {
        $this->fileFactory           = $fileFactory;
        $this->fileRepository        = $fileRepository;
        $this->cuttingService        = $cuttingService;
        $this->cuttingFactory        = $cuttingFactory;
        $this->cuttingPackRepository = $cuttingPackRepository;
    }

    protected function debugOutput($str)
    {
        if ($this->showDebugOutput) {
            echo "\n" . date('Y-m-d H:i:s') . ' ' . $str;
        }
    }

    protected function requestToCorelDraw($filePath)
    {
        $url = param('cuttingCorelDrawConverter');
        //$url = 'http://ts.h3.tsdev.work/test/test-z';
        $this->debugOutput('Upload path: ' . $filePath . ' Size: ' . filesize($filePath) . '  To: ' . $url);

        $ch = curl_init();

        // No sertificate on r1.tsdev.work
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $requestPost = [
            'f' => new CurlFile($filePath)
        ];

        // Binary transfer i.e. --data-BINARY
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestPost);


        $result   = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $resultDir = \Yii::getAlias('@runtime') . '/cuttingConvertResult';
        FileDirHelper::createDir($resultDir);
        $resultTmpPath = $resultDir . '/converted_' . date('Y-m-d_H-i-s') . '_' . rand(0, 10000) . '.zip';
        file_put_contents($resultTmpPath, $result);
        $this->debugOutput('Upload code: ' . $httpCode . ' Result path: ' . $resultTmpPath . ' Result size: ' . filesize($resultTmpPath));
        if ($httpCode === 200) {
            return $resultTmpPath;
        }
        return null;
    }

    protected function unpackResult(CuttingPackFile $packFile, string $resultTmpPath)
    {
        $resultDir = \Yii::getAlias('@runtime') . '/cuttingConvertResult/unpacked_' . date('Y-m-d_H-i-s') . '_' . rand(0, 10000);
        FileDirHelper::createDir($resultDir);

        $za = new \ZipArchive();
        if ($za->open($resultTmpPath) !== true) {
            $this->debugOutput('Answer: '.substr(file_get_contents($resultTmpPath), 0, 200)."\n");
            throw new UploadException(_t('site.upload', 'Cannot open zip file.'));
        }
        $this->debugOutput('Start extract: '.$resultTmpPath."\n");
        $za->extractTo($resultDir);
        $za->close();
        $unpackedSvgList = glob($resultDir . '/*.svg');
        foreach ($unpackedSvgList as $unpackedPath) {
            if (!is_file($unpackedPath)) {
                continue;
            }
            $this->debugOutput('Start append svg: '.$unpackedPath."\n");
            $fileCsv         = $this->fileFactory->createFileFromPath($unpackedPath);
            $currentPages = $packFile->cuttingPackPages;
            foreach ($currentPages as $currentPage) {
                if ($currentPage->file->md5sum === $fileCsv->md5sum) {
                    continue; // Dont add exists files
                }
            }
            $cuttingPackPage = $this->cuttingFactory->createCuttingPackPage($packFile, $fileCsv);
            $packFile->addPage($cuttingPackPage);
            $fileName        = pathinfo($unpackedPath, PATHINFO_FILENAME);
            $unpackedImgList = glob($resultDir . '/' . $fileName . '_Images/*.png');
            foreach ($unpackedImgList as $imagePath) {
                if (!is_file($imagePath)) {
                    continue;
                }
                $this->debugOutput('Start append img: '.$imagePath."\n");
                $fileImg = $this->fileFactory->createFileFromPath($imagePath);
                $this->cuttingService->addFile($packFile->cuttingPack, $fileImg, UuidHelper::generateUuid());

                $packImage = $this->cuttingFactory->createCuttingPackImage($cuttingPackPage, $fileImg);
                $cuttingPackPage->addImage($packImage);
                $imageSvgPath = $fileName . '_Images\\'.pathinfo($imagePath, PATHINFO_BASENAME);
                $content = $fileCsv->getFileContent();
                $content = str_replace($imageSvgPath, CuttingPackPageImage::DOWNLOAD_IMAGE_URL.'?uuid='.$packImage->uuid, $content);
                $fileCsv->setFileContentAndSave($content);
            }

        }
        $packFile->is_converted = 1;
        $this->cuttingPackRepository->save($packFile->cuttingPack);
        $this->debugOutput('Finish extract: '.$resultTmpPath."\n");
    }

    public function convert(CuttingPackFile $packFile)
    {
        $resultTmpPath = $this->requestToCorelDraw($packFile->file->getLocalTmpFilePath());
        if ($resultTmpPath) {
            $this->unpackResult($packFile, $resultTmpPath);
        }
    }
}