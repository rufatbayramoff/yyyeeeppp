/*
 * Class for show and montgage Cutting files in SVG format
 *
 * Dependant: jQuery snap.svg.js
 *
 * example:
 * <div id="worktable"></div>
 * <script>
 *     var wt = new WorkTable("#worktable", "#worktable_menu");
 *     wt.openFile('/path/to/file.svg', function(stats) {
 *         console.log(stats);
 *     });
 * </script>
 */

class WorkTable {

    globalRects = [];
    hilitedPathBlock = [];
    VIEWBOX_VERLOAD = '-1000000 -1000000 10000000 10000000';

    ALL_TAGS = 'path,polygon,polyline,line,rect,circle,ellipse';

    /**
     * Construct worktable inside element choosen     *
     * @param {string} elementSelector
     */
    constructor(elementSelector, menuBlockSelector, showInfoFunction) {
        this.showInfoFunction = showInfoFunction;
        this.menuBlock = $(menuBlockSelector);
        this.worktable = $(elementSelector);
        this.worktable.html('<svg id="snap" style="height: 100%; width:100%;"></svg>'); // recreate inner
        this.snap = Snap("#snap");
        // attach events
        var scope = this;
        this.invX = 0;
        this.invY = 0;
        this.worktable.mousemove(function (ev) {
            scope.mouseX = ev.offsetX;
            scope.mouseY = ev.offsetY;
            if (scope.menuBlock.is(":hidden")) {
                scope.hiliteXY(ev.offsetX, ev.offsetY);
            }
        });
        this.worktable.mousedown(function (ev) {
            scope.mousedown();
        });
        this.worktable.mouseup(function (ev) {
            scope.mouseup();
        });
    }

    /**
     * clean elements in worktable
     */
    cleanUp() {
        this.snap.selectAll('g').forEach(function (e) {
            e.remove()
        });
    }

    /**
     * Load and display file
     * @param {type} url of SVG file preprocessed
     * @returns
     */
    openFile(url, statsCallback) {
        var scope = this;
        this.mainGroup = this.snap.group();
        var tux = Snap.load(url, function (loadedFragment) {
            // preprocessing

            // attaching
            scope.mainGroup.append(loadedFragment);
            scope.visualCleanup(); // scale -> minStrokeWidth
            scope.countPages();
            scope.explodePages();
            scope.zoomAll(); // mainGroup -> scale pageW pageH
            scope.recombine();
            setTimeout(function () { // after animate finish
                // scope.buildHiliteBoxes(scope.mainGroup.select('svg'), false);
                scope.buildHiliteBoxes(scope.mainGroup, false);
                scope.visualChanges(); // scale -> minStrokeWidth
                scope.getStats(statsCallback);
                // startScene();
            }, 100);
        });
    }


    // https://gist.github.com/osvik/0185cb4381b35aad3d3e1f5438ca5ca4

    countPages() {
        this.pageCount = 0;
        this.mainGroup.selectAll('g.Page').forEach((e) => {
            this.pageCount += 1;
        });
        var bBox = this.mainGroup.getBBox();
        this.oldHeight = bBox.height;
    }

    /**
     * zoom to view all really visible elements
     */
    zoomAll() {
        // remove clipping
        this.mainGroup.selectAll('clipPath').forEach((e) => {
            e.remove();
        });
        this.mainGroup.selectAll('svg').forEach((e) => {
            this.loaded = e;
            this.pageW = e.attr('width');
            this.pageH = e.attr('height');
            this.pagePort = e.attr('viewBox') ? e.attr('viewBox') : {width: this.pageW, height: this.pageH};
            e.attr('viewBox', this.VIEWBOX_VERLOAD);
        });

        // find outer box and scale
        var bBox = this.mainGroup.getBBox();
        var scalex = bBox.width / this.worktable.width();
        var scaley = bBox.height * (this.pageCount ? this.pageCount : 1) / this.worktable.height();
        this.scale = Math.max(scalex, scaley);

        var pads = 0.1;
        this.scale = this.scale * (1 + pads);
        var globalDx = -bBox.x / this.scale + (this.worktable.width() - bBox.width / this.scale) / 2;
        var globalDy = -bBox.y / this.scale + (this.worktable.height() - bBox.height / this.scale) / 2;

        // this.viewbox_export = this.mainGroup.selectAll('svg')[0].getBBox();
        this.viewbox_export = this.mainGroup.selectAll('svg')[0].getBBox();
        this.viewbox_export_plain = this.viewbox_export.x + ' ' + this.viewbox_export.y + ' ' + this.viewbox_export.width + ' ' + this.viewbox_export.height;

        this.sceneTransform = new Snap.Matrix();
        this.sceneTransform.translate(globalDx, globalDy);
        this.sceneTransform.scale(1 / this.scale);
        this.mainGroup.animate({transform: this.sceneTransform}, 0);
        this.sceneTransformInv = this.sceneTransform.clone().invert();

        // console.log('scale is', this.scale);
    }

    /**
     * make difference between pages
     */
    explodePages() {
        if (this.pageCount > 1) { // ahh
            // var bBox = this.mainGroup.getBBox();
            var dh = this.oldHeight;
            var pageNum = 0;
            this.mainGroup.selectAll('g.Page').forEach((e) => {
                var scaley = e.transform().globalMatrix.split().scaley;
                var baseDelta = dh * (this.pageCount - 1) / 2 / scaley;
                var pageTransform = new Snap.Matrix();
                pageTransform.translate(0, dh * pageNum / scaley - baseDelta);
                e.animate({transform: pageTransform}, 0);
                pageNum += 1;
            });
        }

    }


    /**
     *  change visual appearance of elements to eliminate typical pitfalls
     */
    visualCleanup() {
        // Hilite text as we cant process it
        this.mainGroup.selectAll('text').forEach((e) => {
            e.remove();
        });

        var styles = this.mainGroup.select("svg").select("style");
        var rules = [];
        if (styles) {
            var styleAsText = styles.innerSVG();
            rules = rulesForCssText(styleAsText);
        }


        // remember source info
        this.mainGroup.selectAll(this.ALL_TAGS).forEach((e) => {
            var st = e.attr('stroke');
            var sw = e.attr('stroke-width');
            var sf = e.attr('fill');
            //var cl = e.attr('class');
            //var cl2 = decomposeClassNames(cl, rules);
            if ((st && st != 'none') || (sf && sf != 'none')) { //   || (cl && cl != 'none')
                e.attr('origdata', st + '@' + sw + '@' + sf); // + '@' + cl2);
                e.attr('origstroke', st);
            } else {
                // delete unused
                e.remove();
            }
        });

    }


    /**
     *  change visual appearance of elements to eliminate typical pitfalls
     */
    visualChanges() {
        // minimal stroke width
        this.minStrokeWidth = 5000 * this.scale;

        var styles = this.mainGroup.select("svg").select("style");
        var rules = [];
        if (styles) {
            var styleAsText = styles.innerSVG();
            rules = rulesForCssText(styleAsText);
        }

        // touch minimal stroke width
        this.mainGroup.selectAll(this.ALL_TAGS).forEach((e) => {
            var sw = e.attr('stroke-width');
            if (sw && parseInt(sw) < this.minStrokeWidth) {
                e.attr('stroke-width', this.minStrokeWidth);
            }
        });

        // change styles fill -> stroke
        this.mainGroup.selectAll(this.ALL_TAGS).forEach((e) => {
            var st = e.attr('stroke');
            var sf = e.attr('fill');
            var sw = e.attr('stroke-width');
            var cl = e.attr('class');
            //var clObj = decomposeClassNameToObject(cl, rules);
            e.attr("class", "");
            if ( e.attr('fill') && e.attr('fill') != 'none' ) {
                e.attr('fill', 'transparent');
            }
            var af = st;
            if (!af || af == 'none') {
                af = sf;
            };
            //if (!af || af == 'none') {
            //	af = clObj.stroke;
            //};
            //if (!af || af == 'none') {
            //	af = clObj.fill;
            //};
            if (!e.attr('stroke') || e.attr('stroke') == 'none') {
                e.attr('stroke', af);
            }
            if (!e.attr('stroke-width') || e.attr('stroke-width') == 'none' || parseInt(e.attr('stroke-width')) < this.minStrokeWidth) {
                e.attr('stroke-width', this.minStrokeWidth);
            }
        });
    }

    /**
     * build boxes and rememeber paths for hiliting
     *
     */
    buildHiliteBoxes(fragment, makeAllCut = false) {
        this.scaleX = parseFloat(this.pageW) / this.pagePort.width;
        this.scaleY = parseFloat(this.pageH) / this.pagePort.height;


        // find outer bboxes, and totalLength by the way
        var totalLength = 0, maxWP = 0, maxHP = 0, lenCut = 0, lenEngrave = 0;
        var paths = [];
        fragment.selectAll(this.ALL_TAGS).forEach((e) => {
            if (e.attr('origdata') && e.attr('origdata') != "none") {
                var bb = e.getBBox();
                if (bb.width > maxWP) {
                    maxWP = bb.width;
                }
                if (bb.height > maxHP) {
                    maxHP = bb.height;
                }
                // var matr = findParentMatrixOfClass(e, 'g', 'Page'); // moot
                var matr = e.transform().globalMatrix.clone().multLeft(this.sceneTransform);
                paths.push({
                    box: bb,
                    boxt: transformBBox(bb, matr),
                    path: e,
                });
                var l = e.getTotalLength();
                totalLength += l;
                var mode = e.attr('cuttingmode');
                if (makeAllCut) {
                    mode = 'cut';
                }
                if (mode == 'cut') lenCut += l;
                if (mode == 'engrave') lenEngrave += l;
            }
        });
        this.maxWP = maxWP * this.scaleX; // Math.round( , 2);
        this.maxHP = maxHP * this.scaleY; // Math.round( , 2);
        this.lenCut = lenCut * Math.max(this.scaleX, this.scaleY);
        this.lenEngrave = lenEngrave * Math.max(this.scaleX, this.scaleY);
        this.totalLength = totalLength * Math.max(this.scaleX, this.scaleY); // Math.round( totalLength * Math.max(scaleX,scaleY), 2);

        // clone bbox
        this.globalRects = paths; // = clone
    }

    /**
     * change and recombine paths geometry - MATH MATH MATH
     */
    recombine() {
        // rearrange all patchs
        this.mainGroup.selectAll('path').forEach((path) => {
            var sw = path.attr('stroke');
            if (sw && sw != 'none') {
                // absolute coords
                var d = path.attr('d'); // ??????????????????????!!!!!!!!!!!!!!!!!!!!!!! polygon have no d!!
                var dabs = Snap.path.toAbsolute(d);
                // rearrange all patchs separated contours
                var paths = [[]];
                var current = 0;
                var hasPhysical = false;
                for (var i = 0; i < dabs.length; i++) {
                    if (dabs[i][0] === 'M') { // Move to new point - oOps!
                        if (hasPhysical) { // remember
                            current++;
                            paths.push([]);
                        } else { // empty contour - drop
                            paths[current] = [];
                        }
                        hasPhysical = false;
                    }
                    if (dabs[i][0] !== 'M' && dabs[i][0] !== 'Z') {// really, we draw something!
                        hasPhysical = true;
                    }
                    paths[current].push(dabs[i]);
                }
                for (var j = 0; j < paths.length; j++) {
                    var path2 = path.clone();
                    path2.attr({d: paths[j]});
                    // hilite as RED if not finished on Z
                    if (paths[j][paths[j].length - 1][0] != 'Z') {
                        // path2.attr({fill: '#f00'});
                    }
                }
                path.remove();
            }
        });
    }


    //////////////////////////////////////////////////////////
    /**
     * calculate common statistics
     * @param {function} statsCallback call after stats detection finished
     * @returns {Object}
     */
    getStats(statsCallback) {
        var units = this.pageW.replace(/[\d\.]+/, '');

        // create boxes with PADDING around known
        var paths = [];
        var DELTA = 0.001;
        for (var i = 0; i < this.globalRects.length; i++) {
            var bb2 = JSON.parse(JSON.stringify(this.globalRects[i].box));
            // make extansion to bb size to join contacted propertly
            bb2.x -= DELTA;
            bb2.y -= DELTA;
            bb2.x2 += DELTA;
            bb2.y2 += DELTA;
            bb2.width = bb2.x2 - bb2.x;
            bb2.height = bb2.y2 - bb2.y;
            paths.push(bb2);
        }

        // joining bboxes
        var hasChanged = true;
        while (hasChanged) {
            hasChanged = false;
            outerloop:
                for (var i = 0; i < paths.length - 1; i++) {
                    for (var j = i + 1; j < paths.length; j++) {
                        if (Snap.path.isBBoxIntersect(paths[i], paths[j])) {
                            // merge
                            paths[i] = mergeBBox(paths[i], paths[j]);
                            paths.splice(j, 1);
                            hasChanged = true;
                            break outerloop;
                        }
                    }
                }
        }

        // joining blocks having no area - to closest block
        hasChanged = true;
        while (hasChanged) {
            hasChanged = false;
            for (var i = 0; i < paths.length; i++) {
                if (paths[i].height > 3 * DELTA && paths[i].width > 3 * DELTA) {
                    continue; // block have area
                };
                var jclosest = -1;
                var distclosest = 9e9;
                for (var j = 0; j < paths.length; j++) {
                    if (i == j) continue;
                    var mindistx = Math.min(
                        Math.abs(paths[i].x -paths[j].x),
                        Math.abs(paths[i].x -paths[j].x2),
                        Math.abs(paths[i].x2 -paths[j].x),
                        Math.abs(paths[i].x2 -paths[j].x2),
                    );
                    var mindisty = Math.min(
                        Math.abs(paths[i].y -paths[j].y),
                        Math.abs(paths[i].y -paths[j].y2),
                        Math.abs(paths[i].y2 -paths[j].y),
                        Math.abs(paths[i].y2 -paths[j].y2),
                    );
                    var dist = Math.sqrt(mindistx*mindistx + mindisty*mindisty);
                    if (dist<distclosest) {
                        distclosest = dist;
                        jclosest = j;
                    }
                }
                if (jclosest>=0) { // closeset found, joining
                    paths[i] = mergeBBox(paths[i], paths[jclosest]);
                    paths.splice(jclosest, 1);
                    hasChanged = true;
                    break
                }
            }
        }

        // joining bboxes again
        hasChanged = true;
        while (hasChanged) {
            hasChanged = false;
            outerloop:
                for (var i = 0; i < paths.length - 1; i++) {
                    for (var j = i + 1; j < paths.length; j++) {
                        if (Snap.path.isBBoxIntersect(paths[i], paths[j])) {
                            // merge
                            paths[i] = mergeBBox(paths[i], paths[j]);
                            paths.splice(j, 1);
                            hasChanged = true;
                            break outerloop;
                        }
                    }
                }
        }


        // total area and max sizes
        var totalArea = 0, maxW = 0, maxH = 0, blockCount = 0;
        var blockResult = [];
        for (var i = 0; i < paths.length; i++) {
            totalArea += paths[i].width * paths[i].height;
            if (paths[i].width > maxW) {
                maxW = paths[i].width;
            }
            if (paths[i].height > maxH) {
                maxH = paths[i].height;
            }
            blockCount += 1;
            blockResult.push({
                w: paths[i].width * this.scaleX,
                h: paths[i].height * this.scaleY,
                x_orig: paths[i].x,
                y_orig: paths[i].y,
                w_orig: paths[i].width,
                h_orig: paths[i].height,
            })
        }
        maxW = maxW * this.scaleX; // Math.round( , 2);
        maxH = maxH * this.scaleY; // Math.round( , 2);
        totalArea = totalArea * Math.max(this.scaleX, this.scaleY) * Math.max(this.scaleX, this.scaleY); // Math.round( , 2);

        var result = {
            totalAreaUnits2: totalArea,
            totalLengthUnits: this.totalLength,
            lenCut: this.lenCut,
            lenEngrave: this.lenEngrave,
            units: units,
            maxBlock: {
                w: maxW,
                h: maxH,
            },
            maxContour: {
                w: this.maxWP,
                h: this.maxHP,
            },
            blocks: blockResult,
        };
        if (typeof statsCallback === "function") {
            statsCallback(result);
        }
        ;
        return result;
    }


    ////////////////////////////////////////////////////////
    // Attach mouse interactions
    /**
     * hilite on mouse move
     * @param {int} x
     * @param {int} y
     */
    hiliteXY(x, y) {
        // If popup do nothing
        this.showInfoFunction('');
        if (this.hiliterBox) this.hiliterBox.remove();
        if (this.hiliterPath) this.hiliterPath.remove();
        if (!this.loaded) {
            return
        }

        var DELTA_MOUSE = 5;
        var inv = this.loaded.transform().globalMatrix.clone().invert();

        //var x2 = inv.x(x, y);
        //var y2 = inv.y(x, y);
        //var xd2 = inv.x(x+DELTA_MOUSE, y);
        //var yd2 = inv.y(x, y+DELTA_MOUSE);
        // moot, transform done inside buiding blocks
        var x2 = x;
        var y2 = y;
        var xd2 = x + DELTA_MOUSE;
        var yd2 = y + DELTA_MOUSE;


        var dx = Math.abs(xd2-x2);
        var dy = Math.abs(yd2-y2);

        //this.invX = x2;
        //this.invY = y2;
        // moot
        this.invX = inv.x(xd2, yd2);
        this.invY = inv.y(xd2, yd2);

        // mouse lasso
        if (this.mouseClicked) {
            /*if (this.mouseBox && Math.abs(this.invX - this.mouseClickX) > 0 && Math.abs(this.invY - this.mouseClickY) > 0) {
                this.mouseBox.stop().animate({
                    x: Math.min(this.invX, this.mouseClickX),
                    y: Math.min(this.invY, this.mouseClickY),
                    width: Math.abs(this.invX - this.mouseClickX),
                    height: Math.abs(this.invY - this.mouseClickY),
                }, 0);
            }*/
            // moot
            if (this.mouseBox && Math.abs(x - this.mouseClickX) > 0 && Math.abs(x - this.mouseClickY) > 0) {
                this.mouseBox.stop().animate({
                    x: Math.min(x, this.mouseClickX),
                    y: Math.min(y, this.mouseClickY),
                    width: Math.abs(x - this.mouseClickX),
                    height: Math.abs(y - this.mouseClickY),
                }, 0);
            }
        } else { // no mouse clicked
            this.hiliteOnlyPaths([]);
            var bbChoosen = null;
            var bbChoosenT = null;
            var iChoosen = null;
            var bbmouse = box(x2-dx,y2-dy,dx*2,dy*2);
            for (var i = 0; i < this.globalRects.length; i++) {
                var bbt = this.globalRects[i].boxt;   // in MOUSE coord system
                var bb = this.globalRects[i].box;
                // if (Snap.path.isPointInsideBBox(bb, x2, y2)) {
                if (Snap.path.isBBoxIntersect(bbt, bbmouse)) {
                    // hilite smallest
                    if (bbChoosen) {
                        if (bbChoosen.width * bbChoosen.height > bb.width * bb.height) {
                            bbChoosen = bb;
                            bbChoosenT = bbt;
                            iChoosen = i;
                        }
                    } else {
                        bbChoosen = bb;
                        bbChoosenT = bbt;
                        iChoosen = i;
                    }
                }
            }
            if (bbChoosen) {
                // box hilite model
                // var matr = loaded.transform().globalMatrix.clone().multLeft(this.sceneTransform);
                //var bbChoosenT2 = transformBBox ( bbChoosenT, this.loaded.transform().globalMatrix.clone().invert() );
                this.hiliterBox = this.snap.rect(bbChoosenT.x, bbChoosenT.y, bbChoosenT.width, bbChoosenT.height);
                this.hiliterBox.attr({
                    fill: 'transparent',
                    stroke: "#f00",
                    strokeWidth: 1, // this.minStrokeWidth * 3
                });

                // path hilite model
                this.lastHilitedPath = this.globalRects[iChoosen].path;
                this.hiliterPath = this.lastHilitedPath.clone();
                this.hiliterPath.attr({
                    fill: 'transparent',
                    stroke: "#f00",
                    strokeWidth: this.minStrokeWidth * 10
                });

                // calculate area
                var units = this.pageW.replace(/[\d\.]+/, '');
                var scaleX = parseFloat(this.pageW) / this.pagePort.width;
                var scaleY = parseFloat(this.pageH) / this.pagePort.height;
                var sizeX = Math.round(bbChoosen.width * scaleX, 2);
                var sizeY = Math.round(bbChoosen.height * scaleY, 2);
                this.selectedArea = {
                    sizeX: sizeX,
                    sizeY: sizeY,
                    units: units
                };

                this.showInfoFunction(this.selectedArea);
            } else {
                this.hiliterPath = null;
            }
        }
    }

    nohilite() {
        // If popup do nothing
        this.showInfoFunction('');
        if (this.hiliterBox) this.hiliterBox.remove();
        if (this.hiliterPath) this.hiliterPath.remove();
        this.hiliterBox = null;
        this.hiliterPath = null;
    }

    mousedown() {
        //this.mouseClickX = this.invX;
        //this.mouseClickY = this.invY;
        // moot
        this.mouseClickX = this.mouseX;
        this.mouseClickY = this.mouseY;

        this.mouseBox = this.snap.rect(0, 0, 1, 1);
        this.mouseBox.attr({
            fill: 'rgba(0,0,255, 0.2)',
            stroke: "#00f",
            strokeWidth: 1, // this.minStrokeWidth * 3
        });
        this.menuBlock.hide();
        this.hiliteXY(this.mouseX, this.mouseY);
        this.mouseClicked = true;
    }

    mouseup() {
        this.mouseClicked = false;
        var choosenPaths = [];
        var bboxforhilite = null;
        if (this.mouseBox) {
            var bbTool = this.mouseBox.getBBox();
            // hilite all matched
            for (var i = 0; i < this.globalRects.length; i++) {
                var bb = this.globalRects[i].boxt;
                if (Snap.path.isPointInsideBBox(bbTool, bb.x, bb.y)
                    && Snap.path.isPointInsideBBox(bbTool, bb.x2, bb.y2)) {
                    // pash is choosen:
                    choosenPaths.push(this.globalRects[i].path);
                    var mbbox = this.globalRects[i].path.getBBox();
                    if (bboxforhilite) {
                        bboxforhilite = mergeBBox(bboxforhilite, mbbox);
                    } else {
                        bboxforhilite = mbbox;
                    }
                }
            }
            if (choosenPaths.length === 0 && this.hiliterPath) {
                //debugger;
                // we select noghing - then choose SINGLE clicked!
                choosenPaths.push(this.lastHilitedPath);
            } else {
                this.hiliteOnlyPaths(choosenPaths);
            }

            // do with selected what we require
            this.selectedPaths = choosenPaths;
            // this.recursiveSelectByFilter(choosenPaths);

            // clean tool
            this.mouseBox.remove();
            this.mouseBox = null;

            if (bboxforhilite) {
                var units = this.pageW.replace(/[\d\.]+/, '');
                var scaleX = parseFloat(this.pageW) / this.pagePort.width;
                var scaleY = parseFloat(this.pageH) / this.pagePort.height;
                var sizeX = Math.round(bboxforhilite.width * scaleX, 2);
                var sizeY = Math.round(bboxforhilite.height * scaleY, 2);
                this.selectedArea = {
                    sizeX: sizeX,
                    sizeY: sizeY,
                    units: units
                };
                this.showInfoFunction(this.selectedArea);
            };
        }
        if (choosenPaths.length) {
            this.menuBlock.show();
            var deltaMenuBlock = 40;
            var xpos, ypos;
            if (this.mouseY > this.worktable.height() / 2) {
                ypos = this.mouseY - this.menuBlock.height() + deltaMenuBlock;
            } else {
                ypos = this.mouseY - deltaMenuBlock;
            }
            if (this.mouseX > this.worktable.width() / 2) {
                xpos = this.mouseX - this.menuBlock.width() + deltaMenuBlock;
            } else {
                xpos = this.mouseX - deltaMenuBlock;
            }
            // debugger;

            this.menuBlock.offset({
                top: ypos,
                left: xpos,
            })
        }
    }

    hiliteOnlyPaths(choosenPaths) {
        // clean up old
        for (var i = 0; i < this.hilitedPathBlock.length; i++) {
            var e = this.hilitedPathBlock[i];
            if (e.removed) continue;
            e.remove();
        }
        this.hilitedPathBlock = [];
        for (var i = 0; i < choosenPaths.length; i++) {
            var e = choosenPaths[i];
            if (e.removed) continue;
            var c = e.clone();
            c.attr({
                fill: 'transparent',
                stroke: "#f00",
                strokeWidth: this.minStrokeWidth * 5
            });
            this.hilitedPathBlock.push(c);
        }
    }

    recursiveSelectByFilter(choosenPaths) {
        var newChoosenPaths = [];
        for (var i = 0; i < choosenPaths.length; i++) {
            var filter = choosenPaths[i].attr('origdata');
            this.mainGroup.selectAll(this.ALL_TAGS).forEach((e) => {
                if (e.attr('origdata') == filter && !newChoosenPaths.includes(e)) {
                    newChoosenPaths.push(e);
                }
            });
        }
        this.hiliteOnlyPaths(newChoosenPaths);
    }

    deleteSelected() {
        if (this.selectedPaths) {
            for (var i = 0; i < this.selectedPaths.length; i++) {
                var e = this.selectedPaths[i];
                if (e.removed) continue;
                e.remove(); // lets deleteSelected!
                this.deleteFromGlobalRects(e);
            }
            ;
        }
        this.selectedPaths = null;
        if (this.hiliterBox) this.hiliterBox.remove();
        if (this.hiliterPath) this.hiliterPath.remove();
        this.menuBlock.hide();
    }

    deleteFromGlobalRects(e) {
        for (var j = 0; j < this.globalRects.length; j++) {
            if (e == this.globalRects[j].path) {
                this.globalRects.splice(j, 1);
            }
        }
    }

    markSelected(mode, enlarge) {
        if (this.selectedPaths) {
            // enlarge by type
            var newChoosenPaths = this.selectedPaths;
            if (enlarge) {
                var newChoosenPaths = [];
                for (var i = 0; i < this.selectedPaths.length; i++) {
                    var filter = this.selectedPaths[i].attr('origdata');
                    this.mainGroup.selectAll(this.ALL_TAGS).forEach((e) => {
                        if (e.attr('origdata') == filter && !newChoosenPaths.includes(e)) {
                            newChoosenPaths.push(e);
                        }
                    });
                }
            }
            for (var i = 0; i < newChoosenPaths.length; i++) {
                var e = newChoosenPaths[i];
                if (e.removed) continue;
                if (mode == 'cut') {
                    e.attr({
                        cuttingmode: mode,
                        fill: 'transparent',
                        // stroke: "#0f0",
                        stroke: "#009900",
                        // strokeWidth: this.minStrokeWidth*5
                    });
                    e.attr("class", "");
                }
                if (mode == 'engrave') {
                    e.attr({
                        cuttingmode: mode,
                        fill: 'transparent',
                        // stroke: "#0ff",
                        stroke: "#004ce6",
                        // strokeWidth: this.minStrokeWidth*5
                    });
                    e.attr("class", "");
                }
            }
        }
        this.selectedPaths = null;
        if (this.hiliterBox) this.hiliterBox.remove();
        if (this.hiliterPath) this.hiliterPath.remove();
        this.menuBlock.hide();
    }

    previewStart() {
        var el = this.mainGroup.select('svg');
        this.savedSVG = el.toDataURL();
        // prepare to export

        // pre-clean
        this.mainGroup.selectAll(this.ALL_TAGS).forEach((path) => {
            var sw = path.attr('cuttingmode');
            if (sw && sw != 'none') {
                // all right TODO clean unused attributes
            } else {
                path.remove();
                this.deleteFromGlobalRects(path);
            }
        });
        this.mainGroup.selectAll('image').forEach((e) => {
            e.remove();
        });
    }

    previewEnd() {
        this.cleanUp();
        this.openFile(this.savedSVG);
    }

    async export(makeAllCut = false) {
        this.nohilite();

        var el_orig = wt.mainGroup.select('svg');
        el_orig = el_orig.clone();
        el_orig.attr('viewBox', this.pagePort);
        // var svg_state = el_orig.outerSVG();
        var svg_state = el_orig.toDataURL(); // 'data:image/svg+xml;base64,' + btoa(svg_state.trim());
        // el_orig.remove();
        // saveFile(svg_state, "svg_state.svg");


        el_orig.selectAll(this.ALL_TAGS).forEach((path) => {
            var sw = path.attr('cuttingmode');
            if (sw && sw != 'none') {
                // all right TODO clean unused attributes
            } else {
                if (!makeAllCut) {
                    path.remove();
                    this.deleteFromGlobalRects(path);
                };
            }
        });
        el_orig.selectAll('image').forEach((e) => {
            e.remove();
        });


        this.buildHiliteBoxes(el_orig, makeAllCut);
        var info = this.getStats();

        var el = el_orig; // wt.mainGroup.select('svg');
        // prepare to export
        var wold = el.attr('width');
        var hold = el.attr('height');
        el.attr('width', 720); // default height and width
        el.attr('height', 540);
        el.attr('viewBox', this.viewbox_export_plain); // this.pagePort);
        // var s = el.outerSVG();

        // this.renderSVG(s);
        for (var i = 0; i < info.blocks.length; i++) {
            var bnds = info.blocks[i].x_orig + ' ' + info.blocks[i].y_orig + ' ' + info.blocks[i].w_orig + ' ' + info.blocks[i].h_orig;
            el.attr('viewBox', bnds);
            // el.attr('width', Math.round(h_fixed * info.blocks[i].w_orig /  info.blocks[i].h_orig) ); // default height and width

            var el_block = el.clone();
            info.blocks[i].lengths = this.deleteOutside(el_block, box(info.blocks[i].x_orig, info.blocks[i].y_orig, info.blocks[i].w_orig, info.blocks[i].h_orig), makeAllCut);
            info.blocks[i].data = await this.renderSVG(el_block.toDataURL()); // ; el_block.outerSVG());
            el_block.remove();
        }


        el.attr('width', wold);
        el.attr('height', hold);
        el.attr('viewBox', this.VIEWBOX_VERLOAD);
        this.menuBlock.hide();
        var result = {
            info: info,
            svg_state: svg_state
        }; // , svg: s


        // reopen same file IS working!
        // this.openFile('data:image/svg+xml;base64,' + btoa(s.trim()), function(){});
        el_orig.remove();
        this.buildHiliteBoxes(wt.mainGroup.select('svg'), false);

        return result;
    }

    deleteOutside(snapBlock, bbox, makeAllCut) {
        var lenCut = 0;
        var lenEngrave = 0;
        snapBlock.selectAll(this.ALL_TAGS).forEach((e) => {
            if (e.attr('origdata') && e.attr('origdata') != "none") {
                var bb = e.getBBox();
                if (!Snap.path.isBBoxIntersect(bb, bbox)) {
                    e.remove();
                } else { // inside bb - lst count it
                    var l = e.getTotalLength();
                    var mode = e.attr('cuttingmode');
                    if (makeAllCut) {
                        mode = 'cut';
                    }
                    if (mode == 'cut') lenCut += l;
                    if (mode == 'engrave') lenEngrave += l;
                }
            }
        });
        lenCut = lenCut * Math.max(this.scaleX, this.scaleY);
        lenEngrave = lenEngrave * Math.max(this.scaleX, this.scaleY);
        return {'lencut':lenCut ,'lenengrave': lenEngrave};
    }

    import() {

    }

    async renderSVG(svgAsDataurl) {
        var self = this;
        var canvas = document.createElement('canvas');
        var context = canvas.getContext("2d");
        document.body.appendChild(canvas);

        var image = new Image;
        var bsrc = svgAsDataurl; // 'data:image/svg+xml;base64,' + btoa(svgAsString.trim());
        image.src = bsrc;
        var png = '';
        // saveFile(image.src, "allclean.svg");
        // image.onload = function () {
        await loadImage(image);
        // console.log('iw=',image.width);
        canvas.setAttribute('width', image.width);
        canvas.setAttribute('height', image.height);
        context.fillStyle = 'white';
        context.fillRect(0, 0, image.width, image.height);
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/png");
        // saveFile(canvasdata, "render.png");
        document.body.removeChild(canvas);
        png = canvasdata;
        // };
        return {'svg': bsrc, 'png': png};
    }


}

/////////////////////////////////////////////////////////////////////
// Support functions

function saveFile(data, name) {
    var a = document.createElement("a");
    a.textContent = name + " ";
    a.download = "export_" + Date.now() + "_" + name;
    a.href = data;
    $("#info")[0].appendChild(a);
    // document.body
}


function box(x,y,width,height) {
    return {
        x: x,
        y: y,
        width: width,
        w: width,
        height: height,
        h: height,
        x2: x + width,
        y2: y + height,
        cx: x + width / 2,
        cy: y + height / 2,
        r1: Math.min(width, height) / 2,
        r2: Math.max(width, height) / 2,
        r0: Math.sqrt(width * width + height * height) / 2,
        // path: rectPath(x, y, width, height),
        vb: [x, y, width, height].join(" ")
    };
}

function mergeBBox(bbox1, bbox2) {
    bbox1.x = Math.min(bbox1.x, bbox2.x);
    bbox1.y = Math.min(bbox1.y, bbox2.y);
    bbox1.x2 = Math.max(bbox1.x2, bbox2.x2);
    bbox1.y2 = Math.max(bbox1.y2, bbox2.y2);
    bbox1.width = bbox1.x2 - bbox1.x;
    bbox1.height = bbox1.y2 - bbox1.y;
    return bbox1;
}

function transformBBox(bbox, matrix) {
    var x1 = matrix.x(bbox.x, bbox.y);
    var y1 = matrix.y(bbox.x, bbox.y);
    var x2 = matrix.x(bbox.x2, bbox.y2);
    var y2 = matrix.y(bbox.x2, bbox.y2);
    bbox = JSON.parse(JSON.stringify(bbox));
    bbox.x = Math.min(x1, x2);
    bbox.y = Math.min(y1, y2);
    bbox.x2 = Math.max(x1, x2);
    bbox.y2 = Math.max(y1, y2);
    bbox.width = bbox.x2 - bbox.x;
    bbox.height = bbox.y2 - bbox.y;
    return bbox;
}

function findParentMatrixOfClass(el, elName, className) {
    while (el) {
        if (el.type === 'svg') return new Snap.Matrix();
        if (el.type === elName && el.hasClass(className)) return el.transform().localMatrix;
        el = el.parent();
    }
    return new Snap.Matrix();
}

loadImage = async img => {
    return new Promise((resolve, reject) => {
        img.onload = async () => {
            // console.log("Image Loaded");
            resolve(true);
        };
    });
};

function rulesForCssText (styleContent) {
    var doc = document.implementation.createHTMLDocument(""),
        styleElement = document.createElement("style");

    styleElement.textContent = styleContent;
    // the style will only be parsed once it is added to a document
    doc.body.appendChild(styleElement);

    return styleElement.sheet.cssRules;
}
function decomposeClassNames(cssNames, ruleset) {
    if (cssNames.trim()=="") return "";
    var output = '';
    var classes = cssNames.split(" ");
    for (var i = 0; i < classes.length; i++) {
        for (var j = 0; j < ruleset.length; j++) {
            if (ruleset[j].selectorText == "." + classes[i]) {
                output += ruleset[j].style.cssText + " ";
            }
        }
    }
    return output.trim();
}
function decomposeClassNameToObject(cssNames, ruleset) {
    if (cssNames.trim()=="") return "";
    var output = {};
    var classes = cssNames.split(" ");
    for (var i = 0; i < classes.length; i++) {
        for (var j = 0; j < ruleset.length; j++) {
            if (ruleset[j].selectorText == "." + classes[i]) {
                for (var k = 0; k < ruleset[j].style.length; k++) {
                    var idx = ruleset[j].style[k];
                    var val = ruleset[j].style[idx];
                    output[idx] = val;
                }
            }
        }
    }
    return output;
}
