<?php

namespace common\modules\cutting\assets;

use backend\assets\CommonAsset;
use frontend\assets\AppAsset;
use yii\web\AssetBundle;

/**
 *
 */
class LaserCuttingPartsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/modules/cutting/assets';

    public $depends = [
        CommonAsset::class,
        AppAsset::class
    ];

    public $css = [
        'css/cuttingWidget.css',
        'css/cuttingParts.css'
    ];

    public $js = [
        'js/worktable.js',
        'js/snap.svg.js',
    ];
}