<?php

namespace common\modules\cutting\serializers;

use common\modules\cutting\models\CuttingWorkpieceMaterialGroup;
use yii\base\BaseObject;

class CuttingWorkpieceMaterialGroupsSerializer extends BaseObject
{
    /**
     * @param CuttingWorkpieceMaterialGroup[] $cuttingWorkpieceMaterialGroups
     * @return array|void
     */
    public static function serialize($cuttingWorkpieceMaterialGroups)
    {
        $list = [];
        foreach ($cuttingWorkpieceMaterialGroups as $cuttingWorkpieceMaterialGroup) {
            foreach ($cuttingWorkpieceMaterialGroup->sizes as $groupSize) {
                foreach ($groupSize->groupColors as $groupColor) {
                    foreach ($groupColor->colors as $color) {
                        $materialId = $cuttingWorkpieceMaterialGroup->material->id;
                        $sizeKey    = $groupSize->size->getKey();
                        $colorKey   = $color->color->id;

                        if (!array_key_exists($materialId, $list)) {
                            $list[$materialId] = [
                                'materialId' => $materialId,
                                'sizes'      => []
                            ];
                        }
                        if (!array_key_exists($sizeKey, $list[$materialId]['sizes'])) {
                            $list[$materialId]['sizes'][$sizeKey] = [
                                'width'     => (int)$groupSize->size->width,
                                'height'    => (int)$groupSize->size->length,
                                'thickness' => (int)$groupSize->size->thickness,
                                'colors'    => []
                            ];
                        }
                        $price = $groupColor->price?->getAmount();
                        if (!$price) {
                            $price = $groupSize->price?->getAmount();
                        }
                        if (!$price) {
                            $price = $cuttingWorkpieceMaterialGroup->price?->getAmount();
                        }
                        $list[$materialId]['sizes'][$sizeKey]['colors'][$colorKey] = [
                            'code'  => $color->color->getCode(),
                            'uuid'  => $color->cuttingWorkpieceMaterial->uuid,
                            'price' => $price,
                        ];
                    }
                }
            }
        }

        foreach ($list as $materialKey => $materialGroup) {
            foreach ($materialGroup['sizes'] as $sizeKey => $sizesGroup) {
                asort($list[$materialKey]['sizes'][$sizeKey]['colors']);
            }
            asort($list[$materialKey]['sizes']);
        }
        return $list;
    }
}
