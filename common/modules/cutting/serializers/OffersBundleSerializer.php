<?php

namespace common\modules\cutting\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\CuttingMaterial;
use common\models\CuttingMaterialColor;
use common\models\CuttingPack;
use common\models\CuttingPackFile;
use common\models\CuttingPackPart;
use common\models\File;
use common\models\PrinterColor;
use common\models\serializers\FileSerializer;
use frontend\controllers\catalog\actions\serializers\PrinterColorSerializer;

class OffersBundleSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            PrinterColor::class    => PrinterColorSerializer::class,
            CuttingMaterial::class => [
                'id',
                'title',
            ],
        ];
    }
}
