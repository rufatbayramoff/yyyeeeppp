<?php

namespace common\modules\cutting\serializers;

use common\models\CuttingWorkpieceMaterial;
use yii\base\BaseObject;

class CuttingWorkpieceMaterialProcessingSerializer extends BaseObject
{
    /**
     * @param CuttingWorkpieceMaterial[] $cuttingWorkpieceMaterials
     * @return array|void
     */
    public static function serialize($cuttingWorkpieceMaterials)
    {
        $list = [];
        foreach ($cuttingWorkpieceMaterials as $cuttingWorkpieceMaterial) {
            $key        = $cuttingWorkpieceMaterial->material_id . '_' .  $cuttingWorkpieceMaterial->thickness;
            $list[$key] = [
                'uuid'       => $key,
                'materialId' => $cuttingWorkpieceMaterial->material_id,
                'thickness'  => $cuttingWorkpieceMaterial->thickness,
                'title'      => $cuttingWorkpieceMaterial->getProcessingTitle()
            ];
        }
        ksort($list);
        return $list;
    }
}
