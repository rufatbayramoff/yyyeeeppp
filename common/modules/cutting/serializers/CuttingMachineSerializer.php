<?php

namespace common\modules\cutting\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\CompanyService;
use common\models\CuttingMachine;
use frontend\modules\mybusiness\serializers\CompanyServiceSerializer;

class CuttingMachineSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CuttingMachine::class => [
                'id',
                'maxWidth'      => 'max_width',
                'maxLength'     => 'max_length',
                'minOrderPrice' => 'min_order_price',
                'technology',
                'companyService'
            ],
            CompanyService::class => CompanyServiceSerializer::class
        ];
    }
}
