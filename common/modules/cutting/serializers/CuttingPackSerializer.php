<?php

namespace common\modules\cutting\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\CuttingMaterial;
use common\models\CuttingMaterialColor;
use common\models\CuttingPack;
use common\models\CuttingPackFile;
use common\models\CuttingPackPage;
use common\models\CuttingPackPart;
use common\models\File;
use common\models\serializers\FileSerializer;

class CuttingPackSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CuttingPack::class     => [
                'uuid',
                'title',
                'isSameMaterialForAllParts' => 'is_same_material_for_all_parts',
                'materialId'                => 'material_id',
                'thickness',
                'colorId'                   => 'color_id',
                'cuttingPackFiles'          => 'activeCuttingPackFiles',
            ],
            CuttingPackFile::class => [
                'uuid',
                'title'            => 'file.name',
                'type',
                'previewUrl',
                'cuttingPackPages',
                'format'           => 'file.extension',
                'fileMd5NameSize'  => 'file.md5NameSize',
                'qty',
                'isConverted'      => 'is_converted'
            ],
            CuttingPackPage::class => [
                'uuid',
                'title',
                'cuttingPackParts' => 'activeCuttingPackParts',
            ],
            CuttingPackPart::class => [
                'uuid',
                'qty',
                'image',
                'width',
                'height',
                'cuttingLength'       => 'cutting_length',
                'engravingLength'     => 'engraving_length',
                'materialId'          => 'material_id',
                'thickness',
                'colorId'             => 'color_id',
                'cuttingPackPageUuid' => 'cutting_pack_page_uuid'
            ]
        ];
    }
}
