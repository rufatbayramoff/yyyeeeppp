<?php

namespace common\modules\cutting\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\CuttingMaterial;
use common\models\CuttingMaterialColor;

class CuttingMaterialSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CuttingMaterial::class      => [
                'id',
                'code',
                'title',
                'defaultThickness' => 'default_thickness',
                'defaultWidth'     => 'default_width',
                'defaultLength'    => 'default_length',
                'colors'           => 'cuttingMaterialColors',
            ],
            CuttingMaterialColor::class => [
                'code'  => 'cuttingColor.render_color',
                'title' => 'cuttingColor.title',
                'rgb'   => 'cuttingColor.rgb'
            ]
        ];
    }
}

