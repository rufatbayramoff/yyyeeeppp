<?php

namespace common\modules\cutting\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\CuttingMachine;
use common\models\CuttingMachineProcessing;
use common\models\CuttingMaterial;
use common\models\CuttingWorkpieceMaterial;

class CuttingProcessingSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CuttingMachineProcessing::class => [
                'uuid',
                'cuttingMachineId' => 'cutting_machine_id',
                'workpieceMaterialProcessingUuid',
                'cuttingPricePerM',
                'engravingPricePerM',
            ],
        ];
    }
}
