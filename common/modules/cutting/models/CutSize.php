<?php

namespace common\modules\cutting\models;

use common\components\ps\locator\Size;
use common\modules\printersList\models\AnswerInfo;

class CutSize
{
    public $width;
    public $length;
    public $thickness;

    /**
     * CutSize constructor.
     */
    private function __construct()
    {
    }

    public static function create($width, $height, $thickness): CutSize
    {
        $size            = new CutSize();
        $size->width     = $width;
        $size->length    = $height;
        $size->thickness = $thickness;
        return $size;
    }

    public static function zero(): CutSize
    {
        return self::create(0, 0, 0);
    }

    public function isMoreOrEqualThen(CutSize $size): bool
    {
        // localSize
        $p1 = $this->width;
        $p2 = $this->length;
        if ($p2 > $p1) {
            $p  = $p1;
            $p1 = $p2;
            $p2 = $p;
        }

        // getSize
        $o1 = $size->width;
        $o2 = $size->length;
        if ($o2 > $o1) {
            $o  = $o1;
            $o1 = $o2;
            $o2 = $o;
        }

        if ($p1 >= $o1 && $p2 >= $o2) {
            return true;
        }
        return false;
    }

    public function isMoreOrEqualWithThickness(CutSize $size): bool
    {
        // localSize
        $p1 = $this->width;
        $p2 = $this->length;
        if ($p2 > $p1) {
            $p  = $p1;
            $p1 = $p2;
            $p2 = $p;
        }

        // getSize
        $o1 = $size->width;
        $o2 = $size->length;
        if ($o2 > $o1) {
            $o  = $o1;
            $o1 = $o2;
            $o2 = $o;
        }

        if ($this->thickness && $size->thickness) {
            if ($p1 >= $o1 && $p2 >= $o2 && $this->thickness >= $size->thickness) {
                return true;
            }
        } else {
            if ($p1 >= $o1 && $p2 >= $o2) {
                return true;
            }
        }
        return false;
    }

    public function isEqual(CutSize $size): bool
    {
        if (($this->width === $size->width && $this->length === $this->length) || ($this->width === $size->length && $this->length == $size->width)) {
            if ($this->thickness && $size->thickness) {
                if ($this->thickness === $size->thickness) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    public function isZero()
    {
        return !$this->width && !$this->length;
    }

    public function getKey()
    {
        return $this->width . '-' . $this->length . '-' . $this->thickness;
    }

}
