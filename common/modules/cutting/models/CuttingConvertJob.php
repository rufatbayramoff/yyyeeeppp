<?php

namespace common\modules\cutting\models;

use common\interfaces\FileBaseInterface;
use common\models\CuttingPackFile;
use common\models\FileJob;
use console\jobs\FileQueueJob;
use console\jobs\model3d\RenderJob;
use console\jobs\QueueGateway;
use console\jobs\RabbitJob;

class CuttingConvertJob extends FileQueueJob  implements RabbitJob
{
    protected $code = FileJob::JOB_CUTTING_CONVERT;

    public function doJob()
    {

    }

    /**
     * @param CuttingPackFile $cuttingPackFile
     * @return FileJob
     * @throws \yii\base\Exception
     */
   public static function addConvertJob(CuttingPackFile $cuttingPackFile)
   {
       $job = CuttingConvertJob::create($cuttingPackFile->file);
       return QueueGateway::addFileJob($job);
   }


    /**
     * @param \common\models\FileJob $job
     * @return bool
     */
    public function isSameJob(\common\models\FileJob $job)
    {
        $args = $job->getArgsData();
        return $args['file_id']===$this->args['file_id'];
    }
}