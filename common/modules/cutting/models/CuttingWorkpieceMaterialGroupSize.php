<?php

namespace common\modules\cutting\models;

use Braintree\Base;
use lib\money\Money;
use yii\base\BaseObject;

class CuttingWorkpieceMaterialGroupSize extends BaseObject
{
    /** @var CutSize */
    public $size;


    /** @var CuttingWorkpieceMaterialGroupColors[] */
    public $groupColors;

    /**
     * @var Money|null
     */
    public $price;
}