<?php

namespace common\modules\cutting\models;

use common\models\CuttingMaterial;
use common\models\PrinterColor;
use yii\base\BaseObject;

class OfferMaterialGroup extends BaseObject
{
    /**
     * @var CuttingMaterial
     */
    public $material;

    /**
     * @var  array
     */
    public $thickensList;

    /**
     * @var PrinterColor
     */
    public $colorsList;

}