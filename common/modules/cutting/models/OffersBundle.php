<?php

namespace common\modules\cutting\models;

use common\modules\printersList\models\AnswerInfo;

class OffersBundle extends \yii\base\BaseObject
{
    public $emptyReason = '';

    /** @var array */
    public $allowedMaterialsColors;

    /** @var array */
    public $offers;

    /** @var int */
    public $totalCount;

    /** @var int */
    public $pageSize;
}
