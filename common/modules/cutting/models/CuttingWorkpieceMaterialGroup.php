<?php

namespace common\modules\cutting\models;

use Braintree\Base;
use common\models\CuttingMaterial;
use common\models\CuttingWorkpieceMaterial;
use common\models\PrinterColor;
use lib\money\Money;
use yii\base\BaseObject;

class CuttingWorkpieceMaterialGroup extends  BaseObject
{
    /**
     * @var CuttingMaterial
     */
    public $material;

    /**
     * @var CuttingWorkpieceMaterialGroupSize[];
     */
    public $sizes;

    public Money|null $price;
}