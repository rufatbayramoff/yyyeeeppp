<?php

namespace common\modules\cutting\models;

use common\components\AbstractPopulator;
use common\components\exceptions\NoActiveModel3d;
use common\interfaces\Model3dBaseInterface;
use common\models\Company;
use common\models\CuttingMachine;
use common\models\CuttingMachineProcessing;
use common\models\CuttingPack;
use common\models\CuttingWorkpieceMaterial;
use common\modules\company\populators\CompanyServicePopulator;
use frontend\models\model3d\PlaceOrderCommonState;
use frontend\models\model3d\PlaceOrderStateInterface;
use frontend\models\model3d\PlaceOrderStateTrait;
use frontend\models\user\UserFacade;
use yii\base\BaseObject;

class PlaceCuttingOrderState extends BaseObject implements PlaceOrderStateInterface
{
    use PlaceOrderStateTrait, PlaceOrderCommonState {
    }

    public const SESSION_KEY = 'placeCuttingOrder';

    public function getCuttingPack(): ?CuttingPack
    {
        $state = $this->getState();
        $cuttingPackUuid = $state['upload']['cuttingPackUuid'] ?? null;
        $cuttingPack = null;
        if ($cuttingPackUuid) {
            $cuttingPack = CuttingPack::tryFind(['uuid'=>$cuttingPackUuid]);
        }
        /* Sometime session is differ?!!
        $userSession = UserFacade::getUserSession();
        if ($cuttingPack && $cuttingPack->user_session_id != $userSession->id) {
            die('Fuck!');
        }*/
        return $cuttingPack;
    }

    public function setCuttingPack(CuttingPack $cuttingPack)
    {
        $state = $this->getState();
        $state['upload']['cuttingPackUuid'] = $cuttingPack->uuid;
        $this->saveState($state);
    }

    public function getPsIdOnly()
    {
        $state = $this->getState();
        return $state['cutting']['psId'] ?? 0;
    }


    public function setPsIdOnly($psId): void
    {
        $state = $this->getState();
        $state['cutting']['psId'] = (int)$psId;
        $this->saveState($state);
    }

    public function setPsMachineIdOnly($machineId): void
    {
        $state = $this->getState();
        $state['cutting']['machineId'] = (int)$machineId;
        $this->saveState($state);
    }

    public function getPsMachineIdOnly()
    {
        $state = $this->getState();
        return $state['cutting']['machineId'] ?? 0;
    }

}