<?php

namespace common\modules\cutting\models;

use common\models\CuttingMaterial;
use common\models\PrinterColor;
use yii\base\BaseObject;

class CuttingPartMaterialInfo extends BaseObject
{
    /** @var CuttingMaterial */
    public $cuttingMaterial;

    /** @var float */
    public $thickness;

    /** @var PrinterColor */
    public $cuttingColor;
}