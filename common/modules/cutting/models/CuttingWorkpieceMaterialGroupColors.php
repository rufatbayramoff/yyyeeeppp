<?php

namespace common\modules\cutting\models;

use common\models\CuttingWorkpieceMaterial;
use common\models\PrinterColor;
use lib\money\Money;

class CuttingWorkpieceMaterialGroupColors extends \yii\base\BaseObject
{
    /** @var CuttingWorkpieceMaterialGroupColor[] */
    public $colors;

    /** @var Money|null */

    public Money|null $price;
}