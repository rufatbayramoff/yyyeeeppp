<?php

namespace common\modules\cutting\models;

use common\models\CuttingWorkpieceMaterial;
use common\models\PrinterColor;
use lib\money\Money;

class CuttingWorkpieceMaterialGroupColor extends \yii\base\BaseObject
{
    /** @var CuttingWorkpieceMaterial */
    public $cuttingWorkpieceMaterial;

    /** @var PrinterColor */
    public $color;
}