<?php
namespace common\modules\cutting\models;

use common\components\ps\locator\Size;
use common\models\CuttingMachine;
use common\models\CuttingPack;
use common\modules\printersList\models\AnswerInfo;

class CuttingItem
{
    /**
     * @var CuttingMachine
     */
    public $cuttingMachine;

    /**
     * @var CuttingPack
     */
    public $cuttingPack;
}
