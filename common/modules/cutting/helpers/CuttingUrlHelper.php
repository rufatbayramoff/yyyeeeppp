<?php

namespace common\modules\cutting\helpers;

use common\models\CuttingMachine;
use common\models\CuttingPack;
use common\models\CuttingPackFile;
use http\Exception\InvalidArgumentException;

class CuttingUrlHelper
{
    public static function addMachine()
    {
        return '/mybusiness/cutting/cutting/create';
    }

    public static function editMachine(CuttingMachine $cuttingMachine)
    {
        return '/mybusiness/cutting/cutting/edit?id=' . $cuttingMachine->id;
    }

    public static function makeOrderUploadFiles()
    {
        return '/my/order-laser-cutting/upload-file';
    }

    public static function previewUrl(CuttingPack $cuttingPack, $isAbsoluteUrl = false)
    {
        return \Yii::$app->params['siteUrl'] . '/my/order-laser-cutting/preview-pack?uuid=' . $cuttingPack->uuid;
    }
}
