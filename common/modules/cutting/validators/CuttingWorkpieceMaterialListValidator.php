<?php

namespace common\modules\cutting\validators;

use common\components\exceptions\BusinessException;
use common\components\exceptions\InvalidModelException;
use common\models\CuttingWorkpieceMaterial;
use yii\base\BaseObject;

class CuttingWorkpieceMaterialListValidator extends BaseObject
{
    /**
     * @param CuttingWorkpieceMaterial[] $list
     */
    public function validateOrFail($list)
    {
        foreach ($list as $cuttingWorkpieceMaterial) {
            if (!$cuttingWorkpieceMaterial->validate()) {
                throw new InvalidModelException($cuttingWorkpieceMaterial);
            }
        }
    }

    /**
     * @param CuttingWorkpieceMaterial[] $list
     */
    public function validateEmpty($list)
    {
        if(empty($list)) {
            throw new BusinessException(_t('site.ps','Materials are required'));
        }
    }
}