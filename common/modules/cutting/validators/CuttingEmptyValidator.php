<?php


namespace common\modules\cutting\validators;


use common\components\exceptions\BusinessException;
use common\components\exceptions\InvalidModelException;
use common\models\CuttingMachine;
use yii\base\UserException;
use yii\web\HttpException;

class CuttingEmptyValidator
{
    public function validateEmpty(CuttingMachine $cuttingMachine)
    {
        $errors = [];
        if (empty($cuttingMachine->companyService->company->cuttingWorkpieceMaterials)) {
            $errors[] = _t('site.ps', 'Materials');
        }
        if (empty($cuttingMachine->cuttingMachineProcessings)) {
            $errors[] = _t('site.ps', 'Rates');
        }
        if (empty($cuttingMachine->companyService->psMachineDeliveries)) {
            $errors[] = _t('site.ps', 'Delivery');
        }
        if (!empty($errors)) {
            $message = implode(', ', $errors);
            $message .= _t('site.ps', ' options are required');
            throw new BusinessException($message, 400);
        }
    }
}