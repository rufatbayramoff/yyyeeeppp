<?php

namespace common\modules\cutting\repositories;

use common\models\CuttingMachine;
use common\models\CuttingMaterial;
use common\models\CuttingPack;
use common\models\repositories\FileRepository;
use common\modules\company\populators\CompanyServicePopulator;
use common\modules\company\repositories\CompanyServiceRepository;
use yii\base\BaseObject;

class CuttingPackRepository extends BaseObject
{

    /** @var FileRepository */
    protected $fileRepository;

    public function injectDependencies(
        FileRepository $fileRepository
    )
    {
        $this->fileRepository = $fileRepository;
    }


    /**
     * @param CuttingPack $cuttingPack
     *
     * @throws \Throwable
     * @throws \common\components\exceptions\BusinessException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\db\StaleObjectException
     */
    public function save(CuttingPack $cuttingPack)
    {
        $cuttingPackFiles = $cuttingPack->cuttingPackFiles;
        $cuttingPack->safeSave();
        foreach ($cuttingPackFiles as $cuttingPackFile) {
            $this->fileRepository->save($cuttingPackFile->file);
            foreach ($cuttingPackFile->cuttingPackPages as $cuttingPackPage) {
                $this->fileRepository->save($cuttingPackPage->file);
                $parts = $cuttingPackPage->cuttingPackParts;
                $images = $cuttingPackPage->cuttingPackPageImages;
                $cuttingPackPage->safeSave();
                foreach ($parts as $packPart) {
                    if ($packPart->forDelete) {
                        $packPart->delete();
                    } else {
                        $packPart->cutting_pack_page_uuid = $cuttingPackPage->uuid;
                        $packPart->safeSave();
                    }
                }
                foreach ($images as $image) {
                    if ($image->file) {
                        $this->fileRepository->save($image->file);
                    }
                    $image->safeSave();
                }
            }

            $cuttingPackFile->cutting_pack_uuid = $cuttingPack->uuid;
            $cuttingPackFile->safeSave();
        }
    }
}
