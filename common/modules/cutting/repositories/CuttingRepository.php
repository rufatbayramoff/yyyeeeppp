<?php

namespace common\modules\cutting\repositories;

use common\models\Company;
use common\models\CompanyService;
use common\models\CuttingMachine;
use common\models\CuttingMaterial;
use common\models\Ps;
use common\modules\company\populators\CompanyServicePopulator;
use common\modules\company\repositories\CompanyServiceRepository;
use frontend\models\user\UserFacade;
use yii\base\BaseObject;

class CuttingRepository extends BaseObject
{
    /**
     * @var CompanyServiceRepository
     */
    protected $companyServiceRepository;

    public function injectDependencies(
        CompanyServiceRepository $companyServiceRepository
    )
    {
        $this->companyServiceRepository = $companyServiceRepository;
    }

    /**
     * @param CuttingMachine $cuttingMachine
     * 
     * @throws \Throwable
     * @throws \common\components\exceptions\BusinessException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function save(CuttingMachine $cuttingMachine)
    {
        $cuttingMachine->safeSave();
        $cuttingMachine->companyService->cutting_machine_id = $cuttingMachine->id;
        $this->companyServiceRepository->setServiceImagesRotate($cuttingMachine->companyService);
        $this->companyServiceRepository->save($cuttingMachine->companyService);
    }

    public function saveProcessing(CuttingMachine $cuttingMachine)
    {
        foreach ($cuttingMachine->cuttingMachineProcessings as $processing) {
            if ($processing->forDelete) {
                $processing->delete();
            } else {
                $processing->safeSave();
            }
        }
    }

    public function saveCompanyProcessings(Company $company)
    {
        foreach ($company->companyServices as $companyService) {
            if ($companyService->isCutting()) {
                $this->saveProcessing($companyService->cuttingMachine);
            }
        }
    }

    public function delete(CuttingMachine $cuttingMachine)
    {
        $this->companyServiceRepository->delete($cuttingMachine->companyService);
    }

    public function getCuttingMaterials()
    {
        return CuttingMaterial::find()->active()->all();
    }
}