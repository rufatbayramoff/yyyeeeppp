<?php

namespace common\modules\cutting\repositories;

use common\models\Company;
use common\models\CuttingWorkpieceMaterial;
use common\modules\company\repositories\CompanyServiceRepository;
use common\modules\cutting\models\CuttingWorkpieceMaterialGroup;
use common\modules\cutting\models\CuttingWorkpieceMaterialGroupColor;
use common\modules\cutting\models\CuttingWorkpieceMaterialGroupColors;
use common\modules\cutting\models\CuttingWorkpieceMaterialGroupSize;
use lib\money\Money;
use yii\base\BaseObject;

class CuttingWorkpieceMaterialRepository extends BaseObject
{
    /**
     * @var CompanyServiceRepository
     */
    protected $companyServiceRepository;

    public function injectDependencies(
        CompanyServiceRepository $companyServiceRepository
    )
    {

    }

    /**
     * Save worpiece materials
     *
     * @param Company $company
     */
    public function save(Company $company)
    {
        foreach ($company->cuttingWorkpieceMaterials as $cuttingWorkpieceMaterial) {
            if ($cuttingWorkpieceMaterial->forDelete) {
                $cuttingWorkpieceMaterial->delete();
                continue;
            }
            $cuttingWorkpieceMaterial->safeSave();
        }
    }

    /**
     * @param CuttingWorkpieceMaterial[] $cuttingWorkpieceMaterials
     */
    public function initWorkpieceMaterialGroup($cuttingWorkpieceMaterials)
    {
        $currency = null;
        $groups = [];
        foreach ($cuttingWorkpieceMaterials as $cuttingWorkpieceMaterial) {

            if (!$cuttingWorkpieceMaterial->material_id || $cuttingWorkpieceMaterial->forDelete) {
                continue;
            }
            $currency = $currency?:$cuttingWorkpieceMaterial->company->currency;
            $cuttingPrice =  Money::create($cuttingWorkpieceMaterial->price, $currency)->round();

            if (!array_key_exists($cuttingWorkpieceMaterial->material_id, $groups)) {
                $group                                          = new CuttingWorkpieceMaterialGroup();
                $group->material                                = $cuttingWorkpieceMaterial->material;
                $group->sizes                                   = [];
                $group->price                                   = $cuttingPrice;
                $groups[$cuttingWorkpieceMaterial->material_id] = $group;
            } else {
                $group = $groups[$cuttingWorkpieceMaterial->material_id];
            }
            $worksize = $cuttingWorkpieceMaterial->getWorkSize();
            $sizeKey  = $worksize->getKey();

            if (!array_key_exists($sizeKey, $group->sizes)) {
                $groupSize              = new CuttingWorkpieceMaterialGroupSize();
                $groupSize->size        = $cuttingWorkpieceMaterial->getWorkSize();
                $groupSize->price       = $cuttingPrice;
                $groupSize->groupColors = [];
                $group->sizes[$sizeKey] = $groupSize;
            } else {
                $groupSize = $group->sizes[$sizeKey];
            }

            if ($groupSize->price?->getAmount() != $group->price?->getAmount()) {
                $group->price = null;
            }

            if (!array_key_exists($cuttingPrice->getAmount(), $groupSize->groupColors)) {
                $groupColors                           = new CuttingWorkpieceMaterialGroupColors();
                $groupColors->price                    = $cuttingPrice;
                $groupSize->groupColors[$cuttingPrice->getAmount()] = $groupColors;
            } else {
                $groupColors = $groupSize->groupColors[$cuttingPrice->getAmount()];
            }
            $groupColor                           = new CuttingWorkpieceMaterialGroupColor();
            $groupColor->color                    = $cuttingWorkpieceMaterial->color;
            $groupColor->cuttingWorkpieceMaterial = $cuttingWorkpieceMaterial;

            $groupColors->colors[$cuttingWorkpieceMaterial->color_id] = $groupColor;

            if ($group->price?->getAmount() != $groupColors->price?->getAmount()) {
                $group->price = null;
            }
            if ($groupSize->price?->getAmount() != $groupColors->price?->getAmount()) {
                $groupSize->price = null;
            }
        }

        // Clean same prices
        foreach ($groups as $group) {
            foreach ($group->sizes as $size) {
                if ($group->price) {
                    $size->price = null;
                }
                foreach ($size->groupColors as $color) {
                    if ($group->price || $size->price) {
                        $color->price = null;
                    }
                }
            }
        }


        return $groups;
    }

    public function getWorkpieceMaterialGroups(Company $company)
    {
        return $this->initWorkpieceMaterialGroup($company->cuttingWorkpieceMaterials);
    }
}
