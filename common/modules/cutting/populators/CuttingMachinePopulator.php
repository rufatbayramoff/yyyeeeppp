<?php

namespace common\modules\cutting\populators;

use common\components\AbstractPopulator;
use common\models\Company;
use common\models\CuttingMachine;
use common\models\CuttingMachineProcessing;
use common\models\CuttingWorkpieceMaterial;
use common\modules\company\populators\CompanyServicePopulator;

class CuttingMachinePopulator extends AbstractPopulator
{
    /**
     * @var CompanyServicePopulator
     */
    protected $companyServicePopulator;

    public function injectDependencies(
        CompanyServicePopulator $companyServicePopulator
    )
    {
        $this->companyServicePopulator = $companyServicePopulator;
    }


    /**
     * @param CuttingMachine $cuttingMachine
     * @param $data
     * @return void
     * @throws \yii\base\InvalidConfigException
     */
    public function populate(CuttingMachine $cuttingMachine, $data)
    {
        // Cutting Machine
        $formName = $cuttingMachine->formName();
        if (array_key_exists($formName, $data)) {
            $cuttingForm = $data[$formName];
            if ($cuttingForm) {
                $this->loadAttributes($cuttingMachine, $cuttingForm);
            }
        }
        // Company Service
        $companyService = $cuttingMachine->companyService;
        $this->companyServicePopulator
            ->populate($companyService, $data, $cuttingMachine->companyService->formName())
            ->populateFiles($companyService, 'images')
            ->populateImagesRotateOptions($companyService, $data);
    }

    public function populateProcessing(CuttingMachine $cuttingMachine, $data)
    {
        if (array_key_exists('processing', $data)) {
            $currentProcessings = $cuttingMachine->cuttingMachineProcessings;

            // Mark for delete
            foreach ($currentProcessings as $processing) {
                $processing->forDelete = 1;
            }

            foreach ($data['processing'] as $processingData) {
                $currentProcessing = $this->getCurrentProcessing($cuttingMachine, $processingData['uuid']);
                if (!$currentProcessing) {
                    $currentProcessing                     = new CuttingMachineProcessing();
                    $currentProcessing->uuid               = $processingData['uuid'];
                    $currentProcessing->cutting_machine_id = $cuttingMachine->id;
                }
                if (array_key_exists('cuttingPrice', $processingData)) {
                    $currentProcessing->cutting_price = $processingData['cuttingPrice'] / 1000;
                }
                if (array_key_exists('engravingPrice', $processingData)) {
                    $currentProcessing->engraving_price = $processingData['engravingPrice'] / 1000;
                }
                if (array_key_exists('workpieceMaterialProcessingUuid', $processingData)) {
                    [$materialId, $thickness] = explode('_', $processingData['workpieceMaterialProcessingUuid']);
                    $currentProcessing->cutting_material_id = $materialId;
                    $currentProcessing->thickness           = $thickness;
                }

                $currentProcessing->forDelete = 0;
                $currentProcessings[]         = $currentProcessing;
            }
            $cuttingMachine->populateRelation('cuttingMachineProcessings', $currentProcessings);
        }
    }

    public function getCurrentProcessing(CuttingMachine $cuttingMachine, $uuid): ?CuttingMachineProcessing
    {
        foreach ($cuttingMachine->cuttingMachineProcessings as $processing) {
            if ($processing->uuid == $uuid) {
                return $processing;
            }
        }
        return null;
    }

    /**
     * @param Company $cuttingMachine
     * @param $data
     */
    public function loadAttributes(CuttingMachine $cuttingMachine, $data)
    {
        $this->populateAttributes($cuttingMachine, $data, [
            'max_width',
            'max_length',
            'min_order_price',
            'technology'
        ]);
    }
}