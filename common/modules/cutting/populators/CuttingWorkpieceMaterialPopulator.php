<?php

namespace common\modules\cutting\populators;

use common\components\AbstractPopulator;
use common\components\UuidHelper;
use common\models\Company;
use common\models\CuttingMachine;
use common\models\CuttingWorkpieceMaterial;
use common\models\repositories\PrinterColorRepository;
use common\modules\company\populators\CompanyServicePopulator;

class CuttingWorkpieceMaterialPopulator extends AbstractPopulator
{
    /**
     * @var CompanyServicePopulator
     */
    protected $companyServicePopulator;

    /**
     * @var PrinterColorRepository
     */
    protected $printerColorRepository;

    public function injectDependencies(
        CompanyServicePopulator $companyServicePopulator,
        PrinterColorRepository $printerColorRepository
    )
    {
        $this->companyServicePopulator = $companyServicePopulator;
        $this->printerColorRepository  = $printerColorRepository;
    }

    public function getCurrentWorkpieceMaterial($currentWorkpieceMaterials, $uuid): ?CuttingWorkpieceMaterial
    {
        foreach ($currentWorkpieceMaterials as $workpieceMaterial) {
            if ($workpieceMaterial->uuid === $uuid) {
                return $workpieceMaterial;
            }
        }
        return null;
    }

    /**
     * @param CuttingWorkpieceMaterial $workpieceMaterial
     * @param array $materialGroupColor
     * @param array $materialGroupSize
     * @param array $materialGroup
     */
    public function loadWorkpieceMaterial(CuttingWorkpieceMaterial $workpieceMaterial, $materialGroupColor, $materialGroupSize, $materialGroup)
    {
        if (array_key_exists('code', $materialGroupColor)) {
            $printerColor                = $this->printerColorRepository->getByCode($materialGroupColor['code']);
            $workpieceMaterial->color_id = $printerColor->id;
        }
        $workpieceMaterial->price       = $materialGroupColor['price'] ?? null;
        $workpieceMaterial->width       = $materialGroupSize['width'] ?? null;
        $workpieceMaterial->height      = $materialGroupSize['height'] ?? null;
        $workpieceMaterial->thickness   = $materialGroupSize['thickness'] ?? null;
        $workpieceMaterial->material_id = $materialGroup['materialId'];
    }

    public function populateWorkpieceMaterialGroups(Company $company, $data): void
    {
        $currentWorkpieceMaterials = $company->cuttingWorkpieceMaterials;

        // Mark for delete
        foreach ($currentWorkpieceMaterials as $currentWorkpieceMaterial) {
            $currentWorkpieceMaterial->forDelete = 1;
        }

        if (array_key_exists('workpieceMaterialGroups', $data)) {
            $materialGroups = $data['workpieceMaterialGroups'];
            foreach ($materialGroups as $materialGroup) {
                $sizes = [];
                foreach ($materialGroup['sizes'] as $materialGroupSize) {
                    foreach ($materialGroupSize['colors'] as $materialGroupColor) {
                        $sizeKey         = ($materialGroupSize['width']??'') . '_' . ($materialGroupSize['height']??'') . '_' . ($materialGroupSize['thickness']??'').'_'.($materialGroupColor['code']??'');
                        $materialGroupColor['materialGroupSize'] = $materialGroupSize;
                        $sizes[$sizeKey] = $materialGroupColor;
                    }
                }
                foreach ($sizes as $materialGroupColor) {
                    $materialGroupSize = $materialGroupColor['materialGroupSize'];
                    $currentWorkpieceMaterial = $this->getCurrentWorkpieceMaterial($currentWorkpieceMaterials, $materialGroupColor['uuid']);
                    if (!$currentWorkpieceMaterial) {
                        $currentWorkpieceMaterial             = new CuttingWorkpieceMaterial();
                        $currentWorkpieceMaterial->uuid       = $materialGroupColor['uuid'];
                        $currentWorkpieceMaterial->company_id = $company->id;
                        $currentWorkpieceMaterials[]          = $currentWorkpieceMaterial;
                    }
                    $this->loadWorkpieceMaterial($currentWorkpieceMaterial, $materialGroupColor, $materialGroupSize, $materialGroup);
                    $currentWorkpieceMaterial->forDelete = 0;
                }
            }
        }
        $company->populateRelation('cuttingWorkpieceMaterials', $currentWorkpieceMaterials);
    }
}
