<?php

namespace common\modules\cutting\populators;

use common\components\AbstractPopulator;
use common\models\Company;
use common\models\CuttingMachine;
use common\models\CuttingMachineProcessing;
use common\models\CuttingPack;
use common\models\CuttingPackFile;
use common\models\CuttingPackPage;
use common\models\CuttingPackPart;
use common\modules\company\populators\CompanyServicePopulator;
use common\modules\cutting\factories\CuttingPackFactory;

class CuttingPackPopulator extends AbstractPopulator
{
    /**
     * @var CompanyServicePopulator
     */
    protected $companyServicePopulator;

    /** @var CuttingPackFactory */
    protected $cuttingPackFactory;

    public function injectDependencies(
        CompanyServicePopulator $companyServicePopulator,
        CuttingPackFactory $cuttingPackFactory
    )
    {
        $this->companyServicePopulator = $companyServicePopulator;
        $this->cuttingPackFactory      = $cuttingPackFactory;
    }


    /**
     * @param CuttingPack $cuttingPack
     * @param $data
     * @return void
     */
    public function populatePackPage(CuttingPack $cuttingPack, $data)
    {
        foreach ($cuttingPack->cuttingPackFiles as $cuttingPackFile) {
            foreach ($cuttingPackFile->cuttingPackPages as $cuttingPackPage) {
                if ($cuttingPackPage->uuid === $data['uuid']) {
                    $this->populatePageSelections($cuttingPackPage, $data);
                    $this->populatePageParts($cuttingPackPage, $data);
                }
            }
        }
    }

    public function populatePageSelections(CuttingPackPage $cuttingPackPage, $data)
    {
        if (!array_key_exists('selections', $data)) {
            return;
        }
        $imgData                     = substr($data['selections'], strpos($data['selections'], ",") + 1);
        $imgData                     = base64_decode($imgData);
        $cuttingPackPage->selections = $imgData;
    }

    public function populatePageParts(CuttingPackPage $cuttingPackPage, $data)
    {
        if (!array_key_exists('cuttingPackParts', $data)) {
            return;
        }
        $cuttingParts = $cuttingPackPage->cuttingPackParts;
        foreach ($cuttingPackPage->cuttingPackParts as $cuttingPackPart) {
            $cuttingPackPart->forDelete = 1;
        }

        foreach ($data['cuttingPackParts'] as $cuttingPackPartInfo) {
            $exists = false;
            foreach ($cuttingPackPage->cuttingPackParts as $cuttingPackPart) {
                if ($cuttingPackPartInfo['uuid'] === $cuttingPackPart->uuid) {
                    $exists = true;
                    $this->loadPartsAttributes($cuttingPackPart, $cuttingPackPartInfo);
                    $cuttingPackPart->forDelete = 0;
                    break;
                }
            }

            if (!$exists) {
                $cuttingPart = $this->cuttingPackFactory->createCuttingPackPart($cuttingPackPage);
                $this->loadPartsAttributes($cuttingPart, $cuttingPackPartInfo);
                $cuttingPart->uuid = $cuttingPackPartInfo['uuid'];
                $cuttingParts[]    = $cuttingPart;
            }
        }
        $cuttingPackPage->populateRelation('cuttingPackParts', $cuttingParts);
    }

    /**
     * @param CuttingPackPart $cuttingPackPart
     * @param $data
     */
    public function loadPartsAttributes(CuttingPackPart $cuttingPackPart, $data)
    {
        $this->populateAttributes($cuttingPackPart, $data, [
            'width',
            'height',
            'cutting'    => 'cutting_length',
            'engrave'    => 'engraving_length',
            'materialId' => 'material_id',
            'thickness',
            'colorId'    => 'color_id',
            'image'
        ]);
    }


    /**
     *
     * @param CuttingPackPart $packPart
     * @param array $data
     */
    public function populatePartMaterialInfo(CuttingPackPart $packPart, $data)
    {
        if (array_key_exists('qty', $data)) {
            $packPart->qty = (int)$data['qty'];
        }
    }

    /**
     * @param CuttingPack $cuttingPack
     * @param $data
     */
    public function populateMaterialInfo(CuttingPack $cuttingPack, $data)
    {
        if (array_key_exists('materialId', $data)) {
            $cuttingPack->material_id = $data['materialId'];
        }
        if (array_key_exists('colorId', $data)) {
            $cuttingPack->color_id = $data['colorId'];
        }
        if (array_key_exists('thickness', $data)) {
            $cuttingPack->thickness = $data['thickness'];
        }

        if (array_key_exists('cuttingParts', $data)) {
            foreach ($cuttingPack->cuttingPackFiles as $packFile) {
                if (!$packFile->is_active) {
                    continue;
                }
                foreach ($packFile->cuttingPackPages as $page) {
                    foreach ($page->cuttingPackParts as $part) {
                        if (!$part->is_active) {
                            continue;
                        }
                        foreach ($data['cuttingParts'] as $cuttingPartInfo) {
                            if ($part->uuid == $cuttingPartInfo['uuid']) {
                                $this->populatePartMaterialInfo($part, $cuttingPartInfo);
                            }
                        }
                    }
                }
            }
        }
    }
}