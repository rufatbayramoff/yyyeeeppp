<?php

namespace common\modules\cutting\factories;

use common\components\PaymentExchangeRateFacade;
use common\models\Company;
use common\models\CompanyService;
use common\models\CuttingMachine;
use common\models\repositories\CompanyServiceCategoryRepository;
use common\models\user\UserIdentityProvider;
use common\modules\company\factories\CompanyServiceFactory;
use frontend\models\user\UserFacade;
use lib\money\Currency;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

class CuttingMachineFactory extends BaseObject
{
    /** @var CompanyServiceFactory */
    protected $companyServiceFactory;

    /** @var CompanyServiceCategoryRepository $companyServiceCategoryRepository */
    protected $companyServiceCategoryRepository;

    /** @var UserIdentityProvider */
    protected $userIdentityProvider;

    public function injectDependencies(
        CompanyServiceFactory $companyServiceFactory,
        CompanyServiceCategoryRepository $companyServiceCategoryRepository,
        UserIdentityProvider $userIdentityProvider
    )
    {
        $this->companyServiceFactory            = $companyServiceFactory;
        $this->companyServiceCategoryRepository = $companyServiceCategoryRepository;
        $this->userIdentityProvider             = $userIdentityProvider;
    }

    /**
     * @return CuttingMachine
     */
    public function createForCurrentUser(): CuttingMachine
    {
        $currentUser = $this->userIdentityProvider->getUser();
        if (!$currentUser) {
            throw new InvalidArgumentException('Anonimus user');
        }
        if (!$currentUser->company) {
            throw new InvalidArgumentException('No company for current user');
        }
        $company = $currentUser->company;
        return $this->createForCompany($company);
    }

    public function createForCompany(Company $company): CuttingMachine
    {
        /** @var CuttingMachine $cuttingMachine */
        $cuttingMachine = \Yii::createObject(CuttingMachine::class);
        $companyService = $this->companyServiceFactory->createDraftService($company);
        $cuttingMachine->setPsMachine($companyService);
        $companyService->setCuttingMachine($cuttingMachine);
        $companyService->visibility      = CompanyService::VISIBILITY_EVERYWHERE;
        $companyService->type            = CompanyService::TYPE_CUTTING;
        $companyService->category_id     = $this->companyServiceCategoryRepository->getLaserCutting()->id;
        $cuttingMachine->min_order_price = PaymentExchangeRateFacade::convert(CuttingMachine::RECCOMEND_MIN_ORDER_PRICE_IN_USD, Currency::USD, UserFacade::getCurrentCurrency(), true);
        return $cuttingMachine;
    }
}