<?php

namespace common\modules\cutting\factories;

use common\components\DateHelper;
use common\components\FileTypesHelper;
use common\components\UuidHelper;
use common\models\CuttingPackFile;
use common\models\CuttingPackPage;
use common\models\CuttingPackPageImage;
use common\models\CuttingPackPart;
use common\models\File;
use common\models\UserSession;
use common\models\CuttingPack;
use common\models\repositories\UserSessionRepository;
use common\models\User;
use common\models\user\UserIdentityProvider;
use Yii;
use yii\base\BaseObject;

class CuttingPackFactory extends BaseObject
{
    /** @var UserIdentityProvider */
    protected $userIdentityProvider;

    /** @var UserSessionRepository */
    protected $userSessionRepository;

    public function injectDependencies(
        UserIdentityProvider $userIdentityProvider,
        UserSessionRepository $userSessionRepository
    )
    {
        $this->userIdentityProvider  = $userIdentityProvider;
        $this->userSessionRepository = $userSessionRepository;
    }

    public function createForCurrentUser(): CuttingPack
    {
        $currentUser        = $this->userIdentityProvider->getUser();
        $currentUserSession = $this->userSessionRepository->getCurrent();
        return $this->createForUser($currentUser, $currentUserSession);
    }

    /**
     * @param $currentUser
     * @param $currentUserSession
     * @return CuttingPack
     * @throws \yii\base\InvalidConfigException
     */
    public function createForUser(?User $currentUser, ?UserSession $currentUserSession): CuttingPack
    {
        $cuttingPack                 = Yii::createObject(CuttingPack::class);
        $cuttingPack->uuid           = UuidHelper::generateUuid();
        $cuttingPack->title          = '';
        $cuttingPack->source         = CuttingPack::SOURCE_WEBSITE;
        $cuttingPack->source_details = [];
        $cuttingPack->populateRelation('material', $cuttingPack->getDefaultMaterial());
        $cuttingPack->material_id                    = $cuttingPack->material->id;
        $cuttingPack->thickness                      = $cuttingPack->material->default_thickness;
        $cuttingPack->user_id                        = $currentUser ? $currentUser->id : null;
        $cuttingPack->user_session_id                = $currentUserSession->id;
        $cuttingPack->created_at                     = DateHelper::now();
        $cuttingPack->updated_at                     = $cuttingPack->created_at;
        $cuttingPack->is_active                      = 1;
        $cuttingPack->is_same_material_for_all_parts = 1;
        return $cuttingPack;
    }

    public function createCuttingPackFile(CuttingPack $cuttingPack, File $file, string $uuid): CuttingPackFile
    {
        $cuttingPackFile       = Yii::createObject(CuttingPackFile::class);
        $cuttingPackFile->uuid = $uuid;
        $cuttingPackFile->populateRelation('cuttingPack', $cuttingPack);
        $cuttingPackFile->cutting_pack_uuid = $cuttingPack->uuid;
        $cuttingPackFile->populateRelation('file', $file);
        $cuttingPackFile->file_uuid = $file->uuid;
        $cuttingPackFile->type      = FileTypesHelper::isImage($file) ? CuttingPackFile::TYPE_IMAGE : CuttingPackFile::TYPE_MODEL;
        $cuttingPackFile->qty       = 1;
        $cuttingPackFile->is_active = 1;
        return $cuttingPackFile;
    }

    public function createCuttingPackPage(CuttingPackFile $cuttingPackFile, File $file): CuttingPackPage
    {
        $cuttingPackPage                         = Yii::createObject(CuttingPackPage::class);
        $cuttingPackPage->uuid                   = UuidHelper::generateUuid();
        $cuttingPackPage->cutting_pack_file_uuid = $cuttingPackFile->uuid;
        $cuttingPackPage->file_uuid              = $file->uuid;
        $cuttingPackPage->populateRelation('file', $file);
        $cuttingPackPage->populateRelation('cuttingPackFile', $cuttingPackFile);
        return $cuttingPackPage;
    }

    public function createCuttingPackImage(CuttingPackPage $cuttingPackPage, File $file): CuttingPackPageImage
    {
        $cuttingPackPageImage                         = Yii::createObject(CuttingPackPageImage::class);
        $cuttingPackPageImage->uuid                   = UuidHelper::generateUuid();
        $cuttingPackPageImage->cutting_pack_page_uuid = $cuttingPackPage->uuid;
        $cuttingPackPageImage->file_uuid              = $file->uuid;
        $cuttingPackPageImage->populateRelation('file', $file);
        $cuttingPackPageImage->populateRelation('cuttingPackPage', $cuttingPackPage);
        return $cuttingPackPageImage;
    }

    public function createCuttingPackPart(CuttingPackPage $packPage)
    {
        $cuttingPackPart                         = Yii::createObject(CuttingPackPart::class);
        $cuttingPackPart->cutting_pack_page_uuid = $packPage->uuid;
        $cuttingPackPart->is_active              = 1;
        $cuttingPackPart->color_id               = null;
        $cuttingPackPart->thickness              = null;
        $cuttingPackPart->material_id            = null;
        $cuttingPackPart->engraving_length       = 0;
        $cuttingPackPart->cutting_length         = 0;
        $cuttingPackPart->qty                    = 1;
        return $cuttingPackPart;
    }
}