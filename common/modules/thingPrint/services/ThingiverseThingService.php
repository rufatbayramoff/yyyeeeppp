<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.05.17
 * Time: 12:27
 */

namespace common\modules\thingPrint\services;

use common\components\ArrayHelper;
use common\components\FileDirHelper;
use common\interfaces\Model3dBasePartInterface;
use common\models\factories\FileFactory;
use common\models\factories\Model3dPartFactory;
use common\models\repositories\Model3dRepository;
use common\models\StoreUnit;
use common\models\ThingiverseThing;
use common\models\ThingiverseThingFile;
use common\modules\model3dRender\models\RenderImageUrl;
use common\modules\thingPrint\components\ThingiverseApiWrapper;
use common\modules\thingPrint\factories\Model3dFactory;
use common\modules\thingPrint\factories\ThingFactory;
use common\modules\thingPrint\factories\ThingiverseThingFactory;
use common\modules\thingPrint\factories\ThingiverseThingFileFactory;
use common\modules\thingPrint\repositories\ThingiverseThingFileRepository;
use common\modules\thingPrint\repositories\ThingiverseThingRepository;
use frontend\models\model3d\Model3dFacade;
use Yii;
use yii\base\Component;

class ThingiverseThingService extends Component
{
    /** @var  ThingiverseApiWrapper */
    public $apiWrapper;

    /** @var  FileFactory */
    public $fileFactory;

    /** @var  ThingFactory */
    public $thingFactory;

    /** @var  ThingiverseThingFactory */
    public $thingiverseThingFactory;

    /** @var  ThingiverseThingFileFactory */
    public $thingiverseThingFileFactory;

    /** @var  Model3dFactory */
    public $model3dFactory;

    /** @var  Model3dPartFactory */
    public $model3dPartFactory;

    /** @var  Model3dRepository */
    public $model3dRepository;

    /** @var  ThingiverseThingRepository */
    public $thingiverseThingRepository;

    /** @var ThingiverseThingFileRepository */
    public $thingiverseThingFileRepository;

    /**
     * @param ThingiverseApiWrapper $apiWrapper
     * @param FileFactory $fileFactory
     * @param ThingFactory $thingFactory
     * @param ThingiverseThingFactory $thingiverseThingFactory
     * @param Model3dFactory $model3dFactory
     * @param Model3dRepository $model3dRepository
     * @param ThingiverseThingRepository $thingiverseThingRepository
     * @param ThingiverseThingFileFactory $thingiverseThingFileFactory
     * @param Model3dPartFactory $model3dPartFactory
     * @param ThingiverseThingFileRepository $thingiverseThingFileRepository
     */
    public function injectDependencies(
        ThingiverseApiWrapper $apiWrapper,
        FileFactory $fileFactory,
        ThingFactory $thingFactory,
        ThingiverseThingFactory $thingiverseThingFactory,
        Model3dFactory $model3dFactory,
        Model3dRepository $model3dRepository,
        ThingiverseThingRepository $thingiverseThingRepository,
        ThingiverseThingFileFactory $thingiverseThingFileFactory,
        Model3dPartFactory $model3dPartFactory,
        ThingiverseThingFileRepository $thingiverseThingFileRepository
    )
    {
        $this->thingFactory = $thingFactory;
        $this->apiWrapper = $apiWrapper;
        $this->fileFactory = $fileFactory;
        $this->thingiverseThingFactory = $thingiverseThingFactory;
        $this->model3dFactory = $model3dFactory;
        $this->model3dRepository = $model3dRepository;
        $this->thingiverseThingRepository = $thingiverseThingRepository;
        $this->thingiverseThingFileFactory = $thingiverseThingFileFactory;
        $this->model3dPartFactory = $model3dPartFactory;
        $this->thingiverseThingFileRepository = $thingiverseThingFileRepository;
    }

    /**
     * @param $thingId
     * @return \common\models\Model3d
     * @throws \common\modules\thingPrint\components\ThingiverseApiException
     * @throws \yii\base\InvalidConfigException
     */
    public function createModel3dByThingId($thingId)
    {
        $thing = $this->apiWrapper->getThing($thingId);
        $this->apiWrapper->downloadThingCover($thing);

        $thingFiles = $this->apiWrapper->getThingFiles($thing->id);
        $thingFiles = $this->apiWrapper->validateAndTrimThingFiles($thingFiles);
        $this->apiWrapper->downloadThingFiles($thingId, $thingFiles);

        $thingiverseThing = $this->thingiverseThingFactory->createByThing($thing, $thingFiles);
        $model3d = $this->model3dFactory->createModel3dFromThing($thing, $thingiverseThing->thingiverseThingFiles);
        $thingiverseThing->setModel3d($model3d);

        $this->model3dRepository->save($model3d);
        $this->thingiverseThingRepository->save($thingiverseThing);
        return $model3d;
    }

    public function updateFiles(ThingiverseThing $thingiverseThing)
    {
        $currentFiles = $this->apiWrapper->getThingFiles($thingiverseThing->thing_id);
        $currentFiles = $this->apiWrapper->validateAndTrimThingFiles($currentFiles);

        $currentFilesIndexed = ArrayHelper::index($currentFiles, 'id');
        $currentFilesIds = array_keys($currentFilesIndexed);
        $oldFilesIndexed = ArrayHelper::index($thingiverseThing->getActiveThingiverseThingFiles(), 'thingfile_id');
        $oldFilesInds = array_keys($oldFilesIndexed);
        if ($currentFilesIndexed == $oldFilesIndexed) {
            return;
        }
        $newFileIds = array_diff($currentFilesIds, $oldFilesInds);
        $newThingFiles = [];
        foreach ($newFileIds as $newFileId) {
            $thingFile = $currentFilesIndexed[$newFileId];
            $this->apiWrapper->downloadThingFile($thingiverseThing->thing_id, $thingFile);
            $newThingFiles[] = $thingFile;
        }
        foreach ($newThingFiles as $newThingFile) {
            $thingiverseFile = $this->thingiverseThingFileFactory->createByThingFile($newThingFile);
            $thingiverseFile->setThing($thingiverseThing);
            $model3dPart = $this->model3dPartFactory->createModel3dPart($thingiverseThing->model3d, $thingiverseFile->file);

            $this->thingiverseThingFileRepository->save($thingiverseFile);
            $model3dPart->safeSave();
        }

        $oldFileIds = array_diff($oldFilesInds, $currentFilesIds);
        foreach ($oldFileIds as $oldFileId) {
            /** @var  ThingiverseThingFile $oldFile */
            $oldFile = $oldFilesIndexed[$oldFileId];
            if ($oldFile) {
                $oldFile->is_active = 0;
                $this->thingiverseThingFileRepository->save($oldFile);
                $model3dPart = $oldFile->getModel3dPart()->one();
                if ($model3dPart) {
                    $model3dPart->user_status = Model3dBasePartInterface::STATUS_INACTIVE;
                    $model3dPart->save(false);
                }
            }
        }
    }

    public function downloadByDirectLink($thingId)
    {
        $url = $this->apiWrapper->api->siteUrl . 'thing:' . $thingId . '/zip';
        $dirname = \Yii::getAlias('@runtime') . '/thingiverse';
        FileDirHelper::createDir($dirname);
        $filePath = $dirname . '/archive' . $thingId . '.zip';
        if (!file_exists($filePath) || filesize($filePath) < 100) {
            $fp = fopen($filePath, 'w+');

            $ch = curl_init();
            // r1.tsdev.work doesn`t work with sertificate
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 180);
            curl_exec($ch);

            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            fclose($fp);
        }
        if (filesize($filePath) < 100) {
            unlink($filePath);
            return null;
        }
        $file = $this->fileFactory->createFileFromPath($filePath);
        return $file;
    }
}