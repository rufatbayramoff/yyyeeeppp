<?php
namespace common\modules\thingPrint\models;

use yii\helpers\Json;

/**
 * Date: 17.01.17
 *
 * Wrapper file for Thing from thingiverse,
 * for easy use
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class Thing {

    public $id;
    public $name;
    public $url;
    public $thumbnail;
    public $description;
    public $instructions;
    public $file_count;
    public $details_parts;
    /**
     * @var ThingFile[]
     */
    public $files = [];

    public $default_image;

    public $tempCoverFilePath;

    /**
     * get full cover url
     * we replace to display large - to get larger image
     *
     * @return mixed
     */
    public function getCoverUrl()
    {
        if ((strpos($this->thumbnail, 'default/Gears') !== false) && $this->default_image && $this->thumbnail = $this->default_image->sizes[0]) {
            $this->thumbnail = $this->default_image->sizes[0]->url;
        }
        return str_replace('thumb_medium', 'display_large', $this->thumbnail);
    }

    /**
     * get details
     *
     * @return string
     */
    public function getDetails()
    {
        if(empty($this->details_parts)){
            return '';
        }
        $result = [];
        foreach($this->details_parts as $part){
            if($part->type==='summary'){
                continue;
            }
            if(!empty($part->data)){
                $result[] = 'Type: ' . $part->name;
                if(!empty($part->data->content)){
                    $result[] = $part->data->content;
                }else{
                    $result[] = 'Data: ' . Json::encode($part->data);
                }
                $result[] = "\n";
            }
        }
        $result = implode("\n", $result);
        return $result;
    }

}