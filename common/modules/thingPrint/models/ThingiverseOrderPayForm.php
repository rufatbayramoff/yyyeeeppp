<?php
/**
 * Date: 30.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\models;

use common\components\DateHelper;
use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\models\ThingiverseOrder;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\services\PaymentService;
use common\modules\thingPrint\components\ThingiverseFacade;
use common\services\StoreOrderService;
use yii\base\BaseObject;
use yii\base\UserException;
use yii\db\Exception;

/**
 * Class OrderForm
 *
 * @package common\modules\thingPrint\models
 */
class ThingiverseOrderPayForm extends BaseObject
{
    /** @var StoreOrder */
    public $order;
    public $thingId;
    public $thingiverseOrderId;

    /** @var StoreOrderService  */
    protected $storeOrderService;

    /** @var PaymentService */
    protected $paymentService;

    /**
     * @param StoreOrderService $storeOrderService
     * @param PaymentService $paymentService
     */
    public function injectDependencies(StoreOrderService $storeOrderService, PaymentService $paymentService)
    {
        $this->storeOrderService = $storeOrderService;
        $this->paymentService = $paymentService;
    }

    /**
     * @param $data
     * @param null $formName
     * @throws \yii\web\NotFoundHttpException
     */
    public function load($data, $formName = null)
    {
        $this->order = StoreOrder::tryFindByPk($data['orderId']);
        $this->thingId = (int)$data['thingId'];
        $this->thingiverseOrderId = (int)$data['thingiverseOrderId'];

    }

    /**
     * @return StoreOrder
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function payOrder()
    {
        $storeOrder = $this->order;
        $paymentInvoice = $storeOrder->getPrimaryInvoice();
        $paymentCheckout = new PaymentCheckout(PaymentTransaction::VENDOR_THINGIVERSE);
        $paymentDetailOperation = $paymentCheckout->pay($paymentInvoice, 0000);

        $this->paymentService->setPayedInvoice($paymentInvoice, $paymentDetailOperation);
        return $storeOrder;
    }

    public function createThingiverseOrder()
    {
        $thingiverseOrder = ThingiverseOrder::addRecord(
            [
                'thingiverse_order_id' => $this->thingiverseOrderId,
                'thing_id'             => $this->thingId,
                'order_id'             => $this->order->id,
                'created_at'           => DateHelper::now(),
                'updated_at'           => DateHelper::now(),
                'is_shipped'           => 0,
                'is_cancelled'         => 0,
                'refund_note'          => '',
                'note'                 => ''
            ]
        );
        return $thingiverseOrder;

    }

    /**
     *
     * @return ThingiverseOrder
     */
    public function bindThingiverseOrder()
    {
        $order = $this->order;
        $thingiverseOrder = ThingiverseOrder::findOne(['thingiverse_order_id' => $this->thingiverseOrderId]);
        if ($thingiverseOrder && empty($thingiverseOrder->order_id)) { // created by webhook
            $thingiverseOrder->order_id = $order->id;
            $thingiverseOrder->save(false);
            return $thingiverseOrder;
        }
        $thingiverseOrder = $this->createThingiverseOrder();
        return $thingiverseOrder;
    }

    public function updateAndCheckThingiverseOrderStatus()
    {
        $thingiverseOrder = ThingiverseFacade::refreshOrderByApi($this->order);
        if ($thingiverseOrder->status!==ThingiverseOrder::STATUS_PLACED) {
            // Order not payed.
            throw new Exception('Order not placed in thingiverse');
        };

    }
}