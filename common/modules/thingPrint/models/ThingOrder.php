<?php
/**
 * Date: 03.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\models;


class ThingOrder
{

    public $id;
    public $thing_id;
    public $amount;
    public $add_date;
    public $modified_date;
    public $charges;
    public $fees;
    public $validated_shipping_address;
    public $is_shipped;
    public $status; //  "status": "Placed", "Dispute" "status": "Completed",
    public $note;

    public $jsonConfig;

    /**
     * ThingFile constructor.
     *
     * @param $jsonConfig
     */
    public function __construct( $jsonConfig)
    {
        $this->jsonConfig = $jsonConfig;
        foreach($jsonConfig as $c=>$v){
            if(!property_exists($this, $c)){
                continue;
            }
            $this->$c = $v;
        }
    }

}