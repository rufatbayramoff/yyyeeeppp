<?php
/**
 * Date: 30.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\models;

use common\components\ArrayHelper;
use common\components\order\builder\OrderBuilder;
use common\models\factories\Model3dReplicaFactory;
use common\models\Model3dReplica;
use common\models\PsPrinter;
use common\models\repositories\Model3dReplicaRepository;
use common\models\repositories\Model3dRepository;
use common\models\StorePricer;
use common\models\ThingiverseUser;
use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\models\ThingiverseOrder;
use common\models\ThingiverseThing;
use common\models\User;
use common\modules\payment\fee\FeeHelper;
use common\services\Model3dService;
use common\services\PrinterMaterialService;
use frontend\components\UserSessionFacade;
use lib\delivery\parcel\ParcelFactory;
use lib\money\Money;
use lib\payment\PaymentCheckout;
use yii\base\UserException;

/**
 * Class OrderForm
 *
 * @package common\modules\thingPrint\models
 */
class ThingiverseOrderForm extends GetChargesForm
{
    /**
     * @var ThingiverseThing
     */
    public $order;
    public $thingiverseOrderId;
    public $thingUserId;
    public $email;
    public $comment;
    public $phone;

    public function load($data, $formName = null)
    {
        parent::load($data);
        $this->thingUserId = (int)$data['thingUserId'];
        $this->email = (string)$data['email'];
        $this->comment = (string)$data['comment'];
        $this->phone = (string)$data['phone'];
    }


    /**
     * @return StoreOrder
     * @throws \yii\web\GoneHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     * @throws \ErrorException
     * @throws \yii\base\UserException
     */
    public function makeOrder()
    {
        $printer = $this->printer;

        $thingiverseUser = ThingiverseUser::tryFind(['thingiverse_id' => $this->thingUserId]);
        if (!empty($this->email)) {
            $thingiverseUser->email = $this->email;
            $thingiverseUser->save(false);
        }
        if (!empty($this->phone)) {
            $this->userAddress->phone = $this->phone;
            $this->userAddress->save(false);
        } else {
            if ($this->userAddress->country->iso_code == 'CN') {
                throw new UserException('Please enter your telephone number.');
            }
        }

        $this->processModel3dReplica($printer);
        $this->initCart($this->model3d, $this->printer);

        $customer = $this->customer; // getting from GetChargesForm
        $printPrice = $this->getPrintPrice();
        $cart = $this->cart;
        $deliveryForm = $this->createDeliveryForm($cart, $printer, $customer);

        $shippingRate = $this->getShippingRate(
            $printer,
            $deliveryForm,
            Money::create($printPrice, UserSessionFacade::getCurrency()),
            ParcelFactory::createFromCart($cart)
        );

        $shippingRate->deliveryForm = $deliveryForm;
        $shippingRate->deliveryParams = $printer->companyService->asDeliveryParams();

        $order = OrderBuilder::create()
            ->customer($customer, $deliveryForm->email)
            ->source(StoreOrder::SOURCE_THINGIVERSE)
            ->canChangeSupplier($cart->canChangePs())
            ->cart($cart)
            ->delivery($shippingRate)
            ->comment(ArrayHelper::firstNotEmpty($this->comment, $deliveryForm->comment))
            ->buildByItems();

        return $order;
    }

    /**
     * we should remove empty items from cart
     *
     * @param PsPrinter $psPrinter
     * @throws \Exception
     */
    private function processModel3dReplica(PsPrinter $psPrinter)
    {
        $this->model3d = Model3dReplicaFactory::createModel3dReplica($this->model3d); // Dublicate model3d for new order
        if (PrinterMaterialService::isNotSettedPrinterMaterial($this->model3d)) {
            Model3dService::setCheapestMaterialInPrinter($this->model3d, $psPrinter);
        }
        $this->model3d->cleanEmptyQty();
        $model3dRepository = \Yii::createObject(Model3dRepository::class);
        $model3dRepository->save($this->model3d);
    }
}