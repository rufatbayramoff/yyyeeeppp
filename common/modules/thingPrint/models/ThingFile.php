<?php
namespace common\modules\thingPrint\models;


/**
 * Date: 17.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class ThingFile
{
    const PUBLIC_EXTENSIONS = ['jpg', 'jpeg', 'gif', 'png'];
    public $id;
    public $size;
    public $name;
    public $url;
    public $downloadUrl;
    public $thumbnail;
    public $date;

    public $tempFilePath;

    /**
     * ThingFile constructor.
     *
     * @param $jsonConfig
     */
    public function __construct( $jsonConfig)
    {
        foreach($jsonConfig as $c=>$v){
            if(!property_exists($this, $c)){
                continue;
            }
            $this->$c = $v;
        }
        if(!empty($jsonConfig->default_image)){
            $this->downloadUrl = $jsonConfig->default_image->url;
            if ($this->downloadUrl === 'https://cdn.thingiverse.com/') {
                $this->downloadUrl = $jsonConfig->public_url;
            }
        }else{
            $this->downloadUrl = $jsonConfig->public_url;
        }
    }

    /**
     * @return mixed
     */
    public function getRender()
    {
        return str_replace('_thumb_medium', '_display_large', $this->thumbnail);
    }

    /**
     * @return string
     */
    public function getBasename()
    {
        $basename  = $this->getFilename() . '.' . $this->getExtension();
        if (in_array(strtolower($this->getExtension()), self::PUBLIC_EXTENSIONS)) {
            $basename = strtolower($basename);
        }
        return $basename;
    }

    /**
     * get file extension
     *
     * @return mixed
     */
    public function getExtension()
    {
        $inf = pathinfo($this->name);
        if(empty($inf['extension'])){
            return '';
        }
        return $inf['extension'];
    }

    /**
     * get name without extension
     * always lower case for images
     *
     * @return mixed
     */
    public function getFilename()
    {
        $inf = pathinfo($this->name);
        return $inf['filename'];
    }
}
