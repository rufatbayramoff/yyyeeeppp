<?php
/**
 * Date: 02.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\models;

/**
 * Class ThingUserAddress
 * {"name":"Test","line1":"339 Main Rd,Montville","line2":"","city":"NJ","state":"NY","postcode":"07045","country":"US","id":8}
 *
 * new:
 * {"name":"De Facto","organisation":null,
 *   "street_address":"2221 River Bend Rd  ",
 *   "dependent_locality":null,
 *    "city":"Plover",
 *   "admin_area":"WI",
 *    "postal_code":"54467",
 *   "sorting_code":null,
 *   "country":"US"}}
 * @package common\modules\thingPrint\models
 */
class ThingUserAddress
{
    public $name;
    public $organisation;
    public $street_address;
    public $city;
    public $admin_area; // state
    public $postal_code;
    public $country;
    public $id; // id from thingiverse
    private $jsonConfig;

    public function __construct( $jsonConfig)
    {
        $this->jsonConfig = $jsonConfig;
        if (is_array($jsonConfig)) {
            foreach ($jsonConfig as $c => $v) {
                if (!property_exists($this, $c)) {
                    continue;
                }
                $this->$c = $v;
            }
        }
    }
}