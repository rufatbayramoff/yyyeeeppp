<?php
/**
 * Date: 31.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\models;

use common\models\File;


/**
 * Class ThingUser
 *
 * @package common\modules\thingPrint\models
 */
class ThingUser
{
    public $id;
    public $name;
    public $full_name;
    public $url;
    /**
     * avatar url
     * @var
     */
    public $thumbnail;
    public $country;
    public $email;
    public $industry;
    public $jsonConfig;
    public function __construct( $jsonConfig)
    {
        $this->jsonConfig = $jsonConfig;
        foreach ($jsonConfig as $c => $v) {
            if (!property_exists($this, $c)) {
                continue;
            }
            $this->$c = $v;
        }
    }

    public function getApiJson()
    {
        return json_encode($this->jsonConfig);
    }

    /**
     * @TODO
     * for now direct link
     * @return File
     */
    public function getAvatarFile()
    {
        return $this->thumbnail;
    }
}