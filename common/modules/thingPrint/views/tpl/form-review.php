<div class="tab-pane">
    <div ng-show="!testIsEmptyModel()">
        <p class="tagline">
            <?= _t('site.printModel3d', 'Set the required quantities for each file and if their sizes appear too small, change the unit of measurement to inches. Click on 3D Viewer for a 360° preview of your files.') ?>
        </p>
        <div class="units">
            <?= _t('site.printModel3d', 'Unit of measurement') . ':' ?>
            <div class="btn-group btn-switcher" data-toggle="buttons">
                <label class="btn btn-default btn-xs" ng-class="{'active':model3d.modelUnits=='mm'}" ng-click="changeScale('mm')">
                    <input type="radio" name="optionsPrice" id="optionsPrice1" autocomplete="off">
                    MM
                </label>
                <label class="btn btn-default btn-xs" ng-class="{'active':model3d.modelUnits=='in'}" ng-click="changeScale('in')">
                    <input type="radio" name="optionsPrice" id="optionsPrice2" autocomplete="off">
                    IN
                </label>
            </div>
        </div>
        <div class="models">
            <div ng-repeat="model3dPart in model3d.parts">
                <?= $this->render('@frontend/views/my/print-model3d/templates/uploadModel3dPart.php') ?>
            </div>

            <p class="thing-print-tagline" ng-show="filesError">
                {{filesError}}
            </p>

            <div class="item-rendering" ng-show="uploadStep.updateModelInProgress">
                <div class="item-rendering__message wave-anim">
                    <?php echo _t('site.model3d', 'Loading...'); ?>
                    <div class="item-rendering__line line-anim "></div>
                </div>
            </div>

            <div class="models__row">
                <button class="btn btn-default btn-sm models__add-btn" ng-click="preview3d()" ng-disabled="!model3d.hasPreview3d()">
                    <span class="tsi tsi-cube m-r10"></span>
                    <?= _t('site.printModel3d', '3D Viewer'); ?>
                </button>
            </div>
        </div>
    </div>
    <div class="aff-widget-bottom" ng-show="1">
        <button ng-click="nextStep()" class="btn btn-primary">
            <?= _t('site.common', 'Next'); ?>
            <span class="tsi tsi-right"></span>
        </button>
    </div>
</div>
