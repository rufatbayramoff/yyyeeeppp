<div class="tab-pane">
    <div ng-if="!canPay()" class="tab-pane">
        <div class="thing-print-pay">
            <div class="row">
                <div class="col-sm-6">
                    <div class="alert alert-danger text-center" style="margin: 20px 0;" role="alert">{{cantPayErrorMessage}}</div>
                </div>
            </div>
        </div>
    </div>
    <div ng-if="canPay()" class="tab-pane">
        <div class="thing-print-pay">
            <div class="row">
                <div class="col-sm-5">
                    <br/>
                    <div class="preview">
                        <div class="preview__hint">
                            <?= _t('site.printModel3d', 'Preview'); ?>
                        </div>
                        <!-- Slider main container -->
                        <div class="preview__slider">
                            <span ng-repeat="model3dPart in model3d.parts">
                                <div class="preview__slide" ng-if="!model3dPart.isCanceled && model3dPart.qty">
                                    <div class="preview__material"
                                         title="{{model3dTextureState.getTexture(model3dPart).printerColor.title}} {{model3dTextureState.getTexture(model3dPart).printerMaterialGroup.title}}">
                                        {{model3dTextureState.getTexture(model3dPart).printerMaterialGroup.title}}
                                        <div class="preview__material-color"
                                             style="background-color: #{{model3dTextureState.getTexture(model3dPart).printerColor.getRgbHex()}}"></div>
                                    </div>
                                    <img class="preview__pic" title="{{model3dpart.title}}" ng-src="{{getModel3dPartPreviewUrl(model3dPart)}}"
                                         main-src="{{getModel3dPartPreviewUrl(model3dPart)}}"
                                         alt-src="/static/images/preloader.gif" onerror="commonJs.checkAltSrcImage(this);">
                                    <div class="preview__qty" title="<?= _t('site.printModel3d', 'Quantity') ?>: {{1*model3dPart.qty}}">&times;{{1*model3dPart.qty}}</div>
                                    <div>
                                        {{1*model3dPart.qty}}
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>

                    <div class="thing-print-pay__address">
                        <h3>Shipping Address</h3>
                        <b>{{deliveryForm.address.name}}</b> <br>
                        {{deliveryForm.address.street_address}}<br>
                        {{deliveryForm.address.city}} {{deliveryForm.address.admin_area}} {{deliveryForm.address.postal_code}}, {{deliveryForm.address.country}} <br/>
                        <hr/>

                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail" class="col-md-3 control-label p-r0">Email <span style="color:#e00f57">*</span></label>
                                <div class="col-md-9">
                                    <input type="email" class="form-control" ng-model="formData.email" id="inputEmail" placeholder="Email">
                                </div>
                                <div ng-show="deliveryForm.address.country=='CN'">
                                    <label for="inputPhone" class="col-md-3 control-label p-r0">Phone</label>
                                    <div class="col-md-9">
                                        <input type="phone" class="form-control" ng-model="formData.phone" id="inputPhone" placeholder="phone">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="thing-print-pay__address-help">
                            Your email address will not be shared with a third party,
                            nor will it be sold or used for purposes other than those relating to your order made through the application.
                        </p>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-2">
                    <div class="thing-print-pay__order">
                        <h3>Order Details</h3>

                        <div class="delivery-order">
                            <div class="delivery-order__print-price" ng-repeat="priceRow in checkoutStep.pricer.getPricesWithTitle()">
                                <div class="delivery-order__print-price__price print_price price__badge">
                                    {{priceRow.price}}
                                </div>
                                <h5 class="delivery-order__print-price__title">
                                    {{priceRow.title}}
                                    <span ng-if="priceRow.code=='ps_print'">
                                        <?= \frontend\widgets\SiteHelpWidget::widget([
                                            'title' => _t('site.ps', 'Printing fee'),
                                            'alias' => 'ps.customer-checkout.printing-fee'
                                        ]); ?>
                                    </span>
                                </h5>
                            </div>
                        </div>
                        <hr/>
                        <div class="delivery-order">
                            <div class="delivery-order__print-price">
                                <div class="delivery-order__print-price__price print_price price__badge">
                                    ${{checkoutStep.pricer.getTotal()}}
                                </div>
                                <h5 class="delivery-order__print-price__title"><strong>Total</strong></h5>
                            </div>
                        </div>
                    </div>
                    <p style="margin-top: 20px;text-align:justify;" class="text-muted small">
                        I, as a customer, acknowledge that Treatstock, nor the manufacturer, are responsible for the size, flaws of the design itself,
                        the applicability of the product for any purpose, or the use of goods ordered.
                        If you are unsure about any of the previously stated conditions, please contact the designer before placing your order.</p>
                    <p>
                        Comment:
                        <textarea ng-model="formData.comment"
                                  style="width:100%;"
                                  placeholder="Leave a comment if you have any requests, instructions, or additional information for the manufacturer"
                                  rows="3"
                                  maxlength="600"></textarea>
                    </p>
                </div>
            </div>
        </div>

        <div class="aff-widget-bottom">
            <a href="javascript:;" ng-click="makeOrder()" class="btn btn-danger thing-print-pay__pay-btn">Proceed to Checkout</a>
        </div>
    </div>
</div>