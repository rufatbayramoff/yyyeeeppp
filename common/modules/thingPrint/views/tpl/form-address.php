<div class="tab-pane">
    <p class="thing-print-tagline">
        Enter the shipping address where you want the manufacturer to send your order.
    </p>
    <div class="thing-print-address">
        <button type="button" class="btn btn-primary" ng-show="!deliveryForm.shippingAddress" ng-click="chooseAddress()" title="Select address">
            <span ng-if="loadingAddress">
                 <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Loading address...'); ?>
            </span>
            <span ng-if="!loadingAddress">
                Add Shipping Address
            </span>
        </button>

        <div class="thing-print-address__btns" ng-show="deliveryForm.shippingAddress">
            <div class="text-left">
                <b>{{deliveryForm.shippingAddress.name}}</b> <br>
                {{deliveryForm.shippingAddress.organisation}}<br>
                {{deliveryForm.shippingAddress.street_address}}<br>
                {{deliveryForm.shippingAddress.city}} {{deliveryForm.shippingAddress.postal_code}}, {{deliveryForm.shippingAddress.country}} <br/>
            </div>
            <br/>
            <button type="button" class="btn btn-primary btn-ghost thing-print-address__change-btn" ng-click="chooseAddress()" title="Select address">
                <span ng-if="loadingAddress">
                 <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Loading address...'); ?>
                </span>
                <span ng-if="!loadingAddress">
                Change address
                </span>
            </button>
        </div>
    </div>
    <p class="thing-print-tagline">
        <span style="color:red">*</span> Refresh webpage if clicking on the Add Shipping Address tab doesn't open a pop-up window.
    </p>
    <div class="aff-widget-bottom" ng-show="1">
        <button ng-click="nextStep()" class="btn btn-primary">
            <?= _t('site.common', 'Next'); ?>
            <span class="tsi tsi-right"></span>
        </button>
    </div>
</div>