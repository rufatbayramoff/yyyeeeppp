<div class="col-md-12">
    <h1 class="text-center m-t30">Checkout Complete</h1>
    <div class="text-center">
        <p><span style="font-size: 96px; color:#82c015;" class="tsi tsi-checkmark-c"></span></p>

        <p>Your order has been successfully submitted to the 3D print service.</p>
        <p>

                Total Order Amount For Printing:  <strong>
                ${{checkoutStep.pricer.getTotal()}}
            </strong>
        </p>
        <p> <b>Please check your email address for a confirmation email with a link to your order.</b> </p>
        <p>
            Your Order ID on Treatstock: #{{formData.orderId}}
        </p>
        <p>
            Your Order ID on Thingiverse: #{{formData.thingiverseOrderId}}
        </p>
        <br/>

    </div>
</div>