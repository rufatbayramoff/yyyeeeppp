<?php

use common\modules\thingPrint\ThingPrintModule;

Yii::$app->angular
    ->controllerParams(
        [
            'formData' => [
                'code'        => $code,
                'thingId'     => $thingId,
                'thingUserId' => $thingUserId
            ]
        ]
    )
    ->controller(
        [
            'print-model3d/model3d-preview'
        ]
    )
    ->controller(
        [
            'OrderPricer',
            'formController',
            'uploadStepController',
            'printersStepController',
            'deliveryStepController',
            'checkoutStepController',
        ],
        ThingPrintModule::ANGULAR_APP_ASSETS
    );
?>

<div class="aff-widget" ng-controller="formController">
    <!-- Nav tabs -->
    <ul class="aff-widget-tabs clearfix">
        <li class="{{steps['upload'].status}}" ui-sref="upload" ui-sref-active="active">
            <a href="#upload" aria-controls="upload">
                <span class="aff-widget-tabs__step">1</span>
                <?= _t('site.printModel3d', 'Review Files') ?>
            </a>
        </li>
        <li class="{{steps['delivery'].status}}" ui-sref="delivery" ui-sref-active="active">
            <a href="#delivery" aria-controls="delivery">
                <span class="aff-widget-tabs__step">2</span>
                <?= _t('site.printModel3d', 'Shipping Address') ?>
            </a>
        </li>
        <li class="{{steps['printers'].status}}" ui-sref="printers" ui-sref-active="active">
            <a href="#print" aria-controls="print">
                <span class="aff-widget-tabs__step">3</span>
                <?= _t('site.printModel3d', 'Print Options') ?>
            </a>
        </li>
        <li class="{{steps['checkout'].status}}" ui-sref="checkout" ui-sref-active="active">
            <a href="#payment" aria-controls="payment">
                <span class="aff-widget-tabs__step">4</span>
                <?= _t('site.printModel3d', 'Order Summary') ?>
            </a>
        </li>
    </ul>
    <div class="aff-widget-content clearfix">
        <ui-view></ui-view>
    </div>
</div>
