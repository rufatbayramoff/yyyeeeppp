<?php

use common\components\JsObjectFactory;
use common\modules\thingPrint\assets\PlacePageAsset;

PlacePageAsset::register($this);

JsObjectFactory::createJsObject(
    'confirmDownloadModelClass',
    'confirmDownloadModelObj',
    ['thingModelId' => $thingModelId]
    ,
    $this
);
?>
<div class="preloader hidden" id="loading-model-progress">
    <h3>Downloading 3d model #<b><?=$thingModelId?></b> from thingiverse ...</h3>
</div>