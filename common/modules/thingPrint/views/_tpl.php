

<div class="thing-print-master">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs thing-print-tab" role="tablist">
        <li role="presentation" class="active">
            <a href="#files" aria-controls="files" role="tab" data-toggle="tab">
                <span class="thing-print-tab__step">1</span>
                Review Files
            </a>
        </li>
        <li role="presentation" class="checked">
            <a href="#address" aria-controls="address" role="tab" data-toggle="tab">
                <span class="thing-print-tab__step">2</span>
                Shipping Address
            </a>
        </li>
        <li role="presentation">
            <a href="#print" aria-controls="print" role="tab" data-toggle="tab">
                <span class="thing-print-tab__step">3</span>
                Print Options
            </a>
        </li>
        <li role="presentation">
            <a href="#payment" aria-controls="payment" role="tab" data-toggle="tab">
                <span class="thing-print-tab__step">4</span>
                Order Summary
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content clearfix">
        <div role="tabpanel" class="tab-pane active" id="files">

            <div class="row">
                <div class="col-sm-9">
                    <p class="thing-print-tagline">Set the required quantities for each file and if their sizes appear too small, change the unit of measurement to inches. Click on 3D Viewer for a 360° preview of your files.</p>
                </div>
                <div class="col-sm-3">
                    <div class="thing-print-units">
                        Scale to:
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-info btn-xs active">
                                <input type="radio" name="optionsPrice" id="optionsPrice" autocomplete="off" checked="checked">
                                MM
                            </label>
                            <label class="btn btn-info btn-xs">
                                <input type="radio" name="optionsPrice" id="optionsPrice" autocomplete="off">
                                IN
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="thing-print-models">
                <div class="thing-print-models__row">
                    <div class="thing-print-models__pic">
                        <img src="https://www.treatstock.com/static/render/5f7beb79e3b4ead0d355eaaa46468d3c_ambient_40_color_ffffff.png">
                        <div class="thing-print-models__pic-qty" title="Quantity: 1">×1</div>
                    </div>
                    <div class="thing-print-models__data">
                        <div class="thing-print-models__info">
                            <h4 class="thing-print-models__title">katanamaskblend2.stl</h4>
                            <div class="thing-print-models__size">
                                Size : 150 x 82.67 x 112.74 mm
                                <br>
                                Weight : 42.67 gr
                            </div>
                        </div>
                        <div class="thing-print-models__qty">
                            <label class="control-label" for="getprintedform-qty">Quantity</label>
                            <input type="number" id="getprintedform-qty" class="form-control input-sm" value="1">
                        </div>
                        <a href="#delete" class="thing-print-models__del" title="Delete">&times;</a>
                    </div>
                </div>


                <div class="thing-print-models__row">
                    <div class="thing-print-models__pic">
                        <img src="https://www.treatstock.com/static/user/fba9d88164f3e2d9109ee770223212a0/model/6395_720x540.png">
                        <div class="thing-print-models__pic-qty" title="Quantity: 1">×1</div>
                    </div>
                    <div class="thing-print-models__data">
                        <div class="thing-print-models__info">
                            <h4 class="thing-print-models__title">SomeLongNameModelForText.stl</h4>
                            <div class="thing-print-models__size">
                                Size : 23.67 x 9.06 x 5.78 mm
                                <br>
                                Weight : 0.71 gr
                            </div>
                        </div>
                        <div class="thing-print-models__qty">
                            <label class="control-label" for="getprintedform-qty">Quantity</label>
                            <input type="number" id="getprintedform-qty" class="form-control input-sm" value="1">
                        </div>
                        <a href="#delete" class="thing-print-models__del" title="Delete">&times;</a>
                    </div>
                </div>
            </div>

            <div class="thing-print-bottom">

                <a href="#address" aria-controls="address" role="tab" data-toggle="tab" class="btn btn-danger">
                    Next
                    <span class="tsi tsi-right"></span>
                </a>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="address">

            <p class="thing-print-tagline">Enter the shipping address where you want the manufacturer to send your order.</p>
            <div class="thing-print-address"  >
                <div class="thing-print-address__btns"  >    <div class="text-left">
                        Some Username <br>
                        11 W 53rd St <br>
                        New York NY 10019, US
                    </div>
                </div>
            </div>

            <div class="thing-print-bottom">
                <button type="button" class="btn btn-primary btn-ghost thing-print-address__change-btn"
                        ng-click="chooseAddress()"  title="Select address">Change address</button>

                <a href="#print" aria-controls="print" role="tab" data-toggle="tab" class="btn btn-danger">
                    Next
                    <span class="tsi tsi-right"></span>
                </a>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="print">

            <p class="thing-print-tagline">Choose the color of the plastic that you want. Then, select a print service near you to 3D print the files in that color.
                <br />
                For a greater choice of materials and colors, visit <a href="https://www.treatstock.com/" target="_blank">Treatstock.com</a>
            </p>

            <div class="clearfix">

                <div class="col-sm-4" ng-controller="ColorsListController">
                    <div class="thing-print-preview">
                        <h4 class="thing-print-preview__title">Preview</h4>

                        <!-- Slider main container -->
                        <div class="thing-print-preview__slider">

                            <div class="thing-print-preview__slide">
                                <img class="thing-print-preview__pic" title="Model name" src="https://www.treatstock.com/static/render/5f7beb79e3b4ead0d355eaaa46468d3c_ambient_40_color_ffffff.png" alt="">
                                <div class="thing-print-preview__qty" title="Quantity: 5">&times;5</div>
                            </div>
                            <div class="thing-print-preview__slide">
                                <img class="thing-print-preview__pic" title="Model name" src="https://www.treatstock.com/static/render/108fe6979f97b0e1f06a4763a45ccace_ambient_40_color_ffffff.png" alt="">
                                <div class="thing-print-preview__qty" title="Quantity: 1">&times;1</div>
                            </div>
                            <div class="thing-print-preview__slide">
                                <img class="thing-print-preview__pic" title="Model name" src="https://www.treatstock.com/static/render/68d79d9a4eac0df096eaac5910e8baff_ambient_40_color_ffffff.png" alt="">
                                <div class="thing-print-preview__qty" title="Quantity: 2">&times;2</div>
                            </div>
                            <div class="thing-print-preview__slide">
                                <img class="thing-print-preview__pic" title="Model name" src="https://www.treatstock.com/static/render/9fcdc74adf698fd9b21ad57f76192a7f_ambient_40_color_ffffff.png" alt="">
                                <div class="thing-print-preview__qty" title="Quantity: 28">&times;28</div>
                            </div>

                        </div>

                    </div>

                    <div class="thing-print-color">
                        <div class="thing-print-color__material">
                            <strong>Material:</strong> PLA (biodegradable plastic)
                        </div>
                        <div class="color-switcher__material-list">
                            <input type="checkbox" class="showhideColorSwitcher" id="showhideColorSwitcher">
                            <label class="color-switcher__mobile" for="showhideColorSwitcher">
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(0,204,51)"></div>
                                    <div class="material-item__label">Green</div>
                                </div>
                                <span class="tsi tsi-down"></span>
                            </label>

                            <div class="color-switcher__color-list">
                                <div class="color-selector-printer-color-block-element material-item " data-material-group-id="16" data-color-id="1">
                                    <div class="color-selector-printer-color-view material-item__color" style="background-color: #00cc33"></div>
                                    <div class="color-selector-printer-color-title material-item__label">Green</div>
                                </div>
                                <div class="color-selector-printer-color-block-element material-item " data-material-group-id="16" data-color-id="2">
                                    <div class="color-selector-printer-color-view material-item__color" style="background-color: #0066cc"></div>
                                    <div class="color-selector-printer-color-title material-item__label">Blue</div>
                                </div>
                                <div class="color-selector-printer-color-block-element material-item " data-material-group-id="16" data-color-id="3">
                                    <div class="color-selector-printer-color-view material-item__color" style="background-color: #ff465b"></div>
                                    <div class="color-selector-printer-color-title material-item__label">Red</div>
                                </div>
                                <div class="color-selector-printer-color-block-element material-item " data-material-group-id="16" data-color-id="4">
                                    <div class="color-selector-printer-color-view material-item__color" style="background-color: #f2d70e"></div>
                                    <div class="color-selector-printer-color-title material-item__label">Yellow</div>
                                </div>
                                <div class="color-selector-printer-color-block-element material-item " data-material-group-id="16" data-color-id="5">
                                    <div class="color-selector-printer-color-view material-item__color" style="background-color: #e6e8e6"></div>
                                    <div class="color-selector-printer-color-title material-item__label">Silver</div>
                                </div>
                                <div class="color-selector-printer-color-block-element material-item material-item--active">
                                    <div class="color-selector-printer-color-view material-item__color" style="background-color: rgb(239, 235, 214 )"></div>
                                    <div class="color-selector-printer-color-title material-item__label ng-binding">Transparent</div>
                                </div>
                                <div class="color-selector-printer-color-block-element material-item " data-material-group-id="16" data-color-id="6">
                                    <div class="color-selector-printer-color-view material-item__color" style="background-color: #8c781a"></div>
                                    <div class="color-selector-printer-color-title material-item__label">Bronze</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-8">

                    <div class="thing-print-printers">
                        <div class="model-printers-available model-printers-available--choosen" style="display: block;">
                            <div class="model-printers-available__wrap">
                                <div class="model-printers-available__info">
                                    <div class="model-printers-available__info__img"><a
                                            href="/c/eds-printing" target="_blank" data-pjax="0"><img
                                                src="https://www.treatstock.com/static/user/fcdf25d6e191893e705819b177cddea0/ps_logo_circle_1478545543_64x64.png"
                                                width="30" height="30" alt="" align="left"></a></div>
                                    <div class="model-printers-available__info__ps">
                                        <div class="model-printers-available__info__ps-title"><a
                                                href="/c/eds-printing" target="_blank" data-pjax="0"><span>Ed's Printing</span></a>
                                        </div>
                                        <div class="model-printers-available__info__ps-rating">
                                            <div id="star-rating-block-68218" class="star-rating-block" itemprop="aggregateRating"
                                                 itemscope="" itemtype="http://schema.org/AggregateRating">
                                                <meta itemprop="ratingValue" content="4.9">
                                                <meta itemprop="worstRating" content="1">
                                                <meta itemprop="bestRating" content="5">
                                                <div class="star-rating rating-xs rating-disabled">
                                                    <div class="rating-container tsi" data-content="">
                                                        <div class="rating-stars" data-content="" style="width: 98%;"></div>
                                                        <input value="4.9" type="number" class="star-rating form-control hide"
                                                               min="0" max="5" step="0.1" data-size="xs" data-symbol=""
                                                               data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="star-rating-count">(<span itemprop="reviewCount">23</span> reviews)</span>
                                        </div>
                                        <div class="model-printers-available__info__ps-printer">

                                            3D Printer:

                                            Prusa i3
                                            <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom"
                                                 title=""
                                                 data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                Pro
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="model-printers-available__delivery">
                                    <div class="model-printers-available__delivery-title">
                                        Canton, Ohio
                                    </div>
                                </div>
                            </div>
                            <div class="model-printers-available__price-btn">
                                <div class="model-printers-available__price"><span class="ts-print-price">
                    $5.77                </span></div>
                                <div class="model-printers-available__print-btn"><a
                                        class="btn btn-danger btn-block ts-ga t-model-printers__print-btn print-by-printer-button"
                                        href="/store/cart/add" title="Get printed" data-target="#modal-getPrinted"
                                        data-method="post" data-categoryga="Filepage" data-actionga="Print" data-labelga=""
                                        data-printer-id="159" data-store-unit-id="948" oncontextmenu="return false;">Print Here</a>
                                </div>
                            </div>
                        </div>

                        <div class="model-printers-available" style="display: block;">
                            <div class="model-printers-available__wrap">
                                <div class="model-printers-available__info">
                                    <div class="model-printers-available__info__img"><a href="/c/nocturne-3d"
                                                                                        target="_blank" data-pjax="0"><img
                                                src="https://www.treatstock.com/static/user/4b29fa4efe4fb7bc667c7b301b74d52d/ps_logo_circle_1482343879_64x64.jpg"
                                                width="30" height="30" alt="" align="left"></a></div>
                                    <div class="model-printers-available__info__ps">
                                        <div class="model-printers-available__info__ps-title"><a
                                                href="/c/nocturne-3d" target="_blank" data-pjax="0"><span>Nocturne 3d</span></a>
                                        </div>
                                        <div class="model-printers-available__info__ps-rating"><span
                                                class="star-rating-count">(<span itemprop="reviewCount">1</span> review)</span>
                                        </div>
                                        <div class="model-printers-available__info__ps-printer">

                                            3D Printer:

                                            LulzBot Mini
                                            <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom"
                                                 title=""
                                                 data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                Pro
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="model-printers-available__delivery">
                                    <div class="model-printers-available__delivery-title">
                                        Carrollton, Texas
                                    </div>
                                </div>
                            </div>
                            <div class="model-printers-available__price-btn">
                                <div class="model-printers-available__price"><span class="ts-print-price">
                    $7.51                </span></div>
                                <div class="model-printers-available__print-btn"><a
                                        class="btn btn-danger btn-block ts-ga t-model-printers__print-btn print-by-printer-button"
                                        href="/store/cart/add" title="Get printed" data-target="#modal-getPrinted"
                                        data-method="post" data-categoryga="Filepage" data-actionga="Print" data-labelga=""
                                        data-printer-id="623" data-store-unit-id="948" oncontextmenu="return false;">Print Here</a>
                                </div>
                            </div>
                        </div>

                        <div class="model-printers-available" style="display: block;">
                            <div class="model-printers-available__wrap">
                                <div class="model-printers-available__info">
                                    <div class="model-printers-available__info__img"><a
                                            href="/c/jwcproductions" target="_blank" data-pjax="0"><img
                                                src="https://www.treatstock.com/static/user/c3614206a443012045cfd75d2600af2d/ps_logo_circle_1476310929_64x64.jpg"
                                                width="30" height="30" alt="" align="left"></a></div>
                                    <div class="model-printers-available__info__ps">
                                        <div class="model-printers-available__info__ps-title"><a
                                                href="/c/jwcproductions" target="_blank" data-pjax="0"><span>Jwcproductions</span></a>
                                        </div>
                                        <div class="model-printers-available__info__ps-rating"><span
                                                class="star-rating-count">(<span itemprop="reviewCount">2</span> reviews)</span>
                                        </div>
                                        <div class="model-printers-available__info__ps-printer">

                                            3D Printer:

                                            FlashForge Creator
                                            <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom"
                                                 title=""
                                                 data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                Pro
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="model-printers-available__delivery">
                                    <div class="model-printers-available__delivery-title">
                                        Harrison Charter Township, Michigan
                                    </div>
                                </div>
                            </div>
                            <div class="model-printers-available__price-btn">
                                <div class="model-printers-available__price"><span class="ts-print-price">
                    $7.51                </span></div>
                                <div class="model-printers-available__print-btn"><a
                                        class="btn btn-danger btn-block ts-ga t-model-printers__print-btn print-by-printer-button"
                                        href="/store/cart/add" title="Get printed" data-target="#modal-getPrinted"
                                        data-method="post" data-categoryga="Filepage" data-actionga="Print" data-labelga=""
                                        data-printer-id="431" data-store-unit-id="948" oncontextmenu="return false;">Print Here</a>
                                </div>
                            </div>
                        </div>

                        <div class="model-printers-available" style="display: none;">
                            <div class="model-printers-available__wrap">
                                <div class="model-printers-available__info">
                                    <div class="model-printers-available__info__img"><a
                                            href="/c/pck-machines-corporation" target="_blank" data-pjax="0"><img
                                                src="https://www.treatstock.com/static/user/c02f9de3c2f3040751818aacc7f60b74/ps_logo_circle_1483987783_64x64.jpg"
                                                width="30" height="30" alt="" align="left"></a></div>
                                    <div class="model-printers-available__info__ps">
                                        <div class="model-printers-available__info__ps-title"><a
                                                href="/c/pck-machines-corporation" target="_blank"
                                                data-pjax="0"><span>PCK Machines Corporation</span></a></div>
                                        <div class="model-printers-available__info__ps-rating"></div>
                                        <div class="model-printers-available__info__ps-printer">

                                            3D Printer:

                                            LulzBot TAZ 6
                                            <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom"
                                                 title=""
                                                 data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                                Pro
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="model-printers-available__delivery">
                                    <div class="model-printers-available__delivery-title">
                                        Parker, Colorado
                                    </div>
                                </div>
                            </div>
                            <div class="model-printers-available__price-btn">
                                <div class="model-printers-available__price"><span class="ts-print-price">
                    $9.00                </span></div>
                                <div class="model-printers-available__print-btn"><a
                                        class="btn btn-danger btn-block ts-ga t-model-printers__print-btn print-by-printer-button"
                                        href="/store/cart/add" title="Get printed" data-target="#modal-getPrinted"
                                        data-method="post" data-categoryga="Filepage" data-actionga="Print" data-labelga=""
                                        data-printer-id="680" data-store-unit-id="948" oncontextmenu="return false;">Print Here</a>
                                </div>
                            </div>
                        </div>

                        <a href="#" class="model-printers-available__show-more js-printers-show-more">Show more print services</a>

                    </div>
                </div>

            </div>


            <div class="thing-print-bottom">
                <a href="#payment" aria-controls="payment" role="tab" data-toggle="tab" class="btn btn-danger">
                    Next
                    <span class="tsi tsi-right"></span>
                </a>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="payment">

            <div class="thing-print-pay">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="thing-print-preview thing-print-preview--4step">
                            <h4 class="thing-print-preview__title">Preview</h4>

                            <!-- Slider main container -->
                            <div class="thing-print-preview__slider">

                                <div class="thing-print-preview__slide">
                                    <img class="thing-print-preview__pic" title="Model name" src="https://www.treatstock.com/static/render/5f7beb79e3b4ead0d355eaaa46468d3c_ambient_40_color_ffffff.png" alt="">
                                    <div class="thing-print-preview__qty" title="Quantity: 5">&times;5</div>
                                </div>
                                <div class="thing-print-preview__slide">
                                    <img class="thing-print-preview__pic" title="Model name" src="https://www.treatstock.com/static/render/108fe6979f97b0e1f06a4763a45ccace_ambient_40_color_ffffff.png" alt="">
                                    <div class="thing-print-preview__qty" title="Quantity: 1">&times;1</div>
                                </div>
                                <div class="thing-print-preview__slide">
                                    <img class="thing-print-preview__pic" title="Model name" src="https://www.treatstock.com/static/render/68d79d9a4eac0df096eaac5910e8baff_ambient_40_color_ffffff.png" alt="">
                                    <div class="thing-print-preview__qty" title="Quantity: 2">&times;2</div>
                                </div>
                                <div class="thing-print-preview__slide">
                                    <img class="thing-print-preview__pic" title="Model name" src="https://www.treatstock.com/static/render/9fcdc74adf698fd9b21ad57f76192a7f_ambient_40_color_ffffff.png" alt="">
                                    <div class="thing-print-preview__qty" title="Quantity: 28">&times;28</div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-5">

                        <div class="thing-print-pay__address">
                            <h3>Ship Here</h3>
                            Some Username <br>
                            11 W 53rd St <br>
                            New York NY 10019, US<br><br>

                            <p>Please provide your email address if you would like to receive notifications about your order status.
                            </p>
                            <form class="form-horizontal">

                                <div class="form-group">
                                    <label for="inputEmail" class="col-md-3 control-label p-r0">Email</label>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                    </div>
                                </div>
                            </form>
                            <p class="thing-print-pay__address-help">
                                Your email address will not be shared with a third party,
                                nor will it be sold or used for purposes other than to notify you about your order status.
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-5 col-sm-offset-2">
                        <div class="thing-print-pay__order">
                            <h3>Order Details</h3>

                            <div class="delivery-order">
                                <div class="delivery-order__print-price">
                                    <div class="delivery-order__print-price__price print_price price__badge">
                                        $2.00
                                    </div>
                                    <h5 class="delivery-order__print-price__title">katanamaskblend2.stl</h5>
                                </div>
                                <div class="delivery-order__print-price">
                                    <div class="delivery-order__print-price__price print_price price__badge">
                                        $1.00
                                    </div>
                                    <h5 class="delivery-order__print-price__title">SomeLongNameModelForText.stl</h5>
                                </div>
                                <div class="delivery-order__print-price">
                                    <div class="delivery-order__print-price__price print_price price__badge">
                                        $2.00
                                    </div>
                                    <h5 class="delivery-order__print-price__title">Print Fee</h5>
                                </div>
                                <div class="delivery-order__delivery-price">
                                    <div class="delivery-order__delivery-price__price price__badge">
                                        $1.00
                                    </div>
                                    <h5 class="delivery-order__delivery-price__title">Service Fee</h5>
                                </div>
                                <div class="delivery-order__delivery-price">
                                    <div
                                        class="delivery-order__delivery-price__price delivery_price price__badge">
                                        TBD
                                    </div>
                                    <h5 class="delivery-order__delivery-price__title">Shipping Fee</h5>
                                </div>
                            </div>
                            <div class="delivery-order__promo">
                                <div class="delivery-order__promo-label">
                                    Promo code?
                                </div>
                                <div class="delivery-order__promo-actions input-group">
                                    <input class="form-control input-sm" type="text" name="code" id="promocode">
                                    <span class="input-group-btn">
                                    <input class="btn btn-info btn-sm" type="button" value="Apply" loader-click="applyPromocode()">
                                </span>
                                </div>
                            </div>
                            <div class="delivery-order">
                                <div class="delivery-order__print-price">
                                    <div class="delivery-order__print-price__price print_price price__badge">
                                        $6.00
                                    </div>
                                    <h5 class="delivery-order__print-price__title"><strong>Total</strong></h5>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="thing-print-bottom">
                <a href="#pay-btn" class="btn btn-danger btn-lg thing-print-pay__pay-btn">Proceed to Checkout</a>
            </div>
        </div>
    </div>

</div>