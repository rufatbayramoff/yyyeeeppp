<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.09.17
 * Time: 14:58
 */

/** @var $this View */

/** @var $currentStepName string */

use common\components\JsObjectFactory;
use common\models\GeoCountry;
use frontend\assets\PrintModel3dAsset;
use lib\geo\GeoNames;
use yii\helpers\ArrayHelper;
use yii\web\View;

$stateData = $stateData ?? [];

Yii::$app->angular
    ->service(
        [
            'maps',
            'geo',
            'router',
            'notify',
            'user',
            'measure',
            'mathUtils',
            'modelService',
            'model3dParseProgress',
            'modal',
        ]
    )
    ->controller(
        [
            'store/filepage/models',
            'store/common-models',
            'store/filepage/printer-models',
            'store/filepage/printer-directives',
            'store/filepage/model-render',
            'store/filepage/color-service',
        ]
    )
    ->constants(
        [
            'globalConfig' =>
                [
                    'staticUrl' => param('staticUrl')
                ],
        ]
    );

PrintModel3dAsset::register($this);
JsObjectFactory::createJsObject(
    'model3dRenderRouteClass',
    'model3dRenderRoute',
    [
        'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl . '/'
    ],
    $this
);
?>
<?php if (!empty($this->title)): ?>
    <div class="container"><H2 align="center"><?= $this->title; ?></H2></div>
<?php endif; ?>


<script type="text/ng-template" id="/thingprint/print/form-review.html">
    <?= $this->render('tpl/form-review.php') ?>
</script>

<script type="text/ng-template" id="/thingprint/print/form-address.html">
    <?= $this->render('tpl/form-address.php') ?>
</script>

<script type="text/ng-template" id="/thingprint/print/form-print.html">
    <div ng-controller="printers-step-thingiverse">
        <?= $this->render('@frontend/views/my/print-model3d/templates/printers.php'); ?>
    </div>
</script>

<script type="text/ng-template" id="/thingprint/print/form-payment.html">
    <?= $this->render('tpl/form-payment.php') ?>
</script>

<script type="text/ng-template" id="/thingprint/print/form-wait.html">
    <?= $this->render('tpl/form-wait.php') ?>
</script>

<script type="text/ng-template" id="/thingprint/print/form-done.html">
    <?= $this->render('tpl/form-done.php') ?>
</script>

<script type="text/ng-template" id="/model3d/preview3d.html">
    <?= $this->render('@frontend/views/my/print-model3d/templates/model3d-preview.php') ?>
</script>

<?= $this->render('tpl/form.php', ['code' => $code, 'thingId' => $thingId, 'thingUserId' => $thingUserId]); ?>

<?php
$this->registerJs("
    TV.init({
        access_token: '" . $accessToken . "',
        api_url: '" . param('thingiverse_api_url') . "',
        target_url: '" . param('thingiverse_url') . "',
        target: parent
    }); 
");
\common\modules\thingPrint\assets\ThingiverseReloadFixAsset::register($this);
?>

