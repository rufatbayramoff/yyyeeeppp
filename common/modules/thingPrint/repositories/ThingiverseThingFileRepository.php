<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.05.17
 * Time: 15:01
 */

namespace common\modules\thingPrint\repositories;

use common\models\repositories\FileRepository;
use common\models\ThingiverseThingFile;
use yii\base\BaseObject;

class ThingiverseThingFileRepository extends BaseObject
{

    /**
     * @param ThingiverseThingFile $thingiverseThingFile
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function save(ThingiverseThingFile $thingiverseThingFile)
    {
        if ($thingiverseThingFile->isRelationPopulated('thing')) {
            $thingiverseThingFile->thing_id = $thingiverseThingFile->thing->thing_id;
        }
        if ($thingiverseThingFile->isRelationPopulated('file')) {
            $fileRepository = \Yii::createObject(FileRepository::class);
            $fileRepository->save($thingiverseThingFile->file);
            $thingiverseThingFile->file_id = $thingiverseThingFile->file->id;
        }

        $thingiverseThingFile->safeSave();
    }
}