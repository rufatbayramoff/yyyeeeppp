<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.05.17
 * Time: 11:17
 */

namespace common\modules\thingPrint\repositories;

use common\models\ThingiverseThing;
use yii\base\BaseObject;

class ThingiverseThingRepository extends BaseObject
{
    /** @var  ThingiverseThingFileRepository */
    public $thingiverseThingFileRepository;

    public function injectDependencies(ThingiverseThingFileRepository $thingiverseThingFileRepository)
    {
        $this->thingiverseThingFileRepository = $thingiverseThingFileRepository;
    }

    public function save(ThingiverseThing $thingiverseThing)
    {
        if ($thingiverseThing->isRelationPopulated('model3d')) {
            $thingiverseThing->model3d_id = $thingiverseThing->model3d->id;
        }
        $thingiverseThing->safeSave();
        foreach ($thingiverseThing->thingiverseThingFiles as $thingiverseThingFile) {
            $this->thingiverseThingFileRepository->save($thingiverseThingFile);
        }
    }
}