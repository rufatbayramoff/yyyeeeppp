<?php

namespace common\modules\thingPrint;

use yii\base\Module;

/**
 * Date: 11.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class ThingPrintModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\thingPrint\controllers';

    public const ANGULAR_APP_ASSETS = '@common/modules/thingPrint/assets/resources/angular';


    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();
    }
}
