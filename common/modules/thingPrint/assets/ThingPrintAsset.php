<?php
/**
 * Date: 16.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class ThingPrintAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/resources';
    public $js = [
    ];

    public $css = [
        'style.css'
    ];

    public $depends = [
        AppAsset::class,
        PostMessageAsset::class,
        ThingiverseIframeSdk::class,
    ];
}