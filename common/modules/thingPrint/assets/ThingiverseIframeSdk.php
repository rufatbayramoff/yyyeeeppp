<?php

/**
 * Date: 11.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
namespace common\modules\thingPrint\assets;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class ThingiverseIframeSdk extends AssetBundle
{
    public $sourcePath = '@vendor/makerbot/thingiverse-iframe-sdk';
    public $js = [
        'js/app.js',
        'js/json2.js',
        'js/tviframesdk.js',
    ];

}
