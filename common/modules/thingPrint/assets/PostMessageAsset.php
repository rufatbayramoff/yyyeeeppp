<?php
/**
 * Date: 16.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\assets;


use yii\web\AssetBundle;

/**
 * Class PostMessageAsset
 * required for iframe sdk requests.
 * internal in sdk - doesn't work with new version of jquery
 *
 * @package common\modules\thingPrint\assets
 */
class PostMessageAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/resources';
    public $js = [
        'postmessage.js',
    ];
}