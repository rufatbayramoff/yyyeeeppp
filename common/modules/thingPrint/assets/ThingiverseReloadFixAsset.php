<?php
/**
 * User: nabi
 */

namespace common\modules\thingPrint\assets;


use yii\web\AssetBundle;
use yii\web\View;

class ThingiverseReloadFixAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/resources';
    public $js = [
        'thingiverse-fix.js',
    ];

    public $jsOptions = [
        'position' => View::POS_END
    ];
}