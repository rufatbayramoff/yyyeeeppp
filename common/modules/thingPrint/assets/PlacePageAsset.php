<?php
namespace common\modules\thingPrint\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class PlacePageAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/resources';
    public $js = [
        'confirm-download.js'
    ];

    public $depends = [
        AppAsset::class,
    ];
}