var confirmDownloadModelClass = {
    config: {
        'thingId': '',
    },
    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        self.confirmImmediately();

    },
    confirmImmediately()
    {
        var self = this;
        setTimeout(function() {
            location.href = '/thingprint/place-page/download-model?thingId='+self.config.thingModelId;
            $('#loading-model-progress').removeClass('hidden');
        }, 100);
    },

    confirm: function () {
        var self = this;
        TS.confirm(
            'You are redirected from Thingiverse.com. We can download model <b>#' + self.config.thingModelId + '</b> for printing for you.',
            function (btn) {
                if (btn === 'ok') {
                    location.href = '/thingprint/place-page/download-model?thingId='+self.config.thingModelId;
                    $('#loading-model-progress').removeClass('hidden');
                } else {
                    location.href = '/my/print-model3d?utm_source=thingiverse';
                }
            },
            {
                confirm: _t('site.order', 'Yes, download it for me'),
                dismiss: _t('site.order', 'No')
            }
        );
    }
};

