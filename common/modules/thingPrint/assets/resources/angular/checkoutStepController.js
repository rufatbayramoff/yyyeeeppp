"use strict";

app.controller('checkout-step', ['$scope', '$router', '$http', '$notifyHttpErrors', '$state', 'controllerParams', function ($scope, $router, $http, $notifyHttpErrors, $state, controllerParams) {

    $scope.canPay = function () {
        return !$scope.cantPayErrorMessage;
    };

    $scope.createTsOrder = function (onSuccess) {
        var params = {
            thingUserId: $scope.formData.thingUserId,
            model3dUid: $scope.model3d.uid,
            userAddressId: $scope.deliveryForm.userAddressId,
            modelTextureInfo: $scope.model3dTextureState.getTexturesWordyFormat($scope.model3d),
            psPrinterId: $scope.offersBundleData.activeOfferItemId,
            comment: $scope.formData.comment,
            email: $scope.formData.email,
            phone: $scope.formData.phone
        };

        // Always create new order
        $http.post('/thingprint/print/make-order', params)
            .then(function (response) {
                $scope.orderId = response.data.order_id;
                $scope.formData.orderId = $scope.orderId;
                onSuccess();
            })
            .catch(function (response) {
                if (response.data) {
                    new TS.Notify({
                        type: 'error',
                        text: commonJs.prepareMessage(response.data.message || 'Error'),
                        automaticClose: true
                    });
                }
            });
    };

    $scope.payTsOrder = function (tries) {
        if (!$scope.formData.orderId) {
            if (tries < 10) {
                tries++;
                setTimeout(function () {
                    $scope.payTsOrder(tries);
                }, 1500);
            }
        }
        var params = {
            'thingId': $scope.formData.thingId,
            'orderId': $scope.formData.orderId,
            'thingiverseOrderId': $scope.formData.thingiverseOrderId
        };
        $http.post('/thingprint/print/pay-order', params)
            .then(function (response) {
                $scope.orderComplete = true;
                $state.go('done'); // show final page
                if (typeof(ga) != 'undefined' && ga) {
                    ga('send', 'pageview', '/thingprint/print/form-done.html');
                }
            })
            .catch(function (response) {
                /*
                if (response.data) {
                    new TS.Notify({
                        type: 'error',
                        text: commonJs.prepareMessage(response.data.message || 'Error'),
                        automaticClose: true
                    });
                } */
            });
    };

    $scope.makeOrder = function () {
        if (!$scope.deliveryForm.userAddressId) {
            new TS.Notify({
                type: 'error',
                text: 'Please enter a shipping address',
                automaticClose: true
            });
            return;
        }
        if (!$scope.offersBundleData.activeOfferItemId) {
            new TS.Notify({
                type: 'error',
                text: 'Please select printer.',
                automaticClose: true
            });
            return;
        }

        function validateEmail(email) {
            if (!email) {
                return false;
            }
            var EMAIL_REGEXP = /^[a-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/;
            return EMAIL_REGEXP.test(email.toLowerCase());
        }

        if (!validateEmail($scope.formData.email)) {
            new TS.Notify({
                type: 'error',
                text: 'An email address is required to place an order.',
                automaticClose: true
            });
            return;
        }
        if ($scope.deliveryForm.address.country == 'CN' && !$scope.formData.phone) {
            new TS.Notify({
                type: 'error',
                text: 'Please enter your telephone number.',
                automaticClose: true
            });
            return;
        }
        var params = {
            thing_id: $scope.formData.thingId,
            amount: $scope.checkoutStep.pricer.getTotal(),
            charges: $scope.checkoutStep.pricer.getCharges(),
            address_id: $scope.deliveryForm.address.id
        };
        $state.go('doneWait');
        if (typeof(ga) != 'undefined' && ga) {
            ga('send', 'pageview', '/thingprint/print/form-wait.html');
        }
        $scope.waitMessage = 'Waiting for response from thingiverse...';
        $scope.createTsOrder(function () {
            params.app_transaction_code = $scope.orderId;
            TV.dialog('payment', params, function (data) {
                if (console) console.log(data);
                if (data.status != 'cancelled') {
                    if (data.ok) {
                        if (data.order_id) {
                            $scope.formData.thingiverseOrderId = data.order_id;
                            $scope.payTsOrder(1);
                        } else {
                            $state.go('checkout');
                        }
                    } else if (data.error) {
                        $scope.waitMessage = 'Errors: ' + data.error;
                    }
                } else {
                    $state.go('checkout');
                    $scope.waitMessage = 'Order cancelled.';
                }
            });
        });
    };
}]);