"use strict";

app.controller('delivery-step', ['$scope', '$router', '$http', '$notifyHttpErrors', 'controllerParams', function ($scope, $router, $http, $notifyHttpErrors, controllerParams) {

    $scope.loadingAddress = false;

    $scope.chooseAddress = function () {
        function processAddress(data) {
            if (data.status == 'cancelled') {
                return;
            }

            $scope.checkoutStep.pricer.reset();

            var shippingAddress;
            if (data.ok && data.id) {
                /** @namespace data.shipping_address */

                shippingAddress = data.shipping_address;
                shippingAddress.id = data.id;
                $scope.deliveryForm.address = shippingAddress; // for faster loading
            } else if (data.error) {
                alert('Error: ' + data.error);
                return;
            }

            $scope.loadingAddress = true;

            $http.post('/thingprint/print/set-address',
                {'address': shippingAddress, 'thingUserId': $scope.formData.thingUserId}
            )
                .then(function (response) {
                    $scope.loadingAddress = false;
                    $scope.deliveryForm.shippingAddress = shippingAddress;
                    /** @namespace response.data.user_address_id */
                    $scope.deliveryForm.userAddressId = response.data.user_address_id;
                    $scope.offersBundleData.updatePrintersTextureSetted = false;
                })
                .catch(function (response) {
                    $scope.loadingAddress = false;
                    $scope.address = undefined;
                    if (response.data) {
                        new TS.Notify({
                            type: 'error',
                            text: commonJs.prepareMessage(response.data.message || 'Error setting address'),
                            automaticClose: true
                        });
                    }
                });
        }

        TV.dialog('address', processAddress);
    };

}]);