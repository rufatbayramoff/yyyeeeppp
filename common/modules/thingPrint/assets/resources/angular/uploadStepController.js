"use strict";

app.controller('upload-step', ['$scope', '$http', '$timeout', '$router', '$modelService', '$model3dParseProgress', 'controllerParams', function ($scope, $http, $timeout, $router, $modelService, $model3dParseProgress, controllerParams) {

    var refreshModelTimeout = 0;
    $scope.filesError = '';

    $scope.isLoadingModel3d = function () {

    };

    $scope.pingPartsStatus = function () {
        $modelService.checkJobs2($scope.model3d.uid, function (response) {
            var modelIsParsed = true;
            var model3dResponse = response.model3d;

            $scope.model3d.loadCancelParts(model3dResponse.canceledParts);
            $scope.model3d.fixParserParts(model3dResponse.parts);

            if (response.isComplited) {
                return true;
            } else {
                return false;
            }
        });
    };

    $scope.refreshModel3dProcess = function () {
        var getParams = {
            thingId: $scope.formData.thingId,
            code: $scope.formData.code
        };

        $http.get($router.getThingiverseModel(), {
            params: getParams
        }).then(function (response) {
            $scope.uploadStep.updateModelInProgress = false;

            if (!response.data.success) {
                $scope.filesError = _t('site.model3d', 'Analyzing files error. Please refresh the page and try again.');
                return;
            }
            $scope.model3d.load(response.data.model3d);
            $scope.model3dTextureState.load(response.data.model3dTextureState);
            if (!$scope.model3d.isCalculated()) {
                $scope.pingPartsStatus();
            }
        }).catch(function (response) {
            $scope.uploadStep.updateModelInProgress = false;
            $scope.filesError = response.message;
            new TS.Notify({
                type: 'error',
                text: response.message || 'Error on parsing 3D Models',
                automaticClose: false
            });
        });
    };

    $scope.refreshModel3d = function () {
        if ($scope.uploadStep.updateModelInProgress) {
            if (refreshModelTimeout) {
                clearTimeout(refreshModelTimeout);
            }
            refreshModelTimeout = setTimeout(function () {
                $scope.refreshModelTimeout = 0;
                $scope.refreshModel3dProcess();
            }, 1000);
            return;
        }
        $scope.uploadStep.updateModelInProgress = true;
        $scope.refreshModel3dProcess();
    };

    $scope.cancelUploadItem = function (model3dPart) {
        model3dPart.qty = 0;
        $scope.updateModel3dPartQty(model3dPart);
    };

    $scope.changeScale = function (scaleUnit) {
        return $modelService.switchScaleUnit($scope.model3d, scaleUnit, function () {
            $scope.offersBundleData.updatePrintersTextureSetted = false;
        });
    };

    $scope.testIsPrintablePartsExists = function () {
        return $modelService.testIsPrintablePartsExists($scope.model3d);
    };

    $scope.updateModel3dPartQty = function (model3dPart) {
        if (model3dPart.qty === '' || model3dPart.qty === null) {
            return;
        }
        $scope.offersBundleData.updatePrintersTextureSetted = false;
        return $modelService.updatePartQty(model3dPart);
    };

    if (!$scope.model3d.uid) {
        $scope.refreshModel3d();
    }
}]);

