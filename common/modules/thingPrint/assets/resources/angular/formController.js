"use strict";

app.config(['$stateProvider', '$routerProvider', function ($stateProvider, router) {
    $stateProvider
        .state('upload', {
            url: '/thingprint/print',
            templateUrl: '/thingprint/print/form-review.html',
            controller: 'upload-step'
        })
        .state('delivery', {
            url: '/thingprint/print_address',
            templateUrl: '/thingprint/print/form-address.html',
            controller: 'delivery-step'
        })
        .state('printers', {
            url: '/thingprint/print_printers',
            templateUrl: '/thingprint/print/form-print.html',
            controller: 'printers-step'
        })
        .state('checkout', {
            url: '/thingprint/print_payment',
            templateUrl: '/thingprint/print/form-payment.html',
            controller: 'checkout-step'
        })
        .state('doneWait', {
            url: '/thingprint/doneWait',
            templateUrl: '/thingprint/print/form-wait.html'
        })
        .state('done', {
            url: '/thingprint/done',
            templateUrl: '/thingprint/print/form-done.html'
        });
}]);


app.controller('formController', ['$scope', '$rootScope', '$router', '$state', '$modal', '$modelRender', 'OrderPricer', 'controllerParams', function ($scope, $rootScope, $router, $state, $modal, $modelRender, OrderPricer, controllerParams) {
    $scope.startUrl = window.location.href;
    $scope.validateErrors = {};
    $scope.model3d = new Model();
    $scope.model3d.sortPartsById();
    $scope.model3dTextureState = new ModelTextureState();
    $scope.uploadStep = {
        updateModelInProgress: false
    };
    $scope.deliveryForm = {
        address: {},
        userAddressId: null
    };
    $scope.formData = controllerParams.formData;
    $scope.formData.comment = '';
    $scope.formData.email = '';
    $scope.formData.phone = '';
    $scope.formData.orderId = '';
    $scope.formData.thingiverseOrderId = '';

    $scope.offersBundleData = {
        offerBundle: {},
        activeOfferItemId: -1,
        allowedMaterials: {},
        selectedMaterialGroupId: null,
        selectedMaterialColorId: null,
        selectedModel3dPartId: null,
        updatePrintersTextureSetted: false,
        sortPrintersBy: 'default',
        fixedLocation: true
    };
    $scope.checkoutStep = {
        pricer: OrderPricer,
        cantPayErrorMessage: ''
    };

    // Printers step controller
    $scope.printersPageInfo = {
        pageSize: 10,
        currentPage: 0,
        numPages: 1
    };

    $scope.steps = {
        'upload': {
            next: 'delivery',
            validate: function () {
                if ($scope.formData.thingiverseOrderId) {
                    window.location.href = $scope.startUrl;
                    return false;
                }
            }
        },
        'delivery': {
            prev: 'upload',
            next: 'printers',
            validate: function () {
                if ($scope.model3d.getActiveModel3dParts().length < 1) {
                    return [
                        _t('site.printModel3d', 'No printable parts exists')
                    ]
                }
                if ($scope.formData.thingiverseOrderId) {
                    window.location = $scope.startUrl;
                    return false;
                }
            }
        },
        'printers': {
            prev: 'delivery',
            next: 'checkout',
            validate: function () {
                if ($scope.model3d.getActiveModel3dParts().length < 1) {
                    return [
                        _t('site.printModel3d', 'No printable parts exists')
                    ]
                }
                if (!$scope.deliveryForm.userAddressId) {
                    return [
                        _t('site.printModel3d', 'Please select address')
                    ]
                }
                if ($scope.formData.thingiverseOrderId) {
                    window.location = $scope.startUrl;
                    return false;
                }
            }
        },
        'checkout': {
            prev: 'delivery',
            next: 'done',
            validate: function () {
                if ($scope.model3d.getActiveModel3dParts().length < 1) {
                    return [
                        _t('site.printModel3d', 'Files with supported formats not found')
                    ]
                }
                if (!$scope.deliveryForm.userAddressId) {
                    return [
                        _t('site.printModel3d', 'Please specify address')
                    ]
                }
                if ($scope.offersBundleData.activeOfferItemId < 1) {
                    return [
                        _t('site.printModel3d', 'Please select a manufacturer')
                    ]
                }
                if ($scope.formData.thingiverseOrderId) {
                    window.location = $scope.startUrl;
                    return false;
                }
            }
        },
        'doneWait': {
            next: 'done',
            validate: function () {
            }
        },
        'done': {
            next: 'done',
            validate: function () {
            }
        }
    };

    $scope.checkStep = function (stepName, showNotifies) {
        if (typeof showNotifies === 'undefined') {
            showNotifies = false;
        }
        var stateStep = $scope.steps[stepName];
        var validateErrors = stateStep.validate();

        if (validateErrors === false) {

            return false;
        }

        if (validateErrors && validateErrors.length) {
            new TS.Notify({
                type: 'error',
                text: commonJs.prepareMessage(validateErrors || 'Error'),
                automaticClose: true
            });
            return false;
        }
        return true;
    };

    $scope.prevStep = function () {
        var stepName = $state.current.name;
        var stateStep = $scope.steps[stepName];
        if (stateStep) {
            $state.go(stateStep.prev);
            TS.NotifyHelper.hideAll();
        }
    };

    $scope.nextStep = function () {
        var stepName = $state.current.name;
        var stateStep = $scope.steps[stepName];
        if (stateStep) {
            if ($scope.checkStep(stateStep.next, true)) {
                $state.go(stateStep.next);
                TS.NotifyHelper.hideAll();
            }
        }
    };

    $scope.parentNextStep = $scope.nextStep;

    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
            if (toState.name) {
                if (!$scope.checkStep(toState.name)) {
                    if (fromState.name) {
                        event.preventDefault();
                        $state.go(fromState.name);
                    }
                }
            }
        }
    );

    $scope.preview3d = function () {
        $modal.open({
            template: '/model3d/preview3d.html',
            onShown: function () {
                $rootScope.$broadcast('model3dPreview:modalOpen');
            },
            scope: {
                model3d: $scope.model3d,
                model3dTextureState: $scope.model3dTextureState,
                selectedModel3dPartId: $scope.offersBundleData.selectedModel3dPartId
            }
        });
    };

    $scope.getModel3dPartPreviewUrl = function (model3dPart) {
        if (model3dPart.isParsed && !model3dPart.isCalculated()) {
            return '/static/images/bad-model.png';
        }
        return $modelRender.getFullUrl(model3dPart, $scope.model3dTextureState.getColorForRendering(model3dPart));
    };

    $scope.isWidget = function () {
        return true;
    };

    // Set init step
    setTimeout(function () {
        $state.go('upload');
    }, 0);

}]);