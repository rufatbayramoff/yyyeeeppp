"use strict";

app.factory('OrderPricer', function () {
    return {
        prices: {
            shipping: 0,
            ps_print: 0,
            fee: 0,
            transaction: 0
        },
        titles: {
            shipping: 'Shipping',
            ps_print: 'Printing Fee',
            fee: 'Service Fee',
            transaction: 'Transaction Fee'
        },
        pricesWithTitle: [],
        priceTotal: undefined,


        reset : function()
        {
            this.prices = {
                shipping: 0,
                ps_print: 0,
                fee: 0,
                transaction: 0
            };
            this.pricesWithTitle = [];
            this.priceTotal = undefined;
        },
        /**
         *
         * @returns {Array}
         */
        getPricesWithTitle: function ()
        {
            if (this.pricesWithTitle.length > 0) {
                return this.pricesWithTitle;
            }

            for (var key in this.prices) {
                if (parseFloat(this.prices[key]) == 0) {
                    continue;
                }
                this.pricesWithTitle.push({price: '$' + parseFloat(this.prices[key]).toFixed(2), title: this.titles[key], code: key });
            }
            return this.pricesWithTitle;
        },

        /**
         *
         * @returns {{}}
         */
        getCharges: function ()
        {
            var result = {};
            for (var key in this.prices) {
                result[this.titles[key]] = parseFloat(this.prices[key].toFixed(2));
            }
            return result;
        },

        /**
         *
         * @returns {string}
         */
        getTotal: function ()
        {
            var total = 0.00;
            for (var key in this.prices) {
                total = parseFloat(this.prices[key].toFixed(2)) + total;
            }
            return total.toFixed(2);
        }
    }
});