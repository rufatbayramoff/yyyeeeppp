"use strict";

app.controller('printers-step-thingiverse', ['$scope', '$router', '$http', '$notify', '$notifyHttpErrors', '$modelRender', '$colorService', 'controllerParams', function ($scope, $router, $http, $notify, $notifyHttpErrors, $modelRender, $colorService, controllerParams) {
    $scope.printHere = function (offerItem) {
        $scope.offersBundleData.activeOfferItemId = offerItem.psPrinter.id;

        var params = {
            model3dUid: $scope.model3d.uid,
            userAddressId: $scope.deliveryForm.userAddressId,
            modelTextureInfo: $scope.model3dTextureState.getTexturesWordyFormat($scope.model3d),
            psPrinterId: offerItem.psPrinter.id
        };

        $http.post($router.getThingiverseCharges(), params)
            .then(function (response) {
                // console.log('ajax finished');
                if (!response.data || !response.data.success) {
                    $notify.error('Error. Please, try again.');
                } else {
                    $scope.checkoutStep.pricer.prices = response.data.charges;
                    $scope.checkoutStep.pricer.pricesWithTitle = [];
                }
            })
            .catch(function (response) {
                if (response.data) {
                    $scope.checkoutStep.cantPayErrorMessage = response.data.message || 'Error getting charges.';
                }
                $scope.checkoutStep.pricer.prices = {
                    shipping: 0,
                    ps_print: 0,
                    fee: 0,
                    transaction: 0
                };
                $scope.checkoutStep.pricer.pricesWithTitle = [];
                $notifyHttpErrors(response);
            });

        $scope.nextStep();
    }
}]);

