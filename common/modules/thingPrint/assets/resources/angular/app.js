"use strict";
/*
app.filter('numkeys', function () {
    return function (object) {
        return object ? Object.keys(object).length : 0;
    }
}).filter('int', function () {
    return function (num) {
        return toInteger(num)
    };
}).filter('intFormatted', function () {
    return function (num) {
        return toInteger(num).toLocaleString('en-EN').replace(',', ' ');
    }
});

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('form', {
            url: '/form',
            templateUrl: '/thingprint/print/form.php',
            controller: 'formController'
        })
        .state('form.review', {
            url: '/review',
            templateUrl: '/thingprint/print/form-review.html'
        })
        .state('form.address', {
            url: '/address',
            templateUrl: '/thingprint/print/form-address.html'
        })
        .state('form.print', {
            url: '/print',
            templateUrl: '/thingprint/print/form-print.html'
        })
        .state('form.payment', {
            url: '/payment',
            templateUrl: '/thingprint/print/form-payment.html'
        })
        .state('form.doneWait', {
            url: '/doneWait',
            templateUrl: '/thingprint/print/form-wait.html'
        })
        .state('form.done', {
            url: '/done',
            templateUrl: '/thingprint/print/form-done.html'
        });
    //noinspection JSUnresolvedFunction
    $urlRouterProvider.otherwise('/form/review');
})
*/
// our controller for the form
// =============================================================================
/*
    .controller('formController', function ($scope, $rootScope, $http, $state, $model3dParseProgress, $flushMessageManager, OrderPricer) {

        // check thingiverse postmessage is working
        var receivedPostmessage = false;
        setTimeout(function () {
            TV.sendMessage({cmd: 'echo', params: {foo: 'bar'}}, function (data) {
                receivedPostmessage = true;
            });
        }, 4000);
        setTimeout(function () {
            if (!receivedPostmessage) {
                $.post("/thingprint/api/log", {form: $scope.formData, document: JSON.stringify(document), limit: 1, key: $scope.formData.thingId}, function (data) {
                    if (data.success) {
                        // document.location.reload();
                    } else {
                        new TS.Notify({
                            type: 'error',
                            text: data.message,
                            automaticClose: false
                        });
                    }
                });
            }
        }, 6000);


        $rootScope.$on('$stateChangeStart',
            function (event, toState) {
                /** @namespace toState.name */
/*
                if (toState.name == 'form.address') {
                    if (typeof(ga) != 'undefined' && ga) {
                        ga('send', 'pageview', '/thingprint/print/form-review.html');
                    }
                    if ($scope.filesError) {
                        new TS.Notify({
                            type: 'error',
                            text: $scope.filesError,
                            automaticClose: true
                        });
                        return false;
                    }
                }
                if (toState.name === 'form.print') {
                    if ($scope.filesError) {
                        new TS.Notify({
                            type: 'error',
                            text: $scope.filesError,
                            automaticClose: true
                        });
                        return false;
                    }
                    if (!$scope.filesCompleted) {
                        new TS.Notify({
                            type: 'error',
                            text: 'Please wait for files parsing...',
                            automaticClose: true
                        });
                        return false;
                    }
                    if (!$scope.address) {
                        new TS.Notify({
                            type: 'error',
                            text: 'Please enter a shipping address',
                            automaticClose: true
                        });
                        return false;
                    }
                    if (typeof(ga) != 'undefined' && ga) {
                        ga('send', 'pageview', '/thingprint/print/form-print.html');
                    }
                    if ($scope.printers.length === 0) {
                        $scope.refreshPrinters();
                    }
                }
                if (toState.name == 'form.payment') {
                    if ($scope.formData.printerId === undefined) {
                        new TS.Notify({
                            type: 'error',
                            text: 'Please select printer.',
                            automaticClose: true
                        });
                        $scope.cantPayErrorMessage = 'Please select printer.';
                        return false;
                    }
                }
            }
        );

        var reloadTotal = 0;
        var reloadMax = 100;
        $scope.address = undefined;
        $scope.modelLink = 'https://www.treatstock.com/';
        $scope.orderComplete = false;
        $scope.cantPayErrorMessage = 'Please complete all steps before checkout.';
        $scope.pricer = OrderPricer;
        $scope.printers = [];
        $scope.colors = [];
        $scope.waitMessage = false;
        $scope.iAgree = {
            value: true
        };
        $scope.refreshFilesTimeout = 0;
        $scope.inFilesRefresh = false;
        $scope.filesError = false;
        $scope.filesCompleted = false;
        $scope.loadingPrinters = false;
        $scope.failedLoadingPrinters = false;
        $scope.needReloadPrinters = false;
        $scope.fileUnits = 'mm';
        $scope.model3dParts = [];
        $scope.material = {
            id: 2,
            title: 'PLA',
            info: 'Rigid plastic'
        };
        $scope.formData = {
            color: '',
            thingId: undefined,
            replicaId: 0,
            thingUserId: undefined,
            userAddressId: undefined,
            printerId: undefined
        };
        $scope.model3dParseProgress = $model3dParseProgress;

        // pagination
        $scope.filteredPrinters = [];
        $scope.currentPage = 0;
        $scope.requestCount = 0;
        $scope.numPerPage = 7;
        $scope.numPages = 0;

        $scope.selectPage = function (page) {
            $scope.currentPage = page;
            $scope.refreshPrinters(false);
        }
        $scope.getNumber = function (num) {
            var returnValue = new Array(Math.round(num));
            return returnValue;
        }
        $scope.init = function (code, thingId, thingUserId) {
            $scope.formData.code = code;
            $scope.formData.thingId = thingId;
            $scope.formData.thingUserId = thingUserId;
            $scope.refreshFiles();
        };

        $scope.resetPrinterList = function () {
            $scope.pricer.reset();
            $scope.formData.printerId = undefined;
            $scope.printers = [];
            $scope.currentPage = 0;
            $scope.numPages = 0;
        }

        /** Files **/
/*
        $scope.updateFilePartQty = function (file) {
            if ($scope.formData.replicaId === 0) {
                new TS.Notify({
                    type: 'error',
                    text: 'Please wait. Parsing 3D Models...',
                    automaticClose: false
                });
                return;
            }
            $scope.resetPrinterList();
            $scope.steps['form.print'].state = 'new';
            $scope.cantPayErrorMessage = 'Please select 3D printer before checkout.';
            $http.post('/thingprint/print/update-file', {'part_id': file.part_id, 'qty': file.qty})
                .then(function (response) {
                    $scope.refreshFiles();
                })
                .catch(function (response) {
                    if (response.data) {
                        $scope.cantPayErrorMessage = response.data.message || 'Error getting charges.';
                    }
                });
        };
        $scope.deleteFile = function (file) {
            if ($scope.formData.replicaId === 0) {
                new TS.Notify({
                    type: 'error',
                    text: 'Please wait. Parsing 3D Models...',
                    automaticClose: false
                });
            }
            file.qty = 0;
            $scope.updateFilePartQty(file);
        };
        $scope.refreshFiles = function () {
            $scope.filesError = false;
            var getUrl = '/thingprint/print/get-replica-files';
            var getParams = {
                replicaId: $scope.formData.replicaId,
                color: $scope.formData.color
            };
            if (!$scope.formData.replicaId) {
                getUrl = '/thingprint/print/get-files';
                getParams = {
                    thingId: $scope.formData.thingId,
                    code: $scope.formData.code,
                    color: $scope.formData.color
                };
            }
            if ($scope.inFilesRefresh) {
                if ($scope.refreshFilesTimeout) {
                    clearTimeout($scope.refreshFilesTimeout);
                }
                $scope.refreshFilesTimeout = setTimeout(function () {
                    $scope.refreshFilesTimeout = 0;
                    $scope.refreshFiles();
                }, 1000);
                return;
            }
            $scope.inFilesRefresh = true;
            $http.get(getUrl, {
                params: getParams
            }).then(function (response) {
                if (response.data.error) {
                    $scope.filesError = _t('site.model3d', 'Analyzing files error. Please refresh the page and try again.');
                    return;
                }

                $scope.inFilesRefresh = false;
                $scope.model3dParts = response.data.model3dParts;
                $scope.model3dParseProgress.showComment(_t('site.model3d', 'Analyzing files'));
                $scope.model3dParseProgress.setTotalFilesCount($scope.model3dParts ? Object.keys($scope.model3dParts).length : 0);
                $scope.model3dParseProgress.showProgress($scope.model3dParts);
                if ($scope.model3dParseProgress.processFinished) {
                    if ($scope.model3dParseProgress.isGoodPartsExists) {
                        // Restore standart progress bar
                        $scope.model3dParseProgress.showComment(_t('site.model3d', 'Loading...'));
                        var clearPrintersListLoad = setInterval(function () {
                            if ($('.printers_list_load').length) {
                                clearTimeout(clearPrintersListLoad);
                                $('.printers_list_load').addClass('printers_list_load_removed').removeClass('printers_list_load');
                            }
                        }, 500);
                    } else {
                        $scope.filesError = _t('site.model3d', 'No valid 3D model files were found.');
                    }
                }

                if (!$scope.formData.replicaId) {
                    $scope.formData.replicaId = response.data.replicaId;
                }
                if (response.data.needReload) {
                    reloadTotal++;
                    if (reloadTotal < reloadMax) {
                        if ($scope.refreshFilesTimeout) {
                            clearTimeout($scope.refreshFilesTimeout);
                        }
                        $scope.refreshFilesTimeout = setTimeout(function () {
                            $scope.refreshFilesTimeout = 0;
                            $scope.refreshFiles();
                        }, 5000);
                    }
                } else {
                    $scope.filesCompleted = true;
                    if (response.data.needResizeTo !== 'mm') {
                        $scope.scaleObjectToInches();
                    }
                    if (response.data.modelLink) {
                        $scope.modelLink = response.data.modelLink;
                    }
                    if ($scope.needReloadPrinters) {
                        $scope.needReloadPrinters = false;
                        $scope.refreshPrinters();
                    }
                }
            })
                .catch(function (response) {
                    $scope.inFilesRefresh = false;
                    $scope.filesError = response.message;
                    new TS.Notify({
                        type: 'error',
                        text: response.message || 'Error on parsing 3D Models',
                        automaticClose: false
                    });
                });
        };

        /** Shipping **/
/*
        $scope.chooseAddress = function () {
            function processAddress(data) {
                if (data.status == 'cancelled') {
                    return;
                }
                $scope.formData.printerId = undefined;
                $scope.pricer.reset();
                $scope.steps['form.print'].state = 'new';

                var shippingAddress;
                if (data.ok && data.id) {
                    /** @namespace data.shipping_address */
/*
                    shippingAddress = data.shipping_address;
                    shippingAddress.id = data.id;
                    $scope.address = shippingAddress; // for faster loading
                } else if (data.error) {
                    alert('Error: ' + data.error);
                    return;
                }
                $scope.currentColor = '';
                $scope.colors = [];
                $scope.formData.color = '';
                $http.post('/thingprint/print/set-address',
                    {'address': shippingAddress, 'thingUserId': $scope.formData.thingUserId}
                )
                    .then(function (response) {
                        $scope.shippingAddress = shippingAddress;
                        /** @namespace response.data.user_address_id */
/*
                        $scope.formData.userAddressId = response.data.user_address_id;
                        $scope.loadingPrinters = false;
                        $scope.printers = [];
                        $scope.refreshPrinters();
                        $scope.steps['form.address'].validate();
                    })
                    .catch(function (response) {
                        $scope.address = undefined;
                        if (response.data) {
                            new TS.Notify({
                                type: 'error',
                                text: commonJs.prepareMessage(response.data.message || 'Error setting address'),
                                automaticClose: true
                            });
                        }
                    });
            }

            TV.dialog('address', processAddress);
        };

        $scope.currentColor = '';
        /** Printers **/
/*
        $scope.chooseColor = function (color) {
            if ((typeof(ga) != 'undefined') && ga) {
                ga('send', 'event', {
                    eventCategory: 'ColorChange',
                    eventAction: 'click',
                    eventLabel: color.code
                });
            }
            $scope.formData.color = color.code;
            $scope.currentColor = color;
            $scope.formData.printerId = undefined;
            $scope.pricer.reset();
            $scope.refreshPrinters();

        };
        $scope.selectPrinter = function (printer) {
            $scope.pricer.pricesWithTitle = [];
            /** @namespace printer.plainPrice */
/*
            $scope.pricer.prices.ps_print = printer.plainPrice;

            $scope.formData.printerId = printer.id;
            //$scope.formData.prices = $scope.pricer.prices;

            $scope.getCharges();
            setTimeout(function () {
                $scope.steps['form.print'].validate();
                $state.go('form.payment');
            }, 200);

        };
        $scope.isCurrentColor = function (color) {
            if ($scope.currentColor === '') {
                $scope.currentColor = color;
            }
            return $scope.formData.color == color.code;
        };
        $scope.isSelectedPrinted = function (printer) {
            return $scope.formData.printerId === printer.id;
        };

        // last print color
        var lastPrintColor = '';
        // get list of printers based on given params
        $scope.refreshPrinters = function (resetPrintersFlag) {
            if (typeof(resetPrintersFlag) === 'undefined') {
                resetPrintersFlag = true;
            }
            if ($scope.loadingPrinters === true) {
                return;
            }
            if (!$scope.formData.replicaId) {
                $scope.refreshFiles();
                new TS.Notify({
                    type: 'error',
                    text: commonJs.prepareMessage('Please wait...'),
                    automaticClose: true
                });
                setTimeout(function () {
                    $scope.refreshPrinters();
                }, 5000);
                return;
            }
            if (resetPrintersFlag) {
                $scope.resetPrinterList();
            }
            $scope.loadingPrinters = true;
            $http.get('/thingprint/print/get-printers',
                {
                    params: {
                        replicaId: $scope.formData.replicaId,
                        material: $scope.formData.material,
                        color: $scope.formData.color,
                        code: $scope.formData.code,
                        userAddressId: $scope.formData.userAddressId,
                        page: $scope.currentPage,
                        pageSize: $scope.numPerPage
                    }
                })
                .then(function (response) {
                    $scope.requestCount++; // required to update filteredPrinters
                    $scope.loadingPrinters = false;
                    $scope.printers = [];
                    $scope.currentColor = '';
                    if (response.data.success) {
                        $scope.printersLoaded = true;
                        $scope.formData.color = response.data.selectedColor.code;
                        if (lastPrintColor != response.data.selectedColor.code) {
                            $scope.refreshFiles();
                            lastPrintColor = response.data.selectedColor.code;
                            $scope.currentColor = response.data.selectedColor;
                        }
                        $scope.colors = response.data.colors;
                        $scope.printers = response.data.printers;
                        $scope.numPages = Math.ceil(response.data.totalCount / $scope.numPerPage);
                    } else {
                        if (response.data.message == 'calculating') {
                            $scope.needReloadPrinters = true;
                        } else {
                            new TS.Notify({
                                type: 'error',
                                text: commonJs.prepareMessage(response.data.message || 'Error. Please try again.'),
                                automaticClose: true
                            });
                        }
                    }
                }).catch(function (response) {
                $scope.needReloadPrinters = true;
                $scope.loadingPrinters = false;
                $scope.failedLoadingPrinters = true;
                if (response.data) {
                    $scope.cantPayErrorMessage = response.data.message || 'Error getting charges.';
                }
            });
        };

        $scope.isScalePrompted = false;

        $scope.scaleObjectToInches = function () {
            if ($scope.isScalePrompted) {
                return;
            }
            var msg = 'This object seems very small. It may have been designed in inches. Would you like to scale it?';
            TS.confirm(msg, function (btn) {
                if (btn === 'ok') {
                    $scope.changeFileUnits('in');
                }
                $scope.isScalePrompted = true;
            }, {confirm: 'Yes', dismiss: 'No'});
        };

        $scope.changeFileUnits = function (units) {
            if ($scope.fileUnits === units) {
                return;
            }
            if (!$scope.formData.replicaId) {
                new TS.Notify({
                    type: 'error',
                    text: 'Please wait. Importing files...',
                    automaticClose: true
                });
                return;
            }
            $scope.filesCompleted = false;
            $scope.filesError = false;
            $scope.model3dParts = [];
            $scope.fileUnits = units;
            $scope.resetPrinterList();
            $http.post('/thingprint/print/change-file-units',
                {
                    units: units,
                    thingId: $scope.formData.thingId,
                    replicaId: $scope.formData.replicaId,
                    code: $scope.formData.code,
                    color: $scope.formData.color
                })
                .then(function (response) {
                    $scope.model3dParts = response.data.model3dParts;
                    $scope.filesCompleted = true;
                })
                .catch(function (response) {
                    if (response.data) {
                        $scope.cantPayErrorMessage = response.data.message || 'Error getting charges.';
                    }
                });
        };

        $scope.getCharges = function () {
            var defaultCalcMsg = 'Please wait. Calculating fees...';
            if ($scope.cantPayErrorMessage === defaultCalcMsg) {
                return;
            }
            $scope.cantPayErrorMessage = defaultCalcMsg;
            $http.post('/thingprint/print/get-charges', $scope.formData)
                .then(function (response) {
                    $scope.pricer.prices = response.data.charges;
                    $scope.pricer.pricesWithTitle = [];
                    $scope.cantPayErrorMessage = '';
                })
                .catch(function (response) {
                    if (response.data) {
                        $scope.cantPayErrorMessage = response.data.message || 'Error getting charges.';
                    }
                });

        };

        /** order payment **/
/*
        $scope.makeOrder = function () {
            if (!$scope.formData.userAddressId) {
                new TS.Notify({
                    type: 'error',
                    text: 'Please enter a shipping address',
                    automaticClose: true
                });
                return;
            }
            if (!$scope.iAgree.value) {
                new TS.Notify({
                    type: 'error',
                    text: 'Please check the box agreeing to our terms and conditions before proceeding.',
                    automaticClose: true
                });
                return;
            }

            function validateEmail(email) {
                if (!email) {
                    return false;
                }
                var EMAIL_REGEXP = /^[a-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/;
                return EMAIL_REGEXP.test(email.toLowerCase());
            }

            if (!validateEmail($scope.formData.email)) {
                new TS.Notify({
                    type: 'error',
                    text: 'An email address is required to place an order.',
                    automaticClose: true
                });
                return;
            }
            if ($scope.address.country == 'CN' && !$scope.formData.phone) {
                new TS.Notify({
                    type: 'error',
                    text: 'Please enter your telephone number.',
                    automaticClose: true
                });
                return;
            }
            var params = {
                thing_id: $scope.formData.thingId,
                amount: $scope.pricer.getTotal(),
                charges: $scope.pricer.getCharges(),
                address_id: $scope.address.id
            };
            $state.go('form.doneWait');
            if (typeof(ga) != 'undefined' && ga) {
                ga('send', 'pageview', '/thingprint/print/form-wait.html');
            }
            $scope.waitMessage = 'Waiting for response from thingiverse...';
            $scope.createTsOrder(); // create order on our side without binding to thingiverse order id and in status payment pending
            TV.dialog('payment', params, function (data) {
                if (console) console.log(data);
                if (data.status != 'cancelled') {
                    if (data.ok) {
                        if (data.order_id) {
                            $scope.formData.thingiverseOrderId = data.order_id;
                            $scope.payTsOrder(1);
                        } else {
                            $state.go('form.payment');
                        }
                    } else if (data.error) {
                        $scope.waitMessage = 'Errors: ' + data.error;
                    }
                } else {
                    $state.go('form.payment');
                    $scope.waitMessage = 'Order cancelled.';
                }
            });
        };

        /**
         * make order on our side
         */
/*
        $scope.createTsOrder = function () {
            $http.post('/thingprint/print/make-order', $scope.formData)
                .then(function (response) {
                    /** @namespace response.data.order_id */
/*
                    $scope.orderId = response.data.order_id;
                    $scope.formData.orderId = $scope.orderId;
                })
                .catch(function (response) {
                    if (response.data) {
                        new TS.Notify({
                            type: 'error',
                            text: commonJs.prepareMessage(response.data.message || 'Error'),
                            automaticClose: true
                        });
                    }
                });
        }

        /**
         * mark as paid
         */
/*
        $scope.payTsOrder = function (tries) {
            if (!$scope.formData.orderId) {
                if (tries < 10) {
                    tries++;
                    setTimeout(function () {
                        $scope.payTsOrder(tries);
                    }, 1500);
                }
            }
            $http.post('/thingprint/print/pay-order', $scope.formData)
                .then(function (response) {
                    /** @namespace response.data.order_id */
/*
                    $scope.orderId = response.data.order_id;
                    $scope.orderComplete = true;
                    $state.go('form.done'); // show final page
                    if (typeof(ga) != 'undefined' && ga) {
                        ga('send', 'pageview', '/thingprint/print/form-done.html');
                    }
                })
                .catch(function (response) {
                    if (response.data) {
                        new TS.Notify({
                            type: 'error',
                            text: commonJs.prepareMessage(response.data.message || 'Error'),
                            automaticClose: true
                        });
                    }
                });
        };

        $scope.canPay = function () {
            var hasPrinter = $scope.formData.printerId !== undefined;
            var hasColor = $scope.formData.color !== undefined;
            var hasAddress = $scope.formData.userAddressId !== undefined;
            var canPayMsg = $scope.cantPayErrorMessage == '';
            var filesLoaded = $scope.filesCompleted;
            return hasPrinter && hasColor && hasAddress && canPayMsg && filesLoaded;
        };

        /** tab states management **/
/*
        $scope.nextStep = function () {
            var stateStep = $scope.steps[$state.current.name];
            stateStep.errorMessage = undefined;
            var isValid = stateStep.validate();

            if (isValid && stateStep.next) {
                $state.go(stateStep.next);
                TS.NotifyHelper.hideAll();
            } else {
                new TS.Notify({
                    type: 'error',
                    text: commonJs.prepareMessage(stateStep.errorMessage || 'Error'),
                    automaticClose: true
                });
            }
        };

        $scope.steps = {
            'form.review': {
                next: 'form.address',
                state: 'new',
                validate: function () {
                    this.state = 'checked';
                    if (typeof(ga) != 'undefined' && ga) {
                        ga('send', 'pageview', '/thingprint/print/form-address.html');
                    }
                    if ($scope.filesError) {
                        this.errorMessage = $scope.filesError;
                        return false;
                    }
                    return true;
                }
            },
            'form.address': {
                next: 'form.print',
                state: 'new',
                validate: function () {
                    this.state = 'checked';
                    if (!$scope.formData.userAddressId) {
                        this.state = 'error';
                        this.errorMessage = 'Please enter a shipping address or wait for verification...';
                        return false;
                    }
                    if ($scope.filesError) {
                        this.errorMessage = $scope.filesError;
                        return false;
                    }
                    return true;
                }
            },
            'form.print': {
                next: 'form.payment',
                state: 'new',
                validate: function () {
                    this.state = 'checked';
                    if (!$scope.formData.printerId) {
                        this.state = 'error';
                        this.errorMessage = 'Please select 3D Printer';
                        return false;
                    }
                    if ($scope.filesError) {
                        this.errorMessage = $scope.filesError;
                        return false;
                    }
                    if ($scope.pricer.prices.fee > 0) {
                    } else {
                        $scope.getCharges();
                    }
                    if (typeof(ga) != 'undefined' && ga) {
                        ga('send', 'pageview', '/thingprint/print/form-payment.html');
                    }
                    return true;
                }
            },
            'form.payment': {
                next: 'form.doneWait',
                state: 'new',
                validate: function () {
                    if (typeof(ga) != 'undefined' && ga) {
                        ga('send', 'pageview', '/thingprint/print/form-payment.html');
                    }
                    return true;
                }
            },
            'form.doneWait': {
                next: 'form.done',
                state: 'new',
                validate: function () {
                    if (typeof(ga) != 'undefined' && ga) {
                        ga('send', 'pageview', '/thingprint/print/form-wait.html');
                    }
                    return true;
                }
            },
            'form.done': {
                state: 'new',
                validate: function () {
                    if (typeof(ga) != 'undefined' && ga) {
                        ga('send', 'pageview', '/thingprint/print/form-done.html');
                    }
                    return true;
                }
            }
        };

    }).filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);

*/