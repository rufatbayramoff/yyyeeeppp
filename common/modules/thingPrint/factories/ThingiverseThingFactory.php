<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.05.17
 * Time: 17:06
 */

namespace common\modules\thingPrint\factories;

use common\components\DateHelper;
use common\models\ThingiverseThing;
use common\modules\thingPrint\models\Thing;
use common\modules\thingPrint\models\ThingFile;
use Yii;
use yii\base\BaseObject;

class ThingiverseThingFactory extends BaseObject
{
    /** @var  ThingiverseThingFileFactory */
    protected $thingiverseThingFileFactory;

    /**
     * @param ThingiverseThingFileFactory $thingiverseThingFileFactory
     */
    public function injectDependencies(ThingiverseThingFileFactory $thingiverseThingFileFactory)
    {
        $this->thingiverseThingFileFactory = $thingiverseThingFileFactory;
    }

    /**
     * @param Thing $thing
     * @param ThingFile[] $thingFiles
     * @return ThingiverseThing
     */
    public function createByThing(Thing $thing, $thingFiles): ThingiverseThing
    {
        /** @var ThingiverseThing $thingiverseThing */
        $thingiverseThing = Yii::createObject(ThingiverseThing::class);
        $thingiverseFiles = [];
        foreach ($thingFiles as $thingFile) {
            $thingiverseFile = $this->thingiverseThingFileFactory->createByThingFile($thingFile);
            $thingiverseFile->setThing($thingiverseThing);
            $thingiverseFiles[] = $thingiverseFile;
        }
        $thingiverseThing->thing_id = $thing->id;
        $thingiverseThing->created_at = DateHelper::now();
        $thingiverseThing->setThingiverseThingFiles($thingiverseFiles);
        return $thingiverseThing;
    }
}