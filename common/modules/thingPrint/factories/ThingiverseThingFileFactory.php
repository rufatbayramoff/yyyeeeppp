<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.05.17
 * Time: 17:09
 */

namespace common\modules\thingPrint\factories;

use common\components\DateHelper;
use common\models\ThingiverseThingFile;
use common\modules\thingPrint\models\ThingFile;
use Yii;

class ThingiverseThingFileFactory
{
    /** @var  FileFactory */
    public $fileFactory;

    /**
     * @param FileFactory $fileFactory
     */
    public function injectDependencies(FileFactory $fileFactory)
    {
        $this->fileFactory = $fileFactory;
    }

    public function createByThingFile(ThingFile $thingFile): ThingiverseThingFile
    {
        /** @var ThingiverseThingFile $thingiverseFile */
        $thingiverseFile = Yii::createObject(ThingiverseThingFile::class);

        $file = $this->fileFactory->createFileFromPath($thingFile->tempFilePath);
        $publicExtensions = ThingFile::PUBLIC_EXTENSIONS;
        if (in_array(strtolower($file->extension), $publicExtensions)) {
            $file->is_public = true;
            $file->setPublicMode(true);
        }
        $thingiverseFile->thingfile_id = $thingFile->id;
        $thingiverseFile->name  = mb_substr($thingFile->name, 0, 245);
        $thingiverseFile->size  = $thingFile->size;
        $thingiverseFile->url   = mb_substr($thingFile->url, 0, 245);
        $thingiverseFile->download_url = mb_substr($thingFile->downloadUrl, 0, 245);
        $thingiverseFile->thumb = mb_substr($thingFile->thumbnail, 0, 245);
        $thingiverseFile->render = $thingFile->getRender();
        $thingiverseFile->created_at = DateHelper::now();
        $thingiverseFile->setFile($file);
        return $thingiverseFile;
    }
}