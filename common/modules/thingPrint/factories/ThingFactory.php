<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.05.17
 * Time: 11:10
 */
namespace common\modules\thingPrint\factories;

use common\models\ThingiverseThing;
use common\modules\thingPrint\models\Thing;
use Yii;

class ThingFactory
{

    public function createThingByThingiverseThingModel(ThingiverseThing $thingiverseThing):Thing
    {
        /** @var Thing $thing */
        $thing = Yii::createObject(Thing::class);
        $thing->id = $thingiverseThing->thing_id;
        $thing->name = $thingiverseThing->formName();
        return $thing;
    }

    /**
     * Thing constructor.
     * create Thing from given json data
     *
     * @param $jsonConfig
     * @return Thing
     */
    public function createThingByJson($jsonConfig):Thing
    {
        /** @var Thing $thing */
        $thing = Yii::createObject(Thing::class);
        foreach($jsonConfig as $c=>$v){
            if(!property_exists($thing, $c)){
                continue;
            }
            $thing->$c = $v;
        }
        return $thing;
    }
}