<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.05.17
 * Time: 14:58
 */

namespace common\modules\thingPrint\factories;

use common\models\factories\FileFactory;
use common\models\Model3d;
use common\models\ThingiverseThingFile;
use common\models\User;
use common\models\UserSession;
use common\modules\product\interfaces\ProductInterface;
use common\modules\thingPrint\components\ThingiverseApiWrapper;
use common\modules\thingPrint\models\Thing;
use yii\base\BaseObject;


class Model3dFactory extends BaseObject
{

    /**
     * @var ThingiverseApiWrapper
     */
    public $api;

    /**
     * @var FileFactory
     */
    public $fileFactory;


    /**
     * @var \common\models\factories\Model3dFactory
     */
    public $model3dBaseFactory;


    public function injectDependencies(ThingiverseApiWrapper $api, FileFactory $fileFactory, \common\models\factories\Model3dFactory $model3dBaseFactory): void
    {
        $this->api = $api;
        $this->fileFactory = $fileFactory;
        $this->model3dBaseFactory = $model3dBaseFactory;
    }

    /**
     * @param Thing $thing
     * @param ThingiverseThingFile[] $thingiverseThingFiles
     * @return Model3d
     */
    public function createModel3dFromThing(Thing $thing, array $thingiverseThingFiles): \common\models\Model3d
    {
        $files = [];
        // create cover file
        $coverFile = $this->fileFactory->createFileFromPath($thing->tempCoverFilePath);
        $coverFile->setPublicMode(true);
        $files[] = $coverFile;

        foreach ($thingiverseThingFiles as $thingiverseThingFile) {
            $files[] = $thingiverseThingFile->file;
        }
        $model3dProperties = [
            'title'           => mb_substr($thing->name, 0, 45),
            'description'     => mb_substr($thing->description, 0, 3000),
            'source'          => Model3d::SOURCE_THINGIVERSE,
            'cae'             => Model3d::CAE_CAD,
            'user_session_id' => 1,
            'product_status'  => ProductInterface::STATUS_PUBLISHED_DIRECTLY
        ];

        if ($thing->getDetails()) {
            $model3dProperties['print_instructions'] = mb_substr($thing->getDetails(), 0, 65000);
        }

        $model3d = $this->model3dBaseFactory->createModel3dByFilesList($files, $model3dProperties);
        return $model3d;
    }
}