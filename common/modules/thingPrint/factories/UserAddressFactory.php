<?php
/**
 * Date: 27.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\factories;


use common\models\GeoCountry;
use common\models\ThingiverseUser;
use common\models\UserAddress;
use common\modules\thingPrint\models\ThingUserAddress;

class UserAddressFactory
{
    /**
     * creates UserAddress object from given array params
     *
     * @param ThingUserAddress $address
     * @param ThingiverseUser $thingiverseUser
     * @return UserAddress
     * @throws \yii\web\NotFoundHttpException
     * @internal param array $params
     */
    public static function create(ThingUserAddress $address, ThingiverseUser $thingiverseUser = null)
    {
        $userAddress = new UserAddress();
        $userAddress->address = $address->street_address;
        $userAddress->extended_address = '';
        $userAddress->city = $address->city;
        $userAddress->region = $address->admin_area;
        $userAddress->zip_code = $address->postal_code;
        $country = GeoCountry::tryFind(['iso_code' => (string)$address->country]);
        $userAddress->country_id = $country->id;
        $userAddress->populateRelation('country', $country);
        $userAddress->contact_name = $address->name;
        $userAddress->company = $address->organisation;
        if($thingiverseUser){
            $userAddress->user_id = $thingiverseUser->user_id;
        }
        return $userAddress;
    }
}