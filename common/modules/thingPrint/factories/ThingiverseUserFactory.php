<?php
/**
 * Date: 31.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\factories;


use common\models\ThingiverseUser;
use common\models\User;
use common\models\UserProfile;
use common\modules\thingPrint\models\ThingUser;
use yii\base\UserException;

class ThingiverseUserFactory
{

    /**
     * creates user based on thingiverse api /users/me response
     * if user already in db, returns our user
     *
     * @param ThingUser $thingUser
     * @param string $accessToken
     * @return User
     * @throws UserException
     */
    public function createUser(ThingUser $thingUser, $accessToken = '')
    {
        $hasUser = ThingiverseUser::findOne(['thingiverse_id'=>$thingUser->id]);
        if($hasUser){
            $this->updateAccessToken($hasUser, $accessToken);
            return $hasUser->user;
        }
        $user = new User();
        $user->username = $thingUser->name . '_tg' . $thingUser->id;
        $user->email = empty($thingUser->email) ? null : $thingUser->email;
        $user->created_at = time();
        $user->updated_at = time();
        $user->lastlogin_at = time();
        $user->auth_key = md5(time() . $thingUser->full_name);
        $user->password_hash = md5(time() . $thingUser->full_name);

        if(!$user->validate()){
            throw new UserException(implode('', $user->getFirstErrors()));
        }
        $user->safeSave();


        $userProfile = new UserProfile();
        $userProfile->avatar_url = $thingUser->getAvatarFile();
        $userProfile->user_id = $user->id;
        $userProfile->current_lang = 'en-US';
        $userProfile->timezone_id = 'America/Los_Angeles';
        $userProfile->full_name = $thingUser->full_name;
        $user->populateRelation('userProfile', $userProfile);
        if(!$userProfile->validate()){
            throw new UserException(implode('', $userProfile->getFirstErrors()));
        }
        $userProfile->safeSave();

        // bind with user
        ThingiverseUser::addRecord([
            'user_id' => $user->id,
            'thingiverse_id' => $thingUser->id,
            'api_json' => $thingUser->getApiJson(),
            'created_at' => dbexpr('NOW()'),
            'access_token' => $accessToken
        ]);

        return $user;
    }

    /**
     * -- move this method to services
     *
     * @param ThingiverseUser $hasUser
     * @param $accessToken
     */
    private function updateAccessToken(ThingiverseUser $hasUser, $accessToken)
    {
        if($hasUser->access_token!==$accessToken){
            $hasUser->access_token = $accessToken;
            $hasUser->updated_at = dbexpr('NOW()');
            $hasUser->safeSave();
        }
    }
}