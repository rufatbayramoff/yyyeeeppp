<?php
/**
 * Date: 24.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\components;

use common\components\Emailer;
use common\models\ThingiverseOrder;
use common\modules\thingPrint\controllers\ApiController;
use common\modules\thingPrint\models\ThingOrder;
use yii\helpers\Html;

/**
 * Class ThingiverseWebhook
 *
 * @url http://sandbox.thingiverse.com/developers/getting-started#webhooks
 * @package common\modules\thingPrint\components
 * @see ApiController - used for webhook call /thingprint/api/webhook
 */
class ThingiverseWebhook
{

    /**
     * order created. manager will be notified via email
     */
    const EVENT_ORDER_CREATED = 'order_created';

    /**
     * user requested refund. manager actions required
     */
    const EVENT_REFUND_REQUESTED = 'refund_requested';

    /**
     * order refunded
     */
    const EVENT_ORDER_REFUNDED = 'order_refunded';

    /**
     * event used to check that webhook is working
     */
    const EVENT_PING = 'ping';

    private $emailer;
    private $thingiverseOrderService;

    public function __construct()
    {
        $this->emailer = new Emailer();
        $this->thingiverseOrderService = new ThingiverseOrderService(new ThingiverseApi());
    }

    /**
     * run command by event
     *
     * @param $event
     * @param $post
     */
    public function run($event, array $post)
    {
        $thingOrder = $this->initOrder($post);
        switch($event){
            case self::EVENT_ORDER_CREATED:
                $this->eventOrderCreated($thingOrder);
                break;
            case self::EVENT_ORDER_REFUNDED:
                $this->eventOrderRefunded($thingOrder);
                break;
            case self::EVENT_REFUND_REQUESTED:
                $this->eventRefundRequested($thingOrder);
                break;
            case self::EVENT_PING:
                \Yii::info($event, 'thingiverse');
                break;
            default:
                \Yii::error('webhook event [' . $event. '] not found', 'thingiverse');

        }
    }

    /**
     * we are notified about order created.
     * but we cannot create new order on our backend, because no information
     * about printer id and color
     *
     * @param ThingOrder $thingOrder
     */
    private function eventOrderCreated(ThingOrder $thingOrder)
    {
        // email to manager about new thing order.
        $thingiverseOrderExists = ThingiverseOrder::find()->where(['thingiverse_order_id' => $thingOrder->id])->exists();
        if (!$thingiverseOrderExists) {
            // lets try to create new order
            $this->thingiverseOrderService->createThingiverseOrderByWebhook($thingOrder);
        } else {
            $this->refreshOrderByApi($thingOrder);
        }
    }

    /**
     * @param ThingOrder $thingOrder
     */
    private function eventRefundRequested(ThingOrder $thingOrder)
    {
        try{
            $link = $this->getBackendLink($thingOrder);
            $message = sprintf('Request REFUND Thingiverse order #%d' . "\n" . 'Link: %s', $thingOrder->id, $link);
            $this->emailer->sendSupportMessage('Request REFUND Thingiverse Order - #' . $thingOrder->id, $message);

            ThingiverseFacade::refreshOrderByWebhook($thingOrder);
        }catch(\Exception $e){
            \Yii::info($e, 'thingiverse');
        }
    }

    /**
     * @param ThingOrder $thingOrder
     */
    private function eventOrderRefunded(ThingOrder $thingOrder)
    {
        try{
            $link = $this->getBackendLink($thingOrder);
            $message = sprintf('REFUNDED Thingiverse order #%d' . "\n" . 'Link: %s', $thingOrder->id, $link);
            $this->emailer->sendSupportMessage('REFUNDED Thingiverse Order - #' . $thingOrder->id, $message);
            $tvOrder = ThingiverseFacade::refreshOrderByWebhook($thingOrder);
            if ($tvOrder && $tvOrder->order->currentAttemp) {
                $storeOrder = $tvOrder->order;
                $$storeOrder->is_dispute_open = 1;
                $storeOrder->safeSave();
                if (!$storeOrder->currentAttemp->isNew() && $storeOrder->isPayed()) {
                    $this->emailer->sendPsOpenDispute($storeOrder->currentAttemp);
                }
            }
        }catch(\Exception $e){
            \Yii::info($e, 'thingiverse');
        }
    }

    /**
     * @param $post
     * @return ThingOrder
     */
    private function initOrder($post)
    {
        if ($post['event'] === self::EVENT_PING || empty($post['order'])) {
            return null;
        }
        $thingOrder = new ThingOrder($post['order']);
        return $thingOrder;
    }

    /**
     * @param $thingOrder
     * @return string
     */
    private function getBackendLink($thingOrder)
    {
        if(YII_ENV=='prod'){
            $a1 = Html::a('View in backend', 'https://backend.treatstock.com/store/store-order?StoreOrderSearch[thingiverseOrderId]='.$thingOrder->id);
            $a2 = Html::a('View on thingiverse', 'https://www.thingiverse.com/order:'. $thingOrder->id);
            return $a1 . ' ' . $a2;
        }else{
            return 'http://backend.vcap.me/store/store-order?StoreOrderSearch[thingiverseOrderId]='.$thingOrder->id;
        }
    }
}