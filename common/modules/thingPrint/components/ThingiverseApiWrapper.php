<?php
/**
 * Date: 19.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\components;

use common\components\FileTypesHelper;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\modules\thingPrint\factories\ThingFactory;
use common\components\FileDirHelper;
use common\modules\thingPrint\models\Thing;
use common\modules\thingPrint\models\ThingFile;
use common\modules\thingPrint\models\ThingOrder;
use common\modules\thingPrint\models\ThingUser;
use Yii;
use yii\helpers\VarDumper;

/**
 * Class ThingiverseApiWrapper
 *
 * @package common\modules\thingPrint\components
 */
class ThingiverseApiWrapper
{
    const SUCCESS_CODE = 200;

    /**
     * @var ThingiverseApi
     */
    public $api;

    /** @var  ThingFactory */
    protected $thingFactory;

    /**
     * ThingiverseApiWrapper constructor.
     *
     * @param ThingiverseApi $api
     * @param ThingFactory|null $thingFactory
     */
    public function __construct(ThingiverseApi $api, ThingFactory $thingFactory = null)
    {
        $this->api = $api;
        if (!$thingFactory) {
            $thingFactory = Yii::createObject(ThingFactory::class);
        }
        $this->thingFactory = $thingFactory;
    }

    public function getAllowedFormats()
    {
        return array_merge(Model3dBasePartInterface::ALLOWED_FORMATS, FileTypesHelper::ALLOW_IMAGES_EXTENSIONS);
    }


    /**
     * call api methods, if they are not defined in wrapper
     *
     * @param $method
     * @param array $arguments
     * @return mixed
     */
    public function __call($method, $arguments = [])
    {
        return call_user_func_array([$this->api, $method], $arguments);
    }

    /**
     * get current user
     *
     * @return ThingUser
     * @throws ThingiverseApiException
     * @internal param string $name
     */
    public function getMe()
    {
        $status = $this->api->getUser('me');
        if ($status !== self::SUCCESS_CODE) {
            throw new ThingiverseApiException($this->api->last_response_error);
        }
        $thingUser = new ThingUser($this->api->response_data);
        return $thingUser;
    }

    /**
     * @param $id
     * @param null $file_id
     * @return ThingFile[]
     * @throws \common\modules\thingPrint\components\ThingiverseApiException
     */
    public function getThingFiles($id, $file_id = null)
    {
        $status = $this->api->getThingFiles($id, $file_id);
        if ($status !== self::SUCCESS_CODE) {
            throw new ThingiverseApiException($this->api->last_response_error);
        }
        $response = $this->api->response_data;
        $result = [];
        foreach ($response as $file) {
            $thingFile = new ThingFile($file);
            $ext = $thingFile->getExtension();
            if (!in_array(strtolower($ext), $this->getAllowedFormats())) {
                continue;
            }
            $result[] = $thingFile;

        }
        return $result;
    }

    /**
     * @param $id
     * @return Thing|null
     * @throws \common\modules\thingPrint\components\ThingiverseApiException
     */
    public function getThing($id)
    {
        $status = $this->api->getThing($id);
        if ($status !== self::SUCCESS_CODE) {
            throw new ThingiverseApiException($this->api->last_response_error . '[' . $status . ']');
        }
        if (empty($this->api->response_data)) {
            throw new ThingiverseApiException('No response data');
        }
        $thing = $this->thingFactory->createThingByJson($this->api->response_data);
        return $thing;
    }

    /**
     * @param Thing $thing
     * @return string
     */
    public function downloadThingCover(Thing $thing)
    {
        $coverUrl = $thing->getCoverUrl();
        $thingdir = \Yii::$app->getRuntimePath() . '/things/' . $thing->id;
        FileDirHelper::createDir($thingdir);
        $toFile = $thingdir . '/' . strtolower(basename($coverUrl));
        $thing->tempCoverFilePath = $toFile;
        if (file_exists($toFile)) {
            return $toFile;
        }
        file_put_contents($toFile, fopen($thing->getCoverUrl(), 'rb'));
        return $toFile;
    }

    /**
     * download file and get path to it
     *
     * @param integer $thingId
     * @param ThingFile $thingFile
     * @return string
     */
    public function downloadThingFile($thingId, ThingFile $thingFile)
    {
        FileDirHelper::createDir(\Yii::$app->getRuntimePath() . '/things/');
        FileDirHelper::createDir(\Yii::$app->getRuntimePath() . '/things/' . $thingId . '/');

        $toFile = \Yii::$app->getRuntimePath() . '/things/' . $thingId . '/' . $thingFile->id . '_' . $thingFile->getFilename() . '.' . $thingFile->getExtension();
        $thingFile->tempFilePath = $toFile;
        if (file_exists($toFile)) {
            return $toFile;
        }
        file_put_contents($toFile, fopen($thingFile->downloadUrl, 'rb'));
        return $toFile;
    }


    /**
     * @param $thingiverseOrderId
     * @return ThingOrder
     * @throws ThingiverseApiException
     */
    public function getOrder($thingiverseOrderId)
    {
        $status = $this->api->getOrder($thingiverseOrderId);
        if ($status !== self::SUCCESS_CODE) {
            throw new ThingiverseApiException($this->api->last_response_error . '[' . $status . ']');
        }
        if (empty($this->api->response_data)) {
            throw new ThingiverseApiException('No response data');
        }
        $thingOrder = new ThingOrder($this->api->response_data);
        return $thingOrder;
    }

    /**
     *
     * @param ThingFile[] $thingFiles
     * @return  ThingFile[] $thingFiles
     */
    public function validateAndTrimThingFiles(array $thingFiles)
    {
        $thingFilesFiltered = [];
        $thingFileNamesMap = [];
        foreach ($thingFiles as $thingFile) {
            if (empty($thingFileNamesMap[$thingFile->name])) {
                $thingFileNamesMap[$thingFile->name] = [];
            }
            $thingFileNamesMap[$thingFile->name][$thingFile->id] = $thingFile;
        }

        foreach ($thingFileNamesMap as $thingFilesByName) {
            if (count($thingFilesByName) === 1) {
                $thingFilesFiltered[] = array_pop($thingFilesByName);
            } else {
                $thingFilesFiltered[] = end($thingFilesByName);
            }
        }
        return $thingFilesFiltered;
    }

    /**
     * @param $thingId
     * @param ThingFile[] $thingFiles
     */
    public function downloadThingFiles($thingId, array $thingFiles)
    {
        foreach ($thingFiles as $thingFile) {
            $this->downloadThingFile($thingId, $thingFile);
        }
    }
}