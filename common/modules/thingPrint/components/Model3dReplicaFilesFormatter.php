<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.05.17
 * Time: 10:43
 */

namespace common\modules\thingPrint\components;

use common\interfaces\Model3dBaseInterface;
use common\models\File;
use common\models\model3d\Model3dNeedToResizePredicate;
use common\models\Model3dPart;
use common\models\Model3dReplica;
use common\models\PrinterColor;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialRepository;
use common\services\Model3dPartService;
use console\jobs\model3d\ParserJob;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;
use lib\MeasurementUtil;

class Model3dReplicaFilesFormatter
{
    /** @var  PrinterMaterialRepository */
    public $printerMaterialRepository;

    /** @var  PrinterColorRepository */
    public $printerColorRepository;

    /** @var Model3dNeedToResizePredicate */
    public $needToResizePredicate;

    public function injectDependencies(
        PrinterMaterialRepository $printerMaterialRepository,
        PrinterColorRepository $printerColorRepository,
        Model3dNeedToResizePredicate $needToResizePredicate
    ): void {
        $this->printerMaterialRepository = $printerMaterialRepository;
        $this->printerColorRepository = $printerColorRepository;
        $this->needToResizePredicate = $needToResizePredicate;
    }

    /**
     * format model3d files for front side json
     * with calculated weight in PLA and with size in current measurement
     * if calc&weight not ready, send needReload=false
     *
     * @param Model3dBaseInterface $model3dReplica
     * @param string $color - render color (Red,White and etc)
     * @return array ['files'=>, 'needReload'=>]
     * @throws \Exception
     */
    public function formatModel3dParts(Model3dBaseInterface $model3dReplica, $color)
    {
        $printerMaterial = $this->printerMaterialRepository->getByCode('PLA');
        $colorObj = $color ? PrinterColor::findOne(['render_color' => $color]) : null;
        $colorHex = $colorObj ? $colorObj->getRgbHex() : null;

        $parts = $model3dReplica->getActiveModel3dParts();
        $needReload = false;
        $partsSizes = [];
        $model3dPartsInfo = [];
        foreach ($parts as $model3dPart) {
            /** @var $model3dPart Model3dPart */
            $imgUrl = Model3dPartService::getModel3dPartRenderUrlIfExists($model3dPart, $colorHex);
            if ($imgUrl) {
                $displayUrl = ImageHtmlHelper::getThumbUrl($imgUrl, ImageHtmlHelper::DEFAULT_WIDTH, ImageHtmlHelper::DEFAULT_HEIGHT);
                $thumbUrl = ImageHtmlHelper::getThumbUrl($imgUrl, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL);
            } else {
                list($displayUrl, $thumbUrl) = self::getThingiverseRender($model3dPart->file);
            }


            $isParsed = $model3dPart->isCaclulatedProperties();
            $isParseError = false;
            if ($isParsed) {
                $partsSizes[] = $model3dPart->getSize();
            } else {
                if (ParserJob::isFailed($model3dPart->file)) {
                    $isParseError = true;
                } else {
                    $needReload = true;
                }
            }

            $model3dPartsInfo[$model3dPart->file->id] = [
                'part_id' => $model3dPart->id,
                'image'   => $displayUrl,
                'thumb'   => $thumbUrl,
                'title'   => $model3dPart->name,
                'parser'  => [
                    'isSuccess' => $isParsed,
                    'isError'   => $isParseError,
                ],
                'weight'  => $isParsed ? round($model3dPart->getWeight($printerMaterial), 2) : 0,
                'size'    => $isParsed ? $model3dPart->getSize()->toStringWithMeasurement('in') .
                    ' (' . $model3dPart->getSize()->toStringWithMeasurement('mm') . ')' : 'calculating...',
                'qty'     => $isParseError ? 0 : $model3dPart->qty
            ];
        }

        if (!$needReload) {// run antivirus and render now
            Model3dFacade::createQueueRenderJobs($model3dReplica->getSourceModel3d());
        }
        $modelLink = $url = param('siteUrl') . Model3dFacade::getStoreUrl($model3dReplica->getSourceModel3d()).'?model3dReplicaId=' . $model3dReplica->id;
        return [
            'replicaId'    => $model3dReplica->id,
            'model3dParts' => $model3dPartsInfo,
            'needReload'   => $needReload,
            'needResizeTo' => count($partsSizes) === count($model3dPartsInfo) ? $this->needToResizePredicate->isNeedResizeTo($partsSizes) : MeasurementUtil::MM,
            'filesUnit'    => $model3dReplica->model_units,
            'modelLink'    => $modelLink
        ];
    }

    /**
     * get render for given file from thingiverse if exists
     *
     * @param File $file
     * @return array
     */
    private static function getThingiverseRender(File $file)
    {
        if ($file->thingiverseThingFile) {
            return [
                $file->thingiverseThingFile->thumb,
                $file->thingiverseThingFile->render,
            ];
        } else {
            return [
                \Yii::$app->params['staticUrl'] . Model3dFacade::EMPTY_RENDER_IMAGE,
                \Yii::$app->params['staticUrl'] . Model3dFacade::EMPTY_RENDER_IMAGE
            ];
        }
    }

}