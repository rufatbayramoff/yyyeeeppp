<?php
/**
 * Date: 03.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\components;


use common\models\StoreOrder;
use common\models\ThingiverseOrder;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyOrderInformer;
use common\modules\thingPrint\models\ThingOrder;

class ThingiverseOrderService
{

    /**
     * @var ThingiverseApi
     */
    private $api;
    /**
     * @var ThingiverseApiWrapper
     */
    private $apiWrapper;

    /**
     * ThingiverseOrderService constructor.
     *
     * @param ThingiverseApi $api
     */
    public function __construct(ThingiverseApi $api)
    {
        $this->api = $api;
        $this->apiWrapper = new ThingiverseApiWrapper($api);
    }

    /**
     * @param StoreOrder $order
     * @param null $isShipped
     * @param null $isCancelled
     * @param null $refundNote
     * @param null $note
     * @return ThingiverseOrder
     * @throws ThingiverseApiException
     */
    public function updateOrder(StoreOrder $order, $isShipped = null, $isCancelled = null, $refundNote = null, $note = null)
    {
        $thingiverseOrder = ThingiverseOrder::tryFind(['order_id' => $order->id]);
        if($isShipped!==null) $thingiverseOrder->is_shipped = $isShipped;
        if($isCancelled!==null) $thingiverseOrder->is_cancelled = $isCancelled;
        if($refundNote!==null) $thingiverseOrder->refund_note = $refundNote;
        if($note!==null) $thingiverseOrder->note = $note;
        $thingiverseOrder->updated_at = dbexpr('NOW()');
        $thingiverseOrder->save(false);
        $this->api->updateOrder($thingiverseOrder->thingiverse_order_id, $isShipped, $isCancelled , $refundNote, $note);
        if(!empty($this->api->last_response_error)){
            $logData = date('Y-m-d H:i:s') . ';' . $order->id . ';' . $thingiverseOrder->thingiverse_order_id . ';' . $isShipped . ';' . $isCancelled . ';' . $refundNote . ';' . $note."\n";
            file_put_contents('/var/www/treatstock/backend/runtime/logs/thingiverse_update_order.log', $logData, FILE_APPEND);
            // throw new ThingiverseApiException($this->api->last_response_error);
        }
        return $thingiverseOrder;
    }

    /**
     * @param StoreOrder $order
     * @return ThingiverseOrder
     * @throws \yii\web\NotFoundHttpException
     * @throws \common\modules\thingPrint\components\ThingiverseApiException
     */
    public function refreshOrderByApi(StoreOrder $order)
    {
        $thingiverseOrder = ThingiverseOrder::tryFind(['order_id' => $order->id]);
        $thingOrder = $this->apiWrapper->getOrder($thingiverseOrder->thingiverse_order_id);
        $thingiverseOrder->status = $thingOrder->status;
        $thingiverseOrder->is_shipped = $thingOrder->is_shipped;
        $thingiverseOrder->note = $thingOrder->note;
        $thingiverseOrder->api_json = $thingOrder->jsonConfig;
        $thingiverseOrder->updated_at = dbexpr('NOW()');
        $thingiverseOrder->save(false);
        return $thingiverseOrder;
    }

    /**
     * update order by webhook
     *
     * @param ThingOrder $thingOrder
     * @return ThingiverseOrder
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function refreshOrderByWebhook(ThingOrder $thingOrder)
    {
        $thingiverseOrder = ThingiverseOrder::tryFind(['thingiverse_order_id' => $thingOrder->id]);

        $thingiverseOrder->status = $thingOrder->status;
        $thingiverseOrder->is_shipped = $thingOrder->is_shipped;
        $thingiverseOrder->note = $thingOrder->note;
        $thingiverseOrder->api_json = $thingOrder->jsonConfig;
        $thingiverseOrder->updated_at = dbexpr('NOW()');
        $thingiverseOrder->save(false);

        return $thingiverseOrder;
    }

    public function createThingiverseOrderByWebhook(ThingOrder $thingOrder)
    {
        // create new one
        $thingiverseOrder = ThingiverseOrder::addRecord(
            [
                'thingiverse_order_id' => $thingOrder->id,
                'thing_id'              => $thingOrder->thing_id,
                'order_id'             => null,
                'created_at'           => dbexpr('NOW()'),
                'updated_at'           => dbexpr('NOW()'),
                'is_shipped'           => 0,
                'is_cancelled'         => 0,
                'status'               => $thingOrder->status,
                'refund_note'          => '',
                'note'                 => '',
                'api_json'             => $thingOrder->jsonConfig
            ]
        );
        return $thingiverseOrder;
    }
}