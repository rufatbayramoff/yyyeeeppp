<?php
/**
 * Date: 16.02.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
namespace common\modules\thingPrint\components;

use common\models\StoreOrder;
use common\models\ThingiverseOrder;
use common\models\ThingiverseUser;
use common\models\User;
use common\modules\thingPrint\models\ThingOrder;

/**
 * Class ThingiverseFacade helps easy to call API functions to
 * - refresh thingiverse order information on our side
 * - update thingiverse order information on their side. mark order as ready,shipped and etc.
 *
 * @package common\modules\thingPrint\components
 */
class ThingiverseFacade
{

    /**
     * refresh thingiverse order information using API access token
     * binned to order
     *
     * @see ThingiverseOrderService::refreshOrderByApi()
     * @param StoreOrder $order
     * @return \common\models\ThingiverseOrder
     */
    public static function refreshOrderByApi(StoreOrder $order)
    {
        $accessToken = self::getUserAccessToken($order->user);
        $api = new ThingiverseApi($accessToken);
        $service = new ThingiverseOrderService($api);
        return $service->refreshOrderByApi($order);
    }

    /**
     * refresh order by webhook.
     *
     * @see ThingiverseOrderService::refreshOrderByWebhook()
     * @param ThingOrder $thingOrder
     * @return ThingiverseOrder
     * @throws \yii\web\NotFoundHttpException
     */
    public static function refreshOrderByWebhook(ThingOrder $thingOrder)
    {
        $api = new ThingiverseApi();
        $service = new ThingiverseOrderService($api);
        return $service->refreshOrderByWebhook($thingOrder);
    }

    /**
     * update thingiverse order
     *
     * @param StoreOrder $order
     * @param null $isShipped
     * @param null $isCancelled
     * @param null $refundNote
     * @param null $note
     * @return ThingiverseOrder
     * @throws \yii\web\NotFoundHttpException
     */
    public static function updateOrder(StoreOrder $order, $isShipped = null, $isCancelled = null, $refundNote = null, $note = null)
    {
        $accessToken = self::getUserAccessToken($order->user);
        $api = new ThingiverseApi($accessToken);
        $service = new ThingiverseOrderService($api);
        return $service->updateOrder($order, $isShipped, $isCancelled, $refundNote, $note);
    }

    /**
     * Try to find access token for given user.
     * this access token is required for API requests.
     *
     * @param User $user
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    private static function getUserAccessToken(User $user)
    {
        $thingUser = ThingiverseUser::tryFind(['user_id'=>$user->id]);
        return $thingUser->access_token;
    }

    /**
     * check if given order is not in status disputed or refunded on thingiverse side.
     *
     * @param StoreOrder $order
     * @throws ThingiverseApiException
     */
    public static function tryValidateOrder(StoreOrder $order)
    {
        $tOrder = self::refreshOrderByApi($order);
        if ($tOrder->status === ThingiverseOrder::STATUS_DISPUTED || $tOrder->status === ThingiverseOrder::STATUS_REFUNDED) {
            throw new ThingiverseApiException(
                _t(
                    'site.order',
                    'Order is in status {status}, because the customer has requested a refund.',
                    ['status' => $tOrder->status]
                )
            );
        }
    }
}