<?php

namespace common\modules\thingPrint\components;


use common\models\ThingiverseReport;
use DateTime;
use DateTimeZone;
use Yii;
use yii\base\UserException;

class ThingiverseReportUpdater
{
    public $login;
    public $password;

    public $debugOutputEnable = true;

    /**
     * @var string
     */
    protected $cookieFilePath;

    /**
     * @var array
     */
    protected $crudOrders = [];

    public const START_PAGE = 50;

    /**
     * New orders add
     *
     * @var int
     */
    public $totalAdd = 0;

    public function debugOutput($output)
    {
        if ($this->debugOutputEnable) {
            if (is_array($output)) {
                print_r($output);
            } else {
                echo $output;
            }
            echo "\n";
        }
    }

    public function loadReport($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
        $this->login();
        $this->loadCrudPages();
        return $this->totalAdd;
    }

    public function updateEmpty($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
        $this->login();
        $emptyDates = ThingiverseReport::find()
            ->where(['app_transaction_code' => null])
            ->orderBy('date desc')
            ->all();
        $crudOrders = [];
        $this->debugOutput('Update count: '.count($emptyDates));
        foreach ($emptyDates as $emptyDateReport) {
            $crudOrders[$emptyDateReport->order_id] = [
                'order_id' => $emptyDateReport->order_id,
                'status'   => $emptyDateReport->status
            ];
        }
        $this->crudOrders = $crudOrders;
        $this->updateCrudPages();
        return $this->totalAdd;
    }

    protected function login()
    {
        $this->cookieFilePath = tempnam(Yii::getAlias('@console/runtime'), 'thingiverseCookie.txt');

        $url = 'https://accounts.thingiverse.com/login';
        $poststring = sprintf('username=%s&password=%s&rememberMe=0&theme=thingiverse&redirect=', $this->login, $this->password);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFilePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFilePath);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $poststring);
        $output = curl_exec($ch);
        curl_close($ch);
        if (!$output) {
            throw new UserException('Login failed');
        }
        $this->debugOutput($output);
    }

    protected function parseCrudPage($output)
    {
        $containerMarker = '<div class="results-container">';
        $buff = substr($output, strpos($output, $containerMarker));
        $orderMarker = '<a href="/order:';
        while ($orderPos = strpos($buff, $orderMarker)) {
            // Order id
            $orderId = (int)substr($buff, $orderPos + strlen($orderMarker), 10);
            if (!array_key_exists($orderId, $this->crudOrders)) {
                $this->crudOrders[$orderId] = [];
            }
            $this->crudOrders[$orderId]['order_id'] = $orderId;
            $buff = substr($buff, $orderPos + 1);
            // Status
            $statusMarker = "<span class='flex-row-item col-5'>";
            $statusPos = strpos($buff, $statusMarker);
            if ($statusPos) {
                $statusPosEnd = strpos($buff, '</span>', $statusPos);
                $status = substr($buff, $statusPos + strlen($statusMarker), $statusPosEnd - $statusPos - strlen($statusMarker));
                $this->crudOrders[$orderId]['status'] = $status;
            }
        }
    }

    protected function loadCrudPage($pageId)
    {
        $this->debugOutput('Loading crud page: ' . $pageId);
        $ch = curl_init('https://www.thingiverse.com/developers/pending/page:' . $pageId);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFilePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFilePath);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode === 200) {
            $this->parseCrudPage($output);
        }
        $this->debugOutput('Result code: ' . $httpCode . ' Size: ' . strlen($output) . ' Orders per page: ' . count($this->crudOrders));
        return $httpCode;
    }

    protected function loadCrudPages()
    {
        for ($pageId = self::START_PAGE; $pageId < 100; $pageId++) {
            $this->crudOrders = [];
            if ($this->loadCrudPage($pageId) === 200) {
                $this->updateCrudPages();
            } else {
                // Redirect to prev page
                break;
            }
        }
    }

    /**
     * Parse order page
     *
     * @param string $orderPage
     * @param ThingiverseReport $thingiverseReport
     */
    protected function parseOrderPage($orderPage, ThingiverseReport $thingiverseReport)
    {
        $result = preg_match('/<strong>Order Date<\/strong>[\s\n]+([0-9\/\s:a-z]+) UTC/', $orderPage, $matches);
        $thingiverseReport->date = $result ? (new DateTime($matches[1], new DateTimeZone('UTC')))->format('Y-m-d H:i:s') : '-';

        $result = preg_match('/ <a href="\/thing:(\d+)">/', $orderPage, $matches);
        $thingiverseReport->thing_id = $result ? $matches[1] : '-';

        $result = preg_match('/<strong>App Transaction Code<\/strong>[\s\n]+<span>(\d+)<\/span>/', $orderPage, $matches);
        $thingiverseReport->app_transaction_code = $result ? $matches[1] : '-';
        $result = preg_match('/Printing Fee:[\s\n]+<span>\$([\d\.]+)/', $orderPage, $matches);
        $thingiverseReport->printing_fee = $result ? $matches[1] : '-';
        $result = preg_match('/Shipping:[\s\n]+<span>\$([\d\.]+)/', $orderPage, $matches);
        $thingiverseReport->shipping = $result ? $matches[1] : '-';
        $result = preg_match('/Refund:[\s\n]+<span>-\$([\d\.]+)/', $orderPage, $matches);
        $thingiverseReport->refund = $result ? $matches[1] : '-';
        $result = preg_match_all('/Transaction Fee:[\s\n]+<span>\$([\d\.]+)/', $orderPage, $matches);
        if ($result) {
            if (count($matches[1]) > 1) {
                $thingiverseReport->treatstock_transaction_fee = $result ? $matches[1][0] : '-';
                $thingiverseReport->transaction_fee = $result ? $matches[1][1] : '-';
            } else {
                $thingiverseReport->treatstock_transaction_fee = '-';
                $thingiverseReport->transaction_fee = $result ? $matches[1][0] : '-';
            }
        }
        $result = preg_match('/Platform Fee:[\s\n]+<span>\$([\d\.]+)/', $orderPage, $matches);
        $thingiverseReport->platform_fee = $result ? $matches[1] : '-';
        $result = preg_match('/Total Earned:[\s\n]+<span>[\s\n]+\$([\d\.]+)/', $orderPage, $matches);
        $thingiverseReport->earnings = $result ? $matches[1] : '-';
    }

    /**
     * Load order details
     *
     * @param ThingiverseReport $thingiverseReport
     * @throws \yii\base\UserException
     */
    protected function loadOrderDetails(ThingiverseReport $thingiverseReport)
    {
        $ch = curl_init('https://www.thingiverse.com/order:' . $thingiverseReport->order_id);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFilePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFilePath);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode === 200) {
            $this->parseOrderPage($output, $thingiverseReport);
            $this->debugOutput('Order: ' . $thingiverseReport->order_id . ' Treatstock order: ' . $thingiverseReport->app_transaction_code . ' Earnings: ' . $thingiverseReport->earnings . ' Refund: ' . $thingiverseReport->refund);
            $thingiverseReport->safeSave();
        }
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    protected function updateCrudPages()
    {
        foreach ($this->crudOrders as $crudOrderInfo) {
            $thingiverseReport = ThingiverseReport::findOne(['order_id' => $crudOrderInfo['order_id']]);
            if (!$thingiverseReport) {
                $thingiverseReport = Yii::createObject(ThingiverseReport::class);
                $thingiverseReport->order_id = $crudOrderInfo['order_id'];
                $thingiverseReport->status = $crudOrderInfo['status'];
                $thingiverseReport->safeSave();
                $this->debugOutput('New order: ' . $thingiverseReport->order_id);
                $this->totalAdd++;
            } else {
                if ($thingiverseReport->status !== $crudOrderInfo['status']) {
                    $this->debugOutput('Change status '.$thingiverseReport->order_id.': ' . $thingiverseReport->status . ' => ' . $crudOrderInfo['status']);
                    $thingiverseReport->status = $crudOrderInfo['status'];
                    $thingiverseReport->safeSave();
                }
            }
            if (!$thingiverseReport->date || !$thingiverseReport->app_transaction_code || !$thingiverseReport->printing_fee) {
                $this->loadOrderDetails($thingiverseReport);
            }
        }
    }

    /**
     * @deprecated now not working, thingiverse broke export filtering by dates
     */
    protected function loadCsv()
    {
        /*
            $ch = curl_init("https://www.thingiverse.com/ajax/developers/pending_orders_csv");
            curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            //curl_setopt ($ch, CURLOPT_POST, 1);
            $output = curl_exec($ch);
            curl_close($ch);
            file_put_contents(\Yii::getAlias('@console/runtime/thingiverse_report.csv'), $output);

            $report = file_get_contents(\Yii::getAlias('@console/runtime/thingiverse_report.csv'));
            $reportLines = explode("\n", $report);
            $result = [];
            $reportOrders = ThingiverseReport::find()->select('order_id')->column();
            $i = 0;
            foreach ($reportLines as $k => $line) {
                if ($k == 0) {
                    continue;
                }
                if (empty($line)) {
                    continue;
                }
                $line = str_replace(['USD', '$'], '', $line);
                $data = str_getcsv($line);
                if (!array_key_exists(2,$data)) {
                    continue;
                }
                //Date,Amount,"Order Number","Thing ID",App,Status,Earnings,"Platform Fee","Transaction Fee",Tip
                if (in_array($data[2], $reportOrders)) {
                    ThingiverseReport::updateRow(['order_id' => $data[2]], ['status' => $data[5]]);
                    continue;
                }
                if (!is_int($data[3])||$data[3]<1) {
                    // Check valid line
                    continue;
                }
                ThingiverseReport::addRecord([
                    'order_id'        => $data[2],
                    'date'            => $data[0],
                    'thing_id'        => $data[3],
                    'status'          => $data[5],
                    'earnings'        => $data[6],
                    'platform_fee'    => $data[7],
                    'transaction_fee' => $data[8],
                ]);
                $i++;
            }
         */
    }
}