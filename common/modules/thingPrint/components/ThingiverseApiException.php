<?php
/**
 * Date: 19.01.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\components;

use yii\base\UserException;

class ThingiverseApiException extends UserException
{

}