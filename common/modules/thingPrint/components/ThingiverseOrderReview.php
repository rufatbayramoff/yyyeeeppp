<?php
/**
 * User: nabi
 */

namespace common\modules\thingPrint\components;


use common\models\StoreOrder;
use common\models\ThingiverseOrder;
use common\models\ThingiverseUser;

class ThingiverseOrderReview
{
    const SESSION_USER_KEY = 'anonymouseReview_user_id';

    /**
     * @param StoreOrder $order
     * @return null|string
     */
    public static function getOrderAccessHash(StoreOrder $order)
    {
        if(!$order->isThingiverseOrder()){
            return null;
        }
        $thingUser = ThingiverseUser::findOne(['user_id'=>$order->user_id]);
        if(!$thingUser){
            return null;
        }
        return md5($thingUser->access_token);
    }

    /**
     * @param ThingiverseOrder $thingOrder
     * @return null|string
     */
    public static function getThingiverseUserEmail(ThingiverseOrder $thingOrder)
    {
        $thingUser = ThingiverseUser::findOne(['user_id'=>$thingOrder->order->user_id]);
        if(!$thingUser){
            return null;
        }
        return $thingUser->email;
    }
}