<?php

namespace common\modules\thingPrint\controllers;

use common\components\BaseController;
use common\models\factories\FileFactory;
use common\models\factories\Model3dFactory;
use common\models\factories\Model3dReplicaFactory;
use common\models\Model3d;
use common\models\repositories\Model3dReplicaRepository;
use common\models\repositories\Model3dRepository;
use common\models\ThingiverseThing;
use common\modules\thingPrint\components\ThingiverseApi;
use common\modules\thingPrint\components\ThingiverseApiWrapper;
use common\modules\thingPrint\services\ThingiverseThingService;
use common\services\Model3dService;
use frontend\models\model3d\PlaceOrderState;
use Yii;
use yii\base\UserException;

class PlacePageController extends BaseController
{
    public $viewPath = '@common/modules/thingPrint/views';

    /* @var ThingiverseThingService */
    public $thingiverseThingService;

    /** @var Model3dService */
    public $model3dService;

    /** @var PlaceOrderState */
    public $placeOrderState;

    public function injectDependencies(ThingiverseThingService $thingiverseThingService, Model3dService $model3dService, PlaceOrderState $placeOrderState)
    {
        $this->thingiverseThingService = $thingiverseThingService;
        $this->model3dService = $model3dService;
        $this->placeOrderState = $placeOrderState;
    }

    public function actionIndex()
    {
        $thingModelId = Yii::$app->request->get('thing_id');
        if (!$thingModelId) {
            $referrer = Yii::$app->request->referrer;
            if ($thingPos = strpos($referrer, 'thing:')) {
                $thingModelId = (int)substr($referrer, $thingPos + strlen('thing:'), 20);
            };
        }
        if (!$thingModelId) {
            return $this->redirect('/my/print-model3d?utm_source=thingiverse');
        }
        return $this->render(
            'placePage.php',
            [
                'thingModelId' => $thingModelId,
            ]
        );
    }

    protected function failedDownload($logMessage)
    {
        Yii::warning($logMessage, 'thingiverse');
        \Yii::$app->getSession()->setFlash('error', 'Sorry, but downloading thingiverse model failed. Please try do it manually.');
        $this->placeOrderState->resetState();
        return $this->redirect('/my/print-model3d?utm_source=thingiverse');
    }

    public function actionDownloadAltModel($thingId)
    {
        $thingId = (int)$thingId;
        $fileArchive = $this->thingiverseThingService->downloadByDirectLink($thingId);
        if (!$fileArchive) {
            return $this->failedDownload('Failed loading file');
        }
        $model3d = $this->model3dService->createModel3dByFilesList(['source' => Model3d::SOURCE_THINGIVERSE], [$fileArchive]);
        return $this->redirect('/my/print-model3d?model3d=' . $model3d->getUid());
    }

    public function actionDownloadModel($thingId)
    {
        $thingiverseThing = ThingiverseThing::findOne(['thing_id' => $thingId]);

        $thingApi = new ThingiverseApi(app('session')->get('thingiverse_token', null));
        if (empty($thingApi->access_token)) {
            try {
                $thingApi->oAuth('');
            } catch (\Exception $e) {
                return $this->failedDownload($e->getTraceAsString());
            }
            if ($thingApi->access_token) {
                app('session')->set('thingiverse_token', $thingApi->access_token);
            }
        }
        $this->thingiverseThingService->apiWrapper->api = $thingApi;
        $model3d = null;
        try {
            if ($thingiverseThing) {
                $this->thingiverseThingService->updateFiles($thingiverseThing);
                $model3d = $thingiverseThing->model3d;
            } else {
                $model3d = $this->thingiverseThingService->createModel3dByThingId($thingId);
                $this->model3dService->createModel3dBackgroundJobs($model3d);
            }
        } catch (\Exception $e) {
            return $this->failedDownload($e);
        }

        return $this->redirect('/my/print-model3d?model3d=' . $model3d->getUid());
    }
}