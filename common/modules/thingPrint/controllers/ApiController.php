<?php
/**
 * Date: 24.03.17
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\thingPrint\controllers;
use common\models\HttpRequestLog;
use common\modules\thingPrint\components\ThingiverseWebhook;
use Yii;
use yii\helpers\VarDumper;

class ApiController extends \common\components\BaseController
{
    protected $detectLanguage = false;

    public function init()
    {
        parent::init();
        $this->enableCsrfValidation = false;
        $this->layout = '@common/modules/thingPrint/views/layout.php';
    }

    /**
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionWebhook()
    {
        app()->httpRequestLogger->initRequestLog(HttpRequestLog::REQUEST_TYPE_THINGIVERSE_HOOK);
        if(app('request')->isPost){
            $post = app('request')->post();
            $thingiverseWebhook = new ThingiverseWebhook();
            $thingiverseWebhook->run($post['event'], $post);
        }
    }

    public function actionLog()
    {
        $result = app('request')->post();
        // log
        $result['browser']  = $_SERVER['HTTP_USER_AGENT'];
        Yii::warning($result,'thingiverse');
        if(!empty($result['limit'])){
            $key = 'tslogapikey_'.$result['key'];
            $session = \Yii::$app->session;
            $limit = (int)$session->get($key, 0);
            $newLimit = $limit+1;
            $session->set($key, $newLimit);
            if($newLimit > (int)$result['limit']){
                return $this->jsonError('A communication error occurred. Please refresh the app or try again later.');
            }
        }
        return $this->jsonSuccess($result);
    }
}