<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.01.18
 * Time: 9:12
 */

namespace common\modules\printersList\components;

use common\modules\printersList\PrintersListModule;
use common\modules\printersList\services\TreeBuilder;
use common\modules\printersList\services\TreeRepository;
use yii\base\Module;

class PrintersTreeUpdater extends \yii\base\Component
{
    /**
     * @var PrintersListModule
     */
    protected $module;


    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    public function update()
    {
        $treeRepository = new TreeRepository();
        $printersTreeBuilder = new TreeBuilder();
        $printersTreeBuilder->init();
        $printersTree = $printersTreeBuilder->initTree();
        $treeRepository->saveToCache($printersTree);
    }

    public function asyncUpdate()
    {
        \Yii::$app->rabbitMq->sendMessage('treatstock.updatePrintersTree', []);
    }

}