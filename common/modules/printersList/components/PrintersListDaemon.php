<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.12.17
 * Time: 15:03
 */

namespace common\modules\printersList\components;

use common\modules\printersList\models\RequestInfo;
use common\modules\printersList\PrintersListModule;
use common\modules\printersList\services\PrintersProcessor;
use common\modules\printersList\services\RequestServer;
use Yii;
use yii\base\Module;


class PrintersListDaemon extends \yii\base\Component
{
    /**
     * @var PrintersProcessor
     */
    protected $printersProcessor;

    /**
     * @var PrintersListModule
     */
    protected $module;


    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    public function initPrintersProcessor()
    {
        $this->printersProcessor = new PrintersProcessor();
        $this->printersProcessor->init();
        return $this->printersProcessor;
    }


    public function listen()
    {
        $this->initPrintersProcessor();
        Yii::$app->getDb()->close();
        $requestListener = new RequestServer($this->module->port);
        $requestListener->listen($this->printersProcessor);
    }

    public function processRequest(RequestInfo $requestInfo)
    {
        return $this->printersProcessor->processRequest($requestInfo);
    }

    public function sendRequest(RequestInfo $requestInfo)
    {
        $requestServer = new RequestServer($this->module->port);
        $answer = $requestServer->sendRequest($requestInfo);
        return $answer;
    }

}