<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.12.17
 * Time: 12:20
 */

namespace common\modules\printersList\services;

use common\components\helpers\BenchmarkHelper;
use common\modules\printersList\models\RequestInfo;

class RequestServer
{
    public $port;



    public function __construct($port)
    {
        $this->port = $port;
    }

    public function listen(PrintersProcessor $printersProcessor)
    {
        $context = new \ZMQContext();
        //  Socket to talk to clients
        $responder = new \ZMQSocket($context, \ZMQ::SOCKET_REP);
        $responder->bind('tcp://127.0.0.1:' . $this->port);
        $this->debugOutput('Start listen.');
        $i = 0;
        while (true) {
            //  Wait for next request from client
            $this->debugOutput('Recv1');
            $requestString = $responder->recv();
            $this->debugOutput('Recv2');
            $startTime = microtime(true);
            /** @var RequestInfo $request */
            $request = igbinary_unserialize($requestString);
            if ($request->operationType === RequestInfo::OPERATION_TYPE_UPDATE_PRINTERS_TREE) {
                $printersProcessor->reloadPrintersTree();
                $responder->send(igbinary_serialize('loaded'));
                continue;
            }
            $firstVolume = reset($request->volumes);
            $this->debugOutput(
                'Iteration: ' . $i . ' Country: ' . $request->countryId . ' Size: ' . $request->size . ' Volume0: ' . $firstVolume->textureInfo->toStringGroupColor(
                ) . ' ' . $firstVolume->volume . ' ml.'
            );

            $answer = $printersProcessor->processRequest($request);
            $answerString = igbinary_serialize($answer);
            $responder->send($answerString, \ZMQ::MODE_DONTWAIT);
            $endTime = microtime(true);
            $this->debugOutput('Result: ' . count($answer->offers) . ' offers. Time: ' . round($endTime - $startTime, 5));
            $i++;
        }
    }

    public function sendRequest(RequestInfo $requestInfo)
    {
        $requestInfoString = igbinary_serialize($requestInfo);
        $context = new \ZMQContext();

        //  Socket to talk to server
        $requester = new \ZMQSocket($context, \ZMQ::SOCKET_REQ);
        BenchmarkHelper::logDebug('$requester->connect(\'tcp://127.0.0.1:\' . $this->port);');
        $requester->connect('tcp://127.0.0.1:' . $this->port);
        BenchmarkHelper::logDebug('$requester->send($requestInfoString);');
        $requester->send($requestInfoString, \ZMQ::MODE_DONTWAIT);
        BenchmarkHelper::logDebug('$answerString = $requester->recv();');
        $answerString = $requester->recv();
        BenchmarkHelper::logDebug('igbinary_unserialize($answerString);');
        return igbinary_unserialize($answerString);
    }

    protected function debugOutput($str)
    {
        echo "\n" . date('Y-m-d H:i:s') . ' ' . $str . ' Usage ' . round(memory_get_usage() / 1024 / 1024, 2) . ' mb.';
    }
}