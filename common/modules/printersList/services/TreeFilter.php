<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.12.17
 * Time: 10:09
 */

namespace common\modules\printersList\services;

use common\components\ps\locator\Size;
use common\models\DeliveryType;
use common\modules\printersList\models\PrinterItemInfo;
use common\modules\printersList\models\PrintersTree;
use common\modules\printersList\models\RequestVolumeInfo;
use common\modules\printersList\models\TextureInfo;

class TreeFilter
{
    public function search(PrintersTree $tree, string $condition, $checkPredicate)
    {
        if (!array_key_exists($condition, $tree->subTrees)) {
            $subTree                    = new PrintersTree();
            $tree->subTrees[$condition] = $subTree;
        } else {
            return $tree->subTrees[$condition];
        }

        foreach ($tree->printerItemList as $printerItem) {
            if ($checkPredicate($printerItem)) {
                $subTree->addPrinterInfo($printerItem);
            }
        }
        return $subTree;
    }

    public function domesticCountry(PrintersTree $tree, int $countryId): PrintersTree
    {
        return $this->search(
            $tree,
            'country=' . $countryId,
            function (PrinterItemInfo $printerItemInfo) use ($countryId) {
                if ($printerItemInfo->countryId == $countryId) {
                    return true;
                }
                return false;
            }
        );
    }

    public function carrierByMyself(PrintersTree $tree): PrintersTree
    {
        return $this->search(
            $tree,
            'carrier=myself',
            function (PrinterItemInfo $printerItemInfo) {
                if ($printerItemInfo->carrierBy === DeliveryType::CARRIER_MYSELF) {
                    return true;
                }
                return false;
            }
        );
    }

    public function internationalCountry(PrintersTree $tree): PrintersTree
    {
        return $this->search(
            $tree,
            'international=1',
            function (PrinterItemInfo $printerItemInfo) {
                if ($printerItemInfo->international) {
                    return true;
                }
                return false;
            }
        );
    }

    public function onlyCertificated($tree): PrintersTree
    {
        return $this->search(
            $tree,
            'certificated=1',
            function (PrinterItemInfo $printerItemInfo) {
                if ($printerItemInfo->certificated) {
                    return true;
                }
                return false;
            }
        );
    }

    public function bySize(PrintersTree $tree, Size $size): PrintersTree
    {
        return $this->search(
            $tree,
            'size=' . $size->toAlignedString(),
            function (PrinterItemInfo $printerItemInfo) use ($size) {
                if ($printerItemInfo->size->isMoreOrEqualThen($size)) {
                    return true;
                }
                return false;
            }
        );
    }

    public function byAllowedCurrency(PrintersTree $tree, $currencies)
    {
        return $this->search(
            $tree,
            'allowedCurrency=' . implode(',', $currencies),
            function (PrinterItemInfo $printerItemInfo) use ($currencies) {

                if (in_array(strtolower($printerItemInfo->currency), $currencies)) {
                    return true;
                }
                return false;
            }
        );
    }

    /**
     * @param PrintersTree $tree
     * @param array $allowedMaterials
     * @return PrintersTree
     */
    public function byAllowedMaterials(PrintersTree $tree, $allowedMaterials)
    {
        $condition = 'allowedMaterials=' . implode(',', $allowedMaterials);
        if (array_key_exists($condition, $tree->subTrees)) {
            return $tree->subTrees[$condition];
        }

        $subTree                    = new PrintersTree();
        $tree->subTrees[$condition] = $subTree;

        foreach ($tree->printerItemList as $printerItem) {
            $filteredTextures = [];
            foreach ($printerItem->textures as $texture) {
                if (array_key_exists($texture->textureInfo->materialId, $allowedMaterials)) {
                    $filteredTextures[$texture->textureInfo->toStringMaterialColor()] = $texture;
                }
            }
            if ($filteredTextures) {
                if (count($filteredTextures) !== count($printerItem->textures)) {
                    // new PrinterItem object must be created
                    $printerItemClone                           = clone $printerItem;
                    $printerItemClone->textures                 = $filteredTextures;
                    $printerItemClone->cheapestItemsTextureInfo = null;
                    $printerItemClone->calcCheapestTextures();
                } else {
                    $printerItemClone = $printerItem;
                }
                $subTree->addPrinterInfo($printerItemClone);
            }
        }
        return $subTree;
    }

    /**
     * @param PrintersTree $tree
     * @param RequestVolumeInfo[] $volumesInfo
     * @return PrintersTree
     */
    public function byAllTexturesInList(PrintersTree $tree, array $volumesInfo): PrintersTree
    {
        $texturesInfoKey = '';
        foreach ($volumesInfo as $volumeInfo) {
            if ($volumeInfo->textureInfo->materialId) {
                $texturesInfoKey .= $volumeInfo->textureInfo->toStringMaterialColor() . ',';
            } else {
                $texturesInfoKey .= $volumeInfo->textureInfo->toStringGroupColor() . ',';
            }
        }
        return $this->search(
            $tree,
            'allTexture=' . $texturesInfoKey,
            function (PrinterItemInfo $printerItemInfo) use ($volumesInfo) {
                return $printerItemInfo->canPrintTextures($volumesInfo);
            }
        );
    }
}