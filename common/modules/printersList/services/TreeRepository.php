<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.01.18
 * Time: 17:16
 */

namespace common\modules\printersList\services;

use common\modules\printersList\models\PrintersTree;

class TreeRepository
{
    public function getFromCache()
    {
        $printersTreeSerialized = \Yii::$app->redis->get('printersTree');
        if ($printersTreeSerialized) {
            $printersTree = igbinary_unserialize($printersTreeSerialized);
            return $printersTree;
        }
        return null;
    }

    public function saveToCache(PrintersTree $printersTree)
    {
        $printersTreeSerialized = igbinary_serialize($printersTree);
        \Yii::$app->redis->set('printersTree', $printersTreeSerialized);
    }
}