<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.12.17
 * Time: 12:05
 */

namespace common\modules\printersList\services\sortStrategy;

use common\models\CompanyService;
use common\models\PsPrinter;
use common\modules\printersList\models\OfferItemInfo;
use common\modules\printersList\models\RequestInfo;
use lib\money\Currency;
use lib\money\Money;

class DefaultCoefficient
{
    public function formCoefficient(OfferItemInfo $offerItemInfo, RequestInfo $requestInfo)
    {
        $ratingCoef         = $offerItemInfo->printerItemInfo->rating;

        // International order, most cost of in delivery
        $latDiff             = ($offerItemInfo->printerItemInfo->locationLat - $requestInfo->locationLat);
        $lonDiff             = $offerItemInfo->printerItemInfo->locationLon - $requestInfo->locationLon;
        $distance            = sqrt($latDiff * $latDiff + $lonDiff * $lonDiff);
        $distanceCoefficient = 1 + $distance * 800;

        return -$ratingCoef + $distanceCoefficient;
    }
}