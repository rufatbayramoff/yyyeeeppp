<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.12.17
 * Time: 12:05
 */

namespace common\modules\printersList\services\sortStrategy;

use common\models\PsPrinter;
use common\modules\printersList\models\OfferItemInfo;
use common\modules\printersList\models\RequestInfo;

class ReviewFormulaCoefficient
{
    public function formCoefficient(OfferItemInfo $offerItemInfo, RequestInfo $requestInfo)
    {
        $rating = $offerItemInfo->printerItemInfo->rating;
        return -$rating;
    }
}