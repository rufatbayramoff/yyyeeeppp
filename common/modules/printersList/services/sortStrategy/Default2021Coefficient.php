<?php


namespace common\modules\printersList\services\sortStrategy;


use common\modules\printersList\models\OfferItemInfo;
use common\modules\printersList\models\RequestInfo;
use lib\money\Currency;

class Default2021Coefficient
{
    public function formCoefficient(OfferItemInfo $offerItemInfo, RequestInfo $requestInfo)
    {
        $costCoef       = $offerItemInfo->price->convertTo(Currency::USD)->getAmount();
        $rating         = $offerItemInfo->printerItemInfo->reviewsRating;
        $isCertificated = $offerItemInfo->printerItemInfo->isCertificated;
        $ordersCount    = $offerItemInfo->printerItemInfo->ordersCount;


        if ($offerItemInfo->printerItemInfo->countryId === 0) {
            // International order, most cost of in delivery
            $latDiff             = ($offerItemInfo->printerItemInfo->locationLat - $requestInfo->locationLat);
            $lonDiff             = $offerItemInfo->printerItemInfo->locationLon - $requestInfo->locationLon;
            $distance            = sqrt($latDiff * $latDiff + $lonDiff * $lonDiff);
            $distanceCoefficient = 1 + $distance * 5.3;
        } else {
            $distanceCoefficient = 1;
        }

        // Forming coefficients
        $ratingCoef = 1;
        if ($rating >= 0 && $rating < 3) {
            $ratingCoef = 1.3;
        }
        if ($rating >= 3 && $rating < 4) {
            $ratingCoef = 1.1;
        }

        $certificationCoef = 3;
        if ($isCertificated) {
            $certificationCoef = 1;
        }

        $ordersCountCoef = 1;
        if ($ordersCount < 10) {
            $ordersCountCoef = 1.3;
        } elseif ($ordersCount >= 10 && $ordersCount < 20) {
            $ordersCountCoef = 1.2;
        } elseif ($ordersCount >= 20 && $ordersCount < 30) {
            $ordersCountCoef = 1.1;
        } elseif ($ordersCount >= 30 && $ordersCount < 40) {
            $ordersCountCoef = 1.05;
        }

        return $costCoef * $ratingCoef * $certificationCoef * $ordersCountCoef * $distanceCoefficient;
    }
}