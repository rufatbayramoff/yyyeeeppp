<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.12.17
 * Time: 14:00
 */

namespace common\modules\printersList\services\sortStrategy;

use common\modules\printersList\models\OfferItemInfo;
use common\modules\printersList\models\RequestInfo;
use lib\money\Currency;

class CheapestCoefficient
{
    public function formCoefficient(OfferItemInfo $offerItemInfo, RequestInfo $requestInfo)
    {
        if (!$offerItemInfo->price || !$offerItemInfo->price->getAmount()) {
            return 99999999999;
        }
        return $offerItemInfo->price->convertTo(Currency::USD)->getAmount();
    }
}