<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.12.17
 * Time: 14:01
 */

namespace common\modules\printersList\services\sortStrategy;

use common\modules\printersList\models\OfferItemInfo;
use common\modules\printersList\models\RequestInfo;

class LessDistanceCoefficient
{
    public function formCoefficient(OfferItemInfo $offerItemInfo, RequestInfo $requestInfo)
    {
        $latDiff = ($offerItemInfo->printerItemInfo->locationLat - $requestInfo->locationLat);
        $lonDiff = $offerItemInfo->printerItemInfo->locationLon - $requestInfo->locationLon;
        return sqrt($latDiff * $latDiff + $lonDiff * $lonDiff);
    }
}