<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.12.17
 * Time: 15:06
 */

namespace common\modules\printersList\services;

use common\components\BaseActiveQuery;
use common\components\PaymentExchangeRateFacade;
use common\components\ps\locator\PrinterRepository;
use common\components\ps\locator\Size;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\PsMachineDelivery;
use common\models\PsPrinter;
use common\models\StoreOrderAttemp;
use common\modules\printersList\models\PrinterItemInfo;
use common\modules\printersList\models\PrinterItemTextureInfo;
use common\modules\printersList\models\PrintersTree;
use common\modules\printersList\models\TextureInfo;
use common\modules\printersList\repositories\DeliveryRepository;
use common\services\ReviewService;
use lib\money\Currency;
use Yii;

class TreeBuilder
{
    /**
     * @var ReviewService
     */
    protected $reviewService;

    /**
     * @var DeliveryRepository
     */
    protected $deliveryRepository;

    public function init()
    {
        $this->reviewService      = Yii::createObject(ReviewService::class);
        $this->deliveryRepository = Yii::createObject(DeliveryRepository::class);
    }


    public function getCompletedOrdersForPsMachine(CompanyService $psMachine)
    {
        $count = StoreOrderAttemp::find()
            ->forMachineId($psMachine->id)
            ->inStatus(StoreOrderAttemp::STATUS_RECEIVED)
            ->withoutStaticCache()
            ->count();

        return $count;
    }


    public function buildRoot()
    {
        // Total disable cache
        $beforeCacheState                         = BaseActiveQuery::$staticCacheGlobalEnable;
        BaseActiveQuery::$staticCacheGlobalEnable = false;

        /** @var PsPrinter[] $domesticDeliveryPrinters */
        $domesticDeliveryPrinters = PrinterRepository::getPublicActive()->all();

        $tree = new PrintersTree();

        foreach ($domesticDeliveryPrinters as $onePrinter) {
            if (!$onePrinter->ps) {
                continue;
            }
            if ($onePrinter->isFilteredByMaxOrdersCount()) {
                continue;
            }
            if (!$onePrinter->companyService->hasDomesticDelivery() && !$onePrinter->companyService->hasInternationalDelivery()) {
                continue;
            }

            $printerItemInfo                  = new PrinterItemInfo();
            $printerItemInfo->psId            = $onePrinter->ps->id;
            $printerItemInfo->printerId       = $onePrinter->id;
            $printerItemInfo->countryId       = $onePrinter->companyService->location->country->id;
            $printerItemInfo->currency        = $onePrinter->companyService->company->currency;
            $printerItemInfo->minPrice        = (float)$onePrinter->min_order_price;
            $printerItemInfo->cashbackPercent = (float)$onePrinter->companyService->company->cashback_percent;
            $printerItemInfo->rating          = (int)$onePrinter->company?->psCatalog?->rating;
            $size                             = $onePrinter->getBuildVolume();
            $sizeObj                          = Size::create($size[0], $size[1], $size[2]);
            $printerItemInfo->size            = $sizeObj;
            $printerItemInfo->certificated    = (int)$onePrinter->isCertificated();
            [
                DeliveryType::INTERNATIONAL => $printerItemInfo->deliveryInternational,
                DeliveryType::STANDARD      => $printerItemInfo->deliveryStandard
            ] = $this->deliveryRepository->packageFee($onePrinter->id);
            $textures = [];

            foreach ($onePrinter->materials as $psPrinterMaterial) {
                foreach ($psPrinterMaterial->psPrinterColors as $psPrinterColor) {
                    if (!$psPrinterMaterial->material->group_id) {
                        continue;
                    }
                    if (!$psPrinterMaterial->material->group->is_active) {
                        continue;
                    }
                    $printerItemTextureInfo                          = new PrinterItemTextureInfo();
                    $textureInfo                                     = new TextureInfo();
                    $textureInfo->materialGroupId                    = $psPrinterMaterial->material->group_id;
                    $textureInfo->materialId                         = $psPrinterMaterial->material->id;
                    $textureInfo->materialColorId                    = $psPrinterColor->color_id;
                    $printerItemTextureInfo->textureInfo             = $textureInfo;
                    $printerItemTextureInfo->pricePerMl              = round($psPrinterColor->getPricePerMl()->getAmount(),2);
                    $textures[$textureInfo->toStringMaterialColor()] = $printerItemTextureInfo;
                }
            }
            $printerItemInfo->textures = $textures;

            $standardCarrier = $onePrinter->companyService->getPsDeliveryTypeByCode(DeliveryType::STANDARD)->carrier ?? '';
            $intlCarrier     = $onePrinter->companyService->getPsDeliveryTypeByCode(DeliveryType::INTERNATIONAL)->carrier ?? '';
            if ((!$intlCarrier && $standardCarrier === DeliveryType::CARRIER_MYSELF) ||
                (($intlCarrier === DeliveryType::CARRIER_MYSELF) || ($standardCarrier === DeliveryType::CARRIER_MYSELF))) {
                $printerItemInfo->carrierBy = DeliveryType::CARRIER_MYSELF;
            }
            if ((!$intlCarrier && $standardCarrier === DeliveryType::CARRIER_TS) ||
                (($intlCarrier === DeliveryType::CARRIER_TS) || ($standardCarrier === DeliveryType::CARRIER_TS))) {
                $printerItemInfo->carrierBy = DeliveryType::CARRIER_TS;
            }
            $printerItemInfo->locationLat            = $onePrinter->companyService->location->lat;
            $printerItemInfo->locationLon            = $onePrinter->companyService->location->lon;
            $printerItemInfo->reviewsRating          = $onePrinter->ps->psCatalog->rating_avg ?? 0;
            $printerItemInfo->isCertificated         = $onePrinter->companyService->isCertificated();
            $printerItemInfo->ordersCount            = $this->getCompletedOrdersForPsMachine($onePrinter->companyService);
            $printerItemInfo->international          = $onePrinter->companyService->hasInternationalDelivery();
            $printerItemInfo->calcCheapestTextures();

            $tree->addPrinterInfo($printerItemInfo);
        }
        BaseActiveQuery::$staticCacheGlobalEnable = $beforeCacheState;
        return $tree;
    }

    protected function formCountriesTreeItems(PrintersTree $tree)
    {
        foreach ($tree->printerItemList as $printerItem) {
            $condition = 'country=' . $printerItem->countryId;
            if (!array_key_exists($condition, $tree->subTrees)) {
                $tree->subTrees[$condition] = new PrintersTree();
            }
            $tree->subTrees[$condition]->addPrinterInfo($printerItem);
        }
    }

    public function initTree()
    {
        $tree = $this->buildRoot();
        $this->formCountriesTreeItems($tree);
        return $tree;
    }
}