<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.12.17
 * Time: 17:33
 */

namespace common\modules\printersList\services;


use common\models\GeoCountry;
use common\modules\payment\components\BonusCalculator;
use common\modules\payment\fee\FeeHelper;
use common\modules\printersList\models\AnswerColorInfo;
use common\modules\printersList\models\AnswerInfo;
use common\modules\printersList\models\OfferItemInfo;
use common\modules\printersList\models\OfferItemPrinterGrouped;
use common\modules\printersList\models\PrinterItemInfo;
use common\modules\printersList\models\PrintersTree;
use common\modules\printersList\models\RequestInfo;
use common\modules\printersList\models\TextureInfo;
use common\modules\printersList\services\sortStrategy\CheapestCoefficient;
use common\modules\printersList\services\sortStrategy\DefaultCoefficient;
use common\modules\printersList\services\sortStrategy\LessDistanceCoefficient;
use common\modules\printersList\services\sortStrategy\Default2021Coefficient;
use common\modules\printersList\services\sortStrategy\ReviewFormulaCoefficient;
use lib\delivery\carrier\vendors\easypost\EasyPost;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;

class PrintersProcessor
{

    /** @var PrintersTree */
    protected $printersTree;

    /** @var array */
    protected $offersCache = [];

    /** @var TreeFilter */
    protected $treeFilter;

    /** @var FeeHelper */
    protected $feeHelper;

    /** @var TreeRepository */
    protected $treeRepository;

    /** @var DefaultCoefficient[] */
    protected $coefficientProcessors = [];

    protected function debugOutput($str)
    {
        echo "\n" . date('Y-m-d H:i:s') . ' ' . $str . ' Usage ' . round(memory_get_usage() / 1024 / 1024, 2) . ' mb.';
    }

    public function init()
    {
        $this->debugOutput('Processor start.');
        $this->initCoefficienProcessors();
        $this->treeRepository = new TreeRepository();
        $this->printersTree   = $this->treeRepository->getFromCache();
        if (!$this->printersTree) {
            $printersTreeBuilder = new TreeBuilder();
            $printersTreeBuilder->init();
            $this->printersTree = $printersTreeBuilder->initTree();
            $this->treeRepository->saveToCache($this->printersTree);
        }
        $this->debugOutput('Tree inited, elements count: ' . count($this->printersTree->printerItemList) . '.');

        $this->feeHelper  = FeeHelper::init();
        $this->treeFilter = new TreeFilter();
    }


    public function reloadPrintersTree()
    {
        $this->printersTree = $this->treeRepository->getFromCache();
        $this->debugOutput('Tree reloated, elements count: ' . count($this->printersTree->printerItemList) . '.');
    }

    protected function initCoefficienProcessors()
    {
        $this->coefficientProcessors[RequestInfo::SORT_STRATEGY_CHEAPEST]     = new CheapestCoefficient();
        $this->coefficientProcessors[RequestInfo::SORT_STRATEGY_DEFAULT]      = new DefaultCoefficient();
        $this->coefficientProcessors[RequestInfo::SORT_STRATEGY_DISTANCE]     = new LessDistanceCoefficient();
        $this->coefficientProcessors[RequestInfo::SORT_STRATEGY_RATING]       = new ReviewFormulaCoefficient();
        $this->coefficientProcessors[RequestInfo::SORT_STRATEGY_2021_DEFAULT] = new Default2021Coefficient();
    }

    protected function formCost(PrinterItemInfo $printerItemInfo, RequestInfo $request, OfferItemInfo $offerItemInfo)
    {
        $volumeCost       = 0;
        $priceTextureInfo = [];
        foreach ($request->volumes as $volumeKey => $volumeInfo) {
            // Search cheapest material in printerItemInfo
            $cheapestItemTextureInfo = null;
            if ($volumeInfo->textureInfo->materialId) {
                $textureKey = $volumeInfo->textureInfo->toStringMaterialColor();
                $volumeCost += $volumeInfo->volume * $printerItemInfo->textures[$textureKey]->pricePerMl;
            } else {
                $cheapestItemTextureInfoKey   = $volumeInfo->textureInfo->toStringGroupColor();
                $cheapestItemTextureInfo      = $printerItemInfo->cheapestItemsTextureInfo[$cheapestItemTextureInfoKey];
                $priceTextureInfo[$volumeKey] = $cheapestItemTextureInfo->textureInfo;
                $volumeCost                   += $volumeInfo->volume * $cheapestItemTextureInfo->pricePerMl;
            }
        }

        if ($request->onlyInternational) {
            $packageFee = $printerItemInfo->deliveryInternational ? Money::create($printerItemInfo->deliveryInternational, $printerItemInfo->currency) : Money::zero($printerItemInfo->currency);
        } else {
            $packageFee = $printerItemInfo->deliveryStandard ? Money::create($printerItemInfo->deliveryStandard, $printerItemInfo->currency) : Money::zero($printerItemInfo->currency);
        }
        $priceWithoutMin = Money::create($volumeCost, $printerItemInfo->currency);
        $totalCost       = Money::create(max($printerItemInfo->minPrice, $priceWithoutMin->getAmount()), $priceWithoutMin->getCurrency());
        $priceWithoutMin = MoneyMath::sum($priceWithoutMin, $packageFee);
        $totalCost       = MoneyMath::sum($totalCost, $packageFee);
        $priceWithoutMin = MoneyMath::sum($priceWithoutMin, $this->feeHelper->getTsCommonFee($priceWithoutMin));
        $totalCost       = MoneyMath::sum($totalCost, $this->feeHelper->getTsCommonFee($totalCost));

        if ($priceWithoutMin->getAmount() < 0.01) {
            $priceWithoutMin = Money::create(0.01, $printerItemInfo->currency);
        }
        if ($totalCost->getAmount() < 0.01) {
            $totalCost = Money::create(0.01, $printerItemInfo->currency);
        }
        $priceWithoutMin = $priceWithoutMin->round();
        $totalCost       = $totalCost->round();

        $offerItemInfo->price            = $totalCost;
        $offerItemInfo->tsBonusAmount    = BonusCalculator::calcInstantOrder($printerItemInfo->cashbackPercent, $totalCost->getAmount());
        $offerItemInfo->priceWithoutMin  = $priceWithoutMin;
        $offerItemInfo->priceTextureInfo = $priceTextureInfo;
    }

    protected function calcCoefficient(OfferItemInfo $offer, RequestInfo $request)
    {
        /** @var DefaultCoefficient $coefficientProcessor */
        $coefficientProcessor = $this->coefficientProcessors[$request->sort];
        return $coefficientProcessor->formCoefficient($offer, $request);
    }

    /**
     * @param PrinterItemInfo[] $printerItemList
     * @param RequestInfo $request
     * @return OfferItemInfo[]
     */
    protected function formOffers($printerItemList, $request)
    {
        /** @var OfferItemInfo[] $offers */
        $offers = [];
        foreach ($printerItemList as $printerItemInfo) {
            $offer = new OfferItemInfo();
            $this->formCost($printerItemInfo, $request, $offer);
            $offer->printerItemInfo = $printerItemInfo;
            $offer->coefficient     = $this->calcCoefficient($offer, $request);
            $offers[]               = $offer;
        }

        // Sorting
        usort(
            $offers,
            function (OfferItemInfo $a, OfferItemInfo $b) {
                return $a->coefficient <=> $b->coefficient;
            }
        );

        // Group by ps
        $groupByPsShortOffersList = [];
        foreach ($offers as $oneOfferItem) {
            if (!array_key_exists($oneOfferItem->printerItemInfo->psId, $groupByPsShortOffersList)) {
                $shortOfferInfo = new OfferItemPrinterGrouped();
                $shortOfferInfo->initByOfferItem($oneOfferItem);
                $groupByPsShortOffersList[$oneOfferItem->printerItemInfo->psId] = $shortOfferInfo;
            }
        }
        return $groupByPsShortOffersList;
    }

    /**
     * @param PrintersTree $printersTree
     */
    protected function formAvailableMaterials(PrintersTree $printersTree): void
    {
        if ($printersTree->availableMaterials) {
            return;
        }
        $availableMaterials = [];
        foreach ($printersTree->printerItemList as $printerItemInfo) {
            foreach ($printerItemInfo->textures as $textures) {
                $materialColorKey                      = $textures->textureInfo->toStringMaterialColor();
                $availableMaterials[$materialColorKey] = $materialColorKey;
            }
        }
        $printersTree->availableMaterials = array_values($availableMaterials);
    }

    /**
     * @param PrintersTree $printersTree
     */
    protected function formAvailableMaterialGroupsAndColors(PrintersTree $printersTree): void
    {
        if ($printersTree->availableMaterialsAndColors) {
            return;
        }
        $availableMaterialsAndColors = [];
        foreach ($printersTree->printerItemList as $printerItemInfo) {
            foreach ($printerItemInfo->textures as $textureMaterialKey => $texture) {
                $textureInfo = $texture->textureInfo;
                if (array_key_exists($textureMaterialKey, $availableMaterialsAndColors)) {
                    $answerInfo = $availableMaterialsAndColors[$textureMaterialKey];
                    $answerInfo->rating++;
                } else {
                    $answerInfo                                       = new AnswerColorInfo();
                    $answerInfo->materialId                           = $textureInfo->materialId;
                    $answerInfo->colorId                              = $textureInfo->materialColorId;
                    $answerInfo->rating                               = 1;
                    $availableMaterialsAndColors[$textureMaterialKey] = $answerInfo;
                }
                $textureGroupKey = $texture->textureInfo->toStringGroupColor();
                if (array_key_exists($textureGroupKey, $availableMaterialsAndColors)) {
                    $answerInfo = $availableMaterialsAndColors[$textureGroupKey];
                    $answerInfo->rating++;
                } else {
                    $answerInfo                                    = new AnswerColorInfo();
                    $answerInfo->groupId                           = $textureInfo->materialGroupId;
                    $answerInfo->colorId                           = $textureInfo->materialColorId;
                    $answerInfo->rating                            = 1;
                    $availableMaterialsAndColors[$textureGroupKey] = $answerInfo;
                }
            }
        }
        $printersTree->availableMaterialsAndColors = $availableMaterialsAndColors;
    }

    /**
     *
     * @param PrintersTree $printersTree
     * @return TextureInfo
     */
    protected function calculateCheapestTexture(PrintersTree $printersTree): ?TextureInfo
    {
        $cheapestCost            = null;
        $cheapestItemTextureInfo = null;

        foreach ($printersTree->printerItemList as $printerItemInfo) {
            if ($cheapestCost === null) {
                $cheapestCost            = $printerItemInfo->cheapestItemTextureCost;
                $cheapestItemTextureInfo = $printerItemInfo->cheapestItemTextureInfo;
            }
            if ($cheapestCost > $printerItemInfo->cheapestItemTextureCost) {
                $cheapestCost            = $printerItemInfo->cheapestItemTextureCost;
                $cheapestItemTextureInfo = $printerItemInfo->cheapestItemTextureInfo;
            }
        }
        return $cheapestItemTextureInfo ? $cheapestItemTextureInfo->textureInfo : null;
    }

    /**
     * @param RequestInfo $request
     * @return bool
     */
    protected function isFilteredByMaterial(RequestInfo $request): bool
    {
        foreach ($request->volumes as $volume) {
            if ($volume->textureInfo->materialId) {
                return true;
            }
        }
        return false;
    }

    public function processRequest(RequestInfo $request)
    {
        $answerInfo = new AnswerInfo();

        if ($request->onlyInternational) {
            $currentTree = $this->treeFilter->internationalCountry($this->printersTree);
            if ($request->onlyCertificated) {
                $currentTree = $this->treeFilter->onlyCertificated($currentTree);
            }
        } else {
            $currentTree = $this->treeFilter->domesticCountry($this->printersTree, $request->countryId);
            if ($request->onlyCertificated) {
                $currentTree = $this->treeFilter->onlyCertificated($currentTree);
            }
        }

        if ($request->currencies) {
            $currentTree = $this->treeFilter->byAllowedCurrency($currentTree, $request->currencies);
        }

        if (!$currentTree->printerItemList) {
            $answerInfo->emptyReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_NO_ACTIVE_MACHINES;
            return $answerInfo;
        }

        $filteredBySizeTree = $currentTree = $this->treeFilter->bySize($currentTree, $request->size);
        if (!$currentTree->printerItemList) {
            $answerInfo->emptyReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_LARGE_SIZE;
            return $answerInfo;
        }

        if ($request->allowedMaterials) {
            $currentTree = $this->treeFilter->byAllowedMaterials($currentTree, $request->allowedMaterials);
            if (!$currentTree->printerItemList) {
                $answerInfo->emptyReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_NO_TEXTURE;
                return $answerInfo;
            }
        }

        if ($request->getTotalVolume() > EasyPost::MAX_EASYPOST_DELIVERY_VOLUME) {
            $currentTree = $this->treeFilter->carrierByMyself($currentTree);
        }

        // Form groups and colors
        $this->formAvailableMaterialGroupsAndColors($currentTree);
        $answerInfo->allowedMaterialColors = $currentTree->availableMaterialsAndColors;

        $currentTree = $this->treeFilter->byAllTexturesInList($currentTree, $request->volumes);
        if (!$currentTree->printerItemList) {
            if (!$request->allowResetTexture) {
                $answerInfo->emptyReason = AnswerInfo::EMPTY_OFFERS_LIST_REASON_NO_TEXTURE;
                return $answerInfo;
            }
            $cheapestTexture = $this->calculateCheapestTexture($filteredBySizeTree);
            $request->setKitTexture($cheapestTexture);
            $currentTree                  = $this->treeFilter->byAllTexturesInList($filteredBySizeTree, $request->volumes);
            $answerInfo->resetTextureInfo = $cheapestTexture;
        }
        $resultPrintersList = $currentTree->printerItemList;
        $offers             = $this->formOffers($resultPrintersList, $request);
        $answerInfo->offers = $offers;
        return $answerInfo;
    }
}