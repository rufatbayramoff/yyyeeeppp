<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.12.17
 * Time: 9:42
 */

namespace common\modules\printersList\services;

use common\components\ps\locator\Size;
use common\interfaces\Model3dBaseInterface;
use common\models\PrinterMaterial;
use common\modules\printersList\models\RequestInfo;
use common\modules\printersList\models\RequestVolumeInfo;
use common\modules\printersList\models\TextureInfo;
use common\modules\psPrinterListMaps\components\FilepageSizeRepository;
use lib\geo\models\Location;
use Yii;


class RequestInfoService
{

    /**
     * @param Model3dBaseInterface $model3d
     * @param Location $location
     * @param $onlyCertificated
     * @param PrinterMaterial[] $allowedMaterials
     * @return RequestInfo
     */
    public function createRequestForModel3d(Model3dBaseInterface $model3d, Location $location, $onlyCertificated, $allowedMaterials, $currencies)
    {
        $requestInfo = new RequestInfo();
        $requestInfo->countryId = $location->geoCountry->id;
        $requestInfo->locationLat = $location->lat;
        $requestInfo->locationLon = $location->lon;
        $sizeModel3dOriginal = $model3d->getSize();
        $filepageSizeRepository = Yii::createObject(FilepageSizeRepository::class);
        $filepageSize = $filepageSizeRepository->getNearestSize($sizeModel3dOriginal);
        $requestInfo->size = $filepageSize->toSize();
        $requestInfo->onlyCertificated = $onlyCertificated ? 1 : 0;
        $requestInfo->currencies = $currencies;
        if ($model3d->isOneTextureForKit()) {
            $modelVolume = $model3d->getVolume();
            $texture = $model3d->getKitTexture();
            $textureInfo = new TextureInfo();
            $textureInfo->materialColorId = $texture->printerColor->id;
            if ($texture->printerMaterial) {
                $textureInfo->materialId = $texture->printerMaterial->id;
            }
            $textureInfo->materialGroupId = $texture->calculatePrinterMaterialGroup()->id;
            $requestVolumeInfo = new RequestVolumeInfo();
            $requestVolumeInfo->textureInfo = $textureInfo;
            $requestVolumeInfo->volume = $modelVolume;
            $requestInfo->volumes[0] = $requestVolumeInfo;
        } else {
            foreach ($model3d->getActiveModel3dParts() as $part) {
                if ($part->qty < 1) {
                    continue;
                }
                $textureInfo = new TextureInfo();
                $texture = $part->getTexture();
                $textureInfo->materialColorId = $texture->printerColor->id;
                if ($texture->printerMaterial) {
                    $textureInfo->materialId = $texture->printerMaterial->id;
                }
                $textureInfo->materialGroupId = $texture->calculatePrinterMaterialGroup()->id;
                $requestVolumeInfo = new RequestVolumeInfo();
                $requestVolumeInfo->textureInfo = $textureInfo;
                $requestVolumeInfo->volume = $part->getVolume();
                $requestInfo->volumes[$part->id] = $requestVolumeInfo;
            }
        }

        $requestInfo->allowedMaterials = [];
        foreach ($allowedMaterials as $allowedMaterial) {
            $requestInfo->allowedMaterials[$allowedMaterial->id] = $allowedMaterial->id;
        }
        $requestInfo->allowResetTexture = false;
        if ($model3d->isAnyTextureAllowed()) {
            $requestInfo->allowResetTexture = true;
        }

        return $requestInfo;
    }

    public function formTestRequest()
    {
        $requestInfo = new RequestInfo();
        $requestInfo->countryId = 233;
        $requestInfo->size = Size::create(20, 30, 10);
        $requestInfo->onlyCertificated = 0;

        $textureInfo = new TextureInfo();
        $textureInfo->materialGroupId = 16;
        $textureInfo->materialColorId = 90;

        $requestVolumeInfo = new RequestVolumeInfo();
        $requestVolumeInfo->textureInfo = $textureInfo;
        $requestVolumeInfo->volume = 9.5;
        $requestInfo->volumes[] = $requestVolumeInfo;

        return $requestInfo;
    }
}