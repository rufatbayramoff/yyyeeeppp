<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.12.17
 * Time: 12:20
 */

namespace common\modules\printersList\services;

use common\components\helpers\BenchmarkHelper;
use common\modules\printersList\models\RequestInfo;
use yii\base\InvalidCallException;

class RequestHttpServer
{
    public $port;


    public function __construct($port)
    {
        $this->port = $port;
    }

    /**
     * @param PrintersProcessor $printersProcessor
     */
    public function listen(PrintersProcessor $printersProcessor)
    {
        if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            $this->debugOutput('Can`t socket_create(): reason: ' . socket_strerror(socket_last_error()));
        }

        if (!socket_set_option($sock, SOL_SOCKET, SO_REUSEADDR, 1)) {
            $this->debugOutput('Can`t set option SO_REUSEADDR: ' . socket_strerror(socket_last_error()));
        }

        if (socket_bind($sock, '127.0.0.1', $this->port) === false) {
            $this->debugOutput('Can`t socket_bind(): reason: ' . socket_strerror(socket_last_error($sock)));
        }

        if (socket_listen($sock, 5) === false) {
            $this->debugOutput('Can`t socket_listen(): reason: ' . socket_strerror(socket_last_error($sock)));
        }

        $i = 1;
        do {
            if (($msgsock = socket_accept($sock)) === false) {
                $this->debugOutput('Can`t socket_accept(): reason: ' . socket_strerror(socket_last_error($sock)));
                break;
            }

            if (false === socket_recv($msgsock, $buff, 8, MSG_WAITALL)) {
                $this->debugOutput('Can`t read packet size');
                break;
            }
            $packetSize = unpack('q', $buff)[1];

            if (false === socket_recv($msgsock, $requestString, $packetSize, MSG_WAITALL)) {
                $this->debugOutput('Can`t read incoming packet');
                break;
            }

            $startTime = microtime(true);
            /** @var RequestInfo $request */
            $request = igbinary_unserialize($requestString);
            if ($request->operationType === RequestInfo::OPERATION_TYPE_UPDATE_PRINTERS_TREE) {
                $printersProcessor->reloadPrintersTree();
                $serialized = igbinary_serialize('loaded');
                $packetSize = strlen($serialized);
                socket_write($msgsock, pack('q', $packetSize), 8);
                socket_write($msgsock, $serialized, $packetSize);
                socket_close($msgsock);
                continue;
            }

            $firstVolume = reset($request->volumes);
            $this->debugOutput(
                'Iteration: ' . $i . ' Country: ' . $request->countryId . ' Size: ' . $request->size . ' Volume0: ' . $firstVolume->textureInfo->toStringGroupColor() . ' ' . $firstVolume->volume . ' ml.'
            );

            $answer = $printersProcessor->processRequest($request);
            $answerString = igbinary_serialize($answer);
            $packetSize = strlen($answerString);
            socket_write($msgsock, pack('q', $packetSize), 8);
            socket_write($msgsock, $answerString, $packetSize);
            $endTime = microtime(true);
            $this->debugOutput('Result: ' . count($answer->offers) . ' offers. Time: ' . round($endTime - $startTime, 5));
            $i++;
            socket_close($msgsock);
        } while (true);
        socket_close($sock);
    }

    public function sendRequest(RequestInfo $requestInfo)
    {
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_connect($socket, '127.0.0.1', $this->port);
        $requestInfoString = igbinary_serialize($requestInfo);
        $packetSize = strlen($requestInfoString);

        socket_write($socket, pack("q", $packetSize), 8);
        socket_write($socket, $requestInfoString, $packetSize);

        if (false === socket_recv($socket, $buf, 8, MSG_WAITALL)) {
            $this->debugOutput('Can`t read packet size');
            throw new InvalidCallException('Cant read from printers list server');
        }
        $packetSize = unpack('q', $buf)[1];
        if (false === socket_recv($socket, $answerString, $packetSize, MSG_WAITALL)) {
            throw new InvalidCallException('Cant read from printers list server');
        }
        if (!$answerString) {
            throw new InvalidCallException('Empty answer from printers list server');
        }
        socket_close($socket);
        return igbinary_unserialize($answerString);
    }

    protected function debugOutput($str)
    {
        echo "\n" . date('Y-m-d H:i:s') . ' ' . $str . ' Usage ' . round(memory_get_usage() / 1024 / 1024, 2) . ' mb.';
    }
}