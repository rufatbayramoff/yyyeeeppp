<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.12.17
 * Time: 18:01
 */

namespace common\modules\printersList\models;

class AnswerInfo
{
    public const EMPTY_OFFERS_LIST_NO_CUTTING_PARTS          = 'no_cutting_parts';
    public const EMPTY_OFFERS_LIST_REASON_NO_ACTIVE_MACHINES = 'no_active_machines';
    public const EMPTY_OFFERS_LIST_REASON_LARGE_SIZE         = 'large_size';
    public const EMPTY_OFFERS_LIST_REASON_NO_DELIVERY        = 'no_delivery';
    public const EMPTY_OFFERS_LIST_REASON_NO_TEXTURE         = 'no_texture';

    public $emptyReason = '';

    /** @var OfferItemPrinterGrouped[] */
    public $offers = [];

    /**
     * List of allowed material before filter by allowed materials
     *
     * @var array
     */
    public $availableMaterials = [];

    /** @var AnswerColorInfo[] */
    public $allowedMaterialColors = [];

    /** @var TextureInfo */
    public $resetTextureInfo;
}