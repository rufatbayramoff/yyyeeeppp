<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.12.17
 * Time: 15:10
 */

namespace common\modules\printersList\models;


use common\components\ps\locator\Size;
use common\models\DeliveryType;
use common\models\PrinterColor;
use common\models\PsMachineDelivery;
use common\services\PrinterMaterialService;
use lib\money\Money;

class PrinterItemInfo
{
    public $psId;
    public $printerId;
    public $countryId;
    public $rating;

    /**
     * Has international delivery
     *
     * @var bool
     */
    public $international;

    /** @var PrinterItemTextureInfo[] */
    public $textures = [];

    /**
     * For cache only
     *
     * @var PrinterItemTextureInfo[]
     */
    public $cheapestItemsTextureInfo;

    /**
     * @var PrinterItemTextureInfo
     */
    public $cheapestItemTextureInfo;

    /**
     * @var float
     */
    public $cheapestItemTextureCost;

    /** @var string */
    public $carrierBy;

    /** @var  Size */
    public $size;
    public $certificated;

    /**
     * Price
     **/
    public $minPrice;

    /** @var float */
    public $cashbackPercent = 0;

    /** @var string */
    public $currency;

    /*
     * For sorting coefficients
     */
    public $locationLat;
    public $locationLon;
    public $reviewsRating; // avg
    public $isCertificated;
    public $ordersCount;

    public $deliveryInternational;
    public $deliveryStandard;


    public const TEST_MODEL_VOLUME = 6; // 6 Ml


    /**
     * @param RequestVolumeInfo[] $volumesInfo
     * @return bool
     */
    public function canPrintTextures($volumesInfo)
    {
        foreach ($volumesInfo as $volumeInfo) {
            if ($volumeInfo->textureInfo->materialId) {
                // Use material code
                if (!array_key_exists($volumeInfo->textureInfo->toStringMaterialColor(), $this->textures)) {
                    return false;
                }
            } elseif (!array_key_exists($volumeInfo->textureInfo->toStringGroupColor(), $this->cheapestItemsTextureInfo)) {
                return false;
            }
        }
        return true;
    }

    public function calcCheapestTextures(): void
    {
        if ($this->cheapestItemsTextureInfo !== null) {
            return;
        }
        $this->cheapestItemsTextureInfo = [];
        $this->textureGroupMaterials    = [];
        $minCost                        = $this->minPrice;
        $cheapestItemTextureInfo        = reset($this->textures);
        $this->cheapestItemTextureCost  = $minCost;

        foreach ($this->textures as $printerItemTextureInfo) {
            $printerTextureKey = $printerItemTextureInfo->textureInfo->toStringGroupColor();
            if (array_key_exists($printerTextureKey, $this->cheapestItemsTextureInfo)) {
                if ($this->cheapestItemsTextureInfo[$printerTextureKey]->pricePerMl > $printerItemTextureInfo->pricePerMl) {
                    $this->cheapestItemsTextureInfo[$printerTextureKey] = $printerItemTextureInfo;
                }
            } else {
                $this->cheapestItemsTextureInfo[$printerTextureKey] = $printerItemTextureInfo;
            }

            $cost = $printerItemTextureInfo->pricePerMl * self::TEST_MODEL_VOLUME;
            if ($cost === $minCost && $printerItemTextureInfo->textureInfo->materialColorId === PrinterColor::WHITE_COLOR_ID) { // Preference White color
                $cheapestItemTextureInfo = $printerItemTextureInfo;
                $minCost                 = $cost;
            }
            if ($cost < $minCost) {
                $cheapestItemTextureInfo = $printerItemTextureInfo;
                $minCost                 = $cost;

            }
        }
        $this->cheapestItemTextureCost = $minCost;
        $this->cheapestItemTextureInfo = $cheapestItemTextureInfo;
    }
}