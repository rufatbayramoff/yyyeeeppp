<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.12.17
 * Time: 14:17
 */

namespace common\modules\printersList\models;

class RequestVolumeInfo
{
    /** @var TextureInfo */
    public $textureInfo;

    /** @var float */
    public $volume;
}