<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.12.17
 * Time: 14:47
 */

namespace common\modules\printersList\models;

class PrintersTree
{
    /**
     * @var PrinterItemInfo[]
     */
    public $printerItemList = [];

    /**
     * @var array
     */
    public $availableMaterialsAndColors = [];

    /**
     * @var PrintersTree[]
     */
    public $subTrees = [];

    public function addPrinterInfo(PrinterItemInfo $printerInfo): void
    {
        $this->printerItemList[$printerInfo->printerId . '_' . $printerInfo->countryId] = $printerInfo;
    }
}