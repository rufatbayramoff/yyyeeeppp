<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.03.18
 * Time: 10:05
 */
namespace common\modules\printersList\models;

class AnswerColorInfo
{
    /**
     * Group Id
     *
     * @var int
     */
    public $groupId;

    /**
     * Material id
     *
     * @var int
     */
    public $materialId;

    /**
     * Color id
     *
     * @var int
     */
    public $colorId;

    /**
     * Color Rating
     *
     * @var float
     */
    public $rating;
}