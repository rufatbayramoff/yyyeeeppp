<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.12.17
 * Time: 9:34
 */

namespace common\modules\printersList\models;

use common\components\ps\locator\Size;

class RequestInfo
{
    public const SORT_STRATEGY_DEFAULT = 'default';
    public const SORT_STRATEGY_2021_DEFAULT = 'default_2021';
    public const SORT_STRATEGY_CHEAPEST = 'price';
    public const SORT_STRATEGY_RATING = 'rating';
    public const SORT_STRATEGY_DISTANCE = 'distance';

    public const OPERATION_TYPE_GET_OFFERS = 0;
    public const OPERATION_TYPE_UPDATE_PRINTERS_TREE = 1;

    public $operationType = self::OPERATION_TYPE_GET_OFFERS;

    /** @var array */
    public $allowedMaterials;
    public $countryId;
    /** @var  Size */
    public $size;
    public $onlyCertificated = false;
    /** @var RequestVolumeInfo[] */
    public $volumes = [];

    public $allowResetTexture = true;
    public $currencies = []; // Allowed currency list

    public $sort = self::SORT_STRATEGY_DEFAULT;

    // Lat lon may be used for sorting
    public $locationLat;
    public $locationLon;

    /**
     * @var bool
     */
    public $onlyInternational = false;

    public function setKitTexture(?TextureInfo $textureInfo)
    {
        foreach ($this->volumes as $volume) {
            $volume->textureInfo = $textureInfo;
        }
    }

    public function populateByArray($array)
    {
        $this->allowedMaterials = $array['allowedMaterials'];
        $this->countryId = $array['countryId'];
        $this->size = $array['size'];
        $this->onlyCertificated = $array['onlyCertificated'];
        $this->allowResetTexture = $array['allowResetTexture'];
        $this->sort = $array['sort'];
        $this->locationLat = $array['locationLat'];
        $this->locationLon = $array['locationLon'];
        foreach ($array['volumes'] as $volumeKey => $volumeArray) {
            $volume = new RequestVolumeInfo();
            $volume->textureInfo = new TextureInfo();
            $volume->textureInfo->materialGroupId = $volumeArray['textureInfo']['materialGroupId'];
            $volume->textureInfo->materialColorId = $volumeArray['textureInfo']['materialColorId'];
            $volume->volume = $volumeArray['volume'];
            $this->volumes[$volumeKey] = $volume;
        }
    }

    public function getTotalVolume()
    {
        $volume = 0;
        foreach ($this->volumes as $oneVolume) {
            $volume+=$oneVolume->volume;
        }
        return $volume;
    }
}