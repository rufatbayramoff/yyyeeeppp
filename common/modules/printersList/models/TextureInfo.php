<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.12.17
 * Time: 11:57
 */

namespace common\modules\printersList\models;

class TextureInfo
{
    public $materialGroupId;
    public $materialId;
    public $materialColorId;

    public function toStringGroupColor()
    {
        return $this->materialGroupId . '_' . $this->materialColorId;
    }

    public function toStringMaterialColor()
    {
        return 'm' . $this->materialId . '_' . $this->materialColorId;
    }
}