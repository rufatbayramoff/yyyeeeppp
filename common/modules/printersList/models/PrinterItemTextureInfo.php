<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.12.17
 * Time: 11:44
 */

namespace common\modules\printersList\models;

class PrinterItemTextureInfo
{
    /** @var TextureInfo */
    public $textureInfo;

    /** @var float */
    public $pricePerMl;
}
