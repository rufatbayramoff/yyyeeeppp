<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.12.17
 * Time: 12:45
 */

namespace common\modules\printersList\models;

use lib\money\Money;

class OfferItemInfo
{
    /** @var Money */
    public $price;

    /** @var float */
    public $tsBonusAmount;

    /** @var Money */
    public $priceWithoutMin;

    /** @var TextureInfo */
    public $priceTextureInfo;

    /** @var PrinterItemInfo */
    public $printerItemInfo;

    /** @var float */
    public $coefficient;
}