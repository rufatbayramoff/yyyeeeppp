<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.12.17
 * Time: 10:36
 */

namespace common\modules\printersList\models;

use lib\money\Money;

/**
 * Class OfferItemPrinterGrouped
 *
 * OfferItem grouped by printer id
 *
 * @package common\modules\printersList\models
 */
class OfferItemPrinterGrouped
{
    /** @var Money */
    public $price;

    /** @var float */
    public $tsBonusAmount;

    /** @var Money */
    public $priceWithoutMin;

    /** @var TextureInfo */
    public $priceTextureInfo;

    /** @var int */
    public $printerId;

    /** @var int */
    public $countryId;

    public function initByOfferItem(OfferItemInfo $offerItemInfo)
    {
        $this->price = $offerItemInfo->price;
        $this->tsBonusAmount = $offerItemInfo->tsBonusAmount;
        $this->priceWithoutMin = $offerItemInfo->priceWithoutMin;
        $this->priceTextureInfo = $offerItemInfo->priceTextureInfo;
        $this->printerId = $offerItemInfo->printerItemInfo->printerId;
        $this->countryId = $offerItemInfo->printerItemInfo->countryId;
    }
}