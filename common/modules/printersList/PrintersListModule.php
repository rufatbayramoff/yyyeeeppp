<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.12.17
 * Time: 15:00
 */

namespace common\modules\printersList;

use common\components\BaseModule;
use common\modules\printersList\components\PrintersListDaemon;
use common\modules\printersList\components\PrintersTreeUpdater;
use common\modules\printersList\models\PrintersTree;

/**
 * Class PrintersListModule
 *
 * @property PrintersListDaemon $printersListDaemon
 * @property PrintersTreeUpdater $printersTreeUpdater
 */
class PrintersListModule extends BaseModule
{

    public $port = 9092;

    protected function getComponentsList()
    {
        return [
            'printersListDaemon' => PrintersListDaemon::class,
            'printersTreeUpdater' => PrintersTreeUpdater::class
        ];
    }


    /**
     * @throws \yii\base\InvalidParamException
     */
    protected function initConfig()
    {

    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();

        $this->initConfig();
        $componentsList = $this->getComponentsList();
        $this->setComponents($componentsList);
        $this->initComponentsModule($componentsList);
    }
}