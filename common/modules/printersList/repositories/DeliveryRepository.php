<?php


namespace common\modules\printersList\repositories;


use common\models\DeliveryType;
use common\models\PsMachineDelivery;

class DeliveryRepository
{
    public function packageFee(int $printerId): array
    {
        /** @var PsMachineDelivery[] $deliveries */
        $deliveries = PsMachineDelivery::find()
            ->innerJoinWith(['deliveryType','psMachine'])
            ->andWhere(['company_service.ps_printer_id' => $printerId])
            ->andWhere(['IS NOT','ps_machine_delivery.packing_price',null])
            ->all();
        $result = [
            DeliveryType::INTERNATIONAL => 0,
            DeliveryType::STANDARD => 0
        ];
        foreach ($deliveries as $delivery) {
            $result[$delivery->deliveryType->code] = $delivery->packing_price;
        }
        return $result;
    }
}