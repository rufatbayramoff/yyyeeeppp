<?php
/**
 * User: nabi
 */

namespace common\modules\onshape\controllers;


use common\components\BaseController;

class DefaultController extends BaseController
{

    public function actionIndex()
    {
        return $this->redirect(['/onshape/app']);
    }
}
