<?php
/**
 * User: nabi
 */

namespace common\modules\onshape\services;


use common\modules\onshape\components\OnshapeApi;
use common\modules\onshape\components\OnshapeModel3dFactory;
use common\modules\onshape\models\OnshapeElement;
use common\modules\onshape\models\OnshapePart;
use common\modules\onshape\models\OnshapeRequest;
use yii\base\BaseObject;
use yii\web\UploadedFile;

/**
 * Class OnshapeService
 *
 * @package common\modules\onshape\services
 */
class OnshapeService extends BaseObject
{

    public $apiKey = '';
    public $secretKey = '';

    /**
     * @var OnshapeModel3dFactory
     */
    private $factory;

    public function init()
    {
        $this->apiKey = param('api.onshape.apiKey', 'Ycb5E22U7CuB0kmXyvjkyNTP');
        $this->secretKey = param('api.onshape.secretKey', 'BYfFS9t0oFtwrFbr1a3PtHJUZCgsyvj8HkizVkVLtnpXTfZX');
        parent::init();
    }

    public function injectDependencies(OnshapeModel3dFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param array $inParams
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    public function loadRequestModel(array $inParams)
    {
        $inParams['class'] = OnshapeRequest::class;
        $obj = \Yii::createObject($inParams);
        return $obj;
    }

    /**
     * @param OnshapeRequest $onshapeRequest
     * @return \common\models\Model3d|null
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function createModel3dByRequest(OnshapeRequest $onshapeRequest)
    {
        $api = new OnshapeApi($onshapeRequest);
        $partStudioElement = OnshapeElement::createElementsFromJson(
            $api->getDocumentElements(),
            OnshapeElement::ELEMENT_TYPE_PARTSTUDIO
        );
        $onshapeRequest->setElement($partStudioElement);
        $parts = OnshapePart::createPartsFromJson(
            $onshapeRequest,
            $api->getPartsMeta()
        );
        $uploadedFiles = [];
        foreach ($parts as $part) {
            $uploadFile = $api->getPartStl($part);
            $uploadFile->name = $part->name . '.stl';
            $uploadedFiles[] = $uploadFile;
        }
        return $this->factory->createModel3d($uploadedFiles);
    }
}
