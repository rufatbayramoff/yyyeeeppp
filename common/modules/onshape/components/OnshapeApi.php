<?php
/**
 * User: nabi
 */

namespace common\modules\onshape\components;

use common\modules\onshape\models\OnshapePart;
use common\modules\onshape\models\OnshapeRequest;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\UploadedFile;

class OnshapeApi
{
    /**
     * @var OnshapeRequest
     */
    public $request;

    public function __construct(OnshapeRequest $request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     * @throws HttpException
     */
    public function getDocumentElements()
    {
        $link = sprintf(
            '%s/api/documents/d/%s/w/%s/elements?withThumbnails=false',
            $this->request->server,
            $this->request->documentId,
            $this->request->workspaceId
        );
        return $this->runRequest($link);

    }
    /**
     * @return string
     * @throws HttpException
     */
    public function getPartsMeta()
    {
        $link = sprintf(
            '%s/api/partstudios/d/%s/w/%s/e/%s/metadata',
            $this->request->server,
            $this->request->documentId,
            $this->request->workspaceId,
            $this->request->elementId
        );
        return $this->runRequest($link);
    }

    /**
     * @param OnshapePart $part
     * @param string $format
     * @return UploadedFile
     * @throws HttpException
     */
    public function getPartStl(OnshapePart $part, $format = 'binary')
    {
        $link = sprintf(
            '%s/api/parts/d/%s/w/%s/e/%s/partid/%s/stl?grouping=true&scale=1.0&units=millimeter&mode=%s',
            $this->request->server,
            $part->documentId,
            $part->workspaceId,
            $part->elementId,
            $part->partId,
            $format
        );
        \Yii::info('get parts stl', 'tsdebug');
        return $this->runDownloadFile($link, true);
    }

    /**
     * @param $link
     * @param bool $manualFollow
     * @return UploadedFile
     * @throws HttpException
     */
    protected function runDownloadFile($link, $manualFollow = false)
    {
        $tempPath = \Yii::getAlias('@frontend') . '/runtime/onshape_' . date('Y-m-d_H-i-s') . '-' . mt_rand(0, 999999) . '.stl';
        $fp = fopen($tempPath, 'w+');
        \Yii::info($link, 'tsdebug');
        \Yii::info($this->request->accessToken->token, 'tsdebug');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        if ($manualFollow) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $this->request->accessToken->token]);
        curl_exec($ch);
        if (curl_errno($ch)) {
            $error = 'Error:' . curl_error($ch);
            \Yii::error($error);
            throw new HttpException(500, $error);
        }
        curl_close($ch);
        fclose($fp);
        if ($manualFollow) {
            $curlResult = file_get_contents($tempPath);
            preg_match('/Location:(.*?)\n/', $curlResult, $matches);
            $newUrl = trim(array_pop($matches));
            return $this->runDownloadFile($newUrl, false);
        }

        $uploadedFile = new UploadedFile();
        $uploadedFile->size = filesize($tempPath);
        $uploadedFile->tempName = $tempPath;
        return $uploadedFile;
    }

    /**
     * @param $link
     * @return mixed
     * @throws HttpException
     */
    protected function runRequest($link)
    {
        $ch = curl_init();
        \Yii::info($link, 'tsdebug');
        \Yii::info($this->request->accessToken->token, 'tsdebug');
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 35);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 35);

        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            [
                'Authorization: Bearer ' . $this->request->accessToken->token,
                'Content-Type: application/json'
            ]
        );

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error = 'Error:' . curl_error($ch);
            \Yii::error($error);
            throw new HttpException(500, $error);
        }
        \Yii::info($result, 'tsdebug');
        curl_close($ch);
        return $result;
    }
}
