<?php
/**
 * User: nabi
 */

namespace common\modules\onshape\components;

use common\interfaces\Model3dBaseInterface;
use common\models\factories\UploadedFileListFactory;
use common\models\Model3d;
use common\models\User;
use common\models\UserSession;
use common\modules\onshape\models\OnshapeRequest;
use common\modules\product\interfaces\ProductInterface;
use common\services\Model3dService;
use frontend\models\user\UserFacade;
use yii\base\BaseObject;
use yii\web\UploadedFile;

/**
 * Class OnshapeModel3dFactory
 *
 * @package common\modules\onshape\components
 */
class OnshapeModel3dFactory extends BaseObject
{

    /**
     * @var User
     */
    private $user;

    /**
     * OnshapeModel3dFactory constructor.
     *
     * @param User|null $user
     * @throws \yii\web\NotFoundHttpException
     */
    public function __construct(User $user = null)
    {
        if ($user !== null) {
            $this->user = $user;
        } else {
            $this->user = UserFacade::getCurrentUser();
        }
    }

    /**
     * @param UploadedFile[] $files
     * @return Model3d|null
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function createModel3d(array $files)
    {
        $model3dProperties = [
            'source' => Model3d::SOURCE_API,
            'cae'    => Model3dBaseInterface::CAE_CAD
        ];
        $model3dUser = $this->user;
        $model3dService = \Yii::createObject(Model3dService::class, [$model3dUser]);
        $model3d = $model3dService->createModel3dByUploadFilesList(
            $model3dProperties,
            $files
        );
        $model3d->price_per_produce = 0;
        $model3d->price_currency = 'USD';
        $model3d->safeSave();
        return $model3d;
    }
}
