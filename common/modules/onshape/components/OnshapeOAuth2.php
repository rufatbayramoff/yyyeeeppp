<?php
/**
 * User: nabi
 */

namespace common\modules\onshape\components;


use yii\authclient\OAuth2;

/**
 * Class OnshapeOAuth2
 *
 *  $oauthClient = new OnshapeOAuth2();
 *  $url = $oauthClient->buildAuthUrl(); // Build authorization URL
 *  Yii::$app->getResponse()->redirect($url); // Redirect to authorization URL.
 * // After user returns at our site:
 *  $code = $_GET['code'];
 *  $accessToken = $oauthClient->fetchAccessToken($code); // Get access token
 *
 * @package common\modules\onshape\components
 */
class OnshapeOAuth2 extends OAuth2
{

    public $clientId;
    public $clientSecret ;

    public $redirectUri = 'https://www.treatstock.com/onshape/app/auth';

    public $apiBaseUrl = 'https://cad.onshape.com/api/';
    public $authUrl = 'https://oauth.onshape.com/oauth/authorize';
    public $tokenUrl = 'https://oauth.onshape.com/oauth/token';

    public $validateAuthState = false;

    public function init()
    {
        parent::init();
        $this->redirectUri = param('api.onshape.redirectUri', param('server') . '/onshape/app/auth');
        $this->clientId = param('api.onshape.oauthKey', 'VCABLDNANGWMHA4VUCLSD5D3JENHRD5YKT3CJQQ=');
        $this->clientSecret = param('api.onshape.oauthSecret', 'JOYPIBHOWV7P2EF4GS7FC7BOXOX3I5CWIPW6IBYOIHTMN2QWZZVQ====');
    }

    /**
     * Initializes authenticated user attributes.
     *
     * @return array auth user attributes.
     */
    protected function initUserAttributes()
    {

    }

    public function getReturnUrl()
    {
        return $this->redirectUri;
    }

    /**
     * this is first user visited url, we save income get parameters (like doc,element,w and etc)
     * @param array $params
     */
    public function setFirstRequest(array $params)
    {
        $keys = [];
        foreach ($params as $key => $val) {
            $this->setState($key, $val);
            $keys[] = $key;
        }
        $this->setState('keys', implode(",", $keys));
    }

    /**
     *
     */
    public function getFirstRequest()
    {
        $keys = explode(",", $this->getState('keys'));
        $request = [];
        foreach ($keys as $key) {
            $request[$key] = $this->getState($key);
        }
        return $request;
    }
}