<?php
/**
 * User: nabi
 */

namespace common\modules\onshape\models;

use yii\authclient\OAuthToken;

/**
 * Class OnshapeRequest
 * first GET request from onshape
 * also accessToken is added for API requests
 *
 * @package common\modules\onshape\models
 */
class OnshapeRequest
{
    public $documentId;
    public $workspaceId;
    public $elementId;
    public $elementType;
    public $server;
    public $userId;
    public $documentState;
    public $defaultWorkspaceName;
    public $clientId;
    public $access;
    public $locale;

    /**
     * @var OAuthToken
     */
    public $accessToken;

    /**
     * @var OnshapeElement
     */
    private $element;

    /**
     * @param OnshapeElement $partStudioElement
     */
    public function setElement(OnshapeElement $partStudioElement)
    {
        $this->element = $partStudioElement;
        $this->elementId = $partStudioElement->id;
    }
}
