<?php
/**
 * User: nabi
 */

namespace common\modules\onshape\models;

use yii\helpers\Json;

/**
 * Class OnshapeElement
 *
 * @package common\modules\onshape\models
 */
class OnshapeElement
{
    const ELEMENT_TYPE_PARTSTUDIO = 'PARTSTUDIO';
    const ELEMENT_TYPE_APPLICATION = 'APPLICATION';
    const ELEMENT_TYPE_ASSEMBLY = 'ASSEMBLY';

    public $elementType;
    public $type;
    public $name;
    public $dataType;
    public $id;
    public $microversionId;

    /**
     * factory method to create onshape elements from JSON
     *
     * @param $json
     * @param null $elementType - if specified only 1 element returned
     * @return OnshapeElement[]|OnshapeElement
     */
    public static function createElementsFromJson($json, $elementType = null)
    {
        $elementsJson = Json::decode($json);
        $elements = [];
        foreach ($elementsJson as $partMeta) {
            $element = new self();
            foreach ($partMeta as $k => $v) {
                if (!property_exists($element, $k)) {
                    continue;
                }
                $element->$k = $v;
            }
            if ($elementType == $element->elementType) {
                return $element;
            }
            $elements[$element->id] = $element;
        }
        return $elements;
    }
}
