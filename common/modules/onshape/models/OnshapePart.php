<?php
/**
 * User: nabi
 */

namespace common\modules\onshape\models;

use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * Class OnshapePart
 *
 * for API request
 *
 * https://cad.onshape.com/api/partstudios/d/<document>/w/<workspace>/e/<element>/metadata
 *
 * @package common\modules\onshape\models
 */
class OnshapePart
{
    public $state;
    public $description;
    public $name;
    public $elementId;
    public $revision;
    public $partId;
    public $microversionId;
    public $partNumber;
    public $bodyType;
    public $configurationId;
    public $isFlattenedBody;
    public $isMesh;
    public $isHidden;

    public $documentId;
    public $workspaceId;

    const STATE_IN_PROGRESS = 'IN_PROGRESS';
    const BODY_TYPE_SOLID = 'solid';
    const BODY_TYPE_WIRE = 'wire';

    const STL_TYPE_BINARY = 'binary';
    const STL_TYPE_TEXT = 'text';

    public static $filterBodyType = [self::BODY_TYPE_SOLID];

    /**
     * create objects from text json
     * factory method
     *
     * @param OnshapeRequest $onshapeRequest
     * @param $json
     * @return OnshapePart[]
     */
    public static function createPartsFromJson(OnshapeRequest $onshapeRequest, $json)
    {
        $partsMeta = Json::decode($json);
        $parts = [];
        foreach ($partsMeta as $partMeta) {
            $onshapePart = new self();
            foreach ($partMeta as $k => $v) {
                if (!property_exists($onshapePart, $k)) {
                    continue;
                }
                $onshapePart->$k = $v;
            }
            if (!in_array($onshapePart->bodyType, self::$filterBodyType)) {
                continue;
            }
            $onshapePart->documentId = $onshapeRequest->documentId;
            $onshapePart->workspaceId = $onshapeRequest->workspaceId;
            $onshapePart->elementId = $onshapeRequest->elementId;
            $parts[$onshapePart->partId] = $onshapePart;
        }
        return $parts;
    }
}
