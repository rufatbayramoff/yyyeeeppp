<?php
/**
 * User: nabi
 */

namespace common\modules\onshape;


use yii\base\Module;

/**
 * Class OnshapeModule
 *
 * onshape.com app module
 *
 * @package common\modules\onshape
 */
class OnshapeModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\onshape\controllers';

    const LAUNCHER_ONSHAPE = 'onshape';
    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();
    }
}
