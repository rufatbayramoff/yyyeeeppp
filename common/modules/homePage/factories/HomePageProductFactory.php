<?php

namespace common\modules\homePage\factories;

use common\components\DateHelper;
use common\models\HomePageCategoryBlock;
use common\models\HomePageCategoryCard;
use common\models\HomePageFeatured;
use yii\base\BaseObject;

class HomePageProductFactory extends BaseObject
{
    /**
     * @return HomePageCategoryBlock
     */
    public function createCategoryBlock(): HomePageCategoryBlock
    {
        $homePageCategoryBlock = new HomePageCategoryBlock();

        $homePageCategoryBlock->setScenario(HomePageCategoryBlock::SCENARIO_CREATE);
        $homePageCategoryBlock->created_at = DateHelper::now();

        return $homePageCategoryBlock;
    }

    /**
     * @return HomePageCategoryBlock
     */
    public function createCategoryCard(): HomePageCategoryCard
    {
        $categoryCard = new HomePageCategoryCard();

        $categoryCard->setScenario(HomePageCategoryCard::SCENARIO_CREATE);
        $categoryCard->created_at = DateHelper::now();

        return $categoryCard;
    }

    /**
     * @return HomePageFeatured
     */
    public function createFeatured(): HomePageFeatured
    {
        $categoryCard = new HomePageFeatured();

        $categoryCard->created_at = DateHelper::now();

        return $categoryCard;
    }
}