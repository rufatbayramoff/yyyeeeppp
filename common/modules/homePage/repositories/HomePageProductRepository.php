<?php

namespace common\modules\homePage\repositories;

use common\models\HomePageCategoryBlock;
use common\models\HomePageCategoryCard;
use common\models\HomePageFeatured;
use yii\base\BaseObject;

class HomePageProductRepository extends BaseObject
{
    /**
     * @return HomePageCategoryBlock[]
     */
    public static function getActiveCategories(): array
    {
        return HomePageCategoryBlock::find()
            ->from([
                'c' => HomePageCategoryBlock::tableName()
            ])
                ->joinWith('productCategory')
            ->where([
                'c.is_active' => 1
            ])
            ->orderBy(['c.position' => SORT_ASC])
            ->all();
    }

    /**
    * @return HomePageCategoryCard[]
    */
    public static function getActiveCardsCategories(): array
    {
        return HomePageCategoryCard::find()
            ->from([
                'c' => HomePageCategoryCard::tableName()
            ])
                ->joinWith('productCategory')
                ->joinWith('coverFile')
            ->where([
                'c.is_active' => 1
            ])
            ->orderBy(['c.position' => SORT_ASC])
            ->all();
    }

    public static function getActiveFeaturedCategories(): array
    {
        return HomePageFeatured::find()
        ->from([
            'f' => HomePageFeatured::tableName()
        ])
            ->joinWith('productCategory', true, 'JOIN')
            ->joinWith('homePageFeaturedProducts')
        ->where([
            'f.is_active' => 1
        ])
        ->orderBy(['f.position' => SORT_ASC])
        ->all();
    }
}