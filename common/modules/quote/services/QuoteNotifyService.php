<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.01.19
 * Time: 14:25
 */

namespace common\modules\quote\services;

use common\components\Emailer;
use common\models\Preorder;
use common\models\User;
use frontend\modules\preorder\components\PreorderEmailer;
use yii\base\BaseObject;

class QuoteNotifyService extends BaseObject
{

    /** @var PreorderEmailer */
    protected $emailer;

    public function injectDependencies(
        PreorderEmailer $emailer
    )
    {
        $this->emailer = $emailer;
    }

    /**
     * @param User $user
     * @param Preorder $quote
     */
    public function notifyAccountCreated(User $user,Preorder $quote): void
    {
        $this->emailer->sendCreatedNewClientForQuote($user, $quote);

    }
}