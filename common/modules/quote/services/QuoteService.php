<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.01.19
 * Time: 14:27
 */

namespace common\modules\quote\services;

use common\models\factories\UserFactory;
use common\models\Preorder;
use common\models\repositories\UserRepository;
use yii\base\BaseObject;

class QuoteService extends BaseObject
{
    /** @var QuoteNotifyService */
    protected $quoteNotifyService;

    /** @var UserFactory */
    protected $userFactory;

    /** @var UserRepository */
    protected $userRepository;

    public function injectDependencies(
        QuoteNotifyService $quoteNotifyService,
        UserFactory $userFactory,
        UserRepository $userRepository
    ) {
        $this->quoteNotifyService = $quoteNotifyService;
        $this->userFactory = $userFactory;
        $this->userRepository = $userRepository;
    }

}