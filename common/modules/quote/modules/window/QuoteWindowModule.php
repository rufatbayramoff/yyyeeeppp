<?php

namespace common\modules\quote\modules\window;

/**
 * quote-window module definition class
 */
class QuoteWindowModule extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\quote\modules\window\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
