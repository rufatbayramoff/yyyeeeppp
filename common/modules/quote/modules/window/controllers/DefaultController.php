<?php

namespace common\modules\quote\modules\window\controllers;

use yii\web\Controller;

/**
 * Default controller for the `quote-window` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
