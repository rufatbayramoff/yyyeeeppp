<?php
/**
 * Date: 11.10.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\affiliate\models;


use common\components\UuidHelper;
use common\models\AffiliateSource;
use common\models\User;
use common\modules\affiliate\services\AffiliateService;
use yii\helpers\Json;

/**
 * Class AffiliatedUserSession
 * container of session objects required for affiliating
 *
 * @package common\modules\affiliate\components
 */
class AffiliateUserSession
{
    /**
     * @var string
     */
    public $uuid;


    /**
     * User uuid or directional UserSourceUuid
     * sourceUuid
     * @var int
     */
    public $sourceUuid;

    /**
     * Affiliate user uuid
     */
    public $affiliateUserUuid;

    /**
     * @var
     */
    public $type;

    /**
     * timestamp user first visit date
     * @var int
     */
    public $visitDate;

    /** @var AffiliateSource */
    protected $affiliateSource;


    public static function create(?AffiliateSource $affiliateSource)
    {
        $affSession                    = new AffiliateUserSession();
        $affSession->uuid              = UuidHelper::generateUuid();
        $affSession->type              = $affiliateSource->type ?? AffiliateSource::TYPE_EMPTY;
        $affSession->sourceUuid        = $affiliateSource->uuid ?? null;
        $affSession->affiliateUserUuid = $affiliateSource->user->uid ?? null;
        $affSession->date              = time();
        return $affSession;
    }

    public function setSource(AffiliateSource $affiliateSource)
    {
        $this->type            = AffiliateSource::TYPE_SOURCE;
        $this->sourceUuid      = $affiliateSource->uuid;
        $this->affiliateSource = $affiliateSource;
    }

    public function getSource(): ?AffiliateSource
    {
        if (!$this->affiliateSource) {
            $this->affiliateSource = AffiliateSource::find()->where(['uuid' => $this->sourceUuid, 'type' => $this->type, 'inactive' => 0])->one();
        }
        return $this->affiliateSource;
    }

    public function setUserType(User $user)
    {
        $this->type              = AffiliateSource::TYPE_USER_UUID;
        $this->affiliateUserUuid = $user->uid;
    }

    public function encodeCookieValue(): string
    {
        $info = [
            'uuid' => $this->uuid,
            'type' => $this->type,
            'date' => $this->visitDate
        ];

        if ($this->sourceUuid) {
            $info['sUuid'] = $this->sourceUuid;
        }
        if ($this->affiliateUserUuid) {
            $info['uUuid'] = $this->affiliateUserUuid;
        }
        return Json::encode($info);
    }

    /**
     *
     * @param string $cookieValue
     */
    public function decodeCookieValue(string $cookieValue)
    {
        if (!$cookieValue) {
            return;
        }
        $value = Json::decode($cookieValue, true);
        if (!$value || !array_key_exists('uuid', $value) || !array_key_exists('sUuid', $value)) {
            return;
        }
        $this->uuid              = (string)$value['uuid'];
        $this->sourceUuid        = (string)$value['sUuid'];
        $this->affiliateUserUuid = empty($value['uUuid']) ? '' : (string)$value['uUuid'];
        $this->type              = (string)$value['type'];
        $this->visitDate         = (string)$value['date'];
    }
}