<?php
/**
 * Created by mitaichik
 */

namespace common\modules\affiliate\models;


use lib\money\Money;

class AffiliateStatistic
{
    /**
     * @var Money
     */
    public $earning;

    /**
     * @var int
     */
    public $ordersCount;

    /**
     * @var int
     */
    public $usersCount;
}