<?php

namespace common\modules\affiliate\actions;

use common\models\AffiliateSource;
use common\modules\affiliate\helpers\AffiliateUrlHelper;
use common\modules\affiliate\services\AffiliateService;
use Yii;
use yii\base\Action;

/**
 * Class ActionUpdate
 *
 * @package common\product\actions
 * @property \common\components\BaseController $controller
 */
class ActionUpdate extends Action
{
    /** @var AffiliateService */
    protected $affiliateService;

    public function injectDependencies(
        AffiliateService $affiliateService
    )
    {
        $this->affiliateService = $affiliateService;
    }

    public function run()
    {
        $id              = Yii::$app->request->get('id');
        $affiliateSource = AffiliateSource::find()->where(['uuid' => $id])->one();
        $this->affiliateService->checkEditAccessForCurrentUser($affiliateSource);

        if (\Yii::$app->request->isPost) {
            $affiliateSource->load(\Yii::$app->request->post());
            if ($affiliateSource->validate()) {
                $affiliateSource->safeSave();
                $this->controller->setFlashMsg(true, _t('site.common', 'Updated'));
                return $this->controller->redirect(AffiliateUrlHelper::sourcesList($affiliateSource->user->company));
            }
        }
        return $this->controller->renderContent($this->controller->renderFile(
            Yii::getAlias('@frontend/modules/mybusiness/views/affiliates/create.php'),
            [
                'affiliateSource' => $affiliateSource,
            ]
        ));
    }
}
