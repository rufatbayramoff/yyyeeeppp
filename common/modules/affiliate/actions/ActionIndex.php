<?php

namespace common\modules\affiliate\actions;

use backend\models\search\AffiliateSourceSearch;
use common\models\AffiliateSource;
use common\modules\affiliate\services\AffiliateService;
use Yii;
use yii\base\Action;
use yii\base\BaseObject;

/**
 * Class ActionIndex
 *
 * @package common\product\actions
 * @property \common\components\BaseController $controller
 */
class ActionIndex extends Action
{
    /** @var AffiliateService */
    protected $affiliateService;

    public function injectDependencies(
        AffiliateService $affiliateService
    )
    {
        $this->affiliateService = $affiliateService;
    }

    public function run()
    {
        $searchModel                                  = new AffiliateSourceSearch();
        $params                                       = \Yii::$app->request->queryParams;
        $params[$searchModel->formName()]['user_id']  = $this->controller->user->id;
        $params[$searchModel->formName()]['inactive'] = 0;
        $dataProvider                                 = $searchModel->search($params);

        return $this->controller->renderContent($this->controller->renderFile(
            Yii::getAlias('@frontend/modules/mybusiness/views/affiliates/index.php'),
                    [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        ));
    }
}
