<?php

namespace common\modules\affiliate\actions;

use common\models\AffiliateSource;
use common\modules\affiliate\services\AffiliateService;
use Yii;
use yii\base\Action;

/**
 * Class ActionView
 *
 * @package common\product\actions
 * @property \common\components\BaseController $controller
 */
class ActionView extends Action
{
    /** @var AffiliateService */
    protected $affiliateService;

    public function injectDependencies(
        AffiliateService $affiliateService
    )
    {
        $this->affiliateService = $affiliateService;
    }

    public function run()
    {
        $id              = Yii::$app->request->get('id');
        $affiliateSource = AffiliateSource::find()->where(['uuid' => $id])->one();
        $this->affiliateService->checkEditAccessForCurrentUser($affiliateSource);
        $awardDataProvider = $this->affiliateService->getAwardsProvider($affiliateSource);
        return $this->controller->renderContent($this->controller->renderFile(
            Yii::getAlias('@frontend/modules/mybusiness/views/affiliates/view.php'),
            [
                'affiliateSource'   => $affiliateSource,
                'awardDataProvider' => $awardDataProvider
            ]
        ));
    }
}
