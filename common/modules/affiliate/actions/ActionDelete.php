<?php
namespace common\modules\affiliate\actions;

use common\components\exceptions\OnlyPostRequestException;
use common\models\AffiliateSource;
use common\modules\affiliate\helpers\AffiliateUrlHelper;
use common\modules\affiliate\services\AffiliateService;
use Yii;
use yii\base\Action;

/**
 * Class ActionDelete
 *
 * @package common\product\actions
 * @property \common\components\BaseController $controller
 */
class ActionDelete extends Action
{
    /** @var AffiliateService */
    protected $affiliateService;

    public function injectDependencies(
        AffiliateService $affiliateService
    )
    {
        $this->affiliateService = $affiliateService;
    }

    public function run()
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $id              = Yii::$app->request->get('id');
        $affiliateSource = AffiliateSource::find()->where(['uuid' => $id])->one();
        $this->affiliateService->checkEditAccessForCurrentUser($affiliateSource);
        if ($affiliateSource->isUserType()) {
            $this->controller->setFlashMsg(false, _t('site.affiliate', 'This type cannot be inactived'));
            return $this->controller->redirect(AffiliateUrlHelper::sourcesList($affiliateSource->user->company));
        }

        $this->affiliateService->inactiveSource($affiliateSource);
        $this->controller->setFlashMsg(true, _t('site.common', 'Inactivated'));
        return $this->controller->redirect(AffiliateUrlHelper::sourcesList($affiliateSource->user->company));
    }
}
