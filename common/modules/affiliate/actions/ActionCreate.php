<?php

namespace common\modules\affiliate\actions;

use common\models\user\UserIdentityProvider;
use common\modules\affiliate\factories\AffiliateSourceFactory;
use common\modules\affiliate\helpers\AffiliateUrlHelper;
use Yii;
use yii\base\Action;

/**
 * Class ActionCreate
 *
 * @package common\product\actions
 * @property \common\components\BaseController $controller
 */
class ActionCreate extends Action
{
    /** @var AffiliateSourceFactory */
    protected $affiliateSourceFactory;

    /** @var UserIdentityProvider */
    protected $userIdentityProvider;

    public function injectDependencies(
        AffiliateSourceFactory $affiliateSourceFactory,
        UserIdentityProvider $userIdentityProvider,
    )
    {
        $this->affiliateSourceFactory = $affiliateSourceFactory;
        $this->userIdentityProvider   = $userIdentityProvider;
    }

    public function run()
    {
        $currentUser     = $this->userIdentityProvider->getUser();
        $affiliateSource = $this->affiliateSourceFactory->createTypeSource($currentUser);
        if (\Yii::$app->request->isPost) {
            $affiliateSource->load(\Yii::$app->request->post());
            if ($affiliateSource->validate()) {
                $affiliateSource->safeSave();
                $this->controller->setFlashMsg(true, _t('site.common', 'Created'));
                return $this->controller->redirect(AffiliateUrlHelper::sourcesList($currentUser->company));
            }
        }
        return $this->controller->renderContent($this->controller->renderFile(
            Yii::getAlias('@frontend/modules/mybusiness/views/affiliates/create.php'),
            [
                'affiliateSource' => $affiliateSource,
            ]
        ));
    }
}
