<?php

namespace common\modules\affiliate\helpers;

class AffiliateUrlHelper
{
    /**
     *
     */
    public static function sourcesList($userHasCompany = false)
    {
        if ($userHasCompany) {
            return '/mybusiness/affiliates';
        } else {
            return '/workbench/affiliates';
        }
    }

    public static function register(): string
    {
        return '/mybusiness/affiliates/registration';
    }

    public static function sourceWidget($userHasCompany = false): string
    {
        if ($userHasCompany) {
            return '/mybusiness/widgets/affiliate-widget';
        } else {
            return '/workbench/affiliate-widget';
        }
    }
}