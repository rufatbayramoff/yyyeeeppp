<?php

namespace common\modules\affiliate\controllers;

use common\components\BaseController;


/**
 * Date: 29.09.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class CabinetController extends BaseController
{

    public $layout = '@frontend/modules/mybusiness/views/layouts/mybusinessLayout';
    public $viewPath = '@frontend/modules/mybusiness/views/statistics/affiliate';

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }
}