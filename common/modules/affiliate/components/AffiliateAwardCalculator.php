<?php

namespace common\modules\affiliate\components;

use common\components\PaymentExchangeRateConverter;
use common\models\AffiliateAward;
use common\models\Model3d;
use common\models\PaymentInvoice;
use common\models\StoreOrder;
use common\models\User;
use lib\money\Currency;

class AffiliateAwardCalculator
{
    /**
     * @param AffiliateAward $award
     * @param array $accounting
     * @return array
     */
    public function calculatorAward(AffiliateAward $award, array $accounting): array
    {
        $feeIn  = 0;
        $feeOut = 0; // Only zero Now

        if ($award->affiliateSource->isAwardTypeFixed()) {
            $feeIn = $award->affiliateSource->getFixdedFee()->getAmount();
            if ($feeIn > $accounting[PaymentInvoice::ACCOUNTING_TYPE_PS_FEE]) {
                // If more than our fee, set it max
                $feeIn = $accounting[PaymentInvoice::ACCOUNTING_TYPE_PS_FEE];
            }
        } elseif ($award->affiliateSource->isAwardTypeFeePercent()) {
            $feePercent = $award->affiliateSource->getTreatstockFeePercent();
            $feeIn      = $accounting[PaymentInvoice::ACCOUNTING_TYPE_PS_FEE]['price'] * $feePercent / 100;
        }
        return [$feeIn, $feeOut];
    }
}
