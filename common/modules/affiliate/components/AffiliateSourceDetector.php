<?php

namespace common\modules\affiliate\components;

use common\models\AffiliateSource;
use common\models\User;
use common\modules\affiliate\factories\AffiliateSourceFactory;
use Yii;
use yii\web\Request;

class AffiliateSourceDetector extends \yii\base\BaseObject
{
    /** @var AffiliateSourceFactory */
    public $affiliateSourceFactory;

    public function injectDependencies(AffiliateSourceFactory $affiliateSourceFactory)
    {
        $this->affiliateSourceFactory = $affiliateSourceFactory;
    }

    public function getUserMarkedSource(Request $request)
    {
        if ($userUid = Yii::$app->request->get('userUid')) {
            $user = User::find()->where(['uid' => $userUid])->one();
            if ($user && $user->isAffiliateAllowed()) {
                $affiliateSource = AffiliateSource::find()->where(
                    [
                        'user_id' => $user->id,
                        'type'       => AffiliateSource::TYPE_USER_UUID,
                        'inactive'   => 0
                    ]
                )->one();
                if (!$affiliateSource) {
                    $affiliateSource = $this->affiliateSourceFactory->createTypeUser($user);
                    $affiliateSource->safeSave();
                }
                return $affiliateSource;
            }
        }
        return null;
    }

    public function getSourceByUuid(Request $request)
    {
        if ($affiliateUuid = Yii::$app->request->get('affiliate')) {
            $source = AffiliateSource::find()->where(['uuid' => $affiliateUuid, 'inactive' => '0'])->one();
            return $source;
        }
        return null;
    }


    public function detect(Request $request)
    {
        if ($source = $this->getUserMarkedSource($request)) {
            // Source by user uuid
            return $source;
        }
        if ($source = $this->getSourceByUuid($request)) {
            return $source;
        }
        return null;
    }
}