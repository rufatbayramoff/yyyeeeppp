<?php

namespace common\modules\affiliate\components;

use common\components\DateHelper;
use common\models\AffiliateSource;
use common\models\AffiliateSourceClient;

class AffiliateSourceClientGC
{
    public function cleanOld()
    {
        AffiliateSourceClient::deleteAll(['<','created_at',DateHelper::subNowSec(AffiliateSourceClient::TIME_LEAVE_INTERVAL)]);
    }
}