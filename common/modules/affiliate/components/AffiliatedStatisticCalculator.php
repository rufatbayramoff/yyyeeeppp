<?php
/**
 * Created by mitaichik
 */

namespace common\modules\affiliate\components;


use common\components\PaymentExchangeRateConverter;
use common\modules\affiliate\models\AffiliateStatistic;
use lib\money\Money;
use yii\base\BaseObject;

class AffiliatedStatisticCalculator extends BaseObject
{
    /**
     * @var PaymentExchangeRateConverter
     */
    // private $convertor;

    /**
     * @param PaymentExchangeRateConverter $convertor
     */
    /*
    public function injectDependencies(PaymentExchangeRateConverter $convertor)
    {
        $this->convertor = $convertor;
    }*/

    /**
     * @param Affiliate $affiliate
     * @return AffiliateStatistic
     */
    /*
    public function calculate(Affiliate $affiliate) : AffiliateStatistic
    {
        $resourcesIds = $this->getResourcesIds($affiliate);

        $stat = new AffiliateStatistic();
        $stat->earning = $this->calculateErnings($resourcesIds, $affiliate->user->userProfile->current_currency_iso ?? Currency::USD);
        $stat->ordersCount = $this->calculateOrdersCount($resourcesIds);
        $stat->usersCount = $this->calculateUsersCount($resourcesIds);
        $stat->visitsCount = $this->calculateVisitsCount($resourcesIds);

        return $stat;
    }*/

    /**
     * @param array $resourcesIds
     * @param string $currency
     * @return Money
     */
    /*
    private function calculateErnings(array $resourcesIds, string $currency) : Money
    {
        $awards = AffiliateAward::findAll(['status' => AffiliateAward::STATUS_PAID, 'resource_id' => $resourcesIds]);

        $earnings = Money::zero($currency);

        foreach ($awards as $award) {
            $earning = Money::create($award->price, $award->price_currency);
            $earning = $this->convertor->convertMoney($earning, $currency);
            $earnings = MoneyMath::sum($earnings, $earning);
        }

        return $earnings;
    }*/

    /**
     * @param array $resourcesIds
     * @return int
     */
    /*
    private function calculateOrdersCount(array $resourcesIds) : int
    {
        return (new Query())
            ->select([
                dbexpr('count(distinct pi.store_order_id)')
            ])
            ->from(['aa' => AffiliateAward::tableName()])
                ->innerJoin(['pi' => PaymentInvoice::tableName()], 'aa.invoice_uuid = pi.uuid')
            ->where([
                'aa.resource_id' => $resourcesIds,
                'aa.status'      => AffiliateAward::STATUS_PAID
            ])->scalar();
    }*/

    /**
     * @param array $resourcesIds
     * @return int
     */
    /*
    private function calculateUsersCount(array $resourcesIds) : int
    {
        return AffiliateUserSource::find()
            ->where([
                'resource_id' => $resourcesIds,
                'status' => AffiliateUserSource::STATUS_ACTIVE,
                'type' => AffiliateUserSource::TYPE_REGISTER
            ])
            ->count();
    }
    */

    /**
     * @param array $resourcesIds
     * @return int
     */
    /*
    private function calculateVisitsCount(array $resourcesIds) : int
    {
        return AffiliateUserSource::find()
            ->where([
                'resource_id' => $resourcesIds,
                'status' => AffiliateUserSource::STATUS_ACTIVE,
                'type' => AffiliateUserSource::TYPE_VISIT
            ])
            ->count();
    }
    */

    /**
     * @param Affiliate $affiliate
     * @return int[]
     */
    /*
    private function getResourcesIds(Affiliate $affiliate) : array
    {
        return ArrayHelper::getColumn($affiliate->getAffiliateResources()->all(), 'id');
    }*/
}