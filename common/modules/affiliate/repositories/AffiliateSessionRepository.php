<?php

namespace common\modules\affiliate\repositories;

use common\models\AffiliateSource;
use common\modules\affiliate\models\AffiliateUserSession;

class AffiliateSessionRepository extends \yii\base\BaseObject
{
    /**
     * @var AffiliateUserSession
     */
    static protected $currentAffiliateSession;

    public function getCurrentAffiliateSession():?AffiliateUserSession
    {
        return self::$currentAffiliateSession;
    }

    public function setCurrentAffiliateSession(AffiliateUserSession $currentAffiliateSession)
    {
        self::$currentAffiliateSession = $currentAffiliateSession;
    }
}