<?php
/**
 * Date: 29.09.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
namespace common\modules\affiliate;

use Yii;
use yii\base\Module;


class AffiliateModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'common\modules\affiliate\controllers';


    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        // @TODO set components
        parent::init();
    }
}
