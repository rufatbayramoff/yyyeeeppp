<?php
/**
 * Date: 29.09.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\modules\affiliate\services;


use backend\models\search\AffiliateAwardSearch;
use common\components\DateHelper;
use common\components\Emailer;
use common\models\AffiliateAward;
use common\models\AffiliateSource;
use common\models\AffiliateSourceClient;
use common\models\AffiliateUserBind;
use common\models\Company;
use common\models\PaymentInvoice;
use common\models\PaymentInvoiceItem;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\modules\affiliate\components\AffiliateAwardCalculator;
use frontend\modules\mybusiness\models\AffiliateCompanyForm;
use lib\money\Currency;
use Yii;
use yii\base\BaseObject;
use yii\base\UserException;
use yii\data\ActiveDataProvider;

class AffiliateService extends BaseObject
{
    /** @var UserIdentityProvider */
    protected $userProviderIdentity;

    /** @var AffiliateAwardCalculator */
    protected $affiliateAwardCalculator;

    /** @var AffiliateSessionService */
    protected $affiliateSessionService;


    public function injectDependencies(
        UserIdentityProvider $userProviderIdentity,
        AffiliateAwardCalculator $affiliateAwardCalculator,
        AffiliateSessionService $affiliateSessionService
    )
    {
        $this->userProviderIdentity = $userProviderIdentity;
        $this->affiliateAwardCalculator = $affiliateAwardCalculator;
        $this->affiliateSessionService = $affiliateSessionService;
    }


    public function checkAccessForUser(User $user, AffiliateSource $affiliateSource)
    {
        if ($affiliateSource->user_id === $user->id) {
            return true;
        }
        throw new UserException('No access');
    }

    public function bindUserWithCurrentAffiliate(User $user)
    {
        if ($user->affiliateSourceClient) {
            return ;
        }
        $affiliateSource = $this->affiliateSessionService->getAffiliateSource(Yii::$app->request);
        if (!$affiliateSource || $affiliateSource->isApiType()) {
            return true;
        }

        $affiliateSourceClient = Yii::createObject(AffiliateSourceClient::class);
        $affiliateSourceClient->created_at = DateHelper::now();
        $affiliateSourceClient->affiliate_source_uuid = $affiliateSource->uuid;
        $affiliateSourceClient->user_id = $user->id;
        $affiliateSourceClient->safeSave();
        $user->populateRelation('affiliateSourceClient', $affiliateSourceClient);
    }

    public function checkEditAccessForCurrentUser(AffiliateSource $affiliateSource)
    {
        $currentUser = $this->userProviderIdentity->getUser();
        return $this->checkAccessForUser($currentUser, $affiliateSource);
    }

    public function inactiveSource(AffiliateSource $affiliateSource)
    {
        $affiliateSource->inactive = 1;
        $affiliateSource->safeSave();
    }


    /**
     * @param AffiliateSource $affiliateSource
     * @return ActiveDataProvider
     */
    public function getAwardsProvider(AffiliateSource $affiliateSource): ActiveDataProvider
    {
        $query = AffiliateAwardSearch::find()
            ->andWhere(['affiliate_source_uuid' => $affiliateSource->uuid])
            ->joinWith('invoice')
            ->andWhere(['payment_invoice.status' => PaymentInvoice::STATUS_PAID]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);
        return $dataProvider;
    }


    /**
     * after order is created, we can bind award to this order,
     * if we have affiliate
     *
     * @param PaymentInvoice $paymentInvoice
     * @param AffiliateAward $affiliateAward
     * @param PaymentInvoiceItem[] $items
     * @param array $accounting
     * @return void
     */
    public function invoiceAccounting(PaymentInvoice $paymentInvoice, AffiliateAward $affiliateAward, & $items, & $accounting)
    {
        [$feeIn, $feeOut] = $this->affiliateAwardCalculator->calculatorAward($affiliateAward, $accounting);

        if ($feeIn) {
            $affiliateAward->price = $feeIn;
            $affiliateAward->currency = $paymentInvoice->currency;
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_AFFILIATE_FEE_IN] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_AFFILIATE_FEE_IN,
                'price'    => $affiliateAward->price,
                'currency' => $affiliateAward->currency,
            ];
        }
        if ($feeOut) {
            $affiliateAward->price = $feeOut;
            $affiliateAward->currency = $paymentInvoice->currency;
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_AFFILIATE_FEE_OUT] = [
                'type'     => PaymentInvoice::ACCOUNTING_TYPE_AFFILIATE_FEE_OUT,
                'price'    => $affiliateAward->price,
                'currency' => $affiliateAward->currency,
            ];
            // Print include fee
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2]['price']            += $affiliateAward->price;
            $accounting[PaymentInvoice::ACCOUNTING_TYPE_PRINTING_WITH_PACKAGE_V2]['item']->total_line += $affiliateAward->price;
        }
        $paymentInvoice->populateRelation('affiliateAward', $affiliateAward);
    }
}
