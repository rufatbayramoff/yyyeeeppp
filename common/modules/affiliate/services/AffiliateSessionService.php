<?php

namespace common\modules\affiliate\services;

use common\components\DateHelper;
use common\models\AffiliateSource;
use common\models\AffiliateSourceReferrer;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\modules\affiliate\components\AffiliateSourceDetector;
use common\modules\affiliate\models\AffiliateUserSession;
use common\modules\affiliate\repositories\AffiliateSessionRepository;
use Yii;
use yii\base\BaseObject;
use yii\web\Cookie;
use yii\web\Request;
use yii\web\Response;

class AffiliateSessionService extends BaseObject
{
    /**
     * key for cookie
     */
    const AFF_COOKIE_KEY = 'affiliate';

    /**
     *  cookie exipire time
     */
    const AFF_COOKIE_EXPIRE = 60*60*24;

    /** @var AffiliateSourceDetector */
    protected $affiliateSourceDetector;

    /** @var UserIdentityProvider */
    protected $userIdentityProvider;

    /** @var AffiliateSessionRepository */
    protected $affiliateSessionRepository;

    public function injectDependencies(AffiliateSourceDetector $affiliateSourceDetector, UserIdentityProvider $userIdentityProvider, AffiliateSessionRepository $affiliateSessionRepository)
    {
        $this->affiliateSourceDetector    = $affiliateSourceDetector;
        $this->userIdentityProvider       = $userIdentityProvider;
        $this->affiliateSessionRepository = $affiliateSessionRepository;
    }

    /**
     *  Init affiliate session, create if it required
     *
     * @param Request $request Need for detect affiliate source
     * @param Response $response Need fo add cookies
     */
    public function initSession(Request $request, Response $response)
    {
        if ($this->checkAffiliateSession($request, $response)) {
            return;
        }
        $this->reInitAffiliateSource($request, $response);
    }

    /**
     * @param Response $response
     * @param string $cookieString
     */
    public function addCookie(Response $response, string $cookieString)
    {
        $response->getCookies()->add(
            new Cookie(
                [
                    'name'   => self::AFF_COOKIE_KEY,
                    'value'  => $cookieString,
                    'expire' => time() + self::AFF_COOKIE_EXPIRE
                ]
            )
        );
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function checkAffiliateSession(Request $request, Response $response)
    {
        $cookie = $request->cookies->getValue(self::AFF_COOKIE_KEY, false);
        if ($savedCookie = \Yii::$app->session->get(self::AFF_COOKIE_KEY, null)) {
            if (!$cookie) {
                $this->addCookie($response, $savedCookie);
                $cookie = $savedCookie;
            }
        } else {
            if ($cookie) {
                \Yii::$app->session->set(self::AFF_COOKIE_KEY, $cookie);
            }
        }
        return is_string($cookie);
    }

    /**
     * @param Request $request
     * @return AffiliateSource| null
     * @throws \yii\base\InvalidConfigException
     */
    public function getAffiliateSource(Request $request): ?AffiliateSource
    {
        $currentSession = $this->affiliateSessionRepository->getCurrentAffiliateSession();
        if ($currentSession) {
            return $currentSession->getSource();
        }

        $affiliateCookie = \Yii::$app->session->get(self::AFF_COOKIE_KEY);
        if (!$affiliateCookie) {
            $affiliateCookie = $request->cookies->getValue(self::AFF_COOKIE_KEY, '');
            if (!$affiliateCookie) {
                $currentUser = $this->userIdentityProvider->getUser();
                if ($currentUser && $currentUser->affiliateSourceClient) {
                    $affiliateSource = $currentUser->affiliateSourceClient->affiliateSource;
                    if ($affiliateSource->isActive()) {
                        return $affiliateSource;
                    }
                }
                return null;
            }
        }

        $affSession = Yii::createObject(AffiliateUserSession::class);
        $affSession->decodeCookieValue($affiliateCookie);
        $this->affiliateSessionRepository->setCurrentAffiliateSession($affSession);
        return $affSession->getSource();
    }

    public function setAffiliateSource(Response $response, AffiliateSource $affiliateSource)
    {
        $affiliateSession = AffiliateUserSession::create($affiliateSource);
        $cookieString     = $affiliateSession->encodeCookieValue();
        $this->addCookie($response, $cookieString);
        \Yii::$app->session->set(self::AFF_COOKIE_KEY, $cookieString);
        $this->affiliateSessionRepository->setCurrentAffiliateSession($affiliateSession);
    }

    public function reInitAffiliateSource(Request $request, Response $response)
    {
        $affiliateSource = $this->affiliateSourceDetector->detect($request);
        if ($affiliateSource) {
            $this->setAffiliateSource($response, $affiliateSource);
        }
        return $affiliateSource;
    }

    protected function isTreatstockReferrer($referrer)
    {
        return Yii::$app->getModule('intlDomains')->domainManager->isTreatstockDomain($referrer);
    }

    public function registerView(AffiliateSource $affiliateSource)
    {
        $affiliateSource->followers_count++;
        $affiliateSource->safeSave();
        $currentReferrer =  Yii::$app->request->referrer;
        if (!$currentReferrer || $this->isTreatstockReferrer($currentReferrer)) {
            return ;
        }

        $oldestReferrer = null;
        foreach ($affiliateSource->affiliateSourceReferrers as $affiliateReferrer) {
            if (!$oldestReferrer || $oldestReferrer->date>$affiliateReferrer->date) {
                $oldestReferrer = $affiliateReferrer;
            }
            if ($affiliateReferrer->referrer === $currentReferrer) {
                $affiliateReferrer->date = DateHelper::now();
                $affiliateReferrer->safeSave();
                return;
            }
        }
        if ($oldestReferrer && count($affiliateSource->affiliateSourceReferrers)>=AffiliateSource::MAX_REFERRER_COUNT) {
            $oldestReferrer->referrer = $currentReferrer;
            $oldestReferrer->date = DateHelper::now();
            $oldestReferrer->safeSave();
        } else {
            $affiliateReferrer = new AffiliateSourceReferrer();
            $affiliateReferrer->affiliate_source_uuid = $affiliateSource->uuid;
            $affiliateReferrer->referrer = $currentReferrer;
            $affiliateReferrer->date = DateHelper::now();
            $affiliateReferrer->safeSave();
        }
    }
}