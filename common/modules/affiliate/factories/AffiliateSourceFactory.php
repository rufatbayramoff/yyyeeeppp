<?php

namespace common\modules\affiliate\factories;

use common\components\DateHelper;
use common\components\UuidHelper;
use common\models\AffiliateSource;
use common\models\ApiExternalSystem;
use common\models\User;
use Yii;
use yii\base\BaseObject;
use yii\db\Query;

class AffiliateSourceFactory extends BaseObject
{
    protected function getSourceUuid()
    {
        do {
            $uid      = UuidHelper::generateRandCode(7);
            $uid[0]   = strtolower($uid[0]); // Always low case
            $checkUid = (new Query())->select('*')->from(AffiliateSource::tableName())->where(['uuid' => $uid])->one();
        } while ($checkUid);
        return $uid;
    }

    public function create(User $user): AffiliateSource
    {
        $affiliateSource                  = Yii::createObject(AffiliateSource::class);
        $affiliateSource->uuid            = $this->getSourceUuid();
        $affiliateSource->created_at      = DateHelper::now();
        $affiliateSource->utm             = '';
        $affiliateSource->award_type      = AffiliateSource::AWARD_TYPE_TS_FEE_PERCENT;
        $affiliateSource->award_config    = [];
        $affiliateSource->inactive        = 0;
        $affiliateSource->followers_count = 0;
        $affiliateSource->user_id         = $user->id;
        $affiliateSource->populateRelation('company', $user->company);
        return $affiliateSource;
    }

    public function createTypeSource(User $user): AffiliateSource
    {
        $affiliateSource       = $this->create($user);
        $affiliateSource->type = AffiliateSource::TYPE_SOURCE;
        return $affiliateSource;
    }

    public function createTypeUser(User $user): AffiliateSource
    {
        $affiliateSource       = $this->create($user);
        $affiliateSource->type = AffiliateSource::TYPE_USER_UUID;
        return $affiliateSource;
    }

    public function createTypeApi(ApiExternalSystem $apiExternalSystem): AffiliateSource
    {
        $affiliateSource                         = $this->create($apiExternalSystem->bindedUser);
        $affiliateSource->api_external_system_id = $apiExternalSystem->id;
        $affiliateSource->type                   = AffiliateSource::TYPE_API;
        return $affiliateSource;
    }
}