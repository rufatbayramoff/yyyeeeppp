<?php

namespace common\modules\affiliate\factories;

use common\components\DateHelper;
use common\models\AffiliateAward;
use common\models\AffiliateSource;
use common\models\PaymentInvoice;
use Yii;
use yii\base\BaseObject;


class AffiliateAwardFactory extends BaseObject
{
    public function create(AffiliateSource $affiliateSource, PaymentInvoice $paymentInvoice): AffiliateAward
    {
        $affiliateAward = Yii::createObject(AffiliateAward::class);
        $affiliateAward->affiliate_source_uuid =$affiliateSource->uuid;
        $affiliateAward->price = 0;
        $affiliateAward->currency = $paymentInvoice->currency;
        $affiliateAward->created_at = DateHelper::now();
        $affiliateAward->invoice_uuid = $paymentInvoice->uuid;
        return $affiliateAward;
    }
}