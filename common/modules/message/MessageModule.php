<?php

namespace common\modules\message;

use yii\base\Module;


/**
 * Class Module
 *
 * @package app\modules\support
 */
class MessageModule extends Module
{
    public $localDeliveryLogPath = '';
    public $checkDeliveryConfig = [
        'host' => '',
        'port' => '',
        'login' => '',
        'password' => '',
    ];
}
