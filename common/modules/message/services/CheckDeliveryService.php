<?php

namespace common\modules\message\services;

use common\components\DateHelper;
use common\models\FailedDeliveryCheckMailbox;
use common\models\InvalidEmail;
use Yii;
use yii\base\InvalidArgumentException;

class CheckDeliveryService
{
    public function parseFailedAttemptsLog($filePath)
    {
        $content = file_get_contents($filePath);
        $lines   = explode("\n", $content);
        foreach ($lines as $line) {
            $lineInfo = explode(";", $line);
            $email    = $lineInfo[0];
            if (strpos($email, '@') < 1) {
                // Invalid email
                continue;
            }
            $exists = InvalidEmail::find()->where(['email' => $email])->one();
            if (!$exists) {
                InvalidEmail::markAsBad($email, InvalidEmail::TYPE_LOCAL_LOG);
            }
        }
    }

    public function autoCheckDeliveryImap($host, $port, $login, $password)
    {
        $mailbox = imap_open("{" . $host . ":" . $port . "/imap/ssl}INBOX", $login, $password);
        if (!$mailbox) {
            throw new InvalidArgumentException('Mailbox check failed');
        }
        $emails = imap_search($mailbox, 'UNSEEN', SE_UID);

        if ($emails) {
            foreach ($emails as $uid) {
                imap_setflag_full($mailbox, $uid, '\\SEEN', SE_UID);
                $alreadyChecked = FailedDeliveryCheckMailbox::find()->where(['email' => $login, 'message_uid' => $uid])->one();
                if ($alreadyChecked) {
                    continue;
                }

                $headers = imap_headerinfo($mailbox, imap_msgno($mailbox, $uid));
                $message = $this->getBody($mailbox, $uid);
                $subject = imap_utf8($headers->subject);

                $this->unsubscribeByMessageText($headers->fromaddress, $headers->toaddress, $subject, $message);

                $check              = Yii::createObject(FailedDeliveryCheckMailbox::class);
                $check->created_at  = DateHelper::now();
                $check->email       = $login;
                $check->message_uid = $uid . '';
                $check->safeSave();
            }
        }
        imap_close($mailbox);
    }

    protected function getBody($imap, $uid)
    {
        $body = $this->getPart($imap, $uid, "TEXT/PLAIN");
        if ($body !== "") {
            return $body;
        }
        $body = $this->getPart($imap, $uid, "TEXT/HTML");
        return strip_tags($body);
    }

    protected function getPart($imap, $uid, $mimetype, $structure = false, $partNumber = false)
    {
        if (!$structure) {
            $structure = imap_fetchstructure($imap, $uid, FT_UID);
        }
        if ($structure) {
            if ($mimetype == $this->getMimeType($structure)) {
                if (!$partNumber) {
                    $partNumber = 1;
                }
                $text = imap_fetchbody($imap, $uid, $partNumber, FT_UID);
                switch ($structure->encoding) {
                    case 3:
                        return imap_base64($text);
                    case 4:
                        return imap_qprint($text);
                    default:
                        return $text;
                }
            }

            // multipart
            if ($structure->type == 1) {
                foreach ($structure->parts as $index => $subStruct) {
                    $prefix = "";
                    if ($partNumber) {
                        $prefix = $partNumber . ".";
                    }
                    $data = $this->getPart($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
                    if ($data) {
                        return $data;
                    }
                }
            }
        }
        return false;
    }

    protected function getMimeType($structure)
    {
        $primaryMimetype = ["TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER"];

        if ($structure->subtype) {
            return $primaryMimetype[(int)$structure->type] . "/" . $structure->subtype;
        }
        return "TEXT/PLAIN";
    }

    protected function detectIsMailDeliverySubsystem($fromAddress, $toAddress, $subject, $message)
    {
        $fromAddressMarkers = [
            'Mail Delivery Subsystem',
            'Treatstock Postmaster',
        ];
        $subjectMarkers     = [
            'Delivery Status Notification (Failure)',
            'Mail delivery failed'
        ];
        foreach ($fromAddressMarkers as $fromAddressMarker) {
            if (strpos($fromAddress, $fromAddressMarker) !== FALSE) {
                return true;
            }
        }
        foreach ($subjectMarkers as $subjectMarker) {
            if (strpos($subject, $subjectMarker) !== FALSE) {
                return true;
            }
        }
        return false;
    }

    protected function getBadMail($message, $skipEmails): array
    {
        $pattern = "/(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";
        preg_match_all($pattern, $message, $matches);
        $badMails = [];
        foreach ($matches[0] as $email) {
            if (in_array($email, $skipEmails)) {
                continue;
            }
            $badMails[$email] = $email;
        }
        return $badMails;
    }

    protected function unsubscribeByMessageText($fromAddress, $toAddress, $subject, $message)
    {
        if ($this->detectIsMailDeliverySubsystem($fromAddress, $toAddress, $subject, $message)) {
            $badMails = $this->getBadMail($message, [$fromAddress, $toAddress]);
            $this->markBadMails($badMails);
        }
    }

    protected function markBadMails($badMails)
    {
        foreach ($badMails as $badMail) {
            $exists = InvalidEmail::find()->where(['email' => $badMail])->one();
            if ($exists) {
                continue;
            }
            InvalidEmail::markAsBad($badMail, InvalidEmail::TYPE_MAIL_DELIVERY_SYSTEM);
        }
    }
}