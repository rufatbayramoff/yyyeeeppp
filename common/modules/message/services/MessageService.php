<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.07.18
 * Time: 10:21
 */

namespace common\modules\message\services;

use common\components\ArrayHelper;
use common\components\DateHelper;
use common\models\message\builders\TopicBuilder;
use common\models\MsgMember;
use common\models\MsgMessage;
use common\models\MsgTopic;
use common\models\Preorder;
use common\models\StoreOrder;
use common\models\User;
use Yii;
use yii\base\BaseObject;

class MessageService extends BaseObject
{
    /**
     * @param Preorder $preOrder
     * @param StoreOrder $order
     * @throws \yii\base\Exception
     */
    public function rebindMessageTopic(Preorder $preOrder, StoreOrder $order)
    {
        $msgTopic = MsgTopic::findOne(['bind_to' => MsgTopic::BIND_OBJECT_PREORDER, 'bind_id' => $preOrder->id]);
        if ($msgTopic) {
            $msgTopic->bind_to = MsgTopic::BIND_OBJECT_ORDER;
            $msgTopic->bind_id = $order->id;
            $msgTopic->safeSave();
        }
    }

    /**
     * @param Preorder $preorder
     * @param User $userFrom
     * @param User $userTo
     * @param $messageText
     * @param null $topicTitle
     * @throws \yii\base\Exception
     */
    public function sendMessageForPreorder(Preorder $preorder, User $userFrom, User $userTo, $messageText, $topicTitle = null)
    {
        $topic = TopicBuilder::builder()
            ->setInitialUser($userFrom)
            ->to($userTo)
            ->bindObject($preorder)
            ->setTitle($topicTitle)
            ->setMessage($messageText)
            ->create();

    }

    protected function addMembersToBotMessageGroup($topicId, $onlyCompanies=false)
    {
        $existsQuery = MsgMember::find()->where(['topic_id' => $topicId])->select('user_id')->asArray();
        $existsIds  = $existsQuery->all();
        $existsIds  = ArrayHelper::getColumn($existsIds, 'user_id');
        $usersQuery      = User::find()->notSystem()->active()->andWhere(['not in', 'user.id', $existsIds])->select('user.id')->asArray();
        if ($onlyCompanies) {
            $usersQuery->hasCompany();
        }
        $users = $usersQuery->all();

        $msgMembers = [];
        foreach ($users as $user) {
            $msgMember                                = [
                'user_id'                => $user['id'],
                'topic_id'               => $topicId,
                'folder_id'              => 2,
                'original_folder'        => 2,
                'have_unreaded_messages' => 0,
                'is_deleted'             => 0,
            ];
            $msgMembers[$user['id'] . '-' . $topicId] = $msgMember;
        }
        if ($msgMembers) {
            Yii::$app->db->createCommand()->batchInsert(MsgMember::tableName(),
                [
                    'user_id',
                    'topic_id',
                    'folder_id',
                    'original_folder',
                    'have_unreaded_messages',
                    'is_deleted',
                ], $msgMembers
            )->execute();
        }


    }

    protected function addAllNonAdminUsersToBotGroup()
    {
        $this->addMembersToBotMessageGroup(MsgTopic::BOT_MESSAGE_ALL);
        $this->addMembersToBotMessageGroup(MsgTopic::BOT_MESSAGE_COMPANIES, true);
    }

    public function createBotMessage($messageInfo)
    {
        $msgMessage             = \Yii::createObject(MsgMessage::class);
        $msgMessage->user_id    = 1;
        $msgMessage->created_at = DateHelper::now();
        $msgMessage->topic_id   = $messageInfo['topic_id'] == MsgTopic::BOT_MESSAGE_COMPANIES ? MsgTopic::BOT_MESSAGE_COMPANIES : MsgTopic::BOT_MESSAGE_ALL;
        $msgMessage->text       = $messageInfo['text'];
        $msgMessage->safeSave();

        $this->addAllNonAdminUsersToBotGroup();

        MsgMember::updateAll(['have_unreaded_messages' => 1], ['topic_id' => $msgMessage->topic_id]);
        MsgTopic::updateAll(['last_message_time' => DateHelper::now()], ['id'=>$msgMessage->topic_id]);

        return $msgMessage;
    }
}