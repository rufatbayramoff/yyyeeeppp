<?php

namespace common\modules\message\services;

use common\models\Preorder;
use yii\base\BaseObject;

class MessageNotifyService extends BaseObject
{
    /** @var MessageService */
    protected $messageService;

    public const NEW_PREORDER_FROM_PS = 'new_preorder_from_ps';

    public function injectDependencies(
        MessageService $messageService
    )
    {
        $this->messageService = $messageService;
    }


    /**
     * Subsystem for possible templates
     *
     * @param $templateName
     * @param $templateArgs
     */
    public function getMessageTemplateText($templateName, $templateArgs)
    {
        if ($templateName === self::NEW_PREORDER_FROM_PS) {
            return 'Hi, I have sent you a new quote: '.$templateArgs['description'];
        }
    }

    public function notifyNewPreorderFromPs(Preorder $preorder)
    {
        $templateText = $this->getMessageTemplateText(self::NEW_PREORDER_FROM_PS, [
            'description' => $preorder->offer_description
        ]);
        return $this->messageService->sendMessageForPreorder($preorder, $preorder->ps->user, $preorder->user, $templateText, 'New quote #'.$preorder->id.' for you');
    }
}