<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.03.18
 * Time: 11:36
 */

namespace common\modules\convert\converters;

use common\interfaces\Model3dBasePartInterface;

interface ConverterInterface
{
    public function convert(Model3dBasePartInterface $model3dPart): bool;
}