<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.03.18
 * Time: 11:31
 */

namespace common\modules\convert\converters;

use common\components\FileDirHelper;
use common\interfaces\Model3dBasePartInterface;
use common\models\model3d\Model3dBase;
use common\models\repositories\FileRepository;
use common\modules\convert\converters\commands\JsApiFilesConvertCommand;
use common\modules\convert\ConvertModule;
use common\modules\thingPrint\factories\FileFactory;
use common\services\Model3dPartService;
use console\jobs\model3d\ParserJob;
use console\jobs\model3d\RenderJob;
use console\jobs\QueueGateway;

class ConverterJSAPI implements ConverterInterface
{

    /** @var ConvertModule */
    public $module;

    /** @var \common\models\factories\FileFactory */
    public $fileFactory;

    /** @var FileRepository */
    public $fileRepository;


    public function injectDependencies(FileFactory $fileFactory, FileRepository $fileRepository)
    {
        $this->fileFactory = $fileFactory;
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param Model3dBasePartInterface $model3dPart
     * @param $convertResult
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function processConvertedResult(Model3dBasePartInterface $model3dPart, $convertResult)
    {
        if ($convertResult['commonView']) {
            $file = $this->fileFactory->createFileFromPath($convertResult['commonView']);
            $this->fileRepository->save($file);

            $commonViewModel3dPart = Model3dPartService::addModel3dPartFile($model3dPart->model3d, $file);
            $commonViewModel3dPart->qty = $convertResult['parts'] ? 0 : 1;
            $commonViewModel3dPart->is_common_view = 1;
            $commonViewModel3dPart->converted_from_part_id = $model3dPart->id;
            $commonViewModel3dPart->title = $model3dPart->getTitleWithoutExt() . '_common';
            $commonViewModel3dPart->safeSave();
            $pJob = ParserJob::create($file);
            QueueGateway::addFileJob($pJob);
            $rJob = RenderJob::create($file);
            QueueGateway::addFileJob($rJob);
        }

        if ($convertResult['parts']) {
            $i = 1;
            foreach ($convertResult['parts'] as $partPath) {
                $file = $this->fileFactory->createFileFromPath($partPath);
                $this->fileRepository->save($file);

                $commonViewModel3dPart = Model3dPartService::addModel3dPartFile($model3dPart->model3d, $file);
                $commonViewModel3dPart->converted_from_part_id = $model3dPart->id;
                $commonViewModel3dPart->title = $model3dPart->getTitleWithoutExt() . '_part' . $i;
                $commonViewModel3dPart->safeSave();
                $pJob = ParserJob::create($file);
                QueueGateway::addFileJob($pJob);
                $rJob = RenderJob::create($file);
                QueueGateway::addFileJob($rJob);
                $i++;
            }
        }
        $model3dPart->user_status = Model3dBasePartInterface::STATUS_INACTIVE;
        $model3dPart->safeSave();
    }


    /**
     * @param Model3dBasePartInterface $model3dPart
     * @return bool
     * @throws \Exception
     * @throws \yii\base\ErrorException
     */
    public function convert(Model3dBasePartInterface $model3dPart): bool
    {
        $jsApiFilesConvertCommand = new JsApiFilesConvertCommand($this->module);
        $filePath = $model3dPart->file->getLocalTmpFilePath();
        $tempDir = \Yii::getAlias('@frontend/runtime/convert/' . $model3dPart->file->getFileExtension() . '/' . $model3dPart->file->id);
        FileDirHelper::createDir($tempDir);
        $convertResult = $jsApiFilesConvertCommand->convert($filePath, $tempDir);
        if ($convertResult) {
            $this->processConvertedResult($model3dPart, $convertResult);
            return true;
        } else {
            // Add time to restart nodeJs
            sleep(3);
        }
        return false;
    }
}