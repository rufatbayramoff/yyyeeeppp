<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.03.18
 * Time: 16:44
 */

namespace common\modules\convert\converters\commands;

use common\modules\convert\ConvertModule;

class JsApiFilesConvertCommand
{
    /** @var ConvertModule */
    public $module;

    /**
     * @param ConvertModule $module
     */
    public function __construct(ConvertModule $module)
    {
        $this->module = $module;
    }

    /**
     * @param string $filePath
     * @param string $outputDir
     * @return array|null
     */
    public function convert($filePath, $outputDir)
    {
        $headers = [];
        $url = $this->module->converterUrl;
        $resultBaseName = $outputDir . '/res';
        $postFields = 'filepathin=' . $filePath . '&filepathout=' . $resultBaseName;

        $ch = curl_init();

        // No sertificate on r1.tsdev.work
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_TIMEOUT, $this->module->converterTimeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->module->converterTimeout);
        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->debugOut($url, $postFields, $httpCode, $result);
        curl_close($ch);
        $resultArr = json_decode($result, true);
        if (is_array($resultArr) && ($resultArr['result'] == 'ok')) {
            $retVal = [
                'commonView' => array_key_exists('joint', $resultArr) ? $resultArr['joint'] : [],
                'parts'      => array_key_exists('parts', $resultArr) ? $resultArr['parts'] : []
            ];
            return $retVal;
        }
        return null;
    }

    public function debugOut($url, $postFields, $httpCode, $result)
    {
        echo "\nConverterJsApi request: " . $url;
        echo "\nPost fields: " . str_replace('\\/', '/', json_encode($postFields));
        echo "\nAnswer http code: " . $httpCode;
        echo "\nResult: \"" . $result . '"';

    }
}