<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.06.17
 * Time: 15:01
 */

namespace common\modules\convert;

use common\components\BaseModule;
use common\modules\convert\components\Converter;

/**
 * Class ConvertModule
 *
 * @property Converter $converter
 * @package common\modules\convert
 */
class ConvertModule extends BaseModule
{
    public $converterUrl = 'http://127.0.0.1:5858/jsapi/private/convert';
    public $converterTimeout = 120; // Time in seconds

    protected function getComponentsList()
    {
        return [
            'converter' => Converter::class,
        ];
    }


    /**
     * @throws \yii\base\InvalidParamException
     */
    protected function initConfig()
    {

    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();
        $this->initConfig();
        $componentsList = $this->getComponentsList();
        $this->setComponents($componentsList);
        $this->initComponentsModule($componentsList);
    }
}