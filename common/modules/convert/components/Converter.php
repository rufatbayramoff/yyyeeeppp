<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.03.18
 * Time: 10:12
 */

namespace common\modules\convert\components;

use common\components\FileDirHelper;
use common\components\FileTypesHelper;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\File;
use common\modules\convert\converters\ConverterInterface;
use common\modules\convert\converters\ConverterJSAPI;
use common\modules\convert\ConvertModule;
use yii\base\Component;
use yii\base\Module;

class Converter extends Component
{
    /**
     * @var ConvertModule
     */
    protected $module;

    public const CONVERTABLE_EXT = [Model3dBasePartInterface::FORMAT_3MF, Model3dBasePartInterface::OBJ_FORMAT];


    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    public function isConvertableFile(File $file)
    {
        return FileTypesHelper::isFileInExt($file, self::CONVERTABLE_EXT);
    }

    public function isConvertablePart(Model3dBasePartInterface $model3dPart)
    {
        return $this->isConvertableFile($model3dPart->file);
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @return bool
     */
    public function needToBeConverted(Model3dBaseInterface $model3d)
    {
        foreach ($model3d->getActiveModel3dParts() as $mode3dPart) {
            if ($this->isConvertablePart($mode3dPart) && !$mode3dPart->isConverted()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $fileExt
     * @return ConverterInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function getConverterByType($fileExt): ConverterInterface
    {
        //Now only one converter
        return \Yii::createObject(['class'=>ConverterJSAPI::class, 'module' => $this->module]);
    }

    /**
     * @param Model3dBasePartInterface $model3dPart
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function convertPart(Model3dBasePartInterface $model3dPart)
    {
        $fileExt = $model3dPart->file->getFileExtension();
        $convertProcessor = $this->getConverterByType($fileExt);
        $convertResult = $convertProcessor->convert($model3dPart);
        return $convertResult;
    }

    public function convertParts(Model3dBaseInterface $model3d)
    {
        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
            if ($this->isConvertablePart($model3dPart) && !$model3dPart->isConverted()) {
                $this->convertPart($model3dPart);
            }
        }
    }
}
