<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.05.18
 * Time: 16:32
 */

use backend\assets\AngularAppAsset;

/** @var \common\models\Product */
$productInfo = \common\modules\product\serializers\ProductSerializer::serialize($product);

Yii::$app->angular
    ->service(['user', 'notify', 'router'])
    ->controller([
        'product/product-models',
        'product/express-delivery',
    ])
    ->controllerParam('product', $productInfo);

?>
<div class="row" ng-controller="ProductExpressDeliveryController">
    <div class="col-lg-9">
        <hr class="m-b0">
        Express delivery:
        <div class="form-group">
            <div class="col-sm-4">
                <?= _t('site.store', 'Country'); ?>
            </div>
            <div class="col-sm-4">
                <?= _t('site.product', 'First item'); ?>
            </div>
            <div class="col-sm-4">
                <?= _t('site.product', 'Follow item'); ?>
            </div>
        </div>
        <div ng-repeat="expressDelivery in product.expressDeliveries">
            <div class="form-group">
                <div class="col-sm-4">
                    <select class="js-country-select-ajax form-control"
                            name="ProductExpressDelivery[{{expressDelivery.uuid}}][countryId]"
                            ng-model="expressDelivery.counrtryId"
                            ng-init="initSelect2();"
                    ></select>
                </div>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="ProductExpressDelivery[{{expressDelivery.uuid}}][firstItem]" ng-model="expressDelivery.firstItem">
                </div>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="ProductExpressDelivery[{{expressDelivery.uuid}}][followItem]" ng-model="expressDelivery.followItem">
                </div>
            </div>
        </div>
        <button ng-click="addLine()" onclick="return false;"><?= _t('site.product', 'Add express delivery')?></button>
    </div>
</div>


