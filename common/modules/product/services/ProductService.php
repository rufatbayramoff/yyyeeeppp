<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 17:23
 */

namespace common\modules\product\services;

use backend\components\NoAccessException;
use common\components\DateHelper;
use common\models\CompanyBlock;
use common\models\Product;
use common\modules\informer\InformerModule;
use common\modules\informer\models\ProductInformer;
use common\modules\product\components\ProductEmailer;
use common\modules\product\components\ProductRejectForm;
use common\modules\product\interfaces\ProductInterface;
use common\modules\product\repositories\ProductRepository;
use frontend\models\user\UserFacade;
use yii\web\GoneHttpException;

class ProductService
{

    /**
     * @var ProductEmailer
     */
    private $emailer;

    /** @var ProductRepository */
    protected $productRepository;

    public function injectDependencies(ProductEmailer $emailer, ProductRepository $productRepository)
    {
        $this->emailer = $emailer;
        $this->productRepository = $productRepository;
    }

    /**
     * @param Product $product
     * @throws GoneHttpException
     */
    public function tryCanBeOrderedByCurrentUser(Product $product)
    {
        if (!$product->isActive()) {
            throw new GoneHttpException(_t('site.store', 'Product is not active'));
        }
        if (!$product->isPublished()) {
            throw new GoneHttpException(_t('site.store', 'Product is not published'));
        }
        $companyActive = $product->company->isActive() ?? false;
        if($companyActive === false) {
            throw new GoneHttpException(_t('site.store', 'The manufacturer is currently inactive.'));
        }

    }

    /**
     * @param Product $product
     * @throws NoAccessException
     * @throws \yii\web\NotFoundHttpException
     */
    public function tryCanEditProduct($product)
    {
        $user = UserFacade::getCurrentUser();
        if (!$user || !$user->company) {
            throw new NoAccessException(_t('site.product', 'Product is not available.'));
        }
        if (($user->company->id !== $product->company->id) || (!$product->is_active)) {
            throw new NoAccessException(_t('site.product', 'Product is not available.'));
        }
    }

    public function isOwner($user, $product)
    {
        return ($user && $user->company && $user->company->id === $product->company->id);
    }

    /**
     * @param ProductInterface $model
     * @param ProductRejectForm $rejectForm
     */
    public function moderatorReject(ProductInterface $model, ProductRejectForm $rejectForm)
    {
        $rejectForm->saveRejectStatus($model);
        $this->emailer->sendProductModerationReject($model, $rejectForm);
        InformerModule::addInformer($model->user, ProductInformer::class);
    }

    /**
     * @param Product $product
     * @throws \yii\base\Exception
     */
    public function moderatorApprove(Product $product)
    {
        $product->productCommon->product_status = Product::STATUS_PUBLISHED_PUBLIC;
        $product->moderated_at                  = DateHelper::now();
        $product->published_at                  = DateHelper::now();
        $this->productRepository->saveProduct($product);
        $this->emailer->sendModerationApprove($product);
        InformerModule::addInformer($product->user, ProductInformer::class);
    }

    public function canPrivateView(Product $product, $code)
    {
        $codeHas = $product->generatePrivateViewCode();
        return $codeHas == $code;
    }

    public function hasCompanyBlockBinded(Product $product, CompanyBlock $companyBlock)
    {
        $blockIds = $product->getBindedCompanyBlockIds();
        if (in_array($companyBlock->id, $blockIds)) {
            return true;
        }
        return false;
    }
}