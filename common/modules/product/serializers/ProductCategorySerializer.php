<?php
/**
 * User: nabi
 */

namespace common\modules\product\serializers;


use common\components\serizaliators\AbstractProperties;
use common\models\ProductCategory;

class ProductCategorySerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            ProductCategory::class      =>  [
                'id',
                'code',
                'title',
                'fullTitle'
            ]
        ];
    }
}