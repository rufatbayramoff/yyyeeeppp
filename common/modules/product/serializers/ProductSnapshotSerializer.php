<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 02.08.18
 * Time: 15:58
 */

namespace common\modules\product\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\File;
use common\models\ProductSnapshot;
use common\models\SiteTag;

class ProductSnapshotSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            ProductSnapshot::class => [
                'uuid',
                'productUuid' => 'product_uuid',
                'title',
                'authorName'  => function (ProductSnapshot $productSnapshot) {
                    $productSnapshot->getAuthor()->getFullNameOrUsername();
                },
                'createdAt'   => 'created_at',
                'productStatus',
                'productStatusLabel',
                'coverUrl',
                'images'      => function (ProductSnapshot $productSnapshot) {
                    $images = [];
                    foreach ($productSnapshot->getImages() as $image) {
                        $images[] = [
                            'name' => $image->getFileName(),
                            'url'  => $image->getFileUrl()
                        ];
                    }
                    return $images;
                },
                'description' => function (ProductSnapshot $productSnapshot) {
                    return $productSnapshot->getDescription();
                },
                'productTags',
                'publicPageUrl',
                'unitType'
            ],
            SiteTag::class         => [
                'text'
            ]
        ];
    }
}