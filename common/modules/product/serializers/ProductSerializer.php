<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.04.18
 * Time: 11:28
 */

namespace common\modules\product\serializers;

use common\components\FileTypesHelper;
use common\components\serizaliators\AbstractProperties;
use common\interfaces\FileBaseInterface;
use common\models\File;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\models\Product;
use common\models\ProductCertification;
use common\models\ProductDelivery;
use frontend\components\image\ImageHtmlHelper;
use common\models\ProductCategory;

class ProductSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        $product              = [
            'uuid',
            'title',
            'description',
            'coverUrl',
            'productTags',
            'productType',
            'singlePrice'       => 'productCommon.single_price',
            'productStatus',
            'productStatusLabel',
            'unitType'          => function (Product $p) {
                return $p->unit_type ?? Product::UNIT_TYPE_PIECE;
            },
            'unitTypeLabel'     => function (Product $p) {
                return $p->getUnitTypeTitle(1);
            },
            'switchQuoteQty'    => 'switch_quote_qty',
            'productPrices'     => function (Product $p) {
                $returnValue = [];
                if ($p->moq_prices) {
                    foreach ($p->moq_prices as $key => $productPrice) {
                        $productPrice['moq'] = $productPrice['moq'];
                        $returnValue[$key]   = $productPrice;
                    }
                    usort($returnValue, function ($a, $b) {
                        return $a['moq'] < $b['moq'];
                    });
                }
                return $returnValue;
            },
            'currency'          => function (Product $p) {
                return $p->company->currency;
            },
            'minOrderQty'       => function (Product $p) {
                return $p->getMinOrderQty();
            },
            'images'            => function (Product $p) {
                return array_values($p->getImages());
            },
            'attachments'       => function (Product $p) {
                return array_values($p->getAttachmentFiles());
            },
            'videos',
            'customProperties'  => function (Product $s) {
                return $s->custom_properties;
            },
            'isNew'             => function (Product $s) {
                return $s->isNewRecord;
            },
            'deliveryDetails'   => function (Product $product) {
                return [
                    'shipFrom'  => $product->getShipFromLocation() ? $product->getShipFromLocation()->toString() : '',
                    'incoterms' => $product->incoterms,
                ];
            },
            'expressDeliveries' => function (Product $p) {
                return $p->productExpressDeliveries;
            },
            'category',
            'companyId'         => 'company_id'

        ];
        $productCategory      = [
            'id',
            'code',
            'title'
        ];
        $productCertification = [
            'id',
            'title',
            'image'    => '',
            'imageSrc' => function (ProductCertification $cert) {
                $file = $cert->file;
                if (FileTypesHelper::isImage($file)) {
                    return ImageHtmlHelper::getThumbUrl($file->getFileUrl(), 256, 256);
                }
                return null;
            },
            'ceritfier',
            'application',
            'issue_date',
            'expire_date',
        ];
        $productDelivery      = [
            'id',
            'product_uuid',
            'countryIso' => function (ProductDelivery $pd) {
                return $pd->expressDeliveryCountry->iso_code;
            },
            'firstItem'  => 'express_delivery_first_item',
            'nextItem'   => 'express_delivery_following_item',

        ];
        $file                 = [
            'id',
            'uuid',
            'src'     => function (FileBaseInterface $file) {
                if (FileTypesHelper::isImage($file)) {
                    return ImageHtmlHelper::getThumbUrl($file->getFileUrl(), 256, 256);
                }
                return null;
            },
            'name',
            'fileUrl' => function (FileBaseInterface $file) {
                if (FileTypesHelper::isImage($file)) {
                    return ImageHtmlHelper::getThumbUrl($file->getFileUrl(), 256, 256);
                } elseif (FileTypesHelper::isVideo($file)) {
                    return ImageHtmlHelper::getVideoPreviewUrl($file, 256, 256);
                } else {
                    return $file->getFileUrl();
                }
            }
        ];
        return [
            Product::class              => $product,
            ProductCertification::class => $productCertification,
            ProductCategory::class      => $productCategory,
            ProductDelivery::class      => $productDelivery,
            File::class                 => $file
        ];
    }
}