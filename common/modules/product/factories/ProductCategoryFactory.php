<?php
/**
 * User: nabi
 */

namespace common\modules\product\factories;


use backend\modules\importer\etl\loaders\CategoriesMigrateLoader;
use common\components\DateHelper;
use common\models\ProductCategory;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

class ProductCategoryFactory
{
    public function createByLoader(CategoriesMigrateLoader $loader)
    {
        $category = new ProductCategory();
        $category->title = $loader->rootCategory;
        $category->is_active = 1;
        $category->position = 1;
        $category->parent_id = ProductCategory::ROOT_ID;
        $category->parent; // populate relation
        $category->code = Inflector::slug($loader->rootCategory);
        $category->updated_at = DateHelper::now();


        $subSlug = $category->code . '-' . Inflector::slug($loader->subCategory);
        if (strlen($subSlug) > 145) {
            $subSlug = Inflector::slug(StringHelper::truncateWords($loader->subCategory, 2, ''));
        }
        $subcategory = new ProductCategory();
        $subcategory->title = $loader->subCategory;
        $subcategory->is_active = 1;
        $subcategory->position = 1;
        $subcategory->code = $subSlug;
        $subcategory->populateRelation('parent', $category);
        $subcategory->updated_at = DateHelper::now();


        $subsubSlug =  $subcategory->code . '-' .  Inflector::slug($loader->subSubCategory);
        if (strlen($subsubSlug) > 145) {
            $subsubSlug = Inflector::slug(StringHelper::truncateWords($loader->subSubCategory, 2, ''));
        }
        $subSubCategory = new ProductCategory();
        $subSubCategory->title = $loader->subSubCategory;
        $subSubCategory->is_active = 1;
        $subSubCategory->position = 1;
        $subSubCategory->code = $subsubSlug;
        $subSubCategory->populateRelation('parent', $subcategory);
        $subSubCategory->updated_at = DateHelper::now();

        return $subSubCategory;
    }
}