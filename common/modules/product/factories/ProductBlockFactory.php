<?php
/**
 * User: nabi
 */

namespace common\modules\product\factories;

use common\components\DateHelper;
use common\models\ProductBlock;
use common\models\repositories\UserSessionRepository;
use common\models\user\UserIdentityProvider;
use frontend\models\user\FrontUser;
use yii\base\BaseObject;

class ProductBlockFactory extends BaseObject
{

    /**
     * @var FrontUser
     */
    public $user = false;


    public function injectDependencies(UserIdentityProvider $userIdentityProvider, UserSessionRepository $userSessionRepository): void
    {
        if ($this->user === false) {
            $this->user = $userIdentityProvider->getUser();
        }
    }

    /**
     * @return ProductBlock
     */
    public function create(): ProductBlock
    {
        $productBlock = new ProductBlock();
        $productBlock->title = '';
        $productBlock->created_at = DateHelper::now();
        return $productBlock;
    }
}