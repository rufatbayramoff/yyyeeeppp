<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.07.18
 * Time: 16:59
 */

namespace common\modules\product\factories;

use common\components\DateHelper;
use common\components\UuidHelper;
use common\models\Product;
use common\models\ProductSnapshot;
use common\models\ProductSnapshotFile;
use common\modules\product\serializers\ProductSerializer;

class ProductSnapshotFactory
{
    /**
     * @param Product $product
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function createProductSnapshot(Product $product)
    {
        $productSnapshot = \Yii::createObject(ProductSnapshot::class);
        $productSnapshot->uuid = UuidHelper::generateUuid();
        $productSnapshot->product_uuid = $product->uuid;
        $productSerialized = ProductSerializer::serialize($product);
        $productSerialized['userId'] = $product->user_id;
        $productSnapshot->description = ['product' => $productSerialized];
        $productSnapshot->created_at = DateHelper::now();
        $productSnapshotFiles = [];
        foreach ($product->getImageFiles() as $imageFile) {
            $productSnapshotFile = \Yii::createObject(ProductSnapshotFile::class);
            $productSnapshotFile->product_snapshot_uuid = $productSnapshot->uuid;
            $productSnapshotFile->file_uuid = $imageFile->uuid;
            $productSnapshotFiles[] = $productSnapshotFile;
        }
        $productSnapshot->populateRelation('productSnapshotFiles', $productSnapshotFiles);
        return $productSnapshot;
    }
}