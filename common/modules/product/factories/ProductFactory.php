<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.18
 * Time: 15:18
 */

namespace common\modules\product\factories;

use common\components\DateHelper;
use common\components\UuidHelper;
use common\models\GeoCountry;
use common\models\loggers\ProductLogger;
use common\models\Product;
use common\models\ProductCommon;
use common\models\ProductDelivery;
use common\models\repositories\UserSessionRepository;
use common\models\user\UserIdentityProvider;
use common\models\UserLocation;
use common\modules\product\interfaces\ProductInterface;
use frontend\models\user\FrontUser;
use lib\money\Currency;
use yii\base\BaseObject;

class ProductFactory extends BaseObject
{
    /**
     * @var FrontUser
     */
    public $user = false;

    /**
     * @var ProductLogger
     */
    public $logger;


    public function injectDependencies(
        UserIdentityProvider $userIdentityProvider,
        UserSessionRepository $userSessionRepository,
        ProductLogger $logger
    ): void
    {
        if ($this->user === false) {
            $this->user = $userIdentityProvider->getUser();
        }
        $this->logger = $logger;
    }

    /**
     * @return Product
     * @throws \Exception
     */
    public function create(): Product
    {
        $product = new Product();
        $product->uuid = UuidHelper::generateUuid();
        $product->populateRelation('productCommon', new ProductCommon());
        $product->product_common_uid = $product->productCommon->uid = ProductCommon::generateUid();
        $product->productCommon->title = '';
        $product->productCommon->created_at = $product->productCommon->updated_at = DateHelper::now();
        $product->productCommon->product_status = ProductInterface::STATUS_DRAFT;
        $product->productCommon->is_active = 1;
        $product->productCommon->category_id = null;
        $product->productCommon->user_id = $this->user->id ?? null;
        $product->productCommon->company_id = $this->user->company->id ?? null;
        $product->productCommon->price_currency = Currency::USD;

        $this->initShipFrom($product);

        if ($this->user->company) {
            $product->incoterms = $this->user->company->incoterms;
        }


        $productDelivery = new ProductDelivery();
        $productDelivery->product_uuid = $product->uuid;
        $productDelivery->country_id = GeoCountry::getUsa()->id;
        $product->populateRelation('productExpressDelivery', $productDelivery);

        return $product;
    }

    private function initShipFrom(Product $product)
    {
        $shipFromLocation = UserLocation::find()
            ->where(['user_id' => $this->user->id, 'location_type' => UserLocation::LOCATION_TYPE_SHIP_FROM])
            ->orderBy('id desc')
            ->one();
        if ($shipFromLocation) {
            $product->ship_from_id = $shipFromLocation->id;
            $product->populateRelation('shipFrom', $shipFromLocation);
        }
        return $product;
    }
}