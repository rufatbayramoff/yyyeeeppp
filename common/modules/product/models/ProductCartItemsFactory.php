<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.04.18
 * Time: 15:53
 */

namespace common\modules\product\models;

use common\components\order\builder\items\OrderItemsFactory;
use common\components\order\builder\OrderItemAdder;
use common\models\StoreOrder;
use common\models\StoreOrderItem;
use common\services\StoreOrderService;
use lib\money\Currency;
use yii\base\UserException;

class ProductCartItemsFactory implements OrderItemsFactory
{
    /**
     * @var StoreOrderService
     */
    protected $orderService;

    /**
     * @var ProductCart
     */
    private $cart;

    /**
     * CartOrderItemsBuilder constructor.
     *
     * @param ProductCart $cart
     */
    public function __construct(ProductCart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @param StoreOrderService $orderService
     */
    public function injectDependencies(StoreOrderService $orderService)
    {
        $this->orderService = $orderService;
    }


    /**
     * Create order items
     *
     * @param StoreOrder $order
     * @param OrderItemAdder $adder
     * @throws UserException
     */
    public function createItems(StoreOrder $order, OrderItemAdder $adder): void
    {
        $customer = $order->user;
        $cartItems = $this->cart->items;
        $orderItems = [];

        if (count($cartItems) === 0) {
            throw new UserException('No items found to add');
        }

        $company = null;
        foreach ($cartItems as $cartItem) {
            $item = new StoreOrderItem();
            $item->product_uuid = $cartItem->product->uuid;
            $priceMoney = $cartItem->product->getPriceMoneyByQty($cartItem->qty);
            $item->price = $priceMoney?$priceMoney->getAmount():null;
            $item->currency = Currency::USD;
            $item->qty = $cartItem->qty;
            $company = $cartItem->product->user->ps;
            $company = reset($company);

            $adder->add($item);

            $orderItems[] = $item;
        }

        if (!$orderItems) {
            throw new UserException('No items added');
        }

        $orderAttemp = $this->orderService->createAttemptForPs(company: $company, order: $order);
        $order->link('currentAttemp', $orderAttemp);
    }


}