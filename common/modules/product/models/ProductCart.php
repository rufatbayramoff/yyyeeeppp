<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.04.18
 * Time: 10:22
 */

namespace common\modules\product\models;

use common\models\Product;

class ProductCart
{
    /** @var ProductCartItem[] */
    public $items = [];


    public function load($data)
    {
        $this->items = [];
        foreach ($data as $productUuid => $productDetails) {
            $productItem = new ProductCartItem();
            $productItem->product = Product::find()->where(['uuid' => $productUuid])->one();
            $productItem->qty = (int)$productDetails['qty'];
            $this->items[$productUuid] = $productItem;
        }
    }

    public function asArray()
    {
        $array = [];
        foreach ($this->items as $cartItem) {
            $money =$cartItem->product->getPriceMoneyByQty($cartItem->qty);
            $price = $money?$money->getAmount():0;
            $array[$cartItem->product->uuid] = [
                'qty'        => (int)$cartItem->qty,
                'price'      => $price,
                'priceTotal' => $price * $cartItem->qty
            ];
        }
        return $array;
    }

    /**
     * @param Product $product
     */
    public function fixOneIfNotExists(Product $product)
    {
        if (!array_key_exists($product->uuid, $this->items) || (count($this->items) > 1)) {
            $productCartItem = new ProductCartItem();
            $productCartItem->product = $product;
            $productCartItem->qty = $productCartItem->product->getMinOrderQty();
            $this->items = [$product->uuid => $productCartItem];
        }
    }
}