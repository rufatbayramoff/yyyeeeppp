<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.04.18
 * Time: 14:38
 */

namespace common\modules\product\models;

use common\models\Model3d;
use common\models\User;
use common\modules\product\interfaces\ProductInterface;

/**
 * Trait ProductTrait
 *
 * @package common\modules\product\models
 * @mixin Model3d
 */
trait ProductTrait
{
    public function getUuid() : string
    {
        return (string)$this->uuid;
    }
    public function getTitle(): string
    {
        return (string)$this->title;
    }

    public function getUpdatedAt(): string
    {
        return (string)$this->updated_at;
    }

    public function getProductStatus(): string
    {
        return $this->product_status ?: ProductInterface::STATUS_DRAFT;
    }

    public function getProductStatusLabel(): string
    {
        return ProductBase::getProductStatusLabels()[$this->getProductStatus()];
    }

    public function isAvailableInCatalog(): bool
    {
        return in_array($this->getProductStatus(), [ProductInterface::STATUS_PUBLISHED_PUBLIC,
            ProductInterface::STATUS_PUBLISHED_UPDATED,
            ProductInterface::STATUS_PUBLISH_PENDING,
            ]);
    }

    public function getDescription(): string
    {
        return $this->description;
    }
    public function getAuthor(): ?User
    {
        return $this->user;
    }

    /**
     * This model was converted by
     *
     * @return bool
     */
    public function isByConverted(): bool
    {
        foreach ($this->model3dParts as $part) {
            if ($part->convertedFromPart) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool|mixed
     */
    public function isPublished(): bool
    {
        return \in_array($this->getProductStatus(), [
            ProductInterface::STATUS_PUBLISHED_PUBLIC,
            ProductInterface::STATUS_PUBLISH_PENDING,
            ProductInterface::STATUS_PUBLISHED_UPDATED,
            ProductInterface::STATUS_PUBLISHED_DIRECTLY
        ], true);
    }
}