<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.08.18
 * Time: 16:13
 */

namespace common\modules\product\models;

use common\models\Product;

class ProductFillPercentInfo
{
    /** @var Product */
    public $product;

    /** @var float */
    public $fillPercent = 0;

    /**
     * Text description for unfilled product fields
     *
     * @var string[]
     */
    public $unfilledFields = [];
}