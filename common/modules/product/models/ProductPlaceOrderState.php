<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 17:15
 */

namespace common\modules\product\models;

use common\components\order\anonim\AnonimOrderHelper;
use common\models\Product;
use common\models\StoreOrder;
use common\modules\product\services\ProductService;
use frontend\models\model3d\PlaceOrderStateInterface;
use frontend\models\model3d\PlaceOrderStateTrait;
use yii\base\BaseObject;
use yii\web\NotFoundHttpException;

class ProductPlaceOrderState extends BaseObject implements PlaceOrderStateInterface
{
    use PlaceOrderStateTrait {

    }

    public const SESSION_KEY = 'productPlaceOrderState';

    /** @var ProductService */
    public $productService;

    public function injectDependencies(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function setProduct(Product $product)
    {
        $state = $this->getState();
        $state['productUuid'] = $product->uuid;
        $this->saveState($state);
    }

    /**
     * @throws \yii\web\GoneHttpException
     * @throws NotFoundHttpException
     */
    public function getProduct()
    {
        $state = $this->getState();
        if (!array_key_exists('productUuid', $state)) {
            throw new NotFoundHttpException('Product order state not found');
        }
        $product = Product::find()->where(['uuid' => $state['productUuid']])->one();
        if (!$product) {
            throw new NotFoundHttpException('Product order state not found');
        }
        $this->productService->tryCanBeOrderedByCurrentUser($product);
        return $product;
    }

    /**
     * @return ProductCart
     * @throws NotFoundHttpException
     * @throws \yii\web\GoneHttpException
     */
    public function getProductCart()
    {
        $state = $this->getState();
        $productCart = new ProductCart();
        $productCart->load($state['productCart']??[]);
        $product = $this->getProduct();
        $productCart->fixOneIfNotExists($product);
        return $productCart;
    }

    public function setProductCart(ProductCart $productCart)
    {
        $state = $this->getState();
        $state['productCart'] = $productCart->asArray();
        $this->saveState($state);
    }

    public function getDeliveryFormState(ProductDeliveryForm $deliveryForm)
    {
        $state = $this->getState();
        if (array_key_exists('deliveryForm', $state)) {
            // Already was filled
            foreach ($state['deliveryForm'] as $name => $value) {
                $deliveryForm->$name = $value;
            }
        }
    }

    public function setDeliveryFormState(ProductDeliveryForm $deliveryForm)
    {
        $state = $this->getState();
        foreach (get_object_vars($deliveryForm) as $key => $value) {
            if ($key === 'deliveryParams') {
                continue;
            }
            if (is_string($value)) {
                $state['deliveryForm'][$key] = $value;
            }
        }
        $this->saveState($state);
    }

    public function setOrderId($orderId)
    {
        $state = $this->getState();
        $state['orderId'] = $orderId;
        $this->saveState($state);
    }

    /**
     * @return null|StoreOrder
     * @throws \yii\base\UserException
     */
    public function getOrder()
    {
        $state = $this->getState();
        $orderId = $state['orderId'] ?? null;
        if ($orderId) {
            $order = StoreOrder::tryFindByPk($orderId);
            AnonimOrderHelper::checkOrderAccess($order);
            return $order;
        }
        return null;
    }

}