<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.04.18
 * Time: 9:46
 */

namespace common\modules\product\models;

use common\interfaces\machine\DeliveryParamsInterface;
use common\models\Company;
use common\models\DeliveryType;
use common\models\Product;
use common\models\PsMachineDelivery;
use common\models\StoreOrder;
use common\models\UserAddress;
use common\models\UserLocation;
use lib\money\Currency;

class ProductDeliveryParams implements DeliveryParamsInterface
{

    /** @var Product */
    public $product;

    /** @var ProductCart */
    public $productCart;

    /**
     * Return default carrier for this machine
     * One of constat DeliveryType::CARRIER_
     *
     * @return string
     */
    public function getDefaultCarrier(): string
    {
        return DeliveryType::CARRIER_MYSELF;
    }

    /**
     * Is order has free delivery for this machine,
     * for exmple, if it cost more than free delivery print price
     *
     * @param StoreOrder $storeOrder
     * @return boolean
     */
    public function isFreeDelivery(StoreOrder $storeOrder): bool
    {
        return false;
    }

    /**
     * Is machine has only pickup delivery
     *
     * @return boolean
     */
    public function onlyPickup(): bool
    {
        return false;
    }

    /**
     * Is machine has pickup delivery
     *
     * @return boolean
     */
    public function hasDeliveryPickup(): bool
    {
        return false;
    }

    /**
     * Is machine has domestic delivery
     *
     * @return boolean
     */
    public function hasDomesticDelivery(): bool
    {
        return true;
    }

    public function hasInternationalDelivery(): bool
    {
        return false;
    }

    /**
     * Is machine has only domestic delivery
     *
     * @return boolean
     */
    public function onlyDomestic(): bool
    {
        return true;
    }

    /**
     * Return delivery type instance by type
     *
     * @param string $deliveryType Constant like DeliveryType::PICKUP
     * @return PsMachineDelivery|null
     */
    public function getPsDeliveryTypeByCode(string $deliveryType): ?PsMachineDelivery
    {
        $psDelivery = new PsMachineDelivery();
        $psDelivery->carrier = DeliveryType::CARRIER_MYSELF;
        $psDelivery->populateRelation('deliveryType', DeliveryType::findByPk(DeliveryType::STANDART_ID));
        $productCartInfo = $this->productCart->items[$this->product->uuid];
        $productDelivery = $this->product->getProductDelivery();
        $psDelivery->carrier_price =  $productDelivery->getExpressDeliveryByQty($productCartInfo->qty);
        return $psDelivery;
    }

    /**
     * Return UserAddress instance of machine location
     *
     * @return UserAddress
     */
    public function getUserAddress(): UserAddress
    {
        return new UserAddress();
    }

    /**
     * Return location
     *
     * @return UserLocation
     */
    public function getCurrentLocation(): UserLocation
    {
        $location = new UserLocation();
        $location->populateRelation('country', $this->product->productDelivery->expressDeliveryCounrty);
        $location->country_id = $this->product->productDelivery->express_delivery_country_id;
        return $location;
    }

    /**
     *
     * @param string $type
     * @return boolean
     */
    public function canEasyPost($type): bool
    {
        return false;
    }

    /**
     * Get available carriers by type
     *
     * @param string $type
     * @return mixed[]
     */
    public function getCarriers(string $type = 'domestic'): array
    {
        return [
            [
                'id'        => "myself",
                'is_active' => true,
                'title'     => _t('ps.delivery', "Self Ship")
            ]
        ];
    }

    /**
     *
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->product->company->currency;
    }

    /**
     *
     *
     * @return PsMachineDelivery[]
     */
    public function getAvailableDeliverieTypes(): array
    {
        return [];
    }

    public function getExpressDeliveryCountries()
    {
        return [$this->product->productDelivery->expressDeliveryCounrty->iso_code];
    }

    /**
     * @return Company
     */
    public function getSenderCompany(): Company
    {
        return $this->product->company;
    }
}