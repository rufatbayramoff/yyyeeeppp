<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.04.18
 * Time: 15:57
 */

namespace common\modules\product\models;

use common\components\order\builder\pricers\OrderItemPricer;
use common\components\order\builder\StorePricerWrapper;
use common\components\order\history\OrderHistoryService;
use common\models\StoreOrder;
use common\modules\payment\fee\FeeHelper;
use common\modules\payment\fee\FeeHelperInterface;

class ProductCartItemsPricer implements OrderItemPricer
{
    /**
     * @var OrderHistoryService
     */
    protected $history;

    /**
     * @var FeeHelperInterface
     */
    protected $feeHelper;

    /**
     * @var ProductCart
     */
    private $cart;

    public const PUBLIC_PRICE_PERCENT_MORE_1000 = 1;
    public const PUBLIC_PRICE_PERCENT_LESS_1000 = 4;

    /**
     * CartOrderItemsBuilder constructor.
     *
     * @param ProductCart $cart
     */
    public function __construct(ProductCart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @param OrderHistoryService $history
     * @param FeeHelper $feeHelper
     */
    public function injectDependencies(OrderHistoryService $history, FeeHelper $feeHelper)
    {
        $this->history = $history;
        $this->feeHelper = $feeHelper;
    }

    /**
     * Add prices to pricer
     *
     * @param StoreOrder $order
     * @param StorePricerWrapper $pricer
     */
    public function addPrices(StoreOrder $order, StorePricerWrapper $pricer): void
    {
        $items = $this->cart->items;
        $item = reset($items);
        $money = $item->product->getPriceMoneyByQty($item->qty);
        $totalProductPrice = $money ? $money->getAmount() * $item->qty : 0;
        $pricer->addProductPrice($totalProductPrice);

        // get ps fee
        $psFee = $totalProductPrice * $this->feeHelper->getManufacturingFeePercent();
        $pricer->addPsFee($psFee);
    }
}