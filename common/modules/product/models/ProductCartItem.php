<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.04.18
 * Time: 15:27
 */
namespace common\modules\product\models;

use common\models\Product;

class ProductCartItem
{
    /** @var Product */
    public $product;

    /** @var float */
    public $qty;

}