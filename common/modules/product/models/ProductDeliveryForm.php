<?php

namespace common\modules\product\models;

use frontend\models\delivery\DeliveryForm;

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.04.18
 * Time: 11:15
 */
class ProductDeliveryForm extends DeliveryForm
{
    public $allowedCountries = [];

    public function __construct(\common\modules\product\models\ProductDeliveryParams $deliveryParams)
    {
        $this->allowedCountries = $deliveryParams->getExpressDeliveryCountries();
        parent::__construct($deliveryParams);
    }

    public function isInternational()
    {
        return false;
    }


    public function validateAddress($attribute)
    {
        $address = null;
        try {
            $address = $this->getUserAddress();
        } catch (\Exception $exception) {
            $this->addError('country', _t('store.delivery', 'Please select country.'));
            return;
        }

        if ($address->country->iso_code == 'CN' && empty($this->phone)) {
            $this->addError('phone', _t('store.delivery', 'Please specify phone number.'));
            return;
        }
    }

}