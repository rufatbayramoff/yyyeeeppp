<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.03.18
 * Time: 16:15
 */

namespace common\modules\product\models;

use common\modules\product\interfaces\ProductInterface;

class ProductBase
{
    public static function getProductStatusLabels()
    {
        $labels = [
            ProductInterface::STATUS_DRAFT                   => _t('site.store', 'Draft'),
            ProductInterface::STATUS_PUBLISHED_PUBLIC        => _t('site.store', 'Published'),
            ProductInterface::STATUS_PUBLISHED_UPDATED       => _t('site.store', 'Published (updated)'),
            ProductInterface::STATUS_PUBLISH_PENDING         => _t('site.store', 'Pending moderation'),
            ProductInterface::STATUS_PUBLISHED_DIRECTLY      => _t('site.store', 'Shared by link'),
            ProductInterface::STATUS_REJECTED                => _t('site.store', 'Reject'),
            ProductInterface::STATUS_PRIVATE                 => _t('site.store', 'Private'),
            ProductInterface::STATUS_PUBLISH_NEED_MODERATION => _t('site.store', 'Awaiting moderation'),
        ];
        return $labels;
    }


}