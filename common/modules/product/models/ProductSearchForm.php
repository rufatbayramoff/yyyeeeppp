<?php

namespace common\modules\product\models;

use common\models\ProductCategory;
use yii\web\NotFoundHttpException;

class ProductSearchForm extends \yii\base\Model
{
    /** @var ProductCategory */
    public $category;

    /** @var string */
    public $search;

    /** @var string */
    public $listSort;

    /** @var string */
    public $sortDirection;

    /** @var array */
    public $tags = [];

    /** @var string  */
    public $type = '';

    public $dfFilters = [];

    public $panelLayoutTemplate = 'product';

    public function populateCategory($categoryCode)
    {
        $this->category = ProductCategory::find()->where(['code' => $categoryCode, 'is_active' => 1])->one();
    }

    public function setSearchPanelTemplate($layout)
    {

        $this->panelLayoutTemplate = $layout;
    }

    public function getSearchPanelTemplate()
    {
        return $this->panelLayoutTemplate;
    }

    public function getSearchText()
    {
        if ($this->search) {
            return $this->search;
        }
        if ($this->tags) {
            return substr(implode(', ',$this->tags), 0, -2);
        }
        return '';
    }

    public function load($data, $formName = null)
    {
        $this->search = $data['text'] ?? '';
        $this->type   = $data['type'] ?? '';
        if (array_key_exists('tags', $data)) {
            $this->tags = [];
            foreach ($data['tags'] as $tag) {
                $this->tags[] = (string)$tag;
            }
        }
    }

}