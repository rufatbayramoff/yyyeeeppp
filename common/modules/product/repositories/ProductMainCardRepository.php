<?php

namespace common\modules\product\repositories;

use common\models\ProductMainCard;

class ProductMainCardRepository
{
    public function groupCards(): array
    {
        $result = [];
        $cards = ProductMainCard::find()->with(['productCommon'])->orderBy('position')->all();
        foreach ($cards as $card) {
            $result[$card->type][] = $card->productCommon;
        }
        return $result;
    }
}