<?php
/**
 * User: nabi
 */

namespace common\modules\product\repositories;


use common\components\ArrayHelper;
use common\models\ProductCategory;
use common\models\ProductCommon;

class ProductCategoryRepository
{

    public function add(ProductCategory $category)
    {
        if ($category->parent) {
            $parent = $this->add($category->parent);
            $category->populateRelation('parent', $parent);
        }
        if (!$category->isNewRecord) {
            return $category;
        }

        // check if category already exists
        $oldCategory = ProductCategory::findOne(['code' => $category->code]);
        if (!$oldCategory) {
            if ($category->parent || $category->parent_id) {
                if ($category->parent->isNewRecord) {
                    $category->parent->safeSave();
                }
                $category->parent_id = $category->parent->id;
            }
            $category->safeSave();
            return $category;
        }
        return $oldCategory;
    }

    public function findCategoryByTitle($category)
    {
        $productCategory = ProductCategory::find()
            ->where(['title' => $category])
            ->one();
        return $productCategory;
    }

    public function findCategoryByTitles($category, $subcategory, $subSubCategory)
    {
        $productCategory = ProductCategory::find()
            ->joinWith('parent as parent')
            ->where(['product_category.title' => $subSubCategory])
            ->andWhere(['parent.title' => $subcategory])->one();
        return $productCategory;
    }

    public function getAllMap()
    {
        if ($categoriesMap = app('cache')->get('productCategoriesAllMap')) {
            return $categoriesMap;
        }

        $categories    = ProductCategory::find()->active()->all();
        $categoriesMap = ArrayHelper::map($categories, 'id', 'fullTitle');

        app('cache')->set('productCategoriesAllMap', $categoriesMap);
        return $categoriesMap;
    }

    public function getFinalMap()
    {
        if ($categoriesMap = app('cache')->get('productCategoriesFinalMap')) {
            return $categoriesMap;
        }

        $categories    = ProductCategory::find()->active()->finalLevel()->all();
        $categoriesMap = ArrayHelper::map($categories, 'id', 'fullTitle');

        app('cache')->set('productCategoriesFinalMap', $categoriesMap);
        return $categoriesMap;
    }

    public function getModel3dCategoriesIds()
    {
        if ($allCategories = app('cache')->get('productCategoriesModel3d')) {
            return $allCategories;
        }

        $level0Query = ProductCommon::find()
            ->select('distinct(`category_id`) as category_id')
            ->joinWith('model3d')
            ->active()
            ->statusPublished()
            ->activeCompany()
            ->andWhere('`model3d`.`id` is not null')
            ->andWhere('`product_common`.`category_id` is not null');
        $level0      = $level0Query->createCommand()->queryAll();
        $level0 = ArrayHelper::getColumn($level0, 'category_id');
        $allCategories = $currentLevel = $level0;

        do {
            $currentLevel = array_flip($currentLevel);
            unset($currentLevel[1]);
            $currentLevel = array_keys($currentLevel);

            $topLevel = ProductCategory::find()
                ->select('distinct(parent_id) as parent_id')
                ->where(['id' => $currentLevel])
                ->createCommand()->queryAll();
            $topLevel = ArrayHelper::getColumn($topLevel, 'parent_id');
            $allCategories = array_merge($allCategories, $topLevel);
            $currentLevel = $topLevel;
        } while (count($topLevel));
        $allCategories = array_keys(array_flip($allCategories));
        sort($allCategories);
        app('cache')->set('productCategoriesModel3d', $allCategories);
        return $allCategories;
    }

    public function subCategories(ProductCategory $productCategory)
    {
        $subCategories = ProductCategory::find()->childsOf($productCategory)->active()->visible()->all();
        $allSubCategories = $subCategories;
        foreach ($subCategories as $subCategory) {
            $allSubCategories = array_merge($allSubCategories, $this->subCategories($subCategory));
        }
        return $allSubCategories;
    }
}