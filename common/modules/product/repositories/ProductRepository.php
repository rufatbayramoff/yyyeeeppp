<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.04.18
 * Time: 14:40
 */

namespace common\modules\product\repositories;

use common\components\ArrayHelper;
use common\models\Company;
use common\models\loggers\ProductLogger;
use common\models\Product;
use common\models\ProductBlock;
use common\models\ProductBlockImage;
use common\models\ProductCategory;
use common\models\ProductCategoryKeyword;
use common\models\ProductCommon;
use common\models\ProductFile;
use common\models\ProductImage;
use common\models\ProductSiteTag;
use common\models\query\PsQuery;
use common\models\repositories\FileRepository;
use common\models\SiteTag;
use common\models\User;
use common\modules\dynamicField\services\DynamicFieldService;
use common\modules\product\interfaces\ProductInterface;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

class ProductRepository extends BaseObject
{
    /** @var FileRepository */
    public $fileRepository;

    /** @var DynamicFieldService */
    public $dynamicFieldService;

    /** @var ProductLogger */
    protected $logger;

    public const PAGE_SIZE = 20;

    public function injectDependencies(FileRepository $fileRepository, DynamicFieldService $dynamicFieldService, ProductLogger $logger)
    {
        $this->fileRepository      = $fileRepository;
        $this->dynamicFieldService = $dynamicFieldService;
        $this->logger              = $logger;
    }

    /**
     * @param ProductRepositoryCondition $condition
     * @return \common\components\BaseActiveQuery
     */
    public function getProductsQuery(ProductRepositoryCondition $condition)
    {
        $productCommonQuery = ProductCommon::find();
        if ($condition->isPublished) {
            $productCommonQuery->andWhere('product_common.company_id is not null');
            $productCommonQuery->joinWith(['company' => function (PsQuery $psQuery) {
                $psQuery->active();
                return $psQuery;
            }]);
            $productCommonQuery->andWhere(['product_common.product_status' => [ProductInterface::STATUS_PUBLISHED_PUBLIC, ProductInterface::STATUS_PUBLISHED_UPDATED]]);
        }
        if ($condition->type === ProductInterface::TYPE_MODEL3D) {
            $productCommonQuery->joinWith('model3d');
            $productCommonQuery->andWhere('model3d.id is not null');
        } elseif ($condition->type === ProductInterface::TYPE_PRODUCT) {
            $productCommonQuery->joinWith('product');
            $productCommonQuery->andWhere('product.id is not null');
        }

        if ($condition->onlyOriginalModels) {
            $productCommonQuery->joinWith('model3d');
            $productCommonQuery->andWhere('model3d.original_model3d_id is null');
        }

        if ($condition->isActive) {
            $productCommonQuery->andWhere(['product_common.is_active' => $condition->isActive]);
        }

        if ($condition->categoryIdList) {
            $productCommonQuery->andWhere(['product_common.category_id' => $condition->categoryIdList]);
        }

        if ($condition->companyId) {
            $productCommonQuery->andWhere(['product_common.company_id' => $condition->companyId]);
        } elseif ($condition->userId) {
            $productCommonQuery->andWhere(['product_common.user_id' => $condition->userId]);
        }

        if ($condition->status) {
            $productCommonQuery->andWhere(['product_common.product_status' => $condition->status]);
        }

        $siteTagsIdList = [];
        if ($condition->searchText) {
            if ($condition->tags) {
                $siteTags = SiteTag::find()->select('id')->where(['text' => $condition->tags])->asArray()->all();
            } else {
                $siteTags = SiteTag::find()->select('id')->where(['text' => $condition->searchText])->asArray()->all();
            }
            $siteTagsIdList = ArrayHelper::getColumn($siteTags, 'id');

            $productCommonQuery->joinWith('product.productSiteTags');
            $productCommonQuery->joinWith('model3d.model3dTags');
            $productCommonQuery->andWhere(['or',
                ['like', 'product_common.title', $condition->searchText],
                ['product_site_tag.site_tag_id' => $siteTagsIdList],
                ['model3d_tag.tag_id' => $siteTagsIdList],
            ]);
        } else {
            $siteTags = SiteTag::find()->select('id')->where(['text' => $condition->tags])->asArray()->all();
            if ($siteTags) {
                $siteTagsIdList = ArrayHelper::getColumn($siteTags, 'id');

                $productCommonQuery->joinWith('product.productSiteTags');
                $productCommonQuery->joinWith('model3d.model3dTags');
                $productCommonQuery->andWhere(['or',
                    ['product_site_tag.site_tag_id' => $siteTagsIdList],
                    ['model3d_tag.tag_id' => $siteTagsIdList],
                ]);
            }
        }
        $productCommonQuery->groupBy('product_common.uid');

        if ($condition->sortBy == ProductRepositoryCondition::SORT_BY_PRICE) {
            $productCommonQuery->orderBy('product_common.updated_at ' . $condition->sortDirection);
        } else {
            $productCommonQuery->orderBy('product_common.updated_at ' . $condition->sortDirection);

        }
        return $productCommonQuery;
    }

    /**
     * @param ProductRepositoryCondition $condition
     * @param int $pageSize
     * @return ActiveDataProvider
     */
    public function getProductsProvider(ProductRepositoryCondition $condition, $pageSize = 50): ActiveDataProvider
    {
        $query = $this->getProductsQuery($condition);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ]
        ]);
        return $dataProvider;
    }


    public function getByUuid($uuid): Product
    {
        $product = Product::find()->where(['uuid' => $uuid])->one();
        if (!$product) {
            \Yii::info('Not found product: ' . VarDumper::dumpAsString($uuid), 'base_ar');
            throw new NotFoundHttpException();
        }
        return $product;
    }

    /**
     * @param $uid
     * @return ProductInterface
     * @throws NotFoundHttpException
     */
    public function getProductCommonByUid($uid): ProductInterface
    {
        $productCommon = ProductCommon::find()->where(['uid' => $uid])->one();
        if (!$productCommon) {
            \Yii::info('Not found product: ' . VarDumper::dumpAsString($uid), 'base_ar');
            throw new NotFoundHttpException();
        }
        return $productCommon->model3d ?: $productCommon->product;
    }

    public function saveProduct(Product $product)
    {
        $isNewRecord = $product->isNewRecord;
        if (!$isNewRecord) {
            $this->logger->logProductCommon($product->productCommon, 'productCommon');
        }
        $product->productCommon->safeSave();
        $relatedRecords = $product->relatedRecords;

        if ($product->dynamicFieldValues) {
            $product->dynamic_fields_values = $this->dynamicFieldService->formDbValueJson($product->dynamicFieldValues);
        }
        if (!$isNewRecord) {
            $this->logger->log($product);
        }
        $product->safeSave();
        if ($isNewRecord) {
            $this->logger->create($product);
        }

        if (array_key_exists('productDelivery', $relatedRecords)) {
            $productDelivery = $relatedRecords['productDelivery'];
            $productDelivery->safeSave();
        }

        if (array_key_exists('productBind', $relatedRecords)) {
            $productBind = $relatedRecords['productBind'];
            $productBind->safeSave();
        }

        if (array_key_exists('productImages', $relatedRecords)) {
            /* @var $productImage ProductImage */
            foreach ($relatedRecords['productImages'] as $productImage) {
                if ($productImage->file->forDelete) {
                    ProductImage::deleteAll(['file_uuid' => $productImage->file->uuid]);
                    $this->fileRepository->delete($productImage->file);
                } else {
                    if ($productImage->isNewRecord) {
                        $this->fileRepository->save($productImage->file);
                    }
                    $productImage->safeSave();
                }
            }
        }

        if (array_key_exists('productAttachments', $relatedRecords)) {
            /* @var $productImage ProductImage */
            foreach ($relatedRecords['productAttachments'] as $productAttachment) {
                if ($productAttachment->file->forDelete) {
                    ProductFile::deleteAll(['file_id' => $productAttachment->file->id]);
                    $this->fileRepository->delete($productAttachment->file);
                } else {
                    if ($productAttachment->isNewRecord) {
                        $this->fileRepository->save($productAttachment->file);
                        $productAttachment->file_id = $productAttachment->file->id;
                    }
                    $productAttachment->safeSave();
                }
            }
        }

        if (array_key_exists('siteTags', $relatedRecords)) {
            $oldTagsIds = ArrayHelper::getColumn($product->productSiteTags, 'site_tag_id');
            $bindedTags = array_filter(ArrayHelper::getColumn($relatedRecords['siteTags'], 'id'));
            if (empty($bindedTags)) {
                $bindedTags = [0];
            }
            ProductSiteTag::deleteAll(['AND', 'product_uuid=:uuid', ['NOT IN', 'site_tag_id', $bindedTags]], [':uuid' => $product->uuid]);
            foreach ($relatedRecords['siteTags'] as $siteTag) {
                if (in_array($siteTag->id, $oldTagsIds)) {
                    continue;
                }
                $siteTag->safeSave();
                $productSiteTag               = new ProductSiteTag();
                $productSiteTag->product_uuid = $product->uuid;
                $productSiteTag->site_tag_id  = $siteTag->id;
                $productSiteTag->safeSave();
            }
        }

        // make visible categories for this product
        if ($product->category && !$product->category->is_visible) {
            $product->category->setAsVisible();
        }
    }

    /**
     * @param ProductBlock $productBlock
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     */
    public function saveProductBlock(ProductBlock $productBlock)
    {
        $relatedRecords = $productBlock->relatedRecords;
        $productBlock->safeSave();
        if (array_key_exists('imageFiles', $relatedRecords)) {
            foreach ($relatedRecords['imageFiles'] as $imageFile) {
                if ($imageFile->forDelete) {
                    ProductBlockImage::deleteAll(['file_uuid' => $imageFile->uuid]);
                    $this->fileRepository->delete($imageFile);
                } else {
                    if ($imageFile->isNewRecord) {
                        $this->fileRepository->save($imageFile);
                        $productImage            = new ProductBlockImage();
                        $productImage->file_uuid = $imageFile->uuid;
                        $productImage->block_id  = $productBlock->id;
                        $productImage->safeSave();
                    }
                }
            }
        }
    }

    /**
     * @param Product $product
     * @param int $limit
     * @return array
     */
    public function getSeeAlsoProductsByProduct(Product $productIn, $limit = 5)
    {
        $products = Product::find()
            ->company($productIn->productCommon->company)
            ->isAvailableInCatalog()
            ->andWhere(['NOT', ['uuid' => $productIn->getUuid()]])->limit($limit)->all();
        $result   = [];
        foreach ($products as $product) {
            if (!$product->isActive()) {
                continue;
            }
            $result[] = $product;
        }
        return $result;
    }

    /**
     * @param Product $product
     *
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function setProductImagesRotate(Product $product): void
    {
        foreach ($product->productImages as $img) {
            $img->setImageRotate();
        }
    }

    public function getCategoriesByKeyWords($title): array
    {
        $titleWordsString = str_replace(['.', '?', '!', ',', ';', '~', '`', '\'', '@', '"', '#', '№', '$', '%', '^', ':', '&', '*', '(', ')', '{', '}', '[', ']', '|', '/', '\\', '<', '>'], ' ', $title);
        $titleWords       = explode(' ', $titleWordsString);
        $keywords         = ProductCategoryKeyword::find()->active()->all();
        $categories       = ProductCategory::find()->finalLevel()->all();
        $categories       = ArrayHelper::index($categories, 'id');

        $resultCategoriesIdList = [];
        foreach ($titleWords as $titleWord) {
            $titleWordLen = mb_strlen($titleWord);
            if (strlen($titleWord) < 2) {
                continue;
            }
            $titleWord = mb_strtolower($titleWord);
            foreach ($keywords as $keywordInfo) {
                $keyword    = $keywordInfo['keyword'];
                $categoryId = $keywordInfo['product_category_id'];
                $keywordLen = mb_strlen($keyword);
                $diffLen    = abs($keywordLen - $titleWordLen);
                if ((($titleWordLen <= 3) && ($keyword === $titleWord)) ||
                    (($titleWordLen > 3) && ($diffLen < 4) && (strpos($titleWord, $keyword) !== FALSE))) {
                    if (!array_key_exists($categoryId, $resultCategoriesIdList)) {
                        $resultCategoriesIdList[$categoryId] = $keywordLen;
                    } else {
                        $resultCategoriesIdList[$categoryId] += $keywordLen;
                    }
                }
            }
        }
        arsort($resultCategoriesIdList);
        $resultCategories = [];
        foreach ($resultCategoriesIdList as $id => $count) {
            $resultCategories[] = $categories[$id];
        }

        $resultCategories = array_slice($resultCategories, 0, 10);

        return $resultCategories;
    }

    public function deleteProductFile($productFile)
    {
        $file = $productFile->file;
        $productFile->delete();
        $this->fileRepository->delete($file);
    }
}