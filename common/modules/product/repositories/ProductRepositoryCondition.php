<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 10.04.18
 * Time: 10:02
 */

namespace common\modules\product\repositories;

use backend\models\search\ProductCommonSearch;
use common\components\ArrayHelper;
use common\models\ProductCategory;
use common\modules\product\models\ProductSearchForm;
use yii\web\NotFoundHttpException;

class ProductRepositoryCondition
{
    public $userId;
    public $companyId;
    public $isActive;
    public $isPublished;
    public $type; // ProductCommonType
    public $onlyOriginalModels;
    public $status;

    /** @var array */
    public $tags = [];

    public $categoryIdList;
    /**
     * @var string Common search text by titles, description, etc...
     */
    public $searchText = '';


    public const SORT_BY_PRICE        = 'price';
    public const SORT_BY_PUBLISH_DATE = 'date';

    public const SORT_ASC  = 'asc';
    public const SORT_DESC = 'desc';

    public $sortBy = self::SORT_BY_PUBLISH_DATE;
    public $sortDirection = self::SORT_DESC;

    public $sortByViewModel3d;


    public function populateByForm(ProductSearchForm $productSearchForm)
    {
        $this->categoryIdList = $productSearchForm->category ?
            (ArrayHelper::getColumn($productSearchForm->category->allChilds(), 'id') + [$productSearchForm->category->id]) : null;
        $this->searchText = $productSearchForm->search;
        $this->tags  = $productSearchForm->tags;
        $this->type  = $productSearchForm->type;
        $this->onlyForPublicCatalog();
    }


    public function onlyForPublicCatalog()
    {
        $this->isActive    = 1;
        $this->isPublished = 1;
    }
}