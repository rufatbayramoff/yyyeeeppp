<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.07.18
 * Time: 17:44
 */
namespace common\modules\product\repositories;

use common\models\ProductSnapshot;

class ProductSnapshotRepository
{
    /**
     * @param ProductSnapshot $productSnapshot
     * @throws \yii\base\Exception
     */
    public function save(ProductSnapshot $productSnapshot)
    {
        $productSnapshotFiles = $productSnapshot->productSnapshotFiles;
        $productSnapshot->safeSave();
        foreach ($productSnapshotFiles as $productSnapshotFile) {
            $productSnapshotFile->safeSave();
        }
    }
}