<?php

namespace common\modules\product\populators;

use common\components\AbstractPopulator;
use common\components\DateHelper;
use common\models\Product;
use common\models\ProductBlock;
use common\models\SiteTag;
use common\modules\dynamicField\models\populators\DynamicFieldValuePopulator;
use common\modules\product\interfaces\ProductInterface;
use yii\helpers\HtmlPurifier;
use yii\helpers\Json;
use yii\web\UploadedFile;

class ProductBlockPopulator extends AbstractPopulator
{

    /**
     * @param ProductBlock $productBlock
     * @param $data
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function populate(ProductBlock $productBlock, $data)
    {
        $formName = $productBlock->formName();
        if (array_key_exists($formName, $data)) {
            $productForm = $data[$formName];
            if ($productForm) {
                $this->loadAttributes($productBlock, $productForm);
                if (array_key_exists('imageFiles', $productForm)) {
                    $this->populateFileAttributes(
                        $productForm['imageFiles'],
                        function ($uuid) use ($productBlock) {
                            return $productBlock->getImageFileByUuid($uuid);
                        }
                    );
                }
            }
        }
        return $this;
    }

    /**
     * @param ProductBlock $productBlock
     * @param $data
     */
    public function loadAttributes(ProductBlock $productBlock, $data)
    {
        $this->populateAttributes(
            $productBlock,
            $data,
            [
                'title',
                'is_visible',
                'position',
                'content',
            ]
        );
        if (!empty($data['content'])) {
            $productBlock->content = HtmlPurifier::process($data['content']);
        }
    }
}