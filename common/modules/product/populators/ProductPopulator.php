<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.04.18
 * Time: 15:05
 */

namespace common\modules\product\populators;

use common\components\AbstractPopulator;
use common\components\DateHelper;
use common\components\UrlHelper;
use common\models\Product;
use common\models\ProductExpressDelivery;
use common\models\SiteTag;
use common\modules\dynamicField\models\populators\DynamicFieldValuePopulator;
use common\modules\product\interfaces\ProductInterface;
use yii\helpers\HtmlPurifier;
use yii\helpers\Json;
use yii\web\UploadedFile;

class ProductPopulator extends AbstractPopulator
{

    /** @var DynamicFieldValuePopulator */
    public $dynamicFieldValuePopulator;

    /**
     * @param DynamicFieldValuePopulator $dynamicFieldValuePopulator
     */
    public function injectDependencies(DynamicFieldValuePopulator $dynamicFieldValuePopulator)
    {
        $this->dynamicFieldValuePopulator = $dynamicFieldValuePopulator;
    }

    /**
     * @param Product $product
     * @param $data
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function populate(Product $product, $data)
    {
        $formName = $product->formName();
        if (array_key_exists($formName, $data)) {
            $productForm = $data[$formName];
            if ($productForm) {
                $this->loadAttributes($product, $productForm);
                if (array_key_exists('productTags', $productForm)) {
                    $this->loadSiteTags($product, $productForm['productTags']);
                }

                if (array_key_exists('dynamicFields', $productForm)) {
                    $this->dynamicFieldValuePopulator->populate($product->getDynamicValues(), $productForm['dynamicFields']);
                }

                if (!array_key_exists('images', $productForm)
                    && array_key_exists('ProductForm', $_FILES) && array_key_exists('images', $_FILES['ProductForm']['name'])) {
                    // Fix php separation stream to POST and _FILES parameters
                    $productForm['images'] = [];
                }
                if (!array_key_exists('attachments', $productForm)
                    && array_key_exists('ProductForm', $_FILES) && array_key_exists('attachments', $_FILES['ProductForm']['name'])) {
                    $productForm['attachments'] = [];
                }


                if (array_key_exists('images', $productForm)) {
                    $this->loadImages($product, $productForm['images']);
                }
                if (array_key_exists('attachments', $productForm)) {
                    $this->loadAttachments($product, $productForm['attachments']);
                }
                if (array_key_exists('submitMode', $productForm)) {
                    $submitMode = $productForm['submitMode'];
                    if ($submitMode === 'publish') {
                        $product->productCommon->product_status = ProductInterface::STATUS_PUBLISH_PENDING;
                        if ($product->getProductCategory() && $product->getProductCategory()->need_premoderation) {
                            $product->productCommon->product_status = ProductInterface::STATUS_PUBLISH_NEED_MODERATION;
                        }

                    } elseif ($submitMode === 'unpublish') {
                        $product->productCommon->product_status = ProductInterface::STATUS_DRAFT;
                    }
                }

            }
        }
        if (array_key_exists('ProductCategory', $data) && array_key_exists('id', $data['ProductCategory'])) {
            $product->productCommon->category_id = (int) $data['ProductCategory']['id'];
        }
        $this->loadExpressDeliveries($product, $data);
        return $this;
    }


    public function loadAttributes(Product $product, $data)
    {
        $this->populateAttributes($product, $data, [
            'supply_ability',
            'supply_ability_time',
            'avg_production',
            'avg_production_time',
            'product_package',
            'outer_package',
            'incoterms',
            'switch_quote_qty',
            'unit_type'
        ]);

        if (!empty($data['title'])) { // only for first tab check
            $data['title'] = trim($data['title']);
            if ($data['title']) {
                $product->switch_quote_qty = null;
                if (array_key_exists('switch_quote_qty', $data)) {
                    $product->switch_quote_qty = (int)$data['switch_quote_qty'];
                }
            }
            $product->productCommon->title = $data['title'];
        }

        if (!empty($data['categoryId'])) {
            $product->productCommon->category_id = $data['categoryId'];
        }

        if (!empty($data['single_price'])) {
            $product->productCommon->single_price = $data['single_price'];
        }

        if (!empty($data['ship_from'])) {
            $product->setShipFromLocation(Json::decode($data['ship_from']));
        }
        if (!empty($data['moq_prices'])) {
            $product->setMoqPrices((array)Json::decode($data['moq_prices']));
        }
        if (!empty($data['custom_properties'])) {
            $product->setCustomProperties((array)Json::decode($data['custom_properties']));
        }
        if (!empty($data['videos'])) {
            $product->setProductVideos((array)Json::decode($data['videos']));
        }
        if (array_key_exists('description', $data)) {
            $product->productCommon->description = HtmlPurifier::process($data['description']);
        }
        $product->deleteCoverPhotoFile = (array_key_exists('deleteCoverPhotoFile', $data) && $data['deleteCoverPhotoFile']) ? 1 : 0;
    }

    public function loadImages(Product $product, $images)
    {
        $existsImages = $product->productImages;
        foreach ($existsImages as $productImage) {
            // Mark all as for delete
            $productImage->file->forDelete = true;
        }
        foreach ($existsImages as $productImage) {
            foreach ($images as $imagePos => $image) {
                if (\is_array($image) && array_key_exists('uuid', $image) && $productImage->file->uuid == $image['uuid']) {
                    $productImage->pos = $imagePos;
                    $productImage->file->forDelete = false;
                }
            }
        }
    }

    /**
     * @param Product $product
     * @param $attachments
     */
    public function loadAttachments(Product $product, $attachments)
    {
        $existsImages = $product->productAttachments;
        foreach ($existsImages as $productImage) {
            $productImage->file->forDelete = true;
        }
        foreach ($existsImages as $productImage) {
            foreach ($attachments as $imagePos => $image) {
                if (\is_array($image) && array_key_exists('id', $image) && $productImage->file->id == $image['id']) {
                    $productImage->file->forDelete = false;
                }
            }
        }
    }

    /**
     * @param Product $product
     * @param $data
     *
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function populateImagesRotateOptions(Product $product, $data)
    {
        $formName = $product->formName();
        if (array_key_exists($formName, $data)) {
            $productForm = $data[$formName];

            if ($productForm) {
                if (array_key_exists('imagesOptions', $productForm)) {
                    $imagesOptions = $productForm['imagesOptions'];

                    $productImages = $product->productImages;

                    foreach ($productImages as $productImage) {
                        if (!empty($imagesOptions[$productImage->pos]) && array_key_exists('rotate', $imagesOptions[$productImage->pos])) {
                            $productImage->rotate = $imagesOptions[$productImage->pos]['rotate'];
                        }
                    }
                }
            }
        }

        return $this;
    }


    /**
     * @param Product $product
     * @param $data
     * @throws \yii\base\InvalidConfigException
     */
    public function loadExpressDeliveries(Product $product, $data)
    {
        $deliveryFormName = 'ProductExpressDelivery';
        if (array_key_exists($deliveryFormName, $data)) {
            $deliveryForm = $data[$deliveryFormName];
            $currentDeliveries = [];
            foreach ($product->productExpressDeliveries as $productExpressDelivery) {
                if (array_key_exists($productExpressDelivery->uuid, $deliveryForm)) {
                    // Exists
                    $this->populateAttributes($productExpressDelivery,
                        $deliveryForm[$productExpressDelivery->uuid], ['country_id', 'first_item', 'following_item', 'is_deleted']
                    );
                    $currentDeliveries[$productExpressDelivery->uuid] = $productExpressDelivery;
                }
            }

            foreach ($deliveryForm as $uuid => $expressDeliveryInfo) {
                {
                    if (array_key_exists($uuid, $currentDeliveries)) {
                        $deliveryInfo = $deliveryForm[$productExpressDelivery->uuid];
                        if ($deliveryInfo['is_deleted']) {
                            continue;
                        }

                        $newProductExpressDelivery = \Yii::createObject(ProductExpressDelivery::class);
                        $this->populateAttributes($newProductExpressDelivery,
                            $deliveryInfo, ['country_id', 'first_item', 'following_item']
                        );
                        $newProductExpressDelivery->product_uuid = $product->uuid;
                    }
                }
            }
        }
    }

    public function loadSiteTags(Product $product, $productTags)
    {
        if (!$productTags) {
            $productTags = [];
        }
        $siteTags = [];
        $productTagsNew = [];
        foreach ($productTags as $productTag) {
            $productTagsSep = $productTag;
            if (!is_array($productTagsSep)) {
                $productTagsSep = explode(",", $productTag);
            }
            if ($productTagsSep) {
                foreach ($productTagsSep as $p) {
                    $productTagsNew[] = $p;
                }
            }
        }
        foreach ($productTagsNew as $productTag) {
            $productTag = trim($productTag);
            if (ctype_digit($productTag)) { // WTF: Form send id or text!
                $siteTag = SiteTag::find()->where(['id' => intval($productTag)])->withoutStaticCache()->one();
            } else {
                if (strlen($productTag) < 2) {
                    continue;
                }
                $siteTag = SiteTag::find()->where(['text' => $productTag])->withoutStaticCache()->one();
            }
            if (!$siteTag) {
                $siteTag = new SiteTag();
                $siteTag->text = $productTag;
            }
            $siteTags[] = $siteTag;
        }
        $product->populateRelation('siteTags', $siteTags);
    }

    /**
     * @param $product
     * @param $newProductStatus
     */
    public function populateAdminForm(Product $product, $data)
    {
        $formName = $product->formName();
        if (array_key_exists($formName, $data)) {
            $productForm = $data[$formName];
            if ($productForm) {
                if (array_key_exists('product_status', $productForm)) {
                    $newProductStatus = $data['ProductForm']['product_status'];
                    $product->moderated_at = DateHelper::now();
                    if (($product->productCommon->product_status !== ProductInterface::STATUS_PUBLISHED_PUBLIC) && ($newProductStatus === ProductInterface::STATUS_PUBLISHED_PUBLIC)) {
                        $product->published_at = DateHelper::now();
                    }
                    $product->productCommon->product_status = $newProductStatus;
                }
                if (array_key_exists('formTitle', $productForm)) {
                    $product->productCommon->title = $productForm['formTitle'];
                }
                if (array_key_exists('formDescription', $productForm)) {
                    $product->productCommon->description = $productForm['formDescription'];
                }
                if (array_key_exists('is_active', $productForm)) {
                    $product->productCommon->is_active = (int)$productForm['is_active'];
                }
                if (array_key_exists('formCategoryId', $productForm)) {
                    $product->productCommon->category_id = (int)$productForm['formCategoryId'];
                }
                if (array_key_exists('moqPricesJson', $productForm)) {
                    $product->setMoqPrices(json_decode($productForm['moqPricesJson'], true));
                }
                if (array_key_exists('customPropertiesJson', $productForm)) {
                    $product->setCustomProperties(json_decode($productForm['customPropertiesJson'], true));
                }
                if (array_key_exists('videosJson', $productForm)) {
                    $videos = json_decode($productForm['videosJson'], true);
                    $newVideos = [];
                    foreach ($videos as $video) {
                        $video['videoId'] = UrlHelper::getParameter($video['url'],'v');
                        $video['thumb'] = 'https://img.youtube.com/vi/'.$video['videoId'].'/default.jpg';
                        $newVideos[]=$video;
                    }
                    $product->videos = $newVideos;
                }
            }
        }

    }
}