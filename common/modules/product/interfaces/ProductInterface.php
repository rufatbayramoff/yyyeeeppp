<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.03.18
 * Time: 11:49
 */

namespace common\modules\product\interfaces;

use common\models\Company;
use common\models\File;
use common\models\ProductCommon;
use common\models\SiteTag;
use common\models\User;
use lib\money\Money;

/**
 * Interface ProductInterface
 *
 * @property ProductCommon $productCommon;
 * @property string $moderated_at
 * @property string $published_at
 * @package common\interfaces
 *
 */
interface ProductInterface
{
    /**
     *  New created product
     */
    public const STATUS_DRAFT = 'draft';

    /**
     * Product published in catalog
     */
    public const STATUS_PUBLISHED_PUBLIC = 'published_public';

    /**
     * Published product was updated and need to be post checked
     */
    public const STATUS_PUBLISHED_UPDATED = 'published_updated';

    /**
     * product published but pre moderation is required.
     */
    public const STATUS_PUBLISH_NEED_MODERATION = 'publish_needmoderation';

    /**
     * Product published and waiting for post moderation
     */
    public const STATUS_PUBLISH_PENDING = 'publish_pending';

    /**
     * Product showed to anybody, who have direct link
     */
    public const STATUS_PUBLISHED_DIRECTLY = 'published_directly';

    /**
     * Product is rejected by moderator
     */
    public const STATUS_REJECTED = 'rejected';

    public const PUBLIC_STATUSES = [
        self::STATUS_PUBLISHED_PUBLIC,
        self::STATUS_PUBLISHED_UPDATED
    ];

    public const PENDING_STATUSES = [
        self::STATUS_PUBLISH_NEED_MODERATION,
        self::STATUS_PUBLISH_PENDING
    ];

    /**
     * Product will be available for only specific users
     */
    public const STATUS_PRIVATE = 'private';


    /** Based by model3d */
    public const TYPE_MODEL3D = 'model3d';

    /** Based by product */
    public const TYPE_PRODUCT = 'product';


    public function getCuid(): string;

    public function getAuthor(): ?User;

    public function getCompanyManufacturer(): ?Company;

    public function getUpdatedAt(): string;

    public function getModeratedAt(): string;

    public function getPublishedAt(): string;

    public function isPublished(): bool;

    public function isActive(): bool;

    public function getProductStatus(): string;

    /**
     * Get human readable status
     *
     * @return string
     */
    public function getProductStatusLabel(): string;

    /**
     * Disable publis_updated allow status
     */
    public function setCantBePublisehedUpdated(): void;

    /**
     * Analyze product changed attibutes, for allow save product as published_updated
     *
     * @return bool
     */
    public function isCanBePublishUpdated(): bool;


    public function isAvailableInCatalog(): bool;

    public function getCoverUrl(): ?string;

    /**
     * @return File[]
     */
    public function getImages(): array;

    public function getTitle(): string;

    public function getDescription(): string;

    /**
     * @return SiteTag[]
     */
    public function getProductTags(): array;

    public function getProductType(): string;

    public function getUuid() : string;

    public function getPublicPageUrl(array $params = []): string;

    public function getPriceMoneyByQty($qty): ?Money;

    public function getMoqFromPriceMoney(): ?Money;

    public function getMinOrderQty(): float;

    public function getUnitTypeTitle($qty): string;

    public function getUnitTypeCode(): string;
}