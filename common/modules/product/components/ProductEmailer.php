<?php
/**
 * User: nabi
 */

namespace common\modules\product\components;

use common\components\Emailer;
use common\modules\product\interfaces\ProductInterface;
use lib\message\MessageServiceInterface;

/**
 * Class ProductEmailer
 * send emails referring to products
 * publishing, rejecting, ...
 *
 * @package common\modules\product\components
 */
class ProductEmailer
{
    /**
     * @var MessageServiceInterface
     */
    private $sender;

    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @param Emailer $emailer
     */
    public function injectDependencies(Emailer $emailer)
    {
        $this->sender = \Yii::$app->message;
        $this->emailer = $emailer;
    }

    /**
     * @param ProductInterface $product
     * @param ProductRejectForm $rejectForm
     */
    public function sendProductModerationReject(ProductInterface $product, ProductRejectForm $rejectForm): void
    {
        $params = [
            'username'     => $product->getAuthor()->username,
            'productTitle' => $product->getTitle(),
            'psName'       => $product->getAuthor()->company->title,
            'companyName'  => $product->getAuthor()->company->title,
            'productLink'  => param('server') . '/mybusiness/products/product/edit?uuid=' . $product->getUuid(),
            'reason'       => $rejectForm->getReason(),
            'comment'      => $rejectForm->getComment(),
            'rejectReason' => $rejectForm->getText(),
        ];
        $this->sender->send($product->getAuthor(), 'company.product.moderation.reject', $params);
    }

    /**
     * @param ProductInterface $product
     */
    public function sendModerationApprove(ProductInterface $product): void
    {
        $params = [
            'username'     => $product->getAuthor()->username,
            'productTitle' => $product->getTitle(),
            'productLink'  => param('server') . '/mybusiness/products/product/edit?uuid=' . $product->getUuid(),
            'companyName'  => $product->getAuthor()->company->title
        ];
        $this->sender->send($product->getAuthor(), 'company.product.moderation.approved', $params);
    }
}