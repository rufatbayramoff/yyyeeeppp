<?php
/**
 * User: nabi
 */

namespace common\modules\product\components;

/**
 * Class CustomPropertiesImporter
 *
 * @package common\modules\product\components
 */
class CustomPropertiesImporter
{
    public $result = [];
    public $delimeters = [':', '-', "\t", ',', ';', "\n"];

    /**
     * @param $text
     * @return array
     */
    public function importProperties($text)
    {
        $lines = explode("\n", $text);

        $result = [];
        $delimeter = $this->findPropertyDelimeter($lines);
        $prevKey = null;
        foreach ($lines as $line) {
            $row = explode($delimeter, $line);
            $key = $row[0];
            $val = $row[1] ?? false;
            if ($val === false && $prevKey && empty($result[$prevKey])) {
                $result[$prevKey] = $key;
            } else {
                $result[$key] = trim($val);
                $prevKey = $key;
            }
        }
        return $result;
    }

    /**
     * @param $lines
     * @return false|int|string
     */
    private function findPropertyDelimeter($lines)
    {
        $found = array_combine($this->delimeters, array_fill(0, count($this->delimeters), 0));
        foreach ($lines as $k => $line) {
            $detectDelimeter = count_chars($line, 1);
            foreach ($detectDelimeter as $i => $val) {
                $char = chr($i);
                if (in_array($char, $this->delimeters)) {
                    $found[chr($i)]++;
                }
            }
        }
        $top = array_search(max($found), $found);
        return $top;
    }
}