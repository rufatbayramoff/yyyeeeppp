<?php
/**
 * User: nabi
 */

namespace common\modules\product\components;

use common\components\exceptions\AssertHelper;
use common\components\reject\BaseRejectForm;
use common\models\CompanyService;
use common\models\Product;
use common\models\SystemReject;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\services\ProductService;
use Yii;
use yii\helpers\Json;

/**
 * Reject Form
 * Display in popup window
 *
 */
class ProductRejectForm extends BaseRejectForm
{
    private $user;
    /**
     * Save status and create log item
     *
     * @param $id
     * @param $userId
     */
    public function saveRejectStatus(Product $product)
    {
        $product->productCommon->product_status = Product::STATUS_REJECTED;
        $productRepository = Yii::createObject(ProductRepository::class);
        $productRepository->saveProduct($product);
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::PRODUCT_REJECT;
    }

    public function setUser($currentUser)
    {
        $this->user = $currentUser;
    }
}