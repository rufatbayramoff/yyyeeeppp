<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.08.18
 * Time: 16:30
 */

namespace common\modules\product\components;

use common\components\DateHelper;
use common\models\Product;
use common\modules\product\models\ProductFillPercentInfo;
use yii\helpers\Html;

class ProductFillPercentCalculator
{
    /**
     * @param Product $product
     * @return ProductFillPercentInfo
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function calculateFillPercent(Product $product): ProductFillPercentInfo
    {
        $fillPercentInfo = \Yii::createObject(ProductFillPercentInfo::class);

        $countImages = count($product->getImages());
        $videoPoints = count($product->videos ?? []) * 4;
        $dynamicFieldPoints = 0;
        foreach ($product->getDynamicValues() as $dynamicFieldValue) {
            if ($dynamicFieldValue->value) {
                if ($dynamicFieldValue->value) {
                    $dynamicFieldPoints += 4;
                }
            }
        };
        $imagePoints = ($countImages > 0 ? 1 : 0) + $countImages;
        $imagePoints = $imagePoints < 10 ? $imagePoints : 10;

        $fillSettings = [
            'title'                 => [
                'title'  => Html::a(_t('site.store', 'Title'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid]),
                'points' => $product->title ? 5 : 0
            ],
            'category'              => [
                'title'  => Html::a(_t('mybusiness.product', 'Category'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid, 'focus'=>'category']),
                'points' => $product->category ? 5 : 0
            ],
            'tags'                  => [
                'title'  => Html::a(_t('mybusiness.product', 'Tags'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid, 'focus'=>'tags']),
                'points' => $product->siteTags ? 3 : 0
            ],
            'images'                => [
                'title'  => Html::a(_t('site.store', 'Photos'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid, 'focus'=>'photos']),
                'points' => $imagePoints // First photo - 2 point, following photos - 1 point for each
            ],
            'unitType'              => [
                'title'  => Html::a(_t('mybusiness.product', 'Unit type'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid, 'focus'=>'unitType']),
                'points' => $product->unit_type ? 2 : 0
            ],
            'price'                 => [
                'title'  => Html::a(_t('mybusiness.product', 'Prices'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid, 'focus'=>'prices']),
                'points' => $product->moq_prices ? 6 : 0
            ],
            'supplyAbility'         => [
                'title'  => Html::a(_t('mybusiness.product', 'Supply ability'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid, 'focus'=>'supplyAbility']),
                'points' => $product->supply_ability ? 1 : 0
            ],
            'avgProduction'         => [
                'title'  => Html::a(_t('mybusiness.product', 'Avg. production time'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid, 'focus'=>'avgProduction']),
                'points' => $product->avg_production ? 1 : 0
            ],
            'description'           => [
                'title'  => Html::a(_t('mybusiness.product', 'Description'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid, 'focus'=>'description']),
                'points' => $product->description ? 7 : 0
            ],

            'specification'         => [
                'title'  => Html::a(_t('mybusiness.product', 'Specifications'), ['/mybusiness/products/product/edit-properties', 'uuid'=>$product->uuid]),
                'points' => $product->custom_properties ? 20 : 0,
            ],
            'incoterms'             => [
                'title'  => Html::a(_t('mybusiness.product', 'Incoterms'), ['/mybusiness/products/product/edit-delivery', 'uuid'=>$product->uuid]),
                'points' => $product->incoterms ? 2 : 0
            ],
            'productPackage'        => [
                'title'  => Html::a( _t('site.store', 'Product packaging'), ['/mybusiness/products/product/edit-delivery', 'uuid'=>$product->uuid]),
                'points' => $product->product_package ? 2 : 0
            ],
            'outerPackage'          => [
                'title'  => Html::a( _t('site.store', 'Shipping packaging'), ['/mybusiness/products/product/edit-delivery', 'uuid'=>$product->uuid]),
                'points' => $product->outer_package ? 2 : 0
            ],
            'shipFrom'              => [
                'title'  => _t('mybusiness.product', 'Ship from'),
                'points' => $product->shipFrom ? 4 : 0
            ],
            'cadFiles'              => [
                'title'  => Html::a(_t('mybusiness.product', 'CAD Files'), ['/mybusiness/products/product-files', 'uuid'=>$product->uuid]),
                'points' => $product->cadFiles ? 10 : 0
            ],
            'videos'                => [
                'title'  => Html::a(_t('mybusiness.product', 'Videos'), ['/mybusiness/products/product/edit-videos', 'uuid'=>$product->uuid]),
                'points' => ($videoPoints <= 10) ? $videoPoints : 10,
            ],
            'certifications'        => [
                'title'  => Html::a(_t('mybusiness.product', 'Certifications'), ['/mybusiness/products/product-certifications', 'uuid'=>$product->uuid]),
                'points' => ($product->productCertifications || $product->getBindedCompanyCertifications()) ? 10 : 0
            ],
            'additionalInformation' => [
                'title'  => Html::a(_t('mybusiness.product', 'Additional Information'), ['/mybusiness/products/product-blocks', 'uuid'=>$product->uuid]),
                'points' => ($product->productBlocks||$product->getBindedCompanyBlocks()) ? 10 : 0,
            ],
            'dynamicFields'         => [
                'title'  => _t('mybusiness.product', 'Attributes'),
                'points' => ($dynamicFieldPoints > 20) ? 20 : $dynamicFieldPoints
            ]
        ];



        $debugInfo = '';
        foreach ($fillSettings as $key => $fillSetting) {
            if ($product->isNewRecord) {
                $fillSetting['title'] = strip_tags($fillSettings[$key]['title']);
            }
            $addUnfilled = $fillSetting['points'] === 0;
            if (!$addUnfilled && $fillSetting['points'] < 10) {
                if ($key == 'images') {
                    $addUnfilled = true;
                    $fillSetting['title'] =   Html::a(_t('mybusiness.product', 'Add more photos'), ['/mybusiness/products/product/edit', 'uuid'=>$product->uuid]);
                }
                if ($key == 'videos') {
                    $addUnfilled = true;
                    $fillSetting['title'] =   Html::a(_t('mybusiness.product', 'Add more videos'), ['/mybusiness/products/product/edit-videos', 'uuid'=>$product->uuid]);
                }
                $fillPercentInfo->fillPercent += $fillSetting['points'];
            }
            if ($addUnfilled) {
                if (($key === 'dynamicFields') && (!$product->getDynamicValues())) {
                    continue;
                }
                $fillPercentInfo->unfilledFields[$key] = $fillSetting['title'];
            } else {
                $fillPercentInfo->fillPercent += $fillSetting['points'];
                $debugInfo .= $fillSetting['title'] . ':' . $fillSetting['points'] . "\n";
            }
        }
        if (\Yii::$app->setting->get('product.productFillInfoLog')) {
            $dataLog = 'Product: ' . $product->uuid . "\nDate: " . DateHelper::now() . "\nTotal:" . $fillPercentInfo->fillPercent . "\n\n" . $debugInfo;
            file_put_contents(\Yii::getAlias('@runtime') . '/logs/product_fill_info.log', $dataLog);
        }

        $fillPercentInfo->fillPercent = ($fillPercentInfo->fillPercent < 100) ? $fillPercentInfo->fillPercent : 100;
        if ($fillPercentInfo->fillPercent === 100) {
            unset($fillPercentInfo->unfilledFields['images']);
            unset($fillPercentInfo->unfilledFields['videos']);
        }
        return $fillPercentInfo;
    }
}