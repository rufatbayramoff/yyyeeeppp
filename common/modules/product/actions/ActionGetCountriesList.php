<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.05.18
 * Time: 10:15
 */

namespace common\modules\product\actions;

use common\components\ArrayHelper;
use common\models\GeoCountry;
use yii\base\Action;

/**
 * Class ActionGetCountriesList
 *
 * @package common\product\actions
 * @property \common\components\BaseController $controller
 */
class ActionGetCountriesList extends Action
{
    public function run()
    {
        $q = \Yii::$app->request->get('q');
        $query = GeoCountry::find();
        if ($q) {
            $query->andWhere(['like', 'title', $q]);
        }
        $countries = [];
        foreach ($query->all() as $country) {
            $countries[] = [
                'id'    => $country->id,
                'text' => $country->title
            ];
        }
        return $this->controller->jsonSuccess(['items' => $countries]);
    }
}