<?php

namespace common\modules\tsCertification\services;


use common\components\DateHelper;
use common\models\Company;
use common\models\CompanyService;
use common\models\Printer;
use common\models\TsCertificationClass;
use yii\base\BaseObject;

class TsCertificationService extends BaseObject
{
    public function deleteClassPrintersList(Printer $printer)
    {
        $printer->ts_certification_class_id = TsCertificationClass::DEFAULT_PRINTERS_CERT;
        $printer->safeSave();
    }

    public function clearClassPrintersList(TsCertificationClass $tsCertification)
    {
        foreach ($tsCertification->printers as $printer) {
            $printer->ts_certification_class_id = TsCertificationClass::DEFAULT_PRINTERS_CERT;
            $printer->safeSave();
        }
    }

    public function importClassPrintersList(TsCertificationClass $tsCertification, $printersList)
    {
        $printersListArr = explode("\n", $printersList);
        foreach ($printersListArr as $printerItem) {
            $printerId = (int)$printerItem;
            if ($printerId) {
                $printer = Printer::tryFindByPk($printerId);
                if ($printer) {
                    $printer->ts_certification_class_id = $tsCertification->id;
                    $printer->safeSave();
                }
            }
        }
    }

    public function fixExpired()
    {
        /** @var CompanyService[] $companyServices */
        $companyServices = CompanyService::find()->where([
            'and',
            ['certification' => CompanyService::CERT_TYPE_VERIFIED],
            [
                '<',
                'certification_expire',
                DateHelper::now()
            ]
        ])->all();
        foreach ($companyServices as $companyService) {
            $companyService->certification = CompanyService::CERT_TYPE_EXPIRED;
            $companyService->safeSave();
        }
    }
}