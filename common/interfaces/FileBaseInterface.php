<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.02.18
 * Time: 9:27
 */

namespace common\interfaces;

use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

/**
 * Interface FileBaseInterface
 *
 * @property $path;
 * @property $is_public;
 * @property $oldAttributes;
 *
 * @package common\interfaces
 * @mixin BaseActiveRecord
 */
interface FileBaseInterface
{
    public const TYPE_FILE_SIMPLE = 'simple';
    public const TYPE_FILE_ADMIN  = 'admin';

    public const STATUS_ACTIVE   = 'active';
    public const STATUS_INACTIVE = 'inactive';

    public const SCENARIO_CREATE               = 'create';
    public const PATH_SALT                     = 'gi5o4Alrpq'; // Salt to name the file path to the files could not bust bust
    public const EXPIRE_HASH_SALT              = 'b9ghsjSWeleq';
    public const PATH_VERSION1                 = 'v1';
    public const PATH_VERSION2                 = 'v2';
    public const PATH_VERSION3                 = 'v3';
    public const FILE_EXPIRE_LINK_DEFAULT_TIME = 60 * 60;  // 1 hour
    public const FILE_EXPIRE_LINK_NEVER        = 60 * 60 * 24 * 31 * 12; // 1 year

    public const PUBLIC_FILE_SERVER  = 'localhost';
    public const PRIVATE_FILE_SERVER = 'storage';

    public function testAllowExtension(): bool;

    public function setOwner($className, $fieldName): void;

    public function getFileUrl($expireTime = FileBaseInterface::FILE_EXPIRE_LINK_DEFAULT_TIME): string;

    public function setFixedPath($path): void;

    public function produceFilePath(): void;

    public function produceStoredName(): void;

    public function getStoredName(): string;

    public function testIsCompressed($localPath): void;

    public function setIgnoreAccessDateMode($mode): void;

    public function getLocalTmpFilePath(): string;

    public function setFileContentAndSave($content): void;

    public function getFileContent(): ?string;

    public function getFileName(): string;

    public function getCleanFileNameV3(): string;

    public function getFileNameWithoutExtension(): string;

    public function getFileExtension(): string;

    public function publishFileToServerAndSave($filePath): void;

    public function setPublicMode($isPublic): void;

    public function setAllowedExtensions($extensions): void;

    public function expireBySeconds($seconds): void;

    public function testIsExists(): bool;

    public function getMd5NameSize(): string;

    public function setInactiveStatus(): void;

    public function setActiveStatus(): void;
}