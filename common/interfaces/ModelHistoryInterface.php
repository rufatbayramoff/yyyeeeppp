<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.08.17
 * Time: 10:20
 */

namespace common\interfaces;

interface ModelHistoryInterface
{
    public function getId();
    public function getModelId();
    public function getCreatedAt();
    public function getUserId();
    public function getActionId();
    public function getSource();
    public function getResult();
    public function getSourceResultDiffRemove();
    public function getSourceResultDiffAdd();
    public function getComment();

    public function setModelId($id);
    public function setCreatedAt(string $date);
    public function setUserId(int $userId);
    public function setActionId(string $actionId);
    public function setSource(string $value);
    public function setResult(string $value);
    public function setComment(string $comment);
}