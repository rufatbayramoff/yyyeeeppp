<?php
/**
 * Date: 02.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\interfaces;


interface DiscountsResolverInterface
{
    public function getDiscounts();

}