<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.08.16
 * Time: 11:19
 */


namespace common\interfaces;

use yii\db\ActiveRecordInterface;

/**
 * Interface BaseActiveRecordInterface
 *
 * @property array $attributes
 * @package common\interfaces
 */
interface BaseActiveRecordInterface extends ActiveRecordInterface
{
    public function getErrors();

    public function safeSave();
}