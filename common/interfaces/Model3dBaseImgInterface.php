<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.08.16
 * Time: 18:29
 */


namespace common\interfaces;

use common\models\File;
use yii\db\ActiveRecord;

/**
 * Interface Model3dBaseImgInterface
 *
 * @property integer $id
 * @property File $file
 */
interface Model3dBaseImgInterface
{

    const JPG_FORMAT = 'jpg';
    const JPEG_FORMAT = 'jpeg';
    const PNG_FORMAT = 'png';
    const GIF_FORMAT = 'gif';

    const ALLOWED_FORMATS = [self::JPG_FORMAT, self::JPEG_FORMAT, self::PNG_FORMAT, self::GIF_FORMAT];

    public function isActive();

    public function getTitle();

    public function setAttachedModel3d(Model3dBaseInterface $model3d): void;

    /**
     * @return Model3dBaseImgInterface|ActiveRecord
     */
    public function getOriginalModel3dImg(): Model3dBaseImgInterface;

    public function getAttachedModel3d(): Model3dBaseInterface;

    public function getUid();
}
