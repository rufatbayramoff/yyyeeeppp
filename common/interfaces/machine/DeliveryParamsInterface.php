<?php
/**
 * Created by mitaichik
 */

namespace common\interfaces\machine;


use common\models\Company;
use common\models\PsMachineDelivery;
use common\models\StoreOrder;
use common\models\UserAddress;
use common\models\UserLocation;

/**
 * Interface MachineDeliveryParamsInterface
 * @package common\interfaces\machine
 */
interface DeliveryParamsInterface
{
    /**
     * Return default carrier for this machine
     * One of constat DeliveryType::CARRIER_
     *
     * @return string
     */
    public function getDefaultCarrier(): string;

    /**
     * Is order has free delivery for this machine,
     * for exmple, if it cost more than free delivery print price
     *
     * @param StoreOrder $storeOrder
     * @return boolean
     */
    public function isFreeDelivery(StoreOrder $storeOrder): bool;

    /**
     * Is machine has only pickup delivery
     *
     * @return boolean
     */
    public function onlyPickup(): bool;

    /**
     * Is machine has pickup delivery
     *
     * @return boolean
     */
    public function hasDeliveryPickup(): bool;

    /**
     * Is machine has domestic delivery
     *
     * @return boolean
     */
    public function hasDomesticDelivery(): bool;

    public function hasInternationalDelivery(): bool;

    /**
     * Is machine has only domestic delivery
     *
     * @return boolean
     */
    public function onlyDomestic(): bool;

    /**
     * Return delivery type instance by type
     *
     * @param string $deliveryType Constant like DeliveryType::PICKUP
     * @return PsMachineDelivery|null
     */
    public function getPsDeliveryTypeByCode(string $deliveryType): ?PsMachineDelivery;

    /**
     * Return UserAddress instance of machine location
     *
     * @return UserAddress
     */
    public function getUserAddress(): UserAddress;

    /**
     * Return location
     *
     * @return UserLocation
     */
    public function getCurrentLocation(): UserLocation;

    /**
     *
     * @param string $type
     * @return boolean
     */
    public function canEasyPost($type): bool;

    /**
     * Get available carriers by type
     *
     * @param string $type
     * @return mixed[]
     */
    public function getCarriers(string $type = 'domestic'): array;

    /**
     *
     *
     * @return string
     */
    public function getCurrency(): string;

    /**
     *
     *
     * @return PsMachineDelivery[]
     */
    public function getAvailableDeliverieTypes(): array;

    /**
     * @return Company
     */
    public function getSenderCompany(): Company;
}