<?php
/**
 * Date: 01.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\interfaces;

interface RepositoryInterface
{
    public function put($object);
    public function remove($object);
    public function get($id);
    public function getAll();
}