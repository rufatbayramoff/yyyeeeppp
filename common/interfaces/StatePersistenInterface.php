<?php
/**
 * Date: 01.12.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\interfaces;


interface StatePersistenInterface
{
    public function __sleep();
    public function __wakeup();
}