<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.08.16
 * Time: 15:28
 */

namespace common\interfaces;

use common\components\ps\locator\Size;
use common\models\base\PrinterMaterialGroup;
use common\models\File;
use common\models\Model3dPartCncParams;
use common\models\Model3dPartProperties;
use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Interface Model3dBasePartInterface
 *
 * @property Model3dBaseInterface $model3d
 * @property Model3dPartProperties $model3dPartProperties
 * @property Model3dPartProperties $model3dPartPropertiesOriginal
 * @property Model3dPartCncParams $model3dPartCncParams
 * @property Model3dBasePartInterface|null $convertedFromPart
 * @property Size $size
 * @property integer $id
 * @property integer $qty
 * @property integer $is_converted
 * @property integer $is_common_view
 * @property File $file
 * @property File $fileSrc
 * @property string $cae
 * @property string $user_status
 * @property string $moderator_status
 * @property Model3dTexture $model3dTexture
 * @mixin Model
 */
interface Model3dBasePartInterface
{
    const STATUS_INACTIVE = 'inactive';
    const STATUS_ACTIVE   = 'active';
    const STATUS_BANNED   = 'banned';

    const STL_FORMAT    = 'stl';
    const PLY_FORMAT    = 'ply';
    const OBJ_FORMAT    = 'obj';
    const STEP_FORMAT   = 'step';
    const STEP_FORMAT_2 = 'stp';
    const IGES_FORMAT   = 'iges';
    const IGES_FORMAT_2 = 'igs';
    const FORMAT_3MF    = '3mf';

    const MAX_3MF_SIZE = 5 * 1024 * 1024;

    const ALLOWED_FORMATS = [self::STL_FORMAT, self::PLY_FORMAT, self::STEP_FORMAT, self::IGES_FORMAT, self::STEP_FORMAT_2, self::IGES_FORMAT_2, self::FORMAT_3MF, self::OBJ_FORMAT];

    /**
     * @return string
     */
    public function getUid();

    /**
     * @return integer|null
     */
    public function getWeight();

    /**
     * @return integer|null
     */
    public function getVolume();

    /**
     * @return float|null
     */
    public function getBoxVolume();


    /**
     * @return bool
     */
    public function isActive();


    /**
     * @return bool
     */
    public function isParsed(): bool;

    /**
     * Set model3dpart inactive user status
     *
     * @return
     */
    public function setInactive();

    /**
     * Set model3dpart active user status
     *
     * @return
     */
    public function setActive();

    /**
     * @return bool
     */
    public function isCaclulatedProperties();

    /**
     * @return Size
     */
    public function getSize();

    /**
     * @return Size
     */
    public function getOriginalSize();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getTitleWithoutExt();

    /**
     * @param PrinterMaterialGroup $materialGroup
     * @param PrinterColor $color
     */
    public function setMaterialGroupAndColor($materialGroup, $color);

    /**
     * @param PrinterMaterial $material
     * @param PrinterColor $color
     * @param null $infill
     */
    public function setMaterialAndColor(PrinterMaterial $material, PrinterColor $color, $infill = null);

    /**
     * Get calculated color:  if it is one color for kit, return model3d color. If it is not kit, return model3d part color.
     *
     * @return Model3dTexture
     */
    public function getCalculatedTexture();

    /**
     * Previous function with verifications:  1. stl with multicolor
     * Model3dpart printable color
     *
     * @return Model3dTexture
     */
    public function getCalculatedPrintableTexture();

    /**
     * @return Model3dTexture
     */
    public function getTexture();

    /**
     * @param Model3dTexture $texture
     */
    public function setTexture(Model3dTexture $texture);

    /**
     * @return void
     */
    public function resetScale();

    public function isScaled(): bool;

    public function getScale(): float;

    /**
     * @param Model3dPartProperties $model3dPartProperties
     * @return mixed
     */
    public function setModel3dPartProperties($model3dPartProperties);


    /**
     * @param Model3dPartCncParams $model3dPartCncParams
     * @return mixed
     */
    public function setModel3dPartCncParams($model3dPartCncParams);

    /**
     * Get model3dpart format
     *
     * @return mixed
     */
    public function getFormat();

    /**
     * @param File $file
     * @return mixed
     */
    public function setFile(File $file);


    /**
     * @return bool
     */
    public function isMulticolorFormat();


    public function setAttachedModel3d(Model3dBaseInterface $model3d): void;

    /**
     * @return Model3dBaseInterface
     */
    public function getAttachedModel3d();

    /**
     * @return Model3dBasePartInterface|ActiveRecord
     */
    public function getOriginalModel3dPartObj();

    /**
     * Original qty of part from Model3d
     *
     * @return int
     */
    public function getOriginalQty(): int;


    /**
     * Is converted to printable parts
     *
     * @return bool
     */
    public function isConverted(): bool;
}


