<?php


namespace common\interfaces;


use lib\delivery\carrier\models\CarrierRate;
use lib\money\Money;

interface DeliveryExpensiveInterface
{
    public function check(CarrierRate $rate, Money $money): bool;
}