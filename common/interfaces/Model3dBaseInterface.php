<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.08.16
 * Time: 14:56
 */

namespace common\interfaces;

use common\components\ps\locator\Size;
use common\models\File;
use common\models\Model3d;
use common\models\Model3dTexture;
use common\models\SiteTag;
use common\models\StoreUnit;
use common\models\User;
use common\modules\product\interfaces\ProductInterface;
use yii\db\ActiveRecord;

/**
 * Interface Model3dBaseInterface
 *
 * @property StoreUnit $storeUnit
 * @property File $coverFile
 * @property string $title
 * @property string $source
 * @property string $model_units
 * @property string $published_at
 * @property string $product_status
 * @property string $price_currency
 * @property integer $id
 * @property float price_per_produce
 * @property Size $size
 * @property User $user
 * @property integer $user_id
 * @property float $scale
 * @property array $attributes
 * @property Model3dBasePartInterface[] $model3dParts
 * @property Model3dBaseImgInterface[] $model3dImgs
 * @package common\interfaces
 */
interface Model3dBaseInterface extends ProductInterface
{

    /**
     * Model statuses
     */
    const STATUS_NEW       = 'new';
    const STATUS_INACTIVE  = 'inactive';
    const STATUS_PUBLISHED = 'published';

    /**
     * api source - model3d created using api
     */
    const SOURCE_API = 'api';

    /**
     * default source
     */
    const SOURCE_WEBSITE = 'website';

    /**
     * default source
     */
    const SOURCE_PS_WIDGET = 'psWidget';

    /**
     * default source
     */
    const SOURCE_PS_FACEBOOK = 'psFacebook';

    /**
     * source thingiverse
     */
    const SOURCE_THINGIVERSE = 'thingiverse';

    /**
     * source widget print upload model by any ps
     */
    const SOURCE_PRINT_MODEL_WIDGET = 'userWidget';
    const SOURCE_USER_WIDGET        = 'userWidget';

    const POSSIBLE_SOURCES = [
        self::SOURCE_PS_WIDGET,
        self::SOURCE_PS_FACEBOOK,
        self::SOURCE_API,
        self::SOURCE_WEBSITE
    ];

    /**
     * system used to design model3d, cad or cam
     * https://en.wikipedia.org/wiki/Category:Computer-aided_engineering
     */
    const CAE_CAM = 'CAM';
    const CAE_CAD = 'CAD';

    const DEFAULT_TITLE = 'Model3d';

    /**
     *  Possible model with empty model3dparts, empty title
     */
    const SCENARIO_EMPTY_MODEL = 'emptyModel';

    /**
     * @return Size
     */
    public function getSize();

    /**
     * @return bool
     */
    public function isEmpty(): bool;

    /**
     * @return mixed
     */
    public function getWeight();

    /**
     * @return float
     */
    public function getVolume();

    /**
     * @return float
     */
    public function getBoxVolume();

    /**
     * @param Model3dBasePartInterface[] $model3dParts
     */
    public function setModel3dParts($model3dParts);

    /**
     * @param Model3dBaseImgInterface[] $model3dImgs
     */
    public function setModel3dImgs($model3dImgs);

    /**
     * @param File $file
     */
    public function setCoverFile(File $file);

    /**
     * @return mixed
     */
    public function zerroCoverFile();


    /**
     * @return string
     */
    public function getViewUrl();

    /**
     * @return Model3dBasePartInterface|null
     */
    public function getLargestPrintingModel3dPart();

    /**
     * @return Model3dBasePartInterface|null
     */
    public function getLargestModel3dPart();


    /**
     * @return bool
     */
    public function isAnyTextureAllowed();


    /**
     * @return mixed
     */
    public function isPublished(): bool;


    public function getAuthor(): ?User;

    /**
     * @return bool
     */
    public function isOneTextureForKit();

    /**
     * @param Model3dTexture $kitTexture
     */
    public function setKitTexture(Model3dTexture $kitTexture);

    /**
     * @return Model3dTexture
     */
    public function getKitTexture();

    /**
     * @return Model3dBasePartInterface[]
     */
    public function getCalculatedModel3dParts();

    /**
     * @return Model3dBasePartInterface[]
     */
    public function getActiveModel3dParts();

    /**
     * @return Model3dBasePartInterface[]
     */
    public function getCanceledParts();

    /**
     * @param SiteTag[] $tags
     */
    public function setSiteTags($tags);

    /**
     * @return bool
     */
    public function isMulticolorFormatModel();

    /**
     * @return bool
     */
    public function isSameTextureForAllParts();

    /**
     * @return bool
     */
    public function isParsed(): bool;

    /**
     * @return mixed
     */
    public function isCalculatedProperties();


    /**
     * @return Model3dBaseImgInterface[]|ActiveRecord[]
     */
    public function getActiveModel3dImages();

    /**
     * @return int
     */
    public function getCadFlag();

    /**
     * @return mixed
     */
    public function setAnyTextureAllowed($isAnyTextureAllowed);

    /**
     * @return string
     */
    public function getUid();


    /**
     * @return Model3d
     */
    public function getSourceModel3d();


    /**
     * @return bool
     */
    public function isKit();
}