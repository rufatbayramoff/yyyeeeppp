<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.07.17
 * Time: 13:35
 */

namespace common\services;


use common\components\exceptions\BusinessException;
use common\components\ModelException;
use common\models\CompanyService;
use common\models\PsPrinter;
use frontend\models\ps\PsPrinterUpdater;
use frontend\modules\mybusiness\widgets\CertificationPdfWidget;
use HttpException;

class PsMachineService
{
    /**
     * @var GeneratorPdf
     */
    protected $generatorPdf;

    /**
     * @param GeneratorPdf $generatorPdf
     */
    public function injectDependencies(GeneratorPdf $generatorPdf)
    {
        $this->generatorPdf = $generatorPdf;
    }

    public function checkEditAccess(CompanyService $psMachine, $user)
    {
        if ($psMachine->ps->user_id != $user->id) {
            throw new BusinessException(_t('site', 'No access to edit'));
        }
    }

    /**
     * restore deleted machine
     *
     * @param CompanyService $psMachine
     * @return bool
     */
    public function restoreMachine(CompanyService $psMachine)
    {
        $psMachine->is_deleted = 0;
        $psMachine->safeSave();
        return true;
    }

    public function updateModeratorStatus(CompanyService $psMachine, $changedAttributesInfo)
    {
        if (($psMachine->moderator_status == CompanyService::MODERATOR_STATUS_NEW) || ($psMachine->moderator_status == CompanyService::MODERATOR_STATUS_DRAFT)) {
            $psMachine->moderator_status = CompanyService::MODERATOR_STATUS_PENDING;
        }
        if ($psMachine->moderator_status == CompanyService::MODERATOR_STATUS_APPROVED) {
            $psMachine->moderator_status = CompanyService::MODERATOR_STATUS_APPROVED_REVIEWING;
        }
    }

    /**
     * @param array|null $data
     * @param \common\models\Company $company
     * @throws \yii\db\Exception
     * @throws ModelException
     */
    public function updateDelivery(?array $data, \common\models\Company $company): void
    {
        if (empty($data)) {
            throw new ModelException(_t('site.store', 'deliveryTypes is empty'));
        }
        if (!array_key_exists('deliveryTypes', $data) || !array_key_exists('location', $data)) {
            throw new ModelException(_t('site.store', 'Delivery Options or Location is empty'));
        }
        /** @var PsPrinter[] $printers */
        $printers = PsPrinter::find()->forUser($company->user)->notDeleted()->all();
        $this->validateEasyPost($printers, $data['deliveryTypes']);
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($printers as $printer) {
                PsPrinterUpdater::createForCreate($printer->companyService)
                    ->updateDeliveryTypes($data);
            }
            $transaction->commit();
        } catch (\Exception $exception) {
            logException($exception, 'update-delivery-printer');
            $transaction->rollBack();
            throw new HttpException(400, "Cant update printservice machine");
        }
    }

    protected function validateEasyPost(array $printers, $deliveryTypes)
    {
        foreach ($printers as $printer) {
            PsPrinterUpdater::createForCreate($printer->companyService)
                ->validateCanEasyPost($deliveryTypes);
        }
    }

    /**
     * @param CompanyService $certification
     * @return string
     * @throws \Exception
     */
    public function generateCertificatePdf(CompanyService $companyService): string
    {
        $content = CertificationPdfWidget::widget(['companyService' => $companyService]);
        return $this->generatorPdf->write($content, 'certificationprinterpdf.html', ['--orientation Landscape']);
    }
}