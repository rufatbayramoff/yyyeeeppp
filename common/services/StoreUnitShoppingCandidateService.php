<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.11.16
 * Time: 15:07
 */

namespace common\services;

use common\models\factories\StoreUnitShoppingCandidateFactory;
use common\models\Model3d;
use common\models\StoreUnit;
use common\models\StoreUnitShoppingCandidate;
use DateTime;
use DateTimeZone;
use frontend\models\user\UserFacade;

class StoreUnitShoppingCandidateService
{

    /**
     * @param StoreUnitShoppingCandidate $storeUnitShoppingCandidate
     * @return bool
     */
    public function isAvailableForCurrentUser(StoreUnitShoppingCandidate $storeUnitShoppingCandidate)
    {
        $userSession = UserFacade::getUserSession();
        if ($userSession && $userSession->id === $storeUnitShoppingCandidate->user_session_id) {
            return true;
        }
        return false;
    }


    /**
     * Form new shopping candidate for model3d if it`s not exists. If it exists update view_date.
     *
     * @param Model3d $model3d
     * @param string $type
     */
    public function formShoppingCandidate(Model3d $model3d, $type)
    {
        $shoppingCandidate = $this->getStoreUnitShoppingCandidate($model3d->storeUnit);
        if (!$shoppingCandidate) {
            $shoppingCandidate = StoreUnitShoppingCandidateFactory::createStoreUnitShoppingCandidate($model3d, $type);
        }
        $shoppingCandidate->view_date = $shoppingCandidate->create_date = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $shoppingCandidate->safeSave();
    }

    /**
     * @param StoreUnit $storeUnit
     * @return StoreUnitShoppingCandidate|null
     */
    public function getStoreUnitShoppingCandidate(StoreUnit $storeUnit)
    {
        $userSession = UserFacade::getUserSession();
        $shoppingCandidate = StoreUnitShoppingCandidate::findOne(['store_unit_id' => $storeUnit->id, 'user_session_id' => $userSession->id]);
        return $shoppingCandidate;
    }
}