<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.07.16
 * Time: 11:49
 */

namespace common\services;


use common\interfaces\FileOwnerInterface;
use common\models\File;

class FileService
{
    public static $imgExtensions = ['jpg', 'jpeg', 'png', 'gif'];


    /**
     * Связать две сущности: файл и модель с указанием конкретного аттрибута
     *
     * @param File $file
     * @param FileOwnerInterface $model
     * @param string $attributeName
     */
    public static function bindModel($file, $model, $attributeName)
    {
        $file->setOwner(get_class($model), $attributeName);
    }

    /**
     * @param File $file
     * @return bool
     */
    public static function isUsed(File $file)
    {
        return $file->model3dImgs || $file->model3dParts || $file->model3dReplicaParts || $file->model3dReplicaImgs;
    }

    /**
     * @param $extension
     * @return bool
     */
    public static function isImageExtension($extension)
    {
        return in_array(strtolower($extension), self::$imgExtensions);
    }

    public static function filterNameForThumbs($baseName)
    {
        return preg_replace('/[^\da-z-]/i', '-', $baseName);
    }


}