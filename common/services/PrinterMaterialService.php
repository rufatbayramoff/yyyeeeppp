<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.06.16
 * Time: 14:44
 */

namespace common\services;


use common\components\ArrayHelper;
use common\components\helpers\ColorHelper;
use common\components\ps\locator\materials\MaterialColorItem;
use common\components\ps\locator\materials\MaterialColorsItem;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\base\PrinterMaterialGroup;
use common\models\factories\Model3dTextureFactory;
use common\models\model3d\Model3dBase;
use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialToColor;
use common\models\PsPrinter;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\models\repositories\PrinterMaterialRepository;
use common\models\StoreOrder;
use common\modules\xss\helpers\XssHelper;
use frontend\models\serialize\Model3dTextureSerializer;
use Yii;
use yii\base\InvalidConfigException;

class PrinterMaterialService
{
    static protected $defaultMaterial = null;
    static protected $defaultColor = null;

    /**
     * @param Model3dTexture[] $textures
     * @return array
     */
    public static function createMaterialColorsItemsFromTextures($textures)
    {
        $returnValue = [];
        foreach ($textures as $texture) {
            $materialColorsItem = new MaterialColorsItem();
            $materialColorsItem->materialGroup = $texture->calculatePrinterMaterialGroup();
            $materialColorsItem->colors = [
                $texture->printerColor->id => $texture->printerColor
            ];
            $returnValue[] = $materialColorsItem;
        }
        return $returnValue;
    }

    /**
     * @param PrinterMaterialGroup $materialGroup
     * @param PrinterColor $colors
     * @param int $maxColorsCount
     * @param Model3dTexture[] $requiredTextures
     * @return array
     */
    public static function sliceColorsGroup($materialGroup, $colors, $maxColorsCount, $requiredTextures)
    {
        $resultColors = [];
        // Add existing colors
        foreach ($requiredTextures as $requireTexture) {
            if ($requireTexture && $requireTexture->calculatePrinterMaterialGroup()->code === $materialGroup->code) {
                foreach ($colors as $colorKey => $color) {
                    if ($color->render_color == $requireTexture->printerColor->render_color) {
                        $resultColors[$colorKey] = $color;
                        unset($colors[$colorKey]);
                    }
                }
            }
        }
        $resultColors = array_replace_recursive($resultColors, $colors);
        $resultColors = array_slice($resultColors, 0, $maxColorsCount);
        return $resultColors;
    }

    /**
     * Подготовить группу материалов для определенной модели для дальнейшего отображения в просмотре модели
     *
     * @param MaterialColorsItem[]|null $availableColorsGroups
     * @param null|integer $maxColorsCount
     * @param Model3dTexture[] $requiredTextures
     * @return \common\components\ps\locator\materials\MaterialColorsItem[]
     */
    public static function formingGroupMaterialsListForColorSelector($availableColorsGroups = null, $maxColorsCount = null, $requiredTextures = null)
    {
        $groups = [];
        if ($availableColorsGroups !== null) {
            foreach ($availableColorsGroups as $materialColorItem) {
                if (array_key_exists($materialColorItem->materialGroup->id, $groups)) {
                    $currentGroup = $groups[$materialColorItem->materialGroup->id];
                    $groups[$materialColorItem->materialGroup->id]->colors = array_replace_recursive($materialColorItem->colors, $currentGroup->colors);
                } else {
                    $groups[$materialColorItem->materialGroup->id] = $materialColorItem;
                }
                if ($maxColorsCount) {
                    $groups[$materialColorItem->materialGroup->id]->colors = self::sliceColorsGroup(
                        $materialColorItem->materialGroup,
                        $groups[$materialColorItem->materialGroup->id]->colors,
                        $maxColorsCount,
                        $requiredTextures
                    );
                }
            }
        } else {
            /** @var PrinterMaterial[] $printerMaterials */
            $printerMaterials = PrinterMaterial::find()->where(['is_active' => 1])->all();

            foreach ($printerMaterials as $printerMaterial) {
                if (!$printerMaterial->group) {
                    continue; // No group for material
                }
                if (!$printerMaterial->group->is_active) {
                    continue;
                }
                $colors = $printerMaterial->colors;
                $colors = ArrayHelper::index($colors, 'id');
                if ($colors) {
                    $materialColorsItem = new MaterialColorsItem();
                    $materialColorsItem->materialGroup = $printerMaterial->group;
                    if (array_key_exists($materialColorsItem->materialGroup->id, $groups)) {
                        $currentGroup = $groups[$materialColorsItem->materialGroup->id];
                        $materialColorsItem->colors = array_replace_recursive($colors, $currentGroup->colors);
                    } else {
                        $materialColorsItem->colors = $colors;
                    }

                    if ($maxColorsCount) {
                        $materialColorsItem->colors = self::sliceColorsGroup(
                            $materialColorsItem->materialGroup,
                            $materialColorsItem->colors,
                            $maxColorsCount,
                            $requiredTextures
                        );
                    }

                    $groups[$materialColorsItem->materialGroup->id] = $materialColorsItem;
                }
            }
        }
        foreach ($groups as $key => $group) {
            uasort($group->colors, function (PrinterColor $color1, PrinterColor $color2) {
                $coef1 = ColorHelper::getColorSortCoefficient($color1->rgb);
                $coef2 = ColorHelper::getColorSortCoefficient($color2->rgb);
                return $coef1 > $coef2;
            });
        }
        uasort(
            $groups,
            function ($group1, $group2) {
                if ($group1->materialGroup->priority == $group2->materialGroup->priority) {
                    return $group1->materialGroup->title < $group2->materialGroup->title;
                }
                return $group1->materialGroup->priority < $group2->materialGroup->priority;
            }
        );
        return $groups;
    }


    /**
     * @return PrinterMaterial
     */
    public static function getDefaultMaterial()
    {
        if (!self::$defaultMaterial) {
            self::$defaultMaterial = PrinterMaterial::findOne(['filament_title' => Model3dTextureFactory::DEFAULT_MATERIAL]);
        }
        return self::$defaultMaterial;
    }

    /**
     * @return PrinterColor
     */
    public static function getDefaultColor()
    {
        if (!self::$defaultColor) {
            self::$defaultColor = PrinterColor::findOne(['render_color' => Model3dTextureFactory::DEFAULT_COLOR]);
        }
        return self::$defaultColor;
    }

    /**
     * @param PrinterMaterialGroup $printerMaterialGroup
     * @param PrinterColor $printerColor
     * @param array $colorsCost
     * @return PrinterMaterial
     * @throws \yii\base\InvalidConfigException
     */
    public static function getCheapestMaterialInGroup(PrinterMaterialGroup $printerMaterialGroup, PrinterColor $printerColor, $colorsCost)
    {
        /** @var PrinterMaterialRepository $printerMaterialRepository */
        $printerMaterialRepository = Yii::createObject(PrinterMaterialRepository::class);
        $cheapestCost = 99999;
        /** @var PrinterMaterial $cheapestMaterial */
        $cheapestMaterial = null;
        foreach ($colorsCost as $materialCode => $oneCost) {
            if (defined('TEST_XSS') && TEST_XSS) {
                $materialCode = XssHelper::cleanXssProtect($materialCode);
            }
            $printerMaterial = $printerMaterialRepository->getByCode($materialCode);
            if (array_key_exists($printerColor->render_color, $oneCost)) {
                $colorInfo = $oneCost[$printerColor->render_color];
                if ($colorInfo['materialGroupCode'] === $printerMaterialGroup->code) {
                    $cost = $colorInfo['price'];
                    if (($cheapestCost > $cost) || (($cheapestCost == $cost) && ($cheapestMaterial->density > $printerMaterial->density))) {
                        $cheapestCost = $cost;
                        $cheapestMaterial = $printerMaterial;
                    }
                }
            }
        }
        return $cheapestMaterial;
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @return Model3dTexture[]
     */
    public static function getModelTextures(Model3dBaseInterface $model3d)
    {
        $textures = [];
        if ($model3d->isOneTextureForKit()) {
            return [$model3d->getKitTexture()];
        }
        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
            $textures[] = $model3dPart->getCalculatedTexture();
        }
        return $textures;
    }

    /**
     * @param PsPrinter $printer
     * @return array
     */
    public static function createAllowedTexturesByPrinter($printer)
    {
        $textures = [];
        foreach ($printer->materials as $psPrinterMaterial) {
            foreach ($psPrinterMaterial->colors as $color) {
                $texture = new Model3dTexture();
                $texture->printerColor = $color;
                $texture->printerMaterial = $psPrinterMaterial->material;
                $textures[] = $texture;
            }

        }
        return $textures;
    }

    /**
     * Test is model3d color in printersAllowColors
     *
     * @param Model3dBase $model3d
     * @param $printersAllowTextures
     * @return bool
     */
    public static function isModel3dTexturesInPrinterAllowColors($model3d, $printersAllowTextures)
    {
        foreach ($printersAllowTextures as $materialColorsItem) {
            /** @var MaterialColorsItem $materialColorsItem */
            if ($materialColorsItem->materialGroup->id === $model3d->getKitTexture()->printerMaterial->group->id) {
                foreach ($materialColorsItem->colors as $color) {
                    if ($color->id === $model3d->getKitTexture()->printerColor->id) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static function setMaterialGroupAndColor(Model3dBaseInterface $model3d, PrinterMaterialGroup $materialGroup, PrinterColor $color)
    {
        if ($model3d->isOneTextureForKit()) {
            $kitTexture = $model3d->getKitTexture();
            $kitTexture->setPrinterMaterialGroup($materialGroup);
            $kitTexture->setPrinterMaterial(null);
            $kitTexture->setPrinterColor($color);

        } else {
            foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
                $partTexture = $model3dPart->getTexture();
                $partTexture->setPrinterMaterialGroup($materialGroup);
                $partTexture->setPrinterMaterial(null);
                $partTexture->setPrinterColor($color);
            }
        }
    }

    /**
     * Test is same textures
     *
     * @param Model3dTexture $a
     * @param Model3dTexture $b
     * @return bool
     */
    public static function isSameTexture(Model3dTexture $a, Model3dTexture $b)
    {
        if ($a->printerMaterial && $b->printerMaterial) {
            return (($a->printerMaterial->id === $b->printerMaterial->id) && ($a->printerColor->id === $b->printerColor->id));
        } else {
            return (($a->calculatePrinterMaterialGroup()->id === $b->calculatePrinterMaterialGroup()->id) && ($a->printerColor->id === $b->printerColor->id));
        }
    }

    public static function getDefaultMulticolorMaterial()
    {
        $defaultMulticolorMaterialId = app('setting')->get('material.defaultMulticolorMaterial');
        if ($defaultMulticolorMaterialId) {
            return PrinterMaterial::tryFindByPk($defaultMulticolorMaterialId);
        } else {
            $printerMaterialToColor = PrinterMaterialToColor::findOne(
                [
                    'color_id' => PrinterColor::MULTICOLOR_ID
                ]
            );
            if (!$printerMaterialToColor) {
                throw new InvalidConfigException('Cant find any multicolor material. Please add multicolor material to PrinterMaterialToColor.');
            }
            return $printerMaterialToColor->material;
        }
    }

    public static function serializeModel3dTexture(Model3dBaseInterface $model3d)
    {
        /** @var Model3dTextureSerializer $model3dTextureSerializer */
        $model3dTextureSerializer = Yii::createObject(Model3dTextureSerializer::class);

        $returnValue = [];
        if ($model3d->isOneTextureForKit()) {
            $texture = $model3d->getKitTexture();
            $returnValue['isOneMaterialForKit'] = true;
            $returnValue['modelTexture'] = $model3dTextureSerializer->serialize($texture);
        } else {
            $returnValue['isOneMaterialForKit'] = false;
            foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
                $texture = $model3dPart->getTexture();
                $returnValue['partsMaterial'][$model3dPart->getOriginalModel3dPartObj()->id] = $model3dTextureSerializer->serialize($texture);
            }
        }
        return $returnValue;
    }

    public static function unSerializeModel3dTexture(Model3dBaseInterface $model3d, $model3dTextureState, $partsByUid = false)
    {
        /** @var Model3dTextureSerializer $model3dTextureSerializer */
        $model3dTextureSerializer = Yii::createObject(Model3dTextureSerializer::class);

        if ($model3dTextureState['isOneMaterialForKit']) {
            $texture = $model3dTextureSerializer->unSerialize($model3dTextureState['modelTexture']);
            $texture->checkTexture();
            $model3d->setKitTexture($texture);
        } else {
            if ($model3d->kitModel3dTexture) {
                $model3d->kitModel3dTexture->isMarkForDelete = true;
            }
            $textureParts = $model3dTextureState['partsMaterial'];
            $model3dParts = [];
            foreach ($model3d->model3dParts as $part) {
                if ($partsByUid) {
                    $model3dParts[$part->getUid()] = $part;
                } else {
                    $model3dParts[$part->getOriginalModel3dPartObj()->id] = $part;
                }
            }
            foreach ($textureParts as $partId => $textureDescription) {
                $texture = $model3dTextureSerializer->unSerialize($textureDescription);
                $texture->checkTexture();
                /** @var Model3dBasePartInterface $model3dPart */
                if (array_key_exists($partId, $model3dParts)) {
                    $model3dPart = $model3dParts[$partId];
                    $model3dPart->setTexture($texture);
                }
            }
        }
    }

    /**
     * @param MaterialColorItem[] $materialItems
     * @return PrinterColor[]
     * @throws InvalidConfigException
     */
    public static function getMaterialColorsItemUsedColors($materialItems)
    {
        $colors = [];
        foreach ($materialItems as $materialItem) {
            if (!array_key_exists($materialItem->colorId, $colors)) {
                $colors[$materialItem->colorId] = $materialItem->getColor();
            }
        }
        return $colors;
    }

    /**
     * @param MaterialColorItem[] $materialItems
     * @return PrinterMaterial[]
     * @throws InvalidConfigException
     */
    public static function getMaterialColorsItemUsedMaterials($materialItems)
    {
        $materialRepository = \Yii::createObject(PrinterMaterialRepository::class);
        $materials = [];
        foreach ($materialItems as $materialItem) {
            if ($materialItem->materialId && !array_key_exists($materialItem->materialId, $materials)) {
                $materials[$materialItem->materialId] = $materialRepository->getById($materialItem->materialId);
            }
        }
        return $materials;
    }

    /**
     * @param MaterialColorItem[] $materialItems
     * @return PrinterMaterialGroup[]
     * @throws InvalidConfigException
     */
    public static function getMaterialColorsItemUsedMaterialGroups($materialItems)
    {
        $materialGroupRepository = \Yii::createObject(PrinterMaterialGroupRepository::class);
        $groups = [];
        foreach ($materialItems as $materialItem) {
            if ($materialItem->groupId && !array_key_exists($materialItem->groupId, $groups)) {
                $groups[$materialItem->groupId] = $materialGroupRepository->getById($materialItem->groupId);
            }
        }
        return $groups;
    }

    public static function isNotSettedPrinterMaterial(Model3dBaseInterface $model3d)
    {
        if ($model3d->isOneTextureForKit()) {
            $texture = $model3d->getKitTexture();
            if (!$texture->printerMaterial) {
                return true;
            }
            return false;
        }

        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
            $texture = $model3dPart->getCalculatedTexture();
            if (!$texture->printerMaterial) {
                return true;
            }
        }
        return false;
    }

    public static function orderMaterials(?StoreOrder $order): string
    {
        if(!$order) {
            return _t('site.model3d', 'Different materials');
        }
        $storeOrderItems = $order->storeOrderItems;
        if(empty($storeOrderItems)) {
            return _t('site.model3d', 'Different materials');
        }
        $materials = [];
        foreach ($storeOrderItems as $storeOrder) {
            if($storeOrder->model3dReplica && $storeOrder->model3dReplica->model3dTexture && $storeOrder->model3dReplica->model3dTexture->printerMaterial) {
                $materials[] = $storeOrder->model3dReplica->model3dTexture->printerMaterial->title;
            }
        }
        if(!empty($materials)){
            return implode(',',$materials);
        }
        return _t('site.model3d', 'Different materials');
    }

    /**
     * @param MaterialColorsItem[] $groupMaterials
     */
    public static function colors(array $groupMaterials): array
    {
        $result = [];
        foreach ($groupMaterials as $materialInfo) {
            foreach ($materialInfo->colors as $color) {
                $result[$color->id] = $color->rgb;
            }
        }
        return $result;
    }
}
