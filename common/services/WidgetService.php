<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.11.17
 * Time: 14:55
 */

namespace common\services;

use common\models\Company;

class WidgetService
{
    // User widget
    public function getUserWidgetHtmlStyle()
    {
        return '<link href=\'' . param('server') . '/css/embed-user.css\' rel=\'stylesheet\' />';
    }

    public function getUserIframeUserUrl($user)
    {
        $iframeUrl = \Yii::$app->params['siteUrl'] . '/order-upload/widget?userUid=' . $user->uid;
        //$iframeUrl = \Yii::$app->params['siteUrl'] . '/my/print-model3d/widget?userUid=' . $user->uid;
        return $iframeUrl;
    }

    // Quote
    public function getGetQuoteHtmlStyle()
    {
        return '<link href=\'' . param('server') . '/css/embed-getquote.css\' rel=\'stylesheet\' />';
    }

    public function getGetQuoteIframeUrl(Company $company)
    {
        $iframeUrl = \Yii::$app->params['siteUrl'] . '/preorder/quote/get-quote-widget?companyUrl=' . HL($company->url);
        return $iframeUrl;
    }


    /**
     * @param $comment
     * @param $style
     * @param $tsLink
     * @param $iframeUrl
     * @return string
     */
    public function getFormWidgetHtmlCode($comment, $style, $tsLink, $iframeUrl, $class='ts-embed-userwidget')
    {
        return $comment . $style . $tsLink . '<iframe class=\''.$class.'\' src=\'' . $iframeUrl . '\' frameborder=\'0\'></iframe>';
    }

    public function getUserWidgetCode($user)
    {
        $comment   = '<!-- UserWidget: ' . $user->uid . ' -->';
        $tsLink    = '<div class=\'ts-embed-tslink\'><a href=\'https://www.treatstock.com/\' target=\'_blank\'>Powered by Treatstock</a></div>';
        $style     = $this->getUserWidgetHtmlStyle();
        $iframeUrl = $this->getUserIframeUserUrl($user);
        return $this->getFormWidgetHtmlCode($comment, $style, $tsLink, $iframeUrl);
    }

    public function getUserWidgetCodeEncoded($user)
    {
        return htmlspecialchars(preg_replace('/\s+/', ' ', $this->getUserWidgetCode($user)));
    }

    public function getGetQuoteWidgetCode(Company $ps)
    {
        $comment   = '<!-- GetAQuoteWidget: ' . $ps->url . ' -->';
        $tsLink    = '';
        $style     = $this->getGetQuoteHtmlStyle();
        $iframeUrl = $this->getGetQuoteIframeUrl($ps);
        return $this->getFormWidgetHtmlCode($comment, $style, $tsLink, $iframeUrl);
    }

    public function getGetQuoteWidgetCodeEncoded(Company $ps)
    {
        return htmlspecialchars(preg_replace('/\s+/', ' ', $this->getGetQuoteWidgetCode($ps)));
    }

    // Quote
    public function getRateHtmlStyle()
    {
        return '<link href=\'' . param('server') . '/css/embed-rate.css\' rel=\'stylesheet\' />';
    }

    public function getRateIframeUrl(Company $company, $wide)
    {
        $iframeUrl = \Yii::$app->params['siteUrl'] . '/c/' . $company->url . '/widget/rate' . ($wide ? '-wide' : '').'?utm_source=ratingwidget';
        return $iframeUrl;
    }

    public function getRateWidgetCode(Company $company)
    {
        $comment   = '<!-- RateWidget: ' . $company->url . ' -->';
        $tsLink    = '';
        $style     = $this->getRateHtmlStyle();
        $iframeUrl = $this->getRateIframeUrl($company, false);
        return $this->getFormWidgetHtmlCode($comment, $style, $tsLink, $iframeUrl, 'ts-embed-rate');
    }

    public function getRateWideWidgetCode(Company $company)
    {
        $comment   = '<!-- RateWideWidget: ' . $company->url . ' -->';
        $tsLink    = '';
        $style     = $this->getRateHtmlStyle();
        $iframeUrl = $this->getRateIframeUrl($company, true);
        return $this->getFormWidgetHtmlCode($comment, $style, $tsLink, $iframeUrl, 'ts-embed-rate-wide');
    }

    public function getRateWidgetCodeEncoded(Company $company)
    {
        return htmlspecialchars(preg_replace('/\s+/', ' ', $this->getRateWidgetCode($company)));
    }
}