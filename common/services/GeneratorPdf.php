<?php


namespace common\services;

use lib\pdf\HtmlToPdf;
use Yii;

class GeneratorPdf
{
    /**
     * @param string $content
     * @param string $tmpFileName
     * @param array $options
     * @return string
     */
    public function write(string $content, string $tmpFileName = 'invoicereportpdf.html', array $options = []): string
    {

        $fileName = $this->fileName($tmpFileName);
        $this->createHtml($fileName, $content);
        $pdf =  HtmlToPdf::convert($fileName, $options);
        unlink($fileName);
        return $pdf;
    }

    /**
     * @param string $tmpFileName
     * @return string
     */
    protected function fileName(string $tmpFileName): string
    {
        return Yii::getAlias('@runtime') . '/'.uniqid(date('Ymd_His'), true).$tmpFileName;
    }

    /**
     * @param string $file
     * @param string $content
     */
    protected function createHtml(string $file,string $content): void
    {
        $content = str_replace('€', '&#128;', $content);
        file_put_contents($file, $content);
    }
}