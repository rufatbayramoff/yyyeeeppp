<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.09.16
 * Time: 13:12
 */

namespace common\services;

use common\components\order\predicates\NeedCertificateForDownloadModelPredicate;
use common\components\order\PriceCalculator;
use common\components\orderOffer\OrderOffer;
use common\components\orderOffer\OrderOfferLocatorService;
use common\models\CatalogCost;
use common\models\customModels\Model3dTextureCatalogCost;
use common\models\GeoCountry;
use common\models\Model3dTexture;
use common\models\PsPrinter;
use common\models\StoreUnit;
use common\modules\printersList\models\RequestInfo;
use DateTime;
use DateTimeZone;
use lib\geo\models\Location;
use lib\money\Currency;
use Yii;

class CatalogCostService
{
    /**
     * @param CatalogCost $catalogCost
     * @throws \HttpException
     * @throws \lib\geo\GeoNamesException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public static function reCalculateCost(CatalogCost $catalogCost): void
    {
        $cost = null;
        /** @var Model3dService $model3dService */
        $model3dService = Yii::createObject(Model3dService::class);
        if ($catalogCost->storeUnit->model3d->isCalculatedProperties() &&
            ($catalogCost->storeUnit->model3d->isCatalogCostAvailable() || $model3dService->isAvailableForCurrentUser($catalogCost->storeUnit->model3d))
        ) {
            $location = new Location();
            $location->country = $catalogCost->country->iso_code;
            $costInfo = self::calculateCostForLocation($catalogCost->storeUnit, $location);

            if ($costInfo !== null) {
                if (!$catalogCost->texture) {
                    $catalogCost->texture = new Model3dTextureCatalogCost();
                }
                $catalogCost->texture->printer_material_group_id = $costInfo['materialGroupId'];
                $catalogCost->texture->printer_color_id = $costInfo['colorId'];
                $catalogCost->ps_printer_id = $costInfo['psPrinterId'];
                $cost = $costInfo['costUsd'];
            }
        }

        if ($cost !== null && $cost < 0.01) {
            $cost = 0.01;
        }

        $catalogCost->cost_usd = $cost;
        $catalogCost->update_date = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        if ($catalogCost->texture) {
            $catalogCost->texture->safeSave();
        }
        $catalogCost->safeSave();
    }

    /**
     * @param StoreUnit $storeUnit
     * @param Location $location
     * @return float|int|null|array
     * @throws \yii\base\InvalidConfigException
     */
    public static function calculateCostForLocation(StoreUnit $storeUnit, Location $location)
    {
        $predicateOnlyCertificated = new NeedCertificateForDownloadModelPredicate();
        $onlyCertificated = $predicateOnlyCertificated->test($storeUnit->model3d);
        $model3d = $storeUnit->model3d;
        $model3d->setAnyTextureAllowed(true);

        $offersLocator = \Yii::createObject(OrderOfferLocatorService::class);
        $offersLocator->initAnonymousBundle($location);
        $offersLocator->setSortMode(RequestInfo::SORT_STRATEGY_CHEAPEST);
        $offersLocator->currencies[] = Currency::USD;
        $offersLocator->onlyCertificated = $onlyCertificated;
        $offersLocator->formOffersListForModel($model3d);
        $allOffersList = $offersLocator->getAllOffersList();
        if (!$allOffersList) {
            return null;
        }
        /** @var OrderOffer $cheapestOffer */
        $cheapestOffer = reset($allOffersList);

        $costModel3dUsd = PriceCalculator::calculateModel3dPriceUsd($model3d, true);
        $costUsd = $costModel3dUsd->getAmount() + $cheapestOffer->anonymousPrintPrice->getAmount();
        /** @var Model3dTexture $texture */
        $largestPart = $model3d->getLargestPrintingModel3dPart();
        if (!$largestPart) {
            return null;
        }
        $texture = $largestPart->getCalculatedTexture();
        return [
            'costUsd'         => $costUsd,
            'psPrinterId'     => $cheapestOffer->getPrinterId(),
            'materialGroupId' => $texture->calculatePrinterMaterialGroup()->id,
            'colorId'         => $texture->printer_color_id
        ];
    }

    public static function createCatalogCostForModel(StoreUnit $storeUnit, $calForCountry = null)
    {
        /** @var GeoCountry[] $countries */
        if ($calForCountry === null) {
            $countries = GeoCountry::find()->all();
        } else {
            $countries = [$calForCountry];
        }
        $returnValue = [];
        foreach ($countries as $country) {
            $countryId = $country->id;
            $calcCost = CatalogCost::findOne(['store_unit_id' => $storeUnit->id, 'country_id' => $countryId]);
            if (!$calcCost) {
                CatalogCost::deleteAll(['store_unit_id' => $storeUnit->id, 'country_id' => $countryId]); // hotfix: If cache empty value, previous request will return null, and save will be failed
                $calcCost = new CatalogCost();
                $calcCost->store_unit_id = $storeUnit->id;
                $calcCost->country_id = $countryId;
                $calcCost->cost_usd = -1;
                $calcCost->update_date = CatalogCost::CATALOG_START_TIME;
                $calcCost->safeSave();
            }
            $returnValue[$countryId] = $calcCost;
        }
        return $returnValue;
    }

    public static function invalidateCache(PsPrinter $psPrinter): void
    {
        CatalogCost::deleteAll(['ps_printer_id' => $psPrinter->id]);
    }
}