<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.10.16
 * Time: 10:04
 */

namespace common\services;

use common\models\Model3dReplica;
use frontend\models\user\UserFacade;
use lib\message\exceptions\BadParamsException;

class Model3dReplicaService
{
    /**
     * @param Model3dReplica $model3dReplica
     * @throws BadParamsException
     */
    public static function tryCheckModel3dReplicaViewRights($model3dReplica)
    {
        $user = UserFacade::getCurrentUser();

        // Allow to view for owner
        if ($model3dReplica->user_id === $user->id) {
            return;
        }


        $storeOrder = $model3dReplica->getStoreOrder();

        // Allow view customer
        if ($storeOrder->user_id == $user->id) {
            return;
        }

        // Allow to view for print server
        foreach ($storeOrder->attemps as $attemp){
            if ($attemp->ps->user_id == $user->id) {
                return;
            }
        }

        throw new BadParamsException('Has not rights for view model3d replica');
    }
}