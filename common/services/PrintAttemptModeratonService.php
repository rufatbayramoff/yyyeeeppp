<?php
/**
 * Created by mitaichik
 */

namespace common\services;


use common\components\exceptions\AssertHelper;
use common\models\repositories\FileRepository;
use common\models\StoreOrderAttemp;
use common\models\factories\FileFactory;
use common\models\StoreOrderAttemptModerationFile;
use frontend\models\ps\AddResultImageForm;

/**
 * Class PrintAttemptModeratonService
 * @package common\services
 */
class PrintAttemptModeratonService
{
    /**
     * @var FileFactory
     */
    private $fileFactory;

    /** @var FileRepository */
    protected $fileRepository;

    /**
     * @param FileFactory $fileFactory
     */
    public function injectDependencies(FileFactory $fileFactory, FileRepository $fileRepository)
    {
        $this->fileFactory = $fileFactory;
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param AddResultImageForm $form
     * @return StoreOrderAttemptModerationFile
     * @throws \Exception
     */
    public function addResult(StoreOrderAttemp $attemp, AddResultImageForm $form)
    {
        $file = $this->fileFactory->createFileFromUploadedFile($form->file);
        $file->is_public = true;
        $file->setOwner(StoreOrderAttemptModerationFile::class, 'file_id');
        $this->fileRepository->save($file);

        $moderationFile = new StoreOrderAttemptModerationFile();
        $moderationFile->file_id = $file->id;

        $attemp->moderation->link('storeOrderAttemptModerationFiles', $moderationFile);

        return $moderationFile;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param StoreOrderAttemptModerationFile $moderationFile
     */
    public function deleteResultFile(StoreOrderAttemp $attemp, StoreOrderAttemptModerationFile $moderationFile)
    {
        AssertHelper::assert($attemp->id == $moderationFile->attempt_id);
        $attemp->moderation->unlink('storeOrderAttemptModerationFiles', $moderationFile, true);
        AssertHelper::assert($moderationFile->file->delete());
    }

}