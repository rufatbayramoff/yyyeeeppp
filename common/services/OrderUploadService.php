<?php

namespace common\services;

use common\components\DateHelper;
use common\components\FileTypesHelper;
use common\interfaces\Model3dBasePartInterface;
use common\models\CuttingPack;
use common\models\File;
use frontend\widgets\Model3dUploadWidget;
use yii\web\NotFoundHttpException;

class OrderUploadService
{
    const PACK_TYPE_PRINTING = 'printing';
    const PACK_TYPE_CUTTING  = 'cutting';
    const PACK_TYPE_CNC      = 'cnc';
    const PACK_TYPE_PREORDER = 'preorder';
    const PACK_TYPE_PREORDER_DOWNLOADED = 'preorder_downloaded';


    public function getFileByUuid($uuid)
    {
        $nowDate = DateHelper::now();
        $file    = File::find()->where(
            [
                'uuid'       => $uuid,
                'ownerClass' => Model3dUploadWidget::class,
                'ownerField' => 'uploadFile',
            ]
        )->andWhere(['>=', 'expire', $nowDate])->one();
        if (!$file) {
            throw new NotFoundHttpException();
        }
        return $file;
    }

    /**
     * @param File[] $files
     */
    public function detectFilesPackType($files)
    {
        $printingFormats = [Model3dBasePartInterface::STL_FORMAT, Model3dBasePartInterface::PLY_FORMAT, Model3dBasePartInterface::OBJ_FORMAT, Model3dBasePartInterface::FORMAT_3MF];
        $cncFormats = [Model3dBasePartInterface::STEP_FORMAT, Model3dBasePartInterface::IGES_FORMAT, Model3dBasePartInterface::STEP_FORMAT_2, Model3dBasePartInterface::IGES_FORMAT_2];
        $cuttingFormats = CuttingPack::ALLOWED_FORMATS;

        $typeDetected = null;
        foreach ($files as $file) {
            if (FileTypesHelper::isFileInExt($file, $printingFormats)) {
                if ($typeDetected==null || $typeDetected==self::PACK_TYPE_PRINTING) {
                    $typeDetected = self::PACK_TYPE_PRINTING;
                } else {
                    $typeDetected = self::PACK_TYPE_PREORDER;
                }
            }
            if (FileTypesHelper::isFileInExt($file, $cncFormats)) {
                if ($typeDetected==null || $typeDetected==self::PACK_TYPE_CNC) {
                    $typeDetected = self::PACK_TYPE_CNC;
                } else {
                    $typeDetected = self::PACK_TYPE_PREORDER;
                }
            }
            if (FileTypesHelper::isFileInExt($file, $cuttingFormats)) {
                if ($typeDetected==null || $typeDetected==self::PACK_TYPE_CUTTING) {
                    $typeDetected = self::PACK_TYPE_CUTTING;
                } else {
                    $typeDetected = self::PACK_TYPE_PREORDER;
                }
            }
        }
        if (!$typeDetected) {
            $typeDetected = self::PACK_TYPE_PREORDER;
        }
        return $typeDetected;
    }
}
