<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 10.04.17
 * Time: 16:31
 */

namespace common\services;

use common\components\ArrayHelper;
use common\components\UserSession;
use common\interfaces\Model3dBaseInterface;
use common\models\factories\Model3dReplicaFactory;
use common\models\model3d\Model3dBase;
use common\models\Model3dReplica;
use common\models\Model3dViewedState;
use console\controllers\BaseModelsController;
use DateTime;
use DateTimeZone;
use frontend\models\model3d\Model3dForm;
use frontend\models\model3d\Model3dItemForm;
use frontend\models\serialize\Model3dTextureSerializer;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use Yii;
use yii\db\IntegrityException;

class Model3dViewedStateService
{
    /**
     * @var FrontUser
     */
    public $user;

    /**
     * @var UserSession
     */
    public $userSession;


    /**
     * Model3dService constructor.
     *
     * @param bool|FrontUser $user
     * @param bool|UserSession $userSession
     * @param array $flowRules
     */
    public function __construct($user = false, $userSession = false, array $flowRules = [])
    {
        if ($user !== false) {
            $this->user = $user;
        } else {
            $this->user = UserFacade::getCurrentUser();
        }

        if ($userSession !== false) {
            $this->userSession = $userSession;
        } else {
            $this->userSession = UserFacade::getUserSession();
        }

        $this->flowRules = $flowRules;
    }

    protected function serializeModel3dPartsQty(Model3dBaseInterface $model3d)
    {
        $qty = [];
        $parts = $model3d->getActiveModel3dParts();
        foreach ($parts as $model3dPart) {
            $qty[$model3dPart->id] = $model3dPart->qty;
        }
        return $qty;
    }

    /**
     * Serialize form model3dState. If it`s replica, get original model3d part id.
     *
     * @param Model3dBaseInterface $model3d
     * @return array
     */
    protected function serializeModel3dTexture(Model3dBaseInterface $model3d)
    {
        /** @var Model3dTextureSerializer $model3dTextureSerializer */
        $model3dTextureSerializer = Yii::createObject(Model3dTextureSerializer::class);

        $returnValue = [];
        if ($model3d->isOneTextureForKit()) {
            $texture = $model3d->getKitTexture();
            $returnValue['isOneMaterialForKit'] = true;
            $returnValue['modelTexture'] = $model3dTextureSerializer->serialize($texture);
        } else {
            $returnValue['isOneMaterialForKit'] = false;
            foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
                $texture = $model3dPart->getTexture();
                $partId = $model3dPart->id;
                if ($model3d instanceof Model3dReplica) {
                    $partId = $model3dPart->original_model3d_part_id;
                }
                $returnValue['partsMaterial'][$partId] = $model3dTextureSerializer->serialize($texture);
            }
        }
        return $returnValue;
    }

    public function registerModel3dViewForm(Model3dItemForm $model3dForm)
    {
        $userSession = UserFacade::getUserSession();
        $model3d = $model3dForm->model3d;

        /** @var Model3dViewedState $model3dViewedState */
        $model3dViewedState = Model3dViewedState::findOne(['user_session_id' => $userSession->id, 'model3d_id' => $model3dForm->originalModel3d->id]);
        if (!$model3dViewedState) {
            $model3dViewedState = Yii::createObject(Model3dViewedState::class);
            $model3dViewedState->user_session_id = $userSession->id;
            $model3dViewedState->model3d_id = $model3dForm->originalModel3d->id;
        }

        $model3dViewedState->model3dTextureInfo = PrinterMaterialService::serializeModel3dTexture($model3d);
        $model3dViewedState->qty_info = [
            'model3dQty'      => $model3dForm->quantity,
            'model3dPartsQty' => ArrayHelper::map($model3d->getActiveModel3dParts(), 'file_id', 'qty')
        ];
        $model3dViewedState->scale_info = [
            'model3dScale' => $model3dForm->scaleBy
        ];
        $model3dViewedState->date = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        try {
            $model3dViewedState->safeSave();
        } catch (IntegrityException $exception) {
            // Do noting is Duplicate entry for key model_viewed, skip it
        }
        return $model3dViewedState;
    }


    public function restoreModel3dState(Model3dViewedState $model3dViewedState)
    {
        $model3d = $model3dViewedState->model3d;
        if ($model3dViewedState->model3dTextureInfo) {
            $model3dTextureInfo = $model3dViewedState->model3dTextureInfo;
            PrinterMaterialService::unSerializeModel3dTexture($model3d, $model3dTextureInfo);
        }
        if ($model3dViewedState->qty_info) {
            $qtyInfo = $model3dViewedState->qty_info;
            $model3dViewedState->model3dQty = $qtyInfo['model3dQty'];
            if ($qtyInfo['model3dPartsQty']) {
                $model3dViewedState->model3dPartsQty = $qtyInfo['model3dPartsQty'];
                $result = Model3dService::setPartsQtyByFileIds($model3d, $model3dViewedState->model3dPartsQty);
                if (!$result) {
                    return false;
                }
            }
        }
        if ($model3dViewedState->scale_info) {
            $scaleInfo = $model3dViewedState->scale_info;
            $model3dViewedState->model3dScale = $scaleInfo['model3dScale'];
            if (round($model3dViewedState->model3dScale, 1) != 100) {
                $model3dReplica = Model3dReplicaFactory::createModel3dReplica($model3d);
                $model3dReplica->setStoreUnit($model3d->storeUnit);
                $model3dReplica = Model3dService::scaleModel3d($model3dReplica, $model3dViewedState->model3dScale / 100);
                $model3d = $model3dReplica;
            }
        }
        $model3dViewedState->model3dRestored = $model3d;
        return true;
    }
}