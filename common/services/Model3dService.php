<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.06.16
 * Time: 12:01
 */

namespace common\services;

use common\components\ArchiveManager;
use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\InvalidModelException;
use common\components\ps\locator\Size;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\ApiFile;
use common\models\ApiPrintablePack;
use common\models\base\PrinterMaterial;
use common\models\factories\FileFactory;
use common\models\factories\Model3dFactory;
use common\models\File;
use common\models\GeoCountry;
use common\models\Model3d;
use common\models\model3d\ScalePartPropertiesOperation;
use common\models\Model3dHistory;
use common\models\Model3dImg;
use common\models\Model3dPart;
use common\models\Model3dPartProperties;
use common\models\Model3dReplica;
use common\models\Model3dTag;
use common\models\Model3dTexture;
use common\models\Model3dViewedState;
use common\models\PsPrinter;
use common\models\repositories\FileRepository;
use common\models\repositories\Model3dRepository;
use common\models\SiteTag;
use common\models\User;
use common\models\UserLocation;
use common\models\UserSession;
use common\modules\product\interfaces\ProductInterface;
use console\jobs\model3d\ConverterJob;
use console\jobs\model3d\ParserJob;
use console\jobs\QueueGateway;
use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use frontend\models\model3d\Model3dFacade;
use frontend\models\ps\PsFacade;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use HttpException;
use lib\file\UploadException;
use lib\MeasurementUtil;
use Yii;
use yii\base\NotSupportedException;
use yii\base\UserException;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\GoneHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;


class Model3dService
{
    /**
     * @var FrontUser
     */
    public $user;

    /**
     * @var UserSession
     */
    public $userSession;


    /**
     * @var  ArchiveManager
     */
    public $archiveManager;


    /** @var  Model3dImageService */
    public $model3dImgService;

    /** @var Model3dRepository */
    public $model3dRepository;


    /**
     * service flow rules.
     * flowRules['skipQueueJobs'] = true - will skip calling queue jobs
     *
     * @var array
     */
    public $flowRules;
    const FLOW_SKIP_QUEUE_JOBS = 'skipQueueJobs';

    /**
     * Model3dService constructor.
     *
     * @param bool|FrontUser $user
     * @param bool|UserSession $userSession
     * @param ArchiveManager $archiveManager
     * @param Model3dImageService $model3dImgService
     * @param Model3dRepository $model3dRepository
     * @param array $flowRules
     * @throws NotFoundHttpException
     */
    public function __construct(
        $user = false,
        $userSession = false,
        ArchiveManager $archiveManager,
        Model3dImageService $model3dImgService,
        Model3dRepository $model3dRepository,
        array $flowRules = []
    )
    {
        if ($user !== false) {
            $this->user = $user;
        } else {
            $this->user = UserFacade::getCurrentUser();
        }

        if ($userSession !== false) {
            $this->userSession = $userSession;
        } else {
            $this->userSession = UserFacade::getUserSession();
        }

        $this->archiveManager    = $archiveManager;
        $this->model3dImgService = $model3dImgService;
        $this->model3dRepository = $model3dRepository;
        $this->flowRules         = $flowRules;
    }

    /**
     * Filter collection and get only active files
     *
     * @param Model3dBaseInterface $model3d
     * @return array
     */
    public static function getModel3dActivePartsAndImagesList($model3d)
    {
        $returnValue = $model3d->getActiveModel3dImages();
        foreach ($model3d->model3dParts as $model3dPart) {
            if (($model3dPart->user_status === Model3dBasePartInterface::STATUS_INACTIVE) || ($model3dPart->moderator_status === Model3dBasePartInterface::STATUS_BANNED)) {
                // Do nothing
            } else {
                $returnValue[$model3dPart->file_id] = $model3dPart;
            }
        }
        return $returnValue;
    }

    /**
     * @param $model3d
     * @return array
     */
    public static function getModel3dImages($model3d)
    {
        $returnValue = [];
        foreach ($model3d->model3dImgs as $model3dImg) {
            $returnValue[$model3dImg->file_id] = $model3dImg;
        }
        return $returnValue;
    }

    /**
     * get estimated print time (EPT) for given model
     * if model is a kit, gives total print time for all 3d files
     * Usage example:
     * $time = Model3dService::calculateModel3dPrintTime($model);
     * echo  str_replace("ago", "", app('formatter')->asRelativeTime(time() - $time, time()));
     *
     * @param Model3dBaseInterface $model3d
     * @return int - minutes to print
     */
    public static function calculateModel3dPrintTime(Model3dBaseInterface $model3d)
    {
        $weight = $model3d->getWeight();
        return $weight * 17;
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param PsPrinter $printer
     * @param PrinterMaterial[]|null $allowedMaterials
     */
    public static function setCheapestMaterialInPrinter(Model3dBaseInterface $model3d, PsPrinter $printer, array $allowedMaterials = null)
    {
        $colorsCost = $printer->getPrintPrices();

        if ($allowedMaterials) {
            $allowedMaterialsFilamentTitles = ArrayHelper::getColumn($allowedMaterials, 'filament_title');
            foreach (array_keys($colorsCost) as $filamentTitle) {
                if (!in_array($filamentTitle, $allowedMaterialsFilamentTitles)) {
                    unset($colorsCost[$filamentTitle]);
                }
            }
        }

        if ($model3d->isOneTextureForKit()) {
            $texture                  = $model3d->getKitTexture();
            if (!$texture->printerMaterial) {
                $texture->printerMaterial = PrinterMaterialService::getCheapestMaterialInGroup($texture->calculatePrinterMaterialGroup(), $texture->printerColor, $colorsCost);
            }
        } else {
            foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
                $texture                  = $model3dPart->getCalculatedTexture();
                if (!$texture->printerMaterial) {
                    $texture->printerMaterial = PrinterMaterialService::getCheapestMaterialInGroup($texture->calculatePrinterMaterialGroup(), $texture->printerColor, $colorsCost);
                }
            }
        }
    }

    public static function resetTextureMatreialInfo(Model3dBaseInterface $model3d)
    {
        if ($model3d->isOneTextureForKit()) {
            $texture = $model3d->getKitTexture();
            if (!$texture->printerMaterialGroup) {
                $texture->printerMaterialGroup = $texture->calculatePrinterMaterialGroup();
            }
            // $texture->printerMaterial = null;
        } else {
            foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
                $texture = $model3dPart->getCalculatedTexture();
                if (!$texture->printerMaterialGroup) {
                    $texture->printerMaterialGroup = $texture->calculatePrinterMaterialGroup();
                }
                //  $texture->printerMaterial = null;
            }
        }
    }

    /**
     * if false, don't show viewstate widget
     * false returned if in current active parts no given file
     *
     * @param Model3dBaseInterface $model3d
     * @param array $model3dPartsQty
     * @return bool
     */
    public static function setPartsQtyByFileIds(Model3dBaseInterface $model3d, array $model3dPartsQty)
    {
        foreach ($model3d->getActiveModel3dParts() as $part) {
            if (!isset($model3dPartsQty[$part->file_id])) {
                return false;
            }
            $part->qty = $model3dPartsQty[$part->file_id] ?? $part->qty;
        }
        return true;
    }


    /**
     * Create and save model3d. By uploadFiles and model3dProperties.
     *
     * @param $model3dProperties
     * @param $uploadFiles - image, stls, archives
     * @return Model3d|null
     * @throws Exception
     */
    public function createModel3dByUploadFilesList($model3dProperties, $uploadFiles)
    {
        $model3d     = null;
        $transaction = app('db')->beginTransaction();
        try {
            $filesInfoList = [];
            /** @var FileFactory $fileFactory */
            $fileFactory = Yii::createObject([
                'class' => FileFactory::class,
                'user'  => $this->user
            ]);
            $fileRepository = Yii::createObject(FileRepository::class);
            foreach ($uploadFiles as $uploadFile) {
                $fileName = $uploadFile->name;
                if (in_array(pathinfo($fileName, PATHINFO_EXTENSION), ArchiveManager::ARCHIVE_FORMATS)) {
                    $filesInArchive = $this->createFilesFromArchive($uploadFile);
                    foreach ($filesInArchive as $file) {
                        $filesInfoList[] = [
                            'file' => $file
                        ];
                    }
                } else {
                    $file = $fileFactory->createFileFromUploadedFile($uploadFile);
                    $fileRepository->save($file);
                    $filesInfoList[] = [
                        'file' => $file
                    ];
                }
            }
            if (empty($filesInfoList)) {
                throw new UploadException(_t('site.upload', 'No 3D Model Files'));
            }
            $model3d = $this->createModel3dByFilesInfoList($model3dProperties, $filesInfoList);
            if ($model3d) {
                $transaction->commit();
                // In transaction manual create background jobs
                $this->createModel3dBackgroundJobs($model3d);
            } else {
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $model3d;
    }

    /**
     * @param UploadedFile $upload
     * @return array
     * @throws \common\components\exceptions\InvalidModelException
     * @throws UploadException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws Exception
     */
    private function createFilesFromArchive(UploadedFile $upload)
    {
        $toFolder = Yii::$app->getRuntimePath() . '/unzip/' . date('y_m_d') . '_' . md5_file($upload->tempName);
        $za       = new \ZipArchive();
        if ($za->open($upload->tempName) !== true) {
            throw new UploadException(_t('site.upload', 'Cannot open zip file.'));
        }
        $za->extractTo($toFolder);
        $za->close();
        $fileRepository = \Yii::createObject(FileRepository::class);
        $objects        = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($toFolder), \RecursiveIteratorIterator::SELF_FIRST);
        $result         = [];
        foreach ($objects as $name => $object) {
            if (is_dir($name)) {
                continue;
            }
            $fileExt = strtolower(pathinfo($name, PATHINFO_EXTENSION));
            $fileFactory = Yii::createObject([
                'class' => FileFactory::class,
                'user'  => $this->user
            ]);
            if (in_array($fileExt, Model3dBasePartInterface::ALLOWED_FORMATS)) {
                $file = $fileFactory->createFileFromPath($name);
                $fileRepository->save($file);
                $result[] = $file;
            } else {
                if (in_array($fileExt, Model3dBaseImgInterface::ALLOWED_FORMATS)) {
                    $img            = $fileFactory->createFileFromPath($name);
                    $img->is_public = true;
                    $fileRepository->save($img);
                    $result[] = $img;
                }
            }
        }
        return $result;
    }

    /**
     * @param $model3dProperties
     * @param $storeUnitProperties
     * @param $filesInfoList
     * @return Model3d
     * @throws InvalidModelException
     * @internal param $files
     */
    public function createModel3dByFilesInfoList($model3dProperties, $filesInfoList)
    {
        $files = [];
        foreach ($filesInfoList as $fileInfo) {
            $files[] = $fileInfo['file'];
        }
        return $this->createModel3dByFilesList($model3dProperties, $files);
    }

    public function createModel3dByFilesList($model3dProperties, $files)
    {
        /** @var Model3dFactory $model3dFactory */
        $model3dFactory = Yii::createObject(
            [
                'class'       => Model3dFactory::class,
                'user'        => $this->user,
                'userSession' => $this->userSession
            ]
        );
        $model3d        = $model3dFactory->createModel3dByFilesList($files, $model3dProperties);
        if (!$model3d->validate()) {
            Yii::error('Validate model3d: ' . Html::errorSummary($model3d), 'app');
            throw new  InvalidModelException($model3d);
        }

        /*
         Model3dPart cann`t be validated because model3d_id attribute is not setted?!
        foreach ($model3d->model3dParts as $model3dPart) {
            if (!$model3dPart->validate()) {
                throw new  InvalidModelException($model3dPart);
            }
        }
        foreach ($model3d->model3dImgs as  $model3dImg) {
            if (!$model3dImg->validate()) {
                throw new  InvalidModelException($model3dImg);
            }
        }
        */


        $this->model3dRepository->save($model3d);

        if (empty($this->flowRules[self::FLOW_SKIP_QUEUE_JOBS])) {
            $this->createModel3dBackgroundJobs($model3d);
        }

        return $model3d;
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function createModel3dBackgroundJobs($model3d)
    {
        if (app('db')->getTransaction() && app('db')->getTransaction()->getIsActive()) {
            return;
        }
        foreach ($model3d->model3dParts as $model3dPart) {
            if (Yii::$app->getModule('convert')->converter->isConvertablePart($model3dPart)) {
                $job = ConverterJob::createByPart($model3dPart);
                QueueGateway::addFileJob($job);
            } else {
                Model3dFacade::createQueueJobs($model3dPart->file, ['model3d_id' => $model3d->id]);
            }
        }
    }

    public function repeateParserJob($model3d)
    {
        if (app('db')->getTransaction() && app('db')->getTransaction()->getIsActive()) {
            return;
        }
        foreach ($model3d->model3dParts as $model3dPart) {
            if (!Yii::$app->getModule('convert')->converter->isConvertablePart($model3dPart) && !$model3dPart->isCaclulatedProperties()) {
                $job = ParserJob::create($model3dPart->file);
                QueueGateway::addFileJob($job, QueueGateway::ADD_STRATEGY_ANYWAY);
            }
        }
    }


    /**
     * Bind image to model3d
     *
     * TODO: Фактически тут функционал фабрики смешанный с save. Реализовать отдельную фабрику Model3dImgFactory.
     *
     * @param Model3d $model3d
     * @param array $image - format ['file_id'=>,'filename'=>, 'displayName'=>,'extension'=>]
     * @return int
     * @throws \InvalidArgumentException
     * @throws \yii\base\Exception
     * @throws Exception
     * @deprecated
     */
    public static function bindImage(Model3d $model3d, $userId, array $image)
    {
        $model3dImgObj = new Model3dImg();
        $fileId        = $image['file_id'];
        $now           = new Expression('NOW()');
        $data          = [
            'user_id'    => $userId,
            'model3d_id' => $model3d->id,
            'basename'   => $image['filename'],
            'extension'  => $image['extension'],
            'type'       => 'owner',
            'title'      => mb_substr($image['displayFilename'], 0, 45),
            'file_id'    => $fileId,
            'created_at' => $now,
            'updated_at' => $now
        ];
        $model3dImgObj->load($data, '');
        $validateRes = $model3dImgObj->validate();
        if ($validateRes && $model3dImgObj->save()) {
            return $model3dImgObj;
        } else {
            \Yii::info(var_export($model3dImgObj->getErrors(), true), 'tsdebug');
            $title = '<h3>' . self::class . '</h3>';
            throw new \InvalidArgumentException($title . Html::errorSummary($model3dImgObj));
        }
    }

    /**
     * bind 3d file to 3d model. one model can have many 3d files.
     * one 3d file can be bined to one 3d model
     *
     * TODO: Фактически тут функционал фабрики смешанный с save. Реализовать отдельную фабрику Model3dPartFactory.
     *
     * @param Model3d $model3d
     * @param array $file - format ['file_id'=>,'filename'=>, 'displayFilename'=>,'extension'=>]
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @deprecated
     */
    public static function bindFile(Model3d $model3d, array $file)
    {
        $model3dPartObj = new Model3dPart();
        $fileId         = $file['file_id'];

        $data = [
            'model3d_id'       => $model3d->id,
            'format'           => $file['extension'],
            'name'             => mb_substr($file['displayFilename'], 0, 45),
            'file_id'          => $fileId,
            'moderator_status' => 'new', // TODO: вынести в константы
            'user_status'      => 'active'
        ];
        $model3dPartObj->load($data, '');
        $model3dPartObj->safeSave();
        return $model3dPartObj;
    }

    /**
     * Update model3d tags
     *
     * @param Model3d $model3d
     * @return bool
     * @throws \Exception
     */
    public static function saveTags(Model3d $model3d)
    {
        /** @var SiteTag[] $caseInsensitiveTags */
        $caseInsensitiveTags = [];
        foreach ($model3d->tags as $tag) {
            $text                       = trim(mb_strtolower($tag->text));
            $caseInsensitiveTags[$text] = $tag;
        }
        if (count($caseInsensitiveTags) > 10) {
            throw new \yii\base\Exception(_t('front.model3d', 'Maximum 10 tags allowed'));
        }
        /** @var SiteTag[] $currentModel3dTagsArray */
        $currentModel3dTagsArray = [];
        foreach ($model3d->model3dTags as $model3dTag) {
            $currentModel3dTagsArray[$model3dTag->tag->text] = $model3dTag;
        }
        $hasUpdates = false;
        foreach ($caseInsensitiveTags as $tagText => $tag) {
            if (array_key_exists($tagText, $currentModel3dTagsArray)) {
                continue;
            } else {
                $hasUpdates = true;
                $tag->safeSave();
                if (Model3dTag::find()->where(['tag_id' => $tag->id, 'model3d_id' => $model3d->id])->exists()) {
                    continue;
                }
                $model3dTag             = new Model3dTag();
                $model3dTag->tag_id     = $tag->id;
                $model3dTag->model3d_id = $model3d->id;
                $model3dTag->safeSave();
            }
        }
        foreach ($currentModel3dTagsArray as $tagText => $model3dTag) {
            if (!array_key_exists($tagText, $caseInsensitiveTags)) {
                $hasUpdates = true;
                $model3dTag->delete();
            }
        }
        if ($hasUpdates) {
            $userId = UserFacade::getCurrentUserId();
            Model3dHistory::log(
                $userId,
                $model3d,
                Model3dHistory::ACTION_EDITTAG,
                null,
                Json::encode(array_keys($currentModel3dTagsArray)),
                Json::encode(array_keys($caseInsensitiveTags))
            );
        }
        $model3d->hasPublishUpdates = $hasUpdates;
    }

    /**
     * @param Model3d $model3d
     * @param bool $affKey
     * @param bool $printablePackPublicToken
     * @return bool
     * @throws \HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function isAvailableForCurrentUser(Model3dBaseInterface $model3d, $affKey = false, $printablePackPublicToken = false)
    {
        if (!$model3d->is_active) {
            return false;
        }
        if (UserFacade::isObjectOwner($model3d, $this->user, $this->userSession)) {
            return true;
        }

        if ($model3d->storeUnit?->isTestOrder()) {
            return true;
        }

        try {
            /** @var Model3dService $model3dService */
            $model3dService = Yii::createObject(Model3dService::class);
            $model3dService->tryValidateModelStore($model3d, $affKey, $printablePackPublicToken);
        } catch (GoneHttpException $exception) {
            \Yii::warning($exception->getMessage(), 'store');
            return false;
        }
        return true;
    }

    protected static function checkIsModel3dViewedByCurrentUser($model3d)
    {
        $userSessionId      = UserFacade::getUserSession();
        $user               = UserFacade::getCurrentUser();
        $model3dViewedState = Model3dViewedState::findOne(['user_session_id' => $userSessionId, 'model3d_id' => $model3d->id]);
        if (!$model3dViewedState && $user) {
            $model3dViewedState = Model3dViewedState::find()->joinWith('userSession')->where(['user_id' => $user->id, 'model3d_id' => $model3d->id])->one();
        }
        $lastPossibleDate = new DateTime('now', new DateTimeZone('UTC'));
        $lastPossibleDate->sub(new DateInterval('PT24H'));
        $lastPossibleDate = $lastPossibleDate->format('Y-m-d H:i:s');

        if ($model3dViewedState && ($model3d->created_at > $lastPossibleDate)) {
            return true;
        }
        return false;
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @throws GoneHttpException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function tryCanBeOrderedByCurrentUser(Model3dBaseInterface $model3d): void
    {
        if (UserFacade::isObjectOwner($model3d)) {
            return;
        }

        if (($model3d instanceof Model3dReplica) && ($model3d->isOrderPayed())) {
            throw new GoneHttpException(_t('site.store', 'Model already ordered'));
        }

        $this->tryValidateModelStore($model3d, null, null);
    }

    /**
     * @param Model3d $model3d
     * @throws \yii\base\UserException
     */
    public function tryCanBeManagedByCurrentUser(Model3d $model3d)
    {
        $user = UserFacade::getCurrentUser();
        if (!$model3d->company) {
            throw new BusinessException('Please create company at first');
        }
        if ($user->company->id === $model3d->company->id) {
            return;
        }
        UserFacade::tryCheckObjectOwner($model3d);
        if ($model3d->originalModel3d) {
            throw new BusinessException('Can`t manage not original model');
        }
    }

    /**
     * @param Model3dBasePartInterface $model3dPart
     * @param Size $newSize
     *
     * @throws \common\components\operations\PostValidateException
     * @throws \common\components\operations\PreValidateException
     * @throws \yii\base\Exception
     */
    public function setPartSize(Model3dBasePartInterface $model3dPart, Size $newSize)
    {
        $currentSize = $model3dPart->getSize();
        $minPercent  = $newSize->height / $currentSize->height;
        $minPercent  = min($minPercent, $newSize->width / $currentSize->width);
        $minPercent  = min($minPercent, $newSize->length / $currentSize->length);

        $scaleOperation = new ScalePartPropertiesOperation($minPercent);
        $scaleOperation->setModel3dPartProperties($model3dPart->model3dPartProperties);
        $scaleOperation->execute();
        $newProperties = $scaleOperation->getResult();
        $newProperties->safeSave();
        $model3dPart->setModel3dPartProperties($newProperties);
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param bool $affKey - affiliate key
     * @param bool $printablePackPublicToken
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     * @throws \HttpException
     * @throws GoneHttpException
     */
    public function tryValidateModelStore(Model3dBaseInterface $model3d, $affKey = false, $printablePackPublicToken = false)
    {
        if ($model3d->user && $model3d->user->status === User::STATUS_DELETED) {
            throw new GoneHttpException(_t('site.error', 'Model not found.'));
        }
        if (!$model3d->isActive()) {
            throw new GoneHttpException(_t('site.error', 'Model inactive'));
        }

        if ($model3d->user_id === User::USER_THINGIVERSE || $model3d->getSourceModel3d()->user_id === User::USER_THINGIVERSE) { // give access to thingiverse
            if ($model3d->getProductStatus() === Model3dBaseInterface::STATUS_REJECTED) {
                throw new GoneHttpException(_t('site.store', 'Model not found or not published.'));
            }
            return true;
        }
        if (!$model3d->user && $model3d->userSession && $this->userSession) {
            // User session check
            if ($model3d->userSession->uuid === $this->userSession->uuid) {
                return true;
            }
        }

        if ($model3d->source === Model3d::SOURCE_API) {
            // @TODO - move to another source later on
            if ($this->user && $model3d->getAuthor() && $model3d->getAuthor()->id === $this->user->id) {
                return true;
            }
            if ($printablePackPublicToken) {
                $printablePack = ApiPrintablePack::tryFind(['public_token' => $printablePackPublicToken]);
                if ($printablePack->model3d->id !== $model3d->id) {
                    if (self::checkIsModel3dViewedByCurrentUser($model3d)) {
                        return true;
                    }
                    throw new NotFoundHttpException(_t('site.store', 'Model not found or not published.'));
                }
                if ($printablePack->status !== ApiPrintablePack::STATUS_ACTIVE) {
                    throw new NotFoundHttpException(_t('site.store', 'Printable pack is inactive.'));
                }
                return true;
            } elseif ($affKey) {
                return static::tryValidateAffiliateModel($affKey);
            } else {
                if (self::checkIsModel3dViewedByCurrentUser($model3d)) {
                    return true;
                }
            }
        }

        if (!$model3d->isPublished()) {
            throw new GoneHttpException(_t('site.store', 'Model not found or not published.'));
        }
        return true;
    }

    /**
     * @param $affKey
     * @return bool
     * @throws HttpException
     */
    private static function tryValidateAffiliateModel($affKey)
    {
        $apiFile = ApiFile::findByAffiliateKey($affKey);

        if ($apiFile && $apiFile->isForSell()) {
            return true;
        }
        throw new \yii\web\HttpException(400, _t('site.store', 'Not valid [akey]'));
    }

    public static function resetScale(Model3dBaseInterface $model3d)
    {
        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
            $model3dPart->resetScale();
        }
        return $model3d;
    }


    /**
     * @param Model3dBaseInterface $model3d
     * @param int $scaleBy
     * @return Model3dBaseInterface
     * @throws BusinessException
     * @throws \common\components\operations\PostValidateException
     * @throws \common\components\operations\PreValidateException
     */
    public static function scaleModel3d(Model3dBaseInterface $model3d, $scaleBy = 1)
    {
        $scaleOperation = new ScalePartPropertiesOperation($scaleBy);
        $model3d->scale = $scaleBy;
        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
            if (!$model3dPart->model3dPartProperties) {
                throw new BusinessException('Scale not available at this moment. Parsing model in progress. Please try again later.');
            }
            $scaleOperation->setModel3dPartProperties($model3dPart->model3dPartProperties);
            $scaleOperation->execute();
            $newProperties = $scaleOperation->getResult();
            $newProperties->safeSave(); // TODO: Remove save from here!
            $model3dPart->setModel3dPartProperties($newProperties);
        }
        return $model3d;
    }

    /**
     * @param Model3dPart $model3dPart
     * @param bool $checkAcces
     * @throws NotSupportedException
     */
    public static function deleteModel3dPart(Model3dPart $model3dPart, $checkAcces = true)
    {
        if ($checkAcces && !UserFacade::isObjectOwner($model3dPart->model3d)) {
            throw new NotSupportedException(_t('front.site', 'You cannot delete this model'));
        }
        $userId = UserFacade::getCurrentUserId();
        Model3dHistory::log(
            $userId,
            $model3dPart->model3d,
            Model3dHistory::ACTION_UPDATE_PART,
            json_encode(['id' => $model3dPart->id, 'name' => $model3dPart->name]),
            json_encode(['status' => 'active']),
            json_encode(['status' => 'deleted'])
        );
        $model3dPart->user_status = Model3dBasePartInterface::STATUS_INACTIVE;
        $model3dPart->save(false);
        //$file->delete();
    }

    public function setActive(Model3dBaseInterface $model3d)
    {
        if ($model3d instanceof  Model3d) {
            $model3d->productCommon->is_active = 1;
            $model3d->productCommon->save(['is_active']);
            return ;
        }
        $model3d->is_active = 1;
        $model3d->save(['is_active']);
    }

    /**
     * @param $model3d
     */
    public function updateProductPublishStatus($model3d)
    {
        if (\in_array($model3d->product_status, [ProductInterface::STATUS_PUBLISHED_UPDATED, ProductInterface::STATUS_PUBLISHED_PUBLIC], true)) {
            $model3d->productCommon->product_status = ProductInterface::STATUS_PUBLISH_PENDING;
            $model3d->productCommon->safeSave();
        }
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param File $file
     * @param bool $skipNotSupported
     * @throws InvalidModelException
     * @throws NotFoundHttpException
     * @throws UploadException
     * @throws UserException
     * @throws \ErrorException
     * @throws \console\jobs\JobException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function addFile(Model3dBaseInterface $model3d, File $file, $skipNotSupported = false)
    {
        // Upack if nessery
        if ($this->archiveManager->isArchive($file)) {
            $filesFromArchive = $this->archiveManager->unpackFiles($file);
            foreach ($filesFromArchive as $fileFromArchive) {
                $this->addFile($model3d, $fileFromArchive, true);
            }
            return;
        }
        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
            if ($model3dPart->file->equalName($file)) {
                return;
            }
        }


        // Check dublicates
        foreach ($model3d->getActiveModel3dImages() as $model3dImage) {
            if ($model3dImage->file->md5sum == $file->md5sum) {
                return;
            }
        }

        $fileExt = $file->getFileExtension();
        if (in_array($fileExt, Model3dBasePartInterface::ALLOWED_FORMATS)) {
            // Is model3dPart
            Model3dPartService::addModel3dPartFileAndSave($model3d, $file);
            if ($model3d->isScaled() && ($model3d->model_units === MeasurementUtil::MM)) {
                Model3dService::resetScale($model3d);
                $this->model3dRepository->save($model3d);
            }
        } elseif (in_array($fileExt, Model3dBaseImgInterface::ALLOWED_FORMATS)) {
            // Is model3dImage
            $this->model3dImgService->addModel3dImgFileAndSave($model3d, $file);
        } else {
            if (!$skipNotSupported) {
                throw new BusinessException(_t('site.error', 'Invalid file format'));
            }
        }

    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param string $fileMd5NameSize
     * @return Model3dBasePartInterface|null
     */
    public function findModel3dPartByMd5NameSize(Model3dBaseInterface $model3d, $fileMd5NameSize)
    {
        foreach ($model3d->model3dParts as $model3dPart) {
            if ($fileMd5NameSize && $model3dPart->isActive() && ($model3dPart->file->getMd5NameSize() === $fileMd5NameSize)) {
                return $model3dPart;
            }
        }
        return null;
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param string $fileMd5NameSize
     * @return Model3dBasePartInterface|null
     */
    public function findModel3dImgByMd5NameSize(Model3dBaseInterface $model3d, $fileMd5NameSize)
    {
        foreach ($model3d->model3dImgs as $model3dImage) {
            if ($fileMd5NameSize && $model3dImage->isActive() && ($model3dImage->file->getMd5NameSize() === $fileMd5NameSize)) {
                return $model3dImage;
            }
        }
        return null;
    }

    /**
     * @param Model3dBaseInterface $model3d
     */
    public function reviewCoverFile(Model3dBaseInterface $model3d)
    {
        $model3dImages = $model3d->getActiveModel3dImages();
        if ($model3dImages) {
            $firstImage = reset($model3dImages);
            $model3d->setCoverFile($firstImage->file);
            return;
        }
        $model3dParts = $model3d->getActiveModel3dParts();
        if ($model3dParts) {
            $firstPart = reset($model3dParts);
            $model3d->setCoverFile($firstPart->file);
            return;
        }
        $model3d->zerroCoverFile();
    }

    /**
     * @param $partOrImgUid
     * @return Model3dBasePartInterface|Model3dBaseImgInterface null
     * @throws \yii\web\GoneHttpException
     * @throws \HttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function deleteFileByUid($partOrImgUid)
    {
        $obj = null;
        if ($model3dPart = $this->model3dRepository->getPartByUid($partOrImgUid)) {
            $model3d = $model3dPart->getAttachedModel3d();
            $obj     = $model3dPart;
            $this->tryCanBeOrderedByCurrentUser($model3d);
            $model3dPart->setInactive();
            $model3dPart->safeSave();
        } elseif ($model3dImage = $this->model3dRepository->getImageByUid($partOrImgUid)) {
            $model3d = $model3dImage->getAttachedModel3d();
            $obj     = $model3dImage;
            $this->tryCanBeOrderedByCurrentUser($model3d);
            $model3dImage->deleted_at = DateHelper::now();
            $model3dImage->safeSave();

        }
        if ($model3d->isEmpty()) {
            $model3d->productCommon->is_active = 0;
            $model3d->productCommon->safeSave();
        } elseif ($model3d && $model3d->coverFile) {
            $this->reviewCoverFile($model3d);
        }
        return $obj;
    }

    public function duplicateModel3dPart($model3dPart, int $qty = 1)
    {
        $model3d                    = $model3dPart->model3d;
        $fileFactory                = Yii::createObject(FileFactory::class);
        $file                       = $fileFactory->createFileFromCopy($model3dPart->file);
        $copyModel3dPart            = Model3dPartService::addModel3dPartSave($model3d, $file);
        if($model3dPartProperties = $model3dPart->model3dPartProperties) {
            $copyProperties             = new Model3dPartProperties();
            $copyProperties->attributes = $model3dPartProperties->attributes;
            unset($copyProperties->id);
            $copyProperties->isNewRecord = true;
            $copyModel3dPart->populateRelation('model3dPartProperties',$copyProperties);
        }
        if($model3dTexture = $model3dPart->model3dTexture) {
            $copy3dTexture = new Model3dTexture();
            $copy3dTexture->attributes = $model3dTexture->attributes;
            unset($copy3dTexture->id);
            $copy3dTexture->isNewRecord = true;
            $copyModel3dPart->setTexture($copy3dTexture);
        }
        $copyModel3dPart->qty = $qty;
        $copyModel3dPart->safeSave();
        $this->model3dRepository->save($model3dPart->model3d);
        return $model3dPart->model3d;
    }

    /**
     * @param $partOrImgUid
     * @return Model3dBasePartInterface|Model3dBaseImgInterface null
     * @throws GoneHttpException
     * @throws HttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function restoreFileByUid($partOrImgUid)
    {
        $obj = null;
        if ($model3dPart = $this->model3dRepository->getPartByUid($partOrImgUid)) {
            $model3d = $model3dPart->getAttachedModel3d();
            $obj     = $model3dPart;
            $this->tryCanBeOrderedByCurrentUser($model3d);
            $model3dPart->setActive();
            $model3dPart->qty = 1;
            $model3dPart->safeSave();
            if (($model3d->getActiveModel3dParts() > 1) && ($model3d->isScaled()) && ($model3d->model_units === MeasurementUtil::MM)) {
                Model3dService::resetScale($model3d);
                $this->model3dRepository->save($model3d);
            }
        } elseif ($model3dImage = $this->model3dRepository->getImageByUid($partOrImgUid)) {
            $model3d = $model3dImage->getAttachedModel3d();
            $obj     = $model3dImage;
            $this->tryCanBeOrderedByCurrentUser($model3d);
            $model3dImage->deleted_at = null;
            $model3dImage->safeSave();
        }
        if ($model3d && $model3d->coverFile) {
            $this->reviewCoverFile($model3d);
        }
        return $obj;
    }


    /**
     * File can be find by $fileMd5NameSize in $model3d
     *
     * @param Model3dBaseInterface $model3d
     * @param $fileMd5NameSize
     * @return Model3dBasePartInterface|Model3dBaseImgInterface null
     */
    public function deleteFileByMd5NameSize(Model3dBaseInterface $model3d, $fileMd5NameSize)
    {
        $obj = null;
        if ($model3dPart = $this->findModel3dPartByMd5NameSize($model3d, $fileMd5NameSize)) {
            $model3dPart->setInactive();
            $model3d = $model3dPart->getAttachedModel3d();
            $obj     = $model3dPart;
            $model3dPart->safeSave();

        } elseif ($model3dImage = $this->findModel3dImgByMd5NameSize($model3d, $fileMd5NameSize)) {
            $model3dImage->deleted_at = DateHelper::now();
            $model3d                  = $model3dImage->getAttachedModel3d();
            $obj                      = $model3dImage;
            $model3dImage->safeSave();
        }

        if ($model3d->coverFile) {
            $this->reviewCoverFile($model3d);
        }
        return $obj;
    }

    public function fixCurrentUserSession(Model3d $model3d)
    {
        $model3d->user_session_id = $this->userSession->id;
        $model3d->safeSave();
    }

    public function fixCurrentUser(Model3d $model3d)
    {
        $this->fixModel3dUser($model3d, $this->user);
    }

    public function fixModel3dUser(Model3d $model3d, FrontUser $user)
    {
        $model3d->user_id = $user->id;
        $model3d->populateRelation('user', $user);
        foreach ($model3d->model3dImgs as $img) {
            $img->user_id = $user->id;
            $img->safeSave();
            $img->file->user_id = $user->id;
            $img->file->safeSave();
        }
        foreach ($model3d->model3dParts as $model3dPart) {
            $model3dPart->file->user_id = $user->id;
            $model3dPart->file->safeSave();
        }
        if ($model3d->coverFile) {
            $model3d->coverFile->user_id = $user->id;
            $model3d->coverFile->safeSave();
        }
        $model3d->safeSave();
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param $isInternational
     * @return array
     */
    public function timeAround(Model3dBaseInterface $model3d, ?GeoCountry $country, bool $isInternational): array
    {
        $hours = PsFacade::getEstimatedPrintingTime($model3d);
        $day = (int)($hours / 24);
        if(!$day || $day < 1) {
            $printingDay = 1;
        } else {
            $printingDay = (int)min($hours / 24, 14);
        }
        if($isInternational) {
            $deliveryDay = 20;
        } else {
            if ($country) {
                $deliveryDay = $country->eta_domestic;
            } else {
                $deliveryDay = 5;
            }
        }
        return ['printingDay' => $printingDay,'deliveryDay' => $deliveryDay];

    }
}