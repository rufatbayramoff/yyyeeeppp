<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.08.16
 * Time: 11:41
 */


namespace common\services;

use common\components\ArrayHelper;
use common\models\factories\LocationFactory;
use common\models\StoreOrderAttemp;
use common\models\DeliveryType;
use common\models\StoreOrder;
use common\models\user\UserIdentityProvider;
use frontend\components\UserSessionFacade;
use frontend\models\delivery\DeliveryForm;
use frontend\models\user\UserLocator;
use lib\geo\LocationUtils;
use \lib\geo\models\Location;

class LocationService
{


    /**
     * @var UserIdentityProvider
     */
    private $identityProvider;

    /**
     * @param UserIdentityProvider $identityProvider
     */
    public function injectDependencies(UserIdentityProvider $identityProvider)
    {
        $this->identityProvider = $identityProvider;
    }

    /**
     * @param StoreOrder $order
     * @return Location
     */
    public static function calulateClientLocationByOrder(StoreOrder $order)
    {
        if ($order->deliveryType->code === DeliveryType::PICKUP) {

            /** @var StoreOrderAttemp $originalAttemp */
            $originalAttemp    = ArrayHelper::first(
                $order->attemps,
                function (StoreOrderAttemp $attemp) {
                    return $attemp->is_offer == false;
                }
            );
            $location          = new Location();
            $printerLocation   = $originalAttemp->machine->location;
            $location->country = $printerLocation->country->iso_code;
            $location->lat     = $printerLocation->lat;
            $location->lon     = $printerLocation->lon;
            return $location;
        } else {
            $location          = new Location();
            $shipAddress       = $order->shipAddress;
            $location->country = $shipAddress->country->iso_code;
            return $location;
        }
    }

    /**
     * @param DeliveryForm $deliveryForm
     * @throws \yii\base\InvalidConfigException
     */
    public function changeUserLocationByDelivery(DeliveryForm $deliveryForm)
    {
        $user = $this->identityProvider->getUser();
        if ($user) {
            $location = LocationFactory::createFromDeliveryForm($deliveryForm);
            if (!empty($location->lat)) {
                UserSessionFacade::setLocationByLocator(UserLocator::createByLocation($location));
            }
        }
    }
}
