<?php
/**
 * Date: 07.07.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace common\services;


use common\components\ActiveQuery;
use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\ParcelCalculateException;
use common\components\order\history\OrderHistoryService;
use common\components\PaymentExchangeRateConverter;
use common\components\reject\RejectInterface;
use common\interfaces\DeliveryExpensiveInterface;
use common\models\Company;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\loggers\PreorderLogger;
use common\models\ModerLog;
use common\models\MsgFolder;
use common\models\MsgMember;
use common\models\MsgTopic;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\PaymentDetailOperation;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionRefund;
use common\models\Preorder;
use common\models\Ps;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderAttempDates;
use common\models\StoreOrderAttempDelivery;
use common\models\StoreOrderAttemptModeration;
use common\models\StoreOrderDelivery;
use common\models\StoreOrderDeliveryHistory;
use common\models\User;
use common\modules\browserPush\services\NotifyPopupService;
use common\modules\informer\InformerModule;
use common\modules\informer\models\EarningInformer;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use common\modules\payment\services\PaymentStoreOrderService;
use common\modules\promocode\components\PromocodeProcessor;
use common\modules\thingPrint\components\ThingiverseFacade;
use frontend\models\community\MessageFacade;
use frontend\models\ps\PsFacade;
use frontend\models\ps\StoreOrderAttempSearchForm;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\modules\preorder\components\PreorderEmailer;
use lib\delivery\delivery\DeliveryFacade;
use lib\delivery\delivery\DeliveryService;
use lib\delivery\delivery\models\DeliveryPostalLabel;
use lib\delivery\parcel\Parcel;
use lib\delivery\parcel\ParcelFactory;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use lib\payment\PaymentManagerFacade;
use yii\base\InvalidArgumentException;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\Url;

class StoreOrderService
{
    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @var DeliveryService
     */
    private $deliveryService;

    /**
     * @var PaymentManagerFacade
     */
    private $paymentManager;

    /**
     * @var OrderHistoryService
     */
    private $history;

    /**
     * @var PaymentExchangeRateConverter
     */
    private $moneyConvertor;

    /**
     * @var UserEmailLoginService
     */
    private $emailLoginService;

    /**
     * @var PreorderEmailer
     */
    protected $preorderEmailer;

    /**
     * @var PreorderLogger
     */
    protected $preorderLogger;

    /**
     * @var NotifyPopupService
     */
    protected $notifyPopupService;

    /**
     * @var PaymentStoreOrderService
     */
    protected $paymentStoreOrderService;


    public const DECLINE_SUCCESSFULL = 'success';

    public const DECLINE_WITHOUT_PAYMENT = 'withoutPayment';
    /**
     * @var Model3dService
     */
    private Model3dService $model3dService;

    /**
     * StoreOrderService constructor.
     *
     * @param Emailer $emailer
     * @param DeliveryService $deliveryService
     * @param PaymentManagerFacade $paymentManager
     * @param OrderHistoryService $history
     * @param PaymentExchangeRateConverter $moneyConvertor
     * @param UserEmailLoginService $emailLoginService
     * @param PreorderEmailer $preorderEmailer
     * @param PreorderLogger $preorderLogger
     * @param PaymentStoreOrderService $paymentStoreOrderService
     * @param NotifyPopupService $notifyPopupService
     */
    public function __construct(
        Emailer                      $emailer,
        DeliveryService              $deliveryService,
        PaymentManagerFacade         $paymentManager,
        OrderHistoryService          $history,
        PaymentExchangeRateConverter $moneyConvertor,
        UserEmailLoginService        $emailLoginService,
        PreorderEmailer              $preorderEmailer,
        PreorderLogger               $preorderLogger,
        PaymentStoreOrderService     $paymentStoreOrderService,
        NotifyPopupService           $notifyPopupService,
        Model3dService               $model3dService
    )
    {
        $this->emailer           = $emailer;
        $this->deliveryService   = $deliveryService;
        $this->paymentManager    = $paymentManager;
        $this->history           = $history;
        $this->moneyConvertor    = $moneyConvertor;
        $this->emailLoginService = $emailLoginService;

        $this->preorderEmailer          = $preorderEmailer;
        $this->preorderLogger           = $preorderLogger;
        $this->paymentStoreOrderService = $paymentStoreOrderService;
        $this->notifyPopupService       = $notifyPopupService;
        $this->model3dService           = $model3dService;
    }

    /**
     * Count all orders by status for print service user
     *
     * @param User $user - print service owner id
     * @return int[]
     */
    public function getOrdersCount(User $user): array
    {
        $counter = array_fill_keys(array_keys(StoreOrderAttemp::$companyStatusesGroups), ['count' => 0, 'informerCount' => 0]);

        $ps = PsFacade::getPsByUserId($user->id);
        if (!$ps) {
            return $counter;
        }

        $storeOrderAttempQuery = StoreOrderAttemp::find()
            ->select('`store_order_attemp`.`id` as id, ANY_VALUE(store_order_attemp.status) as status, ANY_VALUE(informer_store_order_attempt.count) as informer_count')
            ->wasPayedOrQuote()
            ->joinWith('informerStoreOrderAttempts')
            ->groupBy('store_order_attemp.id');
        if ($ps->isCustomerServiceCompany()) {
            $storeOrderAttempQuery->isInterceptionOrCompany($ps);
        } else {
            $storeOrderAttempQuery->isNotInterception()->forCompany($ps);
        }
        $storeOrderAttempSql   = $storeOrderAttempQuery
            ->createCommand()
            ->getRawSql();

        $ordersCountByStatus = \Yii::$app->getDb()->createCommand('SELECT a.status as status, count(a.id) as count, sum(a.informer_count) as informer_count_sum FROM (' . $storeOrderAttempSql . ') a GROUP BY a.status')->queryAll();
        $ordersCountByStatus = ArrayHelper::index($ordersCountByStatus, 'status');

        foreach (StoreOrderAttemp::$companyStatusesGroups as $groupStatus => $groupStatuses) {
            foreach ($groupStatuses as $status) {
                if (isset($ordersCountByStatus[$status])) {
                    $counter[$groupStatus]['count']         += $ordersCountByStatus[$status]['count'];
                    $counter[$groupStatus]['informerCount'] += $ordersCountByStatus[$status]['informer_count_sum'];
                }
            }
        }
        $counter['unpaid'] = 1;
        return $counter;
    }

    /**
     * @param Ps $ps
     * @return int
     */
    public function getNewPreordersCount(Ps $ps): int
    {
        $count = 0;
        $count += (int)Preorder::find()
            ->new()
            ->forPs($ps)
            ->count();

        return $count;
    }


    /**
     * Return count of order in production for PS
     *
     * @param Company $company
     * @return int
     */
    public static function getCompanyProductionOrdersCount(Company $company): int
    {
        return self::getCompanyProductionAttemptsQuery($company)->count();
    }

    /**
     * count all production and NEW status orders.
     *
     * @param User $user
     * @return int
     */
    public static function getCompanyProductionAndNewOrdersCount(User $user): int
    {
        $productionStatuses   = self::getAttempProductionStatuses();
        $productionStatuses[] = StoreOrderAttemp::STATUS_NEW;
        $res                  = StoreOrderAttemp::find()
            ->inStatus($productionStatuses)
            ->notTest()
            ->forPsUser($user);
        return $res->count();
    }

    /**
     * Return received orders count with claim period
     *
     * @param $user
     * @return int|string
     */
    public static function getCompanyAllowClaimOrdersCount($user)
    {
        return self::getCompanyAllowClaimAttemptsQuery($user)->count();
    }

    /**
     * Return count of order in production for Client
     *
     * @param User $user
     * @return int
     */
    public static function getClientProductionOrdersCount(User $user): int
    {
        return self::getClientProductionOrdersQuery($user)->count();
    }

    /**
     * Profit for production orders in USD
     *
     * @param Company $company
     * @return Money[]
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function getProductionOrdersPsProfit(Company $company): array
    {
        /** @var StoreOrderAttemp[] $attempts */
        $attempts = self::getCompanyProductionAttemptsQuery($company)->all();


        $moneysTotal = [];

        foreach ($attempts as $attempt) {
            $money = $this->paymentStoreOrderService->getProductionInvoicePsProfit($attempt);
            if (array_key_exists($money->getCurrency(), $moneysTotal)) {
                $moneysTotal[$money->getCurrency()] = MoneyMath::sum($moneysTotal[$money->getCurrency()], $money);
            } else {
                $moneysTotal[$money->getCurrency()] = $money;
            }
        }

        return $moneysTotal;
    }


    /**
     * used only for deleting.
     * we need to check order status new
     * used only to check if user can delete himself
     *
     * @return array
     */
    public static function getAttempProductionStatuses(): array
    {
        return [
            StoreOrderAttemp::STATUS_ACCEPTED,
            StoreOrderAttemp::STATUS_PRINTING,
            StoreOrderAttemp::STATUS_PRINTED,
            StoreOrderAttemp::STATUS_READY_SEND,
            StoreOrderAttemp::STATUS_SENT,
            StoreOrderAttemp::STATUS_DELIVERED
        ];
    }


    /**
     * @param Company $company
     * @return ActiveQuery
     */
    private static function getCompanyProductionAttemptsQuery(Company $company): ActiveQuery
    {
        $productionStatuses = self::getAttempProductionStatuses();

        $query = StoreOrderAttemp::find()
            ->inStatus($productionStatuses);
        if ($company->isCustomerServiceCompany()) {
            $query->isInterceptionOrCompany($company);
        } else {
            $query->isNotInterception()
            ->forCompany($company);
        }

        return $query;
    }

    /**
     * @param User $user
     * @return ActiveQuery
     */
    private static function getCompanyAllowClaimAttemptsQuery(User $user): ActiveQuery
    {
        $res = StoreOrderAttemp::find()
            ->inStatus([StoreOrderAttemp::STATUS_RECEIVED])
            ->notTest()
            ->andWhere(['>', 'store_order_attemp.created_at', DateHelper::subNowSec(StoreOrder::DISPUTE_ALLOWED_PERIOD)])
            ->forPsUser($user);
        return $res;
    }

    /**
     * @param User $user
     * @return ActiveQuery
     */
    private static function getClientProductionOrdersQuery(User $user): ActiveQuery
    {
        $res = StoreOrder::find()
            ->inProcess()
            ->forUser($user);
        return $res;
    }

    /**
     * @param User $user
     * @return int
     */
    public static function getNewOrdersCount(User $user)
    {
        $count = app('cache')->get('user' . $user->id . '.neworders', false);
        if ($count !== false) {
            return $count;
        }

        $ps = PsFacade::getPsByUserId($user->id);

        if (!$ps) {
            return 0;
        }

        $ordersCount = StoreOrderAttemp::find()
            ->forUndeletdMachinesOrForPs($ps)
            ->inStatus(StoreOrderAttemp::STATUS_NEW)
            ->notForPaymentStatuses([PaymentInvoice::STATUS_AUTHORIZED])
            ->count();

        $preordersCount = Preorder::find()
            ->new()
            ->forPs($ps)
            ->count();

        $count = $ordersCount + $preordersCount;

        app('cache')->set('user' . $user->id . '.neworders', $count, 60 * 3); // 2 minutes
        return $count;
    }

    /**
     * @param StoreOrder $order
     * @return null|string
     */
    public static function getOrderCountryIso(StoreOrder $order)
    {
        return $order->shipAddress ? $order->shipAddress->country->iso_code : null;
    }

    /**
     * is current payment requires processing status and settle payment before order is accepted?
     *
     * @param $storeOrder
     * @return bool
     */
    public static function isPaypalNeedPayment($storeOrder)
    {
        return PaymentInfoHelper::isPaypalPaid($storeOrder) && StoreOrderService::getOrderCountryIso($storeOrder) === 'IT';
    }


    /**
     * get store order attempt deadline
     *
     * @param StoreOrderAttemp $attempt
     * @return int
     */
    public static function getAttemptDeadline(StoreOrderAttemp $attempt)
    {
        $currentTime = DateHelper::addNowSec(60 + rand(0, 150));
        return $currentTime;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @throws BusinessException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \common\modules\thingPrint\components\ThingiverseApiException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function acceptAttemp(StoreOrderAttemp $attemp)
    {
        if ($attemp->isExpired()) {
            throw new BusinessException(_t('site.ps', 'The order has already been accepted by another print service'));
        }

        $this->tryCanChangeAttemp($attemp);

        $order          = $attemp->order;
        $isChangeAttemp = !$order->current_attemp_id;

        if (!$isChangeAttemp && $order->current_attemp_id != $attemp->id) {
            throw new BusinessException(_t('site.ps', 'The order has already been accepted by another print service.'));
        }

        if ($attemp->status != StoreOrderAttemp::STATUS_NEW) {
            throw new BusinessException(_t('site.ps', 'Order already accepted.'));
        }

        if ($order->isThingiverseOrder()) {
            ThingiverseFacade::tryValidateOrder($order);
        }

        $dbTransaction = app('db')->beginTransaction();
        try {
            // Always accepted one attempt stop other attempts
            /** @var StoreOrderAttemp[] $otherAttemps */
            $otherAttemps = StoreOrderAttemp::find()
                ->forOrder($order)
                ->other($attemp)
                ->inStatus(StoreOrderAttemp::STATUS_NEW)
                ->all();

            foreach ($otherAttemps as $otherAttemp) {
                $this->expireAttemp($otherAttemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
            }

            $attemp->status = $attemp->wasDownloaded() || $order->isForPreorder() ? StoreOrderAttemp::STATUS_PRINTING : StoreOrderAttemp::STATUS_ACCEPTED;
            $isUpdated      = StoreOrderAttemp::updateRow(['id' => $attemp->id, 'status' => StoreOrderAttemp::STATUS_NEW], ['status' => $attemp->status]);
            if (!$isUpdated) {
                throw new BusinessException(_t('site.ps', 'Order already accepted.'));
            }

            $attemp->dates->accepted_date   = new Expression('NOW()');
            $attemp->dates->plan_printed_at = $this->calculatePlanPrintedAt($order);

            if (empty($attemp->moderation)) {
                $moderation         = new StoreOrderAttemptModeration();
                $moderation->status = StoreOrderAttemptModeration::STATUS_NEW;
                $attemp->link('moderation', $moderation);
            } else {
                $attemp->moderation->status = StoreOrderAttemptModeration::STATUS_NEW;
                $attemp->moderation->save(false);
            }

            AssertHelper::assertSave($attemp);
            AssertHelper::assertSave($attemp->dates);

            $dbTransaction->commit();
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            throw $e;
        }

        if ($isChangeAttemp) {
            $order->link('currentAttemp', $attemp);
        }

        $this->history->logAcceptAttempt($attemp);

        if ($attemp->order->isOrderProduct()) {
            $attemp->moderation->status = StoreOrderAttemptModeration::STATUS_ACCEPTED;
            $attemp->moderation->save(false);
            $this->readyToSendAttemp($attemp);
            return;
        }
        if ($isChangeAttemp) {
            $order->storeOrderDelivery->delivery_type_id == DeliveryType::PICKUP_ID
                ? $this->emailer->sendClientPrintStartOfferedOrderPickup($order)
                : $this->emailer->sendClientPrintStartOfferedOrderDelivery($order);
        } else {
            if (!$order->preorder) {  // Several emails send then order is payed
                $this->emailer->sendClientPrintStartOrder($order);
            }
        }
        if (!$order->preorder && $order->allowedReceiveMessage()) { // Several emails send then order is payed
            $this->emailer->sendPsAcceptOrderAttemp($attemp);
            MessageFacade::sendOrderNotify($order, $attemp->ps->user, _t('site.ps', 'Hi, I accepted your order. If you have any changes to the order, please describe them in advance.'));
        }
        InformerModule::addCustomerOrderInformer($attemp->order);
        PsFacade::resetOrdersCount($attemp->ps->user);

        $this->paymentStoreOrderService->submitForSettleStoreOrderPayment($order);
    }

    public function changeCarrier(StoreOrder $storeOrder, string $newType, string $newCarrier)
    {
        $lastDelivery = $storeOrder->storeOrderDelivery;
        if (!$lastDelivery) {
            return;
        }
        if ($newType) {
            $oldType = $lastDelivery->deliveryType->code;
            $lastDelivery->setDeliveryTypeByCode($newType);
            $this->history->logChangeDeliveryType($storeOrder, $oldType, $newType);
            $lastDelivery->order->safeSave();
            if ($storeOrder->shipAddress->type != 'ship') {
                $storeOrder->shipAddress->type = 'ship';
                $storeOrder->shipAddress->safeSave();
            }
        }
        if ($newCarrier) {
            $psDeliveryDetails                 = $lastDelivery->ps_delivery_details;
            $oldCarrier                        = $psDeliveryDetails['carrier'];
            $psDeliveryDetails['carrier']      = $newCarrier;
            $lastDelivery->ps_delivery_details = $psDeliveryDetails;
            $this->history->logChangeCarrier($storeOrder, $oldCarrier, $newCarrier);
        }
        $lastDelivery->safeSave();
    }

    /**
     * @param StoreOrder $order
     * @return null|string
     * @todo refactor for use interfaces
     */
    private function calculatePlanPrintedAt(StoreOrder $order): ?string
    {
        if ($order->isFor3dPrinter()) {
            $printTimeInSeconds = PsFacade::getEstimatedPrintingTime($order->orderItem->model3dReplica) * $order->orderItem->qty * 3600;
            $days               = (int)max(min(14, ($printTimeInSeconds / 86400)), 1);
            return DateHelper::addNowSec($days * 86400);
        }

        if ($order->isForPreorder()) {
            /**
             * task # 6608. If you specified the run time "31231231231231" years or more, reset to 1000.
             * In the model "Preorder" added a validator.
             */
            if ($order->preorder->offer_estimate_time > 1000) {
                $order->preorder->offer_estimate_time = 1000;
            }

            $printTimeInSeconds = $order->preorder->offer_estimate_time * 86400;
            return DateHelper::addNowSec($printTimeInSeconds);
        }

        return null;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param int $initiator
     * @param RejectInterface $declineForm
     * @throws BusinessException
     */
    public function declineAttemp(StoreOrderAttemp $attemp, $initiator, RejectInterface $declineForm)
    {
        $this->tryCanChangeAttemp($attemp);

        if ($attemp->status == StoreOrderAttemp::STATUS_CANCELED) {
            throw new BusinessException(_t('site.ps', 'Offer already declined.'));
        }

        $order          = $attemp->order;
        $isChangeAttemp = !$order->current_attemp_id;

        if (!$isChangeAttemp && !$order->can_change_ps) {
            $this->declineOrder($order, $initiator, $declineForm);
            InformerModule::addCustomerOrderInformer($attemp->order);
            return;
        }

        $printRequest = StoreOrderAttemptProcess::create($attemp);
        if (!in_array($initiator, [ChangeOrderStatusEvent::INITIATOR_MODERATOR, ChangeOrderStatusEvent::INITIATOR_SYSTEM]) && !$printRequest->canDeclineOrder()) {
            throw new BusinessException(_t('site.ps', 'Can\'t decline order'));
        }

        $this->cancelAttemp($attemp, $initiator);
        $this->history->logDeclineAttempt($attemp, $declineForm);
        if (!$isChangeAttemp && $order->can_change_ps) {
            $order->unlink('currentAttemp', $attemp);
            $order->link('previousAttemp', $attemp);
            $this->history->logGoToChangeOffer($order);
            $this->emailer->sendClientPsDeclineNewOrder($order, $declineForm);
        }
        InformerModule::addCustomerOrderInformer($attemp->order);
        PsFacade::resetOrdersCount($attemp->ps->user);
    }

    /**
     * @param StoreOrder $order
     * @param CompanyService $machine
     * @param bool $isOffer
     * @return StoreOrderAttemp
     */
    public function createAttempForMachine(StoreOrder $order, CompanyService $machine, $isOffer)
    {
        $orderAttemp             = new StoreOrderAttemp();
        $orderAttemp->ps_id      = $machine->ps_id;
        $orderAttemp->machine_id = $machine->id;
        $orderAttemp->order_id   = $order->id;
        $orderAttemp->is_offer   = (int)$isOffer;
        $orderAttemp->status     = StoreOrderAttemp::STATUS_NEW;
        $orderAttemp->created_at = DateHelper::now();
        AssertHelper::assertSave($orderAttemp);

        // Increase orders count
        if ($machine->ps->psProgressOrdersCount) {
            $machine->ps->psProgressOrdersCount->progress_orders_count++;
            $machine->ps->psProgressOrdersCount->safeSave();
        }

        $dates            = new StoreOrderAttempDates();
        $dates->attemp_id = $orderAttemp->id;
        AssertHelper::assertSave($dates);
        $orderAttemp->link('dates', $dates);

        if ($order->hasDelivery()) {
            $attempDelivery = new StoreOrderAttempDelivery();
            $orderAttemp->link('delivery', $attempDelivery);
        }

        return $orderAttemp;
    }

    /**
     * @param Ps $company
     * @param StoreOrder|null $order
     * @param Preorder|null $preorder
     * @param bool $isOffer
     * @return StoreOrderAttemp
     */
    public function createAttemptForPs(Company $company, ?CompanyService $companyService = null, ?StoreOrder $order = null, ?Preorder $preorder = null, $isOffer = 0)
    {
        $orderAttemp              = new StoreOrderAttemp();
        $orderAttemp->ps_id       = $company->id;
        $orderAttemp->order_id    = $order?->id;
        $orderAttemp->preorder_id = $preorder?->id;
        $orderAttemp->machine_id  = $companyService?->id;
        $orderAttemp->is_offer    = (int)$isOffer;
        $orderAttemp->status      = $preorder ? StoreOrderAttemp::STATUS_QUOTE : StoreOrderAttemp::STATUS_NEW;
        $orderAttemp->created_at  = DateHelper::now();
        AssertHelper::assertSave($orderAttemp);

        $dates            = new StoreOrderAttempDates();
        $dates->attemp_id = $orderAttemp->id;
        AssertHelper::assertSave($dates);
        $orderAttemp->link('dates', $dates);

        if ($order && $order->hasDelivery()) {
            $attempDelivery = new StoreOrderAttempDelivery();
            $orderAttemp->link('delivery', $attempDelivery);
        }

        return $orderAttemp;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param int $initiator
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function cancelAttemp(StoreOrderAttemp $attemp, $initiator)
    {
        $attemp->status = StoreOrderAttemp::STATUS_CANCELED;
        $isUpdated      = StoreOrderAttemp::updateRow(['and', ['id' => $attemp->id], ['!=', 'status', $attemp->status]], ['status' => $attemp->status]);
        if (!$isUpdated) {
            throw new BusinessException(_t('site.ps', 'Order already accepted.'));
        }

        $attemp->cancel_initiator = $initiator;
        $attemp->cancelled_at     = DateHelper::now();
        AssertHelper::assertSave($attemp);
        PsFacade::resetOrdersCount($attemp->ps->user);
        if (!$attemp->order->user->getIsSystem() && $initiator === ChangeOrderStatusEvent::INITIATOR_PS && $attemp->order->allowedReceiveMessage()) {
            MessageFacade::sendOrderNotify($attemp->order, $attemp->ps->user, _t('site.ps', 'Sorry, I declined your order. Technical expert will try to find another option for you.'));
        }
    }

    /**
     * Mark attemp as expired
     *
     * @param StoreOrderAttemp $attemp
     * @param $initiator
     * @throws \common\components\exceptions\InvalidModelException
     */
    protected function expireAttemp(StoreOrderAttemp $attemp, $initiator)
    {
        $attemp->dates->expired_at = DateHelper::now();
        AssertHelper::assertSave($attemp->dates);
        $this->cancelAttemp($attemp, $initiator);
    }

    /**
     * @param StoreOrder $order
     * @param $initiator
     * @param \common\components\reject\RejectInterface $declineForm
     *
     * @return string
     * @throws BusinessException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\InvalidConfigException
     */
    public function declineOrder(StoreOrder $order, $initiator, RejectInterface $declineForm)
    {
        $returnResult = self::DECLINE_SUCCESSFULL;
        $this->tryCanChangeOrder($order);
        if ($order->getIsTestOrder()) {
            throw new BusinessException(_t('site.order', 'Test order can`t be canceled'));
        }

        if ($order->getPaymentStatus() == PaymentInvoice::STATUS_NEW) {
            $cancelPaymentResult = true;
        } else {
            if (!in_array($initiator, [ChangeOrderStatusEvent::INITIATOR_MODERATOR, ChangeOrderStatusEvent::INITIATOR_SYSTEM])
                && $order->currentAttemp && !StoreOrderAttemptProcess::create($order->currentAttemp)->canDeclineOrder()
            ) {
                throw new BusinessException(_t('site.ps', 'Can\'t decline order'));
            }

            $cancelPaymentResult = false;
            $paymentTransaction  = $this->paymentStoreOrderService->findOrderPaymentTransaction($order);

            if ($paymentTransaction && ($paymentTransaction->canBeCanceled() || $paymentTransaction->canBeRefunded()) &&
                (!$paymentTransaction->paymentTransactionRefund) && (!$paymentTransaction->paymentTransactionRefunds)) {
                if ($paymentTransaction->canBeRefunded()) {
                    $refundModel = new PaymentTransactionRefund();
                    $refundModel->initWithTransaction($paymentTransaction);
                    $refundModel->status  = PaymentTransactionRefund::STATUS_APPROVED;
                    $refundModel->comment = _t('site.payment', 'Cancel payment for new order #{orderId}', ['orderId' => $order->id]);
                    $refundModel->safeSave();
                }
                $cancelPaymentResult = $this->paymentStoreOrderService->cancelNewOrderPayment($order, $declineForm->getComment());
            }
        }

        if (!$cancelPaymentResult) {
            $returnResult = self::DECLINE_WITHOUT_PAYMENT;
        } else {
            if ($order->primaryPaymentInvoice) {
                $order->primaryPaymentInvoice->setNewStatus(PaymentInvoice::STATUS_CANCELED);
                $order->primaryPaymentInvoice->save(['status']);
            }
        }

        $attempStatus = $order->currentAttemp ? $order->currentAttemp->status : null;

        foreach ($order->attemps as $attemp) {
            if ($attemp->status !== StoreOrderAttemp::STATUS_CANCELED) {
                $this->cancelAttemp($attemp, $initiator);
            }
        }

        $this->history->logDeclineOrder($order, $declineForm, $initiator);

        $this->updateOrderState($order, StoreOrder::STATE_CLOSED);

        if ($initiator == ChangeOrderStatusEvent::INITIATOR_MODERATOR) {
            $this->emailer->sendClientModeratorDeclineOrder(
                $order,
                [
                    'reason'      => $declineForm->getReason(),
                    'description' => $declineForm->getComment()
                ]
            );
        }

        InformerModule::addCustomerOrderInformer($order);

        if ($initiator == ChangeOrderStatusEvent::INITIATOR_PS && $attempStatus == StoreOrderAttemp::STATUS_NEW) {
            $this->emailer->sendClientPsDeclineNewOrder($order, $declineForm);
        }

        if ($initiator == ChangeOrderStatusEvent::INITIATOR_PS && $attempStatus && $attempStatus != StoreOrderAttemp::STATUS_NEW) {
            $this->emailer->sendPsAcceptButCancel($order);
            $this->emailer->sendClientPsAcceptButCancel($order);
        }

        if ($initiator == ChangeOrderStatusEvent::INITIATOR_CLIENT && $order->isPayed() && $order->currentAttemp && $attempStatus == StoreOrderAttemp::STATUS_NEW) {
            $this->emailer->sendPsClientCancelNewOrder($order->currentAttemp, $declineForm);
        }

        if ($initiator == ChangeOrderStatusEvent::INITIATOR_MODERATOR && $order->currentAttemp && $attempStatus !== StoreOrderAttemp::STATUS_CANCELED) {
            $this->emailer->sendPsModeratorCancelOrder($order->currentAttemp, $declineForm);
        }
        return $returnResult;
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param int $initiator
     * @throws BusinessException
     */
    public function sendAttemp(StoreOrderAttemp $attemp, $initiator)
    {
        $this->tryCanChangeOrder($attemp->order);

        if (!$attemp->order->isOrderProduct() && DeliveryFacade::isCarrierPostDelivery($attemp->order) && !DeliveryPostalLabel::checkPostalLabelExists($attemp)) {
            throw new BusinessException(_t('site.ps', 'Please generate the postal label first'));
        }
        if (!$attemp->isReadySend()) {
            throw new BusinessException(_t('site.ps', 'This order is already dispatched'));
        }

        $isShipped = $attemp->dates->isShipped();

        $previousStatus            = $attemp->status;
        $attemp->status            = StoreOrderAttemp::STATUS_SENT;
        $attemp->dates->shipped_at = new Expression('NOW()');

        AssertHelper::assertSave($attemp);
        AssertHelper::assertSave($attemp->dates);

        $this->emailer->sendPsSentOrder($attemp);

        if ($isShipped && $attemp->delivery->isChangedTrack()) {
            $this->emailer->sendClientDeliveryTrackChanged($attemp->order);
            $this->emailer->sendSupportMessage(
                'Tracking number has been changed for order #' . $attemp->order_id,
                sprintf(
                    'The tracking number for order #%s has been changed from %s (old tracking number) to %s (new tracking number). 
                 Please check order and if required, refund unused shipping label on Easypost.',
                    Html::a($attemp->order_id, Url::to(['/store/store-order/view?id=' . $attemp->order_id], true)),
                    $attemp->delivery->getOldTrackingNumber(),
                    $attemp->delivery->tracking_number
                )
            );
        } else {
            $this->emailer->sendClientSentOrder($attemp->order);
        }

        $this->history->logSendAttempt($attemp, $previousStatus);

        if ($attemp->order->isThingiverseOrder()) {
            ThingiverseFacade::updateOrder($attemp->order, 1, null, null, $attemp->status);
        }
        InformerModule::addCustomerOrderInformer($attemp->order);
        if (DeliveryFacade::isPickupDelivery($attemp->order)) {
            if ($attemp->order->user_id == $attemp->ps->user_id) {
                $this->receivedAttemp($attemp, $initiator);
            } else {
                $this->deliveredAttemp($attemp, $initiator);
            }
        }
    }

    /**
     * @param $attemp
     * @param $initiator
     * @throws BusinessException
     */
    public function deliveredAttemp(StoreOrderAttemp $attemp, $initiator)
    {
        $this->tryCanChangeAttemp($attemp);

        $previousStatus              = $attemp->status;
        $attemp->status              = StoreOrderAttemp::STATUS_DELIVERED;
        $attemp->dates->delivered_at = new Expression('NOW()');
        AssertHelper::assertSave($attemp);
        AssertHelper::assertSave($attemp->dates);

        $this->history->logDeliveredAttempt($attemp, $previousStatus);
        InformerModule::addCustomerOrderInformer($attemp->order);
        if (in_array($initiator, [ChangeOrderStatusEvent::INITIATOR_SYSTEM, ChangeOrderStatusEvent::INITIATOR_MODERATOR, ChangeOrderStatusEvent::INITIATOR_PS])) {
            //$this->emailer->sendClientOrderIsDelivered($attemp->order);
            if (!$attemp->order->isThingiverseOrder()) {
                $link           = param('server') . sprintf('/workbench/order/view/%d?review=1', $attemp->order->id);
                $userEmailLogin = $this->emailLoginService->generateUserEmailLogin($attemp->order->user, $attemp->order->user->getEmail(), $link);
                $this->emailer->sendOrderReviewEmail($attemp->order, $userEmailLogin->getLoginLinkByHash());
            }
        }
    }

    /**
     * @param StoreOrderAttemp $attemp
     */
    public function printingAttemp(StoreOrderAttemp $attemp)
    {
        $this->tryCanChangeAttemp($attemp);

        $previousStatus = $attemp->status;

        $attemp->status                = StoreOrderAttemp::STATUS_PRINTING;
        $attemp->dates->start_print_at = new Expression('NOW()');
        AssertHelper::assertSave($attemp);
        AssertHelper::assertSave($attemp->dates);

        $this->history->logPrintingAttempt($attemp, $previousStatus);
    }

    public function downloadedFiles(StoreOrderAttemp $attemp)
    {
        $this->tryCanChangeAttemp($attemp);

        $attemp->dates->fact_printed_at = new Expression('NOW()');
        AssertHelper::assertSave($attemp);
        AssertHelper::assertSave($attemp->dates);

        $this->history->logComment($attemp->order, '', 'Downloaded files');
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @throws BusinessException
     */
    public function readyToSendAttemp(StoreOrderAttemp $attemp)
    {
        $this->tryCanChangeAttemp($attemp);

        if ($attemp->order->hasCuttingPack()) {

        }

        if (!$attemp->order->isForPreorder()) {
            AssertHelper::assert($attemp->moderation->status == StoreOrderAttemptModeration::STATUS_ACCEPTED);
        }

        $previousStatus                  = $attemp->status;
        $attemp->status                  = StoreOrderAttemp::STATUS_READY_SEND;
        $attemp->dates->ready_to_sent_at = new Expression('NOW()');
        AssertHelper::assertSave($attemp);
        AssertHelper::assertSave($attemp->dates);

        $this->history->logReadyToSent($attemp, $previousStatus);

        if (!$attemp->order->isForPreorder()) {
            // Update printed count of ordered 3d models
            foreach ($attemp->order->storeOrderItems as $orderItem) {
                if ($orderItem->unit) {
                    $orderItem->unit->printed_count++;
                    AssertHelper::assertSave($orderItem->unit);
                }
            }
        }

        InformerModule::addCustomerOrderInformer($attemp->order);
        $this->emailer->sendPsPrintedReviewedOrder($attemp);
        $this->emailer->sendClientPrintedOrder($attemp->order);
    }

    public function producedCuttingAttemp(StoreOrderAttemp $attemp, $initiator)
    {
        $this->tryCanChangeAttemp($attemp);

        if (!StoreOrderAttemptProcess::create($attemp)->canSetAsPrinted() || !$attemp->moderation->files) {
            throw new BusinessException(_t('site.ps', 'Please click on Order Details and upload photos to submit results.'));
        }

        $previousStatus                 = $attemp->status;
        $attemp->status                 = StoreOrderAttemp::STATUS_PRINTED;
        $attemp->dates->finish_print_at = new Expression('NOW()');
        $attemp->moderation->clearPreviousResult();
        AssertHelper::assertSave($attemp);
        AssertHelper::assertSave($attemp->dates);

        $this->history->logPrintedAttempt($attemp, $previousStatus);

        InformerModule::addCustomerOrderInformer($attemp->order);
        if ($attemp->order->user_id == $attemp->ps->user_id && ($attemp->order->hasDelivery() && DeliveryFacade::isPickupDelivery($attemp->order))) {
            $this->sendAttemp($attemp, $initiator);
        } else {
            $this->emailer->sendClientCanAcceptOrder($attemp->order);
        }

    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param $initiator
     * @throws BusinessException
     */
    public function printedAttemp(StoreOrderAttemp $attemp, $initiator)
    {
        $this->tryCanChangeAttemp($attemp);

        if (!StoreOrderAttemptProcess::create($attemp)->canSetAsPrinted() || !$attemp->moderation->files) {
            throw new BusinessException(_t('site.ps', 'Please click on Order Details and upload photos to submit results.'));
        }
        if (!($attemp->isAccepted() || $attemp->isPrinting())) {
            throw new BusinessException(_t('site.ps', 'This order is already pending moderation.'));
        }

        $previousStatus                 = $attemp->status;
        $attemp->status                 = StoreOrderAttemp::STATUS_PRINTED;
        $attemp->dates->finish_print_at = new Expression('NOW()');
        $attemp->moderation->clearPreviousResult();
        AssertHelper::assertSave($attemp);
        AssertHelper::assertSave($attemp->dates);

        $this->history->logPrintedAttempt($attemp, $previousStatus);

        InformerModule::addCustomerOrderInformer($attemp->order);
        if ($attemp->order->user_id == $attemp->ps->user_id && ($attemp->order->hasDelivery() && DeliveryFacade::isPickupDelivery($attemp->order))) {
            $this->sendAttemp($attemp, $initiator);
        } else {
            $this->emailer->sendClientCanAcceptOrder($attemp->order);
        }
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param $initiator
     *
     * @throws BusinessException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \lib\payment\exception\PaymentManagerSystemException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     * @throws \yii\db\Exception
     */
    public function receivedAttemp(StoreOrderAttemp $attemp, $initiator)
    {
        //$transaction = \Yii::$app->db->beginTransaction();
        $this->tryCanChangeAttemp($attemp);
        if ($attemp->isResolved()) {
            throw new BusinessException(_t('site.ps', 'This order is already complete'));
        }

        $previousStatus             = $attemp->status;
        $attemp->status             = StoreOrderAttemp::STATUS_RECEIVED;
        $attemp->dates->received_at = DateHelper::now();
        AssertHelper::assertSave($attemp);
        AssertHelper::assertSave($attemp->dates);

        $this->history->logReceived($attemp, $previousStatus);

        $this->updateOrderState($attemp->order, StoreOrder::STATE_COMPLETED);

        if ($attemp->order->hasCuttingPack()) {
            $this->paymentStoreOrderService->receivedOrderCuttingPack($attemp);
        } elseif ($attemp->order->isForPreorder()) {
            $this->paymentStoreOrderService->receivedOrderByPreorderPayment($attemp);
        } else {
            $this->paymentStoreOrderService->receivedOrderPrint3dPayment($attemp);
        }

        // success printed test prder
        if ($attemp->order->getIsTestOrder()) {
            $this->emailer->sendPsSuccessTestOrder($attemp->ps);
        } else {
            $this->emailer->sendPsReceivedOrder($attemp);
            InformerModule::addInformer($attemp->ps->user, EarningInformer::class);
            // InformerModule::addCompanyOrderInformer($attemp);
        }

        /*
         * move the topic from "Service orders" to "Completed"
         * after the order is completed
         */
        $serviceOrderTopicId = MsgTopic::find()->where([
            'bind_to' => MsgTopic::BIND_OBJECT_ORDER,
            'bind_id' => $attemp->order_id
        ])->scalar();

        if ($serviceOrderTopicId) {
            $psUserId = $attemp->ps->user_id;

            $msgMember = MsgMember::findOne([
                'topic_id' => $serviceOrderTopicId,
                'user_id'  => $psUserId
            ]);

            if ($msgMember) {
                $msgMember->folder_id = MsgFolder::FOLDER_ID_COMPLETED;
                $msgMember->save(true, ['folder_id']);
            }
        }
    }

    /**
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     * @throws \lib\payment\exception\PaymentManagerException
     * @throws \lib\payment\exception\PaymentManagerSystemException
     * @throws \yii\db\Exception
     * @throws \yii\base\Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws BusinessException
     */
    public function receivedAttempTransaction(StoreOrderAttemp $attemp, $initiator)
    {
        $dbtr = \Yii::$app->db->beginTransaction();
        try {
            $this->receivedAttemp($attemp, $initiator);
            $dbtr->commit();
        } catch (\Exception $ex) {
            $dbtr->rollBack();
            logException($ex, 'tsdebug');
            throw  $ex;
        }

    }

    /**
     * @param StoreOrderAttemp $attemp
     * @param null|Parcel $parcel
     * @param bool $checkCan
     * @throws BusinessException
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \lib\delivery\exceptions\UserDeliveryException
     * @throws \yii\console\Exception
     */
    public function buyDeliveryAndLoadPostalImage(StoreOrderAttemp $attemp, Parcel $parcel = null, $checkCan = true)
    {
        if (DeliveryPostalLabel::checkPostalLabelExists($attemp)) {
            return;
        }
        $this->tryCanChangeAttemp($attemp);
        if ($checkCan && !StoreOrderAttemptProcess::create($attemp)->canGetPostalLabel()) {
            throw new BusinessException(_t('site.ps', 'Error while receiving a postal label'));
        }
        $calculatedParcel = ParcelFactory::createFromOrder($attemp->order);
        $this->saveCalulcatedParcel($attemp->order, $calculatedParcel);

        $parcel = $parcel ?: $calculatedParcel;
        $rate   = $this->deliveryService->getCarrierRateForOrderAttemp($attemp, $parcel);

        if (!$rate) {
            throw new BusinessException(_t('site.ps', 'Shipping rate not found for this order.'));
        }

        $shipping          = $attemp->order->primaryPaymentInvoice->storeOrderAmount->getAmountShipping();
        $deliveryExpensive = \Yii::createObject(DeliveryExpensiveInterface::class);
        if ($shipping && $deliveryExpensive->check($rate, $shipping)) {
            $this->saveHistoryOrderParcel($attemp->order, $parcel);
            throw new ParcelCalculateException($attemp->order->id, _t('site.ps', 'The cost of the parcel exceeds that paid by the client.'));
        }
        $buyResult = $this->deliveryService->buyCarrierShippment($rate);

        $usdRate = Money::create($rate->rate, Currency::USD);
        $this->paymentStoreOrderService->payEasypostForDelivery($attemp, $usdRate);

        $attemp->delivery->parcel_width  = $buyResult->getParcel()->width;
        $attemp->delivery->parcel_height = $buyResult->getParcel()->height;
        $attemp->delivery->parcel_length = $buyResult->getParcel()->length;
        $attemp->delivery->parcel_weight = $buyResult->getParcel()->weight;
        $attemp->delivery->label_url     = $buyResult->getLabelUrl();
        $attemp->delivery->public_url    = $buyResult->getPublicUrl();
        AssertHelper::assertSave($attemp->delivery);

        DeliveryPostalLabel::downloadPostalLabelFile(
            $attemp->ps->user->id,
            $attemp,
            $buyResult->getLabelUrl(),
            $buyResult->getCarrier(),
            $buyResult->getTrackingCode()
        );
    }

    public function switchDispute(StoreOrder $storeOrder)
    {
        $storeOrder->is_dispute_open = (int)!$storeOrder->is_dispute_open;
        $storeOrder->safeSave();
        if ($storeOrder->currentAttemp && !$storeOrder->currentAttemp->isNew() && $storeOrder->isPayed()) {
            if ($storeOrder->is_dispute_open) {
                $this->emailer->sendPsOpenDispute($storeOrder->currentAttemp);
            } else {
                $this->emailer->sendPsCloseDispute($storeOrder->currentAttemp);
            }
        }
        $this->history->logSwitchDispute($storeOrder);
    }

    /**
     * @param StoreOrder $storeOrder
     * @param string $comment
     */
    public function saveComment(StoreOrder $storeOrder, string $comment)
    {
        $oldComment = '';
        $moderLog   = ModerLog::findOne(['object_id' => $storeOrder->id, 'action' => 'order_cmt', 'object_type' => ModerLog::TYPE_ORDER]);
        if ($moderLog) {
            $oldComment            = $moderLog->result;
            $moderLog->result      = $comment;
            $moderLog->finished_at = dbexpr('NOW()');
            $moderLog->safeSave();
        } else {
            ModerLog::addRecord(
                [
                    'action'      => 'order_cmt',
                    'object_type' => ModerLog::TYPE_ORDER,
                    'object_id'   => $storeOrder->id,
                    'result'      => $comment
                ]
            );
        }
        $this->history->logComment($storeOrder, $oldComment, $comment);
    }

    public function saveCalulcatedParcel(StoreOrder $order, Parcel $parcel)
    {
        return StoreOrderDelivery::updateRow(
            [
                'order_id' => $order->id,
            ],
            [
                'calculated_parcel_width'   => $parcel->width,
                'calculated_parcel_height'  => $parcel->height,
                'calculated_parcel_length'  => $parcel->length,
                'calculated_parcel_weight'  => $parcel->weight,
                'calculated_parcel_measure' => 'mm',
            ]
        );
    }

    public function saveHistoryOrderParcel(StoreOrder $order, Parcel $parcel)
    {
        $history = StoreOrderDeliveryHistory::findOne(['order_id' => $order->id]);
        if ($history) {
            $history->editParcel($parcel);
        } else {
            $history = StoreOrderDeliveryHistory::createWithParcel($order, $parcel);
        }
        $history->safeSave();
    }

    /**
     * @param StoreOrderAttemp $attemp
     * @throws BusinessException
     */
    protected function tryCanChangeAttemp(StoreOrderAttemp $attemp)
    {
        $this->tryCanChangeOrder($attemp->order);

        if ($attemp->isResolved()) {
            throw new BusinessException(_t('site.order', 'This order has been accepted by another print service.'));
        }
    }

    /**
     * @param StoreOrder $order
     * @throws BusinessException
     */
    protected function tryCanChangeOrder(StoreOrder $order)
    {
        if ($order->order_state === StoreOrder::STATE_CLOSED || $order->order_state === StoreOrder::STATE_REMOVED) {
            throw new BusinessException(_t('site.order', 'Order status is closed, no updates can be done'));
        }
    }

    /**
     * @param StoreOrder $order
     * @param string $state
     */
    protected function updateOrderState(StoreOrder $order, $state)
    {
        $order->order_state = $state;
        AssertHelper::assertSave($order);
    }

    public function clientDislike(StoreOrderAttemp $currentAttemp)
    {
        $this->emailer->sendPsClientDislike($currentAttemp);
    }

    /**
     * @param StoreOrderAttemp $attempt
     * @param $oldShipper
     * @param $oldNumber
     * @param $newShipper
     * @param $newNumber
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function logChangeTracking(StoreOrderAttemp $attempt, $oldShipper, $oldNumber, $newShipper, $newNumber): void
    {
        $this->history->logChangeTracking($attempt, $oldShipper, $oldNumber, $newShipper, $newNumber);
    }

    /**
     * @param StoreOrderAttemp $attempt
     *
     * @return StoreOrderAttemp
     */
    public function createAttempDelivery(StoreOrderAttemp $attempt): StoreOrderAttemp
    {
        if (!$attempt->delivery) {
            $attempt->link('delivery', new StoreOrderAttempDelivery());
        }

        return $attempt;
    }

    public static function getGroupCodeByStatus($status)
    {
        $map = [
            ''                             => 'new',
            StoreOrderAttemp::STATUS_NEW   => 'new',
            StoreOrderAttemp::STATUS_QUOTE => 'new',

            StoreOrderAttemp::STATUS_ACCEPTED   => 'production',
            StoreOrderAttemp::STATUS_PRINTED    => 'production',
            StoreOrderAttemp::STATUS_READY_SEND => 'production',
            StoreOrderAttemp::STATUS_PRINTING   => 'production',

            StoreOrderAttemp::STATUS_SENT      => 'dispatched',
            StoreOrderAttemp::STATUS_DELIVERED => 'delivered',
            StoreOrderAttemp::STATUS_RECEIVED  => 'completed',

            StoreOrderAttemp::STATUS_CANCELED => 'canceled',
        ];
        if (array_key_exists($status, $map)) {
            return $map[$status];
        }
        return '';
    }

    public static function getGroupCodeByOrderStatus($status)
    {
        $map = [
            StoreOrder::STATE_PROCESSING . '_'                                  => 'new',
            StoreOrder::STATE_PROCESSING . '_' . StoreOrderAttemp::STATUS_NEW   => 'new',
            StoreOrder::STATE_PROCESSING . '_' . StoreOrderAttemp::STATUS_QUOTE => 'new',

            StoreOrder::STATE_PROCESSING . '_' . StoreOrderAttemp::STATUS_ACCEPTED   => 'production',
            StoreOrder::STATE_PROCESSING . '_' . StoreOrderAttemp::STATUS_PRINTED    => 'production',
            StoreOrder::STATE_PROCESSING . '_' . StoreOrderAttemp::STATUS_READY_SEND => 'production',
            StoreOrder::STATE_PROCESSING . '_' . StoreOrderAttemp::STATUS_PRINTING   => 'production',

            StoreOrder::STATE_PROCESSING . '_' . StoreOrderAttemp::STATUS_SENT      => 'dispatched',
            StoreOrder::STATE_PROCESSING . '_' . StoreOrderAttemp::STATUS_DELIVERED => 'delivered',
            StoreOrder::STATE_COMPLETED . '_'                                       => 'completed',
            StoreOrder::STATE_COMPLETED . '_' . StoreOrderAttemp::STATUS_RECEIVED   => 'completed',


            StoreOrder::STATE_CLOSED . '_'                                         => 'canceled',
            StoreOrder::STATE_CANCELED . '_'                                       => 'canceled',
            StoreOrder::STATE_CLOSED . '_' . StoreOrderAttemp::STATUS_CANCELED     => 'canceled',
            StoreOrder::STATE_COMPLETED . '_' . StoreOrderAttemp::STATUS_CANCELED  => 'canceled',
            StoreOrder::STATE_PROCESSING . '_' . StoreOrderAttemp::STATUS_CANCELED => 'canceled',
            StoreOrder::STATE_CANCELED . '_' . StoreOrderAttemp::STATUS_CANCELED   => 'canceled',
            '_' . StoreOrderAttemp::STATUS_CANCELED     => 'canceled'

        ];
        if (array_key_exists($status, $map)) {
            return $map[$status];
        }
        throw new  InvalidArgumentException('Not exists status: ' . $status);
    }

    /**
     * @param StoreOrder $storeOrder
     * @param PaymentDetailOperation $paymentOperation
     * @throws BusinessException
     * @throws \yii\base\InvalidConfigException
     */
    public function setPayed(StoreOrder $storeOrder, PaymentDetailOperation $paymentOperation)
    {
        $storeOrder->savePayment($paymentOperation);

        /** @var PromocodeProcessor $promocodeProcessor */
        $promocodeProcessor = \Yii::createObject(PromocodeProcessor::class);
        $promocodeProcessor->markAsUsed($storeOrder->getPrimaryInvoice());

        PsFacade::resetOrdersCount($storeOrder->currentAttemp->ps->user);
        // if cnc order - accept it
        if ($storeOrder->isForPreorder()) {
            $storeOrder->preorder->offer_accepted_at = DateHelper::now();
            $this->acceptAttemp($storeOrder->currentAttemp);
            $this->preorderEmailer->psCustomerAcceptOffer($storeOrder->preorder);
            $this->preorderLogger->log($storeOrder->preorder);
            $storeOrder->preorder->safeSave();

            InformerModule::addCompanyOrderInformer($storeOrder->currentAttemp);
        } else {
            // Change created at for printing service for change expire date
            $storeOrder->currentAttemp->created_at = DateHelper::now();
            $storeOrder->currentAttemp->safeSave();
        }
        InformerModule::addCompanyOrderInformer($storeOrder->currentAttemp);
        $this->notifyPopupService->notifyNewOrder($storeOrder->currentAttemp);
    }

    public function payEasypostForDelivery()
    {

    }

    public function getUnpaidOrders(User $user)
    {
        $form = new StoreOrderAttempSearchForm($user);
        $form->setAttributes(['statusGroup' => 'unpaid']);
        $attemps = $form->getOrdersQuery();
        $attemps = $attemps->all();
        return count($attemps);
    }

    /**
     * @param User $user
     * @throws BusinessException
     */
    public function checkActiveOrder(User $user): void
    {
        $openOrdersCount       = static::getClientProductionOrdersCount($user) + static::getCompanyProductionAndNewOrdersCount($user);
        $allowClaimOrdersCount = static::getCompanyAllowClaimOrdersCount($user);

        if ($openOrdersCount) {
            throw new BusinessException(_t('user.profile', 'You have incomplete orders'));
        }

        if ($allowClaimOrdersCount) {
            throw new BusinessException(_t('user.profile', 'You have orders still awaiting the completion of the dispute periods.'));
        }

        if ($user->company) {
            $activePreordersCount = Preorder::find()->forPs($user->company)->newOrWaitConfirm()->count();
            if ($activePreordersCount) {
                throw new BusinessException(_t('user.profile', 'You have incomplete quotes'));
            }
        }
    }
}