<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.08.16
 * Time: 15:50
 */

namespace common\services;

use common\components\ArrayHelper;
use common\models\Model3d;
use console\jobs\model3d\AntivirusJob;
use frontend\models\model3d\Model3dFacade;

class AntiVirusService
{

    /**
     * Check if antivirus check this model files and has no errors
     * If antivirus job not found - throws an error
     * If antivirus job found and virus detected - throws an error
     * If antivirus job found and status is not 'complete' - throws an error
     *
     * @param Model3d $model3d
     * @throws \yii\base\NotSupportedException
     */
    public static function startAntiVirusCheck(Model3d $model3d)
    {
        $fileIds = ArrayHelper::merge(
            \lib\collection\CollectionDb::getColumn(Model3dFacade::getActive($model3d->model3dParts), 'file_id'),
            \lib\collection\CollectionDb::getColumn($model3d->model3dImgs, 'file_id')
        );

        // let's find antivirus file job
        $filter = ['file_id' => $fileIds, 'operation' => 'antivirus', 'status' => 'complete'];
        /**
         * @var \common\models\FileJob $antivirusJobRow
         */
        $antivirusJobRow = \common\models\FileJob::findOne($filter);
        // if completed not found, check others
        if ($antivirusJobRow == null) {
            $filter = ['file_id' => $fileIds, 'operation' => 'antivirus'];
            $antivirusJobRow = \common\models\FileJob::findByPk($filter);
        }

        if (empty($antivirusJobRow)) {
            throw new \yii\base\NotSupportedException(
                'Run antivirus check, before publishing'
            );
        }

        $antInfo = AntivirusJob::getInfo($antivirusJobRow);
        if ($antInfo['infected'] == 0 && $antInfo['status'] == 'complete') {
        } else {
            throw new \yii\base\NotSupportedException(
                sprintf(
                    "Cannot publish. Antivirus result infected <b>%s</b> status <b>%s</b>",
                    $antInfo['infected'],
                    $antInfo['status']
                )
            );
        }
    }
}