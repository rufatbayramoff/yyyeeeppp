<?php


namespace common\services;


use common\components\exceptions\BusinessException;
use common\components\FileTypesHelper;
use common\interfaces\Model3dBasePartInterface;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\Model3d;
use frontend\models\ps\PsFacade;
use yii\web\UploadedFile;

class Model3dFileService
{
    /**
     * @var Model3dService
     */
    private $model3dService;
    /**
     * @var FileFactory
     */
    private $fileFactory;
    /**
     * @var Model3dImageService
     */
    private $model3dImageService;


    public function __construct(
        Model3dService $model3dService,
        FileFactory $fileFactory,
        Model3dImageService $model3dImageService
    )
    {

        $this->model3dService = $model3dService;
        $this->fileFactory = $fileFactory;
        $this->model3dImageService = $model3dImageService;
    }

    public function archive(int $id, UploadedFile $file)
    {
        /** @var Model3d $model3d */
        $model3d = Model3d::findByPk($id);
        $this->model3dService->tryCanBeManagedByCurrentUser($model3d);
        $model3d->tryValidateInactive();
        /** @var File $file */
        $file = $this->fileFactory->createFileFromUploadedFile($file);
        $this->model3dService->addFile($model3d, $file, true);
        $this->model3dService->updateProductPublishStatus($model3d);
        return [
            'success' => true,
            'message' => 'reload'
        ];
    }

    public function file(int $id, UploadedFile $uploadFile): array
    {
        /** @var Model3d $model3d */
        $model3d = Model3d::findByPk($id);
        $this->model3dService->tryCanBeManagedByCurrentUser($model3d);
        $model3d->tryValidateInactive();

        $file        = $this->fileFactory->createFileFromUploadedFile($uploadFile);
        if (FileTypesHelper::isFileInExt($file, [Model3dBasePartInterface::FORMAT_3MF]) && ($file->size>Model3dBasePartInterface::MAX_3MF_SIZE)) {
            throw new BusinessException('Your file size is too big');
        }
        $model3dPart = Model3dPartService::addModel3dPartFileAndSave($model3d, $file);
        $this->model3dService->updateProductPublishStatus($model3d);


        return [
            'model3did'     => $model3d->id,
            'modelpartid' => $model3dPart->id ?? null,
            'success'     => $model3dPart ? true : false,
            'message'     => 'File uploaded',
            'fileId'      => $model3dPart ? $model3dPart->file->id : null,
            'fileUrl'     => '/static/images/3dicon.png',
            'result'      => [
                'file_id'   => $model3dPart ? $model3dPart->file->id : null,
                'part_id'   => $model3dPart->id ?? null,
                'fileTitle' => $model3dPart->name ?? null,
            ],
            'max'         => PsFacade::getMaxBuildVolume()->__toString(),
            'sizes'       => _t('site.model3d', 'Calculating...'),
            'size_valid'  => true,
            'fileExt'     => $model3dPart ? $model3dPart->file->extension : null,
            'md5sum'      => $model3dPart ? $model3dPart->file->md5sum : null,
        ];
    }

    public function screen(int $id, UploadedFile $uploadFile)
    {
        /** @var Model3d $model3d */
        $model3d = Model3d::tryFindByPk($id);
        $this->model3dService->tryCanBeManagedByCurrentUser($model3d);
        $file         = $this->fileFactory->createFileFromUploadedFile($uploadFile);
        $model3dImage = $this->model3dImageService->addModel3dImgFileAndSave($model3d, $file);
        $this->model3dService->updateProductPublishStatus($model3d);

        return [
            'coverId'  => $model3d->coverFile->id,
            'success'  => true,
            'message'  => _t('front.upload', 'File uploaded'),
            'fileUrl'  => $file->getFileUrl(),
            'id'       => $model3dImage->id,
            'fileId'   => $file->id,
            'filename' => $file->getFileName()
        ];
    }
}