<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.08.16
 * Time: 17:10
 */

namespace common\services;

use common\components\DateHelper;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\factories\Model3dPartFactory;
use common\models\factories\Model3dPartPropertiesFactory;
use common\models\File;
use common\models\FileJob;
use common\models\Model3dPartProperties;
use common\models\PrinterColor;
use common\models\repositories\FileRepository;
use common\modules\model3dRender\models\RenderFileParams;
use console\jobs\model3d\ConverterJob;
use console\jobs\model3d\ParserJob;
use console\jobs\model3d\RenderJob;
use console\jobs\QueueGateway;
use frontend\models\model3d\Model3dFacade;
use Yii;
use yii\helpers\Json;

class Model3dPartService
{
    /**
     *
     * @param Model3dBasePartInterface $model3dPart
     * @param bool $withQty
     * @return string
     */
    public static function getModel3dPartCaption(Model3dBasePartInterface $model3dPart, $withQty = false)
    {
        $size = $model3dPart->getSize();
        if (!$size) {
            return ParserJob::isFailed($model3dPart->file)
                ? _t('site.model3d', 'A calculating error occurred for this model.')
                : _t('site.model3d', 'Calculating...');
        }
        $result = _t('site.store', 'Size') . ': ' . $size->toStringWithMeasurement();
        if ($withQty) {
            $result = $result . '<span class="qty-badge" style="margin:10px;"> x ' . $model3dPart->qty . '</span>';
        }
        return $result;
    }

    /**
     * @param Model3dBasePartInterface $model3dPart
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function getModel3dPartRenderPath(Model3dBasePartInterface $model3dPart)
    {
        /** @var RenderFileParams $renderParams */
        $renderParams = Yii::createObject(
            [
                'class' => RenderFileParams::class,
                'pb'    => $model3dPart->model3d->getCadFlag()
            ]
        );
        $renderParams->color = 'ff0000';
        if ($model3dPart->isMulticolorFormat()) {
            $renderParams->color = PrinterColor::MULTICOLOR;
        }
        $file = $model3dPart->getOriginalModel3dPartObj()->file;
        return Yii::$app->getModule('model3dRender')->renderer->getRenderImagePath($file, $renderParams);
    }

    /**
     * @param Model3dBasePartInterface $model3dPart
     * @param string|null $colorCode
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function getModel3dPartRenderUrl(Model3dBasePartInterface $model3dPart, $colorCode = null)
    {
        /** @var RenderFileParams $renderParams */
        $renderParams = Yii::createObject(
            [
                'class' => RenderFileParams::class,
                'pb'    => $model3dPart->model3d->getCadFlag()
            ]
        );
        $renderParams->color = $colorCode;
        if (!$colorCode) {
            $model3dPartPrinterColor = $model3dPart->getCalculatedPrintableTexture();
            if ($model3dPartPrinterColor->printerColor->isMulticolor()) {
                if ($model3dPart->isMulticolorFormat()) {
                    $renderParams->color = PrinterColor::MULTICOLOR;
                } else {
                    $renderParams->color = 'ffffff';
                }
            } else {
                $renderParams->color = $model3dPartPrinterColor->printerColor->getRgbHex();
            }
        } else {
            if ($colorCode == PrinterColor::MULTICOLOR && !$model3dPart->isMulticolorFormat()) {
                $renderParams->color = 'ffffff';
            }
        }
        if ($renderParams->color === 'ffffff') {
            // set up ambient
            $renderParams->ambient = PrinterColor::WHITE_COLOR_AMBIENT;
        }
        $file = $model3dPart->getOriginalModel3dPartObj()->file;
        return Yii::$app->getModule('model3dRender')->renderer->getRenderImageUrl($file, $renderParams);
    }


    /**
     * Check if render exists for current model,
     * if render with color prefix not exists - create new render job
     *
     * @param Model3dBasePartInterface $model3dPart
     * @return bool
     * @throws \Exception
     */
    public static function checkAndRunFileRender(Model3dBasePartInterface $model3dPart)
    {

        $fullPath = Model3dPartService::getModel3dPartRenderPath($model3dPart);
        if (file_exists($fullPath)) {
            return false;
        }
        if (app('db')->getTransaction() && app('db')->getTransaction()->getIsActive()) {
            return true;
        }

        if (!file_exists($fullPath)) {
            $rJob = RenderJob::create($model3dPart->file);
            QueueGateway::addFileJob($rJob);
            return true;
        }
        if (!$model3dPart->getSize() && ParserJob::isCanRun($model3dPart->file)) {
            // Recalc size again
            $pJob = ParserJob::create($model3dPart->file);
            QueueGateway::addFileJob($pJob);
        }
        return false;
    }

    /**
     * get file render
     *
     * @param Model3dBasePartInterface $model3dPart
     * @param null $color
     * @return null|string
     */
    public static function getModel3dPartRenderUrlIfExists($model3dPart, $color = null)
    {
        $path = Model3dPartService::getModel3dPartRenderPath($model3dPart);
        if (file_exists($path)) {
            return Model3dPartService::getModel3dPartRenderUrl($model3dPart, $color);

        }
        return null;
    }

    public static function addModel3dPartFile(Model3dBaseInterface $model3d, File $file)
    {
        /** @var Model3dPartFactory $model3dPartFactory */
        $model3dPartFactory = Yii::createObject(Model3dPartFactory::class);
        $model3dPart = $model3dPartFactory->createModel3dPart($model3d, $file);
        if ($model3dPart->model3dTexture && !$model3d->getActiveModel3dParts()) {
            // Set texture for all model
            $model3d->setKitTexture($model3dPart->model3dTexture);
        }

        $currentParts = $model3d->model3dParts;
        $currentParts[] = $model3dPart;
        $model3d->setModel3dParts($currentParts);
        if (!$model3d->title) {
            $title = $file->getFileNameWithoutExtension();
            if (mb_strlen($title) > 45) {
                $title = mb_substr($title, 0, 42) . '...';
            }
            $model3d->title = $title;
        }
        return $model3dPart;
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param File $file
     * @return \common\models\Model3dPart|null
     * @throws \Exception
     * @throws \console\jobs\JobException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public static function addModel3dPartFileAndSave(Model3dBaseInterface $model3d, File $file)
    {
        $model3dPart = self::addModel3dPartSave($model3d, $file);

        Model3dFacade::createQueueJobs($file, ['model3d_id' => $model3d->id]);
        if (Yii::$app->getModule('convert')->converter->isConvertablePart($model3dPart)) {
            $job = ConverterJob::createByPart($model3dPart);
            QueueGateway::addFileJob($job);
        } else {
            Model3dFacade::createQueueJobs($file, ['model3d_id' => $model3d->id]);
        }
        return $model3dPart;
    }


    /*
     *
     * ----  For refractor ------
     *
     *
     */


    /**
     * Get model filement from db, or from g-code file if already exists
     *
     *
     * @param  Model3dBasePartInterface $model3dPart
     * @return Model3dPartProperties
     */
    public static function getModel3dPartInfo(Model3dBasePartInterface $model3dPart)
    {
        $modelFileProperties = $model3dPart->model3dPartProperties;
        if ($modelFileProperties && (float)$modelFileProperties->volume > 0 && (float)$modelFileProperties->area > 0) {
            return $modelFileProperties;
        }
        $fileId = $model3dPart->file->id;
        // try to get parser info for volume
        $parserInf = FileJob::findOne(
            [
                'file_id'   => $fileId,
                'operation' => FileJob::JOB_PARSER,
                'status'    => QueueGateway::JOB_STATUS_COMPLETED
            ]
        );
        try {
            if ($parserInf) {
                $resultJson = Json::decode($parserInf->result);
            } else {
                return false;
            }
            $resultJson['volume'] = abs($resultJson['volume']);
            if (!empty($resultJson['area'])) {
                $resultJson['area'] = abs($resultJson['area']);
                Model3dPartProperties::updateRow(
                    [
                        'id' => $model3dPart->model3dPartProperties->id
                    ],
                    [
                        'volume'          => $resultJson['volume'],
                        'area'            => $resultJson['area'],
                        'supports_volume' => $resultJson['supports_volume'],
                        'updated_at'      => dbexpr('NOW()')
                    ]
                );
                $modelFileProperties->volume = $resultJson['volume'];
                $modelFileProperties->area = $resultJson['area'];
                $modelFileProperties->supports_volume = $resultJson['supports_volume'];
            }
        } catch (\Exception $e) {
            return false;
        }

        return $modelFileProperties;
    }

    /**
     * bind parser info if has, to model3d_file
     *
     * @param $model3dPart
     * @return Model3dPartProperties
     * @throws \Exception
     */
    public static function getParserInfo(Model3dBasePartInterface $model3dPart)
    {
        if ($model3dPart->model3dPartProperties) {
            return
                $model3dPart->model3dPartProperties;
        }
        $fileId = $model3dPart->file->id;

        /**
         * @var \common\models\FileJob $parserJob
         */
        $parserJob = FileJob::find()
            ->where(['file_id' => $fileId, 'operation' => FileJob::JOB_PARSER])
            ->andWhere(['!=', 'status', QueueGateway::JOB_STATUS_FAILED])
            ->one();

        if (!$parserJob) {
            if (ParserJob::isCanRun($model3dPart->file)) {
                // Create parser job and return stub
                QueueGateway::addFileJob(ParserJob::create($model3dPart->file));
            }
            return Model3dPartPropertiesFactory::createModel3dPartProperties();
        }

        if ($parserJob->status === QueueGateway::JOB_STATUS_COMPLETED) {
            $resultJson = $parserJob->getResultData();
            $model3dPartProp = Model3dPartPropertiesFactory::createModel3dPartProperties($resultJson);
            $model3dPart->setModel3dPartProperties($model3dPartProp);
            return $model3dPartProp;
        }

        return Model3dPartPropertiesFactory::createModel3dPartProperties();
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param File $file
     * @return Model3dBasePartInterface|\common\models\Model3dPart
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public static function addModel3dPartSave(Model3dBaseInterface $model3d, File $file)
    {
        $model3dPart = self::addModel3dPartFile($model3d, $file);
        $fileRepository = Yii::createObject(FileRepository::class);
        $fileRepository->save($model3dPart->file);

        if (!$model3d->coverFile) {
            $model3d->setCoverFile($file);
        }
        $model3d->productCommon->updated_at = DateHelper::now();
        $model3d->productCommon->is_active = 1;
        $model3d->safeSave();
        $model3d->productCommon->safeSave();
        $model3dPart->safeSave();
        return $model3dPart;
    }
}