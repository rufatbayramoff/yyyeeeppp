<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 07.05.18
 * Time: 16:38
 */

namespace common\services;

use common\components\DateHelper;
use common\components\UuidHelper;
use common\interfaces\Model3dBaseInterface;
use common\models\Model3d;
use common\models\Product;
use common\models\ProductCommon;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\SystemReject;
use common\models\User;
use common\models\UserDeactivatingLog;
use common\models\UserProfileDeleteRequest;
use common\models\UserSms;
use DateTime;
use DateTimeZone;
use frontend\models\user\FrontUser;
use lib\message\Constants;
use lib\message\MessageServiceInterface;
use Yii;
use yii\base\BaseObject;

class UserProfileService extends BaseObject
{
    const APPROVE_CODE_LENGTH = 5;

    /**
     * @var MessageServiceInterface
     */
    public $sender;

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if ($this->sender === null) {
            $this->sender = \Yii::$app->message;
        }
    }

    /**
     * @param FrontUser $user
     * @return UserProfileDeleteRequest|null
     */
    public function getLastDeleteAccountConfirmationCode(FrontUser $user): ?UserProfileDeleteRequest
    {
        $userProfileDeleteRequest = $user->getUserProfileDeleteRequests()
            ->andWhere(['>', 'created_at', DateHelper::subNowSec(UserProfileDeleteRequest::APPROVE_CODE_EXPIRE_SEC)])->orderBy('created_at desc')->one();
        return $userProfileDeleteRequest;
    }

    /**
     * @param FrontUser $user
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function deleteAccountSendConfirmation(FrontUser $user)
    {
        $lastConfirmation = $this->getLastDeleteAccountConfirmationCode($user);
        if (!$lastConfirmation) {
            $deleteProfileConfirmation = \Yii::createObject(UserProfileDeleteRequest::class);
            $deleteProfileConfirmation->user_id = $user->id;
            $deleteProfileConfirmation->approve_code = UuidHelper::generateRandCode(self::APPROVE_CODE_LENGTH);
            $deleteProfileConfirmation->created_at = DateHelper::now();
            $deleteProfileConfirmation->safeSave();
        } else {
            $deleteProfileConfirmation = $lastConfirmation;
        }

        $approveUrl = \Yii::$app->params['siteUrl'] . '/my/profile/delete-confirm-by-url?approveCode=' . $deleteProfileConfirmation->approve_code;

        $params = [
            'username'    => $user->getFullNameOrUsername(),
            'approveUrl'  => $approveUrl,
            'approveCode' => $deleteProfileConfirmation->approve_code
        ];
        $this->sender->sendForce($user, 'system.user_profile.deleteApprove', $params, Constants::SENDER_TYPE_EMAIL);
    }

    /**
     * @param FrontUser $user
     * @param bool $wipePersonalInfo
     * @throws \yii\base\Exception
     * @throws \Throwable
     */
    public function deleteAccount(FrontUser $user, $wipePersonalInfo = false)
    {
        $deleteDate = (new DateTime('now', new DateTimeZone('UTC')))->format('Y_m_d_H_i_s');
        $wipedText = 'wiped_' . $deleteDate;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            // printers
            $ps = Ps::findOne(['user_id' => $user->id]);
            if ($ps) {
                if ($wipePersonalInfo) {
                    $ps->title = $ps->title ? $wipedText : '';
                }
                $ps->safeSave();
                $printers = $ps->psPrinters;
                foreach ($printers as $printer) {
                    if ($wipePersonalInfo) {
                        $printer->title = $printer->title ? $wipedText : '';
                        $printer->description = $printer->description ? $wipedText : '';
                    }
                    $printer->safeSave();
                }
                $services = $ps->companyServices;
                foreach ($services as $service) {
                    if ($wipePersonalInfo) {
                        $service->title = $service->title ? $wipedText : '';
                        $service->description = $service->description ? $wipedText : '';
                    }
                    $service->is_deleted = 1;
                    $service->safeSave();
                }
            }

            /** @var ProductCommon[] $productsCommon */
            $productsCommon = ProductCommon::find()->where(['user_id' => $user->id])->all();
            foreach ($productsCommon as $k => $productCommon) {
                $productCommon->is_active = 0;
                if ($wipePersonalInfo) {
                    $productCommon->title = $productCommon->title ? $wipedText : '';
                    $productCommon->description = $productCommon->description ? $wipedText : '';
                }
                $productCommon->safeSave();
            }

            // user
            $user->status = User::STATUS_DELETED;
            if ($wipePersonalInfo) {
                $user->email = $wipedText;
                $user->username = $wipedText;
            } else {
                $user->email = 'deleted_' . $deleteDate . '-' . $user->email;
                $user->username = 'deleted_' . $deleteDate . '-' . $user->username;
            }
            $user->safeSave();

            // profile
            $profile = $user->userProfile;
            if ($wipePersonalInfo) {
                $profile->website = $profile->website ? $wipedText : '';
                $profile->url_facebook = $profile->url_facebook ? $wipedText : '';
                $profile->url_twitter = $profile->url_twitter ? $wipedText : '';
                $profile->url_instagram = $profile->url_instagram ? $wipedText : '';
                $profile->full_name = $profile->full_name ? $wipedText : '';
                $profile->phone = $profile->phone ? $wipedText : '';
                $profile->firstname = $profile->firstname ? $wipedText : '';
                $profile->lastname = $profile->lastname ? $wipedText : '';
                $profile->safeSave();
            }

            // Osn
            foreach ($user->userOsns as $userOsn) {
                $userOsn->email = $wipedText;
                $userOsn->username = $wipedText;
                $userOsn->safeSave();
            }

            if ($userPaypal = $user->userPaypal) {
                $userPaypal->email = $wipedText;
                $userPaypal->fullname = $wipedText;
                $userPaypal->safeSave();
            }

            if ($user->affiliateSourceClient) {
                $user->affiliateSourceClient->delete();
            }

            if ($user->affiliateSources) {
                foreach ($user->affiliateSources as $affiliateSource) {
                    $affiliateSource->delete();
                }
            }

            if ($wipePersonalInfo) {
                UserSms::updateAll([
                    'phone' => $wipedText
                ], [
                    'user_id' => $user->id
                ]);
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param FrontUser $user
     * @param array $data
     * @return UserDeactivatingLog
     * @throws \yii\base\InvalidConfigException
     */
    public function addDeleteAccountReason(FrontUser $user, $data): UserDeactivatingLog
    {
        /** @var UserDeactivatingLog $m */
        $m = Yii::createObject(UserDeactivatingLog::class);

        $m->setScenario(UserDeactivatingLog::SCENARIO_ADD_REASON);

        if ($m->load($data, '')) {
            $m->created_at = dbexpr('NOW()');
            $m->user_id = $user->id;

            $systemReject = SystemReject::findByPk($m->reason_id);
            $m->reason_title = $systemReject->title ?? null;
        }

        $m->save();

        return $m;
    }
}