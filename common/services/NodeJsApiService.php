<?php
/**
 * User: nabi
 */

namespace common\services;


use yii\base\BaseObject;

class NodeJsApiService extends BaseObject
{
    /**
     * @var NodeJsSe
     */
    private $api;

    /**
     * @var string
     */
    public $server;

    /**
     * @var string
     */
    public $password = self::PASSPHRASE;


    const PASSPHRASE = 'info@treatstock.com';

    public function init()
    {
        parent::init();
        if (empty($this->server)) {
            $this->server = param('watermarkServer', 'http://192.168.66.10:5858');
        }
        $this->api = new NodeJsApi($this->server, $this->password);

    }
}