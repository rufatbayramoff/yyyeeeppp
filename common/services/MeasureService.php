<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.09.16
 * Time: 11:36
 */

namespace common\services;

use frontend\models\user\UserFacade;
use lib\MeasurementUtil;

class MeasureService {

    public static function getWeightFormatted($weightInGr, $measureBy = null)
    {
        $measure = $measureBy ? $measureBy : UserFacade::getCurrentMeasurement();
        if ($measure=='in') {
            $weight = max([round(MeasurementUtil::convertGramToOunce($weightInGr), 2), 0.01]) . ' ' . _t('site.model3d', 'oz');
        } else{
            $weight = max([round($weightInGr, 2), 0.01]) . ' ' . _t('site.model3d', 'gr');
        }

        return $weight;
    }
}