<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.11.17
 * Time: 13:10
 */

namespace common\services;

use common\models\factories\LocationFactory;
use common\models\GeoCity;
use common\models\PrinterMaterial;
use common\models\WikiMaterial;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use common\modules\catalogPs\models\CatalogSearchForm;
use frontend\components\UserSessionFacade;

class WikiMaterialService
{
    /**
     * @param WikiMaterial $wikiMaterial
     * @param GeoCity $geoCity
     */
    public static function formPrintersUrl(WikiMaterial $wikiMaterial, $geoCity)
    {
        $materialCodes = '';
        foreach ($wikiMaterial->printerMaterials as $printerMaterial) {
            $materialCodes.=PrinterMaterial::slugifyFilament($printerMaterial->filament_title).'_';
        }
        $materialCodes = substr($materialCodes, 0,-1);

        $location = null;
        if ($geoCity) {
            $location = LocationFactory::createFromGeoCity($geoCity);
        };

        $url =  CatalogPrintingUrlHelper::printing3dCatalog(location: $location, materialCode: $materialCodes);
        return $url;
    }
}