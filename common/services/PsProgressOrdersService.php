<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.10.17
 * Time: 16:12
 */

namespace common\services;

use common\components\BaseActiveQuery;
use common\components\DateHelper;
use common\models\Company;
use common\models\PaymentInvoice;
use common\models\Ps;
use common\models\PsCatalog;
use common\models\PsProgressOrdersCount;
use common\models\query\PsPrinterMaterialQuery;
use common\models\query\PsQuery;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use Yii;
use yii\db\ActiveQuery;

class PsProgressOrdersService
{

    protected function updateOrderCount($psId, $count)
    {
        $psProgressOrdersCount = PsProgressOrdersCount::find()->where(['ps_id' => $psId])->one();
        if ($psProgressOrdersCount) {
            $psProgressOrdersCount->progress_orders_count = $count;
        } else {
            $psProgressOrdersCount                        = new PsProgressOrdersCount();
            $psProgressOrdersCount->ps_id                 = $psId;
            $psProgressOrdersCount->progress_orders_count = $count;
        }
        $psProgressOrdersCount->safeSave();
    }

    protected function createEmptyPsProgressOrdersCount()
    {
        $sql         = "
SELECT
    ps.id,
    ppoc.progress_orders_count
FROM ps
    left join ps_progress_orders_count ppoc on ps.id = ppoc.ps_id 
WHERE ppoc.progress_orders_count is null
";
        $nullProgressList = Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($nullProgressList as $nullProgress) {
            $psProgressOrdersCount                        = new PsProgressOrdersCount();
            $psProgressOrdersCount->ps_id                 = $nullProgress['id'];
            $psProgressOrdersCount->progress_orders_count = 0;
            $psProgressOrdersCount->safeSave();
        }
    }

    protected function getMinPrice(Ps $ps)
    {
        $minPrice = $ps->getPsPrinters()->visible()->moderated()
            ->joinWith(
                [
                    'psPrinterMaterials' => function (PsPrinterMaterialQuery $psMaterialQuery) {
                        $psMaterialQuery
                            ->active()
                            ->joinWith('colors');

                    }
                ],
                false
            )
            ->andWhere('ps_printer_color.price>0')
            ->select('min(ps_printer_color.price) as minPrice')
            ->scalar();

        return $minPrice ?? 99999;
    }

    public function reCalculateCatlog()
    {
        $psList = Ps::find()->all();
        foreach ($psList as $ps) {
            $psCatalog = $ps->psCatalog;
            if (!$psCatalog) {
                $psCatalog        = new PsCatalog();
                $psCatalog->ps_id = $ps->id;
            }
            $psCatalog->rating_avg   = Yii::$app->reviewService->calculatePsRating($ps);
            $psCatalog->rating_med   = Yii::$app->reviewService->calculatePsRatingByFormula($ps);
            $psCatalog->rating_count = Yii::$app->reviewService->getPsShowedReviewsCount($ps);
            $psCatalog->price_low    = $this->getMinPrice($ps);
            $psCatalog->safeSave();
        }
    }

    public function reCalculateOrdersCount()
    {
        $this->createEmptyPsProgressOrdersCount();
        $sql         = "
SELECT
    ps.id AS ps_id,
    ppoc.ps_id as pg_ps_id,
    ppoc.progress_orders_count,
    COUNT(pi.uuid) as new_count
FROM ps
    left join company_service cs on cs.ps_id = ps.id
    left join store_order_attemp a on a.machine_id = cs.id and a.status in ('new' , 'accepted', 'printing')
    left join store_order o on o.current_attemp_id = a.id and (o.user_id != 1)
    left join payment_invoice pi on pi.uuid = o.primary_payment_invoice_uuid and pi.status in ('".PaymentInvoice::STATUS_AUTHORIZED."', '" . PaymentInvoice::STATUS_PAID . "')
    left join ps_progress_orders_count ppoc on ps.id = ppoc.ps_id 
WHERE 1=1
GROUP BY ps.id
HAVING ppoc.progress_orders_count != new_count";
        $allAttempts = Yii::$app->db->createCommand($sql)->queryAll();
        echo "\nPs count: " . count($allAttempts) . "\n";
        foreach ($allAttempts as $attempt) {
            if (!$attempt['ps_id']) {
                continue;
            }
            if (empty($attempt['pg_ps_id']))
                $this->updateOrderCount($attempt['ps_id'], $attempt['new_count']);
            else {
                PsProgressOrdersCount::updateRow(['ps_id' => $attempt['ps_id']], ['progress_orders_count' => $attempt['new_count']]);
            }
        }
    }

    public function zeroOrdersCount()
    {
        $allPs = Ps::find()->all();
        foreach ($allPs as $onePs) {
            $this->updateOrderCount($onePs->id, 0);
        }
    }
}