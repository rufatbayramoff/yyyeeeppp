<?php


namespace common\services;


use common\models\User;
use Yii;

class ReportPhoneProblemService
{
    public function report(User $user)
    {
        $sender = Yii::$app->message->getSenderByType('email');
        $sender->send(\Yii::$app->params['supportEmail'], "User can't confirm phone number", $this->messageTxt($user), $this->messageHtml($user));
    }

    protected function phone(User $user): string
    {
        if ($user->company) {
            return "{$user->company->phone_code} {$user->company->phone}";
        }
        return "";
    }

    protected function messageTxt(User $user): string
    {
        return <<<TAG
        {$user->username} reported issues with getting an sms confirmation code.\n
        Contact user.\n
        Email: {$user->email}\n
        Phone: {$this->phone($user)}
TAG;
    }

    protected function messageHtml(User $user): string
    {
        return <<<TAG
        {$user->username} reported issues with getting an sms confirmation code.<br/>
        Contact user.<br/>
        Email: {$user->email}<br/>
        Phone: {$this->phone($user)}
TAG;
    }
}