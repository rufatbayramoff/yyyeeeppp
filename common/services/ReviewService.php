<?php
/**
 * Created by mitaichik
 */

namespace common\services;


use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\models\CompanyService;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\populators\ReviewPopulator;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\repositories\FileRepository;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderReview;
use common\models\StoreOrderReviewFile;
use common\models\User;
use common\models\UserEmailSent;
use frontend\models\order\ReviewForm;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\BaseObject;
use yii\db\Expression;

/**
 * Class ReviewService
 * @package common\services
 *
 * @property Emailer $emailer
 * @property ReviewPopulator $populator
 */
class ReviewService extends BaseObject
{
    protected $emailer;

    protected $populator;

    public function injectDependencies(Emailer $emailer, ReviewPopulator $populator)
    {
        $this->emailer = $emailer;
        $this->populator = $populator;
    }

    /**
     * @param ReviewForm $form
     * @param StoreOrder $order
     * @param User $user
     *
     * @return array|StoreOrderReview|null|\yii\db\ActiveRecord
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function createOrderReview(ReviewForm $form, StoreOrder $order, User $user)
    {
        AssertHelper::assertValidate($form);

        $oldReview = StoreOrderReview::find()
            ->forAttemp($order->currentAttemp)
            ->forAttempPs($order->currentAttemp)
            ->one();

        if($oldReview) {
            return $oldReview;
        }

        AssertHelper::assert($user->id == $order->user_id, _t('site.order', 'User cannot create review'));
        AssertHelper::assert($this->canCreateOrderReview($order->currentAttemp), _t('site.order', 'Cannot create review for order'));
        $review = new StoreOrderReview();



        /** @var FileFactory $fileFactory */
        $fileFactory = Yii::createObject(FileFactory::class);

        foreach ($form->images as $image) {
            $file = $fileFactory->createFileFromUploadedFile($image);
            $review->addFiles[] = $file;
        }

        if (!empty($form->printFiles)) {
            foreach ($this->getAllowedAttachFiles($order->currentAttemp) as $attachFile) {
                if (\in_array($attachFile->id, $form->printFiles, false)) {
                    $review->addFiles[] = $attachFile;
                }
            }
        }

        $review->setAttributes($form->getAttributes(null, ['images']));
        $review->store_unit_id = $order->orderItem && $order->orderItem->hasModel() ? $order->orderItem->unit_id : null;
        $review->order_id = $order->id;
        $review->ps_id = $order->currentAttemp->ps->id;
        $review->status = StoreOrderReview::STATUS_NEW;
        $review->created_at = new Expression('NOW()');
        AssertHelper::assertSave($review);
        $this->saveOrderReviewFiles($review);
        return $review;
    }

    /**
     * @param StoreOrderAttemp $attemp
     *
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function canCreateOrderReview(StoreOrderAttemp $attemp): bool
    {
        if ($attemp->status != StoreOrderAttemp::STATUS_RECEIVED &&
            $attemp->status!=StoreOrderAttemp::STATUS_DELIVERED &&
            $attemp->status!=StoreOrderAttemp::STATUS_SENT
        ) {
            if($attemp->order->isThingiverseOrder() && $attemp->status === StoreOrderAttemp::STATUS_DELIVERED){
            } else {
                return false;
            }
        }
        $user = UserFacade::getCurrentUser();

        if (!is_guest() && $user->id === $attemp->ps->user_id) {
            // Order to myself
            return false;
        }

        return !StoreOrderReview::find()
            ->forAttemp($attemp)
            ->forAttempPs($attemp)
            ->exists();
    }

    /**
     * @param Ps $ps
     * @return bool
     */
    public function getPsShowedReviewsCount(Ps $ps)
    {
        $val = (int) StoreOrderReview::find()
            ->forPs($ps)
            ->isCanShow()
            ->count();
        return $val;
    }

    /**
     * @param Ps $ps
     * @return int
     */
    public function calculatePsRating(Ps $ps)
    {
        $val = round(
            StoreOrderReview::find()
                ->select('AVG((rating_quality + rating_speed + rating_communication) / 3)')
                ->forPs($ps)
                ->isCanShow()
                ->scalar(),
            1
        );
        return $val;
    }

    public function calculatePsRatingByFormula(Ps $ps)
    {
        $storeReview = StoreOrderReview::find()
            ->select('AVG((rating_quality + rating_speed + rating_communication) / 3) as avg_rating, COUNT(*) as total')
            ->forPs($ps)
            ->isCanShow()
            ->asArray()
            ->one();
        $val = round(
            $storeReview['avg_rating'],
            2
        );
        $R = $storeReview['total'];
        $A = $val;
        $n = 5;
        $a = 2;
        $rating1 = $R / ($R + $n) * $A + $n / ($R + $n) * $a;

        return $rating1;



    }

    /**
     * @param StoreOrderReview $review
     *
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function rejectOrderReview(StoreOrderReview $review): void
    {
        $review->status = StoreOrderReview::STATUS_REJECTED;
        AssertHelper::assertSave($review);
    }

    /**
     * @param StoreOrderReview $review
     *
     * @throws \yii\base\UserException
     */
    public function setReviewFilesPublised(StoreOrderReview $review): void
    {
        foreach ($review->reviewFilesSort as $file) {
            $file->setPublicMode(true);
            $file->safeSave();
        }
    }

    /**
     * @param StoreOrderReview $review
     *
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\UserException
     */
    public function approveOrderReview(StoreOrderReview $review)
    {
        $review->status = StoreOrderReview::STATUS_MODERATED;
        AssertHelper::assertSave($review);
        $this->psOrderReviewedNotification($review);
        $this->setReviewFilesPublised($review);
    }

    /**
     * @param StoreOrderReview $review
     */
    public function psOrderReviewedNotification(StoreOrderReview $review): void
    {
        $psPrinter = $review->ps;
        $hash = UserEmailSent::makeHash(['ps_printer_id' => $psPrinter->id]);

        /** @var UserEmailSent $sentEmail */
        $sentEmail = UserEmailSent::find()->where([
            'user_id'    => $psPrinter->user->id,
            'email_code' => UserEmailSent::PS_ORDER_REVIEWED,
            'hash'       => $hash
        ])->orderBy(['created_at' => SORT_DESC])->one();

        if ($sentEmail && !$sentEmail->checkTimePassed(3600)) {
            return;
        }

        UserEmailSent::addRecord([
            'user_id'    => $psPrinter->user->id,
            'email_code' => UserEmailSent::PS_ORDER_REVIEWED,
            'created_at' => dbexpr('NOW()'),
            'hash'       => $hash
        ]);

        // send email
        $this->emailer->sendPsOrderReviewed($review);
    }

    /**
     * Retrurn order pri
     * nts files who can attach to review
     *
     * @param StoreOrderAttemp $orderJob
     * @return File[]
     */
    public function getAllowedAttachFiles(StoreOrderAttemp $orderJob)
    {
        $images = [];
        if (!$orderJob->order->currentAttemp) {
            return $images;
        }
        if (!$orderJob->order->currentAttemp->moderation) {
            return $images;
        }
        foreach ($orderJob->order->currentAttemp->moderation->files as $moderationFile) {
            $images[] = $moderationFile->file;
        }

        return $images;
    }

    /**
     * Calculate iavg rating of review.
     *
     * @param StoreOrderReview $review
     * @return float
     */
    public static function calculateAvgRating(StoreOrderReview $review) : float
    {
        return round(((float) $review->rating_communication + (float) $review->rating_quality + (float) $review->rating_speed) / 3, 1);
    }

    /**
     * @param CompanyService $company
     *
     * @return int
     */
    public function getCompanyServiceShowedReviewsCount(CompanyService $company): int
    {
        $key = 'ps_company_service_review_count_' . $company->id . $company->updated_at;

        if (YII_ENV !== 'dev' && Yii::$app->cache->exists($key)) {
            return Yii::$app->cache->get($key);
        }

        $val = (int) StoreOrderReview::find()
            ->select([dbexpr('count(sor.id) as count')])
            ->from(['sor' => StoreOrderReview::tableName()])
                ->innerJoin(['so' => StoreOrder::tableName()], 'so.id = sor.order_id')
                ->innerJoin(['soa' => StoreOrderAttemp::tableName()], 'so.current_attemp_id = soa.id')
                ->innerJoin(['cs' => CompanyService::tableName()], 'cs.id = soa.machine_id')
            ->where([
                'sor.ps_id'  => $company->ps->id,
                'sor.status' => StoreOrderReview::STATUS_MODERATED,
                'cs.id' => $company->id
            ])
            ->scalar();

        if (YII_ENV !== 'dev') {
            Yii::$app->cache->set($key, $val, 3600); // 1 hour
        }

        return $val;
    }

    /**
     * @param CompanyService $company
     *
     * @return float
     */
    public function calculateCompanyServiceRating(CompanyService $company): float
    {
        $key = 'ps_company_service_rate_' . $company->id . $company->updated_at;

        if(Yii::$app->cache->exists($key)){
            return Yii::$app->cache->get($key);
        }

        $val = round(
            StoreOrderReview::find()
                ->select([dbexpr('AVG((sor.rating_quality + sor.rating_speed + sor.rating_communication) / 3)')])
                ->from(['sor' => StoreOrderReview::tableName()])
                ->innerJoin(['so' => StoreOrder::tableName()], 'so.id = sor.order_id')
                ->innerJoin(['soa' => StoreOrderAttemp::tableName()], 'so.current_attemp_id = soa.id')
                ->innerJoin(['cs' => CompanyService::tableName()], 'cs.id = soa.machine_id')
                ->where([
                    'sor.ps_id'  => $company->ps->id,
                    'sor.status' => StoreOrderReview::STATUS_MODERATED,
                    'cs.id' => $company->id
                ])
                ->scalar(),
            1
        );

        if (YII_ENV !== 'dev') {
            Yii::$app->cache->set($key, $val, 3600); // 1 hour
        }

        return $val;
    }

    /**
     * @param StoreOrderReview $review
     *
     * @throws \yii\base\UserException
     */
    public function approveAnswerReview(StoreOrderReview $review)
    {
        $review->verified_answer_status = StoreOrderReview::ANSWER_STATUS_MODERATED;
        $review->answer_reject_comment = null;
        $review->safeSave(['verified_answer_status', 'answer_reject_comment']);
    }

    /**
     * @param StoreOrderReview $review
     */
    public function sendClientReviewAnswer(StoreOrderReview $review)
    {
        if ($review->isCanShow() && !empty($review->answer)) {
            $this->emailer->sendClientReviewAnswer($review);
        }
    }

    /**
     * @param StoreOrderReview $review
     * @param $data
     *
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\db\StaleObjectException
     */
    public function saveOrderReviewAdmin(StoreOrderReview $review, $data): void
    {
        $this->populator->populateAdmin($review, $data);

        AssertHelper::assertValidate($review);

        $this->saveOrderReviewFiles($review);
        $this->setMainImage($review);

        AssertHelper::assertSave($review);
    }

    /**
     * @param StoreOrderReview $review
     *
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function saveOrderReviewFiles(StoreOrderReview $review): void
    {
        /** @var FileRepository $fileRepository */
        $fileRepository = Yii::createObject(FileRepository::class);

        foreach ($review->addFiles as $file) {
            $file->setPublicMode(true);
            $fileRepository->save($file);

            StoreOrderReviewFile::addRecord([
                'review_id' => $review->id,
                'file_uuid' => $file->uuid,
                'created_at' => DateHelper::now()
            ]);
        }

        if (!empty($review->deleteFilesUuid)) {
            foreach ($review->storeOrderReviewFiles as $storeOrderReviewFile) {
                if (\in_array($storeOrderReviewFile->file_uuid, $review->deleteFilesUuid, true)) {
                    $storeOrderReviewFile->delete();

                    if (!$storeOrderReviewFile->isStoreOrderAttemptModerationFile()) {
                        $fileRepository->safeDelete($storeOrderReviewFile->file);
                    }
                }
            }
        }
    }

    /**
     * @param StoreOrderReview $review
     *
     * @return $this
     * @throws \yii\base\UserException
     */
    public function setMainImage(StoreOrderReview $review)
    {
        if ($review->setMainImageUuid === null) {
            return $this;
        }

        StoreOrderReviewFile::updateAll(['is_main' => 0], ['review_id' => $review->id]);

        /** @var StoreOrderReviewFile $storeOrderReviewFile */
        $storeOrderReviewFile = StoreOrderReviewFile::find()->where([
            'review_id' => $review->id,
            'file_uuid' => $review->setMainImageUuid
        ])->one();

        if ($storeOrderReviewFile) {
            $storeOrderReviewFile->is_main = 1;
            $storeOrderReviewFile->safeSave(['is_main']);
        }

        return $this;
    }
}