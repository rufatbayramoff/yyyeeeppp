<?php
/**
 * Created by mitaichik
 */

namespace common\services;


use common\components\ArrayHelper;
use common\components\Emailer;
use common\components\order\PriceCalculator;
use common\components\orderOffer\OrderOffer;
use common\components\orderOffer\OrderOfferFilter;
use common\components\orderOffer\OrderOfferLocatorService;
use common\models\DeliveryType;
use common\models\factories\DeliveryFormFactory;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\PsPrinter;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\modules\payment\fee\FeeHelper;
use frontend\models\ps\PsFacade;
use lib\delivery\parcel\Parcel;
use lib\delivery\parcel\ParcelFactory;
use lib\money\Currency;
use lib\money\Money;
use lib\money\MoneyMath;
use Yii;
use yii\base\UserException;

/**
 * Class ChangeOfferService
 *
 * @package common\services
 */
class ChangeOfferService
{
    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @var StoreOrderService
     */
    private $orderService;

    /** @var DeliveryFormFactory */
    protected $deliveryFormFactory;

    /**
     * ChangeOfferService constructor.
     *
     * @param Emailer $emailer
     * @param StoreOrderService $orderService
     * @param DeliveryFormFactory $deliveryFormFactory
     */
    public function __construct(Emailer $emailer, StoreOrderService $orderService, DeliveryFormFactory $deliveryFormFactory)
    {
        $this->emailer             = $emailer;
        $this->orderService        = $orderService;
        $this->deliveryFormFactory = $deliveryFormFactory;
    }

    /**
     * Find printers for order
     *
     * @param StoreOrder $order
     * @param bool $exactMaterialAndColor
     * @return OrderOffer[]
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function findOffersForStoreOrder(StoreOrder $order, $exactMaterialAndColor = false, $hideCancelled = false, $sameCurrency =true): array
    {
        if ($order->order_state == StoreOrder::STATE_CLOSED || $order->order_state == StoreOrder::STATE_REMOVED) {
            throw new UserException(_t('site.ps', 'Order already closed.'));
        }

        $clientLocation = LocationService::calulateClientLocationByOrder($order);
        $firstItem      = $order->getFirstItem();
        $model3d        = $firstItem->model3dReplica;

        /** @var OrderOfferLocatorService $offersLocator */
        $offersLocator = \Yii::createObject(OrderOfferLocatorService::class);

        if ($exactMaterialAndColor) {
            $textures       = PrinterMaterialService::getModelTextures($model3d);
            $exactMaterials = ArrayHelper::getColumn($textures, 'printerMaterial');
            if ((count($exactMaterials)) == 1 && ($exactMaterials[0] == null)) {
                // Material not selected
            } else {
                $offersLocator->allowedOnlyMaterials = $exactMaterials;
            }
        }
        if ($sameCurrency) {
            $currency = $order->getCurrency();
            $offersLocator->currencies = [$currency];
        }
        if ($order->delivery_type_id === DeliveryType::INTERNATIONAL_STANDART_ID) {
            $offersLocator->onlyInternational = true;
        }

        $offersLocator->initAnonymousBundle($clientLocation);
        $offersLocator->formOffersListForModel($model3d);
        Model3dService::resetTextureMatreialInfo($model3d);
        $allOffersList = $offersLocator->getAllOffersList();

        $offersLocator->formCosts($allOffersList, $model3d);

        /** @var OrderOfferFilter $orderOfferFilter */
        $orderOfferFilter = \Yii::createObject(OrderOfferFilter::class);
        $allOffersList    = $orderOfferFilter->filterByDeliveryType($allOffersList, $order->deliveryType, $order->storeOrderDelivery);

        return $allOffersList;
    }

    /**
     * @param StoreOrder $order
     *
     * @return array
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function getWarnings(StoreOrder $order)
    {
        $warnings = [];
        if ($order->currentAttemp) {
            if ($order->currentAttemp->delivery->tracking_number) {
                $warnings[] = 'Attempt has delivery with tracking number';
            }
        }
        return $warnings;
    }

    /**
     * add printer request to offer
     *
     * @param StoreOrder $order
     * @param PsPrinter[] $printers
     * @throws UserException
     */
    public function makeOffers($order, $printers)
    {
        if ($order->order_state == StoreOrder::STATE_CLOSED || $order->order_state == StoreOrder::STATE_REMOVED) {
            throw new UserException(_t('site.ps', 'Order already closed.'));
        }

        if (!$printers) {
            return;
        }

        if ($currentAttemp = $order->currentAttemp) {
            if (!$currentAttemp->isCancelled()) {
                $this->orderService->cancelAttemp($currentAttemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
                $this->emailer->sendPsAdminChangeOffer($currentAttemp);
            }
            $order->unlink('currentAttemp', $currentAttemp);
            $order->link('previousAttemp', $currentAttemp);
        }

        foreach ($printers as $printer) {
            $sameAlreadyExist = StoreOrderAttemp::find()
                ->forOrder($order)
                ->forPrinterId($printer->id)
                ->notInStatus(StoreOrderAttemp::STATUS_CANCELED)
                ->exists();

            if ($sameAlreadyExist) {
                continue;
            }

            $orderAttemp = $this->orderService->createAttempForMachine($order, $printer->companyService, true);
            $this->emailer->sendPsOfferOrder($orderAttemp, $printer);
            PsFacade::resetOrdersCount($printer->ps->user);
        }
    }

    public function getDeliveryPriceCache(StoreOrder $order, PsPrinter $printer)
    {
        $cacheKey = 'changeOfferShippingPrice2' . $order->id . '-' . $printer->id;
        if (($price = app('cache')->get($cacheKey))) {
            return $price;
        }
        return null;
    }

    public function getDeliveryPrice(StoreOrder $order, PsPrinter $printer)
    {
        $printPrice = PriceCalculator::calculateModel3dPrintPriceWithPackageFee($order->getFirstReplicaItem(), $printer);
        $delivery   = $order->storeOrderDelivery;
        if ($delivery->calculated_parcel_weight) {
            $parcel = new Parcel(
                $delivery->calculated_parcel_width,
                $delivery->calculated_parcel_height,
                $delivery->calculated_parcel_length,
                $delivery->calculated_parcel_weight,
                $delivery->calculated_parcel_measure
            );
        } else {
            $parcel = ParcelFactory::createFromOrder($order);
            $this->orderService->saveCalulcatedParcel($order, $parcel);
        }
        $deliveryForm  = $this->deliveryFormFactory->createByOrder($order, $printer);
        $rate          = Yii::$app->deliveryService->getOrderRate(
            $printer->companyService->asDeliveryParams(),
            $deliveryForm,
            $printPrice,
            $parcel,
            Currency::USD);
        $deliveryPrice = [
            'shipping' => $rate->delveiryCost->round()->getAmount(),
        ];
        $cacheKey      = 'changeOfferShippingPrice2' . $order->id . '-' . $printer->id;
        app('cache')->set($cacheKey, $deliveryPrice);
        return $deliveryPrice;
    }

    /**
     * @param StoreOrder $order
     * @param OrderOffer[] $offers
     * @param bool $isPrintPriceOnly
     * @return array
     * @throws \common\components\exceptions\IllegalStateException
     */
    public function getFormatedOffers(StoreOrder $order, $offers)
    {
        $feeHelper = FeeHelper::init();

        /** @var StoreOrderAttemp $originalAttemp */
        $originalAttemp = ArrayHelper::first(
            $order->attemps,
            function (StoreOrderAttemp $attemp) {
                return $attemp->is_offer == false;
            }
        );
        $offersFormated = [];
        foreach ($offers as $offer) {
            $printer = $offer->getPrinter();

            $printPrice           = $offer->anonymousPrintPrice;
            $feePrice             = $feeHelper->getPrintFeeFromPrice($printPrice);
            $printPriceWithoutFee = MoneyMath::minus($printPrice, $feePrice);

            if ($offer->estimateDeliveryCost) {
                $shippingPriceMoney = $offer->estimateDeliveryCost;
                if ($offer->packingPrice) {
                    $shippingPriceMoney = MoneyMath::minus($shippingPriceMoney, $offer->packingPrice);
                }
                $shippingPrice = $shippingPriceMoney;
                $packagePrice  = $offer->packingPrice;
            } else {
                $deliveryPriceArray = $this->getDeliveryPriceCache($order, $printer);
                if (!$deliveryPriceArray || !$deliveryPriceArray['shipping']) {
                    $shippingPrice      = 'TBC';
                    $packagePriceAmount = $offer->getPrinter()->companyService->asDeliveryParams()->getPsDeliveryTypeByCode($order->deliveryType->code)->packing_price;
                    $packagePrice       = Money::create($packagePriceAmount, $printPrice->getCurrency());
                } else {
                    $shippingPrice = Money::create($deliveryPriceArray['shipping'], $printPrice->getCurrency());
                    $packagePrice  = Money::create($deliveryPriceArray['packing'], $printPrice->getCurrency());
                }

            }
            $producePrice = MoneyMath::sum($printPriceWithoutFee, $packagePrice);

            $isHighlighted = $originalAttemp->ps->equals($printer->ps);
            if (!$isHighlighted) {
                foreach ($order->attemps as $attemp) {
                    if ($attemp->machine->asPrinter()->equals($printer) && ($attemp->status == StoreOrderAttemp::STATUS_CANCELED)) {
                        $isHighlighted = true;
                        break;
                    }
                }
            }
            $isCancelled = false;
            foreach ($order->attemps as $attemp) {
                if ($attemp->machine->asPrinter()->equals($printer) && $attemp->isCancelled() && $attemp->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_PS) {
                    $isCancelled = true;
                }
            }

            $offersFormated[] = [
                'printer' => $printer,

                'producePrice'  => $producePrice,
                'shippingPrice' => $shippingPrice,

                'isHighlighted' => $isHighlighted,
                'isCancelled'   => $isCancelled,
            ];
        }
        usort(
            $offersFormated,
            function ($offer1, $offer2) {
                return $offer1['producePrice']?->getAmount() <=> $offer2['producePrice']?->getAmount();
            }
        );
        return $offersFormated;
    }
}