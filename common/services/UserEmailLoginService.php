<?php
/**
 * User: nabi
 */

namespace common\services;


use common\components\DateHelper;
use common\components\Emailer;
use common\components\exceptions\BusinessException;
use common\models\Company;
use common\models\ThingiverseOrder;
use common\models\ThingiverseUser;
use common\models\User;
use common\models\UserEmailLogin;
use common\modules\xss\helpers\XssHelper;
use common\repositories\BaseActiveRecordRepository;
use frontend\models\user\FrontUser;
use yii\base\BaseObject;
use yii\base\UserException;
use yii\web\Application;
use yii\web\Response;

/**
 * Class UserEmailLoginService
 *
 * Usage:
 * <code>
 * // to get object
 * $r = UserEmailLoginService::requestLoginByEmail(User $user, $email, $requestedUrl)
 *
 * // to use, this will login and redirect user
 * $done = UserEmailLoginService::loginByEmailLink(UserEmailLogin::tryFind(['hash'=>$hash]);
 * </code>
 *
 * @package common\services
 */
class UserEmailLoginService extends BaseObject
{

    /**
     * expired period
     */
    const EXPIRED_PERIOD_HOURS = 12;

    /**
     * @var Emailer
     */
    public $emailer;

    /**
     * @var Response
     */
    public $response;

    /**
     * @var BaseActiveRecordRepository
     */
    public $arRepository;

    public function injectDependencies(Emailer $emailer, Response $response, BaseActiveRecordRepository $arRepository)
    {
        $this->emailer = $emailer;
        $this->response = $response;
        $this->arRepository = $this->arRepository ? $this->arRepository : $arRepository;
    }

    /**
     * 1. checks that login is valid
     * 2. marks as visited
     * 3. logins
     * 4. redirects
     *
     * @param UserEmailLogin $emailLogin
     * @return Response
     * @throws BusinessException
     * @throws \yii\base\Exception
     */
    public function signInByUserEmailLogin(UserEmailLogin $emailLogin)
    {
        if (!$this->isValidEmailLogin($emailLogin)) {
            throw new BusinessException(_t('site.user', 'Invalid sign-in link'));
        }
        $emailLogin = $this->markAsVisited($emailLogin);
        $frontUser = FrontUser::findIdentity($emailLogin->user_id);
        if ($frontUser->needConfirm()) {
            $frontUser->status = User::STATUS_ACTIVE;
            $frontUser->save(false);
            if ($frontUser->company && $frontUser->company->is_backend) {
                $frontUser->company->moderator_status = Company::MSTATUS_CHECKED;
                $frontUser->company->is_backend = 0;
                $frontUser->company->safeSave();
            }
        }

        $this->arRepository->save($emailLogin);
        if (\Yii::$app instanceof Application) {
            if (!is_guest()) {
                app('user')->switchIdentity($frontUser);
            } else {
                \Yii::$app->user->login($frontUser, 0);
            }
        }

        return $this->response->redirect($emailLogin->requested_url, 302, false);
    }

    /**
     * this will create and save user_email_login
     * object is returned. UserEmailLogin::getLoginLinkByHash() - should be used
     *
     * @param User $user
     * @param $email
     * @param $requestedUrl
     * @return UserEmailLogin
     * @throws \yii\base\Exception
     */
    public function generateUserEmailLogin(User $user, $email, $requestedUrl)
    {
        $emailLogin = $this->generateEmailLogin($user, $email);
        $emailLogin->requested_url = $requestedUrl;
        $this->arRepository->save($emailLogin);
        return $emailLogin;
    }

    /**
     * request login by email
     * we need user who requested, email (this is required for thingiverse users, where email is not defined)
     * and requested url
     *
     * @param User $user
     * @param $email
     * @param $requestedUrl
     * @param bool $sendEmail - email with login link will be sent. set false if you plan to send another email
     * @return UserEmailLogin
     * @throws \yii\base\Exception
     */
    public function requestLoginByEmail(User $user, $email, $requestedUrl, $sendEmail = true)
    {
        $emailLogin = $this->generateEmailLogin($user, $email);
        $emailLogin->requested_url = $requestedUrl;
        if ($sendEmail) {
            $this->sendLoginLink($emailLogin);
        }
        $this->arRepository->save($emailLogin);
        return $emailLogin;
    }

    public function sendLoginLink(UserEmailLogin $emailLogin, $updateLink = false, $password = '')
    {
        if ($updateLink) {
            $emailLogin->requested_url = $updateLink;
            $this->arRepository->save($emailLogin);
        }
        return $this->emailer->sendLoginLinkByEmail($emailLogin, $password);
    }

    /**
     * @param $email
     * @return User|static
     */
    public function findValidUserByEmail($email)
    {
        if (empty($email)) {
            return null;
        }
        // try simple user
        $user = User::findOne(['email' => $email]);
        if (!$user) {
            // try thingiverse
            $thingUser = ThingiverseUser::findOne(['email' => $email]);
            if ($thingUser) {
                $user = $thingUser->user;
            }
        }
        return $user;
    }

    /**
     * @param ThingiverseOrder $thingiverseOrder
     * @param string $template
     * @return UserEmailLogin
     * @throws \yii\base\Exception
     */
    public function requestLoginByEmailWithThingiverseOrder(ThingiverseOrder $thingiverseOrder, $template = '')
    {
        $user = $thingiverseOrder->order->user;
        $thingUser = $thingiverseOrder->order->user->thingiverseUser;

        $emailLogin = $this->generateEmailLogin($user, $thingUser->email);
        $emailLogin->requested_url = param('server') . '/workbench/order/view/' . $thingiverseOrder->order->id;
        if ($this->hasEmailLoginRequestToday($thingUser->email, $emailLogin->requested_url)) {
            return $emailLogin;
        }
        $this->emailer->sendLoginLinkByEmailThingiverse($emailLogin, $thingiverseOrder, $template);
        $this->arRepository->save($emailLogin);
        return $emailLogin;
    }

    /**
     * @param $email
     * @param $requestedUrl
     * @return bool
     */
    public function hasEmailLoginRequestToday($email, $requestedUrl)
    {
        return UserEmailLogin::find()
            ->where(['email' => $email, 'requested_url' => $requestedUrl])
            ->andWhere(['>', 'created_at', date("Y-m-d")])
            ->exists();
    }

    /**
     * after finding $emailLogin = UserEmailLogin::tryFind(['hash'=>$hash]);
     * check if it's valid
     *
     * @param UserEmailLogin $emailLogin
     * @return mixed
     */
    public function isValidEmailLogin(UserEmailLogin $emailLogin)
    {
        $result = true;
        if (!empty($emailLogin->visited_at)) {
            $result = false;
        }
        if ($emailLogin->expired_at < DateHelper::now()) {
            $result = false;
        }
        return $result;
    }

    /**
     * mark login as visited (not saved) used by loginByEmailLink
     *
     * @see UserEmailLoginService::signInByUserEmailLogin()
     * @internal
     * @param UserEmailLogin $emailLogin
     * @return UserEmailLogin
     * @throws BusinessException
     */
    public function markAsVisited(UserEmailLogin $emailLogin)
    {
        if (!empty($emailLogin->visited_at)) {
            throw new BusinessException('User login by email not valid');
        }
        $emailLogin->visited_at = DateHelper::now();
        return $emailLogin;
    }

    /**
     * prepare object for user_email_login
     * returned as not saved, but ready to save
     *
     * @see UserEmailLoginService::requestLoginByEmail()
     * @internal
     * @param User $user
     * @param $email
     * @return UserEmailLogin
     */
    public function generateEmailLogin(User $user, $email)
    {
        $emailLogin = new UserEmailLogin();
        $emailLogin->user_id = $user->id;
        $emailLogin->populateRelation('user', $user);
        $emailLogin->email = $email;
        $emailLogin->created_at = DateHelper::now();
        $emailLogin->expired_at = DateHelper::addNowSec(self::EXPIRED_PERIOD_HOURS * 60 * 60);
        $emailLogin->hash = generateUid(16);
        return $emailLogin;
    }
}