<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.07.16
 * Time: 12:39
 */


namespace common\services;


use common\models\CatalogCost;
use common\models\CsWindow;
use common\models\Model3dTexture;
use common\models\Ps;
use common\models\PsCncMachine;
use common\models\PsFacebookPage;
use common\models\CompanyService;
use common\models\PsPrinter;
use frontend\widgets\PrintByPsPrinterWidget;
use Yii;

class PsPrinterService
{
    public static function getCsWindowWidgetHtmlCode(CsWindow $csWindow): string
    {
        $comment   = '<!-- Service: ' . $csWindow->uid . '-' . $csWindow->companyService->getTitleLabel() . ' -->';
        $iframeUrl = \Yii::$app->params['siteUrl'] . '/preorder/window?windowUid=' . $csWindow->uid;
        return htmlspecialchars(preg_replace('/\s+/', ' ', $comment . '<iframe class="ts-embed-printservice" width="100%" height="650px" src="' . $iframeUrl . '" frameborder="0"></iframe>'));
    }

    public static function getPsPrinterWidgetHtmlCode(PsPrinter $psPrinter, $reviewsBlock = true)
    {
        $comment = '<!-- Printer: ' . $psPrinter->id . '-' . $psPrinter->title . ' -->';
        $style   = PrintByPsPrinterWidget::getPsWidgetHtmlStyle();
        //$iframeUrl = \Yii::$app->params['siteUrl'] . '/my/ps-widget/?psPrinterId=' . $psPrinter->id;
        $iframeUrl = \Yii::$app->params['siteUrl'] . '/my/print-model3d/widget?posUid=fixedPsPrinter&psPrinterId=' . $psPrinter->id . ($reviewsBlock ? '' : '&disableReviews=1');
        return htmlspecialchars(preg_replace('/\s+/', ' ', $comment . $style . '<iframe class="ts-embed-printservice" width="100%" height="650px" src="' . $iframeUrl . '" frameborder="0"></iframe>'));
    }

    public static function getPsCncPrinterWidgetHtmlCode(CompanyService $psMachine)
    {
        $style     = PrintByPsPrinterWidget::getPsWidgetHtmlStyle();
        $iframeUrl = \Yii::$app->params['siteUrl'] . '/my/ps-widget/cnc/?widgetCncId=' . $psMachine->id;
        return htmlspecialchars(preg_replace('/\s+/', ' ', $style .
            '<iframe class="ts-embed-printservice" width="100%" height="780px" src="' . $iframeUrl . '" frameborder="0"></iframe>'));
    }

    public static function getPsWidgetUrl(Ps $ps, $reviewsBlock = true)
    {
        return \Yii::$app->params['siteUrl'] . '/order-upload/widget?posUid=fixedPs&psId=' . $ps->id . ($reviewsBlock ? '' : '&disableReviews=1');
    }

    public static function getPsWidgetHtmlCode(Ps $ps, $reviewsBlock = true)
    {
        $comment   = '<!-- Ps: ' . $ps->id . '-' . $ps->title . ' -->';
        $style     = PrintByPsPrinterWidget::getPsWidgetHtmlStyle();
        $iframeUrl = self::getPsWidgetUrl($ps, $reviewsBlock);
        return htmlspecialchars(preg_replace('/\s+/', ' ', $comment . $style . '<iframe class="ts-embed-printservice" width="100%" height="650px" src="' . $iframeUrl . '" frameborder="0"></iframe>'));
    }

    /**
     * @param $ps
     * @param $faceBookPageId
     *
     * @throws \yii\base\UserException
     */
    public static function addFacebookPage($ps, $faceBookPageId)
    {
        $psFacebookPage = PsFacebookPage::findOne(['ps_id' => $ps->id, 'facebook_page_id' => $faceBookPageId]);
        if ($psFacebookPage) {
            return;
        }

        $psFacebookPage                   = new PsFacebookPage();
        $psFacebookPage->ps_id            = $ps->id;
        $psFacebookPage->facebook_page_id = $faceBookPageId . '';
        $psFacebookPage->safeSave();
    }

    public static function removeFacebookPage($ps, $faceBookPageId)
    {
        $psFacebookPage = PsFacebookPage::findOne(['ps_id' => $ps->id, 'facebook_page_id' => $faceBookPageId]);
        if ($psFacebookPage) {
            $psFacebookPage->delete();
        }
    }


    public static function getPsByLink($url)
    {
        if ($pos = strpos($url, '/3d-printing-services/')) {
            $psUrlParameter = substr($url, $pos + strlen('/3d-printing-services/'), 1024);
            $ps             = Ps::findOne(['url' => $psUrlParameter]);
            return $ps;
        }
        return null;
    }

    /**
     * @param Ps $ps
     * @return string
     */
    public static function getFormatedPsDeliveryCountries($ps)
    {
        $returnValue = '';
        foreach ($ps->psPrinters as $psPrinter) {
            $returnValue .= '"' . $psPrinter->companyService->location->country->title . '", ';
        }
        $returnValue = substr($returnValue, 0, -2);
        return $returnValue;
    }

    public static function updatePrintersCache(): void
    {
        Yii::$app->getModule('printersList')->printersTreeUpdater->asyncUpdate();
        CatalogCost::deleteAll();
    }
}