<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.09.17
 * Time: 10:28
 */

namespace common\services;

use common\components\DateHelper;
use common\components\exceptions\BusinessException;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBaseInterface;
use common\models\factories\FileFactory;
use common\models\factories\Model3dImageFactory;
use common\models\File;
use common\models\Model3dHistory;
use common\models\Model3dImg;
use common\models\repositories\FileRepository;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\user\UserFacade;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use yii\base\Component;
use yii\base\UserException;
use yii\imagine\Image;

class Model3dImageService extends  Component
{
    /** @var  FileFactory */
    protected $fileFactory;

    /** @var FileRepository */
    protected $fileRepository;

    /** @var  Model3dImageFactory */
    public $model3dImageFactory;

    public function injectDependencies(
        FileFactory $fileFactory,
        Model3dImageFactory $model3dImageFactory,
        FileRepository $fileRepository
    ) {
        $this->fileFactory = $fileFactory;
        $this->model3dImageFactory = $model3dImageFactory;
        $this->fileRepository = $fileRepository;
    }

    public function addModel3dImgFile(Model3dBaseInterface $model3d, File $file)
    {
        $file->setPublicMode(true);
        ImageHtmlHelper::stripExifInfo($file);
        $model3dImage = $this->model3dImageFactory->createModel3dImage($model3d, $file);
        $model3dImages = $model3d->model3dImgs;
        $model3dImages[] = $model3dImage;
        $model3d->setModel3dImgs($model3dImages);
        if (!$model3d->coverFile || (!in_array($model3d->coverFile->getFileExtension(), Model3dBaseImgInterface::ALLOWED_FORMATS))) {
            $model3d->setCoverFile($file);
        }
        return $model3dImage;
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @param File $file
     * @return Model3dImg
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     */
    public function addModel3dImgFileAndSave(Model3dBaseInterface $model3d, File $file)
    {
        $model3dImage = $this->addModel3dImgFile($model3d, $file);
        $this->fileRepository->save($file);
        $model3d->cover_file_id = $file->id;
        $model3d->productCommon->updated_at = DateHelper::now();
        $model3d->productCommon->is_active = 1;
        $model3d->safeSave();
        $model3d->productCommon->safeSave();
        $model3dImage->safeSave();
        return $model3dImage;
    }

    /**
     * @param Model3dImg $model3dImg
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     */
    public function createOriginalCopyIfNotExists(Model3dImg $model3dImg)
    {
        if (!$model3dImg->originalFile) {
            $originalFile = $this->fileFactory->createFileFromCopy($model3dImg->file);
            $this->fileRepository->save($originalFile);
            $model3dImg->setOriginalFile($originalFile);
            $model3dImg->safeSave();
        }
    }

    public function createCropTempFile(Model3dImg $model3dImg, $cropParams)
    {
        $x1 = app('request')->post('x1');
        $y1 = app('request')->post('y1');

        $h = app('request')->post('h');
        $w = app('request')->post('w');

        $rotate = app('request')->post('rotate');
        $scaleX = app('request')->post('scaleX');
        $scaleY = app('request')->post('scaleY');

        if ($w < 200 || $h < 150) {
            throw new BusinessException(_t('store.model3d', 'Please set crop area larger than 200x150'));
        }

        $image = Image::getImagine()->open($model3dImg->originalFile->getLocalTmpFilePath());

        $cropSizeThumb = new Box($w, $h); //frame size of crop
        $cropPointThumb = new Point($x1, $y1);

        if ($scaleX == -1) {
            $image->flipHorizontally();
        }
        if ($scaleY == -1) {
            $image->flipVertically();
        }

        $image->rotate($rotate);

        $image->crop($cropPointThumb, $cropSizeThumb);

        $tempImgFilePath = Yii::getAlias('@runtime') . '/cropImage_' . $model3dImg->id . '_' . date('Y_m_d_H_i_s');
        $image->save($tempImgFilePath, ['quality' => 100]);
        $model3dImg->file->publishFileToServerAndSave($tempImgFilePath);
        @unlink($tempImgFilePath);
        $model3dImg->updated_at = DateHelper::now();
        $model3dImg->safeSave();

        Model3dHistory::log(
            UserFacade::getCurrentUserId(),
            $model3dImg->model3d,
            Model3dHistory::ACTION_UPDATE_IMG,
            json_encode(['id' => $model3dImg->id, 'name' => $model3dImg->basename, 'crop' => true]),
            json_encode(['cropPosition' => null]),
            json_encode(
                [
                    'cropPosition' => [
                        'x'      => $x1,
                        'y'      => $y1,
                        'h'      => $h,
                        'w'      => $w,
                        'rotate' => $rotate,
                        'scaleX' => $scaleX,
                        'scaleY' => $scaleY
                    ]
                ]
            )
        );
    }
}