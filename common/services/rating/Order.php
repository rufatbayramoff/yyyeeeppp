<?php


namespace common\services\rating;


use common\models\Company;
use common\modules\company\repositories\OrderRepository;
use Yii;

class Order implements Rating
{

    public function __construct()
    {

    }

    public function count(Company $company): Company
    {
        $orderRepository = Yii::createObject(OrderRepository::class);
        $cancel = $orderRepository->canceledPs($company);
        $order = $orderRepository->order($company);

        if($order > 0) {
            $percent = (int)((100 * $cancel) / $order );
            $rating = 0;
            if($percent > 0) {
                $rating = 1000;
            }
            if($percent > 10) {
                $rating = 500;
            }

            $company->psCatalog->addToLog([
                'order_rating' => $rating,
                'order_value' => $percent,
            ]);

            $company->psCatalog->editRating(
                $company->psCatalog->rating + $rating
            );
        } else {
            $company->psCatalog->addToLog([
                'order_rating' => 0,
                'order_value' => 0,
            ]);
        }

        return $company;
    }
}