<?php


namespace common\services\rating;


use common\models\Company;
use common\models\PsCatalog;
use common\modules\company\repositories\OrderRepository;

class Conversion implements Rating
{

    public function count(Company $company): Company
    {
        $orderRepository = \Yii::createObject(OrderRepository::class);

        $preOrderCount = $orderRepository->preorder($company);
        $payOrderCount = $orderRepository->payOrder($company);
        $completedOrderCount = $orderRepository->completedOrder($company);
        $conversion = ($preOrderCount + $payOrderCount) > 0 ? round(($completedOrderCount / ($preOrderCount + $payOrderCount)) * 100, 2) : 0;
        $company->psCatalog->addToLog([
            PsCatalog::CONVERSION => $conversion,
        ]);
        return $company;
    }
}