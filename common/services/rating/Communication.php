<?php


namespace common\services\rating;


use common\models\Company;
use common\models\StoreOrderReview;
use yii\db\Expression;

class Communication implements Rating
{

    public function count(Company $company): Company
    {
        $rating = 0;
        $reviewAvg = $this->review($company);
        if($reviewAvg > 4.1) {
            $rating = 100;
        }
        if($reviewAvg > 4.5) {
            $rating = 500;
        }
        if($reviewAvg > 4.9) {
            $rating = 1000;
        }
        $company->psCatalog->addToLog([
            'communication_rating' => $rating,
            'communication_value' => $reviewAvg,
        ]);
        $company->psCatalog->editRating(
            $company->psCatalog->rating + $rating
        );
        return $company;
    }

    public function review(Company $company): float
    {
        return round(StoreOrderReview::find()
            ->select('AVG((rating_quality + rating_speed + rating_communication) / 3)')
            ->forPs($company)
            ->isCanShow()
            ->andWhere(['>','created_at',new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->scalar(), 2);
    }
}