<?php


namespace common\services\rating;


use common\models\Company;
use common\modules\company\repositories\OrderRepository;
use Yii;

class Recivied implements Rating
{


    public function __construct(private OrderRepository $orderRepository)
    {

    }


    public function count(Company $company): Company
    {
        $rating = 0;
        $conversion = $this->conversion($company);
        if($conversion > 50) {
            $rating = 1000;
        }
        if($conversion > 70) {
            $rating = 2000;
        }
        if($conversion > 90) {
            $rating = 3000;
        }

        $company->psCatalog->addToLog([
            'recivied_rating' => $rating,
            'recivied_value' => $conversion
        ]);

        $company->psCatalog->editRating(
            $company->psCatalog->rating + $rating
        );
        return $company;
    }

    /**
     * @param Company $company
     * @return float|int
     */
    public function conversion(Company $company): float|int
    {
        $preOrderCount = $this->orderRepository->preorder($company);
        $payOrderCount = $this->orderRepository->payOrder($company);
        $completedOrderCount = $this->orderRepository->completedOrder($company);
        return ($preOrderCount + $payOrderCount) > 0 ? round(($completedOrderCount / ($preOrderCount + $payOrderCount)) * 100, 2) : 0;
    }
}