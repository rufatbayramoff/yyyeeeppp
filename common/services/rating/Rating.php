<?php


namespace common\services\rating;


use common\models\Company;

interface Rating
{
    public function count(Company $company): Company;
}