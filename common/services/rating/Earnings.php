<?php


namespace common\services\rating;


use common\models\Company;
use common\models\PaymentAccount;
use common\models\PaymentDetail;
use common\modules\payment\services\PaymentAccountService;
use lib\money\Currency;
use yii\db\Expression;

class Earnings implements Rating
{

    /**
     * Earnings constructor.
     * @param PaymentAccountService $paymentAccountService
     */
    public function __construct(private PaymentAccountService $paymentAccountService)
    {

    }

    public function count(Company $company): Company
    {
        $money = $this->payments($company);
        $rating = (int)($money * 0.5);
        $company->psCatalog->addToLog([
            'earnings_rating' => $rating,
            'earnings_value' => $money
        ]);
        $company->psCatalog->editRating(
            $company->psCatalog->rating + $rating
        );
        return $company;
    }

    private function account(Company $company)
    {
        return $this->paymentAccountService->getUserPaymentAccount($company->user);
    }

    private function payments(Company $company)
    {
        $account = $this->account($company);
        return PaymentDetail::find()
            ->innerJoinWith('paymentDetailOperation')
            ->andWhere(['>','payment_detail_operation.created_at',new Expression('DATE_SUB(now(), INTERVAL 12 MONTH)')])
            ->andWhere(['payment_detail.payment_account_id' => $account->id])
            ->andWhere('payment_detail.amount>0')
            ->sum('payment_detail.amount');
    }


}