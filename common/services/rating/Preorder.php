<?php


namespace common\services\rating;


use common\models\Company;
use common\modules\company\repositories\OrderRepository;
use Yii;

class Preorder implements Rating
{

    public function count(Company $company): Company
    {
        $orderRepository = Yii::createObject(OrderRepository::class);
        $preorderToOrder = $orderRepository->preorder($company);
        $rating = $preorderToOrder * 50;

        $company->psCatalog->addToLog([
            'preorder_rating' => $rating,
            'preorder_value' => $preorderToOrder,
        ]);

        $company->psCatalog->editRating(
            $company->psCatalog->rating + $rating
        );

        return $company;
    }
}