<?php
namespace common\services\rating;


use common\models\Company;
use Yii;

class Manager implements Rating
{
    /**
     * @var string[]
     */
    private array $ratingsClasses;

    public function __construct()
    {
        $this->ratingsClasses = [
            Communication::class,
            Order::class,
            Conversion::class,
            Earnings::class,
            Preorder::class,
            Registration::class,
            Profile::class,
            ResponseTime::class,
            Recivied::class
        ];
    }
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function count(Company $company): Company
    {
        $company->psCatalog->rating = 0;
        foreach ($this->ratingsClasses as $class) {
            /** @var Rating $rating */
            $rating = Yii::createObject($class);
            $company = $rating->count($company);
        }

        return $company;
    }
}