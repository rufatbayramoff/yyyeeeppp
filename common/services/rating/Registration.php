<?php


namespace common\services\rating;


use common\models\Company;
use DateTime;

class Registration implements Rating
{

    /**
     * @throws \Exception
     */
    public function count(Company $company): Company
    {
        $createdAt = new DateTime($company->created_at);
        $now = new DateTime("now");
        $interval = $now->diff($createdAt);
        $year = $interval->y;
        $rating = $year * 1000;

        $company->psCatalog->addToLog([
            'registration_rating' => $rating,
            'registration_value' => $year
        ]);

        $company->psCatalog->editRating(
            $company->psCatalog->rating + $rating
        );
        return $company;
    }
}