<?php


namespace common\services\rating;


use common\models\Company;
use common\models\CompanyService;
use common\modules\cnc\repositories\PsCncMachineRepository;
use common\modules\company\services\CompanyServicesService;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\repositories\ProductRepositoryCondition;
use frontend\models\ps\PsFacade;
use frontend\modules\mybusiness\services\progress\Progress;
use frontend\modules\mybusiness\services\progress\steps\About;
use frontend\modules\mybusiness\services\progress\steps\Phone;
use frontend\modules\mybusiness\services\progress\steps\Portfolio;
use frontend\modules\mybusiness\services\progress\steps\Service;
use frontend\modules\mybusiness\services\progress\steps\Taxes;

class Profile implements Rating
{

    public function __construct(
        private ProductRepository $productRepository,
        private PsCncMachineRepository $psCncMachineRepository,
        private CompanyServicesService $companyServicesService
    )
    {

    }


    public function count(Company $company): Company
    {
        $profileFill = $this->profileFill($company);
        $productCount = $this->productCount($company);
        $serviceCount = $this->serviceCount($company);

        $rating = $productCount + ($serviceCount * 50) + $profileFill;

        $company->psCatalog->addToLog([
            "profile_rating" => $rating,
            "profile_value" => $profileFill,
            "profile_product_count" => $productCount,
            "profile_service_count" => $serviceCount
        ]);

        $company->psCatalog->editRating(
            $company->psCatalog->rating + $rating
        );

        return $company;
    }


    /**
     * @param Company $company
     * @return int
     */
    private function profileFill(Company $company): int
    {
        $progress = new Progress([
            new About($company),
            new Service(),
            new Portfolio(),
            new Phone(),
            new Taxes()
        ]);
        $filledTabs = $progress->checkCount($company);

        return array_reduce($filledTabs,function($acc,bool $value){
            return $acc + (int)$value * 50;
        }, 0);
    }

    private function productCount(Company $company): int
    {
        $condition            = new ProductRepositoryCondition();
        $condition->companyId = $company->id;
        $condition->isActive  = 1;
        $condition->onlyOriginalModels = 1;

        return $this->productRepository->getProductsQuery($condition)->count();
    }

    private function serviceCount(Company $company): int
    {
        $sum = 0;
        $sum += count($this->psCncMachineRepository->getNotDeletedByPs($company));
        $sum += count(PsFacade::getNotDeletedPsPrintersByUserId($company->user_id));
        $sum += count($this->companyServicesService->getCompanyServicesForOwner($company,[CompanyService::TYPE_SERVICE, CompanyService::TYPE_WINDOW, CompanyService::TYPE_CUTTING]));
        return $sum;
    }
}