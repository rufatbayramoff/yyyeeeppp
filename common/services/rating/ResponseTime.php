<?php


namespace common\services\rating;


use common\models\Company;

class ResponseTime implements Rating
{

    public function count(Company $company): Company
    {
        $rating = 0;
        if($company->response_time < 48) {
            $rating = 50;
        }
        if($company->response_time < 24) {
            $rating = 100;
        }
        if($company->response_time < 12) {
            $rating = 500;
        }
        $company->psCatalog->addToLog([
            'response_time_rating' => $rating,
            'response_time_value' => $company->response_time
        ]);

        $company->psCatalog->editRating(
            $company->psCatalog->rating + $rating
        );

        return $company;
    }
}