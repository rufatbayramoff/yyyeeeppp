<?php
/**
 * Created by mitaichik
 */

namespace common\services;


use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\User;
use common\models\Model3d;
use common\models\UserCollection;
use common\models\UserCollectionModel3d;
use common\models\UserCollectionQuery;
use common\models\UserLike;
use common\models\UserStatistics;
use frontend\models\model3d\CollectionFacade;
use yii\base\Exception;

/**
 * Class LikeService
 * @package common\services
 */
class LikeService
{
    /**
     * user like
     *
     * @param string $objType model, img, review, comment
     * @param int $objId
     * @param User $user
     */
    public static function likeObject($objType, $objId, User $user)
    {
        if ($objType == UserLike::TYPE_MODEL3D) {
            $model = Model3d::tryFindByPk($objId);
            self::likeModel($model, $user);
            return;
        }

        $like = new UserLike();
        $like->user_id = $user->id;
        $like->created_at = dbexpr('NOW()');
        $like->object_type = $objType;
        $like->object_id = $objId;

        if (!$like->validate()){
            return;
        }

        AssertHelper::assertSave($like);
        UserStatistics::upLikes($objType, $objId);
    }

    /**
     * Like model
     *
     * @param Model3d $model
     * @param User $user
     * @throws Exception
     */
    public static function likeModel(Model3d $model, User $user)
    {
        if (self::isModelLikedByUser($model, $user)) {
            return;
        }

        if (!UserStatistics::isFavorite($model->user, $user)) {
            UserStatistics::to($model->user_id)->up('favorites')->safeSave();
        }

        AssertHelper::assert(CollectionFacade::bindToCollection($model->id, CollectionFacade::getUserSystemCollection(UserCollection::SYSTEM_TYPE_LIKES)->id));
    }

    /**
     *
     * @param string $objType
     * @param int $objId
     * @param User $user
     */
    public static function unlikeObject($objType, $objId, User $user)
    {

        if ($objType == UserLike::TYPE_MODEL3D) {
            $model = Model3d::tryFindByPk($objId);
            self::unlikeModel($model, $user);
            return;
        }

        $like = UserLike::findOne([
            'user_id' => $user->id,
            'object_type' => $objType,
            'object_id' => $objId
        ]);
        AssertHelper::assert($like->delete());

        UserStatistics::downLikes($objType, $objId);
    }

    /**
     * Unlike
     *
     * @param Model3d $model
     * @param User $user
     * @throws Exception
     */
    public static function unlikeModel(Model3d $model, User $user)
    {
        if (!self::isModelLikedByUser($model, $user)) {
            throw new Exception('Model not liked');
        }

        /** @var UserCollection[] $collections */
        $collections = UserCollection::find()
            ->where([
                'user_id' => $user->id,
                'system_type' => UserCollection::SYSTEM_TYPE_LIKES
            ])
            ->all();

        /**
         *
         * @var UserCollectionModel3d $rel
         */
        foreach ($collections as $collection) {
            $rel = UserCollectionModel3d::findOne([
                    'collection_id' => $collection->id,
                    'model3d_id' => $model->id
                ]);
            if ($rel) {
                AssertHelper::assert(CollectionFacade::unbindItemRelation($rel));
            }
        }
    }

    /**
     * get total object likes
     *
     * @param string $objectType
     * @param int $objectId
     * @return int
     */
    public static function getObjectLikesCount($objectType, $objectId)
    {
        $count = UserLike::find()->where([
            'object_type' => $objectType,
            'object_id' => $objectId
        ])->count();
        return $count;
    }

    /**
     *
     * @param Model3d $model3dObj
     * @return string
     * @throws \yii\db\Exception
     */
    public static function getModelLikesCount(Model3d $model3dObj)
    {
        $count = UserCollectionModel3d::find()
            ->joinWith(['collection' => function (UserCollectionQuery $query) {
                    $query->active();
                }
            ], false)
            ->where([UserCollectionModel3d::column('model3d_id') =>  $model3dObj->id])
            ->count('DISTINCT '.UserCollection::column('user_id'));

        return (int)$count;
    }

    /**
     * Return likes counts array (like [$modelId1 => $likesCount1, $modelId2 => $likesCount2]) for array of 3d models.
     *
     * @param Model3d[] $models
     * @return int[]
     */
    public static function getModelsLikesCount(array $models)
    {
        $likesData = UserCollectionModel3d::find()
            ->select(['model3d_id', 'COUNT( DISTINCT user_id) as likeCount'])
            ->joinWith(['collection' => function (UserCollectionQuery $query) {
                    $query->active();
                }
            ], false)
            ->andWhere(['model3d_id' => ArrayHelper::getColumn($models, 'id')])
            ->groupBy('model3d_id')
            ->asArray()
            ->all();

        $likesData = ArrayHelper::map($likesData, 'model3d_id', 'likeCount');

        $result = [];
        foreach ($models as $model) {
            $result[$model->id] = isset($likesData[$model->id]) ? (int)$likesData[$model->id] : 0;
        }

        return $result;
    }

    /**
     * Is liked model for user
     *
     * @param Model3d $model
     * @param User $user
     * @return bool
     */
    public static function isModelLikedByUser(Model3d $model, User $user)
    {
        $isLiekd = UserCollectionModel3d::find()
            ->joinWith(['collection' => function (UserCollectionQuery $query) use ($user) {
                    $query
                        ->active()
                        ->forUser($user);
                }
            ])
            ->andWhere(['model3d_id' => $model->id])
            ->exists();

        return $isLiekd;
    }

    /**
     * @param User $user
     * @param mixed $object
     * @return bool
     * @throws Exception
     */
    public static function isObjectLikedByUser($object, User $user)
    {
        return UserLike::find()
            ->where([
                'user_id' => $user->id,
                'object_id' => $object->id,
                'object_type' => UserLike::getObjectType($object)
            ])
            ->exists();
    }

    /**
     * Return like count string
     *
     * @param
     *            $count
     * @return string
     */
    public static function getLikesCountString($count)
    {
        if ($count == 0) {
            return _t('front.likes', 'Like it first');
        }

        return $count . ' ' . _t("site.unit", "{n, plural, =0{likes} =1{like} other{likes}}", [
            'n' => $count
        ]);
    }
}