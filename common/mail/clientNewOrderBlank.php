<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 24.08.18
 * Time: 9:56
 *
 * @var \common\models\StoreOrder $storeOrder
 */

use common\models\StorePricer;
use common\services\Model3dPartService;
use frontend\models\model3d\Model3dFacade;
use common\services\MeasureService;

$totalPrice = $storeOrder->getPrimaryInvoice()->getAmountTotal();

?>
<table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse; margin: 10px 0 10px;">
    <tbody>
    <?php if ($storeOrder->isForPreorder()): ?>
        <?php $preorder = $storeOrder->preorder; ?>

        <?php foreach ($preorder->preorderWorks as $work): ?>
            <tr>
                <td valign="top"
                    style="padding-top:10px; padding-right:10px; padding-bottom:10px; padding-left:0; word-break:break-word; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 100px;border-top:1px solid #e0e4e8;">
                    <?php if ($work->getCoverUrl()) { ?>
                        <img src="<?php echo $work->getCoverUrl() ?>" width="100" height="75" style="width: 100px; height: 75px;border: none;border-radius:5px;">
                    <?php } ?>
                </td>
                <td valign="top"
                    style="padding-top:10px; padding-right:10px; padding-bottom:10px; padding-left:10px; word-break:break-word; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 430px;border-top:1px solid #e0e4e8;">
                    <h4 style="line-height:20px; margin: 0 0 10px; padding:0; color:#404448; font-family:Helvetica; font-size:15px; font-weight: bold;">
                        <?php echo H($work->getTextTitle()) ?>
                    </h4>
                    <p style="line-height:20px; margin: 0 0 0; padding:0; color:#404448; font-family:Helvetica; font-size:13px; font-weight: normal; text-align: left;">
                            <span style="display: inline-block">
                                <span style="color: #707478"><?php echo _t('site.store', 'Quantity'); ?>:</span> <?php echo app('formatter')->asDecimal($work->qty); ?>,
                            </span>
                        <span style="display: inline-block">
                                <span style="color: #707478"><?php echo _t('site.store', 'Cost'); ?>:</span> <?php echo displayAsMoney($work->getCostWithFeeMoney()); ?>
                            </span>
                    </p>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <?php foreach ($storeOrder->storeOrderItems as $storeItem): ?>
            <?php if ($storeItem->hasProduct()): ?>
                <?php $product = $storeItem->product; ?>
                <tr>
                    <td valign="top"
                        style="padding-top:10px; padding-right:10px; padding-bottom:10px; padding-left:0; word-break:break-word; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 100px;border-top:1px solid #e0e4e8;">
                        <img src="<?php echo $product->getCoverUrl() ?>" width="100" height="75" style="width: 100px; height: 75px;border: none;border-radius:5px;">
                    </td>
                    <td valign="top"
                        style="padding-top:10px; padding-right:10px; padding-bottom:10px; padding-left:10px; word-break:break-word; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 430px;border-top:1px solid #e0e4e8;">
                        <h4 style="line-height:20px; margin: 0 0 10px; padding:0; color:#404448; font-family:Helvetica; font-size:15px; font-weight: bold;">
                            <?php echo H($product->title); ?>
                        </h4>
                    </td>
                </tr>
            <?php endif; ?>

            <?php if ($storeItem->hasModel()): ?>
                <?php
                $model3d = $storeItem->model3dReplica;
                $cover   = Model3dFacade::getCover($model3d);
                ?>
                <?php foreach ($model3d->getActiveModel3dParts() as $model3dPart): ?>
                    <?php
                    $texture = $model3dPart->getCalculatedTexture();
                    ?>
                    <tr>
                        <td valign="top"
                            style="padding-top:0; padding-right:10px; padding-bottom:0; padding-left:0; word-break:break-word; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 100px;">
                            <img src="<?php echo Model3dPartService::getModel3dPartRenderUrl($model3dPart); ?>" width="100" height="75"
                                 style="width: 100px; height: 75px;border: none;border-radius:5px;">
                        </td>
                        <td valign="top"
                            style="padding-top:0; padding-right:10px; padding-bottom:0; padding-left:10px; word-break:break-word; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 430px;">
                            <h4 style="line-height:20px; margin: 0 0 0; padding:0; color:#707478; font-family:Helvetica; font-size:14px; font-weight: bold;">
                                <?php echo $model3dPart->file->name; ?>
                            </h4>

                            <p style="line-height:20px; margin: 0 0 0; padding:0; color:#404448; font-family:Helvetica; font-size:13px; font-weight: normal; text-align: left;">
                                    <span style="display: inline-block">
                                        <span style="color: #707478"><?php echo _t('site.store', 'Size'); ?>:</span> <?php echo $model3dPart->getSize()->toStringWithMeasurement(); ?>,
                                    </span>
                                <span style="display: inline-block">
                                        <span style="color: #707478"><?php echo _t('site.store', 'Weight'); ?>:</span> <?php echo MeasureService::getWeightFormatted($model3dPart->getWeight()) ?>,
                                    </span>
                                <span style="display: inline-block">
                                        <span style="color: #707478"><?php echo _t('site.store', 'Quantity'); ?>:</span> <?php echo $model3dPart->qty; ?>
                                    </span>
                            </p>

                            <?php if ($texture && $texture->printerColor && $texture->printerMaterial): ?>
                                <p style="line-height:20px; margin: 0 0 15px; padding:0; color:#404448; font-family:Helvetica; font-size:13px; font-weight: normal; text-align: left;">
                                    <span style="color: #707478"><?php echo _t('site.store', 'Material'); ?>:</span>
                                    <?php echo H($texture->printerMaterial->title); ?>
                                    <span style="background-color: rgb(<?php echo($texture->printerColor->rgb); ?>); display:inline-block;margin: 0 5px 0 10px;height: 20px;width: 20px;line-height: 20px;border-radius: 30px;box-shadow: inset 0 0 0 1px rgba(0,0,0,0.12);vertical-align: middle"></span>
                                    <?php echo $texture->printerColor->render_color; ?>
                                </p>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($storeItem->hasCuttingPack()):?>
                <?php
                $cuttingPack = $storeItem->cuttingPack;
                ?>
                <?php foreach ($cuttingPack->getActiveCuttingParts() as $cuttingPart): ?>
                    <tr>
                        <td colspan="2" style="line-height:125%; margin: 10px 0; padding:0; color:#333333; font-family:Helvetica; font-size:15px">
                            <?php
                            echo $this->renderFile('@frontend/modules/workbench/views/service-orders/partial/order/item-cutting-file-part.php', ['cuttingPackPart' => $cuttingPart]);
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
    <?php if ($totalPrice): ?>
        <tfoot>
        <tr>
            <th valign="top" colspan="2"
                style="padding-top:10px; padding-right:10px; padding-bottom:5px; padding-left:0; word-break:break-word; color:#404448; font-family:Helvetica; font-size:16px; font-weight: bold; line-height:1%; text-align:right; width: 430px;border-top:1px solid #e0e4e8;">
                <p style="line-height:20px;margin: 0 0 0 0;padding:0;color: #404448;font-family:Helvetica;font-size: 15px;font-weight: bold;text-align: right;">
                    <?php echo _t('site.store', 'Total'); ?>
                    <span style="display:inline-block;min-width:100px;line-height:20px;margin: 0 0 0 0;padding:0;color: #404448;font-family:Helvetica;font-size: 15px;font-weight: bold;text-align: right;">
                            <?php echo displayAsMoney($totalPrice); ?>
                        </span>
                </p>
            </th>
        </tr>
        </tfoot>
    <?php endif; ?>
</table>
