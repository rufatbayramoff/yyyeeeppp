<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width"/>
</head>

<body>

<div style="height:100%; margin:0; padding:0; width:100%; background-color:#f7f7f7">
    <center>
        <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse; height:100%; margin:0; padding:0; width:100%; background-color:#f7f7f7">
            <tbody>
                <tr>
                    <td align="center" valign="top" style="height:100%; margin:0; padding:20px 10px; width:100%; border:0">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse; border:0; max-width:600px!important; border: 1px solid #e7e7e7;">
                            <tbody>
                                <tr>
                                  <!-- HEADER -->
                                    <td valign="top" style="background-color:#ffffff; border:0; padding:0px;">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:100%; border-collapse:collapse">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" style="padding:10px">
                                                        <table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="min-width:100%; border-collapse:collapse">
                                                            <tbody>
                                                                <tr>
                                                                    <td valign="top" style="padding: 0 10px 0 10px; text-align:left">
                                                                        <a href="https://www.treatstock.com/" target="_blank" title="Treatstock" class="" style="">
                                                                          <img src="https://static.treatstock.com/static/images/logo_2x.png" alt="" width="190" align="left" style="max-width:380px; padding-bottom:0; display:inline!important; vertical-align:bottom; border:0; height:auto; outline:none; text-decoration:none">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>

                                                    <td valign="top" style="padding:10px">
                                                        <table cellpadding="0" cellspacing="0" width="100%" align="right" border="0" style="min-width:100%; border-collapse:collapse">
                                                            <tbody>
                                                                <tr>
                                                                    <td valign="top" style="padding: 0 10px 0 10px; text-align:right">
                                                                      <p style="line-height:125%; margin:5px 0 0; padding:0; color:#333333; font-family:Helvetica; font-size:14px">
                                                                        <?php if(false): ?><a href="TODO" target="_blank" title="Treatstock" class="" style="color:#2d8ee0; underline: none;">
                                                                          <strong>username@gmail.com</strong>
                                                                        </a> <?php endif; ?>
                                                                      </p>
                                                                      <p style="line-height:125%; margin:0; padding:0; color:#999999; font-family:Helvetica; font-size:14px">
                                                                        <strong><?php echo date("m/d/Y"); ?></strong>
                                                                      </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                  <!-- BODY -->
                                    <td valign="top" style="background-color:#ffffff; border:0; padding:10px 0; border: 1px solid #e7e7e7;">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:100%; border-collapse:collapse">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" style="padding:0">
                                                      <table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse">
                                                        <tbody>
                                                          <tr>
                                                            <td valign="top" style="padding-top:10px; padding-right:19px; padding-bottom:10px; padding-left:19px; word-break:break-word; color:#333333; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left">
                                                              <h1 style="line-height:125%; margin: 0 0 20px; padding:0; color:#333333; font-family:Helvetica; font-size:30px; font-weight: normal;">
                                                                Main heading H1. Start letter with H1!
                                                              </h1>
                                                              <h2 style="line-height:125%; margin: 20px 0 10px; padding:0; color:#333333; font-family:Helvetica; font-size:24px; font-weight: normal;">
                                                                Heading level 2 H2
                                                              </h2>
                                                              <h3 style="line-height:125%; margin: 20px 0 10px; padding:0; color:#333333; font-family:Helvetica; font-size:18px; font-weight: normal;">
                                                                Heading level 3 H3
                                                              </h3>
                                                              <p style="line-height:125%; margin: 10px 0; padding:0; color:#333333; font-family:Helvetica; font-size:15px">
                                                                Simple paragraph text block. More text to the text block test. Текст на русском языке.
                                                              </p>
                                                              <a href="TODO" target="_blank" style="display: inline-block; padding:10px 20px; font-family:Helvetica; font-size:15px; font-weight:bold; color:#ffffff; line-height:20px; text-align: center; text-decoration:none; border-radius: 5px; width:auto; background-color: #e00457;">
                                                                Button text
                                                              </a>
                                                              <a href="TODO" target="_blank" style="display: inline-block; padding:10px 20px; font-family:Helvetica; font-size:15px; font-weight:bold; color:#ffffff; line-height:20px; text-align: center; text-decoration:none; border-radius: 5px; width:auto; background-color: #2d8ee0;">
                                                                Button 2 text
                                                              </a>
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                  <!-- FOOTER -->
                                    <td valign="top" style="background-color:#fafafa; border-top: 1px solid #e7e7e7; padding-top:9px; padding-bottom:10px">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:100%; border-collapse:collapse">
                                            <tbody class="x_mcnTextBlockOuter">
                                                <tr>
                                                    <td valign="top" style="padding-top:9px">
                                                        <table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse">
                                                            <tbody>
                                                                <tr>
                                                                    <td valign="top" style="padding-top:0; padding-right:19px; padding-bottom:10px; padding-left:19px; word-break:break-word; color:#999999; font-family:Helvetica; font-size:12px; line-height:150%; text-align:center">
                                                                        You are receiving this email because you subscribed at www.treatstock.com. As a subscribed customer, you will also recieve special offers from Treatstock.
                                                                        <br>
                                                                        <br>
                                                                        <table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse; text-align: left;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" style="padding:0 10px 0 0;">
                                                                                        <a href="https://www.treatstock.com/site/about" target="_blank" style="display: inline-block; line-height:125%; margin: 10px 0; padding:0; color:#999999; font-family:Helvetica; font-size:12px;">
                                                                                          About Us
                                                                                        </a>
                                                                                    </td>
                                                                                    <td valign="top" style="padding:0 5px;">
                                                                                        <a href="https://www.treatstock.com/site/policy" target="_blank" style="display: inline-block; line-height:125%; margin: 10px 0; padding:0; color:#999999; font-family:Helvetica; font-size:12px;">
                                                                                          Privacy Policy
                                                                                        </a>
                                                                                    </td>
                                                                                    <td valign="top" style="padding:0 0 0 10px;">
                                                                                        <a href="https://www.treatstock.com/site/terms" target="_blank" style="display: inline-block; line-height:125%; margin: 10px 0; padding:0; color:#999999; font-family:Helvetica; font-size:12px;">
                                                                                          Terms of Use
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" style="padding:0 10px 0 0;">
                                                                                        <a href="https://www.treatstock.com/site/contact" target="_blank" style="display: inline-block; line-height:125%; margin: 10px 0; padding:0; color:#999999; font-family:Helvetica; font-size:12px;">
                                                                                          Contact Us
                                                                                        </a>
                                                                                    </td>
                                                                                    <td valign="top" style="padding:0 5px;">
                                                                                        <a href="https://www.treatstock.com/site/return-policy" target="_blank" style="display: inline-block; line-height:125%; margin: 10px 0; padding:0; color:#999999; font-family:Helvetica; font-size:12px;">
                                                                                          Return Policy
                                                                                        </a>
                                                                                    </td>
                                                                                    <td valign="top" style="padding:0 0 0 10px;">
                                                                                        <a href="https://www.treatstock.com/help" target="_blank" style="display: inline-block; line-height:125%; margin: 10px 0; padding:0; color:#999999; font-family:Helvetica; font-size:12px;">
                                                                                          Help Center
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>

                                                                        <table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse;margin-top:9px;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" style="padding:15px 0 0; text-align: left; color: #999999; border-top: 1px solid #e7e7e7;">
                                                                                      Treatstock © 2016 — Smart e-commerce & manufacturing platform
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </center>
  </div>

</body>
</html>
