<?php
/** @var \common\models\PsPrinter $psPrinter */
/** @var File $reviewModerationImage */

use common\models\File;
use common\models\Ps;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;

/** @var \common\models\StoreOrder $order */
$machine =  $order->currentAttemp->machine;

$model3d = $order->orderItem->hasModel() ? $order->orderItem->model3dReplica : null;

if ($reviewModerationImage) {
    $cover = ImageHtmlHelper::getThumbUrl($reviewModerationImage->getFileUrl());
} else {
    $cover = $model3d ? Model3dFacade::getCover($model3d)['image'] : null;
}

$user = $order->user->getFullNameOrUsername();
?>

    <div style="margin: 10px 0 20px; line-height:20px; color:#404448; font-family:Helvetica, sans-serif; font-size:15px">
            Hi <?=$user;?>,
    </div>
    <div style="margin: 10px 0 20px; line-height:20px; color:#404448; font-family:Helvetica, sans-serif; font-size:15px">
        Show your appreciation for the manufacturer by leaving a 5-star review for the order.
    </div>

<table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse; margin-bottom: 20px;">
    <tbody>
    <tr>
        <?php if($model3d) : ?>

            <td valign="top" style="padding-top:0; padding-right:10px; padding-bottom:0; padding-left:0; color:#404448; font-family:Helvetica, sans-serif; font-size:16px; line-height:1%; text-align:left; width: 120px;">
                <img src="<?=$cover;?>" width="120" height="90" style="width: 120px; height: 90px;border: none;border-radius:5px;">
            </td>
            <td valign="top" style="padding-top:0; padding-right:10px; padding-bottom:0; padding-left:10px; color:#404448; font-family:Helvetica, sans-serif; font-size:16px; line-height:1%; text-align:left; width: 310px;">
                <h3 style="line-height:20px; margin: 5px 0; padding:0; color:#404448; font-family:Helvetica, sans-serif; font-size:15px; font-weight: bold;">
                    <?=$model3d->title;?>
                </h3>
                <?php if($order->isThingiverseOrder()): ?>
                <div style="line-height:20px; margin: 10px 0 0; padding:0; color:#404448; font-family:Helvetica, sans-serif; font-size:15px;">
                    Thingiverse order: #<?=$order->thingiverseOrder->thingiverse_order_id;?>
                </div>
                <?php endif; ?>
                <div style="line-height:20px; margin: 0 0 10px; padding:0; color:#707478; font-family:Helvetica, sans-serif; font-size:12px">
                    Placed on: <?=date("m/d/Y", strtotime($order->created_at));?>
                </div>
            </td>

        <?php endif; ?>

        <td valign="top" style="padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 90px;white-space: nowrap;">
            <div style="line-height:20px; margin: 5px 0 5px; padding:0; color:#404448; font-family:Helvetica, sans-serif; font-size:15px; font-weight: bold; text-align: right;white-space: nowrap;">
                <?=displayAsCurrency($order->getTotalPrice(), $order->getCurrency());?>
            </div>
        </td>
    </tr>
    </tbody>
</table>



<table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse; margin-bottom: 20px; background-color: #f0f4f8; border-radius: 5px;">
    <tbody>
    <tr>
        <td valign="top" style="padding-top:15px; padding-right:10px; padding-bottom:15px; padding-left:20px; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 180px;">
    <span style="line-height:20px; margin: 0 0 0 0; padding: 0 0 0 0; color:#404448; font-family:Helvetica, sans-serif; font-size:14px">
        Rate manufacturer:
    </span>
        </td>
        <td valign="top" style="padding-top:10px; padding-right:10px; padding-bottom:10px; padding-left:0; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 340px;">
            <style>
                .rating-star:hover {
                    opacity: 0.6;
                }
            </style>
            <?php if(isset($stars)): ?>
                <span style="color:orange;letter-spacing: 5px;font-size:23px;">
        <?php for($i=0;$i<$stars;$i++): ?>
            <span class="tsi tsi-rating-star"> </span>
        <?php endfor; ?>
                    <?php for($i=0;$i<5-$stars;$i++): ?>
                        <span style="color:#ccc;letter-spacing: 1px;" class="tsi tsi-rating-star"> </span>
                    <?php endfor;?>
        </span>

            <?php else: ?>

                <?php for($i=0;$i<5;$i++):?>
                    <a href="<?=$link;?>&stars=<?=$i+1;?>" class="rating-star" target="_blank" style="display: inline-block; width: 30px; height: 30px; margin-right: 10px; text-decoration: none;" title="Rate <?=$i+1;?> Star">
                        <img src="https://static.treatstock.com/static/images/emails/rating-star.png" alt="" width="30" height="30" style="width: 30px; height: 30px;">
                    </a>
                <?php endfor; ?>
            <?php endif; ?>
        </td>
    </tr>
    </tbody>
</table>


<hr style="margin: 20px 0 30px 0; border-top: 1px solid #e0e4e8; border-bottom: none;">

<?php if ($machine): ?>
<table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" style="max-width:100%; min-width:100%; border-collapse:collapse; margin-bottom: 20px;">
    <tbody>
    <tr>
        <td valign="top" style="padding-top:0; padding-right:10px; padding-bottom:0; padding-left:0; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 60px;">
            <img src="<?= Ps::getCircleImage($order->currentAttemp->ps);?>" width="60" height="60" style="width: 60px; height: 60px;border: none;border-radius:5px;">
        </td>
        <td valign="top" style="padding-top:0; padding-right:10px; padding-bottom:0; padding-left:10px; color:#404448; font-family:Helvetica; font-size:16px; line-height:1%; text-align:left; width: 470px;">
            <h3 style="line-height:20px; margin: 5px 0 5px 0; padding:0; color:#404448; font-family:Helvetica, sans-serif; font-size:15px; font-weight: bold;">
                <?=H($order->currentAttemp->ps->title);?>
            </h3>

            <div style="line-height:20px; margin: 0; padding:0; color:#707478; font-family:Helvetica, sans-serif; font-size:15px">
                <?php
                if ($machine->isCnc()) {
                    echo 'Machine';
                } elseif ($machine->isPrinter()) {
                    echo 'Printer';
                };?>: <?=H($machine->getTitleLabel());?>
            </div>
        </td>
    </tr>
    </tbody>
</table>
<?php endif; ?>

<?php if($order->isThingiverseOrder()): ?>
<p style="line-height:20px; margin: 20px 0; padding:0; color:#2d8ee0; font-family:Helvetica; font-size:14px">
    <a href="https://www.thingiverse.com/apps/3d-print-with-treatstock/comments" target="_blank" style="color: #2d8ee0; text-decoration: underline;">Leave a review for our Thingiverse app</a>
</p>
<?php endif; ?>