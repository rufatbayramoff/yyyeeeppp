<?php

use lib\money\Currency;

function _t($cat = 'app', $msg = '', $params = [])
{
    return \Yii::$app->getModule('translation')->i18n->translate($cat, $msg, $params);
}

if (!function_exists('H')) {
    function H($string)
    {
        return \yii\helpers\Html::encode(trim($string));
    }
}

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($string)
    {
        $firstChar = mb_substr($string, 0, 1);
        $then      = mb_substr($string, 1, null);
        return mb_strtoupper($firstChar) . $then;
    }
}

if (!function_exists('HL')) {
    function HL($string)
    {
        return htmlentities(str_replace(' ', '+', $string));
    }
}

function dd($d, $die = true)
{
    \yii\helpers\VarDumper::dump($d, 40, true);
    if ($die) {
        die();
    }

}

function pre($d)
{
    \yii\helpers\VarDumper::dump($d, 20, true);
}

/**
 *
 * @param string $component
 * @return \yii\web\Application|yii\web\Request|\yii\web\User|yii\db\Connection|\common\models\SystemSetting|\yii\i18n\Formatter|\yii\caching\Cache|\yii\web\Response|\yii\web\UrlManager
 */
function app($component = null)
{
    $app = \Yii::$app;
    if ($component) {
        $app = $app->$component;
    }
    return $app;
}

function is_guest()
{
    if (app('user')) {
        return app('user')->isGuest;
    }
    return true;
}

function dbexpr($expr)
{
    return new \yii\db\Expression($expr);
}

/**
 * get param
 *
 * @param string $param
 * @param mixed $default
 * @return mixed
 */
function param($param, $default = false)
{
    return isset(app()->params[$param]) ? app()->params[$param] : $default;
}

/**
 * nicely log exception information
 *
 * @param \Exception $e
 * @param string $category
 */
function logException(\Exception $e, $category)
{
    \Yii::error($e, $category);
}

/**
 * debug formated sql
 *
 * @param yii\db\Query $query
 */
function debugSql(yii\db\Query $query, $console = false)
{
    $sql = debugSqlString($query, $console);
    echo($sql);
}

function debugSqlString($query, $console)
{
    $sql        = $query->createCommand()->getRawSql();
    $keywords   = ['FROM', 'LEFT JOIN', 'AND (', 'WHERE'];
    $keywordsBr = array_map(
        function ($val) use ($console) {
            return $console ? "\n" . $val : "\n<BR />" . $val;
        },
        $keywords
    );

    $sql = str_replace($keywords, $keywordsBr, $sql);
    return $sql;
}

/**
 * due to wrong rounding, we prepare value amount to .00 with half up rounding
 * for example: 0.125 will be converted to 0.13
 *
 *
 * @param float $value
 * @param string $currency
 * @param array $options
 * @param array $textOptions
 * @return string
 */
function displayAsCurrency($value, $currency = null, $options = [], $textOptions = [])
{
    if (empty($currency)) {
        $currency = Currency::USD;
    }
    $valueRound = round($value, 2, PHP_ROUND_HALF_UP);
    return app('formatter')->asCurrency($valueRound, $currency, $options, $textOptions);
}

/**
 * @param \lib\money\Money $money
 * @param array $options
 * @param array $textOptions
 * @return string
 */
function displayAsMoney(\lib\money\Money $money, $options = [], $textOptions = [])
{
    $currency = $money->getCurrency();
    if ($currency === Currency::BNS) {
        return 'Bonus ' . round($money->getAmount(), 2, PHP_ROUND_HALF_UP);
    }
    return app('formatter')->asCurrency(
        round($money->getAmount(), 2, PHP_ROUND_HALF_UP),
        $currency,
        $options,
        $textOptions
    );

}

/**
 * this function is used to convert rgb to hex
 * for example [255,255,255] => ffffff
 *
 * @param array $rgb
 * @return string
 */
function rgb2hex($rgb)
{
    return (substr("000000" . dechex((((int)$rgb[0]) << 16) | (((int)$rgb[1]) << 8) | ((int)$rgb[2])), -6));
    /*
    $hex = "";
    $hex .= str_pad(dechex($rgb[0]), 2, "1", STR_PAD_LEFT);
    $hex .= str_pad(dechex($rgb[1]), 2, "d", STR_PAD_LEFT);
    $hex .= str_pad(dechex($rgb[2]), 2, "f", STR_PAD_LEFT);
    return $hex;
     * 
     */
}

function utf8ize($mixed)
{
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } else {
        if (is_string($mixed)) {
            return utf8_encode($mixed);
        }
    }
    return $mixed;
}

function mainSiteUrl()
{
    return param('httpScheme') . \Yii::$app->getModule('intlDomains')->mainDomain;
}


function glob_recursive($pattern, $flags = 0)
{
    $files = glob($pattern, $flags);
    foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
        $files = array_merge($files, glob_recursive($dir . '/' . basename($pattern), $flags));
    }
    return $files;
}

function generateUid($len = 6)
{
    $returnValue = '';
    $map         = array_merge(range(48, 57), range(65, 90), range(97, 122));
    for ($i = 0; $i < $len; $i++) {
        $rand        = random_int(0, count($map) - 1);
        $returnValue .= chr($map[$rand]);
    };
    return $returnValue;
}

function _strip_utf8mb4($textData, $replaceWith = '')
{
    $replaced = [];
    // Strip overly long 2 byte sequences, as well as characters
    //  above U+10000 and replace with $replace_text
    $result = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]' .
        '|[\x00-\x7F][\x80-\xBF]+' .
        '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*' .
        '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})' .
        '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
        $replaceWith, $textData, -1, $replaced[]);

    // Strip overly long 3 byte sequences and UTF-16 surrogates and replace with $replace_text
    $result = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]' .
        '|\xED[\xA0-\xBF][\x80-\xBF]/S', $replaceWith, $result, -1, $replaced[]);
    return $result;
}

