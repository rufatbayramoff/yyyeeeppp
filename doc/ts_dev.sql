-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 03, 2015 at 04:10 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ts_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_access_1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `group_id`, `code`, `title`, `description`) VALUES
(1, 1, 'user.email.change', 'User can change email', ''),
(2, 1, 'user.profile.update', 'User can update own profile', ''),
(3, 1, 'user.comment.post', 'User can post comment', '');

-- --------------------------------------------------------

--
-- Table structure for table `access_group`
--

CREATE TABLE IF NOT EXISTS `access_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `access_group`
--

INSERT INTO `access_group` (`id`, `title`, `description`, `is_active`) VALUES
(1, 'core', 'Core accesses', b'0'),
(4, 'asdf', 'asdf', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `path` varchar(45) NOT NULL,
  `extension` char(5) NOT NULL,
  `size` int(11) DEFAULT NULL COMMENT 'bytes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `server` set('localhost','static1','static2') NOT NULL DEFAULT 'localhost',
  `status` enum('new','active','inactive','danger') NOT NULL DEFAULT 'new',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_city`
--

CREATE TABLE IF NOT EXISTS `geo_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `timezone_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gcity_1_idx` (`country_id`),
  KEY `fk_geo_city_1_idx` (`region_id`),
  KEY `fk_geo_city_2_idx` (`timezone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_country`
--

CREATE TABLE IF NOT EXISTS `geo_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `iso_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iso_code_UNIQUE` (`iso_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_location`
--

CREATE TABLE IF NOT EXISTS `geo_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `lat` decimal(8,5) DEFAULT NULL,
  `lon` decimal(8,5) DEFAULT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lat_UNIQUE` (`lat`,`lon`),
  KEY `fk_gl1_idx` (`country_id`),
  KEY `fk_gl2_idx` (`city_id`),
  KEY `fk_geo_location_1_idx` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_region`
--

CREATE TABLE IF NOT EXISTS `geo_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`,`country_id`),
  KEY `fk_geo_region_1_idx` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_timezone`
--

CREATE TABLE IF NOT EXISTS `geo_timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(254) DEFAULT NULL,
  `utc_offset` int(11) DEFAULT NULL,
  `daylight` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1432634636),
('m130524_201442_init', 1432634637),
('m140506_102106_rbac_init', 1432639694);

-- --------------------------------------------------------

--
-- Table structure for table `model3d`
--

CREATE TABLE IF NOT EXISTS `model3d` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` varchar(245) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `is_moderated` bit(1) NOT NULL DEFAULT b'0',
  `is_printer_ready` bit(1) NOT NULL DEFAULT b'0' COMMENT 'G code ready',
  `license_id` int(11) NOT NULL,
  `dimensions` varchar(45) DEFAULT NULL COMMENT 'width x height x length',
  `status` enum('new','active','moderation','published','inactive') NOT NULL DEFAULT 'new',
  `files_count` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_model3d_1_idx` (`user_id`),
  KEY `fk_model3d_2_idx` (`license_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `model3d_file`
--

CREATE TABLE IF NOT EXISTS `model3d_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_id` int(11) NOT NULL,
  `format` char(5) NOT NULL DEFAULT 'stpl',
  `name` varchar(45) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `antivirus_checked_at` timestamp NULL DEFAULT NULL,
  `moderator_status` enum('new','checking','ok','banned','review') NOT NULL DEFAULT 'new',
  `moderated_at` timestamp NULL DEFAULT NULL,
  `user_status` enum('active','inactive','published') NOT NULL DEFAULT 'inactive',
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model3d_files_1_idx` (`model3d_id`),
  KEY `fk_model3d_file_1_idx` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `model3d_img`
--

CREATE TABLE IF NOT EXISTS `model3d_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model3d_id` int(11) NOT NULL,
  `size` int(11) DEFAULT NULL COMMENT 'File size (KB)',
  `title` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `moderated_at` timestamp NULL DEFAULT NULL,
  `is_moderated` bit(1) NOT NULL DEFAULT b'0',
  `basename` varchar(45) NOT NULL COMMENT 'File name ',
  `extension` char(5) NOT NULL DEFAULT 'png',
  `user_id` int(11) NOT NULL COMMENT 'User ID who owns file',
  `type` set('owner','review','user') NOT NULL,
  `review_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model3d_files_1_idx` (`model3d_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Images for 3D model' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '-p2-w4krRccLE4vbFvaureQzQ9zDJnvn', '$2y$13$XkZs6Fg/ruZ7nofqpr7KM.JDEwTutGwRxKB5R/PPWgMddpX5AB4bS', NULL, 'admin@local.dev', 10, 1432634668, 1432634668),
(2, 'user', '_M87jQ9cTTip7T7VARCIDlGq1Ktru2Bj', '$2y$13$qpyHhMLI4ocrXF43NJxnkeF6nM.L/btRFSTokwYpCHsB/UPG4D6Hu', 'X9RWtQAgglYR8ttjxVeU2Oxmqq6kTE2k_1432646488', 'user@mail.dev', 10, 1432644301, 1432646488),
(3, 'user2', 'THOgNnK7kK3kPrCUJboIHrmyTcm299At', '$2y$13$7PH1ZJqgvofqBZ0NtEEScuTZlRezzyYPT.eq.wcT.iecCdoYFvjv.', NULL, 'user2@mail.dev', 10, 1432646107, 1432649905);

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_rights_1_idx` (`user_id`),
  KEY `index3` (`access_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`id`, `user_id`, `access_id`, `created_at`) VALUES
(4, 1, 1, NULL),
(5, 1, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_admin`
--

CREATE TABLE IF NOT EXISTS `user_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_admin`
--

INSERT INTO `user_admin` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '-p2-w4krRccLE4vbFvaureQzQ9zDJnvn', '$2y$13$XkZs6Fg/ruZ7nofqpr7KM.JDEwTutGwRxKB5R/PPWgMddpX5AB4bS', NULL, 'admin@local.dev', 10, 1432634668, 1432634668);

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
  `user_id` int(11) NOT NULL,
  `dob_date` date DEFAULT NULL COMMENT 'Date of birth',
  `location_id` int(11) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT '',
  `phone_confirmed` bit(1) DEFAULT b'0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `current_lang` varchar(5) NOT NULL,
  `current_currency_iso` varchar(45) NOT NULL DEFAULT 'usd',
  `current_metrics` enum('sm','in') NOT NULL DEFAULT 'sm',
  `timezone_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `is_printservice` bit(1) DEFAULT b'0',
  `is_modelshop` bit(1) DEFAULT b'0',
  `trust_level` enum('untrusted','trusted','verified') NOT NULL COMMENT 'Trust level',
  `avatar_url` varchar(255) DEFAULT NULL,
  `background_url` varchar(255) DEFAULT NULL,
  `gender` enum('male','female','none','multi') DEFAULT NULL,
  `printservice_title` varchar(255) DEFAULT NULL,
  `modelshop_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `index4` (`user_id`),
  KEY `fk_user_profile_1_idx` (`user_id`),
  KEY `fk_user_profile_2_idx` (`timezone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`user_id`, `dob_date`, `location_id`, `address`, `website`, `phone`, `phone_confirmed`, `updated_at`, `current_lang`, `current_currency_iso`, `current_metrics`, `timezone_id`, `firstname`, `lastname`, `is_printservice`, `is_modelshop`, `trust_level`, `avatar_url`, `background_url`, `gender`, `printservice_title`, `modelshop_title`) VALUES
(1, NULL, NULL, '', '', '1343', b'1', NULL, '', '', '', 0, '', '', b'1', b'1', '', '', '', '', '', ''),
(3, '1983-09-19', 1, '', 'ya.ru', '', b'0', '0000-00-00 00:00:00', 'en-US', 'usd', 'sm', 1, 'John', 'Smith', b'1', b'1', 'untrusted', '', '', 'female', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `access`
--
ALTER TABLE `access`
  ADD CONSTRAINT `fk_access_1` FOREIGN KEY (`group_id`) REFERENCES `access_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `geo_city`
--
ALTER TABLE `geo_city`
  ADD CONSTRAINT `fk_gcity_1` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_geo_city_1` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_geo_city_2` FOREIGN KEY (`timezone_id`) REFERENCES `geo_timezone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `geo_location`
--
ALTER TABLE `geo_location`
  ADD CONSTRAINT `fk_geo_location_1` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gl1` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gl2` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `geo_region`
--
ALTER TABLE `geo_region`
  ADD CONSTRAINT `fk_geo_region_1` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `model3d`
--
ALTER TABLE `model3d`
  ADD CONSTRAINT `fk_model3d_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `model3d_file`
--
ALTER TABLE `model3d_file`
  ADD CONSTRAINT `fk_model3d_files_1` FOREIGN KEY (`model3d_id`) REFERENCES `model3d` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_model3d_file_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_access`
--
ALTER TABLE `user_access`
  ADD CONSTRAINT `fk_user_access_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_access_ibfk_1` FOREIGN KEY (`access_id`) REFERENCES `access` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `fk_user_profile_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
