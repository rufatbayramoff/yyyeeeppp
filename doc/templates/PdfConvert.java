import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Utilities;
import com.itextpdf.text.Font.FontFamily;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.AcroFields.*;


/**
 * javac -cp '.:itextpdf-5.5.7.jar' PdfConvert.java
 * java -cp '.:itextpdf-5.5.7.jar' PdfConvert <file> <fileFilled>
 * 
 * after use 
 *  php yii pdf/generate-html-file <fileFilled>
 * to generate html
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
public class PdfConvert {
    public static void main(String[] args) {
        
        try {
            if(args.length==0){
                args[0] = "tax/fw9.pdf";
                args[1] = "tax/fw9_filled.pdf";
            }
            String fileName = args[0];
            PdfReader reader = new PdfReader(fileName);
            PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(args[1]));
            AcroFields form = stamper.getAcroFields();
            int n = 0;
            for (Iterator i = form.getFields().keySet().iterator(); i.hasNext(); ) {
                String key = (String) i.next();
                String value = "";
               
                switch(form.getFieldType(key)) {
                    case AcroFields.FIELD_TYPE_CHECKBOX:
                        value = "Yes";  
                        String[] checkboxstates = form.getAppearanceStates(key);
                        System.out.print("FIELD_TYPE_CHECKBOX: "); 
                        for(int i2=0;i2<checkboxstates.length;i2++){
                            System.out.print(checkboxstates[i2] + ", ");
                        }
                        form.setField(key, checkboxstates[0]);   
                        break;
                    case AcroFields.FIELD_TYPE_COMBO:
                        System.out.print("FIELD_TYPE_COMBO: "); 
                        break;
                    case AcroFields.FIELD_TYPE_LIST:
                        System.out.print("FIELD_TYPE_LIST: "); 
                        value = "List";                        
                        break;
                    case AcroFields.FIELD_TYPE_NONE:
                        System.out.print("FIELD_TYPE_NONE: "); 
                        value = "None";
                        break;
                    case AcroFields.FIELD_TYPE_PUSHBUTTON:
                        value = "Pushbutton";
                        break;
                    case AcroFields.FIELD_TYPE_RADIOBUTTON:
                        System.out.print("FIELD_TYPE_RADIOBUTTON: "); 
                        value = "Radiobutton";
                        String[] checkboxstates2 = form.getAppearanceStates(key); 
                        System.out.println(checkboxstates2);
                        break;
                    case AcroFields.FIELD_TYPE_SIGNATURE:
                        System.out.print("FIELD_TYPE_SIGNATURE: "); 
                        value = "Signature";
                        break;
                    case AcroFields.FIELD_TYPE_TEXT:
                        System.out.print("FIELD_TYPE_TEXT: "); 
                        value = "Text";
                        break;
                    default:
                        System.out.println("?");
                }
                System.out.println(key + " : ");
                if(value!="Yes"){
                    value = "{IN_" + n + "}";
                    form.setField(key, value);
                }
                n++;
            } 
            stamper.close();       
        } catch (Exception e) {
            System.out.println(e);
        }
    } 
}
