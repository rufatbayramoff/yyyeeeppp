<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 12.06.19
 * Time: 13:37
 */

//    cat ./access.log  | grep "'200','5014'" | grep -v -i 'googlebot' > banIpLog.log
//    php ./banIp.php > banIpRes.log

$fileName = 'banIpLog.log';
$minLen = 15;
$requestCount = 50;
$extInfo = 0;

$content = file_get_contents($fileName);
$lines = explode("\n", $content);
$ips = [];
foreach ($lines as $line) {
    $lineArray = explode("','", $line);
    $ip = $lineArray[0];
    $ip = str_replace("'", '', $ip);
    if (!array_key_exists(6, $lineArray)) {
        continue;
    }
    $code = $lineArray[5];
    $size = $lineArray[6];
    $dateSec = strtotime($lineArray[2]);
    $date = date('Y-m-d H:i:s', $dateSec);
    if ($size != 5014) {
        continue;
    }
    if (!array_key_exists($ip, $ips)) {
        $ips[$ip] = ['start' => $dateSec, 'end' => $dateSec, 'count' => 0];
    }
    $ips[$ip]['start'] = min($dateSec, $ips[$ip]['start']);
    $ips[$ip]['end'] = max($dateSec, $ips[$ip]['end']);
    $ips[$ip]['count'] = $ips[$ip]['count'] + 1;
}
//print_r($ips);
$filteredIps = [];
foreach ($ips as $ip => $ipInfo) {
    $timeLen = $ipInfo['end'] - $ipInfo['start'];
    $count = $ipInfo['count'];
    if (($timeLen > $minLen) && ($count > $requestCount)) {  // 15 Min
        if ($extInfo) {
            echo $ip . "\t start: " . date('Y-m-d H:i:s', $ipInfo['start']) . "\t end: " . date('Y-m-d H:i:s', $ipInfo['end']) . "\t count: " . $count . "\n";
        } else {
            echo $ip . "\n";
        }
    }
}
