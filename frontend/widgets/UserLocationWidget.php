<?php namespace frontend\widgets;

use common\modules\catalogPs\models\CatalogSearchForm;
use frontend\components\UserSessionFacade;
use lib\geo\models\Location;


/**
 * UserLocationWidget - user location
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class UserLocationWidget extends \yii\base\Widget
{

    /**
     * function to replace UpdateLocation
     *
     * @var string
     */
    public $jsCallback;
    public $jsCallbackRedir;

    /**
     * @var Location
     */
    public $location;

    public $btnTitle = 'Shipping Address';

    /**
     *  %city%, %region%, %country%
     * @var string
     */
    public $locationFormat = '%city%, %country%';

    public $dataUrl = '/geo/change-location'; // or /geo/change-location-redir

    public function run()
    { 
        $location = $this->location ? (array)$this->location : (array)UserSessionFacade::getLocation();
        $filterLocation = ['city','region','country'];
        $location = array_filter($location, function($key) use($filterLocation){ return in_array($key, $filterLocation);}, ARRAY_FILTER_USE_KEY);
        $dataTpl = array_combine(array_map(function($k) {
                return '%' . $k . '%';
            }, array_keys($location))
            , $location
        );
        $locationText = str_replace(array_keys($dataTpl), $dataTpl, $this->locationFormat);
        if(!empty($location['city']) && $location['city']==CatalogSearchForm::LOCATION_EMPTY){
            $locationText = _t('site.catalog', 'All Locations');
        }
        if($this->jsCallback){
            $this->view->registerJs('TS.UpdateLocation = ' . $this->jsCallback);
        }
        if($this->jsCallbackRedir){
            $this->view->registerJs('TS.RedirLocation = ' . $this->jsCallbackRedir);
            $this->dataUrl = '/geo/change-location-redir';
        }
        return $this->render('user/location', [
            'dataUrl'  => $this->dataUrl,
            'location' => $locationText,
            'btnTitle' => $this->btnTitle
        ]);
    }
}
