<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.11.16
 * Time: 10:42
 */

namespace frontend\widgets;

use common\components\exceptions\AssertHelper;
use common\components\JsObjectFactory;
use common\models\StoreUnitShoppingCandidate;
use common\services\Model3dService;
use DateTime;
use DateTimeZone;
use frontend\models\user\UserFacade;
use frontend\widgets\assets\ShoppingCandidateAssets;
use Yii;
use yii\base\Widget;
use yii\web\GoneHttpException;

class ShoppingCandidateWidget extends Widget
{
    const HIDDEN_MODE = 'hidden';
    const SHORT_LIST_MODE = 'shortList';
    const LONG_LIST_MODE = 'longList';

    const GROUPS_PRIORITY = [
        StoreUnitShoppingCandidate::TYPE_API,
        StoreUnitShoppingCandidate::TYPE_UTM,
        StoreUnitShoppingCandidate::TYPE_PRINT_HERE
    ];

    public function run()
    {
        $shoppingCandidates = $this->getShoppingCandidatesList();
        if ($shoppingCandidates) {
            $this->registerAssets();
            return $this->render(
                'StoreUnitShoppingCandidate',
                [
                    'shoppingCandidates' => $shoppingCandidates,
                    'viewWidgetMode'     => $this->getViewMode(),
                    'isMinimized'        => array_key_exists('shopping-candidate-list-minimized', $_COOKIE) && $_COOKIE['shopping-candidate-list-minimized']
                ]
            );
        }
    }


    protected function getShoppingCandidatesList()
    {
        /** @var Model3dService $model3dService */
        $model3dService = Yii::createObject(Model3dService::class);
        $userSession = UserFacade::getUserSession();
        /** @var StoreUnitShoppingCandidate[] $shoppingCandidates */
        $shoppingCandidates = StoreUnitShoppingCandidate::find()->where(['user_session_id' => $userSession->id])->orderBy('create_date desc')->all();
        $typesGroups = [];
        foreach (self::GROUPS_PRIORITY as $oneGroupType) {
            $typesGroups[$oneGroupType] = [];
        }

        foreach ($shoppingCandidates as $shoppingCandidate) {
            // Skip unavailable models
            if (!$model3dService->isAvailableForCurrentUser($shoppingCandidate->storeUnit->model3d)) {
                continue;
            }
            if (count($typesGroups[$shoppingCandidate->type]) < 5) {
                $typesGroups[$shoppingCandidate->type][] = $shoppingCandidate;
                $shoppingCandidate->view_date = (new DateTime('now' ,new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
                $shoppingCandidate->safeSave();
            } else {
                // Remove too old
                $shoppingCandidate->delete();
            }
        }
        // Make flat array
        $returnValue = [];
        foreach ($typesGroups as $typeGroup) {
            foreach ($typeGroup as $shoppingCandidate) {
                $returnValue[] = $shoppingCandidate;
            }
        }
        return $returnValue;
    }

    public function getViewMode()
    {
        return array_key_exists(
            'storeUnitShoppingCandidateWidget-hidden',
            $_COOKIE
        ) && $_COOKIE['storeUnitShoppingCandidateWidget-hidden'] ? self::HIDDEN_MODE : self::SHORT_LIST_MODE;
    }

    /**
     * register required assets
     */
    protected function registerAssets()
    {
        $this->getView()->registerAssetBundle(ShoppingCandidateAssets::class);
        JsObjectFactory::createJsObject(
            'storeUnitShoppingCandidateClass',
            'storeUnitShoppingCandidate',
            [
                'viewWidgetMode' => $this->getViewMode()
            ],
            $this->getView()
        );
    }
}