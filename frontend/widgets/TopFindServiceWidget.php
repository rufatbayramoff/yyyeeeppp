<?php
/**
 * User: nabi
 */

namespace frontend\widgets;

use common\components\ArrayHelper;
use common\components\UrlHelper;
use common\models\repositories\PrinterMaterialRepository;
use common\models\SiteTag;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\modules\catalogPs\models\PsPrinterEntity;
use common\modules\catalogPs\repositories\PsPrinterCatalogRepository;
use frontend\components\UserSessionFacade;
use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;
use Yii;
use yii\bootstrap\Widget;

class TopFindServiceWidget extends Widget
{
    /**
     * @var PrinterMaterialRepository
     */
    public $materialRepo;

    /**
     * @var string
     */
    public $viewLayout = 'topFindService';

    public const CATEGORY_3DPRINTING = '3dprinting';
    public const CATEGORY_SERVICES   = 'services';
    public const CATEGORY_PRODUCTS   = 'products';


    public function injectDependencies(PrinterMaterialRepository $materialRepo)
    {
        $this->materialRepo = $materialRepo;
    }

    /**
     * @return string
     */
    public function run(): string
    {
        return $this->render($this->viewLayout, [
            'searchText'     => Yii::$app->request->get('text'),
            'searchSections' => $this->getSearchSections(),
            'defaultSection' => $this->getDefaultSection()
        ]);
    }

    public function productCategoryUrl(): string
    {
        $currentUrl = Yii::$app->request->getUrl();
        if (strpos($currentUrl,'/products/')===0) {
            $currentUrl = UrlHelper::getUrlWithoutParams($currentUrl);
            [$slash, $prefix,$category] = explode('/', $currentUrl);
            return '/products/'.$category;
        }
        return '/products/all';
    }

    public function getSearchSections(): array
    {
        return [
            [
                'label'              => _t('site.main', '3D Printing'),
                'url'                => CatalogPrintingUrlHelper::printing3dCatalog(UserSessionFacade::getLocation()),
                'category'           => self::CATEGORY_3DPRINTING,
                'selectedSectionUrl' => '/3d-printing-services',
            ],
            [
                'label'              => _t('site.main', 'Services'),
                'url'                => '/company-services',
                'category'           => self::CATEGORY_SERVICES,
                'selectedSectionUrl' => '/company-service',
            ],
            [
                'label'              => _t('site.main', 'Products'),
                'url'                => $this->productCategoryUrl(),
                'category'           => self::CATEGORY_PRODUCTS,
                'selectedSectionUrl' => '/products'
            ]
        ];
    }

    /**
     * @return array
     */
    public function getDefaultSection(): array
    {
        $currentUrl = \Yii::$app->request->getUrl();
        $currentUrl = UrlHelper::getUrlWithoutParams($currentUrl);
        foreach ($this->getSearchSections() as $section) {
            if (strpos($currentUrl, $section['selectedSectionUrl']) === 0) {
                return $section;
            }
        }

        return $this->getSearchSections()[0];
    }

    protected static function search3dprinting($query)
    {
        $searchForm                = Yii::createObject(CatalogSearchForm::class);
        $searchForm->international = 1;
        $searchForm->text          = $query;

        $psPrintersRepo = Yii::createObject(
            [
                'class'      => PsPrinterCatalogRepository::class,
                'searchForm' => $searchForm,
                'limit'      => 5
            ]
        );

        $dataProvider = $psPrintersRepo->getDataProvider();
        /** @var PsPrinterEntity[] $models */
        $models = $dataProvider->getModels();
        $items  = [];
        foreach ($models as $psPrinterCatalogEntity) {
            $title = $psPrinterCatalogEntity?->ps?->title;
            if (!$title) {
                continue;
            }
            $items[] = $title;
        }
        return $items;
    }

    protected static function searchProducts($query)
    {
        $tags = SiteTag::find()
            ->andWhere(['like', 'text', $query])
            ->joinWith('model3dTags')
            ->joinWith('productSiteTags')
            ->andWhere('product_site_tag.product_uuid is not null or model3d_tag.model3d_id is not null')
            ->limit(5)
            ->all();
        return ArrayHelper::getColumn($tags, 'text');
    }

    protected static function searchServices($query)
    {
        $tags = SiteTag::find()
            ->andWhere(['like', 'text', $query])
            ->joinWith('companyServiceTags')
            ->andWhere('company_service_tag.company_service_id is not null')
            ->limit(5)
            ->all();
        return ArrayHelper::getColumn($tags, 'text');
    }

    public static function getRelatedList($query, $category): array
    {
        if ($category === self::CATEGORY_3DPRINTING) {
            return self::search3dprinting($query);
        }
        if ($category === self::CATEGORY_PRODUCTS) {
            return self::searchProducts($query);
        }
        if ($category === self::CATEGORY_SERVICES) {
            return self::searchServices($query);
        }
        return [];
    }
}