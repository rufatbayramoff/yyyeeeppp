<?php
/**
 * User: nabi
 */

namespace frontend\widgets;

use common\components\DateHelper;
use frontend\assets\CountdownAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class Countdown extends Widget
{
    /**
     * @var string
     */
    public $datetime;

    public $timeout;
    /**
     *
     * @var string
     */
    public $format = '%H:%M:%S'; // '%H:%M:%S';

    /**
     * update, finish
     * if events are not specified, update and finish are set with simple logic
     * update - check if to show days
     * finish - show $finishedMessage
     *
     * @var array
     */
    public $events = [];

    /**
     * @var array
     */
    public $options = [];

    /**
     * message to show if countdown finished
     *
     * @var string
     */
    public $finishedMessage = "-";

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->datetime) {
            throw new InvalidConfigException('Datetime must be specified');
        }
        $view = $this->getView();
        CountdownAsset::register($view);
        $view->registerJs($this->renderScript());
        $this->options['id'] = $this->id;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $script = '';
        if (Yii::$app->request->isAjax) {
            $script = Html::tag('script', $this->renderScript());
        }
        return Html::tag('span', '', $this->options) . $script;
    }

    /**
     * @return string
     */
    protected function renderScript()
    {
        $dateTime = DateHelper::convert($this->datetime, param('systemTimezone', 'UTC'), \Yii::$app->timeZone);
        //$offset = timezone_offset_get(new \DateTimeZone(app()->timeZone), new \DateTime($this->datetime, new \DateTimeZone(app()->timeZone)));
        if(!empty($this->timeout)){
            $seconds = $this->timeout - time();
        }else{
            $seconds = strtotime($dateTime)  - time();
        }
        $scriptUtc  = '
        var ctDate = function(str){  
            var t = new Date();
            t.setSeconds(t.getSeconds() + str);
            return t;
        }';
        $script = $scriptUtc.'        
        jQuery("#' . $this->id . '").countdown(ctDate(' . intval($seconds) . '), function(e) {$(this).html(e.strftime("' . $this->format . '"))})';

        if (empty($this->events)) {
            $this->events['update'] = 'function(event){
                var format = \'%H:%M:%S\';  
                if(event.offset.totalDays > 0) {
                    if(event.offset.totalDays==1){
                        format = \'%-D day \' + format;
                    }else{
                        format = \'%-D days \' + format;
                    }
                }
                $(this).html(event.strftime(format));
            }';
            $this->events['finish'] = ' function(){ $(this).html("' . $this->finishedMessage . '"); }';
        }

        foreach ($this->events as $event => $callback) {
            $script .= '.on("' . $event . '.countdown", ' . $callback . ')';
        }
        return $script;
    }
}