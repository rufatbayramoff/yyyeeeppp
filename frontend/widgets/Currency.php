<?php namespace frontend\widgets;



class Currency extends \yii\bootstrap\Widget
{
    /**
     * 
     * @return type
     */
    public function run()
    { 
        $currencyList = \common\models\PaymentCurrency::getAll();
        $current = \common\models\PaymentCurrency::getCurrent();
        return $this->render('site/currency', [
                'current' => $current,
                'list' => $currencyList,
        ]);
    }
}
