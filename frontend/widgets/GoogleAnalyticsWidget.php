<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.03.17
 * Time: 13:53
 */

namespace frontend\widgets;

use common\components\EnableInspectletPredicate;
use frontend\models\user\UserFacade;
use yii\base\Widget;

class GoogleAnalyticsWidget extends Widget
{
    public $isSecurePage = false;

    public $enableInspectlet = true;

    /**
     * runs in iframe, we send pageview only on mouseover
     * @var bool
     */
    public $widgetMode = false;

    public function run()
    {
        $enableInspectlet = EnableInspectletPredicate::checkEnable();
        if (UserFacade::needAnalytics() && !$this->isSecurePage) {
            return $this->render(
                'GoogleAnalytics',
                [
                    'enableInspectlet' => $enableInspectlet,
                    'widgetMode'       => $this->widgetMode
                ]
            );
        } else {
            return $this->render(
                'GoogleAnalyticsDebug',
                ['widgetMode' => $this->widgetMode]
            );
        }
    }
}