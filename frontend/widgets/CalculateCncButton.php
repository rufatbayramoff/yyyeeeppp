<?php
/**
 * Created by mitaichik
 */

namespace frontend\widgets;


use common\interfaces\Model3dBaseInterface;
use common\models\CompanyService;
use common\models\User;
use common\modules\cnc\helpers\CncUrlHelper;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use yii\base\Widget;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\helpers\Url;


class CalculateCncButton extends Widget
{
    /**
     * @var Model3dBaseInterface|ActiveRecord
     */
    public $model;

    /**
     * @var CompanyService
     */
    public $machine;

    /**
     * @var string
     */
    public $linkClass;

    /**
     *
     */
    public function run()
    {

        if ($this->model && !$this->machine) {
           return $this->renderModelForAnyOwnMachine(UserFacade::getCurrentUser(), $this->model);
        }

        if ($this->model && $this->machine) {
            return $this->renderModelForMachine($this->model, $this->machine);
        }

        if (!$this->model && $this->machine) {
            return $this->renderUploadForMachine($this->machine);
        }

        return null;
    }


    /**
     * @param User $user
     * @param Model3dBaseInterface $model
     */
    private function renderModelForAnyOwnMachine(User $user = null, Model3dBaseInterface $model)
    {
        if (!$user) {
            return;
        }

        $ps = PsFacade::getPsByUserId($user->id);

        if (!$ps || !$ps->isCncAllowed()) {
            return;
        }

        $modelRoute = ['/workbench/cncm/filepage/index', 'modelId' => $model->getSourceModel3d()->id];

        $route = $ps->haveUndeletedCncMachine()
            ? $modelRoute
            : ['/mybusiness/company/add-cnc', 'redirect' => Url::to($modelRoute)];


        echo Html::a(
            _t('site.cnc', 'Calculate for CNC'),
            $route,
            ['class' => $this->linkClass ?: 'btn btn-primary btn-ghost btn-block']
        );
    }

    /**
     * @param Model3dBaseInterface $model
     * @param CompanyService $machine
     */
    private function renderModelForMachine(Model3dBaseInterface $model, CompanyService $machine)
    {
        $ps = $machine->ps;

        if (!$ps->isCncAllowed() || !$ps->haveUndeletedCncMachine()) {
            return;
        }

        $route = ['/workbench/cncm/filepage/index', 'modelId' => $model->getSourceModel3d()->id, 'machineId' => $machine->id];

        echo Html::a(
            _t('site.cnc', 'Calculate for CNC'),
            $route,
            ['class' => $this->linkClass ?: 'btn btn-primary btn-ghost col-xs-12']
        );
    }

    /**
     * @param CompanyService $machine
     */
    private function renderUploadForMachine(CompanyService $machine)
    {
        if (!$machine->isVisibleForUser(UserFacade::getCurrentUser())) {
            return;
        }

        $ps = $machine->ps;

        if (!$ps->isCncAllowed() || !$ps->haveUndeletedCncMachine()) {
            return;
        }

        $uploadRoute =  CncUrlHelper::tryCncUrl($machine->psCncMachine);

        echo Html::a(
            _t('site.cnc', 'Try CNC'),
            $uploadRoute,
            ['class' => $this->linkClass ?: 'btn btn-primary btn-ghost add-printer m-b10']
        );
    }

    /**
     * @param Model3dBaseInterface $model
     * @return string
     */
    public static function forModel(Model3dBaseInterface $model) : ?string
    {
        return static::widget(['model' => $model]);
    }

    /**
     * @param CompanyService $machine
     * @return null|string
     */
    public static function forMachine(CompanyService $machine) : ?string
    {
        return static::widget(['machine' => $machine]);
    }

    /**
     * @param array $config
     * @return null|string
     */
    public static function upload(array $config = []) : ?string
    {
        return static::widget($config);
    }
}