<?php
/**
 *
 * @author Nabi <n.ibatulin@treatstock.com>
 */
namespace frontend\widgets;

use common\components\JsObjectFactory;
use frontend\assets\SocialButtonsAsset;
use yii\base\Widget;
use yii\web\JqueryAsset;

/**
 * SocialShareWidget widget
 * Usage
 *
 * SocialShareWidget::widget(['link' => $link, 'title' => $title]);
 *
 * @package frontend\widgets
 */
class SocialShareWidget extends Widget
{
    /**
     * link to url, if many buttons on one page
     *
     * @var string
     */
    public $link;
    /**
     * page title of given link
     *
     * @var string
     */
    public $title;

    /**
     * @var array
     */
    public $buttonsConfig = [];

    /**
     * @var boolean
     */
    public $showEmail = false;
    public $showHtmlcode = true;
    /**
     * URL to img for pinterest
     * @var string
     */
    public $imgUrl = '';
    /**
     *
     */
    public function run()
    {
        // @TODO implement button config overrides
        $this->buttonsConfig = array_merge(
            [
                'email' => [
                    'title' => _t('site.share', 'Send email'),
                    'html'  => '<span class="social-likes__icon social-likes__icon_mail"></span>',
                ],

            ],
            $this->buttonsConfig
        );
        $this->registerAssets();
        return $this->render(
            'site/socialShare',
            ['link' => $this->link, 'title' => $this->title, 'id' => 'share_' . $this->id, 'imgUrl' => $this->imgUrl]
        );
    }

    /**
     * register required assets
     */
    private function registerAssets()
    {
        $this->getView()->registerJs("var facebook_app_id = '" . param('facebook_app_id') . "';", \yii\web\View::POS_HEAD);
        $this->getView()->registerAssetBundle(SocialButtonsAsset::class);
        JsObjectFactory::createJsObject(
            'socialButtonsClass',
            'socialButtonsObj' . $this->id,
            [
                'link'  => $this->link,
                'title' => $this->title,
                'id'    => 'share_' . $this->id
            ],
            $this->getView()
        );
    }
}