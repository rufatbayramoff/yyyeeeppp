<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.03.17
 * Time: 12:15
 */

namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class Model3dEmbedAssets extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $js = [
        'js/model3dEmbedWidget.js',
    ];

    public $css = [
    ];

    public $depends = [
        'depends' => AppAsset::class
    ];
}