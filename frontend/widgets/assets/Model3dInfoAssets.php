<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.07.16
 * Time: 14:58
 */
namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class Model3dInfoAssets extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $js = [
        'js/model3dInfoClass.js',
    ];

    public $css = [
    ];

    public $depends = [
        'depends' => AppAsset::class
    ];
}