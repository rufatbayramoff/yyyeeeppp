<?php
/**
 * Date: 15.09.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace frontend\widgets\assets;


use frontend\assets\AppAsset;
use yii\web\AssetBundle;

/**
 * Class Model3dScaleAssets
 *
 * @package frontend\widgets\assets
 */
class Model3dScaleAssets extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/ts/modelScale.js',
    ];

    public $depends = [
        AppAsset::class,
    ];
}