<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.11.16
 * Time: 10:49
 */

namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class ShoppingCandidateAssets extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $js = [
        'js/storeUnitShoppingCandidateClass.js',
    ];

    public $css = [
        'css/storeUnitShoppingCandidateWidget.css'
    ];

    public $depends = [
        AppAsset::class,
    ];
}