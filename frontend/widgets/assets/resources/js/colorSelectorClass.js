/**
 * Created by analitic on 30.06.16.
 */

var colorSelectorClass = {
    config: {
        colorSelectorContainerId: null,
        fotoramaWidgetDivId: 'fotoramaWidgetDiv',
        startColorId: null,
        startMaterialGroupId: null,
        selectUsage: null,
        model3dFormObjName: '',
        reloadLink: null
    },
    colorSelectorContainer: null,
    currentFileId: null,
    currentFileType: null,
    fotoramaWidget: null,
    currentColorId: null,
    currentMaterialGroupId: null,
    select2Obj: null,
    select2Usage: null,

    model3dForm: null,

    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
        self.model3dForm = window[config.model3dFormObjName];

        $(function () {
            self.initColorSelector();
            self.modeld3dForm = $('#model3dEditForm');
            self.initFotorama();
            self.initCurrentFileInfo();
        });
    },

    initColorSelector: function () {
        var self = this;

        self.currentColorId = self.config.startColorId;
        self.currentMaterialGroupId = self.config.startMaterialGroupId;
        var colorSelectorContainer = $('#' + self.config.colorSelectorContainerId);
        self.colorSelectorContainer = colorSelectorContainer;
        if (colorSelectorContainer.find('select.group').length) {
            self.select2Obj = colorSelectorContainer.find('select.group').select2({
                    dropdownCssClass: 'color-selector-dropdown',
                    minimumResultsForSearch: -1,
                    templateResult: function (state) {
                        return self.customizeElement(state);
                    },
                    templateSelection: function (state) {
                        return self.customizeResult(state);
                    }

                }
            );
            self.select2Obj.css('visibility', '');
            self.select2Obj.on("select2:select", function (event) {
                self.onMaterialChange(event);
            });
        } else {
            self.select2Obj = null;
        }

        if (colorSelectorContainer.find('.color-select-js').length) {
            self.select2ColorObj = colorSelectorContainer.find('.color-select-js').select2({
                    dropdownCssClass: 'color-selector-dropdown',
                    minimumResultsForSearch: 1,
                    templateResult: function (state) {
                        return self.customizeElement(state);
                    },
                    templateSelection: function (state) {
                        return self.customizeResult(state);
                    }

                }
            );
            self.select2ColorObj.css('visibility', '');
            self.select2ColorObj.on("select2:select", function (event) {
                self.onColorSelect(event);
            });
        } else {
            self.select2Obj = null;
        }


        if (colorSelectorContainer.find('select.usage').length) {
            self.select2Usage = colorSelectorContainer.find('select.usage').select2({
                dropdownCssClass: 'color-selector-material-dropdown',
                minimumResultsForSearch: -1,
                templateResult: function (state) {
                    return self.customizeElement(state);
                },
                templateSelection: function (state) {
                    return self.customizeResult(state);
                }
            });
            self.select2Usage.css('visibility', '');
            self.select2Usage.on("select2:select", function (event) {

            });
        }

        colorSelectorContainer.on('click', '.color-selector-material-link-long-description', function (event) {
            self.onLongDescriptionClick(event);
        });
        colorSelectorContainer.on('click', '.color-selector-material-link-short-description', function (event) {
            self.onShortDescriptionClick(event);
        });
        $('.color-selector-is-one-material-for-kit').on('click', function (event) {
            self.onClickOneMaterialKit(event);
        });
    },

    customizeElement: function (state) {
        if (!state.id) {
            return state.text;
        }

        var returnHTML = '';
        var imageUrl = $(state.element).data('imageUrl');
        var imgHtml = '';
        var titleHtml = '';
        var shortDescriptionHtml = '';
        var itemHtml = $(state.element).data('item-html');
        if (itemHtml) {
            return $(itemHtml);
        }

        if (imageUrl) {
            imgHtml = '<span class="color-selector-material-image-block"><img src="' + imageUrl + '"></span>';
        }
        var shortDescription = $(state.element).data('shortDescription');

        if (state.text) {
            titleHtml = '<span class="color-selector-material-title">' + state.text + '</span>';
        }
        if (shortDescription) {
            shortDescriptionHtml = '<span class="color-selector-material-short-description">' + shortDescription + '</span>';
        }

        if (shortDescription) {
            shortDescriptionHtml = '<span class="color-selector-material-short-description">' + shortDescription + '</span>';
        }

        returnHTML = imgHtml + titleHtml + shortDescriptionHtml ;
        return $(returnHTML);
    },

    customizeResult: function (state) {
        //debugger;
        if (!state.id) {
            return state.text;
        }

        var itemHtml = $(state.element).data('itemHtml');
        if (itemHtml) {
            return $(itemHtml);
        }

        var imageUrl = $(state.element).data('imageUrl');
        var shortDescription = $(state.element).data('shortDescription');
        var longDescription = $(state.element).data('longDescription');
        var imgHtml = '';
        if (imageUrl) {
            imgHtml = '<span class="color-selector-material-image-block"><img src="' + imageUrl + '"></span>';
        }
        var returnValueHtml =
            '<div class="color-selector-material-selected-block">' +
            imgHtml +
            '<div class="color-selector-material-text-block">' +
            '<span class="color-selector-material-title">' + state.text + '</span>' +
            '<span class="color-selector-material-short-description">' + shortDescription + '</span>';
        if (longDescription) {
            returnValueHtml +=
                '<span class="color-selector-material-link-long-description">' + _t('site.store', 'Learn more') + '</span>' +
                '<span class="color-selector-material-long-description">' + longDescription + '</span>' +
                '<span class="color-selector-material-link-short-description">' + _t('site.store', 'Hide') + '</span>';
        }
        returnValueHtml += '</div></div>';
        var returnValue = $(returnValueHtml);
        // Пердовращаем выпадение списка при клике на кнопку Learn more
        setTimeout(function () {
            $('.color-selector-material-link-long-description, .color-selector-material-link-short-description').on('mousedown', function (event) {
                event.preventDefault();
                event.stopPropagation();
            });
        }, 100);
        return returnValue;
    },

    switchDescriptionMode: function (block, mode) {
        var shortShow = (mode == 'short' ? 'none' : 'inline');
        var longShow = (mode == 'short' ? 'inline' : 'none');
        block.find('.color-selector-material-short-description').css('display', shortShow);
        block.find('.color-selector-material-link-long-description').css('display', shortShow);
        block.find('.color-selector-material-long-description').css('display', longShow);
        block.find('.color-selector-material-link-short-description').css('display', longShow);
    },

    onLongDescriptionClick: function (event) {
        var self = this;
        var obj = $(event.currentTarget);
        self.switchDescriptionMode(obj.parent(), 'short');
        event.preventDefault();
        event.stopPropagation();
    },

    onShortDescriptionClick: function (event) {
        var self = this;
        var obj = $(event.currentTarget);
        self.switchDescriptionMode(obj.parent(), 'long');
        event.preventDefault();
        event.stopPropagation();
    },

    showColorDivSelectorForMaterial: function (materialGroupId) {
        var self = this;
        var activeContainerId = $('#' + self.config.colorSelectorContainerId + '_material_' + materialGroupId);
        $('.color-selector-printer-color-block').removeClass('color-selector-printer-color-block-active');
        activeContainerId.addClass('color-selector-printer-color-block-active');
        $('.color-selector-printer-color-block-element-active').removeClass('color-selector-printer-color-block-element-active').removeClass('material-item--active');
    },

    onMaterialChange: function (event) {
        var self = this;
        var element = $(event.params.data.element);
        var materialGroupId = element.attr('value');
        var colorId = self.getSuitableColorForMaterial(materialGroupId, self.currentColorId);
        self.showColorDivSelectorForMaterial(materialGroupId);
        var dontRefreshPage = self.currentColorId == colorId; // TODO: realize only on edit page
        self.setNewCurrentMaterialAndColor(materialGroupId, colorId);
        self.activeSelectElement(materialGroupId, colorId);
    },

    activeSelectElement: function (materialGroupId, colorId) {
        var self = this;
        var activeContainerId = $('#' + self.config.colorSelectorContainerId + '_material_' + materialGroupId);
        var select = activeContainerId.find('.color-select-js');
        select.val(colorId).trigger('change');
    },

    refreshPage: function (isKitClicked) {
        var self = this;
        var materialGroupId = self.currentMaterialGroupId;
        var colorId = self.currentColorId;
        var isOneMaterialForKit = self.isOneMaterialKit();

        self.modeld3dForm.trigger('changeModelColor', {colorId: colorId, materialGroupId: materialGroupId,  'isOneMaterialForKit': isOneMaterialForKit, isKitClicked: isKitClicked});
    },

    setNewCurrentMaterialAndColor: function (materialGroupId, colorId, dontRefreshPage) {
        var self = this;
        self.currentMaterialGroupId = materialGroupId;
        self.currentColorId = colorId;
        self.refreshPage();
    },

    onColorSelect: function (event) {
        var self = this;
        var obj = $(event.params.data.element);
        if ((typeof(ga) != 'undefined') && ga) {
            ga('send', 'event', {
                eventCategory: 'ColorChange',
                eventAction: 'click',
                eventLabel: obj.data('colorCode')
            });
        }
        self.setNewCurrentMaterialAndColor(obj.data('materialGroupId'), obj.data('colorId'));

    },

    hideMaterialAndColorSelectors: function () {
        $('.color-selector-message-select-model3dpart').removeClass('hidden');
        $('.color-selector-material-and-color-selectors').addClass('hidden');
    },

    showMaterialAndColorSelectors: function () {
        var self = this;
        $('.color-selector-message-select-model3dpart').addClass('hidden');
        $('.color-selector-material-and-color-selectors').removeClass('hidden');
    },

    isColorSelectorAvailible: function () {
        var htmlElement = $('.color-selector-is-one-material-for-kit');
        return htmlElement.length ? true : false;
    },

    isOneMaterialKit: function () {
        var htmlElement = $('.color-selector-is-one-material-for-kit');
        return htmlElement.is(':checked');
    },

    setGuiMaterial: function (currentMaterialGroupId) {
        var self = this;
        if (self.select2Obj) {
            self.select2Obj.val(currentMaterialGroupId).trigger('change');
        }
        self.showColorDivSelectorForMaterial(currentMaterialGroupId);
    },

    setGuiColor: function (currentMaterialGroupId, currentColorId) {
        var self = this;

        $('#color-selector-block_material_' + currentMaterialGroupId).find("[data-color-id='" + currentColorId + "']")
            .addClass('color-selector-printer-color-block-element-active').addClass('material-item--active');

        self.select2ColorObj.val(currentColorId).trigger('change');
    },

    refreshShowKit: function () {
        var self = this;
        if (self.isOneMaterialKit()) {
            self.showMaterialAndColorSelectors();
            self.setGuiMaterial(self.currentMaterialGroupId);
            self.setGuiColor(self.currentMaterialGroupId, self.currentColorId);
        } else {
            if (self.isCurrentModelFileTypeModel()) {
                self.showMaterialAndColorSelectors();
                self.setGuiMaterial(self.currentMaterialGroupId);
                self.setGuiColor(self.currentMaterialGroupId, self.currentColorId);
            } else {
                self.hideMaterialAndColorSelectors();
            }
        }
    },

    onClickOneMaterialKit: function (event) {
        var self = this;
        self.refreshPage(true);
    },

    getSuitableColorForMaterial: function (newMaterialGroupId, currentColorId) {
        var self = this;
        var activeContainerId = $('#' + self.config.colorSelectorContainerId + '_material_' + newMaterialGroupId);
        var sameColorBlock = activeContainerId.find('.color-selector-printer-color-block-element[data-color-id="' + currentColorId + '"]');
        if (sameColorBlock.length) {
            return currentColorId;
        }
        var firstColorBlock = activeContainerId.find('.color-selector-printer-color-block-element').first();
        return firstColorBlock.data('color-id');
    },

// --------- FOTORAMA interface --------
    initFotorama: function () {
        var self = this;
        self.fotoramaWidget = $('#' + self.config.fotoramaWidgetDivId);
    },

    initCurrentFileInfo: function () {
        var self = this;

        setTimeout(function () {
            self.initFotorama();
        }, 2000);
    },
    isCurrentModelFileTypeModel: function (event) {
        var self = this;
        return self.currentFileType == 'model';
    },

    onChangeModelFile: function (selectedItem, allData) {
        var self = this;
        self.currentFileId = selectedItem.fileid;
        self.currentFileType = selectedItem.type;
        if (typeof selectedItem.printercolorid != 'undefined') {
            self.currentColorId = selectedItem.printercolorid;
        }
        if (typeof selectedItem.printermaterialgroupid != 'undefined') {
            self.currentMaterialGroupId = selectedItem.printermaterialgroupid;
        }

        self.refreshShowKit();

        // show or hide  Set one material and color for kit checkbox
        var modelsCount = 0;
        allData.forEach(function (item) {
            if (item.type === "model") {
                modelsCount++;
            }
        });

        if (modelsCount > 1) {
            $('.color-selector-is-one-material-for-kit-group').show();
        }
        else {
            $('.color-selector-is-one-material-for-kit-group').hide();
        }
    }
};
