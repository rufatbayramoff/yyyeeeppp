/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.12.16
 * Time: 15:49
 */

var storeUnitElementClass = {
    config: {
        uid: ''
    },

    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        storeUnitJsPricesObj.calculatePrices();
        return self;
    },

};