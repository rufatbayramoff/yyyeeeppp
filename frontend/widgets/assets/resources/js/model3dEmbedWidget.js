/**
 * Created by analitic on 20.03.17.
 */

var model3dEmbedWidgetClass = {
    config: {
        widgetUuid: null,
        cssUrl: null,
        //origUrl = '" . param('server') . '/3dmodels/' . $model3d->id . "?widget=1';
    },

    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);

        self.fillCopyCode();
    },

    fillCopyCode: function () {
        var self = this;
        document.getElementById('model-embed__code-input-' + self.config.widgetUuid).value =
            '<link href="'+self.config.cssUrl+'" rel="stylesheet" ><iframe class="ts-embed-model" width="300" height="355" src="' + document.getElementById('model-embed__iframe-' + self.config.widgetUuid).src + '" frameborder="0"></iframe>';
    },

    updateEmbed: function () {
        var self = this;

        var hasPrice = document.getElementById('model-embed__show-price-' + self.config.widgetUuid).checked;
        var hasImg = document.getElementById('model-embed__show-image-' + self.config.widgetUuid).checked;
        var emf = document.getElementById('model-embed__iframe-' + self.config.widgetUuid);
        var origUrl = emf.src;

        function addParam(url, param, value) {
            var a = document.createElement('a'), regex = /(?:\?|&amp;|&)+([^=]+)(?:=([^&]*))*/g;
            var match, str = [];
            a.href = url;
            param = encodeURIComponent(param);
            while (match = regex.exec(a.search))
                if (param != match[1]) str.push(match[1] + (match[2] ? "=" + match[2] : ""));

            str.push(param + (value ? "=" + encodeURIComponent(value) : "=0"));
            a.search = str.join("&");
            return a.href;
        }

        var src = addParam(emf.src, 'price', hasPrice ? 1 : 0);
        src = addParam(src, 'img', hasImg ? 1 : 0);
        emf.src = src;
        self.fillCopyCode();
    }
}


