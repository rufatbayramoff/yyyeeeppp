/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.11.16
 * Time: 10:51
 */

var storeUnitShoppingCandidateClass = {
    config: {
        viewWidgetMode: 'shortList'
    },

    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
        return self;
    },

    switchShortLongList: function () {
        if (this.config.viewWidgetMode == 'shortList') {
            $('.js-shopping-candidate-element').removeClass('hidden');
            this.config.viewWidgetMode = 'longList';
            $('.js-shopping-candidate-expand').addClass('hidden');
        }
    },

    resetWindowSize: function() {
        TS.setCookie('shopping-candidate-list-minimized', '');
        $('.widget-api').removeClass('widget-api--minified');
    },

    minimizeWindow: function ()    {
        TS.setCookie('shopping-candidate-list-minimized', '1');
        $('.widget-api').addClass('widget-api--minified');
    },

    deleteModelFromList: function (shoppingCandidateId) {
        $('#store-unit-shopping-candidate-' + shoppingCandidateId).addClass('hide');
        $.post(
            '/catalog/store-unit-shopping-candidate/delete?id='+shoppingCandidateId
        );

        $('.js-shopping-candidate-element.hidden').first().removeClass('hidden');
        var elementsCount = $('.js-shopping-candidate-element').not('.hide').size();
        if (elementsCount<1) {
            $('.widget-api').addClass('hidden');
        }
    }
};

