/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.12.16
 * Time: 16:39
 */

var printByPsPrinterClass = {
    config: {},

    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
        return self;
    },

    changeCategory: function(categoryId) {
        $('.ps-printer-modal__category_models').addClass('hidden');
        $('#ps-printer-modal-category-list-'+categoryId).removeClass('hidden');
    }
};
