/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.07.16
 * Time: 14:51
 */

var model3dInfoClass = {
    config: {
        model3dInfo: null,
        model3dParts: null
    },

    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
        return self;
    },

    getModelPartInfoByFileId: function (fileId) {
        for (key in this.config.model3dParts) {
            val = this.config.model3dParts[key];
            if (val.fileId == fileId) {
                return val;
            }
        }
        return null;
    }
};
