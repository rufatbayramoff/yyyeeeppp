/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.12.16
 * Time: 18:29
 */


var model3dUploadClass = {
    config: {
        maxFileSize: null,
        uploadUrl: null,
        imagesExtensions: [],
        model3dExtensions: [],
        cuttingExtensions: [],
        otherExtensions: [],
        allowedExtensions: [],
        files: [],
        categories: [],
        maxFiles: 50,
        model3dStaticUrl: '/static/images/3dicon_412x309.png',
        othersStaticUrl: '/static/images/file.png',
        previewUrl: '/catalog/upload-model3d/file-preview',
        postFormUrl: '/catalog/upload-model3d/create-model3d' + window.location.search,
        authRequired: false,
        additionalParams: [],
        isPlaceOrderState: false,
        needNewWindow: false,
        isWidget: true
    },
    redirectUrl: null,
    dropzone: null,
    allowedExtensions: [],
    downloadUrl: '',
    uploadedFilesUuids: [],

    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        self.allowedExtensions = config.allowedExtensions;
        self.initDropZone();
        self.initUrlDownload();
        return self;
    },

    initUrlDownload: function () {
        var self = this;
        $('.download-by-url-js').on('click', function () {
            var inLoadText = '<i class="tsi tsi-refresh tsi-spin-custom"></i> ' + _t('front.user', 'Please wait...');
            $('.download-by-url-js').html(inLoadText);
            self.downloadUrl = $('.download-url-text-js').val();
            self.saveUploadForm();
        });
    },

    initDropZone: function () {
        var self = this;
        self.dropzone = dropzone = new Dropzone('#ts-uploader', {
            previewsContainer: '#ts-models-preview',
            paramName: 'files',
            maxFilesize: this.config.maxFileSize,
            maxFiles: self.maxFiles,
            thumbnailWidth: 160,
            thumbnailHeight: 120,
            parallelUploads: 5,
            dictFileTooBig: _t('site.ps', 'File is too big ({{filesize}}MB). Max file size: {{maxFilesize}}MB.'),
            dictMaxFilesExceeded: _t('site.ps', 'You cannot upload more than ') + self.maxFiles + _t('site.ps', ' files for one model or KIT'),
            clickable: '.js-upload-start .fileinput-button',
            url: this.config.uploadUrl,
            acceptedFiles: self.createExtensionsString(self.allowedExtensions),
            previewTemplate: self.getTemplate('.ts-uploader-template'),
            init: function () {
                this.on('addedfile', function (file) {
                    //console.log('addedfile');
                    //console.log(file);
                    var itemEl = $(file.previewElement);
                    var fileExt = file.name.split('.').pop().toLowerCase();

                    // Fix file preview position and template
                    if (_.contains(self.config.model3dExtensions, fileExt)) {
                        $('.ts-uploader-models-header').removeClass('hide');
                        self.dropzone.emit('thumbnail', file, self.config.model3dStaticUrl);
                    } else if (_.contains(self.config.cuttingExtensions, fileExt)) {
                        $('.ts-uploader-cutting-header').removeClass('hide');
                        itemEl.detach().appendTo('#ts-cutting-preview');
                    } else if (_.contains(self.config.imagesExtensions, fileExt)) {
                        $('.ts-uploader-pictures-header').removeClass('hide');
                        itemEl.detach().appendTo('#ts-pictures-preview');
                    } else {
                        $('.ts-uploader-others-header').removeClass('hide');
                        self.dropzone.emit('thumbnail', file, self.config.othersStaticUrl);
                        itemEl.detach().appendTo('#ts-others-preview');
                    }
                });
                this.on('error', function (file, responseJson) {
                    if (file.status === 'error') {
                        $(file.previewElement).find('.preview3d').fadeOut();
                        self.dropzone.removeFile(file);
                        new TS.Notify({
                            type: 'error',
                            text: _.isString(responseJson) ? responseJson : (responseJson.message || _t('site.ps', 'File type is not supported')),
                            target: '.messageBox',
                            automaticClose: true
                        });
                    }
                });
                this.on('success', function (file, responseJson) {
                    responseJson = JSON.parse(responseJson);
                    var fileExt = file.name.split('.').pop().toLowerCase();
                    if (responseJson.success) {
                        var fileUuid = responseJson['uuids'][file.name];
                        if (_.contains(self.config.model3dExtensions, fileExt)) {
                            self.addToRender(file, fileUuid);
                        }
                        self.uploadedFilesUuids.push(fileUuid);
                    }
                });
                this.on('queuecomplete', function () {

                    if (self.dropzone.files && self.dropzone.files.length > 0) {
                        self.saveUploadForm();
                        return;
                    }
                    setTimeout(function () {
                        self.cancelUpload();
                    }, 1500);
                });
                this.on('complete', function (file) {
                    var itemEl = file.previewElement;
                    $(itemEl).find('.progress').remove();
                });
            },
            accept: function (file, done) {
                var fileExt = file.name.split('.').pop().toLowerCase();

                if (!(_.contains(self.config.allowedExtensions, fileExt))) {
                    new TS.Notify({
                        type: 'error',
                        text: self.messages.unsupportedFileTypes,
                        target: '.messageBox'
                    });
                    return false;
                }

                $('.js-upload-start').addClass('hidden');
                $('.upload-preview').removeClass('hidden');
                done();
            },
        });
    },

    /**
     * Create extensions for Dropbox from array of extensions
     * @returns {string}
     */
    createExtensionsString: function () {
        var extArray = [].concat.apply([], arguments);
        extArray = _.map(extArray, function (ext) {
            return '.' + ext;
        });
        return extArray.join(',').toLowerCase();
    },

    /**
     * get template HTML by id
     *
     * @param tplId
     * @returns string
     */
    getTemplate: function (tplId) {
        var tpl = $(tplId)[0].outerHTML;
        tpl = tpl.replace('hidden', '');
        return tpl;
    },

    initCategories: function () {
        let self = this;
        var select = $('.js-category-select').select2({
            placeholder: "Any",
            allowClear: false,
            data: self.config.categories,
            dropdownAutoWidth: true,
            theme: 'krajee',
            width: '100%',
            containerCssClass: ':all:',
        });
    },


    saveUploadForm: function () {
        var self = this;
        $('#ts-upload-creating-model3d-progress').removeClass('hidden');
        $.post(
            self.config.postFormUrl,
            {
                'downloadUrl': self.downloadUrl,
                'uploadedFilesUuids': self.uploadedFilesUuids,
                'additionalParams': self.config.additionalParams,
                'isPlaceOrderState': self.config.isPlaceOrderState,
                'isWidget': self.config.isWidget
            },
            function (resultJson) {
                if (!resultJson) {
                    new TS.Notify({type: 'error', text: 'Failed'});
                    return;
                }
                if (resultJson.success) {
                    if (resultJson.type === 'preorder') {
                        $('#ts-upload-creating-model3d-progress').addClass('hidden');
                        $('#upload-guide-container').addClass('hidden');
                        $('#ts-upload-description').removeClass('hidden');
                        self.initCategories();
                    } else {
                        if (self.config.authRequired && (!TS.User || TS.User.isGuest)) {
                            var loginForm = TS.Visitor.loginForm(resultJson.redirectUrl);
                            loginForm.on('hide.bs.modal', function () {
                                window.location = resultJson.redirectUrl;
                            });
                        } else if (self.config.needNewWindow) {
                            parent.location.href = resultJson.redirectUrl;
                        } else {
                            window.location = resultJson.redirectUrl;
                        }
                    }
                } else {
                    new TS.Notify({type: 'error', text: resultJson.message});
                }
            }
        ).fail(function (response) {
            let message = '';
            try {
                var responseJson = JSON.parse(response.responseText);
                if (responseJson.errors) {
                    message = commonJs.prepareMessage(responseJson.errors);
                } else {
                    message = responseJson.message;
                }
            } catch (e) {
                message = response.responseText.substr(0, 300);
            }
            message = message ? message : _t('site.ps', 'Invalid upload');
            //console.log(responseJson);
            new TS.Notify({
                type: 'error',
                text: message,
                target: '.messageBox',
                automaticClose: true
            });
            setTimeout(function () {
                self.cancelUpload();
            }, 2000);
        });
    },

    cancelUpload: function () {
        location.reload();
    },

    submit: function () {
        let self = this;
        let category = $('#ts-uploader-category').val();
        let description = $('#ts-uploader-description').val();
        if (!category) {
            new TS.Notify({
                type: 'error',
                text: _t('site.ps', 'Please, select category'),
                target: '.messageBox',
                automaticClose: true
            });
            return;
        }
        if (!description) {
            new TS.Notify({
                type: 'error',
                text: _t('site.ps', 'Please, enter description'),
                target: '.messageBox',
                automaticClose: true
            });
            return;
        }

        let redirectUrl = '/company-services?productCategory='
            + encodeURIComponent(category)
            + '&description=' + encodeURIComponent(description)
            + '&uploadedFilesUuids=' + self.uploadedFilesUuids.join('-');
        window.top.location = redirectUrl;
    },

    /**
     * add job to render
     *
     * @param file
     * @param fileUuid
     */
    addToRender: function (file, fileUuid) {
        var self = this;
        var previewUrl = self.config.previewUrl + '?uuid=' + fileUuid + '&rand=' + Math.random();

        $('<img src="' + previewUrl + '">').on('load', function () {
            self.dropzone.emit('thumbnail', file, previewUrl);
            return true;
        }).bind('error', function () {
            setTimeout(function () {
                self.addToRender(file, fileUuid);
            }, 2500);
        });
    },
};
