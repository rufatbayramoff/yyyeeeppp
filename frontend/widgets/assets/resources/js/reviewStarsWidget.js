var reviewStarsWidgetClass = {
    config: {
        uid: null
    },

    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
        self.initStars();
        return self;
    },

    initStars: function () {
        var self = this;
        $(function () {
            $('#star-rating-block-' + self.config.uid + ' .star-rating').rating({
                showCaption: false,
                showClear: false,
                min: 0,
                max: 5,
                readOnly: true,
                starCaptions: {
                    1: _t('site.common', 'Very Bad'),
                    2: _t('site.common', 'Bad'),
                    3: _t('site.common', 'Good'),
                    4: _t('site.common', 'Very Good'),
                    5: _t('site.common', 'Perfect')
                }

            });
        });
    }
}

