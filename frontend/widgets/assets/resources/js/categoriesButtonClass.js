/**
 * View of list store units
 */
var categoriesButtonClass = {
    config: {
        elId: 'js-category-button-main-list'
    },

    el: undefined,


    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
        self.initializeButton();
    },

    /**
     * Init
     */
    initializeButton: function (options) {
        var self = this;
        this.el = $('.'+self.config.elId);

        this.el.find('[data-category-id]').each(function(i, element){
            var categoryId = $(element).data('category-id');
            var parentObj = $(element).parent();
            parentObj.click(function() {
                var uri = new URI(window.location.href);
                uri.setSearch('categoryId', categoryId);
                window.location.href = uri.toString();
            });
        });
    },

    /**
     * On category click
     * @param event
     */
    onCategoryClick: function (event) {
        var el = $(event.currentTarget);
        this.setActiveCategoryId(el.data().categoryId, el.text().trim());
    },
}



