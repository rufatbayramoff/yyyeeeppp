/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.12.16
 * Time: 14:52
 */

var storeUnitJsPricesObj = {
    runningCalculatePriceAjaxCount: 0,

    calculatePricesAjax: function (unInitializedListSrc) {
        var unInitializedList = unInitializedListSrc.slice(0);
        var self = this;
        if (self.runningCalculatePriceAjaxCount < 2) {
            self.runningCalculatePriceAjaxCount++;
            $.ajax({
                    url: '/product/product-ajax/get-price',
                    type: 'GET',
                    data: {'storeUnits': unInitializedList},
                    dataType: 'json',
                    error: function (result) {
                        self.runningCalculatePriceAjaxCount--;
                    },
                    success: function (result) {
                        self.runningCalculatePriceAjaxCount--;
                        result = $.parseJSON(result);
                        if (result.success === true) {
                            for (storeUnitId in result.costs) {
                                var cost = result.costs[storeUnitId];
                                if (cost !== null) {
                                    var htmlCode = '<div class="catalog-item__footer__price-label">' + _t('front.catalog', 'From') + '</div> ' + cost;
                                    $('.catalog-price-store-unit-' + storeUnitId).html(htmlCode);
                                }
                            }
                        } else {

                        }
                    }
                }
            );
        } else {
            setTimeout(function () {
                self.calculatePricesAjax(unInitializedList);
            }, 100);
        }
    },

    calculatePrices: function () {
        var self = this;
        var unInitializedList = [];
        var count = 0;

        $('.js-catalog-price').each(function (index, obj) {
            if (!$(obj).data('costInitialized')) {
                var storeUnitId = $(obj).data('store-unit-id');
                if ($(obj).data('cost-for-printer-id')) {
                    var storeUnitInfo = {
                        'id': storeUnitId,
                        'printerId': $(obj).data('cost-for-printer-id'),
                    }
                } else {
                    var storeUnitInfo = storeUnitId;
                }
                $(obj).data('costInitialized', true);
                unInitializedList.push(storeUnitInfo);
                count++;
                if (count >= 9) {
                    self.calculatePricesAjax(unInitializedList);
                    unInitializedList = [];
                    count = 0;
                }
            }
        });
        if (unInitializedList.length) {
            self.calculatePricesAjax(unInitializedList);
        }
    }
}