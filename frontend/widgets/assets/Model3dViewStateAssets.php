<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.01.17
 * Time: 10:55
 */

namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class Model3dViewStateAssets extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $js = [
        'js/model3dUploadClass.js',
    ];

    public $css = [
        'css/model3dUploadWidget.css'
    ];

    public $depends = [
        AppAsset::class,
    ];
}