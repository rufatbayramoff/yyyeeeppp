<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.12.16
 * Time: 12:01
 */

namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class ReviewStarsAssets extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $js = [
        'js/reviewStarsWidget.js',
    ];

    public $css = [
        'css/reviewStarsWidget.css'
    ];

    public $depends = [
        AppAsset::class,
    ];
}
