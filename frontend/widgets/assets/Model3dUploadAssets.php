<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.12.16
 * Time: 18:28
 */

namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use frontend\assets\DropzoneAsset;
use yii\web\AssetBundle;

class Model3dUploadAssets extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $js = [
        'js/model3dUploadClass.js',
    ];

    public $css = [
        'css/model3dUploadWidget.css',
    ];

    public $depends = [
        DropzoneAsset::class,
        AppAsset::class
    ];
}