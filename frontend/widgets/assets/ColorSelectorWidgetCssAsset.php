<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.06.16
 * Time: 16:25
 */

namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class ColorSelectorWidgetCssAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $css = [
        'css/colorSelectorWidget.css'
    ];

    public $depends = [
        'depends' => AppAsset::class
    ];
}