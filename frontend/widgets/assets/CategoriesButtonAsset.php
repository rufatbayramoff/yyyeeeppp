<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 30.06.16
 * Time: 16:25
 */

namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class CategoriesButtonAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $js = [
        'js/categoriesButtonClass.js',
    ];

    public $css = [
        'css/categoriesButtonWidget.css'
    ];

    public $depends = [
        'depends' => AppAsset::class
    ];
}