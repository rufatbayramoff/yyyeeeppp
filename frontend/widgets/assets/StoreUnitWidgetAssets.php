<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.12.16
 * Time: 15:12
 */

namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class StoreUnitWidgetAssets extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $js = [
        'js/storeUnitElementClass.js',
        'js/storeUnitJsPricesObj.js',
    ];

    public $css = [
    ];

    public $depends = [
        AppAsset::class,
    ];
}