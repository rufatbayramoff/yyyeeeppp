<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.12.16
 * Time: 16:38
 */

namespace frontend\widgets\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;

class PrintByPsPrinterAssets extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/assets/resources';

    public $js = [
        'js/printByPsPrinterClass.js',
    ];

    public $css = [
        'css/printByPsPrinterWidget.css'
    ];

    public $depends = [
        AppAsset::class,
    ];

    public function publish($am)
    {
        parent::publish($am);
    }
}
