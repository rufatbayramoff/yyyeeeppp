<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.08.18
 * Time: 14:10
 */

use common\models\Company;
use frontend\models\ps\PsFacade;

/** @var $company Company */
?>
<a href="<?= \Yii::$app->params['siteUrl'] . PsFacade::getPsLinkAbout($company) ?>" class="item-rendering-external-widget__ps-info" target="_blank" data-pjax="0">
    <img src="<?= HL($company->getLogoUrl(false, 240)) ?>" alt="<?= H($company->title) ?>" class="item-rendering-external-widget__ps-avatar" itemprop="image">
    <h4 class="item-rendering-external-widget__ps-name"><?= H($company->title) ?></h4>
    <div class="item-rendering-external-widget__powered-link">
        <?= _t('site.store', 'Powered by {img} Treatstock', ['img' => '<img src="/static/images/logo-pie.svg" width="12px" height="12px">']); ?>
    </div>
</a>
