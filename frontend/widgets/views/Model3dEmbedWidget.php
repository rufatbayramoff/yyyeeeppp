<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.03.17
 * Time: 11:53
 */
use common\models\Model3d;
use frontend\models\model3d\Model3dFacade;

/** @var string $widgetUuid */
/** @var Model3d $model3d */
?>

<div class="row">
    <div class="col-md-5">
        <div class="model-embed__preview">
            <h4 class="model-embed__title"><?= _t('site.model3d', 'Preview') ?></h4>
            <div id="embedframe">
                <iframe id="model-embed__iframe-<?= $widgetUuid ?>" style="width: 100%; max-width: 300px;" width="300" height="355"
                        src="<?php echo param('server'); ?><?= Model3dFacade::getStoreUrl($model3d); ?>?widget=1&price=1&img=1"
                        frameborder="0"></iframe>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <label class="control-label model-embed__code-label" for="model-embed__code-input-<?= $widgetUuid ?>"><?= _t('site.model3d', 'Code') ?></label>
            <input class="form-control model-embed__code-input" id="model-embed__code-input-<?= $widgetUuid ?>" type="text" value="" onfocus="this.select(true)">

        </div>

        <div class="model-embed__settings">
            <h4 class="model-embed__title"><?= _t('site.model3d', 'Settings') ?></h4>
            <div class="checkbox">
                <input id="model-embed__show-price-<?= $widgetUuid ?>" type="checkbox" checked="checked" onChange="model3dEmbedWidgetObj<?= $widgetUuid ?>.updateEmbed()">
                <label for="checkbox11">
                    <?php echo _t('model3d', 'Show Price'); ?>
                </label>
            </div>
            <div class="checkbox">
                <input id="model-embed__show-image-<?= $widgetUuid ?>" type="checkbox" checked="checked" onChange="model3dEmbedWidgetObj<?= $widgetUuid ?>.updateEmbed()">
                <label for="checkbox21">
                    <?php echo _t('model3d', 'Show Image'); ?>
                </label>
            </div>
        </div>
    </div>
</div>

