<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.03.17
 * Time: 14:01
 */

use frontend\models\user\UserFacade;

$gatrackId = app('setting')->get('site.ga_analytics', false);
$userId = UserFacade::getCurrentUserId();
?>

<!-- Google Tag Manager -->
<script>
    <?php
    if($widgetMode):
        $this->registerJs('var mouseOver = false;
        document.body.onclick = function(){
            if(!mouseOver){
                ga(\'set\', \'dimension1\', \'ts4realUsers\');
                ga(\'send\', \'pageview\');
            }
            mouseOver  = true;
        }');
    endif; ?>

    // ga('require', 'ec');

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    if(ga) {
        ga('create', '<?= $gatrackId ?>', 'auto', {allowLinker: true});
        ga('require', 'linker');
        ga('linker:autoLink', <?=json_encode(\Yii::$app->getModule('intlDomains')->allowedDomains)?>);
        <?php if(!$widgetMode): ?>
        ga('set', 'dimension1', 'ts4realUsers');
        ga('send', 'pageview');
        <?php endif; ?>
        <?php if($userId) { ?>
        ga('set', 'userId', '<?=$userId?>');
        <?php }?>
    }

    (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-W6N6S7Q');


</script>
<?php

if (param('enableInspectlet') && !$widgetMode) {
    ?>
    <script type="text/javascript" id="inspectletjs">
        function turnOnSmartlook() {
            <!-- Smartlook Tracking Code for treatstock.com -->
            window.smartlook||(function(d) {
                var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
                var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
                c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '0a62c79d69b06a56867a283256329ef24258d099');
        }
        <?php
        if ($enableInspectlet) {
        ?>
        turnOnSmartlook();
        <?php
        }
        ?>
    </script>
    <?php
} else {?>
    <script type="text/javascript">
        function turnOnSmartlook() {}
    </script>
<?php
}
?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PXN6N57');
</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PXN6N57"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->