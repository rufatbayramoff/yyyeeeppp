<span class="tsi tsi-warning-c"></span>
<div class="order__progress">
    <?php
    foreach($history as $step):
            if(!empty($step['hidden'])){ continue; }
        ?>
    <div class="order__progress-item <?=!empty($step['checked'])?'order__progress-item--checked':'';?>">
        <div class="order__progress-status"><?=$step['title'];?></div>
        <div class="order__progress-date"><?=$step['comment']; ?></div>
    </div>
    <?php endforeach; ?>
</div>