<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.01.17
 * Time: 15:04
 */
use frontend\widgets\Model3dHiddenForm;
use yii\bootstrap\ActiveForm;

/** @var \frontend\widgets\Model3dViewedStateWidget $widget */

$model3d = $widget->model3d;
//$resultUrl = $widget->forumUrl . '?storeUnitId=' . $widget->model3d->storeUnit->id . '&psPrinterId=' . $widget->psPrinter->id . '&' . http_build_query($widget->forumUrlParams);
$resultUrl = '/my/print-model3d?model3d='.$model3d->getUid().'&psId='.$widget->psPrinter->ps->id.'&posUid=ps'.$widget->psPrinter->ps->id;

$form = ActiveForm:: begin(
    [
        'action' => $resultUrl,
        'method'  => 'post',
    ]
);
?>
    <input type="hidden" name="<?= Model3dHiddenForm::FORM_PREFIX ?>[isOneMaterialForKit]" value="<?= $widget->model3d->isOneTextureForKit() ?>">
<?php
if ($widget->model3d->isOneTextureForKit()) {
    $texture = $widget->model3d->getKitTexture();
    ?>
    <input type="hidden" name="<?= Model3dHiddenForm::FORM_PREFIX ?>[modelMaterial][materialGroupId]" value="<?= $texture->calculatePrinterMaterialGroup()->id ?>">
    <input type="hidden" name="<?= Model3dHiddenForm::FORM_PREFIX ?>[modelMaterial][colorId]" value="<?= $texture->printerColor->id ?>">
    <?php
} else {
    foreach ($widget->model3d->getActiveModel3dParts() as $model3dPart) {
        $texture = $model3dPart->getTexture();
        ?>
        <input type="hidden" name="<?= Model3dHiddenForm::FORM_PREFIX ?>[filesMaterial][<?= $model3dPart->file->id ?>][materialGroupId]"
               value="<?= $texture->calculatePrinterMaterialGroup()->id ?>">
        <input type="hidden" name="<?= Model3dHiddenForm::FORM_PREFIX ?>[filesMaterial][<?= $model3dPart->file->id ?>][colorId]"
               value="<?= $texture->printerColor->id ?>">
        <?php
    }
}
?>
