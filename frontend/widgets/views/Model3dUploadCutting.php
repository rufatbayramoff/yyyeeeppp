<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.12.16
 * Time: 18:27
 */

use yii\widgets\ActiveForm;

/** @var \frontend\widgets\Model3dUploadWidget $widget */

$modelName = '';

?>
<div class='model3d-upload-block'>
    <div class="js-upload-start upload-start">
        <?php
        $form = ActiveForm::begin(['id' => 'ts-uploader', 'action' => '/upload/file-upload']);
        ?>
        <img class="img-responsive upload-start__pic" width="200" height="158" style="left:0;" src="/static/images/upload.png" alt="<?=_t('front.upload', 'Upload your 3D models');?>">
        <p class="upload-start__text">
        </p>

        <div class="item-rendering-external-widget__howto">
            <div class="item-rendering-external-widget__howto-steps m-b0">
                <div class="item-rendering-external-widget__count">1</div>
                <span><?= _t('front.upload', 'Upload your 3D models'); ?></span>
            </div>
        </div>

        <div class="fileinput-button m-b10">
            <span class="btn btn-primary"><?= _t('front.upload', 'Browse files'); ?></span>
        </div>

        <p class="text-muted m-b0 p-l10 p-r10">
            <?= _t('front.upload', 'DXF, CDR, EPS, SVG files are supported'); ?>
        </p>
        <p class="text-muted m-b20 p-l10 p-r10">
            <?= _t('front.upload', 'All files are protected by Treatstock security and '); ?>
            <a href="https://www.watermark3d.com/" target="_blank">Watermark 3D</a>
        </p>

        <div class="item-rendering-external-widget__howto">
            <?php
            $step = 1;
            $step++; ?>
            <div class="item-rendering-external-widget__howto-steps">
                <div class="item-rendering-external-widget__count item-rendering-external-widget__count--disabled"><?=$step;?></div>
                <span><?= _t('front.upload', 'Place an order'); ?></span>
            </div>
        </div>

        <?php $form->end(); ?>
    </div>
    <div class="upload-preview hidden">
        <div class="ts-uploader-views">
            <h3 class="ts-uploader-models-header hide"><?= _t('front.upload', '3D Models'); ?></h3>
            <div class="uploader-item-list ts-uploader-archive files" data-sortable-id="1" aria-dropeffect="move" id="ts-models-preview">
            </div>

            <h3 class="ts-uploader-pictures-header hide"><?= _t('front.upload', 'Model pictures'); ?></h3>
            <div class="uploader-item-list ts-uploader-archive files" data-sortable-id="1" aria-dropeffect="move" id="ts-pictures-preview">
            </div>

            <h3 class="ts-uploader-others-header hide"><?= _t('front.upload', 'Other files'); ?></h3>
            <div class="uploader-item-list ts-uploader-archives files" data-sortable-id="1" aria-dropeffect="move" id="ts-others-preview">
            </div>

            <div class="ts-uploader-template dz-preview uploader-item hidden">
                <img data-dz-thumbnail class="model3d-upload-image-preview" alt="">
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
            </div>
            <div id="ts-upload-creating-model3d-progress" class="hidden">
                <?= _t('site.ps', 'Processing 3D Models...'); ?><img class="m-l10" src="/static/images/loading.gif"  alt="loading...">
            </div>
        </div>
        <button id="ts-uploader-cancel" class="btn btn-default ts-uploader-cancel" onclick="model3dUploadObj.cancelUpload()"><?= _t('site.ps', 'Cancel') ?></button>
    </div>
</div>
