<?php
/**
 * @var string $locationString
 * @var string $searchText
 * @var \common\models\PrinterMaterial $material
 * @var array $searchSections
 * @var array $defaultSection
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>
    <form class="header-bar__findps">
        <div class="header-bar__findps-selector">
            <div class="dropdown js-dropdown">
                <button class="btn btn-link dropdown-toggle" type="button" id="dropdownMaterial" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $defaultSection['label']; ?>
                    <span class="tsi tsi-down"></span>
                </button>
                <ul class="dropdown-menu keep-open js-search-switch" aria-labelledby="dropdownMaterial">
                    <?php foreach ($searchSections as $item): ?>
                        <li><a href="<?php echo $item['url']; ?>"><?php echo $item['label']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="header-bar__findps-search">
            <?=
            \kartik\widgets\Typeahead::widget([
                'id'            => 'searchText',
                'name'          => 'search',
                'value'         => $searchText,
                'options'       => [
                    'placeholder' => _t('site.main', 'I\'m looking for...'),
                    'maxlength'   => \frontend\models\store\StoreSearchForm::MAX_SERACH_LENGTH,
                ],
                'container'     => [
                    'id' => 'searchTextContainer',
                ],
                'scrollable'    => true,
                'pluginOptions' => ['highlight' => true],
                'pluginEvents'  => [
                    'typeahead:select' => 'function() { $(".js-find-service-link").trigger("click") }',
                ],
                'dataset'       => [
                    [
                        'remote' => [
                            'url'     => \yii\helpers\Url::to(['/site/explore-autocomplete']),
                            'prepare' => new yii\web\JsExpression('function(query, settings) { settings.url=settings.url+"?query="+query+"&category=' . $defaultSection['category'] . '"; return settings;}')
                        ],
                        'limit'  => 5
                    ]
                ]
            ]);
            ?>
        </div>

        <div class="header-bar__findps-submit">
            <a href="<?php echo $defaultSection['url']; ?>" class="btn btn-primary btn-ghost js-find-service-link"><span class="tsi tsi-search"></span><?= _t('site.main', 'Search'); ?></a>
        </div>

        <div class="header-bar__findps-upload">
            <a href="/order-upload?utm_source=top_upload" class="btn btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 11 16" width="11" height="16">
                    <path fill="currentColor"
                          d="M9.99998 6H7.86898L10.832 1.555C10.9325 1.4044 10.9902 1.22935 10.999 1.04852C11.0077 0.867687 10.9673 0.687865 10.8819 0.528235C10.7964 0.368605 10.6693 0.235156 10.514 0.142126C10.3587 0.049095 10.181 -2.70837e-05 9.99998 1.12028e-08H4.99998C4.82372 5.68653e-05 4.6506 0.0467025 4.49817 0.135211C4.34574 0.22372 4.21941 0.350946 4.13198 0.504L0.131983 7.504C0.045124 7.65606 -0.00028081 7.82826 0.000310886 8.00338C0.000902581 8.1785 0.04747 8.35039 0.135354 8.50186C0.223239 8.65333 0.349358 8.77906 0.501095 8.86648C0.652831 8.9539 0.824864 8.99994 0.999983 9H4.27698L1.13198 14.504C1.01228 14.7143 0.972861 14.9608 1.02104 15.1979C1.06923 15.435 1.20174 15.6466 1.39401 15.7935C1.58629 15.9403 1.82529 16.0125 2.06672 15.9966C2.30815 15.9807 2.53562 15.8778 2.70698 15.707L10.707 7.707C10.8468 7.56715 10.942 7.38898 10.9806 7.19503C11.0191 7.00108 10.9993 6.80005 10.9237 6.61735C10.848 6.43465 10.7199 6.27848 10.5554 6.1686C10.391 6.05871 10.1977 6.00004 9.99998 6Z"/>
                </svg>
                <?= _t('site.main', 'Get instant quote'); ?>
            </a>
        </div>
    </form>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        var $searchText = $('#searchText'),
            $findServiceLink = $('.js-find-service-link');

        function formTopSearchUrl(searchUrl, searchValue) {
            if (searchUrl.indexOf('?') > 0) {
                searchUrl += "&"
            } else {
                searchUrl += "?"
            }
            searchUrl += "text=" + encodeURIComponent(searchValue).replace(/ /g, "+").replace(/%20/g, "+");
            return searchUrl;
        }

        $('.js-search-switch').on('click', 'a', function (e) {
            e.preventDefault();

            var $this = $(this),
                $dropdown = $this.closest('.js-dropdown');

            $('button', $dropdown).html($this.text() + '<span class="tsi tsi-down"></span>');
            $dropdown.removeClass('open');

            window.location.href = formTopSearchUrl($this.attr('href'), $searchText.val());
        });

        $findServiceLink.click(function (e) {
            e.preventDefault();

            var $this = $(this);
            window.location.href = formTopSearchUrl($this.attr('href'), $searchText.val());
            return false;
        });

        $searchText.keypress(function (e) {
            if (e.keyCode === 13) {
                $findServiceLink.trigger('click');
            }
        });

        <?php $this->endBlock(); ?>
    </script>

<?php $this->registerJs($this->blocks['js1']); ?>