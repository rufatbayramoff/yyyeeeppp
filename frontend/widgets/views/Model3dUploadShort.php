<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.01.17
 * Time: 17:20
 */

use yii\widgets\ActiveForm;

/** @var \frontend\widgets\Model3dUploadWidget $widget */

$modelName = '';
?>
<div class='model3d-upload-block'>
    <div class="js-upload-start ps-profile-upload designer-card">
        <?php
        $form = ActiveForm::begin(['id' => 'ts-uploader', 'action' => '/upload/file-upload']);
        ?>
        <h2 class="designer-card__title"><?= _t('ps.profile', 'Print 3D Models Here') ?></h2>

        <a href="#go-to-upload" class="m-t10 m-b10 fileinput-button">
            <img class="img-responsive ps-profile-upload__pic" width="283" height="167" src="/static/images/upload-model-x2.png" alt="Upload your 3D model">
        </a>
        <a href="#go-to-upload" class="fileinput-button btn btn-danger m-t10 m-b10">
            <span class="tsi tsi-upload-l m-r10"></span>
            <?= _t('ps.profile', 'Order 3D print') ?>
        </a>
        <?php $form->end(); ?>
    </div>
    <div class="upload-preview hidden">
        <div class="ts-uploader-views">
            <h3><?= _t('front.upload', '3D Models'); ?></h3>
            <div class="uploader-item-list ts-uploader-archive files" data-sortable-id="1" aria-dropeffect="move" id="ts-models-preview">
            </div>

            <h3><?= _t('front.upload', 'Model pictures'); ?></h3>
            <div class="uploader-item-list ts-uploader-archive files" data-sortable-id="1" aria-dropeffect="move" id="ts-pictures-preview">
            </div>

            <h3><?= _t('front.upload', 'Other files'); ?></h3>
            <div class="uploader-item-list ts-uploader-archives files" data-sortable-id="1" aria-dropeffect="move" id="ts-others-preview">
            </div>

            <div class="ts-uploader-template dz-preview uploader-item hidden">
                <img data-dz-thumbnail class="model3d-upload-image-preview" alt="">
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
            </div>
        </div>
        <div id="ts-upload-creating-model3d-progress" class="hidden">
            <?= _t('site.ps', '3D Model processing...'); ?><img class="m-l10" src="/static/images/loading.gif"  alt="loading...">
        </div>
        <button id="ts-uploader-cancel" class="btn btn-default ts-uploader-cancel" onclick="model3dUploadObj.cancelUpload()"><?= _t('site.ps', 'Cancel') ?></button>
    </div>
</div>

