<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.12.16
 * Time: 11:58
 */

use frontend\widgets\ReviewStarsWidget;

/** @var \common\models\Company $company */
/** @var int $reviewsCount */
/** @var float $reviewsRating */

if ($withoutJs) { ?>
    <span style="color:orange;letter-spacing: 1px;">
                <?php for ($i = 0; $i < $reviewsRating; $i++): ?>
                    <span class="tsi tsi-rating-star"> </span>
                <?php endfor; ?>
        <?php for ($i = 0; $i < $reviewsRating - 5; $i--): ?>
            <span style="color:#ccc;letter-spacing: 1px;" class="tsi tsi-rating-star"> </span>
        <?php endfor; ?>
            </span>
    <?php
    echo sprintf(
        '<span class="star-rating-range"><strong>%s</strong>/5</span>
                <span class="star-rating-count">(<span itemprop="reviewCount">%s</span> %s)</span>',

        round($reviewsRating, 1),
        $reviewsCount,
        $reviewsCount > 1 ? _t('ps.profile', 'reviews') : _t('ps.profile', 'review'));
} else {
    $url = \yii\helpers\Url::to(['/c/public/reviews','username' => $company->url]);
    echo ReviewStarsWidget::widget(
        [
            'rating'     => $reviewsRating,
            'withSchema' => $withSchema,
            'company'    => $company,
            'html'       => sprintf(
                '<span class="star-rating-range"><strong>%s</strong>/5</span>
                <span class="star-rating-count"><a href="%s">(<span itemprop="reviewCount">%s</span> %s)</span></a>',
                round($reviewsRating, 1),
                $url,
                $reviewsCount,
                $reviewsCount > 1 ? _t('ps.profile', 'reviews') : _t('ps.profile', 'review')
            )
        ]
    );
}
?>
