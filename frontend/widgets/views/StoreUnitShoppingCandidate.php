<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.11.16
 * Time: 11:16
 */
use common\models\StoreUnitShoppingCandidate;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;
use yii\helpers\Html;
?>


<div class="widget-api<?=$isMinimized?' widget-api--minified':''?>">

    <a href="#widget-expand" class="widget-api__expand" onclick="storeUnitShoppingCandidate.resetWindowSize();return false;"><span class="tsi tsi-check-list"></span></a>

    <a href="#widget-minify" class="widget-api__min" onclick="storeUnitShoppingCandidate.minimizeWindow();return false;">&times;</a>

    <div class="widget-api__title">
        <?= _t('front.site', 'Your recent models') ?>
    </div>

    <div class="widget-api__wrap">
        <?php
        /** @var StoreUnitShoppingCandidate[] $shoppingCandidates */
        $position = 1;
        foreach ($shoppingCandidates as $shoppingCandidate) {
            $model3d = $shoppingCandidate->storeUnit->model3d;
            $coverImage = Model3dFacade::getCover($model3d);
            $coverImage['image'] = ImageHtmlHelper::getThumbUrl($coverImage['image'], ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
            $model3dPrintUrl = Model3dFacade::getStoreUrl($model3d);
            $displayModeClass = ($position > 3) ? ' hidden' : '';
            $position++;
            ?>
            <div class="widget-api__model js-shopping-candidate-element<?= $displayModeClass ?>" id="store-unit-shopping-candidate-<?= $shoppingCandidate->id; ?>">
                <a href="<?= $model3dPrintUrl ?>" class="widget-api__model-pic">
                    <img src="<?= $coverImage['image'] ?>" alt="<?= Html::encode($model3d->title) ?>">
                </a>
                <a href="<?= $model3dPrintUrl ?>" class="widget-api__model-title">
                    <?= Html::encode($model3d->title) ?>
                </a>
                <div class="widget-api__model-btn">
                    <a href="<?= $model3dPrintUrl ?>" class="btn btn-danger btn-sm" title="<?= _t('front.site', 'Print this model') ?>">
                        <?= _t('front.site', 'Print') ?>
                    </a>
                </div>
                <div class="widget-api__model-btn">
                    <a href="#" onclick="storeUnitShoppingCandidate.deleteModelFromList(<?= $shoppingCandidate->id; ?>)" title="<?= _t('front.site', 'Remove'); ?>"
                       class="widget-api__delete-btn">
                        <span class="tsi tsi-remove"></span>
                    </a>
                </div>
            </div>
            <?php
        }
        ?>

        <div class="widget-api__show-more">
            <?php
            if (count($shoppingCandidates) > 3) {
            ?>
            <button class="btn btn-info btn-sm widget-api__show-more-btn js-shopping-candidate-expand" onclick="storeUnitShoppingCandidate.switchShortLongList();">
                <?= count($shoppingCandidates) . _t('front.site', ' Models') ?>
            </button>
            <?php } ?>
            <a href="/upload?utm_source=ts_main" class="widget-api__upload-btn">
                <span class="tsi tsi-upload-l"></span>
                <?= _t('front.site', 'Upload more models') ?>
            </a>
        </div>

    </div>

</div>