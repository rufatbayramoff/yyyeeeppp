<?php
/** @var \yii\web\View $this */
/** @var Model3d $model3d */

/** @var string $mode */


use common\models\Model3d;
use common\models\base\Model3d as BaseModel3d;
use common\services\LikeService;
use common\models\UserLike;
use frontend\components\UserUtils;
use frontend\models\model3d\Model3dFacade;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use frontend\components\image\ImageHtmlHelper;
use frontend\widgets\StoreUnitWidget;

/** @var FrontUser $user Current user */
$user = UserFacade::getCurrentUser();

/** @var bool $isOwner current user is owner store unit */
$isOwner = UserFacade::isObjectOwner($model3d);

/**
 * Resolve user like this model
 *
 * @param $user
 * @param $model
 * @param $isOwner
 * @return bool|null
 */
$isLikedFn = function ($user, BaseModel3d $model, $isOwner) {
    if (is_guest() || !$user) {
        return false;
    }
    if (!($user instanceof common\models\User)) {
        return false;
    }

    return LikeService::isModelLikedByUser($model, $user);
};

/** @var bool $isLiked is model liked for current user */
$isLiked = $isLikedFn($user, $model3d, $isOwner);


$category = $model3d->productCategory;

$cover = Model3dFacade::getCover($model3d);

$cover2 = new common\models\model3d\CoverInfo(Model3dFacade::getCover($model3d));

$itemData = [
    'model3d-id' => $model3d->id,
    'is-liked' => $isLiked,
];

$likesCount = LikeService::getModelLikesCount($model3d);
?>
<div class="<?= $mode == StoreUnitWidget::MODE_DESIGNER ? 'col-xs-12 col-sm-6 col-md-4' : 'col-xs-12 col-sm-4 col-md-3' ?>">

    <?= \yii\helpers\Html::beginTag('div', ['class' => 'catalog-item', 'data' => $itemData]) ?>
    <div class="catalog-item__pic">
        <?php if (!empty($model3d->category_id)): ?>
            <div class="catalog-item__category">
                <a class="label label-primary" href="<?php
                echo \yii\helpers\Url::toRoute(['/product/product/index', 'category' => $category->id])
                ?>">
                    <?php echo $category->title; ?>
                </a>
            </div>
        <?php endif; ?>
        <?php if ($mode != 'mini'): ?>
            <div class="catalog-item__author">
                <?php if (!$hideAuthor): ?>
                    <a href="<?= BaseModel3d::getUserStoreUrl($model3d->user); ?>" class="catalog-item__author__avatar">
                        <?php echo \frontend\components\UserUtils::getAvatarForUser($model3d->user, 'store'); ?>
                    </a>
                    <a class="catalog-item__author__name"
                       href="<?= BaseModel3d::getUserStoreUrl($model3d->user); ?>"><?php echo H(UserFacade::getFormattedUserName($model3d->user)); ?></a>
                <?php endif; ?>
            </div>

            <div class="catalog-item__controls">


                <?php if (true || !$isOwner): ?>

                    <div class="catalog-item__controls__item js-store-item-like <?= $isLiked ? 'js-store-item-like-showed' : '' ?>">
                        <span class="tsi <?= $isLiked ? 'tsi-heart' : 'tsi-heart' ?>"></span>
                        <span class="catalog-item__controls__item__hint js-store-item-like-label"><?= _t('front.catalog', $isLiked ? 'Unlike' : 'Like') ?></span>
                    </div>

                <?php endif; ?>


                <div class="catalog-item__controls__item"><a href="<?php echo $itemLink; ?>">
                        <span class="tsi tsi-printer3d"></span>
                        <span class="catalog-item__controls__item__hint"><?= _t('front.site', 'Print') ?></span></a>
                </div>
            </div>
        <?php endif; ?>
        <a class="catalog-item__pic__img" href="<?php echo $itemLink; ?>" title="<?= $title; ?>">
            <img src="<?= ImageHtmlHelper::getThumbUrl($cover['image'], ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT); ?>"
                 alt="<?= $title; ?>"
                 style="max-width: 100%;">
        </a>
    </div>

    <div class="catalog-item__footer">
        <h4 class="catalog-item__footer__title"><a href="<?php echo $itemLink; ?>"> <?php echo $title; ?></a></h4>

        <?php
        if (!$hidePrice) {
            if ($jsCalculatePrice) {
                ?>
                <div class="catalog-item__footer__price js-catalog-price catalog-price-store-unit-<?= $model3d->storeUnit->id ?>"
                     data-store-unit-id="<?= $model3d->storeUnit->id ?>"
                    <?= $jsCalculatePriceForPrinter ? 'data-cost-for-printer-id="' . $jsCalculatePriceForPrinter->id . '"' : '' ?>
                >
                </div>
                <?php
            } else {
                ?>
                <div class="catalog-item__footer__price">
                    <?= _t('front.site', 'from') ?>
                    <?= displayAsMoney($model3d->getPriceMoneyByQty(1)); ?>
                </div>
                <?php
            }
        }
        if (!$hideLikes) {
            ?>
            <div class="catalog-item__footer__stats">
                <div class="catalog-item__footer__stats__likes js-hide-like-if-not-liked"
                     data-likes="<?= $likesCount ?>">
                    <span class="tsi tsi-heart"></span> <span
                            class="js-store-item-likes-count"><?= LikeService::getLikesCountString($likesCount) ?></span>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?= \yii\helpers\Html::endTag('div'); ?>
</div>

 