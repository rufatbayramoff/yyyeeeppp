<?php

use common\models\Ps;
use frontend\models\ps\PsFacade;

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.02.17
 * Time: 17:53
 */

/** @var Ps $ps */
?>
<div class="item-rendering-external-widget__header">
    <h2>
        <?= _t('front.upload', 'Get quote'); ?>
    </h2>
    <?= $this->render('CompanyTitle', ['company' => $ps]) ?>
</div>

