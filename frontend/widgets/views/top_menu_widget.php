<?php

use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 *  @var array $menuItems
 *  @var array $services
*/
?>
<ul id="tabs" class="header-nav nav nav-tabs <?php echo $this->context->navClass?>">
    <li>
        <?php echo Html::a(_t('site.main', 'Products'),[ProductUrlHelper::productsMain()])?>
    </li>
    <li>
        <div class="dropdown dropdown-onhover">
            <button class="btn btn-default dropdown-toggle header-nav-drop__btn" type="button" id="dropdownMenuTop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?= _t('site.main', 'Manufacturing services'); ?> <span class="tsi tsi-down"></span>
            </button>
            <div class="dropdown-menu header-nav-drop" aria-labelledby="dropdownMenuTop">
                <div class="header-nav-drop__content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header-nav-drop__big-list">
                                <?php foreach ($menuItems as $menu):?>
                                <a class="header-nav-drop__big" href="<?php echo $menu['url']?>">
                                    <img alt="<?php echo $menu['label'];?>" class="header-nav-drop__big-icon" src="https://static.treatstock.com/static/images/common/service-cat/<?php echo $menu['img'];?>.svg">
                                    <span class="header-nav-drop__big-text"><?php echo $menu['label'];?></span>
                                </a>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="header-nav-drop__title"><?php echo _t('site.main', 'Services'); ?></div>
                            <div class="header-nav-drop__list">
                                <?php foreach($services as $service):?>
                                <div class="header-nav-drop__list-item">
                                    <?php echo Html::a($service['text'],$service['url'])?>
                                </div>
                                <?php endforeach;?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
</ul>