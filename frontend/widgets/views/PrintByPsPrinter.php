<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.12.16
 * Time: 17:01
 */

use common\components\ArrayHelper;
use common\interfaces\Model3dBaseInterface;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\UserCollection;
use common\services\Model3dService;
use common\services\PsPrinterService;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use frontend\widgets\StoreUnitWidget;
use kartik\select2\Select2;

/** @var Model3dBaseInterface[] $myModels */
/** @var PsPrinter $psPrinter */
/** @var Ps $ps */
/** @var UserCollection[] $collections */

$user = UserFacade::getCurrentUser();
$currentUrl = PsFacade::getPsLink($ps);
if ($psPrinter) {
    $uploadUrl = '/upload?utm_source=ts_seleted_printer&ps_printer_id=' . $psPrinter->id;
    $additionalUrlParams = [
        'psPrinterId' => $psPrinter->id
    ];
    $hidePrice = false;
} else {
    $uploadUrl = '/order-upload?utm_source=top_upload&psId='.$ps->id;
    $additionalUrlParams = [
        'psId'            => $ps->id,
    ];
    $hidePrice = true;
}
?>
<!-- Modal -->
<div class="over-nav-tabs-header">
    <div class="container-fluid">
        <h2><?= _t('ps.profile', 'Select model from') ?></h2>
    </div>
</div>
<div class="nav-tabs__container">
    <ul class="nav nav-tabs" role="tablist">
        <li class="active">
            <a href="#tabUpload" aria-controls="tabUpload" role="tab" data-toggle="tab">
                <?= _t('ps.profile', 'Upload model') ?>
            </a>
        </li>
        <li>
            <a href="#tabMyModels" aria-controls="tabMyModels"
               role="tab" <?= $user ? 'data-toggle="tab"' : 'onclick="TS.Visitor.loginForm(\'' . $currentUrl . '\'); return false;"' ?> >
                <?= _t('ps.profile', 'My Models') ?>
            </a>
        </li>
        <li>
            <a href="#tabCollections" aria-controls="tabCollections"
               role="tab" <?= $user ? 'data-toggle="tab"' : 'onclick="TS.Visitor.loginForm(\'' . $currentUrl . '\'); return false;"' ?>>
                <?= _t('ps.profile', 'Collections') ?>
            </a>
        </li>
    </ul>
</div>

<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="tabUpload">
        <div class="ps-print-modal__upload">
            <h4 class="m-t0"><?= _t('ps.profile', 'Please upload your 3D models that you would like to print.') ?></h4>
            <p class="text-muted"><?= _t('ps.profile', 'STL, PLY, OBJ, 3MF, CDR, DXF, EPS, PDF or SVG files are supported') ?></p>
            <a href="<?= $uploadUrl ?>" class="btn  btn-primary" target="_blank">
                <span class="tsi tsi-upload-l m-r10"></span>
                <?= _t('ps.profile', 'Upload Model') ?>
            </a>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="tabMyModels">
        <?php if (!$user) {
            echo _t('ps.profile', 'Please sign in to view your models');
        } else {
            ?>
            <div class="row ps-print-modal__models-list ps-print-modal__models-list--bigblock">
                <?php if (!$myModels) {
                    echo "<h4 class='text-center m-t30'>";
                    echo _t('ps.profile', 'You do not have any models yet');
                    echo "</h4>";
                } else {
                    foreach ($myModels as $myModel) { ?>
                        <?php
                        echo StoreUnitWidget::widget(
                            [
                                'storeUnit'                  => $myModel->storeUnit,
                                'mode'                       => StoreUnitWidget::MODE_MINI,
                                'hideAuthor'                 => true,
                                'additionalUrlParams'        => $additionalUrlParams,
                                'hideLikes'                  => true,
                                'hidePrice'                  => $hidePrice,
                                'jsCalculatePrice'           => true,
                                'jsCalculatePriceForPrinter' => $psPrinter
                            ]
                        );
                        ?>
                    <?php }
                } ?>
            </div>
            <?php
        }
        ?>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="tabCollections">
        <?php if (!$user) {
            echo _t('ps.profile', 'Please sign in to view your models');
        } else {
            ?>
            <div class="ps-print-modal__collection-select">
                <label><?= _t('ps.profile', 'Collection') ?></label>
                <?= Select2::widget(
                    [
                        'name'          => 'ps-print-modal__collection-select',
                        'hideSearch'    => true,
                        'data'          => ArrayHelper::map($collections, 'id', 'title'),
                        'options'       => [
                            'multiple' => false,
                            'onchange' => 'printByPsPrinterObj.changeCategory(this.value)'
                        ],
                        'pluginLoading' => false,
                    ]
                ); ?>
            </div>

            <div class="row ps-print-modal__models-list">
                <?php
                $isFirstCollection = true;
                foreach ($collections as $collection) {
                    ?>
                    <div class="ps-printer-modal__category_models<?= $isFirstCollection ? '' : ' hidden' ?>" id="ps-printer-modal-category-list-<?= $collection->id ?>">
                        <?php
                        /** @var Model3dService $model3dService */
                        $model3dService = Yii::createObject(Model3dService::class);
                        if ($collection->model3ds) {
                            foreach ($collection->model3ds as $model3d) {
                                if ($model3dService->isAvailableForCurrentUser($model3d) && $model3d->isCalculatedProperties()) {
                                    echo StoreUnitWidget::widget(
                                        [
                                            'storeUnit'                  => $model3d->storeUnit,
                                            'mode'                       => StoreUnitWidget::MODE_MINI,
                                            'hideAuthor'                 => true,
                                            'additionalUrlParams'        => $additionalUrlParams,
                                            'hideLikes'                  => true,
                                            'hidePrice'                  => $hidePrice,
                                            'jsCalculatePrice'           => true,
                                            'jsCalculatePriceForPrinter' => $psPrinter
                                        ]
                                    );
                                }
                            }
                        } else {
                            echo "<h4 class='text-center m-t30'>";
                            echo _t('ps.profile', 'You do not have any models yet');
                            echo "</h4>";
                        }
                        ?>
                    </div>
                    <?php
                    $isFirstCollection = false;
                }
                ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>
