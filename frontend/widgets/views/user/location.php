<?php
/**
 * @var string $btnTitle
 * */
?>
<?php echo '<div class="UserLocationWidget">' ;?>

<?php echo frontend\components\Icon::get('map-marker'); ?>

<?php 

echo \yii\helpers\Html::button(H($location),[
    'data-url' => $dataUrl,
    'class'=>'ts-user-location ts-ajax-modal btn-link',
    'title'=> _t('site.user', $btnTitle),
    'data-target' => '#changelocation'
]) ;

echo '</div>';