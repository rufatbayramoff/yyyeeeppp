<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$model3dId = $this->context->model3dId;
$actionAdd = yii\helpers\Url::toRoute(['my/addtocollection']);
$collections = $this->context->userCollections;
$modelCollections = $this->context->modelCollections;
$mList = \lib\collection\CollectionDb::getList($modelCollections, 'id', 'title'); 

if(count($mList) > 1){
    $collectionAddText = _t('front.site', 'In {total} Collections ', ['total'=>count($mList)]); 
}else if(count($mList)==1){
    $collectionAddText = _t('front.site', 'In: {title} ', ['title'=>array_values($mList)[0]]);
}else{
    $collectionAddText = '<span class="tsi tsi-heart m-r10"></span>' . _t('front.site', 'Add To Collection');
}
$link = app('request')->url;
\yii\widgets\Pjax::begin(['enablePushState'=>false]);
?>
<a href="<?php echo $link ?>" class="pjax-refresh"></a>
<!-- Split button -->
<div class="btn-group dropdown-default collection-widget" id="collection-<?php echo $model3dId; ?>">
 <button type="button" class="btn btn-default collection-title <?php echo count($mList) > 0 ? "btn-default" : "collection-add"; ?>">
        <?php echo $collectionAddText; ?>
 </button>
  <button type="button" class="btn btn-default dropdown-toggle dropdown-menu-right-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="tsi tsi-down"></span>
    <span class="sr-only"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
    <?php 
    foreach($collections as $collection):
        if($collection->title == 'Wish List'){
            $collection->title  = _t('site.user', 'Wish List');
        }
        if($collection->title == 'My Favorites'){
            $collection->title  = _t('site.user', 'My Favorites');
        }
    if(in_array($collection->id, array_keys($mList))):
            ?>
      <li><a href="#" data-pjax="0"  class="collection-reject"  data-id="<?php echo $collection->id; ?>"><?php echo frontend\components\Icon::get('checkmark-c'); ?> <?php echo $collection->title; ?></a></li>
    <?php else: ?>
      <li><a href="#" data-pjax="0"  class="collection-select" data-id="<?php echo $collection->id; ?>"><?php echo \H($collection->title); ?></a></li>
    <?php endif; ?>
    <?php 
    endforeach; 
    ?>
    <li role="separator" class="divider"></li>
    <?php if(!is_guest()): ?>
    <li > 
    <div class="input-group m-r10 m-l10">
          <input type="text" class="newcollection form-control" placeholder="<?= _t('front.site', 'New collection')?>" name="newcollection" />
        <span class="input-group-btn">
          <button class="btn btn-default addcollection" type="button"><?= _t('front.site', 'Add')?></button>
        </span>
      </div>
    </li>
    <?php endif; ?>
  </ul>
</div>
<?php
\yii\widgets\Pjax::end();
?>

<script>
<?php $this->beginBlock('js_1'); ?>
var modelId = <?php echo $model3dId; ?>;    
var callback = <?php echo !empty($this->context->jsCallback)?$this->context->jsCallback:false; ?>; 
var isGuest = <?php echo intval(is_guest()); ?>;
$(document).ready(function() {
    TS.User.initCollectionAddWidget(isGuest, modelId, callback);
});
<?php $this->endBlock('js_1'); ?>
</script>
<?php
$this->registerJs($this->blocks['js_1']);
