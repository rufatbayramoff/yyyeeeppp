<?php
use yii\helpers\Html;
use yii\helpers\Url;
/** @var \yii\web\View $this */
$this->registerAssetBundle(\frontend\assets\UserAsset::class);
$resendUrl = Url::to('@web/user/resend-email');
if(empty($data['email'])){ return '';}
?>
<div id="widget-user-confirm" class="widget-user-confirm alert-warning">
    <?php
    echo ' <span class="widget-user-confirm__text">';
        $data['status'] = sprintf('<span class="label label-danger">%s</span>', _t('front', 'unconfirmed'));
        $data['email'] = H($data['email']);
//        echo _t('front', 'Hi {name}, your account is {status}', $data);
        echo _t('front', 'Thank you for signing up! A message with a confirmation link was sent to {email}', $data);
        echo '. <a href="/help/article/162-why-should-i-confirm-my-account">';
        echo _t('front', 'Why should I confirm my account?');
        echo '</a>';
        $err = _t('front', 'Error request. Please try again');
    echo '</span>';

    echo ' <span class="widget-user-confirm__btns">';
        echo Html::a(_t('front', 'Resend email'), '#', [
            'title' => _t('front', 'Resend email to confirm it'),
            'onclick' => 'TS.User.resendEmail("'.$resendUrl.'");return false;',
            'class' => 'btn btn-primary btn-sm m-l10 m-r10 resend-email'
        ]);

        ?>

        <?php
        echo Html::a(
            _t('front', 'Change your email'),
            Url::to('/profile/profile/change-email'),
            [
                'title' => _t('front', 'Change your email'),
                'class' => 'btn btn-info btn-sm ts-ajax-modal change-email',
                /*'data-target' => '#userChangeEmail',*/
                'callback' => 'function(){
                     console.log($("#wuser-confirm-result").innerHTML = "E-mail changed"));

                }'
            ]
        );
    echo '</span>';
    ?>
</div> 