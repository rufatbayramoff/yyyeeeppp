<?php
$items = $this->context->items;
$current = $this->context->active;
$id = 'switcher-'.rand(0,1000);
?>
<div class="btn-group" id="<?php echo $id; ?>" data-toggle="buttons">
    <?php foreach($items as $k=>$v): 
            $active = $current==$v;
        ?>
    <label class="btn btn-default <?php echo $active ? 'active' : ''; ?>">
      <input type="radio" name="options" data-value="<?php echo $v; ?>" class="switcher-trigger" autocomplete="off" <?php echo $active ? 'checked' : ''; ?>> <?php echo $v; ?>
    </label>
    <?php endforeach; ?> 
</div>
 
<script>
<?php $this->beginBlock('js_1', false); ?> 
    var callback = <?php echo !empty($this->context->jsCallback)?$this->context->jsCallback:false; ?>;
    $('<?php echo "#" . $id; ?> .switcher-trigger').change(function(e){
        e.preventDefault();
        var val = $(this).data('value');     
        var data = <?php echo empty($this->context->ajaxParams) ? "{}" : yii\helpers\Json::encode($this->context->ajaxParams); ?>;
        data.newValue = val;
        $.ajax({
            url: '<?php echo $this->context->actionUrl; ?>',
            type: "POST",
            data: data,
            dataType: 'json', 
            success: function (result) {
                if (result.success === true) {
                   callback(val, result);
                } else {
                }

            }
        });
    });
<?php $this->endBlock(); ?>
</script>

<?php
$this->registerJs($this->blocks['js_1']);
?>