<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.03.17
 * Time: 14:05
 */
/* @var $this \yii\web\View */
/* @var $widgetMode bool */
use frontend\models\user\UserFacade;

$gatrackId = app('setting')->get('site.ga_analytics', false);
$userId = UserFacade::getCurrentUserId();
?>

<script>
    function ga(type, cats, params) {
        if (console) {
            if (typeof params == undefined) {
                console.info('GA DEBUG:', type, cats);
            } else {
                console.info('GA DEBUG:', type, cats, params);
            }
        }
    }
    <?php if($userId) { ?>
    ga('set', 'userId', '<?=$userId?>');
    <?php }?>

    <?php
    if($widgetMode):
        $this->registerJs('var mouseOver = false;
        document.body.onclick = function(){
            if(!mouseOver)
                ga(\'send\', \'pageview\');
            mouseOver  = true;
        }');
    endif; ?>
</script>

