<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.06.16
 * Time: 13:41
 */

use common\components\JsObjectFactory;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use frontend\widgets\assets\ColorSelectorWidgetAsset;
use frontend\widgets\ColorSelectorWidget;
use kartik\select2\Select2Asset;
use yii\helpers\Html;

/** @var $widget ColorSelectorWidget */

if (count($widget->materialList) === 0) {
    // No materials
    return;
}

$containerId = 'colorSelectorHtmlContainer';

ColorSelectorWidgetAsset::register($this);
Select2Asset::register($this);
$containerId = 'color-selector-block';
JsObjectFactory::createJsObject(
    'colorSelectorClass',
    'colorSelectorWidgetObj',
    [
        'colorSelectorContainerId' => $containerId,
        'startColorId'             => $widget->selectedColor->id,
        'startMaterialGroupId'     => $widget->selectedMaterialGroup->id,
        'reloadLink'               => $widget->reloadLink,
        'selectUsage'              => $widget->selectUsage,
        'model3dFormObjName'       => 'model3dFormObj'
    ],
    $this
);
if ($widget->materialListUsageFilter) {
    $groupsUsage = \common\models\PrinterMaterialGroup::getUsageGroups($widget->materialListUsageFilter);
} else {
    $groupsUsage = null;
}
?>
    <div id='<?= $containerId ?>' class="color-selector-block">

        <div class="color-selector-material-and-color-selectors<?= $widget->isCantSelectMaterial ? '' : ' hidden' ?>">
            <?php
            if ($groupsUsage) {
                ?>
                <div class="model-page__sidebar-title">
                    <?= _t('site.model3d', "Product's Application") ?>
                </div>
                <select class="color-selector-select2 usage" name="<?= $widget->formName ?>[materialGroupUsageFilter]">
                    <?php
                    foreach ($groupsUsage as $groupUsage) {
                        $isSelectedUsage = ($widget->selectUsage === $groupUsage['id']);
                        ?>
                        <option value="<?= $groupUsage['id'] ?>" " <?= $isSelectedUsage ? 'selected' : '' ?>
                        data-groups="<?= implode(',', $groupUsage['groups']); ?>"
                        data-image-url="<?= $groupUsage['image']; ?>"
                        data-short-description="<?= H($groupUsage['description']) ?>">
                        <?= H($groupUsage['title']) ?>
                        </option>
                    <?php } ?>
                </select>
                <?php
            }
            ?>


            <div class="model-page__sidebar-title">
                <?= _t('site.model3d', 'Material & color') ?>
            </div>
            <div class="color-selector-message-select-model3dpart<?= ($widget->isCantSelectMaterial ? ' hidden' : '') ?>">
                <?= _t('site.colorSelector', 'You can select individual parts of the KIT and choose colors/materials of each model part.'); ?>
            </div>
            <div class="color-selector-is-one-material-for-kit-group<?= ($widget->isModelKit ? '' : ' hidden') ?>">
                <?php
                echo Html::checkbox(
                    $widget->formName . '[isOneMaterialForKit]',
                    $widget->isOneMaterialForKit,
                    [
                        'class' => 'color-selector-is-one-material-for-kit',
                        'label' => _t('site.colorSelector', 'Set one material and color for kit')
                    ]
                );
                ?>
            </div>
            <?php
            if (count($widget->materialList) === 1) {
                /** @var PrinterMaterial $materialGroup */
                $materialGroup = reset($widget->materialList)->materialGroup;
                ?>
                <div class="color-selector-one-material color-switcher__btn">
                    <div class="color-selector-material-text-block">
                        <span class="color-selector-material-title"><?= H($materialGroup->title) ?></span>
                        <span class="color-selector-material-short-description"><?= H($materialGroup->short_description) ?></span>
                        <?php
                        if ($materialGroup->long_description) {
                            ?>
                            <span class="color-selector-material-link-long-description"><?= _t('site.store', 'Learn more') ?></span>
                            <span class="color-selector-material-long-description"><?= H($materialGroup->long_description) ?></span>
                            <span class="color-selector-material-link-short-description"><?= _t('site.store', 'Hide') ?></span>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <select class="color-selector-select2 group">
                    <?php
                    foreach ($widget->materialList as $materialInfo) {
                        $materialGroup = $materialInfo->materialGroup;
                        $isSelectedMaterialGroup = ($widget->selectedMaterialGroup->id === $materialGroup->id);
                        ?>
                        <option value="<?= $materialGroup->id ?>" <?= $isSelectedMaterialGroup ? 'selected' : '' ?>
                                data-image-url="<?= $materialGroup->photoFile ? $materialGroup->photoFile->getFileUrl() : $materialGroup->getDefaultPhotoFileUrl() ?>"
                                data-short-description="<?= H($materialGroup->short_description) ?>"
                                data-long-description="<?= H($materialGroup->long_description) ?>"
                        >
                            <?= H($materialGroup->title) ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
                <?php
            }
            ?>
            <?php
            foreach ($widget->materialList as $materialInfo) {
                $materialGroup = $materialInfo->materialGroup;
                $colors = $materialInfo->colors;
                $isSelectedMaterialGroup = ($widget->selectedMaterialGroup->id === $materialGroup->id);
                ?>
                <div class="color-selector-printer-color-block color-switcher__material-list <?= $isSelectedMaterialGroup ? 'color-selector-printer-color-block-active' : '' ?>"
                     id="<?= $containerId . '_material_' . $materialGroup->id ?>">
                    <div class="color-switcher__color-list">
                        <select class="color-selector-select2 color-select-js">
                            <?php
                            foreach ($colors as $color) {
                                $isSelectedColor = ($widget->selectedColor->id === $color->id);
                                ?>
                                <option value="<?= $color->id ?>" <?= $isSelectedColor ? 'selected' : '' ?>
                                        class="color-selector-printer-color-block-element"
                                        data-item-html="<div class='color-selector-printer-color-view material-item__color' style='background-color: #<?= H($color->getRgbHex()) ?>'></div><div class='color-selector-printer-color-title material-item__label'><?= H($color->title) ?></div>"
                                        data-material-group-id="<?=$materialGroup->id?>" data-color-id="<?= $color->id ?>" data-color-code="<?= $color->render_color ?>"
                                ><?= H($color->title) ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
<?php

