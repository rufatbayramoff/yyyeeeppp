<?php
use common\modules\catalogPs\CatalogUrlRule;
?>
<ul class="dropdown-menu" aria-labelledby="dropdownHeroMaterial">
    <?php
    foreach($materials as $material): /* @var \common\models\PrinterMaterial $material */?>
        <li><a href="<?= CatalogUrlRule::getPsCatalogUrl(['filter'=>'material-'.$material->getSlugifyFilament()]);
            ?>" class="btn btn-link btn-sm"  title="<?= $material->title; ?>"><?= $material->title; ?></a></li>
    <?php endforeach; ?>
    <li><a class="btn btn-link btn-sm"  href="<?= CatalogUrlRule::getPsCatalogUrl();?>"><?=_t('site.front', 'More materials');?></a></li>
</ul>


