
<?php
use \frontend\components\Icon;

/** @var \yii\web\View $this */
$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);
?>
  
<script type="text/javascript" src="/js/marker-cluster-min.js"></script>


<script>
    var prevoius_infowindow;

    function initialize() {
        setTimeout(function() { // TODO: Change it to ready function
            var styles = [[{
                url: '<?= Icon::getPath('place_level_1'); ?>',
                height: 35,
                width: 30,
                anchor: [4, 0],
                textColor: '#f0ff3d',
                textSize: 10
            }, {
                url: '<?= Icon::getPath('place_level_2'); ?>',
                height: 46,
                width: 40,
                anchor: [8, 0],
                textColor: '#ff0000',
                textSize: 11
            }, {
                url: '<?= Icon::getPath('place_level_3'); ?>',
                width: 50,
                height: 58,
                anchor: [12, 0],
                textSize: 12
            }]];

            var imageUrl = '<?= Icon::getPath('place_item'); ?>';

            var json = <?= \yii\helpers\Json::htmlEncode($this->context->data_locations)?>;

            var center = new google.maps.LatLng(json.markers[0].lat, json.markers[0].lon);

            map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 12,
                center: center,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow({
                content: ''
            });

            if (json.status == 'OK') {

                var markers = [];
                var markerImage = new google.maps.MarkerImage(imageUrl, new google.maps.Size(24, 28));

                for (var i = 0; i < json.markers.length; i++) {

                    var baloon = '<div><strong>' + json.markers[i].title + '</strong></div>';
                    var marker = add_marker(json.markers[i], baloon, markerImage);
                    markers.push(marker);
                }

                var mcOptions = {gridSize: 50, maxZoom: 15, styles: styles[0]};
                markerClusterer = new MarkerClusterer(map, markers, mcOptions);
            }
        }, 2000);
    }

    function add_marker(object, box_html, markerImage) {

        var infowindow = new google.maps.InfoWindow({
            content: box_html
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(object.lat, object.lon),
            map: map,
            title: object.title,
            icon: markerImage
        });

        google.maps.event.addListener(marker, 'click', function () {
            if(prevoius_infowindow) {
                prevoius_infowindow.close();
            }
            infowindow.open(map, marker);
            prevoius_infowindow = infowindow;
        });

        return marker;
    }
    
</script>

<div id="map-canvas" style="width: <?= $this->context->width; ?>px; height: <?= $this->context->height; ?>px;"></div>
