<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.01.17
 * Time: 17:58
 */
use common\components\order\PriceCalculator;
use common\services\Model3dPartService;
use common\services\Model3dService;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;
use frontend\widgets\Model3dHiddenForm;
use yii\bootstrap\ActiveForm;

/** @var \common\models\Model3dViewedState $model3dViewedState */

$model3d = $model3dViewedState->model3dRestored;
Model3dService::setCheapestMaterialInPrinter($model3d, $psPrinter);
$cost = PriceCalculator::calculateModel3dAndStoreUnitPrice($model3d, $psPrinter, true, null, $model3dViewedState->model3dQty);

$activeImages = $model3d->getActiveModel3dImages();
$activeParts = $model3d->getActiveModel3dParts();

if ($model3d->isOneTextureForKit()) {
    $texture = $model3d->getKitModel3dTexture();
    $colorTitle = $texture->printerColor->title;
    $materialGroupTitle = $texture->calculatePrinterMaterialGroup()->title;
} else {
    $colorTitle = _t('site.model3d', 'Different colors');
    $materialGroupTitle = _t('site.model3d', 'Different materials');
}

?>

<div class="ps-profile-model">
    <h4 class="ps-profile-model__name">
        <a href="<?= Model3dFacade::getStoreUrl($model3d) ?>"><?= H($model3d->title) ?></a>
    </h4>

    <img src="<?= Model3dFacade::getCover($model3d)['image'] ?>" alt="" class="ps-profile-model__pic">

    <table class="ps-profile-model__props">
        <tr>
            <td class="ps-profile-model__props-label"><?= _t('site.model3dViewedState', 'Material') ?>:</td>
            <td class="ps-profile-model__props-info"><?= $materialGroupTitle ?></td>
        </tr>

        <tr>
            <td class="ps-profile-model__props-label"><?= _t('site.model3dViewedState', 'Color') ?>:</td>
            <td class="ps-profile-model__props-info"><?= $colorTitle ?></td>
        </tr>
    </table>
    <?php
    if ((count($activeParts) + count($activeImages)) > 1) {
        ?>
        <div class="ps-profile-model__parts <?= (count($activeParts) > 1) ? 'collapse' : '' ?>" id="collapseModelParts">
            <?php
            foreach ($activeParts as $model3dPart) {
                $sizes = $model3dPart->getSize();
                if(!$sizes) continue;
                ?>
                <div class="ps-profile-model__parts-row">
                    <?php
                    $imgUrl = Model3dPartService::getModel3dPartRenderUrlIfExists($model3dPart);
                    $imgUrl = $imgUrl ? ImageHtmlHelper::getThumbUrl(
                        $imgUrl,
                        ImageHtmlHelper::DEFAULT_WIDTH,
                        ImageHtmlHelper::DEFAULT_HEIGHT
                    ) :
                        Yii::$app->params['staticUrl'] . Model3dFacade::EMPTY_RENDER_IMAGE;
                    ?>
                    <img src="<?= $imgUrl ?>" alt="" class="ps-profile-model__parts-pic">
                    <div class="ps-profile-model__parts-data">
                        <h4 class="ps-profile-model__parts-name"><?= H($model3dPart->name) ?></h4>
                        <div class="ps-profile-model__parts-info"><?= _t('site.model3d', 'Size') . ' ' . $model3dPart->getSize()->toStringWithMeasurement() . ' ' . _t(
                                'site.store',
                                'Quantity'
                            ) . ' ' . $model3dPart->qty ?></div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

        <?php
        if (count($activeParts) > 1) {
            ?>
            <a href="#collapseModelParts" class="ps-profile-model__show-parts collapsed" role="button" data-toggle="collapse" aria-expanded="false"
               aria-controls="collapseModelParts">
                <span class="if-collapsed">
                    <?= _t('site.model3dViewedState', 'Show order details') ?>
                </span>
                <span class="if-not-collapsed">
                    <?= _t('site.model3dViewedState', 'Hide') ?>
                </span>
            </a>
            <?php
        }
    }
    ?>

    <div class="ps-profile-model__price"><?= displayAsMoney($cost) ?></div>

    <?= Model3dHiddenForm::widget(
        [
            'model3d'   => $model3d,
            'psPrinter' => $psPrinter
        ]
    ); ?>
    <button type="submit" class="btn btn-danger btn-block">
        <span class="tsi tsi-printer3d"></span>
        <?= _t('site.model3dViewedState', 'Buy Here') ?>
    </button>

    <div class="checkbox checkbox-primary m-b0">
        <?php if(app('request')->get('model3dReplicaId', false)): ?>
        <input type="hidden" name="Model3dEditForm[replicaId]" value="<?=(int)app('request')->get('model3dReplicaId');?>">
        <?php endif; ?>
        <input type="hidden" name="Model3dEditForm[quantity]" value="<?= $model3dViewedState->model3dQty ?>">
        <input type="hidden" name="Model3dEditForm[scaleBy]" value="<?= $model3dViewedState->model3dScale ?>">
        <input type="hidden" name="GetPrintedForm[canChangePs]" id="printed_form_can_change_ps" value="1">
    </div>
    <?php ActiveForm:: end(); ?>

</div>
