<?php
/** @var $widget \frontend\widgets\UploadWidget */

use kartik\select2\Select2KrajeeAsset;
use kartik\select2\ThemeKrajeeAsset;
use yii\widgets\ActiveForm;

Select2KrajeeAsset::register($this);
ThemeKrajeeAsset::register($this);

$this->title = $this->title ?? _t('site.printModel3d', 'Upload a CAD model to get an online instant quote');

?>
<div class='model3d-upload-block'>
    <div class="js-upload-start upload-start upload-start__grey-brdr">
        <?php
        $form = ActiveForm::begin(['id' => 'ts-uploader', 'action' => '/order-upload/add-file']);
        ?>

        <h3 class="upload-widget__lead">
            <?= _t('front.upload', 'Upload a CAD model to get an online quote for manufacturing services'); ?>
        </h3>

        <h3 class="upload-block__cta-title m-t30 text-muted">
            <?= _t('front.upload', 'Drag and drop or'); ?>
        </h3>

        <div class="item-rendering-external-widget__howto">
            <div class="item-rendering-external-widget__howto-steps m-b0">
                <div class="item-rendering-external-widget__count">1</div>
                <span><?= _t('front.upload', 'Upload your 3D models'); ?></span>
            </div>
        </div>

        <div class="fileinput-button m-b30">
            <span class="btn btn-danger"><?= _t('front.upload', 'Browse files'); ?></span>
        </div>

        <div class="upload-block__hint">
            <?= _t('front.upload', 'STL, PLY, OBJ, 3MF, CDR, DXF, EPS, PDF or SVG files are supported'); ?>
        </div>

        <div class="upload-block__input<?= $widget->allowUploadByUrl ? '' : ' hidden' ?>">
            <div class="input-group">
                <input type="text" class="download-url-text-js form-control" placeholder="https://www.thingiverse.com/thing:XXXXXX">
                <span class="input-group-btn">
                        <button class="download-by-url-js btn btn-primary" type="button"><?= _t('site.printModel3d', 'Go') ?></button>
                </span>
            </div>
            <span class="help-block">
                    <?= _t('site.printModel3d', 'Paste a link to the 3D model') ?>
                </span>
        </div>

        <div class="upload-block__hint-links">
            <div class="upload-block__hint-links-body">
                <span class="tsi tsi-lock m-r10"></span>
                <span><?= _t('front.upload', 'All files are protected by'); ?>
                    <a href="https://www.treatstock.com/" target="_blank"
                       class="upload-block__hint-links-ts">Treatstock</a>
                    <?= _t('front.upload', 'security and'); ?>
                    <a href="https://www.watermark3d.com/" target="_blank">Watermark 3D</a>
                </span>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
    <div class="upload-preview hidden">
        <div class="ts-uploader-views">
            <h3 class="ts-uploader-models-header hide"><?= _t('front.upload', '3D Models'); ?></h3>
            <div class="uploader-item-list ts-uploader-archive files" data-sortable-id="1" aria-dropeffect="move" id="ts-models-preview">
            </div>

            <h3 class="ts-uploader-cutting-header hide"><?= _t('front.upload', 'Cutting'); ?></h3>
            <div class="uploader-item-list ts-uploader-cutting files" data-sortable-id="1" aria-dropeffect="move" id="ts-cutting-preview">
            </div>

            <h3 class="ts-uploader-pictures-header hide"><?= _t('front.upload', 'Pictures'); ?></h3>
            <div class="uploader-item-list ts-uploader-archive files" data-sortable-id="1" aria-dropeffect="move" id="ts-pictures-preview">
            </div>

            <h3 class="ts-uploader-others-header hide"><?= _t('front.upload', 'Other files'); ?></h3>
            <div class="uploader-item-list ts-uploader-archives files" data-sortable-id="1" aria-dropeffect="move" id="ts-others-preview">
            </div>

            <div class="ts-uploader-template dz-preview uploader-item hidden">
                <img data-dz-thumbnail class="model3d-upload-image-preview" alt="">
                <span class="uploader-item__name" data-dz-name></span>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
            </div>
            <div id="ts-upload-creating-model3d-progress" class="hidden">
                <?= _t('site.ps', 'Processing...'); ?><img class="m-l10" src="/static/images/loading.gif" alt="loading...">
            </div>
        </div>
        <div id="ts-upload-description" class="hidden">
            <div class="form-group">
                <label for="ts-uploader-category"><?= _t('site.ps', 'Choose Manufacturing Service') ?> <span class="form-required">*</span></label>
                <select class="form-control js-category-select input-sm" id="ts-uploader-category">
                </select>
            </div>
            <div class="form-group">
                <label for="ts-uploader-description"><?= _t('site.order', 'Project Description'); ?> <span class="form-required">*</span></label>
                <textarea id="ts-uploader-description" class="form-control"></textarea>
            </div>
        </div>
        <button id="ts-uploader-submit" class="btn btn-primary m-r15 m-b15 ts-uploader-submit" onclick="model3dUploadObj.submit()"><?= _t('site.ps', 'Submit') ?></button>
        <button id="ts-uploader-cancel" class="btn btn-default m-l0 m-b15 ts-uploader-cancel" onclick="model3dUploadObj.cancelUpload()"><?= _t('site.ps', 'Cancel') ?></button>
    </div>
</div>
