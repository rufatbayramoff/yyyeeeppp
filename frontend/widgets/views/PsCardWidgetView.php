<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 18.08.2017
 * Time: 9:53
 */

use common\models\Ps;
use common\modules\catalogPs\models\PsPrinterEntity;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\ps\PsFacade;
use yii\helpers\Html;

/** @var $printedFilesRepo \common\modules\catalogPs\repositories\PrintedFileRepository */
/** @var $printer PsPrinterEntity */
/** @var $ps Ps */
$printer = $model;
$ps = $printer->getPs();

$materials = [];
$materialsDisplay = [];
if($printer){
    $certLevel = $printer->companyService->getCertificationLabel();
    $materials = $printer->getMaterials();
    $moreMaterialsCount = 0;
    foreach ($materials as $material) {
        if (count($materialsDisplay) < 3) {
            $materialsDisplay[] = $material->material->title;
        } else {
            $moreMaterialsCount++;
        }
    }
}
$getShippingPrice = function(PsPrinterEntity $printer){
    $shippingPrice = _t('ps.shipping', 'Standard Flat Rate');
    $printerDelivery = $printer->getDeliveryTypes();
    foreach ($printerDelivery as $psDelivery){
        $price = $psDelivery->carrier_price
            ? displayAsCurrency($psDelivery->carrier_price, $printer->getCompanyService()->getCurrency()) : '';
        if(!empty($price)){
            $shippingPrice =  $price;
            break;
        }
    }
    return $shippingPrice;
};
$psLink = PsFacade::getPsLink($ps);
$psPicture = Ps::getCircleImageByPs($ps);
?>

<div class="<?=$containerClass;?>">

    <div class="designer-card designer-card--ps-cat">
        <div class="designer-card__ps-rating">
            <?php
            if((int)$printer->ps_rating_count > 0):
                echo \frontend\widgets\PsPrintServiceReviewStarsWidget::widget([
                    'withoutJs' => true,
                    'fromReviewCount' => 0,
                    'reviewsCount' => (int)$printer->ps_rating_count,
                    'reviewsRating'=> (float)$printer->ps_rating,
                    'withSchema' => false
                ]);
            else:
                if ($ps->allowShowBeFirstCommentator()) {
                    echo "<span class='text-muted'>" . _t('site.catalog', 'Be the first to leave a review') . "</span>";
                }
            endif;
            ?>
        </div>

        <?php if($printer):?>
            <div class="cert-label" data-toggle="tooltip" data-placement="bottom" title=""
                 >
                <?= $certLevel; ?>
            </div>
        <?php endif; ?>

        <div class="designer-card__userinfo">
            <a class="designer-card__avatar" href="<?=$psLink;?>" target="_blank"><img src="<?=$psPicture;?>" alt="<?=H($ps->title);?>" title="<?=H($ps->title);?>"/></a>
            <h3 class="designer-card__username">
                <a href="<?=$psLink;?>" target="_blank" title="<?=H($ps->title);?>">
                    <?=H($ps->title);?>
                </a>
            </h3>
        </div>
        <div class="designer-card__ps-pics">
            <?php
            $picturesPs = $ps->getPicturesFiles(3);
            $picturesPrinted = (array)$printedFilesRepo->getById($printer->ps_id);
            $pictures = array_merge($picturesPs, $picturesPrinted);
            if ($pictures): ?>
                <div class="designer-card__ps-portfolio swiper-container">
                    <div class="swiper-wrapper">
                        <?php
                        $totalPics = 3;
                        foreach ($pictures as $picture) {
                            if($totalPics===0) break;
                            $img = ImageHtmlHelper::getThumbUrl($picture->getFileUrl());
                            $imgThumb = ImageHtmlHelper::getThumbUrl($picture->getFileUrl(), 160, 90);
                            if(isset($_GET['img'])){
                                $img = str_replace('http://ts.vcap.me', 'https://static.treatstock.com', $img);
                                $imgThumb = str_replace('http://ts.vcap.me', 'https://static.treatstock.com', $imgThumb);
                            }
                            echo sprintf(
                                '<a class="designer-card__ps-portfolio-item swiper-slide" href="%s" data-lightbox="%s"><img src="%s" alt="%s" title="%s"></a>',
                                $img, 'ps'.$ps->id, $imgThumb, H($ps->title) . ' 3D Printing photo', H($ps->title) . ' 3D Printing photo'
                            );
                            $totalPics--;
                        }
                        ?>
                    </div>
                    <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                </div>
            <?php else: ?>
                <div class="designer-card__ps-pics-empty">
                    <?=_t('site.catalog', 'Images not uploaded'); ?>
                </div>
            <?php endif; ?>
        </div>
        <?php if($printer):
            $printerLocation = sprintf("%s, %s, %s", $printer->city, $printer->region, $printer->country_iso);
            ?>
            <div class="designer-card__ps-loc" title="<?=$printerLocation;?>">
                <span class="tsi tsi-map-marker"></span>
                <?=$printerLocation;?>
            </div>
        <?php else: ?>
        <?php endif; ?>

        <div class="designer-card__about">
            <?= frontend\components\StoreUnitUtils::displayMore($ps->description, 25, false); ?>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Materials:'); ?></span>
                    <?= H(implode(', ', $materialsDisplay)); ?>
                    <?php if ($moreMaterialsCount > 0):
                        echo _t('public.ps', '+{n, plural, =1{1 material} other{# materials}}', ['n' => $moreMaterialsCount]);
                    endif; ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="designer-card__data">
                    <?php if((float)$printer->min_order_price > 0): ?>
                        <span class="designer-card__data-label"><?= _t('site.ps', 'Minimum Charge:'); ?></span>
                        <?=displayAsCurrency($printer->min_order_price, $printer->companyService->company->currency);?>
                    <?php endif; ?>
                </div>
                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Shipping:'); ?></span>
                    <?=$getShippingPrice($printer);?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="designer-card__btn-block">
                    <a class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicPs" data-actionga="PrintHere" href="<?=$psLink;?>">
                        <?= _t('site.ps', 'Visit page'); ?>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
