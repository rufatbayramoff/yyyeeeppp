<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.12.16
 * Time: 16:17
 */

use frontend\components\image\ImageHtmlHelper;
?>
<div id='star-rating-block-<?=$uid?>' class="star-rating-block" <?php if($withSchema): ?>itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" <?php endif; ?>>
    <?php if($withSchema): ?>
    <meta itemprop="ratingValue" content="<?= $rating ?>"/>
    <meta itemprop="ratingCount" content="<?= $count?>" />
    <meta itemprop="worstRating" content="1"/>
    <meta itemprop="bestRating" content="5"/>
    <div class="hidden" itemprop="itemReviewed" itemscope itemtype="https://schema.org/<?=$type?>">
         <img itemprop="image" src="<?= ImageHtmlHelper::getThumbUrl($itemLogoUrl) ?>" alt="<?= H($itemTitle) ?>"/>
         <span itemprop="name"><?= H($itemTitle) ?> </span>
    </div>
    <?php endif; ?>
    <input value="<?= $rating ?>" type="text" class="star-rating" data-min=0 data-max=5 data-step=0.1 data-size="<?=$dataSize;?>" data-symbol="&#xea4b;" data-glyphicon="false"
           data-rating-class="tsi" data-readonly="true">
    <?=$html;?>
</div>
