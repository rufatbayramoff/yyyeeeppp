<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.07.16
 * Time: 14:41
 */

use common\components\JsObjectFactory;
use frontend\widgets\assets\Model3dInfoAssets;
use frontend\widgets\Model3dInfoJs;

/** @var $widget Model3dInfoJs */

Model3dInfoAssets::register($this);

$model3dInfo = [];
$model3dInfo['id'] = $widget->model3d->id;
$model3dInfo['title'] = $widget->model3d->title;
$model3dInfo['description'] = $widget->model3d->description;

$model3dPartsInfo = [];

foreach ($widget->model3d->getActiveModel3dParts() as $model3dPart) {
    $calcColor = $model3dPart->getCalculatedTexture();
    $partInfo['id'] = $model3dPart->id;
    $partInfo['fileId'] = $model3dPart->file->id;
    $partInfo['printerColorId'] = $calcColor->printerColor->id;
    $partInfo['printerColorRgb'] = $calcColor->printerColor->getRgbHex();
    $partInfo['printerMaterialId'] = $calcColor->printerMaterial ? $calcColor->printerMaterial->id : null;
    $partInfo['printerMaterialGroupId'] = $calcColor->calculatePrinterMaterialGroup()->id;
    $model3dPartsInfo[$partInfo['id']] = $partInfo;
}

JsObjectFactory::createJsObject(
    'model3dInfoClass',
    'model3dInfoObj',
    [
        'RequestInfo'  => $model3dInfo,
        'model3dParts' => $model3dPartsInfo,
    ]
    ,
    $this
);