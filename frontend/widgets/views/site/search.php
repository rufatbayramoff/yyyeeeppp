<?php
use yii\helpers\Html;

/** @var string $query  **/
/** @var string $id  **/
/** @var \frontend\components\FrontendWebView $this **/
/** @var boolean $isWidgetMode */

$isWidgetMode = $isWidgetMode ?? false;

$routeForm =  \yii\helpers\Url::toRoute(['/product/product/index']);
if(!empty($category)){
    $routeForm =   \yii\helpers\Url::toRoute(['/product/product/index', 'category' => $category->id, 'slug'=>$category->getSlug()]);
}


?>

<?= Html::beginTag('div', ['class' => 'search-block'])?>

    <form method="get" class="form-horizontal" role="search" action="<?php echo $routeForm; ?>">
        <?php if($isWidgetMode):?>
            <?php echo Html::hiddenInput('widget', 'widget');?>
        <?php endif; ?>

        <div class="input-group">
            <?=
            \kartik\widgets\Typeahead::widget([
                'name' => 'search',
                'value' => Html::decode(\Yii::$app->request->get('search', '')),
                'options' => [
                    'placeholder' => _t('front.site', 'Search'),
                    'maxlength' => \frontend\models\store\StoreSearchForm::MAX_SERACH_LENGTH,
                ],
                'scrollable' => true,
                'pluginOptions' => ['highlight'=>true],
                'dataset' => [
                    [
                        'remote' => [
                            'url' => \yii\helpers\Url::to(['/store/store/explore-autocomplete']) . '?query=%QUERY',
                            'wildcard' => '%QUERY'
                        ],
                        'limit' => 5
                    ]
                ]
            ]);
            ?>

            <button class="search-block__btn" type="submit"><i class="tsi tsi-search"></i></button>

        </div>
        <input type="hidden" name="category"/>
    </form>

<?= Html::endTag('div')?>


