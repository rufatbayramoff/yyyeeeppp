<?php
use yii\helpers\Html;
?>
<div class="dropup" class="currency-box">
    <a id="current-currency" href="#" class="btn btn-default dropdown-toggle" data-placement="top"  data-toggle="dropdown">
     <?= $current->title . ' ' . $current->title_original;?>  <span class="tsi tsi-up"></span></a>
     <ul class="dropdown-menu" role="menu" >
        <?php foreach ($list as $currency):?>
            <li class="item-lang">
                <?= Html::a($currency->title . ' ' . $currency->title_original, 
                    \yii\helpers\Url::toRoute(['site/currency', 'code'=>$currency->currency_iso])) ?>
            </li>
        <?php endforeach;?>
    </ul>
</div>