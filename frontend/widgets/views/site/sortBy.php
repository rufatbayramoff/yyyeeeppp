<?php
/**
 * Created by PhpStorm.
 * User: A1
 * Date: 11.08.2017
 * Time: 17:31
 *
 * @var boolean $isWidgetMode
 */

use yii\helpers\Html;

$isWidgetMode = $isWidgetMode ?? false;

/** @var \yii\widgets\ActiveForm $form */
$form = \yii\widgets\ActiveForm::begin(
    [
        'action' => ['/product/product/index'],
        'method' => 'get',
        'options' => ['class' => 'js-store-filter store-filter-row', 'autocomplete' => 'off']
    ]
);
?>

<?php /* echo $form->field($searchForm, 'priceMin')->hiddenInput()->label(false);*/ ?>
<?php /* echo $form->field($searchForm, 'priceMax')->hiddenInput()->label(false);*/ ?>
<?= $form->field($searchForm, 'search', ['template' => '{input}'])->hiddenInput()->label(false); ?>
<?= $form->field($searchForm, 'category', ['template' => '{input}'])->hiddenInput()->label(false); ?>
<?= $form->field($searchForm, 'sortDirection', ['template' => '{input}'])->hiddenInput()->label(false); ?>

<?php if($isWidgetMode):?>
    <?php echo Html::hiddenInput('widget', 'widget');?>
<?php endif; ?>

<?php foreach ($searchForm->tags as $tag): ?>
    <?= $form->field($searchForm, 'tags[]', ['template' => '{input}'])->hiddenInput(['value' => $tag])->label(false); ?>
<?php endforeach; ?>

    <div class="store-filter__sort-by">
        <h4><?= _t('fornt.catalog', 'Sort by') ?>:</h4>

        <?=
        kartik\select2\Select2::widget(
            [
                'model' => $searchForm,
                'attribute' => 'sort',
                'hideSearch' => true,
                'data' => $searchForm->getSortLabels(),
                'options' => ['placeholder' => _t('fornt.catalog', 'Sort by')]
            ]
        );
        ?>

        <button type="button" class="js-btn-sort btn btn-default btn-circle store-filter__sort-btn">
            <span></span>
        </button>
    </div>
<?php $form->end(); ?>