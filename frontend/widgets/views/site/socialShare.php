<div class="model-share">
<div class="model-share__list js-social-likes<?=$id;?>" <?php if(!empty($link)){ ?>data-url="<?=$link;?>" data-title="<?=H($title);?>" <?php }; ?>>

    <?php if($this->context->showEmail): ?>
    <div class="model-share__btn"><a href="/site/sendemail?link=<?php echo $link; ?>" title="<?php
        echo _t("site.email", "Send email"); ?>" oncontextmenu="return false;" class="ts-ajax-modal">
            <span class="social-likes__icon social-likes__icon_mail"></span></a></div>
    <?php endif; ?>
    <?php if($this->context->showHtmlcode): ?>
    <div class="model-share__btn shareembed<?=$id;?>"  data-placement="bottom" data-container="body" data-toggle="popover"
         data-original-title="<?php echo _t("site.share", "HTML Code"); ?>">
        <span class="social-likes__icon social-likes__icon_code"  title="<?=_t("site.email", "HTML Code"); ?>"></span></div>
    <?php endif; ?>
    <div class="model-share__btn sharelink<?=$id;?>" data-placement="bottom" data-container="body" data-toggle="popover"
         data-original-title="<?php echo _t("site.share", "Share this link"); ?>">
            <span class="social-likes__icon social-likes__icon_link" title="<?=_t("site.email", "Share this link"); ?>" ></span></div>

    <div class="model-share__btn facebook-send<?=$id;?>"  title="<?=_t("site.email", "Facebook Messenger"); ?>" ><span class="social-likes__icon social-likes__icon_facebook-m"></span> Facebook Send</div>
    <div class="model-share__btn facebook" title="<?=_t("site.email", "Share this link"); ?>" data-display="popup" >Facebook</div>
    <div class="model-share__btn twitter" title="<?=_t("site.email", "Tweet this link"); ?>"  data-via="treatstock"> Twitter</div>

    <?php if(isset($imgUrl) && !empty($imgUrl)): ?>
    <div class="model-share__btn pinterest" title="<?=_t("site.email", "Pin this model"); ?>"  data-media="<?=$imgUrl; ?>"> Pinterest</div>
    <?php endif; ?>
</div>

</div>