<?php
use yii\helpers\Html;
?>
<div class="dropdown">
    <a id="current-lang" href="#" class="btn btn-default dropdown-toggle" data-placement="top" data-toggle="dropdown">
        <span class="tsi tsi-globe m-r10"></span><?= $current->title_original; ?> <span class="tsi tsi-down"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <?php foreach ($langs as $lang): ?>
            <li class="item-lang">

                <?= Html::a($lang->title_original, \common\components\UrlHelper::addGetParameter(Yii::$app->request->url, 'ln', $lang->iso_code)); ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>