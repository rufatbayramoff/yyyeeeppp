<?php

/**
 * Created by mitaichik
 */

/* @var $this \yii\web\View */
/** @var int $width */
?>
<div class="star-rating-block star-rating-block-static-link">
    <div class="star-rating rating-xs rating-disabled">
        <div class="rating-container tsi" data-content="">
            <div class="rating-stars" data-content="" style="width: <?= $width ?>%;"></div>
        </div>
    </div>

</div>
