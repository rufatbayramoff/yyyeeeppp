<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.03.17
 * Time: 11:32
 */

namespace frontend\widgets;

use common\components\JsObjectFactory;
use common\models\Model3d;
use frontend\widgets\assets\Model3dEmbedAssets;
use yii\base\Widget;

class Model3dEmbedWidget extends Widget
{
    /** @var  Model3d */
    public $model3d;

    public $widgetUuid = '';

    public function init()
    {
        $this->widgetUuid = substr(md5(microtime(true) . '-' . random_int(0, 999999)), 0, 5);
    }

    public function run()
    {
        $this->registerAssets();
        return $this->render(
            'Model3dEmbedWidget',
            [
                'model3d'    => $this->model3d,
                'widgetUuid' => $this->widgetUuid
            ]
        );
    }

    /**
     * register required assets
     */
    protected function registerAssets()
    {
        $this->getView()->registerAssetBundle(Model3dEmbedAssets::class);
        JsObjectFactory::createJsObject(
            'model3dEmbedWidgetClass',
            'model3dEmbedWidgetObj' . $this->widgetUuid,
            [
                'widgetUuid' => $this->widgetUuid,
                'cssUrl'     => param('server') . '/css/embed-model.css',
            ],
            $this->getView()
        );
    }
}