<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.12.16
 * Time: 16:17
 */

namespace frontend\widgets;


use common\components\JsObjectFactory;
use common\models\Company;
use frontend\widgets\assets\ReviewStarsAssets;
use yii\base\Widget;

class ReviewStarsWidget extends Widget
{
    public $rating;
    public $count = 1;
    public $uid;
    public $html;
    public $withSchema = false;
    public $dataSize = 'xs'; // sm
    public $type = self::TYPE_ORGANIZATION;

    /** @var Company */
    public $company;

    /** Or DirectCompany Params  */
    public $itemLogoUrl;
    public $itemTitle;

    public const TYPE_ORGANIZATION = 'Organization';

    public function run()
    {
        if (!$this->uid) {
            $this->uid = mt_rand(0, 99999);
        }
        $this->registerAssets();
        $this->itemLogoUrl = $this->itemLogoUrl ?? $this ?->company ?->getLogoUrl();
        $this->itemTitle = $this->itemTitle ?? $this ?->company ?->title;

        return $this->render(
            'ReviewStars',
            [
                'dataSize'    => $this->dataSize,
                'uid'         => $this->uid,
                'rating'      => $this->rating,
                'count'       => $this->count,
                'html'        => $this->html,
                'withSchema'  => $this->withSchema,
                'type'        => $this->type,
                'itemTitle'   => $this->itemTitle,
                'itemLogoUrl' => $this->itemLogoUrl
            ]
        );
    }

    /**
     * register required assets
     *
     * @throws \yii\base\InvalidConfigException
     */
    protected function registerAssets()
    {
        $this->getView()->registerAssetBundle(ReviewStarsAssets::class);
        JsObjectFactory::createJsObject(
            'reviewStarsWidgetClass',
            'reviewStarsWidgetObj_' . $this->uid,
            [
                'uid' => $this->uid
            ],
            $this->getView()
        );
    }
}