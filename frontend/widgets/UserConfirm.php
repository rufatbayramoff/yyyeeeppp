<?php namespace frontend\widgets;

use frontend\models\user\UserFacade;

/**
 * User confirm email widget 
 * shows 2 ajax links confirm and change email 
 */
class UserConfirm extends \yii\bootstrap\Widget
{
    /**
     * user model
     * @var \common\models\User
     */
    public $user;
    
    /**
     * @return string
     */
    public function run()
    {
        $data = ['email'=> $this->user->email, 'name'=> H(UserFacade::getFormattedUserName($this->user))];
        return $this->render('user/confirmEmail', [
            'data' => $data
        ]);
    }
}
