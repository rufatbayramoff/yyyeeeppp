<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.01.17
 * Time: 14:53
 */

namespace frontend\widgets;

use common\interfaces\Model3dBaseInterface;
use common\models\Model3d;
use common\models\Model3dViewedState;
use common\models\PsPrinter;
use yii\base\Widget;

class Model3dHiddenForm extends Widget
{
    const FORM_PREFIX = 'Model3dEditForm';

    /** @var  string */
    public $forumUrl = '/store/cart/add';

    /** @var  array */
    public $forumUrlParams = [
        'redirectToDelivery' => true
    ];

    /** @var  Model3dBaseInterface */
    public $model3d;

    /** @var  PsPrinter */
    public $psPrinter;

    public function run()
    {
        $this->registerAssets();
        return $this->render(
            'Model3dViewedStateForm',
            [
                'widget' => $this
            ]
        );
    }

    /**
     * register required assets
     */
    protected function registerAssets()
    {
    }
}