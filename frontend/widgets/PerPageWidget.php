<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace frontend\widgets;


use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\data\DataProviderInterface;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;

/**
 * Widget for show per-page select
 * @package frontend\widgets
 */
class PerPageWidget extends Widget
{
    /**
     * Per page items
     * @var int[]
     */
    public $items = [6, 10, 30, 50];

    /**
     * Data provider
     * @var \yii\data\DataProviderInterface
     */
    public $dataProvider;

    /**
     * Html options of container
     * @var array
     */
    public $options = ['class' => 'pagination'];

    /**
     * Label before widget
     * @var string
     */
    public $label;

    /**
     *
     */
    public function init()
    {
        if(empty($this->items))
        {
            throw new InvalidConfigException('PerPage items cant be empty');
        }

        if(!($this->dataProvider instanceof DataProviderInterface))
        {
            throw new InvalidConfigException('Data provider not set or invalid');
        }
    }

    /**
     *
     */
    public function run()
    {
        if(min($this->items) >= $this->dataProvider->getTotalCount())
        {
            return;
        }

        $items = [];

        if($this->label)
        {
            $items[] = [
                'label' => Html::tag('span', $this->label),
                'encode' => false,
                'options' => ['class' => 'lable'],
            ];
        }

        foreach($this->items as $item)
        {
            $items[] = [
                'label' => $item,
                'url' => Url::current(['per-page' => $item, $this->dataProvider->getPagination()->pageParam => 1]),
                'active' => $this->dataProvider->getPagination()->pageSize == $item,
            ];
        }

        echo Menu::widget([
            'items' => $items,
            'options' => $this->options,
        ]);
    }
}