<?php namespace frontend\widgets;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class SiteHelpWidget extends \yii\bootstrap\Widget
{
    
    /**
     * help alias
     * 
     * @var string 
     */
    public $alias;
    
    /**
     * title for help box
     * 
     * @var string
     */
    public $title = 'Help';
    
    /**
     * HTML or Text which will be used as a link
     * 
     * @var string
     */
    public $triggerHtml = '<i class="tsi tsi-question"></i>';
    
    /**
     * 
     * @return string
     */
    public function run()
    { 
        return $this->render('site/help', [
            'alias' => $this->alias ,
            'title' => $this->title,
            'triggerHtml' => $this->triggerHtml
        ]);
    }
} 