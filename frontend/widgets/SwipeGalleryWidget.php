<?php
/**
 * Created by mitaichik
 */

namespace frontend\widgets;


use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\models\File;
use frontend\assets\LightboxAsset;
use frontend\assets\SwiperAsset;
use frontend\components\image\ImageHtmlHelper;
use yii\base\Widget;
use yii\helpers\Html;

class SwipeGalleryWidget extends Widget
{
    /**
     * Files for gallery.
     * Apply only images and videos.
     * If file another formt, or not exist - it will be filetred and not displayed;
     *
     * @var File[]
     */
    public $files;

    /**
     * Max count of files, who will displayed on gallery.
     *
     * @var int
     */
    public $maxCount;

    /**
     * Thumb size.
     * Array, where first argument - width, second - height.
     *
     * @var int
     */
    public $thumbSize = [ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT];

    /**
     * Options for container tag.
     *
     * @var array
     */
    public $containerOptions = [];

    /**
     * Options for file item tag.
     *
     * @var array
     */
    public $itemOptions = [];

    /**
     * Options for scrollbar tag.
     *
     * @var array
     */
    public $scrollbarOptions = [];

    /**
     * Options for empty tag.
     *
     * @var array
     */
    public $emptyOptions = [];

    /**
     * Text, that will display when files is emptry (or not empty, but files not exist, or bad format).
     *
     * @var string
     */
    public $emptyText;

    /**
     * Show gallery if files is empty (or not empty, but files not exist, or bad format)
     * If true - widget not will render.
     *
     * @var string
     */
    public $showIfEmpty = true;

    /**
     * Alt teg for images.
     * May contains placeholders {name} and {id} who replaced with file properties.
     *
     * @var string
     */
    public $alt = "{name}";

    /**
     * Register assets once for class.
     *
     * This parametr important when we use gallery in list.
     * If true - widget will register asset once for $containerOptions class,
     * otherwise - assets (include init javscript) will registered for each instance of widget by id selector.
     *
     * @var bool
     */
    public $assetsByClass = true;


    /**
     * Title of image
     * @var string
     */
    public $title;

    /**
     * @var array
     */
    private static $registeredAssetsClasses = [];

    /**
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function run() : void
    {
        $files = $this->prepareFilesList($this->files);

        if (!$files) {
            $this->renderEmpty();
            return;
        }

        $id = $this->containerOptions['id'] ?? $this->getId();

        Html::addCssClass($this->containerOptions, 'swiper-container');
        $this->containerOptions['id'] = $id;

        echo Html::beginTag('div', $this->containerOptions);
            echo Html::beginTag('div', ['class' => 'swiper-wrapper']);
                $this->renderFiles($files, $id);
            echo Html::endTag('div');
            $this->renderScrollbar();
        echo Html::endTag('div');

        $this->registerAssets($id);
    }

    /**
     * Render files elements
     * @param File[] $files
     * @param string $id
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    private function renderFiles(array $files, string $id) : void
    {
        Html::addCssClass($this->itemOptions, 'swiper-slide');

        $videoItemOptions = $this->itemOptions;
        $imageItemOptions = ArrayHelper::merge( $this->itemOptions, ['data-lightbox' => $id]);

        foreach ($files as $file) {

            if (FileTypesHelper::isVideo($file)) {
                echo ImageHtmlHelper::getClickableVideo($file, $videoItemOptions, $this->thumbSize[0], $this->thumbSize[1]);
            }

            elseif (FileTypesHelper::isImage($file)) {

                $hash = $file->created_at != $file->updated_at ? '?time=' . md5($file->updated_at) : '';
                $imgUrl = ImageHtmlHelper::getThumbUrlForFile($file, ImageHtmlHelper::LARGE_WIDTH, ImageHtmlHelper::LARGE_HEIGHT) . $hash;
                $thumbUrl = ImageHtmlHelper::getThumbUrlForFile($file, $this->thumbSize[0], $this->thumbSize[1]) . $hash;

                $alt = strtr($this->alt, ['{name}' => \H($file->name), '{id}' => $file->id]);
                $title = $this->title ? strtr($this->title, ['{name}' => \H($file->name), '{id}' => $file->id]) : null;



                $img = Html::img($thumbUrl, ['alt' => $alt, 'title' => $title]);
                echo Html::a($img, $imgUrl, $imageItemOptions);
            }
        }
    }

    /**
     * Render scrollbar element
     */
    public function renderScrollbar() : void
    {
        Html::addCssClass($this->scrollbarOptions, 'swiper-scrollbar');
        echo Html::tag('div', null, $this->scrollbarOptions);
    }

    /**
     * @param string $id
     * @throws \yii\base\InvalidConfigException
     */
    private function registerAssets(string $id) : void
    {
        $this->getView()->registerAssetBundle(LightboxAsset::class);
        $this->getView()->registerAssetBundle(SwiperAsset::class);

        if ($this->assetsByClass) {
            $classes = preg_split('/\s+/', $this->containerOptions['class'], -1, PREG_SPLIT_NO_EMPTY);
            $class = ArrayHelper::first($classes);

            if (in_array($class, self::$registeredAssetsClasses)) {
                return;
            }

            self::$registeredAssetsClasses[] = $class;
            $selector = '.'.$class;
        }
        else {
            $selector = '#'.$id;
        }

        $this->getView()->registerJs("
            new Swiper('{$selector}', {
                scrollbar: '.swiper-scrollbar',
                scrollbarHide: true,
                slidesPerView: 'auto',
                grabCursor: true
            });
        ");
    }

    /**
     * Prepare files list.
     * Remove from list all incorrect files.
     *
     * @param File[] $files
     * @return File[]
     */
    private function prepareFilesList(array $files) : array
    {
        // filter bad formats annd not exists
        $files = array_filter($files, function (File $file) {
            return (FileTypesHelper::isImage($file) || FileTypesHelper::isVideo($file));
        });

        // aplly max counts
        if ($this->maxCount !== null) {
            $files = array_slice($files, 0, $this->maxCount);
        }

        return $files;
    }

    /**
     * Render empty gallery.
     * If not showIfEmpty - empty tag not will rednered.
     * If not defined emptyOptions and emptyText - also empty tag not will rendered.
     */
    private function renderEmpty() : void
    {
        if (!$this->showIfEmpty) {
            return;
        }

        if ($this->emptyOptions || $this->emptyText) {
            echo Html::tag('div', $this->emptyText, $this->emptyOptions);
        }
    }
}