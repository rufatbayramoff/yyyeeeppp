<?php
namespace frontend\widgets;

use lib\geo\GeoException;
use yii\base\Widget;

/**
 * Map Widget Class
 *
 * @deprecated
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 */
class MapWidget extends Widget
{
    public $width;
    public $height;
    public $data_locations;
    public $readonly = false;


    /**
     * Init Map Widget
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Default vendor set to 'google'
     * If we use readonly mode, show the only map with balloons
     *
     * @return mixed
     * @throws GeoException
     */
    public function run()
    {
        return  $this->render('geo/google/locations_points');
    }
}
