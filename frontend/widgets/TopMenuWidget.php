<?php


namespace frontend\widgets;


use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use frontend\components\UserSessionFacade;
use yii\base\Widget;
use common\modules\catalogPs\repositories\CompanyServiceCategoryRepository;


class TopMenuWidget extends Widget
{
    public string $navClass = 'hidden-xs';

    public function __construct(
        private CompanyServiceCategoryRepository $companyServiceCategoryRepository,
        $config = []
    )
    {
        parent::__construct($config);
    }

    public function run()
    {
        $menuItems = [
            [
                'url'   => CatalogPrintingUrlHelper::printing3dCatalog(UserSessionFacade::getLocation()),
                'img'   => '3dprinting',
                'label' => _t('site.main', '3D Printing')],
            [
                'url'   => '/company-service/cnc-manufacturing',
                'img'   => 'cnc-machining',
                'label' => _t('site.main', 'CNC Machining')],
            [
                'url'   => '/company-service/cutting',
                'img'   => 'cutting-laser',
                'label' => _t('site.main', 'Cutting')],
            [
                'url' => CatalogPrintingUrlHelper::printing3dCatalog(UserSessionFacade::getLocation(), usageCode: 'hq-prototyping'),
                'img' => 'hd-prototyping', 'label' => _t('site.main', 'HD Prototyping')
            ],
        ];
        return $this->render('top_menu_widget', [
            'menuItems' => $menuItems,
            'services'  => $this->companyServiceCategoryRepository->categoriesMenu()
        ]);
    }
}