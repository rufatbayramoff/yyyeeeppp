<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.01.17
 * Time: 17:42
 */

namespace frontend\widgets;

use common\models\PsPrinter;
use common\models\Model3dViewedState;
use common\services\Model3dViewedStateService;
use frontend\models\model3d\Model3dFacade;
use frontend\widgets\assets\Model3dViewStateAssets;
use Yii;
use yii\base\Widget;

class Model3dViewedStateWidget extends Widget
{
    /** @var  Model3dViewedState */
    public $model3dViewedState;

    /** @var PsPrinter */
    public $psPrinter;

    public function run()
    {
        /** @var Model3dViewedStateService $model3dViewedStateService */
        $model3dViewedStateService = Yii::createObject(Model3dViewedStateService::class);
        $isRestored = $model3dViewedStateService->restoreModel3dState($this->model3dViewedState);

        if(!$isRestored){
            return '';
        }

        if (!Model3dFacade::isCanPrintModelByTextureAndSize($this->psPrinter, $this->model3dViewedState->model3d)) {
            return '';
        }

        $this->registerAssets();
        return $this->render(
            'Model3dViewedState',
            [
                'model3dViewedState' => $this->model3dViewedState,
                'psPrinter'          => $this->psPrinter
            ]
        );
    }

    /**
     * register required assets
     */
    protected function registerAssets()
    {
        $this->getView()->registerAssetBundle(Model3dViewStateAssets::class);
    }
}