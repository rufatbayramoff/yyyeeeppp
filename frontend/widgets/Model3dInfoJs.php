<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.07.16
 * Time: 14:38
 */

namespace frontend\widgets;

use common\models\Model3d;
use yii\base\Widget;

class Model3dInfoJs extends Widget
{
    /**
     * @var Model3d
     */
    public $model3d;

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        return $this->render(
            'Model3dInfoJs',
            [
                'widget' => $this,
            ]
        );
    }
}