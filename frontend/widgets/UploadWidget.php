<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.12.16
 * Time: 18:27
 */

namespace frontend\widgets;

use common\components\ArchiveManager;
use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\components\JsObjectFactory;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\CuttingPack;
use common\models\ProductCategory;
use common\modules\catalogPs\repositories\CompanyServiceCategoryRepository;
use frontend\widgets\assets\Model3dUploadAssets;
use yii\base\Widget;

class UploadWidget extends Widget
{
    /**
     * @var array
     */
    public $model3dExtensions = [];

    /**
     * @var array
     */
    public $cuttingExtensions = [];

    /**
     * @var array
     */
    public $cncExtensions = [];

    /**
     * @var array
     */
    public $documentsExtensions = [];

    /**
     * @var array
     */
    public $imagesExtensions = [];

    /**
     * @var array
     */
    public $otherExtensions = [];

    public $needNewWindow = false;

    /**
     * $model3dExtensions + $cuttingExtensions + $cncExtensions + $imagesExtensions + $otherExtensions
     * @var array
     */
    public $allowedExt = [];

    /**
     * @var array
     */
    public $additionalParams = [];

    /**
     * Already uploaded files list
     *
     * @var array
     */
    public $files = [];


    /**
     * @var string
     */
    public $textComment = '';


    /**
     * @var integer
     */
    public $maxFileSize = null;

    /**
     * Change default view file
     *
     * @var string
     */
    public $viewType;

    /**
     * Post all files form url
     * @var string
     */
    public $postFormUrl = '/order-upload/save';

    /** @var array */
    public $categories;

    /**
     * Enable upload by url
     *
     * @var bool
     */
    public $allowUploadByUrl = false;

    /**
     * @var bool
     */
    public $isWidget = 1;

    public function init()
    {
        if (!$this->imagesExtensions) {
            $this->imagesExtensions = Model3dBaseImgInterface::ALLOWED_FORMATS;
        }
        if (!$this->model3dExtensions) {
            $this->model3dExtensions = Model3dBasePartInterface::ALLOWED_FORMATS;
        }
        if (!$this->cuttingExtensions) {
            $this->cuttingExtensions = CuttingPack::ALLOWED_FORMATS;
        }
        if (!$this->documentsExtensions) {
            $this->documentsExtensions = FileTypesHelper::ALLOW_DOCS_EXTENSIONS;
        }
        if (!$this->otherExtensions) {
            $this->otherExtensions = ArchiveManager::ARCHIVE_FORMATS;
        }

        $this->allowedExt = FileTypesHelper::getAllowExtensions();

        if (!$this->maxFileSize) {
            $this->maxFileSize = app('setting')->get('upload.maxfilesize', 57);
        }
        if (!$this->textComment) {
            $this->textComment = _t('front.upload', 'Drop your pictures and files to create order');
        }

        if (!$this->categories) {
            $categories     = ProductCategory::find()->level1()->visible()->orderBy('title')->all();
            $categoriesList = [0 => [
                'text' => '',
                'id'   => '',
                'url'  => ''
            ]];
            foreach ($categories as $category) {
                $categoriesList[] = [
                    'text' => $category->title,
                    'id'   => $category->code,
                ];
            }
            $this->categories = $categoriesList;
        }
    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $this->registerAssets();
        return $this->render(
            'upload',
            [
                'widget' => $this,
            ]
        );
    }

    /**
     * register required assets
     */
    protected function registerAssets()
    {
        $this->getView()->registerAssetBundle(Model3dUploadAssets::class);
        JsObjectFactory::createJsObject(
            'model3dUploadClass',
            'model3dUploadObj',
            [
                'maxFileSize'       => $this->maxFileSize,
                'allowedExtensions' => $this->allowedExt,
                'imagesExtensions'  => $this->imagesExtensions,
                'model3dExtensions' => $this->model3dExtensions,
                'cuttingExtensions' => $this->cuttingExtensions,
                'otherExtensions'   => $this->otherExtensions,
                'additionalParams'  => $this->additionalParams,
                'postFormUrl'       => $this->postFormUrl,
                'categories'        => $this->categories,
                'needNewWindow'     => $this->needNewWindow,
                'isWidget'          => $this->isWidget
            ],
            $this->getView()
        );
    }
}