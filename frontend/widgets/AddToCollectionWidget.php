<?php namespace frontend\widgets;
use common\models\UserCollection;

/**
 * Add model to user collection
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class AddToCollectionWidget extends \yii\bootstrap\Widget
{
 
    public $actionUrl = 'my/collection/add-item';
    public $ajaxParams = [];
    public $model3dId = 0;
    public $jsCallback;
    public $userCollections;
    public $modelCollections = [];
    /**
     * prepare all required information for widget 
     * 
     */
    public function init()
    {
        parent::init();

        /** @var \common\models\Model3d $modelObj */
        $modelObj = \common\models\Model3d::tryFindByPk($this->model3dId);
        $userId = \frontend\models\user\UserFacade::getCurrentUserId();

        $modelCollections = \frontend\models\model3d\Model3dFacade::getModelCollections($modelObj, $userId) ?: [];

        $this->modelCollections = array_filter($modelCollections, function(\common\models\base\UserCollection $collection)
        {
            return $collection->system_type != UserCollection::SYSTEM_TYPE_FILES;
        });

        $this->userCollections = UserCollection::find()
            ->active()
            ->forUser(\frontend\models\user\UserFacade::getCurrentUser())
            ->notSystem(UserCollection::SYSTEM_TYPE_FILES)
            ->orderBy('id desc')
            ->all();
    }

    /**
     * 
     * @return type
     */
    public function run()
    {
        if(is_guest()){
            return '';
        }
        return $this->render('user/addToCollection');
    }
}
