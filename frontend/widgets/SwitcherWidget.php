<?php namespace frontend\widgets;
 

/**
 * switcher widget
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class SwitcherWidget extends \yii\bootstrap\Widget
{
 
    public $items; // mm, in
    public $active;
    
    public $actionUrl;
    public $ajaxParams = [];
    public $jsCallback;
    /**
     * prepare all required information for widget
     * get total likes
     * check if current user has liked this model
     */
    public function init()
    {
        parent::init();
    }

    /**
     * 
     * @return type
     */
    public function run()
    {
        return $this->render('switcher');
    }
}
