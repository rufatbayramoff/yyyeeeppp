<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.12.16
 * Time: 16:31
 */

namespace frontend\widgets;

use common\components\JsObjectFactory;
use common\models\base\PsPrinter;
use common\models\Model3d;
use common\models\Ps;
use common\modules\product\interfaces\ProductInterface;
use common\services\Model3dService;
use frontend\models\user\UserFacade;
use frontend\widgets\assets\PrintByPsPrinterAssets;
use Yii;
use yii\base\Widget;

class PrintByPsPrinterWidget extends Widget
{
    // You can set ps or psPrinter

    /** @var  Ps */
    public $ps = null;

    /** @var  PsPrinter */
    public $psPrinter = null;

    public function run()
    {
        $user = UserFacade::getCurrentUser();
        /** @var Model3dService $model3dService */
        $model3dService = Yii::createObject(Model3dService::class);
        if ($user) {
            /** @var Model3d[] $allMyModels */
            $allMyModels = Model3d::find()->user($user)->productStatus([ProductInterface::STATUS_DRAFT])->all();
            $myModels = [];
            foreach ($allMyModels as $myModel) {
                if ($model3dService->isAvailableForCurrentUser($myModel) && $myModel->isCalculatedProperties()) {
                    $myModels[] = $myModel;
                }
            }
            $collections = $user->userCollections;
        } else {
            $myModels = [];
            $collections = [];
        }

        if ($this->psPrinter) {
            $ps = $this->psPrinter->ps;
        } else {
            $ps = $this->ps;
        }


        $this->registerAssets();

        return $this->render(
            'PrintByPsPrinter',
            [
                'ps'          => $ps,
                'psPrinter'   => $this->psPrinter,
                'myModels'    => $myModels,
                'collections' => $collections
            ]
        );
    }

    /**
     * register required assets
     */
    protected function registerAssets()
    {
        $this->getView()->registerAssetBundle(PrintByPsPrinterAssets::class);
        JsObjectFactory::createJsObject(
            'printByPsPrinterClass',
            'printByPsPrinterObj',
            [],
            $this->getView()
        );
    }

    public static function getPsWidgetHtmlStyle()
    {
        return '<link href="' . param('server') . '/css/embed-remote.css" rel="stylesheet" />';
    }
}