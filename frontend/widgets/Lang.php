<?php namespace frontend\widgets;

use common\models\SystemLang;

class Lang extends \yii\bootstrap\Widget
{
    /**
     *
     * @return type
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $langs = SystemLang::getAllActive();
        $current = SystemLang::getCurrent();
        return $this->render('site/lang', [
            'current' => $current,
            'langs' => $langs,
        ]);
    }
}
