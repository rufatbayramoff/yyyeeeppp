<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.06.16
 * Time: 12:59
 */
namespace frontend\widgets;

use backend\assets\AppAsset;
use common\components\ps\locator\materials\MaterialColorsItem;
use common\models\base\PrinterMaterialGroup;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use frontend\widgets\assets\ColorSelectorWidgetAsset;
use yii\base\Widget;

class ColorSelectorWidget extends Widget
{
    /**
     *
     * @var bool
     */
    public $isOneMaterialForKit;

    /**
     * For jpg files can`t select material, if it`s not material for all kit.
     *
     * @var bool
     */
    public $isCantSelectMaterial;

    /**
     *
     * @var
     */
    public $isModelKit;

    /**
     *
     * @var PrinterMaterialGroup
     */
    public $selectedMaterialGroup;

    /**
     *
     * @var PrinterColor
     */
    public $selectedColor;


    /**
     *
     * @var MaterialColorsItem[]
     */
    public $materialList;


    /**
     *
     * @var MaterialColorsItem[]
     */
    public $materialListUsageFilter;

    /**
     * @var string []
     */
    public $materialListOriginCodes;

    /**
     *
     * @var string
     */
    public $formName;

    public $reloadLink;

    public $selectUsage;
    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        return $this->render(
            'ColorSelectorWidget',
            [
                'widget' => $this,
            ]
        );
    }
}