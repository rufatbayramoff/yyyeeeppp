<?php
/**
 * Created by mitaichik
 */

namespace frontend\widgets;


use common\models\model3d\PrintInstruction;
use yii\base\Widget;
use yii\widgets\DetailView;

/**
 * Class PrintInstructionWidget
 * @package frontend\widgets
 */
class PrintInstructionWidget extends Widget
{
    const FORMAT_DETAIL = 'detail';
    const FORMAT_TEXT = 'text';

    /**
     * @var PrintInstruction
     */
    public $model;

    /**
     * @var string
     */
    public $format = self::FORMAT_DETAIL;

    /**
     * @return string
     */
    public function run()
    {
        $model = $this->model;

        if (!$model) {
            return "";
        }

        switch ($this->format) {

            case self::FORMAT_DETAIL : {

                return DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        ['attribute' => 'rafts', 'visible' => !empty($model->rafts)],
                        ['attribute' => 'supports', 'visible' => !empty($model->supports)],
                        ['attribute' => 'resolution', 'visible' => !empty($model->resolution)],
                        ['attribute' => 'infill', 'visible' => true],
                        ['attribute' => 'printInstruction', 'visible' => !empty($model->printInstruction)],
                        ['attribute' => 'postPrintInstruction', 'visible' => !empty($model->postPrintInstruction)],
                        ['attribute' => 'commonInstruction', 'visible' => !empty($model->commonInstruction)],
                    ]
                ]);
            }

            case self::FORMAT_TEXT : {

                $res = '';

                foreach ($model->attributes as $attr => $val) {

                    if (!$val) {
                        continue;
                    }

                    $val = \H($val);
                    $res.= "<span>{$model->getAttributeLabel($attr)}</span> : <span>$val</span>;<br/>";
                }

                return $res;
            }
        }

        return "";
    }
}