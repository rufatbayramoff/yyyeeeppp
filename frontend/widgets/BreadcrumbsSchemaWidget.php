<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class BreadcrumbsSchemaWidget extends Widget
{
    public $breadcrumpsItems = [];

    public function run()
    {
        $itemListElement = [];
        $item            = [
            '@context' => 'https://schema.org',
            '@type'    => 'BreadcrumbList'
        ];
        $position        = 1;
        foreach ($this->breadcrumpsItems as $itemName => $itemUrl) {
            $itemListElement[] = [
                '@type'    => 'ListItem',
                'position' => $position,
                'item'     =>
                    [
                        '@id'  => $itemUrl,
                        'name' => $itemName
                    ]
            ];
            $position++;
        }
        $item["itemListElement"] = $itemListElement;
        echo "\n" . Html::script(json_encode($item, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES), ['type' => 'application/ld+json']);
    }
}