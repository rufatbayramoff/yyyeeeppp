<?php namespace frontend\widgets;

use common\models\SystemLang;

class LangWidget extends \yii\bootstrap\Widget
{
    /**
     *
     * @return type
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $langs = SystemLang::getAllActiveForWidget();
        $current = SystemLang::getCurrent();
        return $this->render('site/langWidget', [
            'current' => $current,
            'langs' => $langs,
        ]);
    }
}
