<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.12.16
 * Time: 11:57
 */

namespace frontend\widgets;

use common\models\Ps;
use Yii;
use yii\base\Widget;

class PsPrintServiceReviewStarsWidget extends Widget
{
    /** @var Ps */
    public $ps;
    public $reviewsRating;
    public $reviewsCount;
    public $fromReviewCount = 2;
    /**
     * add item schema aggregated rating
     *
     * @var bool
     */
    public $withSchema = true;

    /**
     * with js plugin or not
     *
     * @var bool
     */
    public $withoutJs = false;

    public function run()
    {
        $reviewsCount  = $this->reviewsCount !== null ? $this->reviewsCount : $this->ps->psCatalog->rating_count ?? 0;
        $reviewsRating = $this->reviewsRating !== null ? $this->reviewsRating : $this->ps->psCatalog->rating_avg ?? 0;
        if (!$reviewsCount) {
            return '';
        }

        return $this->render(
            'PsPrintServiceReviewStars',
            [
                'company'         => $this->ps,
                'withSchema'      => $this->withSchema,
                'withoutJs'       => $this->withoutJs,
                'reviewsRating'   => $reviewsRating,
                'reviewsCount'    => $reviewsCount,
                'fromReviewCount' => $this->fromReviewCount
            ]
        );
    }
}