<?php namespace frontend\widgets;

use common\components\JsObjectFactory;
use common\models\base\StoreUnit;
use frontend\models\model3d\Model3dFacade;
use frontend\widgets\assets\StoreUnitWidgetAssets;

/**
 * Description of StoreUnitWidget
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class StoreUnitWidget extends \yii\bootstrap\Widget
{

    /**
     * if viewed in store
     */
    const MODE_STORE = 'store';

    const MODE_MINI = 'mini';

    /**
     * if view in public profile
     */
    const MODE_VIEW = 'public';

    /**
     * if viewd in collection
     */
    const MODE_DESIGNER = 'designer';


    /**
     * @var string
     */
    public $uid;

    /**
     * current 3d model
     *
     * @var StoreUnit
     */
    public $storeUnit;

    /**
     * hide author name
     *
     * @var boolean
     */
    public $hideAuthor = false;

    /**
     * default widget mode
     *
     * @var string
     */
    public $mode = 'public';


    /**
     * additional url params
     *
     * @var array
     */
    public $additionalUrlParams = [];


    /**
     * @var bool
     */
    public $hideLikes = false;


    /**
     * @var bool
     */
    public $hidePrice = false;

    /**
     * If parameter false - show store unit designer cost,
     *              true - show real print cost, js request calculating
     *
     * @var bool
     */
    public $jsCalculatePrice = false;


    /**
     * If $jsCalculatePrice used. You can set $jsCalculatePriceForPrinter to calculated price for special printer
     *
     * @var null
     */
    public $jsCalculatePriceForPrinter = null;


    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $this->uid = mt_rand(0, 99999) . '_' . mt_rand(0, 99999);
        $data      = [];

        $model3d                    = $this->storeUnit->model3d;
        $hidePrice                  = $this->hidePrice;
        $hideAuthor                 = $this->hideAuthor;
        $hideLikes                  = $this->hideLikes;
        $jsCalculatePrice           = $this->jsCalculatePrice;
        $jsCalculatePriceForPrinter = $this->jsCalculatePriceForPrinter;
        $authorLink                 = '';
        $coverImage                 = \frontend\models\model3d\Model3dFacade::getCover($model3d);
        $title                      = H($model3d->title);
        $itemLink                   = Model3dFacade::getStoreUrl($model3d, $this->additionalUrlParams);
        $itemLink                   = param('server') . $itemLink;

        $userCurrency = $model3d->company ? $model3d->company->currency : \lib\money\Currency::USD;
        $mode         = $this->mode;
        $price        = displayAsMoney($model3d->getPriceMoneyByQty(1));


        $this->getView()->registerAssetBundle(\frontend\assets\CatalogAsset::class);
        if ($jsCalculatePrice) {
            $this->getView()->registerAssetBundle(StoreUnitWidgetAssets::class);
            JsObjectFactory::createJsObject(
                'storeUnitElementClass',
                'storeUnitElementObj_' . $this->uid,
                [
                    'uid' => $this->uid
                ],
                $this->getView()
            );
        }


        return $this->render(
            'catalog/storeUnit',
            compact(
                'mode',
                'data',
                'model3d',
                'coverImage',
                'userCurrency',
                'authorLink',
                'title',
                'itemLink',
                'price',
                'hideAuthor',
                'hideLikes',
                'hidePrice',
                'jsCalculatePrice',
                'jsCalculatePriceForPrinter'
            )
        );
    }
}
