<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace frontend\widgets;

use common\components\JsObjectFactory;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBaseInterface;
use common\models\base\Model3dImg;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\modules\model3dRender\models\RenderImageUrl;
use common\services\Model3dService;
use frontend\assets\DropzoneAsset;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class ModelFotoramaWidget extends Widget
{

    /**
     * @var Model3dBaseInterface
     */
    public $model;

    /**
     * @var bool
     */
    public $edit = false;

    /**
     * show renders of snapped files for this order
     *
     * @var int
     */
    public $snapOrder = 0;

    /**
     *
     */
    public $currentSelectedFileInfo;

    /**
     * Options for fotorama
     *
     * @var array
     */
    public $fotoramaOptions = [
        'class'                => 'fotorama',
        'data-allowfullscreen' => 'true',
        'data-width'           => '100%',
        'data-maxwidth'        => '100%',
        'data-ratio'           => '4/3',
        'data-loop'            => 'true',
        'data-shadows'         => 'false',
        'data-nav'             => 'thumbs',
        'data-swipe'           => 'false',
        'data-click'           => 'true',
        'data-thumbwidth'      => 48,
        'data-thumbheight'     => 48
    ];

    public $disableFullImageSize = false;
    public $disable3dView = false;
    public $allowAutoRotate = false;


    const ITEM_TYPE_MODEL = 'model';
    const ITEM_TYPE_IMAGE = 'image';

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!($this->model instanceof Model3dBaseInterface)) {
            throw new InvalidConfigException('Model must be Model3d');
        }
        if ($this->disableFullImageSize) {
            $this->fotoramaOptions['data-allowfullscreen'] = 'false';
        }

        if (\Yii::$app->mobileDetect->isMobile()) {
            $this->fotoramaOptions['data-arrows'] = 'always';
        }
    }

    /**
     *
     */
    public function run()
    {
        $model = $this->model;
        $files = Model3dService::getModel3dActivePartsAndImagesList($model);
        $renders = Model3dFacade::getRenderedImages($model->getActiveModel3dParts());
        $cover = Model3dFacade::getCover($this->model, $this->currentSelectedFileInfo ? $this->currentSelectedFileInfo->file_id : null);
        echo Html::beginTag(
                'div',
                $this->fotoramaOptions + [
                    'id'              => $this->id,
                    'data-startindex' => $this->resolveCoverIndex($model, $renders, $files, $cover)
                ]
            ) . "\n\n";
        // render images
        /** @var Model3dBaseImgInterface|Model3dImg $image */
        foreach ($model->model3dImgs as $image) {
            if (!isset($files[$image->file_id])) {
                continue;
            }
            $imgUrlThumb = ImageHtmlHelper::getThumbUrlForFile($image->file, ImageHtmlHelper::DEFAULT_WIDTH, ImageHtmlHelper::DEFAULT_HEIGHT);
            echo Html::beginTag(
                    'div',
                    [
                        'data-img'     => $imgUrlThumb,
                        'data-type'    => self::ITEM_TYPE_IMAGE,
                        'data-id'      => $image->id, // Id of Model3dImg,
                        'data-file-id' => $image->file_id, // Id of File,
                        'data-is-cover' => $this->model->cover_file_id == $image->file_id ? 1 : 0
                    ]
                ) . "\n\n";

            $this->renderImageButtons();
            echo Html::endTag('div') . "\n\n";
            if ($this->edit) {
                echo "\n<span class='slider-item-delete tsi tsi-remove-c'></span>";

            }
        }
        // render models
        foreach ($renders as $render) {

            echo Html::beginTag(
                    'div',
                    [
                        'data-img'                       => $render->url,
                        'data-type'                      => self::ITEM_TYPE_MODEL,
                        'data-title'                     => $render->filename,
                        'data-caption'                   => $render->caption,
                        'data-file-id'                   => $render->fileId,
                        'data-printer-color-id'          => $render->printerColorId,
                        'data-printer-color-rgb'         => $render->printerColorRgb,
                        'data-printer-material-group-id' => $render->printerMaterialGroupId,
                        'data-color-rgb'                 => $render->printerColorRgb,
                        'data-is-replica-model'          => $model instanceof Model3dReplica,
                        'data-md5sum'                    => $render->md5sum,
                        'data-file-ext'                  => RenderImageUrl::getRenderExtension($render->fileExt),
                        'data-render-url-base'           => $render->renderUrlBase,
                        'data-model-part-id'             => $render->model3dPartId,
                        'data-is-parsed'                 => $render->isParsed ? "1" : "0",
                        'data-model3did'                 => $model->id
                    ]
                ) . "\n";

            $this->renderModelButtons();
            echo Html::endTag('div');
        }
        echo Html::endTag('div');
        if ($this->edit) {
            echo '';
        }
        $this->registerAssets();
    }


    /**
     * Resolve index of cover image
     *
     * @param Model3dBaseInterface|Model3d $model
     * @param array $renders
     * @param array $files
     * @param array $cover
     * @return int
     */
    private function resolveCoverIndex(Model3dBaseInterface $model, array $renders, array $files, array $cover)
    {
        $coverFileId = $cover['file_id'];
        $index = 0;
        foreach ($model->model3dImgs as $image) {
            if (!isset($files[$image->file_id])) {
                continue;
            }
            if ($image->file_id == $coverFileId) {
                if (empty(app('request')->get('color'))) {
                    return $index;
                }
            }

            $index++;
        }
        if (!empty(app('request')->get('color'))) {
            return $index;
        }
        foreach ($renders as $render) {
            if ($render['fileId'] == $coverFileId) {
                return $index;
            }
            $index++;
        }
        return $index;
    }

    private function registerAssets()
    {
        if ($this->edit) {
            $this->getView()->registerAssetBundle(DropzoneAsset::class);
            JsObjectFactory::createJsObject(
                'model3dRenderRouteClass',
                'model3dRenderRoute',
                [
                    'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl.'/'
                ],
                $this->getView()
            );
        }
    }

    /**
     * Render buttons for image file
     * When edit this buttons - not forget update model-fotorama-widget.js
     */
    private function renderImageButtons()
    {
        $buttons = [];

        // If it not cover image

        if ($this->edit) {
            $buttons[] = [
                'title'   => _t('site.model', 'Add'),
                'icon'    => 'plus',
                'options' => ['class' => 'js-add-image fotorama-ignore-click dz-clickable']
            ];
            $buttons[] = [
                'title'   => _t('front.site', 'Set as cover image'),
                'icon'    => 'checkmark',
                'options' => ['class' => 'js-set-cover fotorama-ignore-click']
            ];
            // Remove image

            $buttons[] = [
                'title'   => _t('front', 'Remove image'),
                'icon'    => 'bin',
                'options' => ['class' => 'js-remove-image fotorama-ignore-click'],
            ];
            $buttons[] = [
                'title'   => _t('front', 'Crop image'),
                'icon'    => 'crop',
                'options' => ['class' => 'js-crop-image fotorama-ignore-click'],
            ];
        }

        $this->renderButtons($buttons);
    }


    private function renderModelButtons(): void
    {
        $buttons = [];

        // If it not cover image

        if ($this->edit) {
            $buttons[] = [
                'title'   => _t('site.model', 'Add'),
                'icon'    => 'plus',
                'options' => ['class' => 'fotorama-ignore-click js-add-image'],
            ];
            $buttons[] = [
                'title'   => _t('front', 'Remove'),
                'icon'    => 'bin',
                'options' => ['class' => 'fotorama-ignore-click js-remove-file'],
            ];
        }
        $this->renderButtons($buttons);
    }

    /**
     * @param array $buttons
     */
    private function renderButtons(array $buttons)
    {
        echo '<div class="model-slider__controls">';
        foreach ($buttons as $button) {
            $options = isset($button['options']) ? $button['options'] : [];
            Html::addCssClass($options, 'model-slider__controls__item');

            echo Html::beginTag('div', $options);
            echo '<span class="tsi tsi-' . $button['icon'] . '"> </span>';
            echo '<span class="model-slider__controls__item__hint">' . $button['title'] . '</span>';
            echo '</div>';
        }
        echo '</div>';
    }
}
