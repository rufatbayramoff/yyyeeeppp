<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 18.08.2017
 * Time: 9:51
 */

namespace frontend\widgets;


use common\models\PsPrinter;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\modules\catalogPs\repositories\PrintedFileRepository;
use common\modules\catalogPs\repositories\PrinterMaterialRepository;
use common\modules\catalogPs\repositories\PrinterTechnologyRepository;
use common\modules\catalogPs\repositories\PsPrinterEntityRepository;
use frontend\components\UserSessionFacade;
use yii\base\Widget;

/**
 * Class PsCardWidget
 *
 * @package frontend\widgets
 */
class PsCardWidget extends Widget
{
    public $psLinks;
    public $perPage = 6;

    /**
     * @var CatalogSearchForm
     */
    private $searchForm;
    /**
     * @var PrinterMaterialRepository
     */
    private $materialRepo;

    /**
     * @var PrinterTechnologyRepository
     */
    private $technologyRepository;

    /**
     * container class - with responsive container
     * if in swiper - use 'main-page-models__item swiper-slide'
     *
     * @var bool
     */
    public $containerClass = 'responsive-container';

    /**
     * @param CatalogSearchForm $searchForm
     * @param PrinterMaterialRepository $materialRepo
     * @param PrinterTechnologyRepository $technologyRepository
     */
    public function injectDependencies(CatalogSearchForm $searchForm, PrinterMaterialRepository $materialRepo, PrinterTechnologyRepository $technologyRepository)
    {
        $this->searchForm = $searchForm;
        $this->materialRepo = $materialRepo;
        $this->technologyRepository = $technologyRepository;
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $searchForm = $this->searchForm;
        $searchForm->perPage = $this->perPage;
        $searchForm->psLinks = $this->psLinks;
        $searchForm->sort = CatalogSearchForm::SORT_MANUAL;
        if(empty($this->psLinks)){
            return '';
        }
        /* @var $psPrintersRepo PsPrinterEntityRepository */
        $psPrintersRepo = \Yii::createObject(['class' => PsPrinterEntityRepository::class]);
        $dataProvider = $searchForm->getDataProvider($psPrintersRepo);

        $listView = \Yii::createObject(
            [
                'class'        => \yii\widgets\ListView::class,
                'dataProvider' => $dataProvider,
                'itemView'     => '@frontend/widgets/views/PsCardWidgetView',
                'itemOptions'  => ['tag' => false],
                'viewParams'   => [
                    'containerClass'   => $this->containerClass,
                    'printedFilesRepo' => new PrintedFileRepository($dataProvider->getModels()),
                ]
            ]
        );
        return $listView->renderItems();
    }
}