<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\widgets;

/**
 * Description of ImageCropper2
 *
 * @author DeFacto
 */
class ImageCropper2 extends \cozumel\cropper\ImageCropper{
    
    
      protected function registerPlugin($options) {
        $view = $this->getView();
        \cozumel\cropper\ImageCropperAsset::register($view);

        $id = $this->id;

        $options = json_encode($options);

        //better way to do this? let me know!
        $options = str_replace('"' . $this->onInit . '"', $this->onInit, $options);
        $options = str_replace('"' . $this->onSelectStart . '"', $this->onSelectStart, $options);
        $options = str_replace('"' . $this->onSelectChange . '"', $this->onSelectChange, $options);
        $options = str_replace('"' . $this->onSelectEnd . '"', $this->onSelectEnd, $options);

        $view->registerJs("jQuery('{$id}').imgAreaSelect(" . $options . ");");
    }
}
