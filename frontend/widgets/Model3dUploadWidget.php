<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.12.16
 * Time: 18:27
 */

namespace frontend\widgets;

use common\components\ArchiveManager;
use common\components\ArrayHelper;
use common\components\JsObjectFactory;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBasePartInterface;
use frontend\widgets\assets\Model3dUploadAssets;
use yii\base\Widget;

class Model3dUploadWidget extends Widget
{
    /**
     * @var array
     */
    public $model3dExtensions = [];

    /**
     * @var array
     */
    public $imagesExtensions = [];

    /**
     * @var array
     */
    public $otherExtensions = [];

    /**
     * $model3dExtensions + $imagesExtensions
     * @var array
     */
    public $allowedExt = [];

    /**
     * @var array
     */
    public $additionalParams = [];

    /**
     * Already uploaded files list
     *
     * @var array
     */
    public $files = [];


    /**
     * @var string
     */
    public $textComment = '';


    /**
     * @var integer
     */
    public $maxFileSize = null;


    /**
     * @var string
     */
    public $uploadUrl = null;


    /**
     * authorization required
     *
     * @var null
     */
    public $authRequired = null;

    /**
     * Use new widget with place order state
     *
     * @var bool
     */
    public $isPlaceOrderState = 0;

    /**
     * @var
     */
    public $isWidget = 1;

    /**
     * Change default view file
     *
     * @var string
     */
    public $viewType;

    public function init()
    {
        if (!$this->imagesExtensions) {
            $this->imagesExtensions = Model3dBaseImgInterface::ALLOWED_FORMATS;
        }
        if (!$this->model3dExtensions) {
            $this->model3dExtensions = Model3dBasePartInterface::ALLOWED_FORMATS;
        }
        if (!$this->otherExtensions) {
            $this->otherExtensions = ArchiveManager::ARCHIVE_FORMATS;
        }

        $this->allowedExt = ArrayHelper::merge($this->imagesExtensions, $this->model3dExtensions);

        if (!$this->maxFileSize) {
            $this->maxFileSize = app('setting')->get('upload.maxfilesize', 57);
        }
        if (!$this->textComment) {
            if ($this->getUtmSource()) {
                $this->textComment = _t('front.upload', 'Please upload your 3D models');
            } else {
                $this->textComment = _t('front.upload', 'Drop your pictures and 3D files to create a model or kit');
            }
        }

        if (!$this->uploadUrl) {
            $this->uploadUrl = '/catalog/upload-model3d/add-file';
        }
    }

    public function getUtmSource()
    {
        if (array_key_exists('utmSource', $this->additionalParams) && $this->additionalParams['utmSource']) {
            return $this->additionalParams['utmSource'];
        }
        return '';
    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $this->registerAssets();
        return $this->render(
            'Model3dUpload'.ucfirst($this->viewType),
            [
                'widget' => $this,
            ]
        );
    }

    /**
     * register required assets
     */
    protected function registerAssets()
    {
        $this->getView()->registerAssetBundle(Model3dUploadAssets::class);
        JsObjectFactory::createJsObject(
            'model3dUploadClass',
            'model3dUploadObj',
            [
                'maxFileSize'       => $this->maxFileSize,
                'uploadUrl'         => $this->uploadUrl,
                'allowedExtensions' => $this->allowedExt,
                'imagesExtensions'  => $this->imagesExtensions,
                'model3dExtensions' => $this->model3dExtensions,
                'otherExtensions'   => $this->otherExtensions,
                'additionalParams'  => $this->additionalParams,
                'authRequired'      => $this->authRequired,
                'isPlaceOrderState' => $this->isPlaceOrderState,
                'isWidget'          => $this->isWidget,
            ],
            $this->getView()
        );
    }
}