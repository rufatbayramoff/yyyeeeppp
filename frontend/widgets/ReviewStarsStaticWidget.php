<?php
/**
 * Created by mitaichik
 */

namespace frontend\widgets;


use yii\base\Widget;

class ReviewStarsStaticWidget extends Widget
{
    /**
     * @var float
     */
    public $rating;

    /**
     * @return string
     */
    public function run()
    {
        $width = (int) ($this->rating / (5 / 100));
        return $this->render("ReviewStarsStaticWidget", ['width' => $width]);
    }
}