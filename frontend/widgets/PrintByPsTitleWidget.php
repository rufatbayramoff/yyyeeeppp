<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.02.17
 * Time: 17:50
 */

namespace frontend\widgets;

use common\components\JsObjectFactory;
use common\models\base\PsPrinter;
use common\models\Model3d;
use common\models\Ps;
use common\services\Model3dService;
use frontend\models\model3d\Model3dFacade;
use frontend\models\user\UserFacade;
use frontend\widgets\assets\PrintByPsPrinterAssets;
use yii\base\Widget;

class PrintByPsTitleWidget extends Widget
{
    // You can set ps or psPrinter

    /** @var  Ps */
    public $ps = null;

    public function run()
    {
        if (!$this->ps) {
            return ;
        }
        return $this->render(
            'PrintByPsTitle',
            [
                'ps'          => $this->ps,
            ]
        );
    }
 }