<?php
/**
 * User: nabi
 */

namespace frontend\widgets;


use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderHistory;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class StoreOrderStatusHistory extends Widget
{
    /**
     * @var StoreOrderAttemp
     */
    public $storeOrderAttemp;


    /**
     */
    public function init()
    {
        parent::init();
    }

    /**
     *
     * @return mixed
     */
    public function run()
    {
        $historyDates = $this->getHistoryDates($this->storeOrderAttemp);
        $historyDates = $this->fixHistoryDatesStatus($historyDates);
        return $this->render('itemHistory', ['history'=>$historyDates]);
    }

    private function fixHistoryDatesStatus($historyDates)
    {
        $lastDateChecked = 0;
        foreach($historyDates as $k=>$historyDate){
            if($historyDate['checked']){
                $lastDateChecked = $k;
            }
        }
        foreach($historyDates as $k=>$historyDate){
            if($k <= $lastDateChecked){
                $historyDate['checked'] = true;
            }
            $historyDates[$k] = $historyDate;
        }
        return $historyDates;
    }
    private function getHistoryDates(StoreOrderAttemp $storeOrderAttemp)
    {
        $attempStatuses = $storeOrderAttemp->getStatuses();
        $storeOrder = $storeOrderAttemp->order;
        $attempDates = $storeOrderAttemp->dates;
        $storeOrderHistory = StoreOrderHistory::find()
                                ->where(['action_id'=>StoreOrderHistory::ORDER_ATTEMPT_CHANGE_STATUS, 'order_id'=>$storeOrder->id])
                                ->all();
        $historyByCommentFn = function($cmt) use($storeOrderHistory){
            $indexed = ArrayHelper::index($storeOrderHistory, 'comment');
            if(!empty($indexed[$cmt])){
                return $indexed[$cmt];
            }
            return null;
        };
        $statusHistory = [
            [
                'title' => !empty($storeOrder->billed_at)? _t('site.order', 'Placed'): _t('site.order', 'Pending Payment'),
                'checked' => !empty($storeOrder->billed_at),
                'comment' => !empty($storeOrder->billed_at)? app('formatter')->asDate($storeOrder->billed_at) : ''
            ],
            [
                'title' =>  $attempStatuses[StoreOrderAttemp::STATUS_PRINTING],
                'checked' => !empty($attempDates->accepted_date),
                'comment' => !empty($attempDates->accepted_date)? app('formatter')->asDate($attempDates->accepted_date) : ''
            ],
            [
                'hidden' => true,
                'title' =>  $attempStatuses[StoreOrderAttemp::STATUS_PRINTED],
                'checked' => !empty($attempDates->finish_print_at),
                'comment' => !empty($attempDates->finish_print_at)? app('formatter')->asDate($attempDates->finish_print_at) : ''
            ],
            [
                'title' =>  $attempStatuses[StoreOrderAttemp::STATUS_PRINTED],
                'checked' => !empty($historyByCommentFn('Attempt is ready to send')),
                'comment' => !empty($historyByCommentFn('Attempt is ready to send')) ?
                            app('formatter')->asDate($historyByCommentFn('Attempt is ready to send')->created_at) : ''
            ],
            [
                'title' =>  $attempStatuses[StoreOrderAttemp::STATUS_SENT],
                'checked' => !empty($attempDates->shipped_at),
                'comment' => !empty($attempDates->shipped_at)? app('formatter')->asDate($attempDates->shipped_at) : ''
            ],
            [
                'title' =>  $attempStatuses[StoreOrderAttemp::STATUS_DELIVERED],
                'checked' => !empty($attempDates->delivered_at),
                'comment' => !empty($attempDates->delivered_at)? app('formatter')->asDate($attempDates->delivered_at) : ''
            ],
            [
                'title' =>  _t('site.order', 'Completed'), //$attempStatuses[StoreOrderAttemp::STATUS_RECEIVED],
                'checked' => !empty($attempDates->received_at),
                'comment' => !empty($attempDates->received_at)? app('formatter')->asDate($attempDates->received_at) : ''
            ],
        ];
        return $statusHistory;
    }
}
