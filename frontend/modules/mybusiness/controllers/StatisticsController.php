<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\controllers;

use frontend\components\AuthedController;

class StatisticsController extends MybusinessController
{
    public function actionIndex(){

        $this->view->params['section'] = 'mybusiness/widgets';
        return $this->render('index');
    }
}
