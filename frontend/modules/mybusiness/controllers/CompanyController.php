<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\controllers;

use common\components\exceptions\AssertHelper;
use common\models\CompanyCategory;
use common\models\CompanyServiceCategory;
use common\models\Ps;
use common\models\ps\ChangePsUrlForm;
use common\models\repositories\UserRepository;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\services\ReportPhoneProblemService;
use common\services\StoreOrderService;
use frontend\models\ps\AddPsForm;
use frontend\models\ps\PsFacade;
use frontend\models\user\UploadDocumentForm;
use frontend\models\user\UserFacade;
use frontend\models\user\verifyPhone\ConfirmForm;
use frontend\models\user\verifyPhone\SendForm;
use frontend\modules\mybusiness\actions\EditCompanyAction;
use frontend\modules\mybusiness\actions\NewPsAction;
use frontend\modules\mybusiness\components\ResolvePsTrait;
use frontend\modules\mybusiness\models\CompanyProfileForm;
use frontend\modules\mybusiness\models\CompanyServiceForm;
use frontend\modules\mybusiness\models\SearchClientForm;
use frontend\modules\mybusiness\services\CompanyService;
use frontend\modules\mybusiness\services\CompanyServiceManager;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class CompanyController extends MybusinessController
{
    use ResolvePsTrait;

    private const TYPE_DESIGNING = 'designing';
    public $layout = 'aboutLayout';

    /**
     * @var CompanyService
     */
    protected $companyService;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'sendVerifyPhone' => ['post'],
                    'verifyPhone'     => ['post'],
                    'uploadDocument'  => ['post'],
                    'delete-ps'       => ['delete'],
                    'restore-ps'      => ['post'],
                    'create-services' => ['post'],
                    'report-support'  => ['post'],
                ]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'update-ps' => [
                'class' => EditCompanyAction::class,
            ],
            'new-ps' => [
                'class' => NewPsAction::class,
            ]
        ];
    }

    /**
     * @var StoreOrderService
     */
    protected $storeOrderService;

    /**
     * @var CompanyServiceManager
     * */
    protected $serviceManager;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserIdentityProvider
     */
    protected $userIdentityProvider;

    public function injectDependencies(
        StoreOrderService $storeOrderService,
        CompanyServiceManager $serviceManager,
        CompanyService $companyService,
        UserRepository $userRepository,
        UserIdentityProvider $userIdentityProvider
    )
    {
        $this->storeOrderService = $storeOrderService;
        $this->serviceManager = $serviceManager;
        $this->companyService = $companyService;
        $this->userRepository = $userRepository;
        $this->userIdentityProvider = $userIdentityProvider;
    }


    public function actionIndex()
    {
        if ($this->user->status === User::STATUS_DRAFT_COMPANY && !($this->user->company->is_backend??false)) {
            $this->setFlashMsg(false, _t('mybusiness.common', 'Please confirm email first.'), false);
            return $this->redirect('/');
        }
        $ps =  PsFacade::getPsByUserId(UserFacade::getCurrentUserId());
        if ($ps && !$ps->isDeleted()) {
            return $this->redirect('/mybusiness/company/edit-ps');
        }
        return $this->redirect('/mybusiness/company/create-ps');
    }


    /**
     * Create Print Service
     *
     * @return string
     *
     * @transacted
     */
    public function actionCreatePs()
    {
        try {
            $ps = $this->resolvePs();
        } catch (HttpException $exception) {
            $ps = null;
        }
        if($ps && $ps->isDeleted()) {
            return $this->redirect(\Yii::$app->urlManager->createUrl('/mybusiness/company/edit-ps'));
        }
        if ($ps && $ps->companyServices) {
            return $this->redirect(\Yii::$app->urlManager->createUrl('/mybusiness/company'));
        }

        if ($ps) {
            return $this->redirect(
                [
                    '/mybusiness/settings',
                    'fromCreatePs' => 1
                ]
            );
        }

        $type = Yii::$app->request->get('type');
        $isForDesigner = ($type == self::TYPE_DESIGNING);
        $ps = AddPsForm::createForCreate($isForDesigner);
        $user = $this->userIdentityProvider->getUser();
        $user->startCompanyCreate();
        $this->userRepository->save($user);
        return $this->render('create_ps', ['model' => $ps]);
    }

    /**
     * Edit Print service
     *
     * @param null $type
     * @return array|string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionEditPs($type = null)
    {
        /** @var AddPsForm $psForm */
        $psForm = $this->resolvePs(AddPsForm::class);

        if ($type == self::TYPE_DESIGNING) {
            $psForm->is_designer = 1;
        }

        return $this->render('create_ps', ['model' => $psForm]);
    }

    public function actionDeletePs()
    {
        /** @var Ps $ps */
        $ps = $this->resolvePs(AddPsForm::class);
        $this->storeOrderService->checkActiveOrder($ps->user);
        try {
            $ps->softDelete();
            $ps->savePs();
        } catch (\DomainException $exception) {
            $this->setFlashMsg(false, _t('mybusiness.common', $exception->getMessage()), false);
        }
        return $this->redirect('/l/become-supplier');

    }

    public function actionRestorePs()
    {
        /** @var Ps $ps */
        $ps = $this->resolvePs(AddPsForm::class);
        try {
            $ps->restore();
            $ps->savePs();
        } catch (\DomainException $exception) {
            $this->setFlashMsg(false, _t('mybusiness.common', $exception->getMessage()), false);
        }
        return $this->redirect('/mybusiness/company');
    }

    /**
     * Update PS url
     *
     * @transacted
     */
    public function actionUpdatePsUrl()
    {
        $ps = $this->resolvePs();
        $form = new ChangePsUrlForm($ps);
        $form->load(Yii::$app->request->getBodyParams(), '');
        AssertHelper::assertValidate($form);
        $form->process();
    }


    /**
     * Send sms to verify phone
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \common\components\exceptions\BusinessException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \lib\geo\GeoNamesException
     * @throws \yii\base\InvalidConfigException
     * @transacted
     */
    public function actionSendVerifyPhone()
    {
        $form = new SendForm(UserFacade::getCurrentUser());
        $form->load(Yii::$app->request->getBodyParams(), '');
        AssertHelper::assertValidate($form);
        $form->process();
        return $this->jsonSuccess(['message'=>_t('site.ps', 'Please check SMS for verification code')]);
    }

    /**
     * get code to verify phone
     *
     * @return array
     * @throws NotFoundHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @transacted
     */
    public function actionVerifyPhone()
    {
        $form = new ConfirmForm(UserFacade::getCurrentUser());
        $form->load(Yii::$app->request->getBodyParams(), '');
        AssertHelper::assertValidate($form);
        $form->process();
        return $this->jsonReturn(_t('site.ps', _t('site.ps', 'Verified')));
    }

    /**
     * Upload document for verify
     *
     * @transacted
     */
    public function actionUploadDocument()
    {
        $form = new UploadDocumentForm(UserFacade::getCurrentUser());
        $form->loadFromPost();
        AssertHelper::assertValidate($form);
        $form->process();
        $this->setFlashMsg(true, _t('site.ps', 'Document successfully uploaded'));
        $this->jsonReturn(_t('site.ps', _t('site.ps', 'Document successfully uploaded')));
    }

    public function actionEmbed()
    {
        return $this->redirect('/mybusiness/widgets/facebook');
    }

    /**
     * @return string
     */
    public function actionServices(): string
    {
        $services = CompanyServiceCategory::find()->isActive()->orderByPosition()->all();
        $companyCategories = CompanyCategory::find()->select('company_service_category_id')->forCompany($this->resolvePs())->all();
        return $this->render('services',['services' => $services,'companyCategories' => $companyCategories]);
    }

    public function actionCreateServices()
    {
        $form = new CompanyServiceForm();
        if($form->load(Yii::$app->request->post(),'') && $form->validate()) {
            $this->serviceManager->create($form, UserFacade::getCurrentUser());
            return $this->asJson(['success' => true, 'message' => _t('site.ps', 'Successfully saved')]);
        }
        return $this->asJson(['success' => false,'errors' => $form->getErrorSummary(true)]);
    }

    public function actionProfile()
    {
        $form = new CompanyProfileForm();
        $company = $this->resolvePs();
        return $this->render('profile',['company' => $company, 'form' => $form]);
    }

    public function actionSaveProfile()
    {
        $form = new CompanyProfileForm();
        if($form->load(Yii::$app->request->post(),'') && $form->upload()) {
            $this->companyService->editProfileForUser($form, UserFacade::getCurrentUser());
            return $this->asJson(['success' => true, 'message' => _t('site.ps', 'Profile updated')]);
        }
        return $this->asJson(['success' => false,'errors' => $form->getErrorSummary(true)]);
    }

    public function actionReportSupport(ReportPhoneProblemService $service)
    {
        $user = UserFacade::getCurrentUser();
        $service->report($user);
        return $this->jsonReturn(_t('site.ps', _t('site.ps', 'We are sorry you don’t receive a confirmation code. Our support will get in contact with you as soon as possible.')));
    }

    public function actionSearchClients()
    {
        $form = new SearchClientForm($this->resolvePs());
        $search =  $form->search(Yii::$app->request->queryParams);
        return $this->jsonReturn($search);
    }
}

