<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\controllers;

use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\modules\CsWindow\services\CsWindowService;

/**
 * Class WidgetsController
 * @package frontend\modules\mybusiness\controllers
 *
 * @property CsWindowService $windowService
 */
class WidgetsController extends MybusinessController
{

    public $layout = 'widgetsLayout';

    public $ps;

    public $windowService;

    public function injectDependencies(
        CsWindowService $windowService
    ) {
        $this->windowService = $windowService;
    }

    public function init()
    {
        parent::init();
        $this->view->params['section'] = 'mybusiness/widgets';
        if (!$this->user) {
            return;
        }
        $this->ps = PsFacade::getPsByUserId($this->user->id);
    }

    public function actionIndex()
    {
        $ps = PsFacade::getPsByUserId($this->user->id);
        if (!$ps) {
            $this->setFlashMsg(false, _t('site.ps', 'Please save business information'));
            return $this->redirect('/mybusiness/company/create-ps');
        }
        if($ps->isDeleted()) {
            $this->setFlashMsg(false, _t('site.ps', 'The manufacturer is currently inactive.'));
            return $this->redirect('/mybusiness/company/create-ps');
        }

        return $this->redirect('/mybusiness/widgets/printers');
    }

    public function actionPrinters()
    {
        $csWindow = $this->user->company ? $this->windowService->getWindowService($this->user->company) : null;

        $windowService = app()->request->get('windowService') ? true : false;


        return $this->render('treatstock-widget', [
            'user'          => $this->user,
            'ps'            => $this->ps,
            'csWindow'      => $csWindow,
            'windowService' => $windowService
        ]);
    }

    public function actionGetQuote()
    {
        return $this->render('get-quote-widget', ['user' => $this->user, 'ps' => $this->ps]);
    }

    public function actionRate()
    {
        return $this->render('rateget-quote-widget', ['user' => $this->user, 'ps' => $this->ps]);
    }

    public function actionAffiliate()
    {
        return $this->render('user-widget', ['user' => $this->user]);
    }
    
    public function actionStoreModels()
    {
        $query = $this->user->getModel3ds()->storePublished($this->user);
        $models = $query->all();
        return $this->render('store-widget', ['user' => $this->user, 'models3d' => $models]);
    }


    public function actionStoreProducts()
    {
        return $this->render('store-products-widget', ['user' => $this->user]);
    }

    public function actionModels()
    {
        return $this->render('model-widget', ['user' => $this->user]);
    }

    public function actionFacebook()
    {
        return $this->render('facebook-app', ['ps' => $this->ps]);
    }

    public function actionApi()
    {
        return $this->render('api', ['ps' => $this->ps]);
    }

    public function actionAffiliateWidget()
    {
        return $this->render('user-widget', ['user' => $this->user]);
    }
}