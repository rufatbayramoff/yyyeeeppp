<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace frontend\modules\mybusiness\controllers;

use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\InvalidModelException;
use common\components\order\TestOrderFactory;
use common\models\base\PrinterTechnology;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\factories\LocationFactory;
use common\models\Printer;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PrinterToProperty;
use common\models\ps\printers\operations\DeleteOperation;
use common\models\PsMachineDelivery;
use common\models\PsPrinter;
use common\models\PsPrinterColor;
use common\models\PsPrinterMaterial;
use common\models\PsPrinterTest;
use common\models\TsInternalPurchase;
use common\models\TsInternalPurchaseCertification;
use common\models\UserLocation;
use common\modules\tsInternalPurchase\repositories\TsInternalPurchaseRepository;
use common\services\PsMachineService;
use common\services\PsPrinterService;
use common\traits\TransactedControllerTrait;
use frontend\components\UserSessionFacade;
use frontend\models\ps\PsPrinterUpdater;
use frontend\models\user\UserFacade;
use lib\geo\GeoNames;
use lib\money\Currency;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\MethodNotAllowedHttpException;

/**
 * Controller for edit PsPrinter
 *
 * @package frontend\controllers\my
 */
class EditPrinterController extends MybusinessController
{
    use TransactedControllerTrait;

    /**
     *
     * @var string
     */
    protected $viewPath = '@frontend/modules/mybusiness/views/services/edit-printer';

    /**
     *
     * @var string
     */
    public $layout = '@frontend/modules/mybusiness/views/services/edit-printer/layout';

    /**
     * @var TestOrderFactory
     */
    protected $testOrderFactory;

    /**
     * @var PsMachineService
     */
    protected $psMachineService;

    /** @var TsInternalPurchaseRepository */
    protected $tsInternalPurchaseRepository;


    /**
     * @param TestOrderFactory $testOrderFactory
     * @param PsMachineService $psMachineService
     */
    public function injectDependencies(TestOrderFactory $testOrderFactory, PsMachineService $psMachineService, TsInternalPurchaseRepository $tsInternalPurchaseRepository)
    {
        $this->psMachineService             = $psMachineService;
        $this->testOrderFactory             = $testOrderFactory;
        $this->tsInternalPurchaseRepository = $tsInternalPurchaseRepository;
        $this->view->params['section']      = 'mybusiness/services';
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Edit printer method
     *
     * @param
     *            $ps_printer_id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionEditPrinter($ps_printer_id)
    {
        $printer                       = $this->resolvePsPrinter($ps_printer_id);
        $this->view->params['printer'] = $printer;
        return $this->render('printer-model');
    }

    /**
     * Update delivery options of PsPrinter
     *
     * @param
     *            $ps_printer_id
     * @return array|string
     * @throws \lib\geo\GeoNamesException
     */
    public function actionEditDeliveryOptions($ps_printer_id)
    {
        $printer                       = $this->resolvePsPrinter($ps_printer_id);
        $this->view->params['printer'] = $printer;
        return $this->render(
            'delivery-options',
            [
                'psMachine' => $printer->companyService,
            ]
        );
    }

    /**
     * Edit material options
     *
     * @param int $ps_printer_id
     *            PsPrinter id
     * @return null|string
     */
    public function actionEditMaterialOptions($ps_printer_id)
    {
        $printer                       = $this->resolvePsPrinter($ps_printer_id);
        $this->view->params['printer'] = $printer;
        return $this->render(
            'material-options',
            [
                'model' => $printer
            ]
        );
    }

    /**
     * Update printer
     *
     * @return array @transacted
     */
    public function actionUpdatePrinter()
    {
        $post      = Yii::$app->request->getBodyParams();
        $psPrinter = $this->resolvePsPrinter($post['id']);

        PsPrinterUpdater::createForUpdate($psPrinter->companyService)
            ->updatePrinter($post)
            ->updateRedefinedProperties($post['redifinedProperties'])
            ->unpdatePrinterModerationStatus()
            ->invalidatePrintersCahce();

        return $this->jsonReturn(self::jsonPsPrinter($psPrinter));
    }

    /**
     * Update printer materials
     *
     * @return array
     * @throws MethodNotAllowedHttpException @transacted
     */
    public function actionUpdateMaterials()
    {
        $post      = Yii::$app->request->getBodyParams();
        $psPrinter = $this->resolvePsPrinter($post['id']);
        PsPrinterUpdater::createForUpdate($psPrinter->companyService)
            ->updatePrinter($post)
            ->updateMaterials($post['materials'], $post['measure'])
            ->unpdatePrinterModerationStatus()
            ->invalidatePrintersCahce();

        return $this->jsonReturn(self::jsonPsPrinter($psPrinter));
    }

    /**
     * Update delivery options
     *
     * @return array
     * @throws MethodNotAllowedHttpException @transacted
     */
    public function actionUpdateDelivery()
    {
        $post      = Yii::$app->request->getBodyParams();
        $psMachine = $this->resolvePsMachine($post['id']);
        PsPrinterUpdater::createForUpdate($psMachine)
            ->updateDeliveryTypes($post)
            ->updateLocation($post['location'])
            ->unpdatePrinterModerationStatus()
            ->invalidatePrintersCahce();

        return $this->jsonReturn(self::jsonPsMachine($psMachine));
    }

    public function actionIndex()
    {
        return $this->renderContent('test');
    }

    /**
     * Return information about printer and their materials and colors
     *
     * @param int $id
     *            Id of Printer
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPrinter($id)
    {
        /** @var Printer $printer */
        $printer = Printer::tryFindByPk($id);
        return $this->jsonReturn(self::jsonPrinter($printer));
    }

    /**
     *
     * @param
     *            $id
     * @return array
     * @throws BusinessException
     * @throws \common\components\operations\PostValidateException
     * @throws \common\components\operations\PreValidateException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     * @transacted
     */
    public function actionDeletePrinter($id)
    {
        $psPrinter = $this->resolvePsPrinter($id);
        $operation = new DeleteOperation($psPrinter);
        if (!$operation->execute()) {
            return $this->jsonReturn(
                [
                    'errors' => $operation->getErrors()
                ],
                406
            );
        }
        return self::jsonPsPrinter($psPrinter);
    }

    /**
     *
     * @param int $id
     * @return array
     * @throws BusinessException
     * @throws InvalidModelException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     * @transacted
     */
    public function actionChangePrinterStatus($id)
    {
        $psPrinter                             = $this->resolvePsPrinter($id);
        $psPrinter->companyService->visibility = (Yii::$app->request->post('status') == 'active' ? CompanyService::VISIBILITY_EVERYWHERE : CompanyService::VISIBILITY_NOWHERE);
        $psPrinter->companyService->updated_at = DateHelper::now();
        AssertHelper::assertSave($psPrinter->companyService);
        AssertHelper::assertSave($psPrinter);
        PsPrinterService::updatePrintersCache();
        return $this->jsonSuccess([]);
    }

    /**
     * @return array|string
     * @throws BusinessException
     * @throws InvalidModelException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionChangePrinterStatusAll()
    {
        $user = $this->getCurrentUser();

        if (!$user) {
            throw new BusinessException(_t('site', 'No access to edit'));
        }

        $psPrinters = $user->company->psPrinters ?? [];

        foreach ($psPrinters as $psPrinter) {
            $psPrinter->companyService->visibility = Yii::$app->request->post('status');
            $psPrinter->companyService->updated_at = DateHelper::now();
            AssertHelper::assertSave($psPrinter->companyService);
            AssertHelper::assertSave($psPrinter);
        }

        PsPrinterService::updatePrintersCache();
        return $this->jsonReturn(['success' => true, 'message' => _t('site', 'Saved')]);
    }

    /**
     *
     * @param
     *            $ps_printer_id
     * @return string
     * @throws BusinessException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionTest($ps_printer_id)
    {
        $psPrinter                     = $this->resolvePsPrinter($ps_printer_id);
        $this->view->params['printer'] = $psPrinter;
        return $this->render('../certification/certification',
            [
                'psPrinter' => $psPrinter
            ]);
    }

    /**
     * Save test result
     *
     * @param
     *            $ps_printer_id
     * @return array
     * @throws InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionTestSave($company_service_id)
    {
        $companyService      = $this->resolvePsMachine($company_service_id);
        $lastPayedTsPurchase = $this->tsInternalPurchaseRepository->getLastPayedCertForService($companyService);
        $psPrinter           = $companyService->psPrinter;
        $test                = $psPrinter->test ?: PsPrinterTest::create($psPrinter, $lastPayedTsPurchase);
        $test->scenario      = PsPrinterTest::SCENARIO_START_TEST;
        $test->loadFromPost(Yii::$app->request->getBodyParams());
        AssertHelper::assertValidate($test);
        $test->validateTestedPhotos();
        if ($test->getFirstError('testedPhotos')) {
            throw new InvalidModelException($test, $test->getFirstError('testedPhotos'));
        }
        $test->startTest();
        $this->setFlashMsg(true, _t('site.ps.test', 'Photo is saved for moderation.'));
        return $this->jsonSuccess();
    }

    /**
     * Create test order
     *
     * @param $id
     *
     * @throws BusinessException
     * @throws \ErrorException
     * @throws \frontend\components\cart\exceptions\CartEmptyException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     * @transacted
     */
    public function actionSendTestOrder($id)
    {
        $psPrinter = $this->resolvePsPrinter($id);
        $this->testOrderFactory->create($psPrinter);
    }

    /**
     * @param $id
     * @return \yii\console\Response|\yii\web\Response
     * @throws BusinessException
     * @throws InvalidModelException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCertificateDownload($id)
    {
        $psPrinter      = $this->resolvePsPrinter($id);
        $companyService = $psPrinter->companyService;
        if (!$companyService->isCertificated()) {
            throw new InvalidModelException($psPrinter, 'Printer not certified');
        }
        $pdf = $this->psMachineService->generateCertificatePdf($companyService);
        return Yii::$app->response->sendFile($pdf, 'certification.pdf');
    }

    /**
     * Find psPrinter and check that current user is owner
     *
     * @param
     *            $id
     * @return PsPrinter
     * @throws BusinessException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    protected function resolvePsPrinter($id)
    {
        $user    = $this->getCurrentUser(true);
        $printer = PsPrinter::find()->where(['id' => $id])->withoutStaticCache()->tryOne();
        $this->psMachineService->checkEditAccess($printer->companyService, $user);
        return $printer;
    }

    protected function resolvePsMachine($id)
    {
        $user      = $this->getCurrentUser(true);
        $psMachine = CompanyService::tryFindByPk($id);
        $this->psMachineService->checkEditAccess($psMachine, $user);
        return $psMachine;
    }

    /**
     * Return JSON array represent psPrinter
     *
     * @param PsPrinter $printer
     * @return array
     */
    public static function jsonPsPrinter(PsPrinter $printer)
    {
        // TODO colorCurrency и colorMeasure - из-за непраильной логики, после рефакторинга их надо удалить
        return ArrayHelper::toArray(
            $printer,
            [
                PsPrinter::class                          => [
                    'id',
                    'printer_id',
                    'description',
                    'is_ready_thingiverse' => function (PsPrinter $printer) {
                        return !empty($printer->is_ready_thingiverse) ? 1 : 0;
                    },
                    'title',
                    'materials',
                    'redifinedProperties',
                    'min_order_price'      => function (PsPrinter $printer) {
                        return (float)$printer->min_order_price;
                    },
                    'currency_iso'         => function (PsPrinter $psPrinter) {
                        return $psPrinter->companyService->company->currency;
                    },
                    'measure',
                    'visibility'           => 'companyService.visibility',
                    'certification'        => 'companyService.certification',
                    'positional_accuracy'  => function (PsPrinter $printer) {

                        if ($printer->positional_accuracy === null && $printer->isNewRecord) {
                            return null;
                        }

                        if (!$printer->printer) {
                            return null;
                        }

                        return $printer->positional_accuracy === null && !$printer->isNewRecord
                            ? $printer->printer->getMaxPositionalAccuracy()
                            : (float)$printer->positional_accuracy;
                    },
                ],
                PsPrinterMaterial::class                  => [
                    'id',
                    'title',
                    'material_id',
                    'colors'
                ],
                PsPrinterColor::class                     => [
                    'id',
                    'title',
                    'color_id',
                    'price' => function (PsPrinterColor $color) {
                        return (float)$color->price;
                    }
                ],
                \common\models\PsPrinterToProperty::class => [
                    'property_id',
                    'value'
                ],
                PsPrinterTest::class                      => [
                    'type',
                    'status',
                    'comment'
                ]
            ]
        );
    }

    /**
     * Return JSON array represent psMachine
     *
     * @param CompanyService $psMachine
     * @return array
     */
    public static function jsonPsMachine(CompanyService $psMachine)
    {
        return ArrayHelper::toArray(
            $psMachine,
            [
                CompanyService::class    => [
                    'id',
                    'ps_printer_id',
                    'ps_cnc_machine_id',
                    'type',
                    'currency_iso'  => function (CompanyService $companyMachine) {
                        return $companyMachine->ps->currency ?? null;
                    },
                    'location'      => function (CompanyService $companyMachine) {
                        if (!$companyMachine->isNewRecord && $companyMachine->location) {
                            return $companyMachine->location;
                        }

                        $location = UserSessionFacade::getLocation();
                        $user     = UserFacade::getCurrentUser();
                        if ($user && $user->company && $user->company->location) {
                            $locationPs  = $user->company->location;
                            $locationNew = LocationFactory::createFromUserLocation($locationPs);
                            if ($locationNew) {
                                $location = $locationNew;
                            }
                        }

                        $r = [
                            'country_id' => GeoNames::getCountryByISO($location->country)->id,
                            'region'     => $location->region,
                            'city'       => $location->city,
                            'address'    => null,
                            'address2'   => null,
                            'lat'        => $location->lat,
                            'lon'        => $location->lon,
                            'zip_code'   => null
                        ];
                        return $r;
                    },
                    'deliveryTypes' => function (CompanyService $printer) {
                        return $printer->deliveryTypess;
                    },
                ],
                UserLocation::class      => [
                    'country_id',
                    'region' => function (UserLocation $location) {
                        return $location->getUserDataField('region');
                    },
                    'city'   => function (UserLocation $location) {
                        return $location->getUserDataField('city');
                    },
                    'address',
                    'address2',
                    'lat',
                    'lon',
                    'zip_code'
                ],
                PsMachineDelivery::class => [
                    'comment',
                    'delivery_type_id',
                    'carrier',
                    'carrier_price' => function (PsMachineDelivery $delivery) {
                        return $delivery->carrier_price !== null ? (float)$delivery->carrier_price : null;
                    },
                    'free_delivery' => function (PsMachineDelivery $delivery) {
                        return $delivery->free_delivery !== null ? (float)$delivery->free_delivery : null;
                    },
                    'packing_price' => function (PsMachineDelivery $delivery) {
                        return $delivery->packing_price !== null ? (float)$delivery->packing_price : null;
                    }
                ]
            ]
        );
    }

    /**
     * Return json printers
     *
     * @return array
     */
    public static function jsonPrinters()
    {
        $printers = Printer::find()->active()
            ->with('technology', 'properties', 'properties.property', 'materials')
            ->orderBy('firm asc,title asc')
            ->all();

        foreach ($printers as $k => $printer) {
            $printers[$k] = $printer;
        }
        return ArrayHelper::toArray(
            $printers,
            [
                Printer::class           => [
                    'id',
                    'firm',
                    'title',
                    'technology',
                    'properties',
                    'materialsList' => function (Printer $printer) {
                        return join(', ', ArrayHelper::getColumn($printer->materials, 'filament_title'));
                    },
                    'maxPositionalAccuracy'
                ],
                PrinterTechnology::class => [
                    'title'
                ],
                PrinterToProperty::class => [
                    'is_editable' => function (PrinterToProperty $property) {
                        return (bool)$property->is_editable;
                    },
                    'value',
                    'property_id',
                    'title'       => function (PrinterToProperty $property) {
                        return $property->property->title;
                    }
                ]
            ]
        );
    }

    /**
     * Return json printer
     *
     * @param Printer $printer
     * @return array
     */
    public static function jsonPrinter(Printer $printer)
    {
        return ArrayHelper::toArray(
            $printer,
            [
                Printer::class         => [
                    'id',
                    'title',
                    'materials' => function (Printer $printer) {
                        // remove empty materials
                        return array_filter(
                            $printer->materials,
                            function (PrinterMaterial $material) {
                                return (bool)$material->colors;
                            }
                        );
                    },
                    'maxPositionalAccuracy'
                ],
                PrinterMaterial::class => [
                    'id',
                    'title',
                    'colors'  => function (PrinterMaterial $material) {
                        $colors = $material->colors;
                        ArrayHelper::multisort($colors, 'title');
                        return $colors;
                    },
                    'filament_title',
                    'density' => function (PrinterMaterial $material) {
                        return (float)$material->density;
                    }
                ],
                PrinterColor::class    => [
                    'id',
                    'code' => 'render_color',
                    'title',
                    'rgb'
                ]
            ]
        );
    }
}