<?php

namespace frontend\modules\mybusiness\controllers;

use common\components\exceptions\BusinessException;
use common\models\Ps;
use common\models\User;
use common\models\UserAddress;
use common\models\UserPaypal;
use common\models\UserTaxInfo;
use frontend\models\site\DenyCountry;
use frontend\modules\mybusiness\components\TaxUrlHelper;
use lib\pdf\PdfForm;
use yii\helpers\Html;

/**
 * TaxesController
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class
TaxesController extends MybusinessController
{
    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        /** @var Ps $company */
        $company = Ps::find()
            ->forUser($this->user)
            ->one();

        if (!$company) {
            $this->setFlashMsg(false, _t('site.ps', 'Please save business information'));
            return $this->redirect('/mybusiness/company/create-ps');
        }
        if($company->isDeleted()) {
            $this->setFlashMsg(false, _t('site.ps', 'The manufacturer is currently inactive.'));
            return $this->redirect('/mybusiness/company/create-ps');
        }

        return $this->redirect(['step1']);
    }

    /**
     *
     * @param bool $edit
     * @return string
     */
    public function actionStep1($edit = false)
    {
        $user = $this->getCurrentUser(true);
        $taxInfo = UserTaxInfo::findOne([
            'user_id' => $user->id
        ]);
        $taxModel = ! empty($taxInfo) ? $taxInfo : new UserTaxInfo();
        if (! empty($taxInfo) && $taxInfo->status != UserTaxInfo::STATUS_NEW && empty($edit)) {
            return $this->actionFilled();
        }
        if ($edit && ! empty($taxInfo->id)) {
            // reset all data
            $taxModel = UserTaxInfo::updateRecord($taxModel->id, [
                'identification_type' => null,
                'identification' => null,
                'classification' => null,
                'name' => null,
                'business_name' => null,
                'mailing_address_id' => null,
                'status' => 'new',
                'place_country' => null,
                'signature' => null,
                'sign_date' => null,
                'sign_type' => 'form'
            ]);
            $taxModel->is_usa = null;
        }
        if (app('request')->isPost) {
            try {
                $taxModel->load(app('request')->post());
                if (empty($user->userTaxInfo)) {
                    $taxModel->user_id = $user->id;
                    $taxModel->created_at = dbexpr('NOW()');
                }
                if ($taxModel->validate()) {
                    $taxModel->status = UserTaxInfo::STATUS_NEW;
                    $taxModel->safeSave();
                    return $this->redirect(TaxUrlHelper::taxesStep2());
                } else {
                    $this->setFlashMsg(false, Html::errorSummary($taxModel));
                }
            } catch (\Exception $e) {
                \Yii::error($e);
                $this->setFlashMsg(false, $e->getMessage());
            }
        }

        return $this->render('taxes_step1', compact('taxModel'));
    }

    /**
     * User infromation and address information - Tax step
     *
     * @return string|\yii\web\Response
     * @throws \yii\base\ExitException
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionStep2()
    {
        /** @var User $user */
        $user = $this->getCurrentUser(true);
        $taxModel = UserTaxInfo::findOne([
            'user_id' => $user->id
        ]);
        if (empty($taxModel)) {
            return $this->redirect([
                'my/taxes/step1'
            ]);
        }
        if ($taxModel->status != UserTaxInfo::STATUS_NEW) {
            return $this->actionFilled();
        }

        /** @var UserAddress $addressModel */
        $addressModel = ! empty($taxModel->address_id) ? UserAddress::findOne([
            'id' => $taxModel->address_id
        ]) : new UserAddress();
        $tpl = empty($taxModel->is_usa) ? 'taxes_step2_notusa' : 'taxes_step2_usa';
        if (app('request')->isPost) {
            $dbtr = app('db')->beginTransaction();
            try {
                $taxModel->scenario = UserTaxInfo::SCENARIO_STEP2;
                
                $addressModel->load(app('request')->post());
                $taxModel->load(app('request')->post());
                try {
                    // if deny will be required in tax forms
                    DenyCountry::checkCountry($taxModel->place_country);
                    DenyCountry::checkCountry($addressModel->country->iso_code);
                } catch (\Exception $e) {
                    \Yii::error($e);
                    $this->setFlashMsg(false, $e->getMessage());
                    return $this->redirect(app('request')->referrer);
                    //return $this->redirect(TaxUrlHelper::taxesStep3());
                }

                if (empty($taxModel->is_usa)) {
                    $taxModel->scenario = UserTaxInfo::SCENARIO_STEP2_NOUSA;
                } else {
                    $taxModel->place_country = 'US';
                }
                if(!empty($taxModel->identification)){
                    $taxModel->setIdentification(str_replace("-", "", $taxModel->identification));
                }
                $taxModel->validateStep2();
                
                $addressModel->user_id = $user->id;
                $addressModel->created_at = dbexpr('NOW()');
                $addressModel->contact_name = $taxModel->name;
                if ($taxModel->classification !== 'Individual') {
                    $addressModel->contact_name = $taxModel->business_name;
                    $taxModel->name = $taxModel->business_name;
                }
                if (empty($addressModel->zip_code)) {
                    throw new BusinessException(_t('site.tax', 'Postal code cannot be blank.'));
                }
                $addressModel->type = 'tax';
                $taxModel->status = UserTaxInfo::STATUS_NEW;
                if ($addressModel->validate()) {
                    $addressModel->safeSave();
                    $taxModel->address_id = $addressModel->id;
                    if ($taxModel->validate()) {
                        $taxModel->safeSave();
                    } else {
                        $dbtr->rollBack();
                        // $this->setFlashMsg(false, \yii\helpers\Html::errorSummary($taxModel));
                        // return $this->redirect(['my/taxes/step2']);
                        throw new BusinessException(Html::errorSummary($taxModel));
                    }
                    $dbtr->commit();
                    
                    try {
                        // if deny will be required in tax forms
                        DenyCountry::checkCountry($taxModel->place_country);
                        DenyCountry::checkCountry($addressModel->country->iso_code);
                    } catch (\Exception $e) {
                        \Yii::error($e);
                        $this->setFlashMsg(false, $e->getMessage());
                        return $this->redirect(app('request')->referrer);
                    }
                    return $this->redirect(TaxUrlHelper::taxesStep3());
                } else {
                    $this->setFlashMsg(false, Html::errorSummary($addressModel));
                }
            } catch (\Exception $ex) {
                $dbtr->rollBack();
                \Yii::error($ex);
                $this->setFlashMsg(false, $ex->getMessage());
                return $this->redirect(app('request')->referrer);
            }
        }

        $taxModel->identification = "";
        $params = [
            'user' => $user
        ];
        return $this->render($tpl, compact('params', 'taxModel', 'addressModel'));
    }

    /**
     * preview PDF form
     *
     * @return string
     */
    public function actionStep3()
    {
        $user = $this->getCurrentUser(true);
        $taxModel = UserTaxInfo::findOne([
            'user_id' => $user->id
        ]);
        if ($taxModel->status != UserTaxInfo::STATUS_NEW) {
            return $this->actionFilled();
        }

        return $this->render('taxes_step3');
    }

    /**
     * finish tax information
     *
     * @return string
     */
    public function actionStep4()
    {
        $user = $this->getCurrentUser(true);
        /** @var \common\models\UserTaxInfo $taxModel */
        $taxModel = UserTaxInfo::findOne([
            'user_id' => $user->id
        ]);
        if ($taxModel->status != UserTaxInfo::STATUS_NEW) {
            return $this->actionFilled();
        }

        if (app('request')->isPost) {
            $taxModel->scenario = UserTaxInfo::SCENARIO_FINISH;
            $taxModel->load(app('request')->post());
            if ($taxModel->validate()) {
                if ($taxModel->sign_type == 'form') {
                    $taxModel->scenario = UserTaxInfo::SCENARIO_STEP4;
                    $taxModel->sign_date = date("Y-m-d"); // UserTaxInfo::formatTaxSignDate($taxModel->sign_date);
                    $taxModel->validate();
                }
            }
            if ($taxModel->hasErrors()) {
                $this->setFlashMsg(false, Html::errorSummary($taxModel));
            } else {
                $this->setFlashMsg(true, _t("site.taxes", "Tax information submitted. Thank you."));
                $taxModel->status = UserTaxInfo::STATUS_SUBMITTED;
                
                // check paypal info if exists
                /** @var UserPaypal $paypalUser */
                $paypalUser = UserPaypal::findOne([
                    'user_id' => $user->id
                ]);
                if ($paypalUser) {
                    UserPaypal::checkPaypalStatus($paypalUser);
                }
                
                $taxModel->safeSave();
                // check for previous country
                $taxModel->notifyIfCountryChange();

                return $this->redirect(['/mybusiness/settings']);
            }
        }
        return $this->render('taxes_step4', compact('taxModel'));
    }

    /**
     *
     * @return string
     */
    public function actionFilled()
    {
        $user = $this->getCurrentUser(true);
        try {
            \frontend\models\user\UserFacade::getTaxMessage($user);
        } catch (\Exception $e) {
            $this->setFlashMsg(false, $e->getMessage());
        }
        $taxModel = UserTaxInfo::findOne([
            'user_id' => $user->id
        ]);
        
        return $this->render('taxes_filled', compact('taxModel'));
    }

    /**
     * preview HTML from PDF form file using current user tax info data
     *
     * @return string
     */
    public function actionPreview()
    {
        $user = $this->getCurrentUser(true);
        $taxModel = UserTaxInfo::findOne([
            'user_id' => $user->id
        ]);
        $form = $taxModel->getFormType();
        $contentTpl = file_get_contents(\Yii::getAlias('@frontend/modules/mybusiness/views/taxes/' . $form . '.html'));
        $userData = $taxModel->getPdfFormData($form);
        $userData = array_map('strip_tags', $userData);
        $userData['{IN_DATE}'] = date("m.d.Y");
        $content = str_replace(array_keys($userData), array_values($userData), $contentTpl);
        return $content;
    }

    /**
     *
     * @return string
     */
    public function actionGetPdf()
    {
        $user = $this->getCurrentUser(true);
        $taxModel = UserTaxInfo::findOne([
            'user_id' => $user->id
        ]);
        $form = $taxModel->getFormType();
        $pdfForm = new PdfForm($taxModel->getPdfFormFile(), $user->id);
        $userData = $taxModel->getPdfFormData($form, false);
        $filledPdf = $pdfForm->fillForm($userData);
        app('response')->setDownloadHeaders(pathinfo($filledPdf, PATHINFO_BASENAME), 'application/pdf', true);
        return app('response')->sendFile($filledPdf);
    }
}
