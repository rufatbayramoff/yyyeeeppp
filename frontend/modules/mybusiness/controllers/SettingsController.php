<?php

namespace frontend\modules\mybusiness\controllers;


use common\models\User;
use common\modules\payment\services\PaymentAccountService;
use frontend\models\ps\AddPsForm;
use frontend\modules\mybusiness\models\CompanySettingForm;
use frontend\modules\mybusiness\services\CompanyService;

class SettingsController extends MybusinessController
{
    public $layout = 'aboutLayout';

    /** @var PaymentAccountService */
    protected $paymentAccountService;
    /**
     * @var CompanyService
     */
    protected $companyService;

    public function injectDependencies(
        PaymentAccountService $paymentAccountService,
        CompanyService $companyService
    )
    {
        $this->paymentAccountService = $paymentAccountService;
        $this->companyService        = $companyService;
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        if ($this->user->status === User::STATUS_DRAFT_COMPANY && !($this->user->company->is_backend ?? false)) {
            $this->setFlashMsg(false, _t('mybusiness.common', 'Please confirm email first.'), false);
            return $this->redirect('/');
        }
        $company = AddPsForm::find()
            ->forUser($this->user)
            ->one();

        if (!$company) {
            $this->setFlashMsg(false, _t('site.ps', 'Please save business information'));
            return $this->redirect('/mybusiness/company/create-ps');
        }

        return $this->render('settings', [
            'company' => $company,
        ]);
    }

    public function actionSave()
    {
        $form = new CompanySettingForm();
        $form->load(\Yii::$app->request->post(), '');
        if ($form->validate()) {
            $this->companyService->editSettingForUser($form, $this->user);
            return $this->asJson(['success' => true, 'message' => _t('site.ps', 'Settings update')]);
        }
        return $this->asJson(['success' => false, 'errors' => $form->getErrorSummary(true)]);
    }
}
