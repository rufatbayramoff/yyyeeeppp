<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\controllers;

use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\ModelException;
use common\models\Company;
use common\models\CompanyService;
use common\models\CompanyServiceCategory;
use common\models\Model3d;
use common\models\populators\PsCncMachinePopulator;
use common\models\PrinterReview;
use common\models\Ps;
use common\models\ps\ChangePsUrlForm;
use common\models\PsPrinter;
use common\models\repositories\PsMachineRepository;
use common\models\validators\PsMachineValidator;
use common\modules\cnc\factories\PsCncFactory;
use common\modules\cnc\factories\PsCncMachineFactory;
use common\modules\cnc\repositories\PsCncMachineRepository;
use common\modules\cnc\validators\PsCncMachineValidator;
use common\modules\company\services\CompanyServicesService;
use common\services\PsMachineService;
use common\services\PsPrinterService;
use common\traits\TransactedControllerTrait;
use frontend\models\ps\AddPsForm;
use frontend\models\ps\PsFacade;
use frontend\models\ps\PsPrinterUpdater;
use frontend\models\store\StoreFacade;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\components\PrinterReviewService;
use Psr\Log\InvalidArgumentException;
use Yii;
use yii\base\UserException;
use yii\helpers\Html;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Session;

class ServicesController extends MybusinessController
{

    use TransactedControllerTrait;

    /** @var  PsCncFactory */
    protected $cncServiceFactory;

    /** @var  PsCncMachineFactory */
    protected $psCncMachineFactory;

    /** @var  PsCncMachineRepository */
    protected $psCncMachineRepository;

    /** @var  PsMachineRepository */
    protected $psMachineRepository;

    /** @var  PsCncMachinePopulator */
    protected $psCncMachinePopulator;

    /** @var  PsCncMachineValidator */
    protected $psCncMachineValidator;

    /** @var  PsMachineValidator */
    protected $psMachineValidator;

    /** @var  PsMachineService */
    protected $psMachineService;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var CompanyServicesService
     */
    private $companyServicesService;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(),[
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'update-delivery-printer'  => ['post']
                ],
            ]
        ]);
    }

    public function injectDependencies(
        PsCncMachineFactory $psCncMachineFactory,
        PsCncMachineRepository $psCncMachineRepository,
        PsMachineRepository $psMachineRepository,
        PsCncMachinePopulator $psCncMachinePopulator,
        PsCncMachineValidator $psCncMachineValidator,
        PsMachineValidator $psMachineValidator,
        PsMachineService $psMachineService,
        CompanyServicesService $companyServicesService
    )
    {
        $this->psCncMachineFactory    = $psCncMachineFactory;
        $this->psCncMachineRepository = $psCncMachineRepository;
        $this->psMachineRepository    = $psMachineRepository;
        $this->psCncMachinePopulator  = $psCncMachinePopulator;
        $this->psCncMachineValidator  = $psCncMachineValidator;
        $this->psMachineValidator     = $psMachineValidator;
        $this->psMachineService       = $psMachineService;
        $this->companyServicesService = $companyServicesService;
        $this->session                = Yii::$app->session;

        $this->view->params['section'] = 'mybusiness/services';
    }




    /**
     * Show Printers list if user has printers
     * Show Welcome page if user has no printers
     *
     * @param $serviceId
     * @return string
     * @throws \yii\base\InvalidParamException
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function actionIndex($serviceId = null)
    {
        $ps = PsFacade::getPsByUserId($this->user->id);

        if (!$ps) {
            $this->setFlashMsg(false, _t('site.ps', 'Please save business information'));
            return $this->redirect('/mybusiness/company/create-ps');
        }

        if($ps->isDeleted()) {
            $this->setFlashMsg(false, _t('site.ps', 'The manufacturer is currently inactive.'));
            return $this->redirect('/mybusiness/company/create-ps');
        }

        $psPrinters      = PsFacade::getNotDeletedPsPrintersByUserId(UserFacade::getCurrentUserId());
        $psCncMachines   = $this->psCncMachineRepository->getNotDeletedByPs($ps);
        $companyServices = $this->companyServicesService->getCompanyServicesForOwner($ps, [CompanyService::TYPE_SERVICE, CompanyService::TYPE_WINDOW]);
        $cuttingServices = $this->companyServicesService->getCompanyServicesForOwner($ps, [CompanyService::TYPE_CUTTING]);
        if ($serviceId) {
            $psPrinters      = $this->companyServicesService->filterByCompanyServiceId($psPrinters, $serviceId);
            $psCncMachines   = $this->companyServicesService->filterByCompanyServiceId($psCncMachines, $serviceId);
            $companyServices = $this->companyServicesService->getCompanyServiceForOwner($ps, $serviceId);
        }
        $isTestOrderOfferShowed = $this->isTestOrderOfferShowed($psPrinters, $ps);
        $serviceTypes           = $this->companyServicesService->getServicesTypes(true);
        $serviceTypeMachine     = $this->companyServicesService->getServicesTypeMachine();
        $companyServicesCategories = CompanyServiceCategory::find()->isActive()->witOutCutting()->witOutPrinting()->orderBy('position')->all();
        return $this->render(
            'company_services_list',
            [
                'ps'              => $ps,
                'psPrinters'      => $psPrinters,
                'psCncMachines'   => $psCncMachines,
                'companyServices' => $companyServices,
                'cuttingServices' => $cuttingServices,

                'serviceId'                 => $serviceId,
                'serviceTypes'              => $serviceTypes,
                'isTestOrderOfferShowed'    => $isTestOrderOfferShowed,
                'companyServicesCategories' => $companyServicesCategories,
                'serviceTypeMachine'        => $serviceTypeMachine
            ]
        );
    }

    /**
     * Add printer method
     *
     * @param bool $fromCreatePs
     *            If this action run from create ps context
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAddPrinter($fromCreatePs = false)
    {
        $ps                            = $this->resolvePs();
        $previousPrinter               = PsPrinter::find()->lastestCompanyPrinter($ps)->one();
        $printer                       = PsPrinter::factoryNew($ps);
        $this->view->params['printer'] = $printer;
        $this->view->params['previousPrinter'] = $previousPrinter;
        return $this->render(
            'add-printer',
            [
                'fromCreatePs' => $fromCreatePs
            ]
        );
    }


    /**
     * Add cnc - only shows form, saving goes to actionSaveCnc
     *
     * @param bool $fromCreatePs
     *            If this action run from create ps context
     * @param null $redirect
     * @param bool $onlyNew
     * @return string
     * @see self::actionSaveCnc()
     *
     */
    public function actionAddCnc($fromCreatePs = false, $redirect = null, $onlyNew = false)
    {

        $this->view->title = _t('site.ps', 'Add CNC Service');
        $ps                = $this->resolvePs();
        $psMachineDeleted  = $ps->getCncByAvailability(CompanyService::AVAILABILITY_STATUS_DELETED);
        if ($psMachineDeleted && !$onlyNew) {
            return $this->redirect(['/mybusiness/services/restore-cnc', 'psMachineId' => $psMachineDeleted->id]);
        }

        $psCncMachine       = $this->psCncMachineFactory->createCncMachine($ps, []);
        $schemaVars = [
            '__CURRENCY__' => $psCncMachine->companyService->company->paymentCurrency->title_original
        ];
        $cncSchema          = Yii::$app->getModule('cnc')->schemaManager->getInputFormSchema($schemaVars);
        $cncSchemaMaterials = Yii::$app->getModule('cnc')->schemaManager->getInputFormSchemaShortMaterials($schemaVars);

        $redirect
            ? $this->session->set('cnc_create_redirect', $redirect)
            : $this->session->remove('cnc_create_redirect');

        $activePs = $ps->getCncByAvailability(CompanyService::AVAILABILITY_STATUS_ACTIVE);
        if ($activePs) {
            return $this->redirect(['/mybusiness/services/edit-cnc', 'psMachineId' => $activePs->id]);
        }
        return $this->render(
            'add-cnc',
            [
                'tab'                => null,
                'type'               => 'add',
                'psCncMachine'       => $psCncMachine,
                'cncSchema'          => $cncSchema,
                'cncSchemaMaterials' => $cncSchemaMaterials,
                'fromCreatePs'       => $fromCreatePs
            ]
        );
    }

    /**
     *
     * @param $psMachineId
     * @param bool $flag
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionRestoreCnc($psMachineId, $flag = false)
    {
        $this->view->title = _t('site.ps', 'Restore CNC Service');
        $psMachine         = CompanyService::tryFindByPk($psMachineId);
        if (!UserFacade::isObjectOwner($psMachine->ps)) {
            throw new ForbiddenHttpException();
        }
        if ($flag) {
            $psMachine->is_deleted = 0;
            $psMachine->safeSave();
            return $this->redirect('/mybusiness/services/edit-cnc?psMachineId=' . $psMachineId);
        }
        return $this->render('cnc-restore.php', ['psMachineId' => $psMachineId]);
    }


    /**
     * Create printer
     *
     * @transacted
     */
    public function actionCreatePrinter()
    {
        $post      = Yii::$app->request->getBodyParams();
        $ps        = $this->resolvePs();
        $psPrinter = PsPrinter::factoryNew($ps);

        PsPrinterUpdater::createForCreate($psPrinter->companyService)
            ->updateAll($post)
            ->unpdatePrinterModerationStatus()
            ->invalidatePrintersCahce();

        return $this->jsonReturn(EditPrinterController::jsonPsPrinter($psPrinter));
    }

    public function actionUpdateDeliveryPrinter()
    {
        try {
            $this->psMachineService->updateDelivery(
                Yii::$app->request->post('psMachine'),
                $this->resolvePs()
            );
        } catch (ModelException $exception) {
            return $this->jsonReturn(['success' => false, 'message' => $exception->getMessage()]);
        }
        return $this->jsonReturn(['success' => true]);
    }

    /**
     *
     * @transacted
     */
    public function actionSaveCnc()
    {
        $post = Yii::$app->request->getBodyParams();
        $ps   = $this->resolvePs();
        if (isset($post['psMachine']['ps_cnc_machine_id'])) {
            $psCncMachine = $this->psCncMachineRepository->getById($post['psMachine']['ps_cnc_machine_id']);
            $this->psCncMachinePopulator->populate($psCncMachine, $post);
            $this->psMachineService->updateModeratorStatus($psCncMachine->companyService, $post);
        } else {
            // check if another CNC exists
            $hasPs = $ps->getCncByAvailability(CompanyService::AVAILABILITY_STATUS_ACTIVE);
            if ($hasPs) {
                return $this->jsonError(
                    _t(
                        'site.ps',
                        'You already have CNC service. Click {here} to edit it.',
                        ['here' => Html::a(_t('site.ps', 'here'), ['/mybusiness/services/edit-cnc', 'psMachineId' => $hasPs->id])]
                    )
                );
            }
            $psCncMachine = $this->psCncMachineFactory->createCncMachine($ps, $post);
        }

        $this->psCncMachineValidator->tryValidate($psCncMachine);
        $this->psMachineValidator->tryValidate($psCncMachine->companyService);

        $this->psCncMachineRepository->save($psCncMachine);
        $this->psMachineRepository->save($psCncMachine->companyService);

        if ($redirectUrl = $this->session->get('cnc_create_redirect')) {
            $this->session->remove('cnc_create_redirect');
            return $this->jsonReturn(['redirect' => $redirectUrl]);
        }

        $resultJson = Yii::$app->getModule('cnc')->psCncJsonGenerator->generateJsonServiceInfo($psCncMachine->psCnc);
        return $this->jsonReturn($resultJson);
    }

    /**
     * @param $psMachineId
     * @return array
     *
     * @transacted
     */
    public function actionChangeVisibility($psMachineId)
    {
        $psMachine  = $this->resolvePsMachine($psMachineId);
        $visibility = Yii::$app->request->post('visibility');
        if (!in_array($visibility, CompanyService::VISIBILITY_TYPES)) {
            throw new InvalidArgumentException('Invalid visibility');
        }
        $psMachine->visibility = $visibility;
        $psMachine->safeSave();
        return $this->jsonReturn(['psMachineId' => $psMachine->id, 'success' => 'true']);
    }

    /**
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionSelectServiceType()
    {
        try {
            $ps = $this->resolvePs();
        } catch (NotFoundHttpException $e) {
            $ps = null;
        }
        if ($ps && (!$ps->isCncAllowed() || $ps->haveUndeletedCncMachine())) {
            return $this->redirect('/mybusiness/company/add-printer');
        }

        return $this->render('select-service-type', ['ps' => $ps]);
    }

    /**
     * Fund ps for current user
     *
     * @param string $class
     *            Class for resolving PS instance
     * @return Company
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    private function resolvePs($class = Company::class)
    {
        AssertHelper::assert($class == Company::class || is_subclass_of($class, Ps::class), "Class {$class} is not PS");

        if (!UserFacade::getCurrentUserId()) {
            throw new ForbiddenHttpException(_t('site.ps', 'Please, login.'));
        }

        /** @var Company $ps */
        /** @var Company $class */
        $ps = $class::find()->forUser($this->getCurrentUser(true))
            ->one();

        if (!$ps) {
            throw new NotFoundHttpException(_t('site.ps', 'Print Service not found'));
        }

        if (!UserFacade::isObjectOwner($ps)) {
            throw new ForbiddenHttpException();
        }

        return $ps;
    }

    /**
     * Show printers list for current PS
     *
     * @param
     *            $ps_id
     * @return string
     */
    public function actionShowPs($ps_id)
    {
        $params = [
            'user' => $this->getCurrentUser(true)
        ];

        $printers = PsFacade::getNotDeletedPsPrintersByPsId($ps_id);

        return $this->render(
            'ps_printers_list',
            [
                'params'   => $params,
                'printers' => $printers,
                'ps_id'    => $ps_id
            ]
        );
    }

    /**
     * @return \yii\web\Response
     * @throws UserException
     */
    public function actionCertification()
    {
        $ps = $this->resolvePs();
        if ($untestedPrinter = $ps->getFirstUntestedPrinter()) {
            return $this->redirect(['/mybusiness/edit-printer/test', 'ps_id' => $ps->id, 'ps_printer_id' => $untestedPrinter->id]);
        }
        \Yii::$app->getSession()->setFlash('success', _t('site.ps', 'Congratulations! All your printers have been successfully certified.'));
        return $this->redirect(['/mybusiness/company/index']);
    }

    /**
     * Mark that PS read cnc hints on CNC Machine block
     * in My Service page
     */
    public function actionReadCncHints()
    {
        $ps                      = $this->resolvePs();
        $ps->is_cnc_hints_readed = 1;
        AssertHelper::assertSave($ps);
    }

    /**
     * @return string
     */
    public function actionModelStore(): string
    {
        $this->resolvePs(); // just check access

        $user  = UserFacade::getCurrentUser();
        $query = Model3d::find()->myStore($user->id);
        $query->orderBy('updated_at DESC');
        $items = $query->all();
        $items = StoreFacade::convertToCollections($items);

        return $this->render('/ps/index_templates/model-store', ['items' => $items]);
    }


    protected function resolvePsMachine($psMachineId)
    {
        $psMachine = CompanyService::tryFindByPk($psMachineId);

        if (!UserFacade::isObjectOwner($psMachine->ps)) {
            throw new ForbiddenHttpException();
        }

        if (!$psMachine->ps_cnc_machine_id) {
            throw new NotFoundHttpException();
        }

        if ($psMachine->is_deleted) {
            throw new NotFoundHttpException();
        }

        return $psMachine;
    }

    protected function resolvePsCncMachine($psMachineId)
    {
        $psMachine = $this->resolvePsMachine($psMachineId);
        return $this->psCncMachineRepository->getByPsMachine($psMachine);
    }

    /**
     * Edit Cnc machine
     *
     * @return array|string
     * @throws \yii\base\InvalidParamException
     */
    public function actionEditCnc($psMachineId, $tab = '')
    {

        $this->view->title  = _t('site.ps', 'Edit CNC Service');
        $psCncMachine       = $this->resolvePsCncMachine($psMachineId);
        $schemaVars = [
            '__CURRENCY__' => $psCncMachine->companyService->company->paymentCurrency->title_original
        ];
        $cncSchemaMaterials = Yii::$app->getModule('cnc')->schemaManager->getInputFormSchemaShortMaterials($schemaVars);
        $cncSchema          = Yii::$app->getModule('cnc')->schemaManager->getInputFormSchema($schemaVars);

        return $this->render(
            'add-cnc',
            [
                'type'               => 'edit',
                'tab'                => $tab,
                'ps'                 => $psCncMachine->companyService->company,
                'psCncMachine'       => $psCncMachine,
                'cncSchema'          => $cncSchema,
                'cncSchemaMaterials' => $cncSchemaMaterials,
                'fromCreatePs'       => false
            ]
        );
    }

    /**
     * @param $psMachineId
     * @return \yii\web\Response
     *
     * @transacted
     */
    public function actionDeleteCnc($psMachineId)
    {
        $psCncMachine                             = $this->resolvePsCncMachine($psMachineId);
        $psCncMachine->companyService->is_deleted = 1;
        $psCncMachine->companyService->safeSave();
        return $this->redirect('/mybusiness/services');
    }


    /**
     * Update PS
     *
     * @todo delete
     * @transacted
     */
    public function actionUpdatePs()
    {
        /** @var AddPsForm $ps */
        $ps = $this->resolvePs(AddPsForm::class);
        $ps->loadFromPost(Yii::$app->request->getBodyParams());
        AssertHelper::assertValidate($ps);
        $ps->savePs();
        $this->setFlashMsg(true, _t('front.site', 'Saved'));
    }

    /**
     * Update PS url
     *
     * @todo delete
     * @transacted
     */
    public function actionUpdatePsUrl()
    {
        $ps   = $this->resolvePs();
        $form = new ChangePsUrlForm($ps);
        $form->load(Yii::$app->request->getBodyParams(), '');
        AssertHelper::assertValidate($form);
        $form->process();
    }


    /**
     *
     * @return string
     */
    public function action3dPrintersOnMap()
    {
        $params         = [
            'user' => $this->getCurrentUser(true)
        ];
        $params['step'] = 3;
        $dataLocations  = PsFacade::getPrintersLocations($params['user']->id);

        return $this->render(
            '3d_printers_on_map',
            [
                'params'        => $params,
                'dataLocations' => $dataLocations
            ]
        );
    }

    public function actionFacebookRedirect()
    {
        if ($ps = PsFacade::getPsByUserId(UserFacade::getCurrentUserId())) {
            $tabsAdded = app('request')->get('tabs_added', false);
            if ($tabsAdded) {
                $pageIds = array_keys($tabsAdded); // add last one, for future can be one->many relation
                foreach ($pageIds as $pageId) {
                    PsPrinterService::addFacebookPage($ps, $pageId);
                }
                $this->setFlashMsg(true, _t('site.ps', 'Facebook App is now connected with your print service'));
                $this->layout = '@frontend/views/layouts/plain';
                return $this->renderContent('<script>if(window.opener){ window.opener.location.reload();setTimeout(function(){self.close()}, 3000);} </script>');
            }
        }
        $this->layout = '@frontend/views/layouts/plain';
        return $this->render(
            'edit-printer/embed_empty',
            ['ps' => $ps]
        );
    }

    /**
     * @param $pageId
     * @return \yii\web\Response
     *
     * @transacted
     */
    public function actionFbUnbind(
        $pageId
    )
    {
        $ps = PsFacade::getPsByUserId(UserFacade::getCurrentUserId());
        if (!$ps) {
            $this->setFlashMsg(false, _t('site.ps', 'Currently, this user doesn’t have a print service.'));
            return $this->redirect('/mybusiness/company/embed');
        }
        PsPrinterService::removeFacebookPage($ps, $pageId);
        $this->setFlashMsg(
            true,
            _t('site.ps', 'Facebook app has been disconnected from your print service. Please remove the tab from your Facebook public page. Thank you.')
        );
        return $this->redirect('/mybusiness/company/embed');
    }

    /**
     * @param $psPrinters
     * @param $ps
     * @return bool
     * @throws \common\components\exceptions\InvalidModelException
     */
    private function isTestOrderOfferShowed($psPrinters, $ps): bool
    {
        if ($psPrinters) {
            $isTestOrderOfferShowed = $psPrinters || (bool)$ps->is_test_order_offer_showed;
            if (!$ps->is_test_order_offer_showed) {
                $ps->is_test_order_offer_showed = 1;
                AssertHelper::assertSave($ps);
            }
        } else {
            $isTestOrderOfferShowed = true;
        }
        return $isTestOrderOfferShowed;
    }

    public function actionReview($id)
    {
        $psPrinter = PsPrinter::findByPk($id);
        if (!$psPrinter->isOwnToCurrentUser()) {
            return $this->redirect(app('request')->referrer);
        }
        // check if exists
        $printerReview = PrinterReviewService::getReview($psPrinter);
        if (!$printerReview) {
            $printerReview = new PrinterReview();
        }

        return $this->render('reviewPrinter', ['printer' => $psPrinter->printer, 'psPrinter' => $psPrinter, 'printerReview' => $printerReview]);
    }

    /**
     * @param $psPrinterId
     * @throws NotFoundHttpException
     * @throws UserException
     */
    public function actionAddReview($psPrinterId)
    {
        $psPrinter = PsPrinter::tryFindByPk($psPrinterId, 'Printer not found');
        if (!$psPrinter->isOwnToCurrentUser()) {
            throw new UserException(_t('printer.review', 'No access'));
        }
        $printerReview = PrinterReviewService::getReview($psPrinter);
        if (!$printerReview) {
            $printerReview             = new PrinterReview();
            $printerReview->created_at = dbexpr('NOW()');
            $printerReview->user_id    = $this->user->id;
            $printerReview->printer_id = $psPrinter->printer_id;
        }
        $data = \Yii::$app->request->getBodyParams();
        $printerReview->load($data, '');
        $printerReview->status = PrinterReview::STATUS_NEW;
        if (!$printerReview->validate()) {
            throw new BusinessException(Html::errorSummary($printerReview));
        }
        $printerReview->safeSave();
        $reviewOk = _t('printer.review', 'Thank you for submitting a review for your 3D printer!');
        if ($this->isAjax) {
            return $this->jsonMessage($reviewOk);
        }
        $this->setFlashMsg($reviewOk);
        return $this->redirect('/mybusiness/services/review?id=' . $psPrinterId);
    }
}