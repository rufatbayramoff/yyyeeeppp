<?php

namespace frontend\modules\mybusiness\controllers;

use backend\models\search\AffiliateSourceSearch;
use common\components\ArrayHelper;
use common\components\exceptions\OnlyPostRequestException;
use common\models\AffiliateSource;
use common\models\user\UserIdentityProvider;
use common\modules\affiliate\actions\ActionIndex;
use common\modules\affiliate\factories\AffiliateSourceFactory;
use common\modules\affiliate\helpers\AffiliateUrlHelper;
use common\modules\affiliate\services\AffiliateService;
use common\modules\affiliate\services\AffiliateSessionService;
use common\modules\affiliate\actions\ActionCreate;
use common\modules\affiliate\actions\ActionDelete;
use common\modules\affiliate\actions\ActionUpdate;
use common\modules\affiliate\actions\ActionView;
use frontend\models\ps\PsFacade;
use frontend\modules\mybusiness\actionFilters\AffiliateAccessFilter;
use frontend\modules\mybusiness\models\AffiliateCompanyForm;
use frontend\modules\mybusiness\services\CompanyService;
use Yii;
use yii\base\InvalidArgumentException;

class AffiliatesController extends MybusinessController
{
    public $layout = 'widgetsLayout';

    public $ps;

    /** @var AffiliateSourceFactory $affiliateSourceFactory */
    public $affiliateSourceFactory;

    /** @var UserIdentityProvider */
    public $userIdentityProvider;

    /** @var AffiliateSessionService */
    public $affiliateSessionService;

    /** @var AffiliateService */
    public $affiliateService;

    /**
     * @var CompanyService
     */
    protected $companyService;

    public function injectDependencies(
        AffiliateSourceFactory $affiliateSourceFactory,
        UserIdentityProvider $userIdentityProvider,
        AffiliateSessionService $affiliateSessionService,
        AffiliateService $affiliateService,
        CompanyService $companyService
    )
    {
        $this->affiliateSourceFactory  = $affiliateSourceFactory;
        $this->userIdentityProvider    = $userIdentityProvider;
        $this->affiliateSessionService = $affiliateSessionService;
        $this->affiliateService        = $affiliateService;
        $this->companyService          = $companyService;
    }

    public function init()
    {
        parent::init();
        $this->view->params['section'] = 'mybusiness/widgets';
        if (!$this->user) {
            return;
        }
        $this->ps = PsFacade::getPsByUserId($this->user->id);
    }

    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'affiliateAccess' => [
                'class'  => AffiliateAccessFilter::class,
                'except' => ['registration']
            ]
        ]);
    }

    public function actions()
    {
        return array_merge(
            parent::actions(), [
                'view'   => ActionView::class,
                'create' => ActionCreate::class,
                'update' => ActionUpdate::class,
                'delete' => ActionDelete::class,
                'index'  => ActionIndex::class,
            ]
        );
    }

    public function actionRegistration()
    {
        $this->layout = 'mainWithTop';
        $user         = $this->userIdentityProvider->getUser();
        if ($user->company) {
            return $this->redirect(AffiliateUrlHelper::sourcesList($user->company));
        }
        $form = new AffiliateCompanyForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->companyService->newAffiliateCompany($form, $user);
            $this->setFlashMsg(true, _t('site.common', 'Created'));
            return $this->redirect(AffiliateUrlHelper::sourcesList($user->company));
        }
        $user->startCompanyCreate();
        $user->safeSave();
        return $this->render('registration', ['model' => $form]);
    }
}
