<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\controllers;


use backend\models\search\CompanyCertificationSearch;
use backend\models\search\ProductSearch;
use common\components\DateHelper;
use common\models\CompanyCertification;
use common\models\factories\FileFactory;
use common\models\repositories\FileRepository;
use frontend\modules\mybusiness\components\UserCompanyProvider;
use frontend\modules\mybusiness\models\CompanyCertificationBindForm;
use frontend\modules\mybusiness\models\CompanyCertificationForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class CertificationsController extends MybusinessController
{
    /**
     * @var UserCompanyProvider
     */
    private $userCompanyProvider;

    /**
     * @var FileFactory
     */
    private $fileFactory;
    public $layout = 'aboutLayout';
    public function injectDependencies(UserCompanyProvider $userCompanyProvider, FileFactory $fileFactory)
    {
        $this->userCompanyProvider = $userCompanyProvider;
        $this->fileFactory = $fileFactory;
        $this->view->params['section'] = '/mybusiness/certifications';
    }

    public function actionIndex()
    {
        $userCompany = $this->userCompanyProvider->getUserCompany();
        if (!$userCompany) {
            return $this->redirect('/mybusiness/company');
        }
        $searchParams = Yii::$app->request->queryParams;
        $searchParams['company_id'] = $userCompany->id;

        $dataProvider = $this->search($searchParams);

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
            ]
        );
    }
    /**
     * Creates a new CompanyCertification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyCertificationForm();
        if ($model->load(Yii::$app->request->post())) {
            $uploadFile = UploadedFile::getInstance($model, 'file');
            $model->file = $uploadFile;
            if ($uploadFile) {
                $file = $this->fileFactory->createFileFromUploadedFile($uploadFile);
                $file->setPublicMode(1);
                $fileRepo = new FileRepository();
                $fileRepo->save($file);
                $model->file_id = $file->id;
            }else{
                $this->setFlashMsg(false, _t('mybusiness.certification', 'Image/PDF file is required.'));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            $model->company_id = $this->userCompanyProvider->getUserCompany()->id;
            $model->created_at = DateHelper::now();
            if(!$model->validate()){
                $this->setFlashMsg(false, Html::errorSummary($model));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            $model->safeSave();
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CompanyCertification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = CompanyCertification::findByPk($id)) !== null) {
            if ($this->userCompanyProvider->getUserCompany()->id !== $model->company_id) {
                throw new ForbiddenHttpException();
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function search($params)
    {
        $item = new CompanyCertification();
        $query = CompanyCertification::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );

        $item->load($params, '');

        // grid filtering conditions
        $query->andFilterWhere(
            [
                'id'          => $item->id,
                'company_id'  => $item->company_id,
            ]
        );


        return $dataProvider;
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAssignProducts()
    {
        $company = $this->user->company;
        if (!$company) {
            return $this->redirect('/mybusiness/company');
        }
        $productSearchModel = new ProductSearch();
        $certsSearchModel = new CompanyCertificationSearch();

        $model = new CompanyCertificationBindForm();
        $params = Yii::$app->request->get();
        $params[$certsSearchModel->formName()]['company_id'] = $company->id;
        $params[$certsSearchModel->formName()]['is_visible'] = true;

        $params[$productSearchModel->formName()]['company_id'] = $company->id;
        $params[$productSearchModel->formName()]['is_active'] = true;

        $certsDp = $certsSearchModel->search($params);
        $productsDp = $productSearchModel->search($params);
        return $this->render(
            'assign-product',
            [
                'model'              => $model,
                'certs'              => $certsDp,
                'products'           => $productsDp,
                'productSearchModel' => $productSearchModel
            ]
        );
    }

    public function actionBindBlocks()
    {
        $bindForm = new CompanyCertificationBindForm();
        $bindForm->load(\Yii::$app->request->post());

        if ($bindForm->validate()) {
            $bindForm->bindCertifications();
            $this->setFlashMsg(true, 'Done');
        } else {
            $this->setFlashMsg(false, Html::errorSummary($bindForm));
        }
        return $this->redirect('/mybusiness/certifications/assign-products');
    }
}