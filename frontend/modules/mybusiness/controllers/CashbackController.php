<?php
namespace frontend\modules\mybusiness\controllers;

use common\models\Company;
use common\models\CompanyService;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\modules\CsWindow\services\CsWindowService;

/**
 * Class CashbackController
 * @package frontend\modules\mybusiness\controllers
 *
 */
class CashbackController extends MybusinessController
{
    public $layout = 'widgetsLayout';

    /** @var CompanyService */
    protected $companyService;

    /** @var Company */
    public $ps;

    public function injectDependencies(
        CompanyService $companyService
    ) {
        $this->companyService = $companyService;
    }

    public function init()
    {
        parent::init();
        $this->view->params['section'] = 'mybusiness/widgets';
        if (!$this->user) {
            return;
        }
        $this->ps = PsFacade::getPsByUserId($this->user->id);
    }

    public function actionSettingsSave()
    {
        $percent = \Yii::$app->request->post('percent');
        $this->companyService->setCashbackPercent($this->ps, $percent);
        return $this->jsonSuccess([]);
    }

    public function actionSettings()
    {
        return $this->render('settings', ['company' => $this->ps]);
    }
}