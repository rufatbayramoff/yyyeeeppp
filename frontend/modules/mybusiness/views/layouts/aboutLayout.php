<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 04.04.2018
 * Time: 18:14
 */

use common\models\Company;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\services\progress\Progress;
use frontend\modules\mybusiness\widgets\CompanyFillPercentWidget;

$this->beginContent('@frontend/modules/mybusiness/views/layouts/mybusinessLayout.php');
$user = UserFacade::getCurrentUser();


/** @var Company $company */
$company = Company::find()->forUser($user)->one();
?>

<div class="container">

    <div class="row">
        <div class="col-md-9">
            <div class="designer-card p-l0 p-r0 p-t0 p-b0">

                <?= \frontend\modules\mybusiness\widgets\CompanyAboutTabs::widget(['company'=>$company, 'section' =>$this->params['section']??null]); ?>

                <div style="padding: 20px 15px 0;">
                    <?= $content; ?>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <?php echo CompanyFillPercentWidget::widget(['company' => $company])?>
            <a class="m-t10" href="/about/supplier-tips" target="_blank" style="display: inline-block;">
                <span class="tsi tsi-question-c m-r10"></span><?= _t('site.ps', 'Tips for business')?>
            </a>
        </div>
    </div>

</div>

<?php $this->endContent(); ?>
