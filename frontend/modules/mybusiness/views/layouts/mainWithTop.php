<?php
/**
 * User: nabi
 */

$this->beginContent('@frontend/views/layouts/main.php');

?>
<?= $this->render('@frontend/views/common/ie-alert.php'); ?>
<div class="container">
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>