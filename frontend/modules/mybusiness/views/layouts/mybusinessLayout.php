<?php
/**
 * User: nabi
 */

$this->beginContent('@frontend/views/layouts/main.php');

?>
<?= $this->render('@frontend/views/common/ie-alert.php'); ?>
    <div class="over-nav-tabs-header"><div class="container container--wide"><h1><?=_t('site.ps', 'My Business');?></h1></div></div>
    <?=\frontend\modules\mybusiness\widgets\MyBusinessTabs::widget(['section' =>$this->params['section']??null]); ?>

    <?= $content; ?>
<?php $this->endContent(); ?>