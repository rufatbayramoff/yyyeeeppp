<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 04.04.2018
 * Time: 18:14
 */

$this->beginContent('@frontend/modules/mybusiness/views/layouts/mybusinessLayout.php');

$ps = $this->context->ps;
    Yii::$app->angular
        ->service(['notify', 'user', 'router', 'facebookApi', 'modal'])
        ->controller('ps/printers/embed')
        ->constant('facebookApplicationId', Yii::$app->params['facebook_ps_print_application_id'])
        ->controllerParams([
            'ps' => $ps,
            'facebookApplicationId' => Yii::$app->params['facebook_ps_print_application_id'],
            'server' => param('server')
        ])
        ->resource('PsPrinter');
?>

<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="side-nav-mobile">
                <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navMobile">
                    <?= _t('front.user', 'Business Tools Menu')?> <span class="tsi tsi-down"></span>
                </div>
                <?= \frontend\modules\mybusiness\widgets\BusinessToolsTabs::widget([]); ?>
            </div>
        </div>

        <div class="col-sm-9 wide-padding wide-padding--left">
            <?= $content; ?>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>




