<?php

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\mybusiness\models\CompanyServiceAddForm */

/* @var $serviceCategories array|\common\models\CompanyService[]|\common\models\CompanyServiceCategory[]|mixed|\yii\db\ActiveRecord[] */

use common\models\CompanyServiceCategory;
use frontend\modules\mybusiness\serializers\CompanyServiceSerializer;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->registerAssetBundle(\frontend\assets\DropzoneAsset::class);


Yii::$app->angular
    ->service(['notify', 'user', 'modal', 'router'])
    ->directive(['dropzone-image-crop', 'dropzone-button'])
    ->controller('company/service-create')
    ->controllerParam('companyService', CompanyServiceSerializer::serialize($model));


function printOption(\frontend\modules\mybusiness\models\CompanyServiceAddForm $form, $data)
{
    $model =  new CompanyServiceCategory();
    $model->id = $form->category_id;
    $result = $model->getCategoryTree($data);
    foreach ($result as $k => $category) {
        echo sprintf("<option value='%d' %s>%s</option>", $category['id'], $category['selected'], $category['space'] . $category['title']);
    }
}

?>
<div class="container" ng-controller="CompanyServiceCreateController" ng-cloak>

    <?php if($model->isNewRecord): ?>
    <h2 class="m-t0 m-b20"><?=_t('mybusiness.services', 'Provide a service');?></h2>
    <?php else: ?>
    <h2 class="m-t0 m-b20"><?=_t('mybusiness.services', 'Edit service');?></h2>
    <?php endif; ?>

    <div class="company-service-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="designer-card">
            <div class="row">
                <div class="col-sm-8 wide-padding wide-padding--right">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for=""><?= _t('site.ps', 'Title'); ?>  <span class="form-required">*</span></label>
                                <input type="text"  class="form-control" name="CompanyServiceAddForm[title]" maxlength="255" ng-model="companyService.title">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for=""><?= _t('site.ps', 'Type of service'); ?></label>
                                <select class="form-control" name="CompanyServiceAddForm[category_id]" ng-model="companyService.category_id">
                                    <?= printOption($model, $serviceCategories); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <label for=""><?= _t('site.ps', 'Description'); ?>  <span class="form-required">*</span></label>
                    <?= $form->field($model, 'description')->label(false)->widget(
                        \dosamigos\ckeditor\CKEditor::className(),\frontend\components\CKEditorDefault::getDefaults(
                        [
                            'options'       => ['rows' => 4, 'ng-model'=>'companyService.description']
                        ])
                    ) ?>
                </div>

                <div class="col-sm-4">

                    <h4 class="m-t0"><?=_t('mybusiness.services', 'Portfolio images');?></h4>

                    <div class="service-portfolio" ng-if="companyService.pictures.length > 0">

                        <p class="service-portfolio__hint"><?=_t('company.service', 'Click to set primary image');?></p>

                        <div class="service-portfolio__list">
                            <div class="service-portfolio__item" ng-repeat="image in companyService.pictures">
                                <div class="service-portfolio__pic {{image.cssClass}}">
                                    <img ng-src="{{image.src}}" alt="" ng-click="setPrimaryPicture(image, $index)">
                                </div>
                                <button type="button" ng-click="removeFile(image)" class="btn btn-info btn-sm p-l10 p-r10" title="<?=_t('company.service', 'Delete');?>">
                                    <span class="tsi tsi-bin"></span>
                                </button>
                            </div>
                        </div>

                    </div>

                    <button
                        dropzone-button="companyService.pictures"
                        can-change-after-select="true"
                        accepted-files=".jpg,.png,.gif,.jpeg"
                        max-files="20"
                        class="btn btn-primary btn-sm m-b10"
                        title="<?= _t('site.ps', 'Upload'); ?>"
                        type="button">
                            <span class="tsi tsi-plus m-r10"></span>
                            <?= _t('site.ps', 'Upload'); ?>
                    </button>

                </div>
            </div>

            <hr class="m-t10">

            <div class="form-group m-b0">
                <button
                        loader-click="save()"
                        type="button" class="btn btn-primary">
                    <?= _t('site.ps', 'Save') ?>
                </button>
            </div>

        </div>

        <?php ActiveForm::end(); ?>

        <?php if(!$model->isNewRecord): ?>
        <h3><?= _t('mybusiness.product', 'Assigned from company blocks'); ?></h3>
        <ul class="list-unstyled">
            <?php
            $companyBlocks = $model->getBindedCompanyBlocks();
            foreach($companyBlocks as $companyBlock):
                ?>
                <li class="p-b10"><?=Html::a($companyBlock->block->title, ['/mybusiness/company-blocks/view', 'id'=>$companyBlock->block_id], ['target'=>'_blank','class'=>'m-r10']); ?>
                    <?=Html::a(\frontend\components\Icon::get('delete'), ['/mybusiness/company-services/unbind-block', 'id'=>$companyBlock->id], ['class'=>'btn btn-xs btn-circle btn-info']); ?></li>
            <?php endforeach; ?>
        </ul>

        <div class="panel p-b20">
            <?php $form = ActiveForm::begin(['layout' => 'inline', 'action' => '/mybusiness/company-services/bind-block']);
            $blockBind = new \common\models\CompanyBlockBind();
            $blockBind->company_service_id = $model->id;
            $blockList = \common\components\ArrayHelper::map($company->getCompanyBlocks()->asArray()->all(), 'id', 'title');
            $productBlocks = $model->getBindedCompanyBlockIds();
            foreach($blockList as $blockId=>$blockItem){ if(in_array($blockId, $productBlocks)) unset($blockList[$blockId]); }
            if(!empty($blockList)){
                ?>
                <span class="m-r10">
                    <?=_t('mybusines.product', 'Company block'); ?>
                </span>
                <?= $form->field($blockBind, 'company_service_id')->hiddenInput()->label(false); ?>

                <?= $form->field($blockBind, 'block_id')->dropDownList($blockList, ['style'=>'max-width:300px;margin-right:15px;'])->label(false); ?>
                <?= Html::submitButton(Yii::t('mybusiness.product', 'Assign'), ['class' => 'btn btn-primary js-clickProtect']) ?>
            <?php } ?>
            <?php ActiveForm::end(); ?>
        </div>
        <?php endif; ?>
    </div>
</div>