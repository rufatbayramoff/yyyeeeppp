<?php
/** @var \common\models\CompanyServiceType[] $serviceTypes */
/** @var \common\models\CompanyService[] $services */
/* @var $this \yii\web\View */

?>
<div class="container">

    <div class="dropdown dropdown-primary">
        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?= _t('site.ps', 'Provide a service'); ?>
            <span class="tsi tsi-down"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <?php foreach ($serviceTypes as $serviceType): ?>
                <li><a href="#"><?= $serviceType->title; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>

    <table class="table table-striped m-t20">
        <tbody>
        <tr>
            <th>Service Type</th>
            <th width="20%">Title</th>
            <th width="20%">Moderator status</th>
        </tr>
        <?php foreach ($services as $companyService): ?>
            <tr>
                <td><?=$companyService->type; ?></td>
                <td><?=$companyService->getTitleLabel(); ?></td>
                <td><?=$companyService->moderator_status; ?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>

