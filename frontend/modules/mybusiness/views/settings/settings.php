<?php
/**
 * @var $company \frontend\models\ps\AddPsForm
 */

use common\components\DateHelper;
use common\models\UserSms;
use common\models\UserTaxInfo;
use frontend\assets\FlagsAsset;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\components\PayoutViewHelper;
use frontend\modules\mybusiness\serializers\CompanySettingSerializer;

/** @var \yii\web\View $this */

$this->title = _t('mybusiness.settings', 'Company Settings');

$this->registerAssetBundle(FlagsAsset::class);
$companySerialized = CompanySettingSerializer::serialize($company);

$currencies = [['id'=> '4', 'title'=>'US Dollar', 'currency_iso'=>'USD']];
//if ($company->currency === \lib\money\Currency::EUR) {
    $currencies[] = ['id'=> '6', 'title'=>'Euro', 'currency_iso'=>'EUR'];
//}

Yii::$app->angular
    ->service(['notify', 'router', 'user', 'geo'])
    ->controller('ps/ps/settings')
    ->controllerParam('currencies', $currencies)
    ->controllerParam('ps', $companySerialized)
    ->controllerParam('company', $companySerialized);

$sendAgainOver = 0;
$lastTryTime   = UserSms::find()->where(['user_id' => $company->user->id])->orderBy('created_at desc')->one();
if ($lastTryTime) {
    $sendAgainOver = 60 - (time() - DateHelper::strtotimeUtc($lastTryTime->created_at));
}

Yii::$app->angular->controllerParams([
    'sendAgainOver' => max(0, $sendAgainOver),
]);


$taxModel            = UserTaxInfo::findOne(['user_id' => $company->user->id]) ?? new UserTaxInfo();
$taxModel->sign_date = $taxModel->getTaxSignDate();

$payoutHelper = new PayoutViewHelper(UserFacade::getCurrentUser());

echo $this->render('@frontend/modules/mybusiness/views/services/certification/certification-assets.php');
?>


<div ng-cloak ng-controller="PsSettingsController">
    <!-- SETTINGS -->
    <div ng-controller="CertificationController">
        <div ng-controller="SettingsPhoneController">

            <form class="form-horizontal" ng-cloak>

                <!--  PHONE NUMBER -->
                <div class="form-group m-b0">
                    <label for="country"
                           class="col-sm-3 col-lg-2 control-label"><?= _t('mybusiness.settings', 'Phone number') ?></label>

                    <div class="col-sm-9 col-lg-10">
                        <?= $this->render('settings-phone', ['company' => $company]); ?>
                        <div ng-if="getIsNew()"
                             class="help-block">
                            <?= _t('site.ps', 'Treatstock will never send you any spam, nor give away, sell, or disclose your phone number.'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-lg-7 col-sm-offset-3 col-lg-offset-2">
                        <p class="help-block">
                            <?= _t('mybusiness.settings', "If you have a mobile number for your company, get it verified to receive mobile notifications. If you don't have a mobile number, you may enter your business landline without verification."); ?>
                        </p>
                    </div>
                </div>

                <!-- CURRENCY -->


                <div class="form-group">
                    <label for="currency"
                           class="col-sm-3 col-md-3 col-lg-2 control-label"><?= _t('mybusiness.settings', 'Currency') ?></label>
                    <div class="col-sm-5 col-md-4">
                        <select
                                id="currency"
                                ng-model="company.currency"
                                ng-options="currency.currency_iso as currency.title for currency in currencies"
                                ng-change="changeCurrency(company.currency, '{{company.currency}}')"
                                class="form-control">
                        </select>
                    </div>
                </div>

                <!-- Allow incoming offers -->
                <div class="form-group">
                    <label for="currency"
                           class="col-sm-3 col-md-3 col-lg-2 control-label"><?= _t('mybusiness.settings', 'Allow incoming offers') ?></label>
                    <div class="col-sm-5 col-md-4">
                        <select
                                id="allowIncomingQuotes"
                                ng-model="company.allowIncomingQuotes"
                                ng-options="item.id as item.title for item in allowIncomingQuotesSelect"
                                class="form-control">
                        </select>
                    </div>
                </div>

            </form>

        </div>

        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-3 col-lg-2 control-label">
                    <h4 class="m-t0 m-b0">
                        <?= _t('front.user', 'Documents') ?>
                    </h4>
                </div>
                <div class="col-sm-9 col-lg-10 p-t10">


                    <h4 class="m-t0"><?= _t('settings', 'Identification document'); ?></h4>
                    <div>
                        <?php if ($company->user->hasDocument()): ?>

                            <div class="ps-test__required ps-test__required--success m-b0">
                                <span class="tsi tsi-checkmark-c"></span><?= _t('site.ps', 'Document uploaded') ?>
                            </div>
                            <button type="button" class="btn btn-sm btn-default m-t10 m-b20"
                                    ng-click="showUploadDocumentModal()">
                                <?= _t('site.company.test', 'Change Document') ?>
                            </button>

                        <?php else : ?>

                            <div class="ps-test__required ps-test__required--fail">
                                <span class="tsi tsi-warning-c"></span>
                                <a ng-click="showUploadDocumentModal()" class="inspectlet-sensitive"
                                   style="cursor: pointer"><?= _t('site.ps', 'Upload a form of ID') ?></a>
                            </div>

                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-9 col-lg-10 col-sm-offset-3 col-lg-offset-2">

                    <p class="help-block">
                        <?= _t('mybusiness.settings', 'Please note that the information you provide in Settings will not be made public and are used to provide your products and services on the site. You can always view your Public Profile under the About tab to see what information is made public.'); ?>
                    </p>


                    <div class="m-t20 m-b20">
                        <button
                                loader-click="savePs()"
                                ng-disabled="loading"
                                type="button" class="btn btn-primary m-r20">
                            <?= _t('site.ps', 'Save') ?>
                        </button>

                        <span style="display: inline-block;margin-top: 10px;vertical-align: top;"><?= _t('site.ps', 'To confirm information click "Save" button.'); ?></span>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>