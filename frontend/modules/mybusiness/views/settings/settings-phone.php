<?php
use common\models\GeoCountry;
use frontend\components\Icon;
use kartik\widgets\Select2;
?>
<div ng-if="company.phone_status =='checked'">
    <div class="row">
        <div class="col-sm-8">
            <div class="input-group">
                <span class="input-group-addon">+{{company.phone_code}}</span>
                <input type="text" class="form-control" ng-model="company.phone" readonly
                       style="background: white">
            </div>

            <div class="ps-btn-verify__change-btn">
                <button
                    ng-click="changePhoneNumber()"
                    type="button"
                    class="btn btn-default">
                    <span class="tsi tsi-pencil"></span>
                    <?= _t('site.ps', 'Change phone number') ?></button>
            </div>
        </div>
        <div class="col-sm-4 ps-btn-verify__checked">
            <?= Icon::get('checkmark-c') . ' ' . _t('site.ps', 'Verified'); ?>
        </div>
    </div>
</div>

<!-- PHONE NUMBER NOT CHECKED -->
<div class="row" ng-show="company.phone_status !='checked'">
    <div class="col-sm-8">
        <div class="ps-phone-code">
            <div>
                <?php
                echo
                Select2::widget([
                    'model'         => $company,
                    'attribute'     => 'phone_country_iso',
                    'data'          => GeoCountry::getPhoneCodesIso(),
                    'options'       => [
                        'placeholder' => _t('front.model3d', 'Select'),
                        'ng-model'    => 'company.phone_country_iso',
                    ],
                    'pluginOptions' => [
                        'allowClear'        => false,
                        'dropdownAutoWidth' => true,
                        'templateResult'    => new yii\web\JsExpression('function(state) {return $("<span><span class=\"phone-flag-icon flag-icon-"+String(state.id).toLowerCase()+"\"></span>"+state.id+" "+state.text+"</span>");}'),
                        'templateSelection' => new yii\web\JsExpression('function(state) {return $("<span><span class=\"phone-flag-icon flag-icon-"+String(state.id).toLowerCase()+"\"></span>"+state.text.substr(state.text.indexOf("+"),100)+"</span>");}'),
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="ps-phone-field">
            <div>
                <input
                    ng-model="company.phone"
                    type="text" class="form-control"
                    placeholder="<?= _t('site.ps', 'Your phone number') ?>" autocomplete="off">
            </div>
        </div>
        <div class="ps-btn-verify__change-btn">
            <button class="btn btn-warning"
                    type="button"
                    ng-click="cancelOfChangingPhoneNumber()"
                    ng-if="haveVerifyedPhone">
                <span class="tsi tsi-remove-c"></span>
                <?= _t('site.ps', 'Cancel change of phone number') ?></button>
        </div>
    </div>


    <div class="col-sm-4 ps-btn-verify">

        <button
            type="button"
            ng-if="!company.phone_status || company.phone_status == 'new'"
            loader-click="sendVerifyPhoneCode()"
            class="btn btn-block btn-primary">
            <?= _t('site.ps', 'Verify via SMS') ?>
        </button>

        <div ng-if="company.phone_status == 'checking'">
            <div class="input-group">
                <input
                    ng-model="company.$$phoneVerifyCode"
                    type="text" class="form-control"/>
                <div class="input-group-btn">
                    <button
                        type="button"
                        loader-click="confirmVerifyPhoneCode()"
                        class="btn btn-primary btn-block">
                        <?= _t('site.ps', 'Verify'); ?>
                    </button>
                </div>
            </div>

            <div class="m-t10">

                <span ng-if="sendAgainOver > 0"><?= _t('site.ps', 'Send again in {{sendAgainOver}} seconds.'); ?></span>
                <button
                    ng-if="sendAgainOver == 0"
                    loader-click="sendVerifyPhoneCode()"
                    class="btn btn-default btn-block"
                    type="button">
                    <span class="tsi tsi-repeat"></span>
                    <?= _t('site.ps', 'Send again') ?>
                </button>

                <div ng-if="sendAgainOver == 0 && sendAgainTryCount >= 2">
                    <span><?php echo _t('site.ps', "I didn't get message."); ?></span>
                    <button
                        loader-click="reportSupport()"
                        class="btn btn-default btn-block"
                        type="button">
                        <?= _t('site.ps', 'Report a problem') ?>
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>
