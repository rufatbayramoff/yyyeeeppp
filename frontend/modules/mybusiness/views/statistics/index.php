
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <a class="btn btn-default m-b20" href="/mybusiness/widgets">
                <span class="tsi tsi-left" style="margin-right: 5px;"></span><?= _t('mybusiness', 'Back to Tools');?>
            </a>
            <div class="side-nav-mobile">
                <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navMobile">
                    <?= _t('front.user', 'Statistics Menu')?> <span class="tsi tsi-down"></span>
                </div>
                <?= \frontend\modules\mybusiness\widgets\StatisticsSidebarTabs::widget([]); ?>
            </div>
        </div>
        <div class="col-sm-9 wide-padding--left">
            <h2 class="m-t0"><?=_t('mybusiness.statistics', 'Statistics'); ?></h2>
            <p>
                <?=_t('mybusiness.statistics', 'Here you can find affiliate and API statistics information.'); ?>
            </p>
        </div>
    </div>
</div>