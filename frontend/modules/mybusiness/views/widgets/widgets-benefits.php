<div class="ps-share__info-target"></div>
<div class="ps-share__info collapse" id="ps-share__info">
    <h2 class="text-center m-t30 m-b30">
        <?= _t('site.ps', 'Benefits of using My Service Widget'); ?>
    </h2>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/feefree.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'No Transaction Fees'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'When an order is placed through the widget, you as a service pay a 0% transaction fee'); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/customer.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Customer Tools'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'Customers can upload and view their files as a 3D render in different materials, colors and sizes to gain a better understanding of what they want to order'); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/calc.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Price Calculation'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t(
                'site.ps', 'Customers will have access to fast and accurate information about the cost of producing their 3D models with your manufacturing service'
            ); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/instant.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Instant Orders'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'Orders and payment are made directly through the widget without the need for back-and-forth emails, phone calls or confirmations'); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/time.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Save Time'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'Orders are processed automatically allowing you to save time'); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/money.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Save Money'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'The automated process of the widget allows your business to run more efficiently'); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/tech.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Range of Technologies'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'The widget is compatible with a wide range of technologies and machines'); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/receipt.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Order Receipts'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'All orders come with receipts in the form of e-statements which can be downloaded by your customers'); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/security.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Financial Security'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'Customers pay for orders using our secure online payment system, giving you the financial security to carry on with running your business'); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/promo.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Promotional Tool'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'Your current and potential customers will get the latest information about your manufacturing service and its achievements'); ?>
        </p>
    </div>
    <div class="ps-share__info-block">
        <img src="/static/images/widget-page-icons/formats.svg" alt="">
        <h4 class="ps-share__info-title">
            <?= _t('site.ps', 'Multiple Formats'); ?>
        </h4>
        <p class="ps-share__info-text">
            <?= _t('site.ps', 'Various formats of 3D models are supported, including the most popular - STL and PLY'); ?>
        </p>
    </div>
</div>