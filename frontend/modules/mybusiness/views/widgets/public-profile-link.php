<?php
/** @var \common\models\Ps $ps */

use frontend\models\ps\PsFacade;

$publicUrl = PsFacade::getPsLink($ps, [], true);
?>

<div>
    <div class="ps-share__icon ps-share__icon--link">
        <span class="tsi tsi-link"></span>
    </div>
    <h2 class="designer-card__title">
        <?= _t('site.ps', 'My Public Profile Link'); ?>
    </h2>
    <p class="designer-card__about">
        <?= _t(
            'site.ps',
            'Display the public page of your print service on any platform that you like and showcase your printers and their features, as well as your portfolio of successful prints for your clients.'
        ); ?>
    </p>
    <p class="designer-card__about m-b0">
        <a href="<?= $publicUrl ?>" target="_blank">
            <?= H($publicUrl) ?>
        </a>

        <?php if ($ps->canChangeUrl()): ?>
            <button
                ng-click="openChangeUrlPopver()"
                class="btn btn-circle btn-info btn-xs"
                title="<?= _t('site.ps', 'Change PS url'); ?>">

                <span class="tsi tsi-pencil designer-card__edit_url"></span>
            </button>
        <?php endif; ?>
    </p>
</div>
