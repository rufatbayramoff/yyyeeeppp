<?php
/** @var \common\models\Ps $ps */

$ourHost = param('server', 'https://www.treatstock.com');

if(!$ps){
    echo 'Please create Service first.';
    return;
}
?>

<div class="designer-card">
    <div class="ps-share__icon ps-share__icon--fb">
        <span class="soc-icons soc-icons-facebook"></span>
    </div>
    <h2 class="designer-card__title">
        <?= _t('site.ps', 'Facebook App'); ?>
    </h2>
    <p class="designer-card__about">
        <?= _t('site.ps', 'Embed your print service from Treatstock with your printers, materials, and colors on your Facebook public page to receive orders instantly through the most popular social media platform in the world!'); ?>
        <a href="/help/article/89-how-to-embed-the-facebook-app" target="_blank">
            <?= _t('site.ps', 'Need help?'); ?>
        </a>
        <br/>
        <b><?= _t('site.ps', 'Available for 3D printing only.'); ?></b>
    </p>
    <div>
        <?php if ($ps->psFacebookPages) : ?>

            <?php foreach ($ps->psFacebookPages as $facebookPage) : ?>
                <div>
                    <a href="https://www.facebook.com/<?= $facebookPage->facebook_page_id; ?>/app/<?= param('facebook_ps_print_application_id');?>/?ref=page_internal"
                       target="_blank">
                            <span class="soc-icons soc-icons-facebook"></span> <?= 'facebook.com/' . $facebookPage->facebook_page_id; ?>
                    </a>
                    &nbsp;
                    <a href="/mybusiness/services/fb-unbind/?pageId=<?= $facebookPage->facebook_page_id ?>"
                       onclick="self=this;TS.confirm(_t('site.ps', 'Are you sure you want to disable the Facebook app on your public page?'), function(btn){ if(btn==='ok'){ location.href=self.href}});return false;"
                       class="btn btn-default btn-xs"><?= _t('site.ps', 'Disconnect'); ?></a>
                </div>
            <?php endforeach ?>

        <?php endif; ?>

    </div>
    <button class="btn btn-primary"
            onclick="window.open('https://www.facebook.com/v2.1/dialog/pagetab?app_id=<?= param('facebook_ps_print_application_id');
            ?>&display=popup&next=<?= $ourHost; ?>/mybusiness/services/facebook-redirect', 'fb', 'width=600,height=200,top=150,left='+(screen.width/2 - 700/2))">
        <?= _t('site.ps', 'Add Facebook App'); ?>
    </button>

    <p class="designer-card__about m-t10 m-b0">
        <?= _t('site.ps', '* The app only works with public pages'); ?>
    </p>
</div>
