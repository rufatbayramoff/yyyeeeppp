<?php
/**
 * @var \common\models\Ps $ps
 * @var \common\models\User $user
 * @var \common\models\CsWindow $csWindow
 */

use common\models\PsPrinter;
use common\services\PsPrinterService;
use yii\helpers\Html;

$this->title = _t('site.ps','Business Tools - My services widget');

if (!$ps) {

    ?>

    <div class="m-t30">
        <h3><?= _t('site.ps', 'No services found'); ?></h3>
        <p>
            <?= _t('site.ps', 'Please add company information and services in order to access this page.'); ?>
        </p>
        <a class="btn btn-primary" href="/mybusiness/company/create-ps"><?= _t('site.ps', 'Add company information'); ?></a>
    </div>

    <?php
    return;
}
$cncMachineActive = $ps->getCncByAvailability(\common\models\CompanyService::AVAILABILITY_STATUS_PUBLIC);
/** @var PsPrinter[] $activePrinters */
$activePrinters = $ps->getPsPrinters()->visible()->all();

$selectedWindowService = count($activePrinters) <= 0 || $windowService ? true : false;
$previewPossible       = false;
?>


<div class="designer-card" ng-controller="PsPrinterEmbedController">
    <div class="ps-share__icon ps-share__icon--embed m-r10">
        &lt;/&gt;
    </div>

    <h2 class="designer-card__title">
        <?= _t('front.user', 'My Services Widget'); ?>
    </h2>

    <p class="designer-card__about">
        <?= _t(
            'site.ps',
            'Do you have your own website? Simply copy and paste the embed code into your site to monetize your traffic while providing your customers with an innovative way to place an order for your manufacturing services.'
        ); ?>
        <a href="/help/article/88-how-to-embed-the-treatstock-widget" target="_blank">
            <?= _t('site.ps', 'Need help?'); ?>
        </a>
    </p>

    <?php if (count($activePrinters) > 0) { ?>
        <div class="radio radio-primary">
            <input type="radio" id="radiofirst" name="radioPSEmbed" checked="true" ng-click="showEmbedScript(null)">
            <label for="radiofirst"
                   class="ps-share__label_entire_print_service"><?= _t('ps.embeded', 'Machines') ?></label>
        </div>
        <?php foreach ($activePrinters as $printer) { ?>
            <div class="radio radio-primary">
                <input type="radio" id="radio<?= $printer->id; ?>" name="radioPSEmbed"
                       ng-click="showEmbedScript(<?= $printer->id ?>)">
                <label for="radio<?= $printer->id; ?>"><?= H($printer->title) ?></label>
            </div>
            <?php
        }
        $previewPossible = 1;
    }
    ?>

    <?php if ($cncMachineActive) { ?>
        <div class="radio radio-primary">
            <input type="radio"
                   id="cnc<?= $cncMachineActive->id; ?>" <?= count($activePrinters) > 0 ? '' : 'checked="true"'; ?>
                   name="radioPSEmbed" ng-click="showEmbedScript('cnc<?= $cncMachineActive->id ?>')">
            <label for="cnc<?= $cncMachineActive->id; ?>"><?= H('CNC Machine') ?></label>
        </div>
        <?php
        $previewPossible = 1;
    }
    ?>

    <?php if ($csWindow) { ?>
        <div class="radio radio-primary">
            <input type="radio" id="csWindow<?= $csWindow->id; ?>" name="radioPSEmbed"
                   ng-click="showEmbedScript('csWindow<?= $csWindow->id; ?>', 'true')"<?php echo $selectedWindowService ? ' checked' : ''; ?>>
            <label for="csWindow<?= $csWindow->id; ?>"><?= H($csWindow->companyService->getTitleLabel()) ?></label>
        </div>
        <?php
        $previewPossible = 1;
    } ?>

    <label class="checkbox-switch checkbox-switch--xs m-b20<?php echo $selectedWindowService ? ' hidden' : ''; ?>">
        <?php echo Html::checkbox(false, true, [
            'class'    => 'js-reviews-block',
            'ng-click' => 'showEmbedScriptCheckbox()'
        ]); ?>
        <span class="slider"></span>
        <span class="text"><?php echo _t('ps.embeded', 'Show Reviews'); ?></span>
    </label>

    <?php if (count($activePrinters) > 0) { ?>
        <textarea
                class="form-control ps-share__textarea js-embed js-embed-ps<?php echo $selectedWindowService ? ' hidden' : ''; ?>"
                name="publicLink" id="publicLink" rows="4">
            <?php echo PsPrinterService::getPsWidgetHtmlCode($ps); ?>
        </textarea>

        <textarea class="form-control ps-share__textarea js-embed js-embed-ps-dr hidden" name="publicLink"
                  id="publicLink" rows="4">
            <?php echo PsPrinterService::getPsWidgetHtmlCode($ps, false); ?>
        </textarea>
        <?php
        $previewPossible = 1;
    } ?>

    <?php foreach ($activePrinters as $printer) { ?>
        <textarea class="form-control ps-share__textarea js-embed js-embed-printer-<?= $printer->id ?> hidden"
                  name="publicLink" rows="4">
            <?= PsPrinterService::getPsPrinterWidgetHtmlCode($printer); ?>
        </textarea>
        <textarea class="form-control ps-share__textarea js-embed js-embed-printer-dr-<?= $printer->id ?> hidden"
                  name="publicLink" rows="4">
            <?= PsPrinterService::getPsPrinterWidgetHtmlCode($printer, false); ?>
        </textarea>
        <?php
        $previewPossible = 1;
    } ?>

    <?php if ($cncMachineActive) { ?>
        <textarea id="publicLink"
                  class="form-control ps-share__textarea js-embed js-embed-printer-cnc<?= $cncMachineActive->id ?> <?= count($activePrinters) > 0 ? 'hidden' : ''; ?>"
                  name="publicLink" rows="4">
            <?= PsPrinterService::getPsCncPrinterWidgetHtmlCode($cncMachineActive); ?>
        </textarea>
        <?php
        $previewPossible = 1;
    } ?>

    <?php if ($csWindow) { ?>
        <textarea id="publicLink"
                  class="form-control ps-share__textarea js-embed js-embed-printer-csWindow<?= $csWindow->id ?> <?php echo !$selectedWindowService ? ' hidden' : ''; ?>"
                  name="publicLink" rows="4">
            <?php echo PsPrinterService::getCsWindowWidgetHtmlCode($csWindow); ?>
        </textarea>
        <?php
        $previewPossible = 1;
    } ?>
</div>

<?php if ($previewPossible) { ?>
    <div class="panel panel-default m-t30">
        <div class="panel-heading"><?= _t('site.model3d', 'Preview') ?></div>
        <div class="panel-body" id="embedframe"></div>
    </div>
<?php } else { ?>
    <div class="panel panel-default m-t30 panel-body">
        <?= _t('site.model3d', 'You have no active services. Please, create a service first.') ?>
    </div>
<?php } ?>







