<?php

/* @var $ps string */

$this->title = _t('site.ps', 'Business Tools - Api');
?>
<div class="designer-card">

    <h2 class="designer-card__title">API</h2>

    <?php if ($ps->user->apiExternalSystems): ?>
        <h4>API Details</h4>
        <?php foreach ($ps->user->apiExternalSystems as $apiExternalSystem): ?>

            <?= \yii\widgets\DetailView::widget([
                'model'      => $apiExternalSystem,
                'attributes' => [
                    'id',
                    'name',
                    'private_key',
                    'public_upload_key'
                ],
            ]) ?>

        <?php endforeach; ?>
    <?php else: ?>

        <p>To work with our API you will need an <b>API token.</b>
            There are two types of API tokens: Private-key and Public-key.
            The Private-key provides full access to the API and should not be disclosed to users because they will then have the ability to retrieve confidential data about 3D model uploads of other
            users.
            The Public-key is restricted to the upload of new models only.
            Our API supports the following 3D printing file formats: STL, PLY and 3MF. Need help?
            Contact <a href="/site/contact?subject=API technical support" target="blank">technical support</a>.
        </p>

        <a class="btn btn-primary" href="/site/contact?subject=API+tokens+request">Get API tokens</a>

    <?php endif; ?>

    <hr>

    <a href="/l/affiliates" class="btn btn-primary m-b10 m-r20" target="_blank">Affiliate Program</a>
    <a href="/help/article/113-become-an-api-partner" class="btn btn-primary btn-ghost m-b10" target="_blank">View API documentation</a>

</div>