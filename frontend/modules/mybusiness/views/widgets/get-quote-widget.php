<?php

use common\services\PsPrinterService;
use common\services\WidgetService;
use frontend\models\user\UserFacade;

/** @var \common\models\User $user */
$user = UserFacade::getCurrentUser();

/** @var WidgetService $widgetService */
$widgetService = Yii::createObject(WidgetService::class);

$widgetCode        = $widgetService->getGetQuoteWidgetCode($user->company);
$widgetCodeEncoded = $widgetService->getGetQuoteWidgetCodeEncoded($user->company);

$this->title = _t('site.ps','Business Tools - Get a Quote Widget');

?>


<div class="row">
    <div class="col-lg-12">
        <div class="designer-card">
            <div class="row">
                <div class="col-md-4">
                    <div class="ps-share__icon ps-share__icon--embed m-r10">
                        &lt;/&gt;
                    </div>
                    <h2 class="designer-card__title">
                        <?= _t('site.ps', 'Get a Quote Widget'); ?>
                    </h2>
                    <p class="designer-card__about text-justify">
                        <?= _t(
                            'site.ps',
                            'Do you have your own website? Simply copy and paste the embed code into your site to get quotes from your clients.'
                        ); ?>
                    </p>
                </div>
                <div class="col-md-8"><br>
                    <br>
                    <textarea id="publicLink" class="form-control ps-share__textarea js-embed" name="publicLink"
                              rows="5"><?= $widgetCodeEncoded; ?></textarea>

                </div>
            </div>

        </div>
    </div>
</div>

<?php if ($user->company->isActive()) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?= _t('site.model3d', 'Preview') ?></div>
                <div class="panel-body" id="embedframe">
                    <?= $widgetCode; ?>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="panel panel-default m-t30 panel-body">
        <?= _t('site.model3d', 'Company had not been checked by Treatstock yet. Preview will be available after the company gets approved.') ?>
    </div>
<?php } ?>
