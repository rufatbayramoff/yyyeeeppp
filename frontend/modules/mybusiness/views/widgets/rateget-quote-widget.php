<?php

use common\services\PsPrinterService;
use common\services\WidgetService;
use frontend\models\user\UserFacade;
use yii\helpers\Html;

/** @var \common\models\User $user */
$user = UserFacade::getCurrentUser();

/** @var WidgetService $widgetService */
$widgetService = Yii::createObject(WidgetService::class);

$widgetCode        = $widgetService->getRateWidgetCode($user->company);
$widgetCodeWide    = $widgetService->getRateWideWidgetCode($user->company);
$widgetCodeEncoded = $widgetService->getRateWidgetCodeEncoded($user->company);

Yii::$app->angular
    ->service(['notify', 'user', 'router'])
    ->controller('ps/widget/RateWidgetController')
    ->controllerParams([
        'widgetCode' => [
            '' => $widgetCode,
            'wide' => $widgetCodeWide
        ]
    ]);

$this->title = _t('site.ps','Business Tools - Treatstock Rating Widget');

?>


<div class="row" ng-controller="RateWidgetController">
    <div class="col-lg-12">
        <div class="designer-card">
            <div class="ps-share__icon ps-share__icon--embed m-r10">
                &lt;/&gt;
            </div>
            <h2 class="designer-card__title">
                <?= _t('site.ps', 'Treatstock Rating Widget'); ?>
            </h2>
            <div class="row">
                <div class="col-md-4">
                    <p class="designer-card__about text-justify">
                        <?= _t(
                            'site.ps',
                            'Do you have your own website? Copy and paste the embed code to add a Treatstock rating widget to your page. '
                        ); ?>
                    </p>
                    <label class="checkbox-switch checkbox-switch--xs m-b20">
                        <?php echo Html::checkbox(false, false, [
                            'class'    => 'js-reviews-block',
                            'ng-click' => 'wideCheckbox()'
                        ]); ?>
                        <span class="slider"></span>
                        <span class="text"><?php echo _t('ps.embeded', 'Wide mode'); ?></span>
                    </label>
                </div>
                <div class="col-md-8">
                    <textarea id="publicLink" class="form-control ps-share__textarea" rows="5"><?= $widgetCodeEncoded; ?></textarea>
                </div>
            </div>

        </div>
    </div>
</div>

<?php if ($user->company->isActive()) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?= _t('site.model3d', 'Preview') ?></div>
                <div class="panel-body" id="embedframe">
                    <?= $widgetCode; ?>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="panel panel-default m-t30 panel-body">
        <?= _t('site.model3d', 'Company had not been checked by Treatstock yet. Preview will be available after the company gets approved.') ?>
    </div>
<?php } ?>

