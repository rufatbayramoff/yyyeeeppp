<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 15.08.18
 * Time: 16:39
 */

use frontend\components\UserUtils;

$this->title = _t('site.ps','Business Tools - Products Store');
if ($user->getProducts()->published()->all()) {
?>
    <div class="row" ng-controller="PsPrinterEmbedController">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label model-embed__code-label" for="model-embed__code-input">Code</label>
                <input class="form-control model-embed__code-input" id="model-embed__code-input" type="text" value=""
                       onfocus="this.select(true)">
            </div>
        </div>
    </div>

    <div class="panel panel-default m-t30">
        <div class="panel-heading"><?= _t('site.store', 'Preview') ?></div>
        <div class="panel-body" id="embedframe">
            <iframe class="ts-embed-store"
                    width="100%"
                    height="420"
                    src="<?= $user->company->getProductsStoreUrl(true); ?>"
                    frameborder="0"></iframe>
        </div>
    </div>
    <?php
} else { ?>
    <div class="panel panel-default panel-body">
        <p>
            <?=_t('site.store', 'You have not added any products yet');?>
        </p>
        <a class="btn btn-primary btn-sm" href="/mybusiness/products?type=product">
            <?= _t('site.store', 'Add product'); ?>
        </a>
   </div>
<?php
} ?>