<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.11.17
 * Time: 14:37
 */

use common\services\PsPrinterService;
use common\services\WidgetService;
use frontend\models\user\UserFacade;

/** @var \common\models\User $user */

/** @var WidgetService $widgetService */
$widgetService = Yii::createObject(WidgetService::class);

$widgetCode        = $widgetService->getUserWidgetCode($user);
$widgetCodeEncoded = $widgetService->getUserWidgetCodeEncoded($user);

$config = new \lib\message\UserConfig($user);

Yii::$app->angular
    ->service(['notify', 'user', 'router'])
    ->controller('profile/notifications')
    ->controllerParams(
        [
            'userId'       => $user->id,
            'userConfig'   => $config->toArray(),
            'notification' => [
                'groups'  => \lib\message\Constants::getGroupLabels(),
                'times'   => \lib\message\Constants::getTimePolicyLabels(),
                'senders' => \lib\message\Constants::getActiveSendersLabels(UserFacade::getCurrentUser()),
            ]
        ]
    );
$this->title = _t('site.ps', 'Business Tools - Affiliate widget');
?>


<div class="row">
    <div class="col-lg-12">
        <div class="designer-card">
            <div class="row">
                <div class="col-md-4">
                    <div class="ps-share__icon ps-share__icon--embed m-r10">
                        &lt;/&gt;
                    </div>
                    <h2 class="designer-card__title">
                        <?= _t('site.ps', 'Treatstock Widget'); ?>
                    </h2>
                    <p class="designer-card__about text-justify">
                        <?= _t(
                            'site.ps',
                            'Do you have your own website? Simply copy and paste the embed code into your site to establish an automated point of sale for your business while providing your customers with a seamless checkout experience.'
                        ); ?>
                        <br>
                        <br>
                        <?= _t(
                            'site.ps',
                            'To join our affiliate program and qualify for rewards'
                        ); ?>
                        <a href="/site/contact" target="_blank">
                            <?= _t('site.ps', 'contact us'); ?>.
                        </a>
                    </p>
                </div>
                <div class="col-md-8"><br>
                    <br>
                    <textarea id="publicLink" class="form-control ps-share__textarea js-embed" name="publicLink"
                              rows="5"><?= $widgetCodeEncoded; ?></textarea>

                </div>
            </div>

        </div>
    </div>
</div>

<?php if ($user->isAffiliateAllowed()) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?= _t('site.model3d', 'Preview') ?></div>
                <div class="panel-body" id="embedframe">
                    <?= $widgetCode; ?>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="panel panel-default m-t30 panel-body">
        <?= _t('site.model3d', 'Company had not been checked by Treatstock yet. Preview will be available after the company gets approved.') ?>
    </div>
<?php } ?>


