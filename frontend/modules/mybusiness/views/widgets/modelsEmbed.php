<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.03.17
 * Time: 15:20
 */
use common\models\Model3d;
use frontend\widgets\Model3dEmbedWidget;
use yii\web\View;

/** @var View $this */
/** @var Model3d[] $model3ds */

/*
<select onchange=" $('.js-model3d-embed_block').hide(); console.log('#model3d_embed_block-' +this.value+'); $('#model3d_embed_block-' +this.value+').show();" >

$('.js-model3d-embed_block').hide(); $('#model3d_embed_block-' +this.value+').show(); */

?>
<?php
  if ($model3ds) {
      ?>
      <div class="designer-card">
          <div class="row">
              <div class="col-sm-6 col-md-4">
                  <div class="form-group">
                      <label class="control-label model-embed__code-label m-t0"><?= _t('site.store', 'Please select a model:') ?></label>

                      <select class="form-control"
                              onchange=" $('.js-model3d-embed_block').addClass('hidden'); $('#model3d_embed_block-' +this.value).removeClass('hidden');">
                          <?php
                          foreach ($model3ds as $model3d) {
                              ?>
                              <option value="<?= $model3d->id ?>"><?= $model3d->title ?></option>
                              <?php
                          }
                          ?>
                      </select>
                  </div>
              </div>
          </div>

          <hr class="m-t0 m-b10">

          <div class="row">
              <?php
              $isFirst = true;
              foreach ($model3ds as $model3d) {
                  echo '<div class="col-sm-12 js-model3d-embed_block ' . ($isFirst ? '' : ' hidden') . '" id="model3d_embed_block-' . $model3d->id . '">';
                  echo Model3dEmbedWidget::widget(
                      [
                          'model3d' => $model3d
                      ]
                  );
                  echo '</div>';
                  $isFirst = false;
              }
              ?>
          </div>
      </div>
      <?php
  } else {
?>
      <div class="panel panel-default panel-body">
          <p>
              <?= _t('site.store', 'You have not added any 3d models yet'); ?>
          </p>
          <a class="btn btn-primary btn-sm" href="/mybusiness/products?type=model3d">
              <?= _t('site.store', 'Add 3D model'); ?>
          </a>
      </div>
<?php } ?>
