<?php
/** @var \common\models\User $user */
/** @var Model3d[] $models3d */

use common\models\Model3d;
use frontend\components\UserUtils;

$this->title = _t('site.ps','Business Tools - 3D Models Store');

if ($models3d) {
    ?>
    <div class="row" ng-controller="PsPrinterEmbedController">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label model-embed__code-label" for="model-embed__code-input">Code</label>
                <input class="form-control model-embed__code-input" id="model-embed__code-input" type="text" value=""
                       onfocus="this.select(true)">
            </div>
        </div>

        <div class="col-sm-6">
            <div class="model-embed__settings">
                <div class="form-group">
                    <label class="control-label model-embed__code-label"
                           for="embed-count"><?= _t('site.store', 'Max count of models') ?></label>
                    <input
                            type="number"
                            class="form-control"
                            id="embed-count"
                            min="0"
                            max="120"
                            step="1"
                            placeholder="<?= _t('site.store', 'Max count of models') ?>"
                            onchange="updateEmbed()"/>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default m-t30">
        <div class="panel-heading"><?= _t('site.store', 'Preview') ?></div>
        <div class="panel-body" id="embedframe">
            <iframe class="ts-embed-store" width="100%" height="420"
                    src="<?= param('server'); ?><?= UserUtils::getUserStoreUrl($user, ['widget' => 1]); ?>"
                    frameborder="0"></iframe>
        </div>
    </div>
    <?php
} else {
    ?>

    <div class="panel panel-default panel-body">
        <p>
            <?= _t('site.store', 'You have not added any 3d models yet'); ?>
        </p>
        <a class="btn btn-primary btn-sm" href="/mybusiness/products?type=model3d">
            <?= _t('site.store', 'Add 3D model'); ?>
        </a>
    </div>
<?php } ?>
