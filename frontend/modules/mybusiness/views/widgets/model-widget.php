<?php
/** @var \common\models\User $user */

$this->title = _t('site.ps','Business Tools - 3D Model Widget');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="model-embed__frame" id="embedframe">
            <?= $this->render('modelsEmbed', ['model3ds' => $user->getModel3ds()->storePublished($user)->all()]); ?>
        </div>
    </div>
</div>
