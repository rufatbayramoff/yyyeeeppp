<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.11.17
 * Time: 14:37
 */

use common\services\PsPrinterService;
use common\services\WidgetService;
use frontend\models\user\UserFacade;

/** @var $company \common\models\Company */

Yii::$app->angular
    ->service(['notify', 'user', 'router'])
    ->controller('company/cashbackController')
    ->controllerParams(
        [
            'companyCashbackPercent' => $company->cashback_percent,
        ]
    );
$this->title = _t('site.ps','Business Tools - Bonus system');
?>

<div class="row" ng-controller="CashbackController">
    <div class="col-lg-12">
        <div class="designer-card">
            <div class="row">
                <div class="col-md-12">

                    <h2 class="designer-card__title">
                        <?= _t('site.ps', 'Bonus system for customers'); ?>
                    </h2>
                    <p class="designer-card__about">
                        <?= _t('site.ps', 'Treatstock Bonus is a virtual unit that can be used by a Customer to receive stimulations including discounts for their Purchases. All the customers are able to see how many bonuses they get from each order to make a decision. Choose your bonus rate and get your loyal customer.'); ?>
                    </p>

                    <div class="cashback-range">
                        <input type="range" min=0 max="20" step="5" ng-model="companyCashbackPercent" ng-change="updatePercent()" >
                        <div class="cashback-range__label">
                            <div class="cashback-range__label-item">0%</div>
                            <div class="cashback-range__label-item">5%</div>
                            <div class="cashback-range__label-item">10%</div>
                            <div class="cashback-range__label-item">15%</div>
                            <div class="cashback-range__label-item">20%</div>
                        </div>
                    </div>

                    <p class="designer-card__about">
                        <?= _t('site.ps', 'You confirm sending {{companyCashbackPercent}}% of your award to the customer. Treatstock will double it in the form of a bonus. The total bonus for the customer is <b>{{percentPlus}}%</b>'); ?>.
                    </p>
                    <div class="bar bg-info">
                        <?= _t('site.ps', 'We double bonuses for the following order types'); ?>:
                        <ul class="p-l20">
                            <li><?= _t('site.ps', 'Instant order placed through your company page'); ?></li>
                            <li><?= _t('site.ps', 'Quote and Instant Payment'); ?></li>
                            <li><?= _t('site.ps', 'Additional Service for any order'); ?></li>
                            <li><?= _t('site.ps', 'Quote on Products'); ?></li>
                        </ul>
                        <?= _t('site.ps', 'Please note, we do not double a bonus for instant orders. Please visit our <a href="/help">Help Center</a> to find more information.'); ?>.
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
