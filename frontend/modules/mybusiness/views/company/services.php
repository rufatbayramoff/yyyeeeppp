<?php
/**
 * @var \common\models\CompanyServiceCategory[] $services
 * @var \common\models\CompanyCategory[] $companyCategories
 * */
Yii::$app->angular
    ->controller('ps/ps/service')
    ->service(['notify', 'router', 'user', 'modal','router'])
    ->controllerParam('services', $services)
    ->controllerParam('companyCategories', $companyCategories);
$this->title = _t('site.ps', 'Capabilities');
?>

<div ng-controller="PsServiceController" ng-cloak>
    <div class="row" >

        <div class="col-md-12">

            <div class="form-group row">
                <label class="col-sm-3 col-lg-2 control-label p-r0 m-t5 m-b10"><?php echo _t('site.ps','Service Category')?></label>

                <div class="col-sm-9 col-lg-10">

                    <div class="">
                        <button type="button" ng-click="serviceClick(service)" ng-repeat="service in services" ng-class="{active: service.active}" class="btn btn-default btn-sm btn-ghost m-r15 m-b15 ">
                            {{ service.title }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="ps-steps__footer clearfix">

                <button ng-click="save()" type="button" ng-disabled="saveDisable()"
                        class="btn btn-primary">
                    <?php echo _t('site.ps','Save')?>
                </button>

            </div>

        </div>
    </div>
</div>
