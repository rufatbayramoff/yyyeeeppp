<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.03.17
 * Time: 12:06
 */

/** @var string $signedRequest */

use common\models\Ps;
use frontend\models\ps\PsFacade;
use yii\widgets\ActiveForm;

$examplePs = Ps::tryFindByPk(1);
$examplePs->url = 'name';
?>
<p>
    <?= _t('ps.profile', 'Please submit the Public Profile link of the print service on Treatstock to active the app.'); ?>
</p>
<p>
    <?= _t('ps.profile', 'Example: {exampleLink}', ['exampleLink' => PsFacade::getPsLink($examplePs, [], true)]); ?>
</p>
<?php
$form = ActiveForm::begin(
    [
        'id' => 'form-approve',

    ]
);
?>
<div class="row">
    <div class="col-sm-10 <?= $validateMessage ? ' has-error' : '' ?>">
        <div class="input-group">
            <input type="text" class="form-control" name="psLink" value="<?= H($psLink) ?>">

            <span class="input-group-btn">
                        <button class="btn btn-primary"><?= _t('ps.profile', 'Connect') ?></button>
            </span>
            <input type="hidden" name="signed_request" value="<?= H($signedRequest) ?>">

        </div>
        <?= $validateMessage ? '<p class="help-block help-block-error">' . H($validateMessage) . '</p>' : '' ?>
    </div>
</div>
<?php
ActiveForm::end();
?>

