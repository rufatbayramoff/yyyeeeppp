<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.02.17
 * Time: 14:25
 */
use common\models\Ps;
use yii\bootstrap\ActiveForm;

/** @var Ps $ps */
/** @var string $signedRequest */
?>
<?= _t('ps.profile', 'Link "{psName}" with facebook page.', ['psName' => H($ps->title)]); ?>
<?php
$form = ActiveForm::begin([
    'id' => 'form-approve',
]);
?>
<button class="btn btn-primary"><?=_t('ps.profile', 'Yes, approve')?></button>
<input type="hidden" name="psLinkApprove" value="true">
<input type="hidden" name="signed_request" value="<?=H($signedRequest)?>">
<?php
ActiveForm::end();
?>

