<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.02.17
 * Time: 14:23
 */
use common\models\Ps;
use yii\web\View;

/** @var View $this */
/** @var Ps $ps */
?>

<?= _t('ps.profile', '"{psName}" was successfully linked with facebook page.', ['psName' => H($ps->title)]); ?>
<?php
$this->registerJs(
    <<<SCRIPT
setTimeout(function() {
  window.top.reload();
}, 2000);
SCRIPT
);


