<?= $this->renderFile('@app/views/user/my/_top.php', ['user' => \frontend\models\user\UserFacade::getCurrentUser()]);?>



<div class="container">

<div class="row">
    <div class="col-xs-12 wide-padding">
        <h1><?= _t('front.user', 'Print Service'); ?></h1>
    </div>
</div>

<div class="row ps-index ps-welcome">

    <div class="col-md-8">
        <div class="panel box-border">
            <div class="panel-body">
                <div class="text-center">
                    <h2 class="m-b30">
                        <?= _t('site.ps', 'Own a 3D printer?'); ?>
                    <br>
                        <?= _t('site.ps', 'Start your print service and put it to work!'); ?>
                    </h2>

                    <img class="img-responsive center-block m-b20" width="303" height="140" src="/static/images/ps-welcome-service.png">

                    <p class="ps-welcome-info">
                        <?= _t('site.ps', 'Register your Print Service so others can use your services on Treatstock.'); ?>
                    </p>

                    <p>
                        <a href="<?= \Yii::$app->urlManager->createUrl("/mybusiness/services/select-service-type?fromCreatePs=1"); ?>" class="btn btn-primary">
                            <?= _t('site.ps', 'Start Print Service'); ?>...
                        </a>

                        <br>

                        <?php /*?>
                        <a href="/site/ps-learn-more" class="btn btn-link">
                            <?= _t('site.ps', 'Learn more'); ?>
                        </a>
                        <?php */?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel box-border">
            <div class="panel-body">
                <div class="text-center">
                    <h2 class="m-b30">
                        <?= _t('site.ps', 'Print for yourself!'); ?>
                    </h2>

                    <br>

                    <img class="img-responsive center-block m-b20" width="244" height="140" src="/static/images/ps-welcome-yourself.png">

                    <p class="ps-welcome-info">
                        <?= _t('site.ps', 'Register your 3D printer so that you can print 3D models for yourself that are available on Treatstock.'); ?>
                    </p>

                    <p>
                        <a href="<?= \Yii::$app->urlManager->createUrl("/mybusiness/services/select-service-type?fromCreatePs=1"); ?>" class="btn btn-primary">
                            <?= _t('site.ps', 'Print for yourself!'); ?>
                            <!-- todo: paste normal link, it's only copy-paste from previous block-->
                        </a>

                        <br>

                        <?php /*?>
                        <a href="/site/ps-learn-more" class="btn btn-link">
                            <?= _t('site.ps', 'Learn more'); ?>
                        </a>
                        <?php */?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
