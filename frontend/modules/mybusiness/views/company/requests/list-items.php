<?php

use common\components\Model3dWeightCalculator;
use common\models\DeliveryType;
use common\models\message\helpers\UrlHelper;
use common\models\model3d\PrintInstructionParser;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use common\models\StorePricer;
use common\models\UserAddress;
use common\modules\payment\components\PaymentInfoHelper;
use common\services\Model3dPartService;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserFacade;
use frontend\widgets\PrintInstructionWidget;
use frontend\widgets\ReviewStarsStaticWidget;
use frontend\widgets\SiteHelpWidget;
use frontend\widgets\SwipeGalleryWidget;
use lib\delivery\delivery\models\DeliveryPostalLabel;
use lib\delivery\parcel\ParcelFactory;
use lib\MeasurementUtil;
use lib\money\Currency;
use yii\helpers\ArrayHelper;
use \yii\helpers\Html;
use \frontend\models\ps\StoreOrderAttemptProcess;
use \frontend\models\ps\PsFacade;
use yii\helpers\Url;

/** @var StoreOrderAttemp[] $attemps Orders attemps */
/** @var \frontend\models\ps\StoreOrderAttempSearchForm $form */
/** @var $refundService \common\modules\payment\services\RefundService */
/** @var \common\models\Model3dReplica $model3dReplica */
/** @var \common\models\StoreOrderItem $oneStoreItem */

/**
 * @param StoreOrderAttemp $attemp
 * @param \yii\web\View $this
 * @return string|null
 */

Yii::$app->angular
    ->service(['modal', 'notify', 'router', 'user', 'psDownloadModel'])
    ->controller('ps/orders/list')
    ->controllerParams([
        'ps' => $form->getPs()
    ]);


$cancelledTextFn = function (StoreOrderAttemp $attemp) {
    if (!$attemp->isCancelled()) {
        return null;
    }

    $text = '';
    if ($attemp->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_PS) {
        $text .= _t('site.ps', 'Canceled by you at ');
    } elseif ($attemp->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_CLIENT) {
        $text .= _t('site.ps', 'Canceled by customer at ');
    } elseif ($attemp->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_MODERATOR) {
        $text .= _t('site.ps', 'Canceled by moderator at ');
    } elseif ($attemp->isExpired()) {
        $text .= _t('site.ps', 'Expired time period.') . '<br>' .
            _t(
                'site.ps',
                'This order was offered to you and several other print services on a first come first serve basis. As another print service has accepted the order, it is now expired and no longer available. At '
            );
    } else {
        $text .= _t('site.ps', 'Canceled at ');
    }
    return $text . app()->formatter->asDatetime($this->attemp->cancelled_at);
};

$userWightMeasure = UserSessionFacade::isUsMeasurement() ? MeasurementUtil::OUNCE : MeasurementUtil::GRAM;
$currentUser = UserFacade::getCurrentUser();
?>
<div class="row wide-padding" ng-controller="PsOrdersListController">

    <div class="col-xs-12">

        <div class="panel panel-default border-0">

            <?php if (!$attemps): ?>
                <h3 class="no-print-requests"><?= _t("site.ps", "No orders to display"); ?></h3>
            <?php endif ?>

            <?php
            foreach ($attemps as $attemp) :
                $cancelledInNew = $attemp->isCancelled() && $form->statusGroup == StoreOrderAttemp::STATUS_NEW;
                $deliveryDetail = $attemp->order->getOrderDeliveryDetails();
                $isOffer = $attemp->is_offer;
                $order = $attemp->order;
                $attemptProcess = StoreOrderAttemptProcess::create($attemp);
                $deadlineTimer = \frontend\models\order\StoreOrderAttemptDeadline::create($attemp);
                ?>
                <div class="one-print-request panel panel-default box-shadow border-0 <?= $cancelledInNew ? 'one-print-request-cancelled-in-new' : '' ?>"
                     data-order-id="<?= $attemp->id; ?>">
                    <div class="panel-body">
                        <?php if ($order->is_dispute_open): ?>
                            <div class="alert alert-warning"><?= _t('site.order', 'Order is in dispute. Please contact the {link}',
                                    [
                                        'link' => Html::a(_t('site.order', 'customer'), UrlHelper::objectMessageRoute($order))
                                    ]); ?></div>
                        <?php
                        endif; ?>
                        <?php
                        if ($deadlineTimer) {
                            echo '<div class="row " style="margin:-5px 0 10px;"><strong>';
                            echo $deadlineTimer->getTitle() . ':</strong> ';
                            echo \frontend\widgets\Countdown::widget(
                                [
                                    'id'              => 'countdownattemp-' . $attemp->id,
                                    'options'         => ['class' => 'label label-info'],
                                    'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                                    'timeout'         => $deadlineTimer->getTimeout(),
                                    'finishedMessage' => _t('site.ps', '00:00:00')
                                ]
                            );
                            echo '</div>';
                        } ?>
                        <div class="one-print-request__user">
                            <div class="one-print-request__user__avatar">
                                <?= frontend\components\UserUtils::getAvatarForUser($order->user, 'print-request'); ?>
                            </div>

                            <?php if (!$cancelledInNew): ?>

                                <div class="dropdown one-print-request__user__message">
                                    <button class="btn btn-default btn-circle dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu1"
                                            data-toggle="dropdown">
                                        <span class="tsi tsi-message"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">

                                        <?php if ($attemptProcess->canRequestMoreTime()): ?>
                                            <li>
                                                <a
                                                        data-date="<?= $attemp->dates->plan_printed_at ?>"
                                                        ng-click="requestMoreTimeModal(<?= $attemp->id ?>, $event)"
                                                        href="#" class="request-more-time-link"><?= _t('site.ps', 'Request more time'); ?></a>
                                            </li>
                                        <?php endif ?>

                                        <?php if (!$order->user->equals($attemp->ps->user) && !$attemp->isCancelled() && !$order->isAnonim()
                                        ): ?>
                                            <li>
                                                <?= Html::a(_t('site.ps', 'Contact customer'), UrlHelper::objectMessageRoute($order), ['target' => '_blank']); ?>
                                            </li>
                                        <?php endif ?>

                                        <li>
                                            <?= Html::a(_t('site.ps', 'Contact support'), UrlHelper::supportMessageRoute($order), ['target' => '_blank']); ?>
                                        </li>
                                    </ul>
                                </div>

                            <?php endif; ?>

                            <div class="one-print-request__user__info">
                                <?php if ($cancelledInNew || $order->isAnonim() || empty($order->user->email)): ?>
                                    <?= H(UserFacade::getFormattedUserName($order->user)); ?>
                                <?php else: ?>
                                    <a href="<?= HL(Url::toRoute(UrlHelper::objectMessageRoute($order))) ?>"><?= H(
                                            UserFacade::getFormattedUserName($order->user)
                                        ); ?></a>
                                <?php endif; ?>
                                <br>
                                <?= _t("site.ps", "Order #"); ?><b><?= H($attemp->order->id); ?></b>
                            </div>

                        </div>

                        <div class="one-print-request__btns">

                            <?php if ($attemptProcess->canAcceptOrder()): ?>

                                <button
                                        loader-click="setOrderStatus(<?= H($attemp->id) ?>, '<?= StoreOrderAttemp::STATUS_ACCEPTED; ?>')"
                                        class="btn btn-success">
                                    <?= _t("site.ps", "Accept order"); ?>
                                </button>

                                <?php if (PaymentInfoHelper::isPaymentPendingWarningNeed($order, $attemp->machine)): ?>
                                    <p class="bg-warning">
                                        <?= _t('site.ps',
                                            "Payment has been made and is pending authorization. You may accept the order but please don't start printing until payment has been authorized. Need help? {contactLink}",
                                            ['contactLink' => Html::a(_t('site.ps', 'Contact support'), UrlHelper::supportMessageRoute($order), ['target' => '_blank'])]); ?>

                                    </p>
                                <?php endif; ?>
                            <?php endif ?>

                            <?php if ($attemptProcess->canDeclineOrder()) : ?>

                                <button
                                        ng-click="declineOrder(<?= $attemp->id ?>)"
                                        class="btn btn-warning">
                                    <?= _t('front.user', 'Decline'); ?>
                                </button>

                            <?php endif; ?>
                            <?php if ($attemptProcess->canPrintOrder()): ?>

                                <?= Html::a(
                                    _t('site.ps', 'Order Details'),
                                    ['/my/company-order/print', 'attempId' => $attemp->id],
                                    ['class' => 'btn btn-default']
                                ) ?>

                            <?php endif ?>

                            <?php if ($attemptProcess->canSetAsPrinted()) : ?>
                                <?php
                                $isRejected = $attemp->moderation && $attemp->moderation->status == 'rejected';
                                if (!StoreOrderAttemptProcess::create($attemp)->canSetAsPrinted() || !$attemp->moderation->files || $isRejected) : ?>
                                    <?= Html::a(
                                        _t('site.ps', 'Submit results'),
                                        ['/my/company-order/print', 'attempId' => $attemp->id],
                                        ['class' => 'btn btn-success']
                                    ) ?>
                                <?php else: ?>
                                    <button
                                            loader-click="setOrderStatus(<?= $attemp->id ?>, '<?= StoreOrderAttemp::STATUS_PRINTED; ?>')"
                                            class="btn btn-success">
                                        <?= _t('site.ps', 'Submit results'); ?>
                                    </button>
                                <?php endif; ?>
                            <?php endif ?>

                            <?php if ($attemptProcess->canGetPostalLabel()):
                                $parcel = ParcelFactory::createFromOrder($attemp->order);
                                if (false && DeliveryPostalLabel::checkPostalLabelExists($attemp)) { // turnoff
                                    ?>
                                    <button
                                            loader-click="printPostalLabel(<?= $attemp->id ?>)"
                                            class="btn btn-success">
                                        <?= _t('site.ps', 'Postal label'); ?>
                                    </button>
                                    <?php
                                } else {
                                    $parcelInLocalMeasure = $attemp->parcel;
                                    if ($currentUser->userProfile->current_metrics === 'in') {
                                        $parcelInLocalMeasure = \lib\delivery\parcel\ParcelFactory::convertParcelInInch($parcelInLocalMeasure);
                                    }
                                    ?>
                                    <button
                                            loader-click="getPostalLabel(<?= $attemp->id ?>, <?= sprintf('%.2F, %.2F, %.2F, %.2F',
                                                $parcelInLocalMeasure->length, $parcelInLocalMeasure->width, $parcelInLocalMeasure->height, $parcelInLocalMeasure->weight); ?>)"
                                            class="btn btn-success">
                                        <?= _t('site.ps', 'Generate postal label'); ?>
                                    </button>
                                <?php }
                            endif; ?>

                            <?php if ($attemptProcess->canSetAsSentWithTrackingNumber()) :

                                ?>
                                <button
                                        loader-click="setOrderSentWithTrackingNumber(<?= $attemp->id ?>)"
                                        class="btn btn-success">
                                    <?= _t('site.ps', 'Set as shipped'); ?>
                                </button>
                            <?php elseif ($attemptProcess->canSetAsSent()) :
                                $isPickup = \lib\delivery\delivery\DeliveryFacade::isPickupDelivery($attemp->order);
                                $setAsShipped = _t('site.ps', 'Set as shipped');
                                if ($isPickup) {
                                    $setAsShipped = _t('site.ps', 'Set as picked up');
                                }
                                ?>
                                <button
                                        loader-click="setOrderStatus(<?= $attemp->id ?>, '<?= StoreOrderAttemp::STATUS_SENT; ?>')"
                                        class="btn btn-success">
                                    <?= $setAsShipped; ?>
                                </button>


                            <?php elseif ($attemp->isPendingModeration()):
                                $reviewHelp = frontend\widgets\SiteHelpWidget::widget(
                                    [
                                        'alias' => 'ps.printreview'
                                    ]
                                );
                                echo Html::tag('p', _t('site.ps', 'Pending moderation ') . $reviewHelp, ['class' => 'text-info m-t10']);

                            endif; ?>

                            <?php if (false && $attemptProcess->canSetAsDelivered()) : // turned off by #4245 ?>
                                <button
                                        loader-click="setOrderStatus(<?= $attemp->id ?>, '<?= StoreOrderAttemp::STATUS_DELIVERED; ?>')"
                                        class="btn btn-success">
                                    <?= _t('site.ps', 'Set as delivered'); ?>
                                </button>

                                <?php
                                echo $linkHelp = frontend\widgets\SiteHelpWidget::widget(
                                    [
                                        'alias' => 'ps.delivery.setasdelivered'
                                    ]
                                );
                            endif ?>



                            <?php if ($attemptProcess->canShowReceivedMessage()) { ?>
                                <div class="one-print-request__btns-text"><?= _t('site.ps', 'Received on'); ?>
                                    <?= app('formatter')->asDatetime($attemp->dates->received_at, 'short'); ?></div>
                            <?php } ?>

                            <?php if ($attemptProcess->canShowShippedMessage()) { ?>
                                <div class="one-print-request__btns-text"><?= _t('site.ps', 'Dispatched on'); ?>
                                    <?= app('formatter')->asDatetime($attemp->dates->shipped_at, 'short'); ?></div>
                            <?php } ?>

                            <?php if ($attemptProcess->canShowCancelledMessage()) { ?>
                                <div class="one-print-request__btns-text"><?= $cancelledTextFn($attemp) ?></div>
                            <?php } ?>

                            <div>
                                <?php

                                if ($order->currentAttemp && $attemp->id === $order->currentAttemp->id && $deliveryDetail && !empty($deliveryDetail['tracking_number'])) {

                                    $trackingNumber = !$cancelledInNew && DeliveryPostalLabel::checkPostalLabelExists($attemp)
                                        ? Html::a(
                                            '<b>' . $deliveryDetail['tracking_number'] . '</b>',
                                            '/my/company-order/postal-image?attempId=' . $attemp->id,
                                            ['target' => '_blank']
                                        )
                                        : '<b>' . $deliveryDetail['tracking_number'] . '</b>';
                                    if ($attemp->isTrackingNumberVisible()) {
                                        echo _t(
                                            'site.ps',
                                            'Tracking number {tracking_number}, {tracking_shipper}',
                                            [
                                                'tracking_number'  => $trackingNumber,
                                                'tracking_shipper' => $deliveryDetail['tracking_shipper'],
                                            ]
                                        );
                                    }
                                }
                                ?>
                            </div>

                            <?php

                            $review = \common\models\StoreOrderReview::find()
                                ->forAttemp($attemp)
                                ->forPs($attemp->ps)
                                ->isCanShow()
                                ->one();

                            if ($review) {
                                $stars = ReviewStarsStaticWidget::widget(['rating' => $review->getRating()]);
                                echo Html::a($stars, ['/workbench/reviews']);
                            }
                            ?>

                        </div>

                        <?php if ($isOffer && $attemp->isNew()): ?>
                            <div class="pull-left m-t20">
                                <h3 class="m-t0 m-b0 text-danger">
                                    <?= _t('site.ps', 'Print offer'); ?>
                                </h3>
                                <?php if ($order->isThingiverseOrder()): ?>
                                    <p class="m-b0">
                                        <?= _t(
                                            'site.ps',
                                            'This print offer has been sent to you because the original order placed by the customer was declined by the print service. As the customer has already paid for the order, the price for printing and shipping can\'t be changed. If you accept the print offer, you agree to take over and complete the order for the customer for this price.'
                                        ); ?>
                                    </p>
                                <?php else: ?>
                                    <p class="m-b0">
                                        <?= _t('site.ps',
                                            'This print offer has been sent to you because the original order placed by the customer was declined by the print service. You may choose to accept the order as it is, or create an additional payment for the customer with the additional service option available after accepting.'); ?>
                                    </p>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($attemp->moderation && $attemp->moderation->status == 'rejected'): ?>
                            <div class="pull-left m-t20 bg-warning bar">
                                <?= _t('site.ps', 'Order results have not been accepted. Reason: {reason}.', ['reason' => $attemp->moderation->reject_comment]) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php foreach ($order->storeOrderItems as $oneStoreItem) { ?>
                        <div class="panel-body one-print-request__body">
                            <div class="row">
                                <div class="col-sm-3 one-print-request__model-pic text-center">

                                    <?php if ($oneStoreItem->hasModel()): ?>

                                        <?php

                                        $model3dReplica = $oneStoreItem->model3dReplica;
                                        $orderItemLink = Url::toRoute(
                                            [
                                                '/store/model3d-replica',
                                                'id' => $model3dReplica->id,
                                            ]
                                        );
                                        ?>

                                        <?php
                                        $cover = \frontend\models\model3d\Model3dFacade::getCover($model3dReplica);

                                        if ($cover) {
                                            $img = Html::img(
                                                $cover['image'],
                                                [
                                                    'width' => '100',
                                                    'alt'   => \H($model3dReplica->title),
                                                    'class' => 'order-model-image'
                                                ]
                                            );
                                            echo $img;
                                        }
                                        ?>

                                        <?php if (!$cancelledInNew): ?>
                                            <a href="<?= $orderItemLink ?>" target="_blank" class="btn btn-sm btn-link one-print-request__model-view"><?= _t(
                                                    'site.ps',
                                                    '3D View'
                                                ); ?></a>
                                        <?php endif; ?>


                                    <?php endif; ?>

                                </div>


                                <div class="col-sm-9">


                                    <?php if ($oneStoreItem->hasModel()): ?>


                                        <h3 class="one-print-request__model-title">
                                            <?= H($model3dReplica->title); ?>
                                            <?php if ($order->getIsTestOrder()): ?>
                                                <span class="label label-warning"><?= _t('site.ps', 'Test order') ?> <?= SiteHelpWidget::widget(
                                                        ['alias' => 'ps.order.test']
                                                    ) ?></span>
                                            <?php elseif ($order->isThingiverseOrder()): ?>
                                                <span class="label label-warning"><?= _t('site.ps', 'API Partner Order') ?> <?= SiteHelpWidget::widget(
                                                        ['alias' => 'ps.order.thingiverse']
                                                    ) ?></span>

                                            <?php endif; ?>

                                        </h3>
                                        <?php if ($order->isThingiverseOrder()) {
                                            $thingId = $order->thingiverseOrder->thing_id;
                                            $thingLink = 'https://www.thingiverse.com/thing:' . $thingId;
                                            echo _t('site.order', 'Original source: {link}', ['link' => Html::a($thingLink, $thingLink, ['target' => '_blank'])]);
                                        }
                                        ?>


                                        <h4 class="text-muted">
                                            <?= _t('site.ps', 'Total weight'); ?>
                                            :
                                            <?php
                                            $totalWeight = MeasurementUtil::convertWeight(
                                                $model3dReplica->getWeight(),
                                                MeasurementUtil::GRAM,
                                                $userWightMeasure,
                                                true
                                            );
                                            echo $totalWeight * $oneStoreItem->qty . ' ' . _t('site.common', $userWightMeasure);
                                            ?>
                                            -
                                            (<?= _t('site.ps', 'includes support calculations'); ?>) <br/>
                                            <span>

                                                <?php if ($attemp->order->isFor3dPrinter()) : ?>

                                                <?= _t('site.ps', 'Positional Accuracy'); ?> :

                                                     &#xB1; <?= (float)$attemp->machine->asPrinter()->getPositionalAccuracy(); ?> <?= _t('site.common', 'mm') ?></span>

                                        <?php endif ?>

                                        </h4>

                                    <?php endif; ?>

                                    <div class="row">
                                        <div class="col-sm-4">

                                            <?php if ($oneStoreItem->hasModel()): ?>

                                                <table class="one-print-request__table">
                                                    <tr>
                                                        <td class="one-print-request__table__label">
                                                            <?= _t('site.model3d', 'License'); ?>
                                                        </td>
                                                        <td>
                                                            <strong>
                                                                <?php if ($cancelledInNew): ?>
                                                                    <?= _t('site.store', 'Commercial') ?>
                                                                <?php else: ?>
                                                                    <?= Html::a(
                                                                        _t('site.store', 'Commercial'),
                                                                        ['site/license', 'id' => $model3dReplica->storeUnit->license_id],
                                                                        ['target' => '_blank']
                                                                    ); ?>
                                                                <?php endif; ?>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="one-print-request__table__label">
                                                            <?= _t('site.ps', 'Machine'); ?>
                                                        </td>
                                                        <td><?= H($attemp->machine->getTitleLabel()); ?></td>
                                                    </tr>

                                                    <?php if ($order->isFor3dPrinter()) : ?>

                                                        <?php
                                                        if ($model3dReplica->isOneTextureForKit()) {
                                                            $texture = $model3dReplica->getKitTexture();
                                                            ?>
                                                            <tr>
                                                                <td class="one-print-request__table__label">
                                                                    <?= _t('site.ps', 'Material'); ?>
                                                                </td>
                                                                <td><?= $texture ? H($texture->getMaterialFilamentOrGroupTitle() ?? '-') : '-' ?></td>
                                                            </tr>
                                                            <?php if ($texture->calculatePrinterMaterialGroup()): ?>
                                                                <tr>
                                                                    <td class="one-print-request__table__label">
                                                                        <?= _t('site.ps', 'Infill'); ?>
                                                                    </td>
                                                                    <td><?= $texture->infillText() ?></td>
                                                                </tr>
                                                            <?php
                                                            endif; ?>

                                                            <tr>
                                                                <td class="one-print-request__table__label">
                                                                    <?= _t('site.ps', 'Color'); ?>
                                                                </td>
                                                                <td><?= $texture && $texture->printerColor ? H($texture->printerColor->title) : '-'; ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>


                                                    <?php endif; ?>

                                                </table>

                                            <?php endif; ?>

                                        </div>


                                        <div class="col-sm-8">
                                            <table class="one-print-request__table">


                                                <?php if ($oneStoreItem->hasModel()): ?>

                                                    <?php $totalParts = array_sum(ArrayHelper::getColumn($model3dReplica->getCalculatedModel3dParts(),
                                                            'qty')) * $oneStoreItem->qty; ?>
                                                    <tr>
                                                        <td class="one-print-request__table__label">
                                                            <?= _t('site.ps', 'Quantity'); ?>
                                                        </td>
                                                        <td>
                                                            <?php #echo _t('site.ps', '{qty, plural, =1{# piece} other{# pieces}}', ['qty' => $oneStoreItem->qty]);
                                                            ?>
                                                            <?= _t(
                                                                'site.ps',
                                                                '{num, plural, =1{# item} other{# items}}',
                                                                ['num' => $totalParts]
                                                            ); ?>
                                                        </td>
                                                    </tr>

                                                <?php endif; ?>

                                                <?php
                                                $priceForPs = StorePricer::getPrintPriceForPs($attemp->order_id);
                                                if ($priceForPs && $priceForPs->price > 0) :
                                                    ?>

                                                    <tr>
                                                        <?php
                                                        $psFee = StorePricer::getPsFee($attemp->order_id);
                                                        ?>
                                                        <td class="one-print-request__table__label">

                                                            <?php
                                                            if ($psFee) {
                                                                echo SiteHelpWidget::widget([
                                                                    'triggerHtml' => _t('site.ps', 'Manufacturing fee'),
                                                                    'title'       => _t('site.ps', 'How we calculate quotes for clients?'),
                                                                    'alias'       => 'ps.add-printer.chargeClients'
                                                                ]);
                                                            }
                                                            ?></nobr>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $priceForPs = StorePricer::getPrintPriceForPs($attemp->order_id);

                                                            $price = $priceForPs
                                                                ? displayAsCurrency($priceForPs->price, $priceForPs->currency)
                                                                : displayAsCurrency(0, Currency::USD);

                                                            if ($psFee) {
                                                                $clientPrice = StorePricer::getPrintPrice($attemp->order_id);
                                                                $note = _t('site.ps',
                                                                    "Manufacturing fee for the customer is {price1}. Service fee is {price2}. Therefore, your manufacturing income is {price3}",
                                                                    [
                                                                        'price1' => displayAsCurrency($clientPrice->price, $clientPrice->currency),
                                                                        'price2' => displayAsCurrency($psFee->price, $psFee->currency),
                                                                        'price3' => displayAsCurrency($priceForPs->price, $priceForPs->currency)
                                                                    ]
                                                                );
                                                                echo sprintf(
                                                                    '<nobr>%s <span data-toggle="tooltip" data-placement="top" data-original-title="%s" class="tsi tsi-warning-c"></span></nobr>',
                                                                    $price,
                                                                    $note
                                                                );


                                                            } else {
                                                                echo $price;
                                                            }

                                                            if ($refundService->canRefundOrderTransaction($attemp->order)) {
                                                                echo '<br />';
                                                                echo Html::a(
                                                                    _t('site.order', 'Partial Refund'),
                                                                    ['/store/payment/refund-request', 'id' => $attemp->order_id],
                                                                    [
                                                                        'title'      => _t('site.order', 'Offer Partial Refund'),
                                                                        'class'      => 'btn btn-sm btn-link ts-ajax-modal',
                                                                        'style'      => 'margin-left: 0px; padding-left: 0px',
                                                                        'jsCallback' => 'window.location.reload()',
                                                                    ]
                                                                );
                                                            } else {
                                                                $refundsReuested = $refundService->getRefundRequestByOrder($attemp->order);
                                                                if ($refundsReuested) {
                                                                    foreach ($refundsReuested as $refundRequested):
                                                                        echo '<br /><span class="label label-warning">';
                                                                        if ($refundRequested->isApproved()) {
                                                                            echo _t(
                                                                                'site.order',
                                                                                'Refund Approved: {amount}',
                                                                                ['amount' => displayAsMoney($refundRequested->getMoneyAmount())]
                                                                            );
                                                                        } else {
                                                                            echo _t(
                                                                                'site.order',
                                                                                'Refund subm3itted: {amount}',
                                                                                ['amount' => displayAsMoney($refundRequested->getMoneyAmount())]
                                                                            );
                                                                        }
                                                                        echo '</span>';

                                                                    endforeach;;
                                                                } else {
                                                                    echo sprintf(
                                                                        '<br/><nobr>%s <span data-toggle="tooltip" data-placement="top" data-original-title="%s" class="tsi tsi-warning-c"></span></nobr>',
                                                                        _t('site.order', 'Partial refund unavailable'),
                                                                        $refundService->lastErrorMessage
                                                                    );

                                                                }
                                                                echo ' ' . frontend\widgets\SiteHelpWidget::widget(
                                                                        [
                                                                            'alias' => 'ps.order.partialRefund'
                                                                        ]
                                                                    ) . ' ';
                                                            }

                                                            ?>

                                                            <br/>

                                                        </td>
                                                    </tr>

                                                <?php endif; ?>

                                                <?php
                                                $addonPrice = StorePricer::getAddonPrice($order->id);
                                                if (!empty($addonPrice)):
                                                    $addonServices = $order->storeOrderPositions;
                                                    foreach ($addonServices as $addonService):
                                                        if ($addonService->status != StoreOrderPosition::STATUS_PAID) {
                                                            continue;
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td class="one-print-request__table__label">

                                                                <?= _t('site.order', 'Additional service'); ?>
                                                                <span data-toggle="tooltip" data-placement="bottom" data-original-title="<?= H($addonService->title); ?>"
                                                                      title="<?= H($addonService->title); ?>" class="tsi tsi-warning-c"></span>
                                                            </td>
                                                            <td><?php
                                                                echo displayAsCurrency($addonService->amount, $addonService->currency_iso); ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>

                                                <?php if ($order->hasDelivery()) : ?>

                                                    <tr>
                                                        <td class="one-print-request__table__label">
                                                            <?= _t('site.ps', 'Delivery'); ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($attemp->machine->asDeliveryParams()->isFreeDelivery($order)) {
                                                                echo _t('site.ps', 'Free Delivery');
                                                            } else {
                                                                if ($deliveryDetail && $deliveryDetail['carrier'] === common\models\DeliveryType::CARRIER_MYSELF) {
                                                                    $price = StorePricer::getShippingPrice($order->id);
                                                                    if ($price) {
                                                                        echo _t(
                                                                            'site.ps',
                                                                            '{price} Shipping price',
                                                                            ['price' => displayAsCurrency($price->price, $price->currency)]
                                                                        );
                                                                    }
                                                                }
                                                            }

                                                            ?>
                                                            <div>
                                                                <?= PsFacade::getDeliveryTypesIntl($order->deliveryType->code); ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $packagePrice = StorePricer::getPackagePrice($attemp->order_id);
                                                    if ($packagePrice && $packagePrice->price):
                                                        ?>

                                                        <tr>
                                                            <td class="one-print-request__table__label">
                                                                <?= _t('site.ps', 'Packaging fee'); ?>
                                                            </td>
                                                            <td>
                                                                <?= displayAsCurrency($packagePrice->price, $packagePrice->currency); ?>
                                                            </td>
                                                        </tr>

                                                    <?php endif; ?>





                                                    <tr>
                                                    <?php
                                                    $locationLink = _t('site.ps', 'Location');
                                                    if ($order->deliveryType->code === DeliveryType::PICKUP) {
                                                        $printerAddress = UserAddress::getAddressFromLocation($attemp->machine->location);
                                                        $address = UserAddress::formatAddress($printerAddress, true);
                                                    } else {
                                                        if (!$attemp->isShippingAddressVisible()) {
                                                            $order->shipAddress->address = '';
                                                            $order->shipAddress->contact_name = '';
                                                            $order->shipAddress->zip_code = '';
                                                            $order->shipAddress->phone = '';

                                                            if ($attemp->status !== StoreOrderAttemp::STATUS_DELIVERED &&
                                                                $attemp->status !== StoreOrderAttemp::STATUS_RECEIVED &&
                                                                !$attemp->isCancelled()) {
                                                                $locationLink = frontend\widgets\SiteHelpWidget::widget(
                                                                        [
                                                                            'triggerHtml' => _t('site.ps', 'Location'),
                                                                            'alias'       => 'ps.order.shippingAddress'
                                                                        ]
                                                                    ) . ' ';
                                                            }

                                                        }
                                                        $address = UserAddress::formatAddress($order->shipAddress, true);
                                                        $address = str_replace("<br />
                                                        <br />", "<br/>", $address);
                                                    }
                                                    ?>
                                                    <td class="one-print-request__table__label">
                                                        <?= HL($locationLink); ?>
                                                    </td>
                                                    <td>

                                                        <?php
                                                        echo H($address);
                                                        ?>
                                                    </td>
                                                    </tr><?php endif; ?>
                                            </table>
                                        </div>
                                    </div>

                                    <?php if ($attemp->order->comment): ?>
                                        <h4 class="m-t0 m-b0"><?= _t('site.order', 'Customer comment'); ?></h4>
                                        <?= nl2br(H($attemp->order->comment)); ?>
                                    <?php endif; ?>


                                    <?php if ($oneStoreItem->hasModel()): ?>

                                        <?php
                                        if ($printInstruction = PrintInstructionParser::instance()->parse($oneStoreItem->model3dReplica->print_instructions)) {
                                            ?>
                                            <div>
                                                <div class="one-print-request__table__label">
                                                    <?= _t('site.ps', 'Print instructions'); ?>
                                                </div>
                                                <div>
                                                    <?= PrintInstructionWidget::widget(['model' => $printInstruction]) ?>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    <?php endif ?>

                                </div>

                                <?php if ($oneStoreItem->hasModel()): ?>

                                    <div style="margin-left: 15px;">
                                        <button class="btn btn-default  m-b20 m-t10" data-target="#js-files-list-<?= $order->id; ?>" data-toggle="collapse">
                                            <?= _t('site.ps', 'Files'); ?>
                                        </button>
                                    </div>

                                    <div class="collapse <?= $attemp->isNew() ? 'in' : ''; ?>" id="js-files-list-<?= $order->id; ?>">
                                        <div class="col-md-12">

                                            <?php
                                            foreach ($oneStoreItem->model3dReplica->getCalculatedModel3dParts() as $model3dPart) {
                                                $texture = $model3dPart->getCalculatedTexture();
                                                $model3dPartImgUrl = Model3dPartService::getModel3dPartRenderUrl($model3dPart);
                                                if ($model3dPart->qty < 1) {
                                                    continue;
                                                } ?>

                                                <div class="row m-b10">
                                                    <div class="col-sm-3 one-print-request__model-pic">
                                                        <?= Html::img(
                                                            $model3dPartImgUrl,
                                                            [
                                                                'width' => '100',
                                                                'class' => 'order-model-image'
                                                            ]
                                                        );
                                                        ?>

                                                    </div>
                                                    <div class="col-sm-9">
                                                        <h4 class="one-print-request__model-title">
                                                            <button class="btn btn-link one-print-request__model-title-link"
                                                                    loader-click='downloadFiles("<?= $attemp->id ?>", "<?= $model3dPart->file->id ?>")'
                                                                    loader-click-text="<i class='tsi tsi-refresh tsi-spin-custom'></i>  <?= _t('site.order',
                                                                        'Preparing archive for download...') ?>"
                                                                    href="#">
                                                                <?= H($model3dPart->file->getFileName()); ?>
                                                            </button>
                                                        </h4>

                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <table class="one-print-request__table">

                                                                    <?php if ($texture && $texture->getMaterialFilamentOrGroupTitle()): ?>
                                                                        <tr>
                                                                            <td class="one-print-request__table__label">
                                                                                <?= _t('site.ps', 'Material'); ?>
                                                                            </td>
                                                                            <td><?= H($texture->getMaterialFilamentOrGroupTitle()) ?></td>
                                                                        </tr>
                                                                    <?php endif; ?>

                                                                    <?php if ($texture && $texture->printerColor): ?>
                                                                        <tr>
                                                                            <td class="one-print-request__table__label">
                                                                                <?= _t('site.ps', 'Color'); ?>
                                                                            </td>
                                                                            <td><?= H($texture->printerColor->title) ?></td>
                                                                        </tr>
                                                                    <?php endif; ?>

                                                                    <tr>
                                                                        <td class="one-print-request__table__label">
                                                                            <?= _t('site.ps', 'Size'); ?>
                                                                        </td>
                                                                        <td><?= H($model3dPart->getSize()->toStringWithMeasurement()) ?></td>
                                                                    </tr>

                                                                </table>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <table class="one-print-request__table">
                                                                    <?php
                                                                    $netWeight = $model3dPart->getWeight() * $oneStoreItem->qty;
                                                                    $supportWeightOne = Model3dWeightCalculator::calculateModel3dPartSupportWeight($model3dPart) / $model3dPart->qty;
                                                                    ?>
                                                                    <tr>
                                                                        <td class="one-print-request__table__label">
                                                                            <?= _t('site.ps', 'Model weight'); ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                            echo MeasurementUtil::convertWeight(
                                                                                    $model3dPart->getWeight() / $model3dPart->qty - $supportWeightOne,
                                                                                    MeasurementUtil::GRAM,
                                                                                    $userWightMeasure,
                                                                                    true
                                                                                ) . " {$userWightMeasure}";
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="one-print-request__table__label">
                                                                            <?= _t('site.ps', 'Supports weight'); ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php

                                                                            echo MeasurementUtil::convertWeight(
                                                                                    $supportWeightOne,
                                                                                    MeasurementUtil::GRAM,
                                                                                    $userWightMeasure,
                                                                                    true
                                                                                ) . " {$userWightMeasure}";
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="one-print-request__table__label">
                                                                            <?= _t('site.ps', 'Quantity'); ?>
                                                                        </td>
                                                                        <td>
                                                                            <?= $model3dPart->qty * $oneStoreItem->qty; ?>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td class="one-print-request__table__label">
                                                                            <?= _t('site.ps', 'Net Weight'); ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                            echo MeasurementUtil::convertWeight(
                                                                                    $netWeight,
                                                                                    MeasurementUtil::GRAM,
                                                                                    $userWightMeasure,
                                                                                    true
                                                                                ) . " {$userWightMeasure}";
                                                                            ?>
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="row m-b10">
                                                <div class="col-sm-3 one-print-request__model-pic">
                                                </div>
                                                <div class="col-sm-9">
                                                    <button class="btn btn-sm btn-default m-b10"
                                                            loader-click="downloadFiles(<?= $attemp->id ?>)"
                                                            loader-click-text="<i class='tsi tsi-refresh tsi-spin-custom'></i> <?= _t('site.order',
                                                                'Preparing archive for download...') ?>"
                                                            href="#"><?= _t('site.ps', 'Download file(s)'); ?></button>

                                                    <?php if ($attemp->isNew()) { ?>
                                                        <p class="alert alert-warning">
                                                            <?php
                                                            echo _t('site.ps', 'Please do not produce downloaded files without accepting the order.');
                                                            ?>
                                                        </p>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                            <?php
                                            $totalParts = array_sum(ArrayHelper::getColumn($model3dReplica->getCalculatedModel3dParts(), 'qty')) * $oneStoreItem->qty;
                                            if (false && $attemp->isNew()) : // turn off for now #3485
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-3"></div>
                                                    <div class="col-sm-9">
                                                        <div class="panel panel-default pull-left">
                                                            <div class="panel-body">
                                                                <strong>
                                                                    <?= _t('site.ps', 'Total quantity to print: {num}', ['num' => $totalParts]); ?>
                                                                </strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif ?>

                                        </div>
                                    </div>
                                <?php endif ?>

                            </div>
                        </div>
                    <?php } ?>



                    <?php if ($attemp->moderation && $attemp->moderation->files): ?>

                        <div class="panel-body border-b p-b0">
                            <div class="one-print-request__steps-pic-list p-t0">
                                <?=
                                SwipeGalleryWidget::widget([
                                    'files'            => ArrayHelper::getColumn($attemp->moderation->files, 'file'),
                                    'thumbSize'        => [ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL],
                                    'assetsByClass'    => true,
                                    'containerOptions' => ['class' => 'ps-profile-user-review__user-models'],
                                    'itemOptions'      => ['class' => 'ps-profile-user-review__user-models-pic'],
                                    'scrollbarOptions' => ['class' => 'ps-profile-user-review__user-models-scrollbar'],
                                ]);
                                ?>
                            </div>
                            <?php if ($attemp->isAcceptedByUser()): ?>
                                <div class="one-print-request__steps-liked">
                                    <span class="tsi tsi-checkmark-c m-r10"></span><?= _t("site.order", "Liked by customer"); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif ?>




                    <?php if ($attemp->order->isForPreorder()) : ?>
                        <?= $this->render('/ps/requests/_preorder_details', ['oneOrder' => $attemp->order]) ?>
                    <?php endif; ?>


                    <!--- additional services -->
                    <?php
                    echo $this->renderFile('@frontend/views/ps/requests/list-item-additional-services.php', ['attemp' => $attemp, 'order' => $attemp->order]);
                    ?>


                    <div class="panel-body one-print-request__footer">
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="send-print-request-date">
                                    <small>
                                    </small>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="send-print-request-printing-time">
                                    <div class="">
                                        <?php if ($attemp->isNew() || $attemp->status == StoreOrderAttemp::STATUS_ACCEPTED) : ?>
                                            <?= frontend\widgets\SiteHelpWidget::widget(['title' => 'Help', 'alias' => 'ps.estimatetime']); ?>
                                            <?= _t('site.ps', 'Estimated production time'); ?>
                                            <span
                                                    data-date="<?= $attemp->dates->plan_printed_at ?>"
                                                    ng-click="requestMoreTimeModal(<?= $attemp->id ?>, $event)"
                                                    class="request-more-time-link request-more-time-span">
                                            <span class="tsi tsi-alarm-clock"></span><?php if ($attemp->isNew()) : ?><?= $attemp->dates->plan_printed_at ? PsFacade::getAttempDeadLine($attemp) : PsFacade::getEstimatedPrintingTime(
                                                        $oneStoreItem->model3dReplica
                                                    ) * $order->orderItem->qty; ?>
                                                <?php else: ?>
                                                    <?=$attemp->getDeadlineHours(); ?>
                                                <?php endif; ?>
                                                <?= _t('site.ps', 'hours'); ?>
                                        </span>

                                        <?php endif ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            <?php endforeach ?>
        </div>

    </div>

</div>

<?= $this->render('download-model-terms-modal') ?>
