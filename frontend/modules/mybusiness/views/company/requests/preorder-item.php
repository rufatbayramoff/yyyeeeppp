<?php
/** @var \common\models\Preorder $preorder */

use frontend\models\user\UserFacade;
use frontend\modules\preorder\components\PreorderUrlHelper;

?>

<div class="row">
    <div class="col-xs-12">

        <div class="panel panel-default box-shadow border-0">
            <div class="one-print-request">
                <div class="panel-body">
                    <div class="one-print-request__user">
                        <h3 style="margin: 5px 0 0">
                            <a href="<?= PreorderUrlHelper::viewPs($preorder)?>"><?= _t('site.preorder', 'Request #{id}', ['id' => $preorder->id])?></a>
                        </h3>
                    </div>

                    <div class="one-print-request__btns">
                        <a class="btn btn-primary" href="<?= PreorderUrlHelper::viewPs($preorder)?>">
                            <?= _t('site.preorder', 'Details')?>
                        </a>

                        <button loader-click="declinePreorder(<?= $preorder->id?>)" class="btn btn-warning">
                            <?= _t('site.preorder', 'Decline')?>
                        </button>
                    </div>
                </div>

                <div class="panel-body one-print-request__footer border-t">
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <strong><?= _t('site.preorder', 'Client')?></strong>: <?= H(UserFacade::getFormattedUserName($preorder->user)); ?>
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <strong><?= _t('site.preorder', 'Project Description')?></strong>: <?= H($preorder->description); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
