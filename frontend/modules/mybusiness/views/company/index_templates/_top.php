<?php

use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use frontend\widgets\CalculateCncButton;
use yii\bootstrap\Tabs;
use yii\helpers\Url;

/** @var int $step */

$user = UserFacade::getCurrentUser();
$ps = PsFacade::getPsByUserId($user->id);

$this->params['fluid-layout'] = true;

$tabsItems = [
    [
        'label' => _t('site.ps', 'My Services'),
        'active' => $step == 1,
        'url' => '/mybusiness/company',
        'visible' => $ps
    ],
    [
        'label' => _t('site.ps', 'My Sales'),
        'active' => $step == 2,
        'url' => '/my/company-order/print-requests',
        'visible' => $ps
    ],
    [
        'label' => _t('site.ps', 'My Business Tools'),
        'active' => $step == 4,
        'encode' => false,
        'url' => '/mybusiness/company/embed',
        'visible' => $ps
    ],
    [
        'label' => _t('site.ps', 'Model Store'),
        'active' => $step == 5,
        'url' => '/mybusiness/company/model-store',
        'visible' => $ps && $ps->is_designer
    ],
];

?>



<div class="over-nav-tabs-header"><div class="container">
    <h1 class=""><?=_t('site.ps', 'My Services Dashboard');?></h1></div>
</div>

<div class="nav-tabs__container">
    <div class="container">
        <?= Tabs::widget(['items' => $tabsItems])?>

        <?php if($step == 1) : ?>
            <div class="ps-3dprinter-actions">
                <a href="<?= \Yii::$app->urlManager->createUrl('/mybusiness/services/select-service-type'); ?>"
                   class="btn btn-primary add-printer m-b10">
                    <?= _t('site.ps', 'Add Service'); ?>
                </a>

                <?php if($ps->is_designer) : ?>

                    <a class="btn btn-primary  add-printer m-b10"
                       href="<?= Url::to('@web/upload')?>"
                       data-toggle="tooltip"
                       data-placement="bottom"
                       data-original-title="<?= _t('front.collection', 'Upload 3D Model')?>">
                        <span class="tsi tsi-plus"></span>
                        <?= _t('front.collection', 'Upload 3D Model')?>
                    </a>

                <?php endif; ?>

                <?= CalculateCncButton::upload(); ?>
            </div>
        <?php endif ?>

        <?php if($step == 5) : ?>
            <div class="ps-3dprinter-actions">
                <a class="btn btn-primary add-printer m-b10"
                   href="<?= Url::to('@web/upload')?>"
                   data-toggle="tooltip"
                   data-placement="bottom"
                   data-original-title="<?= _t('front.collection', 'Upload 3D Model')?>">
                   <span class="tsi tsi-plus"></span>
                   <?= _t('front.collection', 'Upload 3D Model')?>
                </a>
            </div>
        <?php endif ?>

    </div>
</div>