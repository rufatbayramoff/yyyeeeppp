<?php
use frontend\assets\CollectionAsset;
use frontend\assets\ImageCropAsset;

/** @var \yii\web\View $this */
/** @var array $items */

echo $this->renderFile('@app/views/ps/index_templates/_top.php', ['step' => 5]);


$this->params['page-class'] = 'js-collection-view';

Yii::$app->angular
    ->controller('profile/header')
    ->service(['notify', 'modal', 'router', 'user']);

$this->registerAssetBundle(ImageCropAsset::class);
$this->registerAssetBundle(CollectionAsset::class);
?>

<div class="container js-collection-list">
    <div class="row">

        <?= $this->render('/user/my/collection/store', ['items' => $items]);?>

    </div>

</div>

