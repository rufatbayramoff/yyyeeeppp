
<?php
$class = 'badge-success';
?>



<div class="panel-heading ps-panel-heading">
    <div class="col-md-3">
        <span class="badge <?php echo ($step == 1) ? $class : ''; ?>">1</span> <?= _t('site.ps', 'Print Service Details'); ?>
    </div>
    <div class="col-md-3">
        <span class="badge <?php echo ($step == 2) ? $class : ''; ?>">2</span> <?= _t('site.ps', 'Select Printer Model'); ?>
    </div>
    <div class="col-md-3">
        <span class="badge <?php echo ($step == 3) ? $class : ''; ?>">3</span> <?= _t('site.ps', 'Material Options'); ?>
    </div>
    <div class="col-md-3">
        <span class="badge <?php echo ($step == 4) ? $class : ''; ?>">4</span> <?= _t('site.ps', 'Delivery Details'); ?>
    </div>
</div>
