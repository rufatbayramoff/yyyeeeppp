<?php


use common\models\Company;
use frontend\assets\AngularSortableAssetLib;
use frontend\assets\DropzoneAsset;
use frontend\assets\ImageCropAsset;
use frontend\modules\mybusiness\models\CompanyProfileForm;
use frontend\modules\mybusiness\serializers\CompanyProfileSerializer;

/** @var Company $company */
/** @var CompanyProfileForm $form */
Yii::$app->angular
    ->service(['notify', 'router', 'user', 'modal'])
    ->controller('ps/ps/profile')
    ->directive(['dropzone-image-crop', 'dropzone-images', 'dropzone-image', 'image-rotate'])
    ->controllerParams([
        'ps' => CompanyProfileSerializer::serialize($company),
        'ownershipList' => $form->getOwnershipList(),
        'businessTypeList' => CompanyProfileForm::getBusinessTypeList(),
        'totalEmployeesList' => $form->getTotalEmployeesList(),
        'incoterms' => $form->getIncoterms(),
    ]);
ImageCropAsset::register($this);
DropzoneAsset::register($this);
AngularSortableAssetLib::register($this);
Yii::$app->angular->additionalModules(['angular-sortable-view']);
$this->title = _t('front.user', 'Public Profile Information');

?>
<div ng-controller="PsProfileController">

    <div class="row">

        <div class="col-md-12">

            <form class="form-horizontal ng-pristine ng-valid ng-valid-required ng-valid-pattern">

                <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label"><?php echo _t('site','Website')?></label>

                    <div class="col-sm-9 col-lg-5">
                        <input type="text" ng-model="ps.website"
                               class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                               placeholder="<?php echo _t('site','Your Website Link')?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label"><?php echo _t('site','Business Type')?></label>

                    <div class="col-sm-5 col-md-5">
                        <select id="businessType" ng-model="ps.area_of_activity"
                                ng-options="businessType for businessType in businessTypeList"
                                class="form-control ng-pristine ng-untouched ng-valid ng-not-empty">
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label"><?php echo _t('site','Ownership')?></label>

                    <div class="col-sm-5 col-md-5">
                        <select id="ownership" ng-model="ps.ownership"
                                ng-options="ownership for ownership in ownershipList"
                                class="form-control ng-pristine ng-untouched ng-valid ng-empty">
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label"><?php echo _t('site', 'Incoterms') ?></label>

                    <div class="col-sm-9 col-lg-7">
                        <select multiple="multiple" id="company-incoterms" ng-model="ps.incoterms"
                                ng-options="incoterm for incoterm in incotermsList"
                                class="form-control ng-pristine ng-untouched ng-valid ng-empty">
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label"><?php echo _t('site', 'Year established') ?></label>

                    <div class="col-sm-5 col-md-5">
                        <input ng-model="ps.year_established" pattern="[0-9]{4}" type="text"
                               class="form-control ng-pristine ng-untouched ng-valid ng-empty ng-valid-pattern"
                               placeholder="" autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label"><?php echo _t('site', 'Total employees') ?></label>

                    <div class="col-sm-5 col-md-5">
                        <select id="total_employees" ng-model="ps.total_employees"
                                ng-options="item for item in totalEmployeesList"
                                class="form-control ng-pristine ng-untouched ng-valid ng-empty">
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label"><?php echo _t('site', 'Annual turnover&nbsp;($)') ?></label>

                    <div class="col-sm-5 col-md-5">
                        <input ng-model="ps.annual_turnover" type="number"
                               class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                               placeholder="" autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label"><?php echo _t('site', 'Links') ?></label>

                    <div class="col-sm-9 col-lg-5">
                        <div class="ps-info-links">


                            <div class="ps-info-links__container collapse " id="collapseLinks">
                                <div class="m-b10">
                                    <div class="input-group">
                                        <span class="input-group-addon">Facebook</span>
                                        <input type="text" ng-model="ps.facebook"
                                               class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                               placeholder="<?php echo _t('site','Your Facebook Link')?>">
                                    </div>
                                </div>
                                <div class="m-b10">
                                    <div class="input-group">
                                        <span class="input-group-addon">Instagram</span>
                                        <input type="text" ng-model="ps.instagram"
                                               class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                               placeholder="<?php echo _t('site','Your Instagram Username')?>">
                                    </div>
                                </div>
                                <div class="m-b0">
                                    <div class="input-group">
                                        <span class="input-group-addon">Twitter</span>
                                        <input type="text" ng-model="ps.twitter"
                                               class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                               placeholder="<?php echo _t('site','Your Twitter Username')?>">
                                    </div>
                                </div>
                            </div>

                            <a href="#collapseLinks" class="ps-info-links__link m-t10 collapsed"
                               role="button" data-toggle="collapse" aria-expanded="false"
                               aria-controls="collapseLinks">
                                    <span><?php echo _t('site','Show/Hide more links')?></span>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-5">

                        <label class="control-label"><?php echo _t('site','Logo')?></label>

                        <div class="">
                            <div dropzone-image-crop="setLogoData(imageFile, circleCrop, squareCrop)"
                                 class="text-center ps-upload-photo ps-upload-photo--horizontal ng-isolate-scope"
                                 can-change-after-select="true">

                                <div id="psImagePreview" class="drop-photo-preview">
                                    <img dropzone-image-preview ng-src="{{ps.logoUrl}}"/>
                                </div>
                                <div dropzone-image-click id="dropYourPhoto">
                                    <div class="btn btn-link">
                                    <span class="drop-photo-icon">
                                        <span class="tsi tsi-upload-l"></span>
                                    </span>
                                        <?= _t('site.ps', 'Upload business logo'); ?>
                                    </div>
                                    <div class="drop-photo-hint">
                                        <?= _t('site.ps', 'For best results, please upload an image not less than 200x200 pixels.'); ?>
                                    </div>
                                </div>
                            </div>

                            <input type="file" class="inputfile" id="avatar-file"
                                   accept="image/jpeg,image/png,image/gif,video/mp4,video/3gpp,video/ogg,video/webm,application/x-mpegURL,video/MP2T"
                                   onchange="angular.element(this).scope().onAvatarFileChange(event)">
                        </div>

                    </div>
                    <div class="col-lg-7">

                        <label class="control-label"><?php echo _t('site','Profile banner')?></label>

                        <div class="">
                                <div dropzone-image="ps.coverFile"
                                     class="text-center ps-upload-photo ps-upload-photo--horizontal inspectlet-sensitive"
                                     can-change-after-select="true">
                                    <div class="inspectlet-sensitive drop-photo-preview">
                                        <img dropzone-image-preview ng-src="{{ps.coverUrl}}"/>
                                    </div>

                                    <div dropzone-image-click id="dropYourPhoto">
                                        <div class="btn btn-link">
                                                    <span class="drop-photo-icon">
                                                        <span class="tsi tsi-upload-l"></span>
                                                    </span>
                                            <?= _t('site.ps', 'Upload public profile banner')?>
                                        </div>
                                        <div class="drop-photo-hint">
                                            <?= _t('site.ps', 'For best results, please upload an image not less than 1600x300 pixels and less than 2 MB.'); ?>    </div>
                                    </div>
                                </div>
                        </div>

                    </div>
                </div>

                <div class="form-group col-lg-12">
                    <label class="control-label"><?= _t('site.ps', 'Business Portfolio / Production Process'); ?></label>

                    <div ng-if="!getIsNew()" class="ps-example-gallery p-t10">
                        <p><?= _t('site.ps', 'Upload up to 20 images or videos to showcase your products and services to your clients and gain an edge over your competitors.'); ?></p>

                        <button
                                dropzone-images="ps.pictures"
                                can-change-after-select="true"
                                class="btn btn-primary ps-example-gallery__add-btn"
                                title="<?= _t('site.ps', 'Upload'); ?>"
                                type="button"><span class="tsi tsi-plus m-r10"></span> <?= _t('site.ps', 'Upload'); ?>
                        </button>

                        <div ng-if="ps.pictures.length > 0">
                            <div class="sortable-container js-sortable-container" sv-root sv-part="ps.pictures">
                                <div ng-repeat="image in ps.pictures" image-rotate="image" sv-element="opts" class="sortable-grid-element">
                                    <div sv-handle class="sortable-grid-element__overlay"></div>
                                    <img class="sortable-grid-element-img" ng-src="{{image.fileUrl}}" data-image-rotate-flag="img" alt="">
                                    <button type="button" ng-click="deletePicture(image)" class="sortable-grid-del-btn"
                                            title="<?= _t('company.service', 'Delete'); ?>">
                                        &times;
                                    </button>
                                    <button ng-show="!image.isVideo" type="button" data-image-rotate-flag="button"
                                            class="sortable-grid-rotate-btn"
                                            title="<?= _t('company.service', 'Rotate'); ?>">
                                        <span class="tsi tsi-repeat"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="ps-steps__footer clearfix">


                <a href="/c/<?= HL($company->url); ?>" target="_blank"
                   class="btn btn-link">
                    <?= _t('site.ps', 'Public profile'); ?>
                </a>


                <button ng-click="save()" type="button"
                        class="btn btn-primary">
                    <?php echo _t('site','Save')?>
                </button>

            </div>

        </div>
    </div>


</div>


<script type="text/ng-template" id="/app/ps/ps/change-logo-modal.html">

    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upoad Business Logo</h4>
                </div>
                <div class="modal-body">
                    <div class="row avatar-preview ">
                        <div class="col-md-5 m-b20">
                            <img src="" id="circle-image" class="avatar-preview__crop-img">
                        </div>

                        <div class="col-md-6">
                            <h4 class="avatar-preview__title m-l20">List Logo Preview</h4>
                            <div class="avatar-preview__block m-l20">
                                <div style="width: 64px; height: 64px;"
                                     class="img-circle avatar-preview__img circle-image-preview"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row avatar-preview ">
                        <div class="col-md-5 m-b20">
                            <img src="" id="square-image" class="avatar-preview__crop-img">
                        </div>

                        <div class="col-md-6">

                            <h4 class="avatar-preview__title m-l20">Profile Header Preview</h4>

                            <div class="avatar-preview__block m-l20">
                                <div style="width: 240px; min-height:64px"
                                     class="avatar-preview__img square-image-preview"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button
                                    type="button"
                                    class="btn btn-primary"
                                    loader-click="save()">
                                Save logo
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>