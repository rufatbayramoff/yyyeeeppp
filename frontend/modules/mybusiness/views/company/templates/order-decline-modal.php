<script type="text/ng-template" id="/app/ps/orders/order-decline-modal.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t('front.user', 'Decline reason') ?></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" name="orderDeclineForm">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('app', 'Decline reason')?></label>
                            <div class="col-sm-8">
                                <select
                                    class="form-control"
                                    ng-options="reason.id as reason.title for reason in declineReasons"
                                    ng-model="form.reasonId">
                                    <option value=""><?= _t('site.ps', 'Select reason')?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('app', 'Comments'); ?></label>
                            <div class="col-sm-8">
                                <textarea
                                    ng-model="form.reasonDescription"
                                    class="form-control" placeholder=""></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button
                        ng-click="$dismiss()"
                        type="button" class="btn btn-default"><?=_t('site.ps', 'Cancel')?></button>
                    <button
                        loader-click="decline()"
                        loader-click-unrestored = "true"
                        type="button" class="btn btn-danger"><?=_t('site.ps', 'Decline')?></button>
                </div>
            </div>
        </div>
    </div>
</script>