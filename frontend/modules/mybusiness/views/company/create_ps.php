<?php

use common\models\factories\LocationFactory;
use frontend\modules\mybusiness\serializers\PsEditSerializer;
use frontend\widgets\UserLocationWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var frontend\models\ps\AddPsForm $model */
Yii::$app->angular
    ->service(['notify', 'router', 'user', 'modal', 'googleLocationApi'])
    ->controller('ps/ps/create')
    ->controllerParam('ps', PsEditSerializer::serialize($model));

$ps = $model;
$publicUrl = $model->url;
$this->title = _t('front.user', 'Main Information');
$companyFee = '2%';
?>


<div ng-controller="PsCreateController" ng-cloak>
    <?php if ($ps->getIsRejected()): ?>
        <div class="alert alert-warning alert--text-normal">
            <?php
            $rejectReasonStr = $ps->getLastRejectLog();
            echo _t('site.ps', 'The information provided about your company details wasn\'t approved by the moderator. ');
            echo '<br>';
            if ($rejectReasonStr) {
                echo _t('site.ps', 'Reason: ') . $rejectReasonStr . ' ';
            }

            echo Html::a(_t('site.ps', ' Please edit your company details'), ['/mybusiness/company/edit-ps']);
            ?>
        </div>
    <?php endif; ?>

    <div class="row">

        <div class="col-md-12">

            <div
                    ng-if="!getIsNew()"
                    class="row wide-padding">
            </div>

            <?php

            $form = ActiveForm::begin(['layout' => 'horizontal', 'id' => 'ps-form']); ?>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label"><?= _t('front.user', 'Business name') ?><span
                            class="form-required">*</span></label>

                <div class="col-sm-5 col-md-5">
                    <input
                            ng-model="ps.title"
                            required
                            type="text" class="form-control" placeholder="" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label"><?= _t('site.ps', 'Business description') ?><span
                            class="form-required">*</span></label>

                <div class="col-sm-9 col-lg-7">
                        <textarea
                                ng-model="ps.description"
                                class="form-control"
                                rows="5"
                                placeholder="<?= _t('site.ps', 'Business description cannot exceed 1500 characters. Please note that website & social media links, phone numbers and email addresses are not permitted in the business description.') ?>">
                        </textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label p-t5"><?= _t('front.user', 'Location (City)') ?><span
                            class="form-required">*</span></label>

                <div class="col-sm-5 col-md-5">
                    <div id="user-change-location-form">
                        <?php
                        echo UserLocationWidget::widget(
                            [
                                'btnTitle' => 'Location',
                                'location' => $ps->location ? LocationFactory::createFromUserLocation($ps->location) : null,
                                'jsCallback' => 'function(btn, result, modalId) {
                                     if (result) {
                                        var text = result.location.city+", "+result.location.country;
                                        $(".ts-user-location").html(text);
                                        $("body").trigger("changeLocation", result.location);
                                        return true;
                                     };
                                };'
                            ]
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="ps-steps__footer clearfix">

                <?php if (!$model->getIsNewRecord()) : ?>
                    <?php if ($model->isDeleted()): ?>
                        <?php echo Html::a(_t('site.ps', 'Restore Business'), ['restore-ps'], [
                            'data-confirm' => _t('site.ps', "Are you sure you want to restored Business?"),
                            'data-method' => 'post',
                            'class' => 'btn btn-primary'
                        ]) ?>
                    <?php else: ?>
                        <?php echo Html::a(_t('site.ps', 'Delete Business'), ['delete-ps'], [
                            'data-confirm' => _t('site.ps', "Are you sure you want to delete this Business?"),
                            'data-method' => 'delete',
                            'class' => 'btn btn-info btn-ghost'
                        ]) ?>
                    <?php endif; ?>

                    <a href="/c/<?= HL($publicUrl); ?>" target="_blank"
                       class="btn btn-link">
                        <?= _t('site.ps', 'Public profile'); ?>
                    </a>

                <?php endif; ?>

                <button
                        ng-click="savePs()"
                        ng-disabled="loaded"
                        type="button" class="btn btn-primary">
                    <?= _t('site.ps', 'Save') ?>
                </button>

            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>


</div>