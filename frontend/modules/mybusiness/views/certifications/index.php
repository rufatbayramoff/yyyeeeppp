<?php

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = _t('site.ps', 'Company Certifications');

?>


    <div class="m-b20">
        <?= Html::a('Add Certification', ['create'], ['class' => 'btn btn-success m-r10']) ?>
        <?= Html::a(_t('mybusiness.product', 'Assign to products'), ['assign-products'], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="row p-b10">
        <div class="table-responsive">
            <?= GridView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'columns'      => [
                        [
                            'attribute' => 'file_id',
                            'label'      => 'File',
                            'format' => 'raw',
                            'value' => function($cert){

                                if (\common\components\FileTypesHelper::isImage($cert->file)) {
                                    $downloadLink =  \frontend\components\image\ImageHtmlHelper::getClickableThumb($cert->file) ;
                                }else{
                                    $imgHtml = "<img width='85' src='/static/images/certificate.svg' style='margin-bottom: -5px;'/> ";
                                    $downloadLink =  \yii\helpers\Html::a($imgHtml, $cert->file->getFileUrl(), ['title'=>$cert->title]);
                                }
                                return  $downloadLink;
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'label'     => 'Certification'
                        ],
                        'certifier',
                        'application',
                        'issue_date:date',
                        'expire_date:date',
                        ['class' => 'yii\grid\ActionColumn', 'template'=>'{delete}'],
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered product__cert-table'
                    ],
                ]
            ); ?>

        </div>
    </div>