<?php

/* @var $certs \yii\data\ActiveDataProvider */
?>
<?php

/* @var $blockSearchModel \backend\models\search\CompanyCertificationSearch */
/* @var $this \yii\web\View */
/* @var $model \frontend\modules\mybusiness\models\CompanyCertificationBindForm */
/* @var $productSearchModel \backend\models\search\ProductSearch */
/* @var $products \yii\data\ActiveDataProvider */
?>
<?php


use common\components\ArrayHelper;
use common\models\Product;
use common\models\ProductCategory;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

$serviceCategoryList = \common\models\ProductCategory::getActiveSelectCategories();
?>

    <h3 class="m-t0"><?=_t('mybusiness.product', 'Assign company certifications');?></h3>
    <div class="row">
        <div class="table-responsive">
            <?php
            $columns = [
                'title',
                [
                    'attribute' => 'category_id',
                    'value'     => function (Product $p) {
                        if (!$p->category_id) {
                            return null;
                        }
                        return $p->category->title;
                    },
                    'filter'    => Html::activeDropDownList(
                        $productSearchModel,
                        'category_id',
                        $serviceCategoryList,
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                ],
                #   'company_id',
                //  'updated_at:datetime',
                // 'published_at',
                // 'is_active',
                // 'single_price',
            ];
            $blockModels = $certs->getModels();
            foreach ($blockModels as $blockModel) {
                $columns[] = [
                    'class'  => 'yii\grid\CheckboxColumn',
                    'name'   => 'CompanyCertificationBindForm[certsBind][' . $blockModel->id . ']',
                    'header' => sprintf('<div class="checkbox checkbox-primary m-t0 m-b0">%s <label for="selectall['.$blockModel->id.']">%s</label></div>',
                        ' <input type="checkbox" class="selectblock-all" value="'.$blockModel->id.'" 
                        name="selectall['.$blockModel->id.']" id="selectall['.$blockModel->id.']" />', $blockModel->title ),
                    'checkboxOptions' => function (Product $model, $key, $index, $column) use ($blockModel) {
                        $checked = false;
                        $bindBlocksIds = $model->getBindedCompanyCertificationsIds();
                        if (in_array($blockModel->id, $bindBlocksIds)) {
                            $checked = 'checked';
                        }
                        return ['value' => $model->uuid, 'class'=>'selectblockitem selectblock-'.$blockModel->id, 'checked'=>$checked];
                    }
                ];
            }
            ?>
            <?= GridView::widget(
                [
                    'dataProvider' => $products,
                    'filterModel'  => $productSearchModel,
                    'columns'      => $columns
                ]
            ); ?>
        </div>
        <?php $form = ActiveForm::begin(
            [
                'options' => ['enctype' => 'multipart/form-data'],
                'id' => 'block-bind-form',
                'action'  => '/mybusiness/certifications/bind-blocks'
            ]);
        $model->productUuids = implode(", ", ArrayHelper::getColumn($products->query->all(), 'uuid'));
        ; ?>
        <?= $form->field($model, 'productUuids')->hiddenInput()->label(false) ?>
        <div id="checkbox-elements" style="display:none;"></div>
        <div class="col-lg-12 p-b20" style="text-align:right;">
            <?= \yii\bootstrap\Html::submitButton(
                'Bind',
                [
                    'class' => 'btn btn-primary',
                ]
            ) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php
$this->registerJs("
    $('#block-bind-form').on('submit', function(){
        var names = [];
         $('input.selectblockitem:checked').each(function() {
            names[this.name] = names[this.name] || [];
			names[this.name].push(this.value);
			$('#checkbox-elements').append(this);
        });        
    });
    $('.selectblock-all').on('click', function(e, a){
        var blockId = e.currentTarget.value;
        $('.selectblock-' + blockId).removeAttr('checked');
        if(e.currentTarget.checked)
            $('.selectblock-' + blockId ).attr('checked', 'checked');
            
    });
");
