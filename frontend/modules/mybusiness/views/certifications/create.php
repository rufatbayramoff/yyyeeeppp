<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<div class="container">
    <?php

    $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data'] ]); ?>

    <div class="form-horizontal">

        <div class="form-group">
            <label class="col-sm-3 col-md-2 p-l0">
                <a href="/mybusiness/certifications" class="btn btn-sm btn-default"><span class="tsi tsi-left m-r10"></span><?= _t('mybusiness.certifications', 'Back') ?></a>
            </label>
            <div class="col-sm-6">
                <h3 class="m-t0 m-b0">
                    <?= _t('mybusiness.certifications', 'Add Certification') ?>
                </h3>
            </div>
        </div>

        <div class="form-group field-companycertificationform-title required">
            <label class="control-label col-sm-3 col-md-2" for="companycertificationform-title"><?= _t('mybusiness.certifications', 'Certification Title') ?> <span class="form-required">*</span></label>
            <div class="col-sm-6">
                <input type="text" id="companycertificationform-title" class="form-control" name="CompanyCertificationForm[title]" maxlength="45" aria-required="true">
                <p class="help-block help-block-error "></p>
            </div>

        </div>
        <div class="form-group field-companycertificationform-file">
            <label class="control-label col-sm-3 col-md-2" for="companycertificationform-file"><?= _t('mybusiness.certifications', 'Image/PDF') ?> <span class="form-required">*</span></label>
            <div class="col-sm-6 col-md-5">
                <input type="hidden" name="CompanyCertificationForm[file]" value="">
                <input type="file" id="companycertificationform-file" class="inputfile" name="CompanyCertificationForm[file]" value="" data-multiple-caption="{count} files selected" accept=".pdf, .jpg, .gif, .png">
                <label class="uploadlabel" for="companycertificationform-file">
                    <span>
                        <i class="tsi tsi-upload-l"></i>
                        <?= _t('mybusiness.certifications', 'Upload File') ?>
                    </span>
                </label>
                <p class="help-block help-block-error "></p>
            </div>

        </div>    <div class="form-group field-companycertificationform-certifier">
            <label class="control-label col-sm-3 col-md-2" for="companycertificationform-certifier"><?= _t('mybusiness.certifications', 'Certifier') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="text" id="companycertificationform-certifier" class="form-control" name="CompanyCertificationForm[certifier]" maxlength="45">
                <p class="help-block help-block-error "></p>
            </div>

        </div>
        <div class="form-group field-companycertificationform-application">
            <label class="control-label col-sm-3 col-md-2" for="companycertificationform-application"><?= _t('mybusiness.certifications', 'Application') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="text" id="companycertificationform-application" class="form-control" name="CompanyCertificationForm[application]" maxlength="45">
                <p class="help-block help-block-error "></p>
            </div>

        </div>
        <div class="form-group field-companycertificationform-issue_date">
            <label class="control-label col-sm-3 col-md-2" for="companycertificationform-issue_date"><?= _t('mybusiness.certifications', 'Issue Date') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="date" id="companycertificationform-issue_date" class="form-control" name="CompanyCertificationForm[issue_date]">
                <p class="help-block help-block-error "></p>
            </div>

        </div>

        <div class="form-group field-companycertificationform-expire_date">
            <label class="control-label col-sm-3 col-md-2" for="companycertificationform-expire_date"><?= _t('mybusiness.certifications', 'Expire Date') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="date" id="companycertificationform-expire_date" class="form-control" name="CompanyCertificationForm[expire_date]">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-companycertificationform-expire_date">
            <label class="control-label col-sm-3 col-md-2"></label>
            <div class="col-sm-6 col-md-5">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
