<?php

use frontend\modules\mybusiness\components\TaxUrlHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var \common\models\UserAddress $addressModel */
$this->title = _t('front.user', 'Tax Information Interview');

?>

<div class="container">

<div class="row wide-padding">
    <div class="col-xs-12">

        <h3 class="user-profile__title"><?php echo $this->title; ?></h3>

        <h3 class="tax-progress__title"><?= _t('front.user', 'Step 2 out of 4') ?></h3>

        <div class="tax-progress tax-progress--step2"></div>
        <p class="text-muted">
            <?php echo _t('site.tax', 'Please provide your tax information in English.'); ?>
        </p>
        <div class="panel panel-default box-shadow">
            <?php $form = ActiveForm::begin(); ?>
             <div class="panel-body">
                 <h4>2. Tax information</h4>
                 <div class="row">
                     <div class="col-sm-4">
                          <?php
                            echo $form->field($taxModel, 'classification')
                               ->dropDownList(
                                    common\models\UserTaxInfo::getNonUsaTaxClassifications(),
                                    ['prompt'=>'Select one', 'onChange'=>'TS.Tax.updateTypeNonUsa(this)']
                               )->label(_t('site.tax', 'Type of beneficial owner'));
                            ?>
                     </div>
                     <div id="tax_type"></div>
                     <div id="tax_type_user">
                        <div class="col-sm-4">
                           <?php
                           echo $form->field($taxModel, 'place_country')
                                  ->dropDownList(
                                       \common\models\GeoCountry::getCombo('iso_code'),
                                      ['prompt'=>'Select one', 'id'=>'tax_place_user']
                                  )->label(_t('site.tax', 'Country of citizenship'));
                           ?>
                        </div>
                        <div class="col-sm-4">
                           <?php echo $form->field($taxModel, 'name')->label(_t('site.tax', 'Full name')); ?>
                      
                        </div>
                         <div class="col-sm-4">
                             <?=
                                 $form->field($taxModel, 'dob_date')
                                     ->label(_t('site.tax', 'Date of birth'))
                                     ->widget(kartik\widgets\DatePicker::classname(), [
                                         'options' => ['placeholder' => _t('site.user', 'Enter birth date ...'), 'type'=>'date', 'autocomplete'=>'off'],
                                         'removeButton' => false,
                                         'pluginOptions' => [
                                             'autoclose'=>true,
                                             'format' => 'yyyy-mm-dd'
                                         ]
                                     ]);
                             ?>

                         </div>
                     </div>
                     <div id="tax_type_company" >
                        <div class="col-sm-4">
                           <?php
                               echo $form->field($taxModel, 'place_country')
                                  ->dropDownList(
                                       \common\models\GeoCountry::getCombo('iso_code'),
                                      ['prompt'=>'Select one', 'id'=>'tax_place_company']
                                  )->label(_t('site.tax', 'Place of organization'));
                           ?>
                        </div>
                        <div class="col-sm-4">
                           <?php echo $form->field($taxModel, 'business_name')->label(_t('site.tax', 'Organization')); ?>
                        </div>
                     </div>
                 </div>

                 <hr>

                 <h4>3. <?php echo _t('site.tax', 'Permanent address'); ?></h4>

                  <div class="row">
                        <div class="col-sm-4">
                            <?php  echo $form->field($addressModel, 'address')->label(_t('site.tax', 'Street address')); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php  echo $form->field($addressModel, 'extended_address')->label(_t('site.tax', 'Address 2')); ?>
                        </div>
                        <div class="col-sm-4">
                             <?php  echo $form->field($addressModel, 'city')->label(_t('site.tax', 'City or town')); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <?php
                            if(empty($addressModel->country_id)){
                                $addressModel->country_id = 233;
                            }
                            echo $form->field($addressModel, 'country_id')
                               ->dropDownList(
                                    \common\models\GeoCountry::getCombo(),
                                   ['prompt'=>'Select one']
                               )->label(_t('site.tax', 'Country'));
                            ?>

                        </div>
                        <div class="col-sm-4">
                            <?php echo $form->field($addressModel, 'region')->label(_t('site.tax', 'State/Region')); ?>
                        </div>

                        <div class="col-sm-4">
                            <?php  echo $form->field($addressModel, 'zip_code')->label(_t('site.tax', 'Postal code')); ?>
                        </div>
                    </div>

                 <span style="visibility:hidden"><input type="password"></span>

                 <h4>4. <?php echo _t("site.tax", "If your country of residence has issued you a tax identifying number for tax purposes, you may enter it here"); ?></h4>
                 <div class="row">
                     <div class="col-sm-4">
                         <?php
                         echo $form->field($taxModel, 'identification', [
                             'inputOptions' => [
                                 'autocomplete' => 'off',
                             ]])->label(_t('site.tax', 'Foreign tax identifying number (Optional only for Individuals)'))->passwordInput();
                         ?> <span class="glyphicon glyphicon-eye-open" style="right: 25px;position: absolute;top: 60px;cursor:pointer;"></span>
                         <br />
                         <div class="footnote">
                             <?php echo _t(
                                 'site.tax',
                                 'Treatstock uses cutting-edge cryptographic technologies to provide safe and secure storage of your sensitive information.'
                             ); ?>
                         </div>
                     </div>

                     <div class="col-sm-4"> </div>
                 </div>
             </div>

            <div class="panel-footer panel-footer--pager">
                <a class="btn btn-default" href="<?= TaxUrlHelper::taxesStep1()?>"><?php echo _t('site', 'Back'); ?></a>
                <?= Html::submitButton(_t('site.tax', 'Next'), ['class' => 'btn btn-primary btn-pager--next']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>

</div>


<script>
/* global TS */
<?php $this->beginBlock('js_1', false); ?>
   TS.Tax = TS.Tax || {};
   var taxUser, taxCompany;

    $(".glyphicon-eye-open").mousedown(function(){
        $("#usertaxinfo-identification").attr('type','text');
    }).mouseup(function(){
        $("#usertaxinfo-identification").attr('type','password');
    }).mouseout(function(){
        $("#usertaxinfo-identification").attr('type','password');
    });

   TS.Tax.updateTypeNonUsa = function(el){
       var val = $(el).val();
       if(val === 'Individual'){
            $("#tax_type").html(taxUser);
       }else{
            $("#tax_type").html(taxCompany);
       }
   };
   taxUser = $("#tax_type_user").remove();
   taxCompany = $("#tax_type_company").remove();
   <?php if($taxModel->classification=='Individual'): ?>        
        $("#tax_type").html(taxUser);
    <?php else: ?>        
        $("#tax_type").html(taxCompany);
    <?php endif; ?>
<?php $this->endBlock(); ?>
</script>

<?php
$this->registerJs($this->blocks['js_1']);