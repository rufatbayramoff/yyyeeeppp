<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = _t('front.user', 'Tax Information Interview');

?>

<div class="container">

<div class="row wide-padding">

    <div class="col-xs-12">

        <h3 class="user-profile__title"><?php echo $this->title; ?></h3>

        <h3 class="tax-progress__title"><?= _t('front.user', 'Step 1 out of 4') ?></h3>

        <div class="tax-progress tax-progress--step1"></div>

        <div class="user-my-taxes">

            <!-- Tax Information Interview -->

            <div class="panel panel-default box-shadow">
                <?php $form = ActiveForm::begin(); ?>

                <div class="panel-body">
                    <p>
                        <?php echo _t("site.tax", "This short tax interview will guide you through a step-by-step process to submit required U.S. tax information to Treatstock."); ?>
                    </p>
                    <hr>
                    <h4><?php echo _t("site.tax", "1. For U.S. tax purposes, are you a U.S. person?"); ?> *</h4>

                     <?php

                        echo $form->field($taxModel, 'is_usa', [
                            'enableLabel' => false,
                        ])->radioList(['1'=> _t('site.tax', 'Yes'), '0'=> _t('site.tax', 'No')], [
                            'item' => function($index, $label, $name, $checked, $value) {
                                $lid = 'txlb'.$index;
                                $checkedVal = $checked ? 'checked="checked"' : '';
                                $return = '<div class="radio"><input type="radio" id="'.$lid.'" '.$checkedVal.' name="' . $name . '" value="' . $value . '" tabindex="3">';
                                $return .= '<label class="modal-radio" for="'.$lid.'">';
                                $return .= '<span> ' . ucwords($label) . '</span>';
                                $return .= '</label></div>';
                                return $return;
                            }
                        ]);
                        ?>

                    <div class="footnote">
                        * <?php echo _t("site.tax", "In general, you are considered a U.S. person if you are a (1) U.S. citizen, (2) U.S. resident, or (3) entity organized under the laws of the U.S."); ?><br />
                        <?php echo _t("site.tax", "All companies that operate within the jurisdiction of the United States, under U.S. Tax Law, are required to collect tax information and provide it to the Internal Revenue Service (IRS). In certain circumstances, those companies are required, by law, to withhold a percentage of royalties from designers that are not residents or citizens of the United States."); ?>
                    </div>
                </div>

                <div class="panel-footer panel-footer--pager">
                    <?= Html::submitButton(_t('site.tax', 'Next'), ['class' => 'btn btn-primary btn-pager--next']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>

</div>