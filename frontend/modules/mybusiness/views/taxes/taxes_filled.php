<?php

use common\models\UserAddress;
use frontend\modules\mybusiness\components\TaxUrlHelper;
use yii\helpers\Html;
use common\models\UserTaxInfo;

$taxModel->sign_date = $taxModel->getTaxSignDate();

$this->title = _t('front.user', 'Tax Information Interview Complete');
$this->params['breadcrumbs'][] = ['label' => _t('front.user', 'Private profile'), 'url' => ['/user-profile/']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="container">

<div class="row wide-padding">

    <div class="col-xs-12">

        <h2 class="m-t0 m-b20"><?php echo $this->title; ?></h2>

        <p>
            <?php
            if($taxModel->status == UserTaxInfo::STATUS_CONFIRMED){
                echo Html::tag("span", _t("site.tax", "Your tax information is confirmed"), ['class'=>'text-success']);
            }else if($taxModel->status == UserTaxInfo::STATUS_DECLINE){
                $msg = UserTaxInfo::getDeclineReason($taxModel);
                echo Html::tag("p", _t("site.tax", "Your tax information was declined: ") .  $msg,  
                    ['class'=>'text-danger']
                 ); 
            }else if($taxModel->status == UserTaxInfo::STATUS_SUBMITTED){
                echo Html::tag("span", _t("site.tax", "Tax form {type} has been submitted", [
                    'type' => $taxModel->getFormType(true),
                    'status'=>$taxModel->status]));
            }
            ?>
        </p>

       
        <p>

            <?= _t("site.tax", "You are responsible for the accurate and complete declaration of your taxable income in your country of residence, and to the IRS if required. 
You can find information about the rules and regulations of the IRS here:");
?>
            <a href="https://www.irs.gov/pub/irs-pdf/p901.pdf" target="_blank">www.irs.gov</a>


        </p>

        <p>
            <?php
            $attributes = [
                'created_at:datetime',
                'updated_at:datetime',
                [
                    'attribute' => 'address',
                    'format' => 'raw',
                    'value' => $taxModel->address?UserAddress::formatAddress($taxModel->address, true):'',
                ],
                'classification',
                'status',
                'name'
            ];
            if ($taxModel->classification != 'Individual') {
                $attributes[] = 'business_name';
            }
            if (empty($taxModel->is_usa)) {
                $attributes[] = 'place_country';
            } else {
                $attributes[] = 'identification_type';
                $attributes[] = 'identification';
            }
            if ($taxModel->sign_type == 'form') {
                $attributes[] = 'sign_date';
                $attributes[] = 'signature';
            }

            ?>
            <?= Html::a(_t('site.tax', 'Reset tax information'), TaxUrlHelper::taxesEditStep1(), ['class' => 'btn btn-warning']); ?>
        </p>

    </div>
</div>

</div>