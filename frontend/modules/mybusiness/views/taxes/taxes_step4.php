<?php

use frontend\modules\mybusiness\components\TaxUrlHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$taxModel->sign_date = $taxModel->getTaxSignDate();

$this->title = _t('front.user', 'Tax Information Interview');

?>

<div class="container">

<div class="row wide-padding">
    <div class="col-xs-12">

        <h3 class="user-profile__title"><?php echo $this->title; ?></h3>

        <h3 class="tax-progress__title"><?= _t('front.user', 'Step 4 out of 4') ?></h3>

        <div class="tax-progress tax-progress--step4"></div>

        <div class="user-my-taxes">
             <?php $form = ActiveForm::begin(['layout'=>'horizontal']); ?>
            <div class="panel panel-default box-shadow">
                <div class="panel-body">
                    <h3 class="m-t0"><?php echo _t("site.tax", "Consent to electronic signature"); ?></h3>

                    <p><?php echo _t("site.tax", "In order to electronically sign your tax identity document, it is necessary to obtain your consent. If you do not provide your consent, at the end of the interview you will be required to print the form, sign it with a blue or black pen, and mail it to the address that is provided."); ?></p>

                     <?php
                        $list = [
                            'form' => _t('site.tax', 'I consent to provide my electronic signature'),
                            'email' => _t('site.tax', 'No, I will mail the documents to you') . ' (' ._t('site.tax', 'Sorry, this option is temporarily unavailable') .')'
                        ];
                        echo $form->field($taxModel, 'sign_type', [
                            'enableLabel' => false,
                        ])->radioList($list, [
                            'item' => function($index, $label, $name, $checked, $value) {
                                $lid = 'txlb'.$index;
                                $checkedVal = $checked ? 'checked="checked"' : '';
                                $disabled  = '';
                                if($index==1)
                                    $disabled = 'disabled="disabled"';
                                $return = '<div class="radio"><input type="radio" id="'.$lid.'" '.$checkedVal.' name="' . $name . '" value="' . $value . '" '.$disabled.' onchange="TS.Tax.updateSignType(this)">';
                                $return .= '<label class="modal-radio" for="'.$lid.'">';
                                $return .= '<span> ' . $label . '</span>';
                                $return .= '</label></div>';
                                return $return;
                            }
                        ]);
                        ?>
                    <?php if(false): // hide ?>
                    <div class="pdf-dowывnload" style="display:none;">
                         <iframe name="iFramePdf" src="" data-src="old" style="display:none;" onload="if(this.getAttribute('data-src')!=='old'){
                            this.focus(); this.contentWindow.print();
                        } ; this.setAttribute('data-src', 'next');"></iframe>
                          <a href="<?php echo TaxUrlHelper::pdf()?>" target="iFramePdf" class="btn btn-link">
                              <span class="tsi tsi-printer"></span> <?php echo _t('site', 'Print Form'); ?> <?php echo $taxModel->getFormType(true); ?></a>

                    </div> <?php endif; ?>
                    <hr>

                    <div class="e-signature e-signature--disabled" >
                        <h3><?php echo _t('site.tax', 'Electronic signature'); ?></h3>
                        <p><?php echo _t("site.tax", "Under penalties of perjury, I declare that I have examined the information on Form and to the best of my knowledge and belief it is true, correct, and complete.", ['form'=>$taxModel->getFormType(true)]); ?></p>

                        <?php echo $form->field($taxModel, 'signature')->label(
                                    _t('site.tax',
                                     'Signature of beneficial owner (or individual authorized to sign on behalf of beneficial owner)'))
                                    ->textInput(['readonly' => true, 'placeholder' => 'Type your name']); ?>

                        <?php


                        echo $form->field($taxModel, 'sign_date')->label(
                                    _t('site.tax',
                                     'Date (mm-dd-yyyy)'))->textInput(['readonly' => true]); ?>
                    </div>
                </div>
                <div class="panel-footer panel-footer--pager">
                    <a class="btn btn-default" href="<?= TaxUrlHelper::taxesStep3()?>"><?php echo _t("site.common", "Back"); ?></a>
                    <?= Html::submitButton(_t('site.tax', 'Finish'), ['class' => 'btn btn-primary btn-pager--next']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>

</div>


<script>
/* global TS */
<?php $this->beginBlock('js_1', false); ?>
   TS.Tax = TS.Tax || {};
   var esignHtml;
   TS.Tax.updateSignType = function(el){
       var val = $(el).val(); 
       if(val==='form'){
           $('.e-signature').removeClass('e-signature--disabled');
           $('.pdf-download').hide();
           $('.e-signature').html(esignHtml);
           $('#usertaxinfo-signature').removeAttr('readonly');
       }else if(val==='email'){
           $('.e-signature').addClass('e-signature--disabled');
           $('.pdf-download').show();
           $('.e-signature').html('');
       }
   };

   esignHtml = $('.e-signature').html();
   <?php if(!empty($taxModel->sign_type) && $taxModel->sign_type=='email'): ?>
         $('.e-signature').addClass('e-signature--disabled');
           $('.pdf-download').show();
           $('.e-signature').html('');
   <?php else : ?> 
            $('.e-signature').removeClass('e-signature--disabled');
           $('.pdf-download').hide();
           $('.e-signature').html(esignHtml);
           <?php if(!empty($taxModel->sign_type)): ?>               
               $('#usertaxinfo-signature').removeAttr('readonly');
            <?php endif; ?>
    <?php endif; ?>
   
   
<?php $this->endBlock(); ?>
</script>

<?php
$this->registerJs($this->blocks['js_1']);