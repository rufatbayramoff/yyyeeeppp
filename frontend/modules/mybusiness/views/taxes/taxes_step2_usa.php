<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var \common\models\UserTaxInfo $taxModel */
/** @var \common\models\UserAddress $addressModel */


$this->title = _t('front.user', 'Tax Information Interview');
//$this->title = _t('site.tax', 'US Tax Identification');
#$this->params['breadcrumbs'][] = ['label' => _t('front.user', 'Private profile'), 'url' => ['/user-profile/']];
#$this->params['breadcrumbs'][] = $this->title;
//$taxModel = new common\models\UserTaxInfo();
// $addressModel = new \common\models\UserAddress();

if(empty($taxModel->identification_type)){
    $taxModel->identification_type = 'ein';
}
?>

<div class="container">

<div class="row wide-padding">
    <div class="col-xs-12">

        <h3 class="user-profile__title"><?php echo $this->title; ?></h3>

        <h3 class="tax-progress__title"><?= _t('front.user', 'Step 2 out of 4') ?></h3>

        <div class="tax-progress tax-progress--step2"></div>

        <div class="user-my-taxes">
            <!-- Tax Information Interview -->
            <div class="panel panel-default box-shadow">
                <?php $form = ActiveForm::begin([
                    'options'=>['autocomplete'=>'off'],
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => false,
                ]); ?>
                <div class="panel-body">
                    <h4>2. <?php echo _t("front.user", "Tax information"); ?></h4>
                    <div class="row">
                        <div class="col-sm-4">
                            <?php
                            echo $form->field($taxModel, 'classification')
                               ->dropDownList(
                                    common\models\UserTaxInfo::getTaxClassifications(),
                                   ['prompt'=>'Select one', 'onChange'=>'TS.Tax.updateType(this)']
                               )->label(_t('site.tax', 'Federal Tax Classification'));
                            ?>

                        </div>

                        <div class="col-sm-4">
                           <?php echo $form->field($taxModel, 'name')->label(_t('site.tax', 'Full name')); ?></div>

                        <div class="col-sm-4"> <?php  echo $form->field($taxModel, 'business_name'); ?> </div>
                        <div class="col-sm-4">
                            <div id="tax_type_user">
                                <?=
                                    $form->field($taxModel, 'dob_date')
                                        ->label(_t('site.tax', 'Date of birth'))
                                        ->widget(kartik\widgets\DatePicker::classname(), [
                                            'options' => ['placeholder' => _t('site.user', 'Enter birth date ...')],
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                                'startView' => 2,
                                                'autoclose'=>true,
                                                'format' => 'yyyy-mm-dd',
                                                'endDate' => date('Y-m-d', strtotime('-17 years'))
                                            ]
                                        ]);
                                ?>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <h4>3. <?php echo _t('site.tax', 'Address'); ?></h4>

                    <div class="row">
                        <div class="col-sm-4">
                            <?php  echo $form->field($addressModel, 'address')->label(_t('site.tax', 'Street address')); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php  echo $form->field($addressModel, 'extended_address')->label(_t('site.tax', 'Address 2')); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php  echo $form->field($addressModel, 'city')->label(_t('site.tax', 'City or town')); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <?php
                            if(empty($addressModel->country_id)){
                                $addressModel->country_id = 233;
                            }
                            echo $form->field($addressModel, 'country_id')
                               ->dropDownList(
                                    \common\models\GeoCountry::getCombo(),
                                   ['prompt'=>'Select one']
                               )->label(_t('site.tax', 'Country'));
                            ?>

                        </div>
                        <div class="col-sm-4">
                            <?php echo $form->field($addressModel, 'region')->label(_t('site.tax', 'State/Region')); ?>
                        </div>

                        <div class="col-sm-4">
                        
                            <?php  echo $form->field($addressModel, 'zip_code', [
                                    'inputOptions' => [
                                        'autocomplete' => 'off',
                                    ]])->label(_t('site.tax', 'Postal code')); ?>
                        </div>
                    </div>

                    <h4>4. <?php echo _t("site.tax", "US Tax Identification"); ?></h4>
                    <div class="row">
                        <div class="col-sm-4">

                                <?php
                                    echo $form->field($taxModel, 'identification_type', [
                                        'enableLabel' => false,
                                    ])->radioList(['ssn'=> _t('site.tax', 'SSN or ITIN'), 'ein'=> _t('site.tax', 'EIN')], [
                                        'id'=>'taxIdType',
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            $lid = 'taxIdType'.$index;
                                            $checkedVal = $checked ? 'checked="checked"' : '';
                                            $return = '<div class="radio"><input type="radio" id="'.$lid.'" '.$checkedVal.' name="' . $name . '" value="' . $value . '" tabindex="3">';
                                            $return .= '<label class="modal-radio" for="'.$lid.'">';
                                            $return .= '<span> ' . ucwords($label) . '</span>';
                                            $return .= '</label></div>';
                                            return $return;
                                        }
                                    ]);
                                ?>

                                <div id="taxIdEin" class="pull-right">
                                    <?php
                                    $taxModel->identification_type = 'ein';
                                    echo $form->field($taxModel, 'identification_type')->hiddenInput()->label(false); ?>
                                    <div class="tax-ein"><?php echo _t('site.tax', 'EIN'); ?></div>
                                </div>
                        </div>
                        <div class="col-sm-4">
                            <?php
                            echo $form->field($taxModel, 'identification', [
                                    'inputOptions' => [
                                        'autocomplete' => 'off',
                                    ]])->label(_t('site.tax', 'Identification number'))->passwordInput();
                             ?>
                            <span class="glyphicon glyphicon-eye-open" style="right: 25px;position: absolute;top: 40px;cursor:pointer;"></span>

                        </div>

                        <div class="col-sm-4"> </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
<p class="text-primary"><?php echo _t("site.tax", "We are dedicated to providing the best in cyber security of your personal information. Treatstock uses cutting-edge cryptographic technologies to provide safe and secure storage of your sensitive information and is never stored in your Treatstock account."); ?></p>
                        </div>
                    </div>
                    <span style="visibility:hidden"><input type="password"></span>

                </div>

                <div class="panel-footer panel-footer--pager">
                    <a class="btn btn-default" href="<?= \frontend\modules\mybusiness\components\TaxUrlHelper::taxesStep1()?>"><?php echo _t('site.common', 'Back'); ?></a>
                    <?= Html::submitButton(_t('site.common', 'Next'), ['class' => 'btn btn-primary btn-pager--next']) ?>
                </div>



                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>

</div>

    <script>
        /* global TS */
        <?php $this->beginBlock('js_1', false); ?>
        TS.Tax = TS.Tax || {};
        var einEl, idType ;

        $(".glyphicon-eye-open").mousedown(function(){
            $("#usertaxinfo-identification").attr('type','text');
        }).mouseup(function(){
            $("#usertaxinfo-identification").attr('type','password');
        }).mouseout(function(){
            $("#usertaxinfo-identification").attr('type','password');
        });
        TS.Tax.updateType = function(el){
            var val = $(el).val();
            TS.Tax.updateTypeByText(val);
        };
        TS.Tax.updateTypeByText = function(val){
            if(val==='Individual'){
                $("#taxIdType").html(idType);
                $("#taxIdEin").html('');
                $('#tax_type_user').show();
            }else{
                $("#taxIdEin").html(einEl);
                $('#taxIdType').html('');
                $("#taxIdType1").attr('checked', true);
                $('#tax_type_user').hide();
            }
        };
        einEl = $("#taxIdEin").html();
        idType = $("#taxIdType").html();
        <?php if(!empty($taxModel->classification)): ?>
        TS.Tax.updateTypeByText('<?=H($taxModel->classification);?>');
        <?php endif; ?>
        <?php if($taxModel->classification=='Individual'): ?>
        $("#taxIdEin").html('');
        <?php else: ?>
        $("#taxIdType").html('');
        <?php endif; ?>

        <?php $this->endBlock(); ?>
    </script>

<?php
$this->registerJs($this->blocks['js_1']);