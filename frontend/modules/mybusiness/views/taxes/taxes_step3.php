<?php

use frontend\modules\mybusiness\components\TaxUrlHelper;
use yii\helpers\Html;

$this->title = _t('front.user', 'Tax Information Interview');


?>

<div class="container">

<div class="row wide-padding">
    <div class="col-xs-12">

        <h3 class="user-profile__title"><?php echo $this->title; ?></h3>

        <h3 class="tax-progress__title"><?= _t('front.user', 'Step 3 out of 4') ?></h3>

        <div class="tax-progress tax-progress--step3"></div>

        <div class="user-my-taxes">
             <div class="panel panel-default box-shadow">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-9">
                            <h4 class="m-t0"><?php echo _t('site.tax', 'Please check that the information you have entered is correct. Part I of the tax form is blank to protect your personal information.'); ?></h4>
                        </div>
                        <iframe name="iFramePdf" src="" data-src="old" style="display:none;" onload="if(this.getAttribute('data-src')!=='old'){
                            this.focus(); this.contentWindow.print();
                        };this.setAttribute('data-src', 'next');"></iframe>
                        <div class="col-sm-3 text-right m-b20">
                            <a href="<?= TaxUrlHelper::pdf();
                            ?>" target="iFramePdf" class="btn btn-default"><span class="tsi tsi-printer"></span> <?php echo _t('site.common', 'Print'); ?></a>

                        </div>
                    </div>

                    <div class="form-w9">
                        <iframe src="<?= TaxUrlHelper::preview()?>" style="width:100%;height:500px;">
                               <?php echo Html::a('Preview form', TaxUrlHelper::preview()); ?>
                        </iframe>
                    </div>
                </div>

                <div class="panel-footer panel-footer--pager">
                    <a class="btn btn-default" href="<?= TaxUrlHelper::taxesStep2()?>"><?php echo _t('site.common', 'Back'); ?></a>
                    <a class="btn btn-primary btn-pager--next" href="<?= TaxUrlHelper::taxesStep4()?>"><?php echo _t("site.common", "Next"); ?></a>
                </div>
            </div>
        </div>

    </div>
</div>

</div>