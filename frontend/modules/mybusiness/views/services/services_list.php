<?php
/**
 * User: nabi
 */
/* @var $ps \common\models\Ps|\common\modules\catalogPs\repositories\BaseAR|\frontend\models\ps\AddPsForm|null|\yii\db\ActiveRecord|static */
/* @var $this \yii\web\View */
/* @var $services CompanyService[] */

use common\models\CompanyService;
use common\components\ArrayHelper;
use \yii\helpers\Html;
?>

<?php foreach ($services as $service) { ?>
    <?=$this->render('types/cs_'.$service->type, ['service'=>$service, 'ps'=>$ps]); ?>
<?php } ?>
