<?php
/** @var \common\models\CompanyService $service */
$serviceTitle = $service->getTypeLabel();
if (!empty($service->title)) {
    $serviceTitle = $service->title;
}
$categoryTitle = $service->category?$service->category->title:'';
if(empty($categoryTitle)){
    $categoryTitle = '';
}
?>
<div class="my-ps-printers__item col-xs-12 col-sm-6 col-md-4 p-b30">
    <div class="my-ps-printers__card m-b0">

        <?php if($service->isRejected()): ?>
            <div class="my-ps-printers__block m-t5 m-b0">
                <b><?=_t('mybusiness.services', 'Rejected'); ?>:</b>
                <?= \common\models\ModerLog::getLastRejectModerLog(\common\models\ModerLog::TYPE_SERVICE, $service->id); ?>
            </div>
        <?php endif; ?>

        <div class="my-ps-printers__row border-0 p-t10">

            <span class="my-ps-printers__tech-label label label-info" title="<?= _t('site.ps', 'Window manufacturing'); ?>">
                <?= _t('site.ps', 'Window manufacturing'); ?>
            </span>
            <h3 class="my-ps-printers__heading my-ps-printers__heading--printer">
                <?= H($serviceTitle); ?>
            </h3>
            <div class="clearfix"></div>
            <div class="my-ps-printers__block m-t5 m-b5">
                <strong><?= _t('site.ps', 'Moderation'); ?>:</strong> <?= $service->getModeratorStatusLabel() ?>
            </div>

            <a class="my-ps-printers__edit" href="/mybusiness/company-services/company-service/edit?id=<?= $service->id ?>">
                <span class="tsi tsi-pencil"></span><?= _t('site.ps', 'Edit'); ?>
            </a>
        </div>

        <div class="my-ps-printers__row p-t0 border-b0 border-t">
            <div class="row">
                <div class="col-sm-12">
                    <a href="<?=$service->getPublicPageUrl();?>" target="_blank" class="btn btn-default btn-sm m-t10 m-b10 pull-left"><?=_t('mybusiness.services', 'Preview'); ?></a>
                    <a class="btn btn-default btn-sm m-t10 m-b10 pull-right" data-confirm="<?= _t('yii', 'Are you sure you want to delete this item?') ?>"
                       data-method='post'
                       href="/mybusiness/company-services/company-service/delete?serviceId=<?= $service->id ?>">
                        <span class="tsi tsi-bin m-r10"></span><?= _t('site.ps', 'Delete'); ?>
                    </a>
                </div>
            </div>
        </div>

        <div class="my-ps-printers__row border-b0 p-t5">

            <div class="my-ps-printers__block m-b5">
                <a class="my-ps-printers__edit my-ps-printers__edit--printer m-r15 m-b10" href="/mybusiness/cs-window/manage">
                    <span class="tsi tsi-pencil"></span><?= _t('site.ps', 'Edit Quoting Details'); ?>
                </a>
                <a class="my-ps-printers__edit my-ps-printers__edit--printer m-r15 m-b10" href="/mybusiness/cs-window/manage/quotes">
                    <span class="tsi tsi-list-3"></span><?= _t('site.ps', 'Quote list'); ?>
                </a>
                <a class="my-ps-printers__edit my-ps-printers__edit--printer m-b10" href="/mybusiness/widgets/printers?windowService=1">
                    <span class="tsi tsi-link"></span><?= _t('site.ps', 'Widget code'); ?>
                </a>
            </div>

        </div>

    </div>
</div>