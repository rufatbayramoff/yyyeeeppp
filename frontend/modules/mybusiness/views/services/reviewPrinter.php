<?php
/**
 * User: nabi
 */

use common\models\ModerLog;
use common\models\PrinterReview;

/**
 * @var $printer \common\models\Printer
 * @var $printerReview PrinterReview
 * @var $psPrinter \common\models\PsPrinter
 */

$form = $printerReview->getReviewData();
Yii::$app->angular
    ->service(['notify', 'user', 'router', 'modal'])
    ->resource('PsPrinter')
    ->controller('ps/printers/printerReview')
    ->directive(['star-input'])
    ->controllerParams(
        [
            'printerTitle' => $printer->title,
            'psPrinterId'  => $psPrinter->id,
            'form'         => $form
        ]
    );
$printerReviewCols = $printerReview->getReviewLabels();
?>
<div class="container">

    <div class="row">
        <div class="col-md-8">
            <?php
            if (in_array($printerReview->status, [PrinterReview::STATUS_REJECTED])) { ?>
                <div class="bar bg-warning m-b20">
                    <b><?= _t('mybusiness.services', 'Rejected'); ?>. </b>
                    <?= \common\models\ModerLog::getLastRejectModerLog(\common\models\ModerLog::TYPE_PRINT_REVIEW, $printerReview->id); ?>
                </div>
            <?php } ?>

            <h2 class="m-t0"><?= _t('site.printer', 'Review your 3D printer'); ?></h2>

            <p>
                <?= _t(
                    'site.printer',
                    'As an owner and operator of the {printerTitle}, who better to leave a review and help other members of the 3D printing community when deciding which 3D printer they should buy or which 3D printer they should select to produce their order?',
                    ['printerTitle' => '<b>' . $printer->title . "</b>"]
                ); ?>
            </p>
            <p>
                <?= _t(
                    'site.printer',
                    'Your feedback will be published in our {printerLink} for your particular 3D printer, which can generate leads to your profile page, attract new customers and increase your sales.',
                    ['printerLink' => '<a href="https://www.treatstock.com/machines/3d-printers/">3D Printer\'s Guide</a>']
                ); ?>
            </p>
        </div>
    </div>

    <div ng-controller="PrinterReview">
        <form class="form-horizontal" name="orderReviewForm">

            <h3><?= _t('site.printer', 'How would you rate the {printerTitle} for:', ['printerTitle' => $printer->title]); ?></h3>

            <div class="row">
                <div class="col-md-6">
                    <?php $i = 0;
                    foreach ($printerReviewCols as $code => $title): $i++; ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label m-b0 p-r0"><?= mb_strtoupper($title) ?></label>
                            <div class="col-sm-8">
                                <input star-input="form.<?= $code; ?>" type="number" class="star-rating" data-size="sm" step="1" data-symbol="&#xea4b;" data-glyphicon="false"
                                       data-rating-class="tsi">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <?php
                        if ($i === 5) {
                            echo "</div><div class='col-md-6'>";
                        }
                    endforeach; ?>
                </div>

                <div class="col-md-12">
                    <h3><?= _t('site.printer', 'Please leave your general comments or feedback'); ?>:</h3>
                    <div class="form-group">
                        <div class="col-md-8">
                            <textarea ng-model="form.comments" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </div>
            </div>

        </form>
        <div class="row">
            <div class="col-md-8">
                <button loader-click="save()" loader-click-unrestored="true" type="button" class="btn btn-danger"><?= _t('site.ps', 'Submit') ?></button>
            </div>
        </div>
        <br/>
        <br/>
    </div>

</div>