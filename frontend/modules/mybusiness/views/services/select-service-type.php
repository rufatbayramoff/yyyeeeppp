<?php

/** @var \yii\web\View $this */

use yii\helpers\Url;

/** @var \common\models\Ps|null $ps */
/** @var bool $fromCreatePs */


Yii::$app->angular
    ->service(['maps', 'geo', 'notify', 'router', 'user'])
    ->directive(['google-map', 'google-address-input'])
    ->resource(['Printer', 'PsPrinter'])
    ->controller('ps/printers/select-service-type');

?>
<div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 ps-steps">
                <h2 class="ps-steps__title"><?= _t('front.user', 'Select a service'); ?></h2>
                <div class="row select-cnc">

                    <div class="col-sm-4 col-md-3">
                        <a href='/mybusiness/services/<?= $ps ? 'add-printer' : 'create-ps?type=printer' ?>' class="select-cnc__link">
                            <img src="/static/images/main-page-icons/mp-3d.jpg" class="select-cnc__img">
                            <h4 class="select-cnc__title"><?=_t('front.user', '3D Printing'); ?></h4>
                        </a>
                    </div>

                    <div class="col-sm-4 col-md-3">
                        <a href='/mybusiness/services/<?= $ps ? 'add-cnc' : 'create-ps?type=cnc' ?>' class="select-cnc__link">
                            <img src="/static/images/main-page-icons/mp-cnc.jpg" class="select-cnc__img">
                            <h4 class="select-cnc__title"><?=_t('front.user', 'CNC Machining'); ?></h4>
                        </a>
                    </div>

                    <?php if(!$ps || !$ps->is_designer) :

                        $designerUrl = !$ps || $ps->getIsNewRecord()
                            ? ['/mybusiness/company/create-ps', 'type'=> 'designing']
                            : ['/mybusiness/company/edit-ps', 'type'=> 'designing'];

                        $designerUrl = Url::to($designerUrl);
                        ?>

                        <div class="col-sm-4 col-md-3">
                            <a href="<?= $designerUrl ?>" class="select-cnc__link">
                                <img src="/static/images/main-page-icons/mp-cad.jpg" class="select-cnc__img">
                                <h4 class="select-cnc__title"><?=_t('front.user', 'Designing'); ?></h4>
                            </a>
                        </div>

                    <?php endif; ?>


                </div>
            </div>
        </div>
    </div>
</div>
