<?php

use common\models\DeliveryType;
use common\models\GeoCountry;
use frontend\models\ps\PsFacade;
use lib\geo\GeoNames;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var \common\models\PsMachine $psMachine */

$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);

$carriersDomestic = $psMachine->asDeliveryParams()->getCarriers('domestic');
$carriersIntl = $psMachine->asDeliveryParams()->getCarriers('intl');

$isCnc = $psMachine->isCnc();

Yii::$app->angular
    ->service(['currency', 'maps', 'geo', 'notify', 'user', 'router'])
    ->directive(['google-map', 'google-address-input'])
    ->resource(['PsMachine'])
    ->controller('ps/printers/edit-delivery-options')
    ->controllerParams([
        'country'           => $psMachine->location->country ?? null,
        'deliveryCountries' => ArrayHelper::index($psMachine->psMachineDeliveryCountry,'geo_country_id')
    ])
    ->constants(
        [
            'deliveryTypes'    => ArrayHelper::toArray(
                PsFacade::getDeliveryTypes(),
                [
                    DeliveryType::class => [
                        'id',
                        'code',
                        'title',
                        'is_free_delivery' => function (DeliveryType $deliveryType) {
                            return (bool)$deliveryType->is_free_delivery;
                        },
                        'is_carrier'       => function (DeliveryType $deliveryType) {
                            return (bool)$deliveryType->is_carrier;
                        },
                        'is_work_time'     => function (DeliveryType $deliveryType) {
                            return (bool)$deliveryType->is_work_time;
                        }
                    ]
                ]
            ),
            'countries'        => ArrayHelper::toArray(
                GeoNames::getAllCountries(),
                [GeoCountry::class => ['id', 'iso_code', 'title', 'is_easypost_domestic', 'is_easypost_intl','is_europa']]
            ),
            'defaultCarrier'   => $psMachine->asDeliveryParams()->getDefaultCarrier(),
            'carriersDomestic' => $carriersDomestic,
            'carriersIntl'     => $carriersIntl
        ]
    );
?>

<div ng-controller="PsMachineEditDeliveryOptionsController" class="edit-ps-printer" ng-cloak>
    <div class="row">
        <div class="col-md-7 wide-padding--right">
            <div class="row">
                <div class="col-md-6">
                    <label class="delivery-mini-header">
                        <?= _t("site.ps", "Country"); ?>
                    </label>
                    <div class="form-group">
                        <select
                                ng-model="psMachine.location.country_id"
                                ng-change="onLocationFormChange()"
                                ng-options="country.id as country.title for country in $geo.getCountries()"
                                class="form-control">
                        </select>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="delivery-mini-header">
                        <?= _t("site.ps", "State/Region"); ?>
                    </label>
                    <div class="form-group">
                        <input
                                ng-model="psMachine.location.region"
                                ng-change="onLocationFormChange()"
                                type="text" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="delivery-mini-header">
                        <?= _t("site.ps", "City/Town"); ?>
                    </label>
                    <div class="form-group">
                        <input
                                ng-model="psMachine.location.city"
                                ng-change="onLocationFormChange()"
                                type="text" class="form-control" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="delivery-mini-header">
                        <?= _t("site.ps", "Address 1 (building, street)"); ?>
                    </label>
                    <div class="form-group">
                        <input
                                ng-model="psMachine.location.address"
                                ng-change="onLocationFormChange()"
                                type="text" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="delivery-mini-header">
                        <?= _t("site.ps", "Address 2 (floor, office)"); ?>
                    </label>
                    <div class="form-group">
                        <input
                                ng-model="psMachine.location.address2"
                                ng-change="onLocationFormChange()"
                                type="text" class="form-control" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="delivery-mini-header">
                        <?= _t("site.ps", "ZIP code or postal code"); ?>
                    </label>
                    <div class="form-group">
                        <input
                                ng-model="psMachine.location.zip_code"
                                ng-change="onLocationFormChange()"
                                type="text" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-6">

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div ng-if="approximateAddresses" class="m-t20">

                        <div class="m-b10"><b><?= _t('site.ps', 'This address is incorrect. Did you mean') ?></b>:</div>

                        <div ng-repeat="address in approximateAddresses" style="padding: 10px; margin-bottom: 10px; border-bottom: 1px dotted #aaa">

                            <div class="pull-left">{{$geo.stringifyAddress(address)}}</div>
                            <div class="pull-right">
                                <button
                                        ng-click="selectApproximateAddress(address)"
                                        type="button" class="btn btn-xs btn-default"><?= _t('site.ps', 'Select') ?></button>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>

                    <button
                            ng-click="showOnMap()"
                            type="button" class="btn btn-default m-t20 m-b20"><?= _t("site.ps", "Show on map"); ?></button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div
                            google-map="psMachine.location"
                            on-marker-position-change="onMapMarkerPositionChange(address)"
                            id="map"
                            style="height: 300px; margin-bottom: 15px;border-radius: 5px;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="delivery-mini-header">
                        <?= _t("site.ps", "Search on map"); ?>
                    </label>
                    <div class="form-group">

                        <input
                                id="autocomplete"
                                google-address-input=""
                                on-address-change="onAutocompleteAddressChange(address)"
                                type="text" class="form-control"/>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-5" ng-if="isLoaded">

            <label class="delivery-options-right-header">
                <?= _t("site.ps", "Delivery Options"); ?>
            </label>

            <div class="right-delivery-block">

                <div class="right-delivery-block__item"
                     ng-repeat="deliveryType in allowedDeliveryTypes"
                     id="delivery-type-{{deliveryType.code}}">

                    <div class="checkbox">

                        <input
                                ng-model="$$allowedDeliveryTypes[deliveryType.id]"
                                ng-change="onDeliveryTypeEnabledChange(deliveryType, $$allowedDeliveryTypes[deliveryType.id])"
                                type="checkbox" autocomplete="off"/>

                        <label>{{deliveryType.title}}</label>

                        <div
                                ng-if="$$allowedDeliveryTypes[deliveryType.id]"
                                class="delivery-option-field">

                            <input
                                    ng-if="deliveryType.is_work_time"
                                    ng-model="psMachine.getDeliveryTypeById(deliveryType.id).comment"
                                    type="text" class="form-control delivery-option-input"
                                    placeholder="<?= _t('site.ps', 'Working hours'); ?>" autocomplete="off"/>

                            <div ng-if="deliveryType.is_carrier">

                                <div class="delivery-option-label"
                                     ng-show="psMachine.type!='cnc'"
                                >
                                    <?= _t("site.ps", "Ship everywhere") ?>
                                    <?= frontend\widgets\SiteHelpWidget::widget(['title' => 'Shipping options', 'alias' => 'help.print.shippingoptions']); ?>
                                </div>

                                <select
                                        ng-show="psMachine.type!='cnc'"
                                        ng-model="psMachine.getDeliveryTypeById(deliveryType.id).carrier"
                                        ng-change="onCarrierChange(psMachine.getDeliveryTypeById(deliveryType.id))"
                                        ng-options="carrier.id as carrier.title for carrier in getCarriers(deliveryType) | filter:{is_active:true}"
                                        class="form-control delivery-option-input">
                                </select>
                                <div
                                        ng-if="psMachine.getDeliveryTypeById(deliveryType.id).carrier == 'myself'"
                                        class="delivery-option-input"
                                >
                                    <div class="input-group">
                                        <span class="input-group-addon">{{$currency.getByIso(psMachine.currency_iso).title_original}}</span>

                                        <input
                                                type="number"
                                                step="any"
                                                min="0"
                                                ng-model="psMachine.getDeliveryTypeById(deliveryType.id).carrier_price"
                                                class="form-control" autocomplete="off"
                                                placeholder="<?= _t("site.ps", "The shipping cost"); ?>">
                                    </div>
                                </div>
                            </div>

                            <div ng-if="isNeedDeliveryEuropa(deliveryType)">
                                <div class="m-t15 m-b5 m-l0">
                                    <?php echo _t("site.ps", "Shipping rates per country") ?>
                                </div>
                                <div class="input-group">
                                    <input type="number" min="0" ng-model="allEuropaPrice" class="form-control" placeholder="<?php echo _t("site.ps", "One price for all Europe countries") ?>">
                                    <span class="input-group-btn">
                                        <button type="button" ng-click="allEuropaDelivery(allEuropaPrice)" class="btn btn-default"><?php echo _t("site.ps", "Set") ?></button>
                                    </span>
                                </div>
                                <div class="delivery-option-row" ng-repeat="country in getCountryEuropa() track by $index">
                                    <span class="delivery-option-row__left" for="country-{{country.id}}">{{country.title}}</span>
                                    <div class="delivery-option-row__right">
                                        <div class="input-group">
                                            <span class="input-group-addon">{{$currency.getByIso(psMachine.currency_iso).title_original}}</span>
                                            <input  type="number" step="any" min="0" ng-model="countries[$index].price" ng-change="changeEuropaPrice()" class="form-control input-sm" id="country-{{country.id}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php if ($isCnc): ?>
                                <div ng-if="deliveryType.is_free_delivery" style="padding: 10px; color:darkred;">
                                    <?= _t('site.cnc', 'Shipping fees to be determined in your quote for an order.'); ?>
                                </div>
                            <?php endif; ?>
                            <div
                                    ng-if="deliveryType.is_free_delivery"
                                    class="right-free-delivery">

                                <input
                                        ng-model="psMachine.getDeliveryTypeById(deliveryType.id).$$requireFreeDelivery"
                                        ng-change="onChangeFreeDeliveryTypeRequired(psMachine.getDeliveryTypeById(deliveryType.id))"
                                        type="checkbox" autocomplete="off" class="free_delivery_cb">

                                <label>
                                    <?= _t("site.ps", "Free delivery for orders starting at") ?> {{$currency.getByIso(psMachine.currency_iso).title_original}}
                                    <?php if ($isCnc): ?>
                                        <?= frontend\widgets\SiteHelpWidget::widget(['title' => 'Free delivery', 'alias' => 'cnc.delivery.freedelivery']); ?>
                                    <?php else: ?>
                                        <?= frontend\widgets\SiteHelpWidget::widget(['title' => 'Free delivery', 'alias' => 'ps.delivery.freedelivery']); ?>
                                    <?php endif; ?>
                                    <br>
                                    (<?= _t("site.ps", "including packaging fee")?>)
                                </label>

                                <div class="input-group delivery-option-input">
                                    <span class="input-group-addon">{{$currency.getByIso(psMachine.currency_iso).title_original}}</span>
                                    <input
                                            type="number"
                                            step="any"
                                            min="0"
                                            ng-disabled="!psMachine.getDeliveryTypeById(deliveryType.id).$$requireFreeDelivery"
                                            ng-model="psMachine.getDeliveryTypeById(deliveryType.id).free_delivery"
                                            class="form-control" autocomplete="off" placeholder="<?= _t("site.ps", "Free delivery for orders starting at"); ?>">
                                </div>
                            </div>


                            <div
                                    ng-if="isShowPackingPriceInput(deliveryType)"
                                    class="right-free-delivery">

                                <input
                                        ng-model="psMachine.getDeliveryTypeById(deliveryType.id).$$requirePackagingFee"
                                        ng-change="onChangePackingFeeRequired(psMachine.getDeliveryTypeById(deliveryType.id))"
                                        type="checkbox" autocomplete="off" class="free_delivery_cb">

                                <label>
                                    <?= _t("site.ps", "Packaging fee") ?> {{$currency.getByIso(psMachine.currency_iso).title_original}}
                                    <?= frontend\widgets\SiteHelpWidget::widget(['title' => 'Packaging fee', 'alias' => 'ps.delivery.packagingfee']); ?>
                                </label>

                                <div class="input-group delivery-option-input">
                                    <span class="input-group-addon">{{$currency.getByIso(psMachine.currency_iso).title_original}}</span>
                                    <input
                                            type="number"
                                            step="any"
                                            min="0"
                                            ng-disabled="!psMachine.getDeliveryTypeById(deliveryType.id).$$requirePackagingFee"
                                            ng-model="psMachine.getDeliveryTypeById(deliveryType.id).packing_price"
                                            class="form-control" autocomplete="off" placeholder="<?= _t("site.ps", "Packaging fee"); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button ng-click="deliveryUpdate()" type="button" ng-disabled="isLoadedDeliveryAll" class="btn btn-primary btn-ghost m-b20">
                <?= _t('site.ps', 'Save delivery for all printers')?>
            </button>
        </div>
    </div>
    <script type="text/ng-template" id="/app/ps/printers/validate-address-modal.html">
        <div class="modal fade modal-warning" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?= _t('site.ps', 'Confirm address') ?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?= _t('site.delivery', "The address you have specified doesn't seem to be correct. We found the address:") ?></p>
                        <p ng-bind-html="address | locationBody"></p>
                        <p><?= _t('site.delivery', "Would you like to use the suggested address?") ?></p>
                    </div>
                    <div class="modal-footer">
                        <button
                                ng-click="$dismiss()"
                                type="button" class="btn btn-default"><?= _t('site.ps', 'Cancel') ?></button>
                        <button
                                ng-click="confirmAddress()"
                                type="button" class="btn btn-primary"><?= _t('site.ps', 'Ok') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/ng-template" id="/app/ps/printers/delivery-address-failed-modal.html">
        <div class="modal fade modal-danger" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?= _t('site.ps', 'Address confirmation failed') ?></h4>
                    </div>
                    <div class="modal-body">
                        <p>{{errorMessage}}</p>
                    </div>
                    <div class="modal-footer">
                        <button ng-click="$dismiss()" type="button" class="btn btn-default"><?= _t('site.ps', 'Ok') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </script>
</div>
