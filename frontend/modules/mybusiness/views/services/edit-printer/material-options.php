<?php

/** @var \yii\web\View $this */

Yii::$app->angular
    ->service(['measure', 'notify', 'currency', 'modal', 'user', 'router', 'mathUtils'])
    ->resource(['Printer', 'PsPrinter'])
    ->controller(['ps/printers/edit-material-options', 'ps/printers/order-example']);
?>

<div class="row" ng-controller="PsPrinterEditMaterialsController" ng-cloak>

    <div class="col-md-9 wide-padding--right" ng-if="isLoaded">
        <div class="row ps-material-options-row ps-material-options-head-row">

            <div class="col-md-5 p-l0 hidden-xs hidden-sm">
                <?= _t("site.ps", "Material"); ?>
            </div>
            <div class="col-md-1 p-l0 hidden-xs hidden-sm">
                <?= _t("site.ps", "Color"); ?>
            </div>
            <div class="col-md-6 p-r0 p-l0 price_settings">
                <?= _t("site.ps", "Price per"); ?>
                <select
                    ng-model="psPrinter.measure"
                    ng-options="measure.measure as measure.title for measure in avaliableMeasures"
                    class="form-control price_measure">
                </select>
            </div>
        </div>


        <div>
            <div class="row m-b10">
                <div class="col-md-5"></div>
                <div class="col-md-7">
            <?= frontend\widgets\SiteHelpWidget::widget([
                    'triggerHtml' => _t('site.ps', 'How do we calculate quotes for clients?'),
                    'title'=> _t('site.ps', 'How do we calculate quotes for clients?'),
                    'alias'=> 'ps.add-printer.chargeClients'
                ]
            ); ?>
                </div></div>


            <div class="row ps-material-options-row" ng-repeat="psMaterial in psPrinter.materials" id="material-{{psMaterial.material_id}}">

                <div class="col-md-5 materialsBlock">


                    <div class="row">
                        <div class="col-xs-12 col-md-11">
                            <select
                                class="form-control"
                                ng-model = "psMaterial.material_id"
                                ng-options="material.id as material.title for material in getSelectableMaterials(psMaterial)"
                                ng-change="changeMaterialId(psMaterial)"
                                >
                            </select>
                            <br>
                            <button
                                ng-click="deleteMaterial(psMaterial)"
                                type="button" class="btn btn-sm btn-danger btn-ghost" title="<?= _t("site.ps", "Delete Material"); ?>">
                                <span class="tsi tsi-bin"></span> <?= _t("site.ps", "Delete Material"); ?>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="col-md-7 colorsBlock">
                    <div class="row oneColorRow"
                         ng-repeat="psColor in psMaterial.colors"
                         ng-class="{'is-calculated' : orderExamples.needShowOrderExample(psColor)}"
                         id="material-{{psMaterial.material_id}}-color-{{psColor.color_id}}">

                        <div class="col-md-6">
                            <div class="color-one-block">

                                <div ng-if="!psColor.color_id">
                                    <select
                                        ng-model="psColor.color_id"
                                        ng-options="color.id as color.title for color in getSelectableColors(psMaterial, psColor)"
                                        class="form-control added-color-select ">
                                        <option value=""><?= _t("site.ps", "Choose Color"); ?></option>
                                    </select>
                                </div>

                                <div ng-if="psColor.color_id">
                                    <div class="form-control color-selected" ng-style="{ backgroundColor: 'rgb(' + getColor(psMaterial.material_id, psColor.color_id).rgb + ')'}"></div>
                                    <div class="color-one-block__color-title">{{getColor(psMaterial.material_id, psColor.color_id).title}}</div>
                                </div>

                            </div>
                        </div>


                        <div class="col-md-6 oneColorRow__price">
                            <div class="oneColorRow__weight-currency">
                                {{$currency.getByIso(psPrinter.currency_iso).title_original}}
                            </div>
                            
                            <input
                                ng-model="psColor.price"
                                type="number" step="0.01"
                                required
                                min="0.01"
                                ng-click="orderExamples.showOrderExampleTrigger($event, psColor)"
                                class="form-control ps-color-price-input" placeholder="<?= _t("site.ps", "Input Price"); ?>">
                            <div class="oneColorRow__weight-measure">
                                / {{$measure.getByMetric(psPrinter.measure).title}}
                            </div>
                            <button
                                ng-click="deleteColor(psMaterial, psColor)"
                                type="button" class="btn btn-primary btn-circle" title="<?= _t("site.ps", "Delete Color"); ?>">
                                <span class="tsi tsi-bin"></span>
                            </button>
                        </div>

                        <div class="oneColorRow-calculations-bridge"></div>

                        <div class="oneColorRow-calculations" ng-controller="OrderExampleController" ng-if="orderExamples.needShowOrderExample(psColor)">

                            <h4 class="oneColorRow-calculations__title"><?= _t('site.ps', 'Order Calculation')?></h4>

                            <p><?= _t('site.ps', 'Use our order calculator to determine your expected income based on the price you have set for the selected material. Observe the size, weight, and volume of the model to ensure your rate generates a satisfactory income.')?></p>

                            <img class="oneColorRow-calculations__model-pic" ng-src="{{testModel.renderUrl}}" style="width: 100%">

                            <p>
                                <a href="https://www.treatstock.com/static/tests/mug.stl" class="btn btn-link btn-xs p-l0">
                                    <span class="tsi tsi-download-l m-r10"></span><?=_t('site.ps', 'Download model'); ?>
                                    <span style="font-weight: normal;">(13.5 MB)</span>
                                </a>
                            </p>

                            <div class="row">
                                <div class="col-xs-6 ng-binding oneColorRow-calculations__color">
                                    <b class="m-r10"><?=_t('site.ps', 'Color'); ?>:</b>
                                    <div title="Biodegradable and flexible plastic" class="material-item">
                                        <div class="material-item__color" ng-style="{ backgroundColor: 'rgb(' + color.rgb + ')'}" style="background-color: rgb(0, 0, 255);"></div>
                                        <div class="material-item__label">{{color.title}}</div>
                                    </div>
                                </div>
                                <div class="col-xs-6 ng-binding oneColorRow-calculations__material"><b><?=_t('site.ps', 'Material'); ?>:</b> {{material.filament_title}}</div>
                            </div>

                            <div class="ng-binding">
                                <h4 class="m-b0"><b><?=_t('site.ps', 'Model properties'); ?>:</b></h4>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <?=_t('site.ps', 'Size'); ?>: <br>
                                        X = {{getSizeDimenValue(testModel.size.width) | number:2}} {{$measureType.getMeasureSystemTypeTitle(printerMeasure, $measureType.MEASURE_TYPE_LENGTH)}}<br>
                                        Y = {{getSizeDimenValue(testModel.size.height) | number:2}} {{$measureType.getMeasureSystemTypeTitle(printerMeasure, $measureType.MEASURE_TYPE_LENGTH)}}<br>
                                        Z = {{getSizeDimenValue(testModel.size.length) | number:2}} {{$measureType.getMeasureSystemTypeTitle(printerMeasure, $measureType.MEASURE_TYPE_LENGTH)}}<br>
                                    </div>
                                    <div class="col-xs-6">
                                        <?=_t('site.ps', 'Weight'); ?>: <br>
                                        {{getWeight() | number:2}} {{$measureType.getMeasureSystemTypeTitle(printerMeasure, $measureType.MEASURE_TYPE_WEIGHT)}}<br>
                                        <?=_t('site.ps', 'Volume'); ?>: <br>
                                        {{testModel.filamentVolume | number:2}} <?=_t('site.ps','ml')?><br>
                                    </div>
                                </div>
                            </div>

                            <h4 class="oneColorRow-calculations__price">
                                <b class="ng-binding"><?=_t('site.ps', 'You will receive'); ?>: {{$currency.getByIso(psPrinter.currency_iso).title_original}} {{getCost() | number:2}}</b>
                                <span ng-if="getIsCostIncludeMinOrderPrice()"><br/><?= _t('site.ps', 'This calculation does not include your minimum order charge')?></span>
                            </h4>
                            <div>
                                <?= \yii\helpers\Html::a(_t('site.ps', 'How much should I charge to make prints?'), ['/help/article', 'id' => 40], ['target' => '_blank'])?>
                            </div>
                        </div>
                    </div>

                    <button
                        ng-disabled="!canAddColor(psMaterial)"
                        ng-click="addColor(psMaterial)"
                        type="button" class="btn btn-default btn-sm btn-ghost add-color-button"><span class="tsi tsi-plus"></span>
                        <?= _t("site.ps", "Add New Color"); ?>
                    </button> &nbsp; 
                    <button
                            ng-hide="psMaterial.colors.length==1"
                            ng-click="openSetPriceModal(psMaterial)"
                            type="button" class="btn btn-success btn-sm add-color-button">
                        <span class="tsi tsi-cash"></span> <?= _t('site.ps', 'Set price for all'); ?>
                    </button>

                    <div class="alert alert-info alert--text-normal m-t20" role="alert">
                        <?= _t('site.ps', 'Please add your material colors in accordance with a color sample rather than its title.'); ?>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-7">
                <button
                    ng-disabled="!canAddMaterial()"
                    ng-click="addMaterial()"
                    type="button" class="btn btn-primary m-b30 m-r10">
                    <span class="tsi tsi-plus"></span> <?= _t("site.ps", "Add New Material"); ?>
                </button>

                <button
                    ng-click="openSetPriceModal()"
                    type="button" class="btn btn-success m-b30">
                    <span class="tsi tsi-cash"></span> <?= _t('site.ps', 'Set price for all'); ?>
                </button>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-5">
                <a class="add-new-color" href="/site/contact" target="_blank"><?=_t('site.ps', 'Material or color not on the list? Contact us')?></a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9 additional-price-div m-b20">
                <div class="additional-price-div__label">
                    <label for="additionalWorkCost"><?= _t("site.ps", "Minimum order charge:"); ?></label>
                </div>

                <div class="additional-price-div__help">
                    <?= frontend\widgets\SiteHelpWidget::widget(['title'=>'Minimum Order Charge', 'alias'=>'ps.add-printer.mater-opt.min-charge-for-print']); ?>
                </div>

                <div class="input-group">
                    <span class="input-group-addon">{{$currency.getByIso(psPrinter.currency_iso).title_original}}</span>
                    <input
                        ng-disabled="!psPrinter.$$minOrderPriceEnabled"
                        ng-model="psPrinter.min_order_price"
                        type="number" step="any"
                        class="form-control" autocomplete="off">
                </div>


            </div>
        </div>

    </div>

    <div class="col-md-3">
    </div>

</div>

<script type="text/ng-template" id="/app/ps/printers/set-price-modal.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t('site.ps', 'Set price for all added materials and colors')?></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" name="setPriceForm">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?= _t('site.ps', 'Price')?>:</label>
                            <div class="col-sm-10">
                                <input
                                    ng-model="price"
                                    required
                                    step="0.01"
                                    min="0.01"
                                    type="number" class="form-control">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button
                        ng-click="$dismiss()"
                        type="button" class="btn btn-default"><?=_t('site.ps', 'Cancel')?></button>
                    <button
                        ng-click="applyPrice()"
                        ng-disabled="setPriceForm.$invalid"
                        type="button" class="btn btn-primary"><?=_t('site.ps', 'Ok')?></button>
                </div>
            </div>
        </div>
    </div>
</script>











