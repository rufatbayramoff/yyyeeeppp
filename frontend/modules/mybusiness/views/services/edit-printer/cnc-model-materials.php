<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.07.17
 * Time: 17:38
 */

/** @var \common\models\PsCncMachine $psCncMachine */

/** @var array $cncSchema */

use beowulfenator\JsonEditor\JsonEditorAsset;
use beowulfenator\JsonEditor\JsonEditorWidget;
use frontend\assets\JsonEditorAssetJsTemplate;

JsonEditorAssetJsTemplate::register($this);

?>
<div id="psCncJsonContainerMaterials" class="form-group psCncJsonContainer" psCncJsonContainer="true">
    <input
            id='psCncJsonMaterials'
            name="psCncJsonMaterials"
            ng-model="psCnc.jsonMaterials"
            type="text"
            class="form-control hidden"
            placeholder="<?= _t('site.ps', 'Cnc machine description in JSON format.') ?>"
            autocomplete="off"
            value=''
    >
</div>
<script>

</script>
<?php

?>




