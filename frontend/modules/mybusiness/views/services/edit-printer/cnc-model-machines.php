<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.07.17
 * Time: 17:38
 */

/** @var \common\models\PsCncMachine $psCncMachine */

/** @var \common\modules\cnc\models\PsCnc $psCnc */

use beowulfenator\JsonEditor\JsonEditorAsset;
use beowulfenator\JsonEditor\JsonEditorWidget;
use frontend\assets\JsonEditorAssetJsTemplate;

?>

<div class="tab-content">
    <div class="tab-pane active">

        <div class="row">

            <div ng-if="!isLoaded" class="ng-scope">

                <div class="table-responsive">
                    <table class="table cnc-machine-edit-table">
                        <tr>
                            <th>
                                <?= _t('site.ps', 'Machine Title'); ?>
                            </th>
                            <th>
                                <?= _t('site.ps', 'Machine Type'); ?>
                            </th>
                            <th></th>
                        </tr>
                        <tr ng-repeat="machineInfo in psCnc.shortMachinesList">
                            <td>
                                <input ng-model="machineInfo.title" type="text" class="form-control" placeholder="<?= _t('site.ps', 'Machine Title'); ?>">
                            </td>
                            <td>
                                <select ng-model="machineInfo.type" class="form-control">
                                    <option disabled>
                                        <?= _t('site.ps', 'Machine Type'); ?>
                                    </option>
                                    <option value="milling">
                                        <?= _t('site.ps', 'Milling'); ?>
                                    </option>
                                    <option value="cutting">
                                        <?= _t('site.ps', 'Cutting'); ?>
                                    </option>
                                </select>
                            </td>
                            <td>
                                <button class="btn btn-default" title="<?= _t('site.ps', 'Move Up'); ?>" ng-click="upMachine(machineInfo)"  ng-class='{hidden:$first}'>
                                    <span class="tsi tsi-up"></span>
                                </button>
                                <button class="btn btn-default" title="<?= _t('site.ps', 'Move Down'); ?>" ng-click="downMachine(machineInfo)" ng-class='{hidden:$last}'>
                                    <span class="tsi tsi-down"></span>
                                </button>
                                <button class="btn btn-default" title="<?= _t('site.ps', 'Delete Machine'); ?>" ng-click="deleteMachine(machineInfo)">
                                    <span class="tsi tsi-bin"></span>
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-sm-12">
                    <button class="btn btn-primary btn-ghost m-b20" ng-click='addMachine()'>
                        <span class="tsi tsi-plus m-r10"></span>
                        <?= _t('site.ps', 'Add Machine');?>
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>
