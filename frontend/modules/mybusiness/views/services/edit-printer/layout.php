<?php
/** @var \yii\web\View $this */
/** @var \common\models\PsPrinter $printer */
/** @var string $content */

$printer = $this->params['printer'];

use yii\helpers\Html;

$this->beginContent('@frontend/modules/mybusiness/views/layouts/mybusinessLayout.php');
$printerPs = \common\models\Printer::findByPk($printer->printer_id);
Yii::$app->angular
    ->service(['router', 'modal', 'measure', 'notify', 'currency', 'modal', 'user', 'router', 'mathUtils'])
    ->resource(['PsPrinter', 'Printer', 'PsMachine'])
    ->controller('ps/printers/edit')
    ->controllerParams([
        'psPrinter' => \frontend\modules\mybusiness\controllers\EditPrinterController::jsonPsPrinter($printer),
        'psMachine' => \frontend\modules\mybusiness\controllers\EditPrinterController::jsonPsMachine($printer->psMachine),
        'action' => $this->context->action->id,
        'printer' =>  $printerPs ? \frontend\modules\mybusiness\controllers\EditPrinterController::jsonPrinter($printerPs) : null
    ]);
?>



<div class="container">

    <div class="row" ng-controller="PsPrinterEditController">

        <div class="col-md-12 ps-steps">

            <h2 class="ps-steps__title">
                <?= H($printer->title) ?> <?=_t('front.user', 'Details'); ?>
            </h2>

            <?=
            \yii\widgets\Menu::widget([
                'options' => ['class' => 'nav nav-tabs nav-tabs--secondary', 'id' => 'tabs'],
                'items' => [
                    [
                        'label' =>  _t('site.ps', 'Printer'),
                        'url' => ['/mybusiness/edit-printer/edit-printer', 'ps_id' => $printer->ps->id, 'ps_printer_id' => $printer->id],
                    ],
                    [
                        'label' => _t('site.ps', 'Material Options'),
                        'url' => ['/mybusiness/edit-printer/edit-material-options', 'ps_id' => $printer->ps->id, 'ps_printer_id' => $printer->id],
                    ],
                    [
                        'label' =>  _t('site.ps', 'Delivery Details'),
                        'url' => ['/mybusiness/edit-printer/edit-delivery-options', 'ps_id' => $printer->ps->id, 'ps_printer_id' => $printer->id],
                    ],
                    [
                        'label' =>  _t('site.ps', 'Certification'),
                        'url' => ['/mybusiness/edit-printer/test', 'ps_id' => $printer->ps->id, 'ps_printer_id' => $printer->id],
                    ]
                ]
            ]);
            ?>

            <div id="my-tab-content" class="tab-content">
                <div class="tab-pane active">
                    <?= $content ?>
                </div>
            </div>

            <div class="ps-steps__footer clearfix" ng-if="action != ACTION_TEST">

                <div class="ps-steps__footer-save-btn">
                    <?= Html::a(_t('site.ps', 'Cancel'), ['/mybusiness/services'], ['class' => 'btn btn-link'])?>

                    <button
                        loader-click="savePrinter()"
                        loader-click-unrestored="true"
                        type="button" class="btn btn-primary js-save-printer"
                        >
                        <?= _t('site.ps', 'Save')?>
                    </button>

                </div>

                <div class="ps-steps__footer-del-btn">

                    <button
                        ng-if="action == ACTION_EDIT_PRINTER"
                        loader-click="deletePrinter()"
                        type="button" class="btn btn-warning">
                        <?= _t('site.ps', 'Delete printer')?>
                    </button>

                </div>
            </div>
        </div>
    </div>

</div>
<?php $this->endContent();?>