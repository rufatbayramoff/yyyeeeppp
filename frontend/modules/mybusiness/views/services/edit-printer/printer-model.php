
<?php
/** @var \yii\web\View $this */

Yii::$app->angular
    ->service(['notify', 'user', 'router'])
    ->resource(['Printer', 'PsPrinter'])
    ->controller('ps/printers/edit-printer')
    ->controllerParam('printers', \frontend\modules\mybusiness\controllers\EditPrinterController::jsonPrinters())
?>

<div class="row" ng-controller="PsPrinterEditPrinterController" ng-cloak>

    <div ng-if="isLoaded">

        <div class="col-sm-5">
            <h4 class="ps-printers-select-title">
                <?= _t("site.ps", "Printer model"); ?>
                <a href="/site/contact" class="ps-printers-add-yours" target="_blank">
                    <?= _t('site.ps', 'Printer not on the list? Contact us.'); ?>
                </a>
            </h4>
            <div class="m-t10 m-b20">
                <input ng-model="printerFilter" placeholder="Search printer" class="form-control btn-sm" />
            </div>
            <select
                ng-model="psPrinter.printer_id"
                ng-options="printer.id as printer.title group by printer.firm for printer in printers | filter:printerFilter"
                ng-change="onPrinterChange()"

                ng-disabled="!psPrinter.getIsNew()"
                size="15"
                class="form-control ps-printers-select">
                <option value="" style="display: none"> -</option>
            </select>
        </div>

        <div class="col-sm-7 wide-padding--left right-add-printer-block">

            <div class="form-group">
                <label class="control-label"><?=_t('site.ps', 'Name of your printer')?></label>
                <input
                    ng-model="psPrinter.title"
                    type="text" class="form-control" placeholder="<?=_t('site.ps', 'Name of your printer')?>" autocomplete="off">
            </div>

            <div class="form-group">
                <label class="control-label"><?=_t('site.ps', 'Description')?></label>
                <textarea
                    ng-model="psPrinter.description"
                    class="form-control ps-description" rows="5" placeholder="<?=_t('site.ps', 'Provide some description for your printer')?>"></textarea>

            </div>

            <div ng-if="currentPrinter" class="form-group">
                <label class="control-label"><?=_t('site.ps', 'Positional Accuracy')?></label>
                <input
                    ng-model="psPrinter.positional_accuracy"
                    type="number"
                    step="0.001"
                    max="{{currentPrinter.maxPositionalAccuracy}}"
                    min="0.001"
                    ng-model-options="{allowInvalid: true}"
                    class="form-control"
                    placeholder="<?=_t('site.ps', 'Positional Accuracy')?>" autocomplete="off">
            </div>

            <div class="form-group">

                <h3
                    ng-if="!printerProperties"
                    class="ps-printer-name-header"><?= _t('front.user', 'Please choose printer'); ?>
                </h3>
                <a
                    href="/site/contact"
                    ng-if="!printerProperties"
                    class="ps-printer-name-header"
                    target="_blank"><?= _t('site.ps', 'Printer not on the list? Contact us.'); ?>
                </a>


                <div ng-if="printerProperties">

                    <div
                        ng-if="printerProperties.build_volume"
                        class="row ps-printer-properties-line">
                        <div class="col-md-5">
                            <?= _t('site.ps', 'Build area'); ?>
                        </div>
                        <div class="col-md-7">
                            {{printerProperties.build_volume}}
                        </div>
                    </div>

                    <div
                        ng-if="printerProperties.materials"
                        class="row ps-printer-properties-line">
                        <div class="col-md-5">
                            <?= _t('site.ps', 'Materials'); ?>
                        </div>
                        <div class="col-md-7">
                            {{printerProperties.materials}}
                        </div>
                    </div>

                    <div
                        ng-if="printerProperties.layer_resolution_hight"
                        class="row ps-printer-properties-line">
                        <div class="col-md-5">
                            <?= _t('site.ps', 'Layer resolution (highest)'); ?>
                        </div>
                        <div class="col-md-7">
                            {{printerProperties.layer_resolution_hight}}
                        </div>
                    </div>

                    <div
                        ng-if="printerProperties.technology"
                        class="row ps-printer-properties-line">
                        <div class="col-md-5">
                            <?= _t('site.ps', 'Technology'); ?>
                        </div>
                        <div class="col-md-7">
                            {{printerProperties.technology}}
                        </div>
                    </div>
                </div>
            </div>

            <div ng-if="currentPrinter.haveEditableProperties()">

                <div ng-if="form.showRedefinedProperties">
                    <h4 class="ps-redefine-specs-header">
                        <?= _t('site.ps', 'Redefine specs for your printer'); ?>
                    </h4>

                    <div
                        ng-repeat="editableProperty in currentPrinter.getEditableProperties()"
                        class="row ps-printer-properties-line one-redefined-spec">
                        <div class="col-md-5">
                            {{editableProperty.title}}
                        </div>
                        <div class="col-md-7">


                            <input type="text" class="form-control"
                                   ng-if="editableProperty.property_id != PROPERTY_ID_BUILD_VOLUME"
                                   ng-model="redefinedPorperties[editableProperty.property_id]"
                                   ng-change="onRedefinedPropertyChange(editableProperty)"/>



                            <div ng-if="editableProperty.property_id == PROPERTY_ID_BUILD_VOLUME">

                                <build-volume-input class="m-t20"
                                                    change="onRedefinedPropertyChange(editableProperty)"
                                                    volume="redefinedPorperties[editableProperty.property_id]">

                                </build-volume-input>

                            </div>


                        </div>
                    </div>

                </div>

                <div
                    ng-if="!form.showRedefinedProperties"
                    class="">
                    <div class="ps-redefine-specs">
                        <?= _t('front.user', 'Redefine specs if your printer has different parameters'); ?>
                        <button
                            ng-click="form.showRedefinedProperties = true"
                            type="button" class="btn btn-link redefine-specs-button"><?= _t('front.user', 'Redefine specs'); ?></button>
                    </div>
                </div>

            </div>
            <?php if(false): ?>
            <div class="">
                <div >
                    <div class="checkbox checkbox-warning">
                        <input type="checkbox" ng-model="psPrinter.is_ready_thingiverse"  ng-true-value="1"
                               ng-false-value="0" ng-checked="psPrinter.is_ready_thingiverse == 1" id="is_ready_thingiverse">
                        <label for="is_ready_thingiverse"><span>
                              <?= _t('front.ps', "I'm willing to accept orders from Thingiverse"); ?>
                        </span></label>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>

    </div>
</div>


<script>
    <?php $this->beginBlock('js1', false); ?>


    /* Collapse printer select on tablets and mobiles */
    function collapsePrinterSelect() {
        var isBig = window.matchMedia("screen and (min-width: 1025px)");
        var isMobile = window.matchMedia("only screen and (max-width: 1024px)");

        if (isBig.matches) {
            $('.ps-printers-select').attr("size","15");
        } else if (isMobile.matches) {
            $('.ps-printers-select').attr("size","0");
        }
    }

    collapsePrinterSelect();
    $(window).resize(function () {collapsePrinterSelect()});

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>