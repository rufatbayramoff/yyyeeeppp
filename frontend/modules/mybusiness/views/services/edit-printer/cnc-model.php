<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.07.17
 * Time: 17:38
 */

/** @var \common\models\PsCncMachine $psCncMachine */

/** @var array $cncSchema */
/** @var \yii\web\View $this */

use frontend\assets\JsonEditorAssetJsTemplate;
use frontend\assets\JsonEditorAssetJsTemplateTheme;

JsonEditorAssetJsTemplate::register($this);
JsonEditorAssetJsTemplateTheme::register($this);

?>
<div id="psCncJsonContainer" class="form-group psCncJsonContainer" psCncJsonContainer="true">
    <input
            id='psCncJson'
            name="psCncJson"
            ng-model="psCnc.json"
            type="text"
            class="form-control hidden"
            placeholder="<?= _t('site.ps', 'Cnc machine description in JSON format.') ?>"
            autocomplete="off"
            value=''
    >
</div>

