<?php

use common\components\ArrayHelper;
use common\models\PsCncMachine;
use common\models\PsPrinter;
use common\models\StoreOrderAttemp;
use common\modules\company\components\CompanyServiceInterface;
use common\modules\company\helpers\CompanyServiceUrlHelper;
use common\services\PsPrinterService;
use frontend\models\user\UserFacade;
use frontend\widgets\PsPrintServiceReviewStarsWidget;
use lib\delivery\delivery\DeliveryFacade;
use yii\bootstrap\Alert;
use \yii\helpers\Html;


/** @var \yii\web\View $this */
/** @var \common\models\Ps $ps */
/** @var PsPrinter[] $psPrinters */
/** @var PsCncMachine[] $psCncMachines */
/** @var \common\models\CompanyServiceCategory[] $companyServicesCategories */
/** @var bool $isTestOrderOfferShowed */
/* @var $serviceTypes \common\models\CompanyServiceType[] */
/* @var $serviceTypeMachine \common\models\CompanyServiceType[] */

$isTestOrderResolved = (bool)ArrayHelper::first($psPrinters, function (PsPrinter $printer) {
    return $printer->is_test_order_resolved;
});

/** @var PsPrinter $firstPrinterWithoutTestOrder */
/* @var $serviceId null */
/* @var $companyServices array|\common\models\CompanyService[]|\yii\db\ActiveRecord[] */
$firstPrinterWithoutTestOrder = ArrayHelper::first($psPrinters, function (PsPrinter $printer) {
    return !$printer->is_test_order_resolved;
});

$this->title                   = _t('site.my', 'My Services');
$this->params['breadcrumbs'][] = ['label' => _t('front.user', 'Private profile'), 'url' => ['/user-profile']];
$this->params['breadcrumbs'][] = $this->title;
$user                          = UserFacade::getCurrentUser();
$ourHost                       = param('server', 'https://www.treatstock.com');
$publicUrl                     = \frontend\models\ps\PsFacade::getPsLink($ps);
Yii::$app->angular
    ->service(['notify', 'user', 'router', 'facebookApi', 'modal'])
    ->resource('PsPrinter')
    ->controller('ps/printers/list')
    ->directive(['star-input'])
    ->constant('facebookApplicationId', \Yii::$app->params['facebook_ps_print_application_id'])
    ->controllerParams(
        [
            'ps'                             => $ps,
            'firstPrinterWithoutTestOrderId' => $firstPrinterWithoutTestOrder->id ?? null,
            'needShowTestOrderOffer'         => !$isTestOrderOfferShowed,
        ]
    );
?>
<?php if ($serviceId): ?>
    <div class="container">
        <div class="col-lg-12"><a href="/mybusiness/services" class="btn btn-default order-item__back-btn"><span
                        class="tsi tsi-left"></span>
                <?= _t('site.ps', 'Back'); ?>
            </a>
        </div>

    </div>
<?php endif; ?>
<div class="container">
    <?php if ($ps->phone_status != \common\models\Ps::PHONE_STATUS_CHECKED) {
        echo Alert::widget(
            [
                'options' => ['class' => 'alert-info'],
                'body'    => _t('site.ps', 'We recommend verifying your phone number to improve communication.') .
                    ' ' . Html::a(_t('site.ps', 'Click here to verify.'), ['/mybusiness/settings'])
            ]
        );
    }
    ?>
    <div class="my-ps-printers__title">
        <h3><?= _t('site.ps', 'Equipment'); ?></h3>

        <div class="dropdown dropdown-primary">
            <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <?= _t('site.ps', 'Add'); ?>
                <span class="tsi tsi-down"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="serviceMenu">
                <?php foreach ($serviceTypeMachine as $serviceType): ?>
                    <li><a href="<?= $serviceType->getAddUrl() ?>"><?= $serviceType->title ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <?php if(!empty($psPrinters) || !empty($cuttingServices) || !empty($psCncMachines)):?>
        <div class="row my-ps-printers__items-list">
            <?php if (!empty($psPrinters)): ?>
                <?= $this->render('3d_printer_list', ['psPrinters' => $psPrinters, 'isTestOrderResolved' => $isTestOrderResolved, 'serviceId' => $serviceId, 'ps' => $ps]); ?>
            <?php endif; ?>
            <?php if (!empty($cuttingServices)): ?>
                <?= $this->render('services_list', ['services' => $cuttingServices, 'ps' => $ps]); ?>
            <?php endif; ?>
            <?php if (!empty($psCncMachines)): ?>
                <?= $this->render('cnc_machines_list', ['psCncMachines' => $psCncMachines, 'ps' => $ps]); ?>
            <?php endif; ?>
        </div>
    <?php endif;?>


        <div class="my-ps-printers__title">
            <h3><?= _t('site.ps', 'Services'); ?></h3>

            <div class="dropdown dropdown-primary">
                <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <?= _t('site.ps', 'Add'); ?>
                    <span class="tsi tsi-down"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="serviceMenu">
                    <li>
                        <ul class="dropdown-menu dropdown-menu--2col">
                            <?php foreach ($serviceTypeMachine as $serviceType): ?>
                                <li><a href="<?= $serviceType->getAddUrl() ?>"
                                       class="dropdown-menu--featured"><?= $serviceType->title ?></a></li>
                            <?php endforeach; ?>
                            <?php foreach ($companyServicesCategories as $companyServicesCategory): ?>
                                <li>
                                    <?php echo Html::a(
                                        H($companyServicesCategory->title),
                                        CompanyServiceUrlHelper::addWithType($companyServicesCategory->slug)
                                    ) ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    <?php if (!empty($companyServices)): ?>
        <div class="row my-ps-printers__items-list">
            <?= $this->render('services_list', ['services' => $companyServices, 'ps' => $ps]); ?>
        </div>
    <?php endif; ?>

    <?php if (!$serviceId && empty($psPrinters) && empty($psCncMachines)): ?>
        <div class="my-ps-printers__item m-t20">
            <div class="my-ps-printers__card">
                <div class="row my-ps-printers__row my-ps-printers__row--add-new">
                    <div class="col-xs-12 my-ps-printers__header text-center m-t20">
                        <h3 class="m-t0"><?= _t('site.ps', 'Provide a service'); ?></h3>
                    </div>
                    <div class="col-xs-12">
                        <div class="my-ps-printers__block text-center">

                            <div class="row m-t20 m-b20">

                                <?php foreach ($serviceTypes as $serviceType): ?>
                                    <div class="col-md-4"><a class="btn btn-ghost btn-block btn-primary m-b20"
                                                             href="<?= $serviceType->getAddUrl(); ?>"><?= $serviceType->title; ?></a>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>


<script type="text/ng-template" id="/app/ps/printers/test-order-offer.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t('site.ps', 'Congratulations!') ?></h4>
                </div>
                <div class="modal-body">
                    <p><?= _t('site.ps', 'Congratulations! You have registered your first printer on Treatstock. We have saved all your information and will publish your printer after you complete a test order.') ?></p>
                </div>
                <div class="modal-footer">

                    <button
                            ng-click="$dismiss()"
                            type="button" class="btn btn-default"><?= _t('site.ps', 'Later') ?></button>

                    <button
                            loader-click="createTestOrder()"
                            loader-click-unrestored="true"
                            type="button" class="btn btn-primary"><?= _t('site.ps', 'Request test order') ?></button>
                </div>
            </div>
        </div>
    </div>
</script>

