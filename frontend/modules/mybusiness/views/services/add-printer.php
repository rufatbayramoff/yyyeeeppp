<?php

/** @var \yii\web\View $this */
use frontend\modules\mybusiness\controllers\EditPrinterController;

/** @var bool $fromCreatePs */

/** @var \common\models\PsPrinter $printer */
/** @var \common\models\PsPrinter|null $previousPrinter */
$printer = $this->params['printer'];
$previousPrinter = $this->params['previousPrinter'];

$types = $printer->companyService->deliveryTypess;

Yii::$app->angular
    ->service(['maps', 'geo', 'notify', 'router', 'user', 'ps/baseSteps'])
    ->directive(['google-map', 'google-address-input'])
    ->resource(['Printer', 'PsPrinter', 'PsMachine'])
    ->controller('ps/printers/create')
    ->controllerParam('psPrinter', EditPrinterController::jsonPsPrinter($printer))
    ->controllerParam('previousPsMachine', EditPrinterController::jsonPsMachine($previousPrinter->companyService ?? new \common\models\CompanyService()))
    ->controllerParam('psMachine', EditPrinterController::jsonPsMachine($printer->companyService));

$tabIndex = 1;
?>



<div ng-controller="PsPrinterCreateController">

<div class="container">


<div class="row">

    <div class="col-md-12 ps-steps">

        <?php if(!$fromCreatePs):?>
            <h2 class="ps-steps__title"><?php echo _t('front.user', 'Add new printer'); ?></h2>
        <?php endif?>

        <ul id="tabs" class="nav nav-tabs nav-tabs--secondary back-forward-tabs">
            <?php if($fromCreatePs):?>
                <li>
                    <a>
                        <span class="badge"><?= $tabIndex++ ?></span>
                        <?= _t('site.ps', 'Print Service Details'); ?>
                    </a>
                </li>
            <?php endif?>

            <li ng-class="{active : steps.isCurrentStep(1)}">
                <a>
                    <span class="badge badge-success"><?= $tabIndex++ ?></span>
                    <?= _t('site.ps', 'Select Printer Model'); ?>
                </a>
            </li>
            <li ng-class="{active : steps.isCurrentStep(2)}">
                <a>
                    <span class="badge"><?= $tabIndex++ ?></span>
                    <?= _t('site.ps', 'Material Options'); ?>
                </a>
            </li>
            <li ng-class="{active : steps.isCurrentStep(3)}">
                <a>
                    <span class="badge"><?= $tabIndex++ ?></span>
                    <?= _t('site.ps', 'Delivery Details'); ?>
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div
                ng-if="steps.isCurrentStep(1)"
                class="tab-pane active">
                <?= $this->render('edit-printer/printer-model')?>
            </div>

            <div
                ng-if="steps.isCurrentStep(2)"
                class="tab-pane active">
                <?= $this->render('edit-printer/material-options')?>
            </div>

            <div
                ng-if="steps.isCurrentStep(3)"
                class="tab-pane active">
                <?= $this->render('edit-printer/delivery-options', ['psMachine'=>$printer->companyService])?>
            </div>
        </div>


        <div class="ps-steps__footer clearfix">

                <a href="/mybusiness/services" class="btn btn-link"><?= _t('site.ps', 'Cancel'); ?></a>

                <button
                    ng-if="!steps.isFirstStep()"
                    ng-click="steps.prevStep()"
                    type="button" class="btn btn-primary btn-ghost">
                    &larr; <?= _t('site.ps', 'Back')?>
                </button>

                <button
                    ng-if="!steps.isLastStep()"
                    ng-click="steps.nextStep()"
                    type="button" class="btn btn-primary">
                    <?= _t('site.ps', 'Next Step')?> &rarr;
                </button>

                <button
                    ng-if="steps.isLastStep()"
                    loader-click="savePrinter()"
                    loader-click-unrestored="true"
                    type="button" class="btn btn-primary js-save-printer">
                    <?= _t('site.ps', 'Save printer')?>
                </button>

            </div>
    </div>
    <?php /* ?>
    <div class="col-md-3 sidebar">
         <div class="panel panel-default">
             <div class="panel-body">
                <?= _t('site.ps', "Make a detailed description of the 3D printer you use and its capabilities. The more information you can provide, the less questions the customers will ask."); ?>
             </div>
        </div>
    </div>
    <?php */ ?>
</div>

</div>

</div>
