<script type="text/ng-template" id="/app/services/reviewPrinterModal">

    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= isset($modalTitle)?$modalTitle:_t('site.order', 'Review') ?></h4>
                </div>
                <div class="modal-body" style="overflow: auto;max-height: 76vh;">

                    <p>
                        <?=_t('site.printer', 'As an owner and operator of the {{printerTitle}}, who better to leave a review and help other members of the 3D printing community when deciding which 3D printer they should buy or which 3D printer they should select to produce their order?');?>
                    </p>
                    <p>
                        <?=_t('site.printer', 'Your feedback will be published in our {printerLink} for your particular 3D printer, which can generate leads to your profile page, attract new customers and increase your sales.',
                            ['printerLink' => '<a href="https://www.treatstock.com/machines/3d-printers/">3D Printer\'s Guide</a>']);?>
                    </p>

                    <h3><?=_t('site.printer', 'How would you rate the {{printerTitle}} for:');?></h3>


                    <form class="form-horizontal" name="orderReviewForm">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.order', 'Share your purchase experience on Treatstock')?></label>
                            <div class="col-sm-8">
                                <textarea ng-model="form.comment" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.order', 'Speed')?></label>
                            <div class="col-sm-8">
                                <input star-input="form.rating_speed" type="number" class="star-rating" data-size="sm" step="1" data-symbol="&#xea4b;" data-glyphicon="false"
                                       data-rating-class="tsi">
                                <span class="help-block"><?= _t('site.order', 'How would you rate the speed in which your order was shipped?')?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.order', 'Quality')?></label>
                            <div class="col-sm-8">
                                <input star-input="form.rating_quality" type="number" class="star-rating" data-size="sm" step="1" data-symbol="&#xea4b;" data-glyphicon="false"
                                       data-rating-class="tsi">
                                <span class="help-block"><?= _t('site.order', 'How would you rate the quality of the product(s) received?')?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.order', 'Communication')?></label>
                            <div class="col-sm-8">
                                <input star-input="form.rating_communication" type="number" class="star-rating" data-size="sm" step="1" data-symbol="&#xea4b;"
                                       data-glyphicon="false" data-rating-class="tsi">
                                <span class="help-block"><?= _t('site.order', 'How would you rate the communication of the supplier?')?></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button
                            loader-click="cancel()"
                            ng-click="$dismiss()"
                            type="button" class="btn btn-default">{{form.cancelText}}
                    </button>
                    <button
                            loader-click="save()"
                            loader-click-unrestored="true"
                            type="button" class="btn btn-danger"><?=_t('site.ps', 'Submit')?></button>
                </div>
            </div>
        </div>
    </div>
</script>