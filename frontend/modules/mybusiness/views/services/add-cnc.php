<?php

/** @var \yii\web\View $this */

use common\components\ArrayHelper;
use frontend\modules\mybusiness\controllers\EditPrinterController;

/** @var bool $fromCreatePs */

/** @var \common\models\PsCncMachine $psCncMachine */
$psCnc = $psCncMachine->psCnc;

$psCncJson = Yii::$app->getModule('cnc')->psCncJsonGenerator->generateJsonServiceInfo($psCncMachine->psCnc);

$step = 1;
if (isset($tab) && $tab == 'delivery') {
    $step = 2;
}

$psCncArr = ArrayHelper::toArray($psCnc);

Yii::$app->angular
    ->service(['maps', 'geo', 'notify', 'router', 'user', 'ps/baseSteps'])
    ->directive(['google-map', 'google-address-input'])
    ->resource(['PsCnc', 'PsMachine', 'CncSchema'])
    ->controller('ps/printers/createCnc')
    ->controllerParam('step', $step)
    ->controllerParam('psCnc', $psCncArr)
    ->controllerParam('psCncJson', $psCncJson)
    ->controllerParam('psMachine', EditPrinterController::jsonPsMachine($psCncMachine->companyService))
    ->controllerParam('cncSchema', $cncSchema)
    ->controllerParam('cncSchemaMaterials', $cncSchemaMaterials);


$tabIndex = 1;
?>
<div ng-controller="PsCncCreateController">
    <div class="container">
        <?php if (!empty($type) && $type == 'add'): ?>
            <div class="row">
                <div class="col-sm-8 col-md-7">
                    <h2 class="m-t0"><?= _t('site.ps', 'Welcome to the Beta version of CNC Services'); ?></h2>
                    <p>
                        <?= _t(
                            'site.ps',
                            "Before we officially launch, we'd like to invite all users who offer CNC services to test it out and if you have any suggestions or feedback on how we can improve it for you and your customers, we're here to listen."
                        ); ?>
                        If you have any questions or would like to offer feedback, please <a href="/site/contact" target="_blank">click here</a>.
                    </p>
                </div>
                <div class="col-sm-4 col-md-5">
                    <img style="display: block;margin: -20px auto 0;width: 100%;max-width: 200px;" src="/static/images/main-page-icons/mp-cnc.jpg" alt="CNC Machine">
                </div>
            </div>
        <?php endif; ?>

        <div class="row">

            <div class="col-md-12 ps-steps">

                <ul id="tabs" class="nav nav-tabs nav-tabs--secondary back-forward-tabs">
                    <li ng-class="{active : steps.isCurrentStep(1)}" ng-click="steps.checkStep(1)">
                        <a>
                            <span class="badge badge-success"><?= $tabIndex++ ?></span>
                            <?= _t('site.ps', 'Select Machine'); ?>
                        </a>
                    </li>
                    <li ng-class="{active : steps.isCurrentStep(2)}" ng-click="steps.checkStep(2)">
                        <a>
                            <span class="badge"><?= $tabIndex++ ?></span>
                            <?= _t('site.ps', 'Material Options'); ?>
                        </a>
                    </li>
                    <li ng-class="{active : steps.isCurrentStep(3)}" ng-click="steps.checkStep(3)">
                        <a>
                            <span class="badge"><?= $tabIndex++ ?></span>
                            <?= _t('site.ps', 'Delivery Details'); ?>
                        </a>
                    </li>
                    <li ng-class="{active : steps.isCurrentStep(4)}" ng-click="steps.checkStep(4)">
                        <a>
                            <span class="badge"><?= $tabIndex++ ?></span>
                            <?= _t('site.ps', 'Calculating Details'); ?>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div
                            ng-show="steps.isCurrentStep(1)"
                            class="tab-pane active">
                        <?= $this->render('edit-printer/cnc-model-machines', []) ?>
                    </div>
                    <div
                            ng-show="steps.isCurrentStep(2)"
                            class="tab-pane active">
                        <?= $this->render('edit-printer/cnc-model-materials', []) ?>
                    </div>

                    <div
                            ng-show="steps.isCurrentStep(3)"
                            class="tab-pane active">
                        <?= $this->render('edit-printer/delivery-options', ['psMachine' => $psCncMachine->companyService]) ?>
                    </div>
                    <div
                            ng-show="steps.isCurrentStep(4)"
                            class="tab-pane active">
                        <?= $this->render('edit-printer/cnc-model', []) ?>
                    </div>
                </div>


                <div class="ps-steps__footer clearfix">

                    <a ng-if="!steps.isFirstStep()" onclick="window.onbeforeunload = null;" href="/mybusiness/services" class="btn btn-link"><?= _t('site.ps', 'Cancel'); ?></a>

                    <button
                            ng-if="!steps.isFirstStep()"
                            ng-click="steps.prevStep()"
                            type="button" class="btn btn-primary btn-ghost">
                        &larr; <?= _t('site.ps', 'Back') ?>
                    </button>

                    <button
                            ng-if="!steps.isLastStep()"
                            ng-click="steps.nextStep()"
                            type="button" class="btn btn-primary">
                        <?= _t('site.ps', 'Next Step') ?> &rarr;
                    </button>

                    <button
                            ng-if="steps.isLastStep() || steps.isSaveStep()"
                            loader-click="saveCnc()"
                            loader-click-unrestored="true"
                            type="button" class="btn btn-primary js-save-printer">
                        <?= _t('site.ps', 'Save') ?>
                    </button>

                </div>
            </div>


        </div>

    </div>

</div>

<script>
    window.onbeforeunload = function () {
        return "Are you sure you want to leave without saving?";
    }
</script>
