<?php

use yii\helpers\Html;

/** @var \common\models\User $user */
/** @var \common\models\Ps $ps */
/** @var \yii\web\View $this */

echo $this->render('certification-assets');
?>

<div ng-controller="CertificationController">

    <div>
        <?php if($ps->getIsPhoneVerified()):?>

            <div class="ps-test__required ps-test__required--success">
                <span class="tsi tsi-checkmark-c"></span><?= _t('site.ps', 'Cell phone number is verified.')?>
            </div>

        <?php else :?>

            <div class="ps-test__required ps-test__required--fail">
                <span class="tsi tsi-warning-c"></span>
                <?= Html::a(_t('site.ps', 'Please ensure cell phone number is verified.'), ['/mybusiness/settings', 'fromcertification' => 1])?>
            </div>

        <?php endif ?>
    </div>

    <div>
        <?php if($user->hasDocument()):?>

            <div class="ps-test__required ps-test__required--success m-b0">
                <span class="tsi tsi-checkmark-c"></span><?= _t('site.ps', 'Document uploaded.')?>
            </div>
            <button type="button" class="btn btn-sm btn-default m-t10 m-b20" style="margin-left: 25px;"
                    ng-click="showUploadDocumentModal()">
                <?= _t('site.ps.test', 'Change Document') ?>
            </button>

        <?php else :?>

            <div class="ps-test__required ps-test__required--fail">
                <span class="tsi tsi-warning-c"></span>
                <a ng-click="showUploadDocumentModal()" class="inspectlet-sensitive" style="cursor: pointer"><?= _t('site.ps', 'Please upload a form of I.D.')?></a>
            </div>

        <?php endif ?>
    </div>
</div>