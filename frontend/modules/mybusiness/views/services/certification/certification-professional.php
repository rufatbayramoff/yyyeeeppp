<?php
/** @var \yii\web\View $this */

use common\models\PsPrinter;
use common\models\PsPrinterTest;
use yii\widgets\ActiveForm;

/** @var $companyService \common\models\CompanyService */

$tsCertificationClass = $companyService->getTsCertificationClass();
?>

<div class="row">
    <div class="col-sm-12">
        <div class="ps-test__item1">
            <h2 class="m-t0">
                <?= _t('site.ps.test', 'Certification') ?> - <?= $tsCertificationClass->title ?? '' ?>
            </h2>

            <p class="ps-test__item-description">
                <?= $tsCertificationClass->description ?>
            </p>
            <?php $form = ActiveForm::begin(['method' => 'POST', 'action' => '/ts-internal-purchase/certification/checkout']); ?>
            <div class="cert-table">
                <div class="cert-table__row cert-table__row--head">
                    <div class="cert-table__block"></div>
                    <div class="cert-table__block">
                        <h3>
                            <?= _t('site.ps.test', 'Non-certified')?>
                        </h3>
                        <div class="cert-table__price"><?= _t('site.ps.test', 'Free')?></div>
                        <div class="btn btn-info"><?= _t('site.ps.test', 'Free')?></div>
                    </div>
                    <div class="cert-table__block">
                        <h3>
                            <?= $tsCertificationClass->title ?? '' ?>
                        </h3>
                        <div class="cert-table__price"><?=displayAsMoney($tsCertificationClass->priceMoney)?><small>/<?= _t('site.ps.test', 'year')?></small></div>
                        <input type="hidden" name="companyServiceId" value="<?= $companyService->id; ?>">
                        <button class="btn btn-primary p-l15 p-r15" type="submit"><?= _t('site.ps.test', 'Upgrade') ?></button>
                    </div>
                </div>

                <?php /*
                <div class="cert-table__row">
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'Orders in Production')?>
                    </div>
                    <div class="cert-table__block">
                        1
                    </div>
                    <div class="cert-table__block">
                        3
                    </div>
                </div>
                */?>

                <div class="cert-table__row">
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'Priority on the vendors\' list')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'low')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'high')?>
                    </div>
                </div>

                <div class="cert-table__row">
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'Verification icon')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'no')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'yes')?>
                    </div>
                </div>

                <div class="cert-table__row">
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'ID validation')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'no')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'yes')?>
                    </div>
                </div>

                <div class="cert-table__row">
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'Company page')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'yes')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'yes')?>
                    </div>
                </div>

                <div class="cert-table__row">
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'Personal widget')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'yes')?>
                    </div>
                    <div class="cert-table__block">
                        <?= _t('site.ps.test', 'yes')?>
                    </div>
                </div>

            </div>
            <?php ActiveForm::end(); ?>



        </div>

    </div>

    <div class="col-sm-6 col-md-5">
        <?php if ($companyService && $companyService->isCertificatedFailed()) { ?>
            <div class="ps-test__checking-status bg-danger m-b20">
                <h3 class="m-t0">
                    <?= _t('site.ps.test', 'Last certification is failed') ?>
                </h3>
                <?= _t('site.ps.test', 'Reason') ?>:  <?= $companyService->getCertificationFailedReason() ?? '' ?>
            </div>
        <?php } ?>
    </div>
</div>
