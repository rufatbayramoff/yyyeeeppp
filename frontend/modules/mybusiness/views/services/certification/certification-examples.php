<?php

/** @var PrinterTechnologyCertificationExample[] $examples */

use common\models\PrinterTechnologyCertificationExample;
use frontend\components\image\ImageHtmlHelper;
?>

<?php if($examples) : ?>

    <div class="ps-test__ref">
        <?php foreach ($examples as $example) : ?>
            <a class="ps-test__ref-pics" href="<?= $example->file->getFileUrl()?>" data-lightbox="cert-common">
                <img src="<?= ImageHtmlHelper::getThumbUrlForFile($example->file, 160, 90) ?>" alt="Certification Reference">
            </a>
        <?php endforeach; ?>
    </div>

<?php endif; ?>
