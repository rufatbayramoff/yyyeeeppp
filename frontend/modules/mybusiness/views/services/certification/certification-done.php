<?php

/** @var $companyService \common\models\CompanyService */

use yii\helpers\Html;

?>

<div class="row m-l0 m-r0">
    <div class="col-sm-6 col-md-5 ps-test__checking-status bg-success m-t0 m-b20">
        <h3 class="m-t0">
            <?= _t('site.ps.test', 'Printer is already certified') ?>
        </h3>
        <?= _t('site.ps.test', 'Expire date: ') ?><?= Yii::$app->formatter->asDate($companyService->certification_expire) ?>
    </div>
</div>
<div>
    <?php echo Html::a(_t('site.ps.test', 'Download'), ['certificate-download', 'id' => $companyService->ps_printer_id], ['class' => 'btn btn-primary m-b30']) ?>
</div>



