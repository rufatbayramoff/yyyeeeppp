<?php
/** @var \yii\web\View $this */

use common\models\PsPrinterTest;
use common\modules\company\serializers\CompanyServiceSerializer;
use frontend\modules\mybusiness\serializers\CompanySettingSerializer;


echo $this->render('certification-assets');

/** @var $companyService \common\models\CompanyService */
$company = $companyService->company;

Yii::$app->angular->controllerParam('company', CompanySettingSerializer::serialize($company));
Yii::$app->angular->controllerParam('companyService', CompanyServiceSerializer::serialize($companyService));

?>

<div ng-controller="CertificationController">
    <div class="row" ng-controller="SettingsPhoneController" ng-cloak>

        <div class="col-sm-12 form-horizontal">

            <?php if ($companyService->isCertificationInChecking()) { ?>
                <div class="ps-test__checking-status bg-info m-t0">
                    <h3 class="m-t0">
                        <?= _t('site.ps.test', 'Checking') ?>
                    </h3>
                    <?= _t('site.ps.test', 'Your request is submitted for our moderation team to check the quality of the print results. Once it is approved, your certification will be active.') ?>
                </div>
            <?php } elseif ($companyService->isCertificatedFailed()) { ?>
                <div class="ps-test__checking-status bg-danger m-t0">
                    <?= _t('site.ps.test', 'Failed. Please, try again.') ?>
                    <?php if ($companyService->psPrinter->psPrinterTest->comment): ?>
                        <div><?= _t('site.ps.test', 'Reason') ?>: <?= $companyService->psPrinter->psPrinterTest->comment ?></div>
                    <?php endif; ?>
                </div>
            <?php } else { ?>
                <div class="form-group">
                    <label class="col-sm-4 col-lg-3 control-label ps-test__step">
                        <span class="ps-test__step-count">1</span>
                        <?= _t('site.ps.test', 'Confirm cell phone number') ?>
                    </label>

                    <div class="col-sm-8 col-lg-6">
                        <?=$this->renderFile('@frontend/modules/mybusiness/views/settings/settings-phone.php', ['company' => $company]);?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-lg-3 control-label ps-test__step">
                        <span class="ps-test__step-count">2</span>
                        <?= _t('site.ps.test', 'Upload a form of I.D.') ?>
                    </label>

                    <div class="col-sm-8 col-lg-6 p-t10">
                        <?php if ($company->user->hasDocument()): ?>

                            <div class="ps-test__required ps-test__required--success m-b0">
                                <span class="tsi tsi-checkmark-c"></span><?= _t('site.ps', 'Document uploaded') ?>
                            </div>
                            <button type="button" class="btn btn-sm btn-default m-t10 m-b20"
                                    ng-click="showUploadDocumentModal()">
                                <?= _t('site.ps.test', 'Change Document') ?>
                            </button>

                        <?php else : ?>

                            <div class="ps-test__required ps-test__required--fail">
                                <span class="tsi tsi-warning-c"></span>
                                <a ng-click="showUploadDocumentModal()" class="inspectlet-sensitive"
                                   style="cursor: pointer"><?= _t('site.ps', 'Upload a form of ID') ?></a>
                            </div>

                        <?php endif ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-lg-3 control-label ps-test__step">
                        <span class="ps-test__step-count">3</span>
                        <?= _t('site.ps.test', 'Download and print test model') ?>
                    </label>

                    <div class="col-sm-8 col-lg-6">
                        <!--                    <img src="/static/tests/professional.png" class="ps-test__example-pic"/>-->
                        <a href="/static/tests/professional-test.stl" class="btn btn-primary ps-test__download-btn"><?= _t('site.ps.test', 'Download test model') ?></a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-lg-3 control-label ps-test__step">
                        <span class="ps-test__step-count">4</span>
                        <?= _t('site.ps.test', 'Please upload a photo of the printed test model') ?>
                    </label>

                    <div class="col-sm-8 col-lg-6">
                        <div class="text-center ps-upload-photo ps-upload-photo--ps-test-upload"
                             dropzone-images="professionalTest.testedPhotos"
                             can-change-after-select="true">

                            <div dropzone-images-preview></div>
                            <div dropzone-images-click>
                                <div class="btn btn-link btn-block">
                                                <span class="drop-photo-icon">
                                                    <span class="tsi tsi-upload-l"></span>
                                                </span>
                                    <?= _t('site.ps', 'Browse or drop photo'); ?>
                                </div>
                            </div>
                            <p class="text-muted">
                                <?= _t('site.ps', 'Upload JPG, PNG or GIF files'); ?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-lg-3 control-label"></label>

                    <div class="col-sm-8 col-lg-6">
                        <div class="ps-test__pic-tips m-b20">
                            <?= _t('site.ps', 'Please make sure the photo of the printed test model is of a good quality and close enough to clearly display the details and quality of the print.
                                    You should also upload photos taken from different angles so that we can observe the model properly.') ?>

                            <?= $this->render('certification-examples', ['examples' => $companyService->psPrinter->printer->technology->getCertificationExmplesForType(PsPrinterTest::TYPE_PROFESSIONAL)]) ?>
                        </div>

                        <button
                                loader-click="saveTest(professionalTest)"
                                ng-disabled="!professionalTest.testedPhotos.length"
                                loader-click-unrestored="true"
                                type="button" class="btn btn-primary ps-test__upload-save">
                            <?= _t('site.ps', 'Save result') ?>
                        </button>
                    </div>
                </div>

            <?php } ?>
        </div>
    </div>
</div>