<?php
/** @var \yii\web\View $this */

/** @var PsPrinter $psPrinter */

use common\models\PsPrinter;

$companyService = $psPrinter->companyService;

if ($companyService->isCertificated()) {
    echo $this->render('certification-done', ['companyService' => $companyService]);
} elseif ($companyService->isCertificationInProgress()) {
    echo $this->render('certification-process', ['companyService' => $companyService]);
} else {
    echo $this->render('certification-professional', ['companyService' => $companyService]);
}

