<script type="text/ng-template" id="/app/ps/printers/test/upload-document-modal.html">
    <div class="modal fade inspectlet-sensitive" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t('site.ps', 'Upload an ID')?></h4>
                </div>
                <div class="modal-body">
                    <div dropzone-image="form.documentFile" class="text-center ps-upload-photo  inspectlet-sensitive">
                        <div id="psImagePreview" class="inspectlet-sensitive">
                            <span dropzone-txt-preview style="width: 200px; font-size:24px;"/>
                        </div>
                        <div dropzone-image-click id="dropYourPhoto">
                            <div class="btn btn-link">
                                                    <span class="drop-photo-icon">
                                                        <span class="tsi tsi-upload-l"></span>
                                                    </span>
                                <?= _t('site.ps', "Please upload a form of ID such as a business registration certificate, business bank statement, recent business utility bill, or a valid driver's license or passport for individuals.")?>
                                <br>
                                <br>
                                <?= _t('site.ps', "Supported file formats png, jpg, jpeg, gif")?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button
                        ng-click="$dismiss()"
                        type="button" class="btn btn-default"><?=_t('site.ps', 'Cancel')?></button>
                    <button
                        loader-click="uploadDocumentFile()"
                        ng-disabled="!form.documentFile"
                        loader-click-unrestored="true"
                        type="button" class="btn btn-primary inspectletIgnore"><?=_t('site.ps', 'Save')?></button>
                </div>
            </div>
        </div>
    </div>
</script>