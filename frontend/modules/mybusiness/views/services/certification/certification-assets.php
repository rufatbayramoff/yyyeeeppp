<?php

use common\components\ArrayHelper;
use common\components\serizaliators\porperties\PsEditProperties;
use common\models\GeoCountry;
use common\models\PaymentCurrency;
use common\models\UserSms;
use frontend\assets\DropzoneAsset;
use frontend\assets\FlagsAsset;
use frontend\assets\LightboxAsset;
use frontend\models\user\UserFacade;
use lib\geo\GeoNames;

/** @var \yii\web\View $this */

$this->registerAssetBundle(FlagsAsset::class);
$this->registerAssetBundle(DropzoneAsset::class);
$this->registerAssetBundle(LightboxAsset::class);

Yii::$app->angular
    ->service(['notify', 'user', 'router', 'modal', 'geo'])
    ->resource(['PsPrinter', 'Printer'])
    ->controller(['ps/printers/edit', 'ps/printers/certification', 'ps/ps/settings-phone'])
    ->directive(['dropzone-images', 'dropzone-image'])
    ->constants([
        'countries' => yii\helpers\ArrayHelper::toArray(
            GeoNames::getAllCountries(),
            [GeoCountry::class => ['id', 'iso_code', 'title', 'is_easypost_domestic', 'paypal_payout', 'is_easypost_intl','is_bank_transfer_payout']]
        ),
    ]);

echo $this->render('certification-upload-document-modal');
