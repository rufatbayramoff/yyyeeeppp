<div class="col-xs-12">
    <hr>
    <h3 class="m-b30"><?= _t('site.ps', 'Why should I get certification?') ?></h3>
</div>

<div class="col-sm-4">
    <img class="img-responsive" src="/static/images/ps-cert-benefits-1.png?22052017">
    <h4>
        1. <?= _t('site.ps', 'Only certified machines can receive orders from our catalog of 3D models') ?>
    </h4>
</div>
<div class="col-sm-4">
    <img class="img-responsive" src="/static/images/ps-cert-benefits-2.png?22052017">
    <h4>
        2. <?= _t('site.ps', 'Only certified machines can receive orders from several key API partners') ?>
    </h4>
</div>
<div class="col-sm-4">
    <img class="img-responsive" src="/static/images/ps-cert-benefits-3.png?22052017">
    <h4>
        3. <?= _t('site.ps', 'Customers trust and send more requests to certified machines') ?>
    </h4>
</div>