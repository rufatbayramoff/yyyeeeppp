<?php

use common\components\ArrayHelper;
use common\models\PsCncMachine;
use common\models\CompanyService;
use frontend\models\ps\PsFacade;
use lib\delivery\delivery\DeliveryFacade;
use \yii\helpers\Html;
use yii\helpers\Url;


/** @var \yii\web\View $this */
/** @var \common\models\Ps $ps */
/** @var \common\models\PsCncMachine[] $psCncMachines */

$psVisibility = ArrayHelper::map(
    $psCncMachines,
    function (PsCncMachine $psCncMachine) {
        return 'm' . $psCncMachine->companyService->id;
    },
    function (PsCncMachine $psCncMachine) {
        return $psCncMachine->companyService->visibility;
    }
);

Yii::$app->angular
    ->service(['notify', 'user', 'router', 'facebookApi', 'modal'])
    ->resource('PsCnc')
    ->resource('PsMachine')
    ->controller('ps/printers/listCnc')
    ->controllerParam(
        'psVisibility',
        $psVisibility
    );
?>
    <?php
    foreach ($psCncMachines as $psCncMachine) {
        ?>
        <div ng-controller="PsCncListController" class="my-ps-printers__item col-xs-12 col-sm-6 col-md-4 p-b30">
            <div class="my-ps-printers__card m-b0">

                <div class="my-ps-printers__row border-0 p-t10">
                    <span class="my-ps-printers__tech-label label label-info" title="<?= _t('site.ps', 'CNC Service'); ?>">
                        <?= _t('site.ps', 'CNC Service'); ?>
                    </span>

                    <h3 class="my-ps-printers__heading my-ps-printers__heading--printer">
                        <?= _t('site.ps', 'CNC Service'); ?>
                    </h3>

                    <div class="my-ps-printers__block m-t5 m-b5">
                        <strong><?= _t('site.ps', 'Moderation'); ?>:</strong> <?= $psCncMachine->companyService->getModeratorStatusLabel() ?>
                    </div>
                    <div class="my-ps-printers__block my-ps-printers__block--status m-b15">
                        <strong><?= _t('site.ps', 'Availability'); ?>:</strong>
                        <?= HTML::dropDownList(
                            'psMachineVisibility' . $psCncMachine->id,
                            null,
                            CompanyService::getVisibilityLabels(),
                            [
                                'ng-model'  => "isVisible.m" . $psCncMachine->companyService->id . '',
                                'ng-change' => "onPsMachineVisibilityChange(" . $psCncMachine->companyService->id . ")",
                                'class'     => 'form-control input-xs',
                                'options'   => [
                                    $psCncMachine->companyService->visibility => ['Selected' => true]
                                ]
                            ]
                        ) ?>
                    </div>

                    <div class="clearfix"></div>
                    <a class="my-ps-printers__edit"
                       href="/mybusiness/services/edit-cnc?psMachineId=<?= $psCncMachine->companyService->id ?>">
                        <span class="tsi tsi-pencil"></span><?= _t('site.ps', 'Edit'); ?>
                    </a>
                </div>

                <div class="my-ps-printers__row my-ps-printers__row--loc-mat border-t">
                    <div class="row">
                        <div class="col-sm-12">

                            <?= \frontend\widgets\CalculateCncButton::upload(
                                [
                                    'linkClass' => 'btn btn-primary btn-sm m-t10 m-b10 pull-left',
                                    'machine'   => $psCncMachine->companyService
                                ]
                            ) ?>

                            <a class="btn btn-default btn-sm m-t10 m-b10 pull-right"
                               data-confirm="<?= _t('yii', 'Are you sure you want to delete this item?') ?>"
                               data-method='post'
                               href="/mybusiness/services/delete-cnc?psMachineId=<?= $psCncMachine->companyService->id ?>">
                                <span class="tsi tsi-bin m-r10"></span><?= _t('site.ps', 'Delete'); ?>
                            </a>
                        </div>
                    </div>
                </div>

                <?php if (!$ps->is_cnc_hints_readed): ?>

                    <div class="my-ps-printers__row my-ps-printers__row--loc-mat border-t cnc_hints">

                        <?php if ($psCncMachine->isAllowedWidget()) { ?>
                            <p class="m-t10 m-b10">
                                <?= _t(
                                    'site.cnc',
                                    'You can embed <a href="{link}" target="_blank">our widget</a> on your website and your visitors can place instant orders with you',
                                    ['link' => Url::toRoute(['/mybusiness/widgets/printers'])]
                                ) ?>
                            </p>
                        <?php } ?>

                        <p>
                            <?= _t(
                                'site.cnc',
                                'When your CNC service is approved by moderation, you will be able to receive orders from <a href="{link}" target="_blank">your public profile</a> page on Treatstock.',
                                ['link' => PsFacade::getPsLink($ps)]
                            ) ?>
                        </p>

                        <div class="text-center">
                            <button class="btn btn-primary btn-ghost m-b20"
                                    loader-click="readedCncHits()">
                                <span class="tsi tsi-checkmark m-r10"></span> <?= _t('site.cnc', 'Acknowledged') ?>
                            </button>
                        </div>

                    </div>

                <?php endif; ?>

            </div>
        </div>
        <?php
    }
    ?>