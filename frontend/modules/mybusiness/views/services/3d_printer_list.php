<?php

use common\models\CompanyService;
use common\models\PsPrinter;
use common\models\StoreOrderAttemp;
use common\services\PsPrinterService;
use lib\delivery\delivery\DeliveryFacade;
use \yii\helpers\Html;

?>
<?php if ($psPrinters && !$isTestOrderResolved): ?>

    <div class="col-xs-12">
        <div class="alert alert-warning" role="alert">
            <?= _t('site.ps', 'Please complete a test order for your machine to get it published on Treatstock.'); ?>
        </div>
    </div>

<?php endif; ?>

<?php
/** @var $onePrinter PsPrinter */
foreach ($psPrinters as $onePrinter):

    $rejectReasonStr = false;
    if ($onePrinter->getIsRejected()):
        $rejectReasonStr = $onePrinter->getLastRejectLog();
    endif;

    ?>
    <div ng-controller="PsPrinterListController" class="my-ps-printers__item col-xs-12 col-sm-6 col-md-4 p-b30">
        <div class="my-ps-printers__card m-b0 cardprinter-<?= $onePrinter->id; ?> printer-status-<?= $onePrinter->companyService->moderator_status; ?> <?= !$onePrinter->isAvailable() ? 'my-ps-printers__card--disabled' : ''; ?>">

            <div class="my-ps-printers__row border-0 p-t10">

                        <span class="my-ps-printers__tech-label label label-info" title="<?= _t('site.ps', '3D Printing'); ?>">
                            <?= _t('site.ps', '3D Printing'); ?>
                        </span>

                <h3 class="my-ps-printers__heading my-ps-printers__heading--printer" title="<?= \H($onePrinter->title); ?>">
                    <?= \H($onePrinter->title); ?>
                </h3>

                <div class="my-ps-printers__block m-t5 m-b5">
                    <strong><?= _t('site.ps', 'Status'); ?>:</strong> <span class="m-r5"><?= $onePrinter->companyService->getModeratorStatusLabel(); ?></span>

                    <?php if ($rejectReasonStr): ?>
                        <button type="button" class="btn btn-danger btn-sm p-l15 p-r15" data-toggle="modal" data-target=".modalRejectReason-<?= $onePrinter->id; ?>">
                            <span class="tsi tsi-warning-c m-r5"></span><?= _t('site.ps', 'Reject Reason'); ?>
                        </button>
                    <?php endif; ?>
                </div>

                <div class="my-ps-printers__block my-ps-printers__block--status m-b15">
                    <strong><?= _t('site.ps', 'Available'); ?>:</strong>
                    <label class="checkbox-switch checkbox-switch--xs m-r20">
                        <input
                                ng-init="prinerEnabled[<?= $onePrinter->id ?>]='<?= ($onePrinter->companyService->visibility == CompanyService::VISIBILITY_EVERYWHERE ? 'active' : 'inactive') ?>'"
                                ng-model="prinerEnabled[<?= $onePrinter->id ?>]"
                                ng-change="onPrinterEnabledChange(<?= $onePrinter->id ?>)"
                                ng-true-value="'active'"
                                ng-false-value="'inactive'"
                                type="checkbox">
                        <span class="slider"></span>
                        <span class="text" ng-show="prinerEnabled[<?= $onePrinter->id ?>]=='active'">
                                    <?= _t('yii', 'Yes'); ?>
                                </span>
                        <span class="text" ng-show="prinerEnabled[<?= $onePrinter->id ?>]=='inactive'">
                                    <?= _t('yii', 'No'); ?>
                                </span>
                    </label>
                </div>

                <div class="clearfix"></div>

                <?= Html::a(
                    '<span class="tsi tsi-pencil"></span>' . _t('site.ps', 'Edit'),
                    [
                        '/mybusiness/edit-printer/edit-printer',
                        'ps_id'         => $onePrinter->ps->id,
                        'ps_printer_id' => $onePrinter->id
                    ],
                    ['class' => 'my-ps-printers__edit']
                ) ?>
            </div>

            <div class="my-ps-printers__row my-ps-printers__row--stats p-t10">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="my-ps-printers__block my-ps-printers__block--test-order">
                            <?= _t('site.ps', 'Test Order'); ?> <?= \frontend\widgets\SiteHelpWidget::widget(['alias' => 'ps.order.test']) ?>
                        </div>
                        <div class="my-ps-printers__block my-ps-printers__block--test-order-btn">
                            <?php if (!$onePrinter->is_test_order_resolved && !$onePrinter->testOrder): ?>
                                <button
                                        loader-click="sendTestOrder(<?= $onePrinter->id ?>)"
                                        loader-click-unrestored="true"
                                        class="btn btn-primary btn-sm"
                                        type="button"><?= _t('site.ps.testorder', 'Request test order') ?></button>
                            <?php endif; ?>
                            <?php if (!$onePrinter->is_test_order_resolved && $onePrinter->testOrder): ?>
                                <?php
                                $attempt      = $onePrinter->testOrder->getAttemps()->one();
                                $testOrderUrl = '/workbench/service-order/view?id=' . $attempt->order_id;

                                ?>
                                <a href="<?= $testOrderUrl ?>" class="btn btn-primary btn-sm"
                                   type="button"><?= _t('site.ps.testorder', 'In process') ?></a>
                            <?php endif; ?>
                            <?php if ($onePrinter->is_test_order_resolved): ?>
                                <div class="p-t5 p-b5 text-success" style="min-width: 170px">
                                    <?= _t('site.ps.testorder', 'Resolved') ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="my-ps-printers__block my-ps-printers__block--certification">
                            <?php if ($onePrinter->isCertificated()): ?>
                                <?php echo Html::a(H(mb_ucfirst($onePrinter->printer->tsCertificationClass->title)), ['edit-printer/certificate-download', 'id' => $onePrinter->id]) ?>
                            <?php else: ?>
                                <?= H(mb_ucfirst($onePrinter->printer->tsCertificationClass->title)) ?>
                            <?php endif; ?>
                            <?= _t('site.ps', 'certification'); ?>
                            <?php echo frontend\widgets\SiteHelpWidget::widget(
                                ['title' => _t('site.ps', '3D Printer Certification'), 'alias' => 'ps.certification']
                            ); ?>
                            <br>
                            <span class="my-ps-printers__certification-status">
                                        <?php if ($onePrinter->isCertificated()) { ?>

                                            <?= $onePrinter->getTestStatusLabel() ?>
                                            <?= _t('site.ps', 'until ') .
                                            app()->formatter->asDate($onePrinter->companyService->certification_expire) ?>

                                            <?php
                                        } elseif ($onePrinter->companyService->isNotCertificated()) { ?>
                                            <?= Html::a(
                                                _t('site.ps', 'Get certified'),
                                                ['/mybusiness/edit-printer/test', 'ps_id' => $ps->id, 'ps_printer_id' => $onePrinter->id]
                                            ) ?>
                                        <?php } else { ?>
                                            <?= Html::a(
                                                $onePrinter->getTestStatusLabel(),
                                                ['/mybusiness/edit-printer/test', 'ps_id' => $ps->id, 'ps_printer_id' => $onePrinter->id]
                                            ) ?>
                                        <?php } ?>
                                    </span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="my-ps-printers__row p-b0 border-b0">
                <a target="_blank"
                   href="<?= \frontend\modules\mybusiness\components\PrinterReviewService::getReviewLink($onePrinter); ?>"
                   class="btn btn-default btn-sm m-r10">
                    <span class="tsi tsi-star m-r5"></span>
                    <?= \frontend\modules\mybusiness\components\PrinterReviewService::getReviewLinkLabel($onePrinter); ?>
                </a>
                <?php
                $review = \frontend\modules\mybusiness\components\PrinterReviewService::getReview($onePrinter);
                if ($review && $review->status == \common\models\PrinterReview::STATUS_PUBLISHED): ?>
                    <a href="<?= sprintf('/mybusiness/services/review?id=%d', $onePrinter->id); ?>"
                       class="btn btn-default btn-sm p-l10 p-r10"
                       title="<?= _t('site.ps', 'Edit review'); ?>">
                        <span class="tsi tsi-pencil m-r0"></span></a>
                <?php endif; ?>
            </div>

            <div class="my-ps-printers__row p-b10 border-b0">
                <button class="my-ps-printer__embed-widget" data-placement="bottom"
                        data-container="body"
                        data-toggle="popover"
                        data-original-title="<?php echo _t("site.share", "HTML Code"); ?>" ,
                        data-html="true"
                >
                    <span class="tsi tsi-link"></span> <?= _t('site.catalogps', 'Embed Code'); ?>
                    <div class="popover-content hide">
                        <input class="form-control input-sm" type="text"
                               value="<?= PsPrinterService::getPsPrinterWidgetHtmlCode($onePrinter) ?>">
                    </div>
                </button>
                <?= frontend\widgets\SiteHelpWidget::widget(['title' => 'Ps embed', 'alias' => 'ps.embed']); ?>
            </div>

        </div>
    </div>
    <?php if ($rejectReasonStr): ?>
    <div class="modal modal-danger fade modalRejectReason-<?= $onePrinter->id; ?>" tabindex="-1" role="dialog" aria-labelledby="mymodalRejectReason">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title" id="mymodalRejectReason"><?= _t('site.ps', 'Reject Reason'); ?></h3>
                </div>
                <div class="modal-body">
                    <?= $rejectReasonStr; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php endforeach ?>

<?php echo $this->render('reviewPrinterModal'); ?>