<div class="my-ps-info">

    <div class="container" ng-controller="PsPrinterListHeaderController">
        <div class="row">

            <?php
            if ($currentUser->status == $currentUser::STATUS_UNCONFIRMED) {
                ?>
                <div class="col-sm-12">
                    <div class="alert alert-warning alert--text-normal"><?=_t('site.ps',
                            'Please confirm your account to get your service approved.');?>
                    </div>
                </div>
            <?php  } ?>

            <div class="col-sm-8">
                <div class="my-ps-info__picture">
                    <img src="<?php echo $ps->getLogoUrl(false, 240); ?>" itemprop="image">
                </div>
                <div class="my-ps-info__data">
                    <h2 class="my-ps-info__title">
                        <?= H($ps->title); ?>
                    </h2>
                    <div class="my-ps-info__rating">
                        <a href="<?= $publicUrl; ?>#reviews" target="_blank">
                            <?= PsPrintServiceReviewStarsWidget::widget(['ps' => $ps]); ?>
                        </a>
                    </div>
                    <?php if ($ps->isActive()) { ?>
                        <button class="btn btn-link btn-sm m-t0 m-b0 my-ps-printer__embed-widget" data-placement="bottom" data-container="body"
                                data-toggle="popover"
                                data-original-title="<?php echo _t("site.share", "HTML Code"); ?>" ,
                                data-html="true"
                        >
                            <span class="tsi tsi-link"></span> <?=_t('site.catalogps', 'Embed Code');?>
                            <div class="popover-content hide">
                                <input class="form-control input-sm" type="text" value="<?= PsPrinterService::getPsWidgetHtmlCode($ps) ?>">
                            </div>
                        </button>
                        <?= frontend\widgets\SiteHelpWidget::widget(['title' => 'My Services Widget', 'alias' => 'ps.embed', 'class' => 'm-r10']); ?>
                        <?php
                        if ($ps->psFacebookPages) {
                            $pages = $ps->psFacebookPages;
                            $page = reset($pages);
                            ?>
                            <br /><a href="https://www.facebook.com/<?=$page->facebook_page_id;?>/app/<?=param('facebook_ps_print_application_id');?>/?ref=page_internal" target="_blank"><span class="soc-icons soc-icons-facebook"></span> Facebook App</a>

                            <?php
                        } else {
                            ?>
                            <button class="btn btn-link btn-sm p-l0 p-r0 my-ps-printer__facebook-widget" onclick="window.open('https://www.facebook.com/v2.1/dialog/pagetab?app_id=<?=param('facebook_ps_print_application_id');?>&display=popup&next=<?=$ourHost;?>/mybusiness/widgets', 'fb', 'width=600,height=200,top=150,left='+(screen.width/2 - 700/2))">
                                <span class="soc-icons soc-icons-facebook"></span>
                                <?= _t('site.ps', 'Add Facebook App'); ?>
                            </button>
                            <?php
                        }
                        ?>
                    <?php } ?>
                    <div class="my-ps-info__description">
                        <?= H($ps->description); ?>
                    </div>
                    <?php if ($ps->getIsRejected()): ?>
                        <div class="alert alert-warning alert--text-normal">
                            <?php
                            $rejectReasonStr = $ps->getLastRejectLog();
                            echo _t('site.ps', 'The information provided about your company details wasn\'t approved by the moderator. ');
                            echo '<br>';
                            if ($rejectReasonStr) {
                                echo _t('site.ps', 'Reason: ') . $rejectReasonStr . '. ';
                            }

                            echo Html::a(_t('site.ps', ' Please edit your company details'), ['/mybusiness/services/edit-ps']);
                            ?>
                        </div>
                    <?php endif; ?>
                    <div class="my-ps-info__btns">
                        <a href="/mybusiness/company/edit-ps" class="btn btn-primary btn-sm btn-ghost my-ps-info__edit-btn">
                            <span class="tsi tsi-pencil"></span>
                            <?= _t('site.ps', 'Edit My Service'); ?>
                        </a>
                        <a href="<?= $publicUrl; ?>" target="_blank" class="btn btn-default btn-sm my-ps-info__public-btn">
                            <?= _t('site.ps', 'Show Public Profile'); ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <?php if (($untestedPrinter = $ps->getFirstUntestedPrinter())):
                    $printerTestUrl = \yii\helpers\Url::toRoute(['/mybusiness/edit-printer/test', 'ps_id' => $ps->id, 'ps_printer_id' => $untestedPrinter->id]);
                    ?>
                    <h4 class="my-ps-info__cert-title">
                        <?= _t('site.ps', 'Certification Checklist') ?> &nbsp;
                        <a class="my-ps-info__link-benefits" href="#cert-benefits">
                            <?= _t('site.ps', 'Show Benefits') ?>
                        </a>
                    </h4>
                    <div class="ps-test">
                        <?php echo $this->render('certification/certification-checklist', ['user' => UserFacade::getCurrentUser(), 'ps' => $ps]); ?>
                        <?= Html::a(_t('site.ps', 'Get Certified'), ['/mybusiness/services/certification'], ['class' => 'btn btn-primary m-b20 my-ps-info__cert-btn']); ?>
                    </div>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>