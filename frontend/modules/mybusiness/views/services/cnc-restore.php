<?php

/** @var \yii\web\View $this */
/** @var \common\models\Ps $ps */
/** @var bool $fromCreatePs */


Yii::$app->angular
    ->service(['maps', 'geo', 'notify', 'router', 'user'])
    ->directive(['google-map', 'google-address-input'])
    ->resource(['Printer', 'PsPrinter'])
    ->controller('ps/printers/select-service-type');

$tabIndex = 1;
?>
<div>
    <div class="container m-t20">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2><?php echo _t('front.user', 'Restore deleted CNC Service?'); ?></h2>
                <div class="row alert alert-info">
                    <?=_t('site.ps', 'There is a deleted CNC Service. Do you want to restore and update it?');?>
                </div>
                <div class="row">
                    <a href="/mybusiness/services/restore-cnc?psMachineId=<?=$psMachineId;?>&flag=yes"  class="btn btn-primary btn-l"><?=_t('yii', 'Yes');?></a>

                    <a href="/mybusiness/services/add-cnc?onlyNew=1" class="btn btn-default"><?=_t('site.ps', 'No, create new one');?></a>
                </div>
            </div>
        </div>
    </div>
</div>