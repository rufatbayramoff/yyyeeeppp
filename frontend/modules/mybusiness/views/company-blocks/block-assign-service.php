<?php

/* @var $model \frontend\modules\mybusiness\models\CompanyBlockBindForm */
?>
<?php
/**
 * User: nabi
 */

use common\components\ArrayHelper;
use common\models\CompanyService;
use common\models\CompanyServiceCategory;
use common\models\Product;
use common\models\ProductCategory;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $blocks \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $products \yii\data\ActiveDataProvider */
/* @var $blockSearchModel \backend\models\search\CompanyBlockSearch */
/* @var $searchModel \backend\models\search\ProductSearch */
$serviceCategory = new CompanyServiceCategory();
$serviceCategoryList = $serviceCategory->getCategoryTree(CompanyServiceCategory::find()->withoutRoot()->orderBy('lft')->all(), '-');
?>

<div class="row"> 
    <div class="col-lg-12">
        <h3 class="m-t0"><?=_t('mybusiness.product', 'Assign company blocks');?></h3>
        <div class="table-responsive">
            <?php
            $columns = [
                'title',
                [
                    'label' => _t('site.catalog', 'Category'),
                    'attribute' => 'category_id',
                    'value'     => function (CompanyService $p) {
                        if (!$p->category_id) {
                            return null;
                        }
                        return $p->category->title;
                    },
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'category_id',
                        ArrayHelper::map($serviceCategoryList, 'id', 'title'),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                ],
            ];
            $blockModels = $blocks->getModels();
            foreach ($blockModels as $blockModel) {
                $columns[] = [
                    'class'  => 'yii\grid\CheckboxColumn',
                    'name'   => 'CompanyBlockBindForm[blockBind][' . $blockModel->id . ']',
                    'header' => sprintf('%s <small>%s</small>',
                        ' <input type="checkbox" class="selectblock-all" value="'.$blockModel->id.'" name="selectall['.$blockModel->id.']" />', $blockModel->title ),
                    'checkboxOptions' => function (CompanyService $model, $key, $index, $column) use ($blockModel) {
                        $checked = false;
                        $bindBlocksIds = $model->getBindedCompanyBlockIds();
                        if (in_array($blockModel->id, $bindBlocksIds)) {
                            $checked = 'checked';
                        }
                        return ['value' => $model->id, 'class'=>'selectblockitem selectblock-'.$blockModel->id, 'checked'=>$checked];
                    }
                ];
            }
            ?>
            <?= GridView::widget(
                [
                    'dataProvider' => $companyServicesDp,
                    'filterModel'  => $searchModel,
                    'columns'      => $columns
                ]
            ); ?>
        </div>
    </div>
    <?php $form = ActiveForm::begin(
        [
            'options' => ['enctype' => 'multipart/form-data'],
            'id' => 'block-bind-form',
            'action'  => '/mybusiness/company-blocks/bind-blocks-services'
        ]);
        $model->companyServiceIds = implode(", ", ArrayHelper::getColumn($companyServicesDp->query->all(), 'id'));
    ; ?>
    <?= $form->field($model, 'companyServiceIds')->hiddenInput()->label(false) ?>
    <div id="checkbox-elements" style="display:none;"></div>
    <div class="col-lg-4"></div>
    <div class="col-lg-8 m-b20" style="text-align:right;">
        <?= \yii\bootstrap\Html::submitButton(
            'Save',
            [
                'class' => 'btn btn-primary',
            ]
        ) ?>
    <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$this->registerJs("
    $('#block-bind-form').on('submit', function(){
        var names = [];
         $('input.selectblockitem:checked').each(function() {
            names[this.name] = names[this.name] || [];
			names[this.name].push(this.value);
			$('#checkbox-elements').append(this);
        });        
    });
    $('.selectblock-all').on('click', function(e, a){
        var blockId = e.currentTarget.value;
        $('.selectblock-' + blockId).removeAttr('checked');
        if(e.currentTarget.checked)
            $('.selectblock-' + blockId ).attr('checked', 'checked');
            
    });
");
