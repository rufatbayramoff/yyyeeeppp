<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlock */

$this->title = 'Update  Block: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Additional information', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="Company-block-update Company-edit product-edit">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
