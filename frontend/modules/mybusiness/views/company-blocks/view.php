<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlock */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Company Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Company-block-view Company-edit">

    <p class="m-b0">
        <?= Html::a('<span class="tsi tsi-left m-r10"></span>' . 'Return', ['index'], ['class' => 'btn btn-default']) ?>
    </p>

    <div class="row">
        <hr />
    </div>

    <h3 class="m-t0"><?=H($model->title);?></h3>

    <div class="ugc-content">
        <?=$model->content;?>

        <?php if ($videos = $model->getCompanyBlockVideos()): ?>
            <div class="row">
                <?php foreach ($videos as $video): ?>
                    <div class="col-sm-4">
                        <div class="video-container">
                            <object data="https://www.youtube.com/embed/<?= $video['videoId']; ?>"></object>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>



    <div class="m-t10">
        <?=$model->getImages()?
            \frontend\widgets\SwipeGalleryWidget::widget([
                'files' => $model->getImages(),
                'thumbSize' => [230, 180],
                'containerOptions' => ['class' => 'picture-slider'],
                'itemOptions' => ['class' => 'picture-slider__item'],
                'scrollbarOptions' => ['class' => 'picture-slider__scrollbar'],
            ]):'';
        ?>
    </div>

    <div class="row p-b10">
        <hr />
        <div class="col-sm-12">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm m-b10 m-r10']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-sm m-b10',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.ugc-content table').wrap('<div class="table-responsive m-t10"></div>');
    });
</script>