<?php

/* @var $model \frontend\modules\mybusiness\models\CompanyBlockBindForm */
?>
<?php
/**
 * User: nabi
 */

use common\components\ArrayHelper;
use common\models\Product;
use common\models\ProductCategory;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $blocks \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $products \yii\data\ActiveDataProvider */
/* @var $blockSearchModel \backend\models\search\CompanyBlockSearch */
/* @var $productSearchModel \backend\models\search\ProductSearch */
$serviceCategoryList = \common\models\ProductCategory::getActiveSelectCategories();
?>


   <!-- <div class="col-lg-4">
        <h3>Blocks</h3>
        <?/*= GridView::widget(
            [
                'dataProvider' => $blocks,
                'filterModel'  => $blockSearchModel,
                'columns'      => [

                    ['class' => 'yii\grid\CheckboxColumn', 'name' => 'CompanyBlockBindForm[blocks_id]'],
                    'title',
                    'position',
                    // 'company_id',
                    // 'videos',
                ],
            ]
        ); */?>
    </div>-->

    <h3 class="m-t0"><?=_t('mybusiness.product', 'Assign company blocks');?></h3>
    <div class="row">
        <div class="table-responsive">
            <?php
            $columns = [
                'title',
                [
                    'attribute' => 'category_id',
                    'value'     => function (Product $p) {
                        if (!$p->category_id) {
                            return null;
                        }
                        return $p->category->title;
                    },
                    'filter'    => Html::activeDropDownList(
                        $productSearchModel,
                        'category_id',
                        $serviceCategoryList,
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                ],
                #   'company_id',
              //  'updated_at:datetime',
                // 'published_at',
                // 'is_active',
                // 'single_price',
            ];
            $blockModels = $blocks->getModels();
            foreach ($blockModels as $blockModel) {
                $columns[] = [
                    'class'  => 'yii\grid\CheckboxColumn',
                    'name'   => 'CompanyBlockBindForm[blockBind][' . $blockModel->id . ']',
                    'header' => sprintf('<div class="checkbox checkbox-primary m-t0 m-b0">%s <label for="selectall['.$blockModel->id.']">%s</label>',
                        ' <input type="checkbox" class="selectblock-all" value="'.$blockModel->id.'" 
                        name="selectall['.$blockModel->id.']" id="selectall['.$blockModel->id.']" />', $blockModel->title ),
                    'checkboxOptions' => function (Product $model, $key, $index, $column) use ($blockModel) {
                        $checked = false;
                        $bindBlocksIds = $model->getBindedCompanyBlockIds();
                        if (in_array($blockModel->id, $bindBlocksIds)) {
                            $checked = 'checked';
                        }
                        return ['value' => $model->uuid, 'class'=>'selectblockitem selectblock-'.$blockModel->id, 'checked'=>$checked];
                    }
                ];
            }
            ?>
            <?= GridView::widget(
                [
                    'dataProvider' => $products,
                    'filterModel'  => $productSearchModel,
                    'columns'      => $columns
                ]
            ); ?>
        </div>
        <?php $form = ActiveForm::begin(
            [
                'options' => ['enctype' => 'multipart/form-data'],
                'id' => 'block-bind-form',
                'action'  => '/mybusiness/company-blocks/bind-blocks'
            ]);
            $model->productUuids = implode(", ", ArrayHelper::getColumn($products->query->all(), 'uuid'));
        ; ?>
        <?= $form->field($model, 'productUuids')->hiddenInput()->label(false) ?>
        <div id="checkbox-elements" style="display:none;"></div>
        <div class="col-lg-12 p-b20" style="text-align:right;">
            <?= \yii\bootstrap\Html::submitButton(
                'Bind',
                [
                    'class' => 'btn btn-primary',
                ]
            ) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php
$this->registerJs("
    $('#block-bind-form').on('submit', function(){
        var names = [];
         $('input.selectblockitem:checked').each(function() {
            names[this.name] = names[this.name] || [];
			names[this.name].push(this.value);
			$('#checkbox-elements').append(this);
        });        
    });
    $('.selectblock-all').on('click', function(e, a){
        var blockId = e.currentTarget.value;
        $('.selectblock-' + blockId).removeAttr('checked');
        if(e.currentTarget.checked)
            $('.selectblock-' + blockId ).attr('checked', 'checked');
            
    });
");
