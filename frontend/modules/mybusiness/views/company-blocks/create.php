<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlock */

$this->title = 'Add Information';
$this->params['breadcrumbs'][] = ['label' => 'Company Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Company-block-create Company-edit product-edit p-l0 p-r0">
    <div class="row">
        <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10">
            <h3 class="m-t0 m-b20"><?=_t('mybusiness.block', 'Add information block');?></h3>
        </div>
    </div>
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
