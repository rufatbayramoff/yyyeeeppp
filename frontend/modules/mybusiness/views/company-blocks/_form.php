<?php

use backend\widgets\FilesListWidget;
use common\components\FileTypesHelper;
use common\modules\company\serializers\CompanyBlockSerializer;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyBlock */
/* @var $form yii\widgets\ActiveForm */
/* @var $Company array|\common\models\Company|mixed|null|\yii\db\ActiveRecord */
if ($model->isNewRecord) {
    $model->is_visible = 1;
}


Yii::$app->angular
    ->service(['router', 'user', 'notify'])
    ->directive(['dropzone-button', 'fileread'])
    ->controller(
        [
            'company/companyBlock',
        ]
    )
    ->controllerParam('companyBlock', CompanyBlockSerializer::serialize($model));

Yii::$app->angular
    ->directive(['youtube-embed']);

?>

<div class="company-block-form" ng-controller="CompanyBlockController">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="Companyblock-title"><?= _t('site.store', 'Title') ?></label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="Companyblock-content"><?= _t('site.store', 'Content') ?></label>
            <div class="col-sm-9 col-md-10">
                <?= $form->field($model, 'content')->widget(
                    \dosamigos\ckeditor\CKEditor::className(),
                    \frontend\components\CKEditorDefault::getDefaults(['options' => ['rows' => 3]])
                )->label(false) ?>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"><?= _t('site.store', 'Videos') ?></label>
            <input type="hidden" name="CompanyBlock[videos]" id="companyblock-videos"/>
            <div class="col-sm-9 col-md-10">
                <div class="row m-b10">
                    <div class="col-sm-5 col-md-4">
                        <input class="form-control m-b10" type="text" size="50" ng-model="newVideoUrl" placeholder="YouTube link"/>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <button type="button" ng-click="companyBlock.addVideo(newVideoUrl);newVideoUrl=''"
                                class="btn btn-primary btn-ghost">
                            <?= _t('mybusiness.product', 'Add video'); ?>
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="product-edit__video-list">
                            <div ng-repeat="video in companyBlock.videos" class="product-edit__video-list-item">
                                <a ng-href="#" ng-click="companyBlock.showVideoPreview(video, 'js-previewArea')">
                                    <img ng-src="{{video.thumb}}"/>
                                </a>
                                <button class="product-edit__video-list-del" title="<?= _t('mybusiness.product', 'Delete video'); ?>" ng-click="companyBlock.removeVideo($index)">
                                    &times;
                                </button>
                            </div>
                        </div>
                        <div id="js-previewArea" onclick="document.getElementById('js-previewArea').innerHTML=''"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"><?= _t('site.store', 'Pictures') ?></label>
            <div class="col-sm-9 col-md-10 p-t10">
                <?= FilesListWidget::widget(
                    [
                        //    'emptyText' => '',
                        'formPrefix'             => $model->formName(),
                        'formAttribute'          => 'imageFiles',
                        'filesList'              => $model->getImages(),
                        'rights'                 => [
                            FilesListWidget::ALLOW_DELETE,
                            FilesListWidget::ALLOW_ADD,
                            FilesListWidget::ALLOW_MULTIPLE,
                        ],
                        'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS
                    ]
                ) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"><?= _t('site.store', 'Visibility') ?></label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($model, 'is_visible')->checkbox(['template' => '<div class="checkbox p-t10">{input}{label}</div>']) ?>
            </div>
        </div> <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"> </label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($model, 'is_public_profile')
                    ->checkbox(['template' => '<div class="checkbox">{input}{label}</div>']) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"></label>
            <div class="col-sm-6 col-md-5 p-b20">
                <?= Html::submitButton(
                    $model->isNewRecord ? 'Create' : 'Update',
                    [
                        'class'    => $model->isNewRecord ? 'btn btn-success js-clickProtect' : 'js-clickProtect btn btn-primary',
                        'ng-click' => 'beforeSubmit()',
                    ]
                ) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
