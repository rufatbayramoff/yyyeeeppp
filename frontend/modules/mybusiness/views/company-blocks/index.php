<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Custom Blocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-block-index company-edit">


    <div class="m-b10">

        <?= Html::a(_t('mybusiness.product', 'Add Block'), ['add'], ['class' => 'btn btn-success m-b10 m-r20']) ?>

        <div class="btn-group dropdown dropdown-primary dropdown-onhover m-b10">

            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= _t('mybusiness.product', 'Assign blocks'); ?>
                <span class="tsi tsi-down"></span>
            </button>
            <ul class="dropdown-menu">
                <li>
                    <?= Html::a(_t('mybusiness.product', 'Products'), ['assign-blocks-products'], ['class' => '']) ?>
                </li>
                <li>
                    <?= Html::a(_t('mybusiness.product', 'Services'), ['assign-blocks-services'], ['class' => '']) ?>
                </li>
            </ul>
        </div>

    </div>


    <div class="row">
        <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'format' => 'raw',
                        'value' => function($model){
                            $linkUp = Url::toRoute(['company-blocks/move', 'direction'=>'up', 'id'=>$model->id]);
                            $linkDown = Url::toRoute(['company-blocks/move', 'direction'=>'down', 'id'=>$model->id]);
                            $htmlUp = '<a href="'.$linkUp.'" class="btn btn-info btn-xs p-l10 p-r10"><span class="tsi tsi-up"></span></a>';
                            $htmlDown = '<a href="'.$linkDown.'"  class="btn btn-info btn-xs p-l10 p-r10"><span class="tsi tsi-down"></span></a>';
                            return $htmlUp . ' ' . $htmlDown;

                        }
                    ],
                    [
                        'attribute'=>'content',
                        'format' => 'raw',
                        'value' => function($model){
                            $text = $model->content; //\yii\helpers\StringHelper::truncateWords(strip_tags($model->content), 10) . '...<br/>';
                            $images = \backend\widgets\FilesListWidget::widget(
                                [
                                    'emptyText' => '',
                                    'formPrefix'             => $model->formName(),
                                    'formAttribute'          => 'imageFiles',
                                    'filesList'              => $model->getImages(),
                                ]
                            );
                            $videosHtml = [];
                            if ($videos = $model->getCompanyBlockVideos()):
                                foreach ($videos as $video):
                                    $videosHtml[] = Html::img($video['thumb']);
                                endforeach;
                            endif;
                            $videosHtml = implode(" ", $videosHtml);
                            $header = '<h4 class="m-t0 m-b0">' . Html::a(H($model->title), ['update', 'id'=>$model->id]) .'</h4>';
                            return '<div class="ugc-content">' . $header . $text . '</div>' . $videosHtml. $images;

                        }
                    ],
                    'is_visible:boolean',
                    [
                        'attribute' => 'is_public_profile',
                        'value' => function($model){
                            return $model->is_public_profile ? _t('app', 'Yes') : _t('app', 'No');
                        },
                        'label' => 'In public profile'
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
                'tableOptions' => [
                    'class' => 'table table-bordered m-b0 company-block-index__view-table'
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.ugc-content table').wrap('<div class="table-responsive m-t10"></div>');
    });
</script>