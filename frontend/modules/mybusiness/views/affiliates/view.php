<?php
/** @var \common\models\AffiliateSource $affiliateSource */
/** @var ActiveDataProvider $awardDataProvider */

$this->title = 'View Affiliate Award: ' . $affiliateSource->uuid;

use common\models\AffiliateAward;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

?>
<?= Html::A('Back', \common\modules\affiliate\helpers\AffiliateUrlHelper::sourcesList($affiliateSource->user->company), ['class' => 'btn btn-default']) ?>
<h3><?= _t('site.affiliate', 'Affiliate source') ?></h3>
<div class="row form-group">
    <div class="col-md-3"><b>Uuid:</b></div>
    <div class="col-md-9"><?= $affiliateSource->uuid ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Created at') ?>:</b></div>
    <div class="col-md-9"><?= app('formatter')->asDatetime($affiliateSource->created_at) ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Type') ?>:</b></div>
    <div class="col-md-9"><?= $affiliateSource->getFormattedType() ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Award type') ?>:</b></div>
    <div class="col-md-9"><?= $affiliateSource->getFormattedAwardType() ?></div>
</div>
<?php
if ($affiliateSource->isAwardTypeFixed()) {
    ?>
    <div class="row form-group">
        <div class="col-md-3"><b><?= _t('site.common', 'Fixed fee') ?>:</b></div>
        <div class="col-md-9"><?= $affiliateSource->getFixdedFee() ?></div>
    </div>
    <?php
}
?>
<?php
if ($affiliateSource->isAwardTypeFeePercent()) {
    ?>
    <div class="row form-group">
        <div class="col-md-3"><b><?= _t('site.common', 'Treatstock fee percent') ?>:</b></div>
        <div class="col-md-9"><?= $affiliateSource->getTreatstockFeePercent() ?> %</div>
    </div>
    <?php
}
?>

<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Utm mark') ?>:</b></div>
    <div class="col-md-9"><?= $affiliateSource->utm ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'URL') ?>:</b></div>
    <div class="col-md-9"><a href="<?= $affiliateSource->getUrl() ?>"><?= $affiliateSource->getUrl() ?></a></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Followers') ?>:</b></div>
    <div class="col-md-9"><?= $affiliateSource->getFollowersCount() ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Bind users') ?>:</b></div>
    <div class="col-md-9"><?= $affiliateSource->getBindUsersCount() ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Orders') ?>:</b></div>
    <div class="col-md-9"><?= $affiliateSource->getOrdersCount() ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Award') ?>:</b></div>
    <div class="col-md-9"><?php
        foreach ($affiliateSource->getAwards() as $award) {
            echo displayAsMoney($award) . ' ';
        }
        ?></a></div>
</div>
<h3><?= _t('site.affiliate', 'Statistics') ?></h3>


<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $awardDataProvider,
        'columns'      => [
            'id',
            'created_at',
            'invoice_uuid',
            [
                'attribute' => 'price',
                'value'     => function (AffiliateAward $award) {
                    return displayAsMoney($award->getPriceMoney());
                }
            ],
            [
                'label' => 'Status',
                'value' => function (AffiliateAward $award) {
                    return $award->getStatusText();
                }
            ],
        ],
        'tableOptions' => [
            'class' => 'table table-bordered m-b0 company-block-index__view-table'
        ],
    ]); ?>
</div>