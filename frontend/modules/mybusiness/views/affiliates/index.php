<?php

use common\models\AffiliateSource;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   =  _t('site.ps','Business Tools - Affiliates');
?>
<div class="company-block-index company-edit">

    <div class="designer-card">
        <div class="ps-share__icon ps-share__icon--embed m-r10">
            <i class="tsi tsi-link"></i>
        </div>
        <h2 class="designer-card__title">
            <?= _t('mybusiness.product', 'Affiliate link'); ?>
        </h2>
        <p class="lead">
            <?= _t('mybusiness.product', 'Share the affiliate link and earn a <b>50%</b> recurring commission from each completed order. Make money while you sleep.'); ?>
        </p>
        <div class="row">
            <div class="col-md-4">
                <?= Html::a(_t('mybusiness.product', 'Add affiliate link'), ['create'], ['class' => 'btn btn-primary m-b15']) ?>
            </div>
            <div class="col-md-8">
                <div class="chat-alert m-t0 m-b0">
                    <ol class="m-t0 m-b0 p-l20">
                        <li><?= _t('mybusiness.product', 'Click "Add Affiliate link"'); ?></li>
                        <li><?= _t('mybusiness.product', 'Create your own UTM, e.g. "header"'); ?></li>
                        <li><?= _t('mybusiness.product', ' Share the link with your friends/audience'); ?></li>
                    </ol>
                </div>
            </div>
        </div>

    </div>


    <div class="table-responsive m-b30">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => [
                'uuid',
                [
                    'attribute' => 'created_at',
                    'value'     => function (AffiliateSource $affiliateSource) {
                        return app('formatter')->asDate($affiliateSource->created_at);
                    }
                ],
                [
                    'attribute' => 'type',
                    'value'     => function (AffiliateSource $affiliateSource) {
                        return $affiliateSource->getFormattedType();
                    }
                ],
                'utm',
                [
                    'header' => _t('site.common', 'Followers'),
                    'value'  => function (AffiliateSource $affiliateSource) {
                        return $affiliateSource->getFollowersCount();
                    }
                ],
                [
                    'header' => _t('site.common', 'Bind users'),
                    'value'  => function (AffiliateSource $affiliateSource) {
                        return $affiliateSource->getBindUsersCount();
                    }
                ],
                [
                    'header' => _t('site.common', 'Orders'),
                    'value'  => function (AffiliateSource $affiliateSource) {
                        return $affiliateSource->getOrdersCount();
                    }
                ],
                [
                    'header' => _t('site.common', 'Award'),
                    'value'  => function (AffiliateSource $affiliateSource) {
                        $retVal = '';
                        foreach ($affiliateSource->getAwards() as $award) {
                            $retVal.=displayAsMoney($award).' ';
                        }
                        return $retVal;
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
            'tableOptions' => [
                'class' => 'table table-bordered m-b0 company-block-index__view-table'
            ],
        ]); ?>
    </div>


</div>
