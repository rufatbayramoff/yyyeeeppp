<?php

use frontend\assets\BoostrapRequireFieldAsset;
use frontend\modules\mybusiness\models\AffiliateCompanyForm;
use frontend\widgets\UserLocationWidget;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
/** @var AffiliateCompanyForm $model */
BoostrapRequireFieldAsset::register($this);
?>
<div class="row">
    <div class="col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2 m-t30">
        <div class="panel panel-default box-shadow border-0">
            <div class="panel-body">

                <h1 class="text-center m-t0 m-b10">
                    <?= _t('front.user', 'Become a Partner'); ?>
                </h1>

                <p class="text-center m-b20"><?php echo _t('site.ps','If you want to be registered as a supplier instead proceed to ')?><a href="/l/become-supplier" target="_blank">Create Business</a></p>
                <?php $form = ActiveForm::begin([
                    'id'     => 'affiliate-form',
                    'layout' => 'horizontal',
                    'method' => 'post',
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'wrapper' => 'col-sm-8'
                        ]
                    ]
                ])?>

                <?php echo $form->field($model, 'type')->checkboxList($model->types(),[
                        'item' => function($index, $label, $name, $checked, $value) {
                            return '<div class="checkbox">' . Html::input('checkbox',$name, $value).Html::label($label) . '</div>';
                        }
                ])?>

                <?php echo $form->field($model, 'website');?>
                <?php echo $form->field($model, 'title');?>
                <?php echo $form->field($model, 'fullName');?>
                <?php echo $form->field($model, 'about')->textarea(['rows'=> 3]);?>
                <div class="form-group">
                    <label class="control-label col-sm-4 p-t5" for="partner"><?= _t('front.user', 'Location'); ?><span class="form-required">*</span></label>
                    <div class="col-sm-5 col-md-5">
                        <div id="user-change-location-form">
                            <?php
                            echo UserLocationWidget::widget(
                                [
                                    'jsCallback' => 'function(btn, result, modalId) {
                                         if (result) {
                                            var text = result.location.city+", "+result.location.country;
                                            $(".ts-user-location").html(text);
                                            $("body").trigger("changeLocation", result.location);
                                            return true;
                                         };
                                    };'
                                ]
                            ); ?>
                        </div>
                    </div>
                </div>
                <div class="text-center m-t30 m-b10">
                    <?php echo Html::submitButton(_t('front.user', 'Become a Partner'), ['class' => 'btn btn-primary','ng-cl' => 'send']) ?>
                </div>
                <?php ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>