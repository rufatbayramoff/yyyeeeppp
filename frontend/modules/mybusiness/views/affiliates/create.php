<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $affiliateSource common\models\AffiliateAward */

$this->title = 'Create Affiliate link';
?>
<div class="affiliate-award-create">

    <h2 class="m-t0 m-b20"><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'affiliateSource' => $affiliateSource,
    ]) ?>
</div>
