<?php

use common\modules\affiliate\helpers\AffiliateUrlHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $affiliateSource common\models\AffiliateSource */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row form-group">
    <div class="col-md-3"><b>Uuid:</b></div>
    <div class="col-md-9"><?= $affiliateSource->uuid ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Created at') ?>:</b></div>
    <div class="col-md-9"><?= app('formatter')->asDatetime($affiliateSource->created_at) ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Type') ?>:</b></div>
    <div class="col-md-9"><?= $affiliateSource->getFormattedType() ?></div>
</div>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Award type') ?>:</b></div>
    <div class="col-md-9"><?= $affiliateSource->getFormattedAwardType() ?></div>
</div>

<?php $form = ActiveForm::begin(); ?>
<div class="row form-group">
    <div class="col-md-3"><b><?= _t('site.common', 'Utm mark') ?>:</b></div>
    <div class="col-md-9">
        <?= $form->field($affiliateSource, 'utm', ['template' => "{input}\n{hint}\n{error}"])->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row form-group">
    <div class="col-md-3"></div>
    <div class="col-md-9">
        <?= Html::submitButton($affiliateSource->isNewRecord ? 'Create' : 'Update', ['class' => $affiliateSource->isNewRecord ? 'btn btn-success m-r20' : 'btn btn-primary m-r20']) ?>
        <?= Html::A('Cancel', AffiliateUrlHelper::sourcesList($affiliateSource->user->company), ['class' => 'btn btn-default']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
