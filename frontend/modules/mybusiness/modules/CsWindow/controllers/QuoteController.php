<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\controllers;


use common\components\BaseController;
use common\components\Emailer;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\modules\CsWindow\components\CsWindowQuotePopulator;
use frontend\modules\mybusiness\modules\CsWindow\factories\CsWindowQuoteFactory;
use frontend\modules\mybusiness\modules\CsWindow\repositories\CsWindowQuoteRepository;
use frontend\modules\mybusiness\modules\CsWindow\repositories\CsWindowRepository;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class QuoteController
 * @package frontend\modules\mybusiness\modules\CsWindow\controllers
 *
 * @property CsWindowQuoteRepository $quoteWindowRepository
 * @property CsWindowRepository $windowRepository
 * @property CsWindowQuoteFactory $quoteFactory
 * @property CsWindowQuotePopulator $quotePopulator
 * @property Emailer $emailer
 */
class QuoteController extends BaseController
{
    public $windowRepository;

    public $quoteWindowRepository;

    public $quoteFactory;

    public $quotePopulator;

    public $emailer;

    public function injectDependencies(
        CsWindowRepository $windowRepository,
        CsWindowQuoteRepository $quoteWindowRepository,
        CsWindowQuoteFactory $quoteFactory,
        CsWindowQuotePopulator $quotePopulator,
        Emailer $emailer
    ) {
        $this->windowRepository = $windowRepository;
        $this->quoteWindowRepository = $quoteWindowRepository;
        $this->quoteFactory = $quoteFactory;
        $this->quotePopulator = $quotePopulator;
        $this->emailer = $emailer;

    }

    /**
     * @param $serviceUid
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSaveCalcQuoteInSession($serviceUid)
    {
        $csWindowSnapshot = $this->windowRepository->getSnapshotByUid($serviceUid);

        if (!$csWindowSnapshot) {
            throw new NotFoundHttpException(_t('site.services', 'Service not found or inactive'));
        }

        $this->quoteWindowRepository->saveCalcQuoteInSession($csWindowSnapshot, app()->request->post('csWindowQuote'));

        return $this->jsonSuccess(['csWindowQuote' => []]);
    }


    /**
     * @param $serviceUid
     *
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     * @throws \Exception
     */
    public function actionSaveQuote($serviceUid)
    {
        $csWindowSnapshot = $this->windowRepository->getSnapshotByUid($serviceUid);

        if (!$csWindowSnapshot) {
            throw new NotFoundHttpException(_t('csWindowCalculator', 'Service not found or inactive'));
        }

        $user = $this->getCurrentUser();

        $data = app()->request->post('csWindowQuote');
        $data = !\is_array($data) ? Json::decode($data) : $data;

        $windowQuote = $this->quoteFactory->createWindowQuote($csWindowSnapshot);

        $this->quotePopulator
            ->populateFromJson($windowQuote, $data)
            ->populateUserSession($windowQuote, UserFacade::getUserSession());

        if ($user) {
            $this->quotePopulator->populateUser($windowQuote, $user);
        }

        if ($windowQuote->hasErrors() || !$windowQuote->validate()) {
            return $this->jsonReturn([
                'success'          => false,
                'validationErrors' => $windowQuote->getErrors(),
            ]);
        }

        $this->quoteWindowRepository->save($windowQuote);
        $this->quoteWindowRepository->clearCalcQuoteInSession($csWindowSnapshot);
        $this->emailer->sendQuoteWindowCalculator($windowQuote);

        return $this->jsonSuccess();
    }
}