<?php

namespace frontend\modules\mybusiness\modules\CsWindow;

/**
 * services-window module definition class
 */
class CsWindowModule extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\mybusiness\modules\CsWindow\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
