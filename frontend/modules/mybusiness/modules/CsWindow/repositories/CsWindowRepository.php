<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\repositories;

use common\components\DateHelper;
use common\models\CsWindow;
use common\models\CsWindowSnapshot;
use frontend\modules\mybusiness\modules\CsWindow\services\CsWindowService;

/**
 * Class CsWindowRepository
 *
 * @package frontend\modules\mybusiness\modules\CsWindow\repositories
 *
 * @property CsWindowService $windowService
 */
class CsWindowRepository
{
    public $windowService;

    public function injectDependencies(CsWindowService $windowService)
    {
        $this->windowService = $windowService;
    }

    public function save(CsWindow $csWindow)
    {
        $csWindow->updated_at = DateHelper::now();
        $relatedRecords = $csWindow->relatedRecords;
        $csWindow->safeSave();

        if (array_key_exists('csWindowProfiles', $relatedRecords)) {
            foreach ($relatedRecords['csWindowProfiles'] as $record) {
                if ($record->isDeleted) {
                    $record->delete();
                    continue;
                }
                $record->safeSave();
            }
        }
        if (array_key_exists('csWindowGlasses', $relatedRecords)) {
            foreach ($relatedRecords['csWindowGlasses'] as $record) {
                if ($record->isDeleted) {
                    $record->delete();
                    continue;
                }
                $record->safeSave();
            }
        }
        if (array_key_exists('csWindowFurnitures', $relatedRecords)) {
            foreach ($relatedRecords['csWindowFurnitures'] as $record) {
                if ($record->isDeleted) {
                    $record->delete();
                    continue;
                }
                $record->safeSave();
            }
        }
        if (array_key_exists('csWindowLocations', $relatedRecords)) {
            foreach ($relatedRecords['csWindowLocations'] as $record) {
                if ($record->isDeleted) {
                    $record->delete();
                    continue;
                }
                $record->safeSave();
            }
        }
    }

    /**
     * @param $id
     * @return CsWindow|null
     */
    public function get($id)
    {
        $csWindow = CsWindow::findOne(['id' => $id]);
        return $csWindow;
    }

    /**
     * @param $uid
     * @return CsWindow|null
     */
    public function getByUid($uid)
    {
        $csWindow = CsWindow::findOne(['uid' => $uid]);
        return $csWindow;
    }


    /**
     * @param $uid
     *
     * @return CsWindowSnapshot|null
     */
    public function getSnapshotByUid($uid): ?CsWindowSnapshot
    {
        return CsWindowSnapshot::findOne(['uid' => $uid]);
    }
    /**
     * @param CsWindow $csWindow
     *
     * @return CsWindowSnapshot
     * @throws \Exception
     */
    public function getLatestSnapshot(CsWindow $csWindow): CsWindowSnapshot
    {
        $snapshot = CsWindowSnapshot::find()
            ->where([
                'cs_window_uid' => $csWindow->uid
            ])
            ->orderBy(['created_at' => SORT_DESC])
            ->one();

        return $snapshot ?: $this->windowService->createSnapshot($csWindow);
    }
}