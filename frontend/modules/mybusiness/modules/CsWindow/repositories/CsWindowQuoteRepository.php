<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\repositories;


use common\components\DateHelper;
use common\models\CsWindowQuote;
use common\models\CsWindowQuoteItem;
use common\models\CsWindowSnapshot;
use frontend\modules\mybusiness\modules\CsWindow\components\CsWindowQuotePopulator;
use frontend\modules\mybusiness\modules\CsWindow\factories\CsWindowQuoteFactory;

/**
 * Class CsWindowQuoteRepository
 *
 * @package frontend\modules\mybusiness\modules\CsWindow\repositories
 *
 * @property CsWindowQuotePopulator $quotePopulator
 * @property CsWindowQuoteFactory $quoteFactory
 */
class CsWindowQuoteRepository
{
    public const CALC_QUOTE_SESSION_NAME = '__saveCalcWindowQuoteData';

    public const CALC_WINDOW_SESSION_SNAPSHOT = '__saveCalcWindowSnapshotUid';

    protected $quotePopulator;

    protected $quoteFactory;

    public function injectDependencies(
        CsWindowQuotePopulator $quotePopulator,
        CsWindowQuoteFactory $quoteFactory
    ) {
        $this->quotePopulator = $quotePopulator;
        $this->quoteFactory = $quoteFactory;
    }

    public function setSessionSnapshotUid($snapshotUid): void
    {
        app()->session->set(static::CALC_WINDOW_SESSION_SNAPSHOT, $snapshotUid);
    }

    public function getSessionSnapshotUid(): ?string
    {
        return app()->session->get(static::CALC_WINDOW_SESSION_SNAPSHOT);
    }

    /**
     * @param CsWindowSnapshot $csWindowSnapshot
     * @param $data
     *
     * @return mixed
     */
    public function saveCalcQuoteInSession(CsWindowSnapshot $csWindowSnapshot, $data)
    {
        $session = app()->session;
        $csWindowTempData = $session->get(static::CALC_QUOTE_SESSION_NAME) ?? [];

        $csWindowTempData[$csWindowSnapshot->cs_window_uid] = $data;

        app()->session->set(static::CALC_QUOTE_SESSION_NAME, $csWindowTempData);

        return $csWindowTempData[$csWindowSnapshot->cs_window_uid];
    }

    /**
     * @param CsWindowSnapshot $csWindowSnapshot
     */
    public function clearCalcQuoteInSession(CsWindowSnapshot $csWindowSnapshot): void
    {
        $session = app()->session;
        $csWindowTempData = $session->get(static::CALC_QUOTE_SESSION_NAME) ?? [];

        if (isset($csWindowTempData[$csWindowSnapshot->cs_window_uid])) {
            unset($csWindowTempData[$csWindowSnapshot->cs_window_uid]);
        }
    }

    /**
     * @param CsWindowSnapshot $csWindowSnapshot
     *
     * @return \common\models\CsWindowQuote|null
     * @throws \Exception
     */
    public function getCalcQuoteInSessionData(CsWindowSnapshot $csWindowSnapshot): ?CsWindowQuote
    {
        $session = app()->session;
        $csWindowTempData = $session->get(static::CALC_QUOTE_SESSION_NAME);

        $data = $csWindowTempData[$csWindowSnapshot->cs_window_uid] ?? null;

        if ($data) {
            $windowQuote = $this->quoteFactory->createWindowQuote($csWindowSnapshot);
            $this->quotePopulator->populateFromJson($windowQuote, $data);
            return $windowQuote;
        }

        return null;
    }

    /**
     * @param CsWindowQuote $windowQuote
     * @throws \common\components\exceptions\BusinessException
     */
    public function save(CsWindowQuote $windowQuote): void
    {
        $windowQuote->updated_at = DateHelper::now();
        $relatedRecords = $windowQuote->relatedRecords;
        $windowQuote->safeSave();

        if (array_key_exists('csWindowQuoteItems', $relatedRecords)) {
            /** @var CsWindowQuoteItem $record */
            foreach ($relatedRecords['csWindowQuoteItems'] as $record) {
                $record->safeSave();
            }
        }
    }

    /**
     * @param $uid
     *
     * @return CsWindowQuote
     * @throws \yii\web\NotFoundHttpException
     */
    public function getTryByUid($uid): CsWindowQuote
    {
        return CsWindowQuote::tryFind(['uid' => $uid]);
    }
}