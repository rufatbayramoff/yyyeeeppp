<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\factories;

use common\components\DateHelper;
use common\components\UuidHelper;
use common\models\CsWindowQuote;
use common\models\CsWindowQuoteItem;
use common\models\CsWindowSnapshot;

class CsWindowQuoteFactory
{
    /**
     * @param CsWindowSnapshot $snapshot
     *
     * @return CsWindowQuote
     * @throws \Exception
     */
    public function createWindowQuote(CsWindowSnapshot $snapshot)
    {
        $windowQuote = new CsWindowQuote();

        $windowQuote->uid = substr(UuidHelper::generateUuid(), 0, 10) . '';
        $windowQuote->created_at = DateHelper::now();
        $windowQuote->company_service_id = $snapshot->csWindowU->company_service_id;
        $windowQuote->cs_window_snapshot_uid = $snapshot->uid;

        return $windowQuote;
    }

    /**
     * @param CsWindowQuote $quote
     *
     * @return CsWindowQuoteItem
     * @throws \Exception
     */
    public function createWindowQuoteItem(CsWindowQuote $quote)
    {
        $windowQuoteItem = new CsWindowQuoteItem();

        $windowQuoteItem->uid = substr(UuidHelper::generateUuid(), 0, 10) . '';
        $windowQuoteItem->cs_window_quote_uid = $quote->uid;

        return $windowQuoteItem;
    }
}