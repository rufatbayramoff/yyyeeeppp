<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\factories;

use common\components\DateHelper;
use common\components\UuidHelper;
use common\models\Company;
use common\models\CompanyService;
use common\models\CsWindow;
use common\models\CsWindowSnapshot;
use common\modules\company\components\CompanyServiceInterface;
use common\modules\company\factories\CompanyServiceFactory;

class CsWindowFactory
{
    /**
     * @var CompanyServiceFactory
     */
    public $companyServiceFactory;

    public function injectDependencies(CompanyServiceFactory $companyServiceFactory)
    {
        $this->companyServiceFactory = $companyServiceFactory;
    }

    public function create()
    {
        // @TODO
    }

    public function createFromSnapshot($csWindowSnapshot)
    {
        // @TODO
    }

    /**
     * @param CompanyService $companyService
     * @return CsWindow
     * @throws \Exception
     */
    public function createDraft(CompanyService $companyService)
    {
        $csWindow                     = new CsWindow();
        $csWindow->uid                = substr(UuidHelper::generateUuid(), 0, 10) . '';
        $csWindow->created_at         = DateHelper::now();
        $csWindow->company_service_id = $companyService->id;
        $csWindow->measurement        = 'm';
        $csWindow->populateRelation('companyService', $companyService);
        return $csWindow;
    }

    /**
     * auto create company service for window manufacturing
     *
     * @param Company $company
     * @return CompanyService
     */
    public function createCompanyService(Company $company)
    {
        $serviceDraft              = $this->companyServiceFactory->createEmptyService($company);
        $serviceDraft->type        = CompanyServiceInterface::TYPE_WINDOW;
        $serviceDraft->category_id = null;
        return $serviceDraft;
    }

    /**
     * @param CsWindow $csWindow
     *
     * @return CsWindowSnapshot
     * @throws \Exception
     */
    public function createSnapshot(CsWindow $csWindow): CsWindowSnapshot
    {
        $snapshot = new CsWindowSnapshot();

        $snapshot->uid           = substr(UuidHelper::generateUuid(), 0, 10) . '';
        $snapshot->created_at    = DateHelper::now();
        $snapshot->cs_window_uid = $csWindow->uid;

        $profile = [];

        foreach ($csWindow->csWindowProfiles as $p) {
            $profile[] = $p->toArray();
        }

        $glass = [];

        foreach ($csWindow->csWindowGlasses as $g) {
            $glass[] = $g->toArray();
        }

        $furniture = [];

        foreach ($csWindow->csWindowFurnitures as $f) {
            $furniture[] = $f->toArray();
        }

        $snapshot->profile   = $profile;
        $snapshot->glass     = $glass;
        $snapshot->furniture = $furniture;
        $snapshot->service   = $csWindow->toArray();

        return $snapshot;
    }
}