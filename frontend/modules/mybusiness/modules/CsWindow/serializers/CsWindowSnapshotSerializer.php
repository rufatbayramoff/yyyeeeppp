<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\CompanyService;
use common\models\CsWindow;
use common\models\CsWindowFurniture;
use common\models\CsWindowGlass;
use common\models\CsWindowLocation;
use common\models\CsWindowProfile;
use common\models\CsWindowSnapshot;
use common\models\ModerLog;
use common\models\UserLocation;

class CsWindowSnapshotSerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CsWindowProfile::class   => [
                'id',
                'isDeleted',
                'title',
                'thickness',
                'chambers',
                'max_glass',
                'max_width',
                'max_height',
                'noise_reduction',
                'thermal_resistance',
                'price' => function (CsWindowProfile $el) {
                    return (float)$el->price;
                },
            ],
            CsWindowGlass::class     => [
                'id',
                'isDeleted',
                'title',
                'thickness',
                'chambers',
                'noise_reduction',
                'thermal_resistance',
                'price' => function (CsWindowGlass $el) {
                    return (float)$el->price;
                },
            ],
            CsWindowFurniture::class => [
                'id',
                'isDeleted',
                'title',
                'price'         => function (CsWindowFurniture $el) {
                    return (float)$el->price;
                },
                'price_swivel'  => function (CsWindowFurniture $el) {
                    return (float)$el->price_swivel;
                },
                'price_folding' => function (CsWindowFurniture $el) {
                    return (float)$el->price_folding;
                },
            ],
            CsWindowLocation::class  => [
                'id',
                'isDeleted',
                'location',
                'radius',
            ],
            CsWindowSnapshot::class  => [
                'uid'                => function (CsWindowSnapshot $sp) {
                    return $sp->uid;
                },
                'measurement'        => function (CsWindowSnapshot $sp) {
                    return $sp->snapshotCsWindow->measurement;
                },
                'windowsill'         => function (CsWindowSnapshot $sp) {
                    return (float)$sp->snapshotCsWindow->windowsill;
                },
                'lamination'         => function (CsWindowSnapshot $sp) {
                    return (float)$sp->snapshotCsWindow->lamination;
                },
                'slopes'             => function (CsWindowSnapshot $sp) {
                    return (float)$sp->snapshotCsWindow->slopes;
                },
                'tinting'            => function (CsWindowSnapshot $sp) {
                    return (float)$sp->snapshotCsWindow->tinting;
                },
                'energy_saver'       => function (CsWindowSnapshot $sp) {
                    return (float)$sp->snapshotCsWindow->energy_saver;
                },
                'installation'       => function (CsWindowSnapshot $sp) {
                    return $sp->snapshotCsWindow->installation;
                },
                'profile'            => 'snapshotCsWindowProfiles',
                'glass'              => 'snapshotCsWindowGlasses',
                'furniture'          => 'snapshotCsWindowFurnitures',
                'locations'          => 'snapshotCsWindow.csWindowLocations',
                'currency'           => function () {
                    return '$';
                }
            ]
        ];
    }
}