<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\CompanyService;
use common\models\CsWindow;
use common\models\CsWindowFurniture;
use common\models\CsWindowGlass;
use common\models\CsWindowLocation;
use common\models\CsWindowProfile;
use common\models\ModerLog;
use common\models\UserLocation;

class CsWindowSerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CompanyService::class    => [
                'id',
                'isPublished'          => function (CompanyService $companyService) {
                    return $companyService->isPublished();
                },
                'title'                => function (CompanyService $companyService) {
                    return $companyService->getTitleLabel();
                },
                'visibility'           => function (CompanyService $companyService) {
                    return $companyService->visibility;
                },
                'visibilityLabel'      => function (CompanyService $companyService) {
                    return $companyService->getVisibilityLabel();
                },
                'moderatorStatusLabel' => function (CompanyService $companyService) {
                    $note = '';
                    if ($companyService->moderator_status === CompanyService::MODERATOR_STATUS_REJECTED) {
                        $note = ModerLog::getLastRejectModerLog(ModerLog::TYPE_SERVICE, $companyService->id);
                    }
                    return sprintf('%s %s', $companyService->getModeratorStatusLabel(), $note);
                },
            ],
            CsWindow::class          => [
                'id',
                'uid',
                'company_service_id',
                'measurement',
                'windowsill'     => function (CsWindow $el) {
                    return (float)$el->windowsill;
                },
                'lamination'     => function (CsWindow $el) {
                    return (float)$el->lamination;
                },
                'slopes'         => function (CsWindow $el) {
                    return (float)$el->slopes;
                },
                'tinting'        => function (CsWindow $el) {
                    return (float)$el->tinting;
                },
                'energy_saver'   => function (CsWindow $el) {
                    return (float)$el->energy_saver;
                },
                'installation',
                'status',
                'is_active',
                'companyService' => 'companyService',
                'profile'        => 'csWindowProfiles',
                'glass'          => 'csWindowGlasses',
                'furniture'      => 'csWindowFurnitures',
                'locations'      => 'csWindowLocations',
                'currency'       => function () {
                    return '$'; // just for now
                }
            ],
            CsWindowProfile::class   => [
                'id',
                'isDeleted',
                'title',
                'thickness',
                'chambers',
                'max_glass',
                'max_width',
                'max_height',
                'noise_reduction',
                'thermal_resistance',
                'price' => function (CsWindowProfile $el) {
                    return (float)$el->price;
                },
            ],
            CsWindowGlass::class     => [
                'id',
                'isDeleted',
                'title',
                'thickness',
                'chambers',
                'noise_reduction',
                'thermal_resistance',
                'price' => function (CsWindowGlass $el) {
                    return (float)$el->price;
                },
            ],
            CsWindowFurniture::class => [
                'id',
                'isDeleted',
                'title',
                'price'         => function (CsWindowFurniture $el) {
                    return (float)$el->price;
                },
                'price_swivel'  => function (CsWindowFurniture $el) {
                    return (float)$el->price_swivel;
                },
                'price_folding' => function (CsWindowFurniture $el) {
                    return (float)$el->price_folding;
                },
            ],
            UserLocation::class      => [
                'id',
                'country_id',
                'region' => function (UserLocation $location) {
                    return $location->getUserDataField('region');
                },
                'city'   => function (UserLocation $location) {
                    return $location->getUserDataField('city');
                },
                'formatted_address',
                'address',
                'address2',
                'lat',
                'lon',
                'zip_code'
            ],
            CsWindowLocation::class  => [
                'id',
                'isDeleted',
                'location',
                'radius',
            ],
        ];
    }
}