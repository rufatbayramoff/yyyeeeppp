<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\CsWindowQuote;
use common\models\CsWindowQuoteItem;

class CsWindowQuoteSerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CsWindowQuoteItem::class => [
                'uid',
                'title',
                'quote_parameters',
                'qty',
                'cost',
                'frame_id',
                'glass_id',
                'profile_id',
                'furniture_id',
                'windowsill'   => function (CsWindowQuoteItem $el) {
                    return (bool)$el->windowsill;
                },
                'lamination'   => function (CsWindowQuoteItem $el) {
                    return (bool)$el->lamination;
                },
                'slopes'        => function (CsWindowQuoteItem $el) {
                    return (bool)$el->slopes;
                },
                'tinting'      => function (CsWindowQuoteItem $el) {
                    return (bool)$el->tinting;
                },
                'energy_saver' => function (CsWindowQuoteItem $el) {
                    return (bool)$el->energy_saver;
                },
                'installation' => function (CsWindowQuoteItem $el) {
                    return (bool)$el->installation;
                }
            ],
            CsWindowQuote::class => [
                'uid',
                'cs_window_snapshot_uid',
                'phone',
                'email',
                'contact_name',
                'address',
                'notes',
                'total_price',
                'currency',
                'measurement',
                'status',
                'baseMeasurement' => function (CsWindowQuote $el) {
                    return ($el->csWindowSnapshotU ? $el->csWindowSnapshotU->snapshotCsWindow->measurement === 'm' : null) ? 'mm' : 'inch';
                },
                'items' => 'csWindowQuoteItems'
            ]
        ];
    }
}