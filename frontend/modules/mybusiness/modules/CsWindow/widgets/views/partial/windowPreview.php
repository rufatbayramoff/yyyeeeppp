<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 18.10.18
 * Time: 14:44
 */
?>
<script type="text/ng-template" id="windowPreview_1">
    <div class="window-widget__window">
        <div class="window-widget__window-item">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="1">
                <div ng-if="quote.frame.hasDirection(1, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(1, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(1)">
                    {{quote.frame.getTextDirection(1)}}
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/ng-template" id="windowPreview_2">
    <div class="window-widget__window">
        <div class="window-widget__window-item window-widget__window-item--double">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="2">
                <div ng-if="quote.frame.hasDirection(2, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(2, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(2)">
                    {{quote.frame.getTextDirection(2)}}
                </div>
            </div>
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="3">
                <div ng-if="quote.frame.hasDirection(3, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(3, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(3)">
                    {{quote.frame.getTextDirection(3)}}
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/ng-template" id="windowPreview_3">
    <div class="window-widget__window">
        <div class="window-widget__window-item">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="1">
                <div ng-if="quote.frame.hasDirection(1, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(1, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(1)">
                    {{quote.frame.getTextDirection(1)}}
                </div>
            </div>
        </div>

        <div class="window-widget__window-item">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="2">
                <div ng-if="quote.frame.hasDirection(2, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(2, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(2)">
                    {{quote.frame.getTextDirection(2)}}
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/ng-template" id="windowPreview_4">
    <div class="window-widget__window">
        <div class="window-widget__window-item window-widget__window-item--double">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="2">
                <div ng-if="quote.frame.hasDirection(2, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(2, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(2)">
                    {{quote.frame.getTextDirection(2)}}
                </div>
            </div>
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="3">
                <div ng-if="quote.frame.hasDirection(3, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(3, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(3)">
                    {{quote.frame.getTextDirection(3)}}
                </div>
            </div>
        </div>

        <div class="window-widget__window-item window-widget__window-item--double">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="5">
                <div ng-if="quote.frame.hasDirection(5, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(5, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(5)">
                    {{quote.frame.getTextDirection(5)}}
                </div>
            </div>
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="6">
                <div ng-if="quote.frame.hasDirection(6, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(6, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(6)">
                    {{quote.frame.getTextDirection(6)}}
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/ng-template" id="windowPreview_5">
    <div class="window-widget__window">
        <div class="window-widget__window-item window-widget__window-item--double window-widget__window-item--double-wide">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="2">
                <div ng-if="quote.frame.hasDirection(2, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(2, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(2)">
                    {{quote.frame.getTextDirection(2)}}
                </div>
            </div>
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="3">
                <div ng-if="quote.frame.hasDirection(3, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(3, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(3)">
                    {{quote.frame.getTextDirection(3)}}
                </div>
            </div>
        </div>

        <div class="window-widget__window-item window-widget__window-item--double">
            <div class="window-widget__window-sash"></div>
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="4">
                <div ng-if="quote.frame.hasDirection(4, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(4, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(4)">
                    {{quote.frame.getTextDirection(4)}}
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/ng-template" id="windowPreview_6">
    <div class="window-widget__window">
        <div class="window-widget__window-item">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="1">
                <div ng-if="quote.frame.hasDirection(1, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(1, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(1)">
                    {{quote.frame.getTextDirection(1)}}
                </div>
            </div>
        </div>

        <div class="window-widget__window-item">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="2">
                <div ng-if="quote.frame.hasDirection(2, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(2, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(2)">
                    {{quote.frame.getTextDirection(2)}}
                </div>
            </div>
        </div>

        <div class="window-widget__window-item">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="3">
                <div ng-if="quote.frame.hasDirection(3, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(3, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(3)">
                    {{quote.frame.getTextDirection(3)}}
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/ng-template" id="windowPreview_7">
    <div class="window-widget__window">
        <div class="window-widget__window-item">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="1">
                <div ng-if="quote.frame.hasDirection(1, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(1, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(1)">
                    {{quote.frame.getTextDirection(1)}}
                </div>
            </div>
        </div>

        <div class="window-widget__window-item window-widget__window-item--double">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="3">
                <div ng-if="quote.frame.hasDirection(3, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(3, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(3)">
                    {{quote.frame.getTextDirection(3)}}
                </div>
            </div>
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="4">
                <div ng-if="quote.frame.hasDirection(4, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(4, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(4)">
                    {{quote.frame.getTextDirection(4)}}
                </div>
            </div>
        </div>

        <div class="window-widget__window-item window-widget__window-item--double">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="6">
                <div ng-if="quote.frame.hasDirection(6, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(6, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(6)">
                    {{quote.frame.getTextDirection(6)}}
                </div>
            </div>
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="7">
                <div ng-if="quote.frame.hasDirection(7, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(7, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(7)">
                    {{quote.frame.getTextDirection(7)}}
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/ng-template" id="windowPreview_8">
    <div class="window-widget__window">
        <div class="window-widget__window-item">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="1">
                <div ng-if="quote.frame.hasDirection(1, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(1, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(1)">
                    {{quote.frame.getTextDirection(1)}}
                </div>
            </div>
        </div>

        <div class="window-widget__window-item window-widget__window-item--double window-widget__window-item--double-wide">
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="3">
                <div ng-if="quote.frame.hasDirection(3, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(3, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(3)">
                    {{quote.frame.getTextDirection(3)}}
                </div>
            </div>
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="4">
                <div ng-if="quote.frame.hasDirection(4, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(4, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(4)">
                    {{quote.frame.getTextDirection(4)}}
                </div>
            </div>
        </div>

        <div class="window-widget__window-item window-widget__window-item--double">
            <div class="window-widget__window-sash"></div>
            <div class="window-widget__window-sash js-windowChangeOpenDirection" data-section-id="5">
                <div ng-if="quote.frame.hasDirection(5, 'swivel')" class="window-widget__swivel"></div>
                <div ng-if="quote.frame.hasDirection(5, 'folding')" class="window-widget__folding"></div>
                <div class="window-widget__window-type" ng-if="quote.frame.getTextDirection(5)">
                    {{quote.frame.getTextDirection(5)}}
                </div>
            </div>
        </div>
    </div>
</script>
