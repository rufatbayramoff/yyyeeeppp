<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 27.09.18
 * Time: 9:59
 *
 * @var \common\models\CompanyService $companyService
 * @var array $windowFrames
 * @var \frontend\components\FrontendWebView $this
 * @var \frontend\modules\mybusiness\modules\CsWindow\repositories\CsWindowRepository $windowRepository
 * @var \common\models\CsWindowSnapshot $windowLatestSnapshot
 * @var \common\models\CsWindowQuote $windowQuote
 */

use frontend\assets\AngularJsSliderAsset;
use frontend\modules\mybusiness\modules\CsWindow\serializers\CsWindowQuoteSerializer;
use frontend\modules\mybusiness\modules\CsWindow\serializers\CsWindowSnapshotSerializer;

$this->registerCssFile('@web/css/window-widget.css');
AngularJsSliderAsset::register($this);

Yii::$app->angular
    ->service(['notify', 'user', 'modal', 'router', 'maps', 'geo'])
    ->directive(['dropzone-image-crop', 'dropzone-button', 'fileread', 'image-rotate', 'google-map', 'google-address-input'])
    ->controller([
        'store/common-models',
        'company/window/CsWindowModels',
        'company/window/quote/CsWindowQuoteModels',
        'company/window/CsWindowCalculatorController',
        'company/window/CsWindowCalculatorService',
        'company/window/directives/WindowCalculatorFrame',
    ])
    ->controllerParams([
        'csWindow'      => CsWindowSnapshotSerializer::serialize($windowLatestSnapshot),
        'csWindowQuote' => CsWindowQuoteSerializer::serialize($windowQuote),
        'windowFrames'  => $windowFrames
    ])
    ->additionalModules(['rzModule']);

$logo = $companyService->getLogoImage();

?>
<div class="window-widget" ng-controller="CsWindowCalculatorController" ng-cloak>
    <div class="window-widget__header row">

        <h2 class="window-widget__title ugc-content">
            <?php if($logo): ?>
                <a target="_blank" class="window-widget__avatar" href="<?php echo $companyService->getPublicPageUrl();?>">
                    <img src="<?=$logo;?>" />
                </a>
            <?php endif; ?>

            <a target="_blank" href="<?php echo $companyService->getPublicPageUrl();?>">
                <?php echo _t('csWindowCalculator', '{manufacturerName} window calculator', ['manufacturerName' => $companyService->getTitleLabel()]); ?>
            </a>

        </h2>

        <a href="https://treatstock.com" class="window-widget__copy" target="_blank">
            <span>Powered by</span>
            <img src="https://static.treatstock.com/static/images/ts-logo.svg" width="120px" height="15px">
        </a>
    </div>

    <div class="window-widget__variants" ng-if="!checkCalculatorStep('success')">
        <div ng-repeat="windowQuote in csWindowQuote.quoteItems"
             ng-click="setQuoteActive(windowQuote)"
             class="window-widget__variants-item"
             ng-class="{'active' : csWindowQuote.isQuoteActive(windowQuote)}"
        >
            <div ng-if="windowQuote.hasErrors()" class="window-widget__variants-errors" title="<?= _t('csWindowCalculator', 'Fix errors'); ?>">!</div>

            <button class="window-widget__variants-del"
                    type="button"
                    ng-if="calculatorParam.showAddWindowButton"
                    ng-click="removeWindowQuote(windowQuote)" title="Delete">&times;</button>

            <div class="window-widget__variants-price">
                {{windowQuote.view.getCost()}}</div>

            <window-calculator-frame
                    quote="windowQuote"
                    tpl-id="{{csWindowFrame.getHtmlId(windowQuote.frame.id)}}"
            ></window-calculator-frame>

            <div class="window-widget__variants-caption">
                <strong title="{{windowQuote.title}}">{{windowQuote.title}}  
                    <span class="label-info control-label-sm"><b>x{{windowQuote.qty}}</b></span>
                </strong>
                <span>{{windowQuote.view.getProfileSize()}}</span>
            </div>
        </div>

        <div class="window-widget__variants-item window-widget__variants-item--add"
             ng-if="calculatorParam.showAddWindowButton"
             ng-click="createQuote()">
            <div class="window-widget__variants-add">
                <span class="tsi tsi-plus"></span>
            </div>
            <div class="window-widget__variants-caption">
                <strong><?php echo _t('csWindowCalculator', 'Add window'); ?></strong>
            </div>
        </div>
    </div>

    <div ng-if="checkCalculatorStep('calculator')">
        <div class="window-widget__body" ng-if="csWindowQuote.quoteItemActive">
            <div class="row">
                <div class="col-sm-6 wide-padding wide-padding--right">

                    <div class="window-widget__window-name">

                        <div ng-class="{'has-error': csWindowQuote.quoteItemActive.getError('title')}">
                            <input type="text"
                                   class="form-control"
                                   ng-model="csWindowQuote.quoteItemActive.title"
                                   ng-change="csWindowQuote.quoteItemActive.clearError('title')"
                                   placeholder="<?php echo _t('csWindowCalculator', 'Window name'); ?>"
                            >
                        </div>
                        <div class="btn-group btn-switcher window-widget__measure-switch" data-toggle="buttons">
                            <label class="btn btn-sm btn-default"
                                   ng-class="{'active': csWindowQuote.getMeasurement() == 'mm'}"
                                   ng-click="csWindowQuote.setMeasurement('mm')"
                            >
                                <input type="radio" name="measurement" autocomplete="off">
                                <?php echo _t('csWindowCalculator', 'mm'); ?>
                            </label>
                            <label class="btn btn-sm btn-default"
                                   ng-class="{'active': csWindowQuote.getMeasurement() == 'inch'}"
                                   ng-click="csWindowQuote.setMeasurement('inch')"
                            >
                                <input type="radio" name="measurement" autocomplete="off">
                                <?php echo _t('csWindowCalculator', 'inch'); ?>
                            </label>
                        </div>

                    </div>

                    <div class="window-widget__customize">
                        <window-calculator-frame
                            quote="csWindowQuote.quoteItemActive"
                            tpl-id="{{csWindowFrame.getHtmlId(csWindowQuote.quoteItemActive.frame.id)}}"
                            window-type-switch-click="true"
                        ></window-calculator-frame>

                        <div class="window-widget__size">
                            <rzslider rz-slider-model="csWindowQuote.quoteItemActive.quoteParameters.profile.width"
                                      rz-slider-options="csWindowQuote.quoteItemActive.view.sliderSizeWidthOptions"></rzslider>

                            <div class="input-group input-group-sm window-widget__size-input" ng-class="{'has-error': csWindowQuote.quoteItemActive.getError('profileWidth')}">
                                <input type="text" class="form-control"
                                       ng-model="csWindowQuote.quoteItemActive.quoteParameters.profile.width"
                                       ng-change="csWindowQuote.quoteItemActive.sizeOnChange()"
                                       placeholder="{{csWindowQuote.quoteItemActive.view.sliderSizeWidthOptions.ceil}}">
                                <span class="input-group-addon">{{csWindowQuote.getMeasurement()}}</span>
                            </div>
                            <div class="window-widget__size-clear">
                                {{csWindowQuote.quoteItemActive.view.getProfileSizeConvertRevers(csWindowQuote.quoteItemActive.view.getQuoteProfileWidth())}}
                            </div>
                        </div>

                        <div class="window-widget__size window-widget__size--vertical">
                            <rzslider rz-slider-model="csWindowQuote.quoteItemActive.quoteParameters.profile.height"
                                      rz-slider-options="csWindowQuote.quoteItemActive.view.sliderSizeHeightOptions"></rzslider>

                            <div class="input-group input-group-sm window-widget__size-input"  ng-class="{'has-error': csWindowQuote.quoteItemActive.getError('profileHeight')}">
                                <input type="text" class="form-control"
                                       ng-model="csWindowQuote.quoteItemActive.quoteParameters.profile.height"
                                       ng-change="csWindowQuote.quoteItemActive.sizeOnChange()"
                                       placeholder="{{csWindowQuote.quoteItemActive.view.sliderSizeHeightOptions.ceil}}">
                                <span class="input-group-addon">{{csWindowQuote.getMeasurement()}}</span>
                            </div>
                            <div class="window-widget__size-clear">
                                {{csWindowQuote.quoteItemActive.view.getProfileSizeConvertRevers(csWindowQuote.quoteItemActive.view.getQuoteProfileHeight())}}
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-6">
                    <div class="window-widget__sash">
                        <div class="window-widget__sash-item"
                             ng-repeat="frame in csWindowFrame.frame"
                             ng-click="csWindowQuote.quoteItemActive.setFrame(frame)"
                             ng-class="{'active' : frame.id === csWindowQuote.quoteItemActive.frame.id}"
                        >
                            <window-calculator-frame tpl-id="{{csWindowFrame.getHtmlId(frame.id)}}"></window-calculator-frame>
                        </div>
                    </div>

                    <table class="form-table form-table--top-label window-widget__params">
                        <tr class="form-table__row" ng-if="csWindow.haveProfile()">
                            <td class="form-table__label">
                                <b><?php echo _t('csWindowCalculator', 'Profile'); ?></b>
                            </td>
                            <td class="form-table__data p-b10">
                                <div data-toggle="buttons">
                                    <div class="btn btn-pill"
                                         ng-repeat="profile in csWindow.profile"
                                         ng-class="{'active': profile.id === csWindowQuote.quoteItemActive.profile.id}"
                                         ng-click="setQuoteProfile(profile)"
                                    >
                                        <input type="radio" name="profile" autocomplete="off"/>{{profile.title}}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="form-table__row" ng-if="csWindow.haveGlassByThickness(csWindowQuote.quoteItemActive.profile.getMaxGlass())">
                            <td class="form-table__label">
                                <b><?php echo _t('csWindowCalculator', 'Glasses'); ?></b>
                            </td>
                            <td class="form-table__data p-b10">
                                <div data-toggle="buttons">
                                    <div class="btn btn-pill"
                                         ng-repeat="glass in csWindow.getGlassListByThickness(csWindowQuote.quoteItemActive.profile.getMaxGlass())"
                                         ng-class="{'active': glass.id === csWindowQuote.quoteItemActive.glass.id}"
                                         ng-click="setCorrectGlass(glass)"
                                    >
                                        <input type="radio" name="glass" autocomplete="off"/>{{glass.title}}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="form-table__row" ng-if="csWindowQuote.quoteItemActive.frame.hasDirectionParams()">
                            <td class="form-table__label">
                                <b><?php echo _t('csWindowCalculator', 'Hardware'); ?></b>
                            </td>
                            <td class="form-table__data p-b0">
                                <div data-toggle="buttons">
                                    <div class="btn btn-pill"
                                         ng-repeat="furniture in csWindow.furniture"
                                         ng-class="{'active': furniture.id === csWindowQuote.quoteItemActive.furniture.id}"
                                         ng-click="csWindowQuote.quoteItemActive.setFurniture(furniture)"
                                    >
                                        <input type="radio" name="furniture" autocomplete="off"/>{{furniture.title}}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__data" colspan="2">
                                <label class="checkbox-switch checkbox-switch--xs">
                                    <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.windowsill" ng-change="csWindowQuote.quoteItemActive.optionsOnChange()">
                                    <span class="slider"></span>
                                    <span class="text"><?php echo _t('csWindowCalculator', 'Windowsill'); ?></span>
                                </label>
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__data" colspan="2">
                                <label class="checkbox-switch checkbox-switch--xs">
                                    <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.lamination" ng-change="csWindowQuote.quoteItemActive.optionsOnChange()">
                                    <span class="slider"></span>
                                    <span class="text"><?php echo _t('csWindowCalculator', 'Lamination'); ?></span>
                                </label>
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__data" colspan="2">
                                <label class="checkbox-switch checkbox-switch--xs">
                                    <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.slopes" ng-change="csWindowQuote.quoteItemActive.optionsOnChange()">
                                    <span class="slider"></span>
                                    <span class="text"><?php echo _t('csWindowCalculator', 'Jambs'); ?></span>
                                </label>
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__data" colspan="2">
                                <label class="checkbox-switch checkbox-switch--xs">
                                    <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.tinting" ng-change="csWindowQuote.quoteItemActive.optionsOnChange()">
                                    <span class="slider"></span>
                                    <span class="text"><?php echo _t('csWindowCalculator', 'Tinting'); ?></span>
                                </label>
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__data" colspan="2">
                                <label class="checkbox-switch checkbox-switch--xs">
                                    <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.energySaver" ng-change="csWindowQuote.quoteItemActive.optionsOnChange()">
                                    <span class="slider"></span>
                                    <span class="text"><?php echo _t('csWindowCalculator', 'Energy Saver'); ?></span>
                                </label>
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__data" colspan="2">
                                <label class="checkbox-switch checkbox-switch--xs">
                                    <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.installation" ng-change="csWindowQuote.quoteItemActive.optionsOnChange()">
                                    <span class="slider"></span>
                                    <span class="text"><?php echo _t('csWindowCalculator', 'Installation'); ?></span>
                                </label>
                            </td>
                        </tr>
                    </table>

                    <div class="window-widget__qty" ng-class="{'has-error': csWindowQuote.quoteItemActive.getError('qty')}">
                        <label><?= _t('csWindowCalculator', 'Number of windows'); ?></label>
                        <input type="number"
                               class="form-control input-sm"
                               ng-model="csWindowQuote.quoteItemActive.qty"
                               ng-change="csWindowQuote.quoteItemActive.clearError('qty')"
                               placeholder="1"
                        >
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div ng-if="checkCalculatorStep('form')">

        <div class="window-widget__body">
            <div class="row">
                <div class="col-sm-8 col-md-6">
                    <h2 class="m-t0"><?php echo _t('csWindowCalculator', 'Checkout'); ?></h2>

                    <h3 class="window-widget__summary-price">{{csWindowQuote.getTotalCostFormat()}}</h3>

                    <table class="form-table">
                        <tbody>
                        <tr class="form-table__row">
                            <td class="form-table__label">
                                <b><?php echo _t('csWindowCalculator', 'Contact name'); ?></b>
                            </td>
                            <td class="form-table__data">
                                <input type="text" class="form-control" ng-model="csWindowQuote.contact_name" required autocomplete="off">
                                <p class="help-block help-block-error" id="windowCalculator-contact_name"></p>
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__label">
                                <b><?php echo _t('csWindowCalculator', 'Email'); ?></b>
                            </td>
                            <td class="form-table__data">
                                <input type="email" class="form-control" ng-model="csWindowQuote.email" required autocomplete="off">
                                <p class="help-block help-block-error" id="windowCalculator-email"></p>
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__label">
                                <b><?php echo _t('csWindowCalculator', 'Phone'); ?></b>
                            </td>
                            <td class="form-table__data">
                                <input type="text" class="form-control" ng-model="csWindowQuote.phone" autocomplete="off">
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__label">
                                <b><?php echo _t('csWindowCalculator', 'Address'); ?></b>
                            </td>
                            <td class="form-table__data">
                                <input type="text" class="form-control" ng-model="csWindowQuote.address" autocomplete="off">
                            </td>
                        </tr>
                        <tr class="form-table__row">
                            <td class="form-table__label">
                                <b><?php echo _t('csWindowCalculator', 'Notes'); ?></b>
                            </td>
                            <td class="form-table__data">
                                <textarea class="form-control" rows="3" ng-model="csWindowQuote.notes" id="textArea"></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="window-widget__footer" ng-if="csWindowQuote.hasQuotes()">
        <div ng-if="checkCalculatorStep('calculator')">
            <div class="window-widget__summary">
                <h3 class="window-widget__summary-price">{{csWindowQuote.getTotalCostFormat()}}</h3>

                <button ng-click="stepProceedToCheckout()" class="btn btn-danger window-widget__summary-btn"><?php echo _t('csWindowCalculator', 'Proceed to checkout'); ?></button>
            </div>
        </div>

        <div ng-if="checkCalculatorStep('form')">
            <div class="window-widget__footer-btns">
                <button ng-click="stepCalculator()" class="btn btn-default">Back</button>
                <button ng-click="saveQuote()" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>

    <div ng-if="checkCalculatorStep('success')">
        <div class="window-widget__body">
            <h2 class="text-center m-t10"><?php echo _t("csWindowCalculator", "Checkout complete"); ?></h2>

            <p class="text-center">
                <span style='font-size: 96px; color:#82c015;' class='tsi tsi-checkmark-c'></span>
            </p>

            <p class="text-center">
                <strong>
                    <?php echo _t('csWindowCalculator', 'Total Amount'); ?>: {{csWindowQuote.total_price.amountWithCurrency()}}
                </strong>
            </p>

            <p class="text-center m-b30">
                <a ng-click="stepCalculator(true)" class="btn btn-primary"><?= _t('site.store', 'Back to calculator'); ?></a>
            </p>
        </div>
    </div>
</div>

<?php echo $this->render('partial/windowPreview');?>