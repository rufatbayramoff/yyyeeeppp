<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 27.09.18
 * Time: 9:59
 */

namespace frontend\modules\mybusiness\modules\CsWindow\widgets;

use frontend\modules\mybusiness\modules\CsWindow\repositories\CsWindowQuoteRepository;
use frontend\modules\mybusiness\modules\CsWindow\repositories\CsWindowRepository;
use yii\base\Widget;
use common\models\CompanyService;

/**
 * Class CsWindowCalculatorWidget
 * @package frontend\modules\mybusiness\modules\CsWindow\widgets
 *
 * @property CompanyService $companyService
 * @property CsWindowRepository $windowRepository
 * @property CsWindowQuoteRepository $windowQuoteRepository
 */
class CsWindowCalculatorWidget extends Widget
{
    public $companyService;

    protected $windowRepository;

    protected $windowQuoteRepository;

    public function injectDependencies(
        CsWindowRepository $windowRepository,
        CsWindowQuoteRepository $windowQuoteRepository
    ) {
        $this->windowRepository = $windowRepository;
        $this->windowQuoteRepository = $windowQuoteRepository;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $windowLatestSnapshot = null;
        $snapshotUid = $this->windowQuoteRepository->getSessionSnapshotUid();

        //$windowLatestSnapshot = $snapshotUid ? $this->windowRepository->getSnapshotByUid($snapshotUid) : null;
        $windowLatestSnapshot = $this->windowRepository->getLatestSnapshot($this->companyService->csWindow);

       /* if (!$windowLatestSnapshot) {
            $windowLatestSnapshot = $this->windowRepository->getLatestSnapshot($this->companyService->csWindow);
            $this->windowQuoteRepository->setSessionSnapshotUid($windowLatestSnapshot->uid);
        }*/

        $windowQuote = $this->windowQuoteRepository->getCalcQuoteInSessionData($windowLatestSnapshot);

        return $this->render('csWindowCalculator', [
            'companyService'       => $this->companyService,
            'windowFrames'         => self::getWindowFrames(),
            'windowLatestSnapshot' => $windowLatestSnapshot,
            'windowQuote'          => $windowQuote
        ]);
    }

    /**
     * @return array
     */
    public static function getWindowFrames(): array
    {
        return [
            [
                'id' => 1,
                'sections'       => [
                    ['id' => 1, 'width' => 100, 'height' => 100, 'openDirection' => []]
                ]
            ],
            [
                'id' => 2,
                'sections'       => [
                    [
                        'id' => 1,
                        'width'    => 100,
                        'height'   => 100,
                        'sections' => [
                            ['id' => 2, 'width' => 100, 'height' => 30, 'openDirection' => []],
                            ['id' => 3, 'width' => 100, 'height' => 70, 'openDirection' => []]
                        ],
                        'openDirection' => []
                    ]
                ]
            ],
            [
                'id' => 3,
                'sections'       => [
                    ['id' => 1, 'width' => 50, 'height' => 100, 'openDirection' => []],
                    ['id' => 2, 'width' => 50, 'height' => 100, 'openDirection' => []],
                ]
            ],
            [
                'id' => 4,
                'sections'       => [
                    [
                        'id' => 1,
                        'width'    => 50,
                        'height'   => 100,
                        'sections' => [
                            ['id' => 2, 'width' => 100, 'height' => 30, 'openDirection' => []],
                            ['id' => 3, 'width' => 100, 'height' => 70, 'openDirection' => []]
                        ],
                        'openDirection' => []
                    ],
                    [
                        'id' => 4,
                        'width'    => 50,
                        'height'   => 100,
                        'sections' => [
                            ['id' => 5, 'width' => 100, 'height' => 30, 'openDirection' => []],
                            ['id' => 6, 'width' => 100, 'height' => 70, 'openDirection' => []]
                        ],
                        'openDirection' => []
                    ],
                ]
            ],
            [
                'id' => 5,
                'sections'       => [
                    [
                        'id'       => 1,
                        'width'    => 100,
                        'height'   => 100,
                        'sections' => [
                            ['id' => 2, 'width' => 100, 'height' => 30, 'openDirection' => []],
                            ['id' => 3, 'width' => 50, 'height' => 70, 'openDirection' => []],
                            ['id' => 4, 'width' => 50, 'height' => 70, 'openDirection' => []]
                        ],
                        'openDirection' => []
                    ]
                ]
            ],
            [
                'id' => 6,
                'sections'       => [
                    ['id' => 1, 'width' => 33.333, 'height' => 100, 'openDirection' => []],
                    ['id' => 2, 'width' => 33.333, 'height' => 100, 'openDirection' => []],
                    ['id' => 3, 'width' => 33.333, 'height' => 100, 'openDirection' => []]
                ]
            ],
            [
                'id' => 7,
                'sections'       => [
                    ['id' => 1, 'width' => 33.333, 'height' => 100, 'openDirection' => []],
                    [
                        'id' => 2,
                        'width'    => 33.333,
                        'height'   => 100,
                        'sections' => [
                            ['id' => 3, 'width' => 100, 'height' => 30, 'openDirection' => []],
                            ['id' => 4, 'width' => 100, 'height' => 70, 'openDirection' => []]
                        ],
                        'openDirection' => []
                    ],
                    [
                        'id' => 5,
                        'width'    => 33.333,
                        'height'   => 100,
                        'sections' => [
                            ['id' => 6, 'width' => 100, 'height' => 30, 'openDirection' => []],
                            ['id' => 7, 'width' => 100, 'height' => 70, 'openDirection' => []]
                        ],
                        'openDirection' => []
                    ]
                ]
            ],
            [
                'id' => 8,
                'sections'       => [
                    ['id' => 1, 'width'  => 33.333, 'height' => 100, 'openDirection' => []],
                    [
                        'id' => 2,
                        'width'    => 66.666,
                        'height'   => 100,
                        'sections' => [
                            ['id' => 3, 'width' => 100, 'height' => 30, 'openDirection' => []],
                            ['id' => 4, 'width' => 50, 'height' => 70, 'openDirection' => []],
                            ['id' => 5, 'width' => 50, 'height' => 70, 'openDirection' => []]
                        ],
                        'openDirection' => []
                    ],
                ]
            ]
        ];
    }
}