<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 18.10.18
 * Time: 11:34
 */

namespace frontend\modules\mybusiness\modules\CsWindow\models;

use common\models\CompanyService;
use common\models\CsWindowQuote;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CsWindowQuoteSearch extends CsWindowQuote
{
    public $search;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'uid', 'search', 'email', 'contact_name', 'address', 'phone', 'notes'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function formName()
    {
        return '';
    }

    /**
     * @param array $params
     *
     * @return null|ActiveDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search($params):? ActiveDataProvider
    {
        $query = CsWindowQuote::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        $this->load($params, $this->formName());

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->where([
            'and',
            ['=', 'company_service_id', $this->company_service_id],
            ['!=', 'status', CsWindowQuote::STATUS_DELETED]
        ]);

        $query->andFilterWhere([
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid]);

        if ($this->search) {
            $query->andFilterWhere([
                'or',
                ['like', 'uid', $this->search],
                ['like', 'status', $this->search],
                ['like', 'email', $this->search],
                ['like', 'contact_name', $this->search],
                ['like', 'address', $this->search],
                ['like', 'phone', $this->search],
                ['like', 'notes', $this->search],
            ]);
        }

        $dataProvider->sort = [
            'defaultOrder' => [
                'created_at' => SORT_DESC
            ]
        ];

        return $dataProvider;
    }

    /**
     * @param CompanyService $companyService
     */
    public function setCompanyService(CompanyService $companyService): void
    {
        $this->company_service_id = $companyService->id;
    }
}