<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\components;

use common\components\AbstractPopulator;
use common\models\CsWindowQuote;
use common\models\User;
use common\models\UserSession;
use frontend\modules\mybusiness\modules\CsWindow\factories\CsWindowQuoteFactory;

/**
 * Class CsWindowPopulator
 *
 * @package frontend\modules\mybusiness\modules\CsWindow\components
 * @property CsWindowQuoteFactory $quoteFactory
 */
class CsWindowQuotePopulator extends AbstractPopulator
{
    protected $quoteFactory;

    public function injectDependencies(CsWindowQuoteFactory $quoteFactory)
    {
        $this->quoteFactory = $quoteFactory;
    }

    /**
     * @param CsWindowQuote $windowQuote
     * @param array $dataJson
     *
     * @return CsWindowQuotePopulator
     * @throws \Exception
     */
    public function populateFromJson(CsWindowQuote $windowQuote, array $dataJson): self
    {
        $this
            ->populateBase($windowQuote, $dataJson)
            ->populateItems($windowQuote, $dataJson['items'] ?? []);

        return $this;
    }

    /**
     * @param CsWindowQuote $windowQuote
     * @param $data
     *
     * @return $this
     */
    public function populateBase(CsWindowQuote $windowQuote, $data): self
    {
        $windowQuote->setAttributes([
            'phone'                  => $data['phone'] ?? null,
            'email'                  => $data['email'] ?? null,
            'cs_window_snapshot_uid' => $data['cs_window_snapshot_uid'] ?? null,
            'contact_name'           => $data['contact_name'] ?? null,
            'address'                => $data['address'] ?? null,
            'notes'                  => $data['notes'] ?? null,
            'total_price'            => $data['total_price'] ?? null,
            'currency'               => $data['currency'] ?? null,
            'measurement'            => $data['measurement'] ?? null,
        ]);

        return $this;
    }


    /**
     * @param CsWindowQuote $windowQuote
     * @param $items
     *
     * @return CsWindowQuotePopulator
     * @throws \Exception
     */
    public function populateItems(CsWindowQuote $windowQuote, $items): self
    {
        $relatedModels = [];

        foreach ($items as $item) {
            $itemModel = $this->quoteFactory->createWindowQuoteItem($windowQuote);
            $itemModel->load($item, '');

            $itemModel->windowsill   = $item['windowsill'] ? 1 : 0;
            $itemModel->lamination   = $item['lamination'] ? 1 : 0;
            $itemModel->slopes       = $item['slopes'] ? 1 : 0;
            $itemModel->tinting      = $item['tinting'] ? 1 : 0;
            $itemModel->energy_saver = $item['energy_saver'] ? 1 : 0;
            $itemModel->installation = $item['installation'] ? 1 : 0;

            if ($itemModel->validate()) {
                $relatedModels[] = $itemModel;
            } else {
                $windowQuote->addError('csWindowQuoteItems', $itemModel->getErrorSummary(true));
            }
        }

        $windowQuote->populateRelation('csWindowQuoteItems', $relatedModels);

        return $this;
    }

    /**
     * @param CsWindowQuote $windowQuote
     * @param User $user
     *
     * @return $this
     */
    public function populateUser(CsWindowQuote $windowQuote, User $user): ?self
    {
        $windowQuote->user_id = $user->id;
        return $this;
    }

    /**
     * @param CsWindowQuote $windowQuote
     * @param UserSession $userSession
     *
     * @return $this
     */
    public function populateUserSession(CsWindowQuote $windowQuote, UserSession $userSession): ?self
    {
        $windowQuote->user_session_id = $userSession->id;
        return $this;
    }
}