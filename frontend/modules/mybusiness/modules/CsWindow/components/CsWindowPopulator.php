<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\components;


use common\components\AbstractPopulator;
use common\components\ArrayHelper;
use common\models\CsWindow;
use common\models\CsWindowFurniture;
use common\models\CsWindowGlass;
use common\models\CsWindowLocation;
use common\models\CsWindowProfile;
use common\models\factories\UserLocationFactory;
use common\models\UserLocation;

/**
 * Class CsWindowPopulator
 *
 * @package frontend\modules\mybusiness\modules\CsWindow\components
 */
class CsWindowPopulator extends AbstractPopulator
{
    /**
     * @param CsWindow $csWindow
     * @param array $dataJson
     * @return $this
     */
    public function populateFromJson(CsWindow $csWindow, array $dataJson)
    {
        $this->populateBase($csWindow, $dataJson);

        $user = $csWindow->companyService->company->user;
        $relations = [
            'profile'   => [
                'relation' => 'csWindowProfiles',
                'modelClass'    => CsWindowProfile::class
            ],
            'glass'     => [
                'relation' => 'csWindowGlasses',
                'modelClass'    => CsWindowGlass::class
            ],
            'locations' => [
                'relation'       => 'csWindowLocations',
                'modelClass'          => CsWindowLocation::class,
                'beforeValidate' => function ($row, CsWindowLocation $csLocation) use ($user, $csWindow) {
                    $location = null;
                    if (!empty($row['location']['id'])) {
                        $location = UserLocation::findOne(['id' => $row['location']['id'], 'user_id' => $user['id']]);
                    }
                    if (empty($row['location'])) {
                        $csWindow->addError('csWindowLocations', _t('cswindow.editor', 'Please specify location'));
                        return;
                    } else {
                        if (!empty($row['location']['updated']) || empty($row['location']['id'])) { // create new one
                            $location = UserLocationFactory::createFromAddressData($user, $row['location']);
                            $location->location_type = UserLocation::LOCATION_TYPE_LOCATION;
                            if ($location->validate()) {
                                $location->safeSave();
                            } else {
                                $csWindow->addError('csWindowLocations', $location->getFirstError());
                                return;
                            }
                        }
                    }

                    $csLocation->populateRelation('location', $location);
                    $csLocation->location_id = $location->id;

                }
            ],
            'furniture' => [
                'relation' => 'csWindowFurnitures',
                'modelClass'    => CsWindowFurniture::class
            ],
        ];
        foreach ($relations as $k => $rel) {
            if (!empty($dataJson[$k])) {
                $beforeValidate = $rel['beforeValidate'] ?? null;
                $this->populateWindowRelation($csWindow, $dataJson[$k], $rel['relation'], $rel['modelClass'], $beforeValidate);
            }
        }

        $csWindow->status = $this->detectNewStatus($csWindow);
        return $this;
    }


    /**
     * @param CsWindow $csWindow
     * @param $data
     */
    public function populateBase(CsWindow $csWindow, $data)
    {
        $baseAttributes = [
            'measurement'    => $data['measurement'],
            'windowsill'     => $data['windowsill']??null,
            'lamination'     => $data['lamination']??null,
            'slopes'         => $data['slopes']??null,
            'tinting'        => $data['tinting']??null,
            'energy_saver'   => $data['energy_saver']??null,
            'installation'   => $data['installation']??null,
            'is_active'      => $data['isActive'] ? 1 : 0,
            'price_currency' => $data['currency'],
        ];
        $csWindow->setAttributes($baseAttributes);
    }


    public function populateWindowRelation(CsWindow $csWindow, array $data, $relationName, $modelClass, callable $beforeValidateCallback = null)
    {
        $currentRelatedModels = [];
        // get old binded
        foreach ($csWindow->{$relationName} as $currentRelated) {
            $currentRelated->isDeleted = true;
            $currentRelatedModels[$currentRelated->id] = $currentRelated;
        }
        $relatedModels = [];
        foreach ($data as $row) {
            $related = new $modelClass();
            if (!empty($row['id']) && !empty($currentRelatedModels[$row['id']])) {
                $related = $currentRelatedModels[$row['id']];
            }
            $related->setAttributes($row);
            $related->cs_window_uid = $csWindow->uid;
            if ($beforeValidateCallback) {
                $beforeValidateCallback($row, $related);
            }
            if (!$related->validate()) {
                $csWindow->addError($relationName, '<ul><li>'.implode('</li><li>', $related->getErrorSummary(true)) .'</li></ul>');
                continue;
            }
            $related->isDeleted = false;
            $relatedModels[] = $related;
        }
        foreach ($currentRelatedModels as $id => $related) {
            if ($related->isDeleted) {
                $relatedModels[] = $related;
            }
        }
        $csWindow->populateRelation($relationName, $relatedModels);
    }


    /**
     * @param CsWindow $csWindow
     * @return string
     */
    private function detectNewStatus(CsWindow $csWindow)
    {
        $status = CsWindow::STATUS_PRIVATE;
        if ($csWindow->is_active) {
            $status = CsWindow::STATUS_PUBLISHED_UPDATED;
        }
        if ($csWindow->status == CsWindow::STATUS_REJECTED) {
            $status = CsWindow::STATUS_PUBLISH_PENDING;
        }
        return $status;
    }
}