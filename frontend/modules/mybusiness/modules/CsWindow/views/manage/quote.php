<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 18.10.18
 * Time: 11:01
 *
 * @var \common\models\CompanyService $companyService
 * @var \common\models\base\CsWindowQuote $csWindowQuote
 * @var array $windowFrames
 */

use frontend\modules\mybusiness\modules\CsWindow\serializers\CsWindowQuoteSerializer;
use frontend\modules\mybusiness\modules\CsWindow\serializers\CsWindowSnapshotSerializer;

$this->registerCssFile('@web/css/window-widget.css');

Yii::$app->angular
    ->service(['notify', 'user', 'modal', 'router'])
    ->controller([
        'store/common-models',
        'company/window/CsWindowModels',
        'company/window/quote/CsWindowQuoteModels',
        'company/window/CsWindowQuoteViewController',
        'company/window/CsWindowCalculatorService',
        'company/window/directives/WindowCalculatorFrame',
    ])
    ->controllerParams([
        'csWindow'      => CsWindowSnapshotSerializer::serialize($csWindowQuote->csWindowSnapshotU),
        'csWindowQuote' => CsWindowQuoteSerializer::serialize($csWindowQuote),
        'windowFrames'  => $windowFrames
    ]);

$this->title = _t('csWindowCalculator', 'Quote: {uid}', ['uid' => $csWindowQuote->uid]);

?>
<div class="container" ng-controller="CsWindowQuoteViewController" ng-cloak>

    <div style="max-width: 1000px;margin: 0 auto;">
        <div class="row m-b20">
            <div class="col-sm-3">
                <a href="/mybusiness/cs-window/manage/quotes" class="btn btn-primary btn-block">
                    <span class="tsi tsi-left m-r10"></span> <?php echo _t('csWindowCalculator', 'Back'); ?>
                </a>
            </div>
        </div>
    </div>

    <div style="max-width: 1000px;margin: 0 auto;">
        <div class="row">
            <div class="col-sm-12">
                <table class="form-table">
                    <tbody>
                    <tr class="form-table__row">
                        <td class="form-table__label">
                            <b><?php echo _t('csWindowCalculator', 'Quote UID'); ?></b>
                        </td>
                        <td class="form-table__data">
                            {{csWindowQuote.uid}}
                        </td>
                    </tr>
                    <tr class="form-table__row">
                        <td class="form-table__label">
                            <b><?php echo _t('csWindowCalculator', 'Status'); ?></b>
                        </td>
                        <td class="form-table__data">
                            <span ng-if="csWindowQuote.isNew()" class="label label-success"><?php echo _t('csWindowCalculator', 'New'); ?></span>
                            <span ng-if="csWindowQuote.isViewed()" class="label label-default"><?php echo _t('csWindowCalculator', 'Viewed'); ?></span>
                            <span ng-if="csWindowQuote.isResolved()" class="label label-primary"><?php echo _t('csWindowCalculator', 'Resolved'); ?></span>
                            <span ng-if="csWindowQuote.isDeleted()" class="label label-danger"><?php echo _t('csWindowCalculator', 'Deleted'); ?></span>
                        </td>
                    </tr>
                    <tr class="form-table__row" ng-if="csWindowQuote.contact_name">
                        <td class="form-table__label">
                            <b><?php echo _t('csWindowCalculator', 'Contact name'); ?></b>
                        </td>
                        <td class="form-table__data">
                            {{csWindowQuote.contact_name}}
                        </td>
                    </tr>
                    <tr class="form-table__row" ng-if="csWindowQuote.email">
                        <td class="form-table__label">
                            <b><?php echo _t('csWindowCalculator', 'Email'); ?></b>
                        </td>
                        <td class="form-table__data">
                            {{csWindowQuote.email}}
                        </td>
                    </tr>
                    <tr class="form-table__row" ng-if="csWindowQuote.phone">
                        <td class="form-table__label">
                            <b><?php echo _t('csWindowCalculator', 'Phone'); ?></b>
                        </td>
                        <td class="form-table__data">
                            {{csWindowQuote.phone}}
                        </td>
                    </tr>
                    <tr class="form-table__row" ng-if="csWindowQuote.address">
                        <td class="form-table__label">
                            <b><?php echo _t('csWindowCalculator', 'Address'); ?></b>
                        </td>
                        <td class="form-table__data">
                            {{csWindowQuote.address}}
                        </td>
                    </tr>
                    <tr class="form-table__row" ng-if="csWindowQuote.notes">
                        <td class="form-table__label">
                            <b><?php echo _t('csWindowCalculator', 'Notes'); ?></b>
                        </td>
                        <td class="form-table__data">
                            {{csWindowQuote.notes}}
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="m-b30" ng-if="csWindowQuote.isNew() || csWindowQuote.isViewed()">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default" ng-click="statusChange('resolved')">
                            <input type="radio" name="options" id="option1" autocomplete="off">
                            <?=_t('csWindowCalculator', 'Accept');?>
                        </label>
                        <label class="btn btn-default" ng-click="statusChange('deleted')">
                            <input type="radio" name="options" id="option2" autocomplete="off">
                            <?=_t('csWindowCalculator', 'Decline');?>
                        </label>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="window-widget">
                <div class="window-widget__variants">
                    <div ng-repeat="windowQuote in csWindowQuote.quoteItems"
                         ng-click="csWindowQuote.setQuoteActive(windowQuote)"
                         class="window-widget__variants-item"
                         ng-class="{'active' : csWindowQuote.isQuoteActive(windowQuote)}"
                    >
                        <div class="window-widget__variants-price">{{windowQuote.view.getCost()}}</div>

                        <window-calculator-frame
                                quote="windowQuote"
                                tpl-id="{{csWindowFrame.getHtmlId(windowQuote.frame.id)}}"
                        ></window-calculator-frame>

                        <div class="window-widget__variants-caption">
                            <strong title="{{windowQuote.title}}">{{windowQuote.title}}</strong>
                            <span>{{windowQuote.view.getProfileSize()}}</span>
                        </div>
                    </div>
                </div>

                <div class="window-widget__body" ng-if="csWindowQuote.quoteItemActive">
                    <div class="row">
                        <div class="col-sm-6 wide-padding wide-padding--right">

                            <div class="window-widget__window-name">
                                <div ng-class="{'has-error': csWindowQuote.quoteItemActive.getError('title')}">
                                    <input type="text"
                                           class="form-control"
                                           value="{{csWindowQuote.quoteItemActive.title}}" placeholder="<?php echo _t('csWindowCalculator', 'Window name'); ?>"
                                           disabled
                                    >
                                </div>
                            </div>

                            <div class="window-widget__customize">
                                <window-calculator-frame
                                        quote="csWindowQuote.quoteItemActive"
                                        tpl-id="{{csWindowFrame.getHtmlId(csWindowQuote.quoteItemActive.frame.id)}}"
                                ></window-calculator-frame>

                                <div class="window-widget__size">
                                    <div class="input-group input-group-sm window-widget__size-input">
                                        <input type="text" class="form-control" value="{{csWindowQuote.quoteItemActive.quoteParameters.profile.width}}" disabled>
                                        <span class="input-group-addon">{{csWindowQuote.getMeasurement()}}</span>
                                    </div>
                                    <div class="window-widget__size-clear">
                                        {{csWindowQuote.quoteItemActive.view.getProfileSizeConvertRevers(csWindowQuote.quoteItemActive.view.getQuoteProfileWidth())}}
                                    </div>
                                </div>

                                <div class="window-widget__size window-widget__size--vertical">
                                    <div class="input-group input-group-sm window-widget__size-input">
                                        <input type="text" class="form-control" value="{{csWindowQuote.quoteItemActive.quoteParameters.profile.height}}" disabled>
                                        <span class="input-group-addon">{{csWindowQuote.getMeasurement()}}</span>
                                    </div>
                                    <div class="window-widget__size-clear">
                                        {{csWindowQuote.quoteItemActive.view.getProfileSizeConvertRevers(csWindowQuote.quoteItemActive.view.getQuoteProfileHeight())}}
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-6">
                            <table class="form-table form-table--top-label window-widget__params">
                                <tr class="form-table__row" ng-if="csWindowQuote.quoteItemActive.profile">
                                    <td class="form-table__label">
                                        <b><?php echo _t('csWindowCalculator', 'Profile'); ?></b>
                                    </td>
                                    <td class="form-table__data p-b10">
                                        <div class="btn btn-pill active">{{csWindowQuote.quoteItemActive.profile.title}}</div>
                                    </td>
                                </tr>
                                <tr class="form-table__row" ng-if="csWindowQuote.quoteItemActive.glass">
                                    <td class="form-table__label">
                                        <b><?php echo _t('csWindowCalculator', 'Glasses'); ?></b>
                                    </td>
                                    <td class="form-table__data p-b10">
                                        <div class="btn btn-pill active">{{csWindowQuote.quoteItemActive.glass.title}}</div>
                                    </td>
                                </tr>
                                <tr class="form-table__row" ng-if="csWindowQuote.quoteItemActive.frame.hasDirectionParams()">
                                    <td class="form-table__label">
                                        <b><?php echo _t('csWindowCalculator', 'Hardware'); ?></b>
                                    </td>
                                    <td class="form-table__data p-b0">
                                        <div class="btn btn-pill active">{{csWindowQuote.quoteItemActive.furniture.title}}</div>
                                    </td>
                                </tr>
                                <tr class="form-table__row">
                                    <td class="form-table__data" colspan="2">
                                        <label class="checkbox-switch checkbox-switch--xs">
                                            <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.windowsill" disabled>
                                            <span class="slider"></span>
                                            <span class="text"><?php echo _t('csWindowCalculator', 'Windowsill'); ?></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr class="form-table__row">
                                    <td class="form-table__data" colspan="2">
                                        <label class="checkbox-switch checkbox-switch--xs">
                                            <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.lamination" disabled>
                                            <span class="slider"></span>
                                            <span class="text"><?php echo _t('csWindowCalculator', 'Lamination'); ?></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr class="form-table__row">
                                    <td class="form-table__data" colspan="2">
                                        <label class="checkbox-switch checkbox-switch--xs">
                                            <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.slopes" disabled>
                                            <span class="slider"></span>
                                            <span class="text"><?php echo _t('csWindowCalculator', 'Jambs'); ?></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr class="form-table__row">
                                    <td class="form-table__data" colspan="2">
                                        <label class="checkbox-switch checkbox-switch--xs">
                                            <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.tinting" disabled>
                                            <span class="slider"></span>
                                            <span class="text"><?php echo _t('csWindowCalculator', 'Tinting'); ?></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr class="form-table__row">
                                    <td class="form-table__data" colspan="2">
                                        <label class="checkbox-switch checkbox-switch--xs">
                                            <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.energySaver" disabled>
                                            <span class="slider"></span>
                                            <span class="text"><?php echo _t('csWindowCalculator', 'Energy Saver'); ?></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr class="form-table__row">
                                    <td class="form-table__data" colspan="2">
                                        <label class="checkbox-switch checkbox-switch--xs">
                                            <input type="checkbox" ng-model="csWindowQuote.quoteItemActive.installation" disabled>
                                            <span class="slider"></span>
                                            <span class="text"><?php echo _t('csWindowCalculator', 'Installation'); ?></span>
                                        </label>
                                    </td>
                                </tr>
                            </table>

                            <div class="window-widget__qty">
                                <label><?= _t('csWindowCalculator', 'Number of windows'); ?></label>
                                <input type="number" class="form-control input-sm" value="{{csWindowQuote.quoteItemActive.qty}}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="window-widget__footer" ng-if="csWindowQuote.hasQuotes()">
                    <div class="window-widget__summary">
                        <h3 class="window-widget__summary-price">{{csWindowQuote.getTotalCostFormat()}}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->render('@frontend/modules/mybusiness/modules/CsWindow/widgets/views/partial/windowPreview');?>