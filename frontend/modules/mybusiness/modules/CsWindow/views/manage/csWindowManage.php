<?php
/**
 * User: nabi
 */

use frontend\modules\mybusiness\modules\CsWindow\serializers\CsWindowSerializer;

/* @var $this \yii\web\View */
/* @var $csWindow common\models\CsWindow */


$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);
$csWindowJson = CsWindowSerializer::serialize($csWindow);

Yii::$app->angular
    ->service(['notify', 'user', 'modal', 'router', 'maps', 'geo'])
    ->directive(['dropzone-image-crop', 'dropzone-button', 'fileread', 'image-rotate', 'google-map', 'google-address-input'])
    ->controller(
        [
            'company/window/CsWindowModels',
            'company/window/CsWindowEditorController',
        ]
    )
    ->constants(
        [
            'countries' => \common\components\ArrayHelper::toArray(
                \lib\geo\GeoNames::getAllCountries(),
                [\common\models\GeoCountry::class => ['id', 'iso_code', 'title', 'is_easypost_domestic', 'is_easypost_intl']]
            ),
        ]
    )
    ->controllerParam('csWindow', $csWindowJson);
?>

<div class="container container--wide m-t0 p-t0">

    <div class="row">
        <div class="col-md-12 ps-steps">
            <h2 class="ps-steps__title"><?= _t('service.window', 'Window Manufacturing Service'); ?></h2>

            <div class="window-quoting-details" ng-controller="CsWindowEditorController">

                <div class="p-t20 m-b20">
                    <span class="m-r10"><?= _t('service.window', 'Unit of measurement'); ?>:</span>

                    <div class="btn-group btn-switcher" data-toggle="buttons">
                        <label class="btn btn-default btn-sm" ng-class="{'active':csWindow.measurement=='ft'}" ng-click="csWindow.setMeasurement('ft')" >
                            <input type="radio" value="ft" ng-model="csWindow.measurement" id="optionsPrice1" autocomplete="off">
                            ft
                        </label>
                        <label class="btn btn-default btn-sm" ng-class="{'active':csWindow.measurement=='m'}"   ng-click="csWindow.setMeasurement('m')">
                            <input type="radio" value="m" id="optionsPrice2" autocomplete="off" ng-model="csWindow.measurement">
                            m
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">

                        <h3 class="m-t0"><?= _t('service.window', 'Profile'); ?></h3>
                        <div class="window-quoting-details__block">
                            <div class="table-responsive">
                                <div class="help-block-error"></div>
                                <div class="error-content" id="csWindow-csWindowProfiles"></div>
                                <table class="table table-striped" ng-cloak="true">
                                    <tr>
                                        <th width="25%"><?= _t('service.window', 'Title'); ?></th>
                                        <th><?= _t('service.window', 'Thickness'); ?>,&nbsp;{{csWindow.sizeMeasurement}}</th>
                                        <th><?= _t('service.window', 'Chambers'); ?></th>
                                        <th><?= _t('service.window', 'Max glass thickness'); ?>,&nbsp;{{csWindow.sizeMeasurement}}</th>
                                        <th><?= _t('service.window', 'Max width'); ?>,&nbsp;{{csWindow.sizeMeasurement}}</th>
                                        <th><?= _t('service.window', 'Max height'); ?>,&nbsp;{{csWindow.sizeMeasurement}}</th>
                                        <th><?= _t('service.window', 'Price'); ?> {{csWindow.currency}}/{{csWindow.measurement}}</th>
                                        <th></th>
                                    </tr>
                                    <tr ng-repeat="item in csWindow.profile">
                                        <td><input type="text" class="form-control " ng-model="item.title"></td>
                                        <td>
                                            <input type="number" class="form-control " ng-model="item.thickness">
                                        </td>
                                        <td><input type="number" class="form-control " ng-model="item.chambers"></td>
                                        <td>
                                            <input type="number" class="form-control " ng-model="item.max_glass">
                                        </td>
                                        <td><input type="number" class="form-control " ng-model="item.max_width"></td>
                                        <td><input type="number" class="form-control " ng-model="item.max_height"></td>
                                        <td>
                                            <input type="number" class="form-control " ng-model="item.price">
                                        </td>
                                        <td>
                                            <button type="button" ng-click="csWindow.removeProfile(item)"
                                                    class="btn btn-info p-l10 p-r10" title="<?= _t('front.common', 'Delete'); ?>">
                                                <span class="tsi tsi-bin"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                                <button type="button" ng-click="csWindow.addProfile()"
                                        class="btn btn-info btn-sm p-l10 p-r10 m-l10 m-b10" title="<?= _t('front.common', 'Add'); ?>">
                                    <span class="tsi tsi-plus m-r10"></span> <?= _t('mybusiness.product', 'Add profile'); ?>
                                </button>

                            </div>
                        </div>

                        <h3><?= _t('service.window', 'Glass'); ?></h3>
                        <div class="window-quoting-details__block">
                            <div class="table-responsive">
                                <div class="help-block-error"></div><div class="error-content" id="csWindow-csWindowGlasses"></div>
                                <table class="table table-striped" ng-cloak="true">
                                    <tr>
                                        <th width="25%"><?= _t('service.window', 'Title'); ?></th>
                                        <th><?= _t('service.window', 'Thickness'); ?></th>
                                        <th><?= _t('service.window', 'Chambers'); ?></th>
                                        <th><?= _t('service.window', 'Price'); ?></th>
                                        <th></th>
                                    </tr>
                                    <tr ng-repeat="item in csWindow.glass">
                                        <td><input type="text" class="form-control " ng-model="item.title"></td>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" class="form-control " ng-model="item.thickness">
                                                <span class="input-group-addon">{{csWindow.sizeMeasurement}}</span>
                                            </div>
                                        </td>
                                        <td><input type="number" class="form-control " ng-model="item.chambers"></td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon">{{csWindow.currency}} </span>
                                                <input type="number" class="form-control " ng-model="item.price">
                                                <span class="input-group-addon">/{{csWindow.measurement}}<sup>2</sup></span>
                                            </div>
                                        </td>
                                        <td>
                                            <button type="button" ng-click="csWindow.removeGlass(item)"
                                                    class="btn btn-info p-l10 p-r10" title="<?= _t('front.common', 'Delete'); ?>">
                                                <span class="tsi tsi-bin"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                                <button type="button" ng-click="csWindow.addGlass()"
                                        class="btn btn-info btn-sm p-l10 p-r10 m-l10 m-b10" title="<?= _t('front.common', 'Add'); ?>">
                                    <span class="tsi tsi-plus m-r10"></span> <?= _t('mybusiness.product', 'Add glass'); ?>
                                </button>
                            </div>
                        </div>

                        <h3><?= _t('service.window', 'Window hardware'); ?></h3>
                        <div class="window-quoting-details__block">
                            <div class="table-responsive">
                                <div class="help-block-error"></div><div class="error-content" id="csWindow-csWindowFurnitures"></div>
                                <table class="table table-striped" ng-cloak="true">
                                    <tr>
                                        <th width="25%"><?= _t('service.window', 'Title'); ?></th>
                                        <th><?= _t('service.window', 'Price'); ?></th>
                                        <th><?= _t('service.window', 'Turn'); ?></th>
                                        <th><?= _t('service.window', 'Tilt & Turn'); ?></th>
                                        <th></th>
                                    </tr>
                                    <tr ng-repeat="item in csWindow.furniture">
                                        <td><input type="text" class="form-control " ng-model="item.title"></td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon">{{csWindow.currency}} </span>
                                                <input type="number" class="form-control " ng-model="item.price">
                                                <span class="input-group-addon">/{{csWindow.measurement}}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon">{{csWindow.currency}} </span>
                                                <input type="number" class="form-control " ng-model="item.price_swivel">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon">{{csWindow.currency}} </span>
                                                <input type="number" class="form-control " ng-model="item.price_folding">
                                            </div>
                                        </td>
                                        <td>
                                            <button type="button" ng-click="csWindow.removeFurniture(item)"
                                                    class="btn btn-info p-l10 p-r10" title="<?= _t('front.common', 'Delete'); ?>">
                                                <span class="tsi tsi-bin"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                                <button type="button" ng-click="csWindow.addFurniture()"
                                        class="btn btn-info btn-sm p-l10 p-r10 m-l10 m-b10" title="<?= _t('front.common', 'Add'); ?>">
                                    <span class="tsi tsi-plus m-r10"></span> <?= _t('mybusiness.product', 'Add '); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">

                        <div class="designer-card m-t30">
                            <div class="row" ng-cloak="true">
                                <div class="col-md-4">
                                    <label class="p-t10"><?= _t('', 'Windowsill'); ?></label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{csWindow.currency}} </span>
                                        <input type="number" id="csWindow-windowsill" class="form-control" ng-model="csWindow.windowsill">
                                        <span class="input-group-addon">/{{csWindow.measurement}}</span>
                                    </div>
                                </div>
                                <div class="col-md-12 m-b10"></div>
                                <div class="col-md-4">
                                    <label class="p-t10"><?= _t('', 'Lamination'); ?></label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{csWindow.currency}} </span>
                                        <input type="number" id="csWindow-lamination" class="form-control" ng-model="csWindow.lamination">
                                        <span class="input-group-addon">/{{csWindow.measurement}}</span>
                                    </div>
                                </div>
                                <div class="col-md-12 m-b10"></div>
                                <div class="col-md-4">
                                    <label class="p-t10"><?= _t('', 'Jambs'); ?></label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{csWindow.currency}} </span>
                                        <input type="number"  id="csWindow-slopes"  class="form-control" ng-model="csWindow.slopes">
                                        <span class="input-group-addon">/{{csWindow.measurement}}</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12 m-b10"></div>
                                </div>
                                <div class="col-md-4">
                                    <label class="p-t10"><?= _t('', 'Tinting'); ?></label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{csWindow.currency}} </span>
                                        <input type="number"  id="csWindow-tinting"  class="form-control" ng-model="csWindow.tinting">
                                        <span class="input-group-addon">/{{csWindow.measurement}}<sup>2</sup></span>
                                    </div>
                                </div>
                                <div class="col-md-12 m-b10"></div>
                                <div class="col-md-4">
                                    <label class="p-t10"><?= _t('', 'Energy saver'); ?></label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{csWindow.currency}} </span>
                                        <input type="number"  id="csWindow-energy_saver"  class="form-control" ng-model="csWindow.energy_saver">
                                        <span class="input-group-addon">/{{csWindow.measurement}}<sup>2</sup></span>
                                    </div>
                                </div>
                                <div class="col-md-12 m-b10"></div>
                                <div class="col-md-4">
                                    <label class="p-t10"><?= _t('', 'Installation'); ?></label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type="number"  id="csWindow-installation" class="form-control" ng-model="csWindow.installation">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3>Locations</h3>
                        <div class="window-quoting-details__block">
                            <div class="table-responsive">
                                <table class="table table-striped" ng-cloak="true">
                                    <tr>
                                        <td width="50%">

                                        </td>
                                        <td>
                                            <?= _t('service.windows', 'Delivery in radius'); ?>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr ng-repeat="item in csWindow.locations">
                                        <td>
                                            <input google-address-input="" on-address-change="onAutocompleteAddressChange({address:address, csLocation:item})" ng-model="item.location.formatted_address" type="text" class="form-control"/>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" class="form-control " ng-model="item.radius">
                                                <span class="input-group-addon">km</span>
                                            </div>
                                        </td>
                                        <td>
                                            <button type="button" ng-click="csWindow.removeLocation(item)"
                                                    class="btn btn-info p-l10 p-r10" title="<?= _t('front.common', 'Delete'); ?>">
                                                <span class="tsi tsi-bin"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                                <button type="button" ng-click="csWindow.addLocation()"
                                        class="btn btn-info btn-sm p-l10 p-r10 m-l10 m-b10" title="<?= _t('front.common', 'Add'); ?>">
                                    <span class="tsi tsi-plus m-r10"></span> <?= _t('mybusiness.product', 'Add location'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <div class="row m-t20">
                            <div class="col-sm-6 col-md-4">
                                <strong><?= _t('site.store', 'Moderation status') . ': ' ?></strong>
                                {{csWindow.companyService.moderatorStatusLabel}}
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <strong><?= _t('site.store', 'Published') . ': ' ?></strong>
                                {{csWindow.companyService.visibilityLabel}}
                            </div>
                        </div>


                        <div class="row m-b10">
                            <div class="col-sm-6 col-md-4">
                                <input type="button" class="btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect"
                                       value="<?=_t('mybusiness.store', 'Publish');?>" ng-click="submitForm('publish')"  ng-hide="csWindow.companyService.isPublished">
                                <input type="button" class="btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect"
                                       value="<?=_t('mybusiness.store', 'Save');?>" ng-click="submitForm('publish')"  ng-show="csWindow.companyService.isPublished">
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <input type="button" class="btn btn-ghost btn-info btn-block t-model-edit-unpublish m-b10 p-l10 p-r10 js-clickProtect"
                                       value="<?=_t('mybusiness.store', 'Save unpublished');?>" ng-click="submitForm('unpublish')" ng-hide="csWindow.companyService.isPublished">
                                <input type="button" class="btn btn-ghost btn-info btn-block t-model-edit-unpublish m-b10 p-l10 p-r10 js-clickProtect"
                                       value="<?=_t('mybusiness.store', 'Unpublish');?>" ng-click="submitForm('unpublish')" ng-show="csWindow.companyService.isPublished">
                            </div>

                        </div>
                    </div>
                </div>

            <?php if (false): ?>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="pull-right">
                            Active:
                            <label class="checkbox-switch checkbox-switch--sm">
                                <input type="checkbox" ng-model="csWindow.isActive">
                                <span class="slider"></span>
                            </label>
                            <button class="btn btn-primary" ng-click="save()"><?= _t('service.window', 'Save'); ?></button>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            </div>

        </div>
    </div>
</div>