<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 18.10.18
 * Time: 11:01
 *
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \frontend\modules\mybusiness\modules\CsWindow\models\CsWindowQuoteSearch $csWindowQuoteSearch
 */

use common\models\CsWindowQuote;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = _t('csWindowCalculator', 'Quotes list');

?>
<div class="container">
    <div class="row m-b20">
        <div class="col-sm-6">
            <?php echo Html::beginForm('/mybusiness/cs-window/manage/quotes', 'get'); ?>
                <div class="input-group">
                    <?php echo Html::textInput('search', $csWindowQuoteSearch->search, [
                        'class' => 'form-control',
                        'placeholder' => _t('csWindowCalculator', 'Search for...')
                    ]); ?>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><?php echo _t('csWindowCalculator', 'Search'); ?></button>
                    </span>
                </div>
            <?php echo Html::endForm(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive m-b20">
                <?php echo GridView::widget([
                    'sorter' => false,
                    'showFooter' => false,
                    'summary' => false,
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-striped'],
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label'     => _t('csWindowCalculator', 'Created On'),
                            'format'    => 'datetime'
                        ],
                        [
                            'attribute' => 'contact_name',
                            'label'     => _t('csWindowCalculator', 'Contact name'),
                        ],
                        [
                            'attribute' => 'phone',
                            'label'     => _t('csWindowCalculator', 'Phone'),
                        ],
                        [
                            'attribute' => 'email',
                            'label'     => _t('csWindowCalculator', 'Email'),
                        ],
                        [
                            'attribute' => 'notes',
                            'label'     => _t('csWindowCalculator', 'Notes'),
                            'value'     => function (CsWindowQuote $model) {
                                return \H($model->notes ?? '');
                            }
                        ],
                        [
                            'format'    => 'raw',
                            'attribute' => 'total_price',
                            'label'     => _t('csWindowCalculator', 'Total cost'),
                            'value'     => function (CsWindowQuote $model) {
                                return displayAsCurrency($model->total_price, $model->currency);
                            }
                        ],
                        [
                            'format'    => 'raw',
                            'attribute' => 'status',
                            'label'     => _t('csWindowCalculator', 'Status'),
                            'value'     => function (CsWindowQuote $model) {
                                $class = '';

                                if ($model->status === CsWindowQuote::STATUS_NEW) {
                                    $class = 'label label-success';
                                }

                                if ($model->status === CsWindowQuote::STATUS_VIEWED) {
                                    $class = 'label label-default';
                                }

                                if ($model->status === CsWindowQuote::STATUS_RESOLVED) {
                                    $class = 'label label-primary';
                                }

                                if ($model->status === CsWindowQuote::STATUS_DELETED) {
                                    $class = 'label label-danger';
                                }

                                return Html::tag('span', $model->status, ['class' => $class]);
                            }
                        ],
                        [
                            'attribute'=>'uid',
                            'format' => 'raw',
                            'label' => false,
                            'value' => function(CsWindowQuote $model) {
                                return Html::a(_t('csWindowCalculator', 'Details'), $model->getUrlQuoteDetails(),
                                    ['class' => 'btn btn-primary btn-sm pull-right', 'target'=>'_blank']
                                );
                            }
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>