<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\services;

use common\components\DateHelper;
use common\models\CsWindowQuote;
use common\models\user\UserIdentityProvider;
use yii\web\NotAcceptableHttpException;

/**
 * Class CsWindowService
 * @package frontend\modules\mybusiness\modules\CsWindow\services
 *
 * @property UserIdentityProvider $identityProvider
 */
class CsWindowQuoteService
{
    /**
     * @var UserIdentityProvider
     */
    public $identityProvider;

    public $windowFactory;

    public function injectDependencies(
        UserIdentityProvider $identityProvider
    ) {
        $this->identityProvider = $identityProvider;
    }

    /**
     * @param CsWindowQuote $quote
     *
     * @return bool
     * @throws NotAcceptableHttpException
     */
    public function tryValidateAccessCompany(CsWindowQuote $quote): bool
    {
        $userId = $quote->companyService->company->user_id;
        $currentUser = $this->identityProvider->getUser();

        if ($currentUser && $currentUser->id !== $userId) {
            throw new NotAcceptableHttpException('No access to this object');
        }

        return true;
    }

    /**
     * @param CsWindowQuote $quote
     */
    public function setViewedQuote(CsWindowQuote $quote): void
    {
        if ($quote->isNew()) {
            $quote->viewing_at = DateHelper::now();
            $quote->status = CsWindowQuote::STATUS_VIEWED;
            $quote->save(true, ['status', 'viewing_at']);
        }
    }

    public function quoteUserStatusChange(CsWindowQuote $quote, $status): bool
    {
        if (\in_array($status, [CsWindowQuote::STATUS_DELETED, CsWindowQuote::STATUS_VIEWED, CsWindowQuote::STATUS_RESOLVED], true)) {
            if (\in_array($quote->status, [CsWindowQuote::STATUS_NEW, CsWindowQuote::STATUS_VIEWED], true)) {
                $quote->status = $status;
                return $quote->save(true, ['status']);
            }
        }

        return false;
    }
}