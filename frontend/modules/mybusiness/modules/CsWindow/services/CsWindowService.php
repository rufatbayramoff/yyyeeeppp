<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\CsWindow\services;

use common\models\Company;
use common\models\CompanyService;
use common\models\CsWindow;
use common\models\CsWindowSnapshot;
use common\models\user\UserIdentityProvider;
use common\modules\company\components\CompanyServiceInterface;
use frontend\modules\mybusiness\modules\CsWindow\factories\CsWindowFactory;
use yii\web\NotAcceptableHttpException;

/**
 * Class CsWindowService
 * @package frontend\modules\mybusiness\modules\CsWindow\services
 *
 * @property CsWindowFactory $windowFactory
 */
class CsWindowService
{
    /**
     * @var UserIdentityProvider
     */
    public $identityProvider;

    public $windowFactory;

    public function injectDependencies(
        UserIdentityProvider $identityProvider,
        CsWindowFactory $windowFactory
    ) {
        $this->identityProvider = $identityProvider;
        $this->windowFactory = $windowFactory;
    }

    /**
     * @param CsWindow $csWindow
     *
     * @return CsWindowSnapshot
     * @throws \common\components\exceptions\BusinessException
     */
    public function createSnapshot(CsWindow $csWindow): CsWindowSnapshot
    {
        $snapshot = $this->windowFactory->createSnapshot($csWindow);
        $snapshot->safeSave();

        return $snapshot;
    }

    public function logHistory()
    {
        // @TODO add history
    }

    public function publish()
    {

    }

    /**
     * @param \common\models\CsWindow $csWindow
     * @return bool
     * @throws NotAcceptableHttpException
     */
    public function tryValidateAccess(CsWindow $csWindow)
    {
        $userId = $csWindow->companyService->company->user_id;
        $currentUser = $this->identityProvider->getUser();
        if ($currentUser->id !== $userId) {
            throw new NotAcceptableHttpException('No access to this object');
        }
        return true;
    }

    /**
     * @param CompanyService $company
     * @return null|static|CompanyService
     */
    public function getCompanyService(Company $company)
    {
        $companyService = CompanyService::findOne(['type' => CompanyServiceInterface::TYPE_WINDOW, 'ps_id' => $company->id, 'is_deleted'=>0]);
        return $companyService;
    }

    /**
     * @param Company $company
     *
     * @return CsWindow|null
     */
    public function getWindowService(Company $company): ?CsWindow
    {
        $companyService = $this->getCompanyService($company);

        if ($companyService) {
            return $companyService->csWindow;
        }

        return null;
    }

    /**
     * @param $submitMode
     * @param CompanyService $companyService
     */
    public function updateCompanyServiceAfterSave($submitMode, CompanyService $companyService)
    {
        if ($submitMode === 'publish') {
            $companyService->moderator_status = $companyService->getModeratorStatusAfterUpdate();
            $companyService->visibility = CompanyService::VISIBILITY_EVERYWHERE;
        } elseif ($submitMode === 'unpublish') {
            $companyService->visibility = CompanyService::VISIBILITY_NOWHERE;
        }
        $companyService->save();
    }
}