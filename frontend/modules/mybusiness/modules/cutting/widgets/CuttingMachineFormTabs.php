<?php
namespace frontend\modules\mybusiness\modules\cutting\widgets;

use common\models\CuttingMachine;
use Yii;
use yii\base\Widget;
use yii\bootstrap\Tabs;
use yii\helpers\Url;

class CuttingMachineFormTabs  extends Widget
{
    public $section = null;
    /**
     * @var CuttingMachine
     */
    public $cuttingMachine = null;

    protected $urlPrefix = 'mybusiness/cutting/cutting';

    public function run()
    {
        $links = [
            [
                'url'   => ['edit'],
                'label' => _t('mybusiness.product', 'General')
            ],
            [
                'url'   => ['workpiece-materials'],
                'label' => _t('mybusiness.product', 'Materials')
            ],
            [
                'url'   =>  ['processing-price'],
                'label' => _t('mybusiness.product', 'Rates')
            ],
            [
                'url'   => ['delivery-details'],
                'label' => _t('mybusiness.product', 'Delivery')
            ],
        ];

        $items = [];
        $activeUrl = $this->section ?: Yii::$app->requestedRoute;
        $activeUrlTrim = trim($activeUrl, '/');

        foreach ($links as $k => $v) {
            $url = trim(Url::toRoute($v['url']), '/');
            $activeUrlTrim = str_replace("/index", "", $activeUrlTrim);
            if ($activeUrlTrim == $this->urlPrefix . '/' . $url || $activeUrlTrim == $url) {
                $v['active'] = true;
            }
            $v['url'] = '/'. $url . '?id=' . $this->cuttingMachine->id;
            if ($this->cuttingMachine->isNewRecord) {
                $v['url'] = 'javascript:alert(_t("site.app", "Please save general information first"));';
            }
            $items[] = $v;
        }


        return Tabs::widget(
            [
                'renderTabContent' => false,
                'encodeLabels'     => false,
                'options'          => [
                    'class' => ' nav-tabs--secondary m-b20',
                    'role'  => 'tablist'
                ],
                'items'            => $items,
            ]
        );
    }
}
