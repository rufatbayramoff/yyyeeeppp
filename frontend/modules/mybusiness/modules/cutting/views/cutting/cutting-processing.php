<?php

use common\models\CuttingMachine;
use common\modules\cutting\serializers\CuttingMachineSerializer;
use common\modules\cutting\serializers\CuttingProcessingSerializer;
use common\modules\cutting\serializers\CuttingWorkpieceMaterialProcessingSerializer;
use frontend\modules\mybusiness\modules\cutting\widgets\CuttingMachineFormTabs;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var CuttingMachine $cuttingMachine */
/** @var \common\models\CuttingMaterial[] $cuttingMaterials */


$companyService               = $cuttingMachine->companyService;
$cuttingMachineSerialized     = CuttingMachineSerializer::serialize($cuttingMachine);
$cuttingProcessingSerialized  = CuttingProcessingSerializer::serialize($cuttingMachine->cuttingMachineProcessings);
$workpieceMaterialsSerialized = CuttingWorkpieceMaterialProcessingSerializer::serialize($cuttingMachine->companyService->company->cuttingWorkpieceMaterials);

Yii::$app->angular
    ->service(['notify', 'router', 'user', 'modal','cuttingService'])
    ->controller([
        'ps/cutting/CuttingProcessingController',
        'company/companyServiceModels',
        'ps/cutting/CuttingMachineModels',
        'product/commonLib'
    ])
    ->controllerParams(
        [
            'cuttingMachine'            => $cuttingMachineSerialized,
            'cuttingProcessing'         => $cuttingProcessingSerialized,
            'cuttingWorkpieceMaterials' => $workpieceMaterialsSerialized
        ]);

$form = ActiveForm::begin(
    [
        'id'                     => 'CompanyServicedForm',
        'options'                => ['enctype' => 'multipart/form-data', 'onsubmit' => ''],
        'enableClientValidation' => false,
    ]);
?>

<div ng-controller="CuttingProcessingController">
    <div ng-cloak>
        <div class="row m-b10">
            <div class="col-sm-9 col-md-7 col-lg-8">
                <h3 class="product-add-title ugc-content">
                    <?php echo _t('front.user', 'Edit cutting machine: '); ?>
                    {{companyService.title}}
                </h3>
            </div>
            <div class="col-sm-3 col-md-2"></div>
        </div>

        <div class="designer-card p-l0 p-r0 p-t0 p-b0">

            <?= CuttingMachineFormTabs::widget(['cuttingMachine' => $cuttingMachine]); ?>

            <div class="product-edit">
                <div class="row border-b m-b20 hidden-xs hidden-sm">
                    <div class="col-md-4">
                        <h4><?= _t("site.ps", "Material"); ?></h4>
                    </div>
                    <div class="col-md-3">
                        <h4><?= _t("site.ps", "Cutting rate ({currency} per m)", ['currency' => $companyService->company->paymentCurrency->title_original]); ?></h4>
                    </div>
                    <div class="col-md-3">
                        <h4><?= _t("site.ps", "Engraving rate ({currency} per m)", ['currency' => $companyService->company->paymentCurrency->title_original]); ?></h4>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
                <div ng-repeat="processPrice in processing.list">
                    <div class="row m-b20">
                        <div class="col-xs-12 col-md-4 m-b20">
                            <label class="hidden-md hidden-lg"><?= _t("site.ps", "Material"); ?></label>
                            <select
                                    class="form-control"
                                    ng-model="processPrice.workpieceMaterialProcessingUuid"
                                    ng-options="workpieceMaterial.uuid as workpieceMaterial.title for workpieceMaterial in getSelectableWorkpieceMaterials(processPrice.workpieceMaterialUuid)"
                                    required
                                    ng-change="changeWorkpieceMaterial(processPrice)"
                            >
                            </select>
                        </div>
                        <div class="col-xs-6 col-md-3 m-b20 p-r0">
                            <label class="hidden-md hidden-lg"><?= _t("site.ps", "Cutting rate"); ?></label>
                            <input class="form-control" type="number"
                                   ng-model="processPrice.cuttingPrice"
                                   required
                                   placeholder="<?= _t('site.ps', 'cutting price') ?>">
                            <div class="help-block text-muted m-b0 hidden-md hidden-lg"><?= _t("site.ps", "$ per mm"); ?></div>
                        </div>
                        <div class="col-xs-6 col-md-3 m-b20">
                            <label class="hidden-md hidden-lg"><?= _t("site.ps", "Engraving rate"); ?></label>
                            <input class="form-control" type="number"
                                   ng-model="processPrice.engravingPrice"
                                   required
                                   placeholder="<?= _t('site.ps', 'engraving price') ?>">
                            <div class="help-block text-muted m-b0 hidden-md hidden-lg"><?= _t("site.ps", "$ per mm"); ?></div>
                        </div>
                        <div class="col-xs-12 col-md-2 m-b20">
                            <button
                                    ng-click="deleteProcessing(processPrice)"
                                    type="button" class="btn btn-info btn-circle"
                                    title="<?= _t("site.ps", "Delete"); ?>">
                                <span class="tsi tsi-bin"></span>
                            </button>
                        </div>
                        <div class="col-xs-12">
                            <hr class="m-t0 m-b0">
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-xs-12 col-md-8 col-lg-7">
                        <button
                                ng-disabled="!canAddProcessing()"
                                ng-click="addProcessing()"
                                type="button" class="btn btn-primary m-b10"
                                title="{{getAddProcessingTitle()}}"
                        >
                            <span class="tsi tsi-plus"></span> <?= _t("site.ps", "Add rates"); ?>
                        </button>
                    </div>
                </div>

                <div class="row">
                    <hr>
                    <div class="col-lg-6">
                    </div>
                    <div class="col-lg-6">
                        <div class="row m-b10">
                            <div class="col-sm-4">
                                <button
                                    ng-click="prevStep()"
                                    type="button" class="btn btn-primary btn-ghost btn-block m-b10">
                                    <span class="tsi tsi-left"></span> <?= _t('site.ps', 'Back') ?>
                                </button>
                            </div>
                            <div class="col-sm-4">
                                <button
                                        ng-click="publish()"
                                        ng-if="cuttingMachine.id"
                                        type="button" class="btn btn-primary btn-block m-b10">
                                    <?= _t('site.common', 'Publish'); ?>
                                </button>
                            </div>
                            <div class="col-sm-4 ">
                                <button
                                    ng-click="nextStep()"
                                    ng-disabled="showNextProgress"
                                    type="button" class="btn btn-primary btn-block m-b10">
                                    <span ng-if="showNextProgress">
                                         <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Please wait...'); ?>
                                    </span>
                                    <span ng-if="!showNextProgress">
                                        <?= _t('site.common', 'Next'); ?>
                                        <span class="tsi tsi-right"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

