<?php

use common\models\CuttingMachine;
use common\modules\cutting\serializers\CuttingMachineSerializer;
use common\modules\cutting\serializers\CuttingMaterialSerializer;
use common\modules\cutting\serializers\CuttingWorkpieceMaterialGroupsSerializer;
use frontend\modules\mybusiness\modules\cutting\widgets\CuttingMachineFormTabs;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var CuttingMachine $cuttingMachine */
/** @var \common\models\CuttingMaterial[] $cuttingMaterials */
/** @var \common\modules\cutting\models\CuttingWorkpieceMaterialGroup[] $workpieceMaterialGroups */


$companyService                    = $cuttingMachine->companyService;
$cuttingMachineSerialized          = CuttingMachineSerializer::serialize($cuttingMachine);
$cuttingMaterialsSerialized        = CuttingMaterialSerializer::serialize($cuttingMaterials);
$workpieceMaterialGroupsSerialized = CuttingWorkpieceMaterialGroupsSerializer::serialize($workpieceMaterialGroups);


Yii::$app->angular
    ->service(['notify', 'router', 'user', 'modal', 'cuttingService'])
    ->controller([
        'ps/cutting/CuttingWorkpieceMaterialsController',
        'company/companyServiceModels',
        'ps/cutting/CuttingMachineModels',
        'product/commonLib'
    ])
    ->controllerParams(
        [
            'cuttingMachine'          => $cuttingMachineSerialized,
            'workpieceMaterialGroups' => $workpieceMaterialGroupsSerialized,
            'cuttingMaterials'        => $cuttingMaterialsSerialized
        ]);

$form = ActiveForm::begin(
    [
        'id'                     => 'CompanyServicedForm',
        'options'                => ['enctype' => 'multipart/form-data', 'onsubmit' => ''],
        'enableClientValidation' => false,
    ]);
?>

<div ng-controller="CuttingWorkpieceMaterialsController">
    <div ng-cloak>
        <div class="row m-b10">
            <div class="col-sm-9 col-md-7 col-lg-8">
                <h3 class="product-add-title ugc-content">
                    <?php echo _t('front.user', 'Edit cutting machine: '); ?>
                    {{companyService.title}}
                </h3>
            </div>
            <div class="col-sm-3 col-md-2"></div>
        </div>

        <div class="designer-card p-l0 p-r0 p-t0 p-b0">

            <?= CuttingMachineFormTabs::widget(['cuttingMachine' => $cuttingMachine]); ?>

            <div class="product-edit">
                <div class="row border-b m-b20 hidden-xs hidden-sm">
                    <div class="col-md-3">
                        <h4><?= _t("site.ps", "Material"); ?></h4>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-4">
                                <h4><?= _t("site.ps", "Size (width/length/thickness) mm"); ?></h4>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4><?= _t("site.ps", "Color"); ?></h4>
                                    </div>
                                    <div class="col-md-3">
                                        <h4><?= _t("site.ps", "Price ({currency})", ['currency' => $companyService->company->paymentCurrency->title_original]); ?></h4>
                                    </div>
                                    <div class="col-md-1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div ng-repeat="workpieceMaterialGroup in workpieceMaterialGroups.list">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="hidden-md hidden-lg"><?= _t("site.ps", "Material"); ?></label>
                            <select
                                    class="form-control m-b20"
                                    ng-model="workpieceMaterialGroup.materialId"
                                    ng-options="material.id as material.title for material in getSelectableMaterials(workpieceMaterialGroup)"
                                    required
                                    ng-change="changeMaterialId(workpieceMaterialGroup)"
                            >
                            </select>
                        </div>
                        <div class="col-md-9">
                            <div class="row m-b20"
                                 ng-repeat="materialSize in workpieceMaterialGroup.sizes"
                                 id="materialSize-{{materialSize.id}}"
                            >
                                <div class="col-md-4">
                                    <label class="hidden-md hidden-lg"><?= _t("site.ps", "Size (width/length/thickness) mm"); ?></label>
                                    <div class="row p-r15 m-b20">
                                        <div class="col-xs-4 p-r0">
                                            <input class="form-control" type="number"
                                                   ng-model="materialSize.width"
                                                   required
                                                   placeholder="<?= _t('site.ps', 'width') ?>">
                                        </div>
                                        <div class="col-xs-4 p-r0">
                                            <input class="form-control" type="number"
                                                   ng-model="materialSize.height"
                                                   required
                                                   placeholder="<?= _t('site.ps', 'height') ?>">
                                        </div>
                                        <div class="col-xs-4 p-r0">
                                            <input class="form-control" type="number"
                                                   ng-model="materialSize.thickness"
                                                   required
                                                   placeholder="<?= _t('site.ps', 'thickness') ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="row"
                                         ng-repeat="materialColor in materialSize.colors"
                                         id="materialColor-{{materialColor.id}}"
                                    >
                                        <div class="col-xs-6 col-md-4 m-b20 p-r0">
                                            <label class="hidden-md hidden-lg"><?= _t("site.ps", "Color"); ?></label>
                                            <select
                                                    class="form-control"
                                                    required
                                                    ng-model="materialColor.code"
                                                    ng-options="material.code as material.title for material in getSelectableColors(workpieceMaterialGroup, materialSize, materialColor)"
                                                    ng-change="changeColorId(materialColor)"
                                            >
                                            </select>
                                            <div class="help-block text-warning m-b0" ng-if="!workpieceMaterialGroup.materialId"><?= _t("site.ps", "Please, select material"); ?></div>
                                        </div>
                                        <div class="col-xs-4 col-md-3 m-b20">
                                            <label class="hidden-md hidden-lg"><?= _t("site.ps", "Price ({currency})", ['currency' => $companyService->company->paymentCurrency->title_original]); ?></label>
                                            <input class="form-control" type="number"
                                                   required
                                                   ng-model="materialColor.price"
                                                   placeholder="<?= _t('site.ps', 'price') ?>">
                                        </div>
                                        <div class="col-xs-2 col-md-2 m-b20 p-r0 p-l0">
                                            <label class="hidden-md hidden-lg">&nbsp;</label>
                                            <button
                                                    ng-click="deleteMaterial(workpieceMaterialGroup, materialSize, materialColor)"
                                                    type="button" class="btn btn-info btn-circle display-b"
                                                    title="<?= _t("site.ps", "Delete"); ?>">
                                                <span class="tsi tsi-bin"></span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row" ng-if="canAddColor(materialSize, workpieceMaterialGroup.materialId)">
                                        <div class="col-xs-6 col-md-4 m-b20 p-r0">
                                            <label class=""><?= _t("site.ps", "Add Color"); ?></label>
                                            <select
                                                    class="form-control"
                                                    ng-model="newEmptyColor.code"
                                                    ng-options="material.code as material.title for material in getSelectableColors(workpieceMaterialGroup, materialSize)"
                                                    ng-change="changeEmptyColorId(materialSize, newEmptyColor.code)"
                                            >
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr class="m-t0 m-b0">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row border-b p-b20 m-b20">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-9">
                            <button
                                    ng-disabled="!canAddSize(workpieceMaterialGroup)"
                                    ng-click="addSize(workpieceMaterialGroup)"
                                    type="button"
                                    class="btn btn-default btn-sm btn-ghost add-color-button"><span
                                        class="tsi tsi-plus"></span>
                                <?= _t("site.ps", "Add size"); ?>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-xs-12 col-md-8 col-lg-7">
                        <button
                                ng-disabled="!canAddMaterial()"
                                ng-click="addMaterial()"
                                type="button" class="btn btn-primary m-b20 m-r10"
                                title="{{getAddMaterialTitle()}}"
                        >
                            <span class="tsi tsi-plus"></span> <?= _t("site.ps", "Add material"); ?>
                        </button>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-5">
                        <a class="add-new-color m-b0" href="/site/contact"
                           target="_blank"><?= _t('site.ps', 'Material or color not on the list? Contact us') ?></a>
                    </div>
                </div>
                <div class="row">
                    <hr>
                    <div class="col-lg-6">
                    </div>
                    <div class="col-lg-6">
                        <div class="row m-b10">
                            <div class="col-sm-4">
                                <button
                                        ng-click="prevStep()"
                                        type="button" class="btn btn-primary btn-ghost btn-block m-b10">
                                    <span class="tsi tsi-left"></span> <?= _t('site.ps', 'Back') ?>
                                </button>
                            </div>
                            <div class="col-sm-4">
                                <button
                                        ng-click="publish()"
                                        ng-if="cuttingMachine.id"
                                        type="button" class="btn btn-primary btn-block m-b10">
                                    <?= _t('site.common', 'Publish'); ?>
                                </button>
                            </div>
                            <div class="col-sm-4 ">
                                <button
                                        ng-click="nextStep()"
                                        ng-disabled="showNextProgress"
                                        type="button" class="btn btn-primary btn-block m-b10">
                                    <span ng-if="showNextProgress">
                                         <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Please wait...'); ?>
                                    </span>
                                    <span ng-if="!showNextProgress">
                                        <?= _t('site.common', 'Next'); ?>
                                        <span class="tsi tsi-right"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

