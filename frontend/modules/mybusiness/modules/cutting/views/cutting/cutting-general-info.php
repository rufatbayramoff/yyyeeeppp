<?php

use common\components\FileTypesHelper;
use common\models\CuttingMachine;
use common\modules\cutting\serializers\CuttingMachineSerializer;
use dosamigos\ckeditor\CKEditor;
use frontend\assets\AngularSortableAssetLib;
use frontend\assets\DropzoneAsset;
use frontend\modules\mybusiness\modules\cutting\widgets\CuttingMachineFormTabs;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var CuttingMachine $cuttingMachine */

DropzoneAsset::register($this);
AngularSortableAssetLib::register($this);
Yii::$app->angular->additionalModules(['angular-sortable-view']);

$companyService           = $cuttingMachine->companyService;
$cuttingMachineSerialized = CuttingMachineSerializer::serialize($cuttingMachine);

Yii::$app->angular
    ->service(['geo', 'notify', 'router', 'user', 'modal'])
    ->directive(['dropzone-image-crop', 'dropzone-button', 'fileread', 'image-rotate'])
    ->service(['cuttingService'])
    ->controller([
        'ps/cutting/CuttingMachineController',
        'company/companyServiceModels',
        'ps/cutting/CuttingMachineModels',
        'product/commonLib'
    ])
    ->controllerParams(
        [
            'cuttingMachine' => $cuttingMachineSerialized,
        ]);

$form = ActiveForm::begin(
    [
        'id'                     => 'CompanyServicedForm',
        'options'                => ['enctype' => 'multipart/form-data', 'onsubmit' => ''],
        'enableClientValidation' => false,
    ]);
?>

<div ng-controller="CuttingMachineController">
    <div ng-cloak>
        <div class="row m-b10">
            <div class="col-sm-9 col-md-7 col-lg-8">
                <h3 class="product-add-title ugc-content" ng-if="!cuttingMachine.id">
                    <?php echo _t('front.user', 'Add new cutting machine'); ?>
                </h3>
                <h3 class="product-add-title ugc-content" ng-if="cuttingMachine.id">
                    <?php echo _t('front.user', 'Edit cutting machine: '); ?>
                    {{companyService.title}}
                </h3>
            </div>
            <div class="col-sm-3 col-md-2"></div>
        </div>
        <div class="designer-card p-l0 p-r0 p-t0 p-b0">

            <?= CuttingMachineFormTabs::widget(['cuttingMachine' => $cuttingMachine]); ?>

            <div class="product-edit">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-2"
                               for="service-title"><?= _t('site.store', 'Title') ?>
                            <span class="form-required">*</span></label>
                        <div class="col-sm-6 col-md-5">
                            <?= $form->field($companyService, 'title')
                                ->textInput(['maxlength' => true, 'ng-model' => 'companyService.title'])->label(false) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-2"
                               for="service-description"><?= _t('site.store', 'Description') ?><span
                                    class="form-required">*</span></label>
                        <div class="col-sm-9 col-md-10">
                            <div class="form-group field-companyservice-description">
                <textarea id="companyservice-description"
                          class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                          name="CompanyService[description]"
                          rows="6" ng-model="companyService.description"
                ></textarea>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-2"
                               for="productform-unit_type"><?= _t('site.store', 'Photos') ?></label>
                        <div class="col-sm-9 col-md-10">
                            <div class="sortable-container" sv-root sv-part="companyService.images">
                                <div ng-repeat="image in companyService.images" image-rotate="image"
                                     sv-element="opts"
                                     class="sortable-grid-element">
                                    <div sv-handle class="sortable-grid-element__overlay"></div>
                                    <img class="sortable-grid-element-img" ng-src="{{image.src}}" alt=""
                                         data-image-rotate-flag="img"/>
                                    <button type="button" ng-click="companyService.removeImage(image)"
                                            class="sortable-grid-del-btn"
                                            title="<?= _t('company.service', 'Delete'); ?>">
                                        &times;
                                    </button>
                                    <button type="button" data-image-rotate-flag="button"
                                            class="sortable-grid-rotate-btn"
                                            title="<?= _t('company.service', 'Rotate'); ?>">
                                        <span class="tsi tsi-repeat"></span>
                                    </button>
                                </div>
                            </div>
                            <button
                                    dropzone-button="companyService.images"
                                    can-change-after-select="true"
                                    accepted-files=".<?= implode(',.', FileTypesHelper::ALLOW_IMAGES_EXTENSIONS); ?>"
                                    max-files="20"
                                    class="btn btn-primary btn-sm m-b10"
                                    title="<?= _t('site.ps', 'Upload'); ?>"
                                    type="button">
                                <span class="tsi tsi-plus m-r10"></span>
                                <?= _t('site.ps', 'Upload'); ?>
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-2"
                               for="cutting-service-max-width"><?= _t('site.store', 'Max width (mm)') ?>
                            <span class="form-required">*</span></label>
                        <div class="col-sm-6 col-md-5">
                            <?= $form->field($cuttingMachine, 'max_width')
                                ->textInput(['maxlength' => true, 'ng-model' => 'cuttingMachine.max_width', 'type' => 'number'])->label(false) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-2"
                               for="cutting-service-max-length"><?= _t('site.store', 'Max length (mm)') ?>
                            <span class="form-required">*</span></label>
                        <div class="col-sm-6 col-md-5">
                            <?= $form->field($cuttingMachine, 'max_length')
                                ->textInput(['maxlength' => true, 'ng-model' => 'cuttingMachine.max_length', 'type' => 'number'])->label(false) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-2"
                               for="cutting-service-min-order-price"><?= _t('site.store', 'Min order charge ({currency})', ['currency'=>$cuttingMachine->companyService->company->paymentCurrency->title_original  ]) ?></label>
                        <div class="col-sm-6 col-md-5">
                            <?= $form->field($cuttingMachine, 'min_order_price')
                                ->textInput(['maxlength' => true, 'ng-model' => 'cuttingMachine.min_order_price'])->label(false) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-2"
                               for="cutting-service-min-order-price"><?= _t('site.store', 'Technology') ?>
                               <span class="form-required">*</span></label>
                        <div class="col-sm-6 col-md-5">
                            <?= $form->field($cuttingMachine, 'technology')
                                ->dropDownList(CuttingMachine::getTechnologyLabels(), ['ng-model' => 'cuttingMachine.technology'])->label(false)  ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <hr>
                    <div class="col-lg-8">
                    </div>
                    <div class="col-lg-4">
                        <div class="row m-b10">
                            <div class="col-sm-6">
                                <button
                                        ng-click="publish()"
                                        ng-if="cuttingMachine.id"
                                        type="button" class="btn btn-primary btn-block m-b10">
                            <span ng-if="showNextProgress">
                                 <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Please wait...'); ?>
                            </span>
                                    <span ng-if="!showNextProgress">
                                <?= _t('site.common', 'Publish'); ?>
                            </span>
                                </button>
                            </div>
                            <div class="col-sm-6 ">
                                <button
                                    ng-click="nextStep()"
                                    ng-disabled="showNextProgress"
                                    type="button" class="btn btn-primary btn-block m-b10">
                            <span ng-if="showNextProgress">
                                 <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Please wait...'); ?>
                            </span>
                                    <span ng-if="!showNextProgress">
                                <?= _t('site.common', 'Next'); ?>
                                        <span class="tsi tsi-right"></span>
                            </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
