<?php

/** @var \yii\web\View $this */

use common\modules\cutting\serializers\CuttingMachineSerializer;
use yii\widgets\ActiveForm;

/** @var \common\models\CuttingMachine $cuttingMachine */

$deliveryTypes            = $cuttingMachine->companyService->deliveryTypess;
$cuttingMachineSerialized = CuttingMachineSerializer::serialize($cuttingMachine);

Yii::$app->angular
    ->service(['maps', 'geo', 'notify', 'router', 'user', 'modal', 'ps/baseSteps','cuttingService'])
    ->directive(['google-map', 'google-address-input'])
    ->controller([
        'ps/cutting/CuttingMachineController',
        'company/companyServiceModels',
        'ps/cutting/CuttingMachineModels',
        'product/commonLib'
    ])
    ->controllerParams(
        ['cuttingMachine' => $cuttingMachineSerialized,
         'step'           => $step
        ]
    );
$tabIndex = 1;
?>
<div ng-controller="CuttingMachineController">
    <div class="container" ng-cloak>
        <div class="row m-b10">
            <div class="col-sm-9 col-md-7 col-lg-8">
                <h3 class="product-add-title ugc-content" ng-if="!cuttingMachine.id">
                    <?php echo _t('front.user', 'Add new cutting machine'); ?>
                </h3>
                <h3 class="product-add-title ugc-content" ng-if="cuttingMachine.id">
                    <?php echo _t('front.user', 'Edit cutting machine: '); ?>
                    {{companyService.title}}
                </h3>
            </div>
            <div class="col-sm-3 col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-12 ps-steps">
                <?= \frontend\modules\mybusiness\widgets\CompanyCuttingMachineFormTabs::widget(['cuttingMachine' => $cuttingMachine, 'section' => $this->params['section2'] ?? null]); ?>

                <ul id="tabs" class="nav nav-tabs nav-tabs--secondary back-forward-tabs">
                    <li ng-class="{active : steps.isCurrentStep(1)}">
                        <a href="<?=\common\modules\cutting\helpers\CuttingUrlHelper::editGeneralInformation($cuttingMachine)?>">
                            <?= _t('site.ps', 'General'); ?>
                        </a>
                    </li>
                    <li ng-class="{active : steps.isCurrentStep(2)}">
                        <a>
                            <?= _t('site.ps', 'Materials'); ?>
                        </a>
                    </li>
                    <li ng-class="{active : steps.isCurrentStep(3)}">
                        <a>
                            <?= _t('site.ps', 'Processing'); ?>
                        </a>
                    </li>
                    <li ng-class="{active : steps.isCurrentStep(4)}">
                        <a>
                            <?= _t('site.ps', 'Delivery'); ?>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div
                            ng-if="steps.isCurrentStep(1)"
                            class="tab-pane active">
                        <?= $this->render('edit-cutting/cutting-model', ['cuttingMachine' => $cuttingMachine]) ?>
                    </div>
                    <?php /*
                    <div
                            ng-if="steps.isCurrentStep(2)"
                            class="tab-pane active">
                        <?= $this->render('edit-cutting/workpiece-material') ?>
                    </div>
                    <div
                            ng-if="steps.isCurrentStep(3)"
                            class="tab-pane active">
                        <?= $this->render('edit-cutting/processing-price') ?>
                    </div>
                    <div
                            ng-if="steps.isCurrentStep(4)"
                            class="tab-pane active">
                        <?= $this->render('edit-cutting/delivery-options') ?>
                    </div>
 */ ?>
                </div>


                <div class="ps-steps__footer clearfix">

                    <button
                            ng-if="!steps.isFirstStep()"
                            ng-click="steps.prevStep()"
                            type="button" class="btn btn-primary btn-ghost">
                        <span class="tsi tsi-left"></span> <?= _t('site.ps', 'Back') ?>
                    </button>

                    <button
                            ng-if="!steps.isLastStep()"
                            ng-click="steps.nextStep()"
                            ng-disabled="showNextProgress"
                            type="button" class="btn btn-primary">
                        <span ng-if="showNextProgress">
                             <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Please wait...'); ?>
                        </span>
                        <span ng-if="!showNextProgress">
                            <?= _t('site.common', 'Next'); ?>
                            <span class="tsi tsi-right"></span>
                        </span>
                    </button>
                    <button
                            ng-if="steps.isLastStep()"
                            loader-click="saveCuttingMachine()"
                            loader-click-unrestored="true"
                            type="button" class="btn btn-primary js-save-printer">
                        <?= _t('site.ps', 'Save') ?>
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>
