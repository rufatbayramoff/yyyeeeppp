<?php

use common\models\CuttingMachine;
use common\modules\cutting\serializers\CuttingMachineSerializer;
use frontend\modules\mybusiness\controllers\EditPrinterController;
use frontend\modules\mybusiness\modules\cutting\widgets\CuttingMachineFormTabs;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var CuttingMachine $cuttingMachine */
/** @var \common\models\CuttingMaterial[] $cuttingMaterials */

$companyService           = $cuttingMachine->companyService;
$cuttingMachineSerialized = CuttingMachineSerializer::serialize($cuttingMachine);
$psMachineSerialized      = EditPrinterController::jsonPsMachine($companyService);

Yii::$app->angular
    ->service(['maps', 'geo', 'notify', 'router', 'user', 'modal','cuttingService'])
    ->directive(['google-map', 'google-address-input'])
    ->resource(['PsMachine'])
    ->controller([
        'ps/cutting/CuttingDeliveryController',
        'company/companyServiceModels',
        'ps/cutting/CuttingMachineModels',
        'product/commonLib'
    ])
    ->controllerParams(
        [
            'cuttingMachine' => $cuttingMachineSerialized,
            'psMachine'      => $psMachineSerialized
        ]);

$form = ActiveForm::begin(
    [
        'id'                     => 'CompanyServicedForm',
        'options'                => ['enctype' => 'multipart/form-data', 'onsubmit' => ''],
        'enableClientValidation' => false,
    ]);
?>

<div ng-controller="CuttingDeliveryController">
    <div ng-cloak>
        <div class="row m-b10">
            <div class="col-sm-9 col-md-7 col-lg-8">
                <h3 class="product-add-title ugc-content">
                    <?php echo _t('front.user', 'Edit cutting machine: '); ?>
                    {{companyService.title}}
                </h3>
            </div>
            <div class="col-sm-3 col-md-2"></div>
        </div>

        <div class="designer-card p-l0 p-r0 p-t0 p-b0">

            <?= CuttingMachineFormTabs::widget(['cuttingMachine' => $cuttingMachine]); ?>

            <div class="product-edit">
                <?= $this->renderFile('@frontend/modules/mybusiness/views/services/edit-printer/delivery-options.php', ['psMachine' => $companyService]) ?>

                <div class="row">
                    <hr>
                    <div class="col-lg-6">
                    </div>
                    <div class="col-lg-6">
                        <div class="row m-b10">
                            <div class="col-sm-4">
                                <button
                                    ng-click="prevStep()"
                                    type="button" class="btn btn-primary btn-ghost btn-block m-b10">
                                    <span class="tsi tsi-left"></span> <?= _t('site.ps', 'Back') ?>
                                </button>
                            </div>
                            <div class="col-sm-4">
                                <button
                                        ng-click="publish()"
                                        ng-if="cuttingMachine.id"
                                        type="button" class="btn btn-primary btn-block m-b10">
                                    <?= _t('site.common', 'Publish'); ?>
                                </button>
                            </div>
                            <div class="col-sm-4 ">
                                <button
                                    ng-click="nextStep()"
                                    ng-disabled="showNextProgress"
                                    type="button" class="btn btn-primary btn-block m-b10">
                                    <span ng-if="showNextProgress">
                                         <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Please wait...'); ?>
                                    </span>
                                    <span ng-if="!showNextProgress">
                                        <?= _t('site.common', 'Save'); ?>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>

