<?php
/** @var string $content */
use frontend\models\user\UserFacade;

/** @var \yii\web\View $this */

$this->beginContent('@frontend/modules/mybusiness/views/layouts/mybusinessLayout.php');
?>

<div>
    <div class="container">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>




