<?php

namespace frontend\modules\mybusiness\modules\cutting\controllers;

use common\components\exceptions\OnlyPostRequestException;
use common\models\CuttingMachine;
use common\models\populators\PsMachinePopulator;
use common\models\repositories\PsMachineRepository;
use common\models\validators\PsMachineValidator;
use common\modules\cutting\factories\CuttingMachineFactory;
use common\modules\cutting\populators\CuttingMachinePopulator;
use common\modules\cutting\populators\CuttingWorkpieceMaterialPopulator;
use common\modules\cutting\repositories\CuttingRepository;
use common\modules\cutting\repositories\CuttingWorkpieceMaterialRepository;
use common\modules\cutting\serializers\CuttingMachineSerializer;
use common\modules\cutting\serializers\CuttingProcessingSerializer;
use common\modules\cutting\serializers\CuttingWorkpieceMaterialGroupsSerializer;
use common\modules\cutting\services\CuttingMachineService;
use common\modules\cutting\validators\CuttingEmptyValidator;
use common\modules\cutting\validators\CuttingWorkpieceMaterialListValidator;
use common\modules\product\models\ProductPlaceOrderState;
use common\services\LocationService;
use frontend\models\user\UserFacade;
use Psr\Log\InvalidArgumentException;
use yii\base\UserException;

class CuttingAjaxController extends \common\components\BaseController
{
    /** @var ProductPlaceOrderState */
    public $productPlaceOrderState;

    /** @var CuttingMachineService */
    protected $cuttingService;

    /**
     * @var CuttingMachinePopulator
     */
    protected $cuttingMachinePopulator;

    /**
     * @var CuttingMachineFactory
     */
    protected $cuttingMachineFactory;

    /**
     * @var CuttingRepository
     */
    protected $cuttingMachineRepository;

    /**
     * @var CuttingWorkpieceMaterialPopulator
     */
    protected $cuttingWorkpieceMaterialPopulator;

    /**
     * @var CuttingWorkpieceMaterialRepository
     */
    protected $cuttingWorkpieceMaterialRepository;

    /**
     * @var CuttingWorkpieceMaterialListValidator
     */
    protected $cuttingWorkpieceMaterialListValidator;

    /** @var PsMachinePopulator */
    protected $psMachinePopulator;

    /** @var PsMachineValidator */
    protected $psMachineValidator;

    /** @var PsMachineRepository */
    protected $psMachineRepository;

    /**
     * @var LocationService
     */
    private $locationService;

    /**
     * @var CuttingEmptyValidator
     */
    private $cuttingEmptyValidator;

    public function injectDependencies(
        CuttingMachineService $cuttingService,
        CuttingMachinePopulator $cuttingMachinePopulator,
        CuttingMachineFactory $cuttingMachineFactory,
        CuttingRepository $cuttingMachineRepository,
        CuttingWorkpieceMaterialPopulator $cuttingWorkpieceMaterialPopulator,
        CuttingWorkpieceMaterialRepository $cuttingWorkpieceMaterialRepository,
        CuttingWorkpieceMaterialListValidator $cuttingWorkpieceMaterialListValidator,
        PsMachinePopulator $psMachinePopulator,
        PsMachineValidator $psMachineValidator,
        PsMachineRepository $psMachineRepository,
        CuttingEmptyValidator $cuttingEmptyValidator
    )
    {
        $this->cuttingService                        = $cuttingService;
        $this->cuttingMachinePopulator               = $cuttingMachinePopulator;
        $this->cuttingMachineFactory                 = $cuttingMachineFactory;
        $this->cuttingMachineRepository              = $cuttingMachineRepository;
        $this->cuttingWorkpieceMaterialPopulator     = $cuttingWorkpieceMaterialPopulator;
        $this->cuttingWorkpieceMaterialRepository    = $cuttingWorkpieceMaterialRepository;
        $this->cuttingWorkpieceMaterialListValidator = $cuttingWorkpieceMaterialListValidator;
        $this->psMachinePopulator                    = $psMachinePopulator;
        $this->psMachineValidator                    = $psMachineValidator;
        $this->psMachineRepository                   = $psMachineRepository;
        $this->cuttingEmptyValidator                 = $cuttingEmptyValidator;
    }

    /**
     * @return array
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSaveGeneral()
    {
        if (!\Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $id = \Yii::$app->request->post('id');
        if ($id) {
            $cuttingMachine = CuttingMachine::tryFindByPk($id);
        } else {
            $cuttingMachine = $this->cuttingMachineFactory->createForCurrentUser();
        }
        $this->cuttingMachinePopulator->populate($cuttingMachine, \Yii::$app->request->post());
        $cuttingMachine->validateOrException();
        $this->cuttingMachineRepository->save($cuttingMachine);
        $this->cuttingService->addEmptyProcessingsForNewMaterials($cuttingMachine->companyService->company);
        $this->cuttingMachineRepository->saveCompanyProcessings($cuttingMachine->companyService->company);
        return $this->jsonSuccess(
            [
                'cuttingMachine' => CuttingMachineSerializer::serialize($cuttingMachine)
            ]
        );
    }

    public function actionSaveWorkpieceMaterials()
    {
        if (!\Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $currentUser = $this->getCurrentUser();
        $company     = $currentUser->company;
        $this->cuttingWorkpieceMaterialPopulator->populateWorkpieceMaterialGroups($company, \Yii::$app->request->post());
        $this->cuttingWorkpieceMaterialListValidator->validateOrFail($company->cuttingWorkpieceMaterials);
        $this->cuttingService->addEmptyProcessingsForNewMaterials($company);
        $this->cuttingWorkpieceMaterialRepository->save($company);
        $this->cuttingMachineRepository->saveCompanyProcessings($company);
        $workpieceMaterialGroups = $this->cuttingWorkpieceMaterialRepository->getWorkpieceMaterialGroups($company);
        return $this->jsonSuccess(
            [
                'workpieceMaterialGroups' => CuttingWorkpieceMaterialGroupsSerializer::serialize($workpieceMaterialGroups)
            ]
        );
    }

    public function actionSaveProcessing()
    {
        $cuttingMachine = $this->resolveCuttingMachine();
        $this->cuttingMachinePopulator->populateProcessing($cuttingMachine, \Yii::$app->request->post());
        $cuttingMachine->validateProcessingOrException();
        $this->cuttingMachineRepository->saveProcessing($cuttingMachine);
        $alarms = $cuttingMachine->checkProcessing();

        return $this->jsonSuccess(
            [
                'processing' => CuttingProcessingSerializer::serialize($cuttingMachine->cuttingMachineProcessings),
                'warning'    => reset($alarms)
            ]
        );
    }

    public function actionSaveDelivery()
    {
        $cuttingMachine = $this->resolveCuttingMachine();
        $this->psMachinePopulator->populate($cuttingMachine->companyService, \Yii::$app->request->post('psMachine'));
        $this->psMachineValidator->tryValidate($cuttingMachine->companyService);
        $this->psMachineRepository->save($cuttingMachine->companyService);
        return $this->jsonSuccess([
            'cuttingMachine' => CuttingMachineSerializer::serialize($cuttingMachine)
        ]);
    }

    public function actionSendToModeration()
    {
        $cuttingMachine = $this->resolveCuttingMachine();
        $cuttingMachine->validateOrException();
        $this->cuttingEmptyValidator->validateEmpty($cuttingMachine);
        $this->cuttingWorkpieceMaterialListValidator->validateOrFail($cuttingMachine->companyService->company->cuttingWorkpieceMaterials);
        $cuttingMachine->fullValidateProcessing();
        $this->psMachineValidator->tryValidate($cuttingMachine->companyService);
        $cuttingMachine->companyService->changeToPendingModeration();
        $cuttingMachine->companyService->safeSave();
        return $this->jsonSuccess();
    }

    protected function resolveCuttingMachine()
    {
        $id             = \Yii::$app->request->post('cattingMachineId');
        $cuttingMachine = CuttingMachine::tryFindByPk($id);
        if (!UserFacade::isObjectOwner($cuttingMachine->companyService->company)) {
            throw new UserException('No access to cutting machine');
        }
        return $cuttingMachine;
    }
}