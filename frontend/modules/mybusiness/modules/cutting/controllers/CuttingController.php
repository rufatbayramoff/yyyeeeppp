<?php

namespace frontend\modules\mybusiness\modules\cutting\controllers;

use common\models\CuttingMachine;
use common\modules\cutting\factories\CuttingMachineFactory;
use common\modules\cutting\populators\CuttingMachinePopulator;
use common\modules\cutting\repositories\CuttingRepository;
use common\modules\cutting\repositories\CuttingWorkpieceMaterialRepository;
use common\modules\cutting\serializers\CuttingMachineSerializer;
use common\modules\cutting\services\CuttingMachineService;
use common\modules\product\models\ProductPlaceOrderState;
use common\modules\product\services\ProductService;
use common\services\LocationService;
use frontend\modules\mybusiness\controllers\MybusinessController;
use Psr\Log\InvalidArgumentException;
use Yii;

class CuttingController extends MybusinessController
{
    public $layout = 'companyServicesLayout';

    /** @var CuttingMachineFactory */
    protected $cuttingMachineFactory;

    /** @var CuttingMachineService */
    protected $cuttingMachineService;

    /** @var CuttingRepository */
    protected $cuttingRepository;

    /** @var CuttingWorkpieceMaterialRepository */
    protected $cuttingWorkpieceMaterialRepository;

    public function injectDependencies(CuttingMachineFactory $cuttingMachineFactory,
                                       CuttingMachineService $cuttingMachineService,
                                       CuttingRepository $cuttingRepository,
                                       CuttingWorkpieceMaterialRepository $cuttingWorkpieceMaterialRepository)
    {
        $this->cuttingMachineFactory = $cuttingMachineFactory;
        $this->cuttingMachineService = $cuttingMachineService;
        $this->cuttingRepository     = $cuttingRepository;
        $this->cuttingWorkpieceMaterialRepository = $cuttingWorkpieceMaterialRepository;
    }

    public function init()
    {
        parent::init();
        $this->view->params['section'] = 'mybusiness/services';
    }

    public function actionCreate()
    {
        $user = $this->getCurrentUser();
        if (!$user->company) {
            return $this->redirect('/mybusiness/company/create-ps');
        }
        $cuttingMachine = $this->cuttingMachineFactory->createForCompany($user->company);
        return $this->render(
            'cutting-general-info',
            [
                'cuttingMachine' => $cuttingMachine,
            ]
        );
    }

    protected function resolveCuttingMachine($id)
    {
        $cuttingMachine = CuttingMachine::findByPk($id);
        $this->cuttingMachineService->checkIsAllowEditForCurrentUser($cuttingMachine);
        return $cuttingMachine;
    }

    public function actionEdit($id)
    {
        $cuttingMachine = $this->resolveCuttingMachine($id);
        CuttingMachine::findByPk($id);
        return $this->render(
            'cutting-general-info',
            [
                'cuttingMachine' => $cuttingMachine,
            ]
        );
    }

    public function actionWorkpieceMaterials($id)
    {
        $cuttingMachine = $this->resolveCuttingMachine($id);
        if (!$cuttingMachine->companyService->company->cuttingWorkpieceMaterials) {
            $this->cuttingMachineService->addEmptyWorkpieceMaterial($cuttingMachine->companyService->company);
        }

        $cuttingMaterials = $this->cuttingRepository->getCuttingMaterials();
        $workpieceMaterialGroups = $this->cuttingWorkpieceMaterialRepository->getWorkpieceMaterialGroups($cuttingMachine->companyService->company);
        return $this->render(
            'cutting-workpiece-materials',
            [
                'cuttingMaterials' => $cuttingMaterials,
                'cuttingMachine'   => $cuttingMachine,
                'workpieceMaterialGroups' => $workpieceMaterialGroups,

            ]
        );
    }

    public function actionProcessingPrice($id)
    {
        $cuttingMachine = $this->resolveCuttingMachine($id);
        if (!$cuttingMachine->cuttingMachineProcessings) {
            $this->cuttingMachineService->addEmptyProcessing($cuttingMachine);
        }

        return $this->render(
            'cutting-processing',
            [
                'cuttingMachine'   => $cuttingMachine,
            ]
        );
    }

    public function actionDeliveryDetails($id)
    {
        $cuttingMachine = $this->resolveCuttingMachine($id);

        return $this->render(
            'cutting-delivery',
            [
                'cuttingMachine'   => $cuttingMachine,
            ]
        );
    }

}
