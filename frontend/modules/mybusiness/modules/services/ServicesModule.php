<?php

namespace frontend\modules\mybusiness\modules\services;

/**
 * mybusiness-product module definition class
 */
class ServicesModule extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\mybusiness\modules\services\controllers';

    public const MODULE_PREFIX_URL = '/mybusiness/services';
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
