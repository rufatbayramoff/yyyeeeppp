<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\services\controllers;


use common\components\DateHelper;
use common\models\CompanyCertification;
use common\models\CompanyCertificationBind;
use common\models\CompanyServiceCertification;
use common\models\factories\FileFactory;
use common\models\Product;
use common\models\ProductCertification;
use common\models\repositories\FileRepository;
use frontend\modules\mybusiness\components\UserCompanyProvider;
use frontend\modules\mybusiness\models\CompanyCertificationForm;
use frontend\modules\mybusiness\models\CompanyServiceCertificationForm;
use frontend\modules\mybusiness\models\ProductCertificationForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class CertificationsController extends CompanyServiceController
{

    public function actionIndex($id)
    {
        $companyService = $this->repo->findByIdAndCompany($id, $this->user->company);
        $this->companyService = $companyService;
        $searchParams = ['company_service_id' => $id];
        $dataProvider = $this->search($searchParams);

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'companyService'      => $companyService,
                'company' => $this->user->company
            ]
        );
    }

    /**
     * Creates a new CompanyCertification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd($id = null, $type = null)
    {
        $fileFactory = Yii::createObject(FileFactory::class);
        $companyService = $this->repo->findByIdAndCompany($id, $this->user->company);
        $this->companyService = $companyService;
        $this->view->params['section2'] = 'mybusiness/company-services/certifications';
        $model = new CompanyServiceCertificationForm();
        $model->company_service_id = $companyService->id;
        if ($res = $model->load(Yii::$app->request->post())) {
            $uploadFile = UploadedFile::getInstance($model, 'file');
            $model->file = $uploadFile;
            if ($uploadFile) {
                $file = $fileFactory->createFileFromUploadedFile($uploadFile);
                $file->setPublicMode(1);
                $fileRepo = new FileRepository();
                $fileRepo->save($file);
                $model->file_id = $file->id;
            } else {
                $this->setFlashMsg(false, _t('mybusiness.certification', 'Image/PDF file is required.'));
                return $this->render(
                    'create',
                    [
                        'model' => $model,
                        'product' => $companyService
                    ]
                );
            }
            $model->created_at = DateHelper::now();
            if (!$model->validate()) {
                $this->setFlashMsg(false, Html::errorSummary($model));
            } else {
                $model->safeSave();
                return $this->redirect(['index', 'id' => $id]);
            }
        }
        return $this->render(
            'create',
            [
                'model' => $model,
                'company' => $this->user->company,
                'companyService' => $companyService,
            ]
        );
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $productUuid = $model->product_uuid;
        $model->delete();

        return $this->redirect(['index', 'uuid' => $productUuid]);
    }

    protected function findModel($id)
    {
        if (($model = ProductCertification::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function search($params)
    {
        $item = new CompanyServiceCertification();
        $query = CompanyServiceCertification::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );

        $item->load($params, '');

        $query->andFilterWhere(
            [
                'id'            => $item->id,
                'company_service_id' => $item->company_service_id,
            ]
        );

        return $dataProvider;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \backend\components\NoAccessException
     * @throws \yii\db\StaleObjectException
     */
    public function actionUnbindCertification($id)
    {
        $bind = CompanyCertificationBind::find()->where(['id'=>$id])->one();
        if(!$bind){
            throw new NotFoundHttpException('Not found');
        }
        $companyService = $this->repo->findByIdAndCompany($bind->company_service_id, $this->user->company);
        $this->companyService = $companyService;
        $bind->delete();
        return $this->redirect(['certifications/index', 'id'=>$companyService->id]);
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \backend\components\NoAccessException
     * @throws \yii\base\Exception
     */
    public function actionBindCertification()
    {
        $certificationBind = new CompanyCertificationBind();
        $certificationBind->load(Yii::$app->request->post());
        /**
         * @var $product Product
         */
        $companyService = $this->repo->findByIdAndCompany($certificationBind->company_service_id, $this->user->company);
        $companyBlock = CompanyCertification::tryFindByPk($certificationBind->certification_id);
        if ($companyBlock->company_id === $this->user->company->id) {
            if (!CompanyCertificationBind::find()
                ->where(['company_service_id' => $certificationBind->company_service_id, 'certification_id' => $certificationBind->certification_id])
                ->exists()) {
                $certificationBind->safeSave();
            }
        }
        return $this->redirect(['certifications/index', 'id'=>$certificationBind->company_service_id]);
    }
}