<?php
/**
 * User: nabi
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
 
/* @var $this \yii\web\View */

Yii::$app->angular
    ->directive(['youtube-embed']);
?>
<div class="product-edit">
    <?php
    $form = ActiveForm::begin(
        [ 
            'options' => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return false;',]
        ]
    );
    ?>

    <input type="hidden" name="ProductForm[videos]" id="productform-videos"/>

    <div class="row m-b30">
        <div class="col-sm-5 col-md-4">
            <input class="form-control m-b10" type="text" size="50" ng-model="newVideoUrl" placeholder="YouTube link"/>
        </div>
        <div class="col-sm-6 col-md-4">
            <button type="button" ng-click="companyService.addVideo(newVideoUrl);newVideoUrl=''"
                    class="btn btn-primary btn-ghost">
                <?= _t('mybusiness.product', 'Add video'); ?>
            </button>
            <p class="help-block">
                <?= _t('site.products', "Don't forget to click the Save button after adding or removing videos."); ?>
            </p>
        </div>
    </div>

    <div class="product-edit__video-list">
        <div ng-repeat="video in companyService.videos" class="product-edit__video-list-item">
            <a ng-href="#" ng-click="companyService.showVideoPreview(video, 'js-previewArea')">
                <img ng-src="{{video.thumb}}"/>
            </a>
            <button class="product-edit__video-list-del" title="<?= _t('mybusiness.product', 'Delete video'); ?>" ng-click="companyService.removeVideo($index)">&times;</button>
        </div>
    </div>
    <div id="js-previewArea" onclick="document.getElementById('js-previewArea').innerHTML=''"></div>

    <div class="row">
        <hr>
        <div class="col-lg-8">
        </div>
        <div class="col-lg-4">
            <div class="row m-b10">
                <div class="col-sm-6 ">
                    <input type="button" class="btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect"
                           value="<?=_t('mybusiness.store', 'Publish');?>" ng-click="submitForm('publish')"  ng-hide="companyService.isPublished">
                    <input type="button" class="btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect"
                           value="<?=_t('mybusiness.store', 'Save');?>" ng-click="submitForm('publish')"  ng-show="companyService.isPublished">
                </div>
                <div class="col-sm-6 ">
                    <input type="button" class="btn btn-ghost btn-info btn-block t-model-edit-unpublish m-b10 p-l10 p-r10 js-clickProtect"
                           value="<?=_t('mybusiness.store', 'Save unpublished');?>" ng-click="submitForm('unpublish')" ng-hide="companyService.isPublished">
                    <input type="button" class="btn btn-ghost btn-info btn-block t-model-edit-unpublish m-b10 p-l10 p-r10 js-clickProtect"
                           value="<?=_t('mybusiness.store', 'Unpublish');?>" ng-click="submitForm('unpublish')" ng-show="companyService.isPublished">
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>