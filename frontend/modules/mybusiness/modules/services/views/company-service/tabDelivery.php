<?php
/**
 * User: nabi
 */

use common\models\Product;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $companyService \common\modules\product\interfaces\ProductInterface */
/* @var $this \yii\web\View */



?>

<?php
$form = ActiveForm::begin(
    [
        'options' => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return false',]
    ]
);
?>
<div class="product-edit" >

    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productform-incoterms"><?= _t('site.store', 'Incoterms') ?></label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($companyService, 'incoterms')->dropDownList(Product::getIncotermsList(), ['ng-model'=>'companyService.incoterms'])->label(false); ?>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productform-outer_package"><?= _t('mybusiness.product', 'Ship from'); ?></label>
            <div class="col-sm-6 col-md-5">
                <div id="user-change-location-form">
                    <input type="text" id="productform-ship_from-location" value="<?= $companyService->getShipFromLocation(true)->formatted_address; ?>" class="form-control"/>
                    <input type="hidden" id="productform-ship_from" class="form-control" />
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <hr>
        <div class="col-lg-8">
        </div>
        <div class="col-lg-4">
            <div class="row m-b10">
                <div class="col-sm-6 ">
                    <input type="button" class="btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect"
                           value="<?=_t('mybusiness.store', 'Publish');?>" ng-click="submitForm('publish')"  ng-hide="companyService.isPublished">
                    <input type="button" class="btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect"
                           value="<?=_t('mybusiness.store', 'Save');?>" ng-click="submitForm('publish')"  ng-show="companyService.isPublished">
                </div>
                <div class="col-sm-6 ">
                    <input type="button" class="btn btn-ghost btn-info btn-block t-model-edit-unpublish m-b10 p-l10 p-r10 js-clickProtect"
                           value="<?=_t('mybusiness.store', 'Save unpublished');?>" ng-click="submitForm('unpublish')" ng-hide="companyService.isPublished">
                    <input type="button" class="btn btn-ghost btn-info btn-block t-model-edit-unpublish m-b10 p-l10 p-r10 js-clickProtect"
                           value="<?=_t('mybusiness.store', 'Unpublish');?>" ng-click="submitForm('unpublish')" ng-show="companyService.isPublished">
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>