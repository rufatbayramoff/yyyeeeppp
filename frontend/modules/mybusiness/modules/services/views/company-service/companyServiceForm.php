<?php

/** @var \yii\web\View $this * */
/* @var $companyService CompanyService */
/* @var $company \common\models\Company */
/* @var $model static */
/* @var $serviceCategories array|\common\models\CompanyServiceCategory[]|mixed|\yii\db\ActiveRecord[] */


use common\components\FileTypesHelper;
use common\models\CompanyService;
use common\models\Product;
use common\modules\product\serializers\CompanyBlockSerializer;
use dosamigos\ckeditor\CKEditor;
use frontend\models\user\UserFacade;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;

$user = UserFacade::getCurrentUser();
$this->title = _t('front.user', 'Service');


function printOption( $form, $data)
{
    $model =  new \common\models\CompanyServiceCategory();
    $model->id = $form->category_id;
    $result = $model->getCategoryTree($data);
    foreach ($result as $k => $category) {
        echo sprintf("<option value='%d' %s>%s</option>", $category['id'], $category['selected'], $category['space'] . $category['title']);
    }
}


$form = ActiveForm::begin(
    [
        'id'                     => 'CompanyServiceForm',
        'options'                => ['enctype' => 'multipart/form-data', 'onsubmit' => ''],
        'enableClientValidation' => false,
    ]
);
?>
    <div class="product-edit" >
        <div class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-3 col-md-2" for="productform-title"><?= _t('site.store', 'Title') ?><span class="form-required">*</span></label>
                <div class="col-sm-6 col-md-5">
                    <?= $form->field($companyService, 'title')
                        ->textInput(['maxlength' => true, 'ng-model' => 'companyService.title'])->label(false) ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3 col-md-2"><?= _t('site.ps', 'Type of service') ?></label>
                <div class="col-sm-5 col-md-4">
                    <div class="form-group">
                        <select class="form-control" name="CompanyServiceAddForm[category_id]" ng-model="companyService.category_id">
                            <?= printOption($companyService, $serviceCategories); ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="form-group">
                <label class="control-label col-sm-3 col-md-2" for="productform-unit_type"><?= _t('mybusiness.product', 'Tags') ?></label>

                <div class="col-sm-6 col-md-5">
                    <?php
                    $companyServiceTagsTexts = \yii\helpers\ArrayHelper::map($companyService->tags, 'id', 'text');
                    echo $form->field($companyService, 'tags')->widget(
                        Select2::class,
                        common\components\SimpleSelect2::getAjax(
                            ['company-service/tags'],
                            'id,text',
                            $companyServiceTagsTexts,
                            [
                                'options'       => ['placeholder' => _t('front.model3d', 'Add tags') . '... ', 'multiple' => true ],
                                'pluginOptions' => [
                                    'tags'               => true,
                                    //'allowClear' => true,
                                    'minimumInputLength' => 2,
                                    'maximumInputLength' => 45
                                ]
                            ]
                        )
                    )->label(false); ?>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-sm-3 col-md-2" for="productform-unit_type"><?= _t('site.store', 'Photos') ?></label>
                <div class="col-sm-9 col-md-10">
                    <div class="sortable-container" sv-root sv-part="companyService.images">
                        <div ng-repeat="image in companyService.images" image-rotate="image" sv-element="opts" class="sortable-grid-element">
                            <div sv-handle class="sortable-grid-element__overlay"></div>
                            <img class="sortable-grid-element-img" ng-src="{{image.src}}" alt="" data-image-rotate-flag="img"/>
                            <button type="button" ng-click="companyService.removeImage(image)" class="sortable-grid-del-btn"
                                    title="<?= _t('company.service', 'Delete'); ?>">
                                &times;
                            </button>
                            <button type="button" data-image-rotate-flag="button"
                                    class="sortable-grid-rotate-btn"
                                    title="<?= _t('company.service', 'Rotate'); ?>">
                                <span class="tsi tsi-repeat"></span>
                            </button>
                        </div>
                    </div>
                    <button
                            dropzone-button="companyService.images"
                            can-change-after-select="true"
                            accepted-files=".<?= implode(',.', FileTypesHelper::ALLOW_IMAGES_EXTENSIONS); ?>"
                            max-files="20"
                            class="btn btn-primary btn-sm m-b10"
                            title="<?= _t('site.ps', 'Upload'); ?>"
                            type="button">
                        <span class="tsi tsi-plus m-r10"></span>
                        <?= _t('site.ps', 'Upload'); ?>
                    </button>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3 col-md-2" for="productform-unit_type"> </label>
                <div class="col-sm-9 col-md-8">
                    <div class="row">
                        <div class="col-sm-11 col-md-8">
                            <div class="table-responsive">
                                <table class="table table-bordered" border="1" ng-cloak="true">
                                    <tr>
                                        <td></td>
                                        <th width="25%">
                                            <nobr>
                                            <?= _t('mybusiness.product', 'Price from'); ?>
                                            <?=
                                            frontend\widgets\SiteHelpWidget::widget(['title' => 'Help', 'alias' => 'mybusiness.service.addservice.price'])
                                            ?></nobr>
                                        </th>
                                        <td>  </td>
                                        <th widht="30%"></th>
                                        <th width="20%"><?= _t('mybusiness.product', 'Minimum'); ?></th>
                                        <td></td>
                                    </tr>
                                    <tr ng-repeat="productPrice in companyService.productPrices">
                                        <td><span style="font-size: 16px; line-height: 36px"><?=$companyService->company->paymentCurrency->title_original?></span></td>
                                        <td class="p-l0"><input type="number" step="0.01" min="0.1" class="form-control" ng-model="productPrice.priceWithFee"
                                                   ng-change="calculatePrice(productPrice)"></td>

                                        <td><span style="font-size: 16px; line-height: 36px">/</span></td><td>  <?= $form->field($companyService, 'unit_type')
                                                ->dropDownList(CompanyService::getUnitTypesList(), ['ng-model' => 'companyService.unitType'])->label(false); ?>
                                        </td>
                                        <td class="p-r0"><input type="text" class="form-control" ng-model="productPrice.moq"></td>
                                        <td><span style="font-size: 16px; line-height: 36px">{{companyService.unitTypeLabel}}</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3 col-md-2" for="productform-supply_ability"><?= _t('site.store', 'Service  ability') ?></label>
                <div class="col-sm-6 col-md-4">
                    <div class="row">
                        <div class="col-xs-7 m-r0 p-r0">
                            <?= $form->field($companyService, 'supply_ability')
                                ->input('number', ['maxlength' => true, 'ng-model'=>'companyService.supply_ability'])->label(false); ?>
                        </div>
                        <div class="col-xs-5">
                            <?= $form->field($companyService, 'supply_ability_time')
                                ->dropDownList(Product::getSupplyAbilityTimeList(),  ['ng-model' => 'companyService.supply_ability_time'])->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3 col-md-2" for="productform-avg_production"><?= _t('site.store', 'Avg. production time') ?></label>
                <div class="col-sm-6 col-md-4">
                    <div class="row">
                        <div class="col-xs-7 m-r0 p-r0">
                            <?= $form->field($companyService, 'avg_production')->input('number', ['maxlength' => true, 'ng-model'=>'companyService.avg_production'])->label(false); ?>
                        </div>
                        <div class="col-xs-5">
                            <?= $form->field($companyService, 'avg_production_time')
                                ->dropDownList(Product::getAvgProductionTimeList(), ['ng-model'=>'companyService.avg_production_time'])
                                ->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3 col-md-2" for="productform-description"><?= _t('site.store', 'Description') ?></label>
                <div class="col-sm-9 col-md-10">
                    <?= $form->field($companyService, 'description')
                        ->widget(CKEditor::className(),
                            \frontend\components\CKEditorDefault::getDefaults(['options' => ['ng-model' => 'companyService.description']]))
                        ->label(false); ?>
                </div>
            </div>


        </div>


        <div class="row">
            <hr class="m-t0 m-b0">
        </div>

        <div class="row">
            <div class="col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-2">
                <p class="m-t20">
                    <div class="row">

                <div class="col-lg-4">
                    <strong><?= _t('site.store', 'Moderation status') . ': ' ?></strong><br />
                    {{companyService.moderatorStatusLabel}}
                </div><div class="col-lg-4 pull-left">
                    <strong><?= _t('site.store', 'Published') . ': ' ?></strong><br />
                    {{companyService.visibilityLabel}}</div>
                </div>
                </p>

                <div class="row m-b10">
                    <div class="col-sm-6 col-md-4">
                        <input type="button" class="btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect"
                               value="<?=_t('mybusiness.store', 'Publish');?>" ng-click="submitForm('publish')"  ng-hide="companyService.isPublished">
                        <input type="button" class="btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect"
                               value="<?=_t('mybusiness.store', 'Save');?>" ng-click="submitForm('publish')"  ng-show="companyService.isPublished">
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <input type="button" class="btn btn-ghost btn-info btn-block t-model-edit-unpublish m-b10 p-l10 p-r10 js-clickProtect"
                               value="<?=_t('mybusiness.store', 'Save unpublished');?>" ng-click="submitForm('unpublish')" ng-hide="companyService.isPublished">
                        <input type="button" class="btn btn-ghost btn-info btn-block t-model-edit-unpublish m-b10 p-l10 p-r10 js-clickProtect"
                               value="<?=_t('mybusiness.store', 'Unpublish');?>" ng-click="submitForm('unpublish')" ng-show="companyService.isPublished">
                    </div>
                  
                </div>
            </div>
        </div>

    </div>

<?php ActiveForm::end(); ?>