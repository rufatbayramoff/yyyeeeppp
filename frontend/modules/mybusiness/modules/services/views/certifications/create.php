<?php

/* @var $this \yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<div class="container">
    <?php

    $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data'] ]); ?>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 col-md-2">
                <?= Html::a('<span class="tsi tsi-left m-r10"></span>' . _t('mybusiness.certifications', 'Back'), ['index', 'id'=>$model->company_service_id], ['class' => 'btn btn-sm btn-default']) ?>
            </label>
            <div class="col-sm-6">
                <h3 class="m-t0 m-b0">
                    <?= _t('mybusiness.certifications', 'Add Certification') ?>
                </h3>
            </div>
        </div>
        <div class="form-group field-CompanyServiceCertificationForm-title required">
            <label class="control-label col-sm-3 col-md-2" for="CompanyServiceCertificationForm-title"><?= _t('mybusiness.certifications', 'Title') ?></label>
            <div class="col-sm-6">
                <input type="text" id="CompanyServiceCertificationForm-title" class="form-control" name="CompanyServiceCertificationForm[title]" maxlength="45" aria-required="true">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-CompanyServiceCertificationForm-file">
            <label class="control-label col-sm-3 col-md-2" for="CompanyServiceCertificationForm-file"><?= _t('mybusiness.certifications', 'Image/PDF') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="hidden" name="CompanyServiceCertificationForm[file]" value="">
                <input type="file" id="CompanyServiceCertificationForm-file" class="inputfile" name="CompanyServiceCertificationForm[file]" value="" data-multiple-caption="{count} files selected" accept=".pdf, .jpg, .gif, .png">
                <label class="uploadlabel" for="CompanyServiceCertificationForm-file">
                    <span>
                        <i class="tsi tsi-upload-l"></i>
                        <?= _t('mybusiness.certifications', 'Upload File') ?>
                    </span>
                </label>
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-CompanyServiceCertificationForm-certifier">
            <label class="control-label col-sm-3 col-md-2" for="CompanyServiceCertificationForm-certifier"><?= _t('mybusiness.certifications', 'Certifier') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="text" id="CompanyServiceCertificationForm-certifier" class="form-control" name="CompanyServiceCertificationForm[certifier]" maxlength="45">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-CompanyServiceCertificationForm-application">
            <label class="control-label col-sm-3 col-md-2" for="CompanyServiceCertificationForm-application"><?= _t('mybusiness.certifications', 'Application') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="text" id="CompanyServiceCertificationForm-application" class="form-control" name="CompanyServiceCertificationForm[application]" maxlength="45">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-CompanyServiceCertificationForm-issue_date required">
            <label class="control-label col-sm-3 col-md-2" for="CompanyServiceCertificationForm-issue_date"><?= _t('mybusiness.certifications', 'Issue date') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="date" id="CompanyServiceCertificationForm-issue_date" class="form-control" name="CompanyServiceCertificationForm[issue_date]" aria-required="true">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-CompanyServiceCertificationForm-expire_date">
            <label class="control-label col-sm-3 col-md-2" for="CompanyServiceCertificationForm-expire_date"><?= _t('mybusiness.certifications', 'Expiry date') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="date" id="CompanyServiceCertificationForm-expire_date" class="form-control" name="CompanyServiceCertificationForm[expire_date]">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"></label>
            <div class="col-sm-6 col-md-5">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success js-clickProtect' : 'js-clickProtect btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
