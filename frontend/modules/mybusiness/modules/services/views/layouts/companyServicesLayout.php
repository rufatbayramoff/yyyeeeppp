<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 04.04.2018
 * Time: 18:14
 */

use common\models\CompanyService;
use common\modules\product\serializers\ProductSerializer;
use frontend\assets\AngularSortableAsset;
use frontend\assets\AngularSortableAssetLib;
use frontend\assets\DropzoneAsset;
use frontend\components\angular\AngularService;
use frontend\modules\mybusiness\serializers\CompanyServiceSerializer;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->beginContent('@frontend/modules/mybusiness/views/layouts/mybusinessLayout.php');

DropzoneAsset::register($this);
AngularSortableAssetLib::register($this);
Yii::$app->angular->additionalModules(['angular-sortable-view']);

/**
 * @var $companyService CompanyService
 */
$companyService = $this->context->companyService;
$feeHelper = \common\modules\payment\fee\FeeHelper::init();


$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);

$unitTypesPlural = array_combine(array_values(CompanyService::getUnitTypesList()), array_values(CompanyService::getUnitTypesList(true)));
Yii::$app->angular
    ->service(['notify', 'user', 'modal', 'router', 'googleLocationApi'])
    ->directive(['dropzone-image-crop', 'dropzone-button', 'fileread', 'image-rotate'])
    ->controller(
        [
            'company/companyServiceController',
            'product/commonLib',
            'product/userVideo',
            'company/companyServiceModels'
        ]
    )

    ->constant('locationInputElementId', 'productform-ship_from-location')
    ->constant('locationResultInputElId', 'productform-ship_from')
    ->controllerParam('unitTypes', $unitTypesPlural)
    ->controllerParam('companyService', CompanyServiceSerializer::serialize($companyService));

?>

<div class="container">

    <div class="row m-b10">
        <div class="col-lg-8">
            <h4 class="m-t0 m-b10 ugc-content">
                <?php if ($companyService->isNewRecord) { ?>
                    <?= _t('mybusiness.product', 'Add Service'); ?>
                <?php } else { ?>
                    <?= _t('mybusiness.product', 'Edit Service'); ?>:  <?= H($companyService->title); ?>
                <?php } ?>
            </h4>

            <?php if ($companyService->isRejected()): ?>
                <div class="bar bg-warning m-l20 m-b10">
                    <b><?= _t('mybusiness.services', 'Rejected'); ?>. </b>
                    <?= \common\models\ModerLog::getLastRejectModerLog(\common\models\ModerLog::TYPE_SERVICE, $companyService->id); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-4">
            <?php
            if (!$companyService->isNewRecord) {
                ?>
                <div class="product-action">
                    <a href='<?= Url::toRoute(['/mybusiness/company-services/company-service/delete', 'serviceId' => $companyService->id]) ?>'
                       class="btn btn-default btn-sm m-r20 m-b10"
                       data-confirm="<?= _t('site', 'Are you sure?') ?>"
                       data-method="post"><?= _t('site.store', 'Delete service') ?></a>

                    <a href='<?= $companyService->getPublicPageUrl() ?>' target="_blank"
                       class="btn btn-default btn-sm m-b10"><?= _t('site.store', 'Preview listing') ?></a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <div class="designer-card p-l0 p-r0 p-t0 p-b0">

        <?= \frontend\modules\mybusiness\widgets\CompanyServiceFormTabs::widget(['companyService' => $companyService, 'section' => $this->params['section2'] ?? null]); ?>


        <div id="productFormControllerEl" ng-controller="CompanyServiceCreateController" class="m-t20">
            <?= $content; ?>
        </div>

    </div>


</div>

<?php $this->endContent(); ?>
