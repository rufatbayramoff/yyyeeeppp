<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $companyService \common\models\Product */
/* @var $company \common\models\Company */

$this->title = 'Additional Information';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-block-index product-edit">

    <p class="m-b20">
        <?= Html::a('Add Block', ['add', 'id'=>$companyService->id], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="row">
        <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'format' => 'raw',
                        'value' => function($model){
                            $linkUp = \yii\helpers\Url::toRoute(['move', 'direction'=>'up', 'id'=>$model->id]);
                            $linkDown = \yii\helpers\Url::toRoute(['move', 'direction'=>'down', 'id'=>$model->id]);
                            $htmlUp = '<a href="'.$linkUp.'" class="btn btn-info btn-xs p-l10 p-r10"><span class="tsi tsi-up"></span></a>';
                            $htmlDown = '<a href="'.$linkDown.'"  class="btn btn-info btn-xs p-l10 p-r10"><span class="tsi tsi-down"></span></a>';
                            return $htmlUp . ' ' . $htmlDown;

                        }
                    ],
                    [
                        'attribute'=>'content',
                        'format' => 'raw',
                        'value' => function($model){
                            $text = yii\helpers\StringHelper::truncateWords(strip_tags($model->content), 20) . '...<br/>';
                            $images = \backend\widgets\FilesListWidget::widget(
                                [
                                    'formPrefix'             => $model->formName(),
                                    'formAttribute'          => 'imageFiles',
                                    'filesList'              => $model->getImages(),
                                ]
                            );
                            $header = '<h3 class="m-t0">' . Html::a(H($model->title), ['update', 'id'=>$model->id]) .'</h3>';
                            return '<div class="ugc-content">' . $header . $text . '</div>' . $images;

                        }
                    ],
                    'is_visible:boolean',
                    ['class' => 'yii\grid\ActionColumn'],
                ],
                'tableOptions' => [
                    'class' => 'table table-bordered m-b0 product-block-index__view-table'
                ],
            ]); ?>

        </div>
    </div>

    <h3><?= _t('mybusiness.product', 'Assigned from company blocks'); ?></h3>
    <ul class="list-unstyled">
        <?php
        $companyBlocks = $companyService->getBindedCompanyBlocks();
        foreach($companyBlocks as $companyBlock):
            ?>
            <li class="p-b10"><?=Html::a($companyBlock->block->title, ['/mybusiness/company-blocks/view', 'id'=>$companyBlock->block_id], ['target'=>'_blank','class'=>'m-r10']); ?>
                <?=Html::a(\frontend\components\Icon::get('delete'), ['unbind-block', 'id'=>$companyBlock->id], ['class'=>'btn btn-xs btn-circle btn-info']); ?></li>
        <?php endforeach; ?>
    </ul>

    <div class="panel p-b20">
        <?php $form = ActiveForm::begin(['layout' => 'inline', 'action' => ['bind-block']]);
        $blockBind = new \common\models\CompanyBlockBind();
        $blockBind->company_service_id = $companyService->id;
        $blockList = \common\components\ArrayHelper::map($company->getCompanyBlocks()->asArray()->all(), 'id', 'title');
        $companyServiceBlocks = $companyService->getBindedCompanyBlockIds();
        foreach($blockList as $blockId=>$blockItem){ if(in_array($blockId, $companyServiceBlocks)) unset($blockList[$blockId]); }
        if(!empty($blockList)){
            ?>
            <span class="m-r10">
                <?=_t('mybusines.product', 'Company block'); ?>
            </span>
            <?= $form->field($blockBind, 'company_service_id')->hiddenInput()->label(false); ?>

            <?= $form->field($blockBind, 'block_id')->dropDownList($blockList, ['style'=>'max-width:300px;margin-right:15px;'])->label(false); ?>
            <?= Html::submitButton(Yii::t('mybusiness.product', 'Assign'), ['class' => 'btn btn-primary js-clickProtect']) ?>
        <?php } ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.ugc-content table').wrap('<div class="table-responsive m-t10"></div>');
    });
</script>