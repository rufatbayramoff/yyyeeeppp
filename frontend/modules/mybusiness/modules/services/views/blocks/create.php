<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CompanyServiceBlock */

$this->title = 'Add Information';
$this->params['breadcrumbs'][] = ['label' => ' Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-block-create product-edit">
    <?= $this->render('_form', [
        'model' => $model, 'companyService'=>$companyService
    ]) ?>

</div>
