<?php


namespace frontend\modules\mybusiness\modules\product\helper;

use common\models\SiteTag;
use common\modules\product\interfaces\ProductInterface;
use yii\base\BaseObject;

class ProductUrlHelper extends BaseObject
{
    public static function productsCatalog($category = 'all', $type = '', $text=''): string
    {
        $url = '/products/' . $category;
        $params = '';
        if ($type) {
            $params .= '&type=' . $type;
        }
        if ($text) {
            $params .= '&text=' . $text;
        }
        if ($params) {
            $url.='?'.substr($params,1,1024);
        }
        return $url;
    }

    public static function productsMain(): string
    {
        return '/products';
    }

    public static function productsCatalogTag(SiteTag $tag): string
    {
        return str_replace('??', '?', \yii\helpers\Url::toRoute(['/product/product/index/', 'tags' => [\H($tag->text)]]));
    }

    public static function editProductUrl(ProductInterface $product)
    {
        return '/mybusiness/products/product/edit?uid=' . $product->productCommon->uid;
    }

    public static function productsModel3d()
    {
        return '/products/all?type=model3d';
    }

    public static function createByPs(string $type)
    {
        if ($type==ProductInterface::TYPE_MODEL3D) {
            return '/upload?noredir=1';
        }
        return '/mybusiness/products/product/create';
    }
}