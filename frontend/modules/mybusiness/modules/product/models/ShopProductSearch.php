<?php

namespace frontend\modules\mybusiness\modules\product\models;

use common\models\Product;
use common\models\ProductCommon;
use common\models\SiteTag;
use common\modules\product\interfaces\ProductInterface;
use yii\base\BaseObject;

class ShopProductSearch extends BaseObject
{
    public const STATUS_ALL = 'all';
    public const STATUS_PUBLISHED = 'published';
    public const STATUS_PENDING = 'pending';
    public const STATUS_DRAFT = 'draft';

    public const SEARCH_TYPES_MAP = [
        self::STATUS_ALL       => '',
        self::STATUS_PUBLISHED => ProductInterface::PUBLIC_STATUSES,
        self::STATUS_PENDING   => ProductInterface::PENDING_STATUSES,
        self::STATUS_DRAFT     => ProductInterface::STATUS_DRAFT,
    ];


    public $type = ProductInterface::TYPE_PRODUCT;
    public $status = self::STATUS_ALL;

    public function getSearchTypes()
    {
        return self::SEARCH_TYPES_MAP[$this->status];
    }

    public function isAll()
    {
        return !$this->status || ($this->status === self::STATUS_ALL);
    }

    public function isPublished()
    {
        return $this->status === self::STATUS_PUBLISHED;
    }

    public function isPending()
    {
        return $this->status === self::STATUS_PENDING;
    }

    public function isDraft()
    {
        return $this->status === self::STATUS_DRAFT;
    }

    public function load($data)
    {
        if (!is_array($data)) {
            return;
        }
        if (array_key_exists('status', $data)) {
            $this->status = $data['status'];
        }
        if (array_key_exists('type', $data)) {
            $this->type = $data['type'];
        }
    }

}
