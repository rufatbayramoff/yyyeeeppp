<?php

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\mybusiness\models\ProductCertificationForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<div class="product-edit">
    <?php

    $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data'] ]); ?>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productfileform-file"><?= _t('mybusiness.product', 'File') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="hidden" name="ProductFileForm[file]" value="">
                <input type="file" id="productfileform-file" class="inputfile" name="ProductFileForm[file]" value="" data-multiple-caption="{count} files selected">
                <label class="uploadlabel" for="productfileform-file">
                    <span>
                        <i class="tsi tsi-upload-l"></i>
                        <?= _t('mybusiness.product', 'Upload file') ?>
                    </span>
                </label>
                <p class="help-block help-block-error "></p>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productfileform-title"><?= _t('mybusiness.product', 'Title') ?></label>
            <div class="col-sm-6 col-md-5">
                <div class="">
                    <input type="text" id="productfileform-title" class="form-control" name="ProductFileForm[title]" maxlength="245" aria-required="true">
                    <p class="help-block help-block-error "></p>
                </div>
            </div>
        </div>
        <div class="form-group p-b20">
            <label class="control-label col-sm-3 col-md-2" for="productfileform-title"></label>
            <div class="col-sm-6 col-md-5">
                <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success js-clickProtect' : 'btn btn-primary js-clickProtect']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
