<?php

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

/* @var $product \common\modules\product\interfaces\ProductInterface */

use yii\grid\GridView;
use yii\helpers\Html;

?>


<div class="product-edit">

    <p class="m-b20">
        <?= Html::a(_t('app', 'Add'), ['add', 'uuid' => $product->uuid], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    // echo \yii\widgets\ListView::widget(['dataProvider' => $dataProvider,'itemView'     => '_list']);
    ?>
    <div class="row">
        <div class="table-responsive">
        <?= GridView::widget(
            [
                'dataProvider' => $dataProvider,
                'columns'      => [
                    [
                        'label' => 'Render',
                        'format' => 'raw',
                        'value' => function(\common\models\ProductFile $productFile){
                            if(!\common\components\FileTypesHelper::isRenderable($productFile->file)){
                                return 'Not available';
                            }
                            $renderParams = new \common\modules\model3dRender\models\RenderFileParams();
                            $renderParams->color = 'FFFFFF';
                            $renderParams->ambient = \common\models\PrinterColor::WHITE_COLOR_AMBIENT;
                            if($productFile->file->getFileExtension()=='ply'){
                              #  $renderParams->color = \common\models\PrinterColor::MULTICOLOR;
                            }
                            $img = Yii::$app->getModule('model3dRender')->renderer->getRenderImageUrl($productFile->file, $renderParams);
                            return Html::img($img, ['width'=>150]);
                        }
                    ],
                    [
                        'attribute' => 'title',
                    ],
                    [
                        'attribute' => 'file_id',
                        'label'     => _t('mybusiness.product', 'CAD File'),
                        'format'    => 'raw',
                        'value'     => function (\common\models\ProductFile $productFile) {
                            return Html::a($productFile->file->getCleanFileNameV3(), $productFile->file->getFileUrl());

                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
                ],
                'tableOptions' => [
                    'class' => 'table table-bordered m-b0 product-edit__models-table'
                ],
            ]
        ); ?>
        </div>
    </div>

</div>
