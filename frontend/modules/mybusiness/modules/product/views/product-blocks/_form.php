<?php

use backend\widgets\FilesListWidget;
use common\components\FileTypesHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductBlock */
/* @var $form yii\widgets\ActiveForm */
/* @var $product array|\common\models\Product|mixed|null|\yii\db\ActiveRecord */
$model->product_uuid = $product->uuid;
?>

<div class="product-block-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'product_uuid')->hiddenInput()->label(false); ?>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productblock-title"><?= _t('site.store', 'Title') ?></label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productblock-content"><?= _t('site.store', 'Content') ?></label>
            <div class="col-sm-9 col-md-10">
                <?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), \frontend\components\CKEditorDefault::getDefaults())->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"><?= _t('site.store', 'Visibility') ?></label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($model, 'is_visible')->checkbox(['template' => '<div class="checkbox">{input}{label}</div>']) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"><?= _t('site.store', 'Pictures') ?></label>
            <div class="col-sm-6 col-md-5 p-t10">
                <?= FilesListWidget::widget(
                    [
                        'formPrefix'             => $model->formName(),
                        'formAttribute'          => 'imageFiles',
                        'filesList'              => $model->getImages(),
                        'rights'                 => [
                            FilesListWidget::ALLOW_DELETE,
                            FilesListWidget::ALLOW_ADD,
                            FilesListWidget::ALLOW_MULTIPLE,
                        ],
                        'filesAllowedExtensions' => FileTypesHelper::ALLOW_IMAGES_EXTENSIONS
                    ]
                ) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"></label>
            <div class="col-sm-6 col-md-5 p-b20">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success js-clickProtect' : 'js-clickProtect btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
