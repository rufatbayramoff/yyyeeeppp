<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductBlock */

$this->title = 'Update  Block: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Additional information', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-block-update product-edit">

    <?= $this->render('_form', [
        'model' => $model, 'product' => $product
    ]) ?>

</div>
