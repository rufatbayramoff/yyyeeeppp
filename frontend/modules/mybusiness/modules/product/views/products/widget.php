<?php
/** @var \yii\web\View $this * */
/** @var array $items * */

use frontend\assets\CollectionAsset;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;
use yii\helpers\Url;

$user = UserFacade::getCurrentUser();
$this->title = _t('front.user', 'My Business Widgets');

$this->registerAssetBundle(CollectionAsset::class);


Yii::$app->angular
    ->service(['notify', 'user', 'router', 'facebookApi', 'modal'])
    ->controller('ps/printers/embed')
    ->constant('facebookApplicationId', Yii::$app->params['facebook_ps_print_application_id'])
    ->controllerParams([
        'ps'                    => null,
        'facebookApplicationId' => Yii::$app->params['facebook_ps_print_application_id'],
        'server' => param('server'),
        'userUrl' => UserUtils::getUserStoreUrl($user, true),
    ])
    ->resource('PsPrinter');

?>

<?php $this->beginBlock('control-buttons') ?>
    <a class="btn btn-primary" href="<?= Url::to('@web/upload') ?>"
       data-toggle="tooltip"
       data-placement="bottom" title=""
       data-original-title="<?= _t('front.collection', 'Upload 3D Model') ?>">
        <span class="tsi tsi-plus m-r10"></span>
        <?= _t('front.collection', 'Upload 3D Model') ?>
    </a>
<?php $this->endBlock() ?>

<div ng-controller="PsPrinterEmbedController">
    <div class="row">
        <div class="col-sm-12 col-md-8 m-b20">
            <h2 class="m-t0">
                <?= _t('site.ps', 'My Business Tools'); ?>
            </h2>
            <p><?= _t(
                    'site.ps',
                    'Treatstock has developed a new widget that will allow you to sell your 3D models as printed products and helps you stand out from your competitors. In just a few clicks your customers can view your designs in a secure 3D viewer, customize the material and color to calculate print prices and instantly place an order with a print service near them.'
                ); ?>
                <a class="ps-share__info-show" href="#ps-share__info">
                    <?= _t('site.ps', 'Learn more.'); ?>
                </a>
            </p>
        </div>
        <?php /*
        <div class="col-sm-12 col-md-4 m-b20 hidden-xs">
            <div class="ps-share__banner">
                +10%
                <h4><?= _t('site.ps', 'extra for each order'); ?></h4>
            </div>
        </div>
        */?>
    </div>

    <div class="model-embed__widgets-view">
        <ul class="nav nav-tabs nav-tabs--secondary" role="tablist">
            <li role="presentation" class="active"><a href="#widget-store" aria-controls="widget-store" role="tab" data-toggle="tab"><?= _t('front.user', 'Store Widget') ?></a></li>
            <li role="presentation"><a href="#widget-model" aria-controls="widget-model" role="tab" data-toggle="tab"><?= _t('front.user', 'Model Widget') ?></a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="widget-store">
               <?= $this->render('/ps/index_templates/widgets/store-widget', ['user' => $user]);?>
            </div>

            <div role="tabpanel" class="tab-pane" id="widget-model">
                <?= $this->render('/ps/index_templates/widgets/model-widget', ['user' => $user]);?>
            </div>
        </div>
    </div>
</div>

<?= $this->render('/ps/index_templates/widgets/widgets-benefits')?>