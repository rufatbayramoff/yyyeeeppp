<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.18
 * Time: 10:23
 */

/** @var \common\models\Model3d $product */

?>

<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
    <div class="product-card">
        <div class="product-card__pub-status">
            <?= $product->getProductStatusLabel() ?>
        </div>
        <a class="product-card__pic" href="<?= '/mybusiness/products/product/edit?uid=' . $product->cuid ?>"
           title="<?= H($product->getTitle()) ?>" alt="<?= H($product->getTitle()) ?>">
            <?php if ($product->getCoverUrl()) { ?>
                <img src="<?= $product->getCoverUrl() ?>" alt="<?= H($product->getTitle()) ?>">
            <?php } else { ?>
                <span class="product-card__pic-empty">
                    <?= _t('site.catalog', 'Images not uploaded') ?>
                </span>
            <?php } ?>
        </a>
        <a class="product-card__title" href="<?= '/mybusiness/products/product/edit?uid=' . H($product->cuid) ?>"
           title="<?= H($product->getTitle()) ?>">
            <?= H($product->getTitle()) ?>
        </a>
        <?php if ($product->price_per_produce): ?>
            <div class="product-card__price"><?= displayAsMoney($product->getPriceMoneyByQty(1)); ?>
                <span class="product-card__price-unit"></span>
            </div>
        <?php else: ?>
            <div class="product-card__price"><?= _t('site.product', 'Price on request'); ?></div>
        <?php endif; ?>
        <div class="product-card__min-qty"></div>
        <?= $this->render('productCategory', ['productCategory' => $product->getProductCategory()]);?>
        <?= $this->render('productTags', ['productTags' => $product->getProductTags()]); ?>
    </div>
</div>