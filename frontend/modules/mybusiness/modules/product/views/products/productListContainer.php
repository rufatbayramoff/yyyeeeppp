<?php
/** @var $type string */
/** @var $dataProvider \yii\data\ActiveDataProvider */

if (!$dataProvider->count) {
    ?>

        <div class="alert alert-info" role="alert">
            <?=($type==\common\modules\product\interfaces\ProductInterface::TYPE_PRODUCT)?
                _t('site.store', "Product list is empty"):
                _t('site.store', "3d Models list is empty")
            ; ?>
        </div>

    <?php
} else {
    $listView = Yii::createObject([
        'class'        => \yii\widgets\ListView::class,
        'dataProvider' => $dataProvider,
        'itemView'     => 'productListElement',
    ]);
    echo '<div class="row">';
    echo $listView->renderItems();
    echo '</div>';
    echo $listView->renderPager();
}
?>