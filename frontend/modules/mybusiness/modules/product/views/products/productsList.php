<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.18
 * Time: 9:54
 */

/** @var \yii\web\View $this */

/** @var ShopProductSearch $shopProductSearch */

use common\modules\product\interfaces\ProductInterface;
use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;use frontend\modules\mybusiness\modules\product\models\ShopProductSearch;

$user                       = \frontend\models\user\UserFacade::getCurrentUser();
$this->title                = _t('front.user', 'Store');
$this->params['page-class'] = 'js-collection-list';
$this->params['is_store']   = true;

?>
<div class="designer-card p-0">

    <ul class="nav-tabs--secondary nav nav-tabs" role="tablist">
        <li class="<?=($shopProductSearch->type==ProductInterface::TYPE_PRODUCT)?'active':'';?>"><a href="/mybusiness/products?type=<?=ProductInterface::TYPE_PRODUCT?>"><?= _t('site.store', 'Product'); ?></a></li>
        <li class="<?=($shopProductSearch->type==ProductInterface::TYPE_MODEL3D)?'active':'';?>"><a href="/mybusiness/products?type=<?=ProductInterface::TYPE_MODEL3D?>"><?= _t('site.store', '3D model'); ?></a></li>
    </ul>

    <div class="row shop-filter">
        <div class="col-sm-9">
            <label class="shop-filter__label m-r15 m-b15">Show</label>
            <a href="/mybusiness/products?type=<?= H($shopProductSearch->type) ?>&status=<?= ShopProductSearch::STATUS_ALL ?>"
               class="shop-filter__btns btn <?= $shopProductSearch->isAll() ? 'btn-primary' : 'btn-default' ?> btn-sm m-b15 m-r15">
                <?= _t('site.store', 'All'); ?>
            </a>
            <a href="/mybusiness/products?type=<?= H($shopProductSearch->type) ?>&status=<?= ShopProductSearch::STATUS_PUBLISHED ?>"
               class="shop-filter__btns btn <?= $shopProductSearch->isPublished() ? 'btn-primary' : 'btn-default' ?> btn-sm m-b15 m-r15">
                <?= _t('site.store', 'Published'); ?>
            </a>
            <a href="/mybusiness/products?type=<?= H($shopProductSearch->type) ?>&status=<?= ShopProductSearch::STATUS_PENDING ?>"
               class="shop-filter__btns btn <?= $shopProductSearch->isPending() ? 'btn-primary' : 'btn-default' ?> btn-sm m-b15 m-r15">
                <?= _t('site.store', 'Pending moderation'); ?>
            </a>
            <a href="/mybusiness/products?type=<?= H($shopProductSearch->type) ?>&status=<?= ShopProductSearch::STATUS_DRAFT ?>"
               class="shop-filter__btns btn <?= $shopProductSearch->isDraft() ? 'btn-primary' : 'btn-default' ?> btn-sm m-b15">
                <?= _t('site.store', 'Draft'); ?>
            </a>
        </div>
        <div class="col-sm-3 shop-filter__add">
            <a class="btn btn-primary btn-sm m-b10" href="<?=ProductUrlHelper::createByPs($shopProductSearch->type)?>">
                <?= ($shopProductSearch->type == ProductInterface::TYPE_PRODUCT) ? _t('site.store', 'Add Product') : _t('site.store', 'Add Model'); ?>
            </a>
        </div>
    </div>

    <div class="p-t20 p-l15 p-r15 p-b0">

        <div class="store-layout p-t0">
            <div class="store-layout__content">
                <div id="productsList">
                    <?= $this->render('productListContainer', ['dataProvider' => $dataProvider, 'type'=>$shopProductSearch->type]); ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php /*
<div class="model-edit-tips m-t20 m-b20">
    <h4 class="model-edit-tips__title">
        <?= _t('site.model3d', 'Essential Tips for 3D designers') ?>:
    </h4>
    <ol class="model-edit-tips__list">
        <li>
            <?= _t(
                'site.model3d',
                'Always pay attention to the title, description, and tags of your model. They should reflect the main idea of your model. The title should be clear and self-explanatory.'
            ) ?>
        </li>
        <li>
            <?= _t(
                'site.model3d',
                'Choose the color and material that you recommend for the model. If you have a kit (a model that is assembled of several parts), set a material and color for each part. For duplicate parts, upload only one file and set the quantity required for the kit.'
            ) ?>
        </li>
        <li>
            <?= _t('site.model3d', 'Set the lowest possible price. You can always increase it later.') ?>
        </li>
        <li>
            <?= _t('site.model3d', 'Make sure you upload good quality photos of the model as a manufactured product.') ?>
        </li>
        <li>
            <?= _t(
                'site.model3d',
                'To create a buzz for your model, share it on dedicated online platforms such as social medias, communities, and forums.'
            ) ?>
        </li>
        <li>
            <?= _t(
                'site.model3d',
                'For models that require post-processing, write a step-by-step set of instructions. You can also post it on popular sites adding links to your store.'
            ) ?>
        </li>
        <li>
            <?= _t(
                'site.model3d',
                'Use our store widget for your Treatstock models on your own site or blog, so visitors to your sites can order your designs as finished products.'
            ) ?>
        </li>
    </ol>
</div>

<?php */ ?>
