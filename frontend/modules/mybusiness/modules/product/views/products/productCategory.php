<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.18
 * Time: 11:51
 */

/** @var \common\models\ProductCategory $productCategory */

if (!$productCategory) {
    ?>
    <div class="product-title">No category</div>
    <?php
    return ;
}

if ($productCategory->parent && $productCategory->parent->parent) {
    $fullTitle = $productCategory ? sprintf("%s -> %s -> %s", $productCategory->title, $productCategory->parent->title, $productCategory->parent->parent->title) : '';
} else {
    $fullTitle = '';
}

?>
<div class="product-title" title="<?= H(sprintf($fullTitle)); ?>">
    <?php
    /** @var \common\models\ProductCategory[] $categories */
    $categories      = [];
    $currentCategory = $productCategory;
    do {
        $categories[]    = $currentCategory;
        $currentCategory = $currentCategory->parent;
    } while ($currentCategory);
    $categories = array_reverse($categories);

    #echo '> ' . $productCategory->title; echo ' > ' . $productCategory->parent->title;
    foreach ($categories as $category) {
        if (empty($category->parent_id)) {
            continue;
        }
        echo '> ' . H($category->title);
    }
    ?>
</div>
