<?php
/** @var \yii\web\View $this  **/
/** @var \common\models\base\UserCollectionModel3d[] $items  **/
/** @var common\models\base\UserCollection $collection **/
/** @var array $params **/

use common\services\LikeService;
use frontend\models\model3d\Model3dFacade;
use yii\helpers\Url;
use frontend\components\image\ImageHtmlHelper;
use yii\bootstrap\Html;
$items = $params['items'];
$collection  = $params['collection'];

$this->title = \H($collection->title);

$this->registerAssetBundle(\frontend\assets\CollectionAsset::class);

$this->params['page-class'] = 'js-collection-view';
?>
<h2 class="collection__title">
    <?= $this->title; ?>

    <?php
    if(empty($collection->system_type)){
        echo Html::a(
            '<span class="tsi tsi-pencil"></span>' .
            '<span class="catalog-item__controls__item__hint">'._t('front.collection', 'Edit').'</span>', '@web/my/collection/update/' . $collection->id, [
            'class' => 'ts-ajax-modal catalog-item__controls__item',
            'data-pjax' => '0',
            'data-target' => '#modal_updatecollection',
            'title' => _t('front.collection', 'Edit')
        ]);
    }
    ?>
</h2>

<?php 
$this->beginBlock('control-buttons')?>

    <?php /*              <button type="button" class="btn btn-primary ts-ajax-modal"><span class="tsi tsi-download-l"></span><span class="collection-control__hint">Download Models</span></button>
                    <button type="button" class="btn btn-primary ts-ajax-modal"><span class="tsi tsi-briefcase"></span><span class="collection-control__hint">Publish Models</span></button>
                    <button type="button" class="btn btn-primary ts-ajax-modal"><span class="tsi tsi-upload-l"></span><span class="collection-control__hint">Upload Model</span></button>-->
                */?>

    <a href="<?= Url::to(['/catalog/upload/index'])?>" class="btn btn-primary btn-circle"
        data-toggle="tooltip"
        data-placement="bottom" title=""
        data-original-title="<?= _t('front.collections', 'Upload model')?>">
        <span class="tsi tsi-upload-l"></span>
    </a>

    &nbsp;

    <button type="button" class="btn btn-primary btn-circle js-unbind-checked-items"
        data-toggle="tooltip"
        data-placement="bottom" title=""
        data-original-title="<?= _t('front.collections', 'Delete Models')?>">
        <span class="tsi tsi-bin"></span>
    </button>

<?php $this->endBlock()?>

<div class='row collection'>
<?php 
if(empty($items)){
    echo "<div class='col-xs-12'>";
        echo _t("site.collection", "You do not have any models in your collection.");
    echo "</div>";
}
foreach($items as $k=>$item):
        $model3d = $item->model3d;
        $printsCnt = $model3d->storeUnit ? $model3d->storeUnit->printed_count : 0 ;
        $likesCount = LikeService::getModelLikesCount($model3d);
        $itemLink = Model3dFacade::getPrintUrl($model3d);
    ?>
    <div class='col-sm-6 col-md-4'>
        <div class='catalog-item' data-collection-item-id="<?= $item->id ?>">

            <div class='catalog-item__pic'>

                <div class="catalog-item__controls catalog-item__controls--left">
                    <div class="choosen-item">
                        <div class="checkbox checkbox-warning">
                            <input id="choosen-item__id-<?=$k?>" class="js-item-checkbox" type="checkbox">
                            <label for="choosen-item__id-<?=$k?>"></label>
                        </div>
                    </div>
                </div>
                
                <div class="catalog-item__controls">
                    <?php
                    $options = [
                        'title' => _t('yii', 'Delete'),
                        'aria-label' => _t('yii', 'Delete'),
                        'data-confirm' => _t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'class' => 'catalog-item__controls__item',
                        'data-pjax' => '0',
                    ];
                    echo \yii\helpers\Html::a('<span class="tsi tsi-bin"></span> <span class="catalog-item__controls__item__hint">' .
                        _t('app', 'Delete') . '</span>', '@web/my/collection/unbind-item/' . $item->id, $options);

                    echo \yii\helpers\Html::a(
                        '<span class="tsi tsi-share"></span> <span class="catalog-item__controls__item__hint">' .
                        _t('app', 'Move') . '</span>', '@web/my/collection/move-item/' . $item->id, [
                        'class' => 'ts-ajax-modal catalog-item__controls__item',
                        'data-pjax' => '0',
                        'data-target' => '#modal_updatecollection',
                        'title' => 'Move to another collection'
                    ]);
                    if(frontend\models\user\UserFacade::isObjectOwner($model3d)){
                        echo \yii\helpers\Html::a(
                            '<span class="tsi tsi-edit"></span> <span class="catalog-item__controls__item__hint">' .
                            _t('app', 'Edit') . '</span>',['/my/model3d/edit', 'id'=>$model3d->id], [
                            'class' => 'catalog-item__controls__item',
                            'title' => 'Edit'
                        ]);
                    }
                    
                  
                    ?>
                    <a href="<?= $itemLink; ?>" class="catalog-item__controls__item">
                    <span class="tsi tsi-printer3d"></span>
                    <span class="catalog-item__controls__item__hint"><?php 
                    $msg = _t('front.catalog', '{n, plural, =0{Print It} =1{Printed 1 time} other{Printed # times}}', ['n' => $printsCnt]);
                    echo $msg; ?></span>
                    </a> 
                    <?php
                   
                    ?>
                   
                </div>
                <?php 
                $itemTitle = \yii\helpers\StringHelper::truncate(str_replace("_", " ", $model3d->title), 55);
                $itemTitle = \H($itemTitle);
                $coverImage = Model3dFacade::getCover($model3d);
                if($coverImage){
                    $coverImage['image'] = ImageHtmlHelper::getThumbUrl($coverImage['image'], ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
                    $img =  yii\helpers\Html::img($coverImage['image'], ['alt'=>$itemTitle]);
                    echo yii\helpers\Html::a($img, $itemLink, ['title'=> $itemTitle, 'class' => 'catalog-item__pic__img']);
                }
               
                ?>
            </div>

            <div class="catalog-item__footer">
                <h4 class="catalog-item__footer__title">
                    <?php echo yii\helpers\Html::a($itemTitle,  Model3dFacade::getStoreUrl($model3d)); ?>
                </h4>

                <div class="catalog-item__footer__price">
                     <?=displayAsMoney($model3d->getPriceMoneyByQty(1);?>
                </div>

                <div class="catalog-item__footer__stats">

                    <?php /*    <div class="catalog-item__footer__stats__reviews">
                        <span class="tsi tsi-comment-o"></span> 0 reviews
                    </div> */?>

                    <div class="catalog-item__footer__stats__likes js-hide-like-if-not-liked" data-likes="<?=$likesCount?>">
                        <span class="tsi tsi-heart"></span> <span class="js-store-item-likes-count"><?= LikeService::getLikesCountString($likesCount) ?></span>
                    </div>
                </div>

            </div>
        </div>
    </div>    
<?php endforeach; ?>

</div>