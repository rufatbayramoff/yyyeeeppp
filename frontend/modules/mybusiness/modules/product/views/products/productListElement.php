<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.04.18
 * Time: 16:42
 */

use common\models\ProductCommon;

/** @var ProductCommon $model */
$type = $model->getType();
$extObject = $model->getExtObject();
if ($extObject) {
    echo $this->render('productListElement' . ucfirst($type), ['product' => $extObject]);
}
?>