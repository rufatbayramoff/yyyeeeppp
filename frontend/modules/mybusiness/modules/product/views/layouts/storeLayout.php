<?php
/** @var string $content */
use frontend\models\user\UserFacade;

/** @var \yii\web\View $this */

$this->beginContent('@frontend/modules/mybusiness/views/layouts/mybusinessLayout.php');
$storeActive = false;
if (!isset($this->params['page-class'])) {
    $this->params['page-class'] = 'js-collection-view';
}

$user = UserFacade::getCurrentUser();


?>

<div class="<?= $this->params['page-class'] ?>">
    <div class="container">
        <?= isset($this->blocks['control-buttons']) ? $this->blocks['control-buttons'] : '' ?>
    </div>

    <div class="container">

        <?= $content; ?>

    </div>

</div>

<?php $this->endContent(); ?>




