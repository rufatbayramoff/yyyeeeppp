<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 04.04.2018
 * Time: 18:14
 */

use common\models\ProductCategory;
use common\modules\product\serializers\ProductCategorySerializer;
use common\modules\product\serializers\ProductSerializer;
use frontend\assets\AngularSortableAsset;
use frontend\assets\AngularSortableAssetLib;
use frontend\assets\DropzoneAsset;
use frontend\components\angular\AngularService;
use frontend\modules\mybusiness\widgets\ProductFillPercentWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->beginContent('@frontend/modules/mybusiness/views/layouts/mybusinessLayout.php');

DropzoneAsset::register($this);
AngularSortableAssetLib::register($this);
Yii::$app->angular->additionalModules(['angular-sortable-view']);

/**
 * @var $product \common\models\Product
 */
$product = $this->context->product;
$feeHelper = \common\modules\payment\fee\FeeHelper::init();

$files = $product->imageFiles;
$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);

Yii::$app->angular
    ->service(['router', 'user', 'notify', 'googleLocationApi'])
    ->directive(['dropzone-button', 'fileread'])
    ->controller(
        [
            'product/productForm',
            'product/commonLib',
            'product/userVideo',
            'product/productModels'
        ])
    ->directive(['dropzone-button', 'image-rotate'])
    ->constant('locationInputElementId', 'productform-ship_from-location')
    ->constant('locationResultInputElId', 'productform-ship_from')
    ->controllerParam('product', ProductSerializer::serialize($product))
    ->controllerParam('feePercent', $feeHelper->getManufacturingFeePercent());

?>

<div class="container container--wide">

    <div class="row m-b10">
        <div class="col-sm-9 col-md-7 col-lg-8">
            <h3 class="product-add-title ugc-content">
                <?php if ($product->isNewRecord) { ?>
                    <?= _t('mybusiness.product', 'Add Product'); ?>
                <?php } else { ?>
                    <?= _t('mybusiness.product', 'Edit'); ?>:  <?= H($product->title); ?>
                <?php } ?>
            </h3>

            <?php if ($product->isRejected()): ?>
                <div class="bar bg-warning m-b10">
                    <b><?= _t('mybusiness.services', 'Rejected'); ?>. </b>
                    <?= \common\models\ModerLog::getLastRejectModerLog(\common\models\ModerLog::TYPE_PRODUCT, $product->uuid); ?>
                </div>
            <?php endif; ?>

        </div>
        <div class="col-sm-3 col-md-2">

                <div class="btn-group dropdown dropdown-default dropdown-onhover product-add-action">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="tsi tsi-list-1"></span>  &nbsp;
                        <?= _t('site.store', 'Actions'); ?>
                        <span class="tsi tsi-down"></span>
                    </button>
                    <ul class="dropdown-menu">

                        <?php
                        if (!$product->isNewRecord) {
                            ?>

                            <li>
                                <a href='<?= $product->getPublicPageUrl() ?>' target="_blank"><?= _t('site.store', 'Preview product listing') ?></a>
                            </li>
                        <li>
                                <a href='<?= Url::toRoute(['/mybusiness/products/product/delete', 'uuid' => $product->uuid]) ?>'
                                   data-confirm="<?= _t('site', 'Are you sure?') ?>"
                                   data-method="post"><?= _t('site.store', 'Delete product') ?></a>
                        </li>
                            <?php
                        }
                        ?> <li>
                            <a href="<?=\yii\helpers\Url::toRoute(['product/create']);?>">
                                <?= _t('site.store', 'Add new Product'); ?>
                            </a>
                        </li>
                    </ul>
                </div>


        </div>
    </div>

    <div class="row">
        <div class="col-sm-9 col-lg-10">
            <div class="designer-card p-l0 p-r0 p-t0 p-b0">

                <div id="productFormControllerEl" ng-controller="ProductFormController">
                    <div  class="m-b20">
                        <?= \frontend\modules\mybusiness\widgets\ProductFormTabs::widget(['product' => $product, 'section' => $this->params['section'] ?? null]); ?>
                    </div>
                    <?= $content; ?>
                </div>

            </div>
        </div>
        <div class="col-sm-3 col-lg-2">
            <?=ProductFillPercentWidget::widget(['product'=>$product])?>
        </div>
    </div>



</div>

<?php $this->endContent(); ?>
