<?php

/* @var $this \yii\web\View */
/* @var $model \frontend\modules\mybusiness\models\ProductCertificationForm */
/* @var $product \common\modules\product\interfaces\ProductInterface */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$company = $product->company;
?>

<div class="container">
    <?php

    $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data'] ]); ?>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 col-md-2">
                <?= Html::a('<span class="tsi tsi-left m-r10"></span>' . _t('mybusiness.certifications', 'Back'), ['index', 'uuid'=>$model->product_uuid], ['class' => 'btn btn-sm btn-default']) ?>
            </label>
            <div class="col-sm-6">
                <h3 class="m-t0 m-b0">
                    <?= _t('mybusiness.certifications', 'Add Certification') ?>
                </h3>
            </div>
        </div>
        <div class="form-group field-productcertificationform-title required">
            <label class="control-label col-sm-3 col-md-2" for="productcertificationform-title"><?= _t('mybusiness.certifications', 'Title') ?></label>
            <div class="col-sm-6">
                <input type="text" id="productcertificationform-title" class="form-control" name="ProductCertificationForm[title]" maxlength="45" aria-required="true">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-productcertificationform-file">
            <label class="control-label col-sm-3 col-md-2" for="productcertificationform-file"><?= _t('mybusiness.certifications', 'Image/PDF') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="hidden" name="ProductCertificationForm[file]" value="">
                <input type="file" id="productcertificationform-file" class="inputfile" name="ProductCertificationForm[file]" value="" data-multiple-caption="{count} files selected" accept=".pdf, .jpg, .gif, .png">
                <label class="uploadlabel" for="productcertificationform-file">
                    <span>
                        <i class="tsi tsi-upload-l"></i>
                        <?= _t('mybusiness.certifications', 'Upload File') ?>
                    </span>
                </label>
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-productcertificationform-certifier">
            <label class="control-label col-sm-3 col-md-2" for="productcertificationform-certifier"><?= _t('mybusiness.certifications', 'Certifier') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="text" id="productcertificationform-certifier" class="form-control" name="ProductCertificationForm[certifier]" maxlength="45">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-productcertificationform-application">
            <label class="control-label col-sm-3 col-md-2" for="productcertificationform-application"><?= _t('mybusiness.certifications', 'Application') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="text" id="productcertificationform-application" class="form-control" name="ProductCertificationForm[application]" maxlength="45">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-productcertificationform-issue_date required">
            <label class="control-label col-sm-3 col-md-2" for="productcertificationform-issue_date"><?= _t('mybusiness.certifications', 'Issue date') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="date" id="productcertificationform-issue_date" class="form-control" name="ProductCertificationForm[issue_date]" aria-required="true">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group field-productcertificationform-expire_date">
            <label class="control-label col-sm-3 col-md-2" for="productcertificationform-expire_date"><?= _t('mybusiness.certifications', 'Expiry date') ?></label>
            <div class="col-sm-6 col-md-5">
                <input type="date" id="productcertificationform-expire_date" class="form-control" name="ProductCertificationForm[expire_date]">
                <p class="help-block help-block-error "></p>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"></label>
            <div class="col-sm-6 col-md-5">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success js-clickProtect' : 'js-clickProtect btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
