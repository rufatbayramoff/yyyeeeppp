<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" ng-controller="ProductCategorySelectController">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span>
                </button>
                <h4 class="modal-title"><?= _t('site.product', 'Select category') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="m-t10">
                            <?= _t('mybusiness.product', 'Please assign the appropriate categories for your product.'); ?>
                        <div ng-if="validateText" class="error-summary">{{validateText}}</div>
                        {{labelText}}
                        </p>
                        <div class="row">
                            <?= $this->render('../common/productCategory', ['category' => $category]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button ng-click="cancel()" type="button"
                        class="btn btn-default"><?= _t('site.ps', 'Cancel') ?></button>
                <button loader-click="submit()" type="button"
                        class="btn btn-primary"><?= _t('site.ps', 'Submit') ?></button>
            </div>
        </div>
    </div>
</div>
