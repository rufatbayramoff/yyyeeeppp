<?php

use common\models\ProductCategory;
use common\modules\product\serializers\ProductCategorySerializer;

/** @var $category ProductCategory */

$parentCategories = ProductCategory::getChilds();
Yii::$app->angular->controllerParam('productRootCategories', ProductCategorySerializer::serialize($parentCategories));

$productMainCategoryId = $productSubCategoryId = $productSubSubCategoryId = null;

if ($category) {
    $productMainCategoryId = $category->id;
    if ($category->parent && $category->parent->parent && $category->parent->parent->parent_id) { // 3rd selected
        Yii::$app->angular->controllerParam('productSubCategories', ProductCategory::getChilds($category->parent->parent));
        Yii::$app->angular->controllerParam('productSubSubCategories', ProductCategory::getChilds($category->parent));
        $productMainCategoryId   = $category->parent->parent_id;
        $productSubCategoryId    = $category->parent_id;
        $productSubSubCategoryId = $category->id;
    } else {
        if ($category->parent->parent_id) { // 2nd selected
            Yii::$app->angular->controllerParam('productSubCategories', ProductCategory::getChilds($category->parent));
            Yii::$app->angular->controllerParam('productSubSubCategories', ProductCategory::getChilds($category));
            $productMainCategoryId = $category->parent_id;
            $productSubCategoryId  = $category->id;
        } else { // 1st selected
            Yii::$app->angular->controllerParam('productSubCategories', ProductCategory::getChilds($category));
        }
    }
    Yii::$app->angular->controllerParam('categorySelect.currentCategoryId', $category->id);
    Yii::$app->angular->controllerParam('categorySelect.currentCategoryTitle', $category->getFullTitle());
}

Yii::$app->angular->controllerParam('categorySelect.mainCategoryId', $productMainCategoryId);
Yii::$app->angular->controllerParam('categorySelect.subCategoryId', $productSubCategoryId);
Yii::$app->angular->controllerParam('categorySelect.subSubCategoryId', $productSubSubCategoryId);

?>
<div ng-controller="ProductCategorySelectController">
    <div class="row product-category" ng-if="categorySelect.shortList|numkeys">
        <div ng-repeat="category in categorySelect.shortList" class="col-md-12 product-category__item">
            <button class="btn-link product-category__btn" ng-class="{'product-category-selected':category.id==categorySelect.currentCategoryId}" ng-click="selectCategory(category)">{{category.title}}</button>
        </div>
    </div>
    <div class="row product-category" ng-if="categorySelect.isShortList">
        <div class="col-md-12 product-category__item">
            <button class="btn-link product-category__btn product-category__btn--link" ng-click="showAllCategories()"><?= _t('site.product', 'Show all categories') ?></button>
        </div>
    </div>

    <div class="row m-t10" ng-if="!categorySelect.isShortList">
        <div class="col-md-4 m-b10" style="transition: width 0.5s ease; will-change: width;"
             ng-mouseover="updateColumnWidth($event, true)" ng-mouseout="updateColumnWidth($event ,false)">
            <select size="7" class="form-control" ng-model='categorySelect.mainCategoryId'
                    ng-change="updateCategory()">
                <option style="display:none" value="">select</option>
                <option ng-repeat="category in categorySelect.productRootCategories" value="{{category.id}}">
                    {{category.title}}
                </option>
            </select>
        </div>
        <div class="col-md-4 m-b10" style="transition: width 0.5s ease; will-change: width;"
             ng-mouseover="updateColumnWidth($event,true)" ng-mouseout="updateColumnWidth($event ,false)">
            <select size="7" class="form-control" ng-model="categorySelect.subCategoryId"
                    ng-change="updateSubcategory()" ng-show="categorySelect.productSubCategories">
                <option style="display:none" value="">select</option>
                <option ng-repeat="category in categorySelect.productSubCategories" value="{{category.id}}">
                    {{category.title}}
                </option>
            </select>
        </div>
        <div class="col-md-4 m-b10" style="transition: width 0.5s ease; will-change: width;"
             ng-mouseover="updateColumnWidth($event ,true)" ng-mouseout="updateColumnWidth($event, false)">
            <select size="7" class="form-control" ng-model="categorySelect.subSubCategoryId"
                    ng-change="update()" ng-show="categorySelect.productSubSubCategories">
                <option style="display:none" value="">select</option>
                <option ng-repeat="category in categorySelect.productSubSubCategories" value="{{category.id}}">
                    {{category.title}}
                </option>
            </select>
        </div>
    </div>
</div>