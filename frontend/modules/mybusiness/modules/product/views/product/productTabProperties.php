<?php
/**
 * User: nabi
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $product \common\modules\product\interfaces\ProductInterface */
/* @var $this \yii\web\View */
?>
<div class="product-edit">
    <?php
    $form = ActiveForm::begin(
        [
            'id'      => 'ProductForm',
            'options' => ['enctype' => 'multipart/form-data', 'onsubmit' => '',]
        ]
    );
    ?>
    <input type="hidden" name="ProductForm[custom_properties]" id="productform-custom_properties"/>
    <div class="row">
        <div class="col-lg-10">
            <div class="table-responsive">
                <table class="table product-edit__custom-prop-table" ng-cloak="true">
                    <tr>
                        <?php /* <th></th> */?>
                        <th><?= _t('mybusiness.product', 'Title'); ?></th>
                        <th><?= _t('mybusiness.product', 'Value'); ?></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr ng-repeat="userField in product.customProperties.customProperties | orderBy: 'position'">
                        <?php /* <td>{{userField.position}}</td> */?>
                        <td><input type="text" class="form-control " ng-model="userField.title"></td>
                        <td><input type="text" class="form-control " ng-model="userField.value"></td>
                        <td>
                            <button ng-hide="$first" type="button" ng-click="product.customProperties.productUserFieldMove(userField, 'up')" class="btn btn-info btn-xs p-l10 p-r10">
                                <span class="tsi tsi-up"></span>
                            </button>

                            <button ng-hide="$last" type="button" ng-click="product.customProperties.productUserFieldMove(userField, 'down')" class="btn btn-info btn-xs p-l10 p-r10">
                                <span class="tsi tsi-down"></span>
                            </button>
                        </td>
                        <td>
                            <button type="button" ng-click="product.customProperties.deleteProductUserField(userField)"
                                    class="btn btn-info p-l10 p-r10" title="<?= _t('front.common', 'Delete'); ?>">
                                <span class="tsi tsi-bin"></span>
                            </button></td>
                    </tr>
                </table>
            </div>

            <button type="button" ng-click="product.customProperties.addProductUserField()"
                    class="btn btn-info btn-sm p-l10 p-r10" title="<?= _t('front.common', 'Add'); ?>">
                <span class="tsi tsi-plus m-r10"></span> <?= _t('mybusiness.product', 'Add specification'); ?>
            </button>
            <br />

            <textarea class="form-control m-t20" ng-model="propsDump">

            </textarea>
            <button type="button" class="btn btn-sm btn-default m-t10" ng-click="importProperties()"><?=_t('mybusiness.product', 'Import specifications'); ?></button>
            <p class="help-block"><?=_t('mybusiness.product', 'Copy product specifications (title and value only) from product data sheet, paste in the text field above and click Import specifications.'); ?></p>
        </div>
    </div>

    <div class="row m-b10">
        <hr>
        <div class="col-lg-8">
        </div>
        <div class="col-lg-4">
            <div class="row m-b10">
                <?php
                echo '<div class="col-sm-6">';
                if (!$product->isPublished()) {
                    $txtUpload = _t('site.store.', 'Save unpublished');
                    echo Html::buttonInput(
                        $txtUpload,
                        [
                            'class'    => 'btn btn-ghost btn-info btn-block t-model-edit-unpublish p-l10 p-r10',
                            'ng-click' => 'submitForm("unpublish")',
                            'style'    => 'font-size:16px;',
                        ]
                    );
                }
                echo '</div><div class="col-sm-6">';

                echo Html::buttonInput(
                    $product->isPublished() ? _t('front.user', 'Save') : _t('front.user', 'Publish'),
                    [
                        'class'    => 'btn btn-primary btn-block t-model-edit-publish m-b10',
                        'ng-click' => 'submitForm("publish")',
                    ]
                );


                echo '</div>';
                ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>