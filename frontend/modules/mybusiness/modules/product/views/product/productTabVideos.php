<?php
/**
 * User: nabi
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $product \common\modules\product\interfaces\ProductInterface */
/* @var $this \yii\web\View */

Yii::$app->angular
    ->directive(['youtube-embed']);
?>
<div class="product-edit">
    <?php
    $form = ActiveForm::begin(
        [
            'id'      => 'ProductForm',
            'options' => ['enctype' => 'multipart/form-data', 'onsubmit' => '',]
        ]
    );
    ?>

    <input type="hidden" name="ProductForm[videos]" id="productform-videos"/>

    <div class="row m-b30">
        <div class="col-sm-5 col-md-4">
            <input class="form-control m-b10" type="text" size="50" ng-model="newVideoUrl" placeholder="YouTube link"/>
        </div>
        <div class="col-sm-6 col-md-4">
            <button type="button" ng-click="product.addVideo(newVideoUrl);newVideoUrl=''"
                    class="btn btn-primary btn-ghost">
                <?= _t('mybusiness.product', 'Add video'); ?>
            </button>
            <p class="help-block">
                <?= _t('site.products', "Don't forget to click the Save button after adding or removing videos."); ?>
            </p>
        </div>
    </div>

    <div class="product-edit__video-list">
        <div ng-repeat="video in product.videos" class="product-edit__video-list-item">
            <a ng-href="#" ng-click="product.showVideoPreview(video, 'js-previewArea')">
                <img ng-src="{{video.thumb}}"/>
            </a>
            <button class="product-edit__video-list-del" title="<?= _t('mybusiness.product', 'Delete video'); ?>" ng-click="product.removeVideo($index)">&times;</button>
        </div>
    </div>
    <div id="js-previewArea" onclick="document.getElementById('js-previewArea').innerHTML=''"></div>

    <div class="row">
        <hr>
        <div class="col-lg-8">
        </div>
        <div class="col-lg-4">
            <div class="row m-b10">
                <?php
                echo '<div class="col-sm-6">';
                if (!$product->isPublished()) {
                    $txtUpload = _t('site.store.', 'Save unpublished');
                    echo Html::submitInput(
                        $txtUpload,
                        [
                            'class'    => 'btn btn-ghost btn-info btn-block t-model-edit-unpublish p-l10 p-r10 js-clickProtect',
                            'ng-click' => 'submitForm("unpublish")',
                            'style'    => 'font-size:16px;',
                        ]
                    );
                }
                echo '</div><div class="col-sm-6">';

                echo Html::buttonInput(
                    $product->isPublished() ? _t('front.user', 'Save') : _t('front.user', 'Publish'),
                    [
                        'class'    => 'btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect',
                        'ng-click' => 'submitForm("publish")',
                    ]
                );


                echo '</div>';
                ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>