<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.18
 * Time: 9:54
 */

/** @var \yii\web\View $this * */
/* @var $product \common\modules\product\interfaces\ProductInterface */


use frontend\components\UserUtils;
use yii\helpers\Html;
use frontend\models\model3d\Model3dFacade;
use yii\helpers\Url;
use frontend\components\image\ImageHtmlHelper;

$user = \frontend\models\user\UserFacade::getCurrentUser();
$this->title = _t('front.user', 'Products');
$this->params['page-class'] = 'js-collection-list';
$this->params['is_store'] = true;

?>

<div class="product-edit">
    <?= $product->getTitle() ?>
</div>

