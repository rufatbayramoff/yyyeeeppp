<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.18
 * Time: 15:21
 */


/** @var \yii\web\View $this * */

/** @var \common\modules\product\interfaces\ProductInterface $product * */

use common\components\FileTypesHelper;
use common\models\Product;
use dosamigos\ckeditor\CKEditor;
use frontend\models\user\UserFacade;
use frontend\widgets\SiteHelpWidget;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\jui\JuiAsset;

$user                       = UserFacade::getCurrentUser();
$this->title                = _t('front.user', 'Products');
$this->params['page-class'] = 'js-collection-list';
$this->params['is_store']   = true;

Yii::$app->angular
    ->controller('product/productCategorySelect');

$form = ActiveForm::begin(
    [
        'id'                     => 'ProductForm',
        'options'                => ['enctype' => 'multipart/form-data', 'onsubmit' => 'return false;'],
        'enableClientValidation' => false,
    ]
);
$this->registerAssetBundle(JuiAsset::class);
?>
<div class="product-edit" ng-cloak>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productform-title"><?= _t('site.store', 'Title') ?> <span
                        class="form-required">*</span></label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($product, 'title')->textInput(['ng-model' => 'product.title', 'maxlength' => true])->label(false) ?>
                <div ng-if="isShortTitle()" class="help-block help-block-error m-b0"><?= _t('site.store', 'The title is too short') ?></div>
                <button class="hidden"></button> <?php /* This is stub for event click bug to categories all */ ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"><?= _t('site.store', 'Category') ?> <span class="form-required">*</span></label>
            <div class="col-sm-9 col-md-10">
                                        <span class="hidden">
                        <input type="text" id="product_category_id" ng-model="categorySelect.currentCategoryId"
                               value="<?= $product->productCommon->category_id; ?>" name="ProductForm[categoryId]"/>
                         </span>

                <div class="form-control-static" ng-if="categorySelect.currentCategoryTitle">
                    {{categorySelect.currentCategoryTitle}}
                </div>

                <div class="form-control-static" ng-if="isShortTitle()">
                    <?= _t('site.store', 'Please enter title at first') ?>
                </div>
                <div ng-show="!isShortTitle()">
                    <?= $this->render('../common/productCategory', ['category' => $product->category]) ?>
                </div>
            </div>
        </div>

        <?php
        if (param('allowDynamicFields')) {
            $valsDynamic = $product->getDynamicValues();
            ?>


            <div id="productDynamicValues" class="product-edit__dyna-field">

                <?= $this->render('productDynamicValues', [
                    'dynamicValues' => $valsDynamic,
                    'formName'      => $product->formName()
                ]); ?>


            </div>
            <?php
        }
        ?>
        <div class="form-group jsfocus-tags">
            <label class="control-label col-sm-3 col-md-2"
                   for="productform-unit_type"><?= _t('mybusiness.product', 'Tags') ?></label>

            <div class="col-sm-6 col-md-5">
                <?php
                $productTagsTexts = \yii\helpers\ArrayHelper::map($product->getProductTags(), 'id', 'text');
                echo $form->field($product, 'productTags')->widget(
                    Select2::class,
                    common\components\SimpleSelect2::getAjax(
                        ['product/tags'],
                        'id,text',
                        $productTagsTexts,
                        [
                            'options'       => ['placeholder' => _t('front.model3d', 'Add tags') . '... ', 'multiple' => true],
                            'pluginOptions' => [
                                'tags'               => true,
                                //'allowClear' => true,
                                'minimumInputLength' => 2,
                                'maximumInputLength' => 45
                            ]
                        ]
                    )
                )->label(false); ?>
            </div>
        </div>


        <div class="form-group jsfocus-photos">
            <label class="control-label col-sm-3 col-md-2" for="productform-unit_type"><?= _t('site.store', 'Photos') ?>
                <span class="form-required">*</span></label>
            <div class="col-sm-9 col-md-10">
                <div class="sortable-container" sv-root sv-part="product.images">
                    <div ng-repeat="image in product.images" image-rotate="image" sv-element="opts"
                         class="sortable-grid-element">
                        <div sv-handle class="sortable-grid-element__overlay"></div>
                        <img class="sortable-grid-element-img" ng-src="{{image.src}}" data-image-rotate-flag="img"
                             alt="">
                        <button type="button" ng-click="removeImage(image)" class="sortable-grid-del-btn"
                                title="<?= _t('company.service', 'Delete'); ?>">
                            &times;
                        </button>
                        <button type="button" data-image-rotate-flag="button"
                                class="sortable-grid-rotate-btn"
                                title="<?= _t('company.service', 'Rotate'); ?>">
                            <span class="tsi tsi-repeat"></span>
                        </button>
                    </div>
                </div>
                <button
                        dropzone-button="product.images"
                        can-change-after-select="true"
                        accepted-files=".<?= implode(',.', FileTypesHelper::ALLOW_IMAGES_EXTENSIONS); ?>"
                        max-files="20"
                        class="btn btn-primary btn-sm m-b10"
                        title="<?= _t('site.ps', 'Upload'); ?>"
                        type="button">
                    <span class="tsi tsi-plus m-r10"></span>
                    <?= _t('site.ps', 'Upload'); ?>
                </button>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"
                   for="productform-unit_type"><?= _t('site.store', 'Files') ?></label>
            <div class="col-sm-9 col-md-10">
                <a ng-href="#" dropzone-button="attachments"
                   accepted-files="<?= '.' . implode(', .', FileTypesHelper::getAllowExtensions()) ?>" max-files="50"
                   class="btn btn-default"><span class="tsi tsi-paperclip"></span></a>
                <div ng-if="attachments.length > 0" class="direct-chat__attach-result">
                    <h4><?= _t('frontend.message', 'Attached files:') ?></h4>
                    <div class="direct-chat__attach-result-item" ng-repeat="file in attachments">{{file.name}} <a
                                ng-href="#" class="btn btn-xs btn-link p-l0 p-r0 m-l10 text-muted"
                                type="button" ng-click="deleteAttachedFile(file)"> <span class="tsi tsi-remove"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"
                   for="productform-unit_type"><?= _t('mybusiness.product', 'Unit type') ?></label>
            <div class="col-sm-9 col-md-8">
                <div class="row">
                    <div class="col-sm-4 m-b10">
                        <?= $form->field($product, 'unit_type')->dropDownList(Product::getUnitTypesComboList(), ['ng-model' => 'product.unitType'])->label(false); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2"
                   for="productform-unit_type"><?= _t('mybusiness.product', 'Pricing') ?></label>

            <div class="col-sm-9 col-md-8">
                <div class="row jsfocus-prices">
                    <div class="col-sm-11 col-md-8">
                        <div class="table-responsive">
                            <table class="table table-bordered" ng-cloak="true">
                                <tr>
                                    <th class="p-l0"><?= _t('mybusiness.product', 'Min. order quantity'); ?></th>
                                    <th>
                                        <?= _t('mybusiness.product', 'Price per {unit} ({currency})', ['unit' => '<span>{{product.unitType}}</span><span class="form-required">*</span>', 'currency' => $product->productCommon->company->paymentCurrency->title_original]); ?>
                                        <?= SiteHelpWidget::widget(['alias' => 'help.mybusiness.product.price']); ?>
                                    </th>
                                    <!--<th><? /*= _t('mybusiness.product', "Your income ($)"); */ ?></th>-->
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr ng-repeat="productPrice in productPrices">
                                    <td class="p-l0"><input type="number" class="form-control"
                                                            ng-model="productPrice.moq"
                                                            ng-blur="sortPrices($event, productPrice)" step="any"
                                                            min="0"></td>
                                    <td><input type="number" step="0.01" min="0.1" class="form-control"
                                               ng-model="productPrice.priceWithFee"
                                               ng-blur="calculatePrice(productPrice)"></td>
                                    <td>
                                        <div style="display:none">
                                            <input type="number" step="0.01" min="0.1" class="form-control"
                                                   ng-model="productPrice.price"
                                                   ng-change="calculatePriceWithFee(productPrice)"></div>
                                    </td>
                                    <td class="p-r0">
                                        <button type="button" ng-click="deleteProductPrice(productPrice)"
                                                class="btn btn-info p-l10 p-r10"
                                                title="<?= _t('front.common', 'Delete'); ?>">
                                            <span class="tsi tsi-bin"></span>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <button type="button" ng-click="addProductPrice()"
                                class="btn btn-info btn-sm p-l10 p-r10" title="<?= _t('front.common', 'Add'); ?>">
                            <span class="tsi tsi-plus m-r10"></span> <?= _t('mybusiness.product', 'Add price'); ?>
                        </button>

                        <div class="row m-t20" style="display:none;">
                            <div class="col-sm-9">
                                <div class="checkbox checkbox-primary m-t10 m-b10 p-t0">
                                    <input type="checkbox" id="checkbox_quote" ng-model="isSwitchQuote"/>
                                    <label for="checkbox_quote">
                                        <?= _t('mybusiness.product', 'Switch to Request for quote if quantity is more than'); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-4">
                                <?= $form->field($product, 'switch_quote_qty')
                                    ->input('number', ['maxlength' => true, 'disabled' => true])
                                    ->label(false); ?>
                            </div>
                        </div>
                        <input type="hidden" name="ProductForm[moq_prices]" id="productform-moq_prices"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group jsfocus-supplyAbility">
            <label class="control-label col-sm-3 col-md-2"
                   for="productform-supply_ability"><?= _t('site.store', 'Supply ability') ?></label>
            <div class="col-sm-6 col-md-4">
                <div class="row">
                    <div class="col-xs-7 m-r0 p-r0">
                        <?= $form->field($product, 'supply_ability')->input('number', ['maxlength' => true])->label(false); ?>
                    </div>

                    <div class="col-xs-5">
                        <?= $form->field($product, 'supply_ability_time')->dropDownList(Product::getSupplyAbilityTimeList())->label(false); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group jsfocus-avgProduction">
            <label class="control-label col-sm-3 col-md-2"
                   for="productform-avg_production"><?= _t('site.store', 'Avg. production time') ?></label>
            <div class="col-sm-6 col-md-4">
                <div class="row">
                    <div class="col-xs-7 m-r0 p-r0">
                        <?= $form->field($product, 'avg_production')->input('number', ['maxlength' => true])->label(false); ?>
                    </div>
                    <div class="col-xs-5">
                        <?= $form->field($product, 'avg_production_time')->dropDownList(Product::getAvgProductionTimeList())->label(false); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group jsfocus-description">
            <label class="control-label col-sm-3 col-md-2"
                   for="productform-description"><?= _t('site.store', 'Description') ?> <span
                        class="form-required">*</span></label>
            <div class="col-sm-9 col-md-10">
                <?= $form->field($product, 'description')->widget(CKEditor::className(), \frontend\components\CKEditorDefault::getDefaults())->label(false); ?>
            </div>
        </div>


    </div>

    <?php /*
    <div style="border: 1px solid #dfdfdf; padding: 15px;">
        <div class="row">

            <div class="col-lg-6">
                <div class="form-group">
                    <?= $form->field($product->productDelivery, 'express_delivery_first_item')
                        ->textInput(['maxlength' => true])->label(_t('mybusiness.product', 'One item')); ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <?= $form->field($product->productDelivery, 'express_delivery_following_item')->textInput(['maxlength' => true])
                        ->label(_t('mybusiness.product', 'Each additional item')); ?>
                </div>
            </div>
        </div>
    </div>
    */ ?>


    <div class="row">
        <hr class="m-t0 m-b0">
    </div>

    <div class="row">
        <div class="col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-2">
            <p class="m-t20">
                <strong><?= _t('site.store', 'Status') . ': ' ?></strong>
                <?= $product->getProductStatusLabel() ?>
            </p>

            <div class="row m-b10">
                <div class="col-sm-6 col-md-4">
                    <input type="button" class="btn btn-primary btn-block t-model-edit-publish m-b10 js-clickProtect"
                           value="{{saveButton}}" ng-click="submitForm('publish')">
                </div>
                <div class="col-sm-6 col-md-4">
                    <input type="button" class="btn btn-info btn-block m-b10 js-clickProtect"
                           value="{{unpublishButtonText}}" ng-click="submitForm('unpublish')">
                </div>
            </div>
        </div>
    </div>

</div>

<?php ActiveForm::end(); ?>

<?php
if ($focusEl = app('request')->get('focus', false)):
    $this->registerJs("
        var focusEl = '" . $focusEl . "';
       
        $('html, body').animate({
            scrollTop: $('.jsfocus-' +focusEl).offset().top - 150
        }, 1000);
    ");
endif;

?>
