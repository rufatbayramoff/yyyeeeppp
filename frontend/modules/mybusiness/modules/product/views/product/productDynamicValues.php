<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.06.18
 * Time: 14:28
 */

use common\modules\dynamicField\services\DynamicFieldService;

/** @var $dynamicValues \common\modules\dynamicField\models\DynamicFieldValue[] */
/** @var $formName string */

$dynamicFieldService = \Yii::createObject(DynamicFieldService::class);
if ($dynamicValues) {
    ?>
    <div class="form-group m-b0">


        <label class="control-label col-sm-3 col-md-2 m-b10"><?= _t('site.store', 'Attributes') ?></label>
        <div class="col-sm-6 col-md-5">

            <?php

            foreach ($dynamicValues as $dynamicValue) {
                $inputId = 'ProductForm_' . $dynamicValue->dynamicField->code;
                ?>
                <div class="form-group field-productform-title product-edit__dyna-field-row required <?= $dynamicValue->getValueErrors() ? 'has-error' : '' ?>">
                    <label class="control-label" for="<?= $inputId ?>"><?= $dynamicValue->dynamicField->title ?></label>
                    <?php
                    if ($dynamicValue->dynamicField->is_required) {
                        ?>
                        <span class="form-required">*</span>
                        <?php
                    }
                    ?>
                    <?= $dynamicFieldService->getElementForm($dynamicValue)->getInput($formName) ?>
                    <?php
                    if ($dynamicValue->getValueErrors()) {
                        foreach ($dynamicValue->getValueErrors() as $error) {
                            ?>
                            <div class="help-block help-block-error">* <?= $error ?></div>
                        <?php }
                    } else { ?>
                        <div class="help-block help-block-error"></div>
                    <?php } ?>

                    <div class="help-block m-b0"><?= $dynamicValue->dynamicField->description ?></div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>