<?php
/**
 * User: nabi
 */

use common\models\Product;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $product \common\modules\product\interfaces\ProductInterface */
/* @var $this \yii\web\View */


Yii::$app->angular
    ->controller('product/productFormDelivery')
    ->service(['notify', 'router', 'user', 'geo'])
    ->constants([
        'countries'        => yii\helpers\ArrayHelper::toArray(
            \lib\geo\GeoNames::getAllCountries(),
            [\common\models\GeoCountry::class => ['id', 'iso_code', 'title', 'is_easypost_domestic', 'paypal_payout', 'is_easypost_intl']]
        ),
    ]);

?>

<?php
$form = ActiveForm::begin(
    [
        'id'      => 'ProductForm',
        'options' => ['enctype' => 'multipart/form-data', 'onsubmit' => '',]
    ]
);
?>
<div class="product-edit" ng-controller="ProductFormDeliveryController">

    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productform-incoterms"><?= _t('site.store', 'Incoterms') ?></label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($product, 'incoterms')->dropDownList(Product::getIncotermsList())->label(false); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productform-product_package"><?= _t('site.store', 'Product packaging') ?></label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($product, 'product_package')->textInput(['maxlength' => true, 'list' => 'datalist_product_package'])->label(false); ?>
                <datalist id="datalist_product_package">
                    <?php foreach (Product::getProductPackageList() as $k => $v): ?>
                        <option value="<?= $v; ?>"><?= $v; ?></option>
                    <?php endforeach; ?>
                </datalist>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productform-outer_package"><?= _t('site.store', 'Shipping packaging') ?></label>
            <div class="col-sm-6 col-md-5">
                <?= $form->field($product, 'outer_package')->textInput(['maxlength' => true, 'list' => 'datalist_outer_package'])->label(false); ?>
                <datalist id="datalist_outer_package">
                    <?php foreach (Product::getOuterPackageList() as $v): ?>
                        <option value="<?= $v; ?>"><?= $v; ?></option>
                    <?php endforeach; ?>
                </datalist>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-md-2" for="productform-outer_package"><?= _t('mybusiness.product', 'Ship from'); ?></label>
            <div class="col-sm-6 col-md-5">
                <div id="user-change-location-form">
                    <input type="text" id="productform-ship_from-location" value="<?= $product->getShipFromLocation(true)->formatted_address; ?>" class="form-control"/>
                    <input type="hidden" id="productform-ship_from" class="form-control" name="ProductForm[ship_from]"/>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <?php if(false): ?>
        <div class="col-lg-12">
            <h3><?=_t('mybusiness.product', 'Shipment details'); ?></h3>
            <div class="form-group">
                <label for="country" class="col-sm-3 col-lg-2 control-label"><?= _t('front.user', 'Country')?></label>
                <div class="col-sm-5 col-md-4">
                    <select
                            id="country"
                            ng-model="ps.country_id"
                            ng-options="country.id as country.title for country in $geo.getCountries()"
                            class="form-control">
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <table class="table" ng-cloak="true">
                        <tr>

                            <th width="40%"><?= _t('mybusiness.product', 'Country'); ?></th>
                            <th width="20%"><?= _t('mybusiness.product', 'First item'); ?></th>
                            <th width="20%"><?= _t('mybusiness.product', 'Following item'); ?></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr ng-repeat="productDelivery in product.productExpressDelivery">

                            <td><input type="text" class="form-control " ng-model="productDelivery.countryIso"></td>
                            <td><input type="text" class="form-control " ng-model="productDelivery.firstItem"></td>
                            <td><input type="text" class="form-control " ng-model="productDelivery.nextItem"></td>

                            <td>
                                <button type="button" ng-click="product.deleteProductDelivery(productDelivery)"
                                        class="btn btn-info btn-xs p-l10 p-r10" title="<?= _t('front.common', 'Delete'); ?>">
                                    <span class="tsi tsi-bin"></span>
                                </button></td>
                        </tr>
                    </table>

                    <button type="button" ng-click="product.addProductDelivery()"
                            class="btn btn-info btn-sm p-l10 p-r10" title="<?= _t('front.common', 'Add'); ?>">
                        <span class="tsi tsi-plus"></span> <?= _t('mybusiness.product', 'Add more'); ?>
                    </button>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>

    <div class="row">
        <hr>
        <div class="col-lg-8">
        </div>
        <div class="col-lg-4">
            <div class="row m-b10">
                <?php
                echo '<div class="col-sm-6">';
                if (!$product->isPublished()) {
                    $txtUpload = _t('site.store.', 'Save unpublished');
                    echo Html::buttonInput(
                        $txtUpload,
                        [
                            'class'    => 'btn btn-ghost btn-info btn-block t-model-edit-unpublish p-l10 p-r10',
                            'ng-click' => 'submitForm("unpublish")',
                            'style'    => 'font-size:16px;',
                        ]
                    );
                }
                echo '</div><div class="col-sm-6">';

                echo Html::buttonInput(
                    $product->isPublished() ? _t('front.user', 'Save') : _t('front.user', 'Publish'),
                    [
                        'class'    => 'btn btn-primary btn-block t-model-edit-publish m-b10',
                        'ng-click' => 'submitForm("publish")',
                    ]
                );


                echo '</div>';
                ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>