<?php

namespace frontend\modules\mybusiness\modules\product;

/**
 * mybusiness-product module definition class
 */
class ProductModule extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\mybusiness\modules\product\controllers';

    public const MODULE_PREFIX_URL = '/mybusiness/products';
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
