<?php

namespace frontend\modules\mybusiness\modules\product\controllers;

use backend\models\search\ProductSearch;
use common\models\Model3d;
use common\models\ProductCategory;
use common\modules\product\interfaces\ProductInterface;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\repositories\ProductRepositoryCondition;
use frontend\models\ps\PsFacade;
use frontend\modules\mybusiness\controllers\MybusinessController;
use frontend\modules\mybusiness\modules\product\models\ShopProductSearch;

/**
 * user shop controller - user actions to publish, unpublish models to store
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class ProductsController extends MybusinessController
{

    public $layout = 'storeLayout.php';

    /** @var ProductRepository */
    public $productRepository;

    public function injectDependencies(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }


    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'publish' => ['post'],
                    'remove'  => ['post'],
                ]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function actionIndex($type = '')
    {
        $ps = PsFacade::getPsByUserId($this->user->id);
        if (!$ps) {
            $this->setFlashMsg(false, _t('site.ps', 'Please save business information'));
            return $this->redirect('/mybusiness/company/create-ps');
        }

        if (!$ps->isSettingsSaved()) {
            $this->setFlashMsg(false, _t('site.ps', 'Please, provide your phone number and Save your settings'));
            return $this->redirect('/mybusiness/settings');
        }

        if ($ps->isDeleted()) {
            $this->setFlashMsg(false, _t('site.ps', 'The manufacturer is currently inactive.'));
            return $this->redirect('/mybusiness/company/create-ps');
        }

        $user = $this->getCurrentUser('/');

        $shopProductSearch = new ShopProductSearch();
        $shopProductSearch->load(\Yii::$app->request->get());

        $condition                     = new ProductRepositoryCondition();
        $condition->type               = $type;
        $condition->userId             = $user->id;
        $condition->companyId          = $user->company->id;
        $condition->isActive           = 1;
        $condition->onlyOriginalModels = 1;
        $condition->type               = $shopProductSearch->type;
        $condition->status             = $shopProductSearch->getSearchTypes();

        if ($type == '') {
            $url = $this->detectAvailableProductsUrl($shopProductSearch, $condition, $this->productRepository);
            if ($url) {
                return $this->redirect($url);
            }
        }

        $dataProvider = $this->productRepository->getProductsProvider($condition, 20);

        if (\Yii::$app->request->isAjax) {
            $htmlBlock = $this->renderAjax('productListContainer', ['dataProvider' => $dataProvider]);
            return $this->jsonReturn([
                'htmlBlock' => $htmlBlock
            ]);
        }
        return $this->render('productsList', [
                'user'              => $user,
                'dataProvider'      => $dataProvider,
                'shopProductSearch' => $shopProductSearch
            ]
        );
    }

    public function actionView($uuid)
    {
        $productCommon = $this->productRepository->getByUuid($uuid);
        if ($productCommon->getProductType() === ProductInterface::TYPE_MODEL3D) {
            /** @var Model3d $model3d */
            $model3d = $productCommon;
            return $this->redirect('/my/model/edit/' . $model3d->id);

        }
        return $this->render('productView', ['productCommon' => $productCommon]);
    }

    protected function detectAvailableProductsUrl(
        ShopProductSearch $shopProductSearch,
        ProductRepositoryCondition $condition,
        ProductRepository $productRepository)
    {
        $allSearchTypes = [
            ProductInterface::TYPE_PRODUCT,
            ProductInterface::TYPE_MODEL3D
        ];
        $dataProvider   = $productRepository->getProductsProvider($condition, 20);
        if (!$dataProvider->getTotalCount()) {
            foreach ($allSearchTypes as $searchType) {
                $shopProductSearch->status = ShopProductSearch::STATUS_ALL;
                $condition->type           = $searchType;
                $condition->status         = $shopProductSearch->getSearchTypes();
                $dataProvider              = $this->productRepository->getProductsProvider($condition);
                if ($dataProvider->getTotalCount()) {
                    return '/mybusiness/products?type=' . $searchType . '&status=' . ShopProductSearch::STATUS_ALL;
                }
            }
        }
    }
}
