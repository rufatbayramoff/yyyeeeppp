<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\product\controllers;


use common\components\DateHelper;
use common\models\CompanyCertification;
use common\models\CompanyCertificationBind;
use common\models\factories\FileFactory;
use common\models\Product;
use common\models\ProductCertification;
use common\models\repositories\FileRepository;
use frontend\modules\mybusiness\components\UserCompanyProvider;
use frontend\modules\mybusiness\models\CompanyCertificationForm;
use frontend\modules\mybusiness\models\ProductCertificationForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class ProductCertificationsController extends ProductController
{

    public function actionIndex($uuid)
    {
        $product = $this->productRepository->getByUuid($uuid);
        $this->product = $product;
        $searchParams = ['product_uuid' => $uuid];
        $dataProvider = $this->search($searchParams);

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'product'      => $product
            ]
        );
    }

    /**
     * Creates a new CompanyCertification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd($uuid)
    {
        $product = $this->productRepository->getByUuid($uuid);
        $this->product = $product;
        $this->view->params['section'] = '/mybusiness/products/product-certifications';
        $model = new ProductCertificationForm();
        $model->product_uuid = $product->getUuid();
        if ($model->load(Yii::$app->request->post())) {
            $uploadFile = UploadedFile::getInstance($model, 'file');
            $model->file = $uploadFile;
            if ($uploadFile) {
                $file = $this->fileFactory->createFileFromUploadedFile($uploadFile);
                $file->setPublicMode(1);
                $fileRepo = new FileRepository();
                $fileRepo->save($file);
                $model->file_id = $file->id;
            } else {
                $this->setFlashMsg(false, _t('mybusiness.certification', 'Image/PDF file is required.'));
                return $this->render(
                    'create',
                    [
                        'model' => $model,
                        'product' => $product
                    ]
                );
            }
            $model->created_at = DateHelper::now();
            if (!$model->validate()) {
                $this->setFlashMsg(false, Html::errorSummary($model));
                return $this->render(
                    'create',
                    [
                        'model' => $model,
                        'product' => $product
                    ]
                );
            }
            $model->safeSave();
            return $this->redirect(['index', 'uuid' => $uuid]);
        } else {
            return $this->render(
                'create',
                [
                    'model' => $model,
                    'product' => $product,
                ]
            );
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $productUuid = $model->product_uuid;
        $model->delete();

        return $this->redirect(['index', 'uuid' => $productUuid]);
    }

    protected function findModel($id)
    {
        if (($model = ProductCertification::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function search($params)
    {
        $item = new ProductCertification();
        $query = ProductCertification::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );

        $item->load($params, '');

        $query->andFilterWhere(
            [
                'id'            => $item->id,
                'product_uuid' => $item->product_uuid,
            ]
        );

        return $dataProvider;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \backend\components\NoAccessException
     * @throws \yii\db\StaleObjectException
     */
    public function actionUnbindCertification($id)
    {
        $bind = CompanyCertificationBind::find()->where(['id'=>$id])->one();
        $product = $this->productRepository->getByUuid($bind->product_uuid);
        $this->productService->tryCanEditProduct($product);
        $bind->delete();
        return $this->redirect('/mybusiness/products/product-certifications?uuid='.$product->getUuid());
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \backend\components\NoAccessException
     * @throws \yii\base\Exception
     */
    public function actionBindCertification()
    {
        $certificationBind = new CompanyCertificationBind();
        $certificationBind->load(Yii::$app->request->post());
        /**
         * @var $product Product
         */
        $product = $this->productRepository->getByUuid($certificationBind->product_uuid);
        $this->productService->tryCanEditProduct($product);
        $companyBlock = CompanyCertification::tryFindByPk($certificationBind->certification_id);
        if ($companyBlock->company_id === $product->company->id) {
            if (!CompanyCertificationBind::find()
                ->where(['product_uuid' => $certificationBind->product_uuid, 'certification_id' => $certificationBind->certification_id])
                ->exists()) {
                $certificationBind->safeSave();
            }
        }
        return $this->redirect('/mybusiness/products/product-certifications?uuid='.$product->getUuid());
    }
}