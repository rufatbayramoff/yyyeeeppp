<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.18
 * Time: 15:14
 */


namespace frontend\modules\mybusiness\modules\product\controllers;

use common\components\DateHelper;
use common\models\factories\FileFactory;
use common\models\Model3d;
use common\models\Product;
use common\models\ProductCategory;
use common\modules\dynamicField\services\DynamicFieldService;
use common\modules\product\factories\ProductFactory;
use common\modules\product\interfaces\ProductInterface;
use common\modules\product\populators\ProductPopulator;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\serializers\ProductCategorySerializer;
use common\modules\product\serializers\ProductSerializer;
use common\modules\product\services\ProductService;
use frontend\controllers\actions\TagsListAction;
use frontend\models\ps\PsFacade;
use frontend\modules\mybusiness\controllers\MybusinessController;
use frontend\modules\mybusiness\widgets\ProductFillPercentWidget;
use yii\web\NotFoundHttpException;

class ProductController extends MybusinessController
{

    public $layout = 'productFormLayout.php';

    /** @var ProductRepository */
    public $productRepository;

    /** @var ProductFactory */
    public $productFactory;

    /** @var ProductService */
    public $productService;

    /** @var FileFactory */
    public $fileFactory;

    /** @var ProductPopulator */
    public $productPopulator;

    /** @var DynamicFieldService */
    public $dynamicFieldsService;

    /**
     * @var Product
     */
    public $product = null;

    /**
     * @param ProductRepository $productRepository
     * @param ProductFactory $productFactory
     * @param ProductPopulator $productPopulator
     * @param ProductService $productService
     * @param DynamicFieldService $dynamicFieldsService
     * @param FileFactory $fileFactory
     */
    public function injectDependencies(
        ProductRepository $productRepository,
        ProductFactory $productFactory,
        ProductPopulator $productPopulator,
        ProductService $productService,
        DynamicFieldService $dynamicFieldsService,
        FileFactory $fileFactory
    ) {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->productPopulator = $productPopulator;
        $this->productService = $productService;
        $this->dynamicFieldsService = $dynamicFieldsService;
        $this->fileFactory = $fileFactory;
    }


    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['post']
                ]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function actions()
    {
        return array_merge(parent::actions(),
            [
                'tags' => [
                    'class' => TagsListAction::class
                ]
            ]);
    }

    /**
     * @param Product $product
     * @return bool
     * @throws \Exception
     */
    public function editProduct(Product $product): bool
    {
        if ($formData = \Yii::$app->request->post()) {
            $this->productPopulator
                ->populate($product, $formData)
                ->populateFiles($product, 'images')
                ->populateFiles($product, 'attachments')
                ->populateImagesRotateOptions($product, $formData);
            $product->productCommon->updated_at = DateHelper::now();

            if ($product->validate(null, false)) {
                $this->productRepository->setProductImagesRotate($product);
                $this->productRepository->saveProduct($product);
                \Yii::$app->getSession()->setFlash('success', _t('site.product', 'Product saved'), true);
                return true;
            }
        }

        return false;
    }

    /**
     * @param $uid
     * @return array|string|\yii\web\Response
     * @throws \Exception
     * @throws \backend\components\NoAccessException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionEdit($uid='')
    {
        $this->layout = 'productFormLayout';
        $productCommon = $this->productRepository->getProductCommonByUid($uid);
        $this->product = $productCommon;
        if ($productCommon->getProductType() === ProductInterface::TYPE_MODEL3D) {
            /** @var Model3d $model3d */
            $model3d = $productCommon;
            return $this->redirect('/my/model/edit/' . $model3d->id);
        }
        $productCommon->getAttachmentFiles();

        $this->productService->tryCanEditProduct($productCommon);
        if ($formData = \Yii::$app->request->post()) {
            if ($this->editProduct($productCommon)) {
                return $this->jsonReturn([
                    'success'     => true,
                    'product'     => ProductSerializer::serialize($productCommon),
                    'productFill' => ProductFillPercentWidget::widget(['product' => $productCommon])
                ]);
            } else {
                return $this->jsonReturn([
                    'success'          => false,
                    'validationErrors' => $productCommon->getErrors(),
                    'productFill'      => ProductFillPercentWidget::widget(['product' => $productCommon])
                ]);
            }
        }
        return $this->render('productForm', ['product' => $productCommon]);
    }

    /**
     * @param $uuid
     * @return string|\yii\web\Response
     * @throws \backend\components\NoAccessException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     */
    public function actionEditProperties($uuid)
    {
        $product = $this->productRepository->getByUuid($uuid);
        /**
         * @var Product $product
         */
        $this->product = $product;
        $this->productService->tryCanEditProduct($product);
        if ($formData = \Yii::$app->request->post()) {
            if ($this->editProduct($product)) {
                return $this->jsonReturn([
                    'success'     => true,
                    'productFill' => ProductFillPercentWidget::widget(['product' => $product])
                ]);
            } else {
                return $this->jsonReturn([
                    'success'          => false,
                    'validationErrors' => $product->getErrors(),
                    'productFill'      => ProductFillPercentWidget::widget(['product' => $product])
                ]);
            }
        }
        return $this->render('productTabProperties', ['product' => $product]);
    }

    /**
     * @param $uuid
     * @return array|string
     * @throws \Exception
     * @throws \backend\components\NoAccessException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionEditDelivery($uuid)
    {
        $product = $this->productRepository->getByUuid($uuid);
        $this->product = $product;
        $this->productService->tryCanEditProduct($product);
        if ($formData = \Yii::$app->request->post()) {
            if ($this->editProduct($product)) {
                return $this->jsonReturn([
                    'success' => true,
                    'productFill' => ProductFillPercentWidget::widget(['product' => $product])
                ]);
            } else {
                return $this->jsonReturn([
                    'success'          => false,
                    'validationErrors' => $product->getErrors(),
                    'productFill' => ProductFillPercentWidget::widget(['product' => $product])
                ]);
            }
        }
        return $this->render('productTabDelivery', ['product' => $product]);
    }

    public function actionEditVideos($uuid)
    {
        $product = $this->productRepository->getByUuid($uuid);
        $this->product = $product;
        $this->productService->tryCanEditProduct($product);
        if ($formData = \Yii::$app->request->post()) {
            if ($this->editProduct($product)) {
                return $this->jsonReturn([
                    'success' => true,
                    'productFill' => ProductFillPercentWidget::widget(['product' => $product])
                ]);
            } else {
                return $this->jsonReturn([
                    'success'          => false,
                    'validationErrors' => $product->getErrors(),
                    'productFill' => ProductFillPercentWidget::widget(['product' => $product])
                ]);
            }
        }
        return $this->render('productTabVideos', ['product' => $product]);
    }

    public function actionCreate()
    {
        $ps = PsFacade::getPsByUserId($this->user->id);

        if (!$ps) {
            $this->setFlashMsg(false, _t('site.ps', 'Please save business information'));
            return $this->redirect('/mybusiness/company/create-ps');
        }

        $product = $this->productFactory->create();
        $this->product = $product;
        if ($formData = \Yii::$app->request->post()) {
            if ($this->editProduct($product)) {
                return $this->jsonReturn([
                    'success' => true,
                    'uid'    => $product->productCommon->uid
                ]);
            } else {
                return $this->jsonReturn([
                    'success'          => false,
                    'validationErrors' => $product->getErrors(),
                ]);
            }
        }
        return $this->render('productForm', ['product' => $product]);
    }

    public function actionDelete($uuid)
    {
        $product = $this->productRepository->getByUuid($uuid);
        $this->productService->tryCanEditProduct($product);
        if ($product->getProductType() === ProductInterface::TYPE_PRODUCT) {
            /** @var Product $product */
            $product->productCommon->is_active = 0;
            $product->productCommon->safeSave();
            $this->redirect('/mybusiness/products');
        }
    }

    public function actionCategoriesShortList($title)
    {
        $categories = $this->productRepository->getCategoriesByKeyWords($title);
        return $this->jsonSuccess(['categories' => ProductCategorySerializer::serialize($categories)]);
    }

    /**
     * @param $postValues
     */
    public function reformatDynamicFieldValues($postValues)
    {
        $values = [];
        foreach ($postValues as $postValue) {
            $name = substr($postValue['name'], strlen('ProductForm[dynamicFields]['), -1);
            $values[$name] = $postValue['value'];
        }
        return $values;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdateCategories()
    {
        $categoryId = \Yii::$app->request->post('categoryId');
        $postValues = \Yii::$app->request->post('dynamicFieldValues');
        $category = ProductCategory::tryFindByPk($categoryId, 'Category not found');
        $values = $this->reformatDynamicFieldValues($postValues);
        $dynamicFieldValues = $this->dynamicFieldsService->getDynamicFieldValues($category, $values);
        return $this->renderAjax('productDynamicValues', [
            'dynamicValues' => $dynamicFieldValues,
            'formName'      => 'ProductForm'
        ]);

    }

    /**
     * @param $categoryId
     * @throws NotFoundHttpException
     */
    public function actionSubCategories($categoryId)
    {
        $category = ProductCategory::tryFindByPk($categoryId);
        if (!$category || !$category->is_active) {
            throw new NotFoundHttpException('Category not found');
        }
        $childs = ProductCategory::getChilds($category);
        return $this->jsonSuccess(['categories' => ProductCategorySerializer::serialize($childs)]);
    }
}