<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\modules\product\controllers;


use common\components\DateHelper;
use common\models\CompanyCertification;
use common\models\factories\FileFactory;
use common\models\ProductCertification;
use common\models\ProductFile;
use common\models\repositories\FileRepository;
use console\jobs\model3d\RenderJob;
use console\jobs\QueueGateway;
use frontend\modules\mybusiness\components\UserCompanyProvider;
use frontend\modules\mybusiness\models\CompanyCertificationForm;
use frontend\modules\mybusiness\models\ProductCertificationForm;
use frontend\modules\mybusiness\models\ProductFileForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class ProductFilesController extends ProductController
{

    public function actionIndex($uuid)
    {
        $product = $this->productRepository->getByUuid($uuid);
        $this->product = $product;
        $searchParams['product_uuid'] = $uuid;
        $dataProvider = $this->search($searchParams);

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'product' => $product
            ]
        );
    }

    /**
     * @return mixed
     */
    public function actionAdd($uuid)
    {
        $product = $this->productRepository->getByUuid($uuid);
        $this->product = $product;
        $this->view->params['section'] = '/mybusiness/products/product-files';
        $model = new ProductFileForm();
        if ($model->load(Yii::$app->request->post())) {
            $uploadFile = UploadedFile::getInstance($model, 'file');
            $model->file = $uploadFile;
            if ($uploadFile) {
                $file = $this->fileFactory->createFileFromUploadedFile($uploadFile);
                $file->setPublicMode(1);
                $fileRepo = new FileRepository();
                $fileRepo->save($file);
                $model->file_id = $file->id;
                $rJob = RenderJob::create($file);
                QueueGateway::addFileJob($rJob);
            } else {
                $this->setFlashMsg(false, _t('mybusiness.certification', 'File is required.'));
                return $this->render(
                    'create',
                    [
                        'model' => $model,
                    ]
                );
            }
            $model->product_uuid = $product->getUuid();
            $model->created_at = DateHelper::now();
            if (!$model->validate()) {
                $this->setFlashMsg(false, Html::errorSummary($model));
                return $this->render(
                    'create',
                    [
                        'model' => $model,
                    ]
                );
            }
            $model->type = ProductFile::TYPE_CAD;
            $model->safeSave();
            return $this->redirect(['index', 'uuid'=>$uuid]);
        } else {
            return $this->render(
                'create',
                [
                    'model' => $model,
                ]
            );
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $productUuid = $model->product_uuid;
        $model->delete();

        return $this->redirect(['index', 'uuid'=>$productUuid]);
    }

    protected function findModel($id)
    {
        if (($model = ProductFile::findByPk($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function search($params)
    {
        $item = new ProductFile();
        $item->load($params, '');
        $query = ProductFile::find();
        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => false]);
        $query->andFilterWhere(['product_uuid' => $item->product_uuid, 'type'=> [ProductFile::TYPE_CAD, ProductFile::TYPE_FILE]]);
        return $dataProvider;
    }
}