<?php


namespace frontend\modules\mybusiness\actionFilters;


use common\models\user\UserIdentityProvider;
use frontend\modules\mybusiness\components\UrlHelper;
use yii\base\ActionFilter;

class AffiliateAccessFilter extends ActionFilter
{
    /**
     * @var UserIdentityProvider $userIdentityProvider
     * */
    protected $userIdentityProvider;

    public function injectDependencies(
        UserIdentityProvider $userIdentityProvider
    )
    {
        $this->userIdentityProvider = $userIdentityProvider;
    }


    public function beforeAction($action)
    {
        $user = $this->userIdentityProvider->getUser();
        if (!$user->company) {
            $action->controller->setFlashMsg(false, _t('site.ps', 'Please save business information'));
            $action->controller->redirect(UrlHelper::createPs());
            return false;
        }
        return true;
    }
}