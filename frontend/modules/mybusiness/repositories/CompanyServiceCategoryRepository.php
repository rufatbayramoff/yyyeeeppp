<?php


namespace frontend\modules\mybusiness\repositories;


use common\models\CompanyServiceCategory;

class CompanyServiceCategoryRepository
{
    /**
     * @param array $ids
     * @return null|CompanyServiceCategory[]
     */
    public function findByIds(array $ids): array
    {
        return CompanyServiceCategory::find()->allActive()->andWhere(['id' => $ids])->all();
    }
}