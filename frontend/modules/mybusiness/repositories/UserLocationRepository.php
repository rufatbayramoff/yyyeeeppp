<?php
namespace frontend\modules\mybusiness\repositories;


class UserLocationRepository
{
    public function save(\common\models\UserLocation $location): void
    {
        $location->safeSave();
    }
}