<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\mybusiness\actions;


use common\components\BaseController;
use common\models\User;
use common\models\user\UserIdentityProvider;
use frontend\modules\mybusiness\models\CompanyForm;
use frontend\modules\mybusiness\services\CompanyService;
use HttpException;
use Yii;
use yii\base\Action;

/**
 * Class NewPsAction
 * @package frontend\modules\mybusiness\components
 *
 * @property-read BaseController $controller
 */
class NewPsAction extends Action
{
    /**
     * @var UserIdentityProvider
     */
    protected $userIdentityProvider;

    /**
     * @var CompanyService
     */
    protected $companyService;

    public function injectDependencies(
        UserIdentityProvider $userIdentityProvider,
        CompanyService $companyService
    )
    {
        $this->userIdentityProvider = $userIdentityProvider;
        $this->companyService       = $companyService;
    }

    /**
     * @return bool
     */
    public function beforeRun(): bool
    {
        /** @var User $user */
        $user = $this->userIdentityProvider->getUser();
        if ($user->company) {
            throw new HttpException(400, 'Company not exists');
        }
        return true;
    }


    public function run()
    {
        $form = new CompanyForm();
        if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {
            $this->companyService->createForUser($form->getDto(), $this->userIdentityProvider->getUser());
            return $this->controller->asJson(['success' => true, 'message' => _t('site.ps', 'Successfully saved')]);
        }
        return $this->controller->asJson(['success' => false, 'errors' => $form->getErrorSummary(true)]);
    }
}