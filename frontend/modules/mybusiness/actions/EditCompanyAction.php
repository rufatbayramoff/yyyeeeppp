<?php

namespace frontend\modules\mybusiness\actions;


use common\models\User;
use common\models\user\UserIdentityProvider;
use frontend\modules\mybusiness\models\CompanyForm;
use frontend\modules\mybusiness\services\CompanyService;
use HttpException;
use Yii;

class EditCompanyAction extends \yii\base\Action
{
    /**
     * @var UserIdentityProvider
     */
    protected $userIdentityProvider;

    /**
     * @var CompanyService
     */
    protected $companyService;

    public function injectDependencies(
        UserIdentityProvider $userIdentityProvider,
        CompanyService $companyService
    )
    {
        $this->userIdentityProvider = $userIdentityProvider;
        $this->companyService       = $companyService;
    }

    /**
     * @return bool
     */
    public function beforeRun(): bool
    {
        /** @var User $user */
        $user = $this->userIdentityProvider->getUser();
        if (empty($user->company)) {
            throw new HttpException(400, 'Company not exists');
        }
        return true;
    }

    public function run()
    {
        /** @var User $user */
        $user = $this->userIdentityProvider->getUser();
        $form = new CompanyForm($user->company);
        if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {
            $this->companyService->editForUser($form, $user);
            return $this->controller->asJson(['success' => true, 'message' => 'Company updated']);
        }
        return $this->controller->asJson(['success' => false, 'errors' => $form->getErrorSummary(true)]);
    }
}