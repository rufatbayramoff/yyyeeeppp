<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\serializers;

use common\components\FileTypesHelper;
use common\interfaces\FileBaseInterface;
use common\models\CompanyService;
use common\models\File;
use common\models\SiteTag;
use frontend\components\image\ImageHtmlHelper;
use frontend\modules\mybusiness\models\CompanyServiceAddForm;

class CompanyServiceSerializer extends \common\components\serizaliators\AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            CompanyService::class => [
                'id',
                'title'                => function (CompanyService $s) {
                    return $s->getTitleLabel();
                },
                'ps_id',
                'unitType'             => 'unit_type',
                'videos',
                'description',
                'category_id'          => function ($s) {
                    return (string)$s->category_id;
                },
                'images'               => function (CompanyService $companyService) {
                    $images = array_values($companyService->getImageFiles());
                    return $images;
                },
                'customProperties'     => function ($s) {
                    return $s->custom_properties;
                },
                'isNew'                => function ($s) {
                    return $s->isNewRecord;
                },
                'productPrices'        => 'moq_prices',
                'supply_ability',
                'supply_ability_time',
                'avg_production',
                'avg_production_time',
                'tags',
                'incoterms',
                'unitTypeLabel'        => 'unit_type',
                'isPublished'          => function (CompanyService $companyService) {
                    return $companyService->isPublished();
                },
                'moderatorStatusLabel' => function (CompanyService $companyService) {
                    return $companyService->getModeratorStatusLabel();
                },
                'visibilityLabel'      => function (CompanyService $companyService) {
                    return $companyService->getVisibilityLabel();
                }
            ],
            SiteTag::class        => [
                'id',
                'text'
            ],
            File::class           => [
                'id',
                'isVideo' => function (FileBaseInterface $file) {
                    return FileTypesHelper::isVideo($file);
                },
                'src'     => function (FileBaseInterface $file) {
                    if (FileTypesHelper::isImage($file)) {
                        return ImageHtmlHelper::getThumbUrl($file->getFileUrl(), 256, 256);
                    }
                    return null;
                },
                'fileUrl' => function (FileBaseInterface $file) {
                    if (FileTypesHelper::isImage($file)) {
                        return ImageHtmlHelper::getThumbUrl($file->getFileUrl(), 256, 256);
                    } elseif (FileTypesHelper::isVideo($file)) {
                        return ImageHtmlHelper::getVideoPreviewUrl($file, 256, 256);
                    } else {
                        return $file->getFileUrl();
                    }
                }
            ]
        ];
    }
}