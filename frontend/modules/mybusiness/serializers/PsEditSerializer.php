<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\mybusiness\serializers;


use common\components\serizaliators\AbstractProperties;
use common\models\factories\LocationFactory;
use common\models\Ps;
use frontend\components\UserSessionFacade;
use frontend\models\ps\AddPsForm;

class PsEditSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties(): array
    {
        return [
            AddPsForm::class => [
                'id',
                'title',
                'description',
                'location' => function (Ps $ps) {
                    return $ps->location ? LocationFactory::createFromUserLocation($ps->location) : UserSessionFacade::getLocation();
                }
            ]
        ];
    }
}