<?php


namespace frontend\modules\mybusiness\serializers;


use common\models\User;

class UserSerializer extends \common\components\serizaliators\AbstractProperties
{

    public function getProperties()
    {
        return [
            User::class => [
                'id',
                'username'
            ]
        ];

    }
}