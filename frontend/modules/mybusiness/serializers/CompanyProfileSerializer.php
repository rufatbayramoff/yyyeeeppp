<?php


namespace frontend\modules\mybusiness\serializers;


use common\components\FileTypesHelper;
use common\components\serizaliators\AbstractProperties;
use common\interfaces\FileBaseInterface;
use common\models\Company;
use common\models\File;
use frontend\components\image\ImageHtmlHelper;

class CompanyProfileSerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            Company::class => [
                'website',
                'year_established',
                'area_of_activity',
                'ownership',
                'incoterms' => function(Company $company) {
                  return explode(',',$company->incoterms);
                },
                'year_established',
                'total_employees',
                'annual_turnover',
                'facebook',
                'instagram',
                'twitter',
                'logoFilesize' => function(Company $company){
                    return file_exists($company->getLogoFullPath())
                        ? filesize($company->getLogoFullPath())
                        : 0;
                },
                'logoUrl' => function(Company $company){
                    return $company->logo
                        ? ImageHtmlHelper::getThumbUrl($company->getLogoUrl(true,256), 256, 256)
                        : null;
                },
                'coverUrl' => function(Company $company) {
                    return $company->coverFile
                        ? ImageHtmlHelper::getThumbUrlForFile($company->coverFile, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::THUMB_SMALL)
                        : null;
                },
                'pictures' => function(Company $company){
                    return $company->getPicturesFiles();
                },
            ],
            File::class => [
                'id',
                'isVideo' => function(FileBaseInterface $file){ return FileTypesHelper::isVideo($file);},
                'fileUrl' => function(FileBaseInterface $file){
                    if (FileTypesHelper::isImage($file)) {
                        return ImageHtmlHelper::getThumbUrl($file->getFileUrl(), 256, 256);
                    }
                    elseif (FileTypesHelper::isVideo($file)) {
                        return ImageHtmlHelper::getVideoPreviewUrl($file, 256, 256);
                    }
                    return $file->getFileUrl();
                }
            ]
        ];
    }
}