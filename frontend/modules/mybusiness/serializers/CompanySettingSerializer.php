<?php


namespace frontend\modules\mybusiness\serializers;


use common\components\serizaliators\AbstractProperties;
use common\models\Company;
use common\models\GeoCountry;
use frontend\models\ps\AddPsForm;

class CompanySettingSerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            AddPsForm::class => [
                'currency',
                'country_id' => function (Company $company) {
                    if ($company->country_id) {
                        return $company->country_id;
                    }
                    $country = GeoCountry::find()->where(['iso_code' => $company->phone_country_iso])->one();
                    if ($country) {
                        return $country->id;
                    }
                    return GeoCountry::ID_US;
                },
                'allowIncomingQuotes' => 'user.allow_incoming_quotes',
                'phone',
                'phone_code',
                'phone_country_iso',
                'phone_status',
            ]
        ];
    }
}