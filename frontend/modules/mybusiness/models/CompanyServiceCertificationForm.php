<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\models;


use common\models\CompanyServiceCertification;
use yii\web\UploadedFile;

class CompanyServiceCertificationForm extends CompanyServiceCertification
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['file'], 'file', 'extensions' => 'pdf, jpg, gif, png', 'checkExtensionByMimeType' => false];
        $rules[] = [['issue_date'], 'required'];
        return $rules;
    }
}