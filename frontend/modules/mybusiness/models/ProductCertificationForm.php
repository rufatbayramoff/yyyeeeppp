<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\models;


use common\models\ProductCertification;
use yii\web\UploadedFile;

class ProductCertificationForm extends ProductCertification
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['file'], 'file', 'extensions' => 'pdf, jpg, gif, png', 'checkExtensionByMimeType' => false];
        $rules[] = [['issue_date'], 'required'];
        return $rules;
    }
}