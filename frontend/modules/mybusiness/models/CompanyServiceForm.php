<?php
namespace frontend\modules\mybusiness\models;


use frontend\models\user\UserFacade;
use yii\base\Model;

class CompanyServiceForm extends Model
{
    public $ids = [];

    public function rules():array
    {
        return [
            ['ids', 'required'],
            ['ids', 'each', 'rule' => ['integer']]
        ];
    }
}