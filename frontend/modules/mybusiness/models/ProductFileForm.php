<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\models;


use common\components\FileTypesHelper;
use common\models\ProductCertification;
use common\models\ProductFile;
use yii\web\UploadedFile;

class ProductFileForm extends ProductFile
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        $rules = parent::rules();
        $cadFilesExt = array_merge(FileTypesHelper::RENDER_EXTENSIONS, FileTypesHelper::ALLOW_OTHER_3DMODELS_EXTENSIONS);
        $rules[] = [['file'], 'file', 'extensions' => $cadFilesExt, 'checkExtensionByMimeType' => false];

        return $rules;
    }
}