<?php

namespace frontend\modules\mybusiness\models;

use common\models\Company;
use common\models\factories\LocationFactory;
use frontend\models\site\DenyCountry;
use lib\geo\models\Location;
use yii\base\Model;

/**
 * @property  $address Location
 * */
class CompanyForm extends Model
{
    public $title;
    public $description;
    public $location;

    protected $company;

    public function __construct(Company $company = null, $config = [])
    {
        if ($company) {
            $this->company = $company;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['title', 'description', 'location'], 'required'],
            [['title'], 'unique', 'targetClass' => Company::class, 'filter' => $this->company ? ['<>', 'id', $this->company->id] : null],
            [['title', 'description'], 'trim'],
            [['title'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 1500],
        ];
    }

    public function beforeValidate()
    {
        if(parent::beforeValidate()){
            /** @var Location $address */
            $address = $this->address;
            $denyCountries = array_keys(DenyCountry::getDenyCountries());
            if(in_array($address->country, $denyCountries, true)) {
                $this->addError('location', _t("site", "Unfortunately, Treatstock payment system does not work in your country due to PayPal restrictions."));
                return false;
            }
            if(str_contains(strtolower($this->title), 'treatstock')){
                $this->addError('title', _t("site", "Title contains Treatstock"));
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @return Location
     * @throws \yii\base\InvalidConfigException
     */
    public function getAddress(): Location
    {
        return LocationFactory::createFromArray($this->location);
    }

    public function getDto(): CompanyDto
    {
        return new CompanyDto(
            $this->title,
            $this->description,
            $this->getAddress()
        );
    }

    public function attributeLabels(): array
    {
        return [
            'title' => _t('site.ps','Business name'),
            'description' => _t('site.ps','Business description'),
            'location' => _t('site.ps','Location (City)')
        ];
    }

}