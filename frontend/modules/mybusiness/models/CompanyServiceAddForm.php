<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\models;

use common\components\ArrayHelper;
use common\models\CompanyService;
use common\models\CompanyServiceCategory;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\repositories\FileRepository;
use Yii;
use yii\base\UserException;
use yii\web\UploadedFile;

/**
 * Class CompanyServiceAddForm
 *
 * @package frontend\modules\mybusiness\models
 */
class CompanyServiceAddForm extends CompanyService
{
    public $primaryPictureIndex;


    /**
     * @var array
     */
    private $unbindImageFiles = [];

    /**
     * old and new pictures
     *
     * @var array
     */
    public $pictures = [];

    /**
     * Pictures
     *
     * @var UploadedFile[]
     */
    public $newPictures = [];

    /**
     * @var array
     */
    public $newImageFileIds = [];

    public static function createForm()
    {
        $form = new self();

        return $form;
    }

    public function rules()
    {
        return [
            [['category_id'], 'number'],
            [['description', 'pictures', 'primaryPictureIndex'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [
                'description',
                'filter',
                'filter' => function ($value) {
                    return \yii\helpers\HtmlPurifier::process($value);
                }
            ],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyServiceCategory::class, 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    public function load($data, $formName = null)
    {
        $isLoaded = parent::load($data, $formName);
        if ($isLoaded) {
            $this->newPictures = UploadedFile::getInstancesByName('pictures');
        }
        $this->processPictures();

        return $isLoaded;
    }

    /**
     *
     * @throws UserException
     * @throws \Exception
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    private function processPictures()
    {
        $fileFactory = Yii::createObject(FileFactory::class);
        $fileRepository = Yii::createObject(FileRepository::class);
        $oldFiles = $this->isNewRecord ? [] : $this->getImages();

        $fileIds = array_map('intval', array_filter(ArrayHelper::getColumn($this->pictures, 'id')));

        $newImageFileIds = [];
        foreach ($this->newPictures as $pictureUploadedFile) {
            $file = $fileFactory->createFileFromUploadedFile($pictureUploadedFile);
            $file->setPublicMode(true);
            $fileRepository->save($file);
            $newImageFileIds[] = $file->id;
            $fileIds[] = $file->id;
        }

        $unbindImageFiles = [];
        foreach ($oldFiles as $oldFile) {
            if (!\in_array($oldFile->id, $fileIds, true)) {
                $unbindImageFiles[] = $oldFile;
            }
        }
        $this->unbindImageFiles = $unbindImageFiles;
        $this->newImageFileIds = $newImageFileIds;
    }

    /**
     * @return File[]
     */
    public function getUnbindImageFiles()
    {
        return $this->unbindImageFiles;
    }
}