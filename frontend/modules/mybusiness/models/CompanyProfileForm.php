<?php


namespace frontend\modules\mybusiness\models;


use common\models\Company;
use common\models\Product;
use frontend\models\user\CropForm;
use yii\base\Model;
use yii\web\UploadedFile;

class CompanyProfileForm extends Model
{

    public $website;
    public $area_of_activity;
    public $ownership;
    public $incoterms = [];
    public $year_established;
    public $total_employees;
    public $annual_turnover;
    public $facebook;
    public $instagram;
    public $twitter;
    public $coverFile;
    public $logoFile;
    public $pictures = [];
    public $optionFileUploads = [];
    public $uploadPictures;
    public $circleCrop;
    public $squareCrop;

    public $circleForm;
    public $squareForm;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->circleForm = new CropForm([]);
        $this->squareForm = new CropForm([]);
    }

    public function rules(): array
    {
        return [
            [['area_of_activity','ownership','website','facebook','instagram','twitter'], 'string'],
            [['annual_turnover'],'number'],
            ['area_of_activity', 'in', 'allowArray' => true, 'range' => self::getBusinessTypeList()],
            ['ownership', 'in', 'allowArray' => true, 'range' => $this->getOwnershipList()],
            ['total_employees', 'in', 'allowArray' => true, 'range' => $this->getTotalEmployeesList()],
            ['incoterms', 'each', 'rule' => ['in', 'range' => $this->getIncoterms()]],
            [['area_of_activity','ownership','website','facebook','instagram','twitter'], 'trim'],
            [['year_established'], 'date', 'format' => 'php:Y'],
            [['website','facebook','instagram','twitter'], 'url', 'defaultScheme' => 'http'],
            [['uploadPictures'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 20],
            [['coverFile','logoFile'], 'file', 'skipOnEmpty' => true],
            [['pictures','optionFileUploads'],'safe']
        ];
    }

    /**
     * @return string
     */
    public function getIncotermFormat(): string
    {
        return implode(',', $this->incoterms);
    }

    public function getOwnershipList(): array
    {
        return [
            '-',
            'Sole proprietorship',
            'Partnership',
            'C Corporation',
            'S Corporation',
            'Limited Liability Company (LLC)',
            'Branches (Foreign Corporation)',
            'Joint Venture',
            'Foreign Corporation Representative',
            'Other'
        ];
    }

    public static function getBusinessTypeList(): array
    {
        return [
            Company::MANUFACTURING_BUSINESS,
            Company::SERVICE_BUSINESS,
            Company::MERCHANDISING_BUSINESS,
            Company::AFFILIATE_BUSINESS,
            Company::DEALER,
            Company::DISTRIBUTOR,
        ];
    }

    public function getIncoterms(): array
    {
        return Product::getIncotermsList();
    }

    public function getTotalEmployeesList(): array
    {
        return [
            '-',
            '<10',
            '10-49',
            '49-250',
            '>250'
        ];
    }

    public function load($data, $formName = null)
    {
        $loaded = parent::load($data, $formName);
        $this->circleForm->load($data, 'circleCrop');
        $this->squareForm->load($data, 'squareCrop');
        return $loaded;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        $validate = parent::validate($attributeNames, $clearErrors);
        if($this->logoFile) {
            $circleValidate = $this->circleForm->validate(null, $clearErrors);
            $squareValidate = $this->squareForm->validate(null, $clearErrors);
            return $validate && $circleValidate && $squareValidate;
        }
        return $validate;
    }

    /**
     * @return bool
     */
    public function upload(): bool
    {
        $this->coverFile = UploadedFile::getInstanceByName('coverFile');
        $this->logoFile = UploadedFile::getInstanceByName('logoFile');
        $this->uploadPictures = UploadedFile::getInstancesByName('pictures');
        return $this->validate();
    }
}