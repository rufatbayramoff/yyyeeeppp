<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\models;


use common\components\ArrayHelper;
use common\models\CompanyBlock;
use common\models\CompanyBlockBind;
use common\models\CompanyService;
use common\models\Product;
use common\modules\company\repositories\CompanyServiceRepository;
use common\modules\company\services\CompanyServicesService;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\services\ProductService;
use yii\base\Model;

class CompanyBlockBindForm extends Model
{
    public $blockBind = [];
    public $productUuids;
    public $companyServiceIds;
    public $companyId;
    public $userId;
    public function rules()
    {
        return [
            [['blockBind', 'productUuids', 'companyServiceIds'], 'safe']
        ];
    }

    /**
     * @throws \backend\components\NoAccessException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function bindBlocks()
    {
        if (!empty($this->productUuids)) {
            $this->bindBlocksProducts();
        }
        if (!empty($this->companyServiceIds)) {
            $this->bindBlocksServices();
        }
    }

    /**
     * @param CompanyBlock $block
     * @param CompanyService $service
     * @return bool|CompanyBlockBind
     * @throws \yii\base\Exception
     */
    public function bindCompanyBlockToService(CompanyBlock $block, CompanyService $service)
    {
        if ($block->company_id != $service->ps_id) {
            return false;
        }
        $bindBlock = new CompanyBlockBind();
        $bindBlock->block_id = $block->id;
        $bindBlock->company_service_id = $service->id;
        $bindBlock->safeSave();
        return $bindBlock;
    }

    /**
     * @param CompanyBlock $block
     * @param Product $product
     * @return bool|CompanyBlockBind
     * @throws \yii\base\Exception
     */
    public function bindCompanyBlockToProduct(CompanyBlock $block, Product $product)
    {
        if ($block->company_id != $product->company_id) {
            return false;
        }
        $bindBlock = new CompanyBlockBind();
        $bindBlock->block_id = $block->id;
        $bindBlock->product_uuid = $product->uuid;
        $bindBlock->safeSave();
        return $bindBlock;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    protected function bindBlocksProducts()
    {
        /**
         * @var $productService ProductService
         */
        $productService = \Yii::createObject(ProductService::class);
        $productRepo = \Yii::createObject(ProductRepository::class);
        $blockBinds = $this->blockBind;

        $productUuidsAll = array_map('trim', array_filter(explode(",", $this->productUuids)));
        /** @var Product[] $productsModels */
        $productsModels = ArrayHelper::index(Product::find()->where(['uuid' => $productUuidsAll, 'user_id'=>$this->userId])->all(), 'uuid');
        $productToBlocksMap = [];
        foreach ($productsModels as $productModel) {
            $productToBlocksMap[$productModel->uuid] = [];
        }
        foreach ($blockBinds as $blockId => $productUuids) {
            foreach ($productUuids as $productUuid) {
                $productToBlocksMap[$productUuid][] = $blockId;
            }
        }
        foreach ($productToBlocksMap as $productUuid => $blockIds) {
            $productModel = $productsModels[$productUuid] ?? null;
            if (!$productModel) {
                continue;
            }
            $currentBlocks = $productModel->getBindedCompanyBlockIds();
            // delete old
            foreach ($currentBlocks as $curBlockId) {
                if (!in_array($curBlockId, $blockIds)) { // delete
                    CompanyBlockBind::deleteAll(['block_id' => $curBlockId, 'product_uuid' => $productUuid]);
                }
            }
            // add
            foreach ($blockIds as $newBlockId) {
                if (in_array($newBlockId, $currentBlocks)) {
                    continue;
                }
                $companyBlock = CompanyBlock::tryFindByPk($newBlockId);
                $this->bindCompanyBlockToProduct($companyBlock, $productModel);
            }
        }
    }

    /**
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    private function bindBlocksServices()
    {
        $blockBinds = $this->blockBind;

        $servicesIds = array_map('trim', array_filter(explode(",", $this->companyServiceIds)));
        /** @var CompanyService[] $models */
        $models = ArrayHelper::index(CompanyService::find()->where(['id' => $servicesIds, 'ps_id'=>$this->companyId])->all(), 'id');
        $serviceToBlockMap = [];
        foreach ($models as $model) {
            $serviceToBlockMap[$model->id] = [];
        }
        foreach ($blockBinds as $blockId => $companyServiceIds) {
            foreach ($companyServiceIds as $id) {
                $serviceToBlockMap[$id][] = $blockId;
            }
        }
        foreach ($serviceToBlockMap as $serviceId => $blockIds) {
            $model = $models[$serviceId] ?? null;
            if (!$model) {
                continue;
            }
            $currentBlocks = $model->getBindedCompanyBlockIds();
            // delete old
            foreach ($currentBlocks as $curBlockId) {
                if (!in_array($curBlockId, $blockIds)) {
                    CompanyBlockBind::deleteAll(['block_id' => $curBlockId, 'company_service_id' => $serviceId]);
                }
            }
            // add
            foreach ($blockIds as $newBlockId) {
                if (in_array($newBlockId, $currentBlocks)) {
                    continue;
                }
                $companyBlock = CompanyBlock::tryFindByPk($newBlockId);
                $this->bindCompanyBlockToService($companyBlock, $model);
            }
        }
    }
}