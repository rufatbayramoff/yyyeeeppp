<?php

namespace frontend\modules\mybusiness\models;


use common\components\validators\DenyCountryValidator;
use common\components\validators\PhoneValidator;
use common\models\GeoCountry;
use lib\money\Currency;
use yii\base\Model;

class CompanySettingForm extends Model
{
    public $phone_country_iso;
    public $phone_code;
    public $phone;
    public $allowIncomingQuotes;
    public $currency;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['phone_code', 'phone_country_iso', 'phone', 'currency'], 'string'],
            [['phone_code', 'phone_country_iso', 'phone'], 'trim'],
            ['allowIncomingQuotes', 'boolean'],
            ['phone_country_iso', DenyCountryValidator::class],
            ['phone', PhoneValidator::class, 'phoneCountryIsoAttribute' => 'phone_country_iso', 'phoneCodeAttribute' => 'phone_code', 'correctPhone' => true],
        ];
    }


    public function load($data, $formName = null)
    {
        parent::load($data, $formName);
    }

    /**
     * @return bool
     */
    public function isFillQuotes(): bool
    {
        return $this->allowIncomingQuotes !== null;
    }

    /**
     * @return string|null
     */
    public function phoneNumber(): ?string
    {
        return $this->phone_code . $this->phone;
    }
}