<?php


namespace frontend\modules\mybusiness\models;


use common\models\Company;
use common\models\User;
use yii\base\Model;

class SearchClientForm extends Model
{
    public $q;

    protected $company;

    public function __construct(Company $company, array $params = [])
    {
        parent::__construct($params);
        $this->company = $company;
    }

    public function rules(): array
    {
        return [
            ['q','string'],
            ['q','trim'],
        ];
    }

    public function search(array $params)
    {
        $query = User::find()
            ->notSystem()
            ->notDeveloperAccounts()
            ->notTreatstockAccounts();
        $query->innerJoinWith(['storeOrders.currentAttemp']);
        $query->andWhere(['store_order_attemp.ps_id' => $this->company->id]);
        $this->load($params, '');
        $this->validate($params);
        if($this->q){
            $query->likeUsername($this->q);
        }
        return $query->all();
    }
}