<?php
/**
 * User: nabi
 */
namespace frontend\modules\mybusiness\models;

use yii\base\Model;

class SettingsModel extends Model
{
    public $country;
    public $currency;

    public $paymentMethod = 'paypal';
    public $paymentEmail;

    public $taxForm;
    public $document;

}