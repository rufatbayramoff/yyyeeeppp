<?php


namespace frontend\modules\mybusiness\models;


use common\models\Company;
use common\models\factories\LocationFactory;
use frontend\components\UserSessionFacade;
use frontend\models\site\DenyCountry;
use lib\geo\models\Location;
use yii\base\Model;

/**
 *  * @property  $address Location
 */
class AffiliateCompanyForm extends Model
{
    public $website;
    public $fullName;
    public $title;
    public $about;
    public $type;
    public $location;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->location = (array)UserSessionFacade::getLocation();
    }


    public function rules(): array
    {
        return [
            [['fullName', 'title', 'about', 'type'], 'required'],
            ['type', 'in', 'allowArray' => true, 'range' => array_keys($this->types())],
            ['website', 'url','defaultScheme' => 'http'],
            [['title'], 'unique', 'targetClass' => Company::class],
            [['title'], 'string', 'length' => ['1, 250']],
            [['fullName'], 'string', 'max' => 1500],
            [['title',], 'trim'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'website' => _t('site.ps', 'Website'),
            'fullName' => _t('site.ps', 'Full Name'),
            'title' => _t('site.ps', 'Company'),
            'about' => _t('site.ps', 'About you'),
            'type' => _t('site.ps', 'Partnership type')
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            /** @var Location $address */
            $address = $this->address;
            $denyCountries = array_keys(DenyCountry::getDenyCountries());
            if (in_array($address->country, $denyCountries, true)) {
                $this->addError('location', _t("site", "Unfortunately, Treatstock payment system does not work in your country due to PayPal restrictions."));
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @return Location
     * @throws \yii\base\InvalidConfigException
     */
    public function getAddress(): Location
    {
        return LocationFactory::createFromArray($this->location);
    }

    public function types(): array
    {
        return [
            'affiliate' => _t('site.ps', 'Affiliate'),
            'api' => _t('site.ps', 'API Partner'),
            'media' => _t('site.ps', 'Media Partner')
        ];
    }

    /**
     * @return CompanyDto
     * @throws \yii\base\InvalidConfigException
     */
    public function getDto(): CompanyDto
    {
        return new CompanyDto(
            $this->title,
            $this->about,
            $this->getAddress()
        );
    }
}