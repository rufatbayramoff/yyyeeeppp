<?php


namespace frontend\modules\mybusiness\models;


use lib\geo\models\Location;

class CompanyDto
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $description;
    /**
     * @var Location
     */
    public $location;

    public function __construct(
        string $title,
        string $description,
        Location $location
    )
    {

        $this->title = $title;
        $this->description = $description;
        $this->location = $location;
    }
}