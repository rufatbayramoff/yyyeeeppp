<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\models;


use common\components\ArrayHelper;
use common\models\CompanyBlock;
use common\models\CompanyBlockBind;
use common\models\CompanyCertification;
use common\models\CompanyCertificationBind;
use common\models\Product;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\services\ProductService;
use yii\base\Model;

class CompanyCertificationBindForm extends Model
{
    public $certsBind = [];
    public $productUuids;
    public function rules()
    {
        return [
            [['certsBind', 'productUuids'], 'safe']
        ];
    }

    /**
     * @throws \backend\components\NoAccessException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function bindCertifications()
    {
        /**
         * @var $productService ProductService
         */
        $productService = \Yii::createObject(ProductService::class);
        $productRepo = \Yii::createObject(ProductRepository::class);
        $certsBinds = $this->certsBind;

        $productUuidsAll = array_map('trim', array_filter(explode(",", $this->productUuids)));
        /** @var Product[] $productsModels */
        $productsModels = ArrayHelper::index(Product::find()->where(['uuid' => $productUuidsAll])->all(), 'uuid');
        $productToCertsMap = [];
        foreach ($productsModels as $productModel) {
            $productToCertsMap[$productModel->uuid] = [];
        }
        foreach ($certsBinds as $blockId => $productUuids) {
            foreach ($productUuids as $productUuid) {
                $productToCertsMap[$productUuid][] = $blockId;
            }
        }
        foreach ($productToCertsMap as $productUuid => $certIds) {
            $productModel = $productsModels[$productUuid]??null;
            if (!$productModel) {
                continue;
            }
            $currentBindedCerts = $productModel->getBindedCompanyCertificationsIds();
            // delete old
            foreach ($currentBindedCerts as $curCertId) {
                if (!in_array($curCertId, $certIds)) { // delete
                    CompanyCertificationBind::deleteAll(['certification_id' => $curCertId, 'product_uuid' => $productUuid]);
                }
            }
            // add
            foreach ($certIds as $newCertId) {
                if (in_array($newCertId, $currentBindedCerts)) {
                    continue;
                }
                $companyCert = CompanyCertification::tryFindByPk($newCertId);
                $this->bindToProduct($companyCert, $productModel);
            }
        }
    }

    /**
     * @param CompanyCertification $cert
     * @param Product $product
     * @return bool|CompanyCertificationBind
     * @throws \yii\base\Exception
     */
    public function bindToProduct(CompanyCertification $cert, Product $product)
    {
        if ($cert->company_id != $product->company_id) {
            return false;
        }
        $bindCert = new CompanyCertificationBind();
        $bindCert->certification_id = $cert->id;
        $bindCert->product_uuid = $product->uuid;
        $bindCert->safeSave();
        return $bindCert;
    }
}