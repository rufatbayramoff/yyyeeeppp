<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\models;


use common\models\CompanyCertification;
use yii\web\UploadedFile;

class CompanyCertificationForm extends CompanyCertification
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['file'], 'file'];

        return $rules;
    }
}