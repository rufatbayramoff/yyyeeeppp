<?php

namespace frontend\modules\mybusiness\components;

use common\models\File;
use yii\imagine\Image;

/**
 * Trait ImageRotate
 * @package frontend\modules\mybusiness\components
 */
trait ImageRotateTrait
{
    public $rotate;

    /**
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function setImageRotate(): void
    {
        if (!$this->rotate) {
            return;
        }

        /** @var File $file */
        $file = $this->file;

        if ($file) {
            $location = $file->getLocalTmpFilePath();
            Image::frame($location, 0)->rotate((int) $this->rotate)->save($location);
            $file->publishFileToServerAndSave($location);
        }
    }
}