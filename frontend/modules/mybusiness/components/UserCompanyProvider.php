<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\components;


use common\models\user\UserIdentityProvider;
use frontend\models\ps\PsFacade;

class UserCompanyProvider extends UserIdentityProvider
{

    /**
     * @return \common\models\Ps
     */
    public function getUserCompany()
    {
        $user = $this->getUser();
        return PsFacade::getPsByUserId($user->id);
    }
}