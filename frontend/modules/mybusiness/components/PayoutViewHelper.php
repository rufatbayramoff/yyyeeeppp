<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\mybusiness\components;


use common\models\Payment;
use common\models\User;
use common\models\UserPaypal;
use common\models\UserTaxInfo;
use common\modules\payment\processors\BankPayoutProcessor;
use common\modules\payment\services\PaymentAccountService;
use frontend\models\site\DenyCountry;
use lib\money\Currency;
use lib\money\Money;
use Yii;

class PayoutViewHelper
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function isTaxFormExist(): bool
    {
        return $this->user->userTaxInfo && !in_array($this->user->userTaxInfo->status, [UserTaxInfo::STATUS_DECLINE, UserTaxInfo::STATUS_NEW]);
    }

    /**
     * @return bool
     */
    public function hasPendingPayouts(): bool
    {
        return Payment::hasPendingPayouts($this->user);
    }

    /**
     * @return bool
     */
    public function userPaypalStatusOk(): bool
    {
        return UserPaypal::isUserPaypalOk($this->user->userPaypal);
    }

    /**
     * @param Money[] $balance
     * @return bool
     */
    public function bankTransferAllowed(array $balances): bool
    {
        foreach ($balances as $balance) {
            if ($balance->getAmount() >= BankPayoutProcessor::MINIMUM_PAYOUT) {
                return true;
            };
        }
        return false;
    }

    /**
     * @param Money[] $balances
     * @return bool
     */
    public function hasCurrencySelector(array $balances): bool
    {
        foreach ($balances as $balance) {
            if ($balance->getCurrency() == Currency::EUR) {
                return true;
            };
        }
        return false;
    }

    public function hasCurrency(array $balances,string $currency): bool
    {
        foreach ($balances as $balance) {
            if ($balance->getCurrency() == $currency && ($balance->getAmount() > 1)) {
                return true;
            };
        }
        return false;
    }

    public function hasUsd(array $balances): bool
    {
        return $this->hasCurrency($balances, Currency::USD);
    }

    public function hasEur(array $balances): bool
    {
        return $this->hasCurrency($balances, Currency::EUR);
    }

    /**
     * @param Money[] $balances
     * @return bool
     */
    public function currencySelectedEUR(array $balances): bool
    {
        foreach ($balances as $balance) {
            if ($balance->getCurrency() == Currency::USD && ($balance->getAmount() < 500)) {
                return true;
            };
        }
        return false;
    }

    /**
     * @return bool
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\InvalidConfigException
     */
    public function isDisabled(): bool
    {
        /** @var PaymentAccountService $paymentAccountService */
        $paymentAccountService = Yii::createObject(PaymentAccountService::class);
        return ($paymentAccountService->getUserMainAmount($this->user)->getAmount() < 0) && ($paymentAccountService->getUserMainAmount($this->user, Currency::EUR)->getAmount() < 0);
    }

    /**
     * @return null|string
     */
    public function countryAddress(): ?string
    {
        return !empty($this->user->userTaxInfo->address_id) ? $this->user->userTaxInfo->address->country->iso_code : false;
    }

    /**
     * @return bool
     * @throws \common\components\exceptions\BusinessException
     */
    public function hideDeny(): bool
    {
        return !empty($this->user->userTaxInfo) ? (DenyCountry::checkCountry($this->user->userTaxInfo->place_country, false) &&
            DenyCountry::checkCountry($this->countryAddress(), false)) : true;
    }

    public function isPaypalEnable(): bool
    {
        $company = $this->user->company;
        if (!$company) {
            return false;
        }
        $country = $company->country;
        if (!$country) {
            return false;
        }
        return (bool)$country->paypal_payout;
    }
}