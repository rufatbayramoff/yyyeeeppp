<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\mybusiness\components;


use yii\helpers\Url;

class TaxUrlHelper
{
    /**
     * @return string
     */
    public static function taxes() : string
    {
        return Url::toRoute(['/mybusiness/taxes']);
    }

    /**
     * @return string
     */
    public static function taxesEditStep1() : string
    {
        return Url::toRoute(['/mybusiness/taxes/step1', 'edit' => true]);
    }

    /**
     * @return string
     */
    public static function taxesStep1() : string
    {
        return Url::toRoute(['/mybusiness/taxes/step1']);
    }

    /**
     * @return string
     */
    public static function taxesStep2() : string
    {
        return Url::toRoute(['/mybusiness/taxes/step2']);
    }

    /**
     * @return string
     */
    public static function taxesStep3() : string
    {
        return Url::toRoute(['/mybusiness/taxes/step3']);
    }

    /**
     * @return string
     */
    public static function taxesStep4() : string
    {
        return Url::toRoute(['/mybusiness/taxes/step4']);
    }

    /**
     * @return string
     */
    public static function pdf() : string
    {
        return Url::toRoute(['/mybusiness/taxes/get-pdf']);
    }

    /**
     * @return string
     */
    public static function preview() : string
    {
        return Url::toRoute(['/mybusiness/taxes/preview']);
    }
}