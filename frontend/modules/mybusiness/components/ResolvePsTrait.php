<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\mybusiness\components;


use common\components\exceptions\AssertHelper;
use common\models\Company;
use common\models\Ps;
use frontend\models\ps\AddPsForm;
use frontend\models\user\UserFacade;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Trait ResolvePsTrait
 * @package frontend\modules\mybusiness\components
 */
trait ResolvePsTrait
{
    /**
     * Fund ps for current user
     *
     * @param string $class
     *            Class for resolving PS instance
     * @return Company|AddPsForm
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    private function resolvePs($class = Company::class)
    {
        AssertHelper::assert($class == Company::class || is_subclass_of($class, Company::class), "Class {$class} is not PS");

        if (!UserFacade::getCurrentUserId()) {
            throw new ForbiddenHttpException(_t('site.ps', 'Please, login.'));
        }

        $user = UserFacade::getCurrentUser();

        /** @var Company $ps */
        /** @var Company $class */
        $ps = $class::find()
            ->forUser($user)
            ->one();

        if ($ps instanceof AddPsForm) {
            $ps->allowIncomingQuotes = $user->allow_incoming_quotes;
        }

        if (!$ps) {
            throw new NotFoundHttpException(_t('site.ps', 'Print Service not found'));
        }

        if (!UserFacade::isObjectOwner($ps)) {
            throw new ForbiddenHttpException();
        }

        return $ps;
    }
}