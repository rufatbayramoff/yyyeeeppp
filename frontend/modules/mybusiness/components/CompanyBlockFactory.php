<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\components;


use common\components\DateHelper;
use common\models\Company;
use common\models\CompanyBlock;

class CompanyBlockFactory
{
    /**
     * @return CompanyBlock
     */
    public function create()
    {
        $companyBlock = new CompanyBlock();
        $companyBlock->title = '';
        $companyBlock->created_at = DateHelper::now();
        return $companyBlock;
    }

    /**
     * @param Company $company
     */
    public function createByCompany(Company $company)
    {
        $companyBlock = $this->create();
        $companyBlock->company_id = $company->id;
        return $companyBlock;
    }
}