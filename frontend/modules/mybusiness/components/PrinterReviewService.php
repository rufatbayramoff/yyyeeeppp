<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\components;


use common\models\PrinterReview;
use common\models\PsPrinter;

class PrinterReviewService
{

    /**
     * @param PsPrinter $psPrinter
     * @return array|mixed|null|object|string
     */
    public static function getReviewLinkLabel(PsPrinter $psPrinter)
    {
        $review = self::getReview($psPrinter);
        if (!$review) {
            return _t('site.printer', 'Review your 3D printer');
        }

        return $review->getStatusLabel();
    }

    /**
     * @param PsPrinter $psPrinter
     * @return array|PrinterReview|mixed|null|\yii\db\ActiveRecord
     */
    public static function getReview(PsPrinter $psPrinter)
    {
        $printerReview = PrinterReview::find()
            ->where(['printer_id' => $psPrinter->printer_id, 'user_id' => $psPrinter->ps->user_id])
            ->one();
        return $printerReview;
    }

    /**
     * @param PsPrinter $onePrinter
     * @return string
     */
    public static function getReviewLink(PsPrinter $onePrinter)
    {
        $review = self::getReview($onePrinter);
        if ($review && $review->status == PrinterReview::STATUS_PUBLISHED) {
            return $onePrinter->printer->getPublicPageUrl();
        }
        return sprintf('/mybusiness/services/review?id=%d', $onePrinter->id);
    }

    public static function getPrinterReviews(\common\models\Printer $item)
    {
        $reviews = PrinterReview::find()->where(['printer_id' => $item->id, 'status' => [PrinterReview::STATUS_PUBLISHED, PrinterReview::STATUS_NEW]])->orderBy('id desc')->limit(100)->all();
        return $reviews;
    }

    public static function getColorByRating($rating)
    {
        $ratingVal = ceil($rating);
        $ratingVal = max(1, $ratingVal);
        $colors = [
            1 => '#eb0000',
            '#fa9a1f',
            '#feca1e',
            '#93d24e',
            'green'
        ];

        return $colors[$ratingVal];

    }

    /**
     * @param $printerReview
     * @param $reviews
     * @return array
     */
    public static function calculateAvg(array $reviews): array
    {
        $printerReview = new PrinterReview();
        $printerReviewCols = $printerReview->getReviewLabels();
        $result = [];

        foreach ($reviews as $review) {
            foreach ($printerReviewCols as $code => $title) {
                $result[$code][] = $review->$code;
            }
        }

        $total = 0;
        foreach ($result as $code => $value) {
            $result[$code] = round(array_sum($value) / count($value), 2);
            $total += round(array_sum($value) / count($value), 2);
        }

        if (count($result) > 0) {
            $result['summary'] = round($total / count($result), 2);
        } else {
            $result['summary'] = '';
        }

        return $result;
    }
}