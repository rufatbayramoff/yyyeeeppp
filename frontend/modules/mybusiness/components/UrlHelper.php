<?php


namespace frontend\modules\mybusiness\components;


use yii\helpers\Url;

class UrlHelper
{
    /**
     * @return string
     */
    public static function createPs() : string
    {
        return Url::toRoute(['/mybusiness/company/create-ps']);
    }
}