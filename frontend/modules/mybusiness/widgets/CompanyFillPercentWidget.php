<?php


namespace frontend\modules\mybusiness\widgets;


use common\models\Company;
use frontend\modules\mybusiness\services\progress\Progress;
use frontend\modules\mybusiness\services\progress\steps\About;
use frontend\modules\mybusiness\services\progress\steps\Phone;
use frontend\modules\mybusiness\services\progress\steps\Portfolio;
use frontend\modules\mybusiness\services\progress\steps\Service;
use frontend\modules\mybusiness\services\progress\steps\Taxes;
use yii\base\Widget;

class CompanyFillPercentWidget extends Widget
{
    /**
     * @var Company
     */
    public $company;

    public function run(): string
    {
        $progress = new Progress([
            new About($this->company),
            new Service(),
            new Portfolio(),
            new Phone(),
            new Taxes()
        ]);
        return $this->render('companyFillPercent',['percentResult' => $progress->percent($this->company)]);
    }
}