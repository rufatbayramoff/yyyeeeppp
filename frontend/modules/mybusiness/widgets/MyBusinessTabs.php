<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\widgets;

use frontend\components\image\ImageHtmlHelper;
use frontend\models\user\UserFacade;

use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyInformer;
use common\modules\informer\models\CompanyServiceInformer;
use common\modules\informer\models\ProductInformer;
use common\modules\informer\models\TaxInformer;
use yii\base\Widget;
use Yii;
use yii\bootstrap\Tabs;

class MyBusinessTabs extends Widget
{
    public $section = null;

    public function run()
    {
        $user = UserFacade::getCurrentUser();
        $redDot = ImageHtmlHelper::getRedDot();
        $links = [
            [
                'url'   => '/mybusiness/company',
                'label' => _t('front.user', 'About'). (InformerModule::getInformer($user, CompanyInformer::class) ? $redDot : ''),
            ],
          /*  [
                'url'   => '/mybusiness/settings',
                'label' => _t('front.user', 'Settings')
            ],*/
            [
                'url'   => '/mybusiness/products',
                'label' => _t('front.user', 'Store'). (InformerModule::getInformer($user, ProductInformer::class) ? $redDot : ''),
            ],
            [
                'url'   => '/mybusiness/services',
                'label' => _t('front.user', 'Services') . (InformerModule::getInformer($user, CompanyServiceInformer::class) ? $redDot : ''),
            ],
            [
                'url'   => '/mybusiness/taxes',
                'label' => _t('front.user', 'Taxes'). (InformerModule::getInformer($user, TaxInformer::class) ? $redDot : ''),
            ],
            [
                'url'   => '/mybusiness/widgets',
                'label' => _t('front.user', 'Business Tools')
            ],
            /*[
                'url'   => '/mybusiness/certifications',
                'label' => _t('front.user', 'Certifications')
            ],*/
        ];

        $items = [];
        $activeUrl = $this->section ?: Yii::$app->requestedRoute;
        $activeUrlTrim = trim($activeUrl, '/');

        foreach ($links as $k => $v) {
            $url = trim(\Yii::getAlias(substr($v['url'], 0, strlen($v['url']) - 1)), '/');
            if (strpos($activeUrlTrim, $url) === 0) {
                $v['active'] = true;
            }
            $items[] = $v;
        }


        return '<div class="nav-tabs__container"><div class="container container--wide">' . Tabs::widget(
                [
                    'renderTabContent' => false,
                    'encodeLabels' => false,
                    'options'          => [
                        'class' => '',
                        'role'  => 'tablist'
                    ],
                    'items'            => $items,
                ]
            ) . '</div></div>';
    }
}