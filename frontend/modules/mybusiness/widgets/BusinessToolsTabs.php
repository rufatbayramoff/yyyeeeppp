<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 04.04.2018
 * Time: 18:09
 */

namespace frontend\modules\mybusiness\widgets;


use common\modules\affiliate\helpers\AffiliateUrlHelper;
use frontend\modules\mybusiness\components\UrlHelper;
use yii\base\Widget;
use Yii;
use yii\widgets\Menu;

class BusinessToolsTabs extends Widget
{
    public $section = null;

    public function run()
    {


        $links = [
            [
                'label'   => _t('mybusiness.tools', 'Services'),
                'options' => [
                    'class' => 'text-divider',
                ],
                'url'     => null
            ],
            [
                'url'   => '/mybusiness/widgets/printers',
                'label' => _t('front.user', 'My Services Widget')
            ],
            [
                'url'   => '/mybusiness/widgets/get-quote',
                'label' => _t('front.user', 'Get a Quote Widget')
            ],
            [
                'url'   => '/mybusiness/widgets/rate',
                'label' => _t('front.user', 'Treatstock Rating Widget')
            ],
            [
                'label'   => _t('mybusiness.tools', 'Products'),
                'options' => [
                    'class' => 'text-divider',
                ],
                'url'     => null
            ],
            [
                'url'   => '/mybusiness/widgets/store-products',
                'label' => _t('front.user', 'Products Store')
            ],
            [
                'label'   => _t('mybusiness.tools', '3D Models'),
                'options' => [
                    'class' => 'text-divider',
                ],
                'url'     => null
            ],
            [
                'url'   => '/mybusiness/widgets/store-models',
                'label' => _t('front.user', '3D Models Store')
            ],
            [
                'url'   => '/mybusiness/widgets/models',
                'label' => _t('front.user', '3D Model Widget')
            ],
            [
                'label'   => _t('mybusiness.tools', 'Partnership'),
                'options' => [
                    'class' => 'text-divider',
                ],
                'url'     => null
            ],
            [
                'url'   => '/mybusiness/cashback/settings',
                'label' => _t('front.user', 'Bonus system')
            ],
            [
                'url'   => AffiliateUrlHelper::sourcesList(true),
                'label' => _t('front.user', 'Affiliate link')
            ],
            [
                'url'   => '/mybusiness/widgets/affiliate-widget',
                'label' => _t('front.user', 'Affiliate widget')
            ],
            [
                'url'   => '/mybusiness/widgets/api',
                'label' => _t('front.user', 'API')
            ],
        ];

        $items     = [];
        $activeUrl = $this->section ?: Yii::$app->request->getUrl();
        $activeUrl = \common\components\UrlHelper::getUrlWithoutParams($activeUrl);

        foreach ($links as $k => $v) {
            if (strpos($activeUrl, $v['url']) === 0) {
                $v['active'] = true;
            }
            $items[] = $v;
        }


        return Menu::widget([
            'activateParents' => false,
            'options'         => ['class' => 'side-nav help-nav', 'id' => 'navMobile'],
            'encodeLabels'    => false,
            'items'           => $items,
        ]);
    }
}