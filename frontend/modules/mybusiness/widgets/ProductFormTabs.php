<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\widgets;

use common\models\Product;
use http\Url;
use yii\base\Widget;
use Yii;
use yii\bootstrap\Tabs;

class ProductFormTabs extends Widget
{
    public $section = null;
    /**
     * @var Product
     */
    public $product = null;

    public function run()
    {
        $links = [
            [
                'url'   => '/mybusiness/products/product/edit',
                'label' => _t('mybusiness.product', 'General Information')
            ],
            [
                'url'   => '/mybusiness/products/product/edit-properties',
                'label' => _t('mybusiness.product', 'Specifications')
            ],
            [
                'url'   => '/mybusiness/products/product/edit-delivery',
                'label' => _t('mybusiness.product', 'Delivery Details')
            ],

            [
                'url'   => '/mybusiness/products/product-files',
                'label' => _t('mybusiness.product', 'CAD Files')
            ],
            [
                'url'   => '/mybusiness/products/product/edit-videos',
                'label' => _t('mybusiness.product', 'Videos')
            ],
            [
                'url'   => '/mybusiness/products/product-certifications',
                'label' => _t('mybusiness.product', 'Certifications')
            ],
            [
                'url'   => '/mybusiness/products/product-blocks',
                'label' => _t('mybusiness.product', 'Additional Information')
            ],
        ];

        $items         = [];
        $activeUrl     = $this->section ?: Yii::$app->requestedRoute;
        $activeUrlTrim = trim($activeUrl, '/');

        foreach ($links as $k => $v) {
            $url           = trim(\Yii::getAlias($v['url']), '/');
            $activeUrlTrim = str_replace("/index", "", $activeUrlTrim);
            if ($activeUrlTrim == $url) {
                $v['active'] = true;
            }
            $v['url'] = $v['url'] . '?uid=' . $this->product->productCommon->uid . '&uuid=' . $this->product->uuid;
            if ($this->product->isNewRecord) {
                $v['url'] = 'javascript:alert(_t("site.app", "Please save general information first"));';
            }
            $items[] = $v;
        }


        return Tabs::widget(
            [
                'renderTabContent' => false,
                'encodeLabels'     => false,
                'options'          => [
                    'class' => ' nav-tabs--secondary',
                    'role'  => 'tablist'
                ],
                'items'            => $items,
            ]
        );
    }
}