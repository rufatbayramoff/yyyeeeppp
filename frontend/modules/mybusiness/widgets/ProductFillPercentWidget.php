<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.08.18
 * Time: 14:57
 */

namespace frontend\modules\mybusiness\widgets;

use common\components\helpers\ColorHelper;
use common\models\Product;
use common\modules\product\components\ProductFillPercentCalculator;
use yii\base\Widget;

class ProductFillPercentWidget extends Widget
{
    /**
     * @var Product
     */
    public $product = null;

    /** @var ProductFillPercentCalculator */
    protected $productFillPercentCalculator;

    const COLOR_RED = '#ff401a';
    const COLOR_ORANGE = '#ff8000';
    const COLOR_YELLOW = '#ffbf00';
    const COLOR_GREEN = '#54cc29';

    /**
     * @param ProductFillPercentCalculator $productService
     */
    public function injectDependencies(ProductFillPercentCalculator $productFillPercentCalculator)
    {
        $this->productFillPercentCalculator = $productFillPercentCalculator;
    }

    public function run()
    {
        $fillPercentInfo = $this->productFillPercentCalculator->calculateFillPercent($this->product);

        $color = self::COLOR_GREEN;
        if ($fillPercentInfo->fillPercent<80) {
            $color = self::COLOR_YELLOW;
        }
        if ($fillPercentInfo->fillPercent<50) {
            $color = self::COLOR_ORANGE;
        }
        if ($fillPercentInfo->fillPercent<30) {
            $color = self::COLOR_RED;
        }

        return $this->render('productFillPercent', [
            'fillPercentInfo' => $fillPercentInfo,
            'color'           => $color
        ]);
    }
}