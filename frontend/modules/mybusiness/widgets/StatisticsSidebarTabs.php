<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\widgets;

use yii\base\Widget;

class StatisticsSidebarTabs extends Widget
{
    public $section = null;
    public function run()
    {
        $links = [
            [
                'label' => 'Affiliates',
                'options' => [
                    'class' => 'text-divider',
                ],
                'url' => null
            ],
            [
                'url'   => '/affiliate/cabinet',
                'label' => _t('front.user', 'Your Resources')
            ],
            [
                'url'   => '/affiliate/cabinet/users',
                'label' => _t('front.user', 'Affiliate Users')
            ],
            [
                'url'   => '/affiliate/cabinet/rewards',
                'label' => _t('front.user', 'Affiliate Rewards')
            ],
            [
                'label' => 'API',
                'options' => [
                    'class' => 'text-divider',
                ],
                'url' => null
            ],
            [
                'url'   => '/mybusiness/api-orders',
                'label' => _t('front.user', 'API Orders')
            ],
        ];

        $items = [];
        $curUrl = $this->section ? : \Yii::$app->request->pathInfo;
        foreach ($links as $k => $v) {

            if (trim($curUrl, '/') == trim(\Yii::getAlias($v['url']), '/')) {
                $v['active'] = true;
            }
            $items[] = $v;
        }

        return \yii\widgets\Menu::widget(
            [
                'options'      => ['class' => 'side-nav help-nav', 'id' => 'navMobile'],
                'encodeLabels' => false,
                'items'        => $items,
            ]
        );
    }
}
