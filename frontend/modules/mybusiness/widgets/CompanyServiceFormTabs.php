<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\widgets;

use common\models\Product;
use yii\base\Widget;
use Yii;
use yii\bootstrap\Tabs;
use yii\helpers\Url;

class CompanyServiceFormTabs extends Widget
{
    public $section = null;
    /**
     * @var Product
     */
    public $companyService = null;

    protected $urlPrefix = 'mybusiness/company-services/company-service';

    public function run()
    {
        $links = [
            [
                'url'   => ['company-service/edit'],
                'label' => _t('mybusiness.product', 'General Information')
            ],
            [
                'url'   => ['company-service/edit-properties'],
                'label' => _t('mybusiness.product', 'Specifications')
            ],
            [
                'url'   =>  ['company-service/edit-delivery'],
                'label' => _t('mybusiness.product', 'Delivery Details')
            ],
            [
                'url'   => ['company-service/edit-videos'],
                'label' => _t('mybusiness.product', 'Videos')
            ],
            [
                'url'   => '/mybusiness/company-services/certifications',
                'label' => _t('mybusiness.product', 'Certifications')
            ],
            [
                'url'   => '/mybusiness/company-services/blocks',
                'label' => _t('mybusiness.product', 'Additional Information')
            ],
        ];

        $items = [];
        $activeUrl = $this->section ?: Yii::$app->requestedRoute;
        $activeUrlTrim = trim($activeUrl, '/');

        foreach ($links as $k => $v) {
            $url = trim(Url::toRoute($v['url']), '/');
            $activeUrlTrim = str_replace("/index", "", $activeUrlTrim);
            if ($activeUrlTrim == $this->urlPrefix . '/' . $url || $activeUrlTrim == $url) {
                $v['active'] = true;
            }
            $v['url'] = '/'. $url . '?id=' . $this->companyService->id;
            if ($this->companyService->isNewRecord) {
                $v['url'] = 'javascript:alert(_t("site.app", "Please save general information first"));';
            }
            $items[] = $v;
        }


        return Tabs::widget(
            [
                'renderTabContent' => false,
                'encodeLabels'     => false,
                'options'          => [
                    'class' => ' nav-tabs--secondary',
                    'role'  => 'tablist'
                ],
                'items'            => $items,
            ]
        );
    }
}