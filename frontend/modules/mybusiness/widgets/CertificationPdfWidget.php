<?php


namespace frontend\modules\mybusiness\widgets;


use common\models\CompanyService;
use common\models\TsInternalPurchaseCertification;
use common\modules\tsInternalPurchase\repositories\TsInternalPurchaseRepository;
use yii\base\Widget;

class CertificationPdfWidget extends Widget
{
    /**
     * @var CompanyService
     */
    public $companyService;
    /**
     * @var TsInternalPurchaseRepository
     */
    private $tsInternalPurchaseRepository;

    public function __construct(TsInternalPurchaseRepository $tsInternalPurchaseRepository, $config = [])
    {
        parent::__construct($config);
        $this->tsInternalPurchaseRepository = $tsInternalPurchaseRepository;
    }

    public function run(): string
    {
        /** @var TsInternalPurchaseCertification $lastCertificationPayed */
        $lastCertificationPayed = $this->tsInternalPurchaseRepository->getLastPayedCertForService($this->companyService);
        $tsCertificationClass = $lastCertificationPayed->tsCertificationClass ?? $this->companyService->psPrinter->printer->tsCertificationClass;
        return $this->render('certification', [
            'tsCertificationClass' => $tsCertificationClass,
            'companyService'       => $this->companyService,
            'inlineCss'            => file_get_contents(__DIR__ . '/css/certification.css'),
        ]);
    }
}