<?php
/** @var TsInternalPurchaseCertification $certification */
/** @var tsCertificationClass $tsCertificationClass */
/** @var CompanyService $companyService */

use common\models\CompanyService;
use common\models\TsCertificationClass;
use common\models\TsInternalPurchaseCertification;
use yii\helpers\Url;

?>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap');
    <?=$inlineCss?>
</style>

<div class="cert__header">
    <a href="https://www.treatstock.com/" class="cert__logo" target="_blank">
        <img src="https://static.treatstock.com/static/images/logo_2x.png" width="285px" height="60px">
    </a>
    <div class="cert__logo-descr">Smart Manufacturing Platform</div>
</div>

<div class="cert__type">
    <?php echo $tsCertificationClass->title?> Certificate
</div>

<div class="cert__name-label"><?php echo _t('site.ps','Awarded to')?></div>

<div class="cert__name">
    <?php echo $companyService->ps->title?>
</div>

<div class="container container--80">
    <div class="row">
        <div class="cert__machine">
            <div class="cert__machine-label"><?php echo _t('site.ps.test', 'Machine')?></div>
            <?php echo $companyService->getTitleLabel()?>
        </div>
        <div class="cert__expire">
            <div class="cert__expire-label"><?php echo _t('site.ps.test', 'Expire date')?></div>
            <span class="cert__expire-date"><?= Yii::$app->formatter->asDate($companyService->certification_expire) ?></span>
        </div>
    </div>
</div>

<div class="cert__divider"></div>


<div class="cert__description">
    <?php echo $tsCertificationClass->description?>
</div>

<svg class="cert__bg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.42 155.25">
    <defs>
        <style>.cls-1 {
                fill: currentColor;
            }</style>
    </defs>
    <path class="cls-1"
          d="M419.31,439.38l-61.1-16.51a20.79,20.79,0,0,0-19.58,4.92l-54.28,51a12,12,0,0,0-3.8,8.84,10.51,10.51,0,0,0,10.52,10.52,11,11,0,0,0,2.69-.34l109.5-28.31a9.12,9.12,0,0,1,11.41,8.84v8.73L287.42,520a9.13,9.13,0,0,0,2.29,18,10.72,10.72,0,0,0,2.29-.28L414.66,506v13.49a9.15,9.15,0,0,1-6.83,8.84L287.42,559.46a9.13,9.13,0,0,0,2.29,18,10.72,10.72,0,0,0,2.29-.28l125.45-32.45a20.65,20.65,0,0,0,15.5-20V457A18.43,18.43,0,0,0,419.31,439.38Zm-61.94,26.41A13.6,13.6,0,1,1,371,452.2,13.61,13.61,0,0,1,357.37,465.79Z"
          transform="translate(-280.54 -422.17)"/>
</svg>

<div class="cert__site">
    <div class="cert__site-text"><span>treatstock</span>.com</div>
</div>

<div class="cert__qr">
    <img style="width: 125px; height: 125px;" alt="QR Code" src="<?php echo param('server').'/site/qr?v='.$companyService->company->getPublicCompanyLink()?>"
</div>