<?php

use frontend\assets\ProgressAsset;
use frontend\modules\mybusiness\services\progress\ProgressValue;
use frontend\modules\mybusiness\services\progress\StepUrl;

/** @var $percentResult ProgressValue */

ProgressAsset::register($this);
?>
<div class="product-fill-percent designer-card m-b30" id="companyFillPercent">
    <h4 class="m-t0 text-center"><?= _t('site.ps', 'Companies with more information get more orders')?></h4>
    <div class="progress-circle p<?php echo $percentResult->percent();?> <?php echo $percentResult->percent() > 50 ? 'over50' : ''?>" style="color:<?php echo $percentResult->color()?>">
        <span><?php echo $percentResult->percent()?>%</span>
        <div class="left-half-clipper">
            <div class="first50-bar" style="background-color:<?php echo $percentResult->color()?>"></div>
            <div class="value-bar" style="border-color:<?php echo $percentResult->color()?>"></div>
        </div>
    </div>
    <?php if($percentResult->needFill()):?>
        <label><?= _t('site.ps', 'Start selling:')?></label>
        <ul class="product-fill-percent__list">
            <?php /** @var StepUrl $step */
            foreach($percentResult->steps() as $step):?>
                <li><a href="<?php echo $step->url?>"><?php echo $step->name?></a></li>
            <?php endforeach;?>
        </ul>
    <?php endif;?>
</div>