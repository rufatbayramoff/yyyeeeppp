<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 17.08.18
 * Time: 16:35
 */

/** @var $fillPercentInfo \common\modules\product\models\ProductFillPercentInfo */

use frontend\assets\ProgressAsset;

ProgressAsset::register($this);
?>
<div class="product-fill-percent designer-card" id="productFillPercent">
    <?php if(false): ?>
    <div class="product-fill-percent__value" style="color:<?=$color?>"><?= $fillPercentInfo->fillPercent; ?>% </div>
    <?php endif; ?>
    <div class="progress-circle <?=$fillPercentInfo->fillPercent > 50?'over50':'';?> p<?=$fillPercentInfo->fillPercent;?>" style="color:<?=$color?>">
        <span ><?= $fillPercentInfo->fillPercent; ?>%</span>
        <div class="left-half-clipper">
            <div class="first50-bar" style="background-color:<?=$color?>"></div>
            <div class="value-bar" style="border-color:<?=$color?>"></div>
        </div>
    </div>
<?php if ($fillPercentInfo->unfilledFields) { ?>
    <label><?= _t('mybusiness.product', 'Please add') ?>:</label>
    <ul class="product-fill-percent__list">
        <?php foreach ($fillPercentInfo->unfilledFields as $unfilledField) {
            ?>
            <li><?= $unfilledField ?></li>
            <?php
        } ?>
    </ul>
<?php } ?>
</div>