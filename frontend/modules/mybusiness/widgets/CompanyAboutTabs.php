<?php
/**
 * User: nabi
 */

namespace frontend\modules\mybusiness\widgets;

use common\models\Company;
use Yii;
use yii\base\Widget;
use yii\bootstrap\Tabs;

class CompanyAboutTabs extends Widget
{
    public $section = null;
    /**
     * @var Company
     */
    public $company;

    public function run()
    {
        $links = [
            [
                'url'   => $this->company ? '/mybusiness/company/edit-ps' : '/mybusiness/company/create-ps',
                'label' => _t('mybusiness.product', 'Main Information')
            ],
            [
                'visible' => $this->company,
                'url'   =>   '/mybusiness/company/services',
                'label' => _t('mybusiness.product', 'Capabilities')
            ],
            [
                'visible' => $this->company && $this->company->url,
                'url'   => '/mybusiness/settings',
                'label' => _t('front.user', 'Settings')
            ],
            [
                'visible' => $this->company,
                'url'   => '/mybusiness/company/profile',
                'label' => _t('front.user', 'Public Profile Information')
            ],
            [
                'visible' => $this->company,
                'url'     => '/mybusiness/certifications',
                'label'   => _t('mybusiness.product', 'Certifications')
            ],
            [
                'visible' => $this->company,
                'url'     => '/mybusiness/company-blocks',
                'label'   => _t('mybusiness.product', 'Additional Information')
            ],
        ];
        $items = [];
        $activeUrl = $this->section ?: Yii::$app->requestedRoute;
        $activeUrlTrim = trim($activeUrl, '/');

        foreach ($links as $k => $v) {
            $url = trim(\Yii::getAlias($v['url']), '/');
            $activeUrlTrim = str_replace("/index", "", $activeUrlTrim);
            if ($activeUrlTrim == $url) {
                $v['active'] = true;
            }
            $items[] = $v;
        }


        return Tabs::widget(
            [
                'renderTabContent' => false,
                'encodeLabels'     => false,
                'options'          => [
                    'class' => ' nav-tabs--secondary',
                    'role'  => 'tablist'
                ],
                'items'            => $items,
            ]
        );
    }
}