<?php
namespace frontend\modules\mybusiness\services;


use common\models\CompanyCategory;
use common\modules\company\factories\CompanyServiceFactory;
use common\modules\company\repositories\CompanyServiceRepository;

class CreateMockCompanyService
{
    public function __construct(
        private CompanyServiceRepository $companyServiceRepository,
        private CompanyServiceFactory $companyServiceFactory,
    )
    {

    }

    public function create(CompanyCategory $model)
    {
        if($model->needCreateService() === false){
            return;
        }
        if ($model->companyServiceCategory->code == CompanyCategory::CODE_3D_PRINTING) {
            return;
        }
        if($this->companyServiceRepository->companyServiceExist($model->company, $model->company_service_category_id)){
            return;
        }
        $service = $this->companyServiceFactory->createDraftService($model->company);
        $service->category_id = $model->company_service_category_id;
        $service->title = "{$model->company->title} {$model->companyServiceCategory->title}";
        $service->description = $model->companyServiceCategory->title;
        $service->moq_prices = [
            ['moq' => 1, 'priceWithFee' => 1, 'price' => 1]
        ];
        $this->companyServiceRepository->save($service);

    }
}