<?php


namespace frontend\modules\mybusiness\services;


use common\models\Company;
use yii\helpers\Inflector;

class CompanyGenerateUrlManager
{
    /**
     * @param string $title
     * @return string
     */
    public function generate(string $title)
    {
        $url = Inflector::slug($title);

        $i = 0;
        while (Company::find()->where(['url' => $url])->exists()) {
            $url .= $i;
        }
        return $url;
    }
}