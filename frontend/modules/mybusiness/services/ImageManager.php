<?php


namespace frontend\modules\mybusiness\services;


use common\components\FileTypesHelper;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\Ps;
use common\models\repositories\FileRepository;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\user\CropForm;
use Yii;
use yii\imagine\Image;
use yii\web\UploadedFile;

class ImageManager
{

    /**
     * @var FileFactory
     */
    private $fileFactory;
    /**
     * @var FileRepository
     */
    private $fileRepository;

    public function __construct(
        FileFactory $fileFactory, FileRepository $fileRepository)
    {
        $this->fileFactory = $fileFactory;
        $this->fileRepository = $fileRepository;
    }

    public function saveCover(?UploadedFile $uploadedFile, $fileName = 'cover_file_id', $ownerClass = Ps::class)
    {
        if (!$uploadedFile) {
            return null;
        }
        $file = $this->fileFactory->createFileFromUploadedFile($uploadedFile);
        $file->setPublicMode(true);

        $file->setOwner($ownerClass, $fileName);

        // this method also save file
        ImageHtmlHelper::stripExifInfo($file);
        $this->fileRepository->save($file);
        return $file->id;
    }

    /**
     * @param UploadedFile|null $uploadedFile
     * @return \common\models\File|\common\models\FileAdmin|null
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\UserException
     */
    public function saveFile(?UploadedFile $uploadedFile)
    {
        if (!$uploadedFile) {
            return null;
        }
        $file = $this->fileFactory->createFileFromUploadedFile($uploadedFile);
        $file->setPublicMode(true);
        $this->fileRepository->save($file);
        return $file;
    }

    public function crop(UploadedFile $uploadedFile, CropForm $cropForm, string $fileName, string $filePath)
    {
        $time = time();
        $file = "{$fileName}_{$time}.{$uploadedFile->extension}";
        $image = Image::getImagine()->open($uploadedFile->tempName);
        $cropForm->adapt($image->getSize());
        $image
            ->crop($cropForm->getCropStartPoint(), $cropForm->getCropBox())
            ->save("$filePath/$file");
        return $file;
    }

    public function rotate(?File $file,int $rotate)
    {
        if(!$file) {
            return;
        }

        if (FileTypesHelper::isImage($file)) {
            ImageHtmlHelper::rotateImage($file, $rotate);
        }
    }
}