<?php


namespace frontend\modules\mybusiness\services;


use common\components\ArrayHelper;
use common\models\CompanyCategory;
use common\models\User;
use frontend\modules\mybusiness\models\CompanyServiceForm;
use frontend\modules\mybusiness\repositories\CompanyServiceCategoryRepository;
use yii\web\NotFoundHttpException;

class CompanyServiceManager
{


    /**
     * CompanyServiceManager constructor.
     * @param CompanyServiceCategoryRepository $companyServiceCategoryRepository
     * @param CreateMockCompanyService $createMockCompanyService
     */
    public function __construct(
        private CompanyServiceCategoryRepository $companyServiceCategoryRepository,
        private CreateMockCompanyService $createMockCompanyService
    )
    {
    }

    public function create(CompanyServiceForm $form, User $user): void
    {
        $categories = $this->companyServiceCategoryRepository->findByIds($form->ids);
        if(!$categories) {
            throw new NotFoundHttpException();
        }
        $companyCategories = CompanyCategory::find()->forCompany($user->company)->indexBy('company_service_category_id')->all();
        $diff = array_diff(array_keys($companyCategories), ArrayHelper::getColumn($categories, 'id'));
        if(!empty($diff)) {
            CompanyCategory::deleteAll(['company_service_category_id' => array_values($diff),CompanyCategory::column('company_id') => $user->company->id]);
        }
        foreach ($categories as $category) {
            if(!isset($companyCategories[$category->id])) {
                $companyCategory = CompanyCategory::create(
                    companyId: $user->company->id,
                    categoryId: $category->id
                );
                $companyCategory->safeSave();
                $this->createMockCompanyService->create($companyCategory);
            }
        }
    }
}