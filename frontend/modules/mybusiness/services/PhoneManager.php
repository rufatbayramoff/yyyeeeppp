<?php


namespace frontend\modules\mybusiness\services;


use common\models\Ps;
use common\models\UserSms;
use yii\base\Exception;

class PhoneManager
{
    public function status(int $userId, ?string $phone)
    {
        if (!$phone) {
            return [null, null];
        }

        /** @var UserSms $confirmPhoneSms */
        $confirmPhoneSms = UserSms::find()->forPhoneNumber($phone)
            ->andWhere([
                'user_id' => $userId,
                'type' => UserSms::TYPE_CONFIRM
            ])
            ->latest();

        if (!$confirmPhoneSms || $confirmPhoneSms->status == UserSms::STATUS_NEW) {
            return [Ps::PHONE_STATUS_NEW, null];
        }

        if ($confirmPhoneSms->status == UserSms::STATUS_ACCEPTED) {
            return [Ps::PHONE_STATUS_CHECKING, null];
        }

        if ($confirmPhoneSms->status == UserSms::STATUS_CONFIRMED) {
            return [Ps::PHONE_STATUS_CHECKED, $confirmPhoneSms->gateway];
        }

        throw new Exception("Bad sms status on resolve ps phone status: {$confirmPhoneSms->status}");
    }
}