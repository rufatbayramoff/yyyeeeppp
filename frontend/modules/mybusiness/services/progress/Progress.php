<?php


namespace frontend\modules\mybusiness\services\progress;


use common\models\Company;
use frontend\modules\mybusiness\services\progress\steps\Step;

class Progress
{
    /**
     * @var array
     */
    private $steps;

    /**
     * Progress constructor.
     * @param Step[] $steps
     */
    public function __construct(array $steps)
    {
        $this->steps = $steps;
    }

    public function percent(?Company $company): ProgressValue
    {
        $percent = 0;
        $urls = [];
        foreach ($this->steps as $step) {
            if ($company && $step->check($company)) {
                $percent += $step->value();
            } else {
                $urls[] = $step->stepUrl();
            }
        }
        return new ProgressValue($percent, $urls);
    }

    /**
     * @param Company|null $company
     * @return array
     */
    public function checkCount(?Company $company): array
    {
        $result = [];
        foreach ($this->steps as $step) {
            $result[] = $company && $step->check($company);
        }
        return $result;
    }
}