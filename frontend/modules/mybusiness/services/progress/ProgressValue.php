<?php


namespace frontend\modules\mybusiness\services\progress;


class ProgressValue
{
    /**
     * @var int
     */
    private $percent;

    public const COLOR_RED = '#ff401a';
    public const COLOR_ORANGE = '#ff8000';
    public const COLOR_YELLOW = '#ffbf00';
    public const COLOR_GREEN = '#54cc29';
    /**
     * @var array
     */
    private $steps;

    public function __construct(int $percent, array $steps)
    {
        $this->percent = min($percent, 100);
        $this->steps = $steps;
    }

    public function percent(): int
    {
        return $this->percent;
    }

    public function color(): string
    {
        $color = self::COLOR_GREEN;
        if ($this->percent < 80) {
            $color = self::COLOR_YELLOW;
        }
        if ($this->percent < 50) {
            $color = self::COLOR_ORANGE;
        }
        if ($this->percent < 30) {
            $color = self::COLOR_RED;
        }
        return $color;
    }

    public function needFill(): bool
    {
        return count($this->steps);
    }

    public function steps(): array
    {
        return $this->steps;
    }
}