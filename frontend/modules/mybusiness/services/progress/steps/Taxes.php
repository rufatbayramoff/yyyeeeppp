<?php


namespace frontend\modules\mybusiness\services\progress\steps;


use common\models\Company;
use frontend\modules\mybusiness\services\progress\StepUrl;

class Taxes implements Step
{

    public function check(Company $company): bool
    {
        $tax = $company->user->userTaxInfo;
        if(!$tax) {
            return false;
        }
        return $tax->isSubmitted();
    }

    public function value(): int
    {
        return 10;
    }

    public function stepUrl(): StepUrl
    {
        return new StepUrl(_t('site.ps', 'Taxes'),'/mybusiness/taxes/step1');
    }
}