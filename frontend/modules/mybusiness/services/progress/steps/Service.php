<?php


namespace frontend\modules\mybusiness\services\progress\steps;


use common\models\Company;
use frontend\modules\mybusiness\services\progress\StepUrl;

class Service implements Step
{

    public function check(Company $company): bool
    {
        return
            $company->getCompanyServices()->exists() ||
            $company->getPsPrinters()->exists() ||
            $company->getProducts()->exists() ||
            $company->getModel3ds()->exists();
    }

    public function value(): int
    {
       return 50;
    }

    public function stepUrl(): StepUrl
    {
        return new StepUrl(_t('site.ps', 'Services and Products'),'/mybusiness/services');;
    }
}