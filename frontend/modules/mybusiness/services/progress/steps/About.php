<?php


namespace frontend\modules\mybusiness\services\progress\steps;


use common\models\Company;
use frontend\modules\mybusiness\services\progress\StepUrl;

class About implements Step
{

    /**
     * @var Company|null
     */
    private $company;

    public function __construct(?Company $company)
    {
        $this->company = $company;
    }

    public function check(Company $company): bool
    {
        return $company->title && $company->description && $company->location;
    }

    public function value(): int
    {
        return 10;
    }

    public function stepUrl(): StepUrl
    {
        if($this->company) {
            return new StepUrl(_t('site.ps', 'About'),'/mybusiness/company/edit-ps');
        }
        return new StepUrl(_t('site.ps', 'About'),'/mybusiness/company/create-ps');
    }
}