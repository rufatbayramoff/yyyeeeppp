<?php


namespace frontend\modules\mybusiness\services\progress\steps;


use common\models\Company;
use frontend\modules\mybusiness\services\progress\StepUrl;

interface Step
{
    public function check(Company $company): bool;

    public function value(): int;

    /**
     * @return StepUrl
     */
    public function stepUrl(): StepUrl;
}