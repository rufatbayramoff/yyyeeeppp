<?php


namespace frontend\modules\mybusiness\services\progress\steps;


use common\models\Company;
use frontend\modules\mybusiness\services\progress\StepUrl;

class Phone implements Step
{

    public function check(Company $company): bool
    {
        return $company->getIsPhoneVerified();
    }

    public function value(): int
    {
       return 10;
    }

    public function stepUrl(): StepUrl
    {
        return new StepUrl(_t('site.ps', 'Phone number'),'/mybusiness/settings');
    }
}