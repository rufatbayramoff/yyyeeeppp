<?php


namespace frontend\modules\mybusiness\services\progress\steps;


use common\models\Company;
use frontend\modules\mybusiness\services\progress\StepUrl;
use yii\helpers\Json;

class Portfolio implements Step
{

    public function check(Company $company): bool
    {
        $pictureFileIds = $company->picture_file_ids;
        if(!$pictureFileIds) {
            return false;
        }
        $fileIds = Json::decode($pictureFileIds);
        return $company->logo && !empty($fileIds);
    }

    public function value(): int
    {
        return 20;
    }

    public function stepUrl(): StepUrl
    {
        return new StepUrl(_t('site.ps', 'Portfolio'),'/mybusiness/company/profile');
    }
}