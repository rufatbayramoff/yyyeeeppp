<?php


namespace frontend\modules\mybusiness\services\progress;


class StepUrl
{
    public $name;
    public $url;

    public function __construct(string $name, string $url)
    {
        $this->name = $name;
        $this->url = $url;
    }
}