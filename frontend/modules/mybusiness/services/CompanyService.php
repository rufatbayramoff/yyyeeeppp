<?php

namespace frontend\modules\mybusiness\services;


use common\components\ArrayHelper;
use common\components\Emailer;
use common\components\exceptions\ValidationException;
use common\components\FileDirHelper;
use common\models\Company;
use common\models\factories\CompanyFactory;
use common\models\factories\UserLocationFactory;
use common\models\PaymentAccount;
use common\models\populators\CompanyPopulator;
use common\models\populators\UserPopulator;
use common\models\repositories\CompanyRepository;
use common\models\repositories\FileRepository;
use common\models\repositories\UserRepository;
use common\models\User;
use common\modules\payment\services\PaymentAccountService;
use common\services\PsPrinterService;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\models\AffiliateCompanyForm;
use frontend\modules\mybusiness\models\CompanyDto;
use frontend\modules\mybusiness\models\CompanyForm;
use frontend\modules\mybusiness\models\CompanyProfileForm;
use frontend\modules\mybusiness\models\CompanySettingForm;
use frontend\modules\mybusiness\repositories\UserLocationRepository;
use lib\money\Currency;
use Yii;
use yii\base\Exception;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

class CompanyService
{

    /**
     * @var CompanyRepository
     */
    protected $companyRepository;
    /**
     * @var UserLocationRepository
     */
    protected $userLocationRepository;
    /**
     * @var CompanyGenerateUrlManager
     */
    protected $companyGenerateUrlManager;
    /**
     * @var UserRepository
     */
    protected $userRepository;
    /**
     * @var PhoneManager
     */
    protected $phoneManager;
    /**
     * @var ImageManager
     */
    protected $photoManager;
    /**
     * @var FileRepository
     */
    protected $fileRepository;
    /**
     * @var CompanyFactory
     */
    protected $companyFactory;
    /**
     * @var UserLocationFactory
     */
    protected $locationFactory;
    /**
     * @var CompanyPopulator
     */
    protected $companyPopulator;
    /**
     * @var UserPopulator
     */
    protected $userPopulator;

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /** @var Emailer */
    protected $emailer;

    /**
     * @param CompanyRepository $companyRepository
     * @param UserLocationRepository $userLocationRepository
     * @param CompanyGenerateUrlManager $companyGenerateUrlManager
     * @param UserRepository $userRepository
     * @param FileRepository $fileRepository
     * @param PhoneManager $phoneManager
     * @param ImageManager $photoManager
     * @param CompanyFactory $companyFactory
     * @param UserLocationFactory $locationFactory
     * @param CompanyPopulator $companyPopulator
     * @param UserPopulator $userPopulator
     */
    public function injectDependencies(
        CompanyRepository $companyRepository,
        UserLocationRepository $userLocationRepository,
        CompanyGenerateUrlManager $companyGenerateUrlManager,
        UserRepository $userRepository,
        FileRepository $fileRepository,
        PhoneManager $phoneManager,
        ImageManager $photoManager,
        CompanyFactory $companyFactory,
        UserLocationFactory $locationFactory,
        CompanyPopulator $companyPopulator,
        UserPopulator $userPopulator,
        PaymentAccountService $paymentAccountService,
        Emailer $emailer
    ) {
        $this->companyRepository = $companyRepository;
        $this->userLocationRepository = $userLocationRepository;
        $this->companyGenerateUrlManager = $companyGenerateUrlManager;
        $this->userRepository = $userRepository;
        $this->phoneManager = $phoneManager;
        $this->photoManager = $photoManager;
        $this->fileRepository = $fileRepository;
        $this->companyFactory = $companyFactory;
        $this->locationFactory = $locationFactory;
        $this->companyPopulator = $companyPopulator;
        $this->userPopulator = $userPopulator;
        $this->paymentAccountService = $paymentAccountService;
        $this->emailer = $emailer;
    }

    /**
     * @param CompanyDto $dto
     * @param User $user
     * @return Company
     * @throws ValidationException
     * @throws Exception
     */
    public function createForUser(CompanyDto $dto, User $user): Company
    {
        $location = $this->locationFactory->createForCompany($user, $dto->location);
        $location->validateOrException();
        $company = $this->companyFactory->create(
            $dto->title,
            $dto->description,
            $user,
            $location,
            $this->companyGenerateUrlManager->generate($dto->title)
        );
        $company->validateOrException();
        $this->userLocationRepository->save($location);
        $this->companyRepository->saveCompany($company);
        $user->finishCompanyCreate();
        $this->userRepository->save($user);
        return $company;
    }

    /**
     * @param CompanyForm $form
     * @param User $user
     */
    public function editForUser(CompanyForm $form, User $user): void
    {
        $company = $this->companyRepository->get($user->company->id);
        $newLocation = $this->locationFactory->createForCompany($user, $form->getAddress());
        $newLocation->validateOrException();
        $this->companyPopulator->loadAttributes($company, [
            'title'       => $form->title,
            'description' => $form->description
        ]);
        if ($company->moderator_status === Company::MSTATUS_REJECTED) {
            $company->moderator_status = Company::MSTATUS_CHECKING;
        }
        $this->companyPopulator->loadLocation($company, $newLocation);
        $this->companyRepository->saveCompany($company);

    }

    public function editSettingForUser(CompanySettingForm $form, User $user)
    {
        $company = $this->companyRepository->get($user->company->id);
        $phoneStatuses = $this->phoneManager->status($user->id, $form->phoneNumber());
        $this->companyPopulator->loadSettings($company, $form, $phoneStatuses);
        $this->userPopulator->loadQuote($user, $form);
        $this->userRepository->save($user);
        $this->companyRepository->saveCompany($company);
        PsPrinterService::updatePrintersCache();
    }

    /**
     * @param CompanyProfileForm $form
     * @param User $user
     * @throws NotFoundHttpException
     */
    public function editProfileForUser(CompanyProfileForm $form, User $user): void
    {
        $company = $this->companyRepository->get($user->company->id);
        $this->deleteOldPictures($company, $form);
        $coverId = $this->photoManager->saveCover($form->coverFile);
        $this->companyPopulator->loadProfile($company, $form, $coverId, $this->mergeFiles($form));
        $this->companyPopulator->loadLogo($company, $form, $this->getFolderPath($user->id), $this->photoManager);
        $this->companyRepository->saveCompany($company);
    }

    protected function deleteOldPictures(Company $company, CompanyProfileForm $form)
    {
        if ($company->picture_file_ids) {
            $old = Json::decode($company->picture_file_ids);
            $new = ArrayHelper::getColumn($form->pictures, 'id');
            $this->fileRepository->deleteFileByIds(array_diff($old, $new));
        }
    }

    protected function mergeFiles(CompanyProfileForm $form): array
    {
        $ids = [];
        foreach ($form->pictures as $picture) {
            $file = $this->fileRepository->one($picture['id'] ?? -1);
            if (isset($picture['fileOptions']['rotate'])) {
                $this->photoManager->rotate($file, (int)$picture['fileOptions']['rotate']);
            }
            $ids[] = $file->id;
        }
        foreach ($form->uploadPictures as $index => $uploadFile) {
            $file = $this->photoManager->saveFile($uploadFile);
            if (isset($form->optionFileUploads[$index]['fileOptions']['rotate'])) {
                $this->photoManager->rotate($file, (int)$form->optionFileUploads[$index]['fileOptions']['rotate']);
            }
            $ids[] = $file->id;
        }
        return $ids;
    }

    protected function getFolderPath($userId)
    {
        $userFolder = UserFacade::getUserFolder($userId);
        $finalFolderPath = Yii::getAlias('@static/') . $userFolder;
        FileDirHelper::createDir($finalFolderPath);
        return $finalFolderPath;
    }

    public function newAffiliateCompany(AffiliateCompanyForm $form, User $user): void
    {
        $company = $this->createForUser($form->getDto(), $user);
        $company->area_of_activity = Company::AFFILIATE_BUSINESS;
        $company->safeSave();
        $msg = "Name: {$form->fullName} company_id: {$company->id} with type:".implode(',', $form->type);
        $this->emailer->sendMessage('r.bayramov@treatstock.com','New affiliate', $msg, $msg);
    }
}