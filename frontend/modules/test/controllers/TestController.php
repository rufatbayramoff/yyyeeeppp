<?php

namespace frontend\modules\test\controllers;

use common\components\BaseController;
use common\components\DateHelper;
use common\models\CompanyService;
use common\modules\message\services\CheckDeliveryService;
use common\modules\payment\gateways\vendors\BraintreeGateway;
use frontend\modules\test\services\AutoTestService;
use lib\money\Currency;
use lib\money\Money;
use Yii;

/**
 * Class TestController
 * @package frontend\modules\test\controllers
 */
class TestController extends BaseController
{
    /** @var $autoTestService AutoTestService */
    protected $autoTestService;

    /**
     * @param AutoTestService $autoTestService
     */
    public function injectDependencies(AutoTestService $autoTestService)
    {
        $this->autoTestService = $autoTestService;
    }

    public function actionTestBraintreeNoSec()
    {
        $braintreeGateway = Yii::createObject(BraintreeGateway::class);
        $token            = $braintreeGateway->generatePaymentToken('', '', '', '', Money::create(1.51, Currency::EUR));
        return $this->renderPartial('testBraintree', ['token' => $token]);
    }

    public function actionTestBraintreeSec1()
    {
        $braintreeGateway = Yii::createObject(BraintreeGateway::class);
        $token            = $braintreeGateway->generatePaymentToken('', '', '', '', Money::create(1.51, Currency::EUR));
        return $this->renderPartial('testBraintreeV1', ['token' => $token]);
    }

    public function actionTestBraintreeSec2()
    {
        $braintreeGateway = Yii::createObject(BraintreeGateway::class);
        $token            = $braintreeGateway->generatePaymentToken('', '', '', '', Money::create(1.51, Currency::EUR));
        return $this->renderPartial('testBraintreeV2', ['token' => $token]);
    }

    public function actionTestBraintreeFinish()
    {
        $token = Yii::$app->request->post('payment_method_nonce');
        $braintreeGateway = Yii::createObject(BraintreeGateway::class);
        $result = $braintreeGateway->authorize(Money::create(1.51, Currency::EUR), $token);
        echo "Success: ".$result->isSuccess()."<br>";
        echo "Message: ".$result->getMessage()."<br>";
        echo "Transaction: ".$result?->getTransaction()?->transactionId.' - '.$result?->getTransaction()?->status."<br>";
    }

    /**
     * Start before auto tests
     *
     */
    public function actionInitAutoTest()
    {
        if (YII_ENV !== 'dev' && YII_ENV !== 'jenkins') {
            echo "Invalid YII_ENV";
            return false;
        }
        $this->autoTestService->initAutoTests();
        return "Done";
    }

    /**
     * Get log errors
     */
    public function actionGetLogsReports()
    {
        if (YII_ENV !== 'dev' && YII_ENV !== 'jenkins') {
            echo "Invalid YII_ENV";
            return false;
        }
        return $this->jsonReturn($this->autoTestService->getLogs());
    }

}