<?php
namespace frontend\modules\test;

use yii\base\Module;

class TestModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'frontend\modules\test\controllers';
}
