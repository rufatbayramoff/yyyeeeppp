<?php
namespace frontend\modules\test\actions;

use common\models\BlogPost;
use common\models\repositories\PrinterMaterialRepository;
use common\modules\catalogPs\repositories\PrinterTechnologyRepository;
use common\modules\catalogPs\repositories\UsageGroupsRepository;
use common\modules\homePage\repositories\HomePageProductRepository;
use frontend\components\UserSessionFacade;
use Yii;
use yii\base\Action;

class SiteIndexAction extends Action
{
    public function run()
    {
        $materialRepo         = Yii::createObject(PrinterMaterialRepository::class);
        $technologyRepository = Yii::createObject(PrinterTechnologyRepository::class);
        $usageRepo            = Yii::createObject(UsageGroupsRepository::class);

        $catIds = \Yii::$app->setting->get('mainpage.showcategories', '1,2,3');

        $featuredLocations = \Yii::$app->setting->get(
            'settings.psCatalogFeaturedLocations',
            [
                "Los Angeles, CA, US",
                "San Francisco, CA, US",
                "New York, NY, US",
                "Philadelphia, PA, US",
                "London, UK",
                "Sydney, AU",
                "Boston, MA, US",
                "Detroit, MI, US",
                "Vancouver, BC, CA ",
                "Rome, IT",
                "Paris, FR",
                "Chicago, IL, US",
                "Newark, NJ, US",
                "Beijing, CN",
                "Amsterdam, NL",
                "Atlanta, GA, US",
                "San Diego, US",
                "Portland, OR, US"
            ]
        );

        $materialsCodes    = \Yii::$app->setting->get('settings.psCatalogMaterials', ["PLA", "ABS", "Nylon", "Resin", "Rubber (TPU)"]);
        $technologiesCodes = \Yii::$app->setting->get('settings.psCatalogTechnologies', ["FDM", "CJP", "MJM", "SLA", "SLS"]);
        $materials         = $technologies = [];

        foreach ($materialsCodes as $materialCode) {
            $materials[] = $materialRepo->getByCode($materialCode);
        }
        $materials = array_filter($materials);
        foreach ($technologiesCodes as $techCode) {
            $technologies[] = $technologyRepository->getByCode($techCode);
        }
        $technologies = array_filter($technologies);

        $usageTypes = [];
        foreach ($usageRepo->getAll() as $usageType):
            $usageTypes[$usageType['id']] = $usageType['title'];
        endforeach;
        $locationString = UserSessionFacade::getLocationAsString();

        $productCategoriesBlock = HomePageProductRepository::getActiveCategories();
        $productCategoryCard    = HomePageProductRepository::getActiveCardsCategories();
        $featuredCategories     = HomePageProductRepository::getActiveFeaturedCategories();
        $blogPosts              = BlogPost::getLatest(3);

        return $this->controller->render(
            'index',
            [
                //'featuredLocations'      => array_splice($featuredLocations, 0, 7),
                //'materials'              => $materials,
                //'technologies'           => $technologies,
                //'usageTypes'             => $usageTypes,
                //'productCategoriesBlock' => $productCategoriesBlock,
                //'productCategoryCard'    => $productCategoryCard,
                //'featuredCategories'     => $featuredCategories,
                //'blogPosts'              => $blogPosts
            ]
        );
    }
}