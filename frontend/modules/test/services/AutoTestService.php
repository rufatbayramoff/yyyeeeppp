<?php

namespace frontend\modules\test\services;

use common\components\FileDirHelper;
use yii\helpers\FileHelper;

class AutoTestService extends \yii\base\BaseObject
{
    public function clearLogs()
    {
        FileDirHelper::clearDirectory(\Yii::getAlias('@frontend') . '/runtime');
        FileDirHelper::clearDirectory(\Yii::getAlias('@backend') . '/runtime');
        FileDirHelper::clearDirectory(\Yii::getAlias('@common') . '/runtime');
        FileDirHelper::clearDirectory(\Yii::getAlias('@console') . '/runtime');
        FileDirHelper::clearDirectory(\Yii::getAlias('@frontend') . '/web/assets');
    }


    public function initAutoTests()
    {
        $this->clearLogs();
    }

    public function getLogs()
    {
        $logDirs = [
            \Yii::getAlias('@frontend') . '/runtime/logs',
            \Yii::getAlias('@backend') . '/runtime/logs',
            \Yii::getAlias('@common') . '/runtime/logs',
            \Yii::getAlias('@console') . '/runtime/logs',
        ];
        $logs    = [];
        foreach ($logDirs as $logDir) {
            $files = glob($logDir . '/*');
            foreach ($files as $filePath) {
                $logs[$filePath] = file_get_contents($filePath);
            }
        }
        return $logs;
    }
}