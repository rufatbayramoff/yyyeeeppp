<?php

use yii\bootstrap\Html;

?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
</head>
<body>
Test Braintree Without 3d Sec 1.51 EUR<br>
View developer console for debug<br>
<form id="checkout" method="post" action="/test/test/test-braintree-finish">
    <input type="hidden" name="payment_method_nonce" id="payment_method_nonce">
    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

    <div id="payment-form-3ds" style="max-width: 500px"></div>
    <div id="payment-form"></div>
    <input type="submit">
</form>
<br>
<pre id="resultPayload"></pre>

<script src="https://js.braintreegateway.com/web/dropin/1.27.0/js/dropin.min.js"></script>
<script>
    debugger;
    let braintreeConfig = {
        authorization: '<?=$token?>',
        selector: '#payment-form-3ds',
        threeDSecure: {
            amount: 1.51,
            currency: 'eur'
        }
    };

    braintree.dropin.create(braintreeConfig, function (createErr, instance) {
        if (createErr) {
            console.log('Create Error: ' + JSON.stringify(createErr));
            return;
        }
        let form = document.querySelector('#checkout');
        form.addEventListener('submit', function (event) {
            event.preventDefault();
            instance.requestPaymentMethod(function (err, payload) {
                console.log(err);
                console.log(payload);
                debugger;
                if (!payload.liabilityShifted) {
                    document.querySelector('#resultPayload').innerHTML='3d secure failed';
                    return ;
                }
                if (payload.nonce) {
                    document.querySelector('#payment_method_nonce').value = payload.nonce;
                    form.submit();
                }
            });
        });
    });
</script>
</body>
</html>