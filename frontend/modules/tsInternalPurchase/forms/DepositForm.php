<?php


namespace frontend\modules\tsInternalPurchase\forms;


use common\components\exceptions\ValidationException;
use common\models\User;
use common\modules\payment\services\InvoiceBankService;
use lib\money\Currency;
use lib\money\Money;
use yii\base\Model;

class DepositForm extends Model
{
    public int $userId=0;
    public $balance = '';
    public $currency = Currency::USD;

    public function rules(): array
    {
        return [
            ['userId', 'integer'],
            ['balance', 'required'],
            ['balance', 'double'],
            ['currency', 'required'],
            ['currency', 'string'],
            ['balance', 'compare', 'compareValue' => InvoiceBankService::INVOICE_FROM_USD, 'operator' => '>=', 'type' => 'number'],
        ];
    }

    /**
     * @return Money
     */
    public function money(): Money
    {
        return Money::create($this->balance, $this->currency);
    }

    public function validateOrException()
    {
        if (!$this->validate()) {
            throw new ValidationException($this);
        }
    }

    public function validateUserOrException()
    {
        if ($this->userId<1000) {
            $this->addError('userId', 'Invalid User');
            throw new ValidationException($this);
        }
    }

    public function getUser(): User
    {
        return User::tryFindByPk($this->userId);
    }
}