<?php
namespace frontend\modules\tsInternalPurchase\controllers;


use common\modules\payment\components\PaymentUrlHelper;
use common\modules\tsInternalDeposit\services\TsInternalDepositService;
use frontend\components\AuthedController;
use frontend\models\user\UserFacade;
use frontend\modules\tsInternalPurchase\forms\DepositForm;

class DepositController extends AuthedController
{
    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => \yii\filters\VerbFilter::class,
                'actions' => [
                    'checkout' => ['post'],
                ]
            ],

        ];
    }

    public function actionCheckout(TsInternalDepositService $service)
    {
        $form = new DepositForm();
        $form->load(\Yii::$app->request->post(), '');
        $form->validateOrException();
        $form->userId = $this->getCurrentUser()->id;
        $tsInternalPurchase = $service->createOrder($form);
        $this->redirect(PaymentUrlHelper::payInvoice($tsInternalPurchase->primaryInvoice));

    }
}