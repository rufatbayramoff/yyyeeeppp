<?php

namespace frontend\modules\tsInternalPurchase\controllers;

use common\models\CompanyService;
use common\modules\payment\components\PaymentUrlHelper;
use common\modules\tsInternalPurchase\services\TsInternalPurchaseService;
use frontend\models\user\UserFacade;
use yii\base\InvalidArgumentException;
use yii\base\UserException;

class CertificationController extends \common\components\BaseController
{

    /** @var TsInternalPurchaseService */
    public $tsInternalPurchaseService;

    public function injectDependencies(TsInternalPurchaseService $tsInternalPurchaseService)
    {
        $this->tsInternalPurchaseService = $tsInternalPurchaseService;
    }


    public function actionCheckout()
    {
        if (!\Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $companyService = CompanyService::tryFindByPk(\Yii::$app->request->post('companyServiceId'));
        if (!UserFacade::isObjectOwner($companyService->company)) {
            throw new UserException('Not owner');
        }
        $tsInternalPurchase = $this->tsInternalPurchaseService->createOrder($companyService);
        $this->redirect(PaymentUrlHelper::payInvoice($tsInternalPurchase->primaryInvoice));
    }
}