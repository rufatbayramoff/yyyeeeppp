<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 14:46
 */
/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider|\yii\data\ArrayDataProvider */

use yii\widgets\Pjax;

$dataProvider->id = null;
$listView = Yii::createObject([
    'class'        => \yii\widgets\ListView::class,
    'dataProvider' => $dataProvider,
    'itemView'     => 'productListElement',
]);
?>
<div class="row">
    <?php if ($dataProvider->count) {
        echo $listView->renderItems();
    } else { ?>
        <div class="product-list-empty">
            <?= _t('site.store', 'No products found.') ?>
        </div>
        <?php
    }
    ?>
</div>
<?php
echo $listView->renderPager();
?>

