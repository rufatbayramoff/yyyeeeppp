<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.04.18
 * Time: 10:34
 */

use common\components\ArrayHelper;
use common\models\Product;

/** @var $product Product */

$productSerialized = \common\modules\product\serializers\CompanyBlockSerializer::serialize($product);
$productCartSerialized = $productCart->asArray();
$user = \frontend\models\user\UserFacade::getCurrentUser();
Yii::$app->angular
    ->controller([
        'product/cart',
        'ps/PsCatalogController',
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service',
        'product/productForm',
        'product/productModels',
    ])
    ->service(['notify', 'modal', 'router', 'user'])
    ->directive('dropzone-button')
    ->controllerParam('products', [$product->uuid => $productSerialized])
    ->controllerParam('productCart', $productCartSerialized);

$this->registerAssetBundle(\frontend\assets\DropzoneAsset::class);
echo $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-modal.php');

?>
<?=$this->render('navigationTabs');?>
<div ng-controller="ProductCartController" ng-cloak>
    <?php
    /**
     * Created by PhpStorm.
     * User: analitic
     * Date: 18.04.18
     * Time: 15:59
     */

    /** @var $product \common\models\Product */
    ?>
    <div class="container">
        <div class="designer-card">
            <div class="table-responsive">
                <table class="table product-cart__grid">
                    <tr>
                        <th class="product-cart__grid-pic"></th>
                        <th><?= _t('site.store', 'Description') ?></th>
                        <th><?= _t('site.store', 'Quantity') ?></th>
                        <th><?= _t('site.store', 'Price') ?></th>
                        <th><?= _t('site.store', 'Subtotal'); ?></th>
                    </tr>
                    <tr>
                        <td class="product-cart__grid-pic">
                            <a href="<?=$product->getPublicPageUrl();?>" target="_blank">
                            <?php
                            if ($product->getCoverUrl()) {
                                echo '<img src="' . \frontend\components\image\ImageHtmlHelper::getThumbUrl($product->getCoverUrl(), 100, 100) . '" alt="' . H($product->getTitle()) . '" class="product-cart__cover">';
                            }
                            ?></a>
                        </td>
                        <td>
                            <h4 class="m-t0">
                                <?= H($product->getTitle()) ?>
                            </h4>
                        </td>
                        <td>
                            <input class="form-control input-sm product-cart__grid-qty" type="number" name="productCart[items][<?= $product->uuid ?>]" ng-model="productCart.items['<?= $product->uuid ?>'].qty" ng-change="validateProductQty('<?= $product->uuid ?>')" />
                        </td>
                        <td>
                            ${{productCart.items['<?= $product->uuid ?>'].price}}
                        </td>
                        <td>
                            ${{productCart.items['<?= $product->uuid ?>'].priceTotal}}
                        </td>
                    </tr>
                </table>
            </div>

            <div class="m-b10">

                <button class="btn btn-danger" ng-click="goToDelivery()" ng-disabled="invalidNext" >
                    <span class="tsi tsi-shopping-cart m-r10"></span>
                    <?= _t('front.store', ' Next: delivery '); ?>
                    <span class="tsi tsi-right m-l20" style="margin-right: -10px;"></span>
                </button>
                <span>
                    <span ng-controller="PsCatalogController">
                        <button class="btn btn-danger btn-ghost"
                                loader-click="openCreatePreorder(<?= $product->productCommon->company_id; ?>, {name: '<?= $user ? H($user->getFullNameOrUsername()) : ''; ?>'}, '<?= $product->uuid ?>')">
                            <?= _t('site.ps', 'Get a Quote'); ?>
                        </button>
                    </span>
                </span>
            </div>
        </div>

    </div>
</div>

