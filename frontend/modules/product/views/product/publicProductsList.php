<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 14:46
 */

use common\modules\product\models\ProductSearchForm;
use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;

/* @var $this \yii\web\View */
/* @var $searchModel ProductSearchForm */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $productCategory array|\common\models\ProductCategory|null|\yii\db\ActiveRecord */

$listView                                  = Yii::createObject([
    'class'        => \yii\widgets\ListView::class,
    'dataProvider' => $dataProvider,
    'itemView'     => 'productListElement',
]);
$seo                                       = $this->context->seo;
$defaultTitle                              = _t('site.store', 'Products');
$productCategory                           = $searchModel->category;
$breadcrumps                               = [];
$breadcrumps[_t('site.store', 'Products')] = ProductUrlHelper::productsMain();
if ($productCategory) {
    $currentCategory = $productCategory;
    do {
        $categories[]    = $currentCategory;
        $currentCategory = $currentCategory->parent;
    } while ($currentCategory);
    $categories   = array_reverse($categories);
    $defaultTitle = $productCategory->title;

    foreach ($categories as $category) {
        if (empty($category->parent_id)) {
            continue;
        }
        $code = rtrim($category->code, '-');
        $url  = '/products/' . H($code);
        if ($searchModel->type) {
            $url .= '?type=' . H($searchModel->type);
        }
        $breadcrumps[$category->title] = $url;
    }
}

?>

<div class="store-filter__container">
    <div class="container container--wide">
        <ol class="breadcrumb m-t10 m-b0">
            <?php
            foreach ($breadcrumps as $breadcrumpText => $breadcrumpUrl) {
                echo '<li>';
                echo $breadcrumpUrl ? '<a href="' . HL($breadcrumpUrl) . '">' : '';
                echo H($breadcrumpText);
                echo $breadcrumpUrl ? '</a>' : '';
                echo '</li>';
            }
            ?>
        </ol>
        <?php
        echo \frontend\widgets\BreadcrumbsSchemaWidget::widget([
            'breadcrumpsItems' => $breadcrumps
        ]);
        ?>

        <h1 class="store-filter__categories-title ">
            <?= H($seo->header ?? $defaultTitle); ?>
            <?php if ($searchModel->type === \common\modules\product\interfaces\ProductInterface::TYPE_MODEL3D) {
                echo ' \ ' . _t('site.store', '3D Models');
            } ?>
        </h1>

        <div>
            <?= $seo->header_text ?? '' ?>
        </div>
    </div>

</div>

<div class="container container--wide">
    <div class="store-layout">
        <div class="store-layout__sidebar">
            <?= $this->render('searchPanel', ['searchModel' => $searchModel]); ?>
        </div>
        <div class="store-layout__content">
            <div class="relative">
                <div id="productsList">
                    <?= $this->render('publicProductsListContainer', ['dataProvider' => $dataProvider]); ?>
                </div>
                <div class="hidden" id="loadingProgress">
                    <div style="width: 100%;  height: 100%;  position: absolute;  top: 20px; left: 0;z-index: 10;background-color:white;opacity:0.4">
                    </div>
                    <div class="preloader" style="width: 100%;  height: 100%;  position: absolute;  top: 20px; left: 0;z-index: 15;">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


