<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.04.18
 * Time: 11:16
 */

use common\modules\product\models\ProductPlaceOrderState;

$placeOrderState = \Yii::createObject(ProductPlaceOrderState::class);

$currentStep = 'cart';
if ($_SERVER['REQUEST_URI']=='/product/buy-delivery') {
    $currentStep = 'delivery';
}
if ($_SERVER['REQUEST_URI']=='/product/buy-checkout') {
    $currentStep = 'payment';
}

$product = $placeOrderState->getProduct();
$productCart = $placeOrderState->getProductCart();
$deliveryAllowed = array_key_exists($product->uuid, $productCart->items);
$productOrder = $placeOrderState->getOrder();
$paymentAllowed = $productOrder ? true : false;


$cartStepUrl = '/product/buy/' . $product->uuid;
$deliveryStepUrl = '/product/buy-delivery';
$paymentStepUrl = '/product/buy-checkout';

?>
<div class="container">
    <nav class="product-cart__nav">
        <div class="product-cart__nav-step <?= $currentStep == 'cart' ? 'product-cart__nav-step--selected' : '' ?>">
            <a href="<?= $cartStepUrl ?>"><span class="product-cart__nav-count">1</span><?= _t('site.store', 'Cart') ?></a>
        </div>
        <div class="product-cart__nav-step <?= $currentStep == 'delivery' ? 'product-cart__nav-step--selected' : '' ?>">
            <?php if ($deliveryAllowed) {
                ?>
                <a href="<?= $deliveryStepUrl ?>"><span class="product-cart__nav-count">2</span><?= _t('site.store', 'Delivery') ?></a>
                <?php
            } else {
                ?>
                <span class="product-cart__nav-count">2</span><?= _t('site.store', 'Delivery') ?>
                <?php
            }
            ?>
        </div>
        <div class="product-cart__nav-step <?= $currentStep == 'payment' ? 'product-cart__nav-step--selected' : '' ?>">
            <?php if ($paymentAllowed) {
                ?>
                <a href="<?= $paymentStepUrl ?>"><span class="product-cart__nav-count">3</span><?= _t('site.store', 'Payment') ?></a>
                <?php
            } else {
                ?>
                <span class="product-cart__nav-count">3</span><?= _t('site.store', 'Payment'); ?>
                <?php
            }
            ?>
        </div>
    </nav>
</div>
