<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.04.18
 * Time: 17:52
 */

use common\components\ArrayHelper;
use common\models\GeoCountry;
use common\models\Product;
use lib\geo\GeoNames;

/** @var $product Product */

$productSerialized = \common\modules\product\serializers\CompanyBlockSerializer::serialize($product);


Yii::$app->angular
    ->service(
        [
            'maps',
            'geo',
            'router',
            'notify',
            'user',
            'measure',
            'modal'
        ]
    )
    ->controller(
        [
            'store/filepage/models',
            'store/common-models',
            'store/filepage/printer-models',
            'store/filepage/printer-directives',
        ]
    )
    ->directive(['google-map', 'google-address-input'])
    ->controller('product/delivery')
    ->controllerParam('products', [$product->uuid => $productSerialized])
    ->controllerParam('deliveryForm', $deliveryForm)
    ->constants(
        [
            'globalConfig' =>
                [
                    'staticUrl' => param('staticUrl')
                ],
            'countries'    => ArrayHelper::toArray(
                GeoNames::getAllCountries(),
                [GeoCountry::class => ['id', 'iso_code', 'title', 'is_easypost_domestic', 'is_easypost_intl']]
            ),
        ]
    );

$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);
?>
<?=$this->render('navigationTabs');?>
<div ng-controller="ProductDeliveryController">
    <div class="container">
        <div class="designer-card product-cart__delivery">
            <div class="row">
                <?= $this->render('deliveryCarrier', compact('deliveryForm')); ?>

                <div class="col-sm-12 p-l0 p-r0">
                    <hr>
                </div>

                <div class="col-sm-12 text-right">
                    <button class="btn btn-primary m-b10" ng-click="goToPayment()">
                        <?= _t('site.common', 'Next'); ?>
                        <span class="tsi tsi-right"></span>
                    </button>
                </div>

            </div>
        </div>

    </div>
</div>

