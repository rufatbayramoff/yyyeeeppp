<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 16:21
 */
?>

<div class="container">

    <div class="error-page__pic text-center">

        <h1><?=_t('site.store', 'Sorry, product not found');?></h1>
        <div class="error-page__error-code" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 50px;margin: 0 0 30px; line-height: 60px;">
            ¯\_(ツ)_/¯
        </div>
        <div class="error-page__error-info">
            <?=_t('site.store', 'This product has either been unpublished or deleted by the author.');?>
        </div>

    </div>

    <p class="text-center m-t30">
        <a class="btn btn-primary" href="/products"><?=_t('site.store', 'Find more products');?></a>
    </p>

</div>