<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.06.18
 * Time: 17:41
 */

use common\modules\dynamicField\services\DynamicFieldService;

/** @var \common\modules\dynamicField\models\DynamicFieldValue[] $dynamicFieldValues */

$dynamicFieldService = \Yii::createObject(DynamicFieldService::class);

foreach ($dynamicFieldValues as $dynamicFieldValue) {
    ?>
    <div>
        <?= $dynamicFieldValue->dynamicField->title ?>:
        <?= $dynamicFieldService->getElementRender($dynamicFieldValue)->render() ?>
        <div class="small">
            <?= $dynamicFieldValue->dynamicField->description ?>
        </div>
    </div>
    <br>
    <?php
}
?>

