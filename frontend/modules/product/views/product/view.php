<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 15:59
 */

use common\models\CompanyBlock;
use common\models\Product;
use common\models\ProductFile;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserUtils;
use frontend\modules\product\widgets\ProductSchemaWidget;
use yii\helpers\Html;

/** @var $product Product */
/* @var $this \yii\web\View */
/* @var $seeAlsoProducts array */


Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->directive('dropzone-button')
    ->controllerParams(['z' => 'y'])
    ->controller([
        'ps/PsCatalogController',
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service',
        'product/productForm',
        'product/productModels',
    ]);
$this->registerAssetBundle(\frontend\assets\DropzoneAsset::class);
echo $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-modal.php');
$user            = \frontend\models\user\UserFacade::getCurrentUser();
$linkAuthorStore = UserUtils::getUserProductUrl($product->user);
$seo             = $this->context->seo;

if ($seo) {
    $this->title = $seo->title;
}
$this->title = $product->getTitle() . ' - Treatstock';

$categories      = [];
$currentCategory = $product->category;
do {
    $categories[]    = $currentCategory;
    $currentCategory = $currentCategory->parent;
} while ($currentCategory);
$categories = array_reverse($categories);

echo ProductSchemaWidget::widget([
    'product' => $product,
    'price'   => $product->getPriceMoneyByQty(1)
]);
$breadcrumps                               = [];
$breadcrumps[_t('site.store', 'Products')] = '/products';
foreach ($categories as $category) {
    if (empty($category->parent_id)) {
        continue;
    }
    $code                          = rtrim($category->code, '-');
    $url                           = '/products/' . H($code);
    $breadcrumps[$category->title] = $url;
}
$breadcrumps[$this->title] = \Yii::$app->request->url;
?>

<div class="store-filter__container m-b30">
    <div class="container container--wide">
            <ol class="breadcrumb m-t10 m-b0">
                <?php
                foreach ($breadcrumps as $breadcrumpText => $breadcrumpUrl) {
                    echo '<li>';
                    echo $breadcrumpUrl ? '<a href="' . HL($breadcrumpUrl) . '">' : '';
                    echo H($breadcrumpText);
                    echo $breadcrumpUrl ? '</a>' : '';
                    echo '</li>';
                }
                ?>
            </ol>
            <?php
            echo \frontend\widgets\BreadcrumbsSchemaWidget::widget([
                'breadcrumpsItems' => $breadcrumps
            ]);
            ?>

            <h1 class="store-filter__categories-title">
                <?= H($product->getTitle()) ?>
            </h1>
            <?= $seo->header_text ?? '' ?>
    </div>
</div>

<div class="container product-page" id="productBlockSection0">
    <div class="row">
        <div class="col-sm-6 col-md-7">
            <div class="designer-card p-t0 p-l0 p-r0 p-b0 m-b30">
                <div class="fotorama"
                     data-width="100%"
                     data-height="450px"
                     data-arrows="true"
                     data-click="true"
                     data-nav="thumbs"
                     data-thumbmargin="10"
                     data-thumbwidth="80px"
                     data-thumbheight="60px"
                     data-allowfullscreen="true">
                    <?php
                    if ($product->imageFiles) {
                        foreach ($product->imageFiles as $imageFile) {
                            echo '<img src="' . ImageHtmlHelper::getThumbUrlForFile($imageFile, ImageHtmlHelper::LARGE_WIDTH, ImageHtmlHelper::LARGE_WIDTH) . '" alt="' . H($imageFile->getFileName()) . '">';
                        }
                    } else {
                        ?>
                        <div class="product-page__empty-prod-img">
                            <?= _t('public.product', 'Images not uploaded'); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="model-tags">
                <?php
                foreach ($product->siteTags as $tag) {
                    echo sprintf('<a href="/products/all?tags[]=%s"><span class="label label-default">#%s</span></a>', HL($tag->text), H($tag->text));
                }
                ?>
            </div>
        </div>
        <div class="col-sm-6 col-md-5 wide-padding wide-padding--left">

            <div class="m-b20">
                <div class="model-author">
                    <div><?= _t('site.store', 'by') ?></div>
                    <div class="model-author__userpic">
                        <a href="<?= HL($product->company->getPublicProductsCompanyLink()); ?>">
                            <img
                                    src="<?= HL($product->company->getCompanyLogo(false, 20)) ?: '/static/images/defaultPrinter.png'; ?>"/>
                        </a>
                    </div>
                    <div class="model-author__username">
                        <a href="<?= HL($product->company->getPublicProductsCompanyLink()); ?>"><?php echo H($product->company->title); ?></a>
                    </div>
                </div>
            </div>

            <div class="designer-card product-page-info">


                <?php
                $properties = [
                    /*
                    _t("site.store", 'Price from') => ($product->single_price ?
                        app('formatter')->asCurrency($product->single_price, 'USD') : '') .  ' / ' . $product->getUnitTypeTitle(1),
                    */
                    _t('site.store', 'Min. order')                   => $product->getMinOrderQty() > 1 ? sprintf('%s %s', $product->getMinOrderQty(),
                        $product->getUnitTypeTitle(2)) : null,
                    _t("mybusiness.product", 'Ship from')            => $product->getShipFromLocation(true)->formatted_address,
                    _t("mybusiness.product", 'Supply ability')       => $product->supply_ability ? sprintf(
                        '%s %s / %s',
                        $product->supply_ability,
                        $product->getUnitTypeTitle($product->supply_ability),
                        $product->supply_ability_time
                    ) : '',
                    _t("mybusiness.product", 'Avg. production time') => $product->avg_production ? sprintf(
                        '%s  %s',
                        $product->avg_production,
                        $product->avg_production > 1 ? $product->avg_production_time . 's' : $product->avg_production_time
                    ) : '',
                    _t("mybusiness.product", 'Product packaging')    => $product->product_package,
                    _t("mybusiness.product", 'Shipping packaging')   => $product->outer_package,
                    _t("mybusiness.product", 'Incoterms')            => $product->incoterms,
                    _t("mybusiness.product", 'Payment')              => '',

                ];
                ?>
                <table class="form-table form-table--top-label">
                    <tr class="form-table__row">
                        <td class="form-table__label">
                            <b><?= _t("site.store", 'Product price'); ?></b>
                        </td>
                        <td class="form-table__data">
                            <?php
                            $moqPrices = $product->getMoqPricesFormatted();
                            if (empty($moqPrices)) {
                                echo displayAsCurrency($product->productCommon->single_price, $product->productCommon->company->currency);
                            }
                            foreach ($moqPrices as $price):
                                if ($price['qtyFrom'] == $price['qtyTo']):
                                    ?>
                                    <?= _t('site.store', "&#8805;  {qtyFrom} {unitType} at {price}/{unitCode}", [
                                    'qty'      => H($price['title']),
                                    'qtyFrom'  => H($price['qtyFrom']),
                                    'unitType' => H($price['unitType']),
                                    'price'    => H($price['priceAmount']),
                                    'unitCode' => H($price['unitCode'])
                                ]); ?>
                                <?php else: ?>
                                    <?= _t('site.store', "&#8805;  {qtyFrom} {unitType} at {price}/{unitCode}", [
                                        'qty'      => H($price['title']),
                                        'qtyFrom'  => H($price['qtyFrom']),
                                        'qtyTo'    => H($price['qtyTo']),
                                        'unitType' => H($price['unitType']),
                                        'price'    => H($price['priceAmount']),
                                        'unitCode' => H($price['unitCode'])
                                    ]); ?>
                                <?php endif; ?>
                                <br/>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <?php foreach ($properties as $k => $v):
                        if (empty($v)) {
                            continue;
                        }
                        ?>
                        <tr class="form-table__row">
                            <td class="form-table__label">
                                <b><?= H($k); ?></b>
                            </td>
                            <td class="form-table__data">
                                <?= H($v); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>

                <div class="m-b10" ng-controller="PsCatalogController">
                    <?php if (false): ?>
                        <a href="/product/buy/<?= $product->uuid ?>" class="btn btn-danger ts-ga">

                            <span class="tsi tsi-shopping-cart m-r10"></span>
                            <?= _t('front.store', ' Order now '); ?>
                            <span class="tsi tsi-right m-l20" style="margin-right: -10px;"></span>
                        </a>
                    <?php endif; ?>

                    <button class="btn btn-danger btn-ghost"
                            loader-click="openCreatePreorder(<?= $product->productCommon->company_id; ?>, {name: '<?= $user ? H($user->getFullNameOrUsername()) : ''; ?>'}, '<?= $product->uuid ?>')">
                        <?= _t('site.ps', 'Get a Quote'); ?>
                    </button>
                </div>
            </div>

            <div class="model-share__list m-t10 m-b10">

                <?php
                echo \frontend\widgets\SocialShareWidget::widget(['showHtmlcode' => false, 'showEmail' => false, 'imgUrl' => $product->getCoverUrl()]); ?>


            </div>

            <div class="m-t10 m-b10">
                <ul>
                    <?php
                    $productAttachments = $product->getAttachmentFiles();
                    foreach ($productAttachments as $attachment): ?>
                        <li><?= Html::a($attachment->name, $attachment->getFileUrl(), ['target' => '_blank']); ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="product-page__nav">
    <div class="product-page__nav-inner" id="jsprod-nav">
        <div class="container ">
            <div id="jsprod-nav-tabs">

            </div>
        </div>
    </div>
</div>
<div class="container ">
    <div class="row">
        <div class="col-sm-9 wide-padding wide-padding--right">
            <div class="product-page__section ugc-content productBlock0 m-t0">
                <?php
                $r = [];
                if (param('allowDynamicFields')) {
                    $r = $product->getDynamicValuesAsArray();
                }
                $userProperties       = $product->getCustomProperties();
                $userProperties       = array_merge($r, $userProperties);
                $userPropertiesChunks = $userProperties ? array_chunk($userProperties, ceil(count($userProperties) / 2), true) : [];
                ?>

                <div class="row m-b10">
                    <?php foreach ($userPropertiesChunks as $userPropertiesChunk): ?>

                        <div class="col-md-6">
                            <?php foreach ($userPropertiesChunk as $propertyKey => $propertyValue): ?>
                                <div class="row">
                                    <div class="col-xs-5 col-sm-4 col-md-5 p-r0 m-b10"><b><?= H($propertyKey); ?></b>
                                    </div>
                                    <div class="col-xs-7 col-sm-8 col-md-7 m-b10"><?= H($propertyValue); ?></div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                    <?php endforeach; ?>
                </div>


                <h3><?= _t('public.ps', 'Description'); ?></h3>
                <h3 class="product-page__prod-title">
                    <?= H($product->getTitle()) ?>
                </h3>
                <div>
                    <?= $product->getDescription() ?>
                </div>
            </div>
            <?php if ($files3d = $product->getProductFiles()->andWhere(['type' => [ProductFile::TYPE_CAD, ProductFile::TYPE_FILE]])->all()): ?>
                <div class="product-page__section">
                    <h3 class="product-page__section-title"><?= _t('public.product', 'Visual 3D models'); ?></h3>

                    <div class="responsive-container-list product-3dmodel-list">
                        <?php foreach ($files3d as $file3d): ?>
                            <div class="responsive-container">
                                <?php
                                [$htmlFile3d, $previewLink] = $file3d->getRenderUrl();
                                ?>
                                <div class="product-3dmodel">
                                    <div class="product-3dmodel__pic">
                                        <?= Html::a($htmlFile3d, $previewLink, ['class' => 'jspopup']); ?>
                                    </div>
                                    <div class="product-3dmodel__title">
                                        <?= Html::tag('span', H($file3d->title), ['title' => $file3d->title]); ?>
                                    </div>
                                    <?php /*
                                <div class="product-3dmodel__btn">
                                    <a href="<?= $file3d->file->getFileUrl() ?>" class="btn btn-sm btn-primary btn-ghost">
                                        <span class="tsi tsi-download-l m-r10"></span><?= _t('public.product', 'Download'); ?>
                                    </a>
                                </div>
                                */ ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($videos = $product->getProductVideos()): ?>
                <div class="product-page__section">
                    <h3 class="product-page__section-title"><?= _t('public.product', 'Videos'); ?></h3>
                    <div class="row">
                        <?php foreach ($videos as $video): ?>
                            <div class="col-sm-4">
                                <div class="video-container">
                                    <object data="https://www.youtube.com/embed/<?= $video['videoId']; ?>"></object>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>


            <?php
            $certDataProvider = $product->getCertificationsDataProvider();
            if ($certDataProvider->getTotalCount() > 0):
                ?>
                <div class="product-page__section">
                    <h3 class="product-page__section-title"><?= _t('company.public', 'Product Certifications'); ?></h3>
                    <div class="table-responsive">
                        <?php echo \yii\grid\GridView::widget(
                            [
                                'dataProvider' => $certDataProvider,
                                'columns'      => [
                                    [
                                        'format' => 'raw',
                                        'value'  => function ($cert) {

                                            if (\common\components\FileTypesHelper::isImage($cert->file)) {
                                                $downloadLink = \frontend\components\image\ImageHtmlHelper::getClickableThumb($cert->file);
                                            } else {
                                                $imgHtml      = "<img width='85' src='/static/images/certificate.svg' style='margin-bottom: -5px;'/> ";
                                                $downloadLink = \yii\helpers\Html::a($imgHtml, $cert->file->getFileUrl());
                                            }
                                            return $downloadLink;
                                        }
                                    ],
                                    [
                                        'attribute' => 'title',
                                        'format'    => 'raw',
                                        'label'     => _t('mybusiness.public', 'Certification'),
                                        'value'     => function ($cert) {
                                            $downloadLink = \yii\helpers\Html::a(_t('site.product', 'Download'), $cert->file->getFileUrl());
                                            if (\common\components\FileTypesHelper::isImage($cert->file)) {
                                                $downloadLink = '<br />' . \frontend\components\image\ImageHtmlHelper::getClickableThumb($cert->file);
                                            }

                                            return '<b>' . H($cert->title) . '</b>';
                                        }
                                    ],
                                    'certifier',
                                    'application',
                                    'issue_date:date',
                                    'expire_date:date',
                                ],
                                'tableOptions' => [
                                    'class' => 'table table-bordered product__cert-table'
                                ],
                            ]
                        ); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php foreach ($product->getPublicProductBlocks() as $productBlock): ?>
                <div class="product-page__section ugc-content">
                    <h3 class="product-page__section-title"><?= H($productBlock->title); ?></h3>

                    <?= $productBlock->content; ?>

                    <div class="m-t10">
                        <?= $productBlock->imageFiles ?
                            \frontend\widgets\SwipeGalleryWidget::widget([
                                'files'            => $productBlock->imageFiles,
                                'thumbSize'        => [230, 180],
                                'containerOptions' => ['class' => 'picture-slider'],
                                'itemOptions'      => ['class' => 'picture-slider__item'],
                                'scrollbarOptions' => ['class' => 'picture-slider__scrollbar'],
                            ]) : '';
                        ?>
                    </div>
                    <?php if ($productBlock instanceof CompanyBlock && $videos = $productBlock->getCompanyBlockVideos()): ?>
                        <div class="row">
                            <?php foreach ($videos as $video): ?>
                                <div class="col-sm-4">
                                    <div class="video-container">
                                        <object data="https://www.youtube.com/embed/<?= $video['videoId']; ?>"></object>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

            <?php
            if (false && param('allowDynamicFields')) {
                ?>
                <div class="product-page__section">
                    <h3 class="product-page__section-title"><?= _t('public.ps', 'Attributes'); ?></h3>
                    <?= $this->render('viewDynamicFieldValues', ['dynamicFieldValues' => $product->getDynamicValues()]); ?>
                </div>
                <?php
            }
            ?>

        </div>
        <div class="col-sm-3">
            <h3 class="m-t0"><?= _t('site.store', 'See also') ?></h3>
            <div class="product-rec-card-list">
                <?php foreach ($seeAlsoProducts as $alsoProduct):
                    /** @var $alsoProduct Product */
                    ?>
                    <div class="product-rec-card">
                        <div class="product-card">
                            <a class="product-card__pic" href="<?= $alsoProduct->getPublicPageUrl() ?>"
                               title="<?= H($alsoProduct->getTitle()) ?>" alt="<?= H($alsoProduct->getTitle()) ?>">
                                <?php if ($alsoProduct->getCoverUrl()) { ?>
                                    <img src="<?= $alsoProduct->getCoverUrl() ?>" alt="<?= H($alsoProduct->getTitle()) ?>">
                                <?php } else { ?>
                                    <span class="product-card__pic-empty">
                                        <?= _t('site.catalog', 'Images not uploaded') ?>
                                    </span>
                                <?php } ?>
                            </a>
                            <a class="product-card__title" href="<?= HL($alsoProduct->getPublicPageUrl()) ?>" title="<?= H($alsoProduct->getTitle()) ?>">
                                <?= H($alsoProduct->getTitle()) ?>
                            </a>
                            <a href="<?= HL($alsoProduct->company->getPublicProductsCompanyLink()); ?>" target="_blank" class="product-card__supplier">
                                123<?= H($alsoProduct->company->title); ?>
                            </a>
                            <?php if ($alsoProduct->getPriceMoneyByQty(1)): ?>
                                <div class="product-card__price"><?= displayAsMoney($alsoProduct->getPriceMoneyByQty(1)); ?>
                                    <span class="product-card__price-unit">/<?= $alsoProduct->getUnitTypeTitle(1); ?></span>
                                </div>
                            <?php else: ?>
                                <div class="product-card__price"><?= _t('site.product', 'Price on request'); ?></div>
                            <?php endif; ?>
                            <div class="product-card__min-qty"><?= $alsoProduct->getMinOrderQty(); ?> <?= $alsoProduct->getUnitTypeTitle($alsoProduct->getMinOrderQty()); ?>
                                (<abbr title="<?= _t('site.catalog', 'Minimum order quantity') ?>">MOQ</abbr>)
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<script>
    function initProductPublic() {
        var h3Tags = document.getElementsByClassName('product-page__section-title');
        var html = '<ul class="nav nav-tabs nav-tabs--secondary productBlockTabs" role="tablist">';
        html = html + '<li role="presentation" class="active productBlockTab productBlockTab0" data-block-id=0><a href="#productBlockSection0" role="tab">Product information</a></li>';
        var i = 0;
        _.each(h3Tags, function (tag) {
            i++;
            $(tag).addClass('productBlock' + i);
            $(tag).parent().attr('id', 'productBlockSection' + i);
            html = html + '<li class="productBlockTab productBlockTab' + i + '" data-block-id=' + i + ' role="presentation"><a href="#productBlockSection' + i + '" role="tab">' + tag.innerText + '</a></li>';
        });
        html = html + '</ul>';

        $('#jsprod-nav-tabs').html(html);
        setTimeout(function () {
            var navOffset = $('.product-page__nav').offset().top - $('.product-page__nav').outerHeight(true) - 30;
            $('#jsprod-nav').affix({offset: {top: navOffset}});
        }, 100);

        $('body').scrollspy({target: "#jsprod-nav", offset: 230});

        $(".productBlockTab").click(function (el) {

            var blockId = el.currentTarget.dataset.blockId;
            $('.productBlockTabs li').removeClass('active');
            $(".productBlockTab" + blockId).addClass('active');

            $('html, body').animate({
                scrollTop: $(".productBlock" + blockId).offset().top - 200
            }, 500);
        });
    }

    document.addEventListener("DOMContentLoaded", function (event) {
        initProductPublic();
        $('.ugc-content table').wrap('<div class="table-responsive m-t10"></div>');
    });

</script>
