<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.04.18
 * Time: 9:12
 */

use backend\models\search\ProductSearch;
use common\components\ArrayHelper;
use common\modules\product\repositories\ProductRepositoryCondition;
use frontend\modules\product\widgets\ProductCategoryDropdownWidget;
use yii\helpers\Html;

/** @var $productCategory \common\models\ProductCategory|null */
/** @var $searchModel ProductSearch|ProductRepositoryCondition */
$formUrl = '/products';

$searchParams = $_GET; // TODO: Add search params filter
unset($searchParams['category']);

// search panel inside business area
$productCategory = $searchModel->category;
$isMybusinessArea = false;
if ($searchModel instanceof ProductSearch) {
    $isMybusinessArea = $searchModel->getSearchPanelTemplate() === 'mybusinessProduct';
}
Yii::$app->angular
    ->service(['notify', 'router', 'user'])
    ->controller('product/productSearchPanel')
    ->controllerParams([
        'productSearchRoute' => $isMybusinessArea ? '/mybusiness/products' : null,
        'searchParams'       => $searchParams,
        'categoryCode'       => $isMybusinessArea ? '' : ($productCategory->code ?? '')
    ]);

?>
<div ng-controller="ProductSearchPanelController" id="searchPanel">

    <div class="category-list">
        <?php if (!$productCategory): ?>
            <h4 class="category-list__title"> <?= _t('site.common', 'Categories'); ?></h4>
        <?php endif; ?>
        <?php

        $currentTitle = $allCategoriesTitle = _t('site.common', 'All Categories');
        if ($productCategory) {
            $currentTitle = $productCategory->title;
        }

        echo ProductCategoryDropdownWidget::widget([
            'allCategoriesTitle' => $allCategoriesTitle,
            'currentTitle'       => $currentTitle,
            'layout'             => $searchModel->getSearchPanelTemplate(),
            'activeCategory'     => $productCategory,
            'searchModel'        => $searchModel,
        ]);
        ?>
    </div>


    <?php if (param('allowDynamicFields')) { ?>

        <?= $this->render('searchFilters.php', ['dynamicFieldFilters' => $searchModel->dfFilters]) ?>

    <?php } ?>
</div>