<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.04.18
 * Time: 11:30
 */

use common\models\Payment;
use common\models\Product;
use common\models\StoreOrder;
use common\models\StoreOrderItem;
use common\models\StorePricerType;
use common\modules\product\models\ProductDeliveryForm;
use frontend\models\store\StoreFacade;
use yii\widgets\DetailView;

/** @var $deliveryForm ProductDeliveryForm */

/** @var $product Product */

/** @var $storeOrder StoreOrder */

$paymentService = \Yii::createObject(\common\modules\payment\services\PaymentService::class);

$total = $storeOrder->getTotalPricer();
$amountNetIncome = $paymentAccountService->getUserMainAmount($currentUser);


$storeOrderItem = $storeOrder->storeOrderItems;
/** @var StoreOrderItem $storeOrderItem */
$storeOrderItem = reset($storeOrderItem);

?>
<?=$this->render('navigationTabs');?>
<div class="container">
    <div class="row">
        <div class="col-sm-7 col-md-8 wide-padding wide-padding--right">
            <h1><?php echo _t('site.store', 'Payment'); ?></h1>
            <?= $this->render('@frontend/views/my/print-model3d/checkoutCard', [
                'storeOrder'      => $storeOrder,
                'clientToken'     => $clientToken,
                'postUrl'         => '/store/payment/checkout',
                'amountNetIncome' => $amountNetIncome
            ]) ?>
        </div>

        <div class="col-sm-5 col-md-4 m-t30">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><?= _t('site.store', 'Billing Details'); ?></h3>
                </div>
                <div class="panel-body">
                    <?php
                    $pricerDescriptionIntl = StoreFacade::getPriceTypesIntl($storeOrder->storePricers, $storeOrder);

                    foreach ($storeOrder->storePricers as $pricers) {
                        if (in_array($pricers->pricerType->type, [StorePricerType::FEE, StorePricerType::TAX, StorePricerType::TOTAL])) {
                            continue;
                        }
                        if (empty($pricers->pricerType->operation)) {
                            continue;
                        }

                        if (((float)$pricers->price) == 0) {
                            continue;
                        }

                        $pricerDescription = array_key_exists($pricers->pricerType->type, $pricerDescriptionIntl)
                            ? $pricerDescriptionIntl[$pricers->pricerType->type]
                            : $pricers->pricerType->description;
                        ?>
                        <div class="row billing-row">
                            <div class="col-xs-7"><?= $pricerDescription; ?></div>
                            <div class="col-xs-5 text-right"><strong><?php
                                    echo $pricers->pricerType->type === 'discount' ? '&minus; ' : '';
                                    echo displayAsCurrency($pricers->price, $pricers->currency); ?></strong></div>
                        </div>
                    <?php } ?>

                    <?php if (!empty($fee)) :
                        if ($tax) {
                            $fee->price = $fee->price + $tax->price; // StorePricer
                        }
                        ?>
                        <div class="row billing-row">
                            <div class="col-xs-7"><?= $pricerDescriptionIntl[$fee->pricerType->type]; ?></div>
                            <div class="col-xs-5 text-right"><strong><?= displayAsCurrency($fee->price, $fee->currency); ?></strong></div>
                        </div>
                    <?php endif; ?>

                </div>

                <div class="panel-price">
                    <h5 class="panel-price__title">
                        <?= $pricerDescriptionIntl[$total->pricerType->type]; ?>:
                    </h5>

                    <div class="panel-price__body">
                        <div class="panel-price__body__price total_price">
                            <?= displayAsCurrency($total->price, $total->currency); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><?= _t('site.store', 'Product Details'); ?></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <?= _t('site.printer', 'Product'); ?>:
                        </div>
                        <div class="col-xs-8 p-l0">
                            <b><?= H($storeOrderItem->product->title) ?></b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <?= _t('site.printer', 'Quantity'); ?>:
                        </div>
                        <div class="col-xs-8 p-l0">
                            <b><?= $storeOrderItem->qty ?></b>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><?= _t('site.store', 'Expedited Delivery Address'); ?></h3>
                </div>
                <div class="panel-body">
                    <div class="detail-view--custom-view">
                        <?php
                        echo DetailView::widget([
                            'model'      => $deliveryForm,
                            'attributes' => [
                                [
                                    'attribute' => 'country',
                                    'label'     => _t('site.store', 'Country'),
                                ],
                                [
                                    'attribute' => 'state',
                                    'label'     => _t('site.store', 'State'),
                                ],
                                [
                                    'attribute' => 'city',
                                    'label'     => _t('site.store', 'City'),
                                ],
                                [
                                    'attribute' => 'street',
                                    'label'     => _t('site.store', 'Street address'),
                                ],
                                [
                                    'attribute' => 'street2',
                                    'label'     => _t('site.store', 'Extended address'),
                                    'visible'   => $deliveryForm->street2 ? true : false
                                ],
                                [
                                    'attribute' => 'zip',
                                    'label'     => _t('site.store', 'Zip'),
                                ],
                                [
                                    'attribute' => 'company',
                                    'label'     => _t('site.store', 'Company'),
                                    'visible'   => $deliveryForm->company ? true : false
                                ],
                                [
                                    'attribute' => 'contact_name',
                                    'label'     => _t('site.store', 'Contact name'),
                                ],
                                [
                                    'attribute' => 'phone',
                                    'label'     => _t('site.store', 'Phone'),
                                    'visible'   => $deliveryForm->phone ? true : false
                                ],
                                [
                                    'attribute' => 'email',
                                    'label'     => _t('site.store', 'Email'),
                                ],
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
