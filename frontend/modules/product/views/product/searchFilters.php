<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 24.06.18
 * Time: 12:22
 */

/** @var $dynamicFieldFilters \common\modules\dynamicField\filterTemplates\DynamicFieldFilterInterface[] */

?>
<?php
foreach ($dynamicFieldFilters as $filter) {
    ?>
    <div class="dynamic-field-filter">
        <div class="dynamic-field-filter__label">
            <?= $filter->dynamicField->title ?>
        </div>
        <?= $filter->getElementForm($this); ?>
    </div>
    <?php
}
?>
