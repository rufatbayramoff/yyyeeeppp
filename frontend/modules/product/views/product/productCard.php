<?php
/** @var \common\modules\product\interfaces\ProductInterface $product */

if (!$model) {
    return ;
}
$product = $model->getExtObject();
$company = $product->getCompanyManufacturer();
$productPriceFrom = $product->getMoqFromPriceMoney();

?>

<div class="product-card">
    <a class="product-card__pic" href="<?=$product->getPublicPageUrl()?>" title="<?= H($product->getTitle()) ?>" alt="<?= H($product->getTitle()) ?>">
        <?php if ($product->getCoverUrl()) {?>
            <img src="<?= $product->getCoverUrl() ?>" alt="<?= H($product->getTitle()) ?>">
        <?php } else {?>
            <span class="product-card__pic-empty">
                    <?= _t('site.catalog', 'Images not uploaded') ?>
                </span>
        <?php }?>
    </a>
    <a class="product-card__title" href="<?=HL($product->getPublicPageUrl())?>" title="<?= H($product->getTitle()) ?>"><?= H($product->getTitle()) ?></a>
    <a href="<?=HL($company?->getPublicProductsCompanyLink($product));?>" target="_blank" class="product-card__supplier"><?=H($company?->title);?></a>
    <?php if ($productPriceFrom->getAmount()): ?>
        <div class="product-card__price"><span class="product-card__price-unit"><?=_t('site.product', 'from'); ?></span> <?= displayAsMoney($productPriceFrom); ?><span class="product-card__price-unit">/<?=$product->getUnitTypeCode(1);?></span></div>
    <?php else: ?>
        <div class="product-card__price"><?=_t('site.product', 'Price on request');?></div>
    <?php endif; ?>
    <div class="product-card__min-qty">
        <abbr title="<?= _t('site.catalog', 'Minimum order quantity') ?>">MOQ</abbr>:
        <?=$product->getMinOrderQty(); ?> <?=$product->getUnitTypeTitle($product->getMinOrderQty());?></div>
</div>