<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 15:08
 */

use common\models\Product;
use common\models\ProductBind;


?>

<div class="col-xs-6 col-sm-6 col-md-4 store-layout__item">
    <?php echo $this->render('productCard',['model' => $model])?>
</div>
