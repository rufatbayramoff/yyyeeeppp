<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.06.18
 * Time: 16:52
 */
/** @var $dynamicFieldValues \common\modules\dynamicField\models\DynamicFieldValue[] */

$dynamicFieldService = \Yii::createObject(\common\modules\dynamicField\services\DynamicFieldService::class);
foreach ($dynamicFieldValues as $dynamicFieldValue) {
    ?>
    <div class="row">
        <div class="col-xs-5 col-sm-3 p-r0 m-b10">
            <?= H($dynamicFieldValue->dynamicField->title) ?>
        </div>
        <div class="col-xs-7 col-sm-8  m-b10">
            <?= $dynamicFieldService->displayValue($dynamicFieldValue) ?>
        </div>
    </div>
    <?php
}
?>


