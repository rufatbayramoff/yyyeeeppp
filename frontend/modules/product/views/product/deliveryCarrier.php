<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 20.04.18
 * Time: 10:22
 */

use common\models\GeoCountry;

?>
<div class="col-sm-4 delivery-details wide-padding wide-padding--right">
    <h3 class="delivery-details__title"><?= _t('site.delivery', 'Shipping information'); ?></h3>
    <div class="row">
        <div class="form-group col-sm-12 field-deliveryform-contact_name">
            <label class="control-label" for="deliveryform-contact_name"><?= _t('site.store', 'Contact Name'); ?> <span class="form-required">*</span></label>
            <input name="deliveryform-contact_name" class="form-control" ng-model="deliveryForm.contactName">
            <p class="help-block help-block-error" validation-for="deliveryform-contact_name"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-company">
            <label class="control-label" for="deliveryform-company"><?= _t('site.store', 'Company'); ?></label>
            <input name="deliveryform-company" class="form-control" ng-model="deliveryForm.company">
            <p class="help-block help-block-error" validation-for="deliveryform-company"></p>
        </div>
z
        <div class="form-group col-sm-12 field-deliveryform-phone">
            <label class="control-label" for="deliveryform-phone"><?= _t('site.store', 'Phone'); ?></label>
            <input name="deliveryform-phone" class="form-control" ng-model="deliveryForm.phone">
            <p class="help-block help-block-error" validation-for="deliveryform-phone"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-email">
            <label class="control-label" for="deliveryform-email"><?= _t('site.store', 'Email'); ?> <span class="form-required">*</span></label>
            <input name="deliveryform-email" class="form-control" ng-model="deliveryForm.email">
            <p class="help-block help-block-error" validation-for="deliveryform-email"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-email">
            <span class="form-required">*</span> <?= _t('site.store', 'Required'); ?>
        </div>
    </div>
</div>

<div class="col-sm-8 ">
    <div class="row m-t30">
        <h3 class="delivery-details__title m-t30"></h3>
        <div class="col-sm-6 form-group field-deliveryform-street required">
            <label class="control-label" for="deliveryform-street"><?= _t('site.store', 'Street Address'); ?> <span class="form-required">*</span></label>
            <input name="deliveryform-street" class="form-control" ng-model="deliveryForm.street" aria-required="true">
            <p class="help-block help-block-error" validation-for="deliveryform-street"></p>
        </div>

        <div class="col-sm-6 form-group field-deliveryform-street2">
            <label class="control-label" for="deliveryform-street2"><?= _t('site.store', 'Extended Address'); ?></label>
            <input name="deliveryform-street2" class="form-control" ng-model="deliveryForm.street2">
            <p class="help-block help-block-error" validation-for="deliveryform-street2"></p>
        </div>

        <div class="col-sm-6 form-group field-deliveryform-city required">
            <label class="control-label" for="deliveryform-city"><?= _t('site.store', 'City'); ?> <span class="form-required">*</span></label>
            <input name="deliveryform-city" class="form-control" ng-model="deliveryForm.city" aria-required="true">
            <p class="help-block help-block-error" validation-for="deliveryform-city"></p>
        </div>

        <div class="col-sm-6 form-group field-deliveryform-state">
            <label class="control-label" for="deliveryform-state"><?= _t('site.store', 'State/Region'); ?> <span class="form-required">*</span></label>
            <input name="deliveryform-state" class="form-control" ng-model="deliveryForm.state">
            <p class="help-block help-block-error" validation-for="deliveryform-state"></p>
        </div>


        <div class="col-sm-4 form-group field-deliveryform-zip required">
            <label class="control-label" for="deliveryform-zip"><?= _t('site.store', 'Zip'); ?> <span class="form-required">*</span></label>
            <input name="deliveryform-zip" class="form-control" ng-model="deliveryForm.zip" aria-required="true">
            <p class="help-block help-block-error" validation-for="deliveryform-zip"></p>
        </div>
        <div class="col-sm-8 form-group field-deliveryform-country required">
            <label class="control-label" for="deliveryform-country"><?= _t('site.store', 'Country'); ?></label>
            <select name="deliveryform-country" class="form-control" ng-model="deliveryForm.countryIso" placeholder="Select"
                    aria-required="true">
                <?php foreach (GeoCountry::getCombo('iso_code') as $countryIso => $countryTitle) {
                    ?>
                    <option value="<?= $countryIso ?>" ng-selected="deliveryForm.country == '<?= $countryIso; ?>'"
                            ng-if="allowShowCountryIsoSelect('<?= $countryIso; ?>')"><?= $countryTitle ?></option>
                    <?php
                }
                ?>
            </select>
            <p class="help-block help-block-error" validation-for="deliveryform-country"></p>
        </div>

        <div class="col-md-12">

            <input id="autocomplete"
                   google-address-input=""
                   on-address-change="onAutocompleteAddressChange(address)"
                   type="text" class="form-control"/>
        </div>
        <div class="col-md-12">
            <div google-map="deliveryForm"
                 on-marker-position-change="onMapMarkerPositionChange(address)"
                 id="map"
                 style="height: 250px; margin-top: 15px; margin-bottom: 15px; border-radius: 5px;"></div>
        </div>
    </div>
</div>


<div class="col-sm-12">
    <h4 class="delivery-details-type__title border-0 m-t20 m-b0">
        <?= _t('site.store', 'Comment'); ?>
    </h4>
    <div class="form-group">
        <textarea name="deliveryform-comment" class="form-control" ng-model="deliveryForm.comment" placeholder="<?= _t(
            'site.store',
            'Leave comments if you have any requests, instructions or additional information regarding the production of your order. Please specify if there are parts that need to fit one another so the manufacturer can check them before shipping your order.'
        ); ?>" maxlength="600"></textarea>
        <p class="help-block help-block-error" validation-for="deliveryform-comment"></p>
    </div>
</div>
