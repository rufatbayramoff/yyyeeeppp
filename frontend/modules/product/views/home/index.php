<?php
/**
 * @var View $this
 * @var HomePageCategoryBlock[] $productCategoriesBlock
 * @var HomePageCategoryCard[] $productCategoryCard
 * @var ProductMainCard[] $cards
 */

use common\models\HomePageCategoryBlock;
use common\models\HomePageCategoryCard;
use common\models\ProductCategory;
use common\models\ProductCommon;
use common\models\ProductMainCard;
use common\models\ProductMainCategory;
use common\models\ProductMainPromoBar;
use common\models\ProductMainSlider;
use frontend\assets\IndexPageAsset;
use frontend\assets\LightboxAsset;
use frontend\assets\SwiperAsset;
use frontend\components\image\ImageHtmlHelper;
use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;
use yii\helpers\Html;
use yii\web\View;

$this->registerAssetBundle(IndexPageAsset::class);
$this->registerAssetBundle(SwiperAsset::class);
$this->registerAssetBundle(LightboxAsset::class);

/** @var ProductCommon[] $cards */
/** @var ProductMainCategory[] $productMainCategories */
/** @var ProductMainPromoBar $promoBar */
$featuredProducts = $cards[ProductMainCard::FEATURE] ?? [];
$machines = $cards[ProductMainCard::MACHINE] ?? [];
$materials = $cards[ProductMainCard::MATERIAL] ?? [];
?>
<div class="hero-banner hero-banner--light-grey">
    <div class="container container--wide">
        <div class="products-promo-hero">

            <div class="products-promo-hero__prod">

                <div class="hero-banner__prod-list">
                    <?php /** @var ProductCategory[] $categories */
                    foreach($categories as $category):?>
                        <?php echo Html::a(_t('site.store', $category->title),$category->getViewUrl(),['class' => 'hero-banner__prod-item'])?>
                    <?php endforeach;?>
                </div>
                <a href="<?=ProductUrlHelper::productsModel3d()?>" class="btn btn-default products-promo-hero__prod-btn">
                    <?php echo _t('site.store', '3D Models')?>
                </a>
            </div>

            <div class="products-promo-hero__banner">
                <div class="products-promo-hero__banner-cont swiper-container swiper-container-horizontal"
                     style="cursor: grab;">

                    <div class="swiper-wrapper">
                        <?php /** @var ProductmainSlider[] $sliders */
                        foreach($sliders as $slider):?>
                            <div class="swiper-slide">
                                <a href="<?php echo $slider->url?>" class="products-promo-hero__banner-inner">
                                    <div class="products-promo-hero__banner-item-pic" style="background-image:url('<?php echo $slider->file?->getFileUrl()?>')"></div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>


                    <div class="products-promo-hero__banner-pagination swiper-pagination swiper-pagination-clickable swiper-pagination-bullets">
                    </div>

                </div>

            </div>

            <div class="hero-banner__features">
                <a href="<?php echo $promoBar->url?>" class="hero-banner__features-img">
                    <img src="<?=$promoBar->file?ImageHtmlHelper::getThumbUrlForFile($promoBar->file, 400,600):''?>">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="site-index">

    <div class="promo-blocks-6">
        <div class="container container--wide js-main-page-shop-by-category">
            <div class="promo-blocks-6__list swiper-container">
                <div class="swiper-wrapper">
                    <a href="<?php echo ProductUrlHelper::productsCatalog()?>" class="promo-blocks-6__item swiper-slide t-promo-blocks-6__2">
                        <div class="promo-blocks-6__item-fade"></div>
                        <div class="promo-blocks-6__item-pic"
                             style="background-image: url('http://static.treatstock.com/static/files/75/33/348271_0_d25069ee281b60a25025a4b2a0614337.jpg?date=1534953565')"></div>
                        <h4 class="promo-blocks-6__item-title">
                            <?php echo _t('site.store','All Categories')?>&nbsp;<span class="tsi tsi-right"></span>
                        </h4>
                    </a>
                    <?php foreach ($productMainCategories as $productMainCategory):?>
                        <a title="<?php echo _t('site.store',$productMainCategory->productCategory?->title)?>" href="<?php echo $productMainCategory->productCategory?->getViewUrl()?>"
                           class="promo-blocks-6__item swiper-slide t-promo-blocks-6__3">
                            <div class="promo-blocks-6__item-fade"></div>
                            <div class="promo-blocks-6__item-pic"
                                 style="background-image: url('<?php echo $productMainCategory->productCategory?->file?->getFileUrl()?>')"></div>
                            <h4 class="promo-blocks-6__item-title">
                                <?php echo _t('site.store',$productMainCategory->productCategory?->title); ?><span class="tsi tsi-right"></span>
                            </h4>
                        </a>
                    <?php endforeach; ?>
                </div>
                <div class="promo-blocks-6__scrollbar swiper-scrollbar"></div>
            </div>
        </div>
    </div>

    <div class="container container--wide m-t30">
        <div class="row">
            <div class="col-sm-6">
                <div class="prod-main-promo-card">
                    <h2 class="prod-main-promo-card__title">
                        <?= _t('site.products', 'Machines'); ?>
                    </h2>
                    <div class="row">
                        <?php foreach ($machines as $machine):?>
                            <div class="col-xs-12 col-sm-6">
                                <?php echo $this->render('@frontend/modules/product/views/product/productCard',['model' => $machine])?>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="prod-main-promo-card">
                    <h2 class="prod-main-promo-card__title">
                        <?= _t('site.products', 'Materials'); ?>
                    </h2>
                    <div class="row">
                        <?php foreach ($materials as $material):?>
                            <div class="col-xs-12 col-sm-6">
                                <?php echo $this->render('@frontend/modules/product/views/product/productCard',['model' => $material])?>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="promo-blocks-featured">
        <div class="container container--wide">

            <div class="row">
                <div class="col-xs-12">
                    <h2 class="promo-page-title animated fadeInDown" data-animation-in="fadeInDown"
                        data-animation-out="fadeOutUp" style="opacity: 0;"><?php echo _t('site.store','Featured products')?> </h2>
                    <a href="/products/all" class="promo-page-title__link"><?php echo _t('site.store','View All')?></a>
                </div>
            </div>

            <div class="row">
                <div class="promo-blocks-featured__slider swiper-container">
                    <div class="swiper-wrapper">
                        <?php foreach ($featuredProducts as $featuredProduct):?>
                            <div class="col-sm-4 col-md-3 promo-blocks-featured__slider-item swiper-slide">
                                <?php echo $this->render('@frontend/modules/product/views/product/productCard',['model' => $featuredProduct])?>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <div class="promo-blocks-featured__scrollbar swiper-scrollbar"></div>
                </div>
            </div>
        </div>
    </div>

</div>


<?php #echo ShoppingCandidateWidget::widget();
function minifyJS($buffer)
{

    $buffer = preg_replace("/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))/", "", $buffer);
    $buffer = str_replace(["\r\n", "\r", "\t", "\n", '  ', '    ', '     '], '', $buffer);
    $buffer = preg_replace(['(( )+\))', '(\)( )+)'], ')', $buffer);

    return $buffer;
}

$content = file_get_contents(Yii::getAlias('@frontend/web/js/mainpage.js'));

?>
<?php $this->registerJs(minifyJS($content)); ?>
<?php $js = <<<JS

    var swiper = new Swiper(".slider-products", {
        pagination: {
            el: ".swiper-pagination",
        },
    });

JS; ?>
<?php $this->registerJs($js); ?>

