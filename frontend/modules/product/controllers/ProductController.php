<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 14:40
 */

namespace frontend\modules\product\controllers;

use common\models\Product;
use common\models\ProductCategory;
use common\models\SeoPage;
use common\models\StoreOrder;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\product\models\ProductDeliveryForm;
use common\modules\product\models\ProductDeliveryParams;
use common\modules\product\models\ProductPlaceOrderState;
use common\modules\product\models\ProductSearchForm;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\repositories\ProductRepositoryCondition;
use common\modules\product\services\ProductService;
use frontend\models\user\UserFacade;
use yii\web\NotFoundHttpException;

/**
 * Product controller
 */
class ProductController extends \common\components\BaseController
{

    /** @var ProductPlaceOrderState */
    public $productPlaceOrderState;

    /** @var ProductRepository */
    public $productRepository;

    public const DEFAULT_PAGE_SIZE = 50;


    public function injectDependencies(ProductPlaceOrderState $productPlaceOrderState, ProductRepository $productRepository)
    {
        $this->productPlaceOrderState = $productPlaceOrderState;
        $this->productRepository      = $productRepository;
    }

    protected function redirectCategoryId()
    {
        $url = \Yii::$app->request->getUrl();
        $str = '?category_id=';
        if (strpos($url, $str)) {
            $categoryId = (int)substr($url, strpos($url, $str) + strlen($str), 1024);
            $category   = ProductCategory::findByPk($categoryId);
            if ($category) {
                $newUrl = '/products/' . $category->code;
                if ($category->id === 1) {
                    $newUrl = '/products';
                }
                $this->redirect($newUrl, 301);
                \Yii::$app->end();
            }
        }
    }

    /**
     * @param string $category
     * @throws NotFoundHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        // Check category_id parameter
        return $this->product('');
    }

    public function actionCategory($category)
    {
        return $this->product($category);
    }

    protected function seoForSearch(): bool
    {
        if (!$this->seo) {
            return true;
        }
        return $this->seo && $this->seo->url === 'products';
    }

    /**
     * @param null $slug
     * @param ProductRepository $repository
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($slug, ProductRepository $repository, ProductService $productService)
    {
        if (strpos($slug, '-') === FALSE) {
            throw new NotFoundHttpException('Invalid url format');
        }
        [$uuid, $slugText] = explode('-', $slug);
        $product = $repository->getByUuid($uuid);
        $code    = \Yii::$app->request->get('pvc', null); // private view code
        if ((!$product || !$product->isActive() || !$product->isAvailableInCatalog()) && !$productService->canPrivateView($product, $code)) {
            return $this->render('productNotFound');
        }
        $seeAlsoProducts = $repository->getSeeAlsoProductsByProduct($product);
        return $this->render('view', ['product' => $product, 'seeAlsoProducts' => $seeAlsoProducts]);
    }

    public function actionBuy($uuid)
    {
        $product = Product::find()->where(['uuid' => $uuid])->active()->one();
        if (!$product) {
            return $this->render('productNotFound');
        }
        $this->productPlaceOrderState->setProduct($product);
        $productCart = $this->productPlaceOrderState->getProductCart();
        $productCart->fixOneIfNotExists($product);
        return $this->render('buy', ['product' => $product, 'productCart' => $productCart]);
    }

    /**
     * @param $product
     * @return ProductDeliveryForm
     * @throws \yii\web\NotFoundHttpException
     */
    public function getDeliveryParams($product, $productCart)
    {
        $deliveryParams              = new ProductDeliveryParams();
        $deliveryParams->product     = $product;
        $deliveryParams->productCart = $productCart;
        $deliveryForm                = new ProductDeliveryForm($deliveryParams);
        $user                        = UserFacade::getCurrentUser();
        $deliveryForm->loadByUserAndLocation($user);
        $this->productPlaceOrderState->getDeliveryFormState($deliveryForm);
        return $deliveryForm;
    }

    /**
     * @throws \yii\web\GoneHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionBuyDelivery()
    {
        $product      = $this->productPlaceOrderState->getProduct();
        $productCart  = $this->productPlaceOrderState->getProductCart();
        $deliveryForm = $this->getDeliveryParams($product, $productCart);
        return $this->render('delivery', ['product' => $product, 'deliveryForm' => $deliveryForm]);
    }


    /**
     * @return string
     * @throws NotFoundHttpException
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\web\GoneHttpException
     */
    public function actionBuyCheckout()
    {
        $product      = $this->productPlaceOrderState->getProduct();
        $storeOrder   = $this->productPlaceOrderState->getOrder();
        $productCart  = $this->productPlaceOrderState->getProductCart();
        $deliveryForm = $this->getDeliveryParams($product, $productCart);

        StoreOrder::validateBeforeCheckout($storeOrder);
        $vendor          = PaymentInfoHelper::detectVendor($storeOrder);
        $paymentCheckout = new PaymentCheckout($vendor);

        if ($storeOrder->price_total === 0) {
            $orderPaymentToken = '';
        } else {
            $orderPaymentToken = app('session')->get('brt_token_' . $storeOrder->id, $paymentCheckout);
            if ($orderPaymentToken) {
                app('session')->set('brt_token_' . $storeOrder->id, $orderPaymentToken);
            }
        }

        $params            = [
            'product'      => $product,
            'deliveryForm' => $deliveryForm,
            'storeOrder'   => $storeOrder,
            'user'         => $storeOrder->user,
            'vendor'       => $vendor,
            'clientToken'  => $orderPaymentToken,
        ];
        $this->view->title = _t('site.payment', 'Payment. Order #{orderId}', ['orderId' => $storeOrder->id]);

        return $this->render(
            'checkout',
            $params
        );
    }

    /**
     * @param $category
     * @return array|string
     * @throws \yii\base\InvalidConfigException
     */
    protected function product($category): string|array
    {
        $this->redirectCategoryId();

        $searchModel = new ProductSearchForm();
        $searchModel->load(\Yii::$app->request->get());
        $searchModel->populateCategory($category);

        $condition = \Yii::createObject(ProductRepositoryCondition::class);
        $condition->populateByForm($searchModel);
        $dataProvider = $this->productRepository->getProductsProvider($condition);

        if (\Yii::$app->request->isAjax) {
            $htmlBlock = $this->renderAjax(
                'publicProductsListContainer',
                ['dataProvider' => $dataProvider]
            );
            return $this->jsonReturn(
                [
                    'htmlBlock' => $htmlBlock
                ]
            );
        }
        if ($searchModel->getSearchText()) {
            $searchRequest = \H($searchModel->getSearchText());
            $this->seo     = new SeoPage();
            if ($category) {
                $this->seo->title            = $searchRequest . ' in ' . $searchModel->category->title . ' - Products on Treatstock';
                $this->seo->header           = '"' . $searchRequest . '" in ' . $searchModel->category->title;
                $this->seo->meta_keywords    = _t('site.seo', 'Products, suppliers, wholesale, manufacturers');
                $this->seo->meta_description = _t('site.seo', 'Find wholesale products on Treatstock directly from suppliers. Instant quotes and a global network of manufacturers to help you find perfect products.');
                $this->seo->header_text      = _t('site.seo', 'Here are all the products from категория matching "') . $searchRequest . _t('site.seo', '". If this does not look right, try rewording or shortening your search.');
            } else {
                $this->seo->title            = $searchRequest . _t('site.seo', ' - Products from Manufacturers on Treatstock');
                $this->seo->header           = $searchRequest . _t('site.seo', ' in Products');
                $this->seo->meta_keywords    = _t('site.seo', 'Products, suppliers, wholesale, manufacturers');
                $this->seo->meta_description = _t('site.seo', 'Find wholesale products on Treatstock directly from suppliers. Instant quotes and a global network of manufacturers to help you find perfect products.');
                $this->seo->header_text      = _t('site.seo', 'Below you can find all products matching "') . $searchRequest . _t('site.seo', '". If you are looking for something specific, try selecting a suitable category.');
            }
        }

        return $this->render(
            'publicProductsList',
            ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]
        );
    }
}