<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.04.18
 * Time: 12:53
 */

namespace frontend\modules\product\controllers;

use backend\models\search\ProductSearch;
use common\components\ErrorListHelper;
use common\components\exceptions\OnlyPostRequestException;
use common\components\ModelException;
use common\components\order\anonim\AnonimOrderHelper;
use common\components\order\builder\OrderBuilder;
use common\components\order\PriceCalculator;
use common\components\PaymentExchangeRateFacade;
use common\models\DeliveryType;
use common\models\factories\GeoLocationFactory;
use common\models\factories\UserFactory;
use common\models\Model3d;
use common\models\Product;
use common\models\PsPrinter;
use common\models\StoreUnit;
use common\modules\product\interfaces\ProductInterface;
use common\modules\product\models\ProductDeliveryForm;
use common\modules\product\models\ProductDeliveryParams;
use common\modules\product\models\ProductPlaceOrderState;
use common\modules\product\serializers\ProductSerializer;
use common\modules\product\services\ProductService;
use common\services\LocationService;
use common\services\Model3dService;
use common\services\PrinterMaterialService;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserFacade;
use InvalidArgumentException;
use lib\delivery\delivery\models\DeliveryRate;
use lib\money\Currency;
use lib\money\Money;
use Yii;
use yii\base\InvalidParamException;
use yii\web\UnauthorizedHttpException;

class ProductAjaxController extends \common\components\BaseController
{
    public const MAX_PRICES_COUNT = 20;

    /** @var ProductPlaceOrderState */
    public $productPlaceOrderState;

    /** @var ProductService */
    public $productService;

    /**
     * @var LocationService
     */
    private $locationService;

    public function injectDependencies(ProductPlaceOrderState $productPlaceOrderState, ProductService $productService, LocationService $locationService)
    {
        $this->productPlaceOrderState = $productPlaceOrderState;
        $this->productService         = $productService;
        $this->locationService        = $locationService;
    }

    /**
     * @return array
     * @throws \yii\web\GoneHttpException
     */
    public function actionBuyCartSave()
    {
        if (!\Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $productQty  = \Yii::$app->request->post('items');
        $productCart = $this->productPlaceOrderState->getProductCart();
        $productCart->load($productQty);

        $productUuids = array_keys($productQty);
        $productUuid  = reset($productUuids);
        $product      = Product::find()->where(['uuid' => $productUuid])->one();

        $this->productService->tryCanBeOrderedByCurrentUser($product);

        $this->productPlaceOrderState->setProductCart($productCart);
        $this->productPlaceOrderState->setProduct($product);

        return $this->jsonReturn(
            [
                'success' => true
            ]
        );
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     * @throws UnauthorizedHttpException
     */
    public function actionGetMyProductsList()
    {
        $user = UserFacade::getCurrentUser();
        if (!$user) {
            throw new UnauthorizedHttpException('Only authorized user');
        }
        $productsSerialized = [];
        foreach ($user->company->products as $product) {
            $productsSerialized[] = ProductSerializer::serialize($product);
        }
        return $this->jsonReturn($productsSerialized);
    }

    /**
     * @return array
     * @throws \Exception
     * @throws \yii\web\GoneHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSaveDeliveryInfo()
    {
        if (!\Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $product     = $this->productPlaceOrderState->getProduct();
        $productCart = $this->productPlaceOrderState->getProductCart();
        $productQty  = $productCart->items[$product->uuid]->qty;

        $deliveryInfo = \Yii::$app->request->post('deliveryForm', []);

        $deliveryParams              = new ProductDeliveryParams();
        $deliveryParams->product     = $product;
        $deliveryParams->productCart = $productCart;
        $deliveryForm                = new ProductDeliveryForm($deliveryParams);
        $deliveryForm->loadInfo($deliveryInfo);

        $this->productPlaceOrderState->setDeliveryFormState($deliveryForm);

        $deliveryForm->validate();
        $errors = $deliveryForm->getErrors();
        if ($errors) {
            $validateErrors = ErrorListHelper::addErrorsFormPrefix($errors, 'deliveryform');
            return $this->jsonReturn(
                [
                    'validateErrors' => $validateErrors
                ],
                422
            );

        }

        $deliveryCost                 = $product->productDelivery->getExpressDeliveryByQty($productQty);
        $deliveryRate                 = DeliveryRate::create(null, DeliveryType::STANDARD, Money::create($deliveryCost, Currency::USD), true);
        $deliveryRate->deliveryForm   = $deliveryForm;
        $deliveryRate->deliveryParams = $deliveryParams;

        $user = AnonimOrderHelper::resolveOrderUser();

        $order = OrderBuilder::create()
            ->customer($user, $deliveryForm->email)
            ->comment($deliveryForm->comment)
            ->productCart($productCart)
            ->delivery($deliveryRate)
            ->buildByItems();

        $this->productPlaceOrderState->setOrderId($order->id);

        $this->locationService->changeUserLocationByDelivery($deliveryForm);

        return $this->jsonReturn(
            [
                'success' => true
            ]
        );
    }


    /**
     * @return false|string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\GoneHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws ModelException
     * @internal param array $id
     */
    public function actionGetPrice()
    {
        Yii::$app->session->close();
        $storeUnits = (array)Yii::$app->request->get('storeUnits');
        $model3dId  = (integer)Yii::$app->request->get('model3dId');

        $cnt             = 0;
        $returnCosts     = [];
        $currencyIsoCode = UserFacade::getCurrentCurrency();
        $currentLocation = UserSessionFacade::getLocation();
        if (!$currentLocation->geoCountry) {
            throw new InvalidParamException('Can`t get current user country. Can`t calc cost for country. Country iso code: ' . $currentLocation->country);
        }
        $geoLocation = GeoLocationFactory::createByLocation($currentLocation);


        if ($model3dId) {
            // if model3dId setup directed
            $model3d    = Model3d::tryFindByPk($model3dId);
            $storeUnits = [$model3d->storeUnit->id];
        }

        foreach ($storeUnits as $storeUnitInfo) {
            if (is_array($storeUnitInfo)) {
                $storeUnitId     = $storeUnitInfo['id'];
                $storeUnit       = StoreUnit::tryFindByPk($storeUnitId);
                $customPrinterId = $storeUnitInfo['printerId'];
                $customPrinter   = PsPrinter::findByPk($customPrinterId);
            } else {
                $storeUnit     = StoreUnit::tryFindByPk($storeUnitInfo);
                $customPrinter = null;
            }

            if (!$customPrinter) {
                $cost = PriceCalculator::calculateModel3dCatalogCost($storeUnit, $geoLocation, $currencyIsoCode);
            } else {
                // Calculate cost for custom printer
                $largestPart = $storeUnit->model3d->getLargestPrintingModel3dPart();
                if ($largestPart) {
                    $size = $largestPart->getSize();
                    $cost = null;
                    if ($customPrinter->canPrintSize($size)) {
                        if (Yii::$app->deliveryService->canPrinterDeliveryToCountry($customPrinter->companyService->asDeliveryParams(), UserSessionFacade::getLocation()->country)) {
                            if (!$customPrinter->canPrintTextures(PrinterMaterialService::getModelTextures($storeUnit->model3d))) {
                                $cheapestTexture = $customPrinter->getMostCheapestTexture();
                                $storeUnit->model3d->setKitModel3dTexture($cheapestTexture);
                            }
                            Model3dService::resetTextureMatreialInfo($storeUnit->model3d);
                            Model3dService::setCheapestMaterialInPrinter($storeUnit->model3d, $customPrinter);
                            $cost = PriceCalculator::calculateModel3dAndStoreUnitPriceUsd($storeUnit->model3d, $customPrinter, true, false);
                            if ($cost !== null) {
                                $cost = PaymentExchangeRateFacade::convert($cost, Currency::USD, $currencyIsoCode);
                            }
                        }
                    }
                }

                if ($cost === null) {
                    $cost = PriceCalculator::calculateModel3dCatalogCost($storeUnit, $geoLocation, $currencyIsoCode);
                }
            }


            if ($cost !== null) {
                $cost = displayAsCurrency(round($cost, 2));
            }

            $cnt++;
            if ($cnt > self::MAX_PRICES_COUNT) {
                break;
            }
            $returnCosts[$storeUnit->id] = $cost;
        }
        return json_encode(
            [
                'success' => true,
                'costs'   => $returnCosts
            ],
            JSON_UNESCAPED_UNICODE
        );
    }
}