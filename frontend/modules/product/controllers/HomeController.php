<?php


namespace frontend\modules\product\controllers;


use common\models\ProductCategory;
use common\models\ProductMainCategory;
use common\models\ProductMainPromoBar;
use common\models\ProductMainSlider;
use common\models\PromoBar;
use common\modules\product\repositories\ProductMainCardRepository;

class HomeController extends \common\components\BaseController
{

    private ProductMainCardRepository $productMainCardRepository;

    public function __construct($id, $module, ProductMainCardRepository $productMainCardRepository, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->productMainCardRepository = $productMainCardRepository;
    }

    public function actionIndex()
    {
        $categories = ProductCategory::find()
            ->level1()
            ->notOthers()
            ->isActive()
            ->orderBy(['total_count' => SORT_DESC, 'title'=>SORT_ASC])
            ->limit(8)->all();

        $cards = $this->productMainCardRepository->groupCards();

        $productMainCategories = ProductMainCategory::find()->with(['productCategory'])->orderBy('id')->all();
        $sliders = ProductmainSlider::find()->all();
        $promoBar = ProductMainPromoBar::find()->one();
        return $this->render('index',[
            'categories' => $categories,
            'cards' => $cards,
            'productMainCategories' => $productMainCategories,
            'sliders' => $sliders,
            'promoBar' => $promoBar
        ]);
    }
}