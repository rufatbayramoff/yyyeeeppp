<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 26.07.18
 * Time: 13:49
 *
 * @var \common\models\ProductCategory[] $categories
 * @var string $currentTitle
 * @var string $allCategoriesTitle
 * @var int|bool $activeParentIdCategory
 * @var int|bool $activeIdCategory
 */

use yii\helpers\Url;
$showsAllCategories = ($activeIdCategory || $activeParentIdCategory || $activeParentParentIdCategory);
?>
<ul class="category-list__list">
    <?php if($showsAllCategories): ?>
        &laquo; <a href="/mybusiness/products"><?= H($allCategoriesTitle); ?></a>
    <?php endif; ?>
    <?php foreach ($categories as $category): ?>
        <li class="<?php echo ($category->id === $activeIdCategory ? 'active' : '');?>">
            <a href="<?= Url::toRoute(['/mybusiness/products', 'category_id' =>$category->id]); ?> ">
                <?php echo H($category->title) ?>
            </a>

            <?php if (in_array($category->id, [$activeIdCategory, $activeParentIdCategory, $activeParentParentIdCategory], false)): ?>
                <ul class="category-list__list">
                    <?php foreach ($category->productCategoriesOrderBy as $subCategory): ?>
                        <li class="<?php echo ($subCategory->id === $activeIdCategory) ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute(['/mybusiness/products', 'category_id' =>$subCategory->id]); ?> ">
                                <?php echo H($subCategory->title) ?>
                            </a>
                            <?php if (in_array($subCategory->id, [$activeIdCategory, $activeParentIdCategory, $activeParentParentIdCategory], false)): ?>
                                <ul class="category-list__list">
                                    <?php foreach ($subCategory->productCategoriesOrderBy as $subSubCategory): ?>
                                        <li class="<?php echo ($subSubCategory->id === $activeIdCategory) ? 'active' : ''; ?>">
                                            <a href="<?= Url::toRoute(['/mybusiness/products', 'category_id' =>$subSubCategory->id]); ?> ">
                                                <?php echo H($subSubCategory->title) ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

        </li>
    <?php endforeach; ?>
</ul>

