<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 26.07.18
 * Time: 13:49
 *
 * @var ProductCategory[] $categories
 * @var string $currentTitle
 * @var string $allCategoriesTitle
 * @var int|bool $activeParentIdCategory
 * @var int|bool $activeIdCategory
 */

use common\models\ProductCategory;
use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;
/** @var \common\modules\product\models\ProductSearchForm $searchModel */

$showsAllCategories = ($activeIdCategory || $activeParentIdCategory || $activeParentParentIdCategory);
?>
<ul class="category-list__list">
    <?php if ($showsAllCategories): ?>
        <a href="<?php echo ProductUrlHelper::productsCatalog(type: $searchModel->type, text: $searchModel->search)?>"><span class="tsi tsi-left category-list__arrow"></span><?= H($allCategoriesTitle); ?></a>
    <?php endif; ?>
    <?php foreach ($categories as $category): ?>
        <li class="<?php echo($category->id === $activeIdCategory ? 'active' : ''); ?>">

            <a href="<?php echo HL(ProductUrlHelper::productsCatalog($category->code, $searchModel->type, $searchModel->search)); ?>">
                <?php echo H($category->title) ?>
            </a>

            <?php if (in_array($category->id, [$activeIdCategory, $activeParentIdCategory, $activeParentParentIdCategory], false)): ?>
                <ul class="category-list__list">
                    <?php
                    $subCategories     = $category->getSubcategories(type: $searchModel->type, onlyVisible: true);
                    $showSubCategories = count($subCategories) > 1;
                    ob_start();
                    foreach ($subCategories as $subCategory) { ?>
                        <li class="<?php echo ($subCategory->id === $activeIdCategory) ? 'active' : ''; ?>">
                            <a href="<?php echo HL(ProductUrlHelper::productsCatalog($subCategory->code, $searchModel->type, $searchModel->search)); ?>">
                                <?php echo H($subCategory->title) ?>
                            </a>
                            <?php
                            $subSubCategories = $subCategory->getSubcategories(type: $searchModel->type, onlyVisible: true);
                            if (count($subSubCategories) > 1) {
                                $showSubCategories = true;
                            }
                            if ((count($subSubCategories) > 1) && in_array($subCategory->id, [$activeIdCategory, $activeParentIdCategory, $activeParentParentIdCategory], false)) { ?>
                                <ul class="category-list__list">
                                    <?php
                                    foreach ($subSubCategories as $subSubCategory) { ?>
                                        <li class="<?php echo ($subSubCategory->id === $activeIdCategory) ? 'active' : ''; ?>">
                                            <a href="<?php echo HL(ProductUrlHelper::productsCatalog($subSubCategory->code, $searchModel->type, $searchModel->search)); ?>">
                                                <?php echo $subSubCategory->title ?>
                                            </a>
                                        </li>
                                    <?php }
                                    ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php }
                    if ($showSubCategories) {
                        ob_end_flush();
                    } else {
                        ob_end_clean();
                    }
                    ?>
                </ul>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>

