<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 26.07.18
 * Time: 14:34
 *
 * @var \common\models\ProductCategory[] $categories
 * @var string $currentTitle
 * @var string $allCategoriesTitle
 */

use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;

/** @var \common\modules\product\models\ProductSearchForm $searchModel */

?>
<div class="dropdown header-bottom__categories t-header-bottom__categories">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="<?php echo $currentTitle; ?>">
        <span class="tsi tsi-list-1"></span><span class="header-bottom__categories-text"><?php echo $currentTitle; ?></span><span class="tsi tsi-down"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="<?php echo \yii\helpers\Url::to(['/product/product/index']); ?>" tabindex="-1"><?php echo $allCategoriesTitle; ?></a></li>
        <?php foreach ($categories as $category): ?>
            <li><a href="<?= HL(ProductUrlHelper::productsCatalog($category->code, $searchModel->type)); ?>" tabindex="-1"><?= H($category->title) ?></a></li>
        <?php endforeach; ?>
    </ul>
</div>