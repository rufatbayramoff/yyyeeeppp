<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 26.07.18
 * Time: 12:00
 */

namespace frontend\modules\product\widgets;

use common\models\ProductCategory;
use common\modules\product\interfaces\ProductInterface;
use common\modules\product\models\ProductSearchForm;
use yii\base\BaseObject;
use yii\base\Widget;

class ProductCategoryDropdownWidget extends Widget
{
    /**
     * @var string
     */
    public $layout;

    /**
     * @var ProductSearchForm
     */
    public $searchModel;

    /**
     * @var string
     */
    public $allCategoriesTitle;

    /**
     * @var string
     */
    public $currentTitle;

    /**
     * @var null|ProductCategory
     */
    public $activeCategory;

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->currentTitle) {
            $this->currentTitle = $this->allCategoriesTitle;
        }

        $activeIdCategory             = false;
        $activeParentIdCategory       = false;
        $activeParentParentIdCategory = false;
        if ($this->activeCategory) {
            $activeIdCategory       = $this->activeCategory->id;
            $activeParentIdCategory = $this->activeCategory->parent_id;
            if ($this->activeCategory->parent && $this->activeCategory->parent->parent_id) {
                $activeParentParentIdCategory = $this->activeCategory->parent->parent_id;
            }

        }

        return $this->render($this->getLayout(), [
            'allCategoriesTitle'           => $this->allCategoriesTitle,
            'currentTitle'                 => $this->currentTitle,
            'categories'                   => $this->getCategories(),
            'activeCategory'               => $this->activeCategory,
            'activeIdCategory'             => $activeIdCategory,
            'activeParentIdCategory'       => $activeParentIdCategory,
            'activeParentParentIdCategory' => $activeParentParentIdCategory,
            'searchModel'                  => $this->searchModel
        ]);
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return 'productCategoryDropdown/' . $this->layout;
    }

    /**
     * @return array|ProductCategory[]
     */
    public function getCategories(): array
    {
        $query = ProductCategory::find()
            ->active()
            ->visible()
            ->top()
            ->orderBy(['position' => SORT_ASC]);
        if ($this->searchModel && $this->searchModel->type==ProductInterface::TYPE_MODEL3D) {
             $query->has3dModels();
        }
        $allItems = $query->all();
        return $allItems;
    }
}