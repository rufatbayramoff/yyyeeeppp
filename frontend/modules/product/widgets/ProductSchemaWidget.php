<?php

/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 26.07.18
 * Time: 12:00
 */

namespace frontend\modules\product\widgets;

use common\models\ProductCommon;
use common\modules\product\interfaces\ProductInterface;
use frontend\controllers\catalog\actions\models\Rating;
use frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper;
use lib\money\Money;
use yii\base\Widget;
use yii\helpers\Html;

class ProductSchemaWidget extends Widget
{
    /**
     * @var ProductInterface
     */
    public $product;

    /**
     * @var Rating
     */
    public $rating;

    /**
     * @var Money
     */
    public $price;

    protected function getProductDescription()
    {
        $description = strip_tags($this->product->getDescription());
        $description = str_replace("\r\n", "\n", $description);
        $description = str_replace("\n\n", "\n", $description);
        $description = str_replace("\n\n", "\n", $description);
        if (strlen($description) > 1000) {
            $description = mb_substr($description, 1000) . '...';
        }
        if (!$description) {
            $description = 'Products from treatstock.com';
        }
        return $description;
    }

    /**
     * @return string
     */
    public function run()
    {
        $item = [
            '@context'    => 'https://schema.org',
            '@type'       => 'Product',
            'description' => $this->getProductDescription(),
            'name'        => $this->product->getTitleLabel(),
            'image'       => $this->product->getCoverUrl(),
        ];
        if ($this->rating && $this->rating->reviewsCount) {
            $item['aggregateRating'] = [
                "@type"       => "AggregateRating",
                "ratingValue" => $this->rating->rating,
                "reviewCount" => $this->rating->reviewsCount
            ];
        }
        if ($this->price) {
            $item['offers'] = [
                '@type'         => 'Offer',
                'availability'  => 'https://schema.org/InStock',
                'price'         => $this->price->getAmount(),
                'priceCurrency' => $this->price->getCurrency()
            ];
        }
        echo "\n" . Html::script(json_encode($item, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES), ['type' => 'application/ld+json']);
        $this->registerOgTags();
    }

    protected function registerOgTags()
    {

        $this->view->params['ogtags'] = [
            'og:url'         => $this->product->getPublicPageUrl(),
            'og:type'        => 'product',
            'og:description' => $this->getProductDescription(),
            'og:title'       => $this->product->getTitleLabel(),
            'og:image'       => $this->product->getCoverUrl(),
        ];
        if ($this->price) {
            $this->view->params['ogtags']['product:price:amount']   = $this->price->getAmount();
            $this->view->params['ogtags']['product:price:currency'] = $this->price->getCurrency();
        }
    }

}