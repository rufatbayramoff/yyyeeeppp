<?php
/**
 * User: nabi
 */

namespace frontend\modules\workbench;

use yii\base\Module;

class WorkbenchModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'frontend\modules\workbench\controllers';

    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();

        app()->view->headerSearchFormDisabled();
    }
}
