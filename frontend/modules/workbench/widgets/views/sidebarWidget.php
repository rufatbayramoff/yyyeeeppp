<?php

use lib\money\Money;
use yii\widgets\Menu;

/** @var Money[] $amountsNetIncome */
/** @var int $productionOrdersCount */
/** @var Money $productionOrdersProfit */
/** @var array $menuItems */

$this->registerJs('
    var c = $("#navPsRequest li.active a").html();    
    $(".js-section-title").html(c);
');
?>

<div class="col-sm-3 sidebar">

    <div class="side-nav-mobile">
        <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navPsRequest">
            <span class="js-section-title"><?= _t('site.ps', 'Orders Status') ?></span>
            <span class="tsi tsi-down"></span>
        </div>

        <?=
        Menu::widget([
            'options'      => ['class' => 'side-nav', 'id' => 'navPsRequest'],
            'encodeLabels' => false,
            'items'        => $menuItems,
        ]);
        ?>
    </div>

    <div class="designer-card designer-card--ps-cat m-b30 hidden-xs">
        <h3 class="designer-card__title">
            <?= _t('site.ps', 'Current Balance') ?>
        </h3>
        <div>
            <?php foreach ($amountsNetIncome as $amountNetIncomeItem) {
                echo '<strong>' . displayAsMoney($amountNetIncomeItem) . '</strong> &nbsp; ';
            } ?>
        </div>

        <hr class="m-t10 m-b10">

        <div class="designer-card__data">
                            <span class="designer-card__data-label">
                                <?= _t('site.ps', 'Incomplete orders') ?>:
                            </span>
            <?= $productionOrdersCount; ?>
        </div>

        <?php if ($productionOrdersProfit) { ?>
        <div class="designer-card__data m-b20">
                            <span class="designer-card__data-label">
                                <?= _t('site.ps', 'Pending Earnings') ?>:
                            </span>
            <?php
            foreach ($productionOrdersProfit as $productionOrdersProfitItem) {
                echo displayAsMoney($productionOrdersProfitItem) . ' &nbsp';
            } ?>

        </div>
        <?php } ?>

        <a href="/workbench/payments">
            <?= _t('site.ps', 'See more in Earnings') ?>
        </a>
    </div>

    <div class="hidden-xs">
        <p>
            <a href="/about/3d-printing-quality-policy"><span class="tsi tsi-warning-c m-r10"></span><?= _t('site.ps', 'Quality Policy for 3D Printing') ?></a>
        </p>

        <p>
            <a href="/help/article/121-helpful-tips-to-be-the-best-3d-print-service" target="_blank"><span
                        class="tsi tsi-question-c m-r10"></span><?= _t('site.ps', 'Helpful tips for 3D printing services') ?></a>
        </p>

        <p>
            <a href="/help/article/250-i-received-a-quote-what-should-i-do" target="_blank"><span
                        class="tsi tsi-question-c m-r10"></span><?= _t('site.ps', 'I received a Quote. What should I do?') ?></a>
        </p>
    </div>
</div>