<?php

namespace frontend\modules\workbench\widgets;

/**
 * User: nabi
 */

use common\modules\affiliate\helpers\AffiliateUrlHelper;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyInstantPaymentInformer;
use common\modules\informer\models\CompanyOrderInformer;
use common\modules\informer\models\CustomerOrderInformer;
use common\modules\informer\models\CustomerQuoteInformer;
use common\modules\informer\models\EarningInformer;
use common\modules\informer\models\ReviewInformer;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\user\UserFacade;
use Yii;
use yii\bootstrap\Tabs;

class WorkbenchTabsWidget extends \yii\bootstrap\Widget
{

    public $section = null;


    /**
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     */
    public function run()
    {
        $user                   = UserFacade::getCurrentUser();
        $activeUrl              = $this->section ?: Yii::$app->requestedRoute;
        $totalMessages          = \frontend\models\community\MessageFacade::getUnreadedTotal($user);
        $salesInformer          = InformerModule::getInformer($user, CompanyOrderInformer::class);
        $instantPaymentInformer = InformerModule::getInformer($user, CompanyInstantPaymentInformer::class);
        $ordersInformerCount    = ($salesInformer ? $salesInformer->getCount() : 0) + ($instantPaymentInformer ? $instantPaymentInformer->getCount() : 0);

        $purchaseInformer       = InformerModule::getInformer($user, CustomerOrderInformer::class);
        $customerQuotesInformer = InformerModule::getInformer($user, CustomerQuoteInformer::class);
        $purchaseInformerCount  = ($purchaseInformer ? $purchaseInformer->getCount() : 0) + ($customerQuotesInformer ? $customerQuotesInformer->getCount() : 0);
        $earningInformer        = InformerModule::getInformer($user, EarningInformer::class);
        $reviewInformer         = InformerModule::getInformer($user, ReviewInformer::class);

        $items = [];
        if ($user->company) {
            $items[] = [
                'label' => _t('front.user', 'My Sales') . ($ordersInformerCount ? ImageHtmlHelper::getRedDot($ordersInformerCount) : ''),
                'url'   => '/workbench/service-orders'
            ];
        }

        $items = array_merge($items, [
            [
                'label' => _t('front.user', 'My Purchases') . ($purchaseInformerCount ? ImageHtmlHelper::getRedDot($purchaseInformerCount) : ''),
                'url'   => '/workbench/orders'
            ],
            [
                'label' => _t('front.user', 'Earnings') . ($earningInformer ? $earningInformer->getAsRedDot() : ''),
                'url'   => '/workbench/payments'
            ],
            [
                'label' => _t('front.user', 'Sales Reviews') . ($reviewInformer ? $reviewInformer->getAsRedDot() : ''),
                'url'   => '/workbench/reviews'
            ]
        ]);
        if (!$user->company) {
            $items[] = [
                'label' => _t('front.user', 'Affiliate'),
                'url'   => AffiliateUrlHelper::sourcesList($user->company)
            ];
        }
        foreach ($items as $k => $item) {
            if (trim($item['url'], '/') == trim($activeUrl, '/')) {
                $item['active'] = true;
            }
            $items[$k] = $item;
        }

        if ($this->view->context->layout == '@frontend/views/layouts/plain') {
            return '<div class="over-nav-tabs-header"><div class="container"><h1>' . $this->view->title . '</h1></div></div>';
        }

        return '<div class="over-nav-tabs-header"><div class="container container--wide"><h1>' . _t('site.ps', 'Dashboard') . '</h1></div></div>
                <div class="nav-tabs__container"><div class="container container--wide">' . Tabs::widget(
                [
                    'renderTabContent' => false,
                    'options'          => [
                        'class' => '',
                        'role'  => 'tablist'
                    ],
                    'encodeLabels'     => false,
                    'items'            => $items
                ]
            ) . '</div></div>';
    }
}


