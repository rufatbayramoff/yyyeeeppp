<?php
/**
 * User: nabi
 */

namespace frontend\modules\workbench\widgets;


use common\models\InstantPayment;
use common\models\Preorder;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CustomerQuoteInformer;
use common\services\StoreOrderService;
use frontend\models\store\MyPurchasesForm;
use frontend\models\user\UserFacade;
use yii\base\Widget;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

class CustomerPurchasesSidebarWidget extends Widget
{
    public function run()
    {
        $links = $this->getMenuLinks();

        $items  = [];
        $curUrl = \Yii::$app->request->pathInfo;
        foreach ($links as $v) {
            $countText = '';
            if ($v['informerCount']) {
                if ($v['informerCount']!=$v['count']) {
                    $countText = "<span class='text-danger'>" . $v['informerCount'] . '</span> / '.$v['count'];
                } else {
                    $countText = "<span class='text-danger'>" . $v['informerCount'] . '</span>';
                }
            } elseif ($v['count']) {
                $countText = $v['count'];
            }

            $countLabel    = empty($v['count']) ? '' : Html::tag('span', $countText, ['class' => 'badge']);
            $item          = ['label' => $v['title'] . $countLabel, 'url' => [$v['url']]];
            $currentUrl    = \Yii::getAlias($v['url']);
            if ($pos = strpos($currentUrl, '?')) {
                $currentUrl = substr($currentUrl, 0, $pos);
            }
            if (trim($curUrl, '/') == trim($currentUrl, '/')) {
                $item['active'] = true;
            }
            $items[] = $item;
        }

        return \yii\widgets\Menu::widget(
            [
                'options'      => ['class' => 'side-nav', 'id' => 'navPurchases'],
                'encodeLabels' => false,
                'items'        => $items,
            ]
        );
    }

    public function getOrderCounts(User $user, array $tabs)
    {
        $tabsCount = array_fill_keys(array_keys($tabs), ['count' => 0, 'informerCount' => 0]);
        $tabs = array_merge_recursive($tabs, $tabsCount);

        $storeOrderStatusSql = StoreOrderAttemp::find()
            ->select(dbexpr('concat(ifnull(`store_order_attemp`.`order_id`,""),"_",ifnull(`store_order_attemp`.`preorder_id`,""))  as id, ANY_VALUE(`store_order`.`order_state`) as order_state, ANY_VALUE(`store_order_attemp`.`status`) as attempt_status, ANY_VALUE(`informer_customer_order`.`count`) as informer_count'))
            ->joinWith('preorder')
            ->joinWith('order')
            ->joinWith('order.informerCustomerOrders')
            ->andWhere(['or', ['store_order.user_id' => $user->id], ['preorder.user_id'=>$user->id]])
            ->andWhere(['or', ['<>', 'store_order.order_state', StoreOrder::STATE_REMOVED], '`store_order`.`order_state` is null'])
            ->groupBy('id')
            ->createCommand()
            ->getRawSql();
        $subSql = "SELECT concat(IFNULL(a.order_state,'') , '_', IFNULL(a.attempt_status,'')) as status, count(a.id) as count, sum(a.informer_count) as informer_count_sum FROM (" . $storeOrderStatusSql . ") a GROUP BY status";
        $ordersCountByStatus = \Yii::$app->getDb()->createCommand($subSql)->queryAll();

        foreach ($ordersCountByStatus as $v) {
            if (empty($v['status']) || ($v['status']=='_quote')) {
                continue;
            }
            $groupStatus             = StoreOrderService::getGroupCodeByOrderStatus($v['status']);
            $tabs[$groupStatus]['count'] += $v['count']??0;
            $tabs[$groupStatus]['informerCount'] += $v['informer_count_sum']??0;
        }

        // search quotes count
        $preordersCount           = Preorder::find()->newOrWaitConfirm()->forCustomer($user)->withoutCompany($user->company)->count();
        $tabs['quotes']['count'] = $preordersCount;
        $quoteInformer = InformerModule::getInformer($user, CustomerQuoteInformer::class);
        $tabs['quotes']['informerCount'] = $quoteInformer?$quoteInformer->getCount():0;

        $tabs['instant-payments']['count'] = InstantPayment::find()->fromUser($user)->notPayed()->count();
        return $tabs;
    }

    public static function getDefaultSideBar()
    {
        $widget = new self();
        $tabs = $widget->getMenuLinks();
        foreach ($tabs as $tab) {
            if ($tab['informerCount']) {
                return $tab['url'];
            }
        }
        foreach ($tabs as $tab) {
            if ($tab['count']) {
                return $tab['url'];
            }
        }
        return '/workbench/orders/production';
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function getMenuLinks(): array
    {
        $user        = UserFacade::getCurrentUser();
        $tabs       = [
            'quotes' => [
                'url' => '/workbench/orders/quotes?filter-by=active',
                'title'    => _t('front.user', 'Quotes / Invoices'),
            ],
            'new' => [
                'url' => '/workbench/orders/new',
                'title'    => _t('front.user', 'New Orders'),
            ],
            'production' => [
                'url' => '/workbench/orders/production',
                'title'    => _t('front.user', 'In Production'),
            ],
            'dispatched' => [
                'url'   => '/workbench/orders/dispatched',
                'title'    => _t('front.user', 'Dispatched'),
            ],
            'delivered' => [
                'url'   => '/workbench/orders/delivered',
                'title'    => _t('front.user', 'Delivered'),
            ],
            'completed' => [
                'title'    => _t('front.user', 'Completed'),
                'url' => '/workbench/orders/completed',
            ],
            'canceled' => [
                'title'    => _t('front.user', 'Canceled'),
                'url' => '/workbench/orders/canceled',
            ],
            'instant-payments' => [
                'title'    => _t('front.user', 'Instant payments'),
                'url' => '/workbench/instant-payments/send-list',
            ],
        ];
        $tabs = $this->getOrderCounts($user, $tabs);
        return $tabs;
    }
}
