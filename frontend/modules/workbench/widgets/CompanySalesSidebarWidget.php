<?php
/**
 * User: nabi
 */

namespace frontend\modules\workbench\widgets;


use common\models\Company;
use common\models\Preorder;
use common\models\Ps;
use common\models\StoreOrderAttemp;
use common\modules\instantPayment\helpers\InstantPaymentUrlHelper;
use common\modules\instantPayment\services\InstantPaymentService;
use common\modules\payment\services\PaymentAccountService;
use common\services\StoreOrderService;
use frontend\models\ps\PsFacade;
use frontend\modules\workbench\components\OrderServiceUrlHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class CompanySalesSidebarWidget
 * @package frontend\widgets
 */
class CompanySalesSidebarWidget extends Widget
{
    public const ACTIVE_STATUS_GROUP_QUOTE           = 'quote';
    public const ACTIVE_STATUS_GROUP_INSTANT_PAYMENT = 'instant_payment';


    /**
     * @var Company
     */
    public $ps;

    /**
     * Active status group (for selecting in menu)
     * May be null - it enable default Menu widget behavior
     * @var string|null
     */
    public $activeStatusGroup;

    /**
     * @var StoreOrderService
     */
    protected $orderService;

    /**
     * @var PaymentAccountService
     */
    protected $paymentAccountService;

    /** @var InstantPaymentService */
    protected $instantPaymentService;

    /**
     * @param StoreOrderService $orderService
     * @param PaymentAccountService $paymentService
     */
    public function injectDependencies(StoreOrderService $orderService, PaymentAccountService $paymentAccountService, InstantPaymentService $instantPaymentService)
    {
        $this->orderService          = $orderService;
        $this->paymentAccountService = $paymentAccountService;
        $this->instantPaymentService = $instantPaymentService;
    }


    /**
     * @return mixed
     */
    public function run()
    {
        $menuItems              = $this->getMenuLinks();
        $productionOrdersProfit = $this->orderService->getProductionOrdersPsProfit($this->ps);
        $productionOrdersCount  = $this->orderService->getCompanyProductionOrdersCount($this->ps);
        $amountsNetIncome       = $this->paymentAccountService->getUserMainAmounts($this->ps->user);

        return $this->render('sidebarWidget', [
            'menuItems'              => $menuItems,
            'productionOrdersProfit' => $productionOrdersProfit,
            'productionOrdersCount'  => $productionOrdersCount,
            'amountsNetIncome'       => $amountsNetIncome,
        ]);
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getMenuLinks()
    {
        // Ps orders
        $ordersCount             = $this->orderService->getOrdersCount($this->ps->user);
        $newInstantPaymentsCount = $this->instantPaymentService->getNewPaymentForCompany($this->ps);

        $statusGroupLabels = PsFacade::getCompanyOrdersFolders();

        $result = [];

        foreach ($statusGroupLabels as $statusGroup => $label) {
            $ordersCountTotal = $ordersCount[$statusGroup];
            $countText        = '';
            if ($ordersCountTotal['informerCount']) {
                if ($ordersCountTotal['informerCount'] != $ordersCountTotal['count']) {
                    $countText = "<span class='text-danger'>" . $ordersCountTotal['informerCount'] . '</span> / ' . $ordersCountTotal['count'];
                } else {
                    $countText = "<span class='text-danger'>" . $ordersCountTotal['informerCount'] . '</span>';
                }
            } elseif ($ordersCountTotal['count']) {
                $countText = $ordersCountTotal['count'];
            }

            $result[$statusGroup] = [
                'active'        => $this->activeStatusGroup !== null ? $statusGroup == $this->activeStatusGroup : null,
                'label'         => $label . ' &nbsp; ' . ' <span class="badge">' . $countText . '</span>',
                'url'           => OrderServiceUrlHelper::viewList($statusGroup),
                'informerCount' => $ordersCountTotal['informerCount'],
                'count'         => $ordersCountTotal['count'],
            ];
        }

        $ordersCountTotalUnpaid                            = $this->orderService->getUnpaidOrders($this->ps->user);
        $result[self::ACTIVE_STATUS_GROUP_INSTANT_PAYMENT] = [
            'active' => $this->activeStatusGroup === self::ACTIVE_STATUS_GROUP_INSTANT_PAYMENT,
            'label'  => _t('site.app', 'Instant payments') . ($newInstantPaymentsCount > 0 ? '<span class="badge"><span class="text-danger">' . $newInstantPaymentsCount . '</span>' : ''),
            'url'    => [InstantPaymentUrlHelper::getRecvList()]
        ];

        return $result;
    }

    public static function getDefaultSideBar(Company $company)
    {
        $widget     = Yii::createObject(self::class);
        $widget->ps = $company;
        $tabs       = $widget->getMenuLinks();
        foreach ($tabs as $tab) {
            if (!empty($tab['informerCount'])) {
                return $tab['url'];
            }
        }
        if (!empty($tabs[StoreOrderAttemp::STATUS_NEW]['count'])) { // Priority for new orders
            return $tabs[StoreOrderAttemp::STATUS_NEW]['url'];
        }
        foreach ($tabs as $tab) {
            if (!empty($tab['count'])) {
                return $tab['url'];
            }
        }
        return OrderServiceUrlHelper::viewList(StoreOrderAttemp::STATUS_NEW);
    }
}