<?php
/**
 * User: nabi
 */

namespace frontend\modules\workbench\controllers;

use common\components\ArrayHelper;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyInstantPaymentInformer;
use common\modules\informer\models\CompanyOrderInformer;
use common\modules\instantPayment\helpers\InstantPaymentUrlHelper;
use common\modules\instantPayment\services\InstantPaymentService;
use common\modules\payment\services\RefundService;
use common\services\StoreOrderService;
use common\traits\TransactedControllerTrait;
use frontend\models\ps\StoreOrderAttempSearchForm;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\workbench\components\OrderServiceUrlHelper;
use frontend\modules\workbench\widgets\CompanySalesSidebarWidget;
use Yii;
use yii\base\Module;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

class ServiceOrdersController extends WorkbenchController
{
    /**
     * @var InstantPaymentService
     */
    protected $instantPaymentService;

    /**
     * @var StoreOrderService
     */
    private $storeOrderService;


    public function injectDependencies(InstantPaymentService $instantPaymentService, StoreOrderService $storeOrderService)
    {
        $this->instantPaymentService = $instantPaymentService;
        $this->storeOrderService     = $storeOrderService;
    }

    /**
     * If workbench opened from top menu by clicking on Sales tab
     *
     * @param User $user
     * @return \yii\web\Response
     */
    protected function redirectToOrdersStatusGroupTab(User $user)
    {
        $url = CompanySalesSidebarWidget::getDefaultSideBar($user->company);
        return $this->redirect($url);
    }

    /**
     * @param null $statusGroup
     * @param null $id
     *
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionIndex($statusGroup = null, $id = null)
    {
        $user = $this->user;

        if ($user->status === User::STATUS_DRAFT_COMPANY) {
            $this->setFlashMsg(false, _t('mybusiness.common', 'Please confirm email first.'), false);
            return $this->redirect('/');
        }

        if (!$user->company || !empty($user->company->isDeleted())) {
            return $this->redirect(['/profile']);
        }

        if (!$statusGroup) {
            return $this->redirectToOrdersStatusGroupTab($user);
        }

        $form = new StoreOrderAttempSearchForm($user);
        $form->setAttributes(Yii::$app->request->get());
        $attemptsQuery = $form->getOrdersQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $attemptsQuery,
        ]);

        return $this->render(
            'list',
            [
                'statusGroup'  => $statusGroup,
                'form'         => $form,
                'dataProvider' => $dataProvider,
            ]
        );
    }

}
