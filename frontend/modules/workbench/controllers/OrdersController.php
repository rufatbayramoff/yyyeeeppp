<?php

namespace frontend\modules\workbench\controllers;

use common\models\Preorder;
use common\models\query\StoreOrderAttempQuery;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\ThingiverseUser;
use common\models\User;
use frontend\models\ps\StoreOrderAttempSearchForm;
use frontend\models\store\MyPurchasesForm;
use frontend\models\user\UserFacade;
use frontend\modules\workbench\widgets\CustomerPurchasesSidebarWidget;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * OrderController
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class OrdersController extends WorkbenchController
{
    protected $viewPath = '@frontend/modules/workbench/views/order';

    protected function redirectToOrdersList()
    {
        $sidebarDefaultUrl = CustomerPurchasesSidebarWidget::getDefaultSideBar();
        return $this->redirect($sidebarDefaultUrl);
    }

    /**
     * List of orders
     *
     * @param string $status
     * @param int $perPage
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionIndex($status = null)
    {
        $orderSearch = Yii::$app->request->get('orderSearch', false);
        if (empty($status) && $orderSearch === false) {
            return $this->redirectToOrdersList();
        }

        $form = new MyPurchasesForm();
        $form->load(Yii::$app->request->get());
        $form->statusGroup = $status;
        $form->user = $this->getCurrentUser();
        $dataProvider = $form->getPurchasesDataProvider();

        return $this->render(
            'list',
            [
                'form'         => $form,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
