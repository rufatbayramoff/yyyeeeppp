<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\workbench\controllers\cncm;

use common\components\BaseController;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\order\TestOrderFactory;
use common\components\serizaliators\Serializer;
use common\models\Company;
use common\models\factories\Model3dReplicaFactory;
use common\models\FileJob;
use common\models\PsCncMachine;
use common\models\repositories\Model3dReplicaRepository;
use common\models\repositories\Model3dRepository;
use common\models\StoreUnit;
use common\modules\cnc\api\Api;
use common\modules\cnc\api\Preset;
use common\modules\cnc\api\serializers\CostingSerializer;
use common\modules\cnc\api\serializers\ModelingSerializer;
use common\modules\cnc\components\CncMachineSerializer;
use common\modules\cnc\jobs\AnalyzeJob;
use common\modules\cnc\repositories\PsCncMachineRepository;
use common\services\Model3dService;
use console\jobs\QueueGateway;
use frontend\controllers\catalog\actions\serializers\Model3dSerializer;
use frontend\models\user\UserFacade;
use frontend\modules\workbench\services\CncBudgetService;
use Yii;
use yii\base\InvalidArgumentException;
use yii\caching\Cache;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class FilepageController
 *
 * @package frontend\controllers\cncm
 */
class FilepageController extends BaseController
{
    use CncResolveTrait;

    /**
     * @var Api
     */
    private $api;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var PsCncMachineRepository
     */
    private $psCncMachineRepository;

    /**
     * @var Serializer
     */
    private $modelSerializer;

    /**
     * @var Model3dService
     */
    protected $model3dService;

    /**
     * @var Model3dRepository
     */
    protected $model3dRepository;
    /**
     * @var CncBudgetService
     */
    protected $cncBudgetService;

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->enableCsrfValidation = false;
    }

    /**
     * @param Api $api
     * @param PsCncMachineRepository $psCncMachineRepository
     * @param Model3dService $model3dService
     * @param Model3dRepository $model3dRepository
     * @param CncBudgetService $cncBudgetService
     */
    public function injectDependencies(
        Api               $api, PsCncMachineRepository $psCncMachineRepository,
        Model3dService    $model3dService,
        Model3dRepository $model3dRepository,
        CncBudgetService  $cncBudgetService
    )
    {
        $this->api                    = $api;
        $this->cache                  = Yii::$app->cache;
        $this->psCncMachineRepository = $psCncMachineRepository;
        $this->modelSerializer        = new Serializer(new Model3dSerializer(false));
        $this->model3dService         = $model3dService;
        $this->model3dRepository      = $model3dRepository;
        $this->cncBudgetService       = $cncBudgetService;
    }

    public function actionTryCnc($machineId)
    {
        $machine = PsCncMachine::tryFindByPk($machineId);
        if ($machine->companyService->isDeleted()) {
            throw new ForbiddenHttpException(_t('site.ps', 'Is deleted cnc machine'));
        }
        if (!UserFacade::isObjectOwner($machine->companyService->company)) {
            throw new BusinessException('Only for owners');
        }
        $testStoreUnit  = StoreUnit::tryFindByPk(TestOrderFactory::getTestOrderStoreUnitId());
        $model3dReplica = Model3dReplicaFactory::createModel3dReplica($testStoreUnit->model3d);
        $model3dReplica->setStoreUnit($testStoreUnit);
        Model3dReplicaRepository::save($model3dReplica);
        $cncApi = Yii::createObject(Api::class);
        foreach ($model3dReplica->getActiveModel3dParts() as $model3dPart) {
            $upload                                                                    = $cncApi->upload($model3dPart->file);
            $model3dPartCncParams = $model3dPart->getOriginalModel3dPartObj()->model3dPartCncParams;
            $model3dPartCncParams->finefile  = $upload->getFilepath();
            $model3dPartCncParams->filepath  = $upload->getFilepath();
            $model3dPartCncParams->roughfile = $upload->getFilepath();
            $model3dPartCncParams->safeSave();
        }

        return $this->redirect('/workbench/cncm/filepage/index?modelId=' . $model3dReplica->getUid() . '&machineId=' . $machine->id);
    }

    /**
     * Open page with calculating prices
     *
     * @param int $modelId
     * @param null $machineId
     * @param null $psId
     * @param null $viewType
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionIndex($modelId, $machineId = null, $psId = null, $viewType = null)
    {
        if ($machineId) {
            $machine = PsCncMachine::tryFindByPk($machineId);
            if ($machine->companyService->isDeleted()) {
                throw new ForbiddenHttpException(_t('site.ps', 'Is deleted cnc machine'));
            }
        } elseif ($psId) {
            $machine = $this->findFirstPsCncMachine(Company::tryFindByPk($psId));
        } else {
            $machine = $this->resolveFirstPsCncMachine();
        }

        if (!$machine) {
            throw new ForbiddenHttpException(_t('site.ps', 'No CNC machine found'));
        }

        if ($viewType) {
            $this->layout = '@frontend/views/layouts/plain.php';
        }
        $model = $this->resloveModel($modelId);
        Yii::$app->angular
            ->constant('modelId', $model->getUid())
            ->constant('machineId', $machine->companyService->id);
        return $this->render("cnc-filepage", ['viewType' => $viewType, 'machine' => $machine]);
    }

    /**
     * Load model data
     *
     * @param int $modelId
     * @return array
     */
    public function actionModel($modelId)
    {
        $model = $this->resloveModel($modelId);
        return $this->jsonReturn($this->modelSerializer->serialize($model));
    }

    /**
     * @param $machineId
     * @return array
     */
    public function actionMachine($machineId)
    {
        $machine = $this->resolvePsCncMachine($machineId);
        return CncMachineSerializer::serialize($machine->psCnc);
    }

    /**
     * @param int $partId
     * @return array|null
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionCncParams($partId)
    {
        $part = $this->reslovePart($partId);
        if ($part->model3dPartCncParams) {
            return $this->modelSerializer->serialize($part->model3dPartCncParams);
        }

        /** @var FileJob $job */
        $job = FileJob::find()
            ->where(['file_id' => $part->file->id, 'operation' => FileJob::JOB_CNC_ANALYZE])
            ->orderBy('id DESC')
            ->one();

        if (!$job) {
            $aJob = AnalyzeJob::create($part->file);
            QueueGateway::addFileJob($aJob);
            return null;
        }

        if ($job->status == QueueGateway::JOB_STATUS_FAILED) {
            throw new InvalidArgumentException(_t('site.ps', "Bad {$part->format} file"));
        }

        return null;
    }

    /**
     * @param int $partId
     * @return string
     */
    public function actionLoadStl($partId)
    {
        $part = $this->reslovePart($partId);
        return $this->api->loadFile($part->model3dPartCncParams->finefile);
    }

    /**
     * @param int $partId
     * @param int $machineId
     * @return array
     */
    public function actionCosting($partId, $machineId)
    {
        $preset = new Preset();
        AssertHelper::assert($preset->load(Yii::$app->request->getBodyParams(), ''));
        AssertHelper::assertValidate($preset);
        $budget = $this->cncBudgetService->budget(
            $partId,
            $this->psCncMachineRepository->getMachineByCompanyService((int)$machineId),
            $preset
        );
        return CostingSerializer::serialize($budget);
    }

    /**
     * @param string $partId
     * @param int $machineId
     * @return array
     */
    public function actionModeling($partId, $machineId)
    {
        $cncParams = $this->reslovePart($partId)->model3dPartCncParams;

        $postData = Yii::$app->request->getBodyParams();

        $cacheKey = md5(serialize([$machineId, $partId, $postData]));

        if ($this->cache->exists($cacheKey)) {
            return ModelingSerializer::serialize($this->cache->get($cacheKey));
        }

        $costing = $this->api->createCostingResponse(['costingResult' => $postData['costing']]);

        $preset = new Preset();
        AssertHelper::assert($preset->load($postData['preset'], ''));
        AssertHelper::assertValidate($preset);

        $machine = $this->resolvePsCncMachine($machineId);

        $modelingResponse = $this->api->modeling(
            $cncParams->filepath,
            $cncParams->roughfile,
            $cncParams->finefile,
            $machine->resolveMachineUri(),
            $cncParams->analyze,
            $preset,
            $costing
        );

        $this->cache->set($cacheKey, $modelingResponse, 3600);

        return ModelingSerializer::serialize($modelingResponse);
    }


    /**
     * @param $uri
     * @return string
     * @internal param int $partId
     */
    public function actionLoadModelingFile($uri)
    {
        return $this->api->loadFile($uri);
    }
}

