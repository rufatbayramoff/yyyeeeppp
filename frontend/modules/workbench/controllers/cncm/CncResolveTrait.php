<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\workbench\controllers\cncm;


use common\components\ArrayHelper;
use common\interfaces\Model3dBaseInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\Company;
use common\models\CompanyService;
use common\models\Model3dPart;
use common\models\Ps;
use common\models\PsCncMachine;
use common\models\repositories\Model3dRepository;
use common\modules\cnc\components\CncPreorderAccess;
use common\modules\cnc\repositories\PsCncMachineRepository;
use common\services\Model3dService;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Trait CncResolveTrait
 * @property Model3dService $model3dService
 * @package frontend\controllers\cncm
 */
trait CncResolveTrait
{

    /**
     * @var PsCncMachineRepository
     */
    private $psCncMachineRepository;

    /** @var Model3dRepository */
    protected $model3dRepository;

    /**
     * Find model and check access
     *
     * @param string $modelId
     * @return Model3dBaseInterface
     * @throws ForbiddenHttpException
     */
    private function resloveModel(string $modelId): Model3dBaseInterface
    {
        $model3d = $this->model3dRepository->getByUid($modelId);
        $model3d->tryValidateInactive();
        if (!$this->model3dService->isAvailableForCurrentUser($model3d)) {
            throw new ForbiddenHttpException('Cant access this model.');
        }
        return $model3d;
    }

    /**
     * @param string $partId
     * @return Model3dBasePartInterface
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    private function reslovePart(string $partId) : Model3dBasePartInterface
    {
        /** @var Model3dPart $part */
        $part = $this->model3dRepository->getPartByUid($partId);

        if (!$part) {
            throw new NotFoundHttpException("Part {$partId} not found");
        }

        if (!$this->model3dService->isAvailableForCurrentUser($part->model3d)) {
            throw new ForbiddenHttpException('Cant access this model.');
        }

        return $part;
    }

    /**
     * @param $psMachineId
     * @return PsCncMachine
     * @throws NotFoundHttpException
     */
    private function resolvePsCncMachine($psMachineId) : PsCncMachine
    {
        $psMachine = CompanyService::tryFindByPk($psMachineId);

        if (!$psMachine->ps_cnc_machine_id) {
            throw new NotFoundHttpException();
        }

        if ($psMachine->is_deleted) {
            throw new NotFoundHttpException();
        }

        $psCncMachine = $this->psCncMachineRepository->getByPsMachine($psMachine);

        return $psCncMachine;
    }

    /**
     * @return PsCncMachine|null
     */
    private function resolveFirstPsCncMachine() : ?PsCncMachine
    {
        $ps = PsFacade::getPsByUserId(UserFacade::getCurrentUserId());

        if (!$ps) {
            return null;
        }
        return $this->findFirstPsCncMachine($ps);

    }

    /**
     * @param Ps|Company $ps
     * @return PsCncMachine|null
     * @throws NotFoundHttpException
     */
    private function findFirstPsCncMachine(Ps|Company $ps): ?PsCncMachine
    {
        /** @var PsCncMachine $psCncMachine */
        $psCncMachine = ArrayHelper::first($this->psCncMachineRepository->getNotDeletedByPs($ps));

        if (!$psCncMachine) {
            return null;
        }

        return $this->resolvePsCncMachine($psCncMachine->companyService->id);
    }
}