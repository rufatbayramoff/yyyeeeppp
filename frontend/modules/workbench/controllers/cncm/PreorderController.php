<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\workbench\controllers\cncm;

use common\components\BaseController;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\models\factories\FileFactory;
use common\modules\cnc\components\ArrayToObjectMapper;
use common\modules\cnc\components\CncPreorderAccess;
use common\modules\cnc\models\preorder\PreorderForm;
use common\services\Model3dService;
use common\traits\TransactedControllerTrait;
use frontend\components\angular\AngularService;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use frontend\modules\preorder\components\PreorderFactoryForCnc;
use Yii;
use yii\filters\AccessControl;

/**
 * Class PreorderController
 * @package frontend\modules\workbench\controllers\cncm
 *
 * @property PreorderFactoryForCnc $preorderFactoryForCnc
 */
class PreorderController extends BaseController
{
    use TransactedControllerTrait;
    use CncResolveTrait;

    /** @var PreorderFactoryForCnc */
    protected $preorderFactoryForCnc;

    /**
     * @var AngularService
     */
    private $angular;

    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var CncPreorderAccess
     */
    private $cncAccess;

    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @var Model3dService
     */
    protected $model3dService;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'  => AccessControl::class,
                'except' => ['create'],
                'rules'  => [['allow' => true, 'roles' => ['@']]]
            ],
        ];
    }

    /**
     * @param PreorderFactoryForCnc $cncPreorderFactory
     * @param FileFactory $fileFactory
     * @param CncPreorderAccess $cncAccess
     * @param PreorderFactoryForCnc $preorderFactoryForCnc
     * @param Model3dService $model3dService
     * @param Emailer $emailer
     */
    public function injectDependencies(
        PreorderFactoryForCnc $preorderFactoryForCnc,
        FileFactory $fileFactory,
        CncPreorderAccess $cncAccess,
        Model3dService $model3dService,
        Emailer $emailer
    )
    {
        $this->preorderFactoryForCnc = $preorderFactoryForCnc;
        $this->fileFactory        = $fileFactory;
        $this->cncAccess          = $cncAccess;
        $this->request            = Yii::$app->request;
        $this->response           = Yii::$app->response;
        $this->angular            = Yii::$app->angular;
        $this->model3dService     = $model3dService;
        $this->emailer            = $emailer;
    }

    /**
     * Create preorder
     *
     * @return array
     *
     * @transacted
     */
    public function actionCreate()
    {
        $form = new PreorderForm();
        ArrayToObjectMapper::map($form, $this->request->getBodyParams());
        AssertHelper::assertValidate($form);
        if (is_guest()) {
            if (empty($form->email)) {
                return $this->jsonError('Email is required');
            }
            $validator = new \yii\validators\EmailValidator(['message' => _t('site.promo', 'Please enter a valid email')]);
            if (!$validator->validate($form->email, $error)) {
                return $this->jsonError($error);
            }

            $customerUser = FrontUser::findOne(['email' => $form->email]);
            if ($customerUser) {
            } else {
                // if new user
                $password     = Yii::$app->security->generateRandomString(8);
                $customerUser = UserFacade::createUser(
                    [
                        'email'    => $form->email,
                        'password' => $password,
                    ]
                );
                $this->emailer->sendSignupEmail($customerUser, $password);
                AssertHelper::assert(Yii::$app->user->login($customerUser));
            }
        } else {
            $customerUser = UserFacade::getCurrentUser();
        }

        $preorder = $this->preorderFactoryForCnc->createPreorder($customerUser, $form);

        return ['id' => $preorder->id];
    }


    /**
     * View and edit preorder on ps-side
     *
     * @param $preorderId
     * @return string
     */
    public function actionView($preorderId)
    {
        $preorder = $this->resolvePreorder($preorderId);
        $this->cncAccess->tryCanView($preorder, $this->getCurrentUser());
        $this->angular
            ->constant('preorderId', $preorder->id)
            ->constant('machineId', $preorder->machine_id)
            ->constant('isCustomer', false);

        return $this->render('cnc-preorder-ps', ['preorder' => $preorder]);
    }


    /**
     * View and edit preorder on ps-side
     *
     * @param $preorderId
     * @return string
     */
    public function actionViewCustomer($preorderId)
    {
        $preorder = $this->resolvePreorder($preorderId);
        $this->cncAccess->tryCanView($preorder, $this->getCurrentUser());
        $this->angular
            ->constant('preorderId', $preorder->id)
            ->constant('machineId', $preorder->machine_id)
            ->constant('isCustomer', true);

        return $this->render('cnc-preorder-customer');
    }

}