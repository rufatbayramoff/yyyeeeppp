<?php
/**
 * User: nabi
 */

namespace frontend\modules\workbench\controllers;

use common\components\exceptions\BusinessException;
use common\models\StoreOrderReview;
use common\services\ReviewService;
use frontend\components\AuthedController;
use frontend\models\ps\PsFacade;
use frontend\models\review\StoreOrderReviewSearch;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\UserException;

/**
 * Class ReviewsController
 * @package frontend\modules\workbench\controllers
 *
 * @property ReviewService $reviewService
 */
class ReviewsController extends AuthedController
{
    public $ps;

    protected $reviewService;

    public function injectDependencies(
        ReviewService $reviewService
    ) {
        $this->reviewService = $reviewService;
    }

    public function actionIndex()
    {
        $this->ps = PsFacade::getPsByUserId(UserFacade::getCurrentUserId());

        if (!$this->ps) {
            return $this->render('reviewsEmpty.php');
        }

        /** @var StoreOrderReviewSearch $storeOrderReviewSearch */
        $storeOrderReviewSearch = Yii::createObject(StoreOrderReviewSearch::class);
        $storeOrderReviewSearch->forPs = $this->ps;

        $reviewsProvider = $storeOrderReviewSearch->search(app()->request->get());

        return $this->render('reviews.php', [
            'ps'                     => $this->ps,
            'reviewsProvider'        => $reviewsProvider,
            'storeOrderReviewSearch' => $storeOrderReviewSearch
        ]);
    }

    /**
     * @param $review_id
     *
     * @return string|\yii\web\Response
     * @throws BusinessException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionAnswerReview($review_id)
    {
        $storeOrderReview = StoreOrderReview::tryFindByPk($review_id, _t('site.review', 'Review not found'));

        if (!$storeOrderReview->isCanShow() || !$storeOrderReview->isOwnCompanyToCurrentUser()) {
            throw new BusinessException(_t('site.review', 'No access to this review.'));
        }

        $request = app()->request;

        if ($request->isPost) {
            $storeOrderReview->setScenario(StoreOrderReview::SCENARIO_ANSWER);
            $storeOrderReview->load($request->post());
            $storeOrderReview->verified_answer_status = StoreOrderReview::ANSWER_STATUS_NEW;
            $storeOrderReview->safeSave();

            $this->reviewService->sendClientReviewAnswer($storeOrderReview);

            return $this->redirect($request->referrer.'#user-review-'.$storeOrderReview->id);
        }

        return $this->renderPartial('review-answer-form', [
            'storeOrderReview' => $storeOrderReview
        ]);
    }
}