<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\workbench\controllers;

use common\components\BaseController;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\DeprecatedException;
use common\components\serizaliators\Serializer;
use common\models\Company;
use common\models\loggers\PreorderLogger;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\PaymentInvoice;
use common\models\PaymentTransaction;
use common\models\Preorder;
use common\models\query\PreorderQuery;
use common\models\StoreOrder;
use common\models\User;
use common\models\user\UserIdentityProvider;
use common\modules\informer\InformerModule;
use common\modules\message\services\MessageNotifyService;
use common\modules\message\services\MessageService;
use common\modules\payment\components\PaymentUrlHelper;
use common\modules\payment\factories\PaymentInvoiceListFactory;
use common\modules\payment\fee\FeeHelper;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\services\ProductService;
use common\modules\quote\services\QuoteService;
use common\traits\TransactedControllerTrait;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use frontend\modules\preorder\components\orderfactory\PreorderOrderFactory;
use frontend\modules\preorder\components\PreorderAccess;
use frontend\modules\preorder\components\PreorderEmailer;
use frontend\modules\preorder\components\PreorderFactory;
use frontend\modules\preorder\components\PreorderForm;
use frontend\modules\preorder\components\PreorderResolver;
use frontend\modules\preorder\components\PreorderSerializer;
use frontend\modules\preorder\components\PreorderService;
use frontend\modules\preorder\components\PreorderUpdater;
use frontend\modules\preorder\components\ServicesPreorderDeclineForm;
use lib\money\Money;
use Yii;
use yii\base\InvalidArgumentException;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class PreorderController
 *
 * @package frontend\controllers\preorder
 *
 * @property PaymentInvoiceListFactory $paymentInvoiceListFactory
 * @property PaymentInvoiceRepository $paymentInvoiceRepository
 * @property PreorderService $preorderService
 */
class PreorderController extends BaseController
{
    use TransactedControllerTrait;


    /**
     * @var PreorderFactory
     */
    private $preorderFactory;

    /**
     * @var PreorderResolver
     */
    private $resolver;

    /**
     * @var PreorderAccess
     */
    private $access;

    /**
     * @var PreorderUpdater
     */
    private $updater;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var PreorderOrderFactory
     */
    private $orderFactory;

    /**
     * @var PreorderEmailer
     */
    private $emailer;

    /**
     * @var PreorderLogger
     */
    private $logger;

    /**
     * @var User
     */
    private $user;

    /**
     * @var PreorderService
     */
    private $service;

    /**
     * @var MessageService
     */
    protected $messageService;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var ProductService
     */
    protected $productService;

    protected $paymentInvoiceListFactory;

    protected $paymentInvoiceRepository;

    protected $preorderService;

    /** @var QuoteService */
    protected $quoteService;

    /** @var MessageNotifyService */
    protected $messageNotifyService;

    /**
     * @param PreorderFactory $factory
     * @param PreorderResolver $resolver
     * @param PreorderAccess $access
     * @param PreorderUpdater $updater
     * @param PreorderSerializer $serializer
     * @param PreorderOrderFactory $orderFactory
     * @param PreorderEmailer $emailer
     * @param PreorderLogger $logger
     * @param UserIdentityProvider $userProvider
     * @param PreorderService $service
     * @param MessageService $messageService
     * @param ProductRepository $productRepository
     * @param ProductService $productService
     * @param PaymentInvoiceRepository $paymentInvoiceRepository
     * @param PreorderService $preorderService
     * @param PaymentInvoiceListFactory $paymentInvoiceListFactory
     * @param QuoteService $quoteService
     */
    public function injectDependencies(
        PreorderFactory           $factory,
        PreorderResolver          $resolver,
        PreorderAccess            $access,
        PreorderUpdater           $updater,
        PreorderSerializer        $serializer,
        PreorderOrderFactory      $orderFactory,
        PreorderEmailer           $emailer,
        PreorderLogger            $logger,
        UserIdentityProvider      $userProvider,
        PreorderService           $service,
        MessageService            $messageService,
        ProductRepository         $productRepository,
        ProductService            $productService,
        PaymentInvoiceRepository  $paymentInvoiceRepository,
        PreorderService           $preorderService,
        PaymentInvoiceListFactory $paymentInvoiceListFactory,
        QuoteService              $quoteService,
        MessageNotifyService      $messageNotifyService
    )
    {
        $this->user                      = $userProvider->getUser();
        $this->request                   = \Yii::$app->request;
        $this->response                  = \Yii::$app->response;
        $this->preorderFactory           = $factory;
        $this->resolver                  = $resolver;
        $this->access                    = $access;
        $this->updater                   = $updater;
        $this->serializer                = new Serializer($serializer);
        $this->orderFactory              = $orderFactory;
        $this->emailer                   = $emailer;
        $this->logger                    = $logger;
        $this->service                   = $service;
        $this->messageService            = $messageService;
        $this->productRepository         = $productRepository;
        $this->productService            = $productService;
        $this->paymentInvoiceListFactory = $paymentInvoiceListFactory;
        $this->paymentInvoiceRepository  = $paymentInvoiceRepository;
        $this->preorderService           = $preorderService;
        $this->quoteService              = $quoteService;
        $this->messageNotifyService      = $messageNotifyService;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['create-form', 'create'],
                        'roles'   => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function actionQuotes()
    {
        $ps = PsFacade::getPsByUserId($this->user->id);

        /** @var PreorderQuery $preordersQuery */
        $preordersQuery = Preorder::find()
            ->notPaid()
            ->inNewProcess()
            ->addOrderBy([
                Preorder::column('id') => SORT_DESC
            ]);
        if ($ps->isCustomerServiceCompany()) {
            $preordersQuery->isInterceptionOrForMachineIdOrCompany($ps);
        } else {
            $preordersQuery->isNotInterception()->forPs($ps);
        }

        $filterBy    = app('request')->get('filter-by', null);
        $orderSearch = app('request')->get('orderSearch', null);

        if (!empty($orderSearch)) {
            $preordersQuery
                ->joinWith('ps')
                ->joinWith('user')
                ->joinWith('user.userProfile')
                ->andWhere(
                    'preorder.id=:orderSearch 
                    OR user.username LIKE :orderSearchLike 
                    OR user_profile.firstname LIKE :orderSearchLike 
                    OR user_profile.lastname LIKE :orderSearchLike 
                    OR preorder.description LIKE :orderSearchLike
                    OR preorder.message LIKE :orderSearchLike
                    ',
                    [':orderSearch' => $orderSearch, ':orderSearchLike' => '%' . $orderSearch . '%']);
        }

        if (\in_array($filterBy, [
            Preorder::FILTER_BY_NEW,
            Preorder::FILTER_BY_SUBMITTED,
            Preorder::FILTER_BY_DECLINED,
            Preorder::FILTER_BY_DRAFT,
            Preorder::FILTER_BY_ACTIVE,
        ], false)
        ) {
            switch ($filterBy) {
                case Preorder::FILTER_BY_NEW:
                    $preordersQuery
                        ->new()
                        ->andWhere('preorder.primary_payment_invoice_uuid is null');
                    break;
                case Preorder::FILTER_BY_SUBMITTED:
                    $preordersQuery
                        ->submitted()
                        ->andWhere('preorder.primary_payment_invoice_uuid is not null');
                    break;
                case Preorder::FILTER_BY_DECLINED:
                    $preordersQuery->rejected();
                    break;
                case Preorder::FILTER_BY_DRAFT:
                    $preordersQuery->draft();
                    break;
                case Preorder::FILTER_BY_ACTIVE:
                    $preordersQuery->new();
                    break;
            }
        }

        $preordersQuery = $preordersQuery->all();

        return $this->render(
            'quotes-list',
            [
                'filterBy'    => $filterBy,
                'orderSearch' => $orderSearch,
                'ps'          => $ps,
                'preorders'   => $preordersQuery,
            ]
        );
    }

    /**
     * @param $preorderId
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\ExitException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionViewPs($preorderId)
    {
        $preorder = $this->resolver->resolvePreorderPs($preorderId, $this->getCurrentUser());
        return $this->render('view-ps', ['preorder' => $preorder]);
    }

    /**
     * @param $preorderId
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\ExitException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionEditByPs($preorderId)
    {
        $preorder = $this->resolver->resolvePreorderPs($preorderId, $this->getCurrentUser());
        if (!$this->access->isPs($preorder, $this->getCurrentUser())) {
            return $this->redirect('/workbench/preorder/view-customer?preorderId=' . $preorder->id);
        }
        $this->access->tryIsPs($preorder, $this->getCurrentUser());

        if ($preorder->status === Preorder::STATUS_ACCEPTED) {
            $order = StoreOrder::find()
                ->forPreorder($preorder)
                ->orderByIdDesc()
                ->one();

            if ($order && $order->currentAttemp && $order->isPayed()) {
                $this->redirect('/workbench/service-order/view?id=' . $order->id);
            } else {
                $this->redirect('/workbench/preorder/view-ps?preorderId=' . $preorderId);
            }
        }

        return $this->render('edit-created-by-ps', ['currentUser'=>$this->getCurrentUser(), 'preorder' => $preorder]);
    }

    /**
     * @return string
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCreateByPs()
    {
        // /workbench/preorder/create-by-ps
        /** @var Company $currentPs */
        $currentPs = $this->getCurrentUser()->ps;
        if (!$currentPs) {
            throw  new InvalidArgumentException('Invalid current ps');
        }
        $currentPs = reset($currentPs);
        if (!$currentPs->isActive()) {
            throw  new BusinessException('Company status is not active');
        }
        if ($currentPs->isMaxQuotesCreated()) {
            $this->setFlashMsg(false, _t('front.site', 'Quote limits exceeded') . ' (' . $currentPs->getQuotesLimit() . _t('front.site', ' per day') . ')');
            return $this->redirect(\Yii::$app->request->referrer);
        }

        $customer = User::findOne(['username' => \Yii::$app->request->get('customerUsername')]);
        if ($customer && !$customer->getIsActive()) {
            throw  new BusinessException('Customer is not active');
        }

        $form = new PreorderForm();
        $form->initForPs($currentPs);
        if ($customer) {
            $form->user = $customer->id;
        }
        $preorder = $this->preorderFactory->create($form);

        return $this->redirect(Url::toRoute(['edit-by-ps', 'preorderId' => $preorder->id]));
    }


    /**
     * @param $preorderId
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionViewCustomer($preorderId)
    {
        $preorder = $this->resolver->resolvePreorder($preorderId, $this->getCurrentUser());

        $order = StoreOrder::find()
            ->forPreorder($preorder)
            ->orderByIdDesc()
            ->payed()
            ->one();

        if ($order) {
            $this->setFlashMsg(true, _t('site.order', 'Order #{orderId} created for this quote. {link}', [
                'orderId' => $order->id,
                'link'    => Html::a(_t('site.order', 'Click to visit'), '/workbench/order/view/' . $order->id)
            ]));
        }
        InformerModule::markAsViewedCustomerQuoteInformer($preorder);

        return $this->render('preorder-view', [
            'preorder'     => $preorder,
            'preorderUser' => $preorder->user
        ]);
    }

    /**
     *
     * @param $preorderId
     *
     * @return array
     *
     * @throws ForbiddenHttpException
     * @throws \yii\base\ExitException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     * @transacted
     */
    public function actionAddWork($preorderId)
    {
        $preorder = $this->resolver->resolvePreorder($preorderId, $this->getCurrentUser());
        $this->access
            ->tryCanEdit($preorder, $this->getCurrentUser());

        $work = $this->updater->addWork($preorder, $this->request);

        return $this->serializer->serialize($work);
    }

    /**
     *
     * @param $preorderId
     * @param $workId
     *
     * @return array
     *
     * @throws ForbiddenHttpException
     * @throws \yii\base\ExitException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     * @transacted
     */
    public function actionUpdateWork($preorderId, $workId)
    {
        $preorder = $this->resolver->resolvePreorder($preorderId, $this->getCurrentUser());
        $this->access
            ->tryCanEdit($preorder, $this->getCurrentUser());

        $work = $this->resolver->resolveWork($workId, $preorder);
        $this->updater->updateWork($preorder, $work, $this->request);

        return $this->serializer->serialize($work);
    }

    /**
     *
     * @param int $preorderId
     * @param int $workId
     *
     * @throws ForbiddenHttpException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\UserException
     * @transacted
     */
    public function actionDeleteWork($preorderId, $workId)
    {
        $preorder = $this->resolver->resolvePreorder($preorderId, $this->getCurrentUser());
        $this->access
            ->tryCanEdit($preorder, $this->getCurrentUser());

        $work = $this->resolver->resolveWork($workId, $preorder);
        $this->updater->deleteWork($preorder, $work);
    }

    /**
     * @param $preorderId
     * @param $fileId
     *
     * @throws ForbiddenHttpException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDownloadPreorderFile($preorderId, $fileId)
    {
        $preorder = $this->resolver->resolvePreorder($preorderId, $this->getCurrentUser());
        $file     = $this->resolver->resolveFile($fileId, $preorder);
        $this->response->sendFile($file->getLocalTmpFilePath(), $file->getFileName());
    }

    /**
     * @param $preorderId
     * @param $workId
     * @param $fileId
     * @throws ForbiddenHttpException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDownloadWorkFile($preorderId, $workId, $fileId)
    {
        $preorder = $this->resolver->resolvePreorder($preorderId, $this->getCurrentUser());
        $work     = $this->resolver->resolveWork($workId, $preorder);
        $file     = $this->resolver->resolveWorkFile($fileId, $work);
        $this->response->sendFile($file->getLocalTmpFilePath(), $file->getFileName());
    }

    /**
     *
     * @return array
     * @throws ForbiddenHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSaveDraft()
    {
        $preorderForm = \Yii::$app->request->post();
        $preorderId   = $preorderForm['Preorder']['id'] ?? 0;
        $preorder     = $this->resolver->resolvePreorder($preorderId, $this->getCurrentUser());

        $this->access
            ->tryCanEdit($preorder, $this->getCurrentUser())
            ->tryIsDraft($preorder);

        $preorder->load($preorderForm);
        $preorder->changeStatus(Preorder::STATUS_DRAFT);
        $this->logger->log($preorder, 'save_draft');
        $preorder->safeSave();
        return $this->serializer->serialize($preorder);
    }

    public function actionWithdrawOffer()
    {
        $preorderForm = \Yii::$app->request->post();
        $preorderId   = $preorderForm['Preorder']['id'] ?? 0;
        $preorder     = $this->resolver->resolvePreorder($preorderId, $this->getCurrentUser());

        $this->access
            ->tryCanEdit($preorder, $this->getCurrentUser())
            ->tryIsNotAccepted($preorder);

        $preorder->load($preorderForm);
        $preorder->withdrawOffer();
        $this->logger->log($preorder, 'withdraw_offer');
        $preorder->safeSave();
        return $this->serializer->serialize($preorder);
    }

    /**
     * @param $preorderId
     *
     * @return array
     * @throws ForbiddenHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \common\modules\payment\exception\PaymentException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionMakeOffer()
    {
        $preorderForm = \Yii::$app->request->post();
        $preorderId   = $preorderForm['Preorder']['id'] ?? 0;
        $preorder     = $this->resolver->resolvePreorder($preorderId, $this->getCurrentUser());

        $this->access
            ->tryCanEdit($preorder, $this->getCurrentUser());

        $preorder->load($preorderForm);
        $preorder->changeStatus(Preorder::STATUS_WAIT_CONFIRM);

        if (!$preorder->user) {
            $this->preorderService->formPreorderClient($preorder);
        }

        if ($preorder->user && !$preorder->user->allow_incoming_quotes) {
            throw new BusinessException('Client forbids sending quotes');
        }
        if (!$preorder->validate()) {
            return $this->jsonReturn([
                'success'          => false,
                'validationErrors' => $preorder->getErrors()
            ], 406);
        }
        $this->logger->log($preorder, 'make_offer');
        $preorder->safeSave();

        $paymentInvoices = $this->paymentInvoiceListFactory->createWithPaymentMethodsByPreorder($preorder);
        $this->paymentInvoiceRepository->saveInvoicesArray($paymentInvoices);

        /** @var PaymentInvoice $primaryInvoice */
        $primaryInvoice = $this->paymentInvoiceRepository->getNewInvoiceByPreorderAndPaymentMethod(
            $preorder,
            PaymentTransaction::VENDOR_BRAINTREE
        );

        $preorder->primary_payment_invoice_uuid = $primaryInvoice->uuid;
        $preorder->safeSave();

        $this->emailer->customerPsSendOffer($preorder);
        if (!$preorder->is_client_created) {
            InformerModule::addCustomerQuoteInformer($preorder);
        }
        if ($preorder->isCreatedByPs()) {
            $this->messageNotifyService->notifyNewPreorderFromPs($preorder);
        }
        return $this->serializer->serialize($preorder);
    }

    /**
     * @param $preorderId
     *
     * @return Response
     * @throws ForbiddenHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionAcceptOffer($preorderId)
    {
        $user = $this->getCurrentUser();

        $preorder          = $this->resolver->resolvePreorder($preorderId, $user);
        $failedRedirectUrl = '/workbench/orders/quotes?filter-by=active';

        if ($preorder->isRejected()) {
            $this->setFlashMsg(false, _t('site.order', 'Quote was canceled'));
            return $this->redirect($failedRedirectUrl);
        }

        if ($preorder->isDraft() || $preorder->isNew()) {
            $this->setFlashMsg(false, _t('site.order', 'Manufacturer withdraw offer'));
            return $this->redirect($failedRedirectUrl);
        }

        if (!$preorder->offered) {
            $this->setFlashMsg(false, _t('site.order', 'The manufacturer is not ready to make an offer'));
            return $this->redirect($failedRedirectUrl);
        }

        if ($preorder->isAccepted()) {
            $this->setFlashMsg(false, _t('site.order', 'Already accepted'));
            return $this->redirect($failedRedirectUrl);
        }

        $this->access->tryIsCustomer($preorder, $user);

        $preorder->accept();
        $this->logger->accept($preorder);
        $preorder->safeSave();

        $order = $this->orderFactory->create($preorder);
        $this->messageService->rebindMessageTopic($preorder, $order);

        return $this->redirect(PaymentUrlHelper::payInvoice($preorder->primaryPaymentInvoice));
    }


    /**
     * @param $preorderId
     */
    public function actionRepeat()
    {
        $user       = $this->getCurrentUser();
        $preorderId = Yii::$app->request->post('preorderId');
        $preorder   = Preorder::tryFindByPk($preorderId);
        $isCustomer = $this->access->isCustomer($preorder, $user);

        if (!$isCustomer || !$preorder->isClientCanRepeat()) {
            return $this->jsonError(_t('site.preorder', 'This quote cannot be repeated.'));
        }

        $this->preorderService->reopen($preorder);
        return $this->jsonSuccess(['message' => 'Reopened']);
    }

    /**
     * @param $preorderId
     *
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDecline($preorderId)
    {
        $user = $this->getCurrentUser();

        $preorder   = Preorder::tryFindByPk($preorderId);
        $isCustomer = $this->access->isCustomer($preorder, $user);
        if ($isCustomer) {
            throw new DeprecatedException('Use method CancelOfferByClient');
        }

        if ($preorder && $preorder->status == Preorder::STATUS_ACCEPTED) {
            return $this->jsonError(_t('site.preorder', 'This quote cannot be declined. Please contact client.'));
        }
        if ($preorder->status === Preorder::STATUS_REJECT_CONFIRM_BY_COMPANY) {
            return $this->jsonError(_t('site.preorder', 'This quote has already been rejected.'));
        }

        $preorder = $this->resolver->resolvePreorder($preorderId, $user);


        $formModel = new ServicesPreorderDeclineForm();
        $formModel->load($this->request->post(), '');
        AssertHelper::assertValidate($formModel);

        $this->preorderService->declineOfferByCompany($preorder, (int)$formModel->reasonId, (string)$formModel->reasonDescription);
        $this->emailer->customerPsDeclinePreorder($preorder, $formModel);

        return $this->serializer->serialize($preorder);
    }

    public function actionCancelOfferByClient()
    {
        $preorderId = Yii::$app->request->post('preorderId');
        $preorder   = Preorder::tryFindByPk($preorderId);
        UserFacade::tryCheckObjectOwner($preorder);
        $this->preorderService->cancelOfferByClient($preorder);
        return $this->jsonSuccess(['id' => $preorder->id]);
    }

    public function actionDeclineOfferByClient()
    {
        $preorderId        = Yii::$app->request->post('preorderId');
        $reasonId          = Yii::$app->request->post('reasonId')??0;
        $reasonDescription = Yii::$app->request->post('reasonDescription')??'';
        $preorder          = Preorder::tryFindByPk($preorderId);
        UserFacade::tryCheckObjectOwner($preorder);
        $this->preorderService->declineOfferByClient($preorder, $reasonId, $reasonDescription);
        $this->emailer->psCustomerDeclinePreorder($preorder);
        return $this->jsonSuccess(['id' => $preorder->id]);
    }

    public function actionDeleteOfferByClient()
    {
        $preorderId = Yii::$app->request->post('preorderId');
        $preorder   = Preorder::tryFindByPk($preorderId);
        UserFacade::tryCheckObjectOwner($preorder);
        $this->preorderService->deletePreorder($preorder);
        return $this->jsonSuccess(['id' => $preorder->id]);
    }

    /**
     * @param $preorderId
     * @param $hash
     * @return Response
     * @throws ForbiddenHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionEmailConfirm($preorderId, $hash)
    {
        $preorder = Preorder::tryFindByPk($preorderId);

        if ($preorder->confirm_hash != $hash) {
            throw new ForbiddenHttpException();
        }

        $preorder->user->status = User::STATUS_ACTIVE;
        $preorder->user->safeSave();

        $this->service->confirm($preorder);

        \Yii::$app->session->setFlash("success", _t('site.preorder', 'Request for a quote has been confirmed.'));
        return $this->redirect('/workbench/orders');
    }

    /**
     * @param $preorderId
     * @param $hash
     * @throws ForbiddenHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionEmailDecline($preorderId, $hash)
    {
        $preorder = Preorder::tryFindByPk($preorderId);

        if ($preorder->confirm_hash != $hash) {
            throw new ForbiddenHttpException();
        }

        if ($preorder->status != Preorder::STATUS_WAIT_CONFIRM) {
            throw new ForbiddenHttpException();
        }

        $preorder->changeStatus(Preorder::STATUS_REJECTED_BY_CLIENT);
        $this->logger->log($preorder, 'email_decline');
        $preorder->safeSave();

        \Yii::$app->session->setFlash("success", _t('site.preorder', 'Request for a quote has been declined.'));
        $this->redirect('/workbench/orders');

    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionFeeCalc()
    {
        $totalPrice        = \Yii::$app->request->get('totalPrice');
        $currency          = \Yii::$app->request->get('currency');
        $totalPriceMoney   = Money::create($totalPrice, $currency);
        $totalPriceWithFee = 0;
        if ($this->getCurrentUser()->isCustomerServiceCompany()) {
            return $this->jsonReturn([
                'success'    => true,
                'youWillGet' => $totalPriceMoney
            ]);
        }
        if ($totalPrice) {
            /** @var FeeHelper $feeHelper */
            $feeHelper         = \Yii::createObject(FeeHelper::class);
            $totalPriceWithFee = $totalPriceMoney->getAmount() - $feeHelper->getQuoteFee($totalPriceMoney)->getAmount();
        }
        $feeMoney = Money::create($totalPriceWithFee, $totalPriceMoney->getCurrency());
        return $this->jsonReturn([
            'success'    => true,
            'youWillGet' => $feeMoney
        ]);
    }
}