<?php
/**
 * User: nabi
 */

namespace frontend\modules\workbench\controllers;

use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\InvalidModelException;
use common\components\serizaliators\Serializer;
use common\models\message\builders\TopicBuilder;
use common\models\message\exceptions\CreateManyMessages;
use common\models\message\exceptions\UploadManyFilesException;
use common\models\message\FolderResolver;
use common\models\message\forms\MessageForm;
use common\models\message\forms\TopicForm;
use common\models\message\helpers\UrlHelper;
use common\models\message\MessagesSerializerProperties;
use common\models\MsgFile;
use common\models\MsgFolder;
use common\models\MsgMember;
use common\models\MsgMessage;
use common\models\MsgTopic;
use common\models\StoreOrder;
use common\models\User;
use common\modules\captcha\services\CaptchaService;
use common\modules\instantPayment\helpers\InstantPaymentUrlHelper;
use common\services\UserEmailLoginService;
use frontend\models\community\MessageFacade;
use frontend\models\community\ReportMessageForm;
use frontend\modules\workbench\actions\MessageAction;
use frontend\modules\workbench\models\view\MessageViewModel;
use lib\message\exceptions\BadParamsException;
use Yii;
use yii\base\InvalidCallException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

;

class MessagesController extends WorkbenchController
{

    /** @var CaptchaService */
    protected $captchaService;

    /**
     * @param CaptchaService $captchaService
     */
    public function injectDependencies(
        CaptchaService $captchaService
    )
    {
        $this->captchaService = $captchaService;
    }

    public function behaviors()
    {
        if ($this->user && $this->user->thingiverseUser) {
            $this->layout = '@frontend/views/layouts/plain';
        }
        return [
            'access' => [
                'class'  => \yii\filters\AccessControl::class,
                'except' => ['topic-messages'],
                'rules'  => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function actions(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'index' => [
                'class' => MessageAction::class
            ],
            'unread' => [
                'class' => MessageAction::class,
                'type' =>  MsgMember::TYPE_UNREAD
            ],
            'without-answer' => [
                'class' => MessageAction::class,
                'type' => MsgMember::TYPE_WITHOUT_ANSWER
            ]
        ]);
    }

    /**
     * Search by user messages
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     */
    public function actionSearch()
    {
        $this->checkStatusDraftCompany();

        $searchValue = app()->request->get('search');

        /** @var User $user */
        $user = $this->getCurrentUser(true);

        return $this->render(
            'topics',
            [
                'user'           => $user,
                'folder'         => null,
                'searchValue'    => $searchValue,
                'topicsProvider' => MessageFacade::getFilteredUserMessagesProvider($user, $searchValue),
                'type'           => 'all'
            ]
        );
    }

    /**
     * View topic
     *
     * @param
     *            $topicId
     * @return string
     * @throws HttpException
     */
    public function actionTopic($topicId)
    {
        $user  = $this->getCurrentUser(true);
        $topic = $this->resolveTopic($topicId);

        MessageFacade::markTopicAsReaded($topic, $user);
        \Yii::$app->cache->set('user' . $user->id . '.unreaded', 0, 60 * 2);
        $viewModel = Yii::createObject([
            'class' => MessageViewModel::class,
            'topic' => $topic,
            'user' => $user,
            'messages' => $this->jsonLastMessages(MessageFacade::getFilteredMessages($user, $topic), $user)
        ]);
        return $this->render(
            'topic',
            [
                'viewModel' => $viewModel
            ]
        );
    }

    public function actionEmbedTopic($bindTo = 'order', $bindId = 0)
    {
        $this->layout = '@frontend/views/layouts/plain.php';
        $user         = $this->getCurrentUser(true);
        $form         = TopicForm::createForObject($user, $bindTo, $bindId);
        $topic        = MessageFacade::findTopicByForm($form);

        if (!$topic) {
            try {
                $topic = TopicBuilder::builder()->fromForm($form)->create();
            } catch (BadParamsException $sameUserException) {
                return '';
            }

            if ($bindTo == 'order') {
                /**
                 * @var StoreOrder $order
                 */
                $order = StoreOrder::findByPk($bindId);
                if ($order->currentAttemp) {
                    $psUser = $order->currentAttemp->ps->user;
                    MessageFacade::markTopicAsReaded($topic, $psUser);
                }
            }
        }
        $instantPaymentUrl = InstantPaymentUrlHelper::getByTopicBind($user, $topic);

        MessageFacade::markTopicAsReaded($topic, $user);
        \Yii::$app->cache->set('user' . $user->id . '.unreaded', 0, 60 * 2);
        $messages = $this->jsonLastMessages(MessageFacade::getFilteredMessages($user, $topic), $user);
        return $this->render(
            'topicEmbed',
            [
                'user'     => $user,
                'topic'    => $topic,
                'messages' => $messages,
                'instantPaymentUrl' => $instantPaymentUrl
            ]
        );
    }

    /**
     * Move topic to archive
     *
     * @param
     *            $topicId
     * @return \yii\web\Response
     * @throws \yii\base\InvalidCallException
     * @throws HttpException
     */
    public function actionArchive($topicId)
    {
        if (!app('request')->isPost) {
            throw new InvalidCallException('Invalid request method. Need post method.');
        }
        $user  = $this->getCurrentUser(true);
        $topic = $this->resolveTopic($topicId);
        MessageFacade::moveTopicToArchive($topic, $user);
        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * Delete topic
     *
     * @param
     *            $topicId
     * @return \yii\web\Response
     * @throws \yii\base\InvalidCallException
     */
    public function actionDeleteTopic($topicId)
    {
        if (!app('request')->isPost) {
            throw new InvalidCallException('Invalid request method. Need post method.');
        }
        $user  = $this->getCurrentUser(true);
        $topic = $this->resolveTopic($topicId);
        MessageFacade::deleteTopic($topic, $user);
        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * Add message to topic
     *
     * @param $topicId
     * @param int|null $lastMessageId
     * @return array
     * @throws CreateManyMessages
     * @throws HttpException
     */
    public function actionAddMessage($topicId, $lastMessageId = null)
    {
        $user  = $this->getCurrentUser(true);
        $topic = $this->resolveTopic($topicId);

        $messageForm = new MessageForm();
        if ($messageForm->load(\Yii::$app->request->post(), '') && $messageForm->validate()) {

            MessageFacade::addMessage($topic, $user, $messageForm);
            $messages = MessageFacade::getFilteredMessages($user, $topic, true, $lastMessageId);
            return $this->jsonLastMessages($messages, $user);
        }
        throw new InvalidModelException($messageForm, 'Bad request');
    }

    /**
     * Return last messages of topic
     *
     * @param $topicId
     * @param int|null $lastMessageId
     * @return Response|array
     * @throws HttpException
     */
    public function actionTopicMessages($topicId, $lastMessageId = null)
    {
        $user = $this->getCurrentUser(true); # return $this->redirect('/');
        if (!$user) {
            return $this->redirect('/');
        }
        $topic    = $this->resolveTopic($topicId);
        $messages = MessageFacade::getFilteredMessages($user, $topic, true, $lastMessageId);
        return $this->jsonLastMessages($messages, $user);
    }

    /**
     * @param $fileId
     * @throws NotFoundHttpException
     */
    public function actionDeleteFile($fileId)
    {
        $user = $this->getCurrentUser(true);
        /** @var MsgFile $file */
        $file = MsgFile::find()
            ->forId($fileId)
            ->forUser($user)
            ->active()
            ->one();

        if (!$file) {
            throw new NotFoundHttpException();
        }

        $file->markAsDeleted();
    }

    /**
     * @param $fileId
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($fileId)
    {
        $user = $this->getCurrentUser(true);

        /** @var MsgFile $file */
        $file = MsgFile::find()
            ->forId($fileId)
            ->active()
            ->one();

        if (!$file) {
            throw new NotFoundHttpException();
        }

        if (!$file->isHaveAccess($user)) {
            throw new ForbiddenHttpException();
        }

        return \Yii::$app->response->sendFile($file->file->getLocalTmpFilePath(), $file->file->getFileName());
    }

    /**
     * Convert messages to JSON
     *
     * @param MsgMessage[] $messages
     * @param User $currentUser
     * @return array
     */
    public function jsonLastMessages($messages, User $currentUser)
    {
        $serializer = new Serializer(new MessagesSerializerProperties($currentUser));
        return $serializer->serialize($messages);
    }

    /**
     * Create topic to support
     *
     * @param null $bindTo
     * @param null $bindId
     * @return string|\yii\web\Response
     */
    public function actionCreateSupportTopic($bindTo = null, $bindId = null)
    {
        $user = $this->getCurrentUser(true);
        $form = TopicForm::createToSupport($user, $bindTo, $bindId);
        /** @var MsgFolder $folder */
        $folder = MsgFolder::findOne(MsgFolder::FOLDER_ID_SUPPORT);

        if ($bindId && $existTopic = MessageFacade::findTopicByForm($form)) {
            return $this->redirect(UrlHelper::topicRoute($existTopic));
        }

        if (\Yii::$app->request->isPost && $form->load(\Yii::$app->request->post()) && $form->validate()) {

            TopicBuilder::builder()->fromForm($form)
                ->toSupport()
                ->create();

            return $this->redirectToFolder($folder);
        }

        return $this->render(
            'newTopic',
            [
                'topicForm' => $form,
                'user'      => $user,
                'folder'    => $folder
            ]
        );
    }

    /**
     * Create topic for binded object
     *
     * @param $bindTo
     * @param $bindId
     * @param bool $auto
     * @return string|Response
     * @throws \Exception
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     * @throws \common\models\message\exceptions\InvalidBindedObject
     * @throws \yii\base\Exception
     * @internal param $ $bindTo*            $bindTo
     * @internal param $ $bindId*            $bindId
     */
    public function actionCreateTopicForObject($bindTo, $bindId, $auto = false)
    {
        $user = $this->getCurrentUser(true);
        $form = TopicForm::createForObject($user, $bindTo, $bindId);
        /** @var MsgFolder $folder */
        $folder = MsgFolder::findOne((new FolderResolver())->resolveFolderIdForObject($user, $form->resolveBindedObject()));

        if ($existTopic = MessageFacade::findTopicByForm($form)) {
            $this->sendThingiverseOrderEmailLoginLink($bindTo, $bindId);
            return $this->redirect(UrlHelper::topicRoute($existTopic));
        }

        if (\Yii::$app->request->isPost && $form->load(\Yii::$app->request->post()) && $form->validate()) {
            $topic = TopicBuilder::builder()->fromForm($form)->create();
            $this->sendThingiverseOrderEmailLoginLink($bindTo, $bindId);
            return $this->redirect(UrlHelper::topicRoute($topic));
        }

        return $this->render(
            'newTopic',
            [
                'topicForm' => $form,
                'user'      => $user,
                'folder'    => $folder
            ]
        );
    }

    /**
     * @param $bindTo
     * @param $bindId
     * @return bool
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    private function sendThingiverseOrderEmailLoginLink($bindTo, $bindId)
    {
        if ($bindTo !== 'order') {
            return false;
        }
        $order = StoreOrder::tryFindByPk($bindId);
        if ($order->isThingiverseOrder()) {
            $emailLoginService = \Yii::createObject(UserEmailLoginService::class);
            $emailLoginService->requestLoginByEmailWithThingiverseOrder(
                $order->thingiverseOrder,
                'user.emailThingiverseOrderLinkPs'
            );
            return true;
        }
        return false;
    }

    /**
     * Create personal topic
     *
     * @param string $withUsername Username of user
     * @return string|\yii\web\Response
     * @throws HttpException
     * @throws \Exception
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     * @throws \common\models\message\exceptions\InvalidBindedObject
     * @throws \yii\base\Exception
     */
    public function actionCreatePersonalTopic($withUsername)
    {
        $user     = $this->getCurrentUser(true);
        $withUser = $this->resolveUserUsername($withUsername);
        /** @var MsgFolder $folder */
        $folder = MsgFolder::findOne(MsgFolder::FOLDER_ID_PERSONAL);

        $form = TopicForm::createPersonal($user, $withUser);

        if ($existTopic = MessageFacade::findTopicByForm($form)) {
            return $this->redirect(UrlHelper::topicRoute($existTopic));
        }

        if (\Yii::$app->request->isPost && $form->load(\Yii::$app->request->post()) && $form->validate()) {

            TopicBuilder::builder()->fromForm($form)->create();

            return $this->redirectToFolder($folder);
        }

        return $this->render(
            'newTopic',
            [
                'topicForm' => $form,
                'user'      => $user,
                'folder'    => $folder
            ]
        );
    }

    /**
     * Create personal topic with unknown user
     *
     * @return string|\yii\web\Response
     * @throws HttpException
     * @throws InvalidModelException
     * @throws NotFoundHttpException
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     * @throws \common\models\message\exceptions\InvalidBindedObject
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @internal param $withUserId
     */
    public function actionCreatePersonalTopicWithUnknownUser()
    {
        $user = $this->getCurrentUser(true);

        $form = TopicForm::createPersonalWithUnknownUser($user);
        /** @var MsgFolder $folder */
        $folder = MsgFolder::findOne(MsgFolder::FOLDER_ID_PERSONAL);
        if (\Yii::$app->request->isPost && $form->load(\Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->captchaService->checkGoogleRecaptcha2Response();
            } catch (BusinessException $userException) {
                $this->setFlashMsg(false, $userException->getMessage());
                return $this->redirect(Yii::$app->request->referrer);
            }
            try {
                if ($existTopic = MessageFacade::findTopicByForm($form)) {
                    $messageForm = MessageForm::createFromTopicForm($form);
                    MessageFacade::addMessage($existTopic, $user, $messageForm);
                    return $this->redirect(UrlHelper::topicRoute($existTopic));
                }

                TopicBuilder::builder()->fromForm($form)->create();

                return $this->redirectToFolder($folder);
            } catch (UploadManyFilesException $e) {
                $form->addError('files', $e->getMessage());
            }
        } elseif ($form->hasErrors()) {
            $this->setFlashMsg(false, \yii\helpers\Html::errorSummary($form));
        }
        return $this->render(
            'newTopic',
            [
                'topicForm' => $form,
                'user'      => $user,
                'folder'    => $folder
            ]
        );
    }

    /**
     * User list you can send a message
     *
     * @param string $query
     *            part of username for search
     * @return array
     */
    public function actionUserList($query)
    {
        $this->getCurrentUser(true);
        $users = User::find()->likeUsername($query)
            ->active()
            ->notSystem()
            ->exclude($this->getCurrentUser())
            ->select(['id', dbexpr('IFNULL(username, email) text')])
            ->asArray()
            ->all();

        return $this->jsonReturn(
            [
                'results' => $users
            ]
        );
    }

    /**
     * Report about bad message
     *
     * @param int $topicId Topic id for report
     */
    public function actionReportMessage($topicId)
    {
        $reportForm = new ReportMessageForm($this->getCurrentUser(), $this->resolveTopic($topicId));
        AssertHelper::assert($reportForm->load(\Yii::$app->request->post(), ''));
        AssertHelper::assertValidate($reportForm);
        MessageFacade::reportMessage($reportForm);
    }

    /**
     * Resolve user by id
     *
     * @param string $username
     * @return User
     * @throws HttpException
     */
    private function resolveUserUsername($username)
    {
        $user = \common\models\User::findOne(['username' => $username]);
        if (!$user) {
            throw new HttpException(404, 'Cant find user');
        }
        return $user;
    }

    /**
     * Resolve topic and check that curent user is member of this topic
     *
     * @param
     *            $topicId
     * @return null|MsgTopic
     * @throws HttpException
     */
    private function resolveTopic($topicId)
    {
        if (!$topicId) {
            throw new HttpException('500', 'Bad request');
        }

        /** @var MsgTopic $topic */
        $topic = MsgTopic::findByPk($topicId);

        if (!$topic) {
            throw new NotFoundHttpException('Topic not found');
        }

        if ($this->getCurrentUser()->id === User::USER_CUSTOMER_SERVICE) {
            return $topic;
        }

        if (!$topic->getIsMember($this->getCurrentUser())) {
            throw new BusinessException('Haven\'t access to this topic', 403);
        }

        return $topic;
    }

    /**
     * @param MsgFolder $folder
     * @return Response
     */
    private function redirectToFolder(MsgFolder $folder)
    {
        return $this->redirect(UrlHelper::toFolderRoute($folder));
    }

    /**
     * @throws \yii\base\ExitException
     */
    public function checkStatusDraftCompany()
    {
        if ($this->user->status === User::STATUS_DRAFT_COMPANY) {
            $this->setFlashMsg(false, _t('mybusiness.common', 'Please confirm email first.'), false);
            $this->redirect('/');
            app()->end();
        }
    }
}
