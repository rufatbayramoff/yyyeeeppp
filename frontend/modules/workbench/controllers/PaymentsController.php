<?php
/**
 * User: nabi
 */

namespace frontend\modules\workbench\controllers;

use backend\modules\statistic\reports\ReportExcelWriter;
use common\components\exceptions\OnlyPostRequestException;
use common\components\FileDirHelper;
use common\components\operations\PostValidateException;
use common\models\PaymentAccount;
use common\models\PaymentInvoice;

use common\models\User;
use common\models\UserPaypal;
use common\models\UserTaxInfo;
use common\modules\affiliate\components\AffiliatedStatisticCalculator;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\processors\BankPayoutProcessor;
use common\modules\payment\processors\PayPalPayoutProcessor;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentAccountStatistics;
use common\modules\payment\services\PaymentService;
use frontend\models\payment\PaymentsReport;
use frontend\modules\workbench\actions\InvoiceReportAction;
use lib\money\Currency;
use lib\money\Money;
use lib\pdf\HtmlToPdf;
use treatstock\api\v2\exceptions\UnSuccessException;
use Yii;
use yii\base\UserException;
use yii\web\ForbiddenHttpException;
use frontend\components\AuthedController;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

/**
 * Class PaymentsController
 *
 * @package frontend\modules\workbench\controllers
 *
 * @property PaymentService $paymentService
 * @property PaymentAccountService $paymentAccountService
 * @property AffiliatedStatisticCalculator $affiliatedStatisticCalculator
 * @property PaymentAccountStatistics $paymentAccountStatistics
 */
class PaymentsController extends AuthedController
{

    protected $mode = 'payments';

    protected $paymentService;

    protected $paymentAccountService;

    protected $affiliatedStatisticCalculator;

    protected $paymentAccountStatistics;

    /**
     * @param AffiliatedStatisticCalculator $affiliatedStatisticCalculator
     * @param PaymentService $paymentService
     * @param PaymentAccountService $paymentAccountService
     * @param PaymentAccountStatistics $paymentAccountStatistics
     */
    public function injectDependencies(
        AffiliatedStatisticCalculator $affiliatedStatisticCalculator,
        PaymentService $paymentService,
        PaymentAccountService $paymentAccountService,
        PaymentAccountStatistics $paymentAccountStatistics
    )
    {
        $this->affiliatedStatisticCalculator = $affiliatedStatisticCalculator;
        $this->paymentService                = $paymentService;
        $this->paymentAccountService         = $paymentAccountService;
        $this->paymentAccountStatistics      = $paymentAccountStatistics;
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'out' => ['post'],
                ]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions                   = parent::actions();
        $actions['invoice-report'] = [
            'class' => InvoiceReportAction::class,
        ];
        $actions['fee-report']     = [
            'class'    => InvoiceReportAction::class,
            'method'   => 'fee',
            'fileName' => 'fee_report.pdf'
        ];
        return $actions;
    }

    /**
     * @return \yii\web\Response
     */
    public function actionTransactions()
    {
        return $this->redirect('/workbench/payments');
    }

    public function actionUnlinkPaypal()
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $user                     = $this->getCurrentUser('/');
        $userPaypal               = UserPaypal::tryFind(['user_id' => $user->id]);
        $userPaypal->email        = 'unlinked' . $userPaypal->user_id;
        $userPaypal->check_status = UserPaypal::STATUS_UNLINKED;
        $userPaypal->safeSave();
        return $this->redirect('/workbench/payments');
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        if ($this->user->status === User::STATUS_DRAFT_COMPANY) {
            $this->setFlashMsg(false, _t('mybusiness.common', 'Please confirm email first.'), false);
            return $this->redirect('/');
        }
        $this->mode = 'transaction';
        $user       = $this->getCurrentUser('/');

        if ($user->thingiverseUser) {
            return $this->renderContent('');
        }

        if (app('request')->get('error', false)) { // if paypal gives error
            $errorMsg = app('request')->get('error_description', "Error");
            $error    = app('request')->get('error', "Error Request.");
            \Yii::warning($error . $errorMsg, "paypal");
            return ("<html><head></head><body><h2>" . H($error) . "</h2><p>" . H($errorMsg) . "</p><script>//window.close();</script></body></html>");
        } else {
            if (app('request')->get('code', false)) { // if user paypal confirms email
                $authCode = app('request')->get('code');
                try {
                    UserPaypal::validateByAuth($user->id, $authCode);
                    return $this->renderContent("<html><head></head><body><script>if (window.opener) {window.opener.location.reload(false);window.close();} else {window.location='/workbench/payments'}</script></body></html>");
                } catch (\Exception $ex) {
                    logException($ex, 'tsdebug');
                    $this->setFlashMsg(false, $ex->getMessage());
                    return $this->redirect('/workbench/payments');
                }
            }
        }

        $mainBalances             = $this->paymentAccountService->getUserMainAmounts($user);
        $paymentAccountStatistics = $this->paymentAccountStatistics->getStatisticsForCompany($user);
        $paymentDetails           = $this->paymentAccountService->getPaymentDetailsProvider($user);
        $lastBankPayout           = $this->paymentAccountService->lastDetailsBankPayout($user);

        return $this->render(
            'payments', [
            'user'              => $this->user,
            'mainBalances'      => $mainBalances,
            'accountStatistics' => $paymentAccountStatistics,
            'paymentDetails'    => $paymentDetails,
            'lastBankPayout'    => $lastBankPayout,
        ]);
    }

    /**
     * @param $from
     * @param $to
     * @param bool $pdf
     *
     * @return string
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReport($from, $to, $pdf = false)
    {
        $this->view->title = _t('site.payments', 'Financial Report');
        $user              = $this->getCurrentUser(true);
        $this->layout      = '@frontend/views/layouts/plain.php';
        $limit             = 100000;

        $paymentDetails     = $this->paymentAccountService->getPaymentDetailsProvider($user, 'income', $from, $to, $limit);
        $paymentsOutDetails = $this->paymentAccountService->getPaymentDetailsProvider($user, 'payout', $from, $to, $limit);

        $paymentDetails->sort     = false;
        $paymentsOutDetails->sort = false;

        $fromDate = date("Y-m-d", strtotime($from));
        $toDate   = date("Y-m-d", strtotime($to));

        return $this->render(
            'paymentsReport', [
            'user'               => $user,
            'paymentDetails'     => $paymentDetails,
            'paymentsOutDetails' => $paymentsOutDetails,
            'fromDate'           => $fromDate,
            'toDate'             => $toDate,
            'pdf'                => $pdf
        ]);
    }

    /**
     * @param $from
     * @param $to
     *
     * @return string
     * @throws \PHPExcel_Exception
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDownloadReport($from, $to)
    {
        $user   = $this->getCurrentUser(true);
        $limit             = 100000;

        $paymentDetails     = $this->paymentAccountService->getPaymentDetailsProvider($user, 'income', $from, $to, $limit);
        $paymentsOutDetails = $this->paymentAccountService->getPaymentDetailsProvider($user, 'payout', $from, $to, $limit);

        $data = array_merge($paymentDetails->getModels(), $paymentsOutDetails->getModels());

        $report = new PaymentsReport();
        $report->setRows($data);
        $report->setColumnNames(
            [
                'updated_at'  => _t('site.user', 'Created On'),
                'amount'      => _t('site.user', 'Amount'),
                'type'        => _t('app', 'Type'),
                'descriptionWithoutTags' => _t('app', 'Description')
            ]
        );

        $reportWriter = new ReportExcelWriter('earnings');
        $reportWriter->setFileName($report->getFileName());
        return $reportWriter->write($report);
    }

    /**
     * @param $from
     * @param $to
     *
     * @return \common\models\SystemSetting|\yii\caching\Cache|\yii\db\Connection|\yii\i18n\Formatter|\yii\web\Application|\yii\web\Request|\yii\web\Response|\yii\web\UrlManager|\yii\web\User
     * @throws \yii\base\ErrorException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDownloadPdf($from, $to)
    {
        $user     = $this->getCurrentUser();
        $result   = $this->actionReport($from, $to, true);
        $result   = str_replace('href="/css', 'href="' . param('server') . '/css', $result);
        $result   = str_replace('href="/assets', 'href="' . param('server') . '/assets', $result);
        $cacheDir = \Yii::$app->getRuntimePath() . '/payments/';
        FileDirHelper::createDir($cacheDir);
        $fileHtmlCache = $cacheDir . $user->id . '_' . strtotime($from) . '_' . strtotime($to) . '.html';
        $oldPdfFile    = $cacheDir . $user->id . '_' . strtotime($from) . '_' . strtotime($to) . '.pdf';
        if (file_exists($oldPdfFile)) {
            $filePdf = $oldPdfFile;
        } else {
            file_put_contents($fileHtmlCache, $result);
            $filePdf = HtmlToPdf::convert($fileHtmlCache);
        }
        app('response')->setDownloadHeaders(sprintf('Report %s - %s.pdf', $from, $to), 'application/pdf', false);
        return app('response')->sendFile($filePdf);
    }

    /**
     * @param $invoiceUuid
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionInvoiceData($invoiceUuid)
    {
        $paymentInvoice = PaymentInvoice::tryFind([
            'uuid' => $invoiceUuid
        ]);

        if (!$this->paymentService->checkAccessInvoiceForCurrentUser($paymentInvoice)) {
            throw new ForbiddenHttpException(_t('site.order', 'Access denied for current invoice'));
        }

        return $this->renderPartial('invoiceData', [
            'invoice' => $paymentInvoice
        ]);
    }


    /**
     * request to payout to paypal
     *
     * @return string|\yii\web\Response
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionOut()
    {
        /** @var User $user */
        $user = $this->getCurrentUser(true);

        if ($user->thingiverseUser) {
            return $this->renderContent('');
        }

        $withdrawBy         = Yii::$app->request->post('by');
        $withdrawMoney      = Money::create(app()->request->post('amount'), app()->request->post('currency') ?? Currency::USD);
        $bankAccountDetails = Yii::$app->request->post('bank-details');
        $tax                = PaymentInfoHelper::getTaxRequired($user);

        if (!$tax->isEmpty() && !app()->request->post('forcewithdraw', false)) {
            return $this->render('paymentsOut.php', [
                'tax'                => $tax,
                'withdrawBy'         => $withdrawBy,
                'withdrawMoney'      => $withdrawMoney,
                'bankAccountDetails' => $bankAccountDetails
            ]);
        }

        try {
            if ($withdrawBy === 'bank') {
                if ($withdrawMoney->getCurrency() === Currency::EUR) {
                    throw new UserException('Only usd bank payout allowed now');
                }
                $payPalPayoutProcessor = Yii::createObject(BankPayoutProcessor::class);
                $payPalPayoutProcessor->payOut($user, $bankAccountDetails, $withdrawMoney);
            } else { // Default paypal
                /** @var PayPalPayoutProcessor $payPalPayoutProcessor */
                $payPalPayoutProcessor = Yii::createObject(PayPalPayoutProcessor::class);
                $payPalPayoutProcessor->payOut($user, $withdrawMoney);
            }

            $this->setFlashMsg(true, _t('front.site', 'Payout accepted'));

            UserTaxInfo::taxNotifyOff($user);
        } catch (\Exception $e) {
            \Yii::error($e);
            $this->setFlashMsg(false, $e->getMessage());
        }

        return $this->redirect(['/workbench/payments']);
    }
}
