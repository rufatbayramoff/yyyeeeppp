<?php
/**
 * User: nabi
 */

namespace frontend\modules\workbench\controllers;

use backend\components\NoAccessException;
use common\components\ArrayHelper;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\ParcelCalculateException;
use common\components\order\history\OrderHistoryService;
use common\components\serizaliators\porperties\PrintAttempProperties;
use common\components\serizaliators\Serializer;
use common\interfaces\DeliveryExpensiveInterface;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\Preorder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderAttempDates;
use common\models\StoreOrderAttemptModerationFile;
use common\models\StoreOrderPosition;
use common\models\User;
use common\modules\cutting\services\CuttingPackService;
use common\modules\informer\InformerModule;
use common\modules\payment\models\StoreOrderPositionForm;
use common\modules\payment\services\RefundService;
use common\modules\payment\services\StoreOrderPositionService;
use common\modules\storeOrder\services\TwoTimeExpensive;
use common\services\ChangeOfferService;
use common\services\PrintAttemptModeratonService;
use common\services\StoreOrderService;
use common\traits\TransactedControllerTrait;
use console\jobs\model3d\PrepareDowloadFilesJob;
use console\jobs\QueueGateway;
use frontend\models\community\MessageFacade;
use frontend\models\ps\AddResultImageForm;
use frontend\models\ps\DeclineOrderForm;
use frontend\models\ps\PrepareDownloadFileResponse;
use frontend\models\ps\PsFacade;
use frontend\models\ps\StoreOrderAttempSearchForm;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\models\ps\TrackingNumberForm;
use frontend\models\user\UserFacade;
use frontend\modules\preorder\components\PreorderService;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\workbench\components\OrderServiceUrlHelper;
use lib\delivery\delivery\models\DeliveryPostalLabel;
use lib\delivery\parcel\ParcelFactory;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Module;
use yii\base\UserException;
use yii\db\Exception;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ServiceOrderController extends WorkbenchController
{

    use TransactedControllerTrait;

    /**
     * @var ChangeOfferService
     */
    private $changeOfferService;

    /**
     * @var StoreOrderService
     */
    private $orderService;

    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @var PrintAttemptModeratonService
     */
    private $printAttemptModeratonService;

    /**
     * @var Serializer
     */
    private $printAttmptSerializer;

    /**
     * @var StoreOrderPositionService
     */
    private $orderPositionService;

    /**
     * @var QueueGateway
     */
    private $queueGateway;

    /**
     * @var RefundService
     */
    private $refundService;

    /**
     * @var PreorderService
     */
    protected $preorderService;

    /**
     * @var CuttingPackService
     */
    protected $cuttingPackService;

    protected $viewPath = '@frontend/modules/workbench/views/service-orders';

    /**
     * PrintserviceOrderController constructor.
     *
     * @param string $id
     * @param Module $module
     * @param ChangeOfferService $changeOfferService
     * @param PrintAttemptModeratonService $printAttemptModeratonService
     * @param StoreOrderService $orderService
     * @param StoreOrderPositionService $positionService
     * @param Emailer $emailer
     * @param RefundService $refundService
     * @param CuttingPackService $cuttingPackService
     * @param PreorderService $preorderService
     * @param array $config
     */
    public function __construct(
        $id,
        Module $module,
        ChangeOfferService $changeOfferService,
        PrintAttemptModeratonService $printAttemptModeratonService,
        StoreOrderService $orderService,
        StoreOrderPositionService $positionService,
        Emailer $emailer,
        RefundService $refundService,
        CuttingPackService $cuttingPackService,
        PreorderService $preorderService,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->changeOfferService           = $changeOfferService;
        $this->orderService                 = $orderService;
        $this->orderPositionService         = $positionService;
        $this->emailer                      = $emailer;
        $this->refundService                = $refundService;
        $this->printAttemptModeratonService = $printAttemptModeratonService;
        $this->cuttingPackService           = $cuttingPackService;
        $this->printAttmptSerializer        = PrintAttempProperties::createSerializer();
        $this->queueGateway                 = new QueueGateway();
        $this->preorderService              = $preorderService;
    }

    /**]
     * @param $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     */
    public function actionView()
    {
        $orderId   = Yii::$app->request->get('id');
        $attemptId = Yii::$app->request->get('attemptId');

        $user = $this->getCurrentUser(true);

        $form = new StoreOrderAttempSearchForm($user);
        $form->setAttributes(Yii::$app->request->get());

        $attempt = $form->getOrdersQuery();
        if ($attemptId) {
            $attempt->where(['store_order_attemp.id' => $attemptId]);
        } elseif ($orderId) {
            $attempt->where(['store_order.id' => $orderId]);
        } else {
            throw new InvalidArgumentException('Id or attemptId should be setted in get params');
        }

        $attempt->orderBy(['store_order_attemp.created_at' => SORT_DESC]);

        /** @var StoreOrderAttemp $attempt */
        $attempt = $attempt->one();

        if (!$attempt) {
            throw new NotFoundHttpException(_t('site.order', 'Order not found'));
        }

        $attempt->fixDates();

        InformerModule::markAsViewedCompanyOrderInformer($attempt);

        $statusGroup = $attempt->getStatusGroup();

        if ($attempt->isQuote()) {
            if (UserFacade::isObjectOwner($attempt->preorder->ps)||($attempt->isInterception() && $user->isCustomerServiceCompany())) {
                return $this->redirect(PreorderUrlHelper::viewPs($attempt->preorder));
            }
            return $this->redirect(PreorderUrlHelper::viewCustomer($attempt->preorder));
        };

        if ($statusGroup == StoreOrderAttemp::STATUS_ACCEPTED) {
            return $this->redirect('/workbench/service-order/print?attempId=' . $attempt->id);
        };

        if (\Yii::$app->request->get('notifyCanceled')) {
            if ($attempt->isCancelled() && ($attempt->cancel_initiator === ChangeOrderStatusEvent::INITIATOR_CLIENT)) {
                $this->setFlashMsg(false,
                    _t('site.order', 'Sorry, order #{orderId} was canceled by the customer and is no longer available.',
                        ['orderId' => $attempt->order->id]));
            }
        }

        return $this->render(
            'view',
            [
                'statusGroup' => $statusGroup,
                'form'        => $form,
                'attemp'      => $attempt,
            ]
        );
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'save-request-more-time' => ['post'],
                    'setOrderStatus'         => ['post'],
                    'saveTrackingNumber'     => ['post'],
                    'acceptDownloadPolicy'   => ['post'],
                    'uploadPhotoModal'       => ['post'],
                    'decline-order'          => ['post'],
                    'accept-order-offer'     => ['post'],
                ]
            ],
        ]);
    }

    /**
     * Save time, when user choose Request more time button
     *
     * @param int $attempId
     *
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \common\components\exceptions\InvalidModelException
     * @transacted
     */
    public function actionSaveRequestMoreTime($attempId)
    {
        $attemp = $this->resolveAttemp($attempId);

        $orderDates   = $attemp->dates;
        $oldPlanPrint = (string)$orderDates->plan_printed_at;
        $orderDates->load(Yii::$app->request->bodyParams, '');
        AssertHelper::assertSave($orderDates);

        // log request more time change
        $orderHistoryService = new OrderHistoryService();
        $orderHistoryService->logRequestMoreTime($attemp, $oldPlanPrint, (string)$orderDates->plan_printed_at, (string)$orderDates->request_reason);

        if (UserFacade::getCurrentUserId() != $attemp->order->user_id) {
            $currentTimezone                  = app('formatter')->defaultTimeZone;
            app('formatter')->defaultTimeZone = $attemp->order->user->userProfile->timezone_id;
            $expireTimeUser                   = app('formatter')->asDatetime($orderDates->plan_printed_at) . ' (' . $attemp->order->user->userProfile->timezone_id . ')';
            app('formatter')->defaultTimeZone = $currentTimezone;

            // send message to customer
            MessageFacade::createOrderTopic(
                UserFacade::getCurrentUser(),
                $attemp->order->user,
                $attemp->order,
                _t('site.ps', 'Print service has requested more time'),
                _t(
                    'site.ps',
                    'Hi. More time is required {time} to print order #{order}, reason: {reason}.',
                    [
                        'reason' => $orderDates->request_reason,
                        'time'   => $expireTimeUser,
                        'order'  => $attemp->order->id
                    ]
                )
            );
        }
    }

    /**
     * @param $attempId
     *
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSaveScheduledSentTime($attempId)
    {
        $attemp = $this->resolveAttemp($attempId);

        $orderDates       = $attemp->dates;
        $oldScheduledTime = (string)$orderDates->scheduled_to_sent_at;
        $orderDates->load(Yii::$app->request->bodyParams, '');
        $orderDates->safeSave(['scheduled_to_sent_at', 'request_reason']);

        $orderHistoryService = new OrderHistoryService();
        $orderHistoryService->logScheduledToSentAtTime($attemp, $oldScheduledTime, (string)$orderDates->scheduled_to_sent_at, (string)$orderDates->request_reason);

        if (UserFacade::getCurrentUserId() != $attemp->order->user_id) {
            // send message to customer
            MessageFacade::createOrderTopic(
                UserFacade::getCurrentUser(),
                $attemp->order->user,
                $attemp->order,
                _t('site.ps', 'Manufacturer has requested for more time to dispatch order'),
                _t(
                    'site.ps',
                    'Hi! Sorry but more time is required ({time}) to dispatch order #{order}. Reason: {reason}.',
                    [
                        'reason' => $orderDates->request_reason,
                        'time'   => app('formatter')->asDatetime($orderDates->scheduled_to_sent_at),
                        'order'  => $attemp->order->id
                    ]
                )
            );
        }
    }


    /**
     * Setorder status
     *
     * @param $attempId
     *
     * @return array
     * @throws \yii\db\Exception
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws UserException
     */
    public function actionSetOrderStatus($attempId)
    {
        $user   = $this->getCurrentUser('/');
        $attemp = $this->resolveAttemp($attempId);
        $status = app('request')->post('status');

        InformerModule::addCustomerOrderInformer($attemp->order);
        if ($status == StoreOrderAttemp::STATUS_ACCEPTED) {
            $this->orderService->acceptAttemp($attemp);
        } elseif ($status == StoreOrderAttemp::STATUS_SENT) {
            $this->orderService->sendAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_PS);
        } elseif ($status == StoreOrderAttemp::STATUS_READY_SEND) {
            if ($attemp->order->hasCuttingPack()) {
                $this->orderService->producedCuttingAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_PS);
            } elseif ($attemp->order->hasDelivery() || $attemp->order->isForPreorder()) {
                $this->orderService->readyToSendAttemp($attemp);
            } else {
                $this->orderService->deliveredAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_MODERATOR);
            }
        } elseif ($status == StoreOrderAttemp::STATUS_DELIVERED) {
            $this->orderService->deliveredAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_PS);
        } elseif ($status == StoreOrderAttemp::STATUS_RECEIVED) {
            $this->orderService->receivedAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_PS);
        } elseif ($status == StoreOrderAttemp::STATUS_PRINTED) {
            $this->orderService->printedAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_PS);
        } else {
            throw new Exception("Unknown status {$status}");
        }

        $this->setFlashMsg(true, _t('front.site', 'Order is updated'));
        PsFacade::resetOrdersCount($user);
        $attemp->order->refresh();
        return [
            'statusGroup' => $attemp->getStatusGroup()
        ];
    }

    /**
     * @param $attempId
     *
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function actionSaveTrackingNumber($attempId)
    {
        $form = new TrackingNumberForm();
        $form->load(Yii::$app->request->post(), '');
        AssertHelper::assertValidate($form);

        $attemp = $this->resolveAttemp($attempId);
        if ($form->withoutTracking) {
            $form->trackingNumber = '';
        }

        $this->orderService->createAttempDelivery($attemp);

        $this->orderService->logChangeTracking(
            $attemp,
            $attemp->delivery->tracking_shipper,
            $attemp->delivery->tracking_number,
            $form->shipper,
            $form->trackingNumber
        );

        $attemp->delivery->setTrackingNumber($form->trackingNumber, $form->shipper);

        $this->orderService->sendAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_PS);
        $this->setFlashMsg(true, _t('front.site', 'Order is updated'));
    }

    /**
     * Page for order printing
     *
     * @param int $attempId Order id
     *
     * @return string|Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionPrint($attempId)
    {
        $attempt = $this->resolveAttemp($attempId);
        /** @var User $user */
        $user = $this->getCurrentUser(true);

        if (!StoreOrderAttemptProcess::create($attempt)->canPrintOrder()) {
            return $this->redirect(OrderServiceUrlHelper::viewList($attempt->getStatusGroup()));
        }

        AssertHelper::assert(StoreOrderAttemptProcess::create($attempt)->canPrintOrder(), _t('front.site', 'You can\'t print this order.'), UserException::class);

        InformerModule::markAsViewedCompanyOrderInformer($attempt);

        if ($attempt->order->hasCuttingPack()) {
            $viewFile = 'cuttingProduction';
        } elseif ($attempt->order->isFor3dPrinter()) {
            $viewFile = 'printProduction';
        } else {
            $viewFile = 'preorderProduction';
        }

        return $this->render(
            $viewFile,
            [
                'status'      => StoreOrderAttemp::STATUS_ACCEPTED,
                'attempt'     => $attempt,
                'currentUser' => $user
            ]
        );
    }

    /**
     * Accept download policy
     *
     * @link https://redmine.tsdev.work/issues/962
     * @throws \common\components\exceptions\InvalidModelException
     * @transacted
     */
    public function actionAcceptDownloadPolicy()
    {
        $ps                                  = PsFacade::getPsByUserId(UserFacade::getCurrentUserId());
        $ps->dont_show_download_policy_modal = 1;
        AssertHelper::assertSave($ps);
    }

    /**
     *
     * @param int $attempId
     *
     * @return array|null
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionCanDownloadFiles($attempId)
    {
        $attemp       = $this->resolveAttemp($attempId);
        $printRequest = StoreOrderAttemptProcess::create($attemp);
        $printRequest->tryCheckDownloadFiles();

        return $this->jsonSuccess();
    }

    /**
     * Download model 3D file
     *
     * @param int $attempId
     * @param null $fileId
     *
     * @return string json
     * @throws \yii\base\Exception
     * @transacted
     */
    public function actionDownloadFiles($attempId, $fileId = null)
    {
        $attemp = $this->resolveAttemp($attempId);

        $this->tryCanDownloadFiles($attemp);

        [$zipPath, $archiveFileName] = PrepareDowloadFilesJob::resovleArchiveName($attemp, $fileId);

        if (!file_exists($zipPath)) {
            throw new \yii\base\Exception("File is not prepared");
        }

        $response = Yii::$app->response->sendFile($zipPath, $archiveFileName . '.zip');

        if ($attemp->status == StoreOrderAttemp::STATUS_ACCEPTED) {
            $this->orderService->printingAttemp($attemp);
        }
        $this->orderService->downloadedFiles($attemp);


        return $response;
    }

    /**
     * @param StoreOrderAttemp $attemp
     */
    private function tryCanDownloadFiles(StoreOrderAttemp $attemp)
    {
        if ($attemp->machine->isCutting()) {
            return true;
        } elseif ($attemp->machine->isPrinter()) {
            $printRequest = StoreOrderAttemptProcess::create($attemp);
            AssertHelper::assert($printRequest->canDownloadModel3D(), _t('site.ps', 'No access to 3D model file.'), BusinessException::class);
            AssertHelper::assert(
                $printRequest->canDownloadModel3dPartByLimit(),
                _t('site.ps', 'You need to finish printing your previous orders before you can download and print any more models.'),
                BusinessException::class
            );
        } else {
            throw new BusinessException(_t('site.ps', 'No access to 3D model file.'));
        }
    }

    /**
     * @param $attempId
     *
     * @return array
     */
    public function actionHasPostalLabel($attempId)
    {
        $attemp = $this->resolveAttemp($attempId);
        if (DeliveryPostalLabel::checkPostalLabelExists($attemp)) {
            return $this->jsonSuccess();
        }
        return $this->jsonReturn(['success' => false]);
    }

    /**
     * Download and return postal label file url
     *
     * @param int $attempId
     *
     * @return array|null
     * @throws ParcelCalculateException
     */
    public function actionLoadPostalImage($attempId)
    {
        $attemp = $this->resolveAttemp($attempId);
        $parcel = app('request')->post('parcel', null);
        try {
            $parcel = $parcel ? ParcelFactory::createFromJson($parcel) : null;
            $parcel = $parcel->isZerro() ? null : $parcel;
            Yii::$container->set(DeliveryExpensiveInterface::class, TwoTimeExpensive::class);
            $this->orderService->buyDeliveryAndLoadPostalImage($attemp, $parcel);
        } catch (ParcelCalculateException $exception) {
            $this->emailer->sendSupportGetPostalLabelsError($attemp->order, $exception->getMessage());
            logException($exception, 'easypost');
            throw $exception;
        } catch (\Exception $e) {
            $this->emailer->sendSupportGetPostalLabelsError($attemp->order, $e->getMessage());
            logException($e, 'easypost');
            $errorMessage = $e instanceof UserException
                ? $e->getMessage()
                : _t(
                    'site.ps',
                    'Postal label cannot be generated for this parcel. Please contact Treatstock support.'
                );
            return $this->jsonReturn($errorMessage, 406);
        }

        return $this->jsonReturn(
            [
                'postalLabelUrl'      => Url::to(
                    [
                        '/workbench/service-order/postal-image',
                        'attempId' => $attemp->id
                    ]
                ),
                'postalLabelPrintUrl' => Url::to(
                    [
                        '/workbench/service-order/print-postal-image',
                        'attempId' => $attemp->id
                    ]
                )
            ]
        );
    }


    /**
     *
     * @param
     *            $attempId
     *
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionPostalImage($attempId)
    {
        $attemp          = $this->resolveAttemp($attempId);
        $postalLabelFile = DeliveryPostalLabel::checkPostalLabelExists($attemp);
        AssertHelper::assert($postalLabelFile, "Cant find postal label file for attemp {$attemp->id}");
        return \Yii::$app->response->sendFile(
            $postalLabelFile,
            _t('front.site', 'Postal Label Order Id') . ' ' . $attemp->order->id,
            [
                'inline' => true
            ]
        );
    }

    /**
     * Special page to print postal label
     *
     * @param int $attempId
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     * @throws \common\components\exceptions\AssertException
     */
    public function actionPrintPostalImage($attempId)
    {
        $attemp          = $this->resolveAttemp($attempId);
        $postalLabelFile = DeliveryPostalLabel::checkPostalLabelExists($attemp);
        AssertHelper::assert($postalLabelFile, "Cant find postal label file for order {$attempId}");
        return $this->renderPartial(
            'print_postal_image',
            [
                'attempId' => $attemp->id
            ]
        );
    }

    /**
     * Decline order modal window
     *
     * @param int $attempId
     *
     * @throws UserException
     * @transacted
     */
    public function actionDeclineOrder($attempId)
    {
        $attemp = $this->resolveAttemp($attempId);
        $model  = new DeclineOrderForm();
        $model->load(app('request')->post(), '');
        AssertHelper::assertValidate($model);

        try {
            $this->orderService->declineAttemp($attemp, ChangeOrderStatusEvent::INITIATOR_PS, $model);
            \Yii::$app->session->addFlash("success", _t('front.site', 'Order successfully declined'));
            PsFacade::resetOrdersCount(UserFacade::getCurrentUser());
        } catch (UserException $e) {
            throw $e;
        } catch (\Exception $e) {
            \Yii::error($e);
            throw new UserException(_t('site.order', 'Error while declining order. Please try again.'), 0, $e);
        }
    }


    /**
     * Upload new result image
     *
     * @param $attemptId
     *
     * @return array
     */
    public function actionAddResultImage($attemptId)
    {
        $attempt = $this->resolveAttemp($attemptId);

        $form = new AddResultImageForm();
        AssertHelper::assert($form->load(Yii::$app->request->bodyParams, ''));
        AssertHelper::assertValidate($form);
        $result = $this->printAttemptModeratonService->addResult($attempt, $form);
        return $this->printAttmptSerializer->serialize($result);
    }


    /**
     * @param $attemptId
     * @param $moderationFileId
     */
    public function actionDeleteResultImage($attemptId, $moderationFileId)
    {
        $attempt          = $this->resolveAttemp($attemptId);
        $moderationFileId = StoreOrderAttemptModerationFile::tryFind($moderationFileId);
        $this->printAttemptModeratonService->deleteResultFile($attempt, $moderationFileId);
    }

    /**
     * @param int $attempId
     *
     * @return StoreOrderAttemp
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    private function resolveAttemp($attempId)
    {
        AssertHelper::assert($attempId, 'Order id not set');

        if (!UserFacade::getCurrentUserId()) {
            throw new ForbiddenHttpException(_t('site.ps', 'Plaese, login.'));
        }

        $attemp = StoreOrderAttemp::findByPk($attempId);

        if (!$attemp) {
            throw new NotFoundHttpException();
        }

        if (!UserFacade::isObjectOwner($attemp->interceptionCompany())) {
            throw new ForbiddenHttpException(_t('front.site', 'Access denied for current order'));
        }

        $attemp->fixDates();

        return $attemp;
    }

    public function actionCalcPositionFee()
    {
        $form           = new StoreOrderPositionForm();
        $amount         = (float)app('request')->post('price');
        $currency       = (string)app('request')->post('currency');
        $form->amount   = $amount;
        $form->currency = $currency;
        $fee            = (float)$form->calculateIncludeFee();
        $willGet        = $amount - $fee;
        if ($willGet < 0.02) {
            $willGet = '';
        } else {
            $willGet = displayAsCurrency($willGet, $form->currency);
        }
        return $this->jsonSuccess(['willGet' => $willGet, 'amount' => $amount, 'fee' => $fee]);
    }

    /**
     * @return Response
     * @throws \yii\base\Exception
     */
    public function actionAddPosition()
    {
        $form = new StoreOrderPositionForm();
        $form->load(app('request')->post());
        if (!$form->validate()) {
            $this->setFlashMsg(false, Html::errorSummary($form));
            return $this->redirect(app('request')->referrer);
        }

        try {
            $orderPosition = $form->createPosition();
            $this->orderPositionService->addOrderPosition($orderPosition, false);
            $this->setFlashMsg(true, _t('site.order', 'Additional service submitted'));
        } catch (HttpException $e) {
            $this->setFlashMsg(false, $e->getMessage());
        } catch (UserException $e) {
            $this->setFlashMsg(false, $e->getMessage());
        }

        return $this->redirect(app('request')->referrer);
    }

    /**
     * @return Response
     * @throws \yii\base\Exception
     */
    public function actionCancelAddonPosition()
    {
        $addonPositionId    = \Yii::$app->request->post('addonPositionId');
        $storeOrderPosition = StoreOrderPosition::tryFindByPk($addonPositionId);
        if ($storeOrderPosition->isOwnForUser(UserFacade::getCurrentUser()) && $storeOrderPosition->canBeCanceled()) {
            $this->orderPositionService->cancelOrderPosition($storeOrderPosition);
        }
        return $this->jsonReturn(['success' => true]);
    }

    /**
     * Run task for prepare files for download models,
     * also check if it runned and return actual status
     *
     * @param $attempId
     * @param null $fileId
     *
     * @return PrepareDownloadFileResponse
     */
    public function actionPrepareDownload($attempId, $fileId = null)
    {
        $attemp = $this->resolveAttemp($attempId);

        $this->tryCanDownloadFiles($attemp);

        $fileJob = PrepareDowloadFilesJob::find($attemp, $fileId);

        if (!$fileJob || ($fileJob->status == QueueGateway::JOB_STATUS_COMPLETED && !file_exists($fileJob->getResultData()))) {
            $task = PrepareDowloadFilesJob::createFromAttmp($attemp, $fileId);
            $this->queueGateway->addFileJob($task, QueueGateway::ADD_STRATEGY_ANYWAY);
            return $this->jsonReturn(PrepareDownloadFileResponse::createWait());
        }

        if ($fileJob->status == QueueGateway::JOB_STATUS_COMPLETED) {
            return $this->jsonReturn(PrepareDownloadFileResponse::createSuccess());
        }

        if ($fileJob->status == QueueGateway::JOB_STATUS_FAILED) {
            return $this->jsonReturn(PrepareDownloadFileResponse::createError(_t('site.order', "Error of preparing files") . ' ID: ' . $fileJob->id));
        }

        return $this->jsonReturn(PrepareDownloadFileResponse::createWait());
    }

    public function actionPartsList($attemptId)
    {
        $attemp       = $this->resolveAttemp($attemptId);
        $order        = $attemp->order;
        $this->layout = '@frontend/views/layouts/plain';
        return $this->render('report-part-list.php', ['order' => $order, 'attemp' => $attemp]);
    }

    public function actionDeleteDraftPreorderByPs()
    {
        $preorderId = Yii::$app->request->post('preorderId');
        $preorder   = Preorder::tryFindByPk($preorderId);
        UserFacade::tryCheckObjectOwner($preorder->interceptionCompany());
        $this->preorderService->deletePreorder($preorder);
        return $this->jsonSuccess(['id' => $preorder->id]);
    }

}
