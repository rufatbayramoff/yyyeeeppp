<?php

namespace frontend\modules\workbench\controllers;

use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\order\anonim\OrderMover;
use common\components\order\history\OrderHistoryService;
use common\components\UuidHelper;
use common\models\CuttingPackFile;
use common\models\factories\Model3dFactory;
use common\models\File;
use common\models\Model3dReplica;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\PaymentInvoice;
use common\models\Preorder;
use common\models\PreorderFile;
use common\models\repositories\Model3dRepository;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderHistory;
use common\models\StoreOrderPosition;
use common\models\User;
use common\models\UserLike;
use common\modules\cutting\models\PlaceCuttingOrderState;
use common\modules\cutting\services\CuttingPackService;
use common\modules\informer\InformerModule;
use common\modules\payment\services\PaymentService;
use common\modules\payment\services\StoreOrderPositionService;
use common\modules\thingPrint\components\ThingiverseOrderReview;
use common\services\ReviewService;
use common\services\StoreOrderService;
use common\traits\TransactedControllerTrait;
use frontend\models\model3d\PlaceOrderState;
use frontend\models\order\CancelOrderForm;
use frontend\models\order\ReviewForm;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\DynamicModel;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * OrderController
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class OrderController extends WorkbenchController
{
    /*
     * @var StoreOrderService
     */
    protected $orderService;

    /**
     * @var StoreOrderPositionService
     */
    protected $orderPositionService;

    /**
     * @var OrderHistoryService
     */
    protected $orderHistoryService;

    protected PlaceOrderState $placeOrderState;

    public Model3dRepository $model3dRepository;

    protected PlaceCuttingOrderState $placeCuttingOrderState;

    public function injectDependencies(
        StoreOrderService $orderService,
        StoreOrderPositionService $orderPositionService,
        OrderHistoryService $orderHistoryService,
        PlaceOrderState $placeOrderState,
        Model3dRepository $model3dRepository,
        PlaceCuttingOrderState $placeCuttingOrderState
    )
    {
        $this->orderService         = $orderService;
        $this->orderPositionService = $orderPositionService;
        $this->orderHistoryService  = $orderHistoryService;
        $this->placeOrderState      = $placeOrderState;
        $this->model3dRepository    = $model3dRepository;
        $this->placeCuttingOrderState = $placeCuttingOrderState;
    }


    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'receive' => ['post', 'get'],
                ]
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['confirm-anonim-order', 'add-anonymouse-review', 'add-review', 'anonim-confirm-print', 'view'], 'roles' => ['?']],
                    ['allow' => true, 'roles' => ['@']]
                ]
            ]
        ];
    }

    /**
     * @param $id
     *
     * @return string
     * @throws BusinessException
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     */
    public function actionView($id)
    {
        if (is_guest()) {
            return app()->user->loginRequired();
        }
        $order = StoreOrder::tryFindByPk($id, 'Order not found');

        if (!UserFacade::isObjectOwner($order)) {
            throw new BusinessException('No access to this order');
        }

        InformerModule::markAsViewedCustomerOrderInformer($order);

        return $this->render(
            'viewOrder',
            [
                'currentUser' => $this->getCurrentUser(),
                'storeOrder'  => $order
            ]
        );
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\base\InvalidParamException
     */
    public function actionRefresh($id)
    {

        $this->setFlashMsg(
            true,
            _t(
                "site.order",
                "Order #{order} information updated",
                [
                    'order' => H($id)
                ]
            )
        );
        return $this->redirect(
            Url::toRoute(['/workbench/orders', 'status' => $id])
        );
    }

    /**
     * request to cancel order
     *
     * @param int $id
     *            order id
     *
     * @return string|array
     * @throws BusinessException
     * @throws NotFoundHttpException
     * @transacted
     */
    public function actionCancel($id)
    {
        $order = StoreOrder::tryFindByPk($id, 'Order not found');
        if (!UserFacade::isObjectOwner($order)) {
            throw new BusinessException('No access to this order');
        }
        $orderCancelForm = new CancelOrderForm();
        if (app('request')->isPost) {
            $orderCancelForm->load(\Yii::$app->request->post());
            if ($orderCancelForm->validate()) {
                try {
                    if ($order->currentAttemp && !$order->currentAttemp->isNew()) {
                        throw new BusinessException(_t('site.store', 'The order cannot be canceled. Please contact support.'));
                    }
                    $this->orderService->declineOrder($order, ChangeOrderStatusEvent::INITIATOR_CLIENT, $orderCancelForm);
                    $this->setFlashMsg(true, _t('site.store', 'Order successfully cancelled!'));
                    return $this->jsonSuccess(['message' => 'reload']);
                } catch (BusinessException $e) {
                    return $this->jsonFailure(['message' => $e->getMessage()]);
                } catch (\Exception $e) {
                    Yii::error($e, 'app');
                    return $this->jsonFailure(['message' => _t('site.store', 'An error occurred while declining the order.')]);
                }
            }
            return $this->jsonFailure(['message' => _t('site.store', 'Please choose decline reason!')]);
        }

        return $this->renderAdaptive(
            'cancelOrder',
            [
                'cancelForm' => $orderCancelForm,
                'orderId'    => $id
            ]
        );
    }

    public function actionRemove($id)
    {
        $order = \common\models\StoreOrder::tryFindByPk($id, 'Order not found');
        if (!UserFacade::isObjectOwner($order) || !$order->canUserRemove()) {
            throw new BusinessException("No access to this order");
        }
        $oldOrderState      = $order->order_state;
        $order->order_state = StoreOrder::STATE_REMOVED;
        $order->safeSave();
        if ($order->currentAttemp) {
            $order->currentAttemp->status = StoreOrderAttemp::STATUS_CANCELED;
            $order->currentAttemp->safeSave();
        }
        if ($order->preorder && $order->preorder->status === Preorder::STATUS_ACCEPTED) {
            $order->preorder->status = Preorder::STATUS_CANCELED_BY_CLIENT;
            $order->preorder->safeSave();
        }
        $this->orderHistoryService->log($order, StoreOrderHistory::USER_REMOVE_ORDER, ['newOrderState' => $order->order_state, 'oldOrderState' => $oldOrderState],
            'Client remove order');
        $this->redirect('/workbench/orders');
    }

    public function actionAnonimConfirmPrint($id, $hash, $flag = false)
    {
        $order = StoreOrder::tryFindByPk($id);
        if ($order->isThingiverseOrder()) {
            $orderHash = ThingiverseOrderReview::getOrderAccessHash($order);
            if ($orderHash !== $hash) {
                throw new ForbiddenHttpException('No access to this order');
            }
        } else {
            // only for thingiverse order for now.
            throw new ForbiddenHttpException('No access to this order');
        }
        if ($flag) {
            $currentAttempt = $order->currentAttemp;
            $redirUrl       = ['/workbench/order/anonim-confirm-print', 'id' => $order->id, 'hash' => $hash];
            if ($currentAttempt->isAcceptedByUser()) {

                return $this->redirect($redirUrl);
            }
            UserLike::addRecord(
                [
                    'user_id'     => $order->user_id,
                    'object_type' => UserLike::TYPE_ORDER_ATTEMPT,
                    'object_id'   => $order->current_attemp_id
                ]
            );
            $this->setFlashMsg(true, _t('site.order', 'Liked results'));
            if ($currentAttempt->canUserLike()) {
                return $this->redirect($redirUrl);
            }

            // turned off for now
            /*$dbTransaction = \Yii::$app->db->beginTransaction();
            try {
                $currentAttempt->moderation->acceptModeration();
                $this->orderService->readyToSendAttemp($currentAttempt);
                $dbTransaction->commit();
                $this->setFlashMsg(true, _t('site.order', 'Thank you, the print results have been accepted.'));
            } catch (\Exception $e) {
                $dbTransaction->rollBack();
                logException($e, 'tsdebug');
                $this->setFlashMsg(false, $e->getMessage());
            }*/
            return $this->redirect($redirUrl);
        }

        $this->view->title             = _t('site.order', 'Print Results for Order #{orderId}', ['orderId' => $order->id]);
        $this->view->params['hidenav'] = true;
        return $this->render('anonimLike', ['model' => $order, 'attempt' => $order->currentAttemp, 'hash' => $hash]);
    }


    /**
     * currently Like results button functional
     *
     * @param $attemptId
     * @param $orderId
     *
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionClientAccept($attemptId, $orderId, $dislike = 0)
    {
        $order         = $this->resolveOrder($orderId);
        $currentAttemp = StoreOrderAttemp::findByPk($attemptId);

        if ($currentAttemp->isAcceptedByUser()) {
            return $this->redirect(['/workbench/order/view', 'id' => $orderId]);
        }

        // mark as liked
        UserLike::addRecord(
            [
                'user_id'     => $order->user_id,
                'object_type' => UserLike::TYPE_ORDER_ATTEMPT,
                'object_id'   => $attemptId,
                'is_dislike'  => (int)$dislike
            ]
        );

        if ($currentAttemp->canUserLike()) {
            if ($dislike) {
                $this->orderService->clientDislike($currentAttemp);
            } else {
                $this->setFlashMsg(true, _t('site.order', 'Liked results'));
            }
            return $this->redirect(
                [
                    '/workbench/order/view',
                    'id' => $orderId
                ]
            );
        }
        // turned off for now
        /*
        $dbTransaction = \Yii::$app->db->beginTransaction();
        try {

            $currentAttemp->moderation->acceptModeration();
            $this->orderService->readyToSendAttemp($currentAttemp);
            $dbTransaction->commit();
            $this->setFlashMsg(true, _t('site.order', 'Thank you, the print results have been accepted.'));
        } catch (\Exception $e) {
            $dbTransaction->rollBack();
            logException($e, 'tsdebug');
            $this->setFlashMsg(false, $e->getMessage());
        } */

        return $this->redirect(
            [
                '/workbench/order/view',
                'id' => $orderId
            ]
        );
    }

    public function actionReceiveAjax($id)
    {
        $order = $this->getStoreOrder($id);
        $this->orderService->receivedAttempTransaction($order->currentAttemp, ChangeOrderStatusEvent::INITIATOR_CLIENT);
        return $this->jsonSuccess();
    }

    /**
     * user receives his order, finally
     *
     * @param int $id
     *
     * @return string
     * @throws BusinessException
     * @throws NotFoundHttpException
     */
    public function actionReceive($id)
    {
        $order = $this->getStoreOrder($id);
        try {
            $this->orderService->receivedAttempTransaction($order->currentAttemp, ChangeOrderStatusEvent::INITIATOR_CLIENT);
            $this->setFlashMsg(true, _t('site.order', 'Order marked as received. Thank you.'));
        } catch (\Exception $e) {
            $this->setFlashMsg(false, $e->getMessage());
        }
        return $this->redirect(
            [
                '/workbench/orders'
            ]
        );
    }

    /**
     * Create order review
     *
     * @param $orderId
     *
     * @return array|null
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionAddReview($orderId)
    {
        $form = new ReviewForm();
        $form->loadFromPost(Yii::$app->request->post());
        AssertHelper::assertValidate($form);

        $user  = null;
        $order = StoreOrder::tryFindByPk($orderId);
        if (is_guest()) {
            if (!$order->isThingiverseOrder()) {
                throw new ForbiddenHttpException('Login required');
            }
            $userId = Yii::$app->session->get(ThingiverseOrderReview::SESSION_USER_KEY, false);
            if ($userId) {
                $user = User::tryFindByPk($userId);
            }
        } else {
            $user  = UserFacade::getCurrentUser();
            $order = $this->resolveOrder($orderId);
        }
        if ($user) {
            $review = Yii::$app->reviewService->createOrderReview($form, $order, $user);
            //InformerModule::addInformer($order->currentAttemp->ps->user, ReviewInformer::class);
            if ($order->currentAttemp->status != StoreOrderAttemp::STATUS_RECEIVED) {
                if ($review->rating_quality >= 4 && $order->isThingiverseOrder()) {
                    $this->orderService->receivedAttemp($order->currentAttemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
                } else {
                    if ($review->rating_quality >= 4) { // logic if user came from email
                        $this->orderService->receivedAttemp($order->currentAttemp, ChangeOrderStatusEvent::INITIATOR_SYSTEM);
                    }
                }
            }
            if ($review->rating_quality < 4 || $review->rating_communication < 4 || $review->rating_speed < 4) {
                $emailer = new Emailer();
                $emailer->sendClientOrderReviewBad($order);
            }

            return [
                'id'     => $review->id,
                'rating' => ReviewService::calculateAvgRating($review)
            ];
        }

        return null;
    }

    /**
     * @param $orderId
     * @param int $flag
     *
     * @return Response
     * @throws \common\models\message\exceptions\InvalidBindedObject
     * @throws \yii\base\InvalidConfigException
     */
    public function actionDispute($orderId, $flag = 1)
    {
        $storeOrder = StoreOrder::findByPk($orderId);
        $this->orderService->switchDispute($storeOrder);
        if ($storeOrder->currentAttemp && $storeOrder->currentAttemp->isPayed()) {
            InformerModule::addCompanyOrderInformer($storeOrder->currentAttemp);
        }
        if ($flag == 1) {
            return $this->redirect(\common\models\message\helpers\UrlHelper::supportMessageRoute($storeOrder));
        }
        return $this->redirect(app('request')->referrer);
    }

    /**
     * Confirm anonim order
     *
     * @param $confirmHash
     *
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function actionConfirmAnonimOrder($confirmHash)
    {
        $order = StoreOrder::tryFind(['confirm_hash' => $confirmHash]);

        if (!$order->isAnonim()) {

            if (!UserFacade::isObjectOwner($order)) {
                throw new ForbiddenHttpException(_t('store.order', 'You don’t have access to this order.'));
            }

            \Yii::$app->getSession()->setFlash('success', _t('store.order', 'Order already confirmed.'));
            return $this->redirect(['/workbench/orders/index']);
        }

        /** @var FrontUser $realOrderUser */
        $realOrderUser = FrontUser::tryFind(['email' => $order->shipAddress->email]);

        $orderMover = new OrderMover();
        $orderMover->move($order, $order->user, $realOrderUser);

        if ($realOrderUser->status == FrontUser::STATUS_UNCONFIRMED) {
            $realOrderUser->status = User::STATUS_ACTIVE;
            AssertHelper::assertSave($realOrderUser);
        }

        if (is_guest() || UserFacade::getCurrentUser()->id != $realOrderUser->id) {
            \Yii::$app->user->login($realOrderUser);
        }

        \Yii::$app->getSession()->setFlash('success', _t('store.order', 'Order confirmation successful.'));
        return $this->redirect(['/workbench/orders']);
    }

    /**
     * @return array|bool|Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionPositionCheckout()
    {
        if (!app('request')->isPost) {
            return false;
        }

        $paymentNonce  = app('request')->post('payment_method_nonce');
        $orderPosition = StoreOrderPosition::tryFindByPk(app('request')->post('order_id'));
        $this->resolveOrder($orderPosition->order_id); // to validate

        try {
            $this->orderPositionService->acceptOrderPosition($orderPosition, $paymentNonce);
            \Yii::$app->getSession()->setFlash('success', _t('store.order', 'Additional service accepted.'));
            InformerModule::addCompanyOrderInformer($orderPosition->order->currentAttemp);
        } catch (\Exception $e) {
            \Yii::$app->getSession()->setFlash('success', $e->getMessage());
            logException($e, 'tsdebug');
        }

        return $this->redirect('/workbench/order/view/' . $orderPosition->order_id);

    }

    /**
     * @param $invoiceUuid
     *
     * @return array|string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionPositionDecline($invoiceUuid)
    {
        $paymentInvoice = PaymentInvoice::tryFind($invoiceUuid);
        $order          = $this->resolveOrder($paymentInvoice->orderPosition->order_id);

        $model = new DynamicModel(['orderId', 'declineReason']);
        $model->addRule(['declineReason'], 'string', ['max' => 256])
            ->addRule(['orderId'], 'safe');
        $model->orderId = $order->id;

        if (app('request')->isPost) {
            $model->load(\Yii::$app->request->post());
            if ($model->validate()) {
                try {
                    $this->orderPositionService->declineOrderPosition($order, $paymentInvoice->orderPosition, $model->declineReason);
                    /** @var PaymentService $paymentService */
                    $paymentService = Yii::createObject(PaymentService::class);
                    $paymentService->setCanceledInvoice($paymentInvoice);
                    $this->setFlashMsg(true, _t('store.order', 'Additional service declined.'));
                    return $this->jsonSuccess(['message' => 'reload']);
                } catch (BusinessException $e) {
                    return $this->jsonFailure(['message' => $e->getMessage()]);
                }
            }
            return $this->jsonFailure(['message' => _t('site.store', 'Please enter decline description.')]);
        }

        return $this->renderAdaptive(
            'declineOrderPosition',
            [
                'cancelForm' => $model
            ]
        );
    }

    public function actionRepeat(int $id)
    {
        $order = StoreOrder::tryFindByPk($id, 'Order not found');
        if (!UserFacade::isObjectOwner($order)) {
            throw new UserException("No access to this order");
        }
        $companyIsActive = $order?->currentAttemp?->company?->isActive();
        if($companyIsActive === false) {
            $url = Url::toRoute('company-services',true);
            throw new UserException("Service is inactive. Try to find new vendor\n $url");
        }
        if($order->isForPreorder()){
            return $this->duplicatePreorder($order);
        }
        return $this->duplicateOrder($order);

    }

    /**
     * Resolve order and check access for current user
     *
     * @param int $orderId
     *
     * @return StoreOrder
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    protected function resolveOrder($orderId)
    {
        AssertHelper::assert($orderId, 'Order id not set');

        if (!UserFacade::getCurrentUserId()) {
            throw new ForbiddenHttpException(_t('site.order', 'Please login'));
        }

        /** @var StoreOrder $order */
        $order = StoreOrder::findByPk($orderId);

        if (!$order) {
            throw new NotFoundHttpException();
        }

        if (!UserFacade::isObjectOwner($order)) {
            throw new ForbiddenHttpException(_t('site.order', 'Access denied for current order'));
        }
        return $order;
    }

    /**
     * @param StoreOrder $order
     * @return Response
     * @throws \ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    protected function duplicateOrder(StoreOrder $order): Response
    {
        if($order->hasCuttingPack()) {
            $cuttingPackService = Yii::createObject(CuttingPackService::class);
            $cuttingPack = $cuttingPackService->duplicate($order);
            $this->placeCuttingOrderState->setCuttingPack($cuttingPack);
            $this->placeCuttingOrderState->setStateUid($cuttingPack->uuid);
            return $this->redirect(['/my/order-laser-cutting/select-parts', 'posUid' => $cuttingPack->uuid]);
        }
        /** @var Model3dReplica $model3dReplica */
        $model3dReplica = $order->getFirstReplicaItem();
        $model3d = Model3dFactory::createModel3dByModel3d($model3dReplica->originalModel3d);
        $model3d->setMasterStoreUnit($model3dReplica->storeUnit);
        $this->model3dRepository->save($model3d);
        $this->placeOrderState->setStateUid($model3d->uuid);
        $this->placeOrderState->setModel3d($model3d);
        return $this->redirect(['/my/print-model3d/printers', 'posUid' => $model3d->uuid]);
    }

    protected function duplicatePreorder(StoreOrder $order)
    {
        $psId = $order->currentAttemp->ps_id;
        $preorder = $order->preorder;
        $description = _t('site.order',"Repeate Order #{$order->id} (Preorder #{$preorder->id})");
        $fileUuids = array_map(function(File $file) {
            return $file->uuid;
        },$preorder->files);
        $files = implode('-',$fileUuids);
        return $this->redirect(['/preorder/quote/request-quote','psId' => $psId,'uploadedFilesUuids' => $files,'description' => $description]);
    }

    /**
     * @param $id
     * @return StoreOrder
     * @throws BusinessException
     * @throws NotFoundHttpException
     */
    protected function getStoreOrder($id): StoreOrder
    {
        $order = StoreOrder::tryFindByPk($id, 'Order not found');
        if (!UserFacade::isObjectOwner($order)) {
            throw new BusinessException("No access to this order");
        }
        if ($order->currentAttemp->isResolved()) {
            throw new BusinessException("This order is already complete");
        }
        return $order;
    }

}
