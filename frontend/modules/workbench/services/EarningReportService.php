<?php

namespace frontend\modules\workbench\services;

use common\models\PaymentAccount;
use common\models\PaymentInvoice;
use common\models\User;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\widgets\FeeViewWidget;
use common\modules\payment\widgets\ReceiptViewWidget;
use common\services\GeneratorPdf;
use frontend\modules\workbench\components\InvoiceReportForm;
use yii\db\Expression;

class EarningReportService
{
    /**
     * @var GeneratorPdf
     */
    private $invoiceReportPdf;
    /**
     * @var PaymentAccountService
     */
    private $paymentAccountService;

    public function __construct(
        GeneratorPdf $invoiceReportPdf,
        PaymentAccountService $paymentAccountService
    )
    {
        $this->invoiceReportPdf = $invoiceReportPdf;
        $this->paymentAccountService = $paymentAccountService;
    }

    public function fee(InvoiceReportForm $form, User $user): string
    {
        $account = $this->paymentAccount($user);
        $content = FeeViewWidget::widget(['paidInvoices' => $this->payments($form, $account)]);
        return $this->invoiceReportPdf->write($content);
    }

    public function invoice(InvoiceReportForm $form, User $user): string
    {
        $account = $this->paymentAccount($user);
        $content = ReceiptViewWidget::widget(['paidInvoices' => $this->payments($form, $account)]);
        return $this->invoiceReportPdf->write($content);
    }

    /**
     * @param InvoiceReportForm $form
     * @param PaymentAccount $payment
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    protected function payments(InvoiceReportForm $form, PaymentAccount $payment)
    {
        $query = PaymentInvoice::find();
        $query->withPaymentAccount($payment->id);
        $query->withOutAffiliate($payment->user);
        $query->betweenUpdateAt($form->getTo(), $form->from);
        $query->orderBy(new Expression('max(payment_detail.updated_at) DESC'));
        $query->groupBy(['payment_invoice.uuid']);
        return $query->all();
    }

    /**
     * @param User $user
     * @return PaymentAccount
     * @throws \common\components\exceptions\BusinessException
     * @throws \yii\base\InvalidConfigException
     */
    protected function paymentAccount(User $user): PaymentAccount
    {
        return $this->paymentAccountService->getUserPaymentAccount($user);
    }
}