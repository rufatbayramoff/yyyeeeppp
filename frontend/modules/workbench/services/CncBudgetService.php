<?php


namespace frontend\modules\workbench\services;


use common\models\Model3dPart;
use common\models\PsCncMachine;
use common\models\repositories\Model3dRepository;
use common\modules\cnc\api\Api;
use common\modules\cnc\api\ApiException;
use common\modules\cnc\api\Preset;
use common\modules\cnc\api\responses\costing\CostingResponse;
use common\modules\cnc\repositories\PsCncMachineRepository;
use common\services\Model3dService;
use yii\base\UserException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class CncBudgetService
{
    /**
     * @var PsCncMachineRepository
     */
    protected $psCncMachineRepository;

    /** @var Model3dRepository */
    protected $model3dRepository;
    /**
     * @var Model3dService
     */
    protected $model3dService;
    /**
     * @var Api
     */
    protected $api;

    public function __construct(
        PsCncMachineRepository $psCncMachineRepository,
        Model3dRepository $model3dRepository,
        Model3dService $model3dService,
        Api $api
    ) {
        $this->psCncMachineRepository = $psCncMachineRepository;
        $this->model3dRepository = $model3dRepository;
        $this->model3dService = $model3dService;
        $this->api = $api;
    }

    /**
     * @param string $partId
     * @param PsCncMachine $psCncMachine
     * @param Preset $form
     * @return CostingResponse|null
     * @throws ApiException
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws UserException
     */
    public function budget(string $partId,PsCncMachine $psCncMachine, Preset $form): ?CostingResponse
    {
        $part = $this->findPart($partId);
        try {
            $part->model3dPartCncParams->clearErrorMsg();
            $part->model3dPartCncParams->safeSave();
            return $this->api->budget(
                $part,
                $psCncMachine,
                $form->toJson()
            );
        } catch (ApiException $exception) {
            $part->model3dPartCncParams->budgetError($exception->getMessage());
            $part->model3dPartCncParams->safeSave();
            throw $exception;
        }
    }

    protected function findPart(string $partId)
    {
        /** @var Model3dPart $part */
        $part = $this->model3dRepository->getPartByUid($partId);

        if (!$part) {
            throw new NotFoundHttpException("Part {$partId} not found");
        }

        if (!$this->model3dService->isAvailableForCurrentUser($part->model3d)) {
            throw new ForbiddenHttpException('Cant access this model.');
        }

        return $part;
    }
}