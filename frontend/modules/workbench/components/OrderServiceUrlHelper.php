<?php


namespace frontend\modules\workbench\components;


use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use frontend\modules\preorder\components\PreorderUrlHelper;

class OrderServiceUrlHelper
{

    public static function viewList($statusGroup, $anchor = '', $scheme = false)
    {
        if ($statusGroup == 'quote') {
            return PreorderUrlHelper::viewPsList($scheme);
        } else {
            return ($scheme ? param('server') : '') . '/workbench/service-orders/' . $statusGroup . ($anchor ? '#' . $anchor : '');
        }
    }

    public static function view(StoreOrder $order): string
    {
        return '/workbench/service-order/view?id=' . $order->id;
    }

    public static function print(StoreOrderAttemp $attemp): string
    {
        return '/workbench/service-order/print?attempId=' . $attemp->id;
    }
}