<?php
namespace frontend\modules\workbench\components;

use common\components\BaseForm;

class InvoiceReportForm extends BaseForm
{
    public $from;
    public $to;

    public function rules(): array
    {
        return [
            [['from', 'to'], 'date', 'format' => 'php:Y-m-d']
        ];
    }


    /**
     * @return string|null
     */
    public function getTo(): ?string
    {
        if($this->to) {
            return (new \DateTime("{$this->to} +1 day"))->format('Y-m-d');
        }
        return null;
    }
}