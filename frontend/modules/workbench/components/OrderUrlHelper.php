<?php


namespace frontend\modules\workbench\components;


use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use yii\helpers\Url;

class OrderUrlHelper
{
    /**
     * @param StoreOrderAttemp $attemp
     * @param bool $scheme
     * @return string
     */
    public static function viewStoreOrderAttemp(StoreOrderAttemp $attempt, $scheme = false)
    {
        return ($scheme ? param('server') : '') . '/workbench/service-order/view?attemptId=' . $attempt->id;
    }


    public static function viewModel3dReplica($model3dReplica)
    {
        return Url::toRoute(['/store/model3d-replica', 'id' => $model3dReplica->id]);
    }

    public static function viewByCustomer(StoreOrder $order)
    {
        return '/workbench/order/view/' . $order->id;
    }

    public static function customerOpenDispute(StoreOrder $order)
    {
        return '/workbench/order/dispute?orderId='.$order->id.'&flag=1';
    }

    public static function customerCloseDispute(StoreOrder $order)
    {
        return '/workbench/order/dispute?orderId='.$order->id.'&flag=0';
    }
}