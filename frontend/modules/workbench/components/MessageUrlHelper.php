<?php


namespace frontend\modules\workbench\components;

use common\models\MsgTopic;

class MessageUrlHelper
{

    public static function topic(MsgTopic $topic)
    {
        return "/messages/topic?topicId={$topic->id}";
    }
}