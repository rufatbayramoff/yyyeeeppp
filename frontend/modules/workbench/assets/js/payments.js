$(document).ready(function () {

    $('.btn-report').on('click', function (e) {
        e.preventDefault();
        var submitUrl = $(this).attr('href');
        var form = $('#report-form').eq(0);
        form.attr('action', submitUrl);
        form.submit();
    });

});