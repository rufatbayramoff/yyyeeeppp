<?php
namespace frontend\modules\workbench\assets;


use yii\web\AssetBundle;
use yii\web\YiiAsset;

class PaymentAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/workbench/assets';

    public $js = [
        'js/payments.js',
    ];

    public $depends = [
        YiiAsset::class
    ];
}