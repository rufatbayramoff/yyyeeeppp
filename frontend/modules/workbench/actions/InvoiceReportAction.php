<?php


namespace frontend\modules\workbench\actions;


use common\components\exceptions\InvalidModelException;
use frontend\modules\workbench\services\EarningReportService;
use frontend\modules\workbench\components\InvoiceReportForm;
use Yii;
use yii\base\Action;
use yii\base\InvalidArgumentException;

class InvoiceReportAction extends Action
{
    public $method = 'invoice';
    public $fileName = 'invoice_report.pdf';

    public function run(EarningReportService $service)
    {
        $user = $this->controller->getCurrentUser(true);
        $form = \Yii::createObject(InvoiceReportForm::class);
        if($form->load(\Yii::$app->request->queryParams,'') && $form->validate()) {
            $file = $service->{$this->method}($form, $user);
            return Yii::$app->response->sendFile($file, $this->fileName);
        }
        throw new InvalidArgumentException();
    }
}