<?php


namespace frontend\modules\workbench\actions;


use common\models\MsgFolder;
use common\models\User;
use frontend\models\community\MessageFacade;
use Yii;
use yii\base\Action;
use yii\web\HttpException;

class MessageAction extends Action
{
    public string $type = 'all';

    public function run($folderAlias = null)
    {
        $this->controller->checkStatusDraftCompany();

        /** @var User $user */
        $user   = $this->controller->getCurrentUser(true);
        $folder = $folderAlias ? $this->resolveFolder($folderAlias) : Yii::createObject(MsgFolder::class);
        return $this->controller->render(
            'topics',
            [
                'user'           => $user,
                'folder'         => $folder,
                'searchValue'    => '',
                'topicsProvider' => MessageFacade::getFilteredTopicsProvider($user, $folder, $this->type),
                'type'           => $this->type
            ]
        );
    }

    /**
     * Resolve Folder by alias.
     * If not found - throw exception
     *
     * @param string $folderAlias
     * @return MsgFolder
     * @throws HttpException
     */
    private function resolveFolder($folderAlias)
    {
        $folder = MsgFolder::findOne(
            [
                'alias' => $folderAlias
            ]
        );
        if (!$folder) {
            throw new HttpException('Cant find message folder ' . $folderAlias);
        }
        return $folder;
    }
}