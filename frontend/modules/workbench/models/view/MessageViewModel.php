<?php


namespace frontend\modules\workbench\models\view;


use common\models\MsgTopic;
use common\models\Preorder;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\User;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\workbench\components\OrderServiceUrlHelper;
use frontend\modules\workbench\components\OrderUrlHelper;
use yii\helpers\Html;

class MessageViewModel
{
    /**
     * @var MsgTopic
     */
    public $topic;
    /**
     * @var User
     */
    public $user;

    /**
     * @var array
     */
    public $messages;

    /**
     * @var StoreOrderAttemp
     */
    protected $currentAttempt;


    public function link()
    {
        return Html::a($this->topicTitle(), $this->href());
    }

    public function linkWithText(string $text)
    {
        return Html::a($text, $this->href());
    }

    /**
     * @return string
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     * @throws \Exception
     */
    public function previewModel()
    {

        if ($this->isCutting()) {
            $pack = $this->cuttingPack();
            return Html::img(
                $pack->getPreviewUrl(),
                [
                    'width'    => '100',
                    'alt'      => \H($pack->getTitle()),
                    'class'    => 'direct-chat__order-img',
                    'ng-click' => 'popupImageView($event)'
                ]
            );
        }
        if ($this->isPrinter()) {
            $pack = $this->printerPack();
            return Html::img(
                $pack->getCoverUrl(),
                [
                    'width'    => '100',
                    'alt'      => \H($pack->getTitle()),
                    'class'    => 'direct-chat__order-img',
                    'ng-click' => 'popupImageView($event)'
                ]
            );
        }
    }

    /**
     * @return \common\models\CuttingPack|null
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    protected function cuttingPack()
    {
        /** @var StoreOrder $bindObject */
        $bindObject = $this->bindObject();
        return $bindObject->getCuttingPack();
    }

    protected function printerPack()
    {
        $attempt = $this->attempt();
        return $attempt->order->getFirstReplicaItem();
    }

    protected function isCutting()
    {
        $attempt = $this->attempt();
        return $attempt && $attempt->order && $attempt->order->hasCuttingPack();
    }

    protected function isPrinter()
    {
        $attempt = $this->attempt();
        return $attempt && $attempt->order && $attempt->order->isFor3dPrinter();
    }

    public function materials(): array
    {
        if ($this->isCutting()) {
            return $this->cuttingPackMaterials();
        }
        if ($this->isPrinter()) {
            return $this->printerPackMaterials();
        }
        return [];
    }

    /**
     * @return string
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    protected function href(): string
    {
        if ($this->isPreorder()) {
            return PreorderUrlHelper::viewPs($this->topic->getBindedObject());
        }
        if (!$this->isBindToOrder()) {
            return '#';
        }
        /** @var StoreOrder $order */
        $order = $this->bindObject();
        if ($this->isCustomer()) {
            return OrderUrlHelper::viewByCustomer($order);
        }
        if (!$order->isPayed() || !$order->currentAttemp) {
            return '#';
        }
        if ($order->currentAttemp->isAccepted()) {
            return OrderServiceUrlHelper::print($order->currentAttemp);
        }
        return OrderServiceUrlHelper::view($order);
    }

    /**
     * @return StoreOrderAttempViewModel|null
     */
    public function manufacturePrice()
    {
        $attemp = $this->attempt();
        if (!$attemp) {
            return null;
        }
        if ($this->isOrderForPreorder()) {
            return displayAsMoney($attemp->order->preorder->primaryPaymentInvoice->preorderAmount->getAmountManufacturerWithRefund());
        }
        $award = $attemp->order->primaryPaymentInvoice->getManufacturerAward();
        return $award ? displayAsMoney($award) : null;
    }

    /**
     * @return false
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function isCustomer(): bool
    {
        /** @var StoreOrder $order */
        $order = $this->bindObject();
        if (!$order) {
            return false;
        }
        return $order->isCustomer($this->user);
    }

    /**
     * @return \common\models\PaymentInvoiceItem[]
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function paymentInvoice()
    {
        /** @var StoreOrder $order */
        $order          = $this->bindObject();
        $paymentInvoice = $order->getPrimaryInvoice();
        return $paymentInvoice->paymentInvoiceItems;
    }

    public function getAmountTotalWithRefund()
    {
        /** @var StoreOrder $order */
        $order          = $this->bindObject();
        $paymentInvoice = $order->getPrimaryInvoice();
        return $paymentInvoice->getAmountTotalWithRefund();
    }

    /**
     * @return bool
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function isBindToOrder(): bool
    {
        return $this->topic->isOrder() && $this->bindObject();
    }

    public function isPreorder(): bool
    {
        return $this->topic->isPreorder() && $this->bindObject();
    }

    public function preorderCustomerUrl(): string
    {
        return $this->isPreorder() ? PreorderUrlHelper::viewCustomer($this->bindObject()) : '';
    }

    /**
     * @return \common\models\StoreOrderAttemptModerationFile[]|false
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function orderFiles()
    {
        /** @var StoreOrder $order */
        $order = $this->bindObject();
        if (!$order) {
            return false;
        }
        $currentAttemp = $order->currentAttemp;
        if (!$currentAttemp) {
            return false;
        }
        $moderation = $currentAttemp->moderation;
        if (!$moderation) {
            return false;
        }
        return $moderation->files;
    }

    /**
     * @return bool
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function orderHasFiles(): bool
    {
        return empty($this->orderFiles());
    }

    /**
     * @return mixed|null
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function bindObject()
    {
        return $this->topic->getBindedObject();
    }

    /**
     * @return string
     */
    public function topicTitle(): string
    {
        if ($this->attempt()) {
            return $this->attempt()->getTitleLabel();
        }
        return $this->topic->title;
    }

    /**
     * @return bool
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function orderHasDescription()
    {
        if (!$this->isOrderForPreorder()) {
            return false;
        }
        $attemp = $this->attempt();
        return $attemp && (!empty($attemp->order->preorder->message) || !empty($attemp->order->preorder->description));
    }

    public function preorderHasDescription()
    {
        /** @var Preorder $preorder */
        $preorder = $this->bindObject();
        return !empty($preorder->message) || !empty($preorder->description);

    }

    /**
     * @return string
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function preorderDescription(): ?string
    {
        /** @var Preorder $preorder */
        $preorder = $this->bindObject();
        return $preorder->description;
    }

    /**
     * @return string
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function preorderMessage(): ?string
    {
        /** @var Preorder $preorder */
        $preorder = $this->bindObject();
        return $preorder->message;
    }

    public function preorderBudget()
    {
        /** @var Preorder $preorder */
        $preorder = $this->bindObject();
        return displayAsMoney($preorder->getBudgetMoney());
    }

    public function preorderFiles()
    {
        /** @var Preorder $preorder */
        $preorder = $this->bindObject();
        return $preorder->files;
    }

    public function preorderDeadline()
    {
        /** @var Preorder $preorder */
        $preorder = $this->bindObject();
        return _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}',
            ['n' => (int)$preorder->offer_estimate_time]);
    }

    /**
     * @return string
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function orderDescription(): ?string
    {
        $attemp = $this->attempt();
        if ($attemp) {
            return $attemp->order->preorder->description;
        }
        return null;
    }

    public function orderStatus()
    {
        $attempt = $this->attempt();
        if ($attempt) {
            return $attempt->getCustomerTextStatus();
        }
        return null;
    }

    /**
     * @return string
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function orderMessage(): ?string
    {
        $attemp = $this->attempt();
        if ($attemp) {
            return $attemp->order->preorder->message;
        }
        return null;
    }

    /**
     * @return bool
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    public function isOrderForPreorder(): bool
    {
        $attempt = $this->attempt();
        return $attempt && $attempt->order && $attempt->preorder;
    }

    public function attempt(): ?StoreOrderAttemp
    {
        if (!$this->currentAttempt) {
            if ($this->isPreorder()) {
                /** @var Preorder $preorder */
                $preorder             = $this->bindObject();
                $this->currentAttempt = $preorder->storeOrderAttempt;
            }
            if ($this->isBindToOrder() && $this->bindObject()->currentAttemp) {
                $this->currentAttempt = $this->bindObject()->currentAttemp;
            }
        }
        return $this->currentAttempt;
    }

    /**
     * @return array
     * @throws \common\models\message\exceptions\BindedObjectNotFound
     */
    protected function cuttingPackMaterials(): array
    {
        $result = [];
        $pack   = $this->cuttingPack();
        if ($pack->material) {
            $result['material'] = $pack->material->title;
        }
        if ($pack->color) {
            $result['color'] = ['rgb' => $pack->color->rgb, 'title' => $pack->color->title];
        }
        return $result;
    }

    public function printerPackMaterials(): array
    {
        $result         = [];
        $model3dReplica = $this->printerPack();
        $texture        = $model3dReplica->getKitTexture();
        if ($texture) {
            if ($texture->printerMaterial) {
                $result['material'] = $texture->printerMaterial->title;
            }
            if ($texture->printerColor) {
                $result['color'] = ['rgb' => $texture->printerColor->rgb, 'title' => $texture->printerColor->title];
            }
        }
        return $result;
    }
}