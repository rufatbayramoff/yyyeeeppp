<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 28.08.18
 * Time: 13:58
 */

namespace frontend\modules\workbench\models\view;

use common\components\ArrayHelper;
use common\components\order\PriceCalculator;
use common\models\File;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\PrinterColor;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\fee\FeeHelperStub;
use common\modules\payment\services\PaymentPreorderService;
use common\modules\payment\services\PaymentStoreOrderService;
use frontend\components\UserUtils;
use frontend\models\model3d\Model3dFacade;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\models\user\UserFacade;
use frontend\widgets\SiteHelpWidget;
use lib\delivery\delivery\models\DeliveryPostalLabel;
use Yii;
use yii\base\BaseObject;
use yii\helpers\Html;

/**
 * Class StoreOrderAttempViewModel
 *
 * @property string $titleItem
 * @property string $titleItemRaw
 * @property string $imgItem
 * @property string $urlItem
 * @property string $qty
 * @property string $price
 * @property string $priceNote
 * @property bool $isFor3dPrinter
 * @property string $titleService
 * @property string $materialTitle
 * @property $printerColor
 * @property File $imagesObject
 */
class StoreOrderAttempViewModel extends BaseObject
{
    /** @var StoreOrderAttemp */
    protected $attempt;

    /**
     * @var array
     */
    protected $preorderPropsCache;


    /** @var PaymentStoreOrderService */
    protected $paymentPreorderService;

    /**
     * @var PaymentStoreOrderService
     */
    protected $paymentStoreOrderService;

    /**
     * @param PaymentPreorderService $paymentPreorderService
     * @param PaymentStoreOrderService $paymentStoreOrderService
     */
    public function injectDependencies(PaymentPreorderService $paymentPreorderService, PaymentStoreOrderService $paymentStoreOrderService)
    {
        $this->paymentPreorderService   = $paymentPreorderService;
        $this->paymentStoreOrderService = $paymentStoreOrderService;
    }

    /**
     * @param $attemp
     * @return StoreOrderAttempViewModel
     */
    public static function fill(StoreOrderAttemp $attemp): StoreOrderAttempViewModel
    {
        $viewModel          = Yii::createObject(self::class);
        $viewModel->attempt = $attemp;
        return $viewModel;
    }


    public function getTitleItem(): string
    {
        if ($this->attempt->order) {
            if ($this->attempt->order->hasCuttingPack()) {
                return $this->attempt->order->getCuttingPack()->getTitle();
            }
            if ($this->attempt->order->getFirstReplicaItem()) {
                return $this->attempt->order->getFirstReplicaItem()->getTitle();
            }
        }
        if ($this->attempt->preorder) {
            $preorderProps = $this->getPreorderProps();
            return $preorderProps['title'];
        }

        return '';
    }

    protected function getPreorderProps()
    {
        if ($this->preorderPropsCache) {
            return $this->preorderPropsCache;
        }
        $title  = '';
        $titles = [];
        if ($this->attempt->preorder) {
            if ($this->attempt->preorder->primaryPaymentInvoice) {
                foreach ($this->attempt->preorder->primaryPaymentInvoice->paymentInvoiceItems as $invoiceItem) {
                    $title = $invoiceItem->title;

                    if (!empty($title)) {
                        $titles[] = _t('site.ps', '{title} {c}', [
                            'title' => \H($title),
                            'c'     => '<span class="label label-info">x' . app('formatter')->asDecimal($invoiceItem->qty) . '</span>'
                        ]);
                    }
                }
            } else {
                foreach ($this->attempt->preorder->preorderWorks as $work) {
                    $title = $work->getTextTitle();
                    if (!empty($title)) {
                        $titles[] = _t('site.ps', '{title} {c}', [
                            'title' => \H($title),
                            'c'     => '<span class="label label-info">x' . app('formatter')->asDecimal($work->qty) . '</span>'
                        ]);
                    }
                }
            }


            $titleItemRaw             = implode(',<br>', $titles);
            $this->preorderPropsCache = ['title' => $title, 'titleItemRaw' => $titleItemRaw];
            return $this->preorderPropsCache;
        }
        return null;
    }

    public function getTitleItemRaw(): string
    {
        $info = $this->getPreorderProps();
        return $info['titleItemRaw'] ?? '';
    }

    public function getImgItem(): ?string
    {
        if ($this->attempt->order) {
            if ($this->attempt->order->hasCuttingPack()) {
                return $this->attempt->order->getCuttingPack()->getPreviewUrl();
            }
            if ($this->attempt->order->getFirstReplicaItem()) {
                return $this->attempt->order->getFirstReplicaItem()->getCoverUrl();
            }
        }
        return '';
    }

    public function getUrlItem(): string
    {
        if ($this->attempt->order && $model3dReplica = $this->attempt->order->getFirstReplicaItem()) {
            return Model3dFacade::getUrlModel3dReplica($model3dReplica);
        }

        return '';
    }

    public function getQty(): int
    {
        if ($this->attempt->order) {
            if ($cuttingPack = $this->attempt->order->getCuttingPack()) {
                return $cuttingPack->getTotalQty();
            }
            if ($model3dReplica = $this->attempt->order->getFirstReplicaItem()) {
                return array_sum(ArrayHelper::getColumn($model3dReplica->getCalculatedModel3dParts(), 'qty'));
            }
        }
        if ($this->attempt->preorder) {
            return $this->attempt->preorder->getTotalQty();
        }
        return 0;
    }

    public function getPrice(): string
    {
        $price = null;
        if ($this->attempt->order) {
            $price = $this->attempt->order->primaryPaymentInvoice->getManufacturerAward();
        } elseif ($this->attempt->preorder && $this->attempt->preorder->primaryPaymentInvoice) {
            $price = $this->attempt->preorder->primaryPaymentInvoice->getAmountTotal();
        }
        return $price && $price->getAmount() ? displayAsMoney($price) : '';
    }

    public function getPriceNote(): string
    {
        $invoice = null;
        if ($this->attempt->order) {
            $invoice = $this->attempt->order->primaryPaymentInvoice;
        }
        if ($invoice && $this->attempt->order->getCuttingPack()) {
            $priceForCustomer = $invoice->storeOrderAmount->getAmountCutting();
            $priceForPs       = $invoice->storeOrderAmount->getAmountCuttingForPs();
            $printFee         = $invoice->storeOrderAmount->getAmountPsFee();
            $shippingPrice    = $this->paymentStoreOrderService->getPsShippingFeePaymentDetail($invoice);

            if ($printFee) {
                return $this->getPriceNoteLabel(
                    displayAsMoney($priceForCustomer),
                    displayAsMoney($printFee),
                    displayAsMoney($priceForPs),
                    $shippingPrice ? displayAsMoney($shippingPrice->getMoneyAmount()) : ''
                );
            }
        }
        if ($invoice && $this->attempt->order->isFor3dPrinter()) {
            $priceForCustomer = $invoice->storeOrderAmount->getAmountPrint();

            // price data
            if ($priceForCustomer && $priceForCustomer->getAmount() > 0) {
                $printFee      = $invoice->storeOrderAmount->getAmountPsFee();
                $priceForPs    = $invoice->storeOrderAmount->getAmountPrintForPs();
                $shippingPrice = $this->paymentStoreOrderService->getPsShippingFeePaymentDetail($invoice);

                if ($printFee) {
                    return $this->getPriceNoteLabel(
                        displayAsMoney($priceForCustomer),
                        displayAsMoney($printFee),
                        displayAsMoney($priceForPs),
                        $shippingPrice ? displayAsMoney($shippingPrice->getMoneyAmount()) : ''
                    );
                }
            }
        }
        if ($invoice && $this->attempt->order->preorder) {
            $manufacturerMoney = $invoice ? $invoice->preorderAmount->getAmountManufacturerWithRefund() : null;
            $psFee         = $invoice->preorderAmount->getAmountFeeWithRefund();
            $shippingPrice = $this->paymentStoreOrderService->getPsShippingFeePaymentDetail($invoice); // If shipping by Ps
            $amountTotalWithRefund = $invoice->getAmountTotalWithRefund();

            if ($manufacturerMoney && $psFee) {
                return $this->getPriceNoteLabel(
                    displayAsMoney($amountTotalWithRefund),
                    displayAsMoney($psFee),
                    displayAsMoney($manufacturerMoney),
                    $shippingPrice ? displayAsMoney($shippingPrice->getMoneyAmount()) : ''
                );
            }
        }
        return '';
    }


    public function getTitleService(): string
    {
        return $this->attempt->machine ? $this->attempt->machine->getTitleLabel() : '';
    }

    public function getMaterialTitle(): string
    {
        if ($this->attempt->order) {
            if ($cuttingPack = $this->attempt->order->getCuttingPack()) {
                return $cuttingPack->material->title;
            }
            if ($model3dReplica = $this->attempt->order->getFirstReplicaItem()) {
                if ($model3dReplica->isOneTextureForKit()) {
                    $texture       = $model3dReplica->getKitTexture();
                    $materialTitle = $texture && $texture->printerMaterial ?
                        $texture->printerMaterial->filament_title :
                        $model3dReplica->model3dTexture->printerMaterialGroup->title;
                    return $materialTitle;
                }
            }
        }
        return '';
    }

    public function getMaterialColor(): PrinterColor
    {
        if ($this->attempt->order) {
            if ($cuttingPack = $this->attempt->order->getCuttingPack()) {
                return $cuttingPack->color;
            }
            if ($model3dReplica = $this->attempt->order->getFirstReplicaItem()) {
                if ($model3dReplica->isOneTextureForKit()) {
                    $texture = $model3dReplica->getKitTexture();
                    return $texture->printerColor;
                }
            }
        }
    }

    /**
     * @param User $user
     * @param int $size
     *
     * @return bool|string
     */
    public function getOrderUserAvatar($size = 50)
    {
        if (!$this->attempt->order || !$this->attempt->order->user) {
            return null;
        }

        if ($this->attempt->order->user->company) {
            return $this->attempt->order->user->company->getCompanyLogoOrDefault(false, $size);
        }

        return UserUtils::getAvatarUrl($this->attempt->order->user, $size);
    }


    public function isModel3dRemoved()
    {
        $wasRemoved = $this->attempt->order && $this->attempt->order->isFor3dPrinter() && (!$this->attempt->order->getFirstReplicaItem());
        return $wasRemoved;
    }

    /**
     * @return bool
     */
    public function isPaymentPendingWarningNeed(): bool
    {
        return $this->attempt->order && $this->attempt->machine && PaymentInfoHelper::isPaymentPendingWarningNeed($this->attempt->order, $this->attempt->machine);
    }


    /**
     * @param $costWithFeeMoney
     * @param $costPsFee
     * @param $costMoney
     *
     * @param $shippingPrice
     * @return array|mixed|null|object|string
     */
    public function getPriceNoteLabel($costWithFeeMoney, $costPsFee, $costMoney, $shippingPrice = '')
    {
        $shippingPriceText = '';
        if ($shippingPrice) {
            $shippingPriceText = _t('site.ps', ' Shipping price {price4}. ', [
                    'price4' => $shippingPrice
                ]
            );
        }
        return _t('site.ps', 'Manufacturing fee for the customer is {price1}, including packaging fee. Service fee is {price2}. {shippingPriceText}Therefore, your manufacturing income is {price3}', [
            'price1'            => $costWithFeeMoney,
            'price2'            => $costPsFee,
            'price3'            => $costMoney,
            'shippingPriceText' => $shippingPriceText
        ]);
    }

    /**
     * @return bool
     */
    public function needShowTrackingNumber(): bool
    {
        $deliveryDetail = $this->attempt->order->getOrderDeliveryDetails();
        return $this->attempt && $this->attempt->order && $this->attempt->order->currentAttemp &&
            $this->attempt->id === $this->attempt->order->currentAttemp->id &&
            $deliveryDetail &&
            !empty($deliveryDetail['tracking_number']) &&
            $this->attempt->isTrackingNumberVisible();
    }

    /**
     * @return bool
     */
    public function needShowFooterTableLayout(StoreOrderAttemptProcess $attemptProcess): bool
    {
        return $this->attempt->order && ($this->attempt->isPrinted() ||
                $attemptProcess->canShowReceivedMessage() ||
                $attemptProcess->canShowShippedMessage() ||
                $attemptProcess->canShowCancelledMessage() ||
                $this->needShowTrackingNumber());
    }

    /**
     * @return bool
     */
    public function showEstimatedProduction(): bool
    {
        return (($this->attempt->order && $this->attempt->order->hasModel()) || $this->attempt->machine) && ($this->attempt->isNew()
                || \in_array($this->attempt->status, [StoreOrderAttemp::STATUS_ACCEPTED, StoreOrderAttemp::STATUS_PRINTING], true));
    }

    public function getReceivedDateFormat($format = 'short'): string
    {
        if (!$this->attempt->dates->received_at) {
            return '';
        }

        return _t('site.ps', 'Received on {date}', ['date' => app('formatter')->asDatetime($this->attempt->dates->received_at, $format)]);
    }

    /**
     * @return array|mixed|null|object|string
     */
    public function getCancelledText()
    {
        if (!$this->attempt->isCancelled()) {
            return null;
        }

        $text = '';
        if ($this->attempt->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_PS) {
            $text .= _t('site.ps', 'Canceled by you at ');
        } elseif ($this->attempt->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_CLIENT) {
            $text .= _t('site.ps', 'Canceled by customer at ');
        } elseif ($this->attempt->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_MODERATOR) {
            $text .= _t('site.ps', 'Canceled by moderator at ');
        } elseif ($this->attempt->isExpired()) {
            $text .= _t('site.ps', 'Expired time period.') . '<br>' .
                _t(
                    'site.ps',
                    'This order was offered to you and several other print services on a first come first serve basis. As another print service has accepted the order, it is now expired and no longer available. At '
                );
        } else {
            $text .= _t('site.ps', 'Canceled at ');
        }
        return $text . app()->formatter->asDatetime($this->attempt->cancelled_at);
    }

    /**
     *
     * @return string
     */
    public function getTrackingNumberFormat(): string
    {
        $deliveryDetail = $this->attempt->order->getOrderDeliveryDetails();

        return DeliveryPostalLabel::checkPostalLabelExists($this->attempt) && $deliveryDetail ?
            Html::a(
                '<b>' . $deliveryDetail['tracking_number'] . '</b>',
                '/workbench/service-order/postal-image?attempId=' . $this->attempt->id,
                ['target' => '_blank']
            )
            : '<b>' . $deliveryDetail['tracking_number'] . '</b>';
    }

    public function getTrackingShipper(): string
    {
        $deliveryDetail = $this->attempt->order->getOrderDeliveryDetails();
        return $deliveryDetail['tracking_shipper'] ?? '';
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getLocationLink(): string
    {
        if (!$this->isDeliveryTypePickup() && !$this->attempt->isShippingAddressVisible()) {
            if ($this->attempt->status !== StoreOrderAttemp::STATUS_DELIVERED &&
                $this->attempt->status !== StoreOrderAttemp::STATUS_RECEIVED &&
                !$this->attempt->isCancelled()
            ) {
                return SiteHelpWidget::widget([
                    'triggerHtml' => _t('site.ps', 'Location'),
                    'alias'       => 'ps.order.shippingAddress'
                ]);
            }
        }

        return _t('site.ps', 'Location');
    }

    /**
     * @return string|null
     */
    public function getOrderUserName()
    {
        if ($this->attempt->order && $this->attempt->order->user) {
            if ($this->attempt->order->user->company) {
                return $this->attempt->order->user->company->getTitle();
            }
            return UserFacade::getFormattedUserName($this->attempt->order->user);
        }
        if ($this->attempt->preorder && $this->attempt->preorder) {
            return UserFacade::getFormattedUserName($this->attempt->preorder->user);
        }

        return null;
    }

    public function getCalculateTestOrderPriceFn()
    {
        $psPrinter     = $this->attempt->machine->asPrinter();
        $uniqueCacheId = 'PrintPrices_' . $psPrinter->id;
        app('cache')->set($uniqueCacheId, false);

        $r = PriceCalculator::calculateModel3dPrintPriceWithPackageFee(
            $this->attempt->order->getFirstReplicaItem(),
            $psPrinter,
            false,
            true
        );

        return $r;
    }

}
