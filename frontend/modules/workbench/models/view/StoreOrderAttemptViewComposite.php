<?php

namespace frontend\modules\workbench\models\view;

use common\models\StoreOrderAttemp;
use frontend\models\ps\StoreOrderAttemptProcess;
use yii\base\BaseObject;

class StoreOrderAttemptViewComposite extends BaseObject
{
    /** @var StoreOrderAttemp */
    public $attempt;

    /** @var StoreOrderAttempViewModel */
    public $attemptView;

    /** @var StoreOrderAttemptProcess */
    public $attemptProcess;

    public static function create(StoreOrderAttemp $attempt, StoreOrderAttempViewModel $attemptView, StoreOrderAttemptProcess $attemptProcess)
    {
        $composite                 = new self();
        $composite->attempt        = $attempt;
        $composite->attemptView    = $attemptView;
        $composite->attemptProcess = $attemptProcess;
        return $composite;
    }
}