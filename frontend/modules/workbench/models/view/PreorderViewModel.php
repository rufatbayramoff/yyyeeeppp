<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 28.08.18
 * Time: 13:58
 */

namespace frontend\modules\workbench\models\view;

use common\models\File;
use common\models\PaymentDetail;
use common\models\Preorder;
use common\models\PreorderWork;
use common\modules\payment\services\RefundService;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\widgets\PerPageWidget;
use yii\base\BaseObject;

/**
 * Class PreorderViewModel
 *
 * @package frontend\modules\workbench\models\view
 *
 * @property Preorder $preorder
 * @property PreorderWork[] $preorderWorks
 * @property PreorderWorkViewModel[] $preorderWorksView
 * @property PreorderWorkViewModel[] $preorderWorksInvoiceView
 * @property array $preorderImagesUrl
 * @property string $status
 * @property PaymentDetail[] $refundDetails
 */
class PreorderViewModel extends BaseObject
{
    public $preorder;

    public $preorderWorks;

    public $_preorderImagesUrl;

    public $status;

    /** @var PaymentDetail[] */
    public $refundDetails;

    protected $_preorderWorksView;

    protected $_preorderWorksInvoiceView;

    /**
     * @param Preorder $preorder
     * @return PreorderViewModel
     * @throws \yii\base\InvalidConfigException
     */
    public static function fill(Preorder $preorder)
    {
        $refundService = \Yii::createObject(RefundService::class);
        $view          = new static();

        $view->preorder      = $preorder;
        $view->preorderWorks = $view->preorder->preorderWorks;
        $view->status        = $preorder->getStatusLabeledForPs();
        if ($preorder->primaryPaymentInvoice && $preorder->primaryPaymentInvoice->storeOrder) {
            $view->refundDetails = $refundService->getApprovedRefundsByOrder($preorder->primaryPaymentInvoice->storeOrder);
        } else {
            $view->refundDetails = [];
        }

        return $view;
    }

    /**
     * @return string
     */
    public function getUrlViewPs(): string
    {
        return PreorderUrlHelper::viewPs($this->preorder);
    }

    public function getCompanyTitle():string
    {
        return $this->preorder->company->getTitleLabel();
    }

    public function getCompanyPublicPage():string
    {
        return $this->preorder->company->getPublicCompanyLink();
    }

    public function getCompanyLogo(): string
    {
        return $this->preorder->company->getCompanyLogo();
    }

    /**
     * @return string
     */
    public function getPreorderTitle(): string
    {
        return _t('site.preorder', 'Quote #{id}', ['id' => $this->preorder->id]);
    }

    public function getCreatedAt(): string
    {
        return $this->preorder->created_at;
    }

    public function isCnc()
    {
        return $this->preorder->isCncType();
    }

    /**
     * @return string
     */
    public function getEstimateTimeFormat(): string
    {
        return _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}', ['n' => (int)$this->preorder->estimate_time]);
    }

    /**
     * @return string
     */
    public function getOfferedEstimateTimeFormat(): string
    {
        return _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}', ['n' => (int)$this->preorder->offer_estimate_time]);
    }

    public function isInterception(): bool
    {
        return $this->preorder->is_interception;
    }

    /**
     * @return bool
     */
    public function hasPreorderWorks(): bool
    {
        return !empty($this->preorderWorks);
    }

    /**
     * @return int
     */
    public function getPreorderWorksQty(): int
    {
        if (!$this->hasPreorderWorks()) {
            return 0;
        }

        $qty = 0;

        foreach ($this->preorderWorks as $work) {
            $qty += (int)$work->qty;
        }

        return $qty;
    }

    public function getPreorderImageUrl(): string
    {
        $images = $this->getPreorderImagesUrl();
        $image  = reset($images);
        return $image;
    }

    /**
     * @return array
     */
    public function getPreorderImagesUrl(): array
    {
        if ($this->_preorderImagesUrl === null) {
            $this->_preorderImagesUrl = [];

            foreach ($this->preorderWorks as $work) {
                if ($work->getType() === PreorderWork::TYPE_PRODUCT) {
                    $url = $work->product->getCoverUrl() ?? false;

                    if ($url) {
                        $this->_preorderImagesUrl[] = $url;
                    }

                }
            }

            if ($this->preorder->service) {
                $url = $this->preorder->service->getImageCoverUrl();

                if ($url) {
                    $this->_preorderImagesUrl[] = $url;
                }
            }
        }

        return $this->_preorderImagesUrl;
    }

    /**
     * @return PreorderWorkViewModel[]
     */
    public function getPreorderWorksView(): ?array
    {
        if ($this->_preorderWorksView === null) {
            $this->_preorderWorksView = [];
            foreach ($this->preorderWorks as $work) {
                $this->_preorderWorksView[] = PreorderWorkViewModel::fill($work);
            }
        }

        return $this->_preorderWorksView;
    }

    /**
     * @return PreorderWorkViewModel[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getPreorderWorksInvoiceView(): ?array
    {
        if ($this->_preorderWorksInvoiceView === null) {
            $this->_preorderWorksInvoiceView = [];

            $invoiceItems = $this->preorder->primaryPaymentInvoice->paymentInvoiceItems ?? [];

            foreach ($invoiceItems as $invoiceItem) {
                $this->_preorderWorksInvoiceView[] = PreorderWorkViewModel::fillInvoiceItem($invoiceItem);
            }
        }

        return $this->_preorderWorksInvoiceView;
    }

    /**
     * @return bool
     */
    public function isRejected(): bool
    {
        return in_array($this->preorder->status, [Preorder::STATUS_REJECTED_BY_CLIENT, Preorder::STATUS_REJECT_CONFIRM_BY_COMPANY, Preorder::STATUS_REJECTED_BY_COMPANY]);
    }

    public function waitConfirmByClient(): bool
    {
        return $this->preorder->status === Preorder::STATUS_WAIT_CONFIRM;
    }

    public function allowDecline(): bool
    {
        return !$this->isRejected() && !$this->waitConfirmByClient();
    }

    public function isDraft(): bool
    {
        return $this->preorder->isDraft();
    }

    /**
     * @return string - \H(string)
     */
    public function getDeclineReason(): string
    {
        $reason = '';

        if ($this->preorder->decline_reason) {
            $reason .= $this->preorder->decline_reason;
        }

        if ($this->preorder->decline_comment) {
            $reason .= ($reason ? ': ' : '') . $this->preorder->decline_comment;
        }

        return \H($reason);
    }
}