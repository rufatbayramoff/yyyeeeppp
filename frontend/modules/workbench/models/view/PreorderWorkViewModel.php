<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 28.08.18
 * Time: 13:58
 */

namespace frontend\modules\workbench\models\view;

use common\models\File;
use common\models\PaymentInvoiceItem;
use common\models\PreorderWork;
use common\modules\payment\services\PaymentPreorderService;
use lib\money\Money;
use Yii;
use yii\base\BaseObject;

/**
 * Class PreorderWorkViewModel
 * @package frontend\modules\workbench\models\view
 *
 * @property PreorderWork $work
 * @property PaymentInvoiceItem|null $invoiceItem
 * @property string $title
 * @property string $url
 * @property int $qty
 * @property Money $manufacturer
 * @property Money $total
 */
class PreorderWorkViewModel extends BaseObject
{
    public $work;

    public $title;

    public $url;

    public $qty;

    public $invoiceItem;

    public $manufacturer;

    public $total;

    protected $_workFiles;

    public static function fill(PreorderWork $work)
    {
        $view = new static();

        $view->work = $work;
        $view->title = $view->getTitleByType();
        $view->url = $view->getUrlByType();
        $view->qty = (int) $view->work->qty;

        return $view;
    }

    /**
     * @param PaymentInvoiceItem $invoiceItem
     *
     * @return PreorderWorkViewModel
     * @throws \yii\base\InvalidConfigException
     */
    public static function fillInvoiceItem(PaymentInvoiceItem $invoiceItem)
    {
        $view = new static();

        /** @var PaymentPreorderService $paymentPreorderService */
        $paymentPreorderService = Yii::createObject(PaymentPreorderService::class);

        $view->invoiceItem = $invoiceItem;
        $view->work = $paymentPreorderService->getPreorderWorkByInvoiceItem($invoiceItem);
        $view->title = $invoiceItem->title;
        $view->url = $view->getUrlByType();
        $view->qty = (int) $invoiceItem->qty;
        $view->manufacturer = $view->invoiceItem->paymentInvoice->preorderAmount->getAmountManufacturerByItemUuid($invoiceItem->uuid);
        $view->total = $view->invoiceItem->paymentInvoice->preorderAmount->getAmountTotalByItemUuid($invoiceItem->uuid);

        return $view;
    }

    /**
     * @return string
     */
    protected function getTitleByType(): string
    {
        if (!$this->work) {
            return $this->invoiceItem->title;
        }

        if ($this->work->getType() === PreorderWork::TYPE_PRODUCT) {
            return $this->work->product->getTitle();
        }

        if ($this->work->getType() === PreorderWork::TYPE_SERVICE) {
            return $this->work->companyService->getTitleLabel();
        }

        return (string)$this->work->title;
    }

    /**
     * @return null|string
     */
    protected function getUrlByType(): ?string
    {
        if ($this->work && $this->work->getType() === PreorderWork::TYPE_PRODUCT) {
            return $this->work->product->getPublicPageUrl();
        }

        return null;
    }

    public function getItemViewHtml()
    {
        if ($this->url) {
            return '<a href="'.$this->url.'" target="_blank"> '.$this->title.'</a> <span class="label label-info">x'.$this->qty.'</span>';
        }

        return $this->title. ' <span class="label label-info">x'.$this->qty.'</span>';
    }

    /**
     * @return File[]
     */
    public function getWorkFiles()
    {
        if ($this->_workFiles === null) {
            if ($this->work && $this->work->files) {
                $this->_workFiles = $this->work->files;
            } else {
                $this->_workFiles = [];
            }
        }

        return $this->_workFiles;
    }
}