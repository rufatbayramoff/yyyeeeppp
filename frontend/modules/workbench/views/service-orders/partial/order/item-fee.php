<?php

use common\models\PaymentTransactionRefund;
use common\models\StoreOrderPosition;
use common\modules\payment\services\PaymentStoreOrderService;

/** @var $attemptViewComposite \frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite */

$attempt        = $attemptViewComposite->attempt;
$attemptView    = $attemptViewComposite->attemptView;
$attemptProcess = $attemptViewComposite->attemptProcess;
$invoice        = $attempt->order->primaryPaymentInvoice;

$priceForPs               = $invoice->getAmountManufacturerAwardWithRefund();
$bonusAccured             = $invoice->getAmountBonusAccrued();
$paymentStoreOrderService = Yii::createObject(PaymentStoreOrderService::class);
if ($priceForPs && $priceForPs->getAmount() > 0) {
    ?>
    <tr>
        <td class="one-print-request__table__label">
            <?= _t('site.ps', 'Manufacturing Fee'); ?>
        </td>
        <td>
            <?php $psFee = $invoice->getAmountPsFee(); ?>
            <?php if ($psFee): ?>
                <nobr>
                    <?php echo displayAsMoney($priceForPs); ?>
                    <span data-toggle="tooltip" data-placement="top"
                          data-original-title="<?php echo $attemptView->getPriceNote(); ?>"
                          class="tsi tsi-warning-c"></span>
                </nobr>
            <?php else: ?>
                <?php echo displayAsMoney($priceForPs); ?>
            <?php endif; ?>

            <?php if ($attemptProcess->canRefundOrderTransaction()): ?>
                <br/>
                <button class="btn btn-link btn-xs p-l0 p-r0" style="font-weight: normal;"
                        ng-click="partialRefund(<?= $attempt->id ?>, <?= $priceForPs->getAmount() ?>)"><?= _t('site.order', 'Partial Refund') ?></button>
            <?php else: ?>
                <?php $refundsInfo = $attempt->getRefundRequestByOrder(); ?>
                <?php if ($refundsInfo) {
                    /** @var PaymentTransactionRefund[] $refundsRequested */
                    [$refundsRequested, $lastErrorMessage] = $refundsInfo;
                    if ($refundsRequested) {
                        foreach ($refundsRequested as $refundRequested): ?>
                            <br/>
                            <span class="label label-warning">
                                                  <?php if ($refundRequested->isTotalApproved()): ?>
                                                      <?php echo _t(
                                                          'site.order',
                                                          'Refund Approved: Total'
                                                      ); ?>
                                                  <?php elseif ($refundRequested->isApproved()): ?>
                                                      <?php echo _t(
                                                          'site.order',
                                                          'Refund Approved: {amount}',
                                                          ['amount' => displayAsMoney($refundRequested->getMoneyAmount())]
                                                      ); ?>
                                                  <?php else: ?>
                                                      <?php echo _t(
                                                          'site.order',
                                                          'Refund submitted: {amount}',
                                                          ['amount' => displayAsMoney($refundRequested->getMoneyAmount())]
                                                      ); ?>
                                                  <?php endif; ?>
                           </span><br>
                            <span style="font-size:12px">fff<?=H($refundRequested->comment)?></span>
                        <?php endforeach; ?>
                    <?php } else { ?>
                        <br/>
                        <nobr>
                            <?php echo _t('site.order', 'Partial refund unavailable'); ?> <span
                                    data-toggle="tooltip" data-placement="top"
                                    data-original-title="<?php echo $lastErrorMessage?:'' ?>"
                                    class="tsi tsi-warning-c"></span>
                        </nobr>
                    <?php }
                } ?>
                <?php echo frontend\widgets\SiteHelpWidget::widget(['alias' => 'ps.order.partialRefund']); ?>
            <?php endif; ?>
            <br/>
        </td>
    </tr>
    <?php
}
if ($bonusAccured && $bonusAccured->getAmount() > 0) {
    ?>
    <tr>
        <td class="one-print-request__table__label">
            <?= _t('site.ps', 'Bonus accrued'); ?>
        </td>
        <td>
            + <?php echo $bonusAccured->getAmount(); ?>
        </td>
    </tr>
<?php } ?>

<?php
$addonPrice = $invoice->storeOrderAmount->getAmountAddon();
if ($addonPrice) {
    foreach ($attempt->order->storeOrderPositions as $addonService) {
        if ($addonService->status != StoreOrderPosition::STATUS_PAID) {
            continue;
        }
        ?>
        <tr>
            <td class="one-print-request__table__label">
                <?= _t('site.order', 'Additional service'); ?>
                <span data-toggle="tooltip" data-placement="bottom"
                      data-original-title="<?= H($addonService->title); ?>"
                      title="<?= H($addonService->title); ?>" class="tsi tsi-warning-c">

                                        </span>
            </td>
            <td><?php echo displayAsMoney($addonService->getFullPrice()); ?></td>
        </tr>
    <?php }
} ?>
