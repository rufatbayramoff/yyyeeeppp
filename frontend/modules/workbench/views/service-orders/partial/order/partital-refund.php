<?php
?>
<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog partital-refund-modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?= _t('site.order', 'Close') ?>">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><?= _t('site.order', 'Offer Partial Refund') ?></h4>
            </div>
            <div>
                <input type="hidden" name="orderId" value="{{order.id}}">
                <div class="modal-body no-padding">
                    <div class="form-group">
                        <label class="control-label" for="paymenttransactionrefund-amount">
                            <?= _t('site.order', 'Amount to be refunded') ?>
                        </label>
                        <input type="number"
                               step="0.01"
                               min="0.01"
                               id="paymenttransactionrefund-amount"
                               class="form-control"
                               name="PaymentTransactionRefund[amount]"
                               ng-model="refundAmount"
                               aria-required="true"
                        >
                        <div class="help-block"></div>
                        <div class="bar bg-warning m-t10" ng-if="isFullRefund()"><?=_t('site.order', 'Order will be canceled with full refund')?></div>
                    </div>
                    <div class="form-group field-paymenttransactionrefund-comment required">
                        <label class="control-label"
                               for="paymenttransactionrefund-comment"><?= _t('site.order', 'Comment') ?></label>
                        <textarea id="paymenttransactionrefund-comment" class="form-control"
                                  name="PaymentTransactionRefund[comment]" maxlength="245"
                                  placeholder="<?= _t('site.order', 'Please explain the reason for the partial refund.') ?>"
                                  aria-required="true"
                                  ng-model="refundComment"
                        ></textarea>
                        <div class="help-block"></div>
                    </div>
                    <div class="bar bg-info">
                        <?=_t('site.order', 'In case of a successful partial refund, the customer won\'t be credited with bonuses for the order.')?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal"><?= _t('site.order', 'Close') ?></button>
                    <button class="btn btn-primary" ng-click="submitRefund()"><?= _t('site.order', 'Submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
