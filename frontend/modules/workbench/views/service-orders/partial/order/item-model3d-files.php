<?php

use common\components\Model3dWeightCalculator;
use common\services\Model3dPartService;
use frontend\components\UserSessionFacade;
use lib\MeasurementUtil;
use yii\helpers\Html;

/** @var $attemptViewComposite \frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite */

$attempt        = $attemptViewComposite->attempt;
$attemptView    = $attemptViewComposite->attemptView;
$attemptProcess = $attemptViewComposite->attemptProcess;

$userWightMeasure = UserSessionFacade::isUsMeasurement() ? MeasurementUtil::OUNCE : MeasurementUtil::GRAM;
?>

<div style="margin-left: 15px;">
    <button class="btn btn-default  m-b20 m-t10"
            data-target="#js-files-list-<?php echo $attempt->order->id; ?>" data-toggle="collapse">
        <?= _t('site.ps', 'Files'); ?>
    </button>
</div>

<div class="collapse <?= $attempt->isNew() ? 'in' : ''; ?>"
     id="js-files-list-<?= $attempt->order->id; ?>">
    <div class="col-md-12">
        <?php foreach ($attempt->order->getFirstReplicaItem()->getActiveModel3dParts() as $model3dPart) : ?>
            <?php
            $texture           = $model3dPart->getCalculatedTexture();
            $model3dPartImgUrl = Model3dPartService::getModel3dPartRenderUrl($model3dPart);

            if ($model3dPart->qty < 1) {
                continue;
            }
            ?>

            <div class="row m-b10">
                <div class="col-sm-3 one-print-request__model-pic">
                    <?php echo Html::img(
                        $model3dPartImgUrl, [
                        'width' => '100',
                        'class' => 'order-model-image'
                    ]); ?>
                </div>
                <div class="col-sm-9">
                    <h4 class="one-print-request__model-title">
                        <?php if ($attemptProcess->canDownloadModel3D()) { ?>
                            <button class="btn btn-link one-print-request__model-title-link"
                                    loader-click='downloadFiles("<?= $attempt->id ?>", "<?= $model3dPart->file->id ?>")'
                                    loader-click-text="<i class='tsi tsi-refresh tsi-spin-custom'></i>  <?= _t('site.order',
                                        'Preparing archive for download...') ?>"
                                    href="#">
                                <?= H($model3dPart->file->getFileName()); ?>
                            </button>
                        <?php } else { ?>
                            <?= H($model3dPart->file->getFileName()); ?>
                        <?php } ?>
                    </h4>

                    <div class="row">
                        <div class="col-sm-6">
                            <table class="one-print-request__table">
                                <?php if ($texture && $texture->printerMaterial): ?>
                                    <tr>
                                        <td class="one-print-request__table__label">
                                            <?= _t('site.ps', 'Material'); ?>
                                        </td>
                                        <td><?= H($texture->printerMaterial->title) ?></td>
                                    </tr>
                                <?php endif; ?>
                                <?php if ($texture && $texture->printerMaterial): ?>
                                    <tr>
                                        <td class="one-print-request__table__label">
                                            <?= _t('site.ps', 'Color'); ?>
                                        </td>
                                        <td>
                                            <div class="material-item__color"
                                                 style="background-color: rgb(<?php echo H($texture->printerColor->rgb); ?>)"></div>&nbsp;
                                            <?= H($texture->printerColor->title) ?>
                                        </td>
                                    </tr> <?php endif; ?>
                                <tr>
                                    <td class="one-print-request__table__label">
                                        <?= _t('site.ps', 'Infill'); ?>
                                    </td>
                                    <td>
                                        <?= $texture->infillText() ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="one-print-request__table__label">
                                        <?= _t('site.ps', 'Size'); ?>
                                    </td>
                                    <td><?= $model3dPart->getSize() ? $model3dPart->getSize()->toStringWithMeasurement() : _t('site.ps', 'NOT CALCULATED') ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-5">
                            <table class="one-print-request__table">
                                <?php
                                $netWeight        = $model3dPart->getWeight();
                                $supportWeightOne = Model3dWeightCalculator::calculateModel3dPartSupportWeight($model3dPart) / $model3dPart->qty;
                                ?>
                                <tr>
                                    <td class="one-print-request__table__label">
                                        <?= _t('site.ps', 'Model weight'); ?>
                                    </td>
                                    <td>
                                        <?php echo MeasurementUtil::convertWeight(
                                                $model3dPart->getWeight() / $model3dPart->qty - $supportWeightOne,
                                                MeasurementUtil::GRAM,
                                                $userWightMeasure,
                                                true
                                            ) . " {$userWightMeasure}";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="one-print-request__table__label">
                                        <?= _t('site.ps', 'Supports weight'); ?>
                                    </td>
                                    <td>
                                        <?php echo MeasurementUtil::convertWeight(
                                                $supportWeightOne,
                                                MeasurementUtil::GRAM,
                                                $userWightMeasure,
                                                true
                                            ) . " {$userWightMeasure}";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="one-print-request__table__label">
                                        <?= _t('site.ps', 'Quantity'); ?>
                                    </td>
                                    <td>
                                        <?= $model3dPart->qty; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="one-print-request__table__label">
                                        <?= _t('site.ps', 'Net Weight'); ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo MeasurementUtil::convertWeight(
                                                $netWeight,
                                                MeasurementUtil::GRAM,
                                                $userWightMeasure,
                                                true
                                            ) . " {$userWightMeasure}";
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

        <?php if ($attemptProcess->canDownloadModel3D()) { ?>
            <div class="row m-b10">
                <div class="col-sm-3 one-print-request__model-pic"></div>
                <div class="col-sm-9">
                    <button class="btn btn-sm btn-default m-b10"
                            loader-click="downloadFiles(<?php echo $attempt->id ?>)"
                            loader-click-text="<i class='tsi tsi-refresh tsi-spin-custom'></i> <?= _t('site.order', 'Preparing archive for download...') ?>"
                            href="#"><?= _t('site.ps', 'Download file(s)'); ?>
                    </button>

                    <?php if ($attempt->isNew()): ?>
                        <p class="alert alert-warning">
                            <?php echo _t('site.ps', 'Please do not produce downloaded files without accepting the order.'); ?>
                        </p>
                    <?php endif; ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>