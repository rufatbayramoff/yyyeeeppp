<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 31.08.18
 * Time: 15:08
 *
 */

use common\modules\payment\services\PaymentStoreOrderService;
use frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite;
use yii\helpers\Html;

/** @var $this \yii\web\View */
/** @var StoreOrderAttemptViewComposite $attemptViewComposite */

$attempt        = $attemptViewComposite->attempt;
$attemptView    = $attemptViewComposite->attemptView;
$attemptProcess = $attemptViewComposite->attemptProcess;

$cuttingPack              = $attempt->order->getCuttingPack();
$paymentStoreOrderService = Yii::createObject(PaymentStoreOrderService::class);
?>
<script type="text/ng-template" id="/attempt/partitalRefund.html">
    <?= $this->render('partital-refund.php') ?>
</script>

<div class="panel-body one-print-request__body">
    <div class="row">
        <div class="col-sm-3 one-print-request__model-pic text-center">
            <?php echo Html::img($cuttingPack->getPreviewUrl(), [
                'width'    => '100',
                'alt'      => \H($cuttingPack->getTitle()),
                'class'    => 'order-model-image order-model-data__cutting-pic',
                'ng-click' => 'popupImageView($event)'
            ]); ?>
        </div>
        <div class="col-sm-9">
            <h3 class="one-print-request__model-title">
                <?= H($cuttingPack->getTitle()) ?>
            </h3>

            <h4 class="text-muted">
                <?= _t('site.ps', 'Total cutting length'); ?>:
                <?= $cuttingPack->getTotalCuttingLength() ?> mm
                <br/>
                <?php echo _t('site.ps', 'Total engrave length'); ?>:
                <?= $cuttingPack->getTotalEngraveLength() ?> mm
            </h4>

            <div class="row">
                <div class="col-md-5">
                    <table class="one-print-request__table">
                        <tr>
                            <td class="one-print-request__table__label" width="37%">
                                <?= _t('site.model3d', 'Invoice code'); ?>
                            </td>
                            <td width="63%">
                                <?= $attempt->order->primaryPaymentInvoice->uuid; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="one-print-request__table__label">
                                <?= _t('site.ps', 'Machine'); ?>
                            </td>
                            <td><?= H($attempt->machine->getTitleLabel()); ?></td>
                        </tr>
                        <?php if ($cuttingPack->material): ?>
                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Material'); ?>
                                </td>
                                <td><?= H($cuttingPack->material->title); ?></td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($cuttingPack->color): ?>
                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Color'); ?>
                                </td>
                                <td><?= H($cuttingPack->color->title); ?></td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($cuttingPack->thickness): ?>
                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Thickness'); ?>
                                </td>
                                <td><?= H($cuttingPack->thickness); ?></td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($cuttingPack->getTotalCuttingLength()): ?>
                            <tr>
                                <td class="one-print-request__table__label" style="min-width: 140px">
                                    <?= _t('site.ps', 'Total cutting length'); ?>
                                </td>
                                <td><?= H($cuttingPack->getTotalCuttingLength()); ?> mm</td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($cuttingPack->getTotalEngraveLength()): ?>
                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Total engrave length'); ?>
                                </td>
                                <td><?= H($cuttingPack->getTotalEngraveLength()); ?> mm</td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>

                <div class="col-md-7">
                    <table class="one-print-request__table">
                        <?php if ($cuttingPack->getTotalQty()): ?>
                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Quantity'); ?>
                                </td>
                                <td>
                                    <?= _t(
                                        'site.ps',
                                        '{num, plural, =1{# item} other{# items}}',
                                        ['num' => $cuttingPack->getTotalQty()]
                                    ); ?>
                                </td>
                            </tr>
                        <?php endif; ?>

                        <?= $this->render('item-fee', ['attemptViewComposite' => $attemptViewComposite]) ?>
                        <?= $this->render('item-delivery', ['attemptViewComposite' => $attemptViewComposite]) ?>

                    </table>
                </div>
            </div>

            <?php if ($attempt->order->comment): ?>
                <h4 class="m-t0 m-b0"><?= _t('site.order', 'Customer comment'); ?></h4>
                <?php echo nl2br(H($attempt->order->comment)); ?>
            <?php endif; ?>
        </div>

        <?= $this->render('item-cutting-files', ['cuttingPack' => $cuttingPack, 'attemptViewComposite' => $attemptViewComposite]) ?>
    </div>
</div>
