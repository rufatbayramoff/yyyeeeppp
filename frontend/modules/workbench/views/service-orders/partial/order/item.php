<?php

use common\components\ArrayHelper;
use common\models\message\helpers\UrlHelper;
use common\models\StoreOrderAttemp;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\models\user\UserFacade;
use frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite;
use frontend\modules\workbench\models\view\StoreOrderAttempViewModel;
use frontend\widgets\Countdown;
use frontend\widgets\ReviewStarsStaticWidget;
use frontend\widgets\SiteHelpWidget;
use frontend\widgets\SwipeGalleryWidget;
use lib\delivery\parcel\ParcelFactory;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var StoreOrderAttemp $attempt
 */

Yii::$app->angular
    ->service(['modal', 'notify', 'router', 'user', 'psDownloadModel'])
    ->controller('ps/orders/attemptView')
    ->controller('ps/orders/offerPartialRefund')
    ->controllerParams([
        'company' => $form->getPs()
    ]);

$attemptView          = StoreOrderAttempViewModel::fill($attempt);
$attemptProcess       = StoreOrderAttemptProcess::create($attempt);
$attemptViewComposite = StoreOrderAttemptViewComposite::create($attempt, $attemptView, $attemptProcess);
?>
<div class="row wide-padding" ng-controller="AttemptViewController">
    <div class="col-xs-12">
        <div class="panel panel-default box-shadow border-0">
            <div class="one-print-request" data-order-id="<?php echo $attempt->id; ?>">
                <div class="panel-body">
                    <?php if ($attempt->order->is_dispute_open) : ?>
                        <div class="alert alert-warning">
                            <?php echo _t('site.order', 'Order is in dispute. Please contact the {link}', [
                                'link' => Html::a(_t('site.order', 'customer'), UrlHelper::objectMessageRoute($attempt->order))
                            ]); ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($deadlineTimer = $attempt->getDeadlineCompany()) : ?>
                        <div class="service-order-row__deadline">
                            <?php
                            if ($deadlineTimer->getLabelString()) {
                                echo '<strong>' . $deadlineTimer->getLabelString() . '</strong>';
                            } else {
                                echo '<strong>' . $deadlineTimer->getTitle() . ':</strong>';
                                echo \frontend\widgets\Countdown::widget([
                                    'id'              => 'countdownattemp-' . $attempt->id,
                                    'options'         => ['class' => 'label label-info'],
                                    'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                                    'timeout'         => $deadlineTimer->getTimeout(),
                                    'finishedMessage' => _t('site.ps', '00:00:00')
                                ]);
                            } ?>
                        </div>
                    <?php endif; ?>

                    <div class="one-print-request__user">
                        <?php if ($avatar = $attemptView->getOrderUserAvatar()) : ?>
                            <div class="one-print-request__user__avatar">
                                <?= Html::img($avatar, ['class' => 'print-request-user-img']) ?>
                            </div>
                        <?php endif; ?>
                        <div class="dropdown one-print-request__user__message">
                            <button class="btn btn-default btn-circle dropdown-toggle dropdown-menu-right-toggle"
                                    type="button" id="dropdownMenu1" data-toggle="dropdown">
                                <span class="tsi tsi-message"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <?php if ($attemptProcess->canRequestMoreTime()): ?>
                                    <li>
                                        <a
                                                data-date="<?php echo $attempt->dates->plan_printed_at ?>"
                                                ng-click="requestMoreTimeModal(<?php echo $attempt->id; ?>, $event)"
                                                href="#"
                                                class="request-more-time-link"><?= _t('site.ps', 'Request more time'); ?>
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php if (!$attempt->order->user->equals($attempt->ps->user) && !$attempt->isCancelled() && !$attempt->order->isAnonim()): ?>
                                    <li>
                                        <?= Html::a(_t('site.ps', 'Contact customer'), UrlHelper::objectMessageRoute($attempt->order), ['target' => '_blank']); ?>
                                    </li>
                                <?php endif ?>

                                <li>
                                    <?= Html::a(_t('site.ps', 'Contact support'), UrlHelper::supportMessageRoute($attempt->order), ['target' => '_blank']); ?>
                                </li>
                            </ul>
                        </div>
                        <div class="one-print-request__user__info">
                            <?php if ($attempt->order && $attempt->order->isAnonim()) : ?>
                                <?= H(UserFacade::getFormattedUserName($attempt->order->getPossibleUser())); ?>
                            <?php else : ?>
                                <?php if ($attempt->isCancelled()): ?>
                                    <span> <?php echo H($attemptView->getOrderUserName()); ?></span>
                                <?php else: ?>
                                    <a href="<?= Url::toRoute(UrlHelper::objectMessageRoute($attempt->order)) ?>">
                                        <?php echo H($attemptView->getOrderUserName()); ?>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                            <br>
                            <b><?php echo H($attempt->getTitleLabel()); ?></b>
                        </div>
                    </div>

                    <div class="one-print-request__btns">
                        <?php if ($attemptProcess->canAcceptOrder()): ?>
                            <button
                                    loader-click="setOrderStatus(<?php echo $attempt->id ?>, '<?php echo StoreOrderAttemp::STATUS_ACCEPTED; ?>')"
                                    class="btn btn-success">
                                <?php echo _t("site.ps", "Accept order"); ?>
                            </button>

                            <?php if ($attemptView->isPaymentPendingWarningNeed()): ?>
                                <p class="bg-warning">
                                    <?php echo _t('site.ps',
                                        "Payment has been made and is pending authorization. You may accept the order but please don't start printing until payment has been authorized. Need help? {contactLink}",
                                        [
                                            'contactLink' => Html::a(_t('site.ps', 'Contact support'), UrlHelper::supportMessageRoute($attempt->order),
                                                ['target' => '_blank'])
                                        ]
                                    ); ?>
                                </p>
                            <?php endif; ?>
                        <?php endif ?>

                        <?php if ($attemptProcess->canDeclineOrder()) : ?>
                            <button
                                    ng-click="declineOrderByType(<?php echo $attempt->id ?>, '<?php echo $attempt->order->getOrderType(); ?>')"
                                    class="btn btn-warning btn-sm">
                                <?= _t('front.user', 'Decline'); ?>
                            </button>
                        <?php endif; ?>

                        <?php if ($attemptProcess->canPrintOrder()): ?>
                            <?php echo Html::a(_t('site.ps', 'Order Details'),
                                ['/workbench/service-order/print', 'attempId' => $attempt->id],
                                ['class' => 'btn btn-default']
                            ); ?>
                        <?php endif ?>
                        <?php if ($attemptProcess->canSetAsPrinted()) : ?>
                            <?php if ($attemptProcess->canSubmitResults()): ?>
                                <?php echo Html::a(_t('site.ps', 'Submit results'),
                                    ['/workbench/service-order/print', 'attempId' => $attempt->id],
                                    ['class' => 'btn btn-success']
                                ) ?>
                            <?php else: ?>
                                <button
                                        loader-click="setOrderStatus(<?= $attempt->id ?>, '<?= StoreOrderAttemp::STATUS_PRINTED; ?>')"
                                        class="btn btn-success">
                                    <?= _t('site.ps', 'Submit results'); ?>
                                </button>
                            <?php endif; ?>
                        <?php endif ?>


                        <?php if ($attemptProcess->canGetPostalLabel()) {
                            $parcelInLocalMeasure = ParcelFactory::createFromOrder($attempt->order);
                            if ($attempt->ps->user->userProfile->current_metrics === 'in') {
                                $parcelInLocalMeasure = ParcelFactory::convertParcelInInch($parcelInLocalMeasure);
                            }
                            ?>
                            <button
                                    loader-click="getPostalLabel(<?= $attempt->id ?>, <?= sprintf('%.2F, %.2F, %.2F, %.2F',
                                        $parcelInLocalMeasure->length, $parcelInLocalMeasure->width, $parcelInLocalMeasure->height, $parcelInLocalMeasure->weight); ?>)"
                                    class="btn btn-success">
                                <?= _t('site.ps', 'Generate postal label'); ?>
                            </button>
                        <?php } ?>


                        <?php if ($attemptProcess->canSetAsSentWithTrackingNumber()) : ?>
                            <button
                                    loader-click="setOrderSentWithTrackingNumber(<?= $attempt->id ?>, <?php echo($attempt->order->isForPreorder() ? 'true' : 'false'); ?>)"
                                    class="btn btn-success">
                                <?= _t('site.ps', 'Dispatch'); ?>
                            </button>
                        <?php elseif ($attemptProcess->canSetAsSent()) : ?>
                            <button
                                    loader-click="setOrderStatus(<?= $attempt->id ?>, '<?= StoreOrderAttemp::STATUS_SENT; ?>')"
                                    class="btn btn-success">
                                <?php echo $attempt->isPickupDelivery() ? _t('site.ps', 'Set as picked up') : _t('site.ps', 'Dispatch') ?></button>
                        <?php elseif ($attempt->isPendingModeration()) : ?>
                            <?php echo Html::tag(
                                'p',
                                _t('site.ps', 'Pending moderation ') . SiteHelpWidget::widget(['alias' => 'ps.printreview']),
                                ['class' => 'text-info m-t10']
                            ); ?>
                        <?php endif; ?>

                        <?php if ($attemptProcess->canShowReceivedMessage()) : ?>
                            <div class="one-print-request__btns-text">
                                <?php echo $attemptView->getReceivedDateFormat(); ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($attemptProcess->canShowShippedMessage()) : ?>
                            <div class="one-print-request__btns-text">
                                <?php echo _t('site.ps', 'Dispatched on {date}', ['date' => app('formatter')->asDatetime($attempt->dates->shipped_at, 'short')]); ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($attemptProcess->canShowCancelledMessage()) : ?>
                            <div class="one-print-request__btns-text"><?php $attemptView->getCancelledText(); ?></div>
                        <?php endif; ?>

                        <div>
                            <?php if ($attemptView->needShowTrackingNumber()) : ?>
                                <?php echo _t('site.ps', 'Tracking number {tracking_number}, {tracking_shipper}', [
                                    'tracking_number'  => $attemptView->getTrackingNumberFormat(),
                                    'tracking_shipper' => $attemptView->getTrackingShipper(),
                                ]); ?>
                            <?php endif; ?>

                            <?php if ($attemptProcess->canChangeTrackingNumber()) : ?>
                                <button
                                        loader-click="setOrderSentWithTrackingNumber(<?= $attempt->id ?>, <?php echo($attempt->preorder ? 'true' : 'false'); ?>, <?= Html::encode(\yii\helpers\Json::encode([
                                            'shipper'        => $attemptView->getTrackingShipper(),
                                            'trackingNumber' => $attemptView->getTrackingNumberFormat()
                                        ])) ?>)"
                                        class="btn btn-xs btn-link one-print-request__btns-edit-tracknum">
                                    <span class="tsi tsi-pencil"></span>
                                    <?= _t('site.ps', 'Change tracking number'); ?>
                                </button>
                            <?php endif; ?>
                        </div>
                        <?php if ($attempt->getAttempReview()): ?>
                            <?php echo Html::a(ReviewStarsStaticWidget::widget(['rating' => $attempt->getAttempReview()->getRating()]), ['/workbench/reviews']); ?>
                        <?php endif; ?>
                    </div>

                    <?php if ($attempt->is_offer && $attempt->isNew()): ?>
                        <div class="pull-left m-t20">
                            <h3 class="m-t0 m-b0 text-danger">
                                <?= _t('site.ps', 'Print offer'); ?>
                            </h3>
                            <p class="m-b0">
                                <?= _t('site.ps',
                                    'This print offer has been sent to you because the original order placed by the customer was declined by the print service. You may choose to accept the order as it is, or create an additional payment for the customer with the additional service option available after accepting.'); ?>
                            </p>
                        </div>
                    <?php endif; ?>
                    <?php if ($attempt->order && $attempt->isRejected()): ?>
                        <div class="pull-left m-t20 bg-warning bar">
                            <?= _t('site.ps', 'Order results have not been accepted. Reason: {reason}.', ['reason' => $attempt->getRejectComment()]) ?>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if ($attempt->order->hasCuttingPack()): ?>
                    <?php echo $this->render('item-cutting', [
                        'attemptViewComposite' => $attemptViewComposite,
                    ]); ?>
                <?php elseif ($attempt->order->isFor3dPrinter()): ?>
                    <?php echo $this->render('item-model3d', [
                        'attemptViewComposite' => $attemptViewComposite,

                    ]); ?>
                <?php elseif ($attempt->preorder): ?>
                    <div class="panel-body ">
                        <?php if (!empty($attempt->order->preorder->message) || !empty($attempt->order->preorder->description)): ?>
                            <div class="m-b10">
                                <div><b><?= _t('site.order', 'Description'); ?></b></div>
                                <?= H($attempt->order->preorder->description); ?>
                                <?= H($attempt->order->preorder->message); ?>

                            </div>
                        <?php endif; ?>
                        <?php
                        $addressPreorder = '';
                        if ($attempt->order && $attempt->order->preorder && $attempt->order->preorder->shipTo) {
                            $addressPreorder = \common\models\UserAddress::formatAddressWithFields($attempt->order->preorder->shipTo);
                        } ?>
                        <?php if (!empty($addressPreorder)) { ?>
                            <div class="m-b10">
                                <div class="m-t0">
                                    <b>
                                        <?= _t('site.ps', 'Location'); ?>
                                    </b>
                                </div>
                                <?php echo $addressPreorder ?>
                            </div>
                        <?php } ?>
                        <div class="">

                            <b><?= _t('site.order', 'Estimated time:'); ?></b>
                            <?= _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}', ['n' => (int)$attempt->order->preorder->estimate_time]); ?>

                        </div>
                    </div>
                    <?php echo $this->render('@frontend/modules/workbench/views/preorder/preorder-invoice-table.php', [
                        'preorder'            => $attempt->order->preorder,
                        'showTotal'           => true,
                        'calculateAndShowFee' => false
                    ]); ?>
                <?php endif ?>

                <?php if ($attempt->moderation && $attempt->moderation->files): ?>
                    <div class="panel-body border-b p-b0">
                        <div class="one-print-request__steps-pic-list p-t0">
                            <?=
                            SwipeGalleryWidget::widget([
                                'files'            => ArrayHelper::getColumn($attempt->moderation->files, 'file'),
                                'thumbSize'        => [ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL],
                                'assetsByClass'    => true,
                                'containerOptions' => ['class' => 'ps-profile-user-review__user-models'],
                                'itemOptions'      => ['class' => 'ps-profile-user-review__user-models-pic'],
                                'scrollbarOptions' => ['class' => 'ps-profile-user-review__user-models-scrollbar'],
                            ]);
                            ?>
                        </div>
                        <?php if ($attempt->isDislikedByUser()): ?>
                            <div class="one-print-request__steps-disliked">
                                <span class="tsi tsi-thumbs-down m-r10"></span><?= _t("site.order", "Disliked by customer"); ?>
                            </div>
                        <?php elseif ($attempt->isAcceptedByUser()): ?>
                            <div class="one-print-request__steps-liked">
                                <span class="tsi tsi-checkmark-c m-r10"></span><?= _t("site.order", "Liked by customer"); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif ?>

                <!--- additional services -->
                <?php echo $this->render('@frontend/modules/workbench/views/service-orders/list-item-additional-services.php', [
                    'attemptViewComposite' => $attemptViewComposite
                ]); ?>

                <?php echo $this->render('@frontend/modules/workbench/views/service-orders/print-request-footer.php', [
                    'attemptViewComposite' => $attemptViewComposite
                ]); ?>

            </div>
        </div>
    </div>
</div>

<?php echo $this->render('@frontend/modules/workbench/views/service-orders/modals/download-model-terms-modal.php') ?>
<?php echo $this->render('@frontend/modules/workbench/views/service-orders/modals/request-scheduled-time-modal.php') ?>
