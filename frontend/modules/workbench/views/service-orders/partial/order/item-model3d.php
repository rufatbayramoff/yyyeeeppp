<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 31.08.18
 * Time: 15:08
 *
 * @var \frontend\modules\workbench\models\view\StoreOrderAttempViewModel $attemptView
 */

use frontend\components\UserSessionFacade;
use frontend\modules\workbench\components\OrderUrlHelper;
use frontend\widgets\PrintInstructionWidget;
use frontend\widgets\SiteHelpWidget;
use lib\MeasurementUtil;
use yii\helpers\Html;

/** @var $attemptViewComposite \frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite */

$attempt        = $attemptViewComposite->attempt;
$attemptView    = $attemptViewComposite->attemptView;
$attemptProcess = $attemptViewComposite->attemptProcess;

$userWightMeasure = UserSessionFacade::isUsMeasurement() ? MeasurementUtil::OUNCE : MeasurementUtil::GRAM;
$model3dReplica   = $attempt->order->getFirstReplicaItem();
$commonTexture    = $model3dReplica->getKitModel3dTexture();
?>

<script type="text/ng-template" id="/attempt/partitalRefund.html">
    <?= $this->render('partital-refund.php') ?>
</script>

<div class="panel-body one-print-request__body">
    <div class="row">
        <div class="col-sm-3 one-print-request__model-pic text-center">
            <?php echo Html::img($attemptView->getImgItem(), [
                'width' => '100',
                'alt'   => H($attemptView->getTitleItem()),
                'class' => 'order-model-image'
            ]); ?>

            <a href="<?php echo OrderUrlHelper::viewModel3dReplica($model3dReplica); ?>" target="_blank"
               class="btn btn-sm btn-link one-print-request__model-view">
                <?php echo _t('site.ps', '3D View'); ?>
            </a>
        </div>

        <div class="col-sm-9">
            <h3 class="one-print-request__model-title">
                <?php echo H($attemptView->getTitleItem()); ?>

                <?php if ($attempt->order->getIsTestOrder()) : ?>
                    <span class="label label-warning"><?= _t('site.ps', 'Test order') ?>
                        <?= SiteHelpWidget::widget(['alias' => 'ps.order.test']) ?>
                    </span>
                <?php endif; ?>
            </h3>

            <h4 class="text-muted">
                <?= _t('site.ps', 'Total weight'); ?>:
                <?php $totalWeight = MeasurementUtil::convertWeight($model3dReplica->getWeight(), MeasurementUtil::GRAM, $userWightMeasure, true); ?>
                <?php echo $totalWeight . ' ' . _t('site.common', $userWightMeasure); ?>.
                (<?= _t('site.ps', 'includes support calculations'); ?>)
                <br/>
                <span>
                    <?php echo _t('site.ps', 'Positional accuracy'); ?>
                    : &#xB1; <?php echo $attempt->machine->asPrinter()->getPositionalAccuracy(); ?> <?= _t('site.common', 'mm') ?>
                </span>
            </h4>

            <div class="row">
                <div class="col-md-5">
                    <table class="one-print-request__table">
                        <tr>
                            <td class="one-print-request__table__label" width="37%">
                                <?= _t('site.model3d', 'License'); ?>
                            </td>
                            <td width="63%">
                                <strong>
                                    <?= Html::a(
                                        _t('site.store', 'Commercial'),
                                        ['/site/license', 'id' => 1],
                                        ['target' => '_blank']
                                    ); ?>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td class="one-print-request__table__label" width="37%">
                                <?= _t('site.model3d', 'Invoice code'); ?>
                            </td>
                            <td width="63%">
                                <?= $attempt->order->primaryPaymentInvoice->uuid; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="one-print-request__table__label">
                                <?= _t('site.ps', 'Machine'); ?>
                            </td>
                            <td><?= H($attempt->machine->getTitleLabel()); ?></td>
                        </tr>

                        <?php if ($commonTexture) : ?>
                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Material'); ?>
                                </td>
                                <td><?= H($commonTexture->printerMaterial->title); ?></td>
                            </tr>
                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Infill'); ?>
                                </td>
                                <td>
                                    <?= $commonTexture->infillText() ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Color'); ?>
                                </td>
                                <td>
                                    <?php if ($commonTexture->printerColor) { ?>
                                        <div class="material-item__color" style="background-color: rgb(<?php echo H($commonTexture->printerColor->rgb); ?>)"></div>&nbsp;
                                        <?= H($commonTexture->printerColor->title) ?>
                                    <?php } else { ?>
                                        -
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
                <div class="col-md-7">
                    <table class="one-print-request__table">
                        <tr>
                            <td class="one-print-request__table__label">
                                <?= _t('site.ps', 'Quantity'); ?>
                            </td>
                            <td>
                                <?= _t(
                                    'site.ps',
                                    '{num, plural, =1{# item} other{# items}}',
                                    ['num' => $model3dReplica->getTotalQty()]
                                ); ?>
                            </td>
                        </tr>

                        <?php if ($attempt->order->getIsTestOrder()) : ?>
                            <tr>
                                <td class="one-print-request__table__label">
                                    <?php
                                    echo _t('site.ps', 'Manufacturing Fee');
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $printPrice           = $attemptView->getCalculateTestOrderPriceFn();
                                    $psPrinter            = $attempt->machine->asPrinter();
                                    $printPriceWithoutMin = null;

                                    if ($psPrinter->getMinPrintPriceWithoutFee() > $printPrice?->getAmount()) {
                                        $printPriceWithoutMin = $printPrice;
                                        $printPrice           = \lib\money\Money::usd($psPrinter->getMinPrintPriceWithoutFee());
                                    }
                                    ?>
                                    <?php echo displayAsMoney($printPrice); ?>
                                    <?php if ($printPriceWithoutMin) : ?>
                                        <?php echo '<br /><small>' . _t('site.ps', '(without min. order charge: {price})',
                                                ['price' => displayAsMoney($printPriceWithoutMin)]) . '</small>'; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding: 0;">
                                    <p class="bg-warning bar"
                                       style="padding: 5px 10px;font-size: 12px;line-height: 18px;">
                                        <?= _t('site.ps',
                                            "Please note: this is the manufacturing fee you would receive if this was a real order. If the price is inaccurate, please visit your machine's settings and update the prices."); ?>
                                    </p>
                                </td>
                            </tr>
                        <?php endif; ?>

                        <?= $this->render('item-fee', ['attemptViewComposite' => $attemptViewComposite]) ?>
                        <?= $this->render('item-delivery', ['attemptViewComposite' => $attemptViewComposite]) ?>

                    </table>
                </div>
            </div>

            <?php if ($attempt->order->comment) : ?>
                <h4 class="m-t0 m-b0"><?= _t('site.order', 'Customer comment'); ?></h4>
                <?php echo nl2br(H($attempt->order->comment)); ?>
            <?php endif; ?>

            <?php if ($model3dReplica->print_instructions) : ?>
                <div>
                    <h4 class="m-b0">
                        <?= _t('site.ps', 'Print instructions'); ?>
                    </h4>
                    <div>
                        <?= PrintInstructionWidget::widget(['model' => $model3dReplica->print_instructions]) ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?= $this->render('item-model3d-files', ['attemptViewComposite' => $attemptViewComposite]) ?>
    </div>
</div>
