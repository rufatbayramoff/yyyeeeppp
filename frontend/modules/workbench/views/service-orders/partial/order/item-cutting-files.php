<?php

/** @var \yii\web\View $this */

use common\services\Model3dPartService;
use frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite;
use yii\helpers\Html;

/** @var \common\models\CuttingPack $cuttingPack */

/** @var StoreOrderAttemptViewComposite $attemptViewComposite */
$attempt        = $attemptViewComposite->attempt;
$attemptView    = $attemptViewComposite->attemptView;
$attemptProcess = $attemptViewComposite->attemptProcess;

?>
<div style="margin-left: 15px;">
    <button class="btn btn-default  m-b20 m-t10"
            data-target="#js-files-list-<?php echo $attempt->order->id; ?>" data-toggle="collapse">
        <?= _t('site.ps', 'Files'); ?>
    </button>
</div>

<div class="collapse <?= $attempt->isNew() ? 'in' : ''; ?>"
     id="js-files-list-<?= $attempt->order->id; ?>">
    <div class="col-md-12">
        <?php
        foreach ($cuttingPack->activeCuttingPackFiles as $cuttingPackFile) {
            if ($cuttingPackFile->isImage()) {
                continue;
            }
            ?>
                <h4 class="one-print-request__model-title m-b0">
                    <button class="btn btn-link one-print-request__model-title-link"
                            loader-click='downloadFiles("<?= $attempt->id ?>", "<?= $cuttingPackFile->file->id ?>")'
                            loader-click-text="<i class='tsi tsi-refresh tsi-spin-custom'></i>
                        <?= _t('site.order', 'Preparing archive for download...') ?>"
                            href="#">
                        <?= H($cuttingPackFile->getTitle()); ?>
                    </button>
                </h4>
                <div class="row">
                    <?php
                    foreach ($cuttingPackFile->produceCuttingPackParts as $cuttingPackPart) {
                        echo $this->render('item-cutting-file-part', ['cuttingPackPart' => $cuttingPackPart]);
                    }
                    ?>
                    <div class="col-sm-12 p-l0 p-r0">
                        <hr class="m-t0 m-b15">
                    </div>
                </div>

            <?php
        }
        ?>
        <div class="row m-b10">
<!--            <div class="col-sm-3 one-print-request__model-pic"></div>-->
            <div class="col-sm-12 m-t10">
                <button class="btn btn-sm btn-default m-b10"
                        loader-click="downloadFiles(<?php echo $attempt->id ?>)"
                        loader-click-text="<i class='tsi tsi-refresh tsi-spin-custom'></i> <?= _t('site.order',
                            'Preparing archive for download...') ?>"
                        href="#"><?= _t('site.ps', 'Download file(s)'); ?>
                </button>

                <?php if ($attempt->isNew()): ?>
                    <p class="alert alert-warning">
                        <?php echo _t('site.ps', 'Please do not produce downloaded files without accepting the order.'); ?>
                    </p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
