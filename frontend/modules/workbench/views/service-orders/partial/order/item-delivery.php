<?php

/** @var $attemptViewComposite \frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite */

$attempt        = $attemptViewComposite->attempt;
$attemptView    = $attemptViewComposite->attemptView;
$attemptProcess = $attemptViewComposite->attemptProcess;

?>
<?php if ($attempt->order->hasDelivery()) : ?>
    <tr>
        <td class="one-print-request__table__label">
            <?= _t('site.ps', 'Delivery'); ?>
        </td>
        <td>
            <?php if ($attempt->isFreeDelivery()) : ?>
                <?php echo _t('site.ps', 'Free Delivery'); ?>
            <?php elseif (($deliveryDetails = $attempt->order->getOrderDeliveryDetails()) && ($deliveryDetails['carrier'] === common\models\DeliveryType::CARRIER_MYSELF)) : ?>
                <?php $price = $attempt->order->primaryPaymentInvoice->storeOrderAmount->getAmountShipping(); ?>
                <?php if ($price) : ?>
                    <?php echo _t(
                        'site.ps',
                        'Shipping price {price}',
                        ['price' => displayAsMoney($price)]
                    ); ?>
                <?php endif; ?>
            <?php endif; ?>
            <div>
                <?php echo $attempt->getDeliveryTypesIntl(); ?>
            </div>
        </td>
    </tr>

    <?php $packagePrice = $attempt->order->primaryPaymentInvoice->storeOrderAmount->getAmountPackage(); ?>
    <?php if ($packagePrice && $packagePrice->getAmount()) : ?>
        <tr>
            <td class="one-print-request__table__label">
                <?= _t('site.ps', 'Packaging fee'); ?>
            </td>
            <td>
                <?= displayAsMoney($packagePrice); ?>
            </td>
        </tr>
    <?php endif; ?>

    <tr>
        <td class="one-print-request__table__label">
            <?= _t('site.ps', 'Location'); ?>
        </td>
        <td>
            <?php echo $attempt->getDeliveryFormatAddress(); ?>
        </td>
    </tr>
<?php endif; ?>
