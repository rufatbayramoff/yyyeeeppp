<?php

use common\models\CuttingPackPart;
use yii\helpers\Html;

/** @var CuttingPackPart $cuttingPackPart */
?>
<div class="col-sm-6 col-md-4 order-model-data__model-cutting">
    <div class="order-model-data__model-cutting-card">
        <h4 class="m-t0 m-b0 text-muted">
            <?= _t('site.ps', 'Part '); ?><?= $cuttingPackPart->cuttingPackPage->cuttingPackFile->getPartNum($cuttingPackPart) ?>
        </h4>
        <?= Html::img($cuttingPackPart->getPreviewUrl(), [
            'width'    => '100',
            'class'    => 'order-model-image order-model-data__cutting-pic m-b10 m-t5',
            'ng-click' => 'popupImageView($event)'
        ]); ?>
        <table class="one-print-request__table m-b0">
            <tr>
                <td class="one-print-request__table__label">
                    <?= _t('site.ps', 'Qty'); ?>
                </td>
                <td><?= H($cuttingPackPart->qty) ?> <?= _t('site.ps', 'items')?></td>
            </tr>
            <tr>
                <td class="one-print-request__table__label">
                    <?= _t('site.ps', 'Cutting length'); ?>
                </td>
                <td><?= H($cuttingPackPart->cutting_length) ?> mm</td>
            </tr>
            <tr>
                <td class="one-print-request__table__label">
                    <?= _t('site.ps', 'Engraving length'); ?>
                </td>
                <td><?= H($cuttingPackPart->engraving_length) ?> mm</td>
            </tr>

            <?php if ($cuttingPackPart->calcThickness()): ?>
                <tr>
                    <td class="one-print-request__table__label">
                        <?= _t('site.ps', 'Thickness'); ?>
                    </td>
                    <td><?= H($cuttingPackPart->calcThickness()) ?></td>
                </tr>
            <?php endif; ?>
            <?php if ($cuttingPackPart->calcMaterial()): ?>
                <tr>
                    <td class="one-print-request__table__label">
                        <?= _t('site.ps', 'Material'); ?>
                    </td>
                    <td><?= H($cuttingPackPart->calcMaterial()->title) ?></td>
                </tr>
            <?php endif; ?>
            <?php if ($cuttingPackPart->calcColor()): ?>
                <tr>
                    <td class="one-print-request__table__label">
                        <?= _t('site.ps', 'Color'); ?>
                    </td>
                    <td>
                        <div title="<?= H($cuttingPackPart->calcColor()->title) ?>" class="material-item">
                            <div class="material-item__color"
                                 style="background-color: rgb(<?php echo H($cuttingPackPart->calcColor()->rgb); ?>)"></div>
                            <div class="material-item__label"><?= H($cuttingPackPart->calcColor()->title) ?></div>
                        </div>
                    </td>
                </tr> <?php endif; ?>
        </table>
    </div>
</div>

