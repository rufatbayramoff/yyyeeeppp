<?php


use common\components\Model3dWeightCalculator;
use common\services\Model3dPartService;
use frontend\models\model3d\Model3dFacade;
use frontend\models\store\StoreFacade;
use yii\helpers\Html;

foreach (StoreFacade::getSnapModel3dParts($oneStoreItem) as $model3dPart) :
    if ($model3dPart->qty === 0) {
        continue;
    }
    $netWeight = $model3dPart->getWeight() * $oneStoreItem->qty;
    $supportWeightOne = Model3dWeightCalculator::calculateModel3dPartSupportWeight($model3dPart) / $model3dPart->qty;
    $size = $model3dPart->getSize()->toStringWithMeasurement();
    ?>

    <div class="row one-print-request__store-item js-store-item-file">
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-12 one-print-request__model-pic">

                            <?php

                            $model3d = $oneStoreItem->unit->model3d;
                            $cover = Model3dFacade::getCover($model3d);

                            $fileRender = Model3dPartService::getModel3dPartRenderUrlIfExists($model3dPart);

                            if ($fileRender) {
                                echo Html::img(
                                    $fileRender,
                                    [
                                        'width' => '130',
                                        'class' => 'order-model-image'
                                    ]
                                );
                            } else {
                                if ($cover) {
                                    echo Html::img(
                                        $cover['image'],
                                        [
                                            'width' => '130px',
                                            'alt'   => $model3d->title,
                                            'class' => 'order-model-image'
                                        ]
                                    );
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <table class="one-print-request__table one-print-request__table--print-order">
                        <tr>
                            <td class="one-print-request__table__label">
                                <?= _t('site.ps', 'File name'); ?>:
                            </td>
                            <td>
                                <?= H($model3dPart->name); ?>
                            </td>
                        </tr>

                        <?php if (!$oneOrder->isForPreorder()) : ?>

                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Material'); ?>:
                                </td>
                                <td>
                                    <?php

                                    if ($oneOrder->isFor3dPrinter()) {
                                        $texture = $model3dPart->getCalculatedTexture();
                                        $materialTitle = $texture && $texture->printerMaterial ? $texture->printerMaterial->title : '-';
                                        if ($materialTitle === '-' && $texture->printerMaterialGroup) {
                                            $materialTitle = $texture->printerMaterialGroup->title;
                                        }
                                        echo $materialTitle;
                                    }
                                    ?>
                                </td>
                            </tr>

                        <?php endif; ?>

                        <?php if ($attempt->order->isFor3dPrinter()): ?>

                            <tr>
                                <td class="one-print-request__table__label">
                                    <?= _t('site.ps', 'Color'); ?>:
                                </td>
                                <td>
                                    <?= H($model3dPart->getCalculatedTexture()->printerColor->render_color); ?>
                                </td>
                            </tr>

                        <?php endif ?>

                        <tr>
                            <td class="one-print-request__table__label">
                                <?= _t('site.ps', 'Quantity'); ?>:
                            </td>
                            <td>
                                <?= $model3dPart->qty * $oneStoreItem->qty ?>
                            </td>
                        </tr>



                    </table>
                </div>
            </div>
        </div>
    </div>

<?php endforeach ?>