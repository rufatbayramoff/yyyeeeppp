<?php

use common\models\message\helpers\UrlHelper;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderReview;
use frontend\models\ps\StoreOrderAttempSearchForm;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\modules\workbench\models\view\StoreOrderAttempViewModel;
use frontend\widgets\SiteHelpWidget;
use lib\delivery\parcel\ParcelFactory;
use yii\helpers\Html;

/** @var StoreOrderAttemp $attempt */
/** @var StoreOrderAttempSearchForm $form */

$attempt        = $model;
$review         = $attempt->order ? StoreOrderReview::find()->forAttemp($attempt)->forPs($attempt->ps)->isCanShow()->one() : null;
$attemptView    = StoreOrderAttempViewModel::fill($attempt);
$attemptProcess = StoreOrderAttemptProcess::create($attempt);
?>


    <div class="service-order-row">
        <?php if ($attempt->order && $attempt->isRejected()): ?>
            <div class="service-order-row__status bg-warning">
                <?= _t('site.ps', 'Order results have not been accepted. Reason: {reason}.', ['reason' => $attempt->getRejectComment()]) ?>
            </div>
        <?php endif; ?>

        <?php if ($attemptView->isModel3dRemoved()): ?>
            <div class="service-order-row__status bg-warning">
                <?= _t('site.ps', 'The order model has been moved to the archive.') ?>
            </div>
        <?php endif; ?>

        <?php if ($attempt->is_offer && $attempt->isNew()): ?>
            <div class="service-order-row__status bg-info">
                <h4 class="m-t0 m-b0 text-danger">
                    <?= _t('site.ps', 'Print offer'); ?>
                </h4>
                <p class="m-b0">
                    <?= _t('site.ps',
                        'This print offer has been sent to you because the original order placed by the customer was declined by the print service. You may choose to accept the order as it is, or create an additional payment for the customer with the additional service option available after accepting.'); ?>
                </p>
            </div>
        <?php endif; ?>

        <?php if ($attemptView->isPaymentPendingWarningNeed()): ?>
            <div class="service-order-row__status bg-warning">
                <?= _t('site.ps',
                    "Payment has been made and is pending authorization. You may accept the order but please don't start printing until payment has been authorized. Need help? {contactLink}",
                    ['contactLink' => Html::a(_t('site.ps', 'Contact support'), UrlHelper::supportMessageRoute($attempt->order), ['target' => '_blank'])]); ?>
            </div>
        <?php endif; ?>

        <?php if ($attempt->order && $attempt->order->getIsTestOrder()): ?>
            <div class="service-order-row__status bg-warning">
                <h4 class="m-t0 m-b0">
                    <?= _t('site.ps', 'Test order') ?>&nbsp;<?= SiteHelpWidget::widget(['alias' => 'ps.order.test']) ?>
                </h4>
            </div>
        <?php endif; ?>

        <div class="service-order-row__title">
            <div class="service-order-row__num">
                <a href="<?= $attempt->getViewUrl() ?>">
                <span>
                    <?php echo H($attempt->getTitleLabel()); ?>
                </span>
                </a>
                <?php if ($attempt->preorder && $attempt->preorder->isCncType()) {
                    echo " - <span class='service-order-row__cnc'>CNC</span>";
                }
                ?>
                <?php if ($attempt->informerStoreOrderAttempts) { ?>
                    <div class="service-order-row__notification-label"></div>
                <?php } ?>
            </div>

            <?php if ($deadlineTimer = $attempt->getDeadlineCompany()) : ?>
                <div class="service-order-row__deadline">
                    <?php
                    if ($deadlineTimer->getLabelString()) {
                        echo '<strong>' . $deadlineTimer->getLabelString() . '</strong>';
                    } else {
                        echo '<strong>' . $deadlineTimer->getTitle() . ':</strong>';
                        echo \frontend\widgets\Countdown::widget([
                            'id'              => 'countdownattemp-' . $attempt->id,
                            'options'         => ['class' => 'label label-info'],
                            'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                            'timeout'         => $deadlineTimer->getTimeout(),
                            'finishedMessage' => _t('site.ps', '00:00:00')
                        ]);
                    } ?>
                </div>
            <?php endif; ?>

            <?php if ($review): ?>
                <div class="service-order-row__reviews">
                    <?php
                    $stars = \frontend\widgets\ReviewStarsStaticWidget::widget(['rating' => $review->getRating()]);
                    echo Html::a($stars, '/workbench/reviews');
                    ?>
                </div>
            <?php endif; ?>

            <?php if ($attempt->order && $attempt->order->isPayed()) : ?>
                <div class="service-order-row__actions">
                    <div class="dropdown">
                        <button class="btn btn-default btn-sm dropdown-toggle dropdown-menu-right-toggle" type="button"
                                id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <svg class="svg-ico" xmlns="http://www.w3.org/2000/svg" id="dots3" data-name="dots3"
                                 viewBox="0 0 16 4">
                                <circle cx="2" cy="2" r="2"/>
                                <circle cx="8" cy="2" r="2"/>
                                <circle cx="14" cy="2" r="2"/>
                            </svg>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li>
                                <a href="<?php echo $attempt->getViewUrl(); ?>">
                                    <span class="tsi tsi-eye text-primary"></span><?php echo _t('workbench.mysales', 'View order'); ?>
                                </a>
                            </li>
                            <?php if ($attemptProcess->canAcceptOrder()): ?>
                                <li>
                                    <a loader-click="setOrderStatus(<?php echo $attempt->id; ?>, '<?php echo StoreOrderAttemp::STATUS_ACCEPTED; ?>')"
                                       href="#">
                                        <span class="tsi tsi-checkmark-c text-success"></span><?php echo _t('site.ps', 'Accept'); ?>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if ($attemptProcess->canDeclineOrder()): ?>
                                <li>
                                    <a ng-click="declineOrder(<?php echo $attempt->id ?>)" href="javascript:void(0);">
                                        <span class="tsi tsi-remove-c text-danger"></span><?php echo _t('front.user', 'Decline'); ?>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ($attemptProcess->canSetAsPrinted()) : ?>
                                <li>
                                    <a href="<?php echo $attempt->getViewUrl(); ?>">
                                        <?php if ($attempt->isRejected()): ?>
                                            <span class="tsi tsi-redo text-success"></span><?php echo _t('site.ps', 'Re-submit results'); ?>
                                        <?php else: ?>
                                            <span class="tsi tsi-checkmark-c text-success"></span><?php echo _t('site.ps', 'Submit results'); ?>
                                        <?php endif; ?>
                                    </a>
                                </li>
                            <?php endif ?>
                            <?php if ($attemptProcess->canContactCustomer()) : ?>
                                <li>
                                    <?= Html::a('    <span class="tsi tsi-message text-info"></span> ' . _t('site.ps', 'Contact customer'), UrlHelper::objectMessageRoute($attempt->order), ['target' => '_blank']); ?>
                                </li>
                            <?php endif ?>

                            <?php if ($attemptProcess->canGetPostalLabel()) {
                                $parcelInLocalMeasure = ParcelFactory::createFromOrder($attempt->order);
                                if ($attempt->ps->user->userProfile->current_metrics === 'in') {
                                    $parcelInLocalMeasure = ParcelFactory::convertParcelInInch($parcelInLocalMeasure);
                                }
                                ?>
                                <li>
                                    <a loader-click="getPostalLabel(<?= $attempt->id ?>, <?= sprintf('%.2F, %.2F, %.2F, %.2F',
                                        $parcelInLocalMeasure->length, $parcelInLocalMeasure->width, $parcelInLocalMeasure->height, $parcelInLocalMeasure->weight); ?>)"
                                       href="#">
                                        <span class="tsi tsi-doc text-primary"></span><?php echo _t('site.ps', 'Generate postal label'); ?>
                                    </a>
                                </li>
                            <?php } ?>

                            <?php if ($attemptProcess->canSetAsSentWithTrackingNumber()): ?>
                                <li>
                                    <a loader-click="setOrderSentWithTrackingNumber(<?php echo $attempt->id ?>, <?php echo($attempt->order->isForPreorder() ? 'true' : 'false'); ?>)"
                                       href="#">
                                        <span class="tsi tsi-truck text-success"></span><?php echo _t('site.ps', 'Dispatch'); ?>
                                    </a>
                                </li>
                            <?php elseif ($attemptProcess->canSetAsSent()): ?>
                                <li>
                                    <a loader-click="setOrderStatus(<?= $attempt->id ?>, '<?= StoreOrderAttemp::STATUS_SENT; ?>')"
                                       href="#">
                                        <span class="tsi tsi-truck text-success"></span>
                                        <?php echo _t('site.ps', ($attempt->isPickupDelivery() ? 'Set as picked up' : 'Dispatch')); ?>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <div class="service-order-row__data">
            <div class="designer-card__cert-pic">
                <?php
                if ($imageUrl = $attemptView->getImgItem()) { ?>
                    <a class="service-order-row__pic" href="<?= $attempt->getViewUrl() ?>" target="_blank">
                  <span class="service-order-row__pic-wrap">
                     <img src="<?php echo \HL($imageUrl) ?>" width="80">
                  </span>
                    </a>
                    <?php
                } else {
                    ?>
                    <div class="designer-card__cert-pic-empty">
                        No image
                    </div>
                    <?php
                }
                ?>
            </div>

            <div class="service-order-row__qty">
                <div class="service-order-row__qty-badge" title="<?= _t("site.ps", "Quantity"); ?>">
                    &times;<?php echo \H($attemptView->getQty()); ?></div>
            </div>
            <?php if ($attemptView->getPrice()): ?>
                <div class="service-order-row__price">
                    <strong><?php echo \H($attemptView->getPrice()); ?></strong>
                    <?php if ($priceNote = $attemptView->getPriceNote()): ?>
                        <span data-toggle="tooltip" data-placement="top"
                              data-original-title="<?php echo $priceNote; ?>"
                              class="tsi tsi-warning-c">
                    </span>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <?php if ($attemptView->getMaterialTitle()): ?>
                <div class="service-order-row__mat-color">
                    <div class="m-r10"><?php echo H($attemptView->getMaterialTitle()); ?></div>
                    <?php if ($printerColor = $attemptView->getMaterialColor()): ?>
                        <div class="material-item">
                            <div class="material-item__color"
                                 style="background-color: rgb(<?php echo H($printerColor->rgb); ?>)"></div>
                            <div class="material-item__label">
                                <?php echo H($printerColor->title); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <div class="service-order-row__machine">
                <?php
                echo H($attemptView->getTitleItem());
                echo $attemptView->getTitleItemRaw();
                ?> &nbsp;&nbsp;&nbsp;
                <?php if ($attemptView->getTitleService()) { ?>
                    <span class="tsi tsi-printer3d"></span><?php echo H($attemptView->getTitleService()); ?>
                <?php } ?>
            </div>
        </div>

        <?php if ($attemptProcess->canSetAsSent() && $deliveryFormatAddress = $attempt->getDeliveryFormatAddress(false)) { ?>
            <div class="service-order-row__footer">
                <div><?= _t('site.preorder', 'Created on') ?> <?php echo app()->formatter->asDatetime($attempt->order->created_at, 'short'); ?></div>
                <span class="m-r20">
                <?php echo _t('site.ps', 'Shipping address: {deliveryFormatAddress}', [
                    'deliveryFormatAddress' => $deliveryFormatAddress,
                ]); ?>
            </span>
            </div>
        <?php } elseif ($attemptView->needShowFooterTableLayout($attemptProcess)) { ?>
            <div class="service-order-row__footer">
                <span class="m-r20"><?= _t('site.preorder', 'Created on') ?> <?php echo app()->formatter->asDatetime($attempt->order->created_at, 'short'); ?></span>
                <?php if ($attempt->isPendingModeration()): ?>
                    <?php echo _t('site.ps', 'Pending moderation '); ?>
                    <?php echo SiteHelpWidget::widget(['alias' => 'ps.printreview']); ?>
                <?php else: ?>
                    <?php if ($attemptProcess->canShowReceivedMessage()): ?>
                        <span class="m-r20">
                        <?php echo _t('site.ps', 'Received on {date}', ['date' => app('formatter')->asDatetime($attempt->dates->received_at, 'short')]) ?>
                    </span>
                    <?php endif; ?>

                    <?php if ($attemptProcess->canShowShippedMessage()): ?>
                        <span class="m-r20">
                        <?php echo _t('site.ps', 'Dispatched on {date}', ['date' => app('formatter')->asDatetime($attempt->dates->shipped_at, 'short')]); ?>
                    </span>
                    <?php endif; ?>

                    <?php if ($attemptProcess->canShowCancelledMessage()): ?>
                        <span class="m-r20">
                        <?php echo $attemptView->getCancelledText() ?>
                    </span>
                    <?php endif; ?>

                    <?php if ($attemptView->needShowTrackingNumber()): ?>
                        <span>
                        <?php echo _t('site.ps', 'Tracking number {tracking_number} ({tracking_shipper})', [
                            'tracking_number'  => $attemptView->getTrackingNumberFormat(),
                            'tracking_shipper' => $attemptView->getTrackingShipper(),
                        ]); ?>
                    </span>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        <?php } else { ?>
            <div class="service-order-row__footer">
                <span><?= _t('site.preorder', 'Created on') ?> <?php echo app()->formatter->asDatetime($attempt->created_at, 'short'); ?></span>
            </div>
        <?php } ?>
    </div>

<?php
if (in_array($form->statusGroup, [StoreOrderAttemp::STATUS_SENT, StoreOrderAttemp::STATUS_RECEIVED, StoreOrderAttemp::STATUS_CANCELED])) {
    \common\modules\informer\InformerModule::markAsViewedCompanyOrderInformer($attempt);
}
