<?php
/**
 * @var \frontend\models\ps\StoreOrderAttempSearchForm $form
 */


Yii::$app->angular
    ->service(['modal', 'notify', 'router', 'user', 'psDownloadModel'])
    ->controller('ps/orders/list')
    ->controllerParams([
        'company' => $form->getPs()
    ]);
?>

<div ng-controller="PsOrdersListController">
    <?php if (!$dataProvider->getTotalCount()): ?>
        <h3 class="no-print-requests m-l0 m-r0"><?= _t("site.ps", "No orders found"); ?></h3>
    <?php else: ?>
        <?php
        $listView = Yii::createObject([
            'class'        => \yii\widgets\ListView::class,
            'dataProvider' => $dataProvider,
            'itemView'     => 'attemptItem',
            'viewParams'   => [
                'form' => $form
            ]
        ]); ?>

        <?php if ($listView->dataProvider->getCount() > 0): ?>
            <?php echo $listView->renderItems() ?>
            <div class="row">
                <div class="col-md-8">
                    <?php echo $listView->renderPager() ?>
                </div>
            </div>
        <?php endif;?>
    <?php endif;?>
</div>