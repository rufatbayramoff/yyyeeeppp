<?php

use kartik\form\ActiveForm;
use yii\bootstrap\Html;
use frontend\models\ps\StoreOrderAttempSearchForm;

/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 29.08.18
 * Time: 11:07
 *
 * @var $form StoreOrderAttempSearchForm
 */

?>
<div class="filtersBlock">
    <?php $filterForm = ActiveForm::begin([
        'action'                 => ['', 'statusGroup' => $form->statusGroup],
        'method'                 => 'get',
        'enableClientValidation' => false,
        'id'                     => 'orders-filter-form',
        'options' => [
                'ng-init' => 'initOrdersFilterForm()'
        ]
    ]); ?>

    <div class="row">
        <div class="col-xs-6">
            <?= $filterForm->field($form, 'machineId')
                ->label(false)
                ->dropDownList($form->machinesList(), ["class" => ""]) ?>
        </div>

        <div class="col-xs-6">
            <?=
            $filterForm->field($form, 'orderSearch', [
                'addon' => [
                    'append' => [
                        'content'  => Html::button('<i class="tsi tsi-search"></i>',
                            ['class' => 'btn btn-default p-l0 p-r0', 'id' => 'search-button']),
                        'asButton' => true
                    ]
                ]
            ])
                ->textInput(['autocomplete' => 'off', 'maxlength' => StoreOrderAttempSearchForm::ORDER_SEARCH_MAX_LENGTH])
                ->label(false)
                ->input("", ["class" => ""]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
