<script type="text/ng-template" id="/app/ps/orders/order-request-tracking-number.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t("site.ps", "Tracking number"); ?></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" name="requestTrackingNumber">

                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.ps', 'Shipping company') ?></label>
                            <div class="col-sm-8">
                                <input
                                        ng-model="form.shipper"
                                        type="text" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div ng-hide="form.withoutTracking" class="m-b20">

                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t("site.ps", "Tracking number"); ?></label>
                            <div class="col-sm-8">
                                <input
                                    ng-model="form.trackingNumber"
                                    type="text" class="form-control" placeholder="">
                            </div>
                        </div>
                        </div>

                        <div ng-hide="!form.withoutTracking || form.isPreorder" class="alert alert-warning">
                                <?=_t('site.ps','Please note that orders shipped without a tracking number have a waiting period of 30 days for domestic delivery and 45 days for international delivery, or until the client confirms delivery.');?>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-8">
                                <input ng-model="form.withoutTracking" type="checkbox" id="withTracking">
                                <label for="withTracking">
                                    <?= _t('site.ps', 'Shipping without tracking number') ?>
                                </label>
                            </div>
                        </div>
                    </form>

                    <div class="alert alert-info alert--text-normal m-t20 m-b0">
                        <?= _t("site.ps", "Please do not dispose of the mailing receipt until the order is completed for proof of shipment."); ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button
                        ng-click="$dismiss()"
                        type="button" class="btn btn-default"><?= _t('site.ps', 'Cancel') ?></button>
                    <button
                        loader-click="saveTrackingNumber()"
                        loader-click-unrestored="true"
                        ng-disabled="requestTrackingNumber.$invalid"
                        type="button" class="btn btn-primary"><?= _t('site.ps', 'Save') ?></button>
                </div>
            </div>
        </div>
    </div>
</script>

