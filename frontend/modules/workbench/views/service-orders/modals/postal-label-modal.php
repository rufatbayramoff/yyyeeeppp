<script type="text/ng-template" id="/app/ps/orders/postal-label-modal.html">

    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close t-postal-label-modal-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t("site.ps", "Postal Label"); ?></h4>
                </div>

                <div class="modal-body" style="overflow-y: scroll;max-height: 420px;">
                    <img ng-src="{{postalLabelUrl}}" width="100%" class="print-request-image">
                </div>

                <div class="modal-footer">
                    <button
                        ng-click="$dismiss()"
                        type="button" class="btn btn-default"><?= _t('site.ps', 'Close') ?></button>
                    <a ng-href="{{postalLabelPrintUrl}}" class="btn btn-success" target="_blank"><?= _t("site.ps", "Print"); ?></a>
                </div>
            </div>
        </div>
    </div>

</script>
