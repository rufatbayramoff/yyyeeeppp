<script type="text/ng-template" id="/app/ps/orders/reuqest-more-time-modal.html">

    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t("site.ps", "Set new deadline for completing the order"); ?></h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal" name="requestMoreTimeForm">

                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?= _t('site.ps', 'New deadline') ?></label>
                            <div class="col-sm-10">

                                <div class="input-group date" datetime-picker="form.plan_printed_at">
                                    <input type="text" class="form-control" placeholder="<?= _t('site.ps', 'Deadline') ?>"/>
                                    <span class="input-group-addon">
                                            <span class="tsi tsi-calendar"></span>
                                        </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?= _t("site.ps", "Comments"); ?></label>
                            <div class="col-sm-10">
                                    <textarea
                                        rows="3"
                                        ng-model="form.request_reason"
                                        class="form-control" placeholder="<?= _t("site.ps", "Comments"); ?>"></textarea>
                            </div>
                        </div>

                    </form>

                </div>
                <div class="modal-footer">

                    <button
                        ng-click="$dismiss()"
                        type="button" class="btn btn-default"><?= _t('site.ps', 'Cancel') ?></button>
                    <button
                        loader-click="requestMoreTime()"
                        loader-click-unrestored="true"
                        ng-disabled="requestMoreTimeForm.$invalid"
                        type="button" class="btn btn-primary"><?= _t('site.ps', 'Submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</script>
