<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.05.17
 * Time: 15:33
 */
use yii\helpers\Html;

?>
<script type="text/ng-template" id="/app/ps/orders/download-model-terms-modal.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t('site.ps', 'Download file(s)') ?></h4>
                </div>
                <div class="modal-body">
                    <p><?= _t('site.ps', 'After completing the order, please delete all files you have downloaded in accordance with our {terms}',
                            ['terms'=>Html::a(_t('site.ps','Terms of Use'), '/site/terms', ['target'=>'_blank'])]) ?></p>
                    <div class="checkbox checkbox-warning">
                        <input
                            ng-model="dont_show_download_policy_modal"
                            id="checkbox1" type="checkbox">
                        <label for="checkbox1"><?= _t('site.ps', 'Don\'t show me again') ?></label>
                    </div>
                </div>
                <div class="modal-footer">

                    <button
                        ng-click="cancel()"
                        type="button" class="btn btn-default"><?= _t('site.ps', 'Cancel') ?></button>

                    <button
                        loader-click="agree()"
                        type="button" class="btn btn-primary"><?= _t('site.ps', 'I agree') ?></button>
                </div>
            </div>
        </div>
    </div>
</script>
