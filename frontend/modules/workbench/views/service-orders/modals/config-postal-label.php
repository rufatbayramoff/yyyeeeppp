<script type="text/ng-template" id="/app/ps/orders/config-postal-label.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t("site.ps", "Generate postal label"); ?></h4>
                </div>
                <div class="modal-body">
                    <form class="form-vertical" name="configPostalLabel" id="configPostalLabel">
                        <fieldset ng-disabled="parent.gettingPostalLabel" style="border: none;">
                            <h4 class="m-t0"><?= _t('site.ps', 'Estimated parcel size') ?> ({{currentMeasure}})</h4>

                            <div class="row">
                                <div class="col-xs-4 col-sm-3">
                                    <div class="form-group">
                                        <label><?= _t('site.ps', 'Length') ?></label>
                                        <input ng-model="form.length" type="number" step="0.01"
                                               class="form-control input-sm" placeholder="">
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3">
                                    <div class="form-group">
                                        <label><?= _t('site.ps', 'Width') ?></label>
                                        <input ng-model="form.width" type="number" step="0.01"
                                               class="form-control input-sm" placeholder="">
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3">
                                    <div class="form-group">
                                        <label><?= _t('site.ps', 'Height') ?></label>
                                        <input ng-model="form.height" type="number" step="0.01"
                                               class="form-control input-sm" placeholder="">
                                    </div>
                                </div>
                            </div>

                            <h4><?= _t("site.ps", "Estimated parcel weight"); ?></h4>
                            <div class="row">
                                <div class="col-xs-6 col-sm-4">
                                    <div class="input-group m-b20">
                                        <input
                                                ng-model="form.weight"
                                                type="number" step="0.01" class="form-control input-sm" placeholder="">
                                        <span class="input-group-addon"> {{currentMeasure=='mm'?'gr':'oz'}}</span>
                                    </div>
                                </div>
                            </div>

                            <p class="m-b0">
                                <?= _t('site.ps', 'If the parcel is bigger or weighs more, please enter the correct information to generate a new shipping label.'); ?>
                            </p>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button
                            ng-click="cancelPrintLabel()"
                            type="button" class="btn btn-default"><?= _t('site.ps', 'Cancel') ?></button>
                    <button
                            loader-click="printPostalLabel()"
                            loader-click-unrestored="false"
                            type="button" class="btn btn-primary"><?= _t('site.ps', 'Print postal label') ?></button>
                </div>
            </div>
        </div>
    </div>
</script>