<?php

use common\models\NotifyPopup;
use common\models\StoreOrderAttemp;
use common\modules\browserPush\widgets\SubscribeNotificationsWidget;
use frontend\assets\DatetimePickerAsset;
use frontend\models\ps\DeclineCncPreorderByCustomerForm;
use frontend\models\ps\DeclineCncPreorderByPsForm;
use frontend\models\ps\DeclineOrderForm;
use frontend\models\ps\DeclineProductOrderForm;
use frontend\models\ps\StoreOrderAttempSearchForm;
use frontend\modules\preorder\components\ProductPreorderDeclineForm;
use frontend\modules\preorder\components\ServicesPreorderDeclineForm;
use frontend\modules\workbench\widgets\CompanySalesSidebarWidget;

/** @var string $statusGroup */
/** @var \yii\web\View $this */
/** @var StoreOrderAttempSearchForm $form */
/** @var \common\modules\payment\services\RefundService $refundService */
/** @var StoreOrderAttemp[] $attemps Orders attemps */

$this->registerAssetBundle(DatetimePickerAsset::class);

Yii::$app->angular
    ->service(['modal', 'notify', 'router', 'measure', 'mathUtils', 'user', 'cnc-service'])
    ->directive('datetime-picker')
    ->controller('ps/orders/list')
    ->controller('cnc/cnc-models')
    ->controller('cnc/cnc-models')
    ->controller('store/common-models')
    ->controller('preorder/preorder-models')
    ->controller('preorder/preorder-service')
    ->controller('order/store-order-service')
    ->controller('store/filepage/models')
    ->constants([
            'declineReasons'                    => DeclineOrderForm::getSuggestObjectList(),
            'declineProductReasons'             => DeclineProductOrderForm::getSuggestObjectList(),
            'cncPreorderDeclineReasons'         => DeclineCncPreorderByPsForm::getSuggestObjectList(),
            'cncPreorderDeclineCustomerReasons' => DeclineCncPreorderByCustomerForm::getSuggestObjectList(),
            'preorderDeclineReasons'            => ServicesPreorderDeclineForm::getSuggestObjectList(),
            'servicesPreorderDeclineReasons'    => ServicesPreorderDeclineForm::getSuggestObjectList(),
            'productPreorderDeclineReasons'     => ProductPreorderDeclineForm::getSuggestObjectList(),
        ]
    )
    ->controllerParams([
        'x' => 'y'
    ]);

echo $this->renderFile('@frontend/modules/workbench/views/preorder/templates/order-decline-modal.php');

$this->title                   = _t('site.my', 'My Sales');
$this->params['breadcrumbs'][] = ['label' => _t('front.user', 'Private profile'), 'url' => ['/user-profile/']];
$this->params['breadcrumbs'][] = $this->title;

if ($statusGroup === StoreOrderAttemp::STATUS_NEW) {
    echo SubscribeNotificationsWidget::widget([
        'message' => _t('store.order', 'Get information about new orders using browser push notifications'),
        'code'    => NotifyPopup::CODE_NEW_ORDER
    ]);
}
echo $this->renderFile('@frontend/views/templates/viewImagePopup.php');
?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(); ?>

<div class="container">
    <div class="row ps-index requests-list">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="row">
                    <?= CompanySalesSidebarWidget::widget(['ps' => $form->getPs(), 'activeStatusGroup' => $statusGroup]) ?>

                    <div class="col-sm-9 wide-padding--left">
                        <?= $this->render('partial/filtersBlock', [
                            'form' => $form
                        ]); ?>

                        <?= $this->render('partial/list/itemsTable', [
                            'dataProvider' => $dataProvider,
                            'form'         => $form,
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('modals/postal-label-modal.php'); ?>
    <?= $this->render('modals/reuqest-more-time-modal.php'); ?>
    <?= $this->render('modals/order-request-tracking-number.php'); ?>
    <?= $this->render('modals/config-postal-label.php'); ?>
</div>