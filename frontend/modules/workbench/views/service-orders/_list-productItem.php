<?php

use common\models\DeliveryType;
use common\models\message\helpers\UrlHelper;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use common\models\StorePricer;
use common\models\UserAddress;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\user\UserFacade;
use frontend\widgets\ReviewStarsStaticWidget;
use frontend\widgets\SiteHelpWidget;
use lib\delivery\delivery\models\DeliveryPostalLabel;
use lib\money\Currency;
use \yii\helpers\Html;
use \frontend\models\ps\StoreOrderAttemptProcess;
use \frontend\models\ps\PsFacade;
use yii\helpers\Url;


$cancelledInNew = $attempt->isCancelled() && $form->statusGroup == StoreOrderAttemp::STATUS_NEW;
$deliveryDetail = $attempt->order->getOrderDeliveryDetails();
$isOffer = $attempt->is_offer;
$order = $attempt->order;
$object = StoreOrderAttemptProcess::create($attempt);


$cancelledTextFn = function (StoreOrderAttemp $attemp) {
    if (!$attemp->isCancelled()) {
        return null;
    }

    $text = '';
    if ($attemp->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_PS) {
        $text.=_t('site.ps', 'Canceled by you at ');
    } elseif ($attemp->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_CLIENT) {
        $text.= _t('site.ps', 'Canceled by customer at ');
    } elseif ($attemp->cancel_initiator == ChangeOrderStatusEvent::INITIATOR_MODERATOR) {
        $text.= _t('site.ps', 'Canceled by moderator at ');
    } elseif ($attemp->isExpired()) {
        $text.= _t('site.ps', 'Expired time period.') . '<br>' .
            _t(
                'site.ps',
                'This order was offered to you and several other print services on a first come first serve basis. As another print service has accepted the order, it is now expired and no longer available. At '
            );
    } else {
        $text .= _t('site.ps', 'Canceled at ');
    }
    return $text . app()->formatter->asDatetime($attemp->cancelled_at);
};

?>

<div class="panel panel-default box-shadow border-0">
    <div class="one-print-request data-order-id="<?= $attempt->id; ?>">
    <div class="panel-body">
        <?php if ($order->is_dispute_open): ?>
            <div class="alert alert-warning">
                <?= _t('site.order','Order is in dispute. Please contact the {link}',
                    ['link' => Html::a(_t('site.order', 'customer'), UrlHelper::objectMessageRoute($order))]); ?></div>
        <?php endif; ?>
        <?php if ($deadlineTimer) {
            echo sprintf('<div class="row m-l10 m-b10"><strong>%s</strong>: %s</div>',
                $deadlineTimer->getTitle(),
                \frontend\widgets\Countdown::widget([
                    'id'              => 'countdownattemp-' . $attempt->id,
                    'options'         => ['class' => 'label label-info'],
                    'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                    'timeout'         => $deadlineTimer->getTimeout(),
                    'finishedMessage' => _t('site.ps', '00:00:00')
                ]
            ));
        } ?>
        <div class="one-print-request__user">
            <div class="one-print-request__user__avatar">
                <?= frontend\components\UserUtils::getAvatarForUser($order->user, 'print-request'); ?>
            </div>

            <?php if (!$cancelledInNew): ?>
                <div class="dropdown one-print-request__user__message">
                    <button class="btn btn-default btn-circle dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown">
                        <span class="tsi tsi-message"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                        <?php if ($object->canRequestMoreTime()): ?>
                            <li>
                                <a data-date="<?= $attempt->dates->plan_printed_at ?>" ng-click="requestMoreTimeModal(<?= $attempt->id ?>, $event)"
                                   href="#" class="request-more-time-link"><?= _t('site.ps', 'Request more time'); ?></a>
                            </li>
                        <?php endif ?>
                        <?php if (!$order->user->equals($attempt->ps->user) && !$attempt->isCancelled() && !$order->isAnonim()): ?>
                            <li>
                                <?= Html::a(_t('site.ps', 'Contact customer'), UrlHelper::objectMessageRoute($order), ['target' => '_blank']); ?>
                            </li>
                        <?php endif ?>
                        <li>
                            <?= Html::a(_t('site.ps', 'Contact support'), UrlHelper::supportMessageRoute($order), ['target' => '_blank']); ?>
                        </li>
                    </ul>
                </div>

            <?php endif; ?>

            <div class="one-print-request__user__info">
                <?php if ($cancelledInNew || $order->isAnonim() || empty($order->user->email)): ?>
                    <?= H(UserFacade::getFormattedUserName($order->user)); ?>
                <?php else: ?>
                    <a href="<?= Url::toRoute(UrlHelper::objectMessageRoute($order)) ?>"><?= H(
                            UserFacade::getFormattedUserName($order->user)
                        ); ?></a>
                <?php endif; ?>
                <br>
                    <a href="/workbench/service-order/view?id=<?= $attempt->order->id; ?>">
                        <b><?= _t("site.ps", "Order #"); ?><?= $attempt->order->id; ?></b>
                    </a>
            </div>

        </div>

        <div class="one-print-request__btns">
            <?php if ($object->canAcceptOrder()): ?>
                <button loader-click="setOrderStatus(<?= $attempt->id ?>, '<?= StoreOrderAttemp::STATUS_ACCEPTED; ?>')" class="btn btn-success">
                    <?= _t("site.ps", "Accept order"); ?>
                </button>
            <?php endif ?>

            <?php if ($object->canDeclineOrder()) : ?>
                <button
                        ng-click="declineOrder(<?= $attempt->id ?>)"
                        class="btn btn-warning">
                    <?= _t('front.user', 'Decline'); ?>
                </button>

            <?php endif; ?>
            <?php if ($object->canPrintOrder()): ?>
                <?= Html::a(
                    _t('site.ps', 'Order Details'),
                    ['/workbench/service-order/print', 'attempId' => $attempt->id],
                    ['class' => 'btn btn-default']
                ) ?>

            <?php endif ?>
            <?php if ($object->canSetAsSentWithTrackingNumber()):  ?>
                <button loader-click="setOrderSentWithTrackingNumber(<?= $attempt->id ?>)" class="btn btn-success">
                    <?= _t('site.ps', 'Set as shipped'); ?>
                </button>
            <?php elseif ($object->canSetAsSent()) :
                $isPickup = \lib\delivery\delivery\DeliveryFacade::isPickupDelivery($attempt->order);
                $setAsShipped = _t('site.ps', 'Set as shipped');
                if ($isPickup) {
                    $setAsShipped = _t('site.ps', 'Set as picked up');
                }
                ?>
                <button loader-click="setOrderStatus(<?= $attempt->id ?>, '<?= StoreOrderAttemp::STATUS_SENT; ?>')" class="btn btn-success">
                    <?= $setAsShipped; ?>
                </button>
            <?php endif; ?>
            <?php if ($object->canShowReceivedMessage()) { ?>
                <div class="one-print-request__btns-text"><?= _t('site.ps', 'Received on'); ?>
                    <?= app('formatter')->asDatetime($attempt->dates->received_at, 'short'); ?></div>
            <?php } ?>

            <?php if ($object->canShowShippedMessage()) { ?>
                <div class="one-print-request__btns-text"><?= _t('site.ps', 'Dispatched on'); ?>
                    <?= app('formatter')->asDatetime($attempt->dates->shipped_at, 'short'); ?></div>
            <?php } ?>

            <?php if ($object->canShowCancelledMessage()) { ?>
                <div class="one-print-request__btns-text"><?= $cancelledTextFn($attempt) ?></div>
            <?php } ?>

            <div>
                <?php
                if ($order->currentAttemp && $attempt->id === $order->currentAttemp->id && $deliveryDetail && !empty($deliveryDetail['tracking_number'])) {

                    $trackingNumber = !$cancelledInNew && DeliveryPostalLabel::checkPostalLabelExists($attempt)
                        ? Html::a(
                            '<b>' . $deliveryDetail['tracking_number'] . '</b>',
                            '/workbench/service-order/postal-image?attempId=' . $attempt->id,
                            ['target' => '_blank']
                        )
                        : '<b>' . $deliveryDetail['tracking_number'] . '</b>';
                    if ($attempt->isTrackingNumberVisible()) {
                        echo _t(
                            'site.ps',
                            'Tracking number {tracking_number}, {tracking_shipper}',
                            [
                                'tracking_number'  => $trackingNumber,
                                'tracking_shipper' => $deliveryDetail['tracking_shipper'],
                            ]
                        );
                    }
                }
                ?>
            </div>



            <?php

            $review = \common\models\StoreOrderReview::find()
                ->forAttemp($attempt)
                ->forPs($attempt->ps)
                ->isCanShow()
                ->one();

            if ($review) {
                $stars = ReviewStarsStaticWidget::widget(['rating' => $review->getRating()]);
                echo Html::a($stars, ['/workbench/reviews']);
            }
            ?>


        </div>
    </div>
    <?php foreach ($order->storeOrderItems as $oneStoreItem) {
            $product = $oneStoreItem->product;
    ?>
        <div class="panel-body one-print-request__body">
            <div class="row">
                <h3 class="m-l20"><?=H($oneStoreItem->product->title); ?></h3>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">

                            <?php
                            if ($product->getCoverUrl()) {
                                echo '<img src="' . ImageHtmlHelper::getThumbUrl($product->getCoverUrl(), 200,200) . '" alt="' . $product->getTitle() . '">';
                            }
                            ?>

                        </div>


                        <div class="col-sm-6">
                            <table class="one-print-request__table">
                                <tr>
                                    <td class="one-print-request__table__label">
                                        <?=_t('site.order', 'Quantity');?>
                                    </td>
                                    <td>
                                        <?=$oneStoreItem->qty;?>
                                    </td>
                                </tr>
                                <?php
                                $priceForPs = StorePricer::getProductPrice($attempt->order_id);
                                if ($priceForPs && $priceForPs->price > 0) :
                                    ?>

                                    <tr>
                                        <?php
                                        $psFee = StorePricer::getPsFee($attempt->order_id);
                                        ?>
                                        <td class="one-print-request__table__label">

                                            <?php
                                            if ($psFee) {
                                                echo SiteHelpWidget::widget(
                                                    [
                                                        'triggerHtml' => _t('site.ps', 'Product price'),
                                                        'title'       => _t('site.ps', 'How much will I receive from the product price?'),
                                                        'alias'       => 'help.supplier.product.price'
                                                    ]
                                                );
                                            }
                                            ?></nobr>
                                        </td>
                                        <td>
                                            <?php
                                            $priceForPs = StorePricer::getProductPriceForPs($attempt->order_id);

                                            $price = $priceForPs
                                                ? displayAsCurrency($priceForPs->price, $priceForPs->currency)
                                                : displayAsCurrency(0, Currency::USD);

                                            echo $price;

                                            if ($refundService->canRefundOrderTransaction($attempt->order)) {
                                                echo '<br />';
                                                echo Html::a(
                                                    _t('site.order', 'Partial Refund'),
                                                    ['/store/payment/refund-request', 'id' => $attempt->order_id],
                                                    [
                                                        'title'      => _t('site.order', 'Offer Partial Refund'),
                                                        'class'      => 'btn btn-sm btn-link ts-ajax-modal',
                                                        'style'      => 'margin-left: 0px; padding-left: 0px',
                                                        'jsCallback' => 'window.location.reload()',
                                                    ]
                                                );
                                            } else {
                                                $refundsReuested = $refundService->getRefundRequestByOrder($attempt->order);
                                                if ($refundsReuested) {
                                                    foreach ($refundsReuested as $refundRequested):
                                                        echo '<br /><span class="label label-warning">';
                                                        if ($refundRequested->isApproved()) {
                                                            echo _t(
                                                                'site.order',
                                                                'Refund Approved: {amount}',
                                                                ['amount' => displayAsMoney($refundRequested->getMoneyAmount())]
                                                            );
                                                        } else {
                                                            echo _t(
                                                                'site.order',
                                                                'Refund submitted: {amount}',
                                                                ['amount' => displayAsMoney($refundRequested->getMoneyAmount())]
                                                            );
                                                        }
                                                        echo '</span>';

                                                    endforeach;;
                                                } else {
                                                    echo sprintf(
                                                        '<br/><nobr>%s <span data-toggle="tooltip" data-placement="top" data-original-title="%s" class="tsi tsi-warning-c"></span></nobr>',
                                                        _t('site.order', 'Partial refund unavailable'),
                                                        $refundService->lastErrorMessage
                                                    );

                                                }
                                                echo ' ' . frontend\widgets\SiteHelpWidget::widget(
                                                        [
                                                            'alias' => 'ps.order.partialRefund'
                                                        ]
                                                    ) . ' ';
                                            }

                                            ?>

                                            <br/>

                                        </td>
                                    </tr>

                                <?php endif; ?>

                                <?php
                                $addonPrice = StorePricer::getAddonPrice($order->id);
                                if (!empty($addonPrice)):
                                    $addonServices = $order->storeOrderPositions;
                                    foreach ($addonServices as $addonService):
                                        if ($addonService->status != StoreOrderPosition::STATUS_PAID) {
                                            continue;
                                        }
                                        ?>
                                        <tr>
                                            <td class="one-print-request__table__label">

                                                <?= _t('site.order', 'Additional service'); ?>
                                                <span data-toggle="tooltip" data-placement="bottom" data-original-title="<?= H($addonService->title); ?>"
                                                      title="<?= H($addonService->title); ?>" class="tsi tsi-warning-c"></span>
                                            </td>
                                            <td><?php
                                                echo displayAsCurrency($addonService->amount, $addonService->currency_iso); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                                <?php if ($order->hasDelivery()) : ?>

                                    <tr>
                                        <td class="one-print-request__table__label">
                                            <?= _t('site.ps', 'Delivery'); ?>
                                        </td>
                                        <td>
                                            <?php if ($attempt->machine && $attempt->machine->asDeliveryParams()->isFreeDelivery($order)) {
                                                echo _t('site.ps', 'Free Delivery');
                                            } else {
                                                if ($deliveryDetail && $deliveryDetail['carrier'] === common\models\DeliveryType::CARRIER_MYSELF) {
                                                    $price = StorePricer::getShippingPrice($order->id);
                                                    if ($price) {
                                                        echo _t(
                                                            'site.ps',
                                                            '{price} Shipping price',
                                                            ['price' => displayAsCurrency($price->price, $price->currency)]
                                                        );
                                                    }
                                                }
                                            }

                                            ?>
                                            <div>
                                                <?= PsFacade::getDeliveryTypesIntl($order->deliveryType->code); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $packagePrice = StorePricer::getPackagePrice($attempt->order_id);
                                    if ($packagePrice && $packagePrice->price):
                                        ?>

                                        <tr>
                                            <td class="one-print-request__table__label">
                                                <?= _t('site.ps', 'Packaging fee'); ?>
                                            </td>
                                            <td>
                                                <?= displayAsCurrency($packagePrice->price, $packagePrice->currency); ?>
                                            </td>
                                        </tr>

                                    <?php endif; ?>





                                    <tr>
                                    <?php
                                    $locationLink = _t('site.ps', 'Location');
                                    if ($order->deliveryType->code === DeliveryType::PICKUP) {
                                        $printerAddress = UserAddress::getAddressFromLocation($attempt->machine->location);
                                        $address = UserAddress::formatAddress($printerAddress, true);
                                    } else {
                                        if (!$attempt->isShippingAddressVisible()) {
                                            $order->shipAddress->address = '';
                                            $order->shipAddress->contact_name = '';
                                            $order->shipAddress->zip_code = '';
                                            $order->shipAddress->phone = '';

                                            if ($attempt->status !== StoreOrderAttemp::STATUS_DELIVERED &&
                                                $attempt->status !== StoreOrderAttemp::STATUS_RECEIVED &&
                                                !$attempt->isCancelled()) {
                                                $locationLink = frontend\widgets\SiteHelpWidget::widget(
                                                        [
                                                            'triggerHtml' => _t('site.ps', 'Location'),
                                                            'alias'       => 'ps.order.shippingAddress'
                                                        ]
                                                    ) . ' ';
                                            }

                                        }
                                        $address = UserAddress::formatAddress($order->shipAddress, true);
                                        $address = str_replace(
                                            "<br />
                                                        <br />",
                                            "<br/>",
                                            $address
                                        );
                                    }
                                    ?>
                                    <td class="one-print-request__table__label">
                                        <?= $locationLink; ?>
                                    </td>
                                    <td>

                                        <?php


                                        echo $address;

                                        ?>
                                    </td>
                                    </tr><?php endif; ?>
                            </table>
                        </div>
                    </div>

                    <?php if ($attempt->order->comment): ?>
                        <h4 class="m-t0 m-b0"><?= _t('site.order', 'Customer comment'); ?></h4>
                        <?= nl2br(H($attempt->order->comment)); ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    <?php } ?>

    <!--- additional services -->
    <?php
    echo $this->render('list-item-additional-services.php', ['attemp' => $attempt, 'order' => $attempt->order]);
    ?>


</div>
</div>

