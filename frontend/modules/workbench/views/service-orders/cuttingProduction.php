<?php

use common\components\serizaliators\porperties\PrintAttempProperties;
use common\models\message\helpers\UrlHelper;
use common\models\StoreOrderAttemp;
use frontend\models\ps\DeclineCncPreorderByCustomerForm;
use frontend\models\ps\DeclineCncPreorderByPsForm;
use frontend\models\ps\DeclineOrderForm;
use frontend\models\ps\DeclineProductOrderForm;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\preorder\components\ProductPreorderDeclineForm;
use frontend\modules\preorder\components\ServicesPreorderDeclineForm;
use frontend\modules\workbench\components\OrderServiceUrlHelper;
use frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite;
use frontend\modules\workbench\models\view\StoreOrderAttempViewModel;
use frontend\modules\workbench\widgets\CompanySalesSidebarWidget;
use frontend\widgets\Countdown;
use yii\helpers\Html;
use yii\web\View;

/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 14.09.18
 * Time: 13:35
 *
 * @var View $this
 */
/** @var \common\models\User $currentUser */
/** @var StoreOrderAttemp $attempt */

$attemptView          = StoreOrderAttempViewModel::fill($attempt);
$attemptProcess       = StoreOrderAttemptProcess::create($attempt);
$attemptViewComposite = StoreOrderAttemptViewComposite::create($attempt, $attemptView, $attemptProcess);

$this->registerAssetBundle(\frontend\assets\DatetimePickerAsset::class);
$this->registerAssetBundle(\frontend\assets\DropzoneAsset::class);

Yii::$app->angular
    ->service(['router', 'notify', 'modal', 'user', 'psDownloadModel'])
    ->directive(['dropzone-button', 'image-rotate', 'datetime-picker'])
    ->controller('order/store-order-service')
    ->controller('ps/orders/print')
    ->controller('ps/orders/offerPartialRefund')
    ->constants([
        'declineReasons'                    => DeclineOrderForm::getSuggestObjectList(),
        'preorderDeclineReasons'            => ServicesPreorderDeclineForm::getSuggestObjectList(),
        'cncPreorderDeclineReasons'         => DeclineCncPreorderByPsForm::getSuggestObjectList(),
        'cncPreorderDeclineCustomerReasons' => DeclineCncPreorderByCustomerForm::getSuggestObjectList(),
        'servicesPreorderDeclineReasons'    => ServicesPreorderDeclineForm::getSuggestObjectList(),
        'productPreorderDeclineReasons'     => ProductPreorderDeclineForm::getSuggestObjectList(),
        'declineProductReasons'             => DeclineProductOrderForm::getSuggestObjectList(),
    ])
    ->controllerParams([
            'ps'                      => $attempt->ps,
            'certificationUrl'        => !$attempt->machine || $attempt->machine->isCnc() || $attempt->machine->isCutting() ? null : '/mybusiness/services/' . $attempt->machine->asPrinter()->id . '/edit-printer/' . $attempt->machine->asPrinter()->id . '/test',
            'attemp'                  => PrintAttempProperties::serialize($attempt),
        ]
    );

$this->title = _t('site.my', 'In production');

?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(); ?>

<div class="container">
    <div class="row ps-index">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="row">
                    <?= CompanySalesSidebarWidget::widget(['ps' => $attempt->company, 'activeStatusGroup' => StoreOrderAttemp::STATUS_ACCEPTED]) ?>
                    <div class="col-sm-8 wide-padding wide-padding--left">
                        <div class="panel panel-default box-shadow border-0" ng-controller="PsPrintController">
                            <div class="one-print-request">
                                <div class="panel-body">
                                    <?php if ($attempt->order->is_dispute_open): ?>
                                        <div class="alert alert-warning">
                                            <?php echo _t('site.order', 'Order in dispute. Please contact {link}', [
                                                'link' => Html::a(_t('site.order', 'customer'), UrlHelper::objectMessageRoute($attempt->order))
                                            ]); ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if ($deadlineTimer = $attempt->getDeadline()) : ?>
                                        <div class="row " style="margin:-5px 0 10px;">
                                            <strong><?php echo $deadlineTimer->getTitle(); ?></strong>
                                            <?php echo Countdown::widget([
                                                'id'              => 'countdownattemp-' . $attempt->id,
                                                'options'         => ['class' => 'label label-info'],
                                                'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                                                'timeout'         => $deadlineTimer->getTimeout(),
                                                'finishedMessage' => _t('site.ps', '00:00:00'),
                                            ]); ?>
                                        </div>
                                    <?php endif; ?>

                                    <div class="row">
                                        <div class="col-xs-12 col-md-9">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="one-print-request__user">
                                                        <div class="one-print-request__user__avatar">
                                                            <?php if ($avatar = $attemptView->getOrderUserAvatar()) : ?>
                                                                <div class="one-print-request__user__avatar">
                                                                    <?= Html::img($avatar, ['class' => 'print-request-user-img']) ?>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                        <?php $messageTopic = $attempt->order->activeMessageTopic; ?>
                                                        <div class="dropdown dropdown-order-block">
                                                            <button class="btn btn-default btn-circle dropdown-toggle dropdown-menu-right-toggle"
                                                                    type="button" id="dropdownMenu1"
                                                                    data-toggle="dropdown">
                                                                <span class="tsi tsi-message"></span>
                                                                <?php if($messageTopic && $messageTopic->getMemberForUser($currentUser)->have_unreaded_messages):?>
                                                                    <span class="one-print-request__user__message-badge"></span>
                                                                <?php endif;?>
                                                            </button>
                                                            <ul class="dropdown-menu dropdown-menu-right"
                                                                aria-labelledby="dropdownMenu1">
                                                                <?php if ($attemptProcess->canRequestMoreTime()): ?>
                                                                    <li>
                                                                        <a
                                                                                data-date="<?php echo $attempt->dates->plan_printed_at ?>"
                                                                                ng-click="requestMoreTimeModal(<?php echo $attempt->id; ?>, $event)"
                                                                                href="#"
                                                                                class="request-more-time-link"><?= _t('site.ps', 'Request more time'); ?>
                                                                        </a>
                                                                    </li>
                                                                <?php endif ?>
                                                                <?php if(!$attempt->order->user->equals($attempt->ps->user) && !$attempt->order->isCancelled() && !$attempt->order->isAnonim()):?>
                                                                    <li>
                                                                        <?= Html::a(_t('site.ps', 'Contact customer'), UrlHelper::objectMessageRoute($attempt->order), ['target' => '_blank']); ?>
                                                                    </li>
                                                                <?php endif;?>
                                                                <li>
                                                                    <?= Html::a(_t('site.ps', 'Contact support'), UrlHelper::supportMessageRoute($attempt->order), ['target' => '_blank']); ?>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <div class="one-print-request__user__info">
                                                            <b>
                                                                 <?php echo H($attemptView->getOrderUserName()); ?>
                                                            </b>
                                                            <br/>
                                                            <a href="<?php echo HL($attemptView->getUrlItem()); ?>">
                                                                <b><?php echo H($attempt->getTitleLabel()); ?></b>
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-3 text-center">
                                            <div class="one-print-request__btns one-print-request__btns--print-order"
                                                 data-order-id="<?= $attempt->id; ?>">
                                                <a href="<?= OrderServiceUrlHelper::viewList(StoreOrderAttemp::STATUS_ACCEPTED); ?>"
                                                   class="btn btn-primary btn-block">
                                                    <span class="tsi tsi-arrow-back-l"></span> <?= _t('site.ps', 'Back'); ?>
                                                </a>
                                            </div>
                                            <?php if ($attemptProcess->canSetAsPrinted()): ?>
                                                <button loader-click="setAsReady()"
                                                        class="btn btn-success btn-block p-l0 p-r0 js-set-as-printed">
                                                    <?= _t("site.ps", "Ready"); ?>
                                                </button>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>

                                <?php echo $this->renderFile('@frontend/modules/workbench/views/service-orders/partial/order/item-cutting.php', [
                                    'attemptViewComposite'     => $attemptViewComposite,
                                ]); ?>

                                <div class="panel-body one-print-request__steps">
                                    <div class="row">
                                        <div ng-cloak class="col-sm-12 one-print-request__steps-uploaded">
                                            <div class="one-print-request__steps-pic-list">
                                                <div ng-repeat="moderationFile in attemp.moderation.files"
                                                     class="one-print-request__steps-pic">

                                                    <div class="one-print-request__steps-pic__loader"
                                                         ng-if="moderationFile.isLoading">
                                                        <img src="/static/images/preloader.gif" width="40" height="40">
                                                    </div>

                                                    <div ng-if="!moderationFile.isLoading">
                                                        <div image-rotate-ajax file-uuid="{{moderationFile.fileUuid}}"
                                                             max-size-w="160" max-size-H="120">
                                                            <button
                                                                    ng-click="deleteModerationFile(moderationFile)"
                                                                    type="button"
                                                                    class="one-print-request__steps-pic-delete"
                                                                    title="<?= _t("site.ps", "Delete picture"); ?>">
                                                                &times;
                                                            </button>

                                                            <button ng-show="!moderationFile.isVideo" type="button"
                                                                    data-image-rotate-flag="button"
                                                                    class="one-print-request__steps-pic-rotate"
                                                                    title="<?= _t("site.ps", "Rotate picture"); ?>">
                                                                <span class="tsi tsi-repeat"></span>
                                                            </button>

                                                            <div ng-if="moderationFile.isVideo">
                                                                <video width="100" height="100" controls
                                                                       preload="metadata">
                                                                    <source ng-src="{{moderationFile.imgUrl | url}}"
                                                                            type="video/mp4">
                                                                </video>
                                                            </div>

                                                            <div ng-if="!moderationFile.isVideo">
                                                                <img data-image-rotate-flag="img"
                                                                     ng-src="{{moderationFile.imgUrl}}" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="m-b10 m-t10">
                                                <button
                                                        dropzone-button
                                                        accepted-files="image/*,video/*,.png,.jpg,.jpeg,.gif,.mp4,.m4a,.mkv,.webm,.ogg,.mov,.3gp"
                                                        max-files="50"
                                                        on-accept="onAddResultFile(file)"
                                                        class="btn btn-default js-dropzone-button-click"
                                                        type="button">
                                                    <span class="tsi  tsi-plus-c"
                                                          style="font-size: 36px; color: #2d8ee0;"></span><br/>
                                                    <span style="color: #2d8ee0;"><?= _t('site.ps', 'Upload results') ?></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if ($attempt->preorder): ?>
                                <div class="panel-body one-print-request__body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4 class="m-t0"><?= _t('site.ps', 'Project Description'); ?></h4>
                                            <p><?= H($attempt->preorder->offer_description) ?></p>
                                        </div>

                                        <div class="col-md-6">
                                            <h4 class="m-t0"><?= _t('site.ps', 'Deadline'); ?></h4>
                                            <p><?= _t('site.ps', '{time} Days', ['time' => $attempt->preorder->offer_estimate_time]); ?></p>
                                        </div>
                                    </div>

                                    <?php if ($attempt->preorder->files): ?>
                                        <h4 class="m-t0"><?= _t('site.ps', 'Attached Files'); ?></h4>
                                    <?php endif; ?>

                                    <?php foreach ($attempt->preorder->files as $file): ?>
                                        <div class="simple-file-link">
                                            <a class="simple-file-link__link"
                                               href="<?= PreorderUrlHelper::downloadPreorderFile($attempt->preorder, $file) ?>">
                                                <span><?= H($file->name); ?></span>
                                                <i><?= \Yii::$app->formatter->asShortSize($file->size, 2); ?></i>
                                            </a>
                                        </div>
                                    <?php endforeach; ?>

                                    <?php echo $this->render('@frontend/modules/workbench/views/preorder/preorder-invoice-table.php', [
                                        'preorder'            => $attempt->preorder,
                                        'showTotal'           => true,
                                        'calculateAndShowFee' => false
                                    ]); ?>
                                    <?php endif; ?>
                                </div>

                                <div class="panel-body one-print-request__footer">
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="send-print-request-date">
                                                <?= _t('site.app', 'Created at'); ?>
                                                : <?php echo app('formatter')->asDatetime($attempt->order->created_at); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php echo $this->render('@frontend/modules/workbench/views/service-orders/list-item-additional-services.php', [
                                    'attemptViewComposite' => $attemptViewComposite
                                ]); ?>

                                <?php echo $this->render('@frontend/modules/workbench/views/service-orders/print-request-footer.php', [
                                    'attemptViewComposite' => $attemptViewComposite
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render('modals/download-model-terms-modal') ?>
<?= $this->render('modals/reuqest-more-time-modal.php'); ?>

