<?php

use common\components\serizaliators\porperties\PrintAttempProperties;
use common\models\message\helpers\UrlHelper;
use common\models\model3d\PrintInstructionParser;
use common\models\StoreOrderAttemp;
use frontend\components\StoreUnitUtils;
use frontend\components\UserUtils;
use frontend\models\ps\DeclineCncPreorderByCustomerForm;
use frontend\models\ps\DeclineCncPreorderByPsForm;
use frontend\models\ps\DeclineOrderForm;
use frontend\models\ps\DeclineProductOrderForm;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\models\user\UserFacade;
use frontend\modules\preorder\components\ProductPreorderDeclineForm;
use frontend\modules\preorder\components\ServicesPreorderDeclineForm;
use frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite;
use frontend\modules\workbench\models\view\StoreOrderAttempViewModel;
use frontend\modules\workbench\widgets\CompanySalesSidebarWidget;
use frontend\widgets\PrintInstructionWidget;
use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var StoreOrderAttemp $attempt */
/** @var \common\models\User $currentUser */


$attemptView          = StoreOrderAttempViewModel::fill($attempt);
$attemptProcess       = StoreOrderAttemptProcess::create($attempt);
$attemptViewComposite = StoreOrderAttemptViewComposite::create($attempt, $attemptView, $attemptProcess);
$oneOrder             = $attempt->order;

$this->registerAssetBundle(\frontend\assets\DatetimePickerAsset::class);
$this->registerAssetBundle(\frontend\assets\DropzoneAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

Yii::$app->angular
    ->service(['router', 'notify', 'modal', 'user', 'psDownloadModel'])
    ->directive(['dropzone-button', 'image-rotate', 'datetime-picker'])
    ->controller('order/store-order-service')
    ->controller('ps/orders/print')
    ->controller('ps/orders/offerPartialRefund')
    ->constants([
        'declineReasons'                    => DeclineOrderForm::getSuggestObjectList(),
        'preorderDeclineReasons'            => ServicesPreorderDeclineForm::getSuggestObjectList(),
        'cncPreorderDeclineReasons'         => DeclineCncPreorderByPsForm::getSuggestObjectList(),
        'cncPreorderDeclineCustomerReasons' => DeclineCncPreorderByCustomerForm::getSuggestObjectList(),
        'servicesPreorderDeclineReasons'    => ServicesPreorderDeclineForm::getSuggestObjectList(),
        'productPreorderDeclineReasons'     => ProductPreorderDeclineForm::getSuggestObjectList(),
        'declineProductReasons'             => DeclineProductOrderForm::getSuggestObjectList(),
    ])
    ->controllerParams(
        [
            'company'          => $attempt->ps,
            'certificationUrl' => !$attempt->machine || $attempt->machine->isCnc() ? null : '/mybusiness/services/' . $attempt->machine->asPrinter()->id . '/edit-printer/' . $attempt->machine->asPrinter()->id . '/test',
            'attemp'           => PrintAttempProperties::serialize($attempt),
        ]
    );

$this->title = _t('site.my', 'Printer Requests');

echo $this->renderFile('@frontend/modules/mybusiness/views/company/templates/order-decline-modal.php');
?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(); ?>

<div class="container">
    <div class="row ps-index">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="row">
                    <?= CompanySalesSidebarWidget::widget(['ps' => $attempt->company, 'activeStatusGroup' => StoreOrderAttemp::STATUS_ACCEPTED]) ?>
                    <div class="col-sm-9 wide-padding wide-padding--left">
                        <div class="panel panel-default box-shadow border-0" ng-controller="PsPrintController">
                            <?php foreach ($oneOrder->storeOrderItems as $oneStoreItem) : ?>
                                <div class="one-print-request">
                                    <div class="panel-body">
                                        <?php if ($oneOrder->is_dispute_open) : ?>
                                            <div class="alert alert-warning"><?= _t('site.order', 'Order in dispute. Please contact {link}',
                                                    [
                                                        'link' => Html::a(_t('site.order', 'customer'), UrlHelper::objectMessageRoute($oneOrder))
                                                    ]); ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php
                                        if ($deadlineTimer = $attempt->getDeadlineCompany()) {
                                            echo '  <div class="row " style="margin:-5px 0 10px;"><strong>';
                                            if ($deadlineTimer->getLabelString()) {
                                                echo $deadlineTimer->getLabelString().'</strong>';
                                            } else {
                                                echo $deadlineTimer->getTitle() . ':</strong> ';
                                                echo \frontend\widgets\Countdown::widget([
                                                    'id'              => 'countdownattemp-' . $attempt->id,
                                                    'options'         => ['class' => 'label label-info'],
                                                    'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                                                    'timeout'         => $deadlineTimer->getTimeout(),
                                                    'finishedMessage' => _t('site.ps', '00:00:00')
                                                ]);
                                            }
                                            echo '</div>';
                                        }
                                        ?>

                                        <div class="row">
                                            <div class="col-xs-12 col-md-9">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="one-print-request__user">
                                                            <div class="one-print-request__user__avatar">
                                                                <?php $possibleUser = $oneOrder->user; ?>

                                                                <?php if ($possibleUser->company): ?>
                                                                    <?= Html::img($possibleUser->company->getCompanyLogoOrDefault(false, 64)) ?>
                                                                <?php else: ?>
                                                                    <?= UserUtils::getAvatarForUser($possibleUser, 'message'); ?>
                                                                <?php endif; ?>
                                                            </div>
                                                            <?php $messageTopic = $attempt->order->activeMessageTopic; ?>
                                                            <div class="dropdown one-print-request__user__message">
                                                                <button class="btn btn-default btn-circle dropdown-toggle dropdown-menu-right-toggle"
                                                                        type="button" id="dropdownMenu1" data-toggle="dropdown">
                                                                    <span class="tsi tsi-message"></span>
                                                                    <?php if ($messageTopic && $messageTopic->getIsMember($currentUser) && $messageTopic->getMemberForUser($currentUser)->have_unreaded_messages): ?>
                                                                        <span class="one-print-request__user__message-badge"></span>
                                                                    <?php endif; ?>
                                                                </button>
                                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">

                                                                    <?php if (!$oneOrder->user->equals($attempt->ps->user) && !$oneOrder->isCancelled() && !$oneOrder->isAnonim()
                                                                    ): ?>
                                                                        <li>
                                                                            <?= Html::a(_t('site.ps', 'Contact customer'), UrlHelper::objectMessageRoute($oneOrder), ['target' => '_blank']); ?>
                                                                        </li>
                                                                    <?php endif ?>
                                                                    <?php if ($attemptProcess->canRequestMoreTime()): ?>
                                                                        <li>
                                                                            <a
                                                                                    data-date="<?php echo $attempt->dates->plan_printed_at ?>"
                                                                                    ng-click="requestMoreTimeModal(<?php echo $attempt->id; ?>, $event)"
                                                                                    href="#"
                                                                                    class="request-more-time-link"><?= _t('site.ps', 'Request more time'); ?>
                                                                            </a>
                                                                        </li>
                                                                    <?php endif ?>
                                                                    <li>
                                                                        <?= Html::a(_t('site.ps', 'Contact support'), UrlHelper::supportMessageRoute($oneOrder), ['target' => '_blank']); ?>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            <div class="one-print-request__user__info">
                                                                <b>
                                                                    <?php $possibleUser = $oneOrder->user; ?>

                                                                    <?php if ($possibleUser->company): ?>
                                                                        <?= H($possibleUser->company->title); ?>
                                                                    <?php else: ?>
                                                                        <?= H(UserFacade::getFormattedUserName($possibleUser)); ?>
                                                                    <?php endif; ?>
                                                                </b>
                                                                <br/>
                                                                <div class="">
                                                                    <?= _t('site.ps', ' Order #{order_id}', ['order_id' => $oneOrder->id]); ?>
                                                                    <?php if ($oneOrder->isForPreorder()): ?>
                                                                        (<?= _t('site.order', 'from Quote #{preorderId}', ['preorderId' => $oneOrder->preorder_id]); ?>)
                                                                    <?php endif; ?>
                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>


                                                    <?php if ($oneStoreItem->hasModel()) : ?>

                                                        <div class="col-xs-12 col-sm-12">
                                                            <div class="one-print-request__header-table">
                                                                <table class="one-print-request__table one-print-request__table--print-order">
                                                                    <tr>
                                                                        <td class="one-print-request__table__label">
                                                                            <?= _t('site.ps', 'Model'); ?>:
                                                                        </td>
                                                                        <td>
                                                                            <strong>
                                                                                <?= \H($oneStoreItem->unit->model3d->title); ?>
                                                                            </strong>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                    if ($printInstruction = PrintInstructionParser::instance()->parse($oneStoreItem->model3dReplica->print_instructions)) {
                                                                        ?>
                                                                        <tr>
                                                                            <td class="one-print-request__table__label">
                                                                                <?= _t('site.ps', 'Print instructions'); ?>:
                                                                            </td>
                                                                            <td><br/>
                                                                                <?= StoreUnitUtils::displayMoreHtml(PrintInstructionWidget::widget(['model'  => $printInstruction,
                                                                                                                                                    'format' => PrintInstructionWidget::FORMAT_TEXT
                                                                                ])) ?>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <tr>
                                                                        <td class="one-print-request__table__label">
                                                                            <?= _t('site.ps', 'Printer'); ?>:
                                                                        </td>
                                                                        <td>
                                                                            <?= \H($attempt->machine->getTitleLabel()); ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>

                                                    <?php endif; ?>

                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-3 text-center">

                                                <div class="one-print-request__btns">
                                                    <?php if ($attemptProcess->canSetAsPrinted()): ?>
                                                        <button
                                                                loader-click="setAsPrinted()"
                                                                class="btn btn-success js-set-as-printed">
                                                            <?= _t("site.ps", "Submit results"); ?>
                                                        </button>
                                                    <?php endif ?>

                                                    <?php if ($attemptProcess->canDeclineOrder()) : ?>
                                                        <button
                                                                ng-click="declineOrderByType(<?php echo $attempt->id ?>, '<?php echo $attempt->order->getOrderType(); ?>')"
                                                                class="btn btn-warning btn-sm">
                                                            <?= _t('front.user', 'Decline'); ?>
                                                        </button>
                                                    <?php endif; ?>
                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-body one-print-request__steps">
                                        <div class="row">

                                            <?php if ($oneStoreItem->hasModel()): ?>

                                                <div class="col-sm-4 one-print-request__steps-item  one-print-request__steps-item--success">
                                                    <h4 class="one-print-request__steps-title">
                                                        <div class="one-print-request__steps-count one-print-request__steps-count--1">1</div>
                                                        <?= _t("site.ps", "Print Models"); ?>
                                                    </h4>
                                                    <p>
                                                        <?= _t("site.ps", "Download all files and print them on your printer"); ?>
                                                    </p>

                                                    <?php if ($attemptProcess->canDownloadModel3D()) : ?>

                                                        <button
                                                                loader-click="downloadFiles(<?= $attempt->id ?>)"
                                                                loader-click-text="<i class='tsi tsi-refresh tsi-spin-custom'></i> <?= _t('site.order', 'Preparing archive for download...') ?>"
                                                                class="btn btn-link one-print-request__steps-link"
                                                                type="button">
                                                            <span class="tsi tsi-printer3d"></span> <?= _t('site.ps', 'Download file(s)'); ?>
                                                        </button>

                                                    <?php else : ?>

                                                        <button
                                                                ng-click="offerDoCertification()"
                                                                class="btn btn-link one-print-request__steps-link"
                                                                type="button">
                                                            <span class="tsi tsi-printer3d"></span> <?= _t('site.ps', 'Download file(s)'); ?>
                                                        </button>

                                                    <?php endif ?>

                                                </div>

                                            <?php endif ?>


                                            <div class="col-sm-4 one-print-request__steps-item one-print-request__steps-item--active">
                                                <h4 class="one-print-request__steps-title">
                                                    <div class="one-print-request__steps-count"><?= $oneStoreItem->hasModel() ? 2 : 1 ?></div>
                                                    <?= _t("site.ps", "Upload results"); ?>
                                                </h4>
                                                <p>
                                                    <?= _t("site.ps", "Take photos/videos of your results and upload them."); ?>
                                                </p>

                                                <button
                                                        dropzone-button
                                                        accepted-files="image/*,video/*,.png,.jpg,.jpeg,.gif,.mp4,.m4a,.mkv,.webm,.ogg,.mov,.3gp"
                                                        max-files="50"
                                                        on-accept="onAddResultFile(file)"
                                                        class="btn btn-link one-print-request__steps-link"
                                                        type="button">
                                                    <span class="tsi tsi-upload-l"></span> <?= _t('site.ps', 'Upload results') ?>
                                                </button>

                                            </div>

                                            <div
                                                    ng-class="{'one-print-request__steps-item--active' : canSubmitResult()}"
                                                    class="col-sm-4 one-print-request__steps-item">
                                                <h4 class="one-print-request__steps-title">
                                                    <div class="one-print-request__steps-count"><?= $oneStoreItem->hasModel() ? 3 : 2 ?></div>
                                                    <?= _t("site.ps", "Submit results"); ?>
                                                </h4>
                                                <p>
                                                    <?= _t("site.ps", "Your results will be sent for moderation and made visible to the client."); ?>
                                                </p>


                                                <button
                                                        class="btn btn-link one-print-request__steps-link"
                                                        ng-disabled="!canSubmitResult()"
                                                        loader-click="setAsPrinted()"
                                                        type="button">
                                                    <span class="tsi tsi-checkmark"></span> <?= _t('site.ps', "Submit Results") ?>
                                                </button>


                                            </div>

                                            <div ng-cloak class="col-sm-12 one-print-request__steps-uploaded">

                                                <div ng-if="attemp.moderation.status == 'rejected'" class="m-t20 alert alert-warning" role="alert">
                                                    <?= _t('site.ps', 'Order results have not been accepted. Reason: {reason}.',
                                                        ['reason' => $attempt->moderation->reject_comment]) ?>
                                                </div>

                                                <div class="one-print-request__steps-pic-list">
                                                    <div ng-repeat="moderationFile in attemp.moderation.files"
                                                         class="one-print-request__steps-pic">

                                                        <div class="one-print-request__steps-pic__loader" ng-if="moderationFile.isLoading">
                                                            <img src="/static/images/preloader.gif" width="40" height="40">
                                                        </div>


                                                        <div ng-if="!moderationFile.isLoading" id="submitted-images">
                                                            <div image-rotate-ajax
                                                                 file-uuid="{{moderationFile.fileUuid}}"
                                                                 max-size-w="160"
                                                                 max-size-H="120">
                                                                <button
                                                                        ng-click="deleteModerationFile(moderationFile)"
                                                                        type="button"
                                                                        class="one-print-request__steps-pic-delete" title="<?= _t("site.ps", "Delete picture"); ?>">&times;
                                                                </button>
                                                                <button
                                                                        type="button" data-image-rotate-flag="button" class="one-print-request__steps-pic-rotate"
                                                                        title="<?= _t("site.ps", "Rotate picture"); ?>
                                                                    "><span class="tsi tsi-repeat"></span></button>
                                                                <div ng-if="moderationFile.isVideo">
                                                                    <video width="100" height="100" controls preload="metadata">
                                                                        <source ng-src="{{moderationFile.imgUrl | url}}" type="video/mp4">
                                                                    </video>
                                                                </div>
                                                                <div ng-if="!moderationFile.isVideo">
                                                                    <a ng-href="{{moderationFile.imgUrlOrigin}}" data-lightbox="submitted-images" target="_blank">
                                                                        <img data-image-rotate-flag="img" ng-src="{{moderationFile.imgUrl}}" alt="">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div
                                                        ng-if="attemp.moderation.files == 0"
                                                        class="m-b10 m-t10">
                                                    <?= _t('site.ps',
                                                        'Did you know that the photos/videos you upload for moderation are also used by your clients in their reviews? When taking photos, we recommend using a solid background & good lighting to clearly showcase the quality of your work. Try to take photos from different angles if all the details can\'t be captured in one image. If two or more parts are required to fit one another, take photos showing they fit together.') ?>
                                                </div>
                                                <div
                                                        ng-if="attemp.moderation.files == 0"
                                                        class="m-b10 m-t10">
                                                    <button
                                                            dropzone-button
                                                            accepted-files="image/*,video/*,.png,.jpg,.jpeg,.gif,.mp4,.m4a,.mkv,.webm,.ogg,.mov,.3gp"
                                                            max-files="50"
                                                            on-accept="onAddResultFile(file)"
                                                            class="btn btn-default"
                                                            type="button">
                                                        <span class="tsi  tsi-plus-c" style="font-size: 36px; color: #2d8ee0;"></span><br/>
                                                        <span style="color: #2d8ee0;"><?= _t('site.ps', 'Upload results') ?></span>
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <?php if ($oneStoreItem->hasModel()): ?>

                                        <div class="one-print-request__body">
                                            <?php $priceForPs = $attempt->order->primaryPaymentInvoice->getManufacturerAward(); ?>

                                            <div class="row m-l0 m-r0">
                                                <div class="col-sm-12">
                                                    <h4 class="m-t20 m-b0">
                                                        <?= _t(
                                                            'site.ps',
                                                            'Files for order #{order_id}', ['order_id' => $oneOrder->id]);
                                                        ?>
                                                    </h4>
                                                </div>
                                            </div>


                                            <?php
                                            $cancelledInNew = $attempt->isCancelled() && $form->statusGroup == StoreOrderAttemp::STATUS_NEW;

                                            echo $this->render('partial/order/item-model3d', [
                                                'attemptViewComposite' => $attemptViewComposite,
                                            ]); ?>

                                            <?php #echo $this->render('partial/printProductFilesInfo', ['oneStoreItem'=>$oneStoreItem,  'attemp'=>$attemp, 'oneOrder'=>$oneOrder]);?>
                                        </div>

                                    <?php endif ?>

                                    <div class="panel-body one-print-request__footer">
                                        <div class="row">

                                            <div class="col-sm-6">
                                                <div class="send-print-request-date">
                                                    <?= _t('site.app', 'Created at'); ?>: <?= app('formatter')->asDatetime($oneOrder->created_at); ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>


                            <?php endforeach ?>


                            <?php echo $this->render('list-item-additional-services.php', [
                                'attemptViewComposite' => $attemptViewComposite
                            ]); ?>

                            <?php echo $this->render('@frontend/modules/workbench/views/service-orders/print-request-footer.php', [
                                'attemptViewComposite' => $attemptViewComposite
                            ]); ?>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>

<?= $this->render('modals/download-model-terms-modal') ?>
<?= $this->render('modals/reuqest-more-time-modal.php'); ?>
