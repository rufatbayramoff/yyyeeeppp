<?php

use common\models\StoreOrderHistory;
use common\models\StoreOrderPosition;
use common\modules\payment\models\StoreOrderPositionForm;
use \yii\helpers\Html;

/** @var $attemptViewComposite \frontend\modules\workbench\models\view\StoreOrderAttemptViewComposite */

$attempt        = $attemptViewComposite->attempt;
$attemptView    = $attemptViewComposite->attemptView;
$attemptProcess = $attemptViewComposite->attemptProcess;

$order           = $attempt->order;
$orderPositionDp = StoreOrderPosition::getDataProvider(
    ['order_id' => $order->id],
    20,
    ['sort' => false]
);

?>

<div class="panel-body ">
    <?php
    if ($orderPositionDp->getTotalCount() > 0):
        ?>
        <div class="row">
            <div class="table-responsive">
                <?= \yii\grid\GridView::widget([
                    'showHeader'   => true,
                    'layout'       => '{items}',
                    'options'      => ['class' => 'add-service-table'],
                    'tableOptions' => ['class' => 'table'],
                    'showFooter'   => false,
                    'dataProvider' => $orderPositionDp,
                    'columns'      => [
                        [
                            'attribute'      => 'Additional Services',
                            'format'         => 'raw',
                            'contentOptions' => ['class' => 'add-service-table__title'],
                            'value'          => function (StoreOrderPosition $model) {
                                if (empty($model->file_ids)) {
                                    return H($model->title);
                                }
                                $files  = \common\models\File::findAll($model->file_ids);
                                $result = [];
                                foreach ($files as $k => $v) {
                                    $result[] = Html::a($v->name, $v->getFileUrl(), ['alt' => $v->name, 'target' => '_blank']);
                                }
                                return
                                    sprintf('%s <br /><small>%s %s</small>',
                                        H($model->title),
                                        _t('site.order', 'attachments: '),
                                        (implode(", ", $result))
                                    );

                            }
                        ],
                        [
                            'attribute'      => 'primary_payment_invoice_uuid',
                            'label'          => _t('site.order', 'Invoice Uuid'),
                            'contentOptions' => ['class' => 'add-service-table__status'],
                        ],
                        [
                            'attribute'      => 'status',
                            'format'         => 'raw',
                            'contentOptions' => ['class' => 'add-service-table__status'],
                            'value'          => function (StoreOrderPosition $model) {
                                if ($model->status === StoreOrderPosition::STATUS_NEW) {
                                    return _t('site.order', 'submitted');
                                }

                                if ($model->status === StoreOrderPosition::STATUS_REFUNDED) {
                                    $comment = $model->primaryPaymentInvoice->getRefundComment();
                                    return $model->status . ($comment ? '<br>' . Html::tag('span', $comment, ['class' => 'add-service-table__decline-reason']) : '');
                                }

                                $declineReason = '';
                                if ($model->status === StoreOrderPosition::STATUS_DECLINED) {
                                    $positionsHistory = StoreOrderHistory::find()
                                        ->where(['order_id' => $model->order_id, 'action_id' => StoreOrderHistory::POSITION_DECLINE])
                                        ->all();
                                    foreach ($positionsHistory as $posHistory) {
                                        if (empty($posHistory->data)) {
                                            continue;
                                        }
                                        $historyData = json_decode($posHistory->data, true);
                                        if (empty($historyData)) {
                                            continue;
                                        }
                                        if ($historyData['position_id'] == $model->id) {
                                            $declineReason = $posHistory->comment;
                                        }
                                    }
                                    return sprintf("%s <br /> <span class='add-service-table__decline-reason'>%s</span>", $model->status, \H($declineReason));
                                }
                                return $model->status;
                            }
                        ],
                        [
                            'attribute'      => 'created_at',
                            'contentOptions' => ['class' => 'add-service-table__created'],
                            'format'         => 'date',
                        ],
                        [
                            'attribute'      => 'amount',
                            'format'         => 'raw',
                            'contentOptions' => ['class' => 'add-service-table__amount'],
                            'value'          => function (StoreOrderPosition $model) {

                                if ($model->primaryPaymentInvoice) {
                                    $total         = $model->primaryPaymentInvoice->getAmountTotalWithRefund();
                                    $fee           = $model->primaryPaymentInvoice->additionalServiceAmount->getAmountFeeWithRefund();
                                    $manufacturing = $model->primaryPaymentInvoice->additionalServiceAmount->getAmountManufacturerWithRefund();
                                    $paymentFee    = $model->primaryPaymentInvoice->getAmountPaymentMethodFee();

                                    if ($paymentFee) {
                                        $text = _t('site.order',
                                            'Manufacturing fee for the customer is {total}. Payment method fee is {paymentFee} Service fee is {fee}. Therefore, your manufacturing income is {manufacturing}',
                                            [
                                                'paymentFee'    => displayAsMoney($paymentFee),
                                                'total'         => $total ? displayAsMoney($total) : 0,
                                                'fee'           => $fee ? displayAsMoney($fee) : 0,
                                                'manufacturing' => $manufacturing ? displayAsMoney($manufacturing) : 0
                                            ]);
                                    } else {
                                        $text = _t('site.order',
                                            'Manufacturing fee for the customer is {total}. Service fee is {fee}. Therefore, your manufacturing income is {manufacturing}', [
                                                'total'         => $total ? displayAsMoney($total) : 0,
                                                'fee'           => $fee ? displayAsMoney($fee) : 0,
                                                'manufacturing' => $manufacturing ? displayAsMoney($manufacturing) : 0
                                            ]);
                                    }

                                    $help = Html::tag('span', null, [
                                        'data-toggle'         => 'tooltip',
                                        'data-placement'      => 'top',
                                        'data-original-title' => $text,
                                        'class'               => 'tsi tsi-warning-c'
                                    ]);

                                    return displayAsMoney($model->primaryPaymentInvoice->getAmountTotalWithoutPaymentMethodFee()) . ' ' . $help;
                                }

                                return displayAsMoney($model->getFullPrice());
                            }
                        ],
                        [
                            'attribute'      => 'cancel',
                            'header'         => ' ',
                            'format'         => 'raw',
                            'contentOptions' => ['class' => 'add-service-table__amount'],
                            'value'          => function (StoreOrderPosition $model) {
                                if ($model->canBeCanceled()) {
                                    return '<input class="btn btn-primary btn-sm" type="button" ng-click="cancelAddonPosition(' .
                                        $model->id . ')" value="' . _t('site.order', 'Cancel') . '">';
                                }
                                return '';
                            }
                        ],
                    ],
                ]); ?>
            </div>
        </div>

    <?php endif; ?>
    <?php
    $orderPosition           = new StoreOrderPositionForm();
    $orderPosition->order_id = $order->id;
    if ($orderPosition->canAdd($attempt)) {
        ?>

        <button class="btn btn-default btn-sm" data-target="#js-addservice-<?= $order->id; ?>" data-toggle="collapse">
            <?= _t('site.ps', 'Offer Additional Service'); ?>
        </button>
        &nbsp;
        <?=
    frontend\widgets\SiteHelpWidget::widget([
        'title' => 'Help',
        'alias' => 'ps.3dprinting.additionalservices',
    ])
        ?>


        <div class="collapse " id="js-addservice-<?= $order->id; ?>">
            <?php
            $orderPositionForm = \yii\bootstrap\ActiveForm::begin([
                'action'      => ['/workbench/service-order/add-position'],
                'layout'      => 'inline',
                'options'     => ['style' => 'margin-top:15px;', 'enctype' => 'multipart/form-data'],
                'fieldConfig' => [
                    'labelOptions' => ['class' => ''],
                    'template'     => "{label}<br />{input} ",
                ]
            ]);
            $labelHelp         = frontend\widgets\SiteHelpWidget::widget(['title' => 'Help', 'alias' => 'ps.3dprinting.additionalservices.amount']);
            ?>
            <div class="hide">
                <?= $orderPositionForm->field($orderPosition, 'order_id')->hiddenInput()->label(false); ?>
            </div>
            <div class="one-print-request__add-service">
                <?= $orderPositionForm->field($orderPosition, 'title')->textInput(['class' => 'form-control input-sm']) ?>
                <?= $orderPositionForm->field($orderPosition, 'amount',
                    ['template' => '{label}&nbsp;&nbsp;' . $labelHelp . '<br /> <div class="input-group"><span class="input-group-addon">'.$attempt->company->paymentCurrency->title_original.'</span> {input}</div>'])
                    ->textInput(['size' => 3, 'autocomplete' => 'off', 'class' => 'form-control input-sm', 'onkeyup' => 'showServiceFeePrice(this, ' . $order->id . ')']) ?>
                <?= $orderPositionForm->field($orderPosition, 'currency')->hiddenInput()->label('') ?>
                <?php
                $filesFieldId = 'storeorderpositionform-files' . $order->id;
                ?>
                <div class="form-group field-storeorderpositionform-files">
                    <label class="" for="<?= $filesFieldId; ?>">Files</label>
                    <br>
                    <input type="hidden" name="StoreOrderPositionForm[files][]" value="">
                    <input type="file"
                           id="<?= $filesFieldId; ?>"
                           class="inputfile inputfile--sm"
                           name="StoreOrderPositionForm[files][]"
                           aria-invalid="false"
                           multiple=""
                           data-multiple-caption="{count} files selected"
                           accept="<?= '.' . implode(', .', common\components\FileTypesHelper::getAllowExtensions()) ?>"
                           value="">
                    <label class="uploadlabel" for="<?= $filesFieldId; ?>">
                                        <span>
                                            <i class="tsi tsi-paperclip"></i>
                                            <?= _t('site.main', 'Attach Files'); ?>
                                        </span>
                    </label>
                </div>
                <div class="form-group">
                    <label>&nbsp;</label><br/>
                    <?= \yii\bootstrap\Html::submitButton('Submit ', ['class' => 'btn btn-primary btn-sm']); ?>
                </div>

            </div>
            <div class="one-print-request__add-service-hint">
                <?= _t('site.ps', 'You will get'); ?> : <span id="<?= 'addon_service_fee' . $order->id; ?>">0.00</span>
            </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>

        </div>
        <?php
    }
    ?>

</div>
<script>
    /**
     * get price with fee
     * @param el
     * @param orderId
     */
    function showServiceFeePrice(el, orderId) {
        var price = parseFloat(el.value);

        $.post('/workbench/service-order/calc-position-fee', {
                'price': price,
                'currency': '<?= $order->getCurrency() ?>'
            },
            function (data) {
                if (!data.willGet) {
                    document.getElementById('addon_service_fee' + orderId).innerHTML = '0<br>Minimum price 4';
                } else {
                    document.getElementById('addon_service_fee' + orderId).innerHTML = data.willGet;
                }
            }
        );
    }
</script>

