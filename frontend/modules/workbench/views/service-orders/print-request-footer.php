<?php

use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\models\ps\PsFacade;
use frontend\modules\workbench\models\view\StoreOrderAttempViewModel;
use yii\helpers\Url;

/**
 * @var $attemptView \frontend\modules\workbench\models\view\StoreOrderAttempViewModel
 */
$attempt        = $attemptViewComposite->attempt;
$attemptView    = $attemptViewComposite->attemptView;
$attemptProcess = $attemptViewComposite->attemptProcess;
?>
<div class="panel-body one-print-request__footer">
    <div class="row">
        <div class="col-sm-6">
            <?php if ($attempt->order->hasModel()): ?>
                <a href="<?php echo Url::toRoute(['/workbench/service-order/parts-list', 'attemptId' => $attempt->id]); ?>"
                   target="_blank"
                   class="btn btn-default btn-sm service-order-parts">
                    <span class="tsi tsi-doc"></span>
                    <?= _t('user.order', 'Print parts list'); ?>
                </a>
            <?php endif; ?>
        </div>

        <div class="col-sm-6">
            <div class="send-print-request-printing-time">
                <div class="">
                    <?php if ($attemptProcess->canRequestMoreTime()) : ?>
                        <?php if ($attemptView->showEstimatedProduction()) : ?>
                            <?= frontend\widgets\SiteHelpWidget::widget(['title' => 'Help', 'alias' => 'ps.estimatetime']); ?>
                            <?= _t('site.ps', 'Request more time'); ?>
                            <span data-date="<?= $attempt->dates->plan_printed_at ?>" ng-click="requestMoreTimeModal(<?= $attempt->id ?>, $event)"
                                  class="request-more-time-link request-more-time-span">
                                <?php if ($deadlineTimer = $attempt->getDeadlineCompany()) : ?>
                                    <div class="service-order-row__deadline">
                                        <?php
                                        if ($deadlineTimer->getLabelString()) {
                                            echo '<strong>' . $deadlineTimer->getLabelString() . '</strong>';
                                        } else {
                                            echo '<strong>' . $deadlineTimer->getTitle() . ':</strong>';
                                            echo \frontend\widgets\Countdown::widget([
                                                'id'              => 'countdownattemp-' . $attempt->id,
                                                'options'         => ['class' => 'label label-info'],
                                                'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                                                'timeout'         => $deadlineTimer->getTimeout(),
                                                'finishedMessage' => _t('site.ps', '00:00:00')
                                            ]);
                                        } ?>
                                    </div>
                                <?php endif; ?>
                            </span>
                        <?php elseif ($attempt->isReadySend()) : ?>
                            <?= _t('site.ps', 'Request more time'); ?>
                            <span data-date="<?= $attemptView->dates->scheduled_to_sent_at ?>" ng-click="requestScheduledTimeModal(<?= $attempt->id ?>, $event)"
                                  class="request-more-time-link request-more-time-span">
                                <span class="tsi tsi-alarm-clock"></span><?php echo PsFacade::getAttempScheduledToSentAt($attempt); ?> <?= _t('site.ps', 'hours'); ?>
                            </span>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($attempt->isReadySend()): ?>
    <div class="panel-body one-print-request__footer">
        <div class="row">
            <?php if ($attemptProcess->canRequestMoreTime()) : ?>
                <div class="col-sm-12">
                    <?= _t('site.ps', 'Request more time'); ?>
                    <span data-date="<?= $attemptView->dates->scheduled_to_sent_at ?>" ng-click="requestScheduledTimeModal(<?= $attempt->id ?>, $event)"
                          class="request-more-time-link request-more-time-span">
                        <span class="tsi tsi-alarm-clock"></span><?php echo PsFacade::getAttempScheduledToSentAt($attempt); ?> <?= _t('site.ps', 'hours'); ?>
                    </span>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>


