<?php

use common\components\Model3dWeightCalculator;
use common\models\DeliveryType;
use common\models\message\helpers\UrlHelper;
use common\models\model3d\PrintInstructionParser;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use common\models\StorePricer;
use common\models\UserAddress;
use common\modules\payment\components\PaymentInfoHelper;
use common\services\Model3dPartService;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserSessionFacade;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use frontend\widgets\PrintInstructionWidget;
use frontend\widgets\ReviewStarsStaticWidget;
use frontend\widgets\SiteHelpWidget;
use frontend\widgets\SwipeGalleryWidget;
use lib\delivery\delivery\models\DeliveryPostalLabel;
use lib\delivery\parcel\ParcelFactory;
use lib\MeasurementUtil;
use lib\money\Currency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$userWightMeasure = UserSessionFacade::isUsMeasurement() ? MeasurementUtil::OUNCE : MeasurementUtil::GRAM;

?>
<div class="container report-part-list">
    <div class="row">

        <div class="col-xs-9">
            <h2 class="m-b20"><?= _t("site.ps", "Order #") . $order->id; ?></h2>

        </div>
        <div class="col-xs-3">
            <?php
            if (false && $order->deliveryType && $order->deliveryType->code !== DeliveryType::PICKUP) {
                echo 'Ship to:<p class="m-l20">';
                echo UserAddress::formatAddress($order->shipAddress);
                echo '</p>';
            }
            ?>
        </div>
    </div>

    <div class="row">
        <?php
        foreach ($order->storeOrderItems as $oneStoreItem) :
        $model3dReplica = $oneStoreItem->model3dReplica;
        if ($oneStoreItem->hasModel()): ?>

        <div class="col-xs-12">

            <?php
            $i = 0;
            foreach ($oneStoreItem->model3dReplica->getCalculatedModel3dParts() as $model3dPart) {
                $texture           = $model3dPart->getCalculatedTexture();
                $model3dPartImgUrl = Model3dPartService::getModel3dPartRenderUrl($model3dPart);
                if ($model3dPart->qty < 1) {
                    continue;
                }
                $i++;
                if ($i == 8) {
                    echo "<div style='page-break-after: always;' ><br /></div><div class='margin-bottom:40px;padding-bottom:40px;'><br /><br /></div>";
                }
                ?>

                <div class="row m-b10" style="border: 1px solid #e0e4e8; padding: 10px 0; border-radius: 5px">
                    <div class="col-xs-3 ">
                        <?= Html::img(
                            $model3dPartImgUrl,
                            [
                                'width' => '120',
                            ]
                        );
                        ?>
                    </div>
                    <div class="col-xs-8">
                        <h4 class="one-print-request__model-title">
                            <?= H($model3dPart->file->getFileName()); ?>
                        </h4>
                        <?= _t('site.ps', 'Size'); ?> :
                        <?= $model3dPart->getSize()->toStringWithMeasurement() ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <table width="90%">
                                    <tr>
                                        <?php if ($texture && $texture->printerMaterial): ?>

                                            <td width="35%">
                                                <?= _t('site.ps', 'Material'); ?>: <br/>
                                                <big><?= $texture->printerMaterial->title ?></big></td>
                                        <?php endif; ?>
                                        <?php if ($texture && $texture->printerColor): ?>
                                            <td width="35%">
                                                <?= _t('site.ps', 'Color'); ?>:<br/>
                                                <big><?= $texture->printerColor->title ?></big>

                                            </td>
                                        <?php endif; ?>
                                        <td width="35%">
                                                <?= _t('site.ps', 'Infill'); ?>:<br/>
                                                <big><?= $texture->infillText() ?></big>
                                        </td>

                                        <td width="25%">
                                            <?= _t('site.ps', 'Quantity'); ?>: <br/>
                                            <big>x <?= $model3dPart->qty * $oneStoreItem->qty; ?></big>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-xs-6">

                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<?php endif ?>
<?php endforeach; ?>
<script>
    window.print();
</script>
<style>
    .report-part-list {
        max-width: 800px;
    }

    @media print {
        .report-part-list {
            width: 100%;
            max-width: 100%;
            padding: 0 40px;
        }

        @page {
            size: auto;   /* auto is the initial value */
            margin: 0;  /* this affects the margin in the printer settings */
        }
    }
</style>