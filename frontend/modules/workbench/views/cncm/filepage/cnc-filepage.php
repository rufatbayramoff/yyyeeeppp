<?php
/**
 * Created by mitaichik
 */

use frontend\assets\CncModelViewerAsset;

Yii::$app->angular
    ->service(['notify', 'router', 'user', 'modal'])
    ->controllerParams([
        'z' => 'y',
    ])
    ->controller('preorder/preorder-service')
    ->controller('cnc/cnc-filepage')
    ->controller('cnc/cnc-model-viewer')
    ->controller('store/filepage/models');

/** @var \yii\web\View $this */
/** @var \common\models\PsCncMachine $machine */
/** @var string $viewType */

$this->registerAssetBundle(CncModelViewerAsset::class);
$this->registerCssFile('/css/cnc.css');
$psId = $machine->companyService->ps_id;
$serviceId = $machine->companyService->id;
$resetQuoteUrl = $viewType ? \Yii::$app->params['siteUrl'] . '/my/ps-widget/cnc/?widgetCncId=' . $machine->companyService->id : $machine->companyService->company->getPublicCompanyLink().'/cnc';

echo $this->render('../templates/model-viewer');
echo $this->render('../templates/create-preorder-modal');
?>


<div class="over-nav-tabs-header">
    <div class="container">
        <h1><?= _t('site.cnc', 'CNC Quote')?>
            <span class="pull-right">
                <a href="<?= $resetQuoteUrl  ?>"
                   class="btn btn-default m-b15"><?= _t('site.cnc', 'New quote')?></a>
            </span>
        </h1>
    </div>
</div>

<div class="container" ng-controller="CncFilepageController">

    <div ng-cloak class="cnc_filepage_preloader ng-cloak-loader">
        <img  src="/static/images/preloader.gif" width="60" height="60">
    </div>

    <div class="cnc_filepage_preloader" ng-if="isLoading">
        <img src="/static/images/preloader.gif" width="60" height="60">
    </div>


    <div ng-cloak ng-if="!isLoading">

        <div ng-if="costingBundle.isSuccesfulCalculated()"
            class="row m-b30 hidden-md hidden-lg">
            <div class="col-md-12">
                <span class="m-r20 m-b10" style="display: inline-block;">
                    <strong>
                        <?= _t('site.cnc', 'Total') ?>: {{costingBundle.getTotal() | number : 2}}
                        {{machine.currency}}
                    </strong>
                </span>
                <button class="btn btn-primary"
                        ng-click="openPreorderModal()"
                        type="button">
                    <?= _t('site.cnc', 'Request a Quote') ?>
                </button>
            </div>

        </div>

        <div ng-repeat="part in model.parts"
             ng-init="preset = presets[part.id]"
             class="designer-card">

            <div class="row">

                <div class="text-danger col-md-12" ng-if="analyzeErrors[part.id]">
                    {{analyzeErrors[part.id]}}
                </div>

                <div ng-if="!analyzeErrors[part.id]">

                    <div class="col-xs-12 col-md-4 col-lg-3">

                        <div class="row">

                            <div class="col-xs-12 col-sm-6 col-md-12 m-b10">
                                <model-viewer
                                    part="part"
                                    preset="presets[part.id]"
                                    costing="costingBundle.getCostingForPart(part)"></model-viewer>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-12">
                            <form
                                    ng-if="part.cncParams"
                                    class="">
                                <table class="table cnc_model_view_settings">
                                    <tr>
                                        <td>
                                            <label><?= _t('site.cnc', 'Quantity')?></label>
                                        </td>
                                        <td>
                                            <input
                                                ng-model="preset.count"
                                                type="number" min="1" step="1" class="form-control input-sm cnc_model_view_settings__qty-input">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label><?= _t('site.cnc', 'Material')?></label>
                                        </td>
                                        <td>
                                            <select
                                                ng-model="preset.material"
                                                ng-options="material.title as material.code for material in machine.materials"
                                                class="form-control input-sm">
                                            </select>
                                        </td>
                                    </tr>
                                </table>

                                <div class="form-group">
                                    <label class="control-label m-b0"><?= _t('site.cnc', 'Post Processing')?></label>
                                    <div
                                        ng-repeat="postprocessing in machine.postProcessing"
                                        class="checkbox">
                                        <input
                                            id="processing-{{part.id}}-{{$index}}"
                                            check-list="preset.postprocessing"
                                            value="{{postprocessing.code}}"
                                            type="checkbox">
                                        <label for="processing-{{part.id}}-{{$index}}">{{postprocessing.title}}</label>
                                    </div>
                                </div>

                            </form>
                            </div>

                        </div>


                    </div>

                    <div class="col-xs-12 col-md-8 col-lg-9 wide-padding wide-padding--left">

                        <h3 class="m-t0 m-b15">{{part.name}}</h3>

                        <div
                                ng-cloak
                                ng-if="costingBundle.isSuccesfulCalculated()"
                                class="row hidden-xs hidden-sm">

                            <div class="col-sm-12 m-b15">
                                <span class="m-r20 m-b10" style="display: inline-block;">
                                    <strong>
                                        <?= _t('site.cnc', 'Total') ?>: {{costingBundle.getTotal() | number: 2}}
                                        {{machine.currency}}
                                    </strong>
                                </span>
                                <button class="btn btn-primary"
                                        ng-click="openPreorderModal()"
                                        type="button">
                                    <?= _t('site.cnc', 'Request a Quote') ?>
                                </button>
                            </div>

                        </div>

                        <div
                            class="cnc_preloader"
                            ng-if="!costingBundle.getCostingForPart(part) && !costingBundle.getErrorForPart(part)">
                            <img  src="/static/images/preloader.gif" width="60" height="60">
                            <div class="m-t20">
                                <?= _t('site.cnc', 'This may take a few minutes...')?>
                            </div>
                        </div>

                        <div
                            class="text-danger"
                            ng-if="costingBundle.getErrorForPart(part)">
                            <div ng-bind-html="costingBundle.getErrorForPart(part)"></div>
                            <button data-categoryga="CncUpload" data-actionga="GetAQuote" class="btn btn-danger btn-ghost"   loader-click="openCreatePreorder(<?php echo $psId; ?>,<?php echo $serviceId?>)">
                                <?= _t('site.ps', 'Get a Quote'); ?>
                            </button>
                        </div>

                        <div class="cnc_model_view_service" ng-if="costingBundle.getCostingForPart(part)">
                        <div
                            ng-if="costingBundle.getCostingForPart(part)"
                            ng-init="costing = costingBundle.getCostingForPart(part)"
                            class="table-responsive">

                            <table class="table table-condensed table-bordered m-b0">

                                <thead>
                                    <tr>
                                        <th><?= _t('site.cnc', 'Order Details')?></th>
                                        <th class="text-right"><?= _t('site.cnc', 'Item Cost')?></th>
                                        <th class="text-right"><?= _t('site.cnc', 'Quantity')?></th>
                                        <th class="text-right"><?= _t('site.cnc', 'Total Cost')?></th>
                                    </tr>
                                </thead>


                                <tbody>
                                    <tr ng-repeat="priceElement in costing.priceElements">
                                        <td>{{priceElement.title}}</td>
                                        <td class="text-right">{{priceElement.price | number : 2}} {{machine.currency}}</td>
                                        <td class="text-right">x {{getPreiceElementCount(priceElement, preset)}}</td>
                                        <td class="text-right">{{getTotalPriceElementCost(priceElement, preset) | number : 2}} {{machine.currency}}</td>
                                    </tr>


                                    <tr>
                                        <td colspan="4" class="text-right" style="border-bottom: 0"><b><?= _t('site.cnc', 'Total')?>: {{costing.total | number : 2}} {{machine.currency}}</b></td>
                                    </tr>

                                </tbody>
                            </table>

                        </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>

</div>
