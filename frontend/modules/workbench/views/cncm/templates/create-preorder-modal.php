

<script type="text/ng-template" id="/app/cnc/create-preorder-modal.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                    <h4 class="modal-title"><?= _t('site.cnc', 'Create a Quote')?></h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal" name="preorderForm">

                        <div class="form-group">
                            <label for="preorder-form-qty" class="col-sm-4 control-label"><?= _t('site.cnc', 'Quantity')?></label>
                            <div class="col-sm-8">
                                <span style="display: block; padding: 10px 0 0;">
                                {{preorder.totalQty}}
                                </span>
                            </div>
                        </div>
                        <div class="form-group" ng-if="preorder.isGuest">
                            <label for="preorder-form-name" class="col-sm-4 control-label control-label-sm"><?= _t('site.cnc', 'Client')?></label>
                            <div class="col-sm-8">
                                <input
                                    ng-model="preorder.name"
                                    required
                                    class="form-control input-sm" id="preorder-form-name" placeholder="<?= _t('site.cnc', 'Client')?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="preorder-form-description" class="col-sm-4 control-label control-label-sm"><?= _t('site.cnc', 'Project Description ')?></label>
                            <div class="col-sm-8">
                                <textarea
                                    ng-model="preorder.description"
                                    required
                                    class="form-control input-sm" id="preorder-form-description" placeholder="<?= _t('site.cnc', 'Description')?>" rows="2"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="preorder-form-description"
                                   class="col-sm-4 control-label control-label-sm"><?= _t('site.cnc', 'Budget') ?></label>
                            <div class="col-sm-8">
                                {{preorder.budget}} {{preorder.currency}}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="preorder-form-time" class="col-sm-4 control-label control-label-sm"><?= _t('site.cnc', 'Deadline')?></label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="tsi tsi-alarm-clock"></span></span>
                                    <input class="form-control" type="number" id="preorderTime" placeholder="<?= _t('site.preorder', 'Days') ?>"
                                           min="0"
                                           ng-model="preorder.estimateTime">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" ng-show="preorder.isGuest">
                            <label for="preorder-form-email" class="col-sm-4 control-label control-label-sm"><?= _t('site.cnc', 'E-mail')?></label>
                            <div class="col-sm-8">
                                <input
                                        ng-model="preorder.email"
                                        class="form-control input-sm" id="preorder-form-email" placeholder="<?= _t('site.cnc', 'E-mail')?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4">
                                <p class="form-control-static"><?= _t('site.cnc', 'Terms of delivery will be provided to the services after the final order value is approved.'); ?></p>
                            </div>
                        </div>


                    </form>

                </div>
                <div class="modal-footer">
                    <button
                        ng-click="$dismiss()"
                        type="button" class="btn btn-default"><?=_t('site.ps', 'Cancel')?></button>
                    <button
                        loader-click="create()"
                        loader-click-unrestored="true"
                        ng-disabled="preorderForm.$invalid"
                        type="button" class="btn btn-primary"><?=_t('site.ps', 'Submit')?></button>
                </div>
            </div>
        </div>
    </div>
</script>