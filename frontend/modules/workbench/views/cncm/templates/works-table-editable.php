
<script type="text/ng-template" id="/app/cnc/works-table-editable.html">

    <div>

        <h4 class="m-t0" ng-if="title">{{title}}</h4>

        <div class="table-responsive">
        <table class="table table-condensed cnc_works_table_editable m-b0"
               ng-class="{'table-bordered' : works.length}">


            <thead ng-if="works.length">
                <tr>
                    <th><?= _t('site.cnc', 'Item') ?></th>
                    <th class="text-right"><?= _t('site.cnc', 'Item cost') ?></th>
                    <th ng-if="showQtyColumn" class="text-right"><?= _t('site.cnc', 'Quantity') ?></th>
                    <th class="text-right"><?= _t('site.cnc', 'Total cost') ?></th>
                    <th ng-if="editable"></th>
                </tr>
            </thead>


            <tbody ng-if="works.length">





                <tr ng-repeat="work in works">


                    <!-- VIEW TD -->

                    <td ng-if="!work.$$isEdit">{{work.title}}</td>
                    <td ng-if="!work.$$isEdit" class="text-right">{{work.cost | number : 2}} {{work.currency}}</td>
                    <td ng-if="!work.$$isEdit && showQtyColumn" class="text-right">x {{getWorkQty(work)}}</td>
                    <td ng-if="!work.$$isEdit" class="text-right">{{getWorkCost(work) | number : 2}} {{preorder.getCurrency()}}</td>

                    <!-- EDIT TD -->

                    <td ng-if="work.$$isEdit">
                        <input class="form-control input-sm"
                               ng-model="work.title"/>
                    </td>


                    <td ng-if="work.$$isEdit"
                        class="text-right">

                        <input class="form-control input-sm cnc_works_table_editable--input_cost"
                               ng-model="work.cost"
                               size="7"
                               type="number"
                               min="0.01"
                               step="0.01"/>
                        {{work.currency}}
                    </td>


                    <td ng-if="work.$$isEdit && showQtyColumn" class="text-right">

                        <select class="form-control input-sm"
                                ng-if="work.isForPart()"
                                ng-model="work.per">
                            <option value="part"><?= _t('site.cnc', 'Per part') ?></option>
                            <option value="batch"><?= _t('site.cnc', 'Per batch') ?></option>
                        </select>
                    </td>

                    <td ng-if="work.$$isEdit" class="text-right">{{getWorkCost(work) | number : 2}} {{preorder.getCurrency()}}</td>

                    <!-- ACTION TD -->

                    <td ng-if="editable"
                        class="cnc_works_table_editable--action_td">

                        <div ng-if="!work.$$loading">

                            <button
                                    ng-if="work.$$isEdit"
                                    ng-click="saveWork(work)"
                                    class="btn btn-link btn-xs"
                                    title="<?= _t('site.cnc', 'Submit') ?>"
                                    type="button"><span class="tsi tsi-checkmark"> </span></button>

                            <button
                                    ng-if="!work.$$isEdit"
                                    ng-click="editWork(work)"
                                    class="btn btn-link btn-xs"
                                    title="<?= _t('site.cnc', 'Edit') ?>"
                                    type="button"><span class="tsi tsi tsi-pencil"> </span></button>

                            <button
                                    ng-click="deleteWork(work)"
                                    class="btn btn-link btn-xs"
                                    title="<?= _t('site.cnc', 'Delete') ?>"
                                    type="button"><span class="tsi tsi tsi-remove"> </span></button>

                        </div>

                        <span ng-if="work.$$loading" class="tsi tsi-spinner tsi-spin"></span>

                    </td>
                </tr>

                <tr class="cnc_works_table_editable--total_row">
                    <td colspan="{{showQtyColumn ? 4 : 3}}" class="text-right"><b><?= _t('site.cnc', 'Total cost') ?>: {{getTotalCost() | number : 2}} {{preorder.getCurrency()}}</b></td>
                </tr>



            </tbody>

            <tbody ng-if="!works.length">

            </tbody>

        </table>
        </div>

        <div ng-if="editable">
            <button
                    class="btn btn-default btn-sm"
                    type="button"
                    ng-click="addWork()">
                <span class="tsi tsi-plus m-r10"></span>{{addButtonTitle}}
            </button>
        </div>

    </div>

</script>





