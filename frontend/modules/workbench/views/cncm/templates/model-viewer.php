
<script type="text/ng-template" id="/app/cnc/model-viewer.html">

    <div>
        <div
            ng-if="view.isLoading"
            class="cnc_preloader">

            <img src="/static/images/preloader.gif">

            <div class="m-t20">
                <?= _t('site.cnc', 'This may take a few minutes...')?>
            </div>

        </div>

        <div class="text-danger" ng-if="view.loadingError">
            {{view.loadingError}}
        </div>

        <div
            ng-if="!view.isLoading && !view.loadingError"
            class="cnc_model_view_viewer">

            <div class="cnc_model_view_viewer_render">
            </div>

        </div>

        <div class="checkbox"
             ng-if="!view.isLoading && costing && preset">
            <input
                id="problems-{{part.id}}"
                ng-model="view.isModeling"
                type="checkbox">
            <label for="problems-{{part.id}}"><?= _t('site.cnc', 'Show problem areas')?></label>
        </div>




    </div>

</script>
