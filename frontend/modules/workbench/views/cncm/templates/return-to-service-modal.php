<script type="text/ng-template" id="/app/cnc/return-to-service-modal.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                    <h4 class="modal-title"><?= _t('site.cnc', 'Counter offer')?></h4>
                </div>
                <div class="modal-body">

                    <form name="returnToServiceForm">

                        <div class="form-group">
                            <label for="exampleInputEmail1"><?= _t('site.cnc', 'Comment')?></label>
                            <textarea style="height: 100px" class="form-control"
                                      ng-model="comment"></textarea>
                        </div>

                    </form>

                </div>
                <div class="modal-footer">
                    <button
                        ng-click="$dismiss()"
                        type="button" class="btn btn-default"><?=_t('site.ps', 'Cancel')?></button>
                    <button
                        loader-click="returnToService()"
                        loader-click-unrestored="true"
                        type="button" class="btn btn-primary"><?=_t('site.ps', 'Submit')?></button>
                </div>
            </div>
        </div>
    </div>
</script>