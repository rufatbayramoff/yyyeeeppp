<script type="text/ng-template" id="/app/cnc/files-list.html">

    <div>

        <h4 class="m-t0 m-b0">{{title}}</h4>

        <div class="m-b10"
             ng-if="files.length">

            <div class="cnc_works_table_editable--file " ng-repeat="file in files">

                <span title="{{file.name}}">{{file.name}}</span>

                <button ng-click="downloadFile(file.id)"
                        class="btn btn-link btn-xs"
                        title="<?= _t('site.cnc', 'Download') ?>"
                        type="button"><span class="tsi tsi-download-l"> </span></button>


                <button ng-click="removeFile(file)"
                        ng-if="editable"
                        class="btn btn-link btn-xs"
                        title="<?= _t('site.cnc', 'Delete') ?>"
                        type="button"><span class="tsi tsi-remove"> </span></button>

            </div>


            <div class="cnc_works_table_editable--file "
                 ng-repeat="upload in uploads">

                <span class="tsi tsi-spinner tsi-spin"></span>
                <span>{{upload.name}}</span>

            </div>

        </div>


        <div style="margin: 5px 0 10px;"
             ng-if="!files.length">

            <?= _t('site.cnc', 'No files') ?>

        </div>

        <button ng-if="editable"
                dropzone-button
                max-files="10"
                accepted-files="<?= '.'. implode(', .', common\components\FileTypesHelper::getAllowExtensions())?>"
                on-accept="uploadFile(file)"
                class="btn btn-sm btn-default">
                <span class="tsi tsi-plus m-r10"></span><?= _t('site.cnc', 'Add files') ?></button>

    </div>

</script>

