<?php

use common\models\StoreOrderReview;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var StoreOrderReview $storeOrderReview
 */

?>

<?php $form = ActiveForm::begin([
    'action' => \yii\helpers\Url::to(['/workbench/reviews/answer-review', 'review_id' => $storeOrderReview->id])
]); ?>

    <?php echo $form->field($storeOrderReview, 'answer')->label(false)->textarea(['class' => 'form-control', 'rows' => 4]); ?>

    <?php echo Html::submitButton(($storeOrderReview->answer ? _t('site.review', 'Re-submit') : _t('site.review', 'Submit')),
    ['class' => 'btn btn-primary']); ?>
<?php ActiveForm::end(); ?>
