<?php
/**
 * User: nabi
 */

use frontend\assets\DropzoneAsset;
use frontend\assets\SocialButtonsAsset;
use frontend\models\ps\PsFacade;
use frontend\widgets\PsPrintServiceReviewStarsWidget;
use frontend\widgets\Model3dUploadWidget;
use frontend\widgets\Model3dViewedStateWidget;
use yii\helpers\Url;

/** @var $ps \common\models\Ps */
/** @var $this \yii\web\View */
/** @var $publicServicePage \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\Model3dViewedState $model3dViewedState */
/** @var $reviewsProvider \yii\data\ActiveDataProvider */
/** @var $storeOrderReviewSearch \frontend\models\review\StoreOrderReviewSearch */

echo $this->render('@frontend/views/c/review-share-modal.php');
$this->title = 'Sales Reviews';
$this->registerAssetBundle(DropzoneAsset::class);

Yii::$app->angular
    ->service(['modal', 'user', 'router', 'notify'])
    ->controller('review/shareReview')
    ->directive('dropzone-button');

$this->registerAssetBundle(SocialButtonsAsset::class);

$emptyReviewsCount = 0;

$listView = \yii\widgets\ListView::widget([
    'dataProvider' => $reviewsProvider,
    'itemOptions' => ['tag'=>null],
    'itemView' => '@frontend/views/c/reviewItem'
]);

?>
<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(['section' => 'workbench/reviews']); ?>

<div class="container container--wide">
    <div class="row ps-pub-profile">
        <div class="col-sm-8 wide-padding wide-padding--right">

            <?php if ($ps->psCatalog && $ps->psCatalog->rating_count): ?>

                <h2 class="ps-profile-user-review-title">
                    <?= _t('ps.profile', 'Sales Reviews') ?>
                    <div class="ps-profile-rating">
                        <?= PsPrintServiceReviewStarsWidget::widget(['ps' => $ps]); ?>
                    </div>
                </h2>

                <?php echo $this->render('@frontend/views/c/reviews/searchPanel', ['searchModel' => $storeOrderReviewSearch, 'action' => Url::toRoute(['/workbench/reviews'])]); ?>

                <a name="reviews"></a>
                <?=$listView?>
            <?php else:

                echo ' <p>' . _t('site.ps', 'No reviews yet.') . '</p>';
            endif; ?>

        </div>
        <div class="col-sm-4">
            <h2 class="ps-profile-user-review-title">
                Why Share Reviews?</h2>
            <ul class="checked-list ">
                <li>Save time with the convenience of sharing a review in just a few clicks</li>
                <li>Create content and promote your business on different platforms including Facebook &amp; Linkedin</li>
                <li>Positive feedback coming directly from satisfied customers speaks volumes for potential clients</li>
                <li>Shared reviews are clickable links which bring users to the review and your business landing page</li>
                <li>Provide users with a quick &amp; easy way&nbsp;to order the same items in the review with your business</li>
                <li>Attract more customers from increased online exposure and receive more orders</li>
                <li>Show your customers that their feedback is valued and improve customer retention</li>
            </ul>

        </div>
    </div>
</div>