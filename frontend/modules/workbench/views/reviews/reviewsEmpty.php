<?php
/**
 * User: nabi
 */

use frontend\assets\DropzoneAsset;
use frontend\assets\SocialButtonsAsset;
use frontend\widgets\PsPrintServiceReviewStarsWidget;
use frontend\widgets\Model3dUploadWidget;
use frontend\widgets\Model3dViewedStateWidget;

/** @var $ps \common\models\Ps */
/** @var $this \yii\web\View */
/** @var $publicServicePage \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\Model3dViewedState $model3dViewedState */
/** @var $reviewsProvider \yii\data\ActiveDataProvider */

?>
<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(['section' => 'workbench/reviews']); ?>

<div class="container">
    <div class="row ps-pub-profile">
        <div class="col-sm-12 wide-padding wide-padding--right">


            <?php

                echo ' <p>' . _t('site.ps', 'No reviews yet.') . '</p>';
             ?>

        </div>

    </div>
</div>