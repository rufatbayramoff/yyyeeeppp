<?php
/** @var \common\models\StoreOrder $storeOrder */

use common\models\message\helpers\UrlHelper;
use common\models\Ps;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use common\models\StoreOrderReview;
use common\models\User;
use common\modules\informer\InformerModule;
use common\modules\payment\models\StoreOrderPositionForm;
use frontend\models\ps\PsFacade;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\workbench\components\MessageUrlHelper;
use frontend\widgets\ReviewStarsStaticWidget;
use frontend\widgets\StoreOrderStatusHistory;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var User $currentUser */
/** @var StoreOrder $storeOrder */

$orderStatusIntl = StoreOrderAttemp::getStatusesForCustomer();
if ($storeOrder->is_dispute_open) {
    $supportLink = Html::a(
        _t('front.user', 'Close Dispute'),
        [
            '/workbench/order/dispute',
            'orderId' => $storeOrder->id,
            'flag'    => 0
        ],
        [
            'class' => 'btn  btn-success btn-sm',
            'data'  => [
                'confirm' => _t('site.order', 'Are you sure you want to close the dispute?'),
                'method'  => 'post',
            ],
        ]
    );
} elseif ($storeOrder->allowOpenDispute()) {
    $supportLink = Html::a(
        _t('front.user', 'Open Dispute'),
        ['/workbench/order/dispute', 'orderId' => $storeOrder->id, 'flag' => 1],
        [
            'class' => 'btn btn-ghost btn-default btn-sm',
            'data'  => [
                'confirm' => _t('site.order', 'Are you sure you want to open a dispute?'),
                'method'  => 'post',
            ],
        ]
    );
}

if (!empty($mode) && $mode == 'inner') {
    $psLink = Html::a('<span class="tsi tsi-message"></span>' . _t('front.user', 'Message'), '/workbench/order/view/' . $storeOrder->id . '#order-user-chat',
        ['class' => 'link-order-user-chat ']);
} else {
    $psLink = Html::a('<span class="tsi tsi-message"></span>' .
        _t('front.user', 'Message'), '/workbench/order/view/' . $storeOrder->id, ['class' => '']);
}

$attempt             = $storeOrder->currentAttemp;
$invoice             = $storeOrder->getPrimaryInvoice();
$showedAttemptStatus = $attempt?->showAttempStatus();
$orderPositionForm   = new StoreOrderPositionForm();
$deadlineTimer       = $attempt ? $attempt->getDeadlineClient() : null;

$review = $attempt ? StoreOrderReview::find()
    ->forAttemp($attempt)
    ->forPs($attempt->ps)
    ->isCanShow()
    ->one() : null;

?>
    <div class="order-block">
        <div class="row order__info">
            <div class="order__title">
                <?php if ($storeOrder->is_dispute_open): ?>
                    <div class="alert alert-warning"><?= _t('site.order', 'Order is in dispute. '); ?></div>
                <?php
                endif; ?>
                <h3 class="order__invoice_number">
                    <a href="/workbench/order/view/<?= $storeOrder->id; ?>" class="relative p-r5">
                        <?= _t('site.order', 'Order #{orderId}', ['orderId' => $storeOrder->id]); ?>
                        <?php if ($storeOrder->isForPreorder()) : ?>
                            (<?= _t('site.order', 'from Quote #{preorderId}', ['preorderId' => $storeOrder->preorder_id]); ?>)
                        <?php endif; ?>
                        <?php if ($storeOrder->informerCustomerOrders) { ?>
                            <div class="service-order-row__notification-label"></div>
                        <?php } ?>
                    </a>
                </h3>
                <div class="order__date">
                    <?= _t('user.order', 'Created'); ?>:
                    <?= app('formatter')->asDatetime($storeOrder->created_at); ?>
                </div>
            </div>

            <div class="order__state">
                <div class="order__status-row">
                    <div class="order__payment-status">
                        <div class="order__status order__status--<?= $storeOrder->getPaymentStatus(); ?>">
                            <?= _t('user.order', 'Payment status'); ?>:
                            <strong><?= $storeOrder->primaryPaymentInvoice->getCustomerStatusText() ?? '' ?></strong>
                        </div>
                    </div>
                </div>

                <div class="order__btns">
                    <?php
                    if ($deadlineTimer) {
                        echo '<div class="order__deadline"><span>';
                        if ($deadlineTimer->getLabelString()) {
                            echo $deadlineTimer->getLabelString() . '</span> ';
                        } else {
                            echo $deadlineTimer->getTitle() . ':</span> ';
                            echo \frontend\widgets\Countdown::widget(
                                [
                                    'id'              => 'countdownOrder-' . $storeOrder->id,
                                    'options'         => ['class' => 'label label-info', 'style' => 'min-width:70px;'],
                                    'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                                    'timeout'         => $deadlineTimer->getTimeout(),
                                    'finishedMessage' => _t('site.order', 'Expired')
                                ]
                            );
                        }
                        echo '</div>';
                    }

                    $resultButtonsArray = [];
                    $paymentBankInvoice = $storeOrder->getPrimaryInvoice()->paymentBankInvoice;

                    if ($paymentBankInvoice) {
                        $resultButtonsArray[] = Html::a(
                            '<span class="tsi tsi-doc"></span> ' . _t('site.order', 'Invoice'),
                            ['/store/payment/invoice', 'uuid' => $paymentBankInvoice->uuid],
                            ['class' => 'btn btn-primary btn-sm m-l10 btn-warning', 'target' => '_blank']
                        );
                    }

                    if ($storeOrder->canPay()) {
                        $resultButtonsArray[] = Html::a(
                            '<span class="tsi tsi-wallet"></span> ' . _t('site.order', ' Pay '),
                            Url::toRoute(['/store/payment/pay-invoice', 'invoiceUuid' => $storeOrder->getPrimaryInvoice()->uuid]),
                            ['class' => 'btn btn-primary btn-sm m-l10']
                        );
                    }
                    $resultButtons = implode(' ', $resultButtonsArray);
                    echo $resultButtons;
                    ?>
                </div>

                <?php if ($review): ?>
                    <div class="order__reviews">
                        <?php
                        $stars = ReviewStarsStaticWidget::widget(['rating' => $review->getRating()]);
                        echo Html::a($stars, '/c/' . $attempt->ps->url . '/reviews');
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row order__ps-info">
            <?php if ($attempt): ?>
                <div class="col-sm-6 order__ps-desc">
                    <a class="order__ps-avatar" href="<?= PsFacade::getPsLink($attempt->ps); ?>" target="_blank">
                        <?= Html::img(Ps::getCircleImageByPs($attempt->ps, 64, 64)); ?>
                    </a>
                    <div class="order__ps-name">
                        <h4 class="order__ps-title">
                            <a href="<?= PsFacade::getPsLink($attempt->ps); ?>" target="_blank">
                                <?= H($attempt->ps->title); ?>
                                <?= $attempt->isInterception()? _t('site.ps', 'Customer service'):''; ?>
                            </a>
                        </h4>
                        <?php $messageTopic = $storeOrder->activeMessageTopic; ?>
                        <?php if ($messageTopic) : ?>
                            <div class="order__ps-message">
                                <a href="<?php echo MessageUrlHelper::topic($messageTopic) ?>" class="btn btn-default btn-circle" target="_blank">
                                    <span class="tsi tsi-message"></span>
                                    <?php if ($messageTopic->getIsMember($currentUser) && $messageTopic->getMemberForUser($currentUser)->have_unreaded_messages) : ?>
                                        <span class="one-print-request__user__message-badge"></span>
                                    <?php endif; ?>
                                </a>
                            </div>
                        <?php else: ?>
                            <div class="order__ps-message">
                                <a href="<?php echo Url::to(UrlHelper::objectMessageRoute($storeOrder)) ?>" class="btn btn-default btn-circle" target="_blank">
                                    <span class="tsi tsi-message"></span>
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-sm-6 order__order-status">
                    <div class="order__status order__status--<?= $showedAttemptStatus ?>">
                        <?= _t('user.order', 'Order status'); ?>:
                        <div style="white-space: nowrap;display: inline-block;">
                            <strong><?= $attempt->getCustomerTextStatus() ?></strong>
                            <?php if (!$attempt->isCancelled()) : ?>
                                <?php echo StoreOrderStatusHistory::widget(['storeOrderAttemp' => $attempt]); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <div class="col-sm-5 order__ps-desc">
                    <?php
                    echo Html::a('<span class="tsi tsi-message"></span> ' . _t('front.user', 'Contact Support'), UrlHelper::supportMessageRoute($storeOrder),
                        ['target' => '_blank']);
                    ?>
                </div>
                <div class="col-sm-7 order__order-status">
                    <div class="order__status">
                        <?= _t('user.order', 'Order status'); ?>:
                        <?php if ($storeOrder->isResolved()): ?>
                            <strong><?= _t('user.order', 'Closed'); ?></strong>
                        <?php else: ?>
                            <strong><?= _t('user.order', 'New'); ?></strong>
                        <?php endif; ?>

                    </div>
                </div>
            <?php endif; ?>
        </div>

        <div class="order__body">

            <div class="order__model-data order-model-data">

                <?php if ($storeOrder->isForPreorder()) { ?>

                    <?php if (!empty($storeOrder->preorder->message) || !empty($storeOrder->preorder->description)): ?>
                        <div class="m-b20">
                            <div><b><?= _t('site.order', 'Description'); ?></b></div>
                            <?= H($storeOrder->preorder->description); ?>
                            <?= H($storeOrder->preorder->message); ?>
                        </div>

                        <div class="m-b20">
                            <b><?= _t('site.order', 'Estimated time:'); ?></b>
                            <?= _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}', ['n' => (int)$storeOrder->preorder->estimate_time]); ?>
                        </div>
                    <?php endif; ?>
                    <?php
                    $addressPreorder = '';
                    if ($storeOrder->preorder && $storeOrder->preorder->shipTo) {
                        $addressPreorder = \common\models\UserAddress::formatAddressWithFields($storeOrder->preorder->shipTo);
                    }
                    ?>
                    <?php if (!empty($addressPreorder)) { ?>
                        <div class="m-b20">
                            <div><b><?= _t('site.ps', 'Location'); ?></b></div>
                            <?php echo $addressPreorder ?>
                        </div>
                    <?php } ?>
                    <?php if ($storeOrder->preorder->offered): ?>
                        <div class="row" style="margin-top: -10px;">
                            <?php echo $this->render('@frontend/modules/workbench/views/preorder/preorder-invoice-table.php', [
                                'preorder'            => $storeOrder->preorder,
                                'showTotal'           => true,
                                'calculateAndShowFee' => true
                            ]);
                            ?>
                        </div>
                    <?php endif; ?>

                <?php } else { ?>

                    <?php
                    $receiptInfo = $this->render('receiptInfoWithTitle', ['storeOrder' => $storeOrder, 'invoice' => $invoice]);
                    foreach ($storeOrder->storeOrderItems as $storeItem):
                        ?>
                        <div class="order-model-data__row">
                            <div class="order-model-data__model">
                                <?php
                                if ($storeItem->hasProduct()) {
                                    echo $this->render('itemProduct', ['product' => $storeItem->product, 'receiptInfo' => $receiptInfo]);
                                }
                                if ($storeItem->hasModel()) {
                                    echo $this->render('itemModel3d', ['model3d' => $storeItem->model3dReplica, 'storeOrder' => $storeOrder, 'storeItem' => $storeItem, 'attempt' => $attempt, 'receiptInfo' => $receiptInfo]);
                                }
                                if ($storeItem->hasCuttingPack()) {
                                    echo $this->render('itemCutting', ['cuttingPack' => $storeItem->cuttingPack, 'receiptInfo' => $receiptInfo]);
                                }
                                ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php } ?>

            </div>
        </div>

        <?php

        $orderPositionDp = StoreOrderPosition::getDataProvider(
            ['order_id' => $storeOrder->id, 'status' => [StoreOrderPosition::STATUS_NEW, StoreOrderPosition::STATUS_PAID, StoreOrderPosition::STATUS_REFUNDED]],
            20,
            ['sort' => false]
        );

        if ($orderPositionDp->getTotalCount() > 0):
            ?>
            <div class="order__body order__body--add-service">
                <div class="row">
                    <div class="table-responsive">
                        <?= \yii\grid\GridView::widget(
                            [
                                'showHeader'   => true,
                                'layout'       => '{items}',
                                'options'      => ['class' => 'add-service-table'],
                                'tableOptions' => ['class' => 'table'],
                                'showFooter'   => false,
                                'dataProvider' => $orderPositionDp,
                                'columns'      => [
                                    [
                                        'attribute'      => 'Additional Services',
                                        'header'         => _t('site.order', 'Additional Services ') .
                                            frontend\widgets\SiteHelpWidget::widget(['title' => 'Help', 'alias' => 'ps.3dprinting.additionalservices']),
                                        'format'         => 'raw',
                                        'contentOptions' => ['class' => 'add-service-table__title'],
                                        'value'          => function (StoreOrderPosition $storeOrder) {
                                            if (empty($storeOrder->file_ids)) {
                                                return H($storeOrder->title);
                                            }
                                            $files  = \common\models\File::findAll($storeOrder->file_ids);
                                            $result = [];
                                            foreach ($files as $k => $v) {
                                                $result[] = Html::a($v->name, $v->getFileUrl(), ['alt' => $v->name, 'target' => '_blank']);
                                            }
                                            return
                                                sprintf(
                                                    '%s <br /><small>%s %s</small>',
                                                    H($storeOrder->title),
                                                    _t('site.order', 'attachments: '),
                                                    implode(", ", $result)
                                                );

                                        }
                                    ],
                                    [
                                        'attribute'      => 'primary_payment_invoice_uuid',
                                        'label'          => _t('site.order', 'Invoice Uuid'),
                                        'contentOptions' => ['class' => 'add-service-table__status'],
                                    ],
                                    [
                                        'format'         => 'raw',
                                        'attribute'      => 'status',
                                        'contentOptions' => ['class' => 'add-service-table__status'],
                                        'value'          => function (StoreOrderPosition $orderPosition) {
                                            if ($orderPosition->status === StoreOrderPosition::STATUS_REFUNDED) {
                                                $comment = $orderPosition->primaryPaymentInvoice->getRefundComment();
                                                return $orderPosition->status . ($comment ? '<br>' . Html::tag('span', $comment, ['class' => 'add-service-table__decline-reason']) : '');
                                            }

                                            return $orderPosition->status;
                                        }
                                    ],
                                    [
                                        'attribute'      => 'created_at',
                                        'contentOptions' => ['class' => 'add-service-table__created'],
                                        'format'         => 'date',
                                    ],
                                    [
                                        'attribute'      => 'amount',
                                        'format'         => 'raw',
                                        'contentOptions' => ['class' => 'add-service-table__amount'],
                                        'value'          => function (StoreOrderPosition $position) {
                                            return displayAsMoney($position->getFullPrice());
                                        }
                                    ],
                                    [
                                        'format'         => 'raw',
                                        'contentOptions' => ['class' => 'add-service-table__btns'],
                                        'value'          => function (StoreOrderPosition $storeOrderPosition) use ($orderPositionForm) {
                                            if ($storeOrderPosition->status !== StoreOrderPosition::STATUS_NEW) {
                                                return '';
                                            }
                                            if (empty($storeOrderPosition->order->current_attemp_id)) {
                                                return '';
                                            }
                                            if (!$orderPositionForm->canAdd($storeOrderPosition->order->currentAttemp)) {
                                                if ($storeOrderPosition->order->currentAttemp->status != 'new') {
                                                    return _t('site.order', 'Expired');
                                                }
                                            }
                                            $ok     = Html::a(
                                                _t('site.order', 'Accept'),
                                                ['/store/payment/pay-invoice', 'invoiceUuid' => $storeOrderPosition->primary_payment_invoice_uuid],
                                                [
                                                    'title' => _t('site.order', 'Accept additional service'),
                                                    'class' => 'btn btn-sm btn-success',
                                                ]
                                            );
                                            $cancel = Html::a(
                                                _t('site.order', 'Decline'),
                                                ['/workbench/order/position-decline', 'invoiceUuid' => $storeOrderPosition->primary_payment_invoice_uuid],
                                                [
                                                    'title'      => _t('site.order', 'Decline additional service'),
                                                    'class'      => 'btn btn-sm btn-warning ts-ajax-modal ',
                                                    'jsCallback' => 'window.location.reload()',
                                                ]
                                            );
                                            return $ok . $cancel;
                                        }
                                    ]
                                ],
                            ]
                        ); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>


        <?php if ($storeOrder->isForPreorder() && $storeOrder->preorder->files) : ?>
            <div class="order__body order__body--add-service p-t20 p-b20">
                <h4 class="m-t0"><?= _t('site.ps', 'Attached Files'); ?></h4>
                <div>
                    <?php foreach ($storeOrder->preorder->files as $file) : ?>
                        <div class="simple-file-link">
                            <a class="simple-file-link__link"
                               href="<?= PreorderUrlHelper::downloadPreorderFile($storeOrder->preorder, $file) ?>">
                                <span><?= H($file->name); ?></span>
                                <i><?= \Yii::$app->formatter->asShortSize($file->size, 2); ?></i>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="order__footer">
            <a class="btn btn-primary btn-sm" href="/workbench/order/view/<?= $storeOrder->id; ?>">
                <?= _t('site.order', 'Order details'); ?>
            </a>
            <a class="btn btn-default btn-sm pull-right m-r0" href="/workbench/order/repeat/<?= $storeOrder->id; ?>">
                <?= _t('site.order', 'Repeat Order'); ?>
            </a>
        </div>
    </div>
<?php

InformerModule::markAsViewedCustomerOrderInformer($storeOrder);