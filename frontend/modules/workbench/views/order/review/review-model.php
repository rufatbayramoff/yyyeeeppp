<script type="text/ng-template" id="/app/my/orders/review-modal.html">

    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= isset($modalTitle)?$modalTitle:_t('site.order', 'Review') ?></h4>
                </div>
                <div class="modal-body" style="overflow: auto;max-height: 76vh;">
                    <form class="form-horizontal" name="orderReviewForm">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.order', 'Speed')?></label>
                            <div class="col-sm-8">
                                <input star-input="form.rating_speed" type="number" class="star-rating" data-size="sm" step="1" data-symbol="&#xea4b;" data-glyphicon="false"
                                       data-rating-class="tsi">
                                <span class="help-block"><?= _t('site.order', 'How would you rate the speed in which your order was produced?')?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.order', 'Quality')?></label>
                            <div class="col-sm-8">
                                <input star-input="form.rating_quality" type="number" class="star-rating" data-size="sm" step="1" data-symbol="&#xea4b;" data-glyphicon="false"
                                       data-rating-class="tsi">
                                <span class="help-block"><?= _t('site.order', 'How would you rate the quality of the product(s) received?')?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.order', 'Communication')?></label>
                            <div class="col-sm-8">
                                <input star-input="form.rating_communication" type="number" class="star-rating" data-size="sm" step="1" data-symbol="&#xea4b;"
                                       data-glyphicon="false" data-rating-class="tsi">
                                <span class="help-block"><?= _t('site.order', 'How would you rate the communication of the supplier?')?></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.order', 'Share your purchase experience on Treatstock')?></label>
                            <div class="col-sm-8">
                                <textarea ng-model="form.comment" class="form-control" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('site.order', 'Attach photo(s) to be published with your review')?></label>
                            <div class="col-sm-8">
                                <div class="row review-modal-pics">
                                    <div class="review-modal-pics__item col-xs-6 col-sm-4" ng-repeat="(key, value) in allowedAttachFiles">
                                        <div class="checkbox">
                                            <input
                                                    check-list="form.printFiles"
                                                    value="{{key}}"
                                                    id="{{key}}"
                                                    type="checkbox">
                                            <label for="{{key}}">
                                                <img src="" ng-src="{{value}}">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="text-center ps-upload-photo ps-upload-photo--ps-test-upload"
                             dropzone-images="form.images"
                             can-change-after-select="true">

                            <div dropzone-images-preview></div>
                            <div dropzone-images-click>
                                <div class="btn btn-link p-l0 p-r0" style="margin: 0 auto;">
                                        <span class="drop-photo-icon">
                                            <span class="tsi tsi-upload-l"></span>
                                        </span>
                                    <?= _t('site.order', 'Drop pictures here (maximum of 10)'); ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button
                            loader-click="cancel()"
                            ng-click="$dismiss()"
                            type="button" class="btn btn-default">{{form.cancelText}}
                    </button>
                    <button
                            loader-click="save()"
                            loader-click-unrestored="true"
                            type="button" class="btn btn-danger"><?=_t('site.ps', 'Submit')?></button>
                </div>
            </div>
        </div>
    </div>
</script>