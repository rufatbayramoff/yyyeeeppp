<?php
?>
<div class="order-model-data__subtotal">
    <b class="invoice__number">
        <?= _t('site.order', 'Invoice #{invoice}', ['invoice' => $invoice->uuid]); ?>
    </b>
    <?= $this->render('receiptInfo', ['storeOrder' => $storeOrder, 'invoice' => $invoice]) ?>
</div>
