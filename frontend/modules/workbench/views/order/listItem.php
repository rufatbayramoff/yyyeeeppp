<?php
use common\models\Preorder;
use common\models\StoreOrder;

/** @var common\models\StoreOrder|common\models\Preorder $model */
/** @var \common\models\User $currentUser */
/** @var \yii\web\View $this */


if ($model instanceof StoreOrder) {
    echo $this->render('listItemOrder', ['currentUser' => $currentUser, 'storeOrder' => $model]);
}

if ($model instanceof Preorder) {
    echo $this->render('listItemPreorder', ['currentUser' => $currentUser, 'preorder' => $model]);
}
