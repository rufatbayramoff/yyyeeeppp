<?php

/** @var $model3dReplica \common\models\Model3dReplica */

use common\services\MeasureService;
use common\services\Model3dPartService;

?>

<div class="order-model-data__files">
    <div class="collapse in" id="fileList<?= $model3dReplica->id; ?>">
        <div class="order-model-data__kit-list">
            <?php
            foreach ($model3dReplica->getActiveModel3dParts() as $model3dPart) {
                $texture           = $model3dPart->getCalculatedTexture();
                $model3dPartImgUrl = Model3dPartService::getModel3dPartRenderUrl($model3dPart); ?>

                <div class="order-model-data__kit">
                    <div class="order-model-data__kit-pic">
                        <img src="<?= $model3dPartImgUrl; ?>">
                        <div class="order-model-data__kit-qty">
                            ×<?= $model3dPart->qty; ?>
                        </div>
                    </div>

                    <div class="order-model-data__kit-info">
                        <h4 class="order-model-data__kit-title"
                            title="<?= H($model3dPart->file->name); ?>"><?= H($model3dPart->file->name); ?></h4>

                        <?php if ($texture && $texture->printerColor && $texture->printerMaterial) : ?>
                            <div class="order-model-data__kit-material"
                                 title="<?= $texture->printerColor->render_color; ?> <?= H($texture->printerMaterial->title); ?>">
                                <div class="material-item">
                                    <div class="material-item__color"
                                         style="background-color: rgb(<?= ($texture->printerColor->rgb); ?>)"></div>
                                </div>
                                <?= H($texture->printerMaterial->title); ?>
                            </div>
                            <div>
                                <?= _t('site.ps', 'Infill') ?>
                                : <?= $texture->infillText() ?>
                            </div>
                        <?php endif; ?>

                        <div class="order-model-data__kit-details">
                            <span style="display: inline-block;">
                                 <?= _t('site.store', 'Size'); ?>: <?= $model3dPart->getSize()->toStringWithMeasurement(); ?>,
                            </span>
                            <span style="display: inline-block;">
                                 <?= _t('site.store', 'Weight'); ?>: <?= MeasureService::getWeightFormatted($model3dPart->getWeight()) ?>
                            </span>
                        </div>
                    </div>
                </div>
            <?php }; ?>
        </div>
    </div>
</div>
