<?php
/** @var \common\models\PaymentInvoice $invoice */
/** @var \common\models\StoreOrder $storeOrder */


$refundService = Yii::createObject(\common\modules\payment\services\RefundService::class);
$detailsRefund = $refundService->getApprovedRefundsByOrder($storeOrder);
?>

<table class="order__info-table m-b20">
    <?php foreach ($invoice->getSortedPaymentInvoiceItems() as $invoiceItem): ?>
        <tr>
            <td class="order__info-table-label">
                <?php echo \H($invoiceItem->title); ?>
            </td>
            <td class="order__info-table-info">
                <?php echo displayAsMoney($invoiceItem->getLineTotal()); ?>
            </td>
        </tr>
    <?php endforeach; ?>

    <?php $amountPaymentMethodFee = $invoice->getAmountPaymentMethodFee(); ?>
    <?php if ($amountPaymentMethodFee): ?>
        <tr>
            <td class="order__info-table-label">
                <?php echo _t('payment.receipt', 'Payment method fee'); ?>
            </td>
            <td class="order__info-table-info">
                <?php echo displayAsMoney($amountPaymentMethodFee); ?>
            </td>
        </tr>
    <?php endif; ?>

    <?php
    foreach ($detailsRefund as $detailRefund) {
        if ($detailRefund->payment->paymentInvoice->uuid == $invoice->uuid) {
            ?>
            <tr>
                <td class="order__info-table-label">
                    <?= _t('site.order', 'Partial Refund'); ?>
                    <span class="order__info-table-comment ugc-content">
                              <?= $detailRefund->paymentDetailOperation->toPaymentDetail()->paymentTransactionRefund ? '(' . H($detailRefund->paymentDetailOperation->toPaymentDetail()->paymentTransactionRefund->comment) . ')' : ''; ?>
                            </span>
                </td>
                <td class="order__info-table-info">
                    <?php echo displayAsMoney($detailRefund->getMoneyAmount()); ?>
                </td>
            </tr>
        <?php }
    } ?>
    <tr class="border-t">
        <td class="order__info-table-label order__info-table-label--total"><?= _t('payment.receipt', 'Total'); ?></td>
        <td class="order__info-table-info order__info-table-info--total"><?php echo displayAsMoney($invoice->getAmountTotalWithRefund()); ?></td>
    </tr>
    <?php if (($bonusAccrued = $invoice->getAmountBonusAccrued()) && ($bonusAccrued->getAmount())) { ?>
        <tr>
            <td class="order__info-table-label"><?= _t('payment.receipt', 'Bonus accrued'); ?></td>
            <td class="order__info-table-info ts-print-bonus">+<?php echo H($bonusAccrued->getAmount()); ?></td>
        </tr>
    <?php } ?>
</table>
