<?php

use common\components\ArrayHelper;
use common\models\DeliveryType;
use common\models\File;
use common\models\message\helpers\UrlHelper;
use common\models\Ps;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\StoreOrderPosition;
use common\models\StoreOrderReview;
use common\models\UserAddress;
use common\services\StoreOrderService;
use frontend\assets\DropzoneAsset;
use frontend\assets\LightboxAsset;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\ps\PsFacade;
use frontend\models\ps\StoreOrderAttemptProcess;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\workbench\components\MessageUrlHelper;
use frontend\modules\workbench\components\OrderUrlHelper;
use frontend\modules\workbench\models\view\StoreOrderAttempViewModel;
use frontend\widgets\ReviewStarsStaticWidget;
use frontend\widgets\StoreOrderStatusHistory;
use frontend\widgets\SwipeGalleryWidget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\User $currentUser */
/** @var common\models\StoreOrder $storeOrder */


$this->title = _t('site.orders', 'My Purchases');

$attempt = $storeOrder->currentAttemp;
if ($attempt) {
    $attemptView    = StoreOrderAttempViewModel::fill($attempt);
    $attemptProcess = StoreOrderAttemptProcess::create($attempt);
}
$paymentInvoice = $storeOrder->getPrimaryInvoice();
$deadlineTimer = $attempt?->getDeadlineClient();

$review = $attempt
    ? StoreOrderReview::find()
        ->forAttemp($attempt)
        ->forPs($attempt->ps)
        ->isCanShow()
        ->one()
    : null;


$this->registerAssetBundle(DropzoneAsset::class);
$this->registerAssetBundle(LightboxAsset::class);

Yii::$app->angular
    ->controller('my/orders/view')
    ->controller('store/common-models')
    ->controller('preorder/preorder-models')
    ->controller('preorder/preorder-service')
    ->controller('review/shareReview')
    ->directive('dropzone-button')
    ->directive(['star-input', 'dropzone-images'])
    ->service(['modal', 'notify', 'router', 'user'])
    ->constant(
        'globalConfig',
        [
            'facebook_app_id' => param('facebook_app_id')
        ]
    )
    ->controllerParams(['x' => 'y']);
?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(['section' => 'workbench/orders']); ?>

<?= $this->render('@frontend/views/c/review-share-modal'); ?>
<?= $this->render('review/review-model.php', ['modalTitle' => null]); ?>
<?= $this->renderFile('@frontend/views/templates/viewImagePopup.php'); ?>

<div class="container" ng-controller="PurchaseOrderViewController">
    <div class="row order-item">
        <div class="col-sm-3" id="order-user-chat">

            <a href="/workbench/orders/<?= StoreOrderService::getGroupCodeByStatus($attempt->status??''); ?>" class="btn btn-default order-item__back-btn">
                <span class="tsi tsi-left"></span>
                <?= _t('user.order', 'Back'); ?>
            </a>
        </div>

        <div class="col-sm-9 wide-padding wide-padding--left">
            <div class="order-block">
                <div class="row order__info">
                    <div class="order__title">
                        <?php if ($storeOrder->is_dispute_open): ?>
                            <div class="alert alert-warning"><?= _t('site.order', 'Order is in dispute. '); ?></div>
                        <?php
                        endif; ?>
                        <h3 class="order__invoice_number">
                            <a href="/workbench/order/view/<?= $storeOrder->id; ?>">
                                <?= _t('site.order', 'Order #{orderId}', ['orderId' => $storeOrder->id]); ?>
                                <?php if ($storeOrder->isForPreorder()): ?>
                                    (<?= _t('site.order', 'from Quote #{preorderId}', ['preorderId' => $storeOrder->preorder_id]); ?>)
                                <?php endif; ?>
                            </a>
                        </h3>
                        <div class="order__date">
                            <?= _t('user.order', 'Created'); ?>:
                            <?= app('formatter')->asDatetime($storeOrder->created_at); ?>
                        </div>
                    </div>

                    <div class="order__state">
                        <div class="order__status-row">
                            <div class="order__payment-status">
                                <div class="order__status order__status--<?= $storeOrder->getPaymentStatus(); ?>">
                                    <?= _t('user.order', 'Payment status'); ?>:
                                    <strong><?= $paymentInvoice->getCustomerStatusText() ?? '' ?></strong>
                                </div>
                            </div>
                        </div>

                        <div class="order__btns">
                            <?php
                            if ($deadlineTimer) {
                                echo '<div class="order__deadline"><span>';
                                if ($deadlineTimer->getLabelString()) {
                                    echo $deadlineTimer->getLabelString() . '</span> ';
                                } else {
                                    echo $deadlineTimer->getTitle() . ':</span> ';
                                    echo \frontend\widgets\Countdown::widget(
                                        [
                                            'id'              => 'countdownOrder-' . $storeOrder->id,
                                            'options'         => ['class' => 'label label-info', 'style' => 'min-width:70px;'],
                                            'datetime'        => $deadlineTimer->getDate('Y-m-d H:i:s'),
                                            'timeout'         => $deadlineTimer->getTimeout(),
                                            'finishedMessage' => _t('site.order', 'Expired')
                                        ]
                                    );
                                }
                                echo '</div>';
                            }
                            $links = [];
                            if ($attempt && Yii::$app->reviewService->canCreateOrderReview($attempt) && !$attemptProcess->canReceive()) {
                                $links['review'] = [
                                    'title' => _t('user.order', 'Review'),
                                    'label' => '<span class="tsi tsi-star"></span> ' . _t('user.order', 'Review'),
                                    'attr'  => [
                                        'class'    => 'btn btn-primary btn-sm',
                                        'ng-click' => "showReviewModal({$storeOrder->id}, " . Json::htmlEncode(
                                                ArrayHelper::map(
                                                    Yii::$app->reviewService->getAllowedAttachFiles($attempt),
                                                    'id',
                                                    function (File $file) {
                                                        return ImageHtmlHelper::getThumbUrlForFile($file, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL);
                                                    }
                                                )
                                            ) . ')',
                                        'onClick'  => 'return false;',
                                    ]
                                ];
                            }

                            if ($attempt && $attemptProcess->canReceive()) {
                                $imagesJson       = Json::htmlEncode(
                                    ArrayHelper::map(
                                        Yii::$app->reviewService->getAllowedAttachFiles($attempt),
                                        'id',
                                        function (File $file) {
                                            return ImageHtmlHelper::getThumbUrlForFile($file, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL);
                                        }
                                    )
                                );
                                $links['receive'] = [
                                    'title' => _t('site.store', 'Set as Received'),
                                    'label' => _t('site.store', 'Set as Received'),
                                    'attr'  => [
                                        'class'    => 'btn btn-primary btn-sm',
                                        'ng-click' => "setAsReceivedAndShowReviewModal({$storeOrder->id}, " . $imagesJson . ', ' . (Yii::$app->reviewService->canCreateOrderReview($attempt) ? 'true' : 'false') . ')',
                                        'onClick'  => 'return false;',

                                    ]
                                ];
                                if (app('request')->get('review', false)) {
                                    $this->registerJs("angular.element(document.getElementById('orderReviewCtrl')).scope().showReviewModalAndSetReceived({$storeOrder->id}, " . $imagesJson . ')');
                                }


                            }
                            $resultButtonsArray = [];
                            foreach ($links as $k => $v) {
                                $v['attr']            = array_merge($v['attr'], ['title' => $v['title']]);
                                $resultButtonsArray[] = Html::a(
                                    $v['label'],
                                    ['/workbench/order/' . $k, 'id' => $storeOrder->id],
                                    $v['attr']
                                );
                            }

                            $paymentBankInvoice = $storeOrder->getPrimaryInvoice()->paymentBankInvoice;

                            if ($paymentBankInvoice) {
                                $resultButtonsArray[] = Html::a(
                                    '<span class="tsi tsi-doc"></span> ' . _t('site.order', 'Invoice'),
                                    ['/store/payment/invoice', 'uuid' => $paymentBankInvoice->uuid],
                                    ['class' => 'btn btn-primary btn-sm m-l10 btn-warning', 'target' => '_blank']
                                );
                            }

                            if ($storeOrder->canPay()) {
                                $resultButtonsArray[] = Html::a(
                                    '<span class="tsi tsi-wallet"></span> ' . _t('site.order', ' Pay '),
                                    Url::toRoute(['/store/payment/pay-invoice', 'invoiceUuid' => $storeOrder->getPrimaryInvoice()->uuid]),
                                    ['class' => 'btn btn-primary btn-sm m-l10']
                                );
                            }
                            $resultButtons = implode(' ', $resultButtonsArray);
                            echo $resultButtons;
                            ?>
                        </div>

                        <?php if ($review) : ?>
                            <div class="order__reviews">

                                <?php
                                $stars = ReviewStarsStaticWidget::widget(['rating' => $review->getRating()]);
                                echo Html::a($stars, '/c/' . $attempt->ps->url . '/reviews');
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row order__ps-info">
                    <?php if ($attempt) : ?>
                        <div class="col-sm-6 order__ps-desc">
                            <a class="order__ps-avatar" href="<?= PsFacade::getPsLink($attempt->ps); ?>" target="_blank">
                                <?= Html::img(Ps::getCircleImageByPs($attempt->ps, 64, 64)); ?>
                            </a>
                            <div class="order__ps-name">
                                <h4 class="order__ps-title">
                                    <a href="<?= PsFacade::getPsLink($attempt->ps); ?>" target="_blank">
                                        <?= H($attempt->ps->title); ?>
                                        <?= $attempt->isInterception()? _t('site.ps', 'Customer service'):''; ?>
                                    </a>
                                </h4>
                                <?php $messageTopic = $storeOrder->activeMessageTopic; ?>
                                <?php if ($messageTopic) : ?>
                                    <div class="order__ps-message">
                                        <a href="<?php echo MessageUrlHelper::topic($messageTopic) ?>" class="btn btn-default btn-circle" target="_blank">
                                            <span class="tsi tsi-message"></span>
                                            <?php if ($messageTopic->getIsMember($currentUser) && $messageTopic->getMemberForUser($currentUser)->have_unreaded_messages) : ?>
                                                <span class="one-print-request__user__message-badge"></span>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                <?php else:?>
                                    <div class="order__ps-message">
                                        <a href="<?php echo Url::to(UrlHelper::objectMessageRoute($storeOrder)) ?>" class="btn btn-default btn-circle" target="_blank">
                                            <span class="tsi tsi-message"></span>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-sm-6 order__order-status">
                            <div class="order__status order__status--<?= $attempt->showAttempStatus() ?>">
                                <?= _t('user.order', 'Order status'); ?>:
                                <div style="white-space: nowrap;display: inline-block;">
                                    <strong><?= $attempt->getCustomerTextStatus() ?></strong>
                                    <?php if (!$attempt->isCancelled()) : ?>
                                        <?php echo StoreOrderStatusHistory::widget(['storeOrderAttemp' => $attempt]); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="col-sm-5 order__ps-desc">
                            <?php
                            echo Html::a(
                                '<span class="tsi tsi-message"></span> ' . _t('front.user', 'Contact Support'),
                                UrlHelper::supportMessageRoute($storeOrder),
                                ['target' => '_blank']
                            );
                            ?>
                        </div>
                        <div class="col-sm-7 order__order-status">
                            <div class="order__status">
                                <?= _t('user.order', 'Order status'); ?>:
                                <?php if ($storeOrder->isResolved()) : ?>
                                    <strong><?= _t('user.order', 'Closed'); ?></strong>
                                <?php else : ?>
                                    <strong><?= _t('user.order', 'New'); ?></strong>
                                <?php endif; ?>

                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="order__body">

                    <div class="order__model-data order-model-data">
                        <?php if ($storeOrder->isForPreorder()) { ?>
                            <?php if (!empty($storeOrder->preorder->message) || !empty($storeOrder->preorder->description)) : ?>
                                <div class="m-b20">
                                    <div><b><?= _t('site.order', 'Description'); ?></b></div>
                                    <?= H($storeOrder->preorder->description); ?>
                                    <?= H($storeOrder->preorder->message); ?>
                                </div>

                                <div class="m-b20">
                                    <b><?= _t('site.order', 'Estimated time:'); ?></b>
                                    <?= _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}', ['n' => (int)$storeOrder->preorder->estimate_time]); ?>
                                </div>
                            <?php endif; ?>
                            <?php
                            $addressPreorder = '';
                            if ($storeOrder->preorder && $storeOrder->preorder->shipTo) {
                                $addressPreorder = \common\models\UserAddress::formatAddressWithFields($storeOrder->preorder->shipTo);
                            }
                            ?>
                            <?php if (!empty($addressPreorder)) { ?>
                                <div class="m-b20">
                                    <div><b><?= _t('site.ps', 'Location'); ?></b></div>
                                    <?php echo $addressPreorder ?>
                                </div>
                            <?php } ?>
                            <?php if ($storeOrder->preorder->offered) : ?>
                                <div class="row" style="margin-top: -10px;">
                                    <?php echo $this->render('@frontend/modules/workbench/views/preorder/preorder-invoice-table.php', [
                                        'preorder'            => $storeOrder->preorder,
                                        'showTotal'           => true,
                                        'calculateAndShowFee' => true
                                    ]);
                                    ?>
                                </div>
                            <?php endif; ?>

                        <?php } else { ?>
                            <?php
                            $receiptInfo = $this->render('receiptInfoWithTitle', ['storeOrder' => $storeOrder, 'invoice' => $paymentInvoice]);
                            foreach ($storeOrder->storeOrderItems as $storeItem) :
                                ?>
                                <div class="order-model-data__row">
                                    <div class="order-model-data__model">
                                        <?php
                                        if ($storeItem->hasProduct()) {
                                            echo $this->render('itemProduct', ['product' => $storeItem->product, 'receiptInfo' => $receiptInfo]);
                                        }
                                        if ($storeItem->hasModel()) {
                                            echo $this->render('itemModel3d', ['model3d' => $storeItem->model3dReplica, 'attempt' => $attempt, 'receiptInfo' => $receiptInfo, 'showParts' => true]);
                                        }
                                        if ($storeItem->hasCuttingPack()) {
                                            echo $this->render('itemCutting', ['cuttingPack' => $storeItem->cuttingPack, 'receiptInfo' => $receiptInfo]);
                                        }
                                        ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php } ?>

                    </div>
                </div>

                <?php

                $orderPositionDp = StoreOrderPosition::getDataProvider(
                    ['order_id' => $storeOrder->id, 'status' => [StoreOrderPosition::STATUS_NEW, StoreOrderPosition::STATUS_PAID, StoreOrderPosition::STATUS_REFUNDED]],
                    20,
                    ['sort' => false]
                );

                if ($orderPositionDp->getTotalCount() > 0):
                    ?>
                    <div class="order__body order__body--add-service">
                        <div class="row">
                            <div class="table-responsive">
                                <?= \yii\grid\GridView::widget(
                                    [
                                        'showHeader'   => true,
                                        'layout'       => '{items}',
                                        'options'      => ['class' => 'add-service-table'],
                                        'tableOptions' => ['class' => 'table'],
                                        'showFooter'   => false,
                                        'dataProvider' => $orderPositionDp,
                                        'columns'      => [
                                            [
                                                'attribute'      => 'Additional Services',
                                                'header'         => _t('site.order', 'Additional Services ') .
                                                    frontend\widgets\SiteHelpWidget::widget(['title' => 'Help', 'alias' => 'ps.3dprinting.additionalservices']),
                                                'format'         => 'raw',
                                                'contentOptions' => ['class' => 'add-service-table__title'],
                                                'value'          => function (StoreOrderPosition $storeOrder) {
                                                    if (empty($storeOrder->file_ids)) {
                                                        return H($storeOrder->title);
                                                    }
                                                    $files  = \common\models\File::findAll($storeOrder->file_ids);
                                                    $result = [];
                                                    foreach ($files as $k => $v) {
                                                        $result[] = Html::a($v->name, $v->getFileUrl(), ['alt' => $v->name, 'target' => '_blank']);
                                                    }
                                                    return
                                                        sprintf(
                                                            '%s <br /><small>%s %s</small>',
                                                            H($storeOrder->title),
                                                            _t('site.order', 'attachments: '),
                                                            implode(", ", $result)
                                                        );

                                                }
                                            ],
                                            [
                                                'attribute'      => 'primary_payment_invoice_uuid',
                                                'label'          => _t('site.order', 'Invoice Uuid'),
                                                'contentOptions' => ['class' => 'add-service-table__status'],
                                            ],
                                            [
                                                'format'         => 'raw',
                                                'attribute'      => 'status',
                                                'contentOptions' => ['class' => 'add-service-table__status'],
                                                'value'          => function (StoreOrderPosition $orderPosition) {
                                                    if ($orderPosition->status === StoreOrderPosition::STATUS_REFUNDED) {
                                                        $comment = $orderPosition->primaryPaymentInvoice->getRefundComment();
                                                        return $orderPosition->status . ($comment ? '<br>' . Html::tag('span', $comment, ['class' => 'add-service-table__decline-reason']) : '');
                                                    }

                                                    return $orderPosition->status;
                                                }
                                            ],
                                            [
                                                'attribute'      => 'created_at',
                                                'contentOptions' => ['class' => 'add-service-table__created'],
                                                'format'         => 'date',
                                            ],
                                            [
                                                'attribute'      => 'amount',
                                                'format'         => 'raw',
                                                'contentOptions' => ['class' => 'add-service-table__amount'],
                                                'value'          => function (StoreOrderPosition $position) {
                                                    return displayAsMoney($position->getFullPrice());
                                                }
                                            ],
                                            [
                                                'format'         => 'raw',
                                                'contentOptions' => ['class' => 'add-service-table__btns'],
                                                'value'          => function (StoreOrderPosition $storeOrderPosition) {
                                                    if ($storeOrderPosition->status !== StoreOrderPosition::STATUS_NEW) {
                                                        return '';
                                                    }
                                                    if (empty($storeOrderPosition->order->current_attemp_id)) {
                                                        return '';
                                                    }
                                                    $ok     = Html::a(
                                                        _t('site.order', 'Accept'),
                                                        ['/store/payment/pay-invoice', 'invoiceUuid' => $storeOrderPosition->primary_payment_invoice_uuid],
                                                        [
                                                            'title' => _t('site.order', 'Accept additional service'),
                                                            'class' => 'btn btn-sm btn-success',
                                                        ]
                                                    );
                                                    $cancel = Html::a(
                                                        _t('site.order', 'Decline'),
                                                        ['/workbench/order/position-decline', 'invoiceUuid' => $storeOrderPosition->primary_payment_invoice_uuid],
                                                        [
                                                            'title'      => _t('site.order', 'Decline additional service'),
                                                            'class'      => 'btn btn-sm btn-warning ts-ajax-modal ',
                                                            'jsCallback' => 'window.location.reload()',
                                                        ]
                                                    );
                                                    return $ok . $cancel;
                                                }
                                            ]
                                        ],
                                    ]
                                ); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>


                <?php if ($storeOrder->isForPreorder() && $storeOrder->preorder->files) : ?>
                    <div class="order__body order__body--add-service p-t20 p-b20">

                        <h4 class="m-t0"><?= _t('site.ps', 'Attached Files'); ?></h4>

                        <div>
                            <?php foreach ($storeOrder->preorder->files as $file) : ?>
                                <div class="simple-file-link">
                                    <a class="simple-file-link__link"
                                       href="<?= PreorderUrlHelper::downloadPreorderFile($storeOrder->preorder, $file) ?>">
                                        <span><?= H($file->name); ?></span>
                                        <i><?= \Yii::$app->formatter->asShortSize($file->size, 2); ?></i>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (!empty($storeOrder->comment)) : ?>
                    <div class="order__body order__body--comments">
                        <h4 class="m-b0 m-t0"><?= _t('site.order', 'Comment'); ?></h4>
                        <p><?= H($storeOrder->comment); ?></p>
                    </div>
                <?php endif; ?>

                <?php if (!empty($storeOrder->current_attemp_id) && $storeOrder->currentAttemp->moderation && $storeOrder->currentAttemp->moderation->files) : ?>
                    <div class="order__body order__body--results">
                        <h4 class="m-t0"><?= _t('site.order', 'Order Results'); ?></h4>

                        <?=
                        SwipeGalleryWidget::widget([
                            'files'            => ArrayHelper::getColumn($storeOrder->currentAttemp->moderation->files, 'file'),
                            'thumbSize'        => [160, 120],
                            'assetsByClass'    => true,
                            'containerOptions' => ['class' => 'ps-profile-user-review__user-models'],
                            'itemOptions'      => ['class' => 'ps-profile-user-review__user-models-pic'],
                            'scrollbarOptions' => ['class' => 'ps-profile-user-review__user-models-scrollbar'],
                        ]);
                        ?>

                        <div class="m-b10">
                            <?php if ($attempt->isDislikedByUser()) { ?>
                                <!-- user accepted -->
                                <span class="tsi tsi-thumbs-down"></span> <?= _t('site.order', 'Disliked'); ?>
                            <?php } else {
                                if ($attempt->isAcceptedByUser()) { ?>
                                    <!-- user accepted -->
                                    <span class="tsi tsi-checkmark-c"></span> <?= _t('site.order', 'Liked'); ?>
                                <?php } else {
                                    if ($attempt->status == StoreOrderAttemp::STATUS_ACCEPTED) { ?>
                                        <span>
                        <span class="tsi tsi-clock"></span> <?= _t('site.order', 'Printing in progress...'); ?>
                    </span>
                                    <?php } else {
                                        if ($attempt->canUserLike()) {
                                            $confirmMessage = _t(
                                                'site.order',
                                                'By liking the results, you agree to have your order shipped without being moderated. This will allow the manufacturer to ship your order.'
                                            );
                                            ?>

                                            <a href="<?= Url::toRoute(
                                                [
                                                    '/workbench/order/client-accept',
                                                    'attemptId' => $attempt->id,
                                                    'orderId'   => $storeOrder->id
                                                ]
                                            ); ?>"
                                               onclick="return true; //self=this;TS.confirm('<?= $confirmMessage; ?>',function(btn){ if(btn==='ok'){ location.href=self.href}});return false;"
                                               class="btn btn-primary btn-ghost btn-sm m-r20 m-b10"
                                            >
                                                <span class="tsi tsi-thumbs-up m-r10"></span> <?= _t("site.order", "Like results"); ?>
                                            </a>

                                            <a href="<?= Url::toRoute(
                                                [
                                                    '/workbench/order/client-accept',
                                                    'attemptId' => $attempt->id,
                                                    'orderId'   => $storeOrder->id,
                                                    'dislike'   => 1
                                                ]
                                            ); ?>"
                                               onclick="self=this;TS.confirm('<?= _t('site.order', 'Are you sure?'); ?>', function(btn){ if(btn==='ok'){ location.href=self.href}}, {
                                                       confirm: '<?= _t('site.order', 'Yes'); ?>',
                                                       dismiss: '<?= _t('site.order', 'No'); ?>'
                                                       });return false;"
                                               class="btn btn-info btn-ghost btn-sm m-b10" style="color:#505458;"
                                            >
                                                <span class="tsi tsi-thumbs-down m-r10"></span> <?= _t("site.order", "Dislike results"); ?>
                                            </a>

                                        <?php }
                                    }
                                }
                            } ?>
                        </div>
                    </div>
                <?php endif; ?>


                <!-- delivery block start -->
                <?php
                $showDeliveryInfoFlag = $attempt && $storeOrder->isPayed() && !$attempt->isResolved() && $storeOrder->deliveryType;
                if ($showDeliveryInfoFlag
                    && $storeOrder->deliveryType
                    && $storeOrder->deliveryType->code === DeliveryType::PICKUP && $attempt->machine
                    && !$attempt->isNew()
                ) {
                    $deliveryTitle  = _t('site.order', 'Pickup details');
                    $printerAddress = UserAddress::getAddressFromLocation($attempt->machine->location);
                    $address        = str_replace(
                        " ,",
                        ',',
                        str_replace("off<br />", ",", UserAddress::formatAddress($printerAddress, true))
                    );
                    $pickupDelivery = $attempt->machine->getPsDeliveryTypeByCode(DeliveryType::PICKUP);
                    ?>
                    <div class="order__body order__body--shipping">
                        <h4 class="m-t0"><?= _t('site.order', 'Pickup details'); ?></h4>
                        <table class="order__info-table">
                            <tr>
                                <td class="order__info-table-label"><?= _t('site.order', 'Pickup address'); ?>:</td>
                                <td class="order__info-table-info"><?= $address; ?></td>
                            </tr>
                            <tr>
                                <td class="order__info-table-label"><?= _t('site.order', 'Phone number'); ?>:</td>
                                <td class="order__info-table-info">
                                    +<?php echo H($attempt->ps->phone_code) . ' ' . H($attempt->ps->phone); ?></td>
                            </tr>
                            <tr>
                                <td class="order__info-table-label"><?= _t('site.order', 'Working hours'); ?>:</td>
                                <td class="order__info-table-info"><?= H($pickupDelivery->comment ?? ''); ?></td>
                            </tr>
                        </table>
                    </div>
                <?php } elseif (($storeOrder->deliveryType && $storeOrder->deliveryType->code !== DeliveryType::PICKUP) || ($attempt && $attempt->delivery)) {
                    $address = $storeOrder->shipAddress ? str_replace("ff<br />", ",", UserAddress::formatAddress($storeOrder->shipAddress, true)) : null;
                    ?>
                    <div class="order__body order__body--shipping">
                        <h4 class="m-t0"><?= _t('site.order', 'Shipping details'); ?></h4>
                        <table class="order__info-table">
                            <?php if ($address) : ?>
                                <tr>
                                    <td class="order__info-table-label"><?= _t('site.order', 'Shipping address'); ?>:</td>
                                    <td class="order__info-table-info"><?= (str_replace(" ,", ",", $address)); ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if (!empty($attempt->delivery->tracking_shipper) && !empty($attempt->delivery->tracking_number)) : ?>
                                <?php if (!empty($attempt->delivery->tracking_shipper)) : ?>
                                    <tr>
                                        <td class="order__info-table-label"><?= _t('site.order', 'Shipping Company'); ?>:</td>
                                        <td class="order__info-table-info"><?= H($attempt->delivery->tracking_shipper); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <?php if (!empty($attempt->delivery->tracking_number)) : ?>
                                    <tr>
                                        <td class="order__info-table-label"><?= _t('site.order', 'Tracking Number'); ?>:</td>
                                        <?php if($attempt->delivery->public_url):?>
                                            <td class="order__info-table-info"><?= Html::a(H($attempt->delivery->tracking_number),$attempt->delivery->public_url,['target'=>'_blank'])?></td>
                                        <?php else:?>
                                            <td class="order__info-table-info"><?= H($attempt->delivery->tracking_number); ?></td>
                                        <?php endif;?>
                                    </tr>
                                <?php endif; ?>
                            <?php endif; ?>
                        </table>
                    </div>
                <?php } ?>
                <!-- delivery block end -->
                <div class="order__footer">
                    <?php
                    if ($storeOrder->is_dispute_open) {
                        ?>
                        <a class="btn  btn-success btn-sm"
                           href="<?= OrderUrlHelper::customerCloseDispute($storeOrder) ?>"
                           data-confirm="<?= _t('site.order', 'Are you sure you want to close the dispute?') ?>" data-method="post">
                            <?= _t('front.user', 'Close Dispute') ?>
                        </a>
                    <?php } elseif ($storeOrder->allowOpenDispute()) { ?>
                        <a class="btn btn-ghost btn-default btn-sm"
                           href="<?= OrderUrlHelper::customerOpenDispute($storeOrder); ?>"
                           data-confirm="<?= _t('site.order', 'Are you sure you want to open a dispute?') ?>" data-method="post">
                            <?= _t('front.user', 'Open Dispute') ?>
                        </a>
                    <?php } ?>

                    <?php if ($storeOrder->canUserRemove()) { ?>
                        <a href="/workbench/order/remove/<?= $storeOrder->id; ?>"
                           onclick="self=this;TS.confirm(_t('site.order', 'Are you sure you want to remove this order?'),function(btn){ if(btn==='ok'){ location.href=self.href}});return false;"
                           class="btn btn-ghost btn-default btn-sm">
                            <span class="tsi tsi-bin"></span>
                            <?= _t('site.order', 'Delete'); ?>
                        </a>
                    <?php } else {
                        if ($storeOrder->canUserCancel()) { ?>
                            <a href="/workbench/order/cancel/<?= $storeOrder->id; ?>" jsCallback='window.location.reload()'
                               class="btn  ts-ajax-modal btn-ghost btn-default btn-sm"
                               title="<?= _t('user.order', 'Cancel order'); ?>">
                                <span class="tsi tsi-bin"></span>
                                <?= _t('site.order', 'Cancel Order'); ?>
                            </a>
                        <?php }
                    } ?>
                    <?php if($storeOrder->getFirstReplicaItem()):?>
                        <a class="btn btn-ghost btn-default btn-sm"
                           href="/workbench/order/repeat/<?= $storeOrder->id; ?>">
                            <?= _t('front.user', 'Repeat order') ?>
                        </a>
                    <?php endif;?>
                    <?php if ($storeOrder->isPayed() && $storeOrder->order_state != StoreOrder::STATE_CLOSED) : ?>
                        <a href="<?= Url::toRoute(['/payment/receipt/order/', 'orderId' => $storeOrder->id]); ?>"
                           target="_blank" class="btn btn-default btn-sm order__receipt-btn">
                            <span class="tsi tsi-doc"></span>
                            <?= _t('user.order', 'Receipt'); ?>
                        </a>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
</div>
