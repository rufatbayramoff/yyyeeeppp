<?php
/** @var Preorder $preorder */
/** @var \yii\web\View $this */

use common\models\Preorder;
use common\modules\informer\InformerModule;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserUtils;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\workbench\components\MessageUrlHelper;
use frontend\modules\workbench\models\view\PreorderViewModel;


$viewPreorder = PreorderViewModel::fill($preorder);

?>

<div class="order-block">
    <?php if ($viewPreorder->isRejected() && $viewPreorder->getDeclineReason()):?>
        <div class="order-block__status bg-warning bar">
            <?php echo $viewPreorder->getDeclineReason(); ?>
        </div>
    <?php endif; ?>

    <div class="row order__info">
        <div class="order__title">
            <h3 class="order__number">
                <a href="<?= PreorderUrlHelper::viewCustomer($preorder)?>" class="relative p-r5">
                    <?= _t('site.preorder', 'Quote #{id}', ['id' => $preorder->id])?>
                    <?php if ($preorder->informerCustomerPreorders) {?>
                        <div class="service-order-row__notification-label"></div>
                    <?php } ?>
                </a>
            </h3>
            <div class="order__date"><?= _t('site.ps', 'Created'); ?>: <?= \Yii::$app->formatter->asDatetime($preorder->created_at); ?></div>
        </div>

        <div class="order__state">
            <div class="order__status-row">
                <div class="order__payment-status">
                    <div class="order__status">
                        <?= _t('user.order', 'Status'); ?>:
                        <strong><?= $preorder->getLabeledStatus(); ?></strong>
                    </div>
                    <p>
                        <b><?= _t('site.order', 'Deadline:'); ?></b>
                        <?= _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}', ['n' => (int)$preorder->offer_estimate_time]); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row order__ps-info">
        <div class="col-sm-5 order__ps-desc">
            <a class="order__ps-avatar" href="<?= PsFacade::getPsLink($preorder->ps)?>" target="_blank">
                <?= \yii\helpers\Html::img(\common\models\Ps::getCircleImageByPs($preorder->ps, 64,64));  ?>
            </a>
            <div class="order__ps-name">
                <h4 class="order__ps-title">
                    <a href="<?= PsFacade::getPsLink($preorder->ps);?>" target="_blank">
                        <?= H($preorder->ps->title);?>
                        <?= $preorder->is_interception? _t('site.ps', 'Customer service'):''; ?>
                    </a>
                </h4>
                <?php $msgTopic = $preorder->messageTopic;?>
                <?php if($msgTopic):?>
                    <div class="order__ps-message">
                        <a href="<?php echo MessageUrlHelper::topic($msgTopic)?>" class="btn btn-default btn-circle" target="_blank">
                            <span class="tsi tsi-message"></span>
                        </a>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>

    <?php if($preorder->offered): ?>
        <?php echo $this->render('../preorder/preorder-invoice-table', ['preorder' => $preorder, 'showTotal' => true, 'calculateAndShowFee' => true])?>
    <?php endif; ?>

    <div class="order__footer border-t">
        <a class="btn btn-primary btn-sm" href="<?= PreorderUrlHelper::viewCustomer($preorder)?>">
            <?= _t('site.ps', 'Quote details')?>
        </a>
    </div>

</div>
<?php

InformerModule::markAsViewedCustomerQuoteInformer($preorder);