<?php

use common\models\CuttingPack;

/** @var CuttingPack $cuttingPack */
$cuttingPreviewUrl = $cuttingPack->getPreviewUrl();

?>
<div class="row">
    <div class="col-md-7">
    <div class="order-model-data__model-pic order-model-data__cutting-pic cutting-preview text-center">
        <img src="<?= HL($cuttingPreviewUrl) ?>" ng-click="popupImageView($event)">
    </div>

    <h4 class="order-model-data__model-title ugc-content">
        <?= H($cuttingPack->getTitle()); ?>
    </h4>

    <table class="order__info-table order-model-data__model-table">
        <tbody>
        <tr>
            <td class="order__info-table-label"><?= _t('site.order', 'Quantity'); ?></td>
            <td class="order__info-table-info">
                <?php
                $total = $cuttingPack->getTotalQty();
                echo $total . ' ';
                if ($total == 1) {
                    echo _t('site.order', 'item');
                } else {
                    echo _t('site.order', 'items');
                }
                ?>
            </td>
        </tr>
        <?php
        if ($cuttingPack->material) {
            ?>
            <tr>
                <td class="order__info-table-label"><?= _t('site.order', 'Material'); ?></td>
                <td class="order__info-table-info">
                    <?= H($cuttingPack->material->title) ?>
                </td>
            </tr>
            <?php
        }
        ?>
        <?php
        if ($cuttingPack->color) {
            ?>
            <tr>
                <td class="order__info-table-label"><?= _t('site.order', 'Color'); ?></td>
                <td class="order__info-table-info">
                    <?= H($cuttingPack->color->title) ?>
                </td>
            </tr>
            <?php
        }
        ?>
        <?php
        if ($cuttingPack->thickness) {
            ?>
            <tr>
                <td class="order__info-table-label"><?= _t('site.order', 'Thickness'); ?></td>
                <td class="order__info-table-info">
                    <?= H($cuttingPack->thickness) ?> mm
                </td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td class="order__info-table-label"><?= _t('site.order', 'Total cutting length'); ?></td>
            <td class="order__info-table-info">
                <?= H($cuttingPack->getTotalCuttingLength()) ?> mm
            </td>
        </tr>
        <tr>
            <td class="order__info-table-label"><?= _t('site.order', 'Total engrave length'); ?></td>
            <td class="order__info-table-info">
                <?= H($cuttingPack->getTotalEngraveLength()) ?> mm
            </td>
        </tr>
        </tbody>
    </table>
    </div>
    <div class="col-md-5">
        <?=$receiptInfo?>
    </div>
</div>
<div class="row">
    <hr class="m-t0 m-b0">
    <div class="col-md-12">
        <h4 class="ugc-content"><?=_t('site.cutting', 'Cutting parts')?></h4>
    </div>
<?php
foreach ($cuttingPack->getActiveCuttingParts() as $cuttingPart) {
    echo $this->renderFile('@frontend/modules/workbench/views/service-orders/partial/order/item-cutting-file-part.php', ['cuttingPackPart' => $cuttingPart]);
}
?>
</div>
