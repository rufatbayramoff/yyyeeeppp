<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrder */
/* @var $form yii\widgets\ActiveForm */
/* @var $cancelForm mixed*/
?>


<div class="store-order-cancel-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php
    $form->field($cancelForm, "orderId", ['template' => '{input}'])->hiddenInput();
    echo $form->field($cancelForm, 'declineReason')->textarea(['maxlength' => true]);
    ?>
    <div class="form-group">
        <?= Html::submitButton(_t('front.order', 'Decline'), ['class' => 'btn btn-danger ts-ajax-submit']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
