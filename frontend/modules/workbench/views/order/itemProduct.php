<?php
?>
    <div class="order-model-data__model-pic text-center">
        <a href="<?= $product->getPublicPageUrl() ?>" target="_blank">
            <img src="<?= $product->getCoverUrl() ?>" width="100">
        </a>
    </div>

    <h4 class="order-model-data__model-title ugc-content">
        <?= H($product->title); ?>
    </h4>


