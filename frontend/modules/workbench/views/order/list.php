<?php

use common\models\message\helpers\UrlHelper;
use common\models\Preorder;
use frontend\assets\DropzoneAsset;
use frontend\assets\SocialButtonsAsset;
use frontend\assets\SwiperAsset;
use frontend\models\store\MyPurchasesForm;
use frontend\modules\workbench\widgets\CustomerPurchasesSidebarWidget;
use yii\bootstrap\ActiveForm;
use yii\data\BaseDataProvider;
use yii\helpers\Html;
use yii\widgets\ListView;

/** @var \yii\web\View $this */
/** @var $dataProvider \yii\data\BaseDataProvider */
/** @var $form MyPurchasesForm */
$statusGroup = $form->statusGroup;
$currentUser = $form->user;

Yii::$app->angular
    ->controller('my/orders/list')
    ->service(['modal', 'notify', 'router', 'user'])
    ->controllerParams(['x' => 'y']);


$this->title = _t('site.orders', 'My Purchases');
echo $this->renderFile('@frontend/views/templates/viewImagePopup.php');

?>
<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(['section' => 'workbench/orders']); ?>

<div class="container" id="orderReviewCtrl" ng-controller="MyOrdersListController">

    <div class="row order-page">
        <div class="col-sm-3 sidebar">
            <div class="side-nav-mobile">
                <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navPurchases">
                    <span id="js-section-title"><?= _t('site.ps', 'Orders Status') ?></span>
                    <span class="tsi tsi-down"></span>
                </div>
                <?= \frontend\modules\workbench\widgets\CustomerPurchasesSidebarWidget::widget([]); ?>
            </div>

            <h3 class="m-t0 hidden-xs">
                <?= _t('site.store', 'Need help?'); ?>
            </h3>
            <p class="m-b30 hidden-xs">
                <?php
                $supportLink = Html::a(_t('front.user', 'support'),
                    UrlHelper::supportMessageRoute(null), ['target' => '_blank']);
                echo _t('site.store', 'Contact {support} to find out more information about an order.', ['support' => $supportLink]); ?>
            </p>

        </div>

        <div class="col-sm-9 wide-padding wide-padding--left">
            <div class="">
                <?php $filterForm = ActiveForm::begin([
                    'action'                 => ['/workbench/orders/' . $statusGroup],
                    'method'                 => 'get',
                    'enableClientValidation' => false,
                    'id'                     => 'orders-filter-form',
                ]); ?>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group field-ordersearch">
                            <div class="input-group">
                                <input type="text"
                                       class="form-control"
                                       value="<?= H($form->orderSearch); ?>"
                                       name="orderSearch"
                                       placeholder="<?= $statusGroup == MyPurchasesForm::STATUS_QUOTES ? _t('site.order', 'Search by quote number or supplier name') : _t('site.order', 'Search by order number or supplier name'); ?>"
                                >
                                <span class="input-group-btn">
                                    <button type="submit" id="search-button" class="btn btn-default p-l0 p-r0"><i class="tsi tsi-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <?php if ($statusGroup === MyPurchasesForm::STATUS_QUOTES) : ?>
                        <div class="form-group">
                            <select id="status-code" class="form-control" name="quoteStatus" onchange="this.form.submit()">>
                                <option value=""><?= _t('front.site', 'All'); ?></option>
                                <?php
                                   foreach (Preorder::getStatusFilterForCustomer() as $quoteStatusCode=> $title) {
                                ?>
                                       <option value="<?=$quoteStatusCode?>" <?= $form->quoteStatus==$quoteStatusCode? 'selected':'' ?>><?= $title ?></option>
                                <?php
                                   }
                                ?>
                            </select>
                            <p class="help-block help-block-error"></p>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <?php if ($statusGroup === MyPurchasesForm::STATUS_QUOTES && !$currentUser->getIsActive()) : ?>
                <div class="alert alert-warning alert--text-normal" role="alert">
                    <?php echo _t(
                        'site.preorder.confirm',
                        "We’ve sent you a confirmation email. Please click the link in that email to have your RFQ submitted to the supplier."
                    ); ?>
                </div>
            <?php endif; ?>

            <?php if (!$dataProvider->getCount()) : ?>
                <h3 class="text-muted" style="margin-top: 5px;">
                    <?= $statusGroup === MyPurchasesForm::STATUS_QUOTES ?
                        _t('site.store', 'Not found') :
                        _t('site.store', 'No orders found');
                    ?>
                </h3>
            <?php else : ?>
                <?php
                $listView = Yii::createObject([
                    'class'        => ListView::class,
                    'dataProvider' => $dataProvider,
                    'itemView'     => 'listItem',
                    'viewParams'   => [
                        'currentUser' => $form->user
                    ]
                ]);
                ?>
                <?= $listView->renderItems() ?>
                <div class="row">
                    <div class="col-md-8">
                        <?= $listView->renderPager() ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
