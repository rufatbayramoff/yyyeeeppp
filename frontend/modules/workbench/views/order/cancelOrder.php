<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreOrder */
/* @var $form yii\widgets\ActiveForm */
/* @var $cancelForm frontend\models\order\CancelOrderForm */
?>

<style>
    .store-order-cancel-form select {
        display: block !important;
    }
</style>

<div class="store-order-cancel-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php
    echo $form->field($cancelForm, 'reasonId')->widget(kartik\widgets\Select2::classname(), [
        'data' => $cancelForm->getSuggestList(),
        'options' => ['placeholder' => _t('front.site', 'Select'), 'style' => 'width:100%; display: block;'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    echo $form->field($cancelForm, 'reasonDescription')->textarea(['maxlength' => true]);
    ?>
    <div class="form-group text-right">
        <?= Html::submitButton(_t('front.order', 'Cancel order'), ['class' => 'btn btn-danger ts-ajax-submit m-r15']) ?>
        <?= Html::button(_t('front.order', 'Changed my mind'), ['class' => 'btn btn-default', 'data-dismiss'=>'modal' ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>