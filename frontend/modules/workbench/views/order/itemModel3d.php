<?php


use common\components\ArrayHelper;
use common\models\Model3dReplica;
use frontend\models\model3d\Model3dFacade;
use frontend\models\ps\PsFacade;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $model3d Model3dReplica */
/** @var $showParts bool */
/** @var $attempt \common\models\StoreOrderAttemp */
/** @var $receiptInfo string */

$texture = null;
if ($model3d->isOneTextureForKit()) {
    $texture = $model3d->getKitTexture();
}

$orderItemLink = Url::toRoute(
    [
        '/store/model3d-replica',
        'id' => $model3d->id,
    ]
);

?>
<div class="row">
    <div class="col-md-7">
        <div class="order-model-data__model-pic text-center">
            <a href="<?= $orderItemLink ?>" target="_blank">
                <?php if ($cover = Model3dFacade::getCover($model3d)) : ?>
                    <div class="order-model-data__model-pic"><?= Html::img($cover['image'], ['alt' => $model3d->getTitle(), 'width' => 100]) ?></div>
                <?php endif ?>
            </a>
            <br/>
            <a href="<?= $orderItemLink ?>" target="_blank">3D Viewer</a>
        </div>

        <h4 class="order-model-data__model-title ugc-content">
            <?= H($model3d->getTitle()); ?>
        </h4>

        <table class="order__info-table order-model-data__model-table">
            <tbody>
            <tr>
                <td class="order__info-table-label"><?= _t('site.order', 'Quantity'); ?></td>
                <td class="order__info-table-info">
                    <?php
                    $total = array_sum(ArrayHelper::getColumn($model3d->getCalculatedModel3dParts(), 'qty'));
                    echo $total . ' ';
                    if ($total == 1) {
                        echo _t('site.order', 'item');
                    } else {
                        echo _t('site.order', 'items');
                    }
                    ?>
                </td>
            </tr>

            <?php if ($attempt && $attempt->hasMachine()) : ?>
                <?php
                $layerResolution = PsFacade::getPrinterLayerResolution($attempt->machine);
                if ($layerResolution) :?>
                    <tr>
                        <td class="order__info-table-label"><?= _t('site.printer', 'Layer Resolution'); ?></td>
                        <td class="order__info-table-info p-r10">
                            <small><?= $layerResolution; ?></small>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endif; ?>

            <?php
            if ($model3d->isOneTextureForKit()) {
                $texture = $model3d->getKitTexture();
                ?>
                <?php if ($texture->calculatePrinterMaterialGroup()): ?>
                    <tr>
                        <td class="order__info-table-label"><?= _t('site.order', 'Infill'); ?></td>
                        <td class="order__info-table-info"><?= $texture->infillText(); ?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td class="order__info-table-label"><?= _t('site.order', 'Material & Color'); ?></td>
                    <td class="order__info-table-info">
                        <?= $texture && $texture->printerMaterial ? H($texture->printerMaterial->title) : ''; ?>
                        <?php
                        if ($texture->printerColor) {
                            ?>
                            <div class="material-item">
                                <div class="material-item__color"
                                     style="background-color: rgb(<?= ($texture->printerColor->rgb); ?>)"></div>
                                <div class="material-item__label">
                                    <?= $texture && $texture->printerColor ? $texture->printerColor->render_color : ''; ?></div>
                            </div>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
            <?php } else { ?>
                <tr>
                    <td class="order__info-table-label"><?= _t('site.order', 'Material & Color'); ?></td>
                    <td class="order__info-table-info">
                        <?= _t('site.model3d', 'Different materials/colors'); ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-5">
        <?= $receiptInfo ?? '' ?>
    </div>
</div>

<?php
if (!empty($showParts)) {
    echo $this->render('itemModel3dParts', ['model3dReplica' => $model3d]);
}
?>
