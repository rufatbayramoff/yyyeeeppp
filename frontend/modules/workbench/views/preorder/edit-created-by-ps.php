<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 14.06.19
 * Time: 14:23
 */

use common\components\FileTypesHelper;
use common\models\Preorder;
use common\models\User;
use common\modules\company\serializers\CompanySerializer;
use common\modules\company\serializers\CompanyServiceSerializer;
use common\modules\product\serializers\ProductSerializer;
use frontend\assets\DropzoneAsset;
use frontend\modules\preorder\components\PreorderSerializer;
use frontend\modules\preorder\components\ProductPreorderDeclineForm;
use frontend\modules\preorder\components\ServicesPreorderDeclineForm;
use frontend\modules\workbench\components\MessageUrlHelper;
use frontend\modules\workbench\models\view\PreorderViewModel;
use frontend\modules\workbench\widgets\CompanySalesSidebarWidget;
use kartik\select2\Select2Asset;
use kartik\select2\ThemeKrajeeAsset;
use yii\jui\JuiAsset;
use yii\web\View;

/** @var View $this */
/** @var Preorder $preorder */
/** @var User $currentUser */

$feePercent = 0.5;


$preorderView       = PreorderViewModel::fill($preorder);
$preorderSerialized = PreorderSerializer::serialize($preorder);
$productsList       = ProductSerializer::serialize($preorder->company->products);
$servicesList       = CompanyServiceSerializer::serialize($preorder->company->companyServices);

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->constant('preorder', $preorderSerialized)
    ->directive([
        'dropzone-button',
        'select2',
        'autocomplete'
    ])
    ->controllerParams(
        [
            'company'              => CompanySerializer::serialize($preorder->company),
            'products'             => $productsList,
            'services'             => $servicesList,
            'productTypesSingular' => \common\models\Product::getUnitTypesList(false),
            'productTypesPlural'   => \common\models\Product::getUnitTypesList(true)
        ])
    ->constants([
        'servicesPreorderDeclineReasons' => ServicesPreorderDeclineForm::getSuggestObjectList(),
        'productPreorderDeclineReasons'  => ProductPreorderDeclineForm::getSuggestObjectList(),
    ])
    ->controller([
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service',
        'preorder/preorder-view-ps',
        'preorder/preorder-fee',
        'product/commonLib',
        'product/userVideo',
        'product/productModels',
        'store/common-models',
        'store/filepage/printer-models'
    ]);

$this->registerAssetBundle(DropzoneAsset::class);
$this->registerAssetBundle(Select2Asset::class);
$this->registerAssetBundle(ThemeKrajeeAsset::class); // Select2 style
$this->registerAssetBundle(JuiAsset::class); // Autocompleate

echo $this->render('templates/order-decline-modal.php');
?>
<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(); ?>

    <div class="container" ng-controller="PreorderViewPsController" ng-cloak="">
        <div class="row ps-index">
            <div class="col-md-12">
                <div class="tab-content">

                    <div class="row">

                        <?= CompanySalesSidebarWidget::widget(['ps' => $currentUser->company, 'activeStatusGroup' => CompanySalesSidebarWidget::ACTIVE_STATUS_GROUP_QUOTE]) ?>

                        <div class="col-sm-8 wide-padding wide-padding--left">
                            <!-- 4592 INNER Preorder Quote Print Service Request Sample -->
                            <div class="panel panel-default box-shadow border-0">
                                <div class="one-print-request ng-scope">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="one-print-request__user">
                                                            <h3 style="margin: 0">
                                                                <?= _t('site.ps', 'Create preorder'); ?> #{{preorder.id}}
                                                            </h3>
                                                            <div class="p-t10">
                                                                <strong><?= _t('site.preorder', 'Status') ?></strong>: {{preorder.labeledStatus}}
                                                                <span ng-show="preorder.isReoffer()">
                                                                    <div data-toggle="tooltip" data-placement="bottom" title="<?=_t('site.ps', 'The client has declined the offer. Please re-submit if needed');?>" data-original-title="<?=_t('site.ps', 'The client has declined the offer. Please re-submit if needed');?>" class="tsi tsi-warning-c"></div>
                                                                </span>
                                                                <div ng-show="(preorder.isReoffer() || preorder.isDeclined()) && preorder.declineReason" class="bg-warning">
                                                                    <?php echo _t('site.preorder', 'Reason: ');?>
                                                                    {{preorder.declineReason}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-6 text-center">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body p-t0">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12" ng-if="preorder.user.businessName">

                                                <div class="one-print-request__user">

                                                    <div class="one-print-request__user__avatar">
                                                        <img ng-src="{{preorder.user.avatarUrl}}">
                                                    </div>
                                                    <?php $msgTopic = $preorder->messageTopic; ?>
                                                    <?php if (!$preorder->isSelfPreorder() && $preorder->user_id && $msgTopic) : ?>
                                                        <div class="one-print-request__user__message">
                                                            <a target="_blank" href="<?php echo MessageUrlHelper::topic($msgTopic) ?>" class="btn btn-default btn-circle" type="button">
                                                                <span class="tsi tsi-message"></span>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                    <div class="one-print-request__user__info m-t10">
                                                        <a ng-if="!preorder.user.isUnactive()" ng-href="{{preorder.user.profileUrl}}" target="_blank">
                                                            <strong>{{preorder.user.businessName}}</strong>
                                                        </a>
                                                        <strong ng-if="preorder.user.isUnactive()">{{preorder.user.businessName}}</strong>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body p-b0 one-print-request__body" ng-if="preorder.user.businessName || preorder.description">
                                        <div class="row">
                                            <div class="col-md-8" ng-if="preorder.description">
                                                <h4 class="m-t0"><?= _t('site.ps', 'Project Description'); ?></h4>
                                                <p>{{preorder.description}}</p>
                                            </div>
                                            <div class="col-md-8" ng-if="preorder.shipAddress">
                                                <h4 class="m-t0"><?= _t('site.ps', 'Shipping Address'); ?></h4>
                                                <p ng-bind-html="shipAddressHtml()"></p>
                                            </div>
                                            <div class="col-md-4" ng-if="1">
                                                <strong class="m-t0 ugc-content"><?= _t('site.ps', 'Service'); ?>:</strong> {{preorder.serviceTitle}}
                                            </div>
                                            <div class="col-md-4" ng-if="preorder.productSnapshot">
                                                <h4 class="m-t0 ugc-content"><?= _t('site.ps', 'Product'); ?>: {{preorder.productSnapshot.title}}</h4>
                                                <img style="max-width: 100px; max-height: 100px; margin-bottom: 10px;border-radius: 5px"
                                                     ng-src="{{preorder.productSnapshot.coverUrl}}">
                                            </div>
                                            <div class="col-md-4" ng-if="preorder.productSnapshot">
                                                <p class="m-b10"><strong><?= _t('site.ps', 'Price'); ?>:</strong>
                                                    {{preorder.productPrice.amountWithCurrency()}} / {{preorder.productSnapshot.unitType}}</p>
                                            </div>
                                            <div class="col-md-4" ng-if="preorder.productSnapshot">
                                                <p class="m-b10"><strong><?= _t('site.ps', 'Total price'); ?>:</strong> {{preorder.productTotalPrice.amountWithCurrency()}}</p>
                                            </div>
                                            <div class="col-md-4" ng-if="preorder.qty">
                                                <p class="m-b10"><strong><?= _t('site.ps', 'Quantity'); ?>:</strong> {{preorder.qty | number}}
                                                    {{getUnitTypeLabel(preorder.productSnapshot.unitType, preorder.qty)}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="m-b10" ng-if="preorder.budget">
                                                    <strong><?= _t('site.ps', 'Budget'); ?>:</strong>
                                                    <?= displayAsMoney($preorder->getBudgetMoney()) ?>

                                                    <?php
                                                    $platformFee = \lib\money\Money::usd($preorder->budget * $feePercent);
                                                    $note        = _t(
                                                        'site.ps',
                                                        "Customer will pay {price1}. Platform fee is {price2}. Your income will be {price3}",
                                                        [
                                                            'price1' => displayAsMoney(\lib\money\Money::usd($preorder->budget)),
                                                            'price2' => displayAsMoney($platformFee),
                                                            'price3' => displayAsMoney(\lib\money\Money::usd($preorder->budget - $platformFee->getAmount()))
                                                        ]
                                                    );
                                                    #echo sprintf('<span data-toggle="tooltip" data-placement="top" data-original-title="%s" class="tsi tsi-warning-c"></span>', $note);
                                                    ?>
                                                </p>
                                                <p class="m-b0" ng-if="preorder.estimate_time">
                                                    <strong><?= _t('site.ps', 'Deadline'); ?>:
                                                    </strong>

                                                    <?= _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}',
                                                        ['n' => (int)$preorder->offer_estimate_time]); ?>
                                                </p>
                                            </div>

                                            <div class="col-md-12" ng-if="preorder.files.length">
                                                <h4 class="m-t0"><?= _t('site.ps', 'Attached Files'); ?></h4>

                                                <div class="simple-file-link" ng-repeat="file in preorder.files">
                                                    <a class="simple-file-link__link" ng-href="{{getDownloadFileLink(preorder, file)}}">
                                                        <span>{{file.name}}</span>
                                                        <i>{{file.size}}</i>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row" ng-if="!preorder.user.businessName">
                                            <div class="col-md-12 col-lg-12">
                                                <h4 class="m-t0">
                                                    <?= _t('site.preorder', 'Customer') ?>
                                                </h4>
                                            </div>
                                            <div class="col-md-4 col-lg-3">
                                                <select
                                                        class="form-control m-b10"
                                                        id="select"
                                                        ng-model="invitePreorder"
                                                        ng-change="inviteChange(invitePreorder)"
                                                        ng-options="customer.id as customer.name for customer in customers">
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <div ng-if="isInviteEmail()">
                                                    <input type="email" class="form-control" id="preorderEmail" placeholder="<?= _t('site.preorder', 'Email') ?>"
                                                           email-validator
                                                           ng-model="preorder.email">
                                                    <div class="help-block">
                                                        <?= _t('site.preorder', 'Invite your customer to Treatstock') ?>
                                                    </div>
                                                </div>
                                                <div ng-if="isInviteUser()">
                                                    <select class="form-control js-select-user-search" ng-init="initCustomerSearch()">

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row" ng-if="isShowAttachFiles()">
                                            <label class="control-label col-sm-3 col-md-2"><?= _t('site.preorder', 'Attach Files') ?></label>
                                            <div class="col-sm-9">

                                                <button type="button" class="btn btn-primary"
                                                        dropzone-button="preorder.files"
                                                        accepted-files="<?= '.' . implode(', .', FileTypesHelper::getAllowExtensions()) ?>"
                                                        max-files="20"><i class="tsi tsi-upload-l"></i> <?= _t('site.preorder', 'Browse files') ?></button>

                                                <p class="help-block"><?= _t('site.preorder', 'Maximum limit of') ?> 20 <?= _t('site.preorder', 'files') .
                                                    '. ' . _t('site.preorder', 'Max size: ') . app('setting')->get('upload.maxfilesize', 57) . ' Mb.' ?></p>
                                                <div>
                                                    <span ng-repeat="file in preorder.files" class="m-r10">{{file.name}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-body p-t0 p-b0">

                                        <div id="offer-block">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <label class="control-label" for="projDescr"><?= _t('site.preorder', 'Description') ?></label>
                                                    <textarea class="form-control m-b20" rows="3" id="projDescr" ng-model="preorder.offer_description"></textarea>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label" for="preorderTime"><?= _t('site.preorder', 'Estimated time') ?><span
                                                                    class="form-required">*</span></label>
                                                        <div class="input-group">
                                                            <input class="form-control" type="number" min="0" id="preorderTime" placeholder="<?= _t('site.preorder', 'Days') ?>"
                                                                   ng-model="preorder.offer_estimate_time">
                                                            <span class="input-group-addon"><?= _t('site.preorder', 'Days') ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Cost Table empty -->
                                            <div class="estimated-cost-empty"
                                                 ng-if="!preorder.works.length">
                                                <button type="button" class="btn btn-link btn-sm p-t0 p-b0 p-l0 p-r10" ng-click="addWork()">
                                                    <span class="tsi tsi-pencil"></span>
                                                    <?= _t('site.preorder', 'Create') ?>
                                                </button>
                                                <?= _t('site.preorder', 'a list of items and costs to include in your quote') ?>
                                            </div>
                                            <!-- // Cost Table empty -->

                                            <!-- Cost Table with content -->
                                            <div class="row"
                                                 ng-if="preorder.works.length">
                                                <div class="col-md-12 preorder-works-cnc" ng-if="preorder.isCalculate()">
                                                    <?php echo _t('site.preorder', 'This calculation is based on you CNC machine') ?>
                                                </div>
                                                <div class="preorder-works">

                                                    <div class="preorder-works__row preorder-works__row--head">
                                                        <div class="preorder-works__title"><?= _t('site.ps', 'Items'); ?><span class="form-required">*</span></div>
                                                        <div class="preorder-works__qty"><?= _t('site.ps', 'Quantity'); ?><span class="form-required">*</span></div>
                                                        <div class="preorder-works__cost"><?= _t('site.ps', 'Cost'); ?><span class="form-required">*</span></div>
                                                        <div class="preorder-works__controls"></div>
                                                    </div>

                                                    <div class="preorder-works__row" ng-repeat="work in preorder.works">

                                                        <!-- VIEW TD -->

                                                        <div class="preorder-works__title" ng-if="!work.$$isEdit">
                                                            <div class="preorder-works__mobile-label"><?= _t('site.ps', 'Item'); ?></div>

                                                            {{getWorkTextTitle(work)}}

                                                            <div class="estimated-cost-table__files"
                                                                 ng-if="work.files.length">
                                                                <?= _t('site.ps', 'Attachments'); ?>:
                                                                <a class="m-r10" ng-href="{{getDownloadWorkFileLink(preorder, work, file)}}" ng-repeat="file in work.files">{{file.name}}</a>
                                                            </div>

                                                        </div>
                                                        <div class="preorder-works__qty" ng-if="!work.$$isEdit">
                                                            <div class="preorder-works__mobile-label"><?= _t('site.ps', 'Quantity'); ?><span class="form-required">*</span></div>
                                                            {{work.qty | number}} <span>{{getWorkTextUnit(work, work.qty)}}</span></div>
                                                        <div class="preorder-works__cost" ng-if="!work.$$isEdit">
                                                            <div class="preorder-works__mobile-label"><?= _t('site.ps', 'Cost'); ?><span class="form-required">*</span></div>
                                                            {{work.cost | money}} {{preorder.currency}}
                                                        </div>

                                                        <!-- EDIT TD -->

                                                        <div class="preorder-works__title" ng-if="work.$$isEdit">
                                                            <div class="preorder-works__mobile-label"><?= _t('site.ps', 'Item'); ?></div>
                                                            <div ng-if="work.isProductType()" class="form-group m-b0">
                                                                <select class="form-control input-sm" ng-model="work.productUuid" ng-change="changeProduct(work)" select2>
                                                                    <?php foreach ($preorder->company->products as $product) {
                                                                        if (!$product->isAvailableInCatalog()) {
                                                                            continue;
                                                                        }
                                                                        ?>
                                                                        <option value="<?= $product->uuid ?>"><?= $product->getTitle() ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div ng-if="work.isServiceType()">
                                                                <autocomplete input-class="form-control input-sm"
                                                                              autocomplete-title="work.title"
                                                                              autocomplete-model="work.companyServiceId"
                                                                              autocomplete-elements="getWorkServicesAutocomplete()">
                                                            </div>
                                                            <div ng-if="work.isTextType()">
                                                                <input class="form-control input-sm" ng-model="work.title">
                                                            </div>

                                                            <div class="estimated-cost-table__files" ng-if="work.files.length">
                                                                <?= _t('site.ps', 'Attachments'); ?>:
                                                                <span class="m-r10" ng-repeat="file in work.files">{{file.name}}</span>
                                                            </div>
                                                        </div>
                                                        <div class="preorder-works__qty" ng-if="work.$$isEdit">
                                                            <div class="preorder-works__mobile-label"><?= _t('site.ps', 'Quantity'); ?><span class="form-required">*</span></div>
                                                            <input class="form-control input-sm" size="7" type="number" min="0" ng-model="work.qty"
                                                                   ng-change="changeQty(work)">
                                                        </div>
                                                        <div class="preorder-works__cost" ng-if="work.$$isEdit">
                                                            <div class="preorder-works__mobile-label"><?= _t('site.ps', 'Cost'); ?><span class="form-required">*</span></div>
                                                            <input class="form-control input-sm" size="7" type="number" min="0.01" step="0.01"
                                                                   ng-model="work.cost" ng-change="onUpdateProductCost()">
                                                        </div>

                                                        <!-- ACTION BUTTONS -->

                                                        <div class="preorder-works__controls">
                                                            <div ng-if="!work.$$loading">
                                                                <button
                                                                        ng-if="work.$$isEdit && preorder.isCanMakeOffer()"
                                                                        ng-click="saveWork(work)"
                                                                        class="btn btn-link btn-xs"
                                                                        type="button"><span class="tsi tsi-checkmark"> </span></button>

                                                                <button
                                                                        ng-if="!work.$$isEdit && preorder.isCanMakeOffer()"
                                                                        ng-click="editWork(work)"
                                                                        class="btn btn-link btn-xs"
                                                                        type="button"><span class="tsi tsi tsi-pencil"> </span></button>

                                                                <button ng-if="work.$$isEdit && preorder.isCanMakeOffer()"
                                                                        type="button"
                                                                        class="btn btn-link btn-xs"
                                                                        dropzone-button="work.files"
                                                                        accepted-files="<?= '.' . implode(', .', common\components\FileTypesHelper::getAllowExtensions()) ?>"
                                                                        max-files="20"><span class="tsi tsi tsi-paperclip"></span></button>

                                                                <button
                                                                        ng-if="preorder.isCanMakeOffer()"
                                                                        ng-click="deleteWork(work)"
                                                                        class="btn btn-link btn-xs"
                                                                        type="button"><span class="tsi tsi tsi-remove"> </span></button>

                                                            </div>

                                                            <span ng-if="work.$$loading" class="tsi tsi-spinner tsi-spin"></span>

                                                        </div>

                                                    </div>

                                                    <div class="preorder-works__row preorder-works__row--summary">
                                                        <div class="preorder-works__add">
                                                            <button class="btn btn-default btn-sm" type="button" ng-if="preorder.isCanMakeOffer()" ng-click="addProduct()">
                                                                <span class="tsi tsi-plus m-r10"> </span> <?= _t('site,preorder', 'Add product') ?>
                                                            </button>
                                                            <button class="btn btn-default btn-sm" type="button" ng-if="preorder.isCanMakeOffer()" ng-click="addService()">
                                                                <span class="tsi tsi-plus m-r10"> </span> <?= _t('site,preorder', 'Add service') ?>
                                                            </button>
                                                        </div>
                                                        <div class="preorder-works__total-cost">
                                                            <div><b><?= _t('site,preorder', 'Total cost') ?>: {{getCost() | money}} {{preorder.currency}}</b></div>
                                                            <div ng-if="!youWillGet">
                                                                <?= _t('site,preorder', 'Minimum cost is') ?> : {{getMinCost() | money}}  {{preorder.currency}}
                                                            </div>
                                                            <div ng-if="youWillGet">
                                                                <b><?= _t('site,preorder', 'You get') ?>: {{youWillGet | money}} {{preorder.currency}} </b>
                                                            </div>
                                                            <div ng-if="getBonusAccrued()">
                                                                <b><?= _t('site,preorder', 'Bonus accrued') ?>: +{{getBonusAccrued()}} </b>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                            </div>

                                            <div ng-if="quoteHasBeenEdited" class="alert alert-warning m-t20" role="alert">
                                                <?= _t('site.ps', 'Quote has been edited, please click Submit Quote to send new details.'); ?>
                                            </div>


                                            <!-- // Cost Table with content -->
                                            <div class="m-t20 m-b10">
                                                <button ng-if="preorder.isCanMakeOffer()" type="button" class="btn btn-success btn-sm m-r20 m-b10"
                                                        loader-click-unrestored="true"
                                                        loader-click="makeOffer()">
                                                    <span class="tsi tsi-checkmark"></span>
                                                    <?= _t('site.ps', 'Submit Quote'); ?>
                                                </button>
                                                <button ng-if="preorder.canSaveDraft()" type="button" class="btn btn-default btn-sm m-r20 m-b10"
                                                        loader-click-unrestored="true"
                                                        loader-click="saveDraft()">
                                                    <?= _t('site.ps', 'Save draft'); ?>
                                                </button>
                                                <button ng-if="preorder.canPsCancel()" type="button" class="btn btn-danger btn-sm m-r20 m-b10"
                                                        loader-click-unrestored="true"
                                                        loader-click="withdrawOffer()">
                                                    <?= _t('site.ps', 'Withdraw offer'); ?>
                                                </button>

                                                <button ng-if="preorder.canDelete()" type="button" class="btn btn-danger btn-sm pull-right m-b10"
                                                        loader-click-unrestored="true"
                                                        loader-click="deletePreorder()">
                                                    <?= _t('site.preorder', 'Delete') ?>
                                                </button>
                                                <button ng-if="preorder.canDecline()" type="button" class="btn btn-danger btn-sm pull-right m-b10"
                                                        loader-click-unrestored="true"
                                                        loader-click="declinePreorder()">
                                                    <?php echo _t('site.preorder', 'Decline') ?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-body one-print-request__footer" ng-class="{'border-t' : !preorder.isDeclined()}">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="send-print-request-date text-right" style="margin-top: 5px;">
                                                    <?= _t('site.ps', 'Created'); ?>: {{preorder.created_at}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        $('a[data-target]').click(function () {
            var scroll_elTarget = $(this).attr('data-target');
            if ($(scroll_elTarget).length != 0) {
                $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 115}, 350);
            }
            return false;
        });

        <?php $this->endBlock(); ?>
    </script>
<?php $this->registerJs($this->blocks['js1']); ?>
<?php
\common\modules\informer\InformerModule::markAsViewedCompanyOrderInformer($preorder->storeOrderAttempt);


