<?php

use common\models\StoreOrderAttemp;
use frontend\models\ps\DeclineCncPreorderByCustomerForm;
use frontend\models\ps\DeclineCncPreorderByPsForm;
use frontend\models\ps\DeclineOrderForm;
use frontend\models\ps\DeclineProductOrderForm;
use frontend\models\ps\StoreOrderAttempSearchForm;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\preorder\components\ProductPreorderDeclineForm;
use frontend\modules\preorder\components\ServicesPreorderDeclineForm;
use frontend\modules\workbench\widgets\CompanySalesSidebarWidget;
use yii\bootstrap\ActiveForm;

/** @var \yii\web\View $this */
/** @var StoreOrderAttempSearchForm $form */
/** @var StoreOrderAttemp[] $attemps Orders attemps */
/** @var \common\models\Preorder[] $preorders */

Yii::$app->angular
    ->service(['modal', 'mathUtils', 'measure', 'notify', 'router', 'user', 'cnc-service', 'psDownloadModel'])
    ->directive('datetime-picker')
    ->controller('ps/orders/list')
    ->controller('cnc/cnc-models')
    ->controller('cnc/cnc-models')
    ->controller('store/common-models')
    ->controller('preorder/preorder-models')
    ->controller('preorder/preorder-service')
    ->controller('order/store-order-service')
    ->controller('store/filepage/models')
    ->constants([
        'declineReasons'                    => DeclineOrderForm::getSuggestObjectList(),
        'cncPreorderDeclineReasons'         => DeclineCncPreorderByPsForm::getSuggestObjectList(),
        'cncPreorderDeclineCustomerReasons' => DeclineCncPreorderByCustomerForm::getSuggestObjectList(),
        'preorderDeclineReasons'            => ServicesPreorderDeclineForm::getSuggestObjectList(),
        'declineProductReasons'             => DeclineProductOrderForm::getSuggestObjectList(),
        'servicesPreorderDeclineReasons'    => ServicesPreorderDeclineForm::getSuggestObjectList(),
        'productPreorderDeclineReasons'     => ProductPreorderDeclineForm::getSuggestObjectList(),

    ])
    ->controllerParams([
        'x' => 'y'
    ]);

$this->title = 'My Sales';

?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(); ?>

    <div class="container" ng-controller="PsOrdersListController">
        <div class="row ps-index requests-list">
            <div class="col-md-12">

                <div class="tab-content">

                    <div class="row">

                        <?= CompanySalesSidebarWidget::widget(['ps' => $ps, 'activeStatusGroup' => CompanySalesSidebarWidget::ACTIVE_STATUS_GROUP_QUOTE]) ?>

                        <div class="col-sm-9 wide-padding--left">
                            <div class="orders-filter-form">
                                <?php $filterForm = ActiveForm::begin([
                                    'action'                 => [PreorderUrlHelper::viewPsList()],
                                    'method'                 => 'get',
                                    'enableClientValidation' => false,
                                    'id'                     => 'orders-filter-form',
                                ]); ?>

                                <div class="row m-b20">
                                    <div class="col-md-12 col-lg-5">
                                        <div class="form-group field-ordersearch">

                                            <div class="input-group">
                                                <input type="text" class="form-control" value="<?= H($orderSearch); ?>" name="orderSearch"
                                                       placeholder="<?= _t('site.order', 'Search by quote number or client name'); ?>">
                                                <span class="input-group-btn">
                                            <button type="submit" id="search-button" class="btn btn-default p-l0 p-r0"><i class="tsi tsi-search"></i></button>
                                            </span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="form-group">
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-lg-3">
                                        <div class="form-group">
                                            <button type="button" ng-click="createQuote()" class="btn btn-default p-l10 p-r10 orders-filter-form__create">
                                                <?= _t('site.quote', 'Create Quote'); ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>

                            <?php
                            foreach ($preorders as $preorder) {
                                echo $this->render('preorder-item.php', ['preorder' => $preorder]);
                            }
                            ?>

                            <?php if (!$preorders): ?>
                                <h3 class="no-print-requests m-l0 m-r0"><?= _t("site.ps", "No new quotes found"); ?></h3>
                            <?php endif ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?= $this->render('@frontend/modules/workbench/views/service-orders/modals/download-model-terms-modal'); ?>
<?php echo $this->render('templates/order-decline-modal.php'); ?>