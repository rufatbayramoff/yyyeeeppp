<?php
/**
 * @var \common\models\Preorder $preorder
 * @var  PreorderViewModel $viewPreorder
 */

use frontend\modules\workbench\models\view\PreorderViewModel;
use yii\helpers\ArrayHelper;

$viewPreorder = PreorderViewModel::fill($preorder);

?>

<div class="service-order-row service-order-row--quote">

    <?php if ($viewPreorder->isRejected() && $viewPreorder->getDeclineReason()): ?>
        <div class="service-order-row__status bg-warning bar">
            <?php echo $viewPreorder->getDeclineReason(); ?>
        </div>
    <?php endif; ?>

    <div class="service-order-row__title">
        <div class="service-order-row__num">
            <a href="<?php echo $viewPreorder->getUrlViewPs(); ?>">
                <?php echo $viewPreorder->getPreorderTitle(); ?>
            </a>
            <?php
            if ($viewPreorder->isCnc()) {
                echo " - <span class='service-order-row__cnc'>CNC</span>";
            }
            ?>
            <?php $informerStoreOrderAttempts = $viewPreorder->preorder->storeOrderAttempt->informerStoreOrderAttempts ?? false; ?>
            <?php if ($informerStoreOrderAttempts) { ?>
                <div class="service-order-row__notification-label"></div>
            <?php } ?>
        </div>
        <div class="service-order-row__deadline">
            <?php if ($viewPreorder->preorder->offer_estimate_time): ?>
                <strong><?= _t('site.preorder', 'Deadline') ?>:</strong>
                <span class="label label-info"><?php echo $viewPreorder->getOfferedEstimateTimeFormat(); ?></span>
            <?php elseif ($viewPreorder->preorder->estimate_time): ?>
                <strong><?= _t('site.preorder', 'Deadline') ?>:</strong>
                <span class="label label-info"><?php echo $viewPreorder->getEstimateTimeFormat(); ?></span>
            <?php endif; ?>

            <span class="label<?php echo $viewPreorder->isRejected() ? ' label-warning' : ' label-success'; ?>"
                  title="<?= _t('site.preorder', 'Status') ?>: <?php echo $viewPreorder->status; ?>">
                <?php echo $viewPreorder->status; ?>
            </span>
        </div>

        <div class="service-order-row__actions">
            <div class="dropdown">
                <button class="btn btn-default btn-sm dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    <svg class="svg-ico" xmlns="http://www.w3.org/2000/svg" id="dots3" data-name="dots3"
                         viewBox="0 0 16 4">
                        <circle cx="2" cy="2" r="2"></circle>
                        <circle cx="8" cy="2" r="2"></circle>
                        <circle cx="14" cy="2" r="2"></circle>
                    </svg>
                </button>

                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                    <li>
                        <a href="<?php echo $viewPreorder->getUrlViewPs(); ?>">
                            <span class="tsi tsi-eye text-primary"></span><?php echo _t('site.preorder', 'View quote') ?>
                        </a>
                    </li>
                    <?php if ($viewPreorder->isDraft()) { ?>
                        <li>
                            <a loader-click="deletePreorder(<?php echo $viewPreorder->preorder->id ?>)" href="javascript:void(0);">
                                <span class="tsi tsi-remove-c text-danger"></span><?php echo _t('site.preorder', 'Delete') ?>
                            </a>
                        </li>
                    <?php } elseif ($viewPreorder->allowDecline()) { ?>
                        <li>
                            <a loader-click="declinePreorder(<?php echo $viewPreorder->preorder->id ?>)" href="javascript:void(0);">
                                <span class="tsi tsi-remove-c text-danger"></span><?php echo _t('site.preorder', 'Decline') ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="service-order-row__data">

        <div class="service-order-row__data">
            <?php if ($viewPreorder->getPreorderImageUrl()) : ?>
                <a class="service-order-row__pic" href="<?= $viewPreorder->getUrlViewPs() ?>" target="_blank">
                <span class="service-order-row__pic-wrap">
                    <img src="<?= $viewPreorder->getPreorderImageUrl() ?>" width="80">
                 </span>
                </a>
            <?php else : ?>
                <?php if ($viewPreorder->isInterception()) : ?>
                    <a class="service-order-row__pic" href="<?= $viewPreorder->getCompanyPublicPage() ?>" target="_blank">
                <span class="service-order-row__pic-wrap">
                    <img src="<?= $viewPreorder->getCompanyLogo() ?>" width="80">
                    <?= H($viewPreorder->getCompanyTitle()) ?>
                 </span>
                    </a>
                <?php else : ?>
                    <div class="service-order-row__pic service-order-row__pic--empty">
                        <span>No Image</span>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>

        <div class="service-order-row__qty">
            <?php if ($viewPreorder->hasPreorderWorks()): ?>
                <div class="service-order-row__qty-badge" title="Quantity">×<?php echo $viewPreorder->getPreorderWorksQty(); ?></div>
            <?php endif; ?>
        </div>

        <div class="service-order-row__price">
            <?php if ($viewPreorder->hasPreorderWorks()): ?>
                <div class="text-muted"><?= _t('site.preorder', 'Total price') ?></div>
                <strong><?php echo displayAsMoney($viewPreorder->preorder->getCalcTotalCostWithFee()) ?></strong>
            <?php elseif ($viewPreorder->preorder->budget): ?>
                <div class="text-muted"><?= _t('site.preorder', 'Budget') ?></div>
                <strong><?php echo displayAsMoney($viewPreorder->preorder->getBudgetMoney()) ?></strong>
            <?php endif; ?>
        </div>

        <div class="service-order-row__machine">
            <?php if ($viewPreorder->hasPreorderWorks()): ?>
                <div class="ugc-content">
                    <div class="text-muted"><?= _t('site.preorder', 'Items') ?></div>
                    <?php echo implode(', ', ArrayHelper::getColumn($viewPreorder->preorderWorksView, 'itemViewHtml')); ?>
                </div>
            <?php endif; ?>

            <?php if ($viewPreorder->preorder->description): ?>
                <div class="ugc-content">
                    <div class="text-muted"><?= _t('site.preorder', 'Project Description') ?></div>
                    <?php echo H($viewPreorder->preorder->description); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="service-order-row__footer">
        <span><?= _t('site.preorder', 'Created on') ?> <?php echo app()->formatter->asDatetime($viewPreorder->getCreatedAt(), 'short'); ?></span>
    </div>

</div>

