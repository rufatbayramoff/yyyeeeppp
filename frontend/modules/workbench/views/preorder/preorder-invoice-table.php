<?php
/** @var \common\models\Preorder $preorder */
/** @var \yii\web\View $this */
/** @var boolean $showTotal */

/* @var $calculateAndShowFee bool */

use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\workbench\models\view\PreorderViewModel;

if (!isset($showTotal)) {
    $showTotal = false;
}

if (!isset($calculateAndShowFee)) {
    $calculateAndShowFee = false;
}


if (!isset($hidePaymentMethodFee)) {
    $hidePaymentMethodFee = false;
}

$preorderView = PreorderViewModel::fill($preorder);
$totalCost    = $hidePaymentMethodFee ? $preorder->primaryPaymentInvoice->getAmountTotalWithoutPaymentMethodFee() : $preorder->primaryPaymentInvoice->getAmountTotalWithRefund();
?>
<div class="table-responsive">
    <table class="table table-condensed estimated-cost-table table-bordered">
        <thead>
        <tr>
            <th><?= _t('site.ps', 'Items'); ?></th>
            <th><?= _t('site.ps', 'Quantity'); ?></th>
            <th><?= _t('site.ps', 'Cost'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($preorderView->preorderWorksInvoiceView as $invoiceItem) {
            if ($invoiceItem->total) {
                ?>
                <tr>
                    <td>
                        <?= H($invoiceItem->title) ?>
                        <?php if ($invoiceItem->getWorkFiles()): ?>
                            <div class="estimated-cost-table__files">
                                <?php echo _t('site.ps', 'Attachments'); ?>:
                                <?php foreach ($invoiceItem->getWorkFiles() as $file): ?>
                                    <a class="m-r10" href="<?php echo PreorderUrlHelper::downloadPreorderWorkFile($preorder, $invoiceItem->work, $file) ?>"><?= $file->name ?></a>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php echo app('formatter')->asDecimal($invoiceItem->qty) ?>
                    </td>
                    <td>
                        <?php echo displayAsMoney($invoiceItem->total); ?>
                    </td>
                </tr>
            <?php }
        } ?>
        <?php foreach ($preorderView->refundDetails as $detailRefund): ?>
            <tr>
                <td>
                    <?= _t('site.order', 'Partial Refund'); ?>
                    <span class="order__info-table-comment ugc-content">
                       <?= $detailRefund->paymentDetailOperation->toPaymentDetail()->paymentTransactionRefund ? '(' . H($detailRefund->paymentDetailOperation->toPaymentDetail()->paymentTransactionRefund->comment) . ')' : ''; ?>
                    </span>
                </td>
                <td></td>
                <td>
                    <?php echo displayAsMoney($detailRefund->getMoneyAmount()); ?>
                </td>
            </tr>
        <?php endforeach; ?>

        <?php if (!$hidePaymentMethodFee && $amountCardFee = $preorder->primaryPaymentInvoice->getAmountPaymentMethodFee()): ?>
            <tr>
                <td>
                    <?php echo _t('site.ps', 'Payment method fee') ?>
                </td>
                <td></td>
                <td>
                    <?php echo displayAsMoney($amountCardFee); ?>
                </td>
            </tr>
        <?php endif; ?>

        <?php if ($showTotal) : ?>
            <tr class="estimated-cost-table__total_row">
                <td></td>
                <td></td>
                <td colspan="1">
                    <b>
                        <?php echo _t('site.ps', 'Total cost') ?>: <?php echo displayAsMoney($totalCost) ?>
                        <?php if (!$calculateAndShowFee && $totalCost->getAmount() > 0): ?>
                            <br><?php echo _t('site.ps', 'You get') ?>: <?php echo displayAsMoney($preorder->primaryPaymentInvoice->preorderAmount->getAmountManufacturerWithRefund()) ?>
                            <?php if ($hidePaymentMethodFee): ?>
                                <span data-toggle="tooltip" data-placement="top" data-original-title="<?php echo $preorder->getPriceTooltip($hidePaymentMethodFee); ?>"
                                      class="tsi tsi-warning-c"></span>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if (($bonusAccrued = $preorder?->primaryPaymentInvoice?->getAmountBonusAccrued()) && ($bonusAccrued->getAmount())) { ?>
                            <div>
                                <?= _t('site.ps', 'Bonus accrued'); ?>: +<?= $bonusAccrued->getAmount() ?>
                            </div>
                        <?php } ?>
                    </b>
                </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>