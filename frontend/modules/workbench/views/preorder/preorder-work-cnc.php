<?php

use common\models\PreorderCncWork;
use lib\money\Money;

/** @var PreorderCncWork[] $preorderCncWorks */

?>

<div class="table-responsive">
    <table class="table table-condensed estimated-cost-table table-bordered">
        <thead>
        <tr>
            <th><?= _t('site.ps', 'Items'); ?></th>
            <th><?= _t('site.ps', 'Quantity'); ?></th>
            <th><?= _t('site.ps', 'Cost'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($preorderCncWorks as $preorderCncWork): ?>
            <tr>
                <td>
                    <?= H($preorderCncWork->title) ?>
                </td>
                <td>
                    <?php echo app('formatter')->asDecimal($preorderCncWork->qty) ?>
                </td>
                <td>
                    <?php echo displayAsMoney($preorderCncWork->getPriceMoney()); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
