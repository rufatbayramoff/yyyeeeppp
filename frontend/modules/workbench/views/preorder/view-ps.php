<?php

use common\models\Preorder;
use common\modules\company\serializers\CompanyServiceSerializer;
use common\modules\product\serializers\ProductSerializer;
use frontend\modules\preorder\components\PreorderSerializer;
use yii\web\View;

/** @var View $this */
/** @var Preorder $preorder */
$feePercent = 0.5;


$preorderSerialized = PreorderSerializer::serialize($preorder);
$productsList = ProductSerializer::serialize($preorder->company->products);
$servicesList = CompanyServiceSerializer::serialize($preorder->company->companyServices);
?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(); ?>

<div class="container">
    <div class="row ps-index">
        <div class="col-md-12">
            <div class="tab-content">

                <div class="row">
                    <div class="col-sm-8 wide-padding wide-padding--left">

                        <!-- 4592 INNER Preorder Quote Print Service Request Sample -->
                        <div class="panel panel-default box-shadow border-0">
                            <div class="one-print-request ng-scope">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="one-print-request__user">
                                                        <h3 style="margin: 0">
                                                            <?= _t('site.ps', 'Quote'); ?> #<?= $preorder->id; ?>
                                                        </h3>
                                                        <div class="p-t10">
                                                            <strong><?= _t('site.preorder', 'Status') ?></strong>: <?= $preorder->getLabeledStatus(); ?>
                                                        </div>
                                                        <?php if ($preorder->storeOrder) {
                                                            ?>
                                                            <strong><?= _t('site.preorder', 'Payment status') ?></strong>:
                                                            <?= $preorder->storeOrder->primaryPaymentInvoice->getStatusText() ?? '' ?>
                                                            <?php
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6 text-center">
                                            <div class="one-print-request__btns one-print-request__btns--print-order"
                                                 data-order-id="24">
                                                <a  href="javascript:history.back()"
                                                   class="btn btn-primary">
                                                    <span class="tsi tsi-arrow-back-l"></span> <?= _t('site.ps', 'Back'); ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body p-b0 one-print-request__body">
                                    <div class="row">
                                        <?php if ($preorder->description) { ?>
                                            <div class="col-md-8">
                                                <h4 class="m-t0"><?= _t('site.ps', 'Project Description'); ?></h4>
                                                <p><?= H($preorder->description); ?></p>
                                            </div>
                                        <?php } ?>
                                        <?php if ($preorder->productSnapshot) { ?>
                                            <div class="col-md-4">
                                                <h4 class="m-t0 ugc-content"><?= _t('site.ps', 'Product'); ?>: <?= H($preorder->productSnapshot->getTitle()); ?></h4>
                                                <img style="max-width: 100px; max-height: 100px; margin-bottom: 10px;border-radius: 5px"
                                                     ng-src="<?= HL($preorder->productSnapshot->getCoverUrl()) ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <p class="m-b10">
                                                    <strong><?= _t('site.ps', 'Price'); ?>:</strong>
                                                    <?= H($preorder->productSnapshot->amountWithCurrency()); ?> / <?= H($preorder->productSnapshot->getUnitType()) ?>
                                                </p>
                                            </div>
                                        <?php } ?>
                                        <?php if ($preorder->qty) { ?>
                                            <div class="col-md-4">
                                                <p class="m-b10"><strong>
                                                        <?= _t('site.ps', 'Quantity'); ?>:</strong>
                                                    <?= H($preorder->qty) ?> <?= $preorder->productSnapshot->getUnitType(); ?>
                                                </p>
                                            </div>
                                        <?php } ?>
                                        <div class="col-md-4">
                                            <?php if ($preorder->budget) { ?>
                                                <p class="m-b10">
                                                    <strong><?= _t('site.ps', 'Budget'); ?>:</strong>
                                                    <?= displayAsMoney($preorder->getBudgetMoney()) ?>

                                                    <?php
                                                    $platformFee = \lib\money\Money::usd($preorder->budget * $feePercent);
                                                    $note = _t(
                                                        'site.ps',
                                                        "Customer will pay {price1}. Platform fee is {price2}. Your income will be {price3}",
                                                        [
                                                            'price1' => displayAsMoney(\lib\money\Money::usd($preorder->budget)),
                                                            'price2' => displayAsMoney($platformFee),
                                                            'price3' => displayAsMoney(\lib\money\Money::usd($preorder->budget - $platformFee->getAmount()))
                                                        ]
                                                    );
                                                    #echo sprintf('<span data-toggle="tooltip" data-placement="top" data-original-title="%s" class="tsi tsi-warning-c"></span>', $note);
                                                    ?>
                                                </p>
                                            <?php } ?>
                                            <?php if ($preorder->estimate_time) { ?>
                                                <p class="m-b0">
                                                    <strong><?= _t('site.ps', 'Deadline'); ?>: </strong>
                                                    <?= _t('site.order',
                                                        '{n,plural,=0{Not specified} =1{1 day} other{# days}}',
                                                        ['n' => (int)$preorder->offer_estimate_time]); ?>
                                                </p>
                                            <?php } ?>
                                        </div>

                                        <?php if ($preorder->files) {
                                            ?>
                                            <div class="col-md-12" ng-if="preorder.files.length">
                                                <h4 class="m-t0"><?= _t('site.ps', 'Attached Files'); ?></h4>

                                                <?php foreach ($preorder->files as $file) { ?>
                                                    <div class="simple-file-link">
                                                        <a class="simple-file-link__link" href="<?= $file->getFileUrl() ?>">
                                                            <span><?= H($file->getFileName()) ?></span>
                                                            <i><?= H($file->size) ?></i>
                                                        </a>
                                                    </div>
                                                    <?php
                                                } ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>


                                </div>
                                <div class="panel-body p-t0 p-b0">
                                    <div id="offer-block">
                                        <div class="row m-t20">
                                            <div class="preorder-works">
                                                <div class="preorder-works__row preorder-works__row--head">
                                                    <div class="preorder-works__title"><?= _t('site.ps', 'Items'); ?></div>
                                                    <div class="preorder-works__qty"><?= _t('site.ps', 'Quantity'); ?><span class="form-required">*</span></div>
                                                    <div class="preorder-works__cost"><?= _t('site.ps', 'Cost'); ?><span class="form-required">*</span></div>
                                                </div>

                                                <?php foreach ($preorder->preorderWorks as $work) { ?>
                                                    <div class="preorder-works__row">
                                                        <div class="preorder-works__title">
                                                            <div class="preorder-works__mobile-label"><?= _t('site.ps', 'Item'); ?></div>
                                                            <?= H($work->getTextTitle()) ?>

                                                            <?php
                                                            if ($preorder->files) {
                                                                ?>
                                                                <div class="estimated-cost-table__files">
                                                                    <?= _t('site.ps', 'Attachments'); ?>:
                                                                    <?php
                                                                    foreach ($preorder->files as $workFile) {
                                                                        ?>
                                                                        <a class="m-r10" href="<?= HL($workFile->getFileUrl()) ?>"><?= H($workFile->getFileName()); ?></a>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <?php
                                                        if ($work->qty) {
                                                            ?>
                                                            <div class="preorder-works__qty">
                                                                <div class="preorder-works__mobile-label"><?= _t('site.ps', 'Quantity'); ?></div>
                                                                <?= H($work->qty); ?> <span><?= H($work->getUnitTypeLabel()); ?></span>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($work->cost) {
                                                            ?>
                                                            <div class="preorder-works__cost">
                                                                <div class="preorder-works__mobile-label"><?= _t('site.ps', 'Cost'); ?></div>
                                                                <?= H(displayAsMoney(\lib\money\Money::create($work->cost, \lib\money\Currency::USD))) ?>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body one-print-request__footer">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="send-print-request-date text-right" style="margin-top: 5px;">
                                                <?= _t('site.ps', 'Created'); ?>: <?= H($preorder->created_at); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

