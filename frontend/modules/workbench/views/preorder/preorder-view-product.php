<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.08.18
 * Time: 10:57
 */

use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserUtils;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;

/** @var \common\models\Preorder $preorder */
/** @var \yii\web\View $this */
/** @var \common\models\User $preorderUser */

$productSnapshot = $preorder->productSnapshot;
$price = $productSnapshot->getPriceMoneyByQty($preorder->qty);
$totalPrice = \lib\money\Money::create($price->getAmount() * $preorder->qty, $price->getCurrency());
?>
<?php if ($preorder->productSnapshot) { ?>
    <div class="col-md-4">
        <h4 class="m-t0 ugc-content"><?= _t('site.ps', 'Product'); ?>: <?= H($productSnapshot->getTitle()) ?></h4>
        <?php if ($coverUrl = $productSnapshot->getCoverUrl()) {
            ?>
            <img style="max-width: 100px; max-height: 100px; margin-bottom: 10px;border-radius: 5px;" src="<?= ImageHtmlHelper::getThumbUrl($coverUrl, ImageHtmlHelper::THUMB_SMALL, ImageHtmlHelper::THUMB_SMALL) ?>">
            <?php
        } ?>
    </div>
    <div class="col-md-4">
        <p class="m-b10"><strong><?= _t('site.ps', 'Price'); ?>:</strong> <?= displayAsMoney($price) ?> / <?= $productSnapshot->getUnitType() ?></p>
    </div>
    <div class="col-md-4">
        <p class="m-b10"><strong><?= _t('site.ps', 'Total price'); ?>:</strong> <?= displayAsMoney($totalPrice) ?></p>
    </div>
<?php } ?>



