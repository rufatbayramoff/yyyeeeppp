<?php
/** @var \common\models\Preorder $preorder */

use frontend\components\UserUtils;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use frontend\modules\preorder\components\CustomerPreorderDeclineForm;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\modules\workbench\components\MessageUrlHelper;
use frontend\modules\workbench\models\view\PreorderViewModel;

/** @var \yii\web\View $this */
/** @var \common\models\User $preorderUser */

$this->title   = _t('site.orders', 'My Purchases');
$currentUserId = UserFacade::getCurrentUserId();

Yii::$app->angular
    ->service(['modal', 'notify', 'router', 'user'])
    ->controller('preorder/preorder-view-customer')
    ->controller('store/common-models')
    ->controller('preorder/preorder-models')
    ->controller('preorder/preorder-service')
    ->controllerParams([
        'preorderCustomerDeclineReasons' => CustomerPreorderDeclineForm::getSuggestObjectList(),
    ]);

$viewPreorder = PreorderViewModel::fill($preorder);

?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(['section' => 'workbench/orders']); ?>

<?php echo $this->render('templates/order-decline-modal.php'); ?>

<div class="container" ng-controller="PreorderViewCustomreController">

    <div class="row order-item">

        <div class="col-sm-3">
            <a href="/workbench/orders/quotes" class="btn btn-default order-item__back-btn">
                <span class="tsi tsi-left"></span>
                <?= _t('user.order', 'Back'); ?>
            </a>
        </div>

        <div class="col-sm-9 wide-padding wide-padding--right">
            <?php if ($preorderUser->status !== \common\models\User::STATUS_ACTIVE): ?>
                <div class="alert alert-warning alert--text-normal" role="alert">
                    <?php echo _t('site.preorder.confirm',
                        "We’ve sent you a confirmation email. Please click the link in that email to have your RFQ submitted to the supplier."); ?>
                </div>
            <?php endif; ?>

            <div class="order-block">
                <?php if ($viewPreorder->isRejected() && $viewPreorder->getDeclineReason()): ?>
                    <div class="order-block__status bg-warning bar">
                        <?php echo $viewPreorder->getDeclineReason(); ?>
                    </div>
                <?php endif; ?>

                <div class="row order__info border-b0">
                    <div class="order__title">
                        <h3 class="order__number">
                            <?= _t('site.preorder', 'Quote #{id}', ['id' => $preorder->id]) ?>
                        </h3>
                        <div class="order__date"><?= _t('site.ps', 'Created'); ?>: <?= \Yii::$app->formatter->asDatetime($preorder->created_at); ?></div>
                    </div>
                    <div class="order__state">
                        <div class="order__status-row">
                            <div class="order__payment-status">
                                <div class="order__status">
                                    <?= _t('user.order', 'Status'); ?>:
                                    <strong><?= $preorder->getLabeledStatus(); ?></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                if (!$preorder->isCreatedByPs()) {
                    ?>
                    <div class="order__body p-l0 p-r0 clearfix">
                        <?php
                        if ($preorder->productSnapshot) {
                            echo $this->render('preorder-view-product.php', ['preorder' => $preorder]);
                        }
                        ?>
                        <?php if ($preorder->company) { ?>
                            <div class="col-md-8">
                                <h4 class="m-t0"><?= _t('site.ps', 'Company'); ?></h4>
                                <div class="m-b10">
                                    <a class="m-r10" href="<?= UserUtils::getUserPublicProfileUrl($preorder->company->user); ?>"><?= H($preorder->company->getTitle()) ?></a>
                                    <?php $messageTopic = $preorder->messageTopic; ?>
                                    <?php if ($messageTopic): ?>
                                        <div class="order__ps-message">
                                            <a href="<?php echo MessageUrlHelper::topic($messageTopic) ?>" class="btn btn-default btn-circle" target="_blank">
                                                <span class="tsi tsi-message"></span>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                    <?php
                                    if ($preorder->is_interception) {
                                        ?>
                                        <div>
                                            <?= _t('site.ps', 'Customer service'); ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if ($preorder->description) { ?>
                            <div class="col-md-8">
                                <h4 class="m-t0"><?= _t('site.ps', 'Description'); ?></h4>
                                <p><?= H($preorder->description) ?></p>
                            </div>
                        <?php } ?>

                        <?php
                        $addressPreorder = '';
                        if ($shipTo = $preorder->shipTo) {
                            $addressPreorder = \common\models\UserAddress::formatAddressWithFields($shipTo);
                        }
                        ?>
                        <?php if (!empty($addressPreorder)) { ?>
                            <div class="col-md-8">
                                <h4 class="m-t0"><?= _t('site.ps', 'Location'); ?></h4>
                                <?php echo $addressPreorder ?>
                            </div>
                        <?php } ?>

                        <?php if ($preorder->qty) { ?>
                            <div class="col-md-4">
                                <p class="m-b10"><strong><?= _t('site.ps', 'Qty'); ?>:</strong>
                                    <?= Yii::$app->formatter->asDecimal($preorder->qty) ?> <?= $preorder->getUnitTypeLabel() ?>
                                </p>
                            </div>
                        <?php } ?>

                        <div class="col-md-4">
                            <?php if ($preorder->service) { ?>
                                <p class="m-b10"><strong><?= _t('site.ps', 'Service'); ?>:</strong>
                                    <?= H($preorder->service->getTitleLabel()) ?></p>
                            <?php } ?>

                            <?php if ($preorder->budget): ?>
                                <p class="m-b10"><strong><?= _t('site.ps', 'Budget'); ?>
                                        :</strong> <?= displayAsMoney($preorder->getBudgetMoney()) ?></p>
                            <?php endif; ?>

                            <?php if ($preorder->estimate_time): ?>
                                <p>
                                    <b><?= _t('site.order', 'Deadline:'); ?></b>
                                    <?= _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}',
                                        ['n' => (int)$preorder->estimate_time]); ?>
                                </p>

                            <?php endif; ?>
                        </div>

                        <?php if ($preorder->files) : ?>

                            <div class="col-md-12">
                                <h4 class="m-t0"><?= _t('site.ps', 'Attached Files'); ?></h4>
                                <?php foreach ($preorder->files as $file) : ?>

                                    <div class="simple-file-link">
                                        <a class="simple-file-link__link"
                                           href="<?= PreorderUrlHelper::downloadPreorderFile($preorder, $file) ?>">
                                            <span><?= H($file->name); ?></span>
                                            <i><?= \Yii::$app->formatter->asShortSize($file->size, 2); ?></i>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                        <?php endif; ?>

                        <?php if ($preorder->preorderCncWorks && !$preorder->offered) : ?>
                            <div class="col-md-12">
                                <?= $this->render('preorder-work-cnc', ['preorderCncWorks' => $preorder->preorderCncWorks]) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php
                }
                ?>

                <?php if ($preorder->offered): ?>
                    <div class="panel-body p-t0 p-b0">
                        <h3 class="m-b0"><?= _t('site.ps', 'Supplier\'s Quote'); ?></h3>
                    </div>
                    <?php
                    if ($preorder->isCreatedByPs()) {
                        ?>
                        <div class="row order__ps-info">
                            <div class="col-sm-5 order__ps-desc">
                                <a class="order__ps-avatar" href="<?= PsFacade::getPsLink($preorder->ps) ?>" target="_blank">
                                    <img src="<?= UserFacade::getSalesAvatarUrl($preorder->ps->user) ?>" alt="">
                                </a>
                                <div class="order__ps-name">
                                    <h4 class="order__ps-title">
                                        <a href="<?= PsFacade::getPsLink($preorder->ps) ?>" target="_blank">
                                            <?= H(UserFacade::getSalesTitle($preorder->ps->user)); ?>
                                        </a>
                                    </h4>
                                    <div class="order__ps-message">
                                        <?php $messageTopic = $preorder->messageTopic ?>
                                        <?php if ($messageTopic): ?>
                                            <a href="<?php echo MessageUrlHelper::topic($messageTopic) ?>" class="btn btn-default btn-circle" target="_blank">
                                                <span class="tsi tsi-message"></span>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="row m-r0 m-l0">
                        <?php if ($preorder->offer_description): ?>
                            <div class="col-md-8">
                                <label class="control-label"><?= _t('site.ps', 'Description'); ?></label>
                                <p><?= H($preorder->offer_description) ?></p>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <p>
                                    <b><?= _t('site.order', 'Estimated time:'); ?></b>

                                    <?= _t('site.order', '{n,plural,=0{Not specified} =1{1 day} other{# days}}',
                                        ['n' => (int)$preorder->offer_estimate_time]); ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body p-t0 p-b0">
                        <?php if ($preorder->primaryPaymentInvoice && $preorder->primaryPaymentInvoice->getSortedPaymentInvoiceItems()): ?>
                            <div class="row">
                                <?php echo $this->render('preorder-invoice-table.php', [
                                    'preorder'             => $preorder,
                                    'showTotal'            => true,
                                    'calculateAndShowFee'  => true,
                                    'hidePaymentMethodFee' => true
                                ]) ?>
                            </div>
                        <?php else : ?>
                            <div class="estimated-cost-empty">
                                <?= _t('site.ps', 'Manufacturer hasn\'t issued the list of services yet'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class="order__footer border-t">
                    <?php if ($preorder->isClientCanAccept()) : ?>
                        <a href="<?= PreorderUrlHelper::acceptOffer($preorder) ?>" class="btn btn-success btn-sm">
                            <span class="tsi tsi-checkmark m-r10"></span>
                            <?= _t('site.ps', 'Accept Quote'); ?>
                        </a>
                    <?php endif; ?>
                    <?php if ($preorder->isClientCanDecline()) : ?>
                        <button class="btn btn-default btn-sm" loader-click-unrestored="true" loader-click="declinePreorder(<?= $preorder->id; ?>)">
                            <span class="tsi tsi-remove"></span>
                            <?= _t('site.ps', 'Decline'); ?>
                        </button>
                    <?php endif; ?>
                    <?php if ($preorder->isClientCanCancel()) : ?>
                        <button class="btn btn-default btn-sm" loader-click-unrestored="true" loader-click="cancelPreorder(<?= $preorder->id; ?>)">
                            <span class="tsi tsi-bin"></span>
                            <?= _t('site.ps', 'Cancel'); ?>
                        </button>
                    <?php endif; ?>
                    <?php if ($preorder->isClientCanDelete()) : ?>
                        <button class="btn btn-default btn-sm" loader-click-unrestored="true" loader-click="deletePreorder(<?= $preorder->id; ?>)">
                            <span class="tsi tsi-bin"></span>
                            <?= _t('site.ps', 'Delete'); ?>
                        </button>
                    <?php endif; ?>
                    <?php if ($preorder->isClientCanRepeat()) : ?>
                        <button class="btn btn-default btn-sm" loader-click-unrestored="true" loader-click="repeatPreorder(<?= $preorder->id; ?>)">
                            <span class="tsi tsi-repeat"></span>
                            <?= _t('site.ps', 'Repeat'); ?>
                        </button>
                    <?php endif; ?>
                </div>


            </div>
        </div>

    </div>
</div>

