<?php
sleep(0);
?>
<div class="designer-card"  ng-controller="PsCatalogController">
    <h2 class="designer-card__title">
        <?=_t('site.ps', 'Request a Free Quote');?>
    </h2>

    <div class="form-group">
        <label class="control-label" for="preorderDescr"><?=_t('site.ps', 'Project Description');?><span class="form-required">*</span></label>
        <textarea class="form-control" ng-model="preorder.description" rows="2" required="required"></textarea>
    </div>

    <div class="row m-b10">
        <div class="col-md-7">
            <div class="form-group m-r0 m-l0">
                <label class="control-label"><?=_t('site.ps', 'Budget');?></label>
                <div class="input-group">
                    <input class="form-control" type="number" ng-model="preorder.budget" placeholder="Price" min="0">
                    <div ng-if="costingBundle.isSuccesfulCalculated()" class="row hidden-xs hidden-sm ng-scope">

                        <div class="col-sm-12 m-b15">
                                <span class="m-r20 m-b10" style="display: inline-block;">
                                    <strong class="ng-binding">
                                        Total: 11.63
                                        USD
                                    </strong>
                                </span>
                            <button class="btn btn-primary" ng-click="openPreorderModal()" type="button">
                                Request a Quote                                </button>
                        </div>

                    </div>                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group m-r0 m-l0">
                <label class="control-label"><?=_t('site.ps', 'Deadline');?></label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="tsi tsi-alarm-clock"></span></span>
                    <input class="form-control" type="number" ng-model="preorder.estimateTime" placeholder="Days" min="0">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="designer-card__btn-block">
                <button class="btn btn-danger btn-ghost"   loader-click="openCreatePreorder(<?= $ps->id; ?> )">
                    <?= _t('site.ps', 'Get a Quote'); ?>
                </button>
            </div>
        </div>
    </div>
</div>