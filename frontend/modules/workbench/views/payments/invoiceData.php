<?php

/**
 * @var \common\models\PaymentInvoice $invoice
 */

use common\modules\payment\widgets\ReceiptViewWidget;

echo ReceiptViewWidget::widget(['paidInvoices' => [$invoice]]);

