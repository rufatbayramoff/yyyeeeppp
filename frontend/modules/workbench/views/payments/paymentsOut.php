<?php

use lib\money\Currency;
use lib\money\Money;
use \yii\helpers\Html;
use \yii\bootstrap\ActiveForm;

/**
 * @var Money $withdrawMoney
 * @var common\modules\payment\taxes\Tax $tax
 * @var string $bankAccountDetails
 * @var string $withdrawBy
 */

$amountTxt   = displayAsMoney($withdrawMoney);
$taxFormLink = Html::a(_t('site.tax', 'tax forms'), ['/my/taxes'], ['target' => '_blank']);

$txt = _t('user.taxes', 'We will withdraw {taxRate}% of {amount} because your {taxFormLink} 
are either not filled out, incomplete or still pending for approval. 
If you have any concerns or comments, please {contactLink}. Final approval of the tax forms can take up to three business days.', [
    'taxRate'      => $tax->getValue(),
    'by'           => $withdrawBy,
    'amount'       => $amountTxt,
    'taxFormLink'  => $taxFormLink,
    'bank-details' => $bankAccountDetails,
    'contactLink'  => Html::a(_t("site", "contact support"), ["site/contact"], ['target' => "_blank"])
]);

if ($tax->userTaxStatus == common\models\UserTaxInfo::STATUS_CONFIRMED ||
    $tax->userTaxStatus == common\models\UserTaxInfo::STATUS_SUBMITTED) {
    $txt = _t("user.taxes", "Without a completed tax form, we will need to deduct {taxRate}% from {amount}. <a href='/site/contact'>Contact us</a> for solving problems.", [
        'taxRate'      => $tax->getValue(),
        'by'           => $withdrawBy,
        'amount'       => $amountTxt,
        'bank-details' => $bankAccountDetails,
        'taxFormLink'  => $taxFormLink
    ]);
}
?>
<div class="container">
    <h1></h1>
    <div class="alert alert-warning alert-dismissible alert--text-normal" role="alert">
        <?php echo $txt; ?>
        <br/><br/>
        <?php $form = ActiveForm::begin(['action' => ['/workbench/payments/out']]); ?>
        <div>
            <input type="hidden" name="" value="1"/>

            <?php echo Html::hiddenInput('forcewithdraw', 1); ?>
            <?php echo Html::hiddenInput('amount', $withdrawMoney->getAmount()); ?>
            <?php echo Html::hiddenInput('by', $withdrawBy); ?>
            <?php echo Html::hiddenInput('bank-details', $bankAccountDetails); ?>

            <?php echo Html::submitButton(_t('site.payout', 'Request payout anyway'), ['class' => 'btn btn-danger']) ?>
            &nbsp; OR &nbsp;
            <?php echo Html::a(_t('site.payout', 'Complete tax form'), ['/mybusiness/taxes/'], ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>