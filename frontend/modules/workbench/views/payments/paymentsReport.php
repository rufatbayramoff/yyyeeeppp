<?php

/**
 * @var \common\models\User $user
 * @var string $fromDate
 * @var string $toDate
 * @var \yii\data\ActiveDataProvider $paymentDetails
 * @var \yii\data\ActiveDataProvider $paymentsOutDetails
 * @var string $totalIncome
 * @var string $totalWithdraw
 * @var string $totalBalance
 */

use common\models\PaymentDetail;
use common\modules\payment\helpers\PaymentDetailHelper;
use frontend\models\user\UserFacade;
use yii\grid\GridView;

?>
    <div id="printableArea">
        <style>
            .payments-report {}

            @media print {
                .payments-report {
                    width: 100%;
                }
                .no-print {
                    display: none;
                }
            }

            .payments-report__ts-logo {
                float: right;
                width: 190px;
                height: 40px;
                margin: 15px 0 10px;
            }

            @media (max-width: 767px) {
                .payments-report__ts-logo {
                    float: none;
                    margin: 0 0 5px;
                    width: 95px;
                    height: 20px;
                }
            }

            @media print {
                .payments-report__head-l {
                    float: left;
                    width: 66.66666667%;
                }

                .payments-report__head-r {
                    float: left;
                    width: 33.33333333%;
                }

                .payments-report__ts-logo {
                    float: right;
                    width: 190px;
                    height: 40px;
                    margin: 15px 0 10px;
                }
            }

            .payments-report__divider {
                margin: 5px 0;
                border-top: 1px solid #e0e4e8;
            }

            .payments-report__subtotal {
                margin-top: -10px;
                text-align: right;
            }

            @media screen and (max-width: 600px) {
                .payments-report__subtotal {
                    text-align: left;
                    margin-bottom: 20px;
                }
            }

            .table.payments-report__table > tbody > tr > th,
            .table.payments-report__table > tbody > tr > td {
                padding: 5px 0;
            }

            .table.payments-report__table tr:nth-child(3) {
                border-top: 1px solid #e0e4e8;
            }
        </style>

        <div class="container payments-report">
            <div class="row">
                <div class="col-sm-8 payments-report__head-l">
                    <h3><?php echo UserFacade::getFormattedUserName($user); ?></h3>
                </div>
                <div class="col-sm-4 payments-report__head-r">
                    <img class="payments-report__ts-logo" src="https://static.treatstock.com/static/images/logo_2x.png" alt="Treatstock logo">
                </div>
                <div class="col-xs-12">
                    <hr class="payments-report__divider">
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <h2><?php echo _t('site.payments', 'Financial Report'); ?></h2>
                    <p>
                        <?php echo _t('site.payments',
                            'From {fromDate} to {toDate}',
                            [
                                'fromDate' => Yii::$app->formatter->asDate($fromDate),
                                'toDate'   => Yii::$app->formatter->asDate($toDate)
                            ]
                        ); ?>
                    </p>
                </div>
            </div>

            <div class="table-responsive m-b30">
                <div class="table-responsive m-b20">
                    <h3 class="m-t10 m-b10"><?= _t('site.payments', 'Payments Received'); ?></h3>
                    <?php echo GridView::widget(
                        [
                            'showFooter'   => false,
                            'showHeader'   => true,
                            'summary'      => false,
                            'tableOptions' => ['class' => 'table table-bordered'],
                            'dataProvider' => $paymentDetails,
                            'columns'      => [
                                [
                                    'attribute' => 'n',
                                    'label'     => _t('site.user', 'Invoice'),
                                    'value'     => function (PaymentDetail $model) {
                                        return $model->paymentDetailOperation->payment->payment_invoice_uuid;
                                    }
                                ],
                                [
                                    'attribute' => 'formDate',
                                    'label'     => _t('site.user', 'Created On'),
                                    'format'    => 'datetime'
                                ],
                                [
                                    'attribute' => 'type',
                                    'label'     => _t('site.user', 'Type'),
                                    'value'     => function (PaymentDetail $model) {
                                        return \H($model->getTypeTitle());
                                    }
                                ],
                                [
                                    'attribute' => 'description',
                                    'label'     => _t('site.user', 'Description'),
                                    'format'    => 'raw',
                                    'value'     => function (PaymentDetail $model) {
                                        return PaymentDetailHelper::getFullDescription($model);
                                    }
                                ],
                                [
                                    'format'    => 'raw',
                                    'attribute' => 'amount',
                                    'label'     => _t('site.user', 'Amount'),
                                    'value'     => function (PaymentDetail $model) {
                                        return displayAsMoney($model->getMoneyAmount());
                                    }
                                ],
                            ],
                        ]
                    );
                    $total1 = 0;

                    foreach ($paymentDetails->getModels() as $model) {
                        $total1 += $model->amount;
                    }

                    $total1 = \lib\money\Money::create($total1, 'USD');

                    ?>

                    <h4 class="payments-report__subtotal">
                        <?php echo _t('site.payments', 'Total: {total}', ['total' => displayAsMoney($total1)]); ?>
                    </h4>

                    <h3 class="m-t10 m-b10"><?= _t('site.payments', 'Withdraws & Expenses'); ?></h3>
                    <?php echo GridView::widget(
                        [
                            'showFooter'   => false,
                            'showHeader'   => true,
                            'summary'      => false,
                            'tableOptions' => ['class' => 'table table-bordered'],
                            'dataProvider' => $paymentsOutDetails,
                            'columns'      => [
                                [
                                    'attribute' => 'formDate',
                                    'label'     => _t('site.user', 'Created On'),
                                    'format'    => 'datetime'
                                ],
                                [
                                    'attribute' => 'type',
                                    'label'     => _t('site.user', 'Type'),
                                    'value'     => function (PaymentDetail $model) {
                                        return \H($model->getTypeTitle());
                                    }
                                ],
                                [
                                    'attribute' => 'description',
                                    'label'     => _t('site.user', 'Description'),
                                    'format'    => 'raw',
                                    'value'     => function (PaymentDetail $model) {
                                        return PaymentDetailHelper::getFullDescription($model);
                                    }
                                ],
                                [
                                    'format'    => 'raw',
                                    'attribute' => 'original_amount',
                                    'label'     => _t('site.user', 'Amount'),
                                    'value'     => function (PaymentDetail $model) {
                                        return displayAsMoney($model->getMoneyAmount());
                                    }
                                ],
                            ],
                        ]
                    );
                    $total2 = 0;
                    foreach ($paymentsOutDetails->getModels() as $model) {
                        $total2 += $model->amount;
                    }

                    $total2 = \lib\money\Money::create($total2, 'USD');
                    ?>

                    <h4 class="payments-report__subtotal">
                        <?php echo _t('site.payments', 'Total: {total}', ['total' => displayAsMoney($total2)]); ?>
                    </h4>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-4">
                    <table class="table payments-report__table">
                        <tbody>
                        <tr>
                            <th><?= _t('site.payments', 'Income'); ?></th>
                            <td class="text-right"><?php echo displayAsMoney($total1); ?></td>
                        </tr>
                        <tr>
                            <th><?= _t('site.payments', 'Withdrawn'); ?></th>
                            <td class="text-right"><?php echo displayAsMoney($total2); ?></td>
                        </tr>
                        <tr>
                            <th><?= _t('site.payments', 'Ending Balance'); ?></th>
                            <td class="text-right"><?php echo displayAsMoney(\lib\money\MoneyMath::minus($total1, \lib\money\MoneyMath::abs($total2))); ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br/><br/>
        </div>
    </div>
<?php if (!$pdf): ?>
    <div style="text-align:center;m-b20">
        <button class="btn btn-sm btn-default no-print" onclick="printDiv('printableArea')"><span class="tsi tsi-printer"></span> Print</button>
        <?php /* <a href="<?= \yii\helpers\Url::toRoute(['/workbench/payments/download-pdf', 'from' => $fromDate, 'to' => $toDate]) ?>" class="btn btn-sm btn-default no-print"><span
                    class="tsi tsi-download-l"></span> Download PDF</a> */ ?>
        <a href="<?= \yii\helpers\Url::toRoute(['/workbench/payments/download-report', 'from' => $fromDate, 'to' => $toDate]) ?>" class="btn btn-sm btn-default no-print"><span
                    class="tsi  tsi-wallet"></span> Download Excel</a>
        <br/><br/>
    </div>
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
<?php endif; ?>