<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<script type="text/ng-template" id="/static/templates/popUpPay.html">
    <div ng-controller="BalanceInputController" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?= _t('site.ps', 'Add Funds') ?></h4>
                </div>

                <div class="modal-body">
                    <form name="formCheckout" action="<?php echo Url::to(['/ts-internal-purchase/deposit/checkout'])?>" ng-submit="checkout()" method="post">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="balance"><?= _t('site.ps', 'Enter Amount') ?></label>
                                </div>
                                <div class="col-sm-8">
                                    <input min="{{minInput}}" name="balance" ng-model="balance" id="balance" type="number" class="form-control m-b10" required>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control m-b10" ng-model="currency" name="currency">
                                        <option value="<?=\lib\money\Currency::USD?>">USD</option>
                                        <option value="<?=\lib\money\Currency::EUR?>">EUR</option>
                                    </select>
                                </div>
                            </div>
                            <small class="form-text text-muted"><?php echo _t('site.ps','Minimal amount 400')?></small>
                        </div>
                        <div class="form-group">
                            <button ng-disabled="isSubmit" class="btn btn-primary" type="submit"><?php echo _t('site.ps','Generate Invoice')?></button>
                        </div>
                        <?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</script>