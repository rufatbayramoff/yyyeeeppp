<?php

use common\models\PaymentTransaction;
use common\modules\payment\gateways\vendors\BankPayoutGateway;
use common\modules\payment\processors\BankPayoutProcessor;
use frontend\assets\AppAsset;
use frontend\modules\mybusiness\components\PayoutViewHelper;
use frontend\modules\mybusiness\components\TaxUrlHelper;
use frontend\widgets\SiteHelpWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


/**
 * @var \common\models\User $user
 * @var \lib\money\Money[] $balances
 * @var PaymentTransaction|null $lastBankPayout
 */

$helper = new PayoutViewHelper($user);
?>

<?php
if (!$helper->hideDeny()) {
    echo SiteHelpWidget::widget([
        'triggerHtml' => _t('site.payments', 'Unfortunately, by law, companies in the United States are forbidden from doing business with companies in your country.'),
        'alias'       => 'site.payments.laws'
    ]);
} else {
    if (!$helper->isPaypalEnable()) {
        echo Html::tag(
            'p',
            _t('site.tax', 'Please see below to determine if PayPal services are available in your country/region of residence.'),
            ['class' => 'alert alert-info alert--text-normal']
        );
    }

    if (!$helper->isTaxFormExist()) {
        $taxFormLink = Html::a(_t("site.tax", "tax information"), TaxUrlHelper::taxes());

        echo Html::tag(
            'p',
            _t('site.tax', 'You need to complete your {taxFormLink} before you can withdraw funds.', ['taxFormLink' => $taxFormLink]),
            ['class' => 'alert alert-warning alert--text-normal']
        );
    } else { ?>

        <div class="alert alert-info alert--text-normal" role="alert">
            <?php echo _t('site.payout', 'We don\'t charge any amount for payout'); ?>
        </div>

        <hr>
        <div class="payments-payout">
            <h4 style="font-size: 20px">
                <?php echo _t('mybusiness.settings', 'By paypal'); ?>
            </h4>
            <div class="">
                <?php
                if ($helper->userPaypalStatusOk()) {
                    ?>
                    <?php ActiveForm::begin(['action' => ['/workbench/payments/out'], 'layout' => 'inline']); ?>
                    <div class="payments-payout__form">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo Html::textInput('amount', null, [
                                        'maxlength'   => false,
                                        'placeholder' => _t('site.payout', 'Amount'),
                                        'disabled'    => $helper->isDisabled(),
                                        'class'       => 'form-control m-b10'
                                    ]); ?>
                                </div>
                                <?php if ($helper->hasCurrencySelector($balances)) { ?>
                                    <div class="col-md-6">
                                        <select class="form-control m-b10 ng-pristine ng-valid ng-not-empty ng-touched"
                                                name="currency"
                                        "<?= $helper->isDisabled() ? 'disabled' : '' ?>"
                                        >
                                        <option value="USD" <?=$helper->hasUsd($balances)?'':'disabled'?>>USD</option>
                                        <option value="EUR" <?=$helper->hasEur($balances)?'':'disabled'?> <?php if ($helper->currencySelectedEUR($balances)) {
                                            echo 'selected';
                                        } ?> >EUR
                                        </option>
                                        </select>
                                    </div>
                                <?php } ?>
                                <div class="col-md-12">
                                    <input type="hidden" name="by" value="paypal">
                                    <?php echo Html::submitButton(_t('site.payout', 'Request paypal payout'), [
                                        'onclick'  => new yii\web\JsExpression('setTimeout(function(){event.target.disabled=true},0);'),
                                        'class'    => 'btn btn-success m-b10',
                                        'disabled' => $helper->isDisabled()]) ?>
                                </div>
                            </div>
                        </div>

                        <span class="help-block"><?= _t('site.payout', 'The minimum payout amount is 10'); ?></span>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <p>
                        <?php echo _t("site.payout", "PayPal email to use for payout") . ": <b>" . H($user->userPaypal->email), "</b>"; ?>
                    </p>
                    <?php if ($helper->hasPendingPayouts()): ?>
                        <p class="text-muted">
                            <?php echo _t('site.payments', 'Your PayPal account cannot be changed while you have pending payouts.'); ?>
                        </p>
                    <?php else: ?>
                        <div>
                            <a class="payments-payout__change"
                               href='/workbench/payments/unlink-paypal'
                               data-method='post'
                               data-confirm='<?= Yii::t('yii', 'Are you sure want unlink PayPal? You can relink it any time.'); ?>'>
                                <?= _t('site.tax', 'Unlink PayPal'); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                    <div class="alert alert-info alert--text-normal m-b0">
                        <?= _t('site.payout', 'After you request a payout from your Treatstock account, you will receive the payout amount in your PayPal account within 10 working days. The minimum payout amount from your Treatstock account to your PayPal account is $10. If you experience any problems with payout to your PayPal account, please contact us.'); ?>
                    </div>
                <?php } else { ?>
                    <p>
                        <?php echo _t('mybusiness.settings', 'Log in with PayPal to withdraw your funds.'); ?>
                    </p>
                    <span id="lippButton"></span>
                <?php } ?>
            </div>
        </div>
        <div class="payments-payout">
            <h4 class="m-t30 m-b0" style="font-size: 20px">
                <?php echo _t('mybusiness.settings', 'By bank Transfer'); ?>
            </h4>
            <div class="m-b10 text-muted"><?php echo _t('mybusiness.settings', 'It is currently running in test mode'); ?></div>

            <?php if ($helper->bankTransferAllowed($balances)) { ?>

                <?php ActiveForm::begin(['action' => ['/workbench/payments/out']]); ?>

                <div class="payments-payout__form">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <?php echo Html::textInput('amount', null, [
                                    'maxlength'   => false,
                                    'min'         => BankPayoutProcessor::MINIMUM_PAYOUT,
                                    'type'        => 'number',
                                    'placeholder' => _t('site.payout', 'Amount'),
                                    'class'       => 'form-control m-b10',
                                    'id'          => 'inputAmount'
                                ]); ?>
                            </div>
                            <?php if ($helper->hasCurrencySelector($balances)) { ?>
                                <div class="col-md-6">
                                    <select class="form-control m-b10 ng-pristine ng-valid ng-not-empty ng-touched"
                                            name="currency">
                                        <option value="USD" <?=$helper->hasUsd($balances)?'':'disabled'?>>USD</option>
                                        <option value="EUR" <?=$helper->hasEur($balances)?'':'disabled'?> <?php if ($helper->currencySelectedEUR($balances)) {
                                            echo 'selected';
                                        } ?> >EUR
                                        </option>
                                    </select>
                                    <?php if ($helper->hasEur($balances)) { ?>
                                    <div class="validationErrorText">Only USD bank payout allowed</div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div class="col-md-12">
                                <span class="help-block m-b20"><?= _t('site.payout', 'The minimum payout amount is ') . BankPayoutProcessor::MINIMUM_PAYOUT; ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputDetails">
                            <?php echo _t('mybusiness.settings', 'Enter your bank account details'); ?>
                        </label>
                        <textarea name="bank-details" class="form-control m-b0" rows="3"
                                  id="inputDetails"
                                  placeholder="<?php echo _t('site.payout', ''); ?>"
                                  style="width: 100%"><?php echo $lastBankPayout ? H($lastBankPayout->details[BankPayoutGateway::ACCOUNT_DETAILS]) : '' ?></textarea>
                        <input type="hidden" name="by" value="bank">
                        <span class="help-block">
                            <?php echo _t('site.payout', 'Name/Business Name, Bank Account details, address. Currency of your bank account (USD EUR CNY GBP INR)'); ?>
                        </span>
                    </div>

                    <div class="alert alert-info alert--text-normal" role="alert">
                        <?php echo _t('site.payout', 'You can withdraw the funds in a preferred currency. Exchange rate may vary up to +/-1% (due to exchange rate fluctuation)'); ?>
                    </div>

                    <div class="alert alert-warning alert--text-normal" role="alert">
                        <?php echo _t('site.payout', 'For bank payments in USD outside of the US, your bank will likely charge a fee of $25-50 to make a SWIFT transfer'); ?>
                    </div>

                    <?php echo Html::submitButton(_t('site.payout', 'Request bank payout'), ['class' => 'btn btn-success']) ?>

                </div>

                <?php ActiveForm::end(); ?>

            <?php } else { ?>
                <?php echo _t('mybusiness.settings', 'Direct bank transfer allowed from amount $') . BankPayoutProcessor::MINIMUM_PAYOUT; ?>
            <?php }; ?>
        </div>
    <?php } ?>

    <script>
        <?php $this->beginBlock('payout2')?>
        paypal.use(["login"], function (login) {
            login.render({
                "appid": '<?=param("paypal_client_id");?>',
                <?php if(param('paypal_type') == 'sandbox'): ?>
                "authend": 'sandbox',
                <?php endif; ?>
                "scopes": "email profile address",
                "containerid": "lippButton",
                "locale": "<?php echo \Yii::$app->language; ?>",
                "returnurl": '<?=param("paypal_return_url");?>',
                disabled: true
            });
        });
        <?php $this->endBlock()?>
    </script>
    <?php
    $this->registerJs($this->blocks['payout2']);
    $this->on('endBody', function () { // fix paypal's requirejs conflicts
        $this->registerJsFile('https://www.paypalobjects.com/js/external/api.js', ['depends' => [AppAsset::class]]);
    });
}