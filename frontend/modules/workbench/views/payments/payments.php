<?php

use common\models\PaymentDetail;
use common\models\PaymentTransaction;
use common\models\User;
use common\modules\payment\helpers\PaymentDetailHelper;
use common\modules\payment\services\InvoiceBankService;
use frontend\modules\mybusiness\components\PayoutViewHelper;
use frontend\modules\workbench\assets\PaymentAsset;
use frontend\modules\workbench\widgets\WorkbenchTabsWidget;
use lib\money\Money;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var User $user
 * @var Money[] $mainBalances
 * @var array $accountStatistics
 * @var ActiveDataProvider $paymentDetails
 * @var PaymentTransaction|null $lastBankPayout
 * @var View $this
 */

$this->title = _t('site.my', 'Earnings');

$helper = new PayoutViewHelper($user);
PaymentAsset::register($this);

Yii::$app->angular
    ->service(['modal'])
    ->controllerParams(
        [
            'minInput' => InvoiceBankService::INVOICE_FROM_USD
        ])
    ->controller('payment/controllers/EarningController')
    ->controller('payment/controllers/BalanceInputController');
?>

<?= WorkbenchTabsWidget::widget(['section' => 'workbench/payments']); ?>
<?php echo $this->render('partials/popupPay'); ?>
<div class="container" ng-controller="EarningController">
    <div class="row wide-padding">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="payments-balance">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4 class="payments-balance__label">
                                    <?php echo _t('site.payout', 'Balance') ?>
                                </h4>
                                <div class="payments-balance__value">
                                    <?php foreach ($mainBalances as $mainBalance) { ?>
                                        <div class="m-r15">
                                            <?php echo displayAsMoney($mainBalance) ?>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                    <button ng-click="showPopUp()" class="btn btn-sm btn-primary btn-ghost m-t10 m-r10">
                                        <?php echo _t('site.payout', 'Add Funds') ?>
                                    </button>

<!--                                    <button class="btn btn-sm btn-primary btn-ghost m-t10" data-toggle="modal" data-target=".convertModal">-->
<!--                                        --><?php //echo _t('site.payout', 'Convert to bonuses') ?>
<!--                                    </button>-->

                                </div>

                                <div class="modal fade convertModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <h3 class="modal-title" id="myModalLabel"><?php echo _t('site.payout', 'Convert balance to bonuses') ?></h3>
                                            </div>
                                            <div class="modal-body">

                                                <div class="bar bg-info m-b15">
                                                    <?php echo _t('site.payout', 'The money that you have on your balance can be converted to bonuses. Bonuses represent an absolute number without any connection or equivalent to currency. $100 = 100 Bonuses') ?>
                                                </div>

                                                <form name="formCheckout" action="asdf" ng-submit="checkout()" method="post">
                                                    <div class="form-group m-b0">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label for="balance"><?= _t('site.ps', 'Enter Amount') ?></label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input min="{{minInput}}" name="balance" ng-model="balance" id="balance" type="number" class="form-control m-b10" required>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <button ng-disabled="isSubmit" class="btn btn-primary btn-block p-l5 p-r5" type="submit"><?php echo _t('site.ps','Convert')?></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div>
                                    <?= _t('site.payout', 'Did you know that you can use your income for purchases on Treatstock?'); ?>
                                    <a href="https://www.treatstock.com/help/article/80-what-can-i-do-with-my-income-on-treatstock"
                                       target="_blank">
                                        <?= _t('site.payout', 'Learn more'); ?>
                                    </a>
                                </div>
                            </div>
                            <div class="payments-balance__divider"></div>
                            <div class="col-sm-8">

                                <h4 class="payments-balance__label">
                                    <?= _t('site.payout', 'Withdraw'); ?>
                                </h4>

                                <?= $this->render('payments-withdraw', ['user' => $user, 'balances' => $mainBalances, 'lastBankPayout' =>$lastBankPayout]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            if ($user->company) {
                ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="payments-balance">
                            <h4 class="payments-balance__label">
                                <?= _t('site.payout', 'Statistics'); ?>
                            </h4>
                            <ul class="nav nav-tabs nav-tabs--secondary" role="tablist">
                                <li role="presentation" class="active"><a href="#payments-balance__stats-manufacturer"
                                                                          aria-controls="payments-balance__stats-manufacturer"
                                                                          role="tab" data-toggle="tab"
                                                                          aria-expanded="true"><?= _t('site.payout', 'My Business'); ?></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="row payments-balance__stats tab-pane active"
                                     id="payments-balance__stats-manufacturer">
                                    <div class="col-sm-3">
                                        <h4><?= _t('site.payout', 'Total income'); ?></h4>
                                        <span><?php
                                            foreach ($accountStatistics['totalIncome'] as $money) {
                                                echo displayAsMoney($money) . ' <br> ';
                                            }
                                            ?></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <h4><?= _t('site.payout', 'Completed orders'); ?></h4>
                                        <span><?= $accountStatistics['ordersResolved']; ?></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <h4><?= _t('site.payout', 'Total items produced'); ?></h4>
                                        <span><?= $accountStatistics['modelsPrinted']; ?></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <h4><?= _t('site.payout', '3D models sold'); ?></h4>
                                        <span><?= $accountStatistics['modelsSold']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default payments-create-report">
                <div class="panel-body p-b0">
                    <p><?= _t('site.payout', 'You have been a Treatstock user since {date}', ['date' => Yii::$app->formatter->asDate($user->created_at)]); ?></p>
                    <?php $form = ActiveForm::begin([
                        'id'      => 'report-form',
                        'action'  => '/workbench/payments/report',
                        'layout'  => 'inline',
                        'method'  => 'get',
                        'options' => ['target' => '_blank']
                    ]); ?>
                    <div class="form-group">
                        <label class="payments-create-report__label"><?= _t('site.payout', 'From'); ?></label>
                        <input type="date" name="from" class="form-control input-sm payments-create-report__input"
                               value="<?= date("Y-m-d", $user->created_at); ?>">
                    </div>
                    <div class="form-group">
                        <label class="payments-create-report__label"><?= _t('site.payout', 'to'); ?></label>
                        <input type="date" name="to" class="form-control input-sm payments-create-report__input"
                               value="<?= date("Y-m-d"); ?>">
                    </div>
                    <div class="form-group">
                        <?php echo Html::a(_t('site.payout', 'Create report'), ['report'], [
                            'class' => 'btn btn-sm btn-primary btn-ghost payments-create-report__btn btn-report m-r10',
                        ]); ?>
                    </div>
                    <div class="form-group">
                        <?php echo Html::a(_t('site.payout', 'Download Order Invoices'), ['invoice-report'], [
                            'class' => 'btn btn-sm btn-primary btn-ghost payments-create-report__btn btn-report m-r10',
                        ]); ?>
                    </div>
                    <div class="form-group">
                        <?php echo Html::a(_t('site.payout', 'Download Treatstock Fee Invoices'), ['fee-report'], [
                            'class' => 'btn btn-sm btn-primary btn-ghost payments-create-report__btn btn-report',
                        ]); ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive m-b20 payments-table">
                <?php echo GridView::widget([
                    'sorter'       => false,
                    'showFooter'   => false,
                    'summary'      => false,
                    'dataProvider' => $paymentDetails,
                    'columns'      => [
                        [
                            'label'  => _t('site.user', 'Invoice'),
                            'format' => 'raw',
                            'value'  => function (PaymentDetail $model) use ($user) {
                                $paymentInvoice = $model->payment->paymentInvoice ?? null;
                                if (!$paymentInvoice) {
                                    return '';
                                }
                                $paymentInvoiceUuid = H($paymentInvoice->uuid);
                                if ($paymentInvoice->checkAccessForUser($user)) {
                                    return Html::a($paymentInvoiceUuid, ['/workbench/payments/invoice-data', 'invoiceUuid' => $paymentInvoiceUuid], [
                                        'title'  => _t('site.user', 'Invoice #{invoice}', ['invoice' => $paymentInvoiceUuid]),
                                        'target' => '_blank'
                                    ]);
                                }

                                return Html::tag('p', $paymentInvoiceUuid);
                            }
                        ],
                        [
                            'label'     => _t('site.user', 'Created On'),
                            'attribute' => 'formDate',
                            'format'    => 'datetime'
                        ],
                        [
                            'format'    => 'raw',
                            'attribute' => 'amount',
                            'label'     => _t('site.user', 'Amount'),
                            'value'     => function (PaymentDetail $model) {
                                $priceInfo = PaymentDetailHelper::getPriceInfo($model);
                                $tooltip   = '';

                                if ($priceInfo) {
                                    $tooltip = ' <span data-toggle="tooltip" data-placement="top" data-original-title="' . $priceInfo . '" class="tsi tsi-warning-c"></span>';
                                }

                                $showBlock = '<span style="font-weight: bold">';
                                if ($model->getMoneyAmount()->getAmount() > 0) {
                                    $showBlock = '<span style="font-weight: bold">';
                                }
                                $showBlock .= displayAsMoney($model->getMoneyAmount());
                                $showBlock .= '</span>' . $tooltip;
                                return $showBlock;
                            }
                        ],
                        [
                            'attribute' => 'type',
                            'label'     => _t('site.user', 'Transaction'),
                            'value'     => function (PaymentDetail $model) {
                                return H($model->getTypeTitle());
                            }
                        ],
                        [
                            'attribute' => 'description',
                            'format'    => 'raw',
                            'label'     => _t('site.user', 'Description'),
                            'value'     => function (PaymentDetail $model) {
                                return nl2br(PaymentDetailHelper::getFullDescription($model));
                            }
                        ]
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>