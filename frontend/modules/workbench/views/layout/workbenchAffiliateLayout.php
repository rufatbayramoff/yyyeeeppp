<?php
/**
 * Created by PhpStorm.
 * User: DeFacto
 * Date: 04.04.2018
 * Time: 18:14
 */

use common\modules\affiliate\helpers\AffiliateUrlHelper;
use yii\widgets\Menu;

$items     = [

    [
        'url'   => AffiliateUrlHelper::sourcesList(false),
        'label' => _t('front.user', 'Affiliate link')

    ],
    [
        'url'   => AffiliateUrlHelper::sourceWidget(false),
        'label' => _t('front.user', 'Affiliate widget')
    ]

];
$activeUrl = \common\components\UrlHelper::getUrlWithoutParams(Yii::$app->request->getUrl());
foreach ($items as $k => $v) {
    if (strpos($activeUrl, $v['url']) === 0) {
        $items[$k]['active'] = true;
    }
}

$this->beginContent(__DIR__ . '/workbenchLayout.php');
?>

<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="side-nav-mobile">
                <?= Menu::widget([
                    'activateParents' => false,
                    'options'         => ['class' => 'side-nav help-nav', 'id' => 'navMobile'],
                    'encodeLabels'    => false,
                    'items'           => $items,
                ]); ?>
            </div>
        </div>

        <div class="col-sm-9 wide-padding wide-padding--left">
            <?= $content; ?>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>




