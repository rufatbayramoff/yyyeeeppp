<?php

use frontend\modules\workbench\widgets\WorkbenchTabsWidget;

$this->beginContent('@frontend/views/layouts/main.php');
?>

<?= WorkbenchTabsWidget::widget(['section' => 'workbench/affiliates']); ?>

    <div class="container">
        <?= $content; ?>
    </div>

<?php $this->endContent(); ?>