<?php

use frontend\modules\workbench\widgets\CompanySalesSidebarWidget;


$this->title = _t('site.my', 'My Sales');
$this->params['breadcrumbs'][] = ['label' => _t('front.user', 'Private profile'), 'url' => ['/user-profile/']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(); ?>

<div class="container">
    <div class="row ps-index requests-list">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="row">
                    <?= CompanySalesSidebarWidget::widget(['ps' => $ps, 'activeStatusGroup' => CompanySalesSidebarWidget::ACTIVE_STATUS_GROUP_INSTANT_PAYMENT]) ?>
                    <div class="col-sm-9 wide-padding--left">
                        <?= $this->render('filtersBlock', [
                            'searchModel' => $searchModel
                        ]); ?>

                        <?= $this->render('itemsRecvTable', [
                            'dataProvider'  => $dataProvider,
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>