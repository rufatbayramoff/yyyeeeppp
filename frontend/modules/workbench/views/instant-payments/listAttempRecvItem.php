<?php
use common\modules\instantPayment\helpers\InstantPaymentUrlHelper;
use yii\helpers\Url;

/** @var \common\models\InstantPayment $instantPayment */

$instantPayment = $model;
$company        = $instantPayment->toUser->company;

?>
<div class="order-block">
    <div class="row order__info">
        <div class="order__title">
            <h3 class="order__invoice_number">
                <?= _t('site.order', 'Instant payment #{uuid}', ['uuid' => $instantPayment->uuid]); ?>
            </h3>
            <div>
                <?= _t('user.order', 'From'); ?>:
                <?= H($instantPayment->fromUser->getFullNameOrUsername()); ?>
            </div>
            <div class="order__date">
                <?= _t('user.order', 'Created'); ?>:
                <?= app('formatter')->asDatetime($instantPayment->created_at); ?>
            </div>
        </div>

        <div class="order__state">
            <div class="order__status-row">
                <div class="order__payment-status">
                    <div class="order__status">
                        <?= _t('user.order', 'Status'); ?>:
                        <strong><?= $instantPayment->getTextStatus(); ?></strong>
                        <span data-toggle="tooltip" data-placement="top" data-original-title="<?= $instantPayment->getCompanyStatusDescription(); ?>" class="tsi tsi-warning-c"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="order__body p-b10">
        <table class="order__info-table order-model-data__model-table">
            <tbody>
            <tr>
                <td class="order__info-table-label"><?= _t('site.order', 'Description'); ?>: </td>
                <td class="order__info-table-info"><?= H($instantPayment->descr) ?></td>
            </tr>
            <tr>
                <td class="order__info-table-label">
                    <b><?= _t('site.order', 'Award'); ?>: </b>
                </td>
                <td class="order__info-table-info p-r10">
                    <b><?= displayAsMoney($instantPayment->primaryInvoice->getAmountInstantPaymentAward()) ?></b>
                    &nbsp;<span data-toggle="tooltip" data-placement="top" data-original-title="<?=H($instantPayment->getAwardDescription())?>" class="tsi tsi-warning-c"></span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="order__footer">
        <?php if ($instantPayment->isWaitingForConfirm()) { ?>
            <a href="<?=InstantPaymentUrlHelper::approveUrl($instantPayment)?>" class="btn btn-primary btn-sm"><?= _t('site.order', 'Approve')?></a>
        <?php } ?>
        <?php if ($instantPayment->isApproved()) { ?>
            <a href="<?= Url::toRoute(['/payment/receipt/invoice', 'uuid' => $instantPayment->primary_invoice_uuid]); ?>"
               target="_blank" class="btn btn-default btn-sm order__receipt-btn">
                <span class="tsi tsi-doc"></span>
                <?= _t('user.order', 'Receipt'); ?>
            </a>
        <?php } ?>
        <?php if ($instantPayment->allowCancel()) { ?>
            <a href="/workbench/instant-payments/cancel?uuid=<?=$instantPayment->uuid?>"
               class="btn btn-ghost btn-default btn-sm order__receipt-btn"
               data-confirm="Are you sure you want to cancel instant payment?"
               data-method="post"
            >
                <span class="tsi tsi-bin"></span>
                <?= _t('site.order', 'Cancel')?>
            </a>
        <?php } ?>
    </div>
</div>
