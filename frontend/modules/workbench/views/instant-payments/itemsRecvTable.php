<?php
/**
 * @var StoreOrderAttemp[] $attemps Orders attemps
 * @var \frontend\models\ps\StoreOrderAttempSearchForm $form
 */

use common\models\StoreOrderAttemp;

?>
<div>
    <?php if (!$dataProvider->getCount()): ?>
        <h3 class="no-print-requests m-l0 m-r0"><?= _t('site.ps', 'No payments found'); ?></h3>
    <?php else: ?>
        <?php
        $listView = Yii::createObject([
            'class'        => \yii\widgets\ListView::class,
            'dataProvider' => $dataProvider,
            'itemView'     => 'listAttempRecvItem',
        ]);
        ?>
        <?php if ($listView->dataProvider->getCount() > 0): ?>
            <?php echo $listView->renderItems() ?>
            <div class="row">
                <div class="col-md-8">
                    <?php echo $listView->renderPager() ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>