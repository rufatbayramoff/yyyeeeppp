<?php

use frontend\modules\preorder\components\PreorderUrlHelper;
use kartik\widgets\ActiveForm;

/** @var $instantPayment \common\models\InstantPayment */

$forCompany                    = $instantPayment->toUser->company;
$this->title                   = _t('site.my', 'Create instant payment');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(['section' => 'workbench/orders']); ?>

<div class="container" ng-cloak="">
    <div class="row ps-index">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="row mob-layout">
                    <div class="col-sm-8 wide-padding">
                        <div class="panel panel-default box-shadow border-0">
                            <div class="one-print-request ng-scope">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="one-print-request__user">
                                                        <h3 style="margin: 0">
                                                            <?= _t('site.ps', 'Create instant payment to '); ?>
                                                            <a href="<?= $forCompany->getPublicCompanyLink() ?>"><?= H($forCompany->getTitle()) ?></a>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6 text-center">
                                            <div class="one-print-request__btns one-print-request__btns--print-order"
                                                 data-order-id="24">
                                                <a href="#" onclick="window.history.back();"
                                                   class="btn btn-primary">
                                                    <span class="tsi tsi-arrow-back-l"></span> <?= _t('site.ps', 'Back'); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body p-b0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-warning">
                                                <?= _t('site.ps',
                                                    'The service will receive funds immediately. If you want a guarantee of work and postpay services, use <a href="{createUrl}" target="_blank">Get a Quote</a>.',
                                                    [
                                                        'createUrl' => PreorderUrlHelper::getCreateOfferForPs($instantPayment->toUser->company)
                                                    ]
                                                ); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php $form = ActiveForm::begin(['options' => [
                                    'method'  => 'post',
                                    'enctype' => 'multipart/form-data'
                                ]]) ?>
                                <div class="panel-body p-b0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($instantPayment, 'descr')->textarea(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?= $form->field($instantPayment, 'sum')->input('number')->label(_t('site.ps', 'Price')) ?>
                                        </div>
                                        <div class="col-md-6">
                                            <?= $form->field($instantPayment, 'currency')->dropDownList(\lib\money\Currency::ALLOW_SELECT_CURRENCIES, ['class' => '']) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body p-b0">
                                    <div class="row">
                                        <div class="col-md-12 p-b30">
                                            <input type="submit" class="btn btn-primary" value="Pay">
                                        </div>
                                    </div>
                                </div>
                                <?php ActiveForm::end() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
