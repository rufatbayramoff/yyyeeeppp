<?php

use backend\models\search\InstantPaymentSearch;
use frontend\models\ps\StoreOrderAttempSearchForm;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/** @var $searchModel  \backend\models\search\InstantPaymentSearch */
?>
<div class="filtersBlock">
    <?php $filterForm = ActiveForm::begin([
        'action'                 => [''],
        'method'                 => 'get',
        'enableClientValidation' => false,
        'id'                     => 'filter-form',
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?=
            $filterForm->field($searchModel, 'searchText', [
                'addon' => [
                    'append' => [
                        'content'  => Html::submitButton('<i class="tsi tsi-search"></i>',
                            ['class' => 'btn btn-default p-l0 p-r0', 'id' => 'search-button']),
                        'asButton' => true
                    ]
                ]
            ])
                ->textInput(['autocomplete' => 'off', 'maxlength' => StoreOrderAttempSearchForm::ORDER_SEARCH_MAX_LENGTH])
                ->label(false)
                ->input("", ["class" => ""]);
            ?>
        </div>
        <div class="col-md-6">
            <label class="m-r10" style="display: inline-block;"><?= _t('site.order', 'Filter by'); ?>:</label>
            <select class="form-control" onchange="$('#filter-form').submit();" name="InstantPaymentSearch[filterBy]"
                    style="display: inline-block; width: auto">
                <option value="<?php echo InstantPaymentSearch::FILTER_BY_ALL; ?>"><?= _t('site.order', 'All'); ?></option>
                <option value="<?php echo InstantPaymentSearch::FILTER_BY_ACTIVE; ?>" <?= in_array($searchModel->filterStatus, [InstantPaymentSearch::FILTER_BY_ACTIVE_PAYED, InstantPaymentSearch::FILTER_BY_ACTIVE]) ? 'selected="selected"' : ''; ?>>
                    <?= _t('site.order', 'Active'); ?>
                </option>
                <option value="<?php echo InstantPaymentSearch::FILTER_BY_CANCEL; ?>" <?= $searchModel->filterStatus === InstantPaymentSearch::FILTER_BY_CANCEL ? 'selected="selected"' : ''; ?>>
                    <?= _t('site.order', 'Canceled'); ?>
                </option>
            </select>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
