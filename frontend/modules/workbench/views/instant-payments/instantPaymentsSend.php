<?php


$this->title                   = _t('site.my', 'My Purchases');
$this->params['breadcrumbs'][] = ['label' => _t('front.user', 'Private profile'), 'url' => ['/user-profile/']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= \frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(['section' => 'workbench/orders']); ?>

<div class="container">
    <div class="row order-page">
        <div class="col-sm-3 sidebar">
            <div class="side-nav-mobile">
                <div class="side-nav-mobile__title" data-toggle="collapse" data-target="#navPurchases">
                    <span id="js-section-title"><?= _t('site.ps', 'Orders Status') ?></span>
                    <span class="tsi tsi-down"></span>
                </div>
                <?= \frontend\modules\workbench\widgets\CustomerPurchasesSidebarWidget::widget([]); ?>
            </div>
        </div>

        <div class="col-sm-9 wide-padding wide-padding--left">
            <?= $this->render('filtersBlock', [
                'searchModel' => $searchModel
            ]); ?>

            <?php if ($currentUser->status !== \common\models\User::STATUS_ACTIVE): ?>
                <div class="alert alert-warning alert--text-normal" role="alert">
                    <?php echo _t('site.preorder.confirm',
                        "We’ve sent you a confirmation email. Please click the link in that email to have your RFQ submitted to the supplier."); ?>
                </div>
            <?php endif; ?>

            <?= $this->render('itemsSendTable', [
                'dataProvider' => $dataProvider,
            ]); ?>

        </div>
    </div>
</div>
