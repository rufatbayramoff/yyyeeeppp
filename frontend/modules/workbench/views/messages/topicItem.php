<?php

use common\models\message\helpers\UrlHelper;
use common\models\MsgFolder;
use common\models\User;
use frontend\components\UserUtils;
use frontend\models\community\MessageFacade;
use frontend\models\user\UserFacade;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/** @var \common\models\MsgFolder $type */
/** @var \common\models\MsgTopic $model */
/** @var \common\models\User $user */

$lastMessage = MessageFacade::getLastFilteredMessage($user, $model);
$isReply     = $lastMessage && $lastMessage->user_id == $user->id;   // Является ли текущее сообщение ответом


/**
 * Redner message files list
 *
 * @param array $msgFiles
 * @return null|string
 */
$filesFn = function (array $msgFiles): ?string {

    if (!$msgFiles) {
        return null;
    }

    $filesNames = \common\components\ArrayHelper::getColumn($msgFiles, 'file.name');
    return _t('site.messages', 'Attached files : {files}', ['files' => implode(', ', $filesNames)]);
};


?>
<div class="chat-message <?= $model->getMemberForUser($user)->have_unreaded_messages ? 'chat-unread' : '' ?>">
    <?php if($model->getMemberForUser($user)->have_unreaded_messages):?>
        <div class="chat-unread__badge"></div>
    <?php endif;?>
    <div class="row">

        <?php if (!$model->isBotSupport()) { ?>
            <div class="chat-message__close">
                <?php if ($model->getMemberForUser($user)->folder_id != MsgFolder::FOLDER_ID_ARCHIVE): ?>
                    <?= Html::a(
                        '<span aria-hidden="true">&times;</span>',
                        UrlHelper::archiveRoute($model),
                        ['class' => 'close', 'data-method' => 'post']
                    ); ?>
                <?php else : ?>
                    <?= Html::a(
                        '<span aria-hidden="true">&times;</span>',
                        UrlHelper::deleteRoute($model),
                        ['class' => 'close', 'data-method' => 'post']
                    ); ?>

                <?php endif ?>
            </div>
        <?php } ?>

        <a href="<?= yii\helpers\Url::toRoute(UrlHelper::topicRoute($model)); ?>" class="chat-message__gotolink">
            <div class="col-xs-12 col-md-4">
                <div class="chat-message-user">
                    <?php foreach ($model->getOtherUsers($user) as $otherUser): ?>
                        <div class="chat-message-user__avatar">
                            <?php if ($otherUser->company): ?>
                                <?= Html::img($otherUser->company->getCompanyLogoOrDefault(false, 50)) ?>
                            <?php else: ?>
                                <?= UserUtils::getAvatarForUser($otherUser, 'message'); ?>
                            <?php endif; ?>
                        </div>
                        <div class="chat-message-user__info">
                            <div class="chat-message-user__author-name text-danger">
                                <?php if ($otherUser->company): ?>
                                    <?= H($otherUser->company->title) ?>
                                <?php else: ?>
                                    <?= H(UserFacade::getFormattedUserName($otherUser)); ?>
                                <?php endif; ?>
                            </div>
                            <div class="chat-date">
                                <?= $lastMessage ? Yii::$app->formatter->asRelativeTime($lastMessage->created_at) : ''; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="col-xs-12 col-md-8 chat-message-body">
                <h3 class="chat-message__topic-title"><?= \H($model->title); ?></h3>
                <div class="chat-message-text">
                    <h4><?= H($model->title); ?>
                        <?php if ($model->bind_to == 'order' && stripos($model->title, 'order') === false) {
                            echo " (Order #" . $model->bind_id . ")";
                        } ?>
                    </h4>

                    <?php if ($isReply): ?>
                        <div class="chat-message__recepient-avatar">
                            <?php if ($user->company): ?>
                                <?= Html::img($user->company->getCompanyLogoOrDefault(false, 50)) ?>
                            <?php else: ?>
                                <?= UserUtils::getAvatarForUser($user, 'message'); ?>
                            <?php endif; ?>
                        </div>
                    <?php endif ?>

                    <div class="chat-message__topic-text">
                        <?php if ($lastMessage): ?>
                            <?php echo StringHelper::truncateWords(\H(trim($lastMessage->text)), 10); ?>
                            <?php if ($model->isBlocked) { ?>
                                <div class="chat-message__topic-blocked"><?= _t('site', 'This topic is blocked.') ?></div>
                                <?php
                            }
                            ?>

                            <div><?= $filesFn($lastMessage->msgFiles) ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </a>

    </div>

</div>