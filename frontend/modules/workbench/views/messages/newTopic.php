<?php
/** @var \common\models\message\forms\TopicForm $topicForm */

use common\components\FileTypesHelper;
use common\modules\captcha\widgets\GoogleRecaptcha2;
use frontend\models\user\UserFacade;

/** @var \common\models\MsgFolder $folder */

/** @var \common\models\User $user */
/** @var \yii\bootstrap\ActiveForm $form */

/** @var \yii\web\View $this */
$this->beginContent('@frontend/modules/workbench/views/messages/layout.php', ['user' => $user, 'currentFolder' => $folder]);
$this->registerJs("TS.uploadInit();");
?>
    <h3 class="direct-chat__page-title"><?php echo _t('fron.site', 'New message'); ?></h3>
    <div class="form-new-message">
        <?php if ($user->status == \common\models\User::STATUS_UNCONFIRMED): ?>
            <div class="alert alert-warning">
                <?= _t('site.user', 'Please confirm your email in order to send messages'); ?>
            </div>
        <?php else: ?>


            <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?php if ($topicForm->scenario == \common\models\message\forms\TopicForm::SCENARIO_PERSONAL_WITH_UNKNOWN_USER): ?>

                <?=
                $form->field($topicForm, 'memberUserId')->widget(
                    kartik\select2\Select2::classname(),
                    [
                        'initValueText' => '',
                        'options'       => ['placeholder' => _t('front.messages', 'Select user')],
                        'pluginOptions' => [
                            'allowClear'         => false,
                            'minimumInputLength' => 2,
                            'ajax'               => [
                                'url'      => \yii\helpers\Url::to(['/workbench/messages/user-list']),
                                'dataType' => 'json',
                                'data'     => new yii\web\JsExpression('function(params) { return {query:params.term}; }')
                            ],
                        ],
                    ]
                );
                ?>

            <?php else: ?>

                <div class="topicform-memberuser-directmsg">
                    <?= _t('front.site', 'With user') ?>
                    <span>
                        "<?= H(UserFacade::getFormattedUserName($topicForm->getMemberUser())) ?>"
                    </span>
                </div>

            <?php endif; ?>

            <?= $form->field($topicForm, 'title')
            ->label(false)
            ->textInput(['maxlength' => true, 'placeholder' => _t('front.site', 'Subject')])
            ?>

            <?= $form->field($topicForm, 'message')
            ->label(false)
            ->textArea(['rows' => 10, 'placeholder' => _t('front.site', 'Your message')])
            ?>

            <div class="form-group">
                <?= GoogleRecaptcha2::widget() ?>
            </div>

            <?=
            $form->field($topicForm, 'files[]', ['template' => '{input}{label}{error}'])
                ->fileInput([
                    'class'    => 'inputfile',
                    'multiple' => true,
                    'accept'   => implode(', ', array_map(function (string $ext) {
                        return '.' . $ext;
                    }, FileTypesHelper::getAllowExtensions())),
                ])
                ->label('<span><i class="tsi tsi-upload-l"></i> ' . _t('front.user', 'Choose a file') . '</span>', ['class' => 'uploadlabel']);
            ?>

            <?= $form->field($topicForm, 'bindTo')->label(false)->hiddenInput() ?>
            <?= $form->field($topicForm, 'bindId')->label(false)->hiddenInput() ?>

            <div class="form-group">
                <?= yii\helpers\Html::submitButton(_t('front.site', ' Send'), ['class' => 'btn btn-primary']); ?>
                <?php if (!Yii::$app->request->isAjax && Yii::$app->request->referrer): ?>
                    <?= yii\helpers\Html::a(_t('front.site', 'Cancel'), Yii::$app->request->referrer, ['class' => 'btn btn-link']); ?>
                <?php endif ?>
                <?php
                if ($topicForm->scenario !== \common\models\message\forms\TopicForm::SCENARIO_PERSONAL_WITH_UNKNOWN_USER) {
                    $toUser     = $topicForm->getMemberUser();
                    $userPsList = $toUser->ps;
                    $toUserPs   = reset($userPsList);
                    if ($toUserPs && $toUserPs->isCanPrintInStore()) {
                        ?>
                        <button href="/store/print-by/ps/<?= $toUserPs->id ?>" class="btn ts-ajax-modal"
                                title='<?= _t('public.ps', 'Print with ') . '"' . H($toUserPs->title) . '"' ?>'>
                            <span class="tsi tsi-printer3d"></span>
                            <?= _t('public.ps', 'Print with ') . '"' . H($toUserPs->title) . '"' ?>
                        </button>
                        <?php
                    }
                }
                ?>
            </div>

            <?php \yii\bootstrap\ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
<?php $this->endContent(); ?>