<?php

use common\components\ArrayHelper;
use common\models\MsgFolder;
use common\models\MsgTopic;
use common\modules\instantPayment\helpers\InstantPaymentUrlHelper;
use frontend\assets\DropzoneAsset;
use frontend\models\community\MessageFacade;
use frontend\models\community\ReportMessageForm;
use frontend\modules\preorder\components\PreorderUrlHelper;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \frontend\modules\workbench\models\view\MessageViewModel $viewModel */

MessageFacade::filterOneTopic($viewModel->topic, $viewModel->user);
DropzoneAsset::register($this);

Yii::$app->angular
    ->service(['notify', 'user', 'router', 'modal'])
    ->directive(['enter-click', 'dropzone-button'])
    ->controller('messages/topic')
    ->controllerParams(
        [
            'topic'                => ArrayHelper::toArray($viewModel->topic, [MsgTopic::class => ['id']]),
            'messages'             => $viewModel->messages,
            'reportMessageReasons' => ReportMessageForm::getSuggestObjectList(),
        ]
    );


$member = $viewModel->topic->getMemberForUser($viewModel->user);
$folder = $member->getIsDeleted() ? $member->originalFolder : $member->folder;
$this->beginContent('@frontend/modules/workbench/views/messages/layout.php', ['user' => $viewModel->user, 'currentFolder' => $folder]);
$currentUrl        = yii\helpers\Url::current();
$otherUsers        = $viewModel->topic->getOtherUsers($viewModel->user);
$otherUsersNames   = $viewModel->topic->getChatUsersList($viewModel->user);
$instantPaymentUrl = !$viewModel->isPreorder() ? InstantPaymentUrlHelper::getByTopicBind($viewModel->user, $viewModel->topic) : '';
$makeOfferUrl      = '';
if ($viewModel->user && $viewModel->user->company && $viewModel->user->company->canMakeOffer() && $folder->id !== MsgFolder::FOLDER_ID_SUPPORT && !$viewModel->isPreorder()) {
    foreach ($otherUsers as $oneOtherUser) {
        $customerUser = $oneOtherUser; //Possible should add some filter params
        break;
    }
    if ($customerUser->allow_incoming_quotes) {
        $makeOfferUrl = HL(PreorderUrlHelper::getCreateOfferUrl($customerUser));
    }
}
$createOrderUrl = '';
$getQuoteUrl    = '';
if ($otherUsers) {
    $toUser                = reset($otherUsers);
    $viewModel->userPsList = $toUser->ps;
    $toUserPs              = reset($viewModel->userPsList);

    if ($toUserPs && $toUserPs->isCanPrintInStore() && !$viewModel->isPreorder()) {
        $createOrderUrl = '/store/print-by/ps/' . $toUserPs->id;
        /** @var \common\models\CompanyService $companyService */
        $companyServices = $toUserPs->companyServices;
        $companyService  = reset($companyServices);
        if ($companyService) {
            $getQuoteUrl = HL($companyService->getCreateQuoteUrl());
        }
    }
}
    ?>
    <div class="box direct-chat" ng-controller="TopicController" ng-cloak>

        <div class="row">
            <div class="col-md-12">
                <h3 class="direct-chat__page-title">
                    <?=
                    _t(
                        'front.chat',
                        'Dialogue with {usernames} with subject: {theme}',
                        [
                            'usernames' => implode(', ', $otherUsersNames),
                            'theme'     => H($viewModel->topicTitle())
                        ]
                    );
                    ?>
                    <?php if (!$viewModel->topic->isBotSupport() && !$member->getIsDeleted()): ?>
                        <div class="direct-chat__delete">
                            <?php if ($folder->id != MsgFolder::FOLDER_ID_ARCHIVE): ?>
                                <?= Html::a(
                                    '<span aria-hidden="true" class="tsi tsi-bin"></span>' . _t('frontend.message', 'Archive dialogue'),
                                    \common\models\message\helpers\UrlHelper::archiveRoute($viewModel->topic),
                                    ['class' => 'close', 'data-method' => 'post']
                                ); ?>

                            <?php else : ?>

                                <?= Html::a(
                                    '<span aria-hidden="true" class="tsi tsi-bin"></span>' . _t('frontend.message', 'Delete dialogue'),
                                    \common\models\message\helpers\UrlHelper::deleteRoute($viewModel->topic),
                                    ['class' => 'close', 'data-method' => 'post']
                                ); ?>

                            <?php endif ?>
                        </div>
                    <?php endif ?>
                </h3>
            </div>
        </div>

        <?php if ($viewModel->isPreorder() && $viewModel->attempt()): ?>
            <?php echo $this->render('partials/_preorder', ['viewModel' => $viewModel]) ?>
        <?php endif; ?>
        <?php if ($viewModel->isBindToOrder() && $viewModel->attempt()): ?>
            <?php echo $this->render('partials/_order', ['viewModel' => $viewModel]) ?>
        <?php endif; ?>
        <div class="box-body" style="display: block;">
            <div class="direct-chat-messages">

                <div
                        class="direct-chat-msg"
                        ng-class="{'author' : message.user.isAuthor}"
                        ng-repeat="message in messages">

                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name text-danger">{{message.user.username}}</span>
                        <span class="direct-chat-timestamp">{{message.timeString}}</span>
                    </div>
                    <div ng-bind-html="message.user.avatar"></div>
                    <div class="direct-chat-text" ng-class="{'direct-chat-blocked' : message.isBlocked}">
                        <span ng-bind-html="message.text"></span>
                        <div class="direct-chat-blocked-text" ng-if="message.isBlocked">
                            (<?= _t('site', 'Sorry, but this message was blocked.') ?>)
                        </div>


                        <div
                                ng-if="message.files.length > 0"
                                class="m-t10">
                            <div
                                    class="m-b10"
                                    ng-repeat="file in message.files">

                                <a href="{{file.url}}" target="_blank">{{file.filename}}</a>

                                <button
                                        ng-if="message.user.isAuthor"
                                        loader-click="deleteFile(message, file)"
                                        class="btn btn-link btn-xs p-l0 m-l10 text-muted"
                                        type="button">
                                    <span class="tsi tsi-remove"></span>
                                </button>


                            </div>
                            <div>{{message.filesExpiredMsg}}</div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <a href="<?= $currentUrl; ?>" class="refresh-chat"></a>

        <div class="box-footer" style="display: block;">

            <div class="row">
                <?php if (!$viewModel->topic->isBotSupport()) { ?>
                    <div class="col-xs-12">
                        <div class="chat-input">
                        <textarea
                                onkeyup="textAreaAdjust(this)"
                                ng-model="currentMessage"
                                ctrl-enter-click="sendMessage()"
                                name="message"
                                placeholder="<?= _t('frontend.message', 'Type Message ...') ?>"
                                class="form-control chat-input__text chat-send-message"
                                rows="2"
                                maxlength="2000"></textarea>

                            <span class="chat-input__attach">
                            <button
                                    dropzone-button="files"
                                    accepted-files="<?= '.' . implode(', .', common\components\FileTypesHelper::getAllowExtensions()) ?>"
                                    max-files="50"
                                    class="btn btn-default"><span class="tsi tsi-paperclip"></span></button>
                        </span>

                            <span class="chat-input__send">
                            <button
                                    ng-click="sendMessage()"
                                    ng-disabled="!sendButtonEnabled"
                                    type="button"
                                    class="btn btn-primary chat-send"><?= _t('frontend.message', 'Send') ?></button>
                        </span>
                        </div>

                        <div ng-if="files.length > 0" class="direct-chat__attach-result">
                            <h4><?= _t('frontend.message', 'Attached files:') ?></h4>
                            <div
                                    class="direct-chat__attach-result-item"
                                    ng-repeat="file in files">{{file.name}}

                                <button class="btn btn-xs btn-link p-l0 p-r0 m-l10 text-muted"
                                        type="button"
                                        ng-click="deleteAttachedFile(file)">
                                    <span class="tsi tsi-remove"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="chat-alert">
            <h4 class="chat-alert__title">
                <span class="tsi tsi-warning-c"></span>
                <?= _t('frontend.message', 'Safety tip') ?>
            </h4>
            <?php
            $helpLink = yii\helpers\Html::a("Help Center", ['/help/article/48-how-do-i-know-my-information-is-secure'], ['target' => '_blank']);
            echo _t(
                'frontend.message',
                'To avoid the risk of potential fraud and to ensure the protection of your personal information, you should only communicate and pay directly through Treatstock. To learn more, please visit our {help}.',
                ['help' => $helpLink]
            );
            ?>
        </div>

    </div>
    <?= $this->render('_messaging-problem-modal') ?>

    <?php $this->beginBlock('sidebar'); ?>
    <div class="row">
        <div class="col-xs-3 col-sm-12">
            <?= Html::a(
                '<span class="tsi tsi-left"></span>' . '<span class="hidden-xs m-l10">' . _t('front.site', 'Back') . '</span>',
                \common\models\message\helpers\UrlHelper::toFolderRoute($folder),
                ['class' => 'btn btn-default btn-block m-b20 p-l10 p-r10']
            ); ?>

        </div>

        <div class="col-xs-9 col-sm-12">
            <?php if ($instantPaymentUrl || $makeOfferUrl || $createOrderUrl || $getQuoteUrl) { ?>
            <div class="dropdown m-b20">
                <button class="btn btn-default btn-block dropdown-toggle" type="button" id="dropdownOrderAdd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= _t('site.ps', 'Order Add-Ons'); ?>
                    <span class="tsi tsi-down"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownOrderAdd">
                    <?php if ($makeOfferUrl) { ?>
                        <li>
                            <a href="<?= $makeOfferUrl ?>">
                            <span class="dropdown-menu__title">
                                <?= _t('site.ps', 'Make offer'); ?>
                            </span>
                                <span class="dropdown-menu__description">
                                <?= _t('site.ps', 'Create a pre-order'); ?>
                            </span>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ($instantPaymentUrl) { ?>
                        <li>
                            <a href="<?= $instantPaymentUrl ?>">
                            <span class="dropdown-menu__title">
                                <?= _t('site.ps', 'Instant payment'); ?>
                            </span>
                                <span class="dropdown-menu__description">
                                <?= _t('site.ps', 'Transfer money instantly'); ?>
                            </span>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ($createOrderUrl) { ?>
                        <li>
                            <a href="<?=$createOrderUrl?>"
                               class="ts-ajax-modal"
                               data-categoryga="print"
                               data-actionga="click"
                               data-labelga="print_from_message"
                               title='<?= _t('public.ps', 'Create Order') ?>'>
                            <span class="dropdown-menu__title">
                                <?= _t('site.ps', 'Create order'); ?>
                            </span>
                                <span class="dropdown-menu__description">
                                <?= _t('site.ps', 'Place an instant order'); ?>
                            </span>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ($getQuoteUrl) { ?>

                        <li>
                            <a href="<?= $getQuoteUrl ?>">
                            <span class="dropdown-menu__title">
                                <?= _t('site.ps', 'Get a Quote'); ?>
                            </span>
                                <span class="dropdown-menu__description">
                                <?= _t('site.ps', 'For a custom-made or specific request'); ?>
                            </span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <?php }?>
        </div>
    </div>
    <?php $this->endBlock(); ?>

    <?php $this->endContent(); ?>