<?php /** @var \frontend\modules\workbench\models\view\MessageViewModel $viewModel */

use frontend\modules\preorder\components\PreorderUrlHelper; ?>

<div class="direct-chat__order clearfix">
    <div class="direct-chat__order-num">
        <?php echo $viewModel->link(); ?>
        <a ng-click="showOrderInfo = false" ng-show="showOrderInfo" href="#" class="direct-chat__order-hide-btn">
            <i class="tsi tsi-down m-r5"></i><?= _t('site', 'Show') ?>
        </a>
        <a ng-click="showOrderInfo = true" ng-show="!showOrderInfo" ="#" class="direct-chat__order-hide-btn">
        <i class="tsi tsi-up m-r5"></i><?= _t('site', 'Hide') ?>
        </a>
    </div>
    <div class="row" ng-class="{hide: showOrderInfo}">
        <?php if($viewModel->preorderHasDescription()):?>
            <div class="col-md-9">
                <h4 class="m-t0 m-b0"><?php echo _t('site', 'Project description') ?></h4>
                <p><?php echo H($viewModel->preorderDescription()); ?></p>
            </div>
        <?php endif;?>
        <div class="col-md-3">
            <p class="m-b0">
                <strong><?php echo _t('site', 'Budget') ?></strong>: <?php echo $viewModel->preorderBudget()?>
            </p>
            <p>
                <strong><?php echo _t('site', 'Deadline') ?></strong>: <?php echo $viewModel->preorderDeadline()?>
            </p>
        </div>
    </div>
    <?php if($viewModel->preorderFiles()):?>
    <div class="direct-chat__order-files">
        <h4 class="m-t0"><?php echo _t('site', 'Attached files') ?></h4>
        <?php foreach ($viewModel->preorderFiles() as $file):?>
            <div class="simple-file-link">
                <a class="simple-file-link__link" href="<?php echo PreorderUrlHelper::downloadPreorderFile($viewModel->bindObject(), $file) ?>">
                    <span><?php echo H($file->name); ?></span>
                    <i><?php echo \Yii::$app->formatter->asShortSize($file->size, 2); ?></i>
                </a>
            </div>
        <?php endforeach;?>
    </div>
    <?php endif;?>
    <?php if($viewModel->isPreorder()):?>
        <div class="direct-chat__order-num">
            <div class="col-xs-2 col-sm-2">
                <a href="<?php echo HL($viewModel->preorderCustomerUrl())?>"
                   class="btn btn-danger btn-ghost btn-block m-b20 p-l10 p-r10"
                   data-categoryga="print"
                   data-actionga="click"
                   data-labelga="print_from_message"
                   title='<?= _t('public.ps', 'View offer')?>'>
                    <?= _t('public.ps', 'View offer')?>
                </a>
            </div>
        </div>
    <?php endif;?>
</div>