<?php
/** @var \frontend\modules\workbench\models\view\MessageViewModel $viewModel */
?>
<div class="direct-chat__order clearfix">
    <div class="direct-chat__order-num clearfix">
        <?php echo $viewModel->link(); ?>
        <?php if($status = $viewModel->orderStatus()):?>
        <span class="label label-success m-l10" title="<?= _t('site', "Status: $status") ?>">
                    <?= _t('site', "Status: $status") ?>
                </span>
        <?php endif;?>
        <a ng-click="showOrderInfo = false" ng-show="showOrderInfo" href="#" class="direct-chat__order-hide-btn">
            <i class="tsi tsi-down m-r5"></i><?= _t('site', 'Show') ?>
        </a>
        <a ng-click="showOrderInfo = true" ng-show="!showOrderInfo" ="#" class="direct-chat__order-hide-btn">
            <i class="tsi tsi-up m-r5"></i><?= _t('site', 'Hide') ?>
        </a>
    </div>
    <div id="order-info" class="row" ng-class="{hide: showOrderInfo}">
        <div class="col-xs-4 col-sm-3 col-md-2 p-r0">
            <?php echo $viewModel->previewModel()?>
            <p class="text-center">
                <?php echo $viewModel->linkWithText(_t('site', 'View files'))?>
            </p>
        </div>
        <div class="col-xs-8 col-sm-6 col-md-7">
            <?php if($viewModel->orderHasDescription()):?>
                <h4 class="m-t0 m-b0"><?= _t('site', 'Project description') ?></h4>
                <?= H($viewModel->orderDescription()); ?>
                <?= H($viewModel->orderMessage()); ?>
            <?php endif;?>
            <?php if(!empty($viewModel->materials())):?>
            <div class="m-b15">
                <div>
                    <strong><?= _t('site', 'Materials') ?></strong>
                </div>
                <div class="direct-chat__order-material">
                    <?php if(isset($viewModel->materials()['material'])):?>
                        <span><?php echo _t('site', $viewModel->materials()['material']) ?></span>
                    <?php endif;?>
                    <?php if(isset($viewModel->materials()['color'])):?>
                        <div class="material-item">
                            <div class="material-item__color" style="background-color: rgb(<?php echo H($viewModel->materials()['color']['rgb'])?>)"></div>
                            <div class="material-item__label">
                                <?php echo H($viewModel->materials()['color']['title'])?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            <?php endif;?>
        </div>
        <?php if($viewModel->isCustomer()):?>
            <div class="col-xs-offset-4 col-xs-8 col-sm-offset-0 col-sm-3 col-md-3">
                <div>
                    <strong><?php echo _t('payment.receipt', 'Total'); ?></strong>
                </div>
                <p>
                    <?php echo $viewModel->getAmountTotalWithRefund()?>
                </p>
            </div>
        <?php elseif($fee = $viewModel->manufacturePrice()):?>
            <div class="col-xs-offset-4 col-xs-8 col-sm-offset-0 col-sm-3 col-md-3">
                <div>
                    <strong><?= _t('site', 'Manufacturing Fee') ?></strong>
                </div>
                <p>
                    <?php echo $fee?>
                </p>
            </div>
        <?php endif;?>
    </div>
</div>