<?php

use common\components\ArrayHelper;
use common\components\FileTypesHelper;
use common\models\MsgTopic;
use frontend\assets\DropzoneAsset;
use frontend\models\community\MessageFacade;
use frontend\models\community\ReportMessageForm;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \common\models\User $user */
/** @var MsgTopic $topic */
/** @var string $instantPaymentUrl */


MessageFacade::filterOneTopic($topic, $user);
DropzoneAsset::register($this);

Yii::$app->angular
    ->service(['notify', 'user', 'router', 'modal'])
    ->directive(['enter-click', 'dropzone-button'])
    ->controller('messages/topic')
    ->controllerParams(
        [
            'topic'                => ArrayHelper::toArray($topic, [MsgTopic::class => ['id']]),
            'messages'             => $messages,
            'reportMessageReasons' => ReportMessageForm::getSuggestObjectList(),
        ]
    );


$member = $topic->getMemberForUser($user);
$folder = $member->getIsDeleted() ? $member->originalFolder : $member->folder;
$currentUrl = yii\helpers\Url::current();
$otherUsersNames = $topic->getChatUsersList($user);
?>

    <div class="box direct-chat order-item clearfix" ng-controller="TopicController" ng-cloak>

        <h4 class="direct-chat__page-title">
            <?=
            _t(
                'front.chat',
                'Dialogue with {usernames} with subject: {theme}',
                [
                    'usernames' => implode(', ', $otherUsersNames),
                    'theme'     => H($topic->title)
                ]
            );
            ?>
        </h4>

        <div class="box-body">
            <div class="direct-chat-messages">

                <div
                    class="direct-chat-msg"
                    ng-class="{'author' : message.user.isAuthor}"
                    ng-repeat="message in messages">

                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name text-danger">{{message.user.username}}</span>
                        <span class="direct-chat-timestamp">{{message.timeString}}</span>
                    </div>
                    <div ng-bind-html="message.user.avatar"></div>
                    <div class="direct-chat-text" ng-class="{'direct-chat-blocked' : message.isBlocked}">
                        <span ng-bind-html="message.text"></span>
                        <div class="direct-chat-blocked-text" ng-if="message.isBlocked"> (<?= _t('site', 'Sorry, but this message was blocked.') ?>)</div>


                        <div
                            ng-if="message.files.length > 0"
                            class="m-t10">
                            <div
                                class="m-b10"
                                ng-repeat="file in message.files">

                                <a href="{{file.url}}" target="_blank">{{file.filename}}</a>

                                <button
                                    ng-if="message.user.isAuthor"
                                    loader-click="deleteFile(message, file)"
                                    class="btn btn-link btn-xs p-l0 m-l10 text-muted"
                                    type="button">
                                    <span class="tsi tsi-remove"></span>
                                </button>


                            </div>
                            <div>{{message.filesExpiredMsg}}</div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

        <a href="<?= $currentUrl; ?>" class="refresh-chat"></a>

        <div class="box-footer" style="display: block;">

            <div class="chat-input">
                <textarea
                    onkeyup="textAreaAdjust(this)"
                    ng-model="currentMessage"
                    ctrl-enter-click="sendMessage()"
                    name="message"
                    placeholder="<?= _t('frontend.message', 'Type Message ...') ?>"
                    class="form-control chat-input__text chat-send-message"
                    maxlength="2000"></textarea>

                <span class="chat-input__attach">
                    <button
                        dropzone-button="files"
                        accepted-files="<?= '.'. implode(', .', common\components\FileTypesHelper::getAllowExtensions())?>"
                        max-files="50"
                        class="btn btn-default"><span class="tsi tsi-paperclip"></span></button>
                </span>

                <span class="chat-input__send">
                    <button
                        ng-click="sendMessage()"
                        ng-disabled="!sendButtonEnabled"
                        type="button"
                        class="btn btn-primary chat-send"><?= _t('frontend.message', 'Send') ?></button>
                </span>
            </div>

            <div ng-if="files.length > 0" class="direct-chat__attach-result">
                <h4><?=_t('frontend.message', 'Attached files:')?></h4>
                <div
                    class="direct-chat__attach-result-item"
                    ng-repeat="file in files">{{file.name}}

                    <button class="btn btn-xs btn-link p-l0 p-r0 m-l10 text-muted"
                        type="button"
                        ng-click="deleteAttachedFile(file)">
                        <span class="tsi tsi-remove"></span>
                    </button>
                </div>
            </div>


            <?php /*
            <p><?=_t('site.messages', 'Personal messages support {files} only.', ['files' => implode(', ', FileTypesHelper::getAllowExtensions())])?></p>
            */ ?>

            <!--
            <?= Html::a(
                _t('front.site', 'Back to all messages'),
                \common\models\message\helpers\UrlHelper::toFolderRoute($folder),
                ['class' => 'btn btn-default btn-sm m-b20']
            ); ?> -->
        </div>

        <div class="chat-alert">
            <h4 class="chat-alert__title">
                <span class="tsi tsi-warning-c"></span>
                <?= _t('frontend.message', 'Safety tip') ?>
            </h4>
            <?php
            $helpLink = yii\helpers\Html::a("Help Center", ['/help/article/48-how-do-i-know-my-information-is-secure'], ['target' => '_blank']);
            echo _t(
                'frontend.message',
                'To avoid the risk of potential fraud and to ensure the protection of your personal information, you should only communicate and pay directly through Treatstock. To learn more, please visit our {help}.',
                ['help' => $helpLink]
            );
            ?>
        </div>

    </div>


   <?= $this->render('_messaging-problem-modal')?>