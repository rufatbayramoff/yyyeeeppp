<?php
/** @var \yii\base\View $this */

use common\models\MsgMember;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\User $user */
/** @var \yii\data\ArrayDataProvider $topicsProvider */
/** @var \common\models\MsgFolder $folder */
/** @var string $searchValue */
/** @var string $type */

$this->beginContent('@frontend/modules/workbench/views/messages/layout.php', ['user' => $user, 'currentFolder' => $folder]);

/** @var \yii\widgets\ListView $listView */
$listView = Yii::createObject([
    'class' => \yii\widgets\ListView::class,
    'dataProvider' => $topicsProvider,
    'itemView' => 'topicItem',
    'viewParams' => [
        'user' => $user,
        'folder' => $folder,
    ],
]);

?>
<div class="row">
    <div class="col-md-8 m-b15 m-t5">
        <label class="m-r10">
            <?= _t('front.messages', 'Show'); ?>
        </label>
        <div class="btn-group btn-group-sm chat-filter">
            <label class="btn btn-default <?php echo $type === 'all' ? 'active' : ''?>">
                <?php echo Html::a(_t('front.messages', 'All'),['/messages']) ?>
            </label>
            <label class="btn btn-default <?php echo $type === MsgMember::TYPE_UNREAD ? 'active' : ''?>">
                <?php echo Html::a(_t('front.messages', 'Unread'),['unread']) ?>
            </label>
            <label class="btn btn-default <?php echo $type === MsgMember::TYPE_WITHOUT_ANSWER ? 'active' : ''?>">
                <?php echo Html::a(_t('front.messages', 'Without Answer'),['without-answer']) ?>
            </label>
        </div>
    </div>
    <div class="col-md-4">
        <?php echo Html::beginForm(Url::toRoute(['/workbench/messages/search']), 'GET'); ?>
        <div class="input-group m-b20">
            <?php echo Html::textInput('search', $searchValue, ['class'=>'form-control', 'placeholder'=>'Search...']); ?>
            <span class="input-group-btn">
                <?php echo Html::submitButton('<span class="tsi tsi-search"></span>', [
                    'class' => 'btn btn-info btn-ghost p-l20 p-r20'
                ]); ?>
            </span>
        </div>
        <?php echo Html::endForm(); ?>
    </div>
</div>

<?php if($listView->dataProvider->getCount() > 0):?>

    <!-- Items -->

    <div>
        <?=$listView->renderItems();?>
    </div>

    <!-- Pagination and per-page-->

    <div class="row">
        <div class="col-md-8">
            <?=$listView->renderPager();?>
        </div>
        <div class="col-md-4 text-right">
            <?=
                \frontend\widgets\PerPageWidget::widget([
                    'dataProvider' => $topicsProvider,
                    'label' => _t('front.per-page', 'Show'),
                    'items' => [10, 50, 100]
                ])
            ?>
        </div>
    </div>


<?php else: ?>
    <div class="row">
        <div class="col-xs-12">
            <p class="m-b20"><?= _t('front.messages', 'No messages found'); ?></p>
        </div>
    </div>

<?php endif; ?>


<?php $this->endContent();?>