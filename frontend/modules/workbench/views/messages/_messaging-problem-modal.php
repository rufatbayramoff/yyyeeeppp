<script type="text/ng-template" id="/app/messages/report-message.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t('frontend.message', 'Report Problem') ?></h4>
                </div>
                <div class="modal-body">

                    <p><?= _t(
                            "frontend.message",
                            "You are acknowledging that Treatstock has your permission to read this dialogue to help resolve the issue."
                        ); ?></p>

                    <form class="form-horizontal" name="reportMessageForm">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('frontend.message', 'Reason') ?></label>
                            <div class="col-sm-8">
                                <select
                                        class="form-control"
                                        ng-options="reason.id as reason.title for reason in reportMessageReasons"
                                        ng-model="form.reasonId">
                                    <option value=""><?= _t('app', 'Select reason') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= _t('app', 'Comments'); ?></label>
                            <div class="col-sm-8">
                                <textarea rows="3"
                                        ng-model="form.reasonDescription"
                                        class="form-control" placeholder=""></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button
                            ng-click="$dismiss()"
                            type="button" class="btn btn-default"><?= _t('site.ps', 'Cancel') ?></button>
                    <button
                            loader-click="report()"
                            type="button" class="btn btn-primary"><?= _t('frontend.message', 'Submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</script>