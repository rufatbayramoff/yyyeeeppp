<?php

use common\models\MsgFolder;
use frontend\models\community\MessageFacade;

/** @var \common\models\User $user */
/** @var \common\models\MsgFolder|null $currentFolder */
/** @var string $content */

$folders = MessageFacade::getFoldersForUser($user);

$menuItems[] = [
    'label'  => _t('site.ps', 'All'),
    'url'    => ['/messages'],
    'active' => $currentFolder && !$currentFolder->id
];
$this->title = 'Messages';
// Make menu config
foreach ($folders as $folder) {
    $unreadedCount = MessageFacade::getUnreadedTopicsCount($user, $folder);
    if ($folder->alias == 'completed-orders' || $folder->alias == 'service-orders') {
        if (!\frontend\models\user\UserFacade::hasPs($user)) {
            continue;
        }
    }
    $menuItems[] = [
        'label'  => H($folder->title) . ($unreadedCount ? ' <span class="badge">' . $unreadedCount . '</span>' : ''),
        'url'    => ['/messages', 'folderAlias' => H($folder->alias)],
        'active' => isset($currentFolder) && $currentFolder->id == $folder->id,
    ];
}
?>

    <div class="over-nav-tabs-header">
        <div class="container container--wide">
            <h1><?php echo $this->title?></h1>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-sm-4 col-md-3 sidebar">
                <?php
                if (isset($currentFolder)) {
                    if ($currentFolder->id == MsgFolder::FOLDER_ID_PERSONAL) {
                        echo '<div class="chat-message__new-btn">';
                        echo \yii\helpers\Html::a(
                            '<span class="tsi tsi-message"></span> ' . _t('front.messages', 'New message'),
                            ['/workbench/messages/create-personal-topic-with-unknown-user'],
                            ['class' => 'btn btn-danger btn-block']
                        );
                        echo '</div>';
                    } elseif ($currentFolder->id == MsgFolder::FOLDER_ID_SUPPORT) {
                        echo '<div class="chat-message__new-btn">';
                        echo \yii\helpers\Html::a(
                            '<span class="tsi tsi-message"></span> ' . _t('front.messages', 'New message'),
                            ['/workbench/messages/create-support-topic'],
                            ['class' => 'btn btn-danger btn-block']
                        );
                        echo '</div>';
                    }
                }
                ?>
                <?php if(isset($this->blocks['sidebar'])):?>
                    <?php echo $this->blocks['sidebar']?>
                <?php else:?>
                    <div class="dropdown dropdown-primary side-nav-dropdown">
                        <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="js-section-title"><?= _t('site.ps', 'All') ?></span>
                            <span class="tsi tsi-down"></span>
                        </button>
                        <!-- Folders links -->
                        <?= \yii\widgets\Menu::widget(
                            [
                                'items'        => $menuItems,
                                'options'      => ['class' => 'dropdown-menu', 'id' => 'navChatRequest'],
                                'encodeLabels' => false
                            ]
                        ) ?>
                    </div>
                <?php endif;?>
            </div>
            <div class="col-sm-8 col-md-9 mainbar wide-padding wide-padding--left">
                <?= $content ?>
            </div>
        </div>

    </div>

<?php
$this->registerJs(
    '
    $(".js-section-title").html($("#navChatRequest li.active a").html());
'
);
