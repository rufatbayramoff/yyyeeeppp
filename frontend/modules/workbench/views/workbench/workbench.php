<?php
/**
 * User: nabi
 */

?>

<?=\frontend\modules\workbench\widgets\WorkbenchTabsWidget::widget(); ?>

<div class="container m-t20">
    <div class="row">
        <div class="col-lg-10">
            <h3>Become a supplier to provide products and services to a global market
            </h3>
            <a href="/mybusiness/company" class="btn  btn-primary">Create a Business</a>
        </div>
    </div>
</div>
