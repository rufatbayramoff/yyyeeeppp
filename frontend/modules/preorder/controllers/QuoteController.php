<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.07.18
 * Time: 13:38
 */

namespace frontend\modules\preorder\controllers;

use common\components\BaseController;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\OnlyPostRequestException;
use common\models\Company;
use common\models\repositories\FileRepository;
use common\models\SeoPage;
use common\modules\captcha\services\CaptchaService;
use common\modules\company\repositories\CompanyServiceRepository;
use common\modules\company\services\CompanyServicesService;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\services\ProductService;
use common\traits\TransactedControllerTrait;
use frontend\modules\preorder\components\PreorderFactory;
use frontend\modules\preorder\components\PreorderForm;
use frontend\modules\preorder\components\PreorderSerializer;
use frontend\modules\preorder\components\PreorderService;
use HttpException;
use yii\base\UserException;
use yii\web\NotFoundHttpException;

/**
 * Class QuoteController
 *
 * @package frontend\modules\preorder\controllers
 *
 * @property-read CompanyServiceRepository $companyServiceRepository
 * @property-read CompanyServicesService $companyServicesService
 */
class QuoteController extends BaseController
{
    public $layout = 'quoteLayout.php';

    use TransactedControllerTrait;

    /**
     * @var PreorderService
     */
    protected $preorderService;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var PreorderFactory
     */
    protected $preorderFactory;

    protected $companyServiceRepository;

    protected $companyServicesService;

    /** @var CaptchaService */
    protected $captchaService;

    /**
     * @var FileRepository
     */
    protected $fileRepository;

    /**
     * @param PreorderService $preorderService
     * @param ProductRepository $productRepository
     * @param ProductService $productService
     * @param PreorderFactory $preorderFactory
     * @param CompanyServiceRepository $companyServiceRepository
     * @param CompanyServicesService $companyServicesService
     */
    public function injectDependencies(
        PreorderService $preorderService,
        ProductRepository $productRepository,
        ProductService $productService,
        PreorderFactory $preorderFactory,
        CompanyServiceRepository $companyServiceRepository,
        CompanyServicesService $companyServicesService,
        CaptchaService $captchaService,
        FileRepository $fileRepository
    )
    {
        $this->preorderService          = $preorderService;
        $this->productRepository        = $productRepository;
        $this->productService           = $productService;
        $this->preorderFactory          = $preorderFactory;
        $this->companyServiceRepository = $companyServiceRepository;
        $this->companyServicesService   = $companyServicesService;
        $this->captchaService           = $captchaService;
        $this->fileRepository           = $fileRepository;
    }

    public function actionGetQuoteWidget($companyUrl)
    {
        $company = Company::find()->active()->url($companyUrl)->one();
        if (!$company) {
            return "Not found (not active) company";
        }
        $this->layout = '@frontend/views/layouts/plain';
        return $this->render('getQuoteWidget.php', ['company' => $company]);
    }


    /**
     * @param $psId
     * @param null $productUuid
     * @param null $serviceId
     *
     * @return string
     *
     * @throws \yii\base\ExitException
     * @throws \yii\web\GoneHttpException
     * @throws \yii\web\NotFoundHttpException *@throws \yii\base\ExitException
     */
    public function actionRequestQuote($psId, $productUuid = null, $serviceId = null, $uploadedFilesUuids = null)
    {
        $user    = $this->getCurrentUser();
        $company = Company::find()->active()->where(['ps.id' => $psId])->one();
        if ($company === null) {
            throw new NotFoundHttpException(_t('common.ps', 'The manufacturer is currently inactive.'));
        }
        $form       = new PreorderForm();
        $form->psId = $psId;
        $form->currency = $company->currency;
        $form->name = $user ? ($user->userProfile->full_name ? $user->userProfile->full_name : ($user->username ?? '')) : '';

        $form->loadInlinePreorder(\Yii::$app->request->get());

        if ($productUuid) {
            $product = $this->productRepository->getByUuid($productUuid);
            $this->productService->tryCanBeOrderedByCurrentUser($product);
            $form->product = $product;
        }

        if ($serviceId) {
            $service = $this->companyServiceRepository->getTryById($serviceId);
            $this->companyServicesService->tryCanBeOrdered($service);
            $form->companyService = $service;
        }

        if ($uploadedFilesUuids) {
            $uploadedFilesUuids = explode('-', $uploadedFilesUuids);
            $form->existsFiles = $this->fileRepository->findCurrentUserFiles($uploadedFilesUuids);
        }

        if (!$this->seo) {
            $this->seo                   = new SeoPage();
            $this->seo->title            = _t('site.app', 'Request a Quote - {company}', ['company' => H($company->title)]);
            $this->seo->meta_description = _t('site.app', 'Request Online Quote from {company}', ['company' => H($company->title)]);
        }
        $this->view->noindex();
        return $this->render('requestQuote.php', ['preorderForm' => $form,'company' => $company, 'currentUser' => $this->getCurrentUser()]);
    }

    /**
     * @transacted
     * @return array
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRequestQuoteCreate()
    {
        if (!\Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }

        $this->captchaService->checkGoogleRecaptcha2Response();

        $form = new PreorderForm();
        $form->load(\Yii::$app->request->post());
        AssertHelper::assertValidate($form);

        $preorder = $this->preorderFactory->create($form);

        return $this->jsonReturn([
            'success'  => true,
            'preorder' => PreorderSerializer::serialize($preorder)
        ]);
    }

    /**
     * @return array
     */
    public function actionSelectClients($term)
    {
        if (!$term || !\is_string($term) || strlen($term) < PreorderForm::MIN_CLIENT_SEARCH_NAME_LEN) {
            return $this->jsonReturn([]);
        };
        $clients = $this->preorderService->searchClientsByName($term);
        return $this->jsonReturn($clients);
    }
}