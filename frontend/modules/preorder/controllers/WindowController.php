<?php
/**
 * User: nabi
 */

namespace frontend\modules\preorder\controllers;

use common\components\BaseController;
use frontend\modules\mybusiness\modules\CsWindow\repositories\CsWindowRepository;
use frontend\modules\mybusiness\modules\CsWindow\services\CsWindowService;
use yii\base\UserException;

/**
 * Class WidgetController
 *
 * @property CsWindowService $service
 * @property CsWindowRepository $repository
 */
class WindowController extends BaseController
{
    public $service;

    public $repository;

    public $layout = '@frontend/views/layouts/plain';

    public function injectDependencies(
        CsWindowService $service,
        CsWindowRepository $repository
    ) {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function actionIndex()
    {
        $csWindow = $this->repository->getByUid(app()->request->get('windowUid'));

        $errorMessage = false;

        if (!$csWindow) {
            $errorMessage = 'Service does not exist or is not activated.';
        }

        $companyService = $csWindow->companyService ?? null;

        if ($companyService && !$companyService->isPublished()) {
            $errorMessage = 'Service does not exist or is not activated.';
        }

        return $this->render('index', [
            'companyService' => $companyService,
            'errorMessage' => $errorMessage
        ]);
    }
}