<?php


namespace frontend\modules\preorder\repositories;


use yii\db\Query;

class PreorderRepository
{
    /**
     * @param int $preorderId
     * @return bool
     */
    public function hasCncError(int $preorderId): bool
    {
        $query = new Query();
        $query->select('mdpcp.error_msg');
        $query->from('preorder_file pf');
        $query->innerJoin('file f','f.id = pf.file_id');
        $query->innerJoin('model3d_part mdp','mdp.file_id = f.id');
        $query->innerJoin('model3d_part_cnc_params mdpcp','mdpcp.id = mdp.model3d_part_cnc_params_id');
        $query->where(['pf.preorder_id' => $preorderId]);
        return $query->exists();
    }
}