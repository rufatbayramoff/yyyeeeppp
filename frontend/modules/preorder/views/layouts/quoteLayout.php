<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.07.18
 * Time: 14:18
 */

$this->beginContent('@frontend/views/layouts/main.php'); ?>  <?= $this->render('@frontend/views/common/ie-alert.php'); ?>
<?= $content; ?>
<?php $this->endContent(); ?>