<?php
/**
 * Created by PhpStorm.
 * User: d_varfolomeev
 * Date: 19.10.18
 * Time: 10:38
 *
 * @var \common\models\CompanyService $companyService
 */
?>

<?php if ($errorMessage):?>
    <div class="row">
        <div class="col-sm-10">
            <div class="input-group">
            <?php echo '<p class="help-block help-block-error">' . H($errorMessage) . '</p>'; ?>
        </div>
    </div>
<?php else: ?>
    <?php echo \frontend\modules\mybusiness\modules\CsWindow\widgets\CsWindowCalculatorWidget::widget([
        'companyService' => $companyService
    ]);?>
<?php endif; ?>
