<?php

/** @var \common\models\Company $company */

use yii\helpers\Html; ?>

<div class="create-quote-widget">
    <form action="request-quote" method="get" target='_top'>
        <h2 class="designer-card__title">
            <?= _t('site.ps', 'Request a Free Quote'); ?>
        </h2>

        <div class="form-group">
            <label class="control-label" for="preorderDescr"><?= _t('site.ps', 'Project Description'); ?><span
                        class="form-required">*</span></label>
            <textarea class="form-control" name="description" rows="2" required="required"></textarea>
        </div>

        <div class="row m-b10">
            <div class="col-md-6">
                <div class="form-group m-r0 m-l0">
                    <label class="control-label"><?= _t('site.ps', 'Budget'); ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><?=$company->currency?></span>
                        <input class="form-control" type="number" name="budget" placeholder="Price" min="0">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group m-r0 m-l0">
                    <label class="control-label"><?= _t('site.ps', 'Deadline'); ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="tsi tsi-alarm-clock"></span></span>
                        <input class="form-control" type="number" name="estimateTime"
                               placeholder="Days" min="0">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="designer-card__btn-block">
                    <input type="hidden" name="psId" value="<?=$company->id?>">
                    <button type=sumbit class="btn btn-danger btn-ghost">
                        <?= _t('site.ps', 'Get a Quote'); ?>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
