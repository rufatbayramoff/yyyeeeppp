<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.07.18
 * Time: 13:39
 */

use common\components\FileTypesHelper;
use common\components\helpers\ResponseTimeHelper;
use common\models\Product;
use common\modules\captcha\widgets\GoogleRecaptcha2;
use common\modules\payment\components\BonusCalculator;
use yii\jui\JuiAsset;

/** @var $preorderForm \frontend\modules\preorder\components\PreorderForm */
/** @var $currentUser common\models\User */
/** @var $company \common\models\Company */

$preorderFormSerialized = \frontend\modules\preorder\components\PreorderFormSerializer::serialize($preorderForm);

$product = $preorderForm->product;
$this->registerAssetBundle(\frontend\assets\DropzoneAsset::class);
$unitTypesPlural = array_combine(array_values(Product::getUnitTypesList()), array_values(Product::getUnitTypesList(true)));
Yii::$app->angular
    ->service(['notify', 'router', 'user', 'modal'])
    ->directive('dropzone-button')
    ->controller([
        'product/commonLib',
        'product/userVideo',
        'product/productModels',
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service',
        'preorder/request-quote',
    ])
    ->controllerParams([
        'unitTypes'        => $unitTypesPlural,
        'preorderForm'     => $preorderFormSerialized,
        'minClientNameLen' => \frontend\modules\preorder\components\PreorderForm::MIN_CLIENT_SEARCH_NAME_LEN
    ]);
$this->registerAssetBundle(JuiAsset::class); // Autocompleate

?>
<div class="over-nav-tabs-header">
    <div class="container"><h1><?= !empty($header) ? $header : _t('site.ps', 'Request a Quote'); ?></h1></div>
</div>
<div ng-controller="RequestQuoteController" ng-cloak>
    <form name="preorderForm" id="preorderForm" class="form-horizontal">
        <div class="container">
            <div class="form-group row">
                <label class="control-label col-sm-3 col-md-2" for="preorderProduct"><?= _t('site.preorder', 'Company') ?></label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-12 ugc-content">
                            <div class="preorder-service-ava">
                                <img src="<?php echo $company->getLogoUrl();?>" alt="<?php echo H($company->getTitleLabel());?>">
                            </div>
                            <div class="preorder-service-name">
                                <h4 class="m-t0 m-b0"><?php echo H($company->getTitleLabel());?></h4>
                            </div>
                            <?= _t('site.ps', 'Customer service'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" ng-if="preorder.companyService.getCoverUrl()">
                            <img ng-src="{{preorder.companyService.getCoverUrl()}}" class="preorder-product-image">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row" ng-if="isShowName()">
                <label class="control-label col-sm-3 col-md-2" for="preorderName"><?= _t('site.preorder', 'Your name') ?><span class="form-required">*</span></label>
                <div class="col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="preorderName" placeholder="<?= _t('site.preorder', 'Client name') ?>"
                           ng-model="preorder.name"
                           required="required">
                </div>
            </div>
            <div class="form-group row" ng-if="isShowSelectClient()">
                <label class="control-label col-sm-3 col-md-2" for="clientNameAutocomplete"><?= _t('site.preorder', 'Client') ?><span class="form-required">*</span></label>
                <div class="col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="clientNameAutocomplete" placeholder="<?= _t('site.preorder', 'Client') ?>"
                           ng-model="preorder.name"
                           required="required">
                </div>
            </div>
            <div class="form-group row" ng-if="psShowName()">
                <label class="control-label col-sm-3 col-md-2" for="yourName"><?= _t('site.preorder', 'Your name') ?><span class="form-required">*</span></label>
                <div class="col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="yourName" placeholder="<?= _t('site.preorder', 'Your name') ?>"
                           ng-model="preorder.name"
                           required="required">
                </div>
            </div>

            <div class="form-group row" ng-if="isShowProduct()">
                <label class="control-label col-sm-3 col-md-2" for="preorderProduct"><?= _t('site.preorder', 'Product') ?></label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-12 ugc-content">
                            <h4>{{preorder.product.title}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><img ng-src="{{preorder.product.coverUrl}}" class="preorder-product-image"></div>
                        <div class="col-sm-9">
                            <div class="row" ng-show="preorder.product.deliveryDetails.shipFrom">
                                <label class="control-label col-sm-4 col-md-3 p-t0">
                                    <?= _t('site.product', 'Ship from') ?>
                                </label>
                                <div class="col-sm-8 m-b10">
                                    {{preorder.product.deliveryDetails.shipFrom}}&nbsp;
                                </div>
                            </div>
                            <div class="row" ng-show="preorder.product.deliveryDetails.incoterms">
                                <label class="control-label col-sm-4 col-md-3 p-t0">
                                    <?= _t('site.product', 'Incoterms') ?>
                                </label>
                                <div class="col-sm-8 m-b10">
                                    {{preorder.product.deliveryDetails.incoterms}}&nbsp;
                                </div>
                            </div>
                            <?php if ($preorderForm->product): ?>
                                <div class="row">
                                    <label class="control-label col-sm-4 col-md-3 p-t0">
                                        <?= _t('site.product', 'Price') ?>
                                    </label>
                                    <div class="col-sm-8">
                                        <?php
                                        $moqPrices = $preorderForm->product->getMoqPricesFormatted();
                                        if (empty($moqPrices)) {
                                            echo displayAsCurrency($preorderForm->product->productCommon->single_price, $preorderForm->product->productCommon->company->currency);
                                        }
                                        foreach ($moqPrices as $price): ?>
                                            <div class="m-r10">
                                                <?php
                                                if ($price['qtyFrom'] == $price['qtyTo']):
                                                    ?>
                                                    <?= _t('site.store', "&#8805;  {qtyFrom} {unitType} at {price}/{unitCode}", [
                                                    'qty'      => $price['title'],
                                                    'qtyFrom'  => $price['qtyFrom'],
                                                    'unitType' => $price['unitType'],
                                                    'price'    => $price['priceAmount'],
                                                    'unitCode' => $price['unitCode']
                                                ]); ?>
                                                <?php else: ?>
                                                    <?= _t('site.store', "&#8805;  {qtyFrom} {unitType} at {price}/{unitCode}", [
                                                        'qty'      => $price['title'],
                                                        'qtyFrom'  => $price['qtyFrom'],
                                                        'qtyTo'    => $price['qtyTo'],
                                                        'unitType' => $price['unitType'],
                                                        'price'    => $price['priceAmount'],
                                                        'unitCode' => $price['unitCode']
                                                    ]); ?>
                                                <?php endif; ?></div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <?php if ($product->getMinOrderQty() > 1): ?>
                                    <div class="row">
                                        <label class="control-label col-sm-4 col-md-3 p-t0">
                                            <?= _t('site.order', 'Min. order') ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <?= sprintf('%s %s', $product->getMinOrderQty(), $product->getUnitTypeTitle(2)); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row" ng-if="isShowQuantity()">
                <label class="control-label col-sm-3 col-md-2" for="preorderQuantity"><?= _t('site.preorder', 'Quantity') ?><span class="form-required">*</span>
                </label>
                <div class="col-sm-5">
                    <input type=number class="form-control pull-left" id="preorderQuantity" ng-model="preorder.quantity" required="required" style="max-width: 90px"
                           min="{{preorder.getMinQty()}}" step="any">
                    <div class="form-control-static pull-left m-l10">
                        {{getUnitTypeMeasurement()}}
                    </div>
                </div>
            </div>

            <div class="form-group row" ng-if="isShowTotalPrice()">
                <label class="control-label col-sm-3 col-md-2" for="preorderTotal"><?= _t('site.preorder', 'Total price') ?>
                </label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        {{preorder.totalPriceFormatted()}}
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3 col-md-2" for="preorderCompany">
                    <?= _t('site.preorder', 'Company (optional)') ?>
                </label>
                <div class="col-sm-6 col-md-5">
                    <input class="form-control" ng-model="preorder.shipTo.company" id="preorderCompany" placeholder="<?= _t('site.preorder', 'Company') ?>">
                </div>
            </div>

            <div class="form-group row" ng-if="isShowProjectDescription()">
                <label class="control-label col-sm-3 col-md-2" for="preorderDescr"><?= _t('site.preorder', 'Project Description') ?><span
                            class="form-required">*</span></label>
                <div class="col-sm-6 col-md-5">
                                <textarea class="form-control" rows="3" id="preorderDescr"
                                          ng-model="preorder.description"
                                          required="required"></textarea>
                </div>
            </div>
            <div class="form-group row" ng-if="isShowAttachFiles()">
                <label class="control-label col-sm-3 col-md-2"><?= _t('site.preorder', 'Attach Files') ?></label>
                <div class="col-sm-9">
                    <button type="button" class="btn btn-primary"
                            dropzone-button="preorder.files"
                            accepted-files="<?= '.' . implode(', .', FileTypesHelper::getAllowExtensions()) ?>"
                            max-files="20"><i class="tsi tsi-upload-l"></i> <?= _t('site.preorder', 'Browse files') ?></button>

                    <p class="help-block"><?= _t('site.preorder', 'Maximum limit of') ?> 20 <?= _t('site.preorder', 'files') .
                        '. ' . _t('site.preorder', 'Max size: ') . app('setting')->get('upload.maxfilesize', 57) . ' Mb.' ?></p>
                    <div class="simple-file-link" ng-repeat="file in preorder.files">
                        <button ng-click="removeFile($index)" title="Delete File">×</button>
                        <span class="simple-file-link__link">{{file.name}}</span>
                    </div>
                    <div class="simple-file-link" ng-repeat="file in preorder.existsFiles">
                        <button ng-click="removeExistFile($index)" title="Delete File">×</button>
                        <span class="simple-file-link__link">{{file.name}}</span>
                    </div>
                </div>
            </div>
            <div class="form-group row" ng-if="isShowBudget()">
                <label class="control-label col-sm-3 col-md-2" for="preorderPrice"><?= _t('site.preorder', 'Budget') ?></label>
                <div class="col-sm-6 col-md-5">
                    <div class="input-group">
                        <input class="form-control" type="number" id="preorderPrice" placeholder="<?= _t('site.preorder', 'Price') ?>"
                               min="0"
                               ng-model="preorder.budget">
                        <div class="input-group-btn dropdown">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{preorder.currency}} <span class="tsi tsi-down"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#" ng-click="setCurrency('<?=$company->currency?>')"><?=$company->paymentCurrency->title_original?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3 col-md-2" for="preorderTime"><?= _t('site.preorder', 'Deadline') ?></label>
                <div class="col-sm-6 col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="tsi tsi-alarm-clock"></span></span>
                        <input class="form-control" type="number" id="preorderTime" placeholder="<?= _t('site.preorder', 'Days') ?>"
                               min="0"
                               ng-model="preorder.estimateTime">
                    </div>
                </div>
            </div>

            <div class="form-group row" ng-if="isGuest">
                <label class="control-label col-sm-3 col-md-2" for="preorderEmail"><?= _t('site.preorder', 'Email address') ?><span
                            class="form-required">*</span></label>
                <div class="col-sm-6 col-md-5">
                    <input type="email" class="form-control" id="preorderEmail" placeholder="<?= _t('site.preorder', 'Email') ?>"
                           required
                           email-validator
                           ng-model="preorder.email">
                </div>
            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3 col-md-2" for="shipTo"><?= _t('site.preorder', 'Location') ?><span
                            class="form-required">*</span></label>
                <div class="col-sm-9 p-t10">
                    <input type=hidden class="form-control" rows="3" ng-model="preorder.shipTo" id='preorderShipTo' required="required">
                    <?php
                    $location = $preorderForm->shipTo ? \common\models\factories\LocationFactory::createFromUserAddress($preorderForm->shipTo) : null;
                    echo \frontend\widgets\UserLocationWidget::widget(
                        [
                            'location'       => $location,
                            'locationFormat' => $location ? '%city%, %country%' : '%city%',
                            'jsCallback'     => 'function(btn, result, modalId) {
                             if (result) {
                                $("body").trigger("setPreorderAddress", result.location );
                                var text = result.location.city+", "+result.location.country;
                                $(".ts-user-location").html(text);
                                return true;
                             };
                    };'
                        ]
                    ); ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3 col-md-2" for="preorderShippingAddress"><?= _t('site.preorder', 'Shipping Address') ?>
                </label>
                <div class="col-sm-6 col-md-5">
                    <input class="form-control" ng-model="preorder.shipTo.address" id="preorderShippingAddress" placeholder="<?= _t('site.preorder', 'Shipping Address') ?>">
                </div>
            </div>

            <div class="form-group row" ng-if="isShowMessageToManufacturer()">
                <label class="control-label col-sm-3 col-md-2" for="preorderMsg"><?= _t('site.preorder', 'Message to manufacturer') ?></label>
                <div class="col-sm-6 col-md-5">
                    <textarea class="form-control" rows="3" id="preorderMsg" ng-model="preorder.message"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 col-md-2"></div>
                <div class="col-sm-6 col-md-5 m-b10">
                    <?=GoogleRecaptcha2::widget()?>
                </div>
            </div>

            <div class="row m-b20">
                <div ng-if="!preorder.createdByPs" class="col-sm-5 col-sm-offset-3 col-md-offset-2">
                    <?= _t('site.preorder', 'We will review your request and respond with a quote shortly. Please note that the manufacturer may be changed if the original one doesn`t accept the quote. We thank you for your patience.') ?>
                </div>
                <div ng-if="preorder.createdByPs" class="col-sm-5 col-sm-offset-3 col-md-offset-2">
                    <?= _t('site.preorder', 'The customer will review your request and respond to a quote shortly. We thank you for your patience.') ?>
                </div>
            </div>

            <div class="form-group row" ng-if="isGuest">
                <div class="col-sm-5 col-sm-offset-3 col-md-offset-2">
                    <div class="checkbox checkbox-primary">
                        <input id="preorderChkbx" type="checkbox" ng-model="form.acceptTerms">
                        <label for="preorderChkbx"><?= _t('site.preorder', 'By using this Service, you agree to our <a href="{termsLink}" target="_blank">Terms</a> and that you have read our
                                        <a href="{privacyPolicyLink}" target="_blank">Privacy Policy</a>, including our <a href="{cookieUseLink}" target="_blank">Cookie use</a>',
                                [
                                    'termsLink'         => '/site/terms',
                                    'privacyPolicyLink' => '/site/policy',
                                    'cookieUseLink'     => '/site/policy#cookie'
                                ]) ?></label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9 col-sm-offset-3 col-md-offset-2">
                    <button type="button" class="btn btn-default m-r20" onclick="window.history.back();"><?= _t('site.preorder', 'Back') ?></button>
                    <button class="btn btn-primary"
                            loader-click="submitForm()"
                    ><?= _t('site.preorder', 'Submit') ?></button>
                </div>
            </div>
        </div>
    </form>
</div>