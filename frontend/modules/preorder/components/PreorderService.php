<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components;


use backend\models\search\PreorderWorkSearch;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\models\Company;
use common\models\factories\UserFactory;
use common\models\loggers\PreorderLogger;
use common\models\orders\events\ChangeOrderStatusEvent;
use common\models\Preorder;
use common\models\repositories\UserRepository;
use common\models\StoreOrderAttemp;
use common\models\SystemReject;
use common\models\User;
use common\models\UserProfile;
use common\modules\informer\InformerModule;
use common\modules\xss\helpers\XssHelper;
use yii\base\UserException;
use yii\data\ActiveDataProvider;

class PreorderService
{
    /**
     * @var \common\models\loggers\PreorderLogger
     */
    protected $logger;

    /**
     * @var PreorderEmailer
     */
    protected $emailer;

    /** @var UserFactory */
    protected $userFactory;

    /** @var UserRepository */
    protected $userRepository;

    public const MAX_SHOW_CLIENTS_SELECT_COUNT = 20;

    /**
     * @param PreorderEmailer $emailer
     * @param UserFactory $userFactory
     */
    public function injectDependencies(PreorderEmailer $emailer, UserFactory $userFactory, UserRepository $userRepository, \common\models\loggers\PreorderLogger $logger)
    {
        $this->emailer        = $emailer;
        $this->userFactory    = $userFactory;
        $this->userRepository = $userRepository;
        $this->logger         = $logger;
    }

    /**
     * @param Preorder $preorder
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function confirm(Preorder $preorder): void
    {
        $preorder->confirm_hash = Preorder::CONFIRMED_HASH;
        $this->logger->log($preorder, 'confirm');
        $preorder->safeSave();

        InformerModule::addCompanyOrderInformer($preorder->storeOrderAttempt);
        $this->emailer->psNewPreorder($preorder);
    }

    /**
     * @param User $user
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function confirmAllNotConfirmed(User $user): void
    {
        $notConfiremdPreorders = Preorder::find()
            ->forCustomer($user)
            ->waitComfirm()
            ->all();

        foreach ($notConfiremdPreorders as $preorder) {
            $this->confirm($preorder);
        }
    }

    public function reopen(Preorder $preorder)
    {
        $preorder->changeStatus(Preorder::STATUS_NEW);
        $this->logger->log($preorder, 'reopen');
        $preorder->storeOrderAttempt->status = StoreOrderAttemp::STATUS_NEW;
        $preorder->storeOrderAttempt->safeSave();
    }

    public function searchClientsByName($name)
    {
        $nameSlashes = addslashes($name);
        $users       = User::find()->where('email like \'' . $nameSlashes . '%\' or username like\'%' . $nameSlashes . '%\'')->all();
        $profiles    = UserProfile::find()->where(['like', 'full_name', $name])->all();
        foreach ($profiles as $profile) {
            $users[] = $profile->user;
        }
        $companies = Company::find()->where(['like', 'title', $name])->all();
        foreach ($companies as $company) {
            $users[] = $company->user;
        }

        $clients = [];
        foreach ($users as $user) {
            if (!XssHelper::cleanXssProtect($user->email) || !$user->getIsActive()) {
                continue;
            }
            $label              = $user->company ? $user->company->getTitle() : $user->email;
            $clients[$user->id] = [
                'id'    => $user->id,
                'label' => $label,
                'value' => $user->email
            ];
        }
        $clients = array_slice($clients, 0, self::MAX_SHOW_CLIENTS_SELECT_COUNT);
        return $clients;
    }

    /**
     * @param Preorder $preorder
     * @throws UserException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function formPreorderClient(Preorder $preorder)
    {
        if (!$preorder->email) {
            throw new BusinessException(_t('site.preorder', 'Empty email not allowed'));
        }
        $client = User::find()->where(['email' => $preorder->email])->one();
        if ($client) {
            // Client
            $preorder->user_id = $client->id;
            $preorder->populateRelation('user', $client);
            $preorder->is_client_created = 0;
        } else {
            $client = $this->userFactory->createUserByEmail($preorder->email);
            $this->userRepository->save($client);

            $preorder->is_client_created = 1;
            // Client not exist, create client
            $preorder->user_id = $client->id;
            $preorder->populateRelation('user', $client);
        }
    }

    public static function getWorksDataProvider(Preorder $preorder): ActiveDataProvider
    {
        $preorderWorkSearch              = \Yii::createObject(PreorderWorkSearch::class);
        $preorderWorkSearch->preorder_id = $preorder->id;
        $preorderWorkDataProvider        = $preorderWorkSearch->search([]);
        return $preorderWorkDataProvider;
    }

    public function deletePreorder(Preorder $preorder)
    {
        if ($preorder->paymentInvoices) {
            $preorder->status                    = Preorder::STATUS_DELETED_BY_CLIENT;
            $preorder->storeOrderAttempt->status = StoreOrderAttemp::STATUS_CANCELED;
            $this->logger->log($preorder, 'delete_by_client');
            $preorder->save(false);
            $preorder->storeOrderAttempt->save(false);
            return;
        }
        $preorder->delete();
    }

    public function cancelOfferByClient(Preorder $preorder)
    {
        $preorder->status                    = Preorder::STATUS_CANCELED_BY_CLIENT;
        $preorder->storeOrderAttempt->status = StoreOrderAttemp::STATUS_CANCELED;
        $this->logger->log($preorder, 'cancel_by_client');
        $preorder->save(false);
        $preorder->storeOrderAttempt->save(false);
    }

    public function declineOfferByClient(Preorder $preorder, int $reasonId = 0, string $reasonDescription = '')
    {
        $preorder->changeStatus(Preorder::STATUS_REJECTED_BY_CLIENT);
        if ($reasonId) {
            $systemReject              = SystemReject::tryFindByPk($reasonId);
            $preorder->decline_reason  = $systemReject->title;
            $preorder->decline_comment = $reasonDescription;
        }
        $this->logger->log($preorder, 'decline_by_client');
        $preorder->safeSave();
        InformerModule::addCompanyOrderInformer($preorder->storeOrderAttempt);
        $this->emailer->psCustomerDeclinePreorder($preorder);
    }

    public function declineOfferByCompany(Preorder $preorder, int $reasonId = 0, string $reasonDescription = '')
    {
        if ($preorder->status === Preorder::STATUS_REJECTED_BY_CLIENT) {
            $preorder->changeStatus(Preorder::STATUS_REJECT_CONFIRM_BY_COMPANY);
        } else {
            $preorder->changeStatus(Preorder::STATUS_REJECTED_BY_COMPANY);
        }

        if ($reasonId) {
            $systemReject              = SystemReject::tryFindByPk($reasonId);
            $preorder->decline_reason = $systemReject->title;
        }
        if ($reasonDescription) {
            $preorder->decline_comment = $reasonDescription;
        }
        $preorder->decline_initiator = ChangeOrderStatusEvent::INITIATOR_PS;

        $this->logger->log($preorder, 'decline_by_company');
        $preorder->safeSave(['status', 'status_updated_at', 'decline_reason', 'decline_comment', 'decline_initiator']);

        $preorder->storeOrderAttempt->status = StoreOrderAttemp::STATUS_CANCELED;
        $preorder->storeOrderAttempt->safeSave();
    }

    public function declineOfferByModerator(Preorder $preorder)
    {
        $preorder->changeStatus(Preorder::STATUS_REJECTED_BY_COMPANY);
        $preorder->safeSave();
        $this->logger->log($preorder);
        $this->emailer->customerModeratorDeclinePreorder($preorder);
        $this->emailer->psModeratorDeclinePreorder($preorder);
    }
}