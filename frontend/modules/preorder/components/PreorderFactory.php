<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components;


use common\components\DateHelper;
use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\message\builders\TopicBuilder;
use common\models\MsgTopic;
use common\models\Preorder;
use common\models\PreorderFile;
use common\models\PreorderWork;
use common\models\Ps;
use common\models\repositories\FileRepository;
use common\models\User;
use common\modules\informer\InformerModule;
use common\modules\product\factories\ProductSnapshotFactory;
use common\modules\product\repositories\ProductSnapshotRepository;
use common\services\StoreOrderService;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\Security;

/**
 * Class PreorderFactory
 *
 * @package frontend\preorder
 */
class PreorderFactory
{
    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @var PreorderEmailer
     */
    private $preorderEmailer;

    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var FileRepository
     */
    private $fileRepository;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var \common\models\loggers\PreorderLogger
     */
    private $logger;

    /**
     * @var ProductSnapshotFactory
     */
    protected $productSnapshotFactory;

    /**
     * @var ProductSnapshotRepository
     */
    protected $productSnapshotRepository;

    /**
     * @param Emailer $emailer
     * @param FileFactory $factory
     * @param PreorderEmailer $preorderEmailer
     * @param \common\models\loggers\PreorderLogger $logger
     * @param FileRepository $fileRepository
     * @param ProductSnapshotFactory $productSnapshotFactory
     * @param ProductSnapshotRepository $productSnapshotRepository
     */
    public function injectDependencies(
        Emailer                               $emailer,
        FileFactory                           $factory,
        PreorderEmailer                       $preorderEmailer,
        \common\models\loggers\PreorderLogger $logger,
        FileRepository                        $fileRepository,
        ProductSnapshotFactory                $productSnapshotFactory,
        ProductSnapshotRepository             $productSnapshotRepository
    )
    {
        $this->emailer                   = $emailer;
        $this->fileFactory               = $factory;
        $this->preorderEmailer           = $preorderEmailer;
        $this->security                  = Yii::$app->security;
        $this->logger                    = $logger;
        $this->fileRepository            = $fileRepository;
        $this->productSnapshotFactory    = $productSnapshotFactory;
        $this->productSnapshotRepository = $productSnapshotRepository;
    }

    /**
     * @param PreorderForm $form
     * @param User $customer
     * @return Preorder
     * @throws \Exception
     * TODO: Factory should not save
     */
    public function create(PreorderForm $form, User $customer = null): Preorder
    {
        [$customer, $needConfirm] = $this->resolveCustomerUser($form);
        $needConfirm = $needConfirm && !$form->createdByPs;

        $preorder = new Preorder();

        $preorder->setAttributes($form->getAttributes(null, ['files']));
        $preorder->status              = Preorder::STATUS_NEW;
        $preorder->status_updated_at   = DateHelper::now();
        $preorder->estimate_time       = $form->estimateTime;
        $preorder->user_id             = $customer ? $customer->id : null;
        $preorder->ps_id               = $form->psId;
        $preorder->currency            = $form->currency;
        $preorder->offer_description   = $preorder->description;
        $preorder->offer_estimate_time = $preorder->estimate_time;
        $preorder->qty                 = $form->qty;
        $preorder->is_interception     = 1;
        if ($preorder->user && $preorder->user->isCustomerServiceCompany()) {
            $preorder->is_interception = 0;
        } else {
            if ($form->createdByPs) {
                $preorder->is_interception = 0;
            } else {
                $preorder->is_interception = 1;
            }
        }
        if ($form->createdByPs) {
            $preorder->created_by = Preorder::CREATED_BY_PS;
        } else {
            $preorder->created_by = Preorder::CREATED_BY_CLIENT;
        }
        if ($form->status) {
            $preorder->status = $form->status;
        }

        if ($needConfirm && !$form->createdByPs) {
            $preorder->confirm_hash = $this->security->generateRandomString();
        }

        if ($form->product) {
            // Fix product snapshot
            $productSnapshot = $this->productSnapshotFactory->createProductSnapshot($form->product);
            $this->productSnapshotRepository->save($productSnapshot);
            $preorder->product_snapshot_uuid = $productSnapshot->uuid;
        } elseif ($form->companyService) {
            $preorder->service_id = $form->companyService->id;
        }

        $preorder->safeSave();

        if ($form->product) {
            // Add preorder work
            $preorderderWork       = Yii::createObject(PreorderWork::class);
            $preorderderWork->qty  = $preorder->qty;
            $preorderderWork->cost = $form->product->getPriceMoneyByQty($preorderderWork->qty)->getAmount() * $preorderderWork->qty;
            $preorderderWork->link('preorder', $preorder);
            $preorderderWork->link('product', $form->product);
            $preorderderWork->safeSave();
        }
        if ($form->shipTo && $form->shipTo->address) {
            $form->shipTo->safeSave();
            $preorder->populateRelation('shipTo', $form->shipTo);
            $preorder->ship_to_id = $form->shipTo->id;
            $preorder->safeSave();
        }

        foreach ($form->files as $file) {
            $file = $this->fileFactory->createFileFromUploadedFile($file);
            $file->setPublicMode(false);
            $this->fileRepository->save($file);
            $this->savePreorderFile($file, $preorder);
        }

        if ($form->existsFiles && $customer) {
            foreach ($form->existsFiles as $file) {
                $this->savePreorderFile($file, $preorder);
            }
        }

        $storeOrderService = Yii::createObject(StoreOrderService::class);
        $attempt           = $storeOrderService->createAttemptForPs(company: $preorder->company, companyService: $preorder->service, preorder: $preorder);

        if ($customer && !$preorder->isSelfPreorder()) {
            $customer->status = User::STATUS_ACTIVE;
            $companyUser      = $preorder->interceptionCompany()->user;
            $this->createTopic($form, $preorder, $customer, $companyUser);
        }

        if ($needConfirm) {
            $this->preorderEmailer->customerAcceptPreorder($preorder);
        }

        $this->preorderEmailer->psNewPreorder($preorder);
        InformerModule::addCompanyOrderInformer($attempt);

        $this->logger->create($preorder);

        return $preorder;
    }

    protected function savePreorderFile(File $file, Preorder $preorder): void
    {

        $preorderFile = PreorderFile::create($file->id, $preorder->id);
        AssertHelper::assertSave($preorderFile);
    }

    /**
     * @param PreorderForm $form
     * @param Preorder $preorder
     * @param User $customer
     * @param User $psUser
     * @return MsgTopic
     */
    private function createTopic(PreorderForm $form, Preorder $preorder, User $customer, User $psUser): MsgTopic
    {
        return TopicBuilder::builder()
            ->setInitialUser($customer)
            ->to($psUser)
            ->setTitle(_t('site.cnc', "Quote #{id}", ['id' => $preorder->id]))
            ->setMessage($form->message)
            ->bindObject($preorder)
            ->dontNotify()
            ->create();
    }

    /**
     * @param PreorderForm $form
     * @return array
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    private function resolveCustomerUser(PreorderForm $form): array
    {
        if ($form->createdByPs) {
            $customer = null;
            if ($form->user) {
                $customer = User::findOne(['id' => $form->user]);
            }
            return [$customer, false];
        }
        $customer = User::findOne(['email' => $form->email]);

        if ($customer) {
            return [$customer, $customer->status != User::STATUS_ACTIVE];
        }

        $currentUser = UserFacade::getCurrentUser();

        if ($currentUser) {
            return [$currentUser, true];
        }

        // if new user

        $password = Yii::$app->security->generateRandomString(8);
        $customer = UserFacade::createUser([
            'email'    => $form->email,
            'password' => $password,
        ]);
        //   $this->emailer->sendSignupEmail($customer, $password);
        AssertHelper::assert(Yii::$app->user->login($customer));

        return [$customer, true];
    }
}