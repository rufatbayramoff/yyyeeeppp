<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components;


use common\components\serizaliators\AbstractProperties;
use common\components\serizaliators\porperties\MoneyProperties;
use common\models\File;
use common\models\Preorder;
use common\models\PreorderWork;
use common\models\ProductSnapshot;
use common\models\User;
use common\models\UserAddress;
use common\modules\product\serializers\ProductSnapshotSerializer;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;
use frontend\modules\preorder\repositories\PreorderRepository;
use lib\money\Money;

class PreorderSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {

        $preorderRepository = \Yii::createObject(PreorderRepository::class);
        return [
            Preorder::class => [
                'id',
                'status',
                'labeledStatus'            => function (Preorder $preorder) {
                    return $preorder->getStatusLabeledForPs();
                },
                'name',
                'email',
                'description',
                'budget',
                'currency',
                'qty',
                'shipTo',
                'shipAddress'              => function (Preorder $preorder) {
                    $shipTo = $preorder->shipTo;
                    if (!$shipTo) {
                        return '';
                    }
                    return UserAddress::formatAddress($shipTo);
                },
                'serviceType'              => function (Preorder $preorder) {
                    return $preorder->service ? $preorder->service->type : '';
                },
                'estimate_time',
                'user',
                'files',
                'offered',
                'created_at'               => function (Preorder $preorder) {
                    return \Yii::$app->formatter->asDatetime($preorder->created_at);
                },
                'works'                    => function (Preorder $preorder) {
                    return $preorder->preorderWorks;
                },
                'offer_description',
                'offer_estimate_time',
                'isShowDeclineReasonModal' => function (Preorder $preorder) {
                    if (in_array($preorder->status, [Preorder::STATUS_REJECT_CONFIRM_BY_COMPANY, Preorder::STATUS_REJECTED_BY_CLIENT])) {
                        return false;
                    }
                    return true;
                },
                'declineReason'            => function (Preorder $preorder) {
                    $reason = '';
                    if ($preorder->decline_reason) {
                        $reason .= $preorder->decline_reason;
                    }
                    if ($preorder->decline_comment) {
                        $reason .= ($reason ? ': ' : '') . $preorder->decline_comment;
                    }
                    return $reason;
                },
                'serviceTitle'             => function (Preorder $preorder) {
                    return $preorder?->service?->getTitleLabel();
                },
                'productSnapshot',
                'productPrice'             => function (Preorder $preorder) {
                    return $preorder->productSnapshot ? $preorder->productSnapshot->getPriceMoneyByQty($preorder->qty) : null;
                },
                'productTotalPrice'        => function (Preorder $preorder) {
                    if (!$preorder->productSnapshot) {
                        return null;
                    }
                    $price = $preorder->productSnapshot->getPriceMoneyByQty($preorder->qty);
                    if ($price === null) {
                        return null;
                    }
                    $totalPrice = Money::create($price->getAmount() * $preorder->qty, $price->getCurrency());
                    return $totalPrice;
                },
                'isForProduct'             => function (Preorder $preorder) {
                    return $preorder->isForProduct();
                },
                'hasBudgetError'           => function (Preorder $preorder) use ($preorderRepository): bool {
                    if (!$preorder->isCncType()) {
                        return false;
                    }
                    return $preorderRepository->hasCncError($preorder->id);
                }
            ],

            PreorderWork::class => [
                'id',
                'title',
                'productUuid'      => 'product_uuid',
                'companyServiceId' => 'company_service_id',
                'qty'              => function (PreorderWork $work) {
                    return (float)$work->qty;
                },
                'cost'             => function (PreorderWork $work) {
                    return (float)$work->cost;
                },
                'files',
                'type'
            ],

            User::class => [
                'id',
                'status',
                'businessName' => function (User $user) {
                    return UserFacade::getFormattedBusinessOrUserName($user);
                },
                'username'     => function (User $user) {
                    return UserFacade::getFormattedUserName($user);
                },
                'avatarUrl'    => function (User $user) {
                    return UserUtils::getCompanyAvatarUrl($user);
                },
                'profileUrl'   => function (User $user) {
                    return UserUtils::getUserPublicProfileUrl($user);
                }
            ],

            File::class => [
                'id',
                'name',
                'size' => function (File $file) {
                    return \Yii::$app->formatter->asShortSize($file->size, 2);
                }
            ],

            ProductSnapshot::class => ProductSnapshotSerializer::class,
            Money::class           => MoneyProperties::class
        ];
    }
}