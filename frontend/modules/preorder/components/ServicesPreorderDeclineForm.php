<?php
namespace frontend\modules\preorder\components;

use common\components\reject\BaseRejectForm;
use common\models\SystemReject;

/**
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */
class ServicesPreorderDeclineForm extends BaseRejectForm
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reasonId' => \_t('app', 'Decline reason'),
            'reasonDescription' => \_t('app', 'Comments')
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::SERVICES_DECLINE_PREORDER;
    }
}
