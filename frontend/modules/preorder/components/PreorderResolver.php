<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components;


use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\File;
use common\models\Preorder;
use common\models\PreorderWork;
use common\models\User;
use yii\web\ForbiddenHttpException;

/**
 * Class PreorderResolver
 * @package frontend\preorder
 */
class PreorderResolver
{
    /**
     * @var PreorderAccess
     */
    private $access;
    
    /**
     * @param PreorderAccess $access
     */
    public function injectDependencies(PreorderAccess $access)
    {
        $this->access = $access;
    }

    /**
     * @param int $preorderId
     * @param User $user
     *
     * @return Preorder
     * @throws ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function resolvePreorder(int $preorderId, User $user) : Preorder
    {
        $preorder = Preorder::tryFind($preorderId);
        $this->access->tryCanView($preorder, $user);
        return $preorder;
    }

    /**
     * @param int $preorderId
     * @param User $user
     *
     * @return Preorder
     * @throws ForbiddenHttpException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function resolvePreorderPs(int $preorderId, User $user) : Preorder
    {
        $preorder = Preorder::tryFind($preorderId);
        $this->access->tryCanView($preorder, $user);
        return $preorder;
    }
    
    /**
     *
     * @param int $fileId
     * @param Preorder $preorder
     * @return File
     * @throws ForbiddenHttpException
     */
    public function resolveFile(int $fileId, Preorder $preorder) : File
    {
        /** @var File $file */
        $file = ArrayHelper::findARById($preorder->files, $fileId);
        AssertHelper::assert($file, null, ForbiddenHttpException::class);
        return $file;
    }

    /**
     * @param int $workId
     * @param Preorder $preorder
     * @return PreorderWork
     * @throws ForbiddenHttpException
     */
    public function resolveWork(int $workId, Preorder $preorder) : PreorderWork
    {
        /** @var PreorderWork $work */
        $work = ArrayHelper::findARById($preorder->preorderWorks, $workId);
        AssertHelper::assert($work, null, ForbiddenHttpException::class);
        return $work;
    }

    /**
     * @param int $fileId
     * @param PreorderWork $work
     * @return File
     */
    public function resolveWorkFile(int $fileId, PreorderWork $work) : File
    {
        /** @var File $file */
        $file = ArrayHelper::findARById($work->files, $fileId);
        AssertHelper::assert($file, null, ForbiddenHttpException::class);
        return $file;
    }
}