<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components\orderfactory;


use common\components\exceptions\AssertHelper;
use common\components\order\builder\OrderBuilder;
use common\models\Preorder;
use common\models\StoreOrder;
use common\modules\informer\InformerModule;
use common\modules\informer\models\CompanyOrderInformer;

/**
 * Class PreorderOrderFactory
 * @package frontend\preorder
 */
class PreorderOrderFactory
{
    /**
     * @param Preorder $preorder
     *
     * @return StoreOrder
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function create(Preorder $preorder) : StoreOrder
    {
        return OrderBuilder::create()->buildByPreorder($preorder);
    }
}