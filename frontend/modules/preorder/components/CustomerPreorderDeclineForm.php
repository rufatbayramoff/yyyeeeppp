<?php
namespace frontend\modules\preorder\components;

use common\components\reject\BaseRejectForm;
use common\models\SystemReject;

class CustomerPreorderDeclineForm extends BaseRejectForm
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reasonId' => \_t('app', 'Decline reason'),
            'reasonDescription' => \_t('app', 'Comments')
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected static function getRejectGroup(): string
    {
        return SystemReject::CUSTOMER_DECLINE_PREORDER;
    }
}
