<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components;


use common\models\Company;
use common\models\File;
use common\models\Preorder;
use common\models\PreorderWork;
use common\models\Ps;
use common\models\User;
use yii\helpers\Url;

/**
 * Class PreorderUrlHelper
 * @package frontend\preorder
 */
class PreorderUrlHelper
{
    /**
     * @param Preorder $preorder
     * @return string
     */
    public static function viewPs(Preorder $preorder): string
    {
        if ($preorder->isCancelled()) {
            return Url::toRoute(['/workbench/preorder/view-ps', 'preorderId' => $preorder->id, true]);
        } else {
            return Url::toRoute(['/workbench/preorder/edit-by-ps', 'preorderId' => $preorder->id, true]);
        }
    }

    /**
     * @param Preorder $preorder
     * @return string
     */
    public static function viewCustomer(Preorder $preorder): string
    {
        return Url::toRoute(['/workbench/preorder/view-customer', 'preorderId' => $preorder->id], true);
    }

    public static function viewPsList($scheme = false): string
    {
        return ($scheme ? param('server') : '') . '/workbench/preorder/quotes';
    }

    /**
     * @param Preorder $preorder
     * @param PreorderWork $work
     * @param File $file
     * @return string
     */
    public static function downloadPreorderWorkFile(Preorder $preorder, PreorderWork $work, File $file): string
    {
        return Url::toRoute(['/workbench/preorder/download-work-file',
            'preorderId' => $preorder->id,
            'workId'     => $work->id,
            'fileId'     => $file->id
        ]);
    }

    /**
     * @param Preorder $preorder
     * @param File $file
     * @return string
     */
    public static function downloadPreorderFile(Preorder $preorder, File $file): string
    {
        return Url::toRoute(['/workbench/preorder/download-preorder-file',
            'preorderId' => $preorder->id,
            'fileId'     => $file->id
        ]);
    }

    /**
     * @param Preorder $preorder
     * @return string
     */
    public static function acceptOffer(Preorder $preorder): string
    {
        return Url::toRoute(['/workbench/preorder/accept-offer', 'preorderId' => $preorder->id]);
    }

    /**
     * @param Preorder $preorder
     * @return string
     */
    public static function decline(Preorder $preorder): string
    {
        return Url::toRoute(['/workbench/preorder/decline', 'preorderId' => $preorder->id]);
    }

    /**
     * @param Preorder $preorder
     * @return string
     */
    public static function dialog(Preorder $preorder): string
    {
        return "/messages/embed-topic?bindId={$preorder->id}&bindTo=preorder";
    }

    /**
     * @param Preorder $preorder
     * @return string
     */
    public static function confirmFromMail(Preorder $preorder): string
    {
        return Url::toRoute(['/workbench/preorder/email-confirm', 'preorderId' => $preorder->id, 'hash' => $preorder->confirm_hash], true);
    }

    /**
     * @param Preorder $preorder
     * @return string
     */
    public static function declineFromMail(Preorder $preorder): string
    {
        return Url::toRoute(['/workbench/preorder/email-decline', 'preorderId' => $preorder->id, 'hash' => $preorder->confirm_hash], true);
    }


    public static function getCreateOfferUrl(User $customerUser)
    {
        return '/workbench/preorder/create-by-ps?customerUsername=' . $customerUser->username;
    }

    public static function getCreateOfferForPs(Ps $ps)
    {
        return '/preorder/quote/request-quote?psId=' . $ps->id;
    }
}