<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components;


use common\components\FileTypesHelper;
use common\models\Company;
use common\models\CompanyService;
use common\models\factories\UserAddressFactory;
use common\models\File;
use common\models\populators\UserAddressPopulator;
use common\models\Preorder;
use common\models\Product;
use common\models\Ps;
use common\models\repositories\FileRepository;
use common\models\User;
use common\models\UserAddress;
use common\models\UserLocation;
use common\models\UserSession;
use common\modules\company\repositories\CompanyServiceRepository;
use common\modules\company\services\CompanyServicesService;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\services\ProductService;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserFacade;
use lib\geo\models\Location;
use lib\money\Currency;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PreorderForm
 *
 * @package frontend\preorder
 */
class PreorderForm extends Model
{
    public const MIN_CLIENT_SEARCH_NAME_LEN = 5;

    /**
     * @var int
     */
    public $psId;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $name;

    /** @var Product */
    public $product;

    /** @var CompanyService */
    public $companyService;

    /**
     * @var string
     */
    public $description;

    /**
     * @var integer
     */
    public $qty;

    /**
     * Files from form
     *
     * @var UploadedFile[]
     */
    public $files = [];

    /**
     * This files already saved in database
     *
     * @var File[]
     */
    public $existsFiles;

    /**
     * @var string
     */
    public $message;

    /**
     * Budget in USD
     *
     * @var int
     */
    public $budget;


    /**
     * @var string
     */
    public $currency = Currency::USD;

    /**
     * Estimate produce time in days
     *
     * @var int
     */
    public $estimateTime;

    /**
     * @var string
     */
    public $email;

    /**
     * @var UserAddress
     */
    public $shipTo;

    public $user;

    /** @var FileRepository */
    protected $fileRepository;

    /**
     * @var bool
     */
    public $createdByPs = false;

    /**
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function init(): void
    {
        parent::init();
        $this->user = UserFacade::getCurrentUser();
        $this->fileRepository = Yii::createObject(FileRepository::class);
        $location = UserSessionFacade::getLocation();
        $userAddressFactory = \Yii::createObject(UserAddressFactory::class);
        $userAddress = $userAddressFactory->createByLocation($location);
        $this->shipTo = $userAddress;
    }

    public function initForPs(Ps $ps)
    {
        $this->init();
        $this->psId = $ps->id;
        $this->status = Preorder::STATUS_DRAFT;
        $this->user = null;
        $this->createdByPs = true;
        $this->currency = $ps->currency;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'psId'], 'required'],
            [['description', 'message'], 'string', 'max' => 2000],
            ['name', 'string', 'max' => 255],
            [['budget', 'estimateTime', 'psId', 'createdByPs'], 'integer'],
            [['currency'], 'string'],
            ['email', 'email'],
            ['qty', 'qtyValidator'],
            //  ['email', 'required', 'when' => function(){return UserFacade::getCurrentUser() === null;}],
            ['files', 'file', 'maxFiles' => 20, 'extensions' => FileTypesHelper::getAllowExtensions(), 'checkExtensionByMimeType' => false],
        ];
    }
    
    public function qtyValidator()
    {
        if ($this->product && $this->product->getMinOrderQty() > $this->qty) {
            $this->addError('qty', _t('site.preorder', 'Quantity should be more than or equal to {min}', ['min' => $this->product->getMinOrderQty()]));
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => _t('site.preorder', 'Project Description'),
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function load($data, $formName = '')
    {
        $isLoaded = parent::load($data, $formName);
        if (array_key_exists('quantity', $data)) {
            $this->qty = (float)$data['quantity'];
        }
        if ($this->createdByPs) {
            $currentUser = UserFacade::getCurrentUser();
            $this->psId = $currentUser->company->id;
        }
        if (array_key_exists('product', $data) && $data['product']) {
            $productRepository = \Yii::createObject(ProductRepository::class);
            $productService = \Yii::createObject(ProductService::class);
            /** @var Product $product */
            $product = $productRepository->getByUuid($data['product']['uuid']);
            $productService->tryCanBeOrderedByCurrentUser($product);
            $this->product = $product;
            $this->psId = $product->company->id;
        } elseif (array_key_exists('companyService', $data) && $data['companyService']) {
            /** @var CompanyServiceRepository $companyServiceRepository */
            $companyServiceRepository = \Yii::createObject(CompanyServiceRepository::class);

            /** @var CompanyServicesService $companyServicesService */
            $companyServicesService = \Yii::createObject(CompanyServicesService::class);

            $service = $companyServiceRepository->getTryById($data['companyService']['id']);
            $companyServicesService->tryCanBeOrdered($service);

            $this->companyService = $service;
            $this->psId = $service->company->id;
        }

        if ($data['shipTo']) {
            $userAddressPopulator = Yii::createObject(UserAddressPopulator::class);
            if (!$this->shipTo) {
                $this->shipTo = new UserAddress();
            }
            $userAddressPopulator->populate($this->shipTo, $data['shipTo']);
            if (empty($this->shipTo->address)) {
                $this->shipTo->address = '-';
            }
            if ($this->user) {
                $this->shipTo->user_id = $this->user->id;
            } else {
                $this->shipTo->user_id = User::USER_ANONIM;
            }
        }
        if (empty($this->name)) {
            $this->name = $this->email;
        }
        if (empty($this->email)) {
            if ($this->createdByPs) {
                $this->email = $this->name;
            } else {
                $this->email = $this->user->email;
            }
        }

        if ($isLoaded) {
            $this->files = UploadedFile::getInstancesByName('files');
        }

        if (array_key_exists('existsFiles', $data) && is_array($data['existsFiles'])) {
            $existsFiles = [];
            foreach ($data['existsFiles'] as $existsFile) {
                if (is_string($existsFile)) {
                    $existsFiles[] = $existsFile;
                }
                if (is_array($existsFile) && array_key_exists('uuid', $existsFile)) {
                    $existsFiles[] = (string) $existsFile['uuid'];
                }
            }
            $this->existsFiles = $this->fileRepository->findCurrentUserFiles($data['existsFiles']);
        }
        return $isLoaded;
    }

    public function loadInlinePreorder($data): void
    {
        if (array_key_exists('budget', $data)) {
            $this->budget = (float)$data['budget'];
        }

        if (array_key_exists('currency', $data)) {
            $this->currency = (string)$data['currency'];
        }

        if (array_key_exists('estimateTime', $data)) {
            $this->estimateTime = (float)$data['estimateTime'];
        }

        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
    }

    /**
     * @return bool|void
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        $validate = parent::validate();
        if (!$validate) {
            return $validate;
        }
        return true;
    }

    /**
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function save()
    {
        $preorderFactory = \Yii::createObject(PreorderFactory::class);
        $preorder = $preorderFactory->create($this);
        $preorder->validate();
        $preorder->save();
    }
}