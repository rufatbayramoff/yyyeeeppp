<?php

namespace frontend\modules\preorder\components;

use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\interfaces\Model3dBasePartInterface;
use common\models\CompanyService;
use common\models\factories\Model3dReplicaFactory;
use common\models\loggers\PreorderLogger;
use common\models\message\builders\TopicBuilder;
use common\models\Model3d;
use common\models\Model3dPart;
use common\models\Model3dReplica;
use common\models\Model3dReplicaPart;
use common\models\MsgTopic;
use common\models\Preorder;
use common\models\PreorderFile;
use common\models\PreorderWork;
use common\models\PreorderWorkCnc;
use common\models\repositories\Model3dReplicaRepository;
use common\models\repositories\Model3dRepository;
use common\models\User;
use common\modules\cnc\api\Api;
use common\modules\cnc\api\Preset;
use common\modules\cnc\api\serializers\CostingSerializer;
use common\modules\cnc\models\preorder\PreorderForm;
use common\modules\informer\InformerModule;
use common\services\StoreOrderService;
use frontend\modules\workbench\services\CncBudgetService;
use Yii;

/**
 * Class PreorderFactoryForCnc
 * @package frontend\modules\preorder\components
 *
 * @property Api $api
 * @property PreorderEmailer $preorderEmailer
 */
class PreorderFactoryForCnc
{
    protected $api;

    protected $emailer;

    /** @var Model3dRepository */
    protected $model3dRepository;

    /** @var CncBudgetService     */
    protected $cncBudgetService;

    /** @var PreorderLogger     */
    protected $logger;

    /**
     * @param Api $api
     * @param PreorderEmailer $preorderEmailer
     * @param Model3dRepository $model3dRepository
     * @param CncBudgetService $cncBudgetService
     */
    public function injectDependencies(
        Api $api,
        PreorderEmailer $preorderEmailer,
        Model3dRepository $model3dRepository,
        CncBudgetService $cncBudgetService,
        PreorderLogger $logger,
    )
    {
        $this->api = $api;
        $this->preorderEmailer = $preorderEmailer;
        $this->model3dRepository = $model3dRepository;
        $this->cncBudgetService = $cncBudgetService;
        $this->logger = $logger;
    }

    /**
     * @param User $customer
     * @param PreorderForm $form
     *
     * @return Preorder
     * @throws \ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function createPreorder(User $customer, PreorderForm $form) : Preorder
    {
        /** @var Model3d $model */
        $model = $this->model3dRepository->getByUid($form->modelId);
        /** @var CompanyService $machine */
        $machine = CompanyService::tryFind($form->machineId);

        if ($model instanceof Model3d) {
            $replica = $this->createReplica($model, $form);
        } else {
            $replica = $model;
        }
        $preorder = $this->createPreorderInternal($customer, $form, $replica, $machine);

        if(!$preorder->isSelfPreorder()) {
            $customer->status = User::STATUS_ACTIVE;
            $this->createTopic($preorder, $customer, $preorder->interceptionCompany()->user);
        }

        /** @var Model3dReplicaPart $part */
        foreach ($model->getActiveModel3dParts() as $part) {
            $preset = $form->presets[$part->id];
            $this->createWorks($preorder, $part, $preset, $machine);

            if ($part->file_id) {
                $preorderFile = new PreorderFile();
                $preorderFile->preorder_id = $preorder->id;
                $preorderFile->file_id = $part->file_id;
                AssertHelper::assertSave($preorderFile);
            }
        }

        $preorder->refresh();

        $storeOrderService =Yii::createObject(StoreOrderService::class);
        $attempt = $storeOrderService->createAttemptForPs(company: $preorder->company, companyService: $preorder->service, preorder: $preorder);
        $this->preorderEmailer->psNewPreorder($preorder);
        InformerModule::addCompanyOrderInformer($attempt);

        return $preorder;
    }

    /**
     * @param Model3d $model
     * @param PreorderForm $form
     *
     * @return Model3dReplica
     * @throws \ErrorException
     * @throws \yii\base\Exception
     */
    private function createReplica(Model3d $model, PreorderForm $form) : Model3dReplica
    {
        $qty = [];

        foreach ($form->presets as $partId => $preset) {
            /** @var Model3dPart $part */
            $part = ArrayHelper::findARById($model->model3dParts, $partId);
            $qty[$part->file->id] = $preset->count;
        }

        $replica = Model3dReplicaFactory::createModel3dReplica($model, $qty);
        $replica->setStoreUnit($model->storeUnit);
        Model3dReplicaRepository::save($replica);

        return $replica;
    }

    /**
     * @param User $customer
     * @param PreorderForm $form
     * @param Model3dReplica $replica
     * @param CompanyService $machine
     *
     * @return Preorder
     * @throws \common\components\exceptions\InvalidModelException
     */
    private function createPreorderInternal(User $customer, PreorderForm $form, Model3dReplica $replica, CompanyService $machine) : Preorder
    {
        $preorder = new Preorder();

        $preorder->status = Preorder::STATUS_NEW;
        $preorder->user_id = $customer->id;
        $preorder->ps_id = $machine->ps_id;
        if ($customer?->isCustomerServiceCompany()) {
            $preorder->is_interception = 0;
        } else {
            $preorder->is_interception = 1;
        }

        $preorder->budget = $form->preorder->budget;
        $preorder->currency = $machine->company->currency;
        $preorder->description = $form->preorder->description;
        $preorder->estimate_time = $form->preorder->estimateTime;
        $preorder->offer_description = $preorder->description;
        $preorder->offer_estimate_time = $preorder->estimate_time;
        $preorder->name = $form->preorder->name;
        $preorder->email = $form->preorder->email;
        $preorder->service_id  = $machine->id;
        $preorder->safeSave();

        return $preorder;
    }

    /**
     * @param Preorder $preorder
     * @param User $customer
     * @param User $machineOwner
     *
     * @return MsgTopic
     * @throws \yii\base\Exception
     */
    private function createTopic(Preorder $preorder, User $customer, User $machineOwner) : MsgTopic
    {
        return TopicBuilder::builder()
            ->setInitialUser($customer)
            ->to($machineOwner)
            ->setTitle(_t('site.cnc', 'Quote'))
            ->setMessage(_t('site.cnc', 'You have new Quote'))
            ->bindObject($preorder)
            ->create();
    }

    /**
     * @param Preorder $preorder
     * @param Model3dBasePartInterface $part
     * @param Preset $preset
     * @param CompanyService $machine
     *
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    private function createWorks(Preorder $preorder, Model3dBasePartInterface $part, Preset $preset, CompanyService $machine)
    {
        $costing = $this->cncBudgetService->budget(
            $part->getOriginalModel3dPartObj()->getUid(),
            $machine->psCncMachine,
            $preset
        );

        foreach ($costing->getPriceElements() as $element) {
            $work = new PreorderWork();
            $work->preorder_id = $preorder->id;
            $work->title = $element->getTitle().' ('.$part->name.')';
            $work->qty = $element->getIsPerPart() ? $costing->getCount() : 1;
            $work->cost = $element->getPrice() *  $work->qty;
            AssertHelper::assertSave($work);
            $this->logger->createWork($work);

            $cncWork = \Yii::createObject(PreorderWorkCnc::class);
            $cncWork->preorder_id = $preorder->id;
            $cncWork->preorder_work_id = $work->id;
            $cncWork->title = $work->title;
            $cncWork->qty    = $work->qty;
            $cncWork->price = $work->cost;
            $cncWork->instrument = CostingSerializer::serialize($costing->getInstrument());
            $cncWork->safeSave();
        }
    }
}