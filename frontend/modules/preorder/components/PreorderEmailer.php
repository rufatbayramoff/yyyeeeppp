<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components;


use common\components\Emailer;
use common\components\reject\RejectInterface;
use common\models\Preorder;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\modules\company\services\CompanyVerifyService;
use common\modules\quote\services\QuoteNotifyService;
use common\services\UserEmailLoginService;
use lib\message\Constants;
use lib\message\MessageServiceInterface;
use Yii;
use yii\helpers\Url;

/**
 * Class PreorderEmailer
 * @package frontend\preorder
 */
class PreorderEmailer
{
    /**
     * @var MessageServiceInterface
     */
    private $sender;

    /**
     * @var UserEmailLoginService
     */
    private $emailLoginService;

    /**
     * @param Emailer $emailer
     */
    public function injectDependencies(Emailer $emailer, UserEmailLoginService $emailLoginService)
    {
        $this->sender = \Yii::$app->message;
        $this->emailLoginService = $emailLoginService;
    }

    /**
     * @param Preorder $preorder
     */
    public function psNewPreorder(Preorder $preorder): void
    {
        if($preorder->interceptionCompany()->needClaim()) {
            $verifyService = \Yii::createObject(CompanyVerifyService::class);
            $companyVerify = $verifyService->createCompanyVerify($preorder->interceptionCompany());
            $params = [
                'psName'  => $preorder->company->title,
                'quoteId' => $preorder->id,
                'link'    => $verifyService->getVerifyAndAcceptLink($companyVerify),
            ];
            $this->sender->sendForce($preorder->interceptionCompany()->user, 'ps.preorder.new.needClaim', $params, 'email');
            return;
        }
        if($preorder->created_by === Preorder::CREATED_BY_CLIENT) {
            $link = PreorderUrlHelper::viewPsList(true);
            $params = [
                'psName'  => $preorder->company->title,
                'quoteId' => $preorder->id,
            ];
            if($preorder->interceptionCompany()->user->isUnconfirmed()){
                $params['link'] = $this->emailLoginService->generateUserEmailLogin($preorder->interceptionCompany()->user, $preorder->interceptionCompany()->user->email, $link)->getLoginLinkByHash(true);
            } else {
                $params['link'] = $link;
            }
            $this->sender->send($preorder->interceptionCompany()->user, 'ps.preorder.new', $params);
        }
    }

    public function customerPsSendNewClientOffer($preorder)
    {
        $quoteNotifyService = Yii::createObject(QuoteNotifyService::class);
        $quoteNotifyService->notifyAccountCreated($preorder->user, $preorder);
    }

    public function customerPsSendClientOffer($preorder)
    {
        $params = [
            'psName'   => $preorder->ps->title,
            'userName' => $preorder->user->username,
            'quoteId'  => $preorder->id,
            'link'     => param('server') . '/workbench/orders',
        ];
        $this->sender->send($preorder->user, 'customer.preorder.psSendOffer', $params);
    }

    /**
     * @param Preorder $preorder
     */
    public function customerPsSendOffer(Preorder $preorder): void
    {
        if ($preorder->is_client_created) {
            $this->customerPsSendNewClientOffer($preorder);
        } else {
            $this->customerPsSendClientOffer($preorder);
        }

    }

    /**
     * @param Preorder $preorder
     */
    public function psCustomerDeclinePreorder(Preorder $preorder): void
    {
        $params = [
            'psName'         => $preorder->ps->title,
            'quoteId'        => $preorder->id,
            'quoteViewPsUrl' => Url::toRoute(['/workbench/preorder/view-ps', 'preorderId' => $preorder->id], true),
        ];
        if ($preorder->ps->user->status == User::STATUS_DRAFT_COMPANY) {
        } else {
            $this->sender->send($preorder->ps->user, 'ps.preorder.customerDeclinePreorder', $params);
        }
    }

    /**
     * @param Preorder $preorder
     * @param RejectInterface $reject
     */
    public function customerPsDeclinePreorder(Preorder $preorder, RejectInterface $reject): void
    {
        $params = [
            'psName'               => $preorder->ps->title,
            'userName'             => $preorder->user->username,
            'quoteId'              => $preorder->id,
            'quoteViewCustomerUrl' => Url::toRoute(['/workbench/preorder/view-customer', 'preorderId' => $preorder->id], true),
            'reason'               => $reject->getReason(),
            'comment'              => $reject->getComment(),
        ];
        $this->sender->send($preorder->user, 'customer.preorder.psDeclinePreorder', $params);
    }

    /**
     * @param Preorder $preorder
     */
    public function customerModeratorDeclinePreorder(Preorder $preorder): void
    {
        $params = [
            'psName'   => $preorder->ps->title,
            'userName' => $preorder->user->username,
            'quoteId'  => $preorder->id,
        ];
        $this->sender->send($preorder->user, 'customer.preorder.moderatorDeclinePreorder', $params);
    }

    /**
     * @param Preorder $preorder
     */
    public function psModeratorDeclinePreorder(Preorder $preorder): void
    {
        $params = [
            'quoteId' => $preorder->id,
            'psName'  => $preorder->ps->title,
        ];
        $this->sender->send($preorder->ps->user, 'ps.preorder.moderatorDeclinePreorder', $params);
    }

    /**
     * @param Preorder $preorder
     */
    public function psCustomerAcceptOffer(Preorder $preorder): void
    {
        $params = [
            'psName'  => $preorder->ps->title,
            'quoteId' => $preorder->id,
            'link'    => Url::toRoute(['/workbench/service-orders', 'statusGroup' => StoreOrderAttemp::STATUS_ACCEPTED], true),
        ];
        $this->sender->sendForceEmail($preorder->ps->user->email, 'ps.preorder.customerAcceptOffer', $params, "en-US");
    }

    /**
     * @param Preorder $preorder
     */
    public function customerAcceptPreorder(Preorder $preorder): void
    {
        $params = [
            'psName'      => $preorder->ps->title,
            'quoteId'     => $preorder->id,
            'userName'    => $preorder->user->username,
            'confirmLink' => PreorderUrlHelper::confirmFromMail($preorder),
            'declineLink' => PreorderUrlHelper::declineFromMail($preorder)
        ];
        $this->sender->sendForceEmail($preorder->user->email, 'customer.preorder.confirmPreorder', $params, "en-US");
    }

    public function sendCreatedNewClientForQuote(User $user, Preorder $quote): void
    {
        $link = PreorderUrlHelper::viewCustomer($quote);

        $preorderLink = $this->emailLoginService->generateUserEmailLogin($user, $user->email, $link)->getLoginLinkByHash();
        $params = [
            'quoteId'          => $quote->id,
            'companyTitle'     => H($quote->company->title),
            'quoteDescription' => H($quote->description),
            'link'             => $preorderLink

        ];
        $this->sender->sendForceEmail($user->email, 'createdClientFroQuote', $params, $user->userProfile->current_lang);
    }
}