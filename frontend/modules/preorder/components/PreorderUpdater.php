<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components;


use common\components\exceptions\AssertHelper;
use common\models\base\Preorder;
use common\models\factories\FileFactory;
use common\models\loggers\PreorderLogger;
use common\models\PreorderWork;
use common\models\PreorderWorkFile;
use common\models\repositories\FileRepository;
use yii\web\Request;
use yii\web\UploadedFile;

/**
 * Class PreorderUpdater
 * @package frontend\preorder
 */
class PreorderUpdater
{
    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var \common\models\loggers\PreorderLogger
     */
    protected $logger;

    /**
     * @var FileRepository
     */
    protected $fileRepository;

    /**
     * @param FileFactory $factory
     * @param FileRepository $fileRepository
     */
    public function injectDependencies(FileFactory $factory, FileRepository $fileRepository, PreorderLogger $logger)
    {
        $this->fileFactory    = $factory;
        $this->fileRepository = $fileRepository;
        $this->logger         = $logger;
    }

    /**
     * @param Preorder $preorder
     * @param Request $request
     * @return PreorderWork
     */
    public function addWork(Preorder $preorder, Request $request): PreorderWork
    {
        $work = new PreorderWork();
        $this->updateWorkInternal($preorder, $work, $request);
        $preorder->refresh();
        return $work;
    }

    /**
     * @param Preorder $preorder
     * @param PreorderWork $work
     * @param Request $request
     * @return PreorderWork
     */
    public function updateWork(Preorder $preorder, PreorderWork $work, Request $request): PreorderWork
    {
        $this->updateWorkInternal($preorder, $work, $request);
        return $work;
    }

    /**
     * @param Preorder $preorder
     * @param PreorderWork $work
     */
    public function deleteWork(Preorder $preorder, PreorderWork $work)
    {
        foreach ($work->preorderWorkFiles as $fileLink) {
            AssertHelper::assert($fileLink->delete());
        }
        AssertHelper::assert($work->delete());
        $preorder->refresh();
    }

    /**
     * @param Preorder $preorder
     * @param PreorderWork $work
     * @param Request $request
     */
    private function updateWorkInternal(Preorder $preorder, PreorderWork $work, Request $request): void
    {
        $work->load($request->getBodyParams(), '');
        $work->preorder_id = $preorder->id;
        $this->logger->logWork($work);
        AssertHelper::assertSave($work);

        $files = UploadedFile::getInstancesByName('files');

        foreach ($files as $file) {
            $file = $this->fileFactory->createFileFromUploadedFile($file);
            $file->setPublicMode(false);
            $this->fileRepository->save($file);

            $preorderWorkFile          = new PreorderWorkFile();
            $preorderWorkFile->file_id = $file->id;
            $preorderWorkFile->work_id = $work->id;
            AssertHelper::assertSave($preorderWorkFile);
        }
        $work->refresh();
    }
}