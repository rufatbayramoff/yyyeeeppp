<?php
/**
 * Created by mitaichik
 */

namespace frontend\modules\preorder\components;


use backend\components\NoAccessException;
use common\components\exceptions\BusinessException;
use common\models\Preorder;
use common\models\User;
use yii\base\UserException;
use yii\web\ForbiddenHttpException;

class PreorderAccess
{
    /**
     * @param Preorder $preorder
     * @param User $user
     * @return bool
     */
    public function isCustomer(Preorder $preorder, User $user): bool
    {
        return $preorder->user && $preorder->user->equals($user);
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     * @return bool
     */
    public function isPs(Preorder $preorder, User $user): bool
    {
        if ($preorder->is_interception && $user->isCustomerServiceCompany()) {
            return true;
        }
        if ($preorder->ps->user->equals($user)) {
            return true;
        }
        return false;
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     * @return $this
     * @throws ForbiddenHttpException
     */
    public function tryIsCustomer(Preorder $preorder, User $user): PreorderAccess
    {
        if (!$this->isCustomer($preorder, $user)) {
            $this->throwForrobin();
        }
        return $this;
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     * @return $this
     * @throws ForbiddenHttpException
     */
    public function tryIsPs(Preorder $preorder, User $user): PreorderAccess
    {
        if ($this->isPs($preorder, $user)) {
            return $this;
        }
        return $this->throwForrobin();
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     *
     * @return PreorderAccess
     * @throws UserException
     */
    public function tryCanView(Preorder $preorder, User $user): PreorderAccess
    {
        if ($preorder->is_interception && $user->isCustomerServiceCompany()) {
            return $this;
        }
        if ($this->isCustomer($preorder, $user)) {
            return $this;
        }
        if ($this->isPs($preorder, $user)) {
            return $this;
        }
        return $this->throwForrobin();
    }

    /**
     * @param Preorder $preorder
     * @param User $user
     *
     * @return PreorderAccess
     * @throws ForbiddenHttpException
     * @throws UserException
     */
    public function tryCanEdit(Preorder $preorder, User $user): PreorderAccess
    {
        $this->tryIsPs($preorder, $user);

        if ($preorder->isDeletedByClient()) {
            throw new BusinessException(_t('site.preorder', 'Customer deleted the quote.'));
        }

        $invoice = $preorder->primaryPaymentInvoice;

        if ($invoice && $invoice->storeOrder && $invoice->storeOrder->currentAttemp) {
            throw new BusinessException(_t('site.preorder', 'Customer accepted the quote.'));
        }

        return $this;
    }

    public function tryIsDraft(Preorder $preorder)
    {
        if (!in_array($preorder->status, [Preorder::STATUS_NEW, Preorder::STATUS_DRAFT])) {
            throw new BusinessException(_t('site.preorder', 'Preorder is not draft.'));
        }
        return $this;
    }


    /**
     * @param Preorder $preorder
     *
     * @return PreorderAccess
     * @throws UserException
     */
    public function tryIsRejected(Preorder $preorder): PreorderAccess
    {
        if ($preorder->status === Preorder::STATUS_REJECTED_BY_CLIENT) {
            throw new BusinessException(_t('site.preorder', 'Quote was canceled.'));
        }

        return $this;
    }

    /**
     * @param Preorder $preorder
     * @return PreorderAccess
     * @throws UserException
     */
    public function tryIsNotAccepted(Preorder $preorder): PreorderAccess
    {
        if (!in_array($preorder->status, [Preorder::STATUS_WAIT_CONFIRM])) {
            throw new BusinessException(_t('site.preorder', 'Quote is not wait confirm.'));
        }

        return $this;
    }

    /**
     * @throws UserException
     */
    public function throwForrobin()
    {
        throw new ForbiddenHttpException();
    }
}