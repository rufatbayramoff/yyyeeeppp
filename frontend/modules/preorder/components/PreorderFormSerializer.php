<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.07.18
 * Time: 16:01
 */

namespace frontend\modules\preorder\components;


use common\components\serizaliators\AbstractProperties;
use common\components\serizaliators\porperties\UserAddressSerializer;
use common\models\CompanyService;
use common\models\File;
use common\models\Preorder;
use common\models\PreorderWork;
use common\models\Product;
use common\models\serializers\FileSerializer;
use common\models\User;
use common\models\UserAddress;
use common\models\UserLocation;
use common\modules\product\serializers\ProductSerializer;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;
use frontend\modules\mybusiness\serializers\CompanyServiceSerializer;

class PreorderFormSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            PreorderForm::class => [
                'psId',
                'name',
                'product',
                'description',
                'message',
                'budget',
                'currency',
                'estimateTime',
                'email',
                'shipTo',
                'files',
                'existsFiles',
                'createdByPs'    => function (PreorderForm $preorderForm) {
                    return (int)$preorderForm->createdByPs;
                },
                'companyService' => function (PreorderForm $preorderForm) {
                    return $preorderForm->companyService ?
                        CompanyServiceSerializer::serialize($preorderForm->companyService)
                        : null;
                }
            ],
            UserAddress ::class => UserAddressSerializer::class,
            Product::class      => ProductSerializer::class,
        ];
    }
}