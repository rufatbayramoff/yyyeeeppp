<?php
/**
 * User: nabi
 */

namespace frontend\modules\profile;

use yii\base\Module;

class ProfileModule extends Module
{

    /**
     * @var string
     */
    public $controllerNamespace = 'frontend\modules\profile\controllers';

    /**
     * init
     *
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        parent::init();

        app()->view->headerSearchFormDisabled();
    }
}
