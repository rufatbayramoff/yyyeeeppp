<?php
/**
 * User: nabi
 */

namespace frontend\modules\profile\controllers;

use common\components\exceptions\BusinessException;
use frontend\components\AuthedController;
use frontend\models\user\SettingsForm;
use frontend\models\user\UserFacade;
use Yii;
use yii\helpers\Html;

class SettingsController extends AuthedController
{

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionIndex()
    {
        $params = [
            'user' => $this->getCurrentUser(true)
        ];
        /** @var SettingsForm $model */
        $model = SettingsForm::tryFind(
            [
                'user_id' => $params['user']->id
            ]
        );
        if (empty($model->current_lang)) {
            $model->current_lang = 'en-US';
        }
        if (app('request')->isPost) {
            $post = app('request')->post();
            if ($model->load($post) && $model->validate()) {
                $model->user_id = $params['user']->id;
                $isLangChanged = $model->checkLanguageChange();
                $model->safeSave();
                $this->setFlashMsg(true, _t('front.site', 'Updated'));
                $redirectUrl = '/profile/settings';
                if ($isLangChanged) {
                    $redirectUrl = Yii::$app->getModule('intlDomains')->domainManager->getUrlWithLangDomain($redirectUrl, $model->current_lang);
                }
                return $this->redirect($redirectUrl);
            }
            if ($model->hasErrors()) {
                $this->setFlashMsg(false, Html::errorSummary($model));
            }
        }
        return $this->render(
            'settings',
            [
                'params'      => $params,
                'model'       => $model,
                'systemLangs' => $model->getLangs(),
                'timezones'   => $model->getTimezones(),
                'currency'    => $model->getCurrency()
            ]
        );
    }

    /**
     *
     * @return string
     */
    public function actionDelete()
    {
        try {
            $user = UserFacade::getCurrentUser();
            UserFacade::requestToDeleteUser($user);
            $this->setFlashMsg(true, _t("site.user", "Request to deactivate your account was submitted."));
            // app('user')->logout(false);
            return $this->redirect([
                '/profile/settings'
            ]);
        } catch (BusinessException $e) {
            $this->setFlashMsg(false, $e->getMessage());
        } catch (\Exception $e) {
            $this->setFlashMsg(false, "Server error. Please try again.");
        }
        return $this->redirect([
            '/profile'
        ]);
    }

    /**
     *
     * @return string
     */
    public function actionCancelDelete()
    {
        try {
            $user = UserFacade::getCurrentUser();
            UserFacade::cancelRequestToDeleteUser($user);
            $this->setFlashMsg(true, _t("site.user", "Request to deactivate your account was cancelled."));
            return $this->redirect([
                '/profile/settings'
            ]);
        } catch (BusinessException $e) {
            $this->setFlashMsg(false, $e->getMessage());
        } catch (\Exception $e) {
            $this->setFlashMsg(false, "Server error. Please try again.");
        }
        return $this->redirect([
            '/profile/settings'
        ]);
    }
}