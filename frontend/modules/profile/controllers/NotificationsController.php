<?php
/**
 * User: nabi
 */

namespace frontend\modules\profile\controllers;


use common\components\ArrayHelper;
use common\components\exceptions\AssertHelper;
use common\models\user\UserIdentityProvider;
use common\models\UserProfile;
use frontend\components\AuthedController;
use frontend\models\user\UserFacade;
use Yii;
use yii\helpers\Json;

class NotificationsController extends AuthedController
{
    protected $viewPath = '@frontend/modules/profile/views/profile';

    public $user;

    public function injectDependencies(UserIdentityProvider $identityProvider)
    {
        $this->user = $identityProvider->getUser();
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'saveNotificationConfig' => ['post'],
                ]
            ]
        ]);
    }

    /**
     * Show edit notify params page
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('notifications', ['user'=>$this->user]);
    }

    /**
     * @return string
     * @throws \common\components\exceptions\InvalidModelException
     */
    public function actionSaveNotificationConfig()
    {

        $user = UserFacade::getCurrentUser();
        $json = Yii::$app->request->bodyParams;
        $user->notify_config = Json::encode($json['notify_config']);
        $user->userProfile->updateSetting([
            UserProfile::SETTINGS_NOCHECKONLINE => $json['noCheckOnline'],
            UserProfile::SETTINGS_DIALOG_AUTO_CREATE => $json['allowDialogAutoCreate'],
            UserProfile::SETTINGS_COMPANY_DIALOG_AUTO_CREATE => $json['allowCompanyDialogAutoCreate']
        ]);
        $user->addHistoryRecord('change_notify_config', Yii::$app->request->bodyParams);
        AssertHelper::assertSave($user);
        return _t('front.site', 'Updated');
    }
}
