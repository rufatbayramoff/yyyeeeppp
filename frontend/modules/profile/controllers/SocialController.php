<?php
/**
 * User: nabi
 */

namespace frontend\modules\profile\controllers;

use frontend\components\AuthedController;

class SocialController extends AuthedController
{

    public function actionIndex()
    {
        $user = $this->getCurrentUser(true);
        $userOsnRows = \common\models\UserOsn::find()->where(
            [
                'user_id' => $user->id
            ]
        )
            ->asArray()
            ->all();
        return $this->render(
            'social',
            [
                'userOsnRows' => $userOsnRows
            ]
        );
    }
}
