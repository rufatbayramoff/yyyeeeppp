<?php
/**
 * User: nabi
 */
namespace frontend\modules\profile\controllers;

use frontend\components\AuthedController;
use frontend\models\user\ChangeAvatarForm;
use frontend\models\user\SettingsForm;
use frontend\models\user\UserFacade;
use Yii;
use common\components\BaseController;
use common\models\GeoCountry;
use common\models\user\UserIdentityProvider;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserAccountAddressForm;
use frontend\models\user\UserAccountForm;
use yii\base\UserException;
use yii\helpers\Html;

class ProfileController extends AuthedController
{

    public function actionUpdate()
    {
        $user = $this->getCurrentUser();
        $post = app('request')->post();

        $accountForm = UserAccountForm::findOne([
            'user_id' => $user->id
        ]);
        $accountForm->load($post);
        $accountForm->user_id = $user->id;

        // prepare user address
        $userAddress = UserAccountAddressForm::findOne([
            'user_id' => $user->id,
            'type' => 'profile'
        ]);
        if (empty($userAddress)) {
            $userAddress = new UserAccountAddressForm();
            $userAddress->created_at = dbexpr("NOW()");
        }
        $userAddress->load($post, 'UserAddress');
        $userAddress->type = 'profile';

        $userLocation = UserSessionFacade::getLocation();
        if (empty($userLocation->city)) {
            $userAddress->validate();
            $this->setFlashMsg(false, Html::errorSummary($userAddress));
            return $this->redirect([
                '/profile'
            ]);
        }
        $userAddress->city = $userLocation->city;
        $userAddress->region = $userLocation->region;
        $userAddress->country_id = GeoCountry::tryFind(['iso_code' => $userLocation['country']])->id;

        $userAddress->user_id = $user->id;

        if($userAddress->id > 0) {
            if ($accountForm->validate()) {
                if ($userAddress->validate()) {
                    $userAddress->safeSave();
                    $accountForm->address_id = $userAddress->id;
                    $accountForm->safeSave();
                    $this->setFlashMsg(true, 'Profile updated');
                } else {
                    $this->setFlashMsg(false, Html::errorSummary($userAddress));
                }
            } else {
                $this->setFlashMsg(false, Html::errorSummary($accountForm));
            }
        }else{
            if ($accountForm->validate()) {
                $accountForm->safeSave();
            }
        }
        return $this->redirect([
            '/profile'
        ]);
    }

    public function actionIndex()
    {
        $user = $this->user;
        $accountForm = UserAccountForm::findOne([
            'user_id' => $user->id
        ]);
        return $this->render('myProfile', ['user' => $user, 'accountForm' => $accountForm]);
    }

    /**
     * Password change action
     *
     * @return string
     */
    public function actionChangePassword()
    {
        $userId = $this->user->id;
        $model = new \frontend\models\user\ChangePasswordForm();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->changePassword()) {
                    \common\models\UserLog::log($userId, 'password', 'log', 'password changed');
                    $this->setFlashMsg('success', _t('front', 'New password was saved.'));
                    return $this->redirect(['/profile']);
                } else {
                    \common\models\UserLog::log($userId, 'password', 'log', 'password failed');
                    $this->setFlashMsg(false, _t('front', 'Not valid password.'));
                }
            } else {
                \common\models\UserLog::log($userId, 'password', 'log', 'password not valid!');
                $this->setFlashMsg(false, _t('front', 'Not valid password.'));
            }
        }
        return $this->render('changePassword', [
            'model' => $model
        ]);
    }

    /**
     * change user email action
     *
     * @return type
     */
    public function actionChangeEmail()
    {
        /** @var FrontUser $user */
        $user = Yii::$app->user->getIdentity();
        $model = new \frontend\models\user\ChangeEmailForm($user);

        if (Yii::$app->request->getIsPost()) {

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                // If user is unconfirmed - just change email
                if ($user->getIsUnconfirmed() || $user->isEmptyEmail()) {
                    $model->changeEmail();
                    $msg = _t('front', 'New e-mail was sent to you. ');
                    $result = true;
                }

                // Else - create change email request
                else {
                    UserFacade::createChangeEmailRequest($model);
                    $model->markChanged();
                    $msg = _t('front', 'An email containing a link has been sent to your new email address. You must follow the link in the email in order to confirm the change of email address.');
                    $result = true;
                }
            } else {
                $msg = \yii\helpers\Html::errorSummary($model);
                $result = false;
            }

            if (Yii::$app->request->isAjax) {
                if ($result) {
                    Yii::$app->getSession()->setFlash('success', $msg);
                    return $this->jsonSuccess([
                        'message' => 'reload'
                    ]);
                } else {
                    return $this->jsonError($msg);
                }
            }
            Yii::$app->getSession()->setFlash($result ? 'success' : 'failure', $msg);
        }
        return $this->renderAdaptive('changeEmail', [
            'model' => $model
        ]);
    }

    /**
     * Change avatar for user
     */
    public function actionChangeAvatar()
    {
        $form = new ChangeAvatarForm();
        $form->loadFromPost(Yii::$app->request->getBodyParams());
        $form->process();
    }

}