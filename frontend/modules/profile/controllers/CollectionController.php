<?php
/**
 * User: nabi
 */

namespace frontend\modules\profile\controllers;


use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\models\Model3d;
use common\models\UserCollection;
use common\models\UserCollectionModel3d;
use common\models\UserStatistics;
use common\services\LikeService;
use frontend\components\AuthedController;
use frontend\components\UserUtils;
use frontend\models\model3d\CollectionFacade;
use Yii;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\web\ForbiddenHttpException;

/**
 * CollectionController
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class CollectionController extends AuthedController
{

    /**
     * Use own layout
     *
     * @var string
     */
    public $layout = 'collection.php';

    /**
     */
    public function init()
    {
        parent::init();
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete'     => ['post'],
                    'add-item'   => ['post'],
                    'add-simple' => ['post']
                ]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * render collections list or collection items list
     *
     * @param string $id
     * @return mixed
     * @throws Exception
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\base\UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex($id = 'list')
    {
        if (is_numeric($id)) {
            return $this->actionCollection($id);
        }
        $user = $this->getCurrentUser();
        $params = [
            'user' => $user
        ];
        if ($this->hasMethod('action' . ucfirst($id))) {
            return $this->runAction($id);
        }
        $params['items'] = CollectionFacade::getUserCollections();
        $two = 0;
        foreach ($params['items'] as $k => $item) {
            if ($item->system_type == 'likes') {
                $two++;
            }
            if ($item->system_type == 'files') {
                $item->is_active = 0;
                $item->safeSave();
                unset($params['items'][$k]);
            }
        }
        if ($two < 2) {
            CollectionFacade::createCollection($user->id, _t('front.collection.names', 'Wish List'), UserCollection::SYSTEM_TYPE_LIKES);
        }
        return $this->render(
            'collectionList',
            [
                'user' => $this->user,
                'params' => $params
            ]
        );
    }

    /**
     * add new user collection
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $model = new \frontend\models\user\CollectionForm();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->user_id = \frontend\models\user\UserFacade::getCurrentUserId();
            $result = [
                'success' => true,
                'message' => _t('front.site', 'reload')
            ];
            if ($model->load($post) && $model->validate()) {
                $model->user_id = \frontend\models\user\UserFacade::getCurrentUserId();
                $model->created_at = dbexpr("NOW()");
                $model->updated_at = dbexpr("NOW()");
                $model->safeSave();
            }
            if ($model->hasErrors()) {
                $result['message'] = \yii\helpers\Html::errorSummary($model);
                $result['success'] = false;
            }
            if ($this->isAjax) {
                return $this->jsonReturn($result);
            }
            $this->setFlashMsg($result['success'], $result['message']);
        }
        return $this->renderAdaptive(
            'collectionAdd',
            [
                'model' => $model
            ]
        );
    }

    /**
     * add collection - using ajax request
     *
     * @return mixed
     */
    public function actionAddSimple()
    {
        $post = \Yii::$app->request->post();
        $model = new UserCollection();
        $model->user_id = \frontend\models\user\UserFacade::getCurrentUserId();
        $model->title = \H($post['title']);
        $model->is_active = 1;
        $model->is_visible = 1;
        $model->created_at = dbexpr('NOW()');
        $result = [
            'success' => true,
            'message' => _t('front.site', 'reload')
        ];
        if ($model->validate()) {
            $model->safeSave();
            $result['collectionId'] = $model->id;
        }
        if ($model->hasErrors()) {
            $result['message'] = \yii\helpers\Html::errorSummary($model);
            $result['success'] = false;
        }
        return $this->jsonReturn($result);
    }

    /**
     * update user collection
     *
     * @param int $id
     *            - collection id
     * @return mixed
     * @throws Exception
     * @throws \yii\base\UserException
     */
    public function actionUpdate($id)
    {
        $model = CollectionFacade::getById($id);

        if (!\frontend\models\user\UserFacade::isObjectOwner($model)) {
            throw new BusinessException("No access to edit this collection.");
        }
        if (Yii::$app->request->isPost) {
            // TODO Move it to bussines logic
            if ($model->getIsSystem()) {
                throw new Exception('Cant delete system collection');
            }

            $post = Yii::$app->request->post();
            $result = [
                'success' => true,
                'message' => _t('front.site', 'reload')
            ];
            $model->id = $id;
            if ($model->load($post) && $model->validate()) {
                $model->id = $id;
                $model->user_id = \frontend\models\user\UserFacade::getCurrentUserId();
                $model->safeSave();
            } else {
                $result['success'] = false;
                $result['message'] = \yii\helpers\Html::errorSummary($model);
            }
            if ($this->isAjax) {
                return $this->jsonReturn($result);
            }
            $this->setFlashMsg($result['success'], $result['message']);
        }
        return $this->renderAdaptive(
            'collectionAdd',
            [
                'model' => $model
            ]
        );
    }

    /**
     * delete user collection
     *
     * @param int $id
     *            - collection id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $res = CollectionFacade::deleteCollection($id);
        $result = [
            'success' => $res,
            'message' => $res ? _t('front.site', 'Collection deleted') : _t('front.site', 'Collection cannot be deleted')
        ];

        $this->setFlashMsg($result['success'], $result['message']);
        return $this->redirect(
            [
                '/profile/collection'
            ]
        );
    }

    /**
     * Delete collections
     *
     * @throws InvalidArgumentException
     */
    public function actionDeleteCollections()
    {
        $ids = Yii::$app->request->post('ids');

        if (empty($ids)) {
            throw new InvalidArgumentException(_t('front.collections', 'Empty collections'));
        }

        /** @var UserCollection[] $models */
        $models = UserCollection::findAll($ids);

        if (empty($models)) {
            throw new InvalidArgumentException(_t('front.collections', 'Empty collections'));
        }

        foreach ($models as $model) {
            AssertHelper::assert(CollectionFacade::deleteCollection($model->id), _t('front.collections', 'Cant delete collection'), InvalidArgumentException::class);
        }

        $this->setFlashMsg(true, _t('front.site', 'Collections deleted'));
    }

    /**
     * bind item to collection
     *
     * @param int $id
     * @return array
     */
    public function actionAddItem($id)
    {
        $model3dId = app('request')->post('model3d_id');
        $userId = \frontend\models\user\UserFacade::getCurrentUserId();
        /** @var \common\models\Model3d $modelObj */
        $modelObj = \common\models\Model3d::tryFindByPk($model3dId, _t('front.site', 'Model not found'));
        $collectionInfo = UserCollection::tryFindByPk($id);
        $res = CollectionFacade::bindToCollection($model3dId, $id);
        $modelCollections = \frontend\models\model3d\Model3dFacade::getModelCollections($modelObj, $userId);
        $title = $collectionInfo['title'];

        $collectionsCount = $modelObj->user_id == $userId ? count($modelCollections) - 1 : count($modelCollections);

        if ($collectionsCount > 1) {
            $title = _t(
                'front.site',
                'In {total} Collections ',
                [
                    'total' => $collectionsCount
                ]
            );
        }
        $message = $res ? _t(
            'front.site',
            'Item added to {title} collection',
            [
                'title' => $collectionInfo['title']
            ]
        ) : _t('front.site', 'Item cannot be added to this collection');
        $result = [
            'success' => $res,
            'title'   => $title,
            'message' => $message
        ];

        if ($collectionInfo->system_type === UserCollection::SYSTEM_TYPE_LIKES) {
            UserStatistics::upLikes(UserStatistics::TYPE_MODEL3D, $model3dId);
        }
        return $this->jsonReturn($result);
    }

    /**
     * bind to default user collection
     *
     * @return array
     */
    public function actionBindDefault()
    {
        $model3dId = app('request')->post('model3d_id');
        $messageError = _t('front.site', 'Item already added to like collection');
        try {
            /** @var \common\models\UserCollection $collectionInfo */
            $collectionInfo = CollectionFacade::getUserSystemCollection(UserCollection::SYSTEM_TYPE_LIKES);
            $res = CollectionFacade::bindToCollection($model3dId, $collectionInfo['id']);
            UserStatistics::upLikes(UserStatistics::TYPE_MODEL3D, $model3dId);
        } catch (\yii\base\Exception $e) {
            $res = true; // turn off negative message
            $messageError = $e->getMessage();
        }

        $result = [
            'success' => $res,
            'title'   => $collectionInfo['title'],
            'message' => $res ? _t(
                'front.site',
                'Item added to {collection} collection',
                [
                    'collection' => $collectionInfo['title']
                ]
            ) : $messageError
        ];
        return $this->jsonReturn($result);
    }

    /**
     * unbind item from collection
     *
     * @param int $id
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUnbindItem($id)
    {
        /** @var UserCollectionModel3d $relInf */
        $relInf = UserCollectionModel3d::tryFindByPk($id);
        $collectionId = $relInf->collection_id;
        $res = CollectionFacade::unbindItemRelation($relInf);
        $result = [
            'success' => $res,
            'message' => $res ? _t('front.site', 'Item removed from collection') : _t('front.site', 'Item cannot be removed from this collection')
        ];
        $this->setFlashMsg($result['success'], $result['message']);
        return $this->redirect(
            [
                '/profile/collection/' . $collectionId
            ]
        );
    }

    /**
     * Unliked model
     *
     * @return array
     */
    public function actionLike()
    {
        /** @var Model3d $model */
        $model = Model3d::tryFindByPk(Yii::$app->request->post('model3dId'));
        $user = $this->getCurrentUser(true);

        LikeService::likeModel($model, $user);
        $likesCount = LikeService::getModelLikesCount($model);

        return $this->jsonSuccess(
            [
                'success'     => true,
                'likeMessage' => LikeService::getLikesCountString($likesCount),
                'likesCount'  => $likesCount,
                'message'     => _t('front.collection', 'Like'),
                'liked'       => true
            ]
        );
    }

    /**
     * Liked model
     *
     * @return array
     */
    public function actionUnlike()
    {
        /** @var Model3d $model */
        $model = Model3d::tryFindByPk(Yii::$app->request->post('model3dId'));
        $user = $this->getCurrentUser(true);

        LikeService::unlikeModel($model, $user);
        $likesCount = LikeService::getModelLikesCount($model);

        return $this->jsonSuccess(
            [
                'success'     => true,
                'likeMessage' => LikeService::getLikesCountString($likesCount),
                'likesCount'  => $likesCount,
                'message'     => _t('front.collection', 'Unlike'),
                'liked'       => false
            ]
        );
    }

    /**
     *
     * @return \yii\web\Response
     * @throws InvalidArgumentException
     */
    public function actionUnbindItems()
    {
        $ids = Yii::$app->request->post('ids');

        if (empty($ids)) {
            throw new InvalidArgumentException(_t('front.collections', 'No items to unbind'));
        }

        /** @var UserCollectionModel3d $models */
        $models = UserCollectionModel3d::findAll($ids);

        if (empty($models)) {
            throw new InvalidArgumentException(_t('front.collections', 'No items to unbind'));
        }

        foreach ($models as $model) {
            AssertHelper::assert(CollectionFacade::unbindItemRelation($model), _t('front.collections', 'Can\'t unbind item'), InvalidArgumentException::class);
        }

        $this->setFlashMsg(true, _t('front.site', 'Items removed from the collection'));
    }

    /**
     * unbind model from collection by rel
     *
     * @param int $collection
     * @param int $model
     * @return array
     */
    public function actionUnbindModel($collection, $model)
    {
        /** @var \common\models\base\UserCollectionModel3d $rel */
        $rel = \common\models\base\UserCollectionModel3d::tryFind(
            [
                'collection_id' => $collection,
                'model3d_id'    => $model
            ]
        );
        CollectionFacade::unbindItemRelation($rel);
        return $this->jsonMessage(_t('front.site', 'Done'));
    }

    /**
     * move item to another collection
     *
     * @param int $id
     * @return array|string
     */
    public function actionMoveItem($id)
    {
        /** @var UserCollectionModel3d $relModel */
        $relModel = UserCollectionModel3d::tryFindByPk($id);
        $cUser = $this->getCurrentUser(true);
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $relModel->load($post);
            $res = true;
            /** @var UserCollection $collectionModel */
            $collectionModel = UserCollection::tryFindByPk($relModel->collection_id);
            if (CollectionFacade::isOwner($collectionModel->user_id)) {
                $res = $relModel->safeSave();
            }
            $result = [
                'success' => $res,
                'message' => $res ? 'reload' : _t('front.site', 'Item is not moved')
            ];
            return $this->jsonReturn($result);
        }
        $colItems = CollectionFacade::getList($cUser->id);
        return $this->renderAdaptive(
            'collectionItemMove',
            [
                'model'           => $relModel,
                'collectionItems' => $colItems
            ]
        );
    }

    /**
     * show collection items
     * if tries to open other user collection, redirects to user
     *
     * @param int $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCollection($id)
    {
        $user = $this->getCurrentUser();
        /** @var UserCollection $collection */
        $collection = UserCollection::tryFindByPk($id);

        if (empty($user) || $collection->user_id != $user->id) {
            return $this->redirect(UserUtils::getUserCollectionRoute($collection));
        }
        if ($collection->user_id != $user->id) {
            throw new ForbiddenHttpException();
        }

        $params['user'] = $user;
        $params['collection'] = $collection;
        $params['items'] = $collection->userCollectionModel3ds;
        return $this->render(
            'collection',
            [
                'user' => $this->user,
                'params' => $params
            ]
        );
    }


    /**
     * get breadcrumbs for edit page
     *
     * @param mixed $modelCollection
     * @return array
     */
    private function getBreadcrumbs($modelCollection)
    {
        $breadcrumbs = [
            [
                'label' => _t('front.user', 'Private profile'),
                'url'   => [
                    '/profile'
                ]
            ],
            [
                'label' => _t('front.upload', 'Collections'),
                'url'   => [
                    '/profile/collection'
                ]
            ]
        ];
        if ($modelCollection) {
            $breadcrumbs[] = [
                'label' => _t('front.upload', $modelCollection->title),
                'url'   => [
                    '/profile/collection/' . $modelCollection->id
                ]
            ];
        }
        return $breadcrumbs;
    }
}
