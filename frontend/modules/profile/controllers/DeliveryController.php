<?php
/**
 * User: nabi
 */

namespace frontend\modules\profile\controllers;

use common\components\exceptions\BusinessException;
use common\models\UserAddress;
use frontend\components\AuthedController;

/**
 * CollectionController
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class DeliveryController extends AuthedController
{
    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        $currentUser = $this->getCurrentUser(true);
        $items = \common\models\UserAddress::getLatestShip($currentUser->id, 100);
        $params = [
            'user' => $this->getCurrentUser(true)
        ];
        return $this->render('delivery', [
            'params' => $params,
            'items' => $items
        ]);
    }

    /**
     *
     * @param
     *            $id
     * @return \yii\web\Response
     * @throws BusinessException
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRemove($id)
    {
        $this->getCurrentUser(true);
        /** @var UserAddress $address */
        $address = UserAddress::tryFindByPk($id);
        if (! \frontend\models\user\UserFacade::isObjectOwner($address)) {
            throw new BusinessException(_t("site.error", "No access"));
        }
        UserAddress::updateRow(['id'=>$id], [
            'type' => 'deleted'
        ]);
        return $this->redirect([
            '/profile/delivery'
        ]);
    }
}
