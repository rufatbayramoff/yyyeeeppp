<?php
/**
 * User: nabi
 */


echo \yii\bootstrap\Tabs::widget(
    [
        'renderTabContent' => false,
        'options'          => [
            'class' => '',
            'role'  => 'tablist'
        ],
        'items'            => $items
    ]
);


