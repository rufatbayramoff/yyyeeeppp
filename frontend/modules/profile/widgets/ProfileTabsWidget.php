<?php

namespace frontend\modules\profile\widgets;

/**
 * User: nabi
 */
use Yii;

class ProfileTabsWidget extends \yii\bootstrap\Widget
{

    public $section;

    /**
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $activeUrl = $this->section ? : Yii::$app->requestedRoute;

        $items = [
            [
                'label' => _t('front.user', 'My Profile'),
                'url'   => '/profile'
            ],
            [
                'label' => _t('front.user', 'Settings'),
                'url'   => '/profile/settings'
            ],
            [
                'label' => _t('front.user', 'Notifications'),
                'url'   => '/profile/notifications'
            ],
            [
                'label' => _t('front.user', 'Delivery'),
                'url'   => '/profile/delivery'
            ],
//            [
//                'label' => _t('front.user', 'Social'),
//                'url'   => '/profile/social'
//            ],
            [
                'label' => _t('front.user', 'Wish list'),
                'url'   => '/profile/collection'
            ]
        ];
        foreach ($items as $k => $item) {
            if (trim($item['url'], '/') == trim($activeUrl, '/')) {
                $item['active'] = true;
            }
            $items[$k] = $item;
        }
        /*
        return $this->render(
            'profileTabs',
            [
                'items' => $items
            ]
        );
        */
        return '<div class="over-nav-tabs-header"><div class="container"><h1>' . _t('site.ps', 'Profile') . '</h1></div></div>
                <div class="nav-tabs__container"><div class="container">' . $this->render(
            'profileTabs',
            [
                'items' => $items
            ]
        ) . '</div></div>';
    }
}
