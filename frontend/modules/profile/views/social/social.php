

<?=\frontend\modules\profile\widgets\ProfileTabsWidget::widget(); ?>

<div class="container">

    <h3 class="m-t0 m-b20"><?php echo _t('front.site', 'Social networks'); ?></h3>


    <div class="table-responsive">
        <table class="table table-bordered user-profile__social-net-table">
            <tbody>
            <tr>
                <th><?= _t('front.site', 'Social network')?></th>
                <th><?= _t('front.site', 'User')?></th>
                <th><?= _t('front.site', 'Email')?></th>
                <th><?= _t('front.site', 'Last used')?></th>
                <th><?= _t('front.site', 'Actions')?></th>
            </tr>

            <?php foreach ($userOsnRows as $row): ?>
                <tr>
                    <td>
                        <?php echo $row['osn_code']; ?>
                    </td>
                    <td>
                        <?php echo $row['username']; ?>
                    </td>
                    <td>
                        <?php echo $row['email']; ?>
                    </td>
                    <td>
                        <?php echo $row['updated_at']; ?>
                    </td>
                    <td>
                        <input type="button" class="btn btn-default btn-sm" value="<?= _t('front.site', 'Unbind')?>" />
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>

        </table>
    </div>

</div>