<?php
/** @var string $content */
use frontend\models\user\UserFacade;

/** @var \yii\web\View $this */

$this->beginContent('@frontend/views/layouts/main.php');
$storeActive = false;
if (!isset($this->params['page-class'])) {
    $this->params['page-class'] = 'js-collection-view';
}
if (isset($this->params['is_store'])) {
    $storeActive = true;
}

$tabs = [
    [
        'label' => _t('app', 'My Collections'),
        'url'   => ['/profile/collection/index'],
    ],
    [
        'label'  => _t('app', 'My Store'),
        'url'    => ['/profile/store/index'],
        'visible' => UserFacade::hasUploads(UserFacade::getCurrentUser()),
        'active' => $storeActive
    ]
];
$user = UserFacade::getCurrentUser();
$publishedModels = $user->getModel3ds()->storePublished($user)->all();
if ($publishedModels) {
    $tabs[] = [
        'label' => _t('app', 'My Business Tools'),
        'url'   => ['/profile/store/widget'],
    ];
}

?>

<?=\frontend\modules\profile\widgets\ProfileTabsWidget::widget(['section'=>'profile/collection']); ?>

<div class="<?= $this->params['page-class'] ?>">

        <div class="container m-b30">
                <?= isset($this->blocks['control-buttons']) ? $this->blocks['control-buttons'] : '' ?>

        </div>


    <div class="container">

        <?= $content; ?>

    </div>

</div>

<?php $this->endContent(); ?>




