<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserCollection */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="user-collection-form">
    <?php $form = ActiveForm::begin(['enableAjaxValidation'=>true, 'layout'=>'horizontal']); ?>
    <div class="form-message"></div>
    <div class="row">
        <div class="col-lg-8">
            <?php  
             echo Html::activeDropDownList(
                          $model, 
                          'collection_id', 
                          $collectionItems,
                          ['class'=>'form-control'])
            ?>
        </div>
        <div class="col-lg-4">
          <?php echo Html::submitButton($model->isNewRecord ? _t('front.collection', 'Create') : _t('front.collection', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success ts-ajax-submit' : 'btn btn-primary ts-ajax-submit']);
            ?>
        </div>
    </div>
    
    
    <?php ActiveForm::end(); ?>
</div>
