<?php
/** @var \yii\web\View $this  **/
/** @var array $params **/

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\image\ImageHtmlHelper;

$this->registerAssetBundle(\frontend\assets\CollectionAsset::class);

$this->title = _t('front.model', 'Collections');
$this->params['page-class'] = 'js-collection-list';

/** @var \common\models\UserCollection[] $items */
$items = $params['items'];
?>

<?php $this->beginBlock('control-buttons')?>

    <button
        type="button"
        class="btn btn-primary ts-ajax-modal"
        value="<?= Url::to('/profile/collection/add')?>"
        data-target="#modal_addcollection"
        data-toggle="tooltip"
        data-placement="bottom" title=""
        data-original-title="<?= _t('front.collection', 'Create New Collection')?>">

        <span class="tsi tsi-plus"></span>
        <?= _t('front.collection', 'Create New Collection')?>
    </button>

	<?php if(count($items) > 2): ?>

    &nbsp;

    <button type="button" class="btn btn-primary btn-circle js-collections-delete"
        data-toggle="tooltip"
        data-placement="bottom" title=""
        data-original-title="<?= _t('front.collection', 'Delete Collection')?>">

        <span class="tsi tsi-bin"></span>
    </button>

    <?php endif;?>

<?php $this->endBlock()?>

<div class="row my-collection-list">
<?php 
foreach($items as $collection):
    $coverImg = \frontend\models\model3d\CollectionFacade::getCollectionCover($collection->id, $collection->userCollectionModel3ds);
    $total = count($collection->userCollectionModel3ds);
?>
    <div class='col-sm-6 col-md-4'>
        <div
            class='catalog-item collection-item js-collection'
            data-id="<?php echo $collection->id; ?>"
            data-size="<?=$total?>"
            data-title="<?=  \H($collection->title);?>"
            >
            <div class='catalog-item__pic'>
                <?php if(!$collection->getIsSystem()): ?>
                    <div class="catalog-item__controls catalog-item__controls--left">
                        <div class="choosen-item">
                            <div class="checkbox checkbox-warning">
                                <input id="choosen-item__id" class="js-item-checkbox" type="checkbox">
                                <label for="choosen-item__id"></label>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item__controls"> 
                    <?php 
                        if ($total === 0) {
                            $options = [
                                'title' => _t('yii', 'Delete'),
                                'aria-label' => _t('yii', 'Delete'),
                                'data-confirm' => _t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'class' => 'catalog-item__controls__item',
                                'data-pjax' => '0',
                            ];
                            echo Html::a(
                                '<span class="tsi tsi-bin"></span> <span class="catalog-item__controls__item__hint">' .
                                _t('app', 'Delete'), '/profile/collection/delete/' . $collection->id, $options). "</span>";
                        }
                      
                        echo Html::a(
                            '<span class="tsi tsi-pencil"></span>' .
                            '<span class="catalog-item__controls__item__hint">'._t('front.collection', 'Edit').'</span>', '/profile/collection/update/' . $collection->id, [
                            'class' => 'ts-ajax-modal catalog-item__controls__item',
                            'data-pjax' => '0',
                            'data-target' => '#modal_updatecollection',
                            'title' => _t('front.collection', 'Edit')
                        ]);
                    ?>
                    </div>
                <?php endif ?>
                <div class="catalog-item__pic__img">
                    <div class="catalog-item__pic__img__fotorama js-catalog-item-slider">
                        <?php foreach($coverImg as $img):
                            $img = ImageHtmlHelper::getThumbUrl($img, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
                        ?>
                            <?= Html::beginTag('div', ['data-img' => $img]); ?>
                                <?= Html::a('', ['/profile/collection/' . $collection->id])?>
                            <?= Html::endTag('div'); ?>
                        <?php endforeach; ?>
                    </div>
                    <?php if(empty($coverImg)): ?>
                    <div class="catalog-item__empty">
                        <?= _t('front.collection', 'You haven\'t liked any models yet'); ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>        
            <div class='catalog-item__footer'>
              
                <div class='catalog-item__footer__price'>  <?php 
                  if(empty($collection->is_visible)){
                            echo sprintf("<span class='tsi tsi-lock'>%s</span> ", _t("site.collection", ""));
                      }
                      ?>
                    <?php echo _t('front.model3d', '{n, plural, =0{empty} =1{1 item} other{# items}}', ['n'=>$total]); ?>
                </div>

                <h4 class='catalog-item__footer__title'>
                    <?= Html::a(\H($collection->title), ['/profile/collection/' . $collection->id]); ?>
                </h4>
            </div>

            <div class="collection-item__decor-1"></div>
            <div class="collection-item__decor-2"></div>
            <div class="collection-item__decor-3"></div>

        </div>
    </div>    
<?php endforeach; ?>
</div>