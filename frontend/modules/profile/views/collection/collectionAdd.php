<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserCollection */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $model->is_active = true;
    $model->is_visible = true;
}
?>

<div class="user-collection-form">

    <?php $form = ActiveForm::begin(['enableAjaxValidation'=>false]); ?>
    <div class="form-message"></div>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?php #echo $form->field($model, 'description')->textInput(['maxlength' => true]); ?>
    <?php 
    #echo $form->field($model, 'is_visible')->label( _t('front.site', 'Public'))->checkbox(['template' => '<div class="checkbox">{input}{label}</div>']);
    ?>
                     
    
    <?php echo $form->field($model, 'is_visible', [
                ])->radioList([1=> _t('site.collection', 'Public'), 0=> _t('site.collection', 'Private')], [
                    'item' => function($index, $label, $name, $checked, $value) {
                        $lid = 'dtrmunit' . $index;
                        $return = '<div class="radio radio-inline"><input type="radio" ' .
                            ($checked ? 'checked=checked' : '') . ' id="' . $lid . '" name="' . $name . '" value="' . $value . '" tabindex="3">';
                        $return .= '<label class="modal-radio" for="' . $lid . '">';
                        $return .= '<span> ' . ucwords($label) . '</span>';
                        $return .= '</label></div>';
                        return $return;
                    }
                ]);
        ?>
    <div class="form-group">
        <?php echo Html::button($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success ts-ajax-submit' : 'btn btn-primary ts-ajax-submit']);
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
