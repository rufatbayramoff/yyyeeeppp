<?php
\frontend\assets\ImageCropAsset::register($this);
?>
<script type="text/ng-template" id="/app/profile/change-avatar-modal.html">

    <div class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t('front.user', 'Change profile picture')?></h4>
                </div>
                <div class="modal-body">

                    <div ng-if="!form.isAvatarLoaded" class="text-center m-t30 m-b30">
                        <input type="file" class="inputfile" id="avatar-file"
                               accept="image/jpeg,image/png,image/gif"
                               onchange="angular.element(this).scope().onAvatarFileChange(event)">
                        <label for="avatar-file">
                            <strong>
                                <i class="tsi tsi-upload-l"></i> <?= _t('site.profile', 'Choose a file'); ?>
                            </strong>
                        </label>
                    </div>

                    <p><?=_t('front.user', 'Please upload pictures with a size that is not less than 100x100 pixels. Max size of file is 1.5 MB');?></p>

                    <div ng-if="form.isAvatarLoaded">

                        <div class="row avatar-preview">
                            <div class="col-md-6 m-b20">
                                <div class="avatar-preview__crop-block">
                                    <img src="" id="crop-image" class="avatar-preview__crop-img">
                                </div>
                            </div>

                            <div class="col-md-6">

                                <h4 class="avatar-preview__title"><?=_t('site.profile', 'Preview of profile picture')?></h4>

                                <div class="avatar-preview__block">
                                    <div style="width: 100px; height: 100px;" class="img-circle avatar-preview__img"></div>
                                    <div style="width: 60px; height: 60px;" class="img-circle avatar-preview__img"></div>
                                    <div style="width: 34px; height: 34px;" class="img-circle avatar-preview__img"></div>
                                </div>

                                <button
                                    type="button"
                                    class="btn btn-primary"
                                    loader-click="saveAvatar()">
                                    <?= _t('site.profile', 'Save avatar')?>
                                </button>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</script>