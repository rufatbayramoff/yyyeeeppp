<?php

use frontend\components\UserUtils;
use \yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/** @var \yii\web\View $this */
/** @var \frontend\models\user\UserAccountForm $accountForm */
/** @var \frontend\models\user\UserAccountAddressForm $userAddress */
/** @var \common\models\UserProfile $userProfile */
/** @var \common\models\User $user */


Yii::$app->angular
    ->controller('profile/header')
    ->service(['notify', 'modal', 'router', 'user']);

$this->title = _t('front.user', 'Private profile');
$userProfile = $user->userProfile;
$userAddress = $user->userAddresses ? $user->userAddresses[0] : null;


if ($userAddress && empty($userAddress->country_id)) {
    $currentLocation = \frontend\components\UserSessionFacade::getLocation();
    $userAddress->country = $currentLocation['country'];
}
?>

<?= \frontend\modules\profile\widgets\ProfileTabsWidget::widget(); ?>

    <div class="container" ng-controller="ProfileHeaderController">

        <div class="user-private-profile">
            <?php $form = ActiveForm::begin(['action' => ['/profile/profile/update']]); ?>

            <div class="row">
                <div class="col-xs-12 tab-pane fade active in" id="profile-profile">

                    <div class="designer-card">
                        <h2 class="designer-card__title"><?= _t('front.user', 'Contact Information') ?></h2>

                        <div class="row">
                            <div class="col-sm-4 col-md-3 user-profile__edit-avatar">
                                <?php echo frontend\components\UserUtils::getAvatarForUser($user, 'top-profile'); ?>
                                <a href="#" class="btn btn-primary btn-ghost btn-sm m-b20" ng-click="showChangeAvatarModal()">
                                    <span class="tsi tsi-camera m-r10"></span><?= _t('front.user', 'Change avatar') ?>
                                </a>
                            </div>
                            <div class="col-sm-8 col-md-3">
                                <?php
                                echo $form->field($accountForm, 'full_name');
                                ?>
                            </div>
                            <div class="col-sm-4 col-md-3">
                                <?php
                                $accountForm->username = $user->username;
                                echo $form->field($accountForm, 'username')->textInput(['readonly' => empty($user->can_change_username)])->label(_t('site.user', 'Username'));
                                if (!empty($user->can_change_username)) {
                                    echo Html::tag('p', _t("site.user", 'Note: Your username can be changed only once.'), ['class' => 'text']);
                                }
                                ?>
                                <?php
                                echo yii\helpers\Html::a('<span class="tsi tsi-lock m-r10"></span>' . _t('user.profile', 'Change password'),
                                    '/profile/profile/change-password', [
                                        'class' => 'btn btn-primary btn-ghost m-b20 btn-sm m-b20'
                                    ]
                                );
                                ?>
                            </div>
                            <div class="col-sm-4 col-md-3">
                                <?php
                                echo $form->field($user, 'email')->textInput(['readonly' => true]);
                                ?>
                                <?=
                                Html::a('<i class="tsi tsi-mail m-r10"></i>' . _t('user.profile', 'Change email'), Url::to('/profile/profile/change-email'), [
                                        'title'       => _t('front', 'Change email'),
                                        'class'       => 'ts-ajax-modal change-email btn btn-primary btn-ghost btn-sm m-b20',
                                        'data-target' => '#userChangeEmail',
                                        'callback'    => 'function(){
                                        $("#wuser-confirm-result").innerHTML = "E-mail changed";
                                    }'
                                    ]
                                );
                                ?>
                                <?php
                                /** @var \common\models\user\ChangeEmailRequest[] $changeEmailRequests */
                                if ($changeEmailRequests = \common\models\user\ChangeEmailRequest::find()->forUser($user)->unconfirmed()->all()):
                                    ?>
                                    <div style="margin-top: -10px;font-size: 12px;line-height: 18px;">
                                        <?= _t('user.profile', 'Email with link has been sent to your new email address and awaiting confirmation: {emails}',
                                            ['emails' => implode(', ', \yii\helpers\ArrayHelper::getColumn($changeEmailRequests, 'email'))]) ?>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>

                    <div class="designer-card">
                        <h2 class="designer-card__title"><?= _t('front.user', 'Public Information') ?></h2>

                        <div class="user-profile-privacy-text">
                            <span class="tsi tsi-warning-c"></span> <?php echo _t('front.profile', 'This information will be displayed on your public profile'); ?>
                        </div>

                        <div class="row m-t30">
                            <div class="col-sm-3">
                                <?php
                                echo $form->field($accountForm, 'website');
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <?php
                                echo $form->field($accountForm, 'url_facebook')->textInput()->
                                label('<svg class="soc-ico m-r10" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="96.124px" height="96.123px" viewBox="0 0 96.124 96.123">
                                <path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803   c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654   c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246   c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z"></path>
                            </svg>' . _t('user.profile', 'Facebook Link'));
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <?php
                                echo $form->field($accountForm, 'url_instagram')->textInput()->
                                label('<svg class="soc-ico m-r10" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="169.063px" height="169.063px" viewBox="0 0 169.063 169.063">
                                <path d="M122.406,0H46.654C20.929,0,0,20.93,0,46.655v75.752c0,25.726,20.929,46.655,46.654,46.655h75.752   c25.727,0,46.656-20.93,46.656-46.655V46.655C169.063,20.93,148.133,0,122.406,0z M154.063,122.407   c0,17.455-14.201,31.655-31.656,31.655H46.654C29.2,154.063,15,139.862,15,122.407V46.655C15,29.201,29.2,15,46.654,15h75.752   c17.455,0,31.656,14.201,31.656,31.655V122.407z"></path>
                                <path d="M84.531,40.97c-24.021,0-43.563,19.542-43.563,43.563c0,24.02,19.542,43.561,43.563,43.561s43.563-19.541,43.563-43.561   C128.094,60.512,108.552,40.97,84.531,40.97z M84.531,113.093c-15.749,0-28.563-12.812-28.563-28.561   c0-15.75,12.813-28.563,28.563-28.563s28.563,12.813,28.563,28.563C113.094,100.281,100.28,113.093,84.531,113.093z"></path>
                                <path d="M129.921,28.251c-2.89,0-5.729,1.17-7.77,3.22c-2.051,2.04-3.23,4.88-3.23,7.78c0,2.891,1.18,5.73,3.23,7.78   c2.04,2.04,4.88,3.22,7.77,3.22c2.9,0,5.73-1.18,7.78-3.22c2.05-2.05,3.22-4.89,3.22-7.78c0-2.9-1.17-5.74-3.22-7.78   C135.661,29.421,132.821,28.251,129.921,28.251z"></path>
                            </svg>' . _t('user.profile', 'Instagram Username'));
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <?php
                                echo $form->field($accountForm, 'url_twitter')->textInput()->
                                label('<svg class="soc-ico m-r10" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 612 612">
                                <path d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411    c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513    c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101    c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104    c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194    c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485    c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z"></path>
                            </svg>' . _t('user.profile', 'Twitter Username'));
                                ?>
                            </div>
                        </div>
                        <div class="row m-t20">
                            <div class="col-sm-4">
                                <p>
                                <?=_t('user.profile', 'Public page url')?>: <a href="<?=HL(UserUtils::getUserPublicProfileUrl($user))?>"><?=HL(UserUtils::getUserPublicProfileUrl($user))?></a>
                                </p>
                            </div>
                            <div class="col-sm-8">
                                <div class="checkbox m-t0 m-b0">
                                    <input type="hidden" name="UserAccountForm[is_hidden_public_page]" value="0">
                                    <input type="checkbox" id="useraccountform-is_hidden_public_page" name="UserAccountForm[is_hidden_public_page]" value="1" <?=$accountForm->is_hidden_public_page?'checked':''?> aria-invalid="false">
                                    <label for="useraccountform-is_hidden_public_page">
                                        <?= _t('front.user', 'Hide public page') ?>
                                    </label>
                                </div>
							</div>
						</div>
                    </div>

                    <div class="designer-card m-b30">
                        <h2 class="designer-card__title"><?= _t('front.user', 'Personal Information') ?></h2>
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <?php
                                #echo $form->field($accountForm, 'dob_date')->label(_t('user.profile', 'Date of birth'));
                                $d = date("m/d/Y", strtotime($accountForm->dob_date));

                                echo $form->field($accountForm, 'dob_date')->widget(kartik\widgets\DatePicker::classname(), [
                                    'options'       => ['placeholder' => _t('site.user', 'Enter birth date ...')],
                                    'removeButton'  => false,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format'    => 'yyyy-mm-dd'
                                    ]
                                ]);

                                ?>
                            </div>

                            <div class="col-sm-6 col-md-4">
                                <?php
                                echo $form->field($accountForm, 'gender')->widget(
                                    Select2::classname(), [
                                        'data'          => [
                                            'male'   => _t('front.my', 'Male'),
                                            'female' => _t('front.my', 'Female'),
                                            'none'   => _t('front.my', 'None')
                                        ],
                                        'hideSearch'    => true,
                                        'options'       => ['placeholder' => _t('front.my', 'Select')],
                                        'pluginLoading' => true,
                                        'pluginOptions' => [
                                            'allowClear' => false
                                        ],
                                    ]
                                );

                                ?>
                            </div>

                            <div class="col-sm-6 col-md-4">

                                <div class="form-group">
                                    <label>
                                        <?php echo _t('user.profile', 'Shipping location'); ?>
                                    </label>
                                    <div class="user-profile__location">
                                        <?php
                                        echo frontend\widgets\UserLocationWidget::widget([]);
                                        ?>
                                    </div>
                                </div>

                            </div>


                            <?php
                            /*
                            echo $form->field($userAddress, 'country')->widget(
                                \kartik\select2\Select2::classname(), [
                                    'data' => \common\models\GeoCountry::getCombo('iso_code'),
                                    'options' => ['placeholder' => _t('front.site', 'Select')],
                                    'pluginLoading' => true
                                ]
                            );

                            echo $form->field($userAddress, 'region');
                            echo $form->field($userAddress, 'city');
                            if ($userAddress) {
                                echo $form->field($userAddress, 'zip_code')->label(_t('site.user', 'Zip code'));
                                echo $form->field($userAddress, 'address')->label(_t('site.user', 'Address'));
                            }
                            */
                            ?>


                        </div>
                    </div>


                    <div class="row p-t10">
                        <div class="col-sm-4">
                            <?= Html::submitButton(_t('app', 'Save Changes'), ['class' => 'btn btn-primary m-b10']) ?>
                        </div>
                        <div class="col-sm-8 text-right">
                            <?= Html::button(_t('app', 'Delete account'), [
                                'class'       => 'btn btn-danger ts-ajax-modal',
                                'title'       => _t('user.profile', 'Delete account'),
                                'data-target' => '#deleteAccountModal',
                                'value'       => Url::toRoute('/my/profile/delete-request')
                            ]) ?>
                        </div>
                    </div>


                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>

<?= $this->renderFile('@frontend/modules/profile/views/profile/changeAvatar.php'); ?>