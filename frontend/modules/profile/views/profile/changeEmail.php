<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\user\ChangeEmailForm */

?>
<div class="user-change-email"> 
    <?php
    $form = ActiveForm::begin([
        'layout' => 'inline',
        'id' => 'user-change-email-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false
    ]);
    ?>
    <div class="form-message"></div>
    <div class="text-center">
        <label>E-mail: </label>
        <?= $form->field($model, 'email')->textInput() ?>

        <?= Html::submitButton(_t('front', 'Change e-mail'),
            ['class' => 'btn btn-primary ts-ajax-submit', 'name' => 'submit-button']) ?>
    </div>
<?php ActiveForm::end(); ?>
</div>
