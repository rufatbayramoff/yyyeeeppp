<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\user\ResetPasswordForm */

$this->title = _t('front', 'Change password');
?>
<div class="site-reset-password">
<div class="container">

    <div class="row">

        <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">

            <div class="panel panel-default box-shadow m-t30 border-0">
                <div class="panel-body">

                    <h1 class="text-center m-t0"><?= \H($this->title) ?></h1>

                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                    <?php echo Html::errorSummary($model); ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'newPassword')->passwordInput() ?>
                    <div class="form-group">
                        <?= Html::submitButton(_t('front', 'Change password'), ['class' => 'btn btn-primary', 'name'=>'submit-button']) ?>
                        <?= Html::a(_t('site.common', 'Cancel'), ['/profile'], ['class' => 'btn btn-link'])?>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>

    </div>
</div>
</div>