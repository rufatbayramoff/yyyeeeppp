<?php

use frontend\models\user\UserFacade;

/** @var \common\models\User $user */

$config = new \lib\message\UserConfig($user);
Yii::$app->angular
    ->service(['notify', 'user', 'router'])
    ->controller('profile/notifications')
    ->controllerParams([
        'userId' => $user->id,
        'userConfig' => $config->toArray(),
        'userSettings' => $config->getUserSettings(),
        'noCheckOnline' => $config->isNoCheckOnline(),
        'allowDialogAutoCreate' => $config->allowDialogAutoCreate(),
        'allowCompanyDialogAutoCreate' => $config->allowCompanyDialogAutoCreate(),
        'notification' => [
            'groups' => \lib\message\Constants::getGroupLabels(),
            'times' => \lib\message\Constants::getTimePolicyLabels(),
            'senders' => \lib\message\Constants::getActiveSendersLabels(UserFacade::getCurrentUser()),
        ]
    ]);

$this->title = 'Notifications';

?>

<?=\frontend\modules\profile\widgets\ProfileTabsWidget::widget(); ?>

<div class="container" ng-controller="ProfileNotificationController" ng-cloak>

    <div
        ng-if="!haveSenders"
        class="row">

        <div class="col-md-12">
            <p><?= _t('site.settings', 'Please confirm your email address to set your notification preferences.')?></p>
        </div>
    </div>

    <div
        ng-if="haveSenders"
        class="user-notify">

        <div class="row user-notify__row">
            <div class="col-xs-12 col-sm-4 col-md-3"></div>
            <div ng-repeat="(sender, senderLabel) in notification.senders" class="col-xs-6 col-sm-3 col-md-2  user-notify__label">
                {{senderLabel}}
            </div>
        </div>

        <div
            ng-repeat="(group, groupsTitle) in notification.groups"
            class="row user-notify__row">
            <div class="col-xs-12 col-sm-4 col-md-3 user-notify__title">
                {{groupsTitle}}
            </div>

            <div
                ng-repeat="(sender, senderLabel) in notification.senders"
                class="col-xs-6 col-sm-3 col-md-2 user-notify__label">
                <button
                    ng-click="changeType(group, sender)"
                    type="button" class="btn btn-primary btn-sm">
                    {{notification.times[userConfig[group][sender]]}}
                </button>
            </div>
        </div>

        <div class="row m-t20">
            <div class="col-xs-12 col-sm-4 col-md-3"></div>
            <div class="col-xs-12 col-sm-8 col-md-9">

                <label class="checkbox-switch checkbox-switch--xs">
                    <input type="checkbox" name="noCheckOnline" ng-model="form.noCheckOnline">
                    <span class="slider"></span>
                    <span class="text"><?=_t('user.notifications', 'Always send emails and do not check user online status'); ?></span>
                </label>
            </div>
        </div>
        <div class="row m-t10">
            <div class="col-xs-12 col-sm-4 col-md-3"></div>
            <div class="col-xs-12 col-sm-8 col-md-9">

                <label class="checkbox-switch checkbox-switch--xs">
                    <input type="checkbox" name="allowDialogAutoCreate" ng-model="form.allowDialogAutoCreate">
                    <span class="slider"></span>
                    <span class="text"><?=_t('user.notifications', 'Recieve automatic notifications from suppliers. Some companies don\'t send private messages'); ?></span>
                </label>
            </div>
        </div>
        <div class="row m-t10">
            <div class="col-xs-12 col-sm-4 col-md-3"></div>
            <div class="col-xs-12 col-sm-8 col-md-9">

                <label class="checkbox-switch checkbox-switch--xs">
                    <input type="checkbox" name="allowCompanyDialogAutoCreate" ng-model="form.allowCompanyDialogAutoCreate">
                    <span class="slider"></span>
                    <span class="text"><?=_t('user.notifications', 'Send auto-replies for accepted orders'); ?></span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <hr class="m-b10">
                <button type="button" class="btn btn-primary m-t10 m-b20"
                        loader-click="saveNotificationConfig()"><?= _t('app', 'Save Changes') ?></button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <p><?= _t('site.settings', '"Working hours" - You\'ll receive messages from 9 am to 8 pm.') ?></p>
                <p><?= _t('site.settings', '"Daily" - You\'ll receive all messages from the previous day at 9 am.') ?></p>
            </div>
            <div class="col-sm-7">
                <div class="alert alert-info alert--text-normal">
                    <?= _t('site.settings', 'Please add <b>no-reply@treatstock.com</b> to your email address book or safe senders list to ensure that notifications don’t accidentally get sent to the spam/junk folder.')?>
                </div>
            </div>
        </div>
    </div>
</div>