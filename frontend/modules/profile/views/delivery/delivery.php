<?php
use yii\helpers\Html;

$this->title = _t('front.user', 'Delivery Address');
?>

<?=\frontend\modules\profile\widgets\ProfileTabsWidget::widget(); ?>

<div class="container">

    <div class="row wide-padding">

        <div class="col-xs-12">

            <h3 class="m-t0 m-b20"><?php echo $this->title; ?></h3>

            <div class="responsive-container-list">
                <?php
                if(empty($items)){
                    echo '<div style="font-size: 14px; margin: 0 15px 20px;">';
                    echo _t('site.delivery', 'In order to enter a delivery address you must first place an order');
                    echo '</div>';
                }else{
                    foreach($items as $address): ?>
                        <div class="responsive-container">
                            <div class="previous-delivers__item previous-delivers__item--delivery-page">

                                <div class="previous-delivers__item__text">
                                    <?php
                                    echo common\models\UserAddress::formatAddress($address, true);
                                    ?>

                                    <?php
                                    if(!empty($address->phone)){
                                        echo _t('site.store', 'Phone'), ': '; echo \H($address->phone);
                                    } ?>
                                </div>

                                <?php
                                echo Html::a("<span class='tsi tsi-bin m-r10'></span>" . _t("front.site", "Remove"),
                                    yii\helpers\Url::toRoute(['/profile/delivery/remove', 'id'=>$address->id]),
                                    ['class'=>'btn btn-danger btn-sm btn-block btn-ghost']
                                );
                                ?>
                            </div>

                        </div>
                    <?php endforeach;
                }?>
            </div>

        </div>

    </div>

</div>