<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\SimpleSelect2;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Personal Settings';

$user = $params['user'];
$userId = $user->id;
?>

<?=\frontend\modules\profile\widgets\ProfileTabsWidget::widget(); ?>

<div class="container ">

    <div class="user-my-settings">
        <h3 class="user-profile__title"><?php echo _t('front.user', 'Settings'); ?></h3>

            <?php $form = ActiveForm::begin(); ?>
            <?php
            echo '<div class="row">';
            /*
#$form->field($model, 'timezone_id')->textInput()
echo '<div class="col-sm-6 col-md-4 user-my-settings__block">';
echo $form->field($model, 'current_currency_iso')->widget(
   Select2::classname(),
   [
       'data'          => $currency,
       'hideSearch'    => true,
       'options'       => ['placeholder' => _t('front.my', 'Select')],
       'pluginLoading' => true
   ]
);
echo '</div>'; */
            echo '<div class="col-sm-6 col-md-4 user-my-settings__block">';
            echo $form->field($model, 'current_lang')->widget(
                Select2::classname(),
                [
                    'data'          => $systemLangs,
                    'hideSearch'    => true,
                    'options'       => ['placeholder' => _t('front.my', 'Select')],
                    'pluginLoading' => true,

                ]
            );
            echo '</div>';

            echo '<div class="col-sm-6 col-md-4 user-my-settings__block">';

            echo $form->field($model, 'current_metrics')->widget(
                Select2::classname(),
                [
                    'data'          => [
                        'in' => _t('front.user', 'U.S. Standard Measurements'),
                        'cm' => _t('front.user', 'Metric System')
                    ],
                    'hideSearch'    => true,
                    'options'       => ['placeholder' => _t('front.my', 'Select')],
                    'pluginLoading' => true,

                ]
            );
            echo '</div>';

            echo '<div class="col-sm-6 col-md-4 user-my-settings__block">';
            if (empty($model->timezone_id)) {
                $model->timezone_id = \frontend\components\UserSessionFacade::getTimezone();
            }
            echo $form->field($model, 'timezone_id')->widget(
                Select2::classname(),
                [
                    'data'          => $timezones,
                    'options'       => ['placeholder' => _t('front.my', 'Select')],
                    'pluginLoading' => true,
                ]
            );
            echo '</div>';
            if (!empty($user['access_token'])) {
                echo '<div class="col-sm-7 col-md-5" style="font-size: 16px;"> <br />';
                echo '<pre><b>' . frontend\widgets\SiteHelpWidget::widget(['alias' => 'api.help']) .
                    ' ' . _t('user.settings', 'API Key:') . '</b> ' . H($user['access_token']) .
                    '</pre>';

                echo '</div>';
            }
            echo '</div>';
            ?>


        <div class="row">
            <div class="col-sm-12">
                <?= Html::submitButton(_t('site.profile', 'Save Changes'), ['class' => 'btn btn-primary m-t10', 'data-pjax' => 1]) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>


    </div>
    <div class="row">
        <div class="col-xs-12 wide-padding text-right">
            <hr class="m-b20"/>
            <?php
            /*
            // check if sent?
            $userRequest = common\models\UserRequest::findOne(['user_id' => $user->id, 'request_type' => 'delete']);

            if ($userRequest && $userRequest->status != 'revert' && $userRequest->status !== 'done') {
                if ($userRequest->status == \common\models\UserRequest::STATUS_NEW) {
                    echo '<span class="text-danger">' . _t('front.user', 'Deactivate request in progress. ') . '</span>';
                    echo Html::a(
                        _t('front.user', 'Cancel'),
                        '/profile/settings/cancel-delete',
                        ['class' => 'btn btn-xs btn-primary', 'data-confirm' => _t('site', 'Are you sure?'), 'data-method' => 'post']
                    );
                } else {
                    if ($userRequest->status == \common\models\UserRequest::STATUS_CANCEL) {
                        echo '<span class="text-danger">' . _t('front.user', 'The request to deactivate your account has been cancelled. Please contact us.') . '</span>';
                    }
                }
            } else {
                echo Html::a(
                    _t('front.user', 'Request to deactivate account'),
                    '/profile/settings/delete',
                    [
                        'class'         => 'btn btn-xs btn-info',
                        'oncontextmenu' => 'return false',
                        'data-confirm'  => _t('site', 'Are you sure?'),
                        'data-method'   => 'post'
                    ]
                );
            }*/
            ?>
        </div>
    </div>
</div>
