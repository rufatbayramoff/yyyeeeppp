<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Cyber Note">
    <title>Cyber Note</title>

    <!-- Custom styles for this template -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/site.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@300;500;700&display=swap" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">
    <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>

<body class="d-flex h-100 text-center bg-dark">

<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="mb-auto">
        <a href="/" class="head-logo">
            <img src="img/cnote-logo.svg" alt="CNote">
        </a>
    </header>

    <main class="">
        <h1 class="mp-title text-white">cybernote</h1>
        <p class="lead mp-lead text-white mb-5">Credit Notes in Blockchain</p>

        <div class="p-md-5 mx-md-5">
            <div class="mb-3">
                <h3 style="height: 58px" class="text-white">Thank you for confirmation</h3>
            </div>
            <button style="opacity: 0;" class="w-100 btn btn-lg btn-lime mb-3" type="submit">Submit</button>
            <small style="opacity: 0" class="text-muted">By clicking Submit, you agree to the terms of use.</small>

        </div>
    </main>

    <footer class="mt-auto text-white-50">
        <p></p>
    </footer>
</div>


</body>
</html>

