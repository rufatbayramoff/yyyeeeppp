/**
 * Created by analitic on 03.08.16.
 */
var socialButtonsClass = {
    config: {},
    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);

        //$(function () {
            var wId = config.id || '';
            var link = config.link || false;
            var title = config.title || false;
            self.initSocialButtons(wId, link, title);
        //});
    },

    /**
     * get share link pop-up
     *
     * @param $doc
     * @param link
     * @returns {string}
     */
    getShareLink: function ($doc, link) {
        return "<div class='text-center'><input onfocus='$(this).select()' class='form-control m-b10 share-input' type='text' value='" + link
            + "' /></div>";
    },

    /**
     * get embed link. adds widget=1 param to given link
     *
     * @param $doc
     * @param link
     * @returns {string}
     */
    getShareEmbed: function ($doc, link) {
        if (link.indexOf('?') > 0) {
            link = link + '&widget=1';
        } else {
            link = link + '?widget=1';
        }
        var iframe = '<iframe width="auto" height="550" src="' + link + '" frameborder="0"></iframe>';
        return "<div class='text-center'><input onfocus='$(this).select();' class='form-control m-b10 share-input' size=55 type='text' value='"
            + iframe + "' /></div>";
    },

    /**
     * init social buttons
     */
    initSocialButtons: function (id, link, title) {

        var link = link || document.location.href;
        var documentTitle = title || document.title;

        var shareLinkContent = this.getShareLink($('body'),link);
        var shareEmbedContent = this.getShareEmbed($('body'),link);
        $('.sharelink'+id).popover({trigger: 'focus', html: true, content: shareLinkContent});
        $('.shareembed'+id).popover({trigger: 'focus', html: true, content: shareEmbedContent});
        $('.shareembed'+id).on('click', function () {
            $(this).popover('toggle');
        });
        $('body').find('.sharelink'+id).on('click', function () {
            $(this).popover('toggle');
        });
        window.fbAsyncInit = function () {
            FB.init({
                appId: facebook_app_id,
                xfbml: true,
                version: 'v2.5'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $('.facebook-send'+id).on('click', function () {
            FB.ui({
                method: 'send',
                link: link
            }, function (response) {
            });
        });
        $('.js-social-likes'+id).socialLikes({
            counters : true,
            zeroes:  false,
            title: documentTitle
        });

        $(document).on('popup_opened.social-likes', function(event, service) {
            if ((typeof(ga) != 'undefined') && ga){
                ga('send', 'event', {
                    eventCategory: 'SocialShare',
                    eventAction:  service,
                    eventLabel: location.href
                });
            }
        });
    }
};
