var uuidHelper = {
    BEGIN_TIME: 1523277224.117, //2018 9 Apr
    ADD_LEN: 7,
    RAND_CHARS: '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
    toBase: function ($num) {
        $b = this.RAND_CHARS.length;
        $base = this.RAND_CHARS;
        $r = Math.floor($num % $b);
        $res = $base[$r];
        $q = Math.floor($num / $b);
        while ($q) {
            $r = Math.floor($q % $b);
            $q = Math.floor($q / $b);
            $res = $base[$r] + $res;
        }
        return $res;
    },

    random_int: function (min, max) {
        max = max + 1;
        return Math.floor(Math.random() * (max - min)) + min;
    },

    generateRandCode: function ($len) {
        $randCharsCount = this.RAND_CHARS.length - 1;
        $returnValue = '';
        for ($i = 0; $i < $len; $i++) {
            $returnValue += this.RAND_CHARS[this.random_int(0, $randCharsCount)];
        }
        return $returnValue;
    },

    microtime: function (get_as_float) {
        var now = new Date().getTime() / 1000;
        var s = parseInt(now, 10);
        return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
    },

    generateUuid: function () {
        $addLen = this.ADD_LEN;
        $microtime = this.microtime(true);
        $nowTime = ($microtime - this.BEGIN_TIME) * 1000;
        $microtimeEnc = this.toBase($nowTime);
        $resUuid = $microtimeEnc;
        $resUuid += this.generateRandCode($addLen);
        return $resUuid;
    }
};

