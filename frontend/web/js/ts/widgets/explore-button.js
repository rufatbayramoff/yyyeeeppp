/**
 * Widget of explore button
 */
var ExploreButton = Backbone.View.extend({

    /**
     *
     */
    events : {
        'click [data-category-id]' : 'onCategoryClick'
    },

    /**
     *
     */
    initialize: function()
    {
        this.$buttonLabel = this.$('button span:first');
    },

    /**
     *
     * @param event
     */
    onCategoryClick : function(event)
    {
        var link = $(event.currentTarget),
            categoryId = parseInt(link.data('categoryId'));

        this.$buttonLabel.text(link.text());
        if(categoryId < 0){
            categoryId = '';
        }
        this.$el.trigger("explore-button:change", {categoryId : categoryId});
    }
});
