/**
 * Widget of top search form
 */
var Search = Backbone.View.extend({

    /**
     *
     */
    events : {
        'explore-button:change #search-explore' : 'onCategoryChange'
    },

    /**
     *
     * @param event
     * @param data
     */
    onCategoryChange : function(event, data)
    { 
        if(parseInt(data.categoryId) > 0){
            this.$('input[name=category]').val(data.categoryId);
        }else{
            this.$('input[name=category]').val('');
        }
    }
});
