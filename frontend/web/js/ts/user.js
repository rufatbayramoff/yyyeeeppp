
/**
 * base user object, functions for authed user
 * 
 * @type Object
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
TS = TS || {};
TS.User = {

    /**
     * Is guest
     */
    isGuest : true,

    /**
     * User name
     */
    username : undefined,

    /**
     * User id
     */
    userId : undefined,

    /**
     *
     */
    widgetResultId: '#wuser-confirm-result',
    /**
     * request to resend email
     *  
     * @returns {Boolean}
     */
    resendEmail: function (url)
    {
        var me = this;
        $.ajax({
            type: 'POST',
            cache: false,
            url: url,
            success: function (response) {
                if (response.success) {
                    $(me.widgetResultId).html(response.message);
                    $('.resend-email').hide();
                } else {
                    $(me.widgetResultId).html(response.message || 'error_user1');
                }
            }
        });
        return false;
    },
    /**
     * request email change
     * 
     * @returns 
     */
    changeEmail: function ()
    {
        var me = this;
    },
    
    /**
     * init event listeners for collection widget
     * 
     * @param {type} isGuest
     * @param {type} modelId
     * @param {type} callback
     * @returns {undefined}
     */
    initCollectionAddWidget: function(isGuest, modelId, callback)
    {
        var me = this;
        
        // add collection and add model to this new collection
        $('button.addcollection').off('click').click(function(a,b){
            var title = $('#collection-'+modelId).find('.newcollection').val();
            $.ajax({
                url: '/profile/collection/add-simple',
                type: "POST",
                data: {'title': title},
                dataType: 'json',
                error: function (result) {
                    result = result.responseJSON;
                    new TS.Notify({type: 'error', text : _t('site.user', 'Please specify correct title.')});
                },
                success: function (result) {
                    if (result.success === false) {
                        new TS.Notify({type: 'error', text: _t('site.user', 'Please specify correct title.')});
                    }else{
                        me.addToCollection(modelId, result.collectionId, callback);
                    }
                }
            });
        });

        // remove from collection
        $('.collection-reject').off('click').on('click', function (e) {
            e.preventDefault();
            var colId = $(this).data('id');
            TS.User.removeFromCollection(modelId, colId, function (json) {
                location.reload();
            });
        });

        // add to default collection
        $('.collection-add').off('click').on('click', function(e){
            TS.User.addToCollection(modelId, 0, function (json) {
                 callback(json);
            });
        });
        // don't close on click
        $('#collection-'+modelId + ' .dropdown-menu').off('click').on('click', function(e) {
            e.preventDefault(); 
            return false; 
        });

        // add to collection
        $('.collection-select').off('click').on('click', function (e) {
            e.preventDefault();
            var colId = $(this).data('id');
            if (isGuest) {
                TS.Visitor.loginForm();
            } else {
                TS.User.addToCollection(modelId, colId, function (json) {
                    if (json.success) {
                        $('#collection-' + modelId + ' .collection-title').addClass('btn-primary');
                        $('#collection-' + modelId + ' .collection-title').html(json.title);
                    } 
                    callback(json);
                });
            }
        });
    },
    /**
     * remove model3d from specified collection
     * 
     * @param {type} itemId
     * @param {type} collectionId
     * @param {type} callback
     * @returns {undefined}
     */
    removeFromCollection: function (itemId, collectionId, callback) {
        $.ajax({
            url: '/profile/collection/unbind-model/?collection=' + collectionId + '&model=' + itemId,
            type: "POST",
            data: {'model3d_id': itemId},
            dataType: 'json',
            error: function (result) {
                result = result.responseJSON;
                new TS.Notify({type: 'error'});
            },
            success: function (result) {
                if (result.success === false) {
                    new TS.Notify({type: 'error', text: result.message});
                } else if (callback) {
                    callback(result);
                }
            }
        });
    },
    
    /**
     * add model3d to collection
     * 
     * @param {type} fileId
     * @param {type} collectionId
     * @param {type} callback
     * @returns {undefined}
     */
    addToCollection : function(fileId, collectionId, callback){
        var url = '/profile/collection/add-item/' + collectionId;

        if (collectionId === 0) {
            url = '/profile/collection/bind-default';
        }
        $.ajax({
            url: url,
            type: "POST",
            data: {'model3d_id': fileId},
            dataType: 'json',
            error: function (result) {
                result = result.responseJSON;
            },
            success: function (result) {
                if(result.success===false){
                     new TS.Notify({ type : 'error', text: result.message});
                }else if(callback){
                    callback(result);
                    location.reload();
                }
            }
        });
    }
};