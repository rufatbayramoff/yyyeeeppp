var loginFormClass = {
    config: {
        redirectTo: '/',
        rememberMe: true
    },

    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        self.reInitRemoteLoginUrls();
    },

    reInitRemoteLoginUrls: function () {
        var self = this;
        osnUrlQuery = '&redirectTo=' + encodeURIComponent(self.config.redirectTo);
        if (self.config.rememberMe) {
            osnUrlQuery += '&rememberMe=1';
        }

        $('a.auth-link.google').attr('href', '/user/auth-remote?authclient=google' + osnUrlQuery);
        $('a.auth-link.facebook').attr('href', '/user/auth-remote?authclient=facebook' + osnUrlQuery);
    }
};
