/**
 * user visitor object
 *
 * @type type
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
TS = TS || {};
TS.Visitor = {

    signUpForm: function (redirTo) {
        return this.loginForm(redirTo, 'signUp');
    },

    /**
     *  shows login form
     *
     * @param {string} [mode] Flag who tab we must show: signUp, login
     * @param {string|undefined} [redirectTo]
     * @returns {undefined}
     */
    loginForm: function (redirectTo, mode) {
        var loginForm = $('#loginModal');

        if (!redirectTo) {
            redirectTo = window.location.href;
        }

        // Test login on other
        if (!siteParams.config.isGlobalSession) {
            var authUrl = '/user/login';
            if (mode=='signUp') {
                authUrl = '/user/signup';
            }

            authUrl = authUrl+'?redirectTo=' + encodeURIComponent(redirectTo);
            authUrl = TS.formRestoreSessionUrl(authUrl);
            window.location.href = authUrl;
            return;
        }

        $('#loginform-redirectto, #signupform-redirectto', loginForm).val(redirectTo);

        // update osn links
        var isRemember = !!$('#loginform-rememberme').prop('checked');

        osnUrlQuery = '&redirectTo=' + encodeURIComponent(redirectTo);
        if (isRemember) {
            osnUrlQuery += '&rememberMe=1';
        }
        $('a.auth-link.google').on('click', function(){
            if ((typeof(ga) != 'undefined') && ga && mode=='signUp') {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'registration',
                    eventAction:  'click',
                    eventLabel: 'registration'
                });
            }
        });
        $('a.auth-link.facebook').on('click', function(){
            if ((typeof(ga) != 'undefined') && ga && mode=='signUp') {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'registration',
                    eventAction:  'click',
                    eventLabel: 'registration'
                });
            }
        });
        $('#loginAjax-modal_signup_tab').on('click', function() {
            mode="signUp";
        });
        $('#loginAjax-modal_signin_tab').on('click', function() {
            mode="signIn";
        });

        loginForm.submit(function(){
            if (mode=='signUp') {
                if (typeof(grecaptcha)!=='undefined' && grecaptcha && !grecaptcha.getResponse()) {
                    $('#recaptureRequired').removeClass('hidden');
                    return false;
                } else {
                    $('#recaptureRequired').addClass('hidden');
                }
            }
            if ((typeof(ga) != 'undefined') && ga && mode=='signUp') {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'registration',
                    eventAction:  'click',
                    eventLabel: 'registration'
                });
            }
        });

        $('a.auth-link.google').attr('href', '/user/auth-remote?authclient=google' + osnUrlQuery);
        $('a.auth-link.facebook').attr('href', '/user/auth-remote?authclient=facebook' + osnUrlQuery);

        loginForm
            .modal('show')
            .css('z-index', 1051);

        if (mode === 'signUp') {
            $('.nav-tabs a[href="#user-loginAjax-signup"]', loginForm).tab('show');
        }
        else {
            $('.nav-tabs a[href="#user-loginAjax-login"]', loginForm).tab('show');
        }
        return loginForm;
    }
};
