TS = TS || {};

/**
 *
 * @type {{bindedView: Array, registerBindView: Function, init: Function}}
 *
 */
TS.Directives = {

    /**
     *
     */
    directives : {},

    /**
     *
     * @param name
     * @param viewClass
     */
    registerDirective  : function(name, viewClass)
    {
        if(this.directives[name])
        {
            throw "Directive already registered";
        }

        if(!viewClass.prototype.directiveSelector)
        {
            throw "Bind View must contain binded selector";
        }

        this.directives[name] = viewClass;
    },

    /**
     *
     */
    init : function()
    {
        _.each(this.directives, function (viewClass, name)
        {
            var elements = $(viewClass.prototype.directiveSelector);
            _.each(elements, function(element)
            {
                element = $(element);
                var data = element.data(),
                    bindedDirectives = data.$$bindedDirectives || {};

                // if directive already binded
                if(bindedDirectives[name])
                {
                    return;
                }

                var modelData = _.clone(data);
                modelData['$$bindedDirectives'] = undefined;


                var model = new TS.Directives.DataModel(data);
                bindedDirectives[name] = new viewClass({el: element, model: model});
                element.data('$$bindedDirectives', bindedDirectives);
            });
        });
    }

};


/**
 *
 */
TS.Directives.DataModel = Backbone.Model.extend({


});



/**
 * Init ts view on document ready
 */
$(function ()
{
    if(TS.Directives){
        TS.Directives.init();
    }
});