/**
 * TS - main app object/namespace
 *
 * @type Object
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
var TS = {
    /**
     * init data
     *
     * @returns {undefined}
     */
    init: function () {
        TS.initMobileSearch();
    },
    initMobileSearch: function () {
        var t = $('.adaptive-search-block').insertBefore($('nav.header-bar div.container'));
    },
    confirm: function (question, callback, settingsNew) {
        var settings = {
            title: '',
            message: question,
            confirm: 'OK',
            dismiss: 'Cancel',
            canClose: true
        };
        if (settingsNew) {
            settings.confirm = typeof (settingsNew.confirm) == "undefined" ? 'OK' : settingsNew.confirm;
            settings.dismiss = typeof (settingsNew.dismiss) == "undefined" ? 'Cancel' : settingsNew.dismiss;
            if (typeof settingsNew.canClose !== 'undefined') {
                settings.canClose = settingsNew.canClose;
            }
        }
        var tpl = '<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="confirm-label" aria-hidden="true">\n\
                        <div class="modal-dialog modal-md">\n\
            <div class="modal-content">\n\
              <div class="modal-header">\n\ '
            + (settings.canClose ? '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>\n\ ' : '')
            + '                <h4 class="modal-title" id="confirm-label"></h4>\n\
                      </div>\n\
                      <div class="modal-body">\n\
                        <p class="message"></p>\n\
                      </div>\n\
                      <div class="modal-footer">\n\ '
            + (settings.dismiss ? '<button type="button" class="btn btn-default dismiss" data-dismiss="modal"></button>\n\ ' : '')
            + (settings.confirm ? '<button type="button" class="btn btn-primary confirm" data-dismiss="modal"></button>\n\ ' : '')
            + '</div>\n\
                    </div>\n\
                  </div>\n\
                </div>';

        $('#confirm').remove();

        $(tpl).insertAfter('footer');
        $confirmWin = $('#confirm');
        $('.message', $confirmWin).html(settings.message);
        $('.confirm', $confirmWin).html(settings.confirm);
        $('.dismiss', $confirmWin).html(settings.dismiss);

        $confirmWin.on('click', '.confirm', function (event) {
            $confirmWin.data('confirm', true);
        });

        $confirmWin.on('hide.bs.modal', function (event) {
            if ($confirmWin.data('confirm')) {
                setTimeout(function () { // Async ok function
                    callback('ok');
                }, 0);
            } else {
                setTimeout(function () {
                    callback('cancel');
                }, 0);
            }
        });
        $confirmWin.modal('show');
    },
    setCookie: function (key, value) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (120 * 1000));
        if (value === "") {
            document.cookie = key + '=' + value + ';path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        } else {
            document.cookie = key + '=' + value + ';path=/;expires=' + expires.toUTCString();
        }
    },
    getCookie: function (name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    },
    uploadInit: function () {
        var inputs = document.querySelectorAll('.inputfile');
        Array.prototype.forEach.call(inputs, function (input) {
            var label = input.nextElementSibling,
                labelVal = label.innerHTML;

            input.addEventListener('change', function (e) {
                var fileName = '';
                if (this.files && this.files.length > 1)
                    fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                else
                    fileName = e.target.value.split('\\').pop();

                if (fileName)
                    $(label.querySelector('span')).text(fileName);
                else
                    $(label).text(labelVal);
            });
        });
    },

    /**
     * tsrm - abbr. ts read more
     * usage example :
     * <p> some text here <span class='tsrm-content'>hidden here</span>. <button class='tsrm-toggle'>Read More</button></p>
     * @returns {undefined}
     */
    initReadMore: function () {
        $('.tsrm-content').addClass('hide');

        $('.tsrm-toggle').on('click', function (e) {
            e.preventDefault();
            $(this).next('.tsrm-content').toggleClass('hide');
            $(this).remove();
        });
    },
    initAjaxAction: function () {
        $('body').on('click', '.ts-ajax', function (e) {
            e.preventDefault();
            var elm = $(this);
            var ajaxLink = elm.attr('href') || elm.attr('value');
            var callbackjs = new Function('elm', 'result', elm.attr('jsCallback'));
            $.ajax({
                url: ajaxLink,
                type: "POST",
                dataType: 'json',
                error: function (result) {
                    var msg = 'Error. Please try again.';
                    if (result.responseJson && result.responseJson.message) {
                        msg = result.responseJson.message;
                    }
                    new TS.Notify({
                        type: 'error',
                        text: result.responseText || msg,
                        target: '.messageBox',
                        automaticClose: false
                    });
                },
                success: function (result) {
                    if (result.message === 'reload') {
                        window.location.reload();
                        return;
                    } else if (result.redir) {
                        document.location = result.redir;
                    }

                    if (result.success === true) {
                        if (typeof callbackjs == 'function') {
                            callbackjs(elm, result);
                        } else {
                            new TS.Notify({
                                text: result.message
                            });
                        }
                    } else {
                        new TS.Notify({
                            type: 'error',
                            text: result.message,
                            target: '.messageBox',
                            automaticClose: true
                        });
                    }
                }
            });
        });
    },

    /**
     * init external links
     */
    initExternalLink: function () {

        $('body').on('click', '.externallink', function (e) {
            e.preventDefault();
            var elm = $(this);
            var url = elm.attr('data-go');
            window.open(url);
            return false;
        });
    },
    initAnimation: function () {
        //Animate element when its scroll
        //***
        // Add .animated class
        // Appear animation:    data-animation-in="animateCssEffect"
        // Hide animation:      data-animation-out="animateCssEffect"
        // Example:
        // <h2 class="animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">This title animated</h2>
        //***

        var windowH = $(window).height();

        $('.animated').each(function () {

            var elPos = $(this).offset().top;
            $(this).css("opacity", "0");

            if (elPos <= windowH) {
                $(this).removeClass($(this).data('animation-out')).addClass($(this).data('animation-in')).css("opacity", "1");
            }
        });

        $(window).scroll(this.updateAnimation);

        $('[data-toggle="tab"]').on('shown.bs.tab', this.updateAnimation);
    },


    updateAnimation: function () {
        $('.animated').each(function () {
            var elPos = $(this).offset().top;
            var topOfWindow = $(window).scrollTop();
            var windowH = $(window).height();
            if (elPos < topOfWindow + windowH) {
                $(this).removeClass($(this).data('animation-out')).addClass($(this).data('animation-in')).css("opacity", "1");
            } else {
                $(this).removeClass($(this).data('animation-in')).addClass($(this).data('animation-out')).css("opacity", "1");
            }
            ;
            /*
            if (elPos < topOfWindow+70) {
                $(this).removeClass($(this).data('animation-in')).addClass($(this).data('animation-out')).css("opacity", "1");
            };
            */
        });
    },


    /**
     * init event handlers for UI
     *
     * @returns {undefined}
     */
    initEventListeners: function () {
        // init ajax modal pop up
        this.initAjaxModal();
        // init external link click
        this.initExternalLink();
        // init read more link
        this.initReadMore();
        // ajax click action
        this.initAjaxAction();
        // ajax form submit
        this.initAjaxFormSubmit();
        // init ts-confirm link
        this.initConfirmLink();
        // init collection add (like) link
        this.initLikeLink();
        // click with disable for 15 secs.
        this.initClickProtect();
        // hide popover
        this.initHidePopover();
        // init google events
        this.initGaEvents();
        // activet order progress popover
        this.showOrderProgress();
        // activate animation on scrolling
        this.initAnimation();
    },

    /**
     *
     */
    initGaEvents: function () {
        $('body').on('click', '.ts-ga', function (e) {
            var elm = $(this),
                category = elm.data('categoryga') || false,
                action = elm.data('actionga') || false,
                label = elm.data('labelga') || '';
            if (!category || !action) {
                if (console) {
                    console.log(_t('site.common', 'No category and action for given ts-ga link.'));
                }
                return;
            }
            var params = {
                hitType: 'event',
                eventCategory: category,
                eventAction: action,
                eventLabel: label
            };
            if ((typeof (ga) != 'undefined') && ga) {
                ga('send', params);
            }
        });
    },

    /**
     * init ajax modal click event
     */
    initAjaxModal: function () {
        var me = this;
        var modalCallback = false;
        $('body').on('click', '.ts-ajax-modal', function (e) {
            e.preventDefault();
            var elm = $(this),
                target = elm.attr('data-target') || 'ajaxmodal1',
                addModalDialogClass = elm.attr('data-modal-dialog-class'),
                ajaxUrl = elm.attr('href') || elm.attr('value') || elm.attr('data-url'),
                modalTitle = elm.attr('title') || elm.attr('data-original-title') || '',
                modalCallback = elm.attr('callback') || false,
                dataMethod = elm.attr('data-method') || 'get';
            if ($(target).length === 0) {
                $(me.generateModal(target, modalTitle, undefined, addModalDialogClass)).insertAfter('footer');
            }
            var targetId = target == 'ajaxmodal1' ? '#' + target : target;
            $(targetId).modal('show').find('.modal-body').html("<div class='preloader'>" + _t("site", "Loading...") + "</div>");
            $.ajax({
                url: ajaxUrl,
                type: dataMethod,
                success: function (response) {
                    $(targetId).modal('show').find('.modal-body').html(response);
                },
                error: function (xhr) {
                    if (xhr.status == 401 || xhr.status == 403) {
                        $(targetId).modal('hide').on('hidden.bs.modal', function () {
                            $(this).remove();
                        });
                        TS.Visitor.loginForm(location.pathname);
                        return;
                    }
                    $(targetId).modal('show').find('.modal-body').html(xhr.responseText);
                }
            });
            return false;
        });
    },
    initClickProtect: function () {
        $(document.body).on("click", ".js-clickProtect", function () {
            setTimeout(function () {
                $('.js-clickProtect').prop('disabled', true);
            }, 100);
            setTimeout(function () {
                $('.js-clickProtect').prop('disabled', false);
            }, 2000); // 15 seconds
        });
    },


    initLikeLink: function () {
        $('.ts-add-collection').click(function (a, b) {
            var modelId = $(a.currentTarget).data('id');
            var callback = $(a.currentTarget).data('callback');
            $.ajax({
                url: '/profile/collection/bind-default',
                type: "POST",
                data: {'model3d_id': modelId},
                dataType: 'json',
                error: function (result) {
                    result = result.responseJSON;
                },
                success: function (result) {
                    if (result.success === false) {
                        new TS.Notify({type: 'error', text: result.message});
                    } else if (callback) {
                        callback(result);
                    } else {
                        new TS.Notify({type: 'success', text: result.message || _t('site.common', 'Done'), automaticClose: true});
                    }
                }
            });
        });
    },
    /**
     * show confirm dialog before
     *
     * @returns {undefined}
     */
    initConfirmLink: function () {
        yii.confirm = function (message, ok, cancel) {
            TS.confirm(message, function (btn) {
                if (btn === 'ok') {
                    !ok || ok();
                } else {
                    !cancel || cancel();
                }
            }, {confirm: 'Yes', dismiss: 'No'});
        };

        $('body').on('click', '.ts-confirm-btn', function (e) {
            var el = this;
            var msg = $(el).data('message') || _t('site.common', 'Are you sure?');
            e.preventDefault();
            TS.confirm(msg, function (btn) {
                if (btn === 'ok') {
                    $(el).parents('form:first').submit();
                } else {

                }
            }, {confirm: _t('site.common', 'Yes'), dismiss: _t('site.common', 'No')});
        });

    },
    initAjaxFormSubmit: function () {
        $('body').on('click', '.ts-ajax-submit', function (e) {

            e.preventDefault();
            var elm = $(this);
            var $form = $(this).parents('form').first();
            var $modal = $form.parents('.modal').first();
            var isModalMode = $modal.length > 0;
            elm.attr('disabled', 'disabled');
            var modalId = ''; //
            if (isModalMode) {
                modalId = '#' + ($modal.attr('id'));
            } else {
            }
            var postData = $form.serialize();
            if (elm.attr('data-action')) {
                postData.action = elm.attr('data-action');
            }
            var callbackjs = (elm.attr('jsCallback')) ? eval(elm.attr('jsCallback')) : '';
            $.ajax({
                url: $form.attr('action'),
                type: "POST",
                data: postData,
                dataType: 'json',
                error: function (result) {
                    result = result.responseJSON;
                    var modalContainer = modalBody = elm.parent();
                    if (isModalMode) {
                        modalContainer = $(modalId);
                        modalBody = modalContainer.find('.modal-body');
                    }
                    var msgContainer = modalContainer.find('.form-message');
                    msgContainer.removeClass('alert-success');
                    msgContainer.addClass('alert-warning');
                    if (result) {
                        msgContainer.html(result.message || 'Done');
                        msgContainer.fadeOut().fadeIn();
                    }
                    elm.removeAttr('disabled');
                },
                success: function (result) {
                    var modalContainer = modalBody = elm.parent();
                    if (isModalMode) {
                        modalContainer = $(modalId);
                        modalBody = modalContainer.find('.modal-body');
                    }
                    elm.removeAttr('disabled');
                    if (result.message === 'reload') {
                        document.location.reload();
                        return;
                    }
                    if (result.callbackjs) {
                        eval(result.callbackjs);
                        return;
                    }
                    if (result.redir) {
                        document.location.href = result.redir;
                        return;
                    }
                    var msgContainer = modalContainer.find('.form-message');
                    if (msgContainer.length === 0) {
                        modalBody.prepend('<div class="alert form-message"></div>');
                        msgContainer = modalContainer.find('.form-message');
                    }

                    if (result.success === true) {
                        if (typeof callbackjs === 'function') {
                            msgContainer.remove();
                            var callBackResult = callbackjs(elm, result, modalId);
                            if (callBackResult !== true) {
                                return;
                            }
                        }
                        msgContainer.removeClass('alert-warning');
                        msgContainer.addClass('alert-success');
                        msgContainer.html(result.message || 'Done');
                        $form[0].reset();
                        setTimeout(function () {
                            $(modalId).modal('hide');
                        }, 300);
                        $modal.trigger('modalAjaxSuccess', result);
                    } else {
                        msgContainer.removeClass('alert-success');
                        msgContainer.addClass('alert-warning');
                        msgContainer.html(result.message || 'Done');
                    }
                    msgContainer.fadeOut().fadeIn();
                }
            });
            return false;
        });
    },
    /**
     * generate html markup for modal window if not exists
     *
     * @param {String} target
     * @param {String} header
     * @param {String} [buttons]
     * @param {String} addModalDialogClass
     * @returns {String}
     */
    generateModal: function (target, header, buttons, addModalDialogClass) {
        header = header || 'Add';
        addModalDialogClass = (typeof addModalDialogClass === "string") ? addModalDialogClass : 'modal-lg';
        target = target.replace('#', '');
        if (buttons) {
            buttons = '<div class="modal-footer">' + buttons + '</div>';
        } else {
            buttons = '';
        }
        var closex = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        var html =
            '<div id="' + target + '" class="fade modal" role="dialog">\n\
                <div class="modal-dialog ' + addModalDialogClass + '">\n\
                    <div class="modal-content">\n\
                        <div class="modal-header">\n\
                            ' + closex + '\n\
                            <h4>' + header + '</h4>\n\
                        </div>\n\
                        <div class="modal-body"></div>\n\
                        ' + buttons + '\
                    </div>\n\
                </div>\n\
            </div>';
        return html;
    },

    /**
     * TODO: callback params not released yet
     *
     * @param modalName
     * @param modalTitle
     * @param href
     * @param data
     * @param dataMethod
     * @param callbackSuccess
     * @param callbackFailed
     * @returns {boolean}
     */
    createAjaxModal: function (modalName, modalTitle, href, data, dataMethod, callbackSuccess, callbackFailed) {
        var target = 'ajaxmodal1',
            ajaxUrl = href;
        if ($(target).length === 0) {
            $(this.generateModal(target, modalTitle)).insertAfter('footer');
        }
        var targetId = '#' + target;
        $(targetId).modal('show').find('.modal-body').html("<div class='preloader'>" + _t("site", "Loading...") + "</div>");
        $.ajax({
            url: ajaxUrl,
            data: data,
            type: dataMethod,
            success: function (response) {
                $(targetId).modal('show').find('.modal-body').html(response);
            },
            error: function (xhr) {
                if (xhr.status == 401 || xhr.status == 403) {
                    $(targetId).modal('hide').on('hidden.bs.modal', function () {
                        $(this).remove();
                    });
                    TS.Visitor.loginForm(location.pathname);
                    return;
                }
                $(targetId).modal('show').find('.modal-body').html(xhr.responseText);
            }
        });
        return false;
    },

    filterFormData: function (formData, filterValue) {
        var data = {};
        for (key in formData) {
            parameter = formData[key];
            if (parameter.name.substr(0, filterValue.length) != filterValue) {
                data[parameter.name] = parameter.value
            }
        }
        return data;
    },

    /**
     * to like or unlike
     *
     * @param {type} obj
     * @returns {undefined}
     */
    toLike: function (obj) {
        var id = obj.data('id');
        var type = obj.data('type');
        var action = obj.data('action');
        $.ajax({
            url: action,
            type: "POST",
            data: {'object_id': id, 'object_type': type},
            dataType: 'json',
            error: function (result) {
                result = result.responseJSON;
            },
            success: function (result) {
                if (result.success === true) {
                    obj.data('action', result.action);
                    obj.data('action', result.action);
                    if (result.liked) {
                        obj.find('span.glyphicon').removeClass('glyphicon-heart-empty').addClass('glyphicon-heart');
                    } else {
                        obj.find('span.glyphicon').removeClass('glyphicon-heart').addClass('glyphicon-heart-empty');
                    }
                    obj.find('span.liketxt').html(result.message);
                } else {

                }

            }
        });
    },

    acceptCookie: function () {
        $.ajax({
            url: '/site/accept-cookie',
            type: "GET",
        });
        $('.cookie-modal').hide();
        return false;
    },

    /**
     *
     * @param {string} messageTax
     * @returns {boolean}
     */
    showNeedTaxInfo: function (messageTax) {
        var target = '#model3dpublished';
        var button = '<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>';
        $(TS.generateModal(target, 'Information', button)).insertAfter('footer');
        $(target).find('.modal-body').addClass('alert-warning');
        $(target).modal('show').find('.modal-body').html(messageTax);
    },

    initHidePopover: function () {
        $('body').on('click', function (e) {
            $('[data-toggle="popover"]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        /*
        $(document).on('mouseleave', '.popover',function(e){
            $('#sharelink').popover('hide');
            $('#shareembed').popover('hide');
        });*/
    },

    showOrderProgress: function () {
        $('.order__status').on('click', function (e) {
            $(this).toggleClass("open");
        });
    },

    formRestoreSessionUrl: function (url) {
        var returnUrl = new URI(url);
        returnUrl.addQuery('restore-session', 1);
        if (typeof (siteParams.config.restoreSessionSalt) != 'undefined') {
            returnUrl.query({'restore-session-salt': siteParams.config.restoreSessionSalt});
        }
        return returnUrl.toString();
    },

    checkGlobalSession: function (onSuccess) {
        var self = this;
        if (siteParams.config.isGlobalSession) {
            onSuccess();
            return;
        }
        var preloadImage = new Image();
        preloadImage.src = self.formRestoreSessionUrl('/ping-global-session');
        preloadImage.onload = function (event) {
            onSuccess();
        };
        preloadImage.onerror = function (event) {
            onSuccess();
        };
    },
};

$(document).ready(function () {
    TS.init();
    TS.initEventListeners();

    jQuery('.jspopup').click(function (a, b, c) {
        var link = a.currentTarget.href;
        var newWin = window.open(link, " ", "width=880,height=740,resizable=yes,scrollbars=yes,status=yes");
        newWin.focus();
        return false;
    });
});

