"use strict";

/**
 * Service for working witch currency
 */
app.factory('$model3dParseProgress', ['$flushMessageManager', function ($flushMessageManager) {

    var $model3dParseProgress = {};

    $model3dParseProgress.processedPartsInfo = {};

    $model3dParseProgress.currentProgress = 1;

    /**
     * if  -1  is not setted
     * @type {number}
     */
    $model3dParseProgress.totalFiles = -1;

    $model3dParseProgress.oneFileMaxTime = 60;

    $model3dParseProgress.startTimestamp = new Date().getTime() / 1000;

    $model3dParseProgress.isGoodPartsExists = false;

    $model3dParseProgress.processFinished = false;

    $model3dParseProgress.setTotalFilesCount = function (totalFilesCount) {
        if ($model3dParseProgress.totalFiles == -1) {
            $model3dParseProgress.totalFiles = totalFilesCount;
            window.setInterval($model3dParseProgress.backgroudProgress, 500);
        }
    };


    $model3dParseProgress.goToUploadPage = function () {
        window.history.back();
    };

    $model3dParseProgress.showOpenViewPage = function () {
        $('#loadComment').html(_t('site.model3d', 'Loading...'))
    };

    $model3dParseProgress.showBackToUpload = function () {
        $('#loadComment').html(_t('site.model3d', 'Invalid files format.'))
    };

    $model3dParseProgress.showSuccessParsed = function (fileName) {
        $('#loadComment').html(_t('site.model3d', 'Parsed file: ') + fileName)
    };

    $model3dParseProgress.showFailedParsed = function (fileName) {
        $('#loadComment').html(fileName + ' ' + _t('site.model3d', 'file format not supported.'))
    };

    $model3dParseProgress.showComment = function (text) {
        $('#loadComment').html(_t('site.model3d', text))
    };

    $model3dParseProgress.refreshProgressBar = function (currentProcess) {
        if ($model3dParseProgress.currentProgress < currentProcess) {
            $model3dParseProgress.currentProgress = currentProcess;
        }
        if ($model3dParseProgress.currentProgress > 100) {
            $model3dParseProgress.currentProgress = 100;
        }
        $('.line-anim').width($model3dParseProgress.currentProgress + '%');
    };

    $model3dParseProgress.formProgressBar = function (currentFilesProcessedCount) {

        var currentProgress = 0;
        if (currentFilesProcessedCount > 0) {
            currentProgress = ($model3dParseProgress.totalFiles / currentFilesProcessedCount * 100);
        }
        if (currentProgress > 100) {
            currentProgress = 90;
        }
        $model3dParseProgress.refreshProgressBar(currentProgress);
    };

    $model3dParseProgress.showProgress = function (model3dParts) {
        var partInfo;
        var fileId;
        for (fileId in model3dParts) {
            if ($model3dParseProgress.processedPartsInfo[fileId]) {
                // already processed
            } else {
                partInfo = model3dParts[fileId];
                if (partInfo.parser.isSuccess) {
                    $model3dParseProgress.showSuccessParsed(partInfo.title);
                    $model3dParseProgress.processedPartsInfo[fileId] = partInfo;
                    $model3dParseProgress.isGoodPartsExists = true;
                }
                if (partInfo.parser.isError) {
                    $model3dParseProgress.showFailedParsed(partInfo.title);
                    $flushMessageManager.flush('File ' + partInfo.title + ' has incorrect format.', 'error', false);
                    $model3dParseProgress.processedPartsInfo[fileId] = partInfo;
                }
            }
        }
        var processedCount = Object.keys($model3dParseProgress.processedPartsInfo).length;
        $model3dParseProgress.formProgressBar(processedCount);
        if (processedCount == $model3dParseProgress.totalFiles) {
            $model3dParseProgress.processFinished = true;
            if ($model3dParseProgress.isGoodPartsExists) {
                window.setTimeout($model3dParseProgress.showOpenViewPage, 1000);
            } else {
                $model3dParseProgress.showBackToUpload();
            }
        }
    };

    $model3dParseProgress.backgroudProgress = function () {
        var percent;
        if ($model3dParseProgress.currentProgress >= 90) {
            percent = $model3dParseProgress.currentProgress + 1;
        } else {
            var totalMaxTime = $model3dParseProgress.oneFileMaxTime * $model3dParseProgress.totalFiles;
            var timeLeft = (new Date().getTime() / 1000 - $model3dParseProgress.startTimestamp);
            percent = parseInt(timeLeft / totalMaxTime * 100);
        }
        //console.log('Percent: ' + percent);
        $model3dParseProgress.refreshProgressBar(percent);
    };


    return $model3dParseProgress;
}]);

