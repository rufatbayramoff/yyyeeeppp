"use strict";

/**
 * Service for working witch currency
 */
app.factory('$currency', function($$currencyData)
{
    var $currency = {};

    /**
     * @type {*[]}
     */
    $currency.list = $$currencyData;

    /**
     * Return currency by iso code
     * @param iso
     */
    $currency.getByIso = function(iso)
    {
        return _.find(this.list, function(currency){return currency.currency_iso == iso; });
    };

    return $currency;
});