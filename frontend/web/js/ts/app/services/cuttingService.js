"use strict";

app.factory('$cuttingService', function ($http, $notify,$notifyHttpErrors, $router, $q) {
    var service = {};

    service.sendToModeration = function (machine) {
        var defer = $q.defer();
        $http.post($router.sendToModeration(), {
            'cattingMachineId': machine.id,
        }).then(function (data) {
            var answer = data['data'];
            if (answer['success']) {
                $notify.success(_t('mybusiness.service', 'Service is sent to moderation'));
                defer.resolve(answer);
            }
        }, function error(response) {
            $notifyHttpErrors(response);
            defer.reject();
        })
        return defer.promise;
    }

    return service;
});