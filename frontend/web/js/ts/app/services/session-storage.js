"use strict";

/**
 * Session storage service.
 * If browser not support session storage, this service will not throw any exceptions.
 * On store to storage we miss information about object prototype, so it return plain object.
 * Some parts of code from https://github.com/gsklee/ngStorage/blob/master/ngStorage.js
 */
app.factory('$sessionStorage',['$window', function($window)
{
    /**
     * Serializer
     * @type {{serialize: function, unserialize: function}}
     */
    var serializer = {
        serialize : angular.toJson,
        unserialize : angular.fromJson
    };

    /**
     * Test sesson storage
     * @returns {boolean}
     */
    function isSessionStorageSupported() {
        var supported;
        try {
            supported = !!$window.sessionStorage;
        }
        catch(err) {
            supported = false;
        }

        if(supported) {
            var key = '__' + Math.round(Math.random() * 1e7);
            try {
                $window.sessionStorage.setItem(key, key);
                $window.sessionStorage.removeItem(key);
            }
            catch(err) {
                supported = false;
            }
        }
        return supported;
    }

    /**
     * Is session storage supported
     * @type {boolean}
     */
    var isSupported = isSessionStorageSupported();


    var $sessionStorage = {};

    /**
     * Save data to session storage
     * @param {String} key
     * @param {*} value
     */
    $sessionStorage.set = function (key, value)
    {
        if (!isSupported || value === undefined) {
            return;
        }

        $window.sessionStorage.setItem(key, serializer.serialize(value));
    };

    /**
     * Load data from session storage
     * @param {String} key
     * @param {*} [defaultValue]
     */
    $sessionStorage.get = function (key, defaultValue)
    {
        if (!isSupported) {
            return undefined;
        }

        var data = $window.sessionStorage.getItem(key);

        if (data === "undefined" || data === undefined ){
            return defaultValue;
        }

        return serializer.unserialize(data);
    };

    /**
     * Remove key
     * @param {String} key
     */
    $sessionStorage.remove = function (key)
    {
        if (!isSupported) {
            return;
        }

        $window.sessionStorage.removeItem(key);
    };

    /**
     * Remove all session data
     */
    $sessionStorage.clear = function ()
    {
        if (!isSupported) {
            return;
        }

        $window.sessionStorage.clear();
    };

    /**
     * Is browser support session storage
     * @returns {boolean}
     */
    $sessionStorage.isSupported = function ()
    {
        return isSupported;
    };

    return $sessionStorage;
}]);

