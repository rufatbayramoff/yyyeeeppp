"use strict";

/**
 * Json editor helper
 */
app.factory('$jsonEditorHelper', function () {

    var $jsonEditorHelper = {};

    $jsonEditorHelper.formDefaultValues = function (value, schema) {
        //schema
        //return text.toString() .replace(/\s+/g, '');
        return {};
    };

    return $jsonEditorHelper;
});