"use strict";

/**
 * Service for working witch measure
 */
app.factory('$measure', function ($measureType, $measureSystem) {
    var $measure = {
        MEASURE_CM: {
            measure: 'cm',
            title: _t('site.common', 'cm'),
            type: $measureType.MEASURE_TYPE_LENGTH,
            system: $measureSystem.MEASURE_SYSTEM_METRIC,
            convertCoefficient: 0.01
        },
        MEASURE_MM: {
            measure: 'mm',
            title: _t('site.common', 'mm'),
            type: $measureType.MEASURE_TYPE_LENGTH,
            system: $measureSystem.MEASURE_SYSTEM_METRIC,
            convertCoefficient: 0.001
        },
        MEASURE_IN: {
            measure: 'in',
            title: _t('site.common', 'in'),
            type: $measureType.MEASURE_TYPE_LENGTH,
            system: $measureSystem.MEASURE_SYSTEM_AMERICAN,
            convertCoefficient: 0.0254
        },
        MEASURE_GR: {
            measure: 'gr',
            title: _t('site.common', 'g'),
            type: $measureType.MEASURE_TYPE_WEIGHT,
            system: $measureSystem.MEASURE_SYSTEM_METRIC,
            convertCoefficient: 0.001
        },
        MEASURE_OZ: {
            measure: 'oz',
            title: _t('site.common', 'oz'),
            type: $measureType.MEASURE_TYPE_WEIGHT,
            system: $measureSystem.MEASURE_SYSTEM_AMERICAN,
            convertCoefficient: 0.0283495
        },
        MEASURE_ML: {
            measure: 'ml',
            title: _t('site.common', 'ml'),
            type: $measureType.MEASURE_TYPE_WEIGHT,
            system: $measureSystem.MEASURE_SYSTEM_METRIC,
            convertCoefficient: 0.001
        }
    };

    $measure.list = $measure;

    /**
     * Return measure by weight metric
     * @param weightMetric
     * @returns {*}
     */
    $measure.getByMetric = function (weightMetric) {
        return _.find(this.list, function (measure) {
            return measure.measure == weightMetric;
        });
    };
    return $measure;
});

/**
 * Service for working witch measure
 */
app.factory('$measureType', function ($injector) {
    var $measureType = {
        MEASURE_TYPE_WEIGHT: 'weight',
        MEASURE_TYPE_LENGTH: 'length',
        MEASURE_TYPE_VOLUME: 'volume'
    };

    $measureType.getMeasureSystemTypeTitle = function(measure, type) {
        var typeMetric = measure.system.defaultTypes[type];
        return $injector.get('$measure').getByMetric(typeMetric).title;
    };
    return $measureType;
});


/**
 * Service for working witch measure
 */
app.factory('$measureSystem', function () {
    var $measureSystem = {
        MEASURE_SYSTEM_METRIC: {
            system: 'cm',
            defaultTypes: {
                weight: 'gr',
                length: 'cm',
                volume: 'ml'
            }
        },
        MEASURE_SYSTEM_AMERICAN: {
            system: 'in',
            defaultTypes: {
                weight: 'oz',
                length: 'in',
                volume: 'ml'
            }
        }
    };
    return $measureSystem;
});


/**
 * Service for working witch measure
 */
app.factory('$measureConverter', function ($measure, $mathUtils)
{
    var $measureConvert = {};


    /**
     *
     * @param fromMeasure
     * @param toMeasure
     * @param value
     * @return {number}
     */
    $measureConvert.convert = function (fromMeasure, toMeasure, value)
    {
        if (fromMeasure.type != toMeasure.type) {
            return 0;
        }
        return fromMeasure.convertCoefficient * value / toMeasure.convertCoefficient;
    };

    /**
     *
     * @param fromMeasureMetric
     * @param toMeasureMetric
     * @param value
     * @return {number}
     */
    $measureConvert.convertByMetric = function (fromMeasureMetric, toMeasureMetric, value)
    {
        var fromMeasure = $measure.getByMetric(fromMeasureMetric);
        var toMeasure = $measure.getByMetric(toMeasureMetric);

        if (fromMeasure.type != toMeasure.type) {
            return 0;
        }
        return fromMeasure.convertCoefficient * value / toMeasure.convertCoefficient;
    };

    /**
     *
     * @param {Size} size
     * @param toMeasure
     * @param {boolean} [round]
     */
    $measureConvert.convertSize = function (size, toMeasure, round)
    {
        if (size.measure == toMeasure.measure && !round) {
            return size;
        }

        if (size.measure == toMeasure.measure && round) {
            return this.roundSize(angular.copy(size));
        }

        var fromMeasure = $measure.getByMetric(size.measure);
        var newSize = new Size();
        newSize.measure = toMeasure.measure;
        newSize.height = this.convert(fromMeasure, toMeasure, size.height);
        newSize.length = this.convert(fromMeasure, toMeasure, size.length);
        newSize.width = this.convert(fromMeasure, toMeasure, size.width);

        if (round) {
           this.roundSize(newSize);
        }
        return newSize;
    };

    /**
     * Round size
     * @param newSize
     */
    $measureConvert.roundSize = function (newSize)
    {
        newSize.height = $mathUtils.round(newSize.height);
        newSize.length = $mathUtils.round(newSize.length);
        newSize.width = $mathUtils.round(newSize.width);
        return newSize;
    };

    return $measureConvert;
});