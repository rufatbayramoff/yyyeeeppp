"use strict";

/**
 * Camelizer service.
 */
app.factory('$camelizer', function () {

    var $camelizer = {};

    $camelizer.camelize = function (text) {
        return text.toString()
            .replace(/\s+/g, ' ')           // Replace spaces with single space
            .replace(/[^\w\-]+/g, ' ')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '')             // Trim - from end of text
            .replace(/(?:^\w|[A-Z]|\b\w)/g, function (letter, index) {
                return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
            })
            .replace(/\s+/g, '');
    };

    return $camelizer;
});