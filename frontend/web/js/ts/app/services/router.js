"use strict";

/**
 * Notifi service.
 * Now it just wrapper for TS.Notify functions
 */
app.provider('$router', function () {


    var $router = {
        posUid: ''
    };

    function initPosUid() {
        var posUid = '';
        var href = window.location.href;
        var p1 = href.indexOf('posUid=');
        var p2 = href.indexOf('&', p1 + 1);
        if (p1 > 1) {
            if (p2 === -1) {
                p2 = 99999999;
            }
            posUid += href.substr(p1, p2 - p1);
        }
        $router.posUid = posUid;
    }

    initPosUid();

    /**
     * Set pos uid
     * @param {String} newPosUid
     */
    $router.setPosUid = function setPosUid(newPosUid) {
        this.posUid = newPosUid;
    };

    /**
     * Set url
     * @param {String} url
     */
    $router.setUrl = function setUrl(url) {
        window.location.href = url;
    };

    $router.getMybusinessServices = function () {
        return '/mybusiness/services';
    };

    /**
     * Go to printer list
     * @param {bool} [checkTaxes]
     */
    $router.toPsPrintersList = function (checkTaxes) {
        checkTaxes = checkTaxes === undefined ? true : checkTaxes;
        $router.setUrl('/mybusiness/services' + (checkTaxes ? '?taxcheck=1' : ''));
    };


    /**
     * Go to add psPrinter
     * @param {bool|boolean} [formCreatePs]
     */
    $router.toAddPrinter = function (formCreatePs) {
        $router.setUrl('/mybusiness/services/add-printer' + (formCreatePs ? '?fromCreatePs=1' : ''));
    };

    /**
     * Go to add psPrinter
     * @param {bool|boolean} [formCreatePs]
     */
    $router.toAddCnc = function (formCreatePs) {
        $router.setUrl('/mybusiness/services/add-cnc' + (formCreatePs ? '?fromCreatePs=1' : ''));
    };

    /**
     * To list with print requests on ps
     * @param {String} [status]
     */
    $router.toPsPrintRequests = function (status) {
        $router.setUrl('/workbench/service-orders/' + (status || 'new'));
    };

    $router.psRefundRequest = function () {
        return '/store/payment/refund-request'
    };

    $router.viewPrintAttemp = function (attempId) {
        return '/workbench/service-order/print?attempId=' + attempId
    };

    $router.toPsQuotesRequests = function () {
        $router.setUrl('/workbench/preorder/quotes');
    };


    /**
     * To user profile
     */
    $router.toUserProfile = function () {
        $router.setUrl('/profile');
    };

    /**
     * Reload current page
     */
    $router.reload = function () {
        window.location.reload();
    };

    /**
     * Go to url
     */
    $router.to = function (url) {
        $router.setUrl(url);
    };

    /**
     * Change url without reload
     * @param url
     */
    $router.changeUrl = function (url, title) {
        window.history.pushState({}, title, url);
    };

    /**
     * Create callback function with redirect to url
     * @param url
     */
    $router.toFn = function (url) {
        $router.setUrl(url);
    };

    /**
     * Retrun current location
     * @returns {string}
     */
    $router.getCurrentLocation = function () {
        return location.href;
    };

    /**
     * Set user geo location
     */
    $router.setGeoLocation = function () {
        return '/geo/set-location';
    };

    /**
     * Add/replace hash to url
     * @param hash
     */
    $router.setHash = function (hash) {
        window.location.hash = '#' + hash;
    };

    /**
     * To user profile
     */
    $router.toOrderDelivery = function () {
        $router.setUrl('/store/delivery');
    };

    /**
     * To preorder fee
     */
    $router.getPreorderFeeCalc = function () {
        return '/workbench/preorder/fee-calc';
    };

    $router.getCreateQuoteByPs = function () {
        return '/workbench/preorder/create-by-ps';
    };

    $router.toCreateQuoteByPs = function () {
        $router.setUrl(this.getCreateQuoteByPs());
    };

    $router.getCreateRequestForQuote = function () {
        return '/preorder/quote/request-quote';
    };

    $router.getCreateRequestForQuoteAjax = function () {
        return '/preorder/quote/request-quote-create';
    };

    $router.getViewPreorderOrderPs = function (preorderId) {
        return '/workbench/preorder/edit-by-ps?preorderId=' + preorderId;
    };


    $router.getViewPreorderOrderCustomer = function (preorderId) {
        return '/workbench/preorder/view-customer?preorderId=' + preorderId;
    };

    $router.getPreorderQuotes = function () {
        return '/workbench/preorder/quotes';
    };

    $router.getQuoteClientsAutocompleteSource = function () {
        return '/preorder/quote/select-clients';
    };

    /**
     * To my orders
     */
    $router.toMyOrders = function () {
        $router.setUrl('/workbench/orders');
    };

    $router.toMyPreorders = function () {
        $router.setUrl('/workbench/orders/quotes');
    };

    $router.toPrintModel3dIndex = function () {
        $router.setUrl($router.getPrintModel3dIndex());
    };

    $router.getPrintModel3dIsWidget = function () {
        return (window.location.href.indexOf('widget') === -1) ? '' : '/widget';
    };

    $router.getPosUid = function (addQuestionSymbol) {
        if (!this.posUid) {
            return '';
        }
        if (addQuestionSymbol > 0) {
            return '?posUid=' + this.posUid;
        }
        return 'posUid=' + this.posUid;
    };

    $router.getPrintModel3dIndex = function () {
        return '/my/print-model3d' + $router.getPrintModel3dIsWidget() + '?posUid';
    };

    $router.getPrintModel3dPrinters = function () {
        return '/my/print-model3d/printers' + $router.getPrintModel3dIsWidget() + '?posUid';
    };

    $router.getPrintModel3dDelivery = function () {
        return '/my/print-model3d/delivery' + $router.getPrintModel3dIsWidget() + '?posUid';
    };

    $router.getPrintModel3dCheckout = function () {
        return '/my/print-model3d/checkout' + $router.getPrintModel3dIsWidget() + '?posUid';
    };

    $router.getPrintModelApplyPromoCode = function () {
        return '/my/print-model3d-ajax/promo-code' + $router.getPrintModel3dIsWidget() + $router.getPosUid(1);
    };

    $router.getPrintModel3dPayOrder = function () {
        return '/my/print-model3d-ajax/pay-order' + $router.getPrintModel3dIsWidget() + $router.getPosUid(1);
    };

    $router.getBillingDetailsPrint = function () {
        return '/my/print-model3d-ajax/billing-details-print' + $router.getPosUid(1);
    };

    $router.getCreateModel3d = function () {
        return '/my/print-model3d-ajax/create-model3d' + $router.getPosUid(1);
    };

    $router.getPrintModel3dUpload = function () {
        return '/my/print-model3d-ajax/upload-file' + $router.getPosUid(1);
    };

    $router.getPrintModel3dCancel = function () {
        return '/my/print-model3d-ajax/cancel-file' + $router.getPosUid(1);
    };

    $router.getPrintModel3dQty = function () {
        return '/my/print-model3d-ajax/qty-change';
    };

    $router.getPrintModel3dRestore = function () {
        return '/my/print-model3d-ajax/restore-file' + $router.getPosUid(1);
    };

    $router.getPrintModel3dScale = function () {
        return '/my/print-model3d-ajax/change-scale';
    };

    $router.getPrintModel3dDuplicate = function () {
        return '/my/print-model3d-ajax/duplicate-file' + $router.getPosUid(1);
    };

    $router.getPrintModel3dPartsSize = function () {
        return '/my/print-model3d-ajax/change-parts-size';
    };

    $router.getPrintModel3dPrintersList = function (model3dUid, currentPage, pageSize, currency, sortBy, isWidget, psIdOnly, psPrinterIdOnly) {
        return '/my/print-model3d-ajax/get-printers-list?model3dUid=' + model3dUid + '&currency=' + currency + '&page=' + currentPage + '&pageSize=' + pageSize + '&sortBy=' + sortBy + (isWidget ? '&widget=1' : '') + (psIdOnly ? '&manualSettedPsId=' + psIdOnly : '') + (psPrinterIdOnly ? '&manualSettedPsPrinterId=' + psPrinterIdOnly : '') + '&' + $router.getPosUid(0);
    };

    $router.getPrintModel3dSaveCart = function () {
        return '/my/print-model3d-ajax/save-cart' + $router.getPosUid(1);
    };

    $router.getGetDeliveryInfo = function () {
        return '/my/print-model3d-ajax/get-delivery-info' + $router.getPosUid(1);
    };

    $router.getGetDeliveryRate = function () {
        return '/my/print-model3d-ajax/get-delivery-rate' + $router.getPosUid(1);
    };

    $router.getCheckAndSaveDeliveryInfo = function () {
        return '/my/print-model3d-ajax/save-delivery-info' + $router.getPosUid(1);
    };

    $router.getMaterialsGuide = function (type, allowedList) {
        if (type && allowedList) {
            return '/my/print-model3d-ajax/materials-guide?type=' + type + '&materials=' + allowedList.join(',');
        }
        if (type) {
            return '/my/print-model3d-ajax/materials-guide?type=' + type;
        }
        return '/my/print-model3d-ajax/materials-guide?materials=' + allowedList.join(',');
    };

    /**
     * Product
     * @returns {string}
     */
    $router.getProductSaveCart = function () {
        return '/product/ajax/buy-cart-save';
    };

    $router.getProductBuyDelivery = function () {
        return '/product/buy-delivery';
    };

    $router.getProductBuyPayment = function () {
        return '/product/buy-checkout';
    };

    $router.getProductSaveDeliveryInfo = function () {
        return '/product/ajax/save-delivery-info';
    };

    $router.getCurrentUserProductsAjax = function () {
        return '/product/ajax/get-my-products-list'
    };

    $router.getSearchClients = function () {
        return '/mybusiness/company/search-clients'
    };

    /**
     * Client Cutting
     */
    $router.getCuttingPackUpload = function () {
        return '/my/order-laser-cutting/upload-file?posUid';
    };

    $router.getCuttingPackParts = function () {
        return '/my/order-laser-cutting/select-parts?posUid';

    };
    $router.getCuttingOffers = function () {
        return '/my/order-laser-cutting/offers?posUid';
    };
    $router.getCuttingDelivery = function () {
        return '/my/order-laser-cutting/delivery?posUid';
    };
    $router.getCuttingCheckout = function () {
        return '/my/order-laser-cutting/checkout?posUid';
    };

    $router.getAjaxCuttingPayOrder = function () {
        return '/my/order-laser-cutting-ajax/pay-order' + $router.getPosUid(1);
    };

    $router.getAjaxCreateCuttingPack = function () {
        return '/my/order-laser-cutting-ajax/create' + $router.getPosUid(1);
    };
    $router.getAjaxCuttingPackUpload = function () {
        return '/my/order-laser-cutting-ajax/upload-file' + $router.getPosUid(1);
    };
    $router.getAjaxCuttingPackCancel = function () {
        return '/my/order-laser-cutting-ajax/cancel-file' + $router.getPosUid(1);
    };
    $router.getAjaxCuttingPackRestore = function () {
        return '/my/order-laser-cutting-ajax/restore-file' + $router.getPosUid(1);
    };
    $router.getAjaxCuttingChangeFileQty = function () {
        return '/my/order-laser-cutting-ajax/qty-change' + $router.getPosUid(1);
    };
    $router.getAjaxPingCuttingPack = function () {
        return '/my/order-laser-cutting-ajax/ping' + $router.getPosUid(1);
    };
    $router.getAjaxCuttingSvgDownload = function (cuttingPageUuid, forceOriginal) {
        return '/my/order-laser-cutting-ajax/svg-download?packPageUuid='
            + cuttingPageUuid
            + (forceOriginal ? '&forceOriginal=1' : '')
            + '&' + $router.getPosUid();
    };
    $router.getAjaxCuttingSaveParts = function () {
        return '/my/order-laser-cutting-ajax/save-parts' + $router.getPosUid(1);
    };
    $router.getAjaxCuttingOffers = function (cuttingUuid, currentPage, pageSize, sortBy, isWidget, psIdOnly) {
        if (psIdOnly) {
            return '/my/order-laser-cutting-ajax/offers?manualSettedPsId=' + psIdOnly + "&" + $router.getPosUid(0);
        }
        return '/my/order-laser-cutting-ajax/offers' + $router.getPosUid(1);
    };

    $router.getAjaxCuttingSelectOffer = function () {
        return '/my/order-laser-cutting-ajax/select-offer' + $router.getPosUid(1);
    };

    $router.getAjaxCuttingDeliveryInfo = function () {
        return '/my/order-laser-cutting-ajax/delivery' + $router.getPosUid(1);
    };

    $router.getAjaxCuttingCheckAndSaveDeliveryInfo = function () {
        return '/my/order-laser-cutting-ajax/save-delivery-info' + $router.getPosUid(1);
    };

    $router.getAjaxCuttingBillingDetailsPrint = function () {
        return '/my/order-laser-cutting-ajax/billing-details-print' + $router.getPosUid(1);
    };

    /**
     * Mybusiness Cutting
     */

    $router.editCuttingMachine = function (id) {
        return '/mybusiness/cutting/cutting/edit?id=' + id;
    };

    $router.editCuttingWorkpieceMaterials = function (id) {
        return '/mybusiness/cutting/cutting/workpiece-materials?id=' + id;
    };

    $router.editCuttingProcessing = function (id) {
        return '/mybusiness/cutting/cutting/processing-price?id=' + id;
    };

    $router.editCuttingDelivery = function (id) {
        return '/mybusiness/cutting/cutting/delivery-details?id=' + id;
    };

    $router.saveAjaxCuttingMachine = function () {
        return '/mybusiness/cutting/cutting-ajax/save-general';
    };

    $router.sendToModeration = function () {
        return '/mybusiness/cutting/cutting-ajax/send-to-moderation';
    };

    $router.saveAjaxWorkpieceMaterials = function () {
        return '/mybusiness/cutting/cutting-ajax/save-workpiece-materials';
    };

    $router.saveAjaxCuttingProcessing = function () {
        return '/mybusiness/cutting/cutting-ajax/save-processing';
    };

    $router.saveAjaxCuttingDelivery = function () {
        return '/mybusiness/cutting/cutting-ajax/save-delivery';
    };


    /**
     * Product Form
     */
    $router.getDynamicFieldCategories = function () {
        return '/mybusiness/products/product/update-categories';
    };

    $router.getBusinessProducts = function (type) {
        return '/mybusiness/products' + (type ? '?type=' + type : '');
    };

    $router.getProductEdit = function (uid) {
        return '/mybusiness/products/product/edit?uid=' + uid;
    };

    $router.getProductShotCategoryList = function (title) {
        return '/mybusiness/products/product/categories-short-list?title=' + title;
    };

    /**
     * Model3d Form
     */

    $router.getSaveModel3d = function (model3dId) {
        return '/my/model/edit/' + model3dId;
    };

    $router.getDeleteModel3d = function (model3dId) {
        return '/my/model/delete/' + model3dId;
    };

    /**
     * Product catalog search  form
     */
    $router.getProductsList = function () {
        return '/products';
    };

    /**
     * Product catalog search  form
     */
    $router.getProductsCatalogList = function () {
        return '/products/all';
    };


    /**
     * Thingiverse
     */

    $router.getThingiverseModel = function () {
        return '/thingprint/print/get-model3d';
    };

    $router.getThingiverseCharges = function () {
        return '/thingprint/print/get-charges';
    };

    /**
     * Images
     */
    $router.getImageRotate = function (fileUuid) {
        return '/image/rotate?fileUuid=' + fileUuid
    };

    /**
     * To order payment
     * @param {string} invoiceUuid
     */
    $router.toInvoicePayment = function (invoiceUuid) {
        $router.setUrl('/store/payment/pay-invoice/' + invoiceUuid);
    };

    /**
     * @returns {string}
     */
    $router.getInvoiceBillingDetails = function (invoiceUuid) {
        return '/store/payment/invoice-billing-details?invoiceUuid=' + invoiceUuid;
    };

    $router.getSaveReceiptBuyerAdditionDetails = function (invoiceUuid, receiptId) {
        return '/payment/receipt/save-buyer-details?invoiceUuid=' + invoiceUuid + '&receiptId=' + receiptId;
    };

    $router.getSaveReceiptComment = function (invoiceUuid, receiptId) {
        return '/payment/receipt/save-comment?invoiceUuid=' + invoiceUuid + '&receiptId=' + receiptId;
    };


    /**
     * Certification form
     */
    $router.getPsServiceTestSave = function (companyServiceId) {
        return '/mybusiness/edit-printer/test-save?company_service_id=' + companyServiceId;
    }


    $router.urlWithoutGetParams = function (url) {
        var p1 = url.indexOf('?');
        if (p1 > 0) {
            return url.substr(0, p1);
        }
        return url;
    };

    $router.windowCalculatorQuoteInSession = function (serviceUid) {
        return '/mybusiness/cs-window/quote/save-calc-quote-in-session?serviceUid=' + serviceUid;
    };

    $router.windowCalculatorSaveQuote = function (serviceUid) {
        return '/mybusiness/cs-window/quote/save-quote?serviceUid=' + serviceUid;
    };

    $router.windowQuoteStatusChange = function (quoteUid) {
        return '/mybusiness/cs-window/manage/quote-status-change?quoteUid=' + quoteUid;
    };

    $router.deleteModel = function (model3d) {
        return '/my/model/delete/' + model3d;
    };

    $router.deleteImage = function (imageId) {
        return '/my/model/delete-image?uid=MI:' + imageId;
    };

    $router.deletePart = function (partId) {
        return '/my/model/delete-part?uid=MP:' + partId;
    };

    $router.setCover = function (model3d) {
        return '/my/model/set-cover/' + model3d;
    };

    $router.createSupportTopicOrder = function (orderId) {
        return '/messages/create-support-topic?bindId=' + orderId + '&bindTo=order';
    };


    $router.printingCatalog = function (location, international, technology, usage, material, sort, text) {
        let url = '/3d-printing-services?';
        if (location) {
            url += 'location=' + location + '&';
        }
        if (international) {
            url += 'international=1&';
        }
        if (technology) {
            url += 'technology=' + technology + '&';
        }
        if (usage) {
            url += 'usage=' + usage + '&';
        }
        if (material) {
            url += 'material=' + material + '&';
        }
        if (sort) {
            url += 'sort=' + sort + '&';
        }
        if (text) {
            url += 'text=' + text + '&';
        }
        url = url.substr(0, url.length - 1);
        return url;
    };

    $router.wikiMachinesCatalog = function (category, technology, brand, material, search) {
        let url = '/machines/';
        if (category) {
            url += category + '/';
        }
        if (technology) {
            url += 'technology-' + technology + '--';
        }
        if (brand) {
            url += 'brand-' + brand.replace(/ /g, '+') + '--';
        }
        if (material) {
            url += 'material-' + material + '--';
        }
        if (url.slice(-2) === '--') {
            url = url.substr(0, url.length - 2);
        } else {
            url = url.substr(0, url.length - 1);
        }
        if (search) {
            url += '?search=' + search;
        }
        return url;
    };

    $router.$get = function () {
        return $router;
    };

    return $router;
});
