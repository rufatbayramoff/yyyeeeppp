"use strict";

/**
 * Service for working witch geo
 */
app.factory('$googleLocationApi', function (locationInputElementId, locationResultInputElId) {

    var $googleLocationApi = {
        locationInputElementId : locationInputElementId,
        locationResultInputElId : locationResultInputElId,
        updateObject : { obj : null, field: null}
    };
    var componentForm = {
        street_number: 'short_name', // building number
        route: 'long_name', // street name
        locality: 'short_name', // city name
        sublocality_level_1: "short_name",
        administrative_area_level_1: 'long_name', // state
        country: 'short_name', // country
        postal_code: 'short_name' // postal code
    };
    $googleLocationApi.filledForm = {};
    $googleLocationApi.autocomplete = null;
    $googleLocationApi.initAutocomplete = function () {
        $googleLocationApi.autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById($googleLocationApi.locationInputElementId))
        );
        $googleLocationApi.autocomplete.addListener('place_changed', $googleLocationApi.fillInAddress);
    };
    $googleLocationApi.init = function () {
        if(!document.getElementById($googleLocationApi.locationInputElementId)){
            return;
        }
        if (window.google) {
            $googleLocationApi.initAutocomplete();
        }
        else {
            setTimeout($googleLocationApi.init, 500);
        }
    };
    $googleLocationApi.getFilledForm = function(){
        return $googleLocationApi.filledForm;
    };

    $googleLocationApi.registerUpdateProperty = function(obj, field){
        $googleLocationApi.updateObject = { obj: obj, field: field};
    }

    $googleLocationApi.fillInAddress = function () {
        var place = $googleLocationApi.autocomplete.getPlace();
        for (var i = 0; i < place.address_components.length; i++) {
            var addressCmp = place.address_components[i];
            var addressType = addressCmp.types[0];
            var addressType2 = false;
            if (addressCmp.types[1]) {
                addressType2 = addressCmp.types[1];
            }
            if (componentForm[addressType]) {
                $googleLocationApi.filledForm[addressType] = addressCmp[componentForm[addressType]] || addressCmp['short_name'];
            } else if (addressType2) {
                $googleLocationApi.filledForm[addressType2] = addressCmp[componentForm[addressType2]];
            }
            if (addressType == 'administrative_area_level_2') {
                if (!$googleLocationApi.filledForm['administrative_area_level_1'])
                    $googleLocationApi.filledForm['administrative_area_level_1'] = addressCmp['long_name'];
            }
        }
        if(!$googleLocationApi.filledForm['locality']){
            $googleLocationApi.filledForm['locality'] = $googleLocationApi.filledForm['sublocality_level_1'];
        }
        $googleLocationApi.filledForm['formatted_address'] = place.formatted_address;
        $googleLocationApi.filledForm['lat'] = place.geometry.location.lat();
        $googleLocationApi.filledForm['lon'] = place.geometry.location.lng();
        if ($googleLocationApi.updateObject.obj) {
            $googleLocationApi.updateObject.obj[$googleLocationApi.updateObject.field] = $googleLocationApi.filledForm;
        }
        if(document.getElementById($googleLocationApi.locationResultInputElId))
            document.getElementById($googleLocationApi.locationResultInputElId).value = angular.toJson($googleLocationApi.filledForm);
    };
    return $googleLocationApi;
});