"use strict";

/**
 * Service for working witch geo
 */
app.factory('$facebookApi', function (facebookApplicationId) {

    var $facebookApi = {};

    $facebookApi.addApplicationToPageModal = function () {
        if (typeof(FB) != 'undefined') {
            FB.ui({
                method: 'pagetab',
                redirect_uri: 'https://www.treatstock.com/'
            }, function (response) {
            });
        } else {
            $.ajax({
                url: '//connect.facebook.net/en_US/sdk.js',
                dataType: "script",
                success: function () {
                    FB.init({
                        appId: facebookApplicationId,
                        status: true,
                        cookie: true,
                        xfbml: true,
                        version: 'v2.1'
                    });
                    FB.ui({
                        method: 'pagetab',
                        redirect_uri: 'https://www.treatstock.com/'
                    }, function (response) {
                    });
                }
            });
        }

    };

    return $facebookApi;
});