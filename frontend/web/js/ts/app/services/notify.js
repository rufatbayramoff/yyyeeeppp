"use strict";

/**
 * Notifi service.
 * Now it just wrapper for TS.Notify functions
 */
app.factory('$notify', function()
{
    var $notify = {};

    /**
     * Prepare message to string
     * @param {String|String[]}message
     * @returns {String}
     */
    var prepareMessage = function(message)
    {
        return commonJs.prepareMessage(message);
    };

    /**
     * Show error notify
     * @param {String|String[]} message
     * @param {boolean} [autoClose]
     */
    $notify.error = function(message, autoClose)
    {
        new TS.Notify({ type : 'error', text: prepareMessage(message),  automaticClose: autoClose === undefined ? true : !!autoClose});
    };

    /**
     * Show success notify
     * @param {String|String[]} message
     * @param {boolean} [autoClose]
     */
    $notify.success = function(message, autoClose)
    {
        new TS.Notify({ type : 'success', text: prepareMessage(message),  automaticClose: autoClose === undefined ? true : !!autoClose});
    };

    /**
     * Show success notify
     * @param {String|String[]} message
     * @param {boolean} [autoClose]
     */
    $notify.info = function(message, autoClose)
    {
        new TS.Notify({ type : 'information', text: prepareMessage(message),  automaticClose: autoClose === undefined ? true : !!autoClose});
    };

    /**
     * Show success notify
     * @param {String|String[]} message
     * @param {boolean} [autoClose]
     */
    $notify.warning = function(message, autoClose)
    {
        new TS.Notify({ type : 'warning', text: prepareMessage(message),  automaticClose: autoClose === undefined ? true : !!autoClose});
    };

    return $notify;
});
