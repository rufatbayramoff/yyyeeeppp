"use strict";

/**
 * Notifi service.
 * Now it just wrapper for TS.Notify functions
 */
app.factory('$mathUtils', function()
{
    var $mathUtils = {};

    /**
     *
     * @param value
     * @param type
     * @param exp
     * @return {*}
     */
    var decimalAdjust = function (value, type, exp)
    {
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    };

    /**
     *
     * @param value
     * @return {*}
     */
    $mathUtils.round = function(value)
    {
        return decimalAdjust(value, 'round', -2)
    };

    /**
     *
     * @param value
     * @return {*}
     */
    $mathUtils.ceil = function(value)
    {
        return decimalAdjust(value, 'ceil', 0);
    };

    return $mathUtils;
});
