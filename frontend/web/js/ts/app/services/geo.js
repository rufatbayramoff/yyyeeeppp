"use strict";

/**
 * Service for working witch geo
 */
app.factory('$geo', function(countries)
{
    var $geo = {};

    /**
     * Return list of countries
     * @returns {*}
     */
    $geo.getCountries = function()
    {
        if(!countries || _.isEmpty(countries))
        {
            throw "Countries not loaded";
        }
        return countries;
    };

    /**
     * Return country by id
     * @param countryId
     */
    $geo.getCountryById = function(countryId)
    {
        return _.find(this.getCountries(), function(country){ return country.id == countryId});
    };

    /**
     * Return country by iso code
     * @param isoCode
     */
    $geo.getCountryByIsoCode = function(isoCode)
    {
        return _.find(this.getCountries(), function(country){ return country.iso_code == isoCode});
    };

    /**
     * Convert address object to string represent
     * @param address
     * @returns {string}
     */
    $geo.stringifyAddress = function(address)
    {
        var addressChunks = [];

        addressChunks.push(this.getCountryById(address.country_id).title);
        addressChunks.push(address.region);
        addressChunks.push(address.city);
        addressChunks.push(address.address);

        if(address.address2)
        {
            addressChunks.push(address.address2);
        }

        addressChunks = _.filter(addressChunks, function(val){ return !_.isEmpty(val); });

        return addressChunks.reverse().join(', ');
    };

    return $geo;
});