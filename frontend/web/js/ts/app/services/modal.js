"use strict";

app.factory('$modal', function($templateCache, $compile, $rootScope, $controller, $q)
{
    /**
     * Default config for modal
     * @type {{scope: undefined, template: undefined}}
     */
    var defaultModalConfig = {
        scope : undefined,
        template : undefined,
        templateContent : undefined,
        controller : undefined,
        onShown : angular.noop,
        onClose : angular.noop
    };


    /**
     * Prepare config : merge with default config and resolve them
     * @param config
     */
    var prepareConfig = function (config)
    {
        if(!config.templateContent && !config.template){
            throw new Error('Bad modal configuration');
        }

        if(!config.scope){
            config.scope = $rootScope.$new(true);
        }

        if(config.scope.constructor != $rootScope.constructor){
            config.scope = angular.extend($rootScope.$new(true), config.scope);
        }

        config = angular.extend({}, defaultModalConfig, config);
        return config;
    };


    var $modal = {};

    /**
     * Open modal dialog
     * @param config
     */
    $modal.open = function(config)
    {
        config = prepareConfig(config);

        var template = config.templateContent || $templateCache.get(config.template);
        var modal = $compile(template)(config.scope);

        if(modal.length === 0){
            throw new Error("Undefined modal template " + config.template);
        }

        modal.modal({})
            .on('hidden.bs.modal', function()
            {
                config.onClose.call(config.scope);
                $(this).remove();
            })
            .on('shown.bs.modal', config.onShown);

        config.scope.$dismiss = function()
        {
            modal.modal('hide');
        };


        if(config.controller){
            $controller(config.controller, {$scope: config.scope});
        }

        return config.scope;
    };

    /**
     *
     * @param {String} text
     * @param {String} [yes]
     * @param {String} [no]
     * @param {String} [title]
     */
    $modal.confirm = function (text, yes, no, title)
    {
        yes = yes || _t('site', 'Ok');
        no = no || _t('site', 'Cancel');
        title = title || _t('site', 'Confirm');

        var isResolved = false;
        var defer = $q.defer();

        $modal.open({
            templateContent : '\n' +
            '<div class="modal fade" tabindex="-1" role="dialog">\n' +
            '    <div class="modal-dialog">\n' +
            '        <div class="modal-content">\n' +
            '            <div class="modal-header">\n' +
            '                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n' +
            '                <h4 class="modal-title">{{title}}</h4>\n' +
            '            </div>\n' +
            '\n' +
            '            <div class="modal-body">\n' +
            '                <p>{{text}}</p>\n' +
            '            </div>\n' +
            '\n' +
            '            <div class="modal-footer">\n' +
            '                <button\n' +
            '                        ng-click="yesClick()"\n' +
            '                        type="button" class="btn btn-primary">{{yes}}</button>\n' +
            '\n' +
            '                <button\n' +
            '                        ng-click="noClick()"\n' +
            '                        type="button" class="btn btn-default">{{no}}</button>\n' +
            '\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>',
            scope : {
                yes : yes,
                no : no,
                text : text,
                title : title,

                yesClick : function () {
                    isResolved = true;
                    defer.resolve();
                    this.$dismiss();
                },

                noClick : function () {
                    isResolved = true;
                    defer.reject();
                    this.$dismiss();
                }
            },
            onClose : function () {

                if(!isResolved) {
                    defer.reject();
                }
            }
        });

        return defer.promise;
    };
















    return $modal;
});
