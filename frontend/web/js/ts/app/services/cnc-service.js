"use strict";

app.factory('$cncService2', function ($http, $notify, $modal, $q, controllerParams)
{
    var $cncService2 = {};

    /**
     *
     * @param {number} preorderId
     * @param {*} [declineForm]
     * @return {*}
     */
    $cncService2.decline = function (preorderId, declineForm)
    {
        if(!declineForm) {
            declineForm = {};
        }
        return $http.post('/workbench/cncm/preorder/decline', declineForm, {params : {preorderId : preorderId}})
            .then(function (response) {return $q.resolve(new CncPreorder(response.data)); });
    };


    /**
     *
     * @param {number} preorderId
     * @param {boolean} [dismiss]
     * @param {boolean} isCutomer
     */
    $cncService2.openDeclineModal = function (preorderId, dismiss, isCutomer)
    {
        var defer = $q.defer();

        $modal.open({
            template : '/app/ps/orders/order-decline-modal.html',
            onClose : function(){defer.reject()},
            controller : function($scope)
            {
                /**
                 * Decline reasons
                 * @type {*}
                 */
                $scope.declineReasons = isCutomer
                    ? controllerParams.cncPreorderDeclineCustomerReasons
                    : controllerParams.cncPreorderDeclineReasons;

                /**
                 * Decline form
                 * @type {*}
                 */
                $scope.form = {
                    reasonId : undefined,
                    reasonDescription  : undefined
                };

                /**
                 *
                 */
                $scope.validateForm = function()
                {
                    var errors = [];
                    if(!$scope.form.reasonId){
                        errors.push(_t('site.ps', 'Please select decline reason'));
                    }
                    return errors;
                };

                /**
                 *
                 * @returns {*}
                 */
                $scope.decline = function ()
                {
                    var errors = $scope.validateForm();

                    if(!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }

                    return $cncService2.decline(preorderId, $scope.form)
                        .then(
                            function(preorder){defer.resolve(preorder); if (dismiss) $scope.$dismiss();  },
                            function (response) {defer.reject(response);}
                        );
                };
            }
        });

        return defer.promise;
    };


    return $cncService2;
});