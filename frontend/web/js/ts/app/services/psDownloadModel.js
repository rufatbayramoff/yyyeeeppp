/**
 * Created by analitic on 26.05.17.
 */

"use strict";

/**
 * Download store order attempt file
 */
app.factory('$psDownloadModel', function ($q, $modal, $http, $notify, $notifyHttpErrors, $timeout) {
    var $psDownloadModel = {};

    /**
     * Show message that need upload photo after printing or doenload file
     */
    $psDownloadModel.notifiNeedUploadPhoto = function () {
        $notify.info(_t('site.ps', 'You should upload photo of printed model by clicking "Register result" in order to inform Treatstock about success of each attempt.'));
    };

    /**
     *
     * @param attemptId
     * @param fileId
     * @param showNeedUploadNotify
     */
    $psDownloadModel.openFile = function (attemptId, fileId, showNeedUploadNotify)
    {
        if (typeof(fileId) === 'undefined') {
            fileId = '';
        }
        $http.get('/workbench/service-order/can-download-files', {params: {attempId: attemptId}})
            .then(function () {
                window.open('/workbench/service-order/download-files?attempId=' + attemptId + '&fileId=' + fileId, '_self');

                if (showNeedUploadNotify) {
                    $psDownloadModel.notifiNeedUploadPhoto();
                }
            })
            .catch($notifyHttpErrors);
    };

    /**
     *
     * @param {Ps} ps
     * @param {int} attemptId
     * @param {int} [fileId]
     * @param {bool} [showNeedUploadNotify]
     */
    $psDownloadModel.downloadFiles = function (ps, attemptId, fileId, showNeedUploadNotify)
    {
        return $psDownloadModel
            .acceptDownloadPolisy(ps)
            .then(function(){return $psDownloadModel.prepareFiles(attemptId, fileId)})
            .then(function(){$psDownloadModel.openFile(attemptId, fileId, showNeedUploadNotify)});
    };

    /**
     *
     * @param {int} attempId
     * @param {int} [fileId]
     */
    $psDownloadModel.prepareFiles = function (attempId, fileId)
    {
        var defer = $q.defer();

        var load = function () {

            $http.post('/workbench/service-order/prepare-download', {}, {params : {attempId : attempId, fileId : fileId}})
                .then(function (response) {

                    var prepareResponse = new PrepareDownloadFileResponse(response.data);

                    if (prepareResponse.isSuccess()) {
                        defer.resolve();
                        return;
                    }

                    if (prepareResponse.isError()) {
                        defer.reject(prepareResponse.error);
                        return;
                    }

                    $timeout(load, 1500);

                }, function (response) {
                    defer.reject(response);
                });
        };

        load();

        return defer.promise;
    };

    /**
     *
     * @param {*} ps
     */
    $psDownloadModel.acceptDownloadPolisy = function (ps)
    {
        var defer = $q.defer();

        if (ps && ps.dont_show_download_policy_modal) {
            defer.resolve();
            return defer.promise;
        }

        $modal.open({
            template: '/app/ps/orders/download-model-terms-modal.html',
            scope: {
                dont_show_download_policy_modal: false,
            },
            controller: function ($scope)
            {
                /**
                 * Accept policy
                 * @return {Promise}
                 */
                $scope.agree = function ()
                {
                    if ($scope.dont_show_download_policy_modal) {
                        return $http.post('/workbench/service-order/accept-download-policy', {})
                            .success(function () {
                                $scope.$dismiss();
                                ps.dont_show_download_policy_modal = 1;
                                defer.resolve();
                            })
                            .catch($notifyHttpErrors)
                    }
                    else {
                        $scope.$dismiss();
                        defer.resolve();
                    }
                };

                /**
                 * Cancel accept dialog
                 */
                $scope.cancel = function ()
                {
                    $scope.$dismiss();
                    defer.reject();
                }
            }
        });

        return defer.promise;
    };

    return $psDownloadModel;
});


/**
 * @property {String} status
 * @property {String} [error]
 * @constructor
 */
function PrepareDownloadFileResponse(data)
{
    angular.extend(this, data);
}

PrepareDownloadFileResponse.STATUS_WAIT = 'wait';
PrepareDownloadFileResponse.STATUS_SUCCESS = 'success';
PrepareDownloadFileResponse.STATUS_ERROR = 'error';

/**
 *
 * @return {boolean}
 */
PrepareDownloadFileResponse.prototype.isSuccess = function ()
{
    return this.status === PrepareDownloadFileResponse.STATUS_SUCCESS;
};

/**
 *
 * @return {boolean}
 */
PrepareDownloadFileResponse.prototype.isError = function ()
{
    return this.status === PrepareDownloadFileResponse.STATUS_ERROR;
};

