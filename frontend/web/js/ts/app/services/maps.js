"use strict";

/**
 * Service for working witch google maps
 */
app.factory('$maps', function($q, $geo, $window)
{
    if (!$window.google || !$window.google.maps){
        return {};
    }

    /**
     * Geodecoder
     * @type {google.maps.Geocoder}
     */
    var geocoder = new google.maps.Geocoder();

    /**
     * Service object
     */
    var $maps = {};

    /**
     * Create request to geocoder witch geocodeParams as params
     * Return result array with promise, like query in ngResource
     * @param {*} geocodeParams
     * @param {int} [limit] limit of returned addresses
     * @returns {Array}
     */
    var geocodeRequestFn = function (geocodeParams, limit) {
        var addresses = [];
        addresses.$promise = $q(function (resolve, reject) {
            geocoder.geocode(geocodeParams, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (limit) {
                            results = results.slice(0, limit);
                        }

                        _.each(results, function (googlePlace) {
                            addresses.push($maps.createAddressObject(googlePlace));
                        });

                        resolve(addresses);
                    }
                    else {
                        reject();
                    }
                }
            );
        });

        return addresses;
    };

    /**
     * Decode coords to addresses.
     * Return array with promise, like query in ngResource
     * @param {number} lat
     * @param {number} lon
     * @param {int} [limit] limit of returned addresses
     * @returns {*[]}
     */
    $maps.geodecode = function (lat, lon, limit) {
        var coords = new google.maps.LatLng(lat, lon);
        return geocodeRequestFn({latLng: coords}, limit);
    };

    /**
     * Encode address string to address objects.
     * Return array with promise, like query in ngResource
     * @param {string} addressString
     * @param {int} [limit] limit of returned addresses
     * @returns {*[]}
     */
    $maps.geocode = function (addressString, limit) {
        return geocodeRequestFn({address: addressString}, limit);
    };


    /**
     * Return current address based on browser geolocation
     * @returns {*}
     */
    $maps.getCurrentAddress = function () {
        var address = {};

        address.$promise = $q(function (resolve, reject) {
            if (!navigator.geolocation) {
                reject();
            }

            navigator.geolocation.getCurrentPosition(function (position) {
                $maps.geodecode(position.coords.latitude, position.coords.longitude, 1).$promise
                    .then(function (addresses) {
                        if (_.isEmpty(addresses)) {
                            reject();
                        }

                        angular.extend(address, addresses.shift());
                        resolve(address);

                    })
                    .catch(function () {
                        reject();
                    });
            }, reject);

        });

        return address;
    };

    /**
     * Create address object (locality) from google place
     * @param googlePlace
     * @returns {{country_id: int, region: string, city: string, address: string, address2: string|undefined, lat: string|undefined, lon: string|undefined, zip_code: string, $$approximately : boolean, $$rawPlace : *}}
     */
    $maps.createAddressObject = function (googlePlace) {
        var address = {
            country_id: undefined,
            counsty_iso: undefined,
            region: undefined,
            city: undefined,
            address: undefined,
            address2: undefined,
            lat: undefined,
            lon: undefined,
            zip_code: undefined
        };

        var streetAddress = [];

        _.each(googlePlace.address_components.reverse(), function (addressComponent) {
            switch (addressComponent.types[0]) {
                case "postal_code":
                    address.zip_code = addressComponent.short_name;
                    break;

                case "street_number":
                    address.$$homePrecision = true;
                    streetAddress.push(addressComponent.short_name);
                    break;

                case "route":
                    streetAddress.push(addressComponent.short_name);
                    break;

                case "administrative_area_level_1":
                    address.region = addressComponent.long_name;
                    break;

                case "country":
                    address.country_id = $geo.getCountryByIsoCode(addressComponent.short_name).id;
                    address.country_iso = addressComponent.short_name;
                    break;

                case "locality":
                    address.city = addressComponent.short_name;
                    break;
            }
        });

        if (!address.city && address.region) {
            address.city = address.region;
        }

        address.address = streetAddress.reverse().join(' ');

        if (googlePlace.geometry.location) {
            address.lat = googlePlace.geometry.location.lat();
            address.lon = googlePlace.geometry.location.lng();
        }

        address.$$rawPlace = googlePlace;
        address.$$approximately = !!googlePlace.partial_match;

        return address;
    };

    return $maps;
});