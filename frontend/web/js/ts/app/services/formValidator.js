"use strict";

/**
 * Form validator
 */
app.factory('$formValidator', function () {
    var $formValidator = {};

    $formValidator.clearErrors = function () {
        $('.validationError').removeClass('validationError');
        $('.validationErrorText').html('').removeClass('validationErrorText');
    };

    $formValidator.applyValidate = function (errorList) {
        for (var key in errorList) {
            var errorText = errorList[key];
            $('[name="' + key + '"]').addClass('validationError');
            // Fix attribute if not exists
            $('[name="' + key + '"]').parent().find('.help-block').each(function (id, element) {
                if (!$(element).attr('validation-for')) {
                    $(element).attr('validation-for', key);
                }
            });
            $('[validation-for="'+key+'"]').addClass('validationErrorText').html(errorText);
        }
        return;
    };

    $formValidator.applyValidateById = function (errorList, idPrefix) {
        for (var key in errorList) {
            var errorText = errorList[key];
            var input = $('#' + idPrefix + key + '');
            if (!input.length) {
                var ucfirstKey = key.charAt(0).toUpperCase() + key.slice(1);
                input = $('#' + idPrefix + ucfirstKey + '');
            }
            if (input) {
                input.addClass('validationError');
                input.parent().find('.help-block-error').addClass('validationErrorText').html(errorText);
            }
        }
        return;
    };


    return $formValidator;
});