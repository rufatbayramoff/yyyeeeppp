"use strict";

/**
 * Service for working with flush messages
 */
app.factory('$flushMessageManager', ['$sessionStorage', function ($sessionStorage) {
    var $flushMessageManager = {};

    /**
     * Add new flush message:
     *   messageTypes: error, success, warn
     * @returns {*}
     */
    $flushMessageManager.flush = function (messageText, messageType, autoClose) {
        messageType = (typeof (messageType) == 'undefined') ? 'success' : messageType;
        autoClose = (typeof (autoClose) == 'undefined') ? true : autoClose;
        var messages = $sessionStorage.get('flushMessages', []);
        if (!messages) {
            messages = [];
        }
        var message = {
            text: messageText,
            type: messageType,
            autoClose: autoClose

        };
        messages.push(message);
        $sessionStorage.set('flushMessages', messages);
    };

    $flushMessageManager.showMessages = function () {
        var messages = $sessionStorage.get('flushMessages', []);
        var key;
        for (key in messages) {
            if (!messages.hasOwnProperty(key)) continue;
            var message = messages[key];
            $flushMessageManager.processMessage(message);
        }
        $sessionStorage.set('flushMessages', []);
    };

    $flushMessageManager.processMessage = function (message) {
        new TS.Notify({
            type: message.type,
            text: message.text,
            automaticClose: message.autoClose
        });
    };

    return $flushMessageManager;
}]);

