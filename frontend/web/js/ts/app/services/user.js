"use strict";

/**
 * User service.
 * Now it just wrapper for TS.Notify functions
 */
app.factory('$user', function ($http, $q, $$userData) {
    var $user = angular.extend({}, $$userData);

    if ((typeof TS !== 'undefined') && (TS.User)) {
        if (TS.User.isGuest) {
            $user.isGuest = true;
        }
        else {
            $user.isGuest = false;
            $user.userId = TS.User.userId;
            $user.username = TS.User.username;
        }
    } else {
        $user.isGuest = true;
    }

    /**
     * Open login modal
     * @param redirectTo
     */
    $user.openLoginModal = function (redirectTo) {
        TS.Visitor.loginForm(redirectTo);
    };

    /**
     * Check that email already exist
     * @param email
     * @returns {*}
     */
    $user.checkEmailExist = function (email) {
        var deffer = $q.defer();
        $http.post('/store/delivery/check-email-exist', {email: email})
            .then(function (response) {
                deffer.resolve(response.data)
            }, deffer.reject);
        return deffer.promise;
    };

    return $user;
});
