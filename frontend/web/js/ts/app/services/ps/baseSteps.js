/**
 * Created by analitic on 17.07.17.
 */

app.factory('baseSteps', function ($resource) {
    var baseSteps = {
        'currentStep' : 1,
        'stepsCount' : 1
    };

    /**
     *
     * @returns {boolean}
     */
    baseSteps.isFirstStep  = function()
    {
        return this.currentStep === 1;
    };

    /**
     *
     * @returns {boolean}
     */
    baseSteps.isLastStep = function()
    {
        return this.currentStep === this.stepsCount;
    };

    /**
     *
     * @param step
     * @returns {boolean}
     */
    baseSteps.isCurrentStep = function(step)
    {
        return this.currentStep === step;
    };

    /**
     *
     */
    baseSteps.nextStep = function()
    {
        if(this.currentStep < this.stepsCount)
        {
            this.checkStep(this.currentStep + 1);
        }
    };

    /**
     *
     */
    baseSteps.prevStep = function()
    {
        if(this.currentStep > 1)
        {
            this.checkStep(this.currentStep - 1)
        }
    };

    /**
     * Set step
     * @param step
     */
    baseSteps.checkStep = function(step)
    {
        if(this.onBeforeStepChange(step, this.currentStep))
        {
            this.currentStep = step;
            if (this.onStepChanged){
                this.onStepChanged(this.currentStep);
            }
        }
    };

    /**
     * Event on step change.
     * If this method return false - step will not changed
     * @returns {boolean}
     * @param {int} newStep
     * @param {int} oldStep
     */
    baseSteps.onBeforeStepChange = function(newStep, oldStep)
    {
        return true;
    };

    /**
     * On step changed
     * @param {int} newStep
     */
    baseSteps.onStepChanged = function (newStep)
    {
    };

    return baseSteps;
});