"use strict";

/**
 * Service for working witch currency
 */
app.factory('$modelService', function ($http, $router, $rootScope, $notifyHttpErrors, $q, $timeout) {
    var $modelService = {};

    $modelService.checkJobs2 = function () {
        var checkJobs = {
            jobTimeout: 0
        };

        checkJobs.checkJobsRequest = function () {
            $http.get('/catalog/model3d/check-jobs', {params: {modelId: checkJobs.modelId}}).then(function (response) {
                    if (checkJobs.tickFunction(response.data)) {
                        checkJobs.jobTimeout = 0;
                    } else {
                        checkJobs.jobTimeout = setTimeout(
                            checkJobs.checkJobsRequest, 1000
                        )
                    }
                }
            )
        };

        /**
         * Check jobs finished for model with tickFunction
         * @param {int} modelId
         * @param {function} tickFunction
         */
        checkJobs.check = function (modelId, tickFunction) {
            if (checkJobs.jobTimeout) {
                return;
            }
            checkJobs.modelId = modelId;
            checkJobs.tickFunction = tickFunction;

            checkJobs.jobTimeout = setTimeout(
                checkJobs.checkJobsRequest, 0
            )
        };
        return checkJobs.check;
    }();


    /**
     *
     */
    $modelService.checkJobs = function () {
        var checkJobs = {
            /**
             * Runned deffers
             */
            $$runnedDeffers: []
        };

        /**
         * Do request to server
         * @param {int} modelId
         * @param {int} [partId]
         */
        var loadData = function (modelId, partId) {
            return $http.get('/catalog/model3d/check-jobs', {params: {modelId: modelId, partId: partId}})
        };

        /**
         * Load data one time
         * @param {int} modelId
         * @param {int} [partId]
         */
        checkJobs.check = function (modelId, partId) {
            return loadData(modelId, partId).then(
                function (response) {
                    return $q.resolve(response.data);
                }
            );
        };

        /**
         * Wait while all jobs is complited
         * @param {int} modelId
         * @param {int} [partId]
         */
        checkJobs.waitComplited = function (modelId, partId) {
            var defer = $q.defer();
            defer.stopped = false;
            var tickHandler;

            defer.promise.tickSubscribe = function (handler) {
                if (!angular.isFunction(handler)) {
                    throw new Error("Tick handler must be function");
                }
                tickHandler = handler;
                return defer.promise;
            };

            var onSuccessTickLoad = function (response) {
                if (tickHandler) {
                    tickHandler(response);
                }

                if (response.isComplited) {
                    defer.resolve(response);
                    return;
                }

                repeatRequest();
            };

            var repeatRequest = function () {
                $timeout(checkInternal, 1000);
            };

            var checkInternal = function () {
                if (defer.stopped) {
                    return;
                }
                var promise = checkJobs.check(modelId, partId)
                    .then(onSuccessTickLoad, repeatRequest);
            };

            checkInternal();

            checkJobs.$$runnedDeffers.push(defer);

            return defer.promise;
        };


        /**
         * Stop all created and runned checkJobs
         */
        checkJobs.stopAll = function () {
            checkJobs.$$runnedDeffers.forEach(function (defer) {
                defer.stopped = true;
            });
            checkJobs.$$runnedDeffers.length = 0;
        };

        return checkJobs;
    }();

    $modelService.cancelUploadItem = function (uploadFileInfo) {
        if ((typeof uploadFileInfo.uploadItem) !== 'undefined') {
            uploadFileInfo.uploadItem.cancel();
        }

        uploadFileInfo.isCanceled = true;

        var data = {
            'uid': uploadFileInfo.uid,
            'fileMd5NameSize': uploadFileInfo.fileMd5NameSize
        };
        $http.post($router.getPrintModel3dCancel(), data)
            .then(function (responce) {

            })
            .catch($notifyHttpErrors)
    };

    $modelService.restoreUploadItem = function (uploadFileInfo, successCallback) {
        if ((typeof uploadFileInfo.qty) !== 'undefined') {
            uploadFileInfo.qty = 1;
        }

        uploadFileInfo.isCanceled = false;
        var data = {
            'uid': uploadFileInfo.uid,
        };
        $http.post($router.getPrintModel3dRestore(), data)
            .then(function (responce) {
                successCallback(responce);
            })
            .catch($notifyHttpErrors)
    };

    $modelService.updatePartQty = function (model3dPart) {
        var data = {
            'uid': model3dPart.uid,
            'fileMd5NameSize': model3dPart.fileMd5NameSize,
            'qty': model3dPart.qty
        };
        return $http.post($router.getPrintModel3dQty(), data)
            .then(function (responce) {
                model3dPart.weight = responce.data.weight;
                return responce;
            })
            .catch($notifyHttpErrors)
    };

    $modelService.testHasStepFiles = function (model3d) {
        for (var partKey in model3d.parts) {
            if (!model3d.parts.hasOwnProperty(partKey)) {
                continue;
            }
            let part = model3d.parts[partKey];
            if (!part.isCanceled && part.isStepFile()) {
                return true;
            }
        }
        return false;
    };

    $modelService.testIsEmptyModel = function (model3d) {
        for (var partKey in model3d.parts) {
            if (!model3d.parts.hasOwnProperty(partKey)) {
                continue;
            }
            if (!model3d.parts[partKey].isCanceled) {
                return false;
            }
        }
        for (var imgKey in model3d.images) {
            if (!model3d.images.hasOwnProperty(imgKey)) {
                continue;
            }
            if (!model3d.images[imgKey].isCanceled) {
                return false;
            }
        }
        return true;
    };

    $modelService.switchScaleUnit = function (model3d, scaleUnit, successCallBack) {
        model3d.modelUnits = scaleUnit;
        var data = {
            'model3dUid': model3d.uid,
            'scaleUnit': scaleUnit
        };
        $http.post($router.getPrintModel3dScale(), data)
            .then(function (responce) {
                $modelService.updateModel3dInfo(model3d, responce.data.scaledModel3d);
                successCallBack();
            })
            .catch($notifyHttpErrors)

    };

    /**
     *
     * @param {Model} model3d
     * @param {ModelPart} model3dPart
     * @param successCallBack
     */
    $modelService.updatePartScale = function (model3d, model3dPart, newSize, successCallBack) {
        var data = {
            'parts': [
                {
                    'model3dPartUid': model3dPart.uid,
                    'size': newSize
                }
            ]
        };
        $http.post($router.getPrintModel3dPartsSize(), data)
            .then(function (responce) {
                var modelInfo = _.first(responce.data.scaledModel3dList);
                $modelService.updateModel3dInfo(model3d, modelInfo);
                successCallBack();
            })
            .catch($notifyHttpErrors);
    };

    $modelService.updateModel3dInfo = function (model3d, model3dInfo) {
        if (!model3d.id) {
            model3d.id = model3dInfo.id;
        }
        if(!model3d.uid) {
            model3d.uid = model3dInfo.uid;
        }
        for (var partInfoKey in model3dInfo.parts) {
            if (!model3dInfo.parts.hasOwnProperty(partInfoKey)) {
                continue;
            }
            for (var partKey in model3d.parts) {
                if (!model3d.parts.hasOwnProperty(partKey)) {
                    continue;
                }
                var part = model3d.parts[partKey];
                var partInfo = model3dInfo.parts[partInfoKey];
                if (part.uid) {
                    if (part.uid === partInfo.uid) {
                        part.load(partInfo);
                    }
                } else {
                    if (part.fileMd5NameSize) {
                        if (part.fileMd5NameSize == partInfo.fileMd5NameSize) {
                            part.load(partInfo);
                        }
                    }
                }
            }
        }
    };
    
    $modelService.duplicate = function (data, successCallBack) {
        $http.post($router.getPrintModel3dDuplicate(), data)
            .then(function (res) {
                successCallBack(res);
            })
            .catch($notifyHttpErrors)
    }


    $modelService.testIsPrintablePartsExists = function (model3d) {

    };

    return $modelService;
});