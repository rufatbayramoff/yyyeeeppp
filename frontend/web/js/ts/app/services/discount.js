"use strict";


app.factory('$discount', function(discountSettings)
{
    var $discount = {
        priceTypes : ['model', 'print', 'fee', 'delivery', 'all'],
        realPrices : {
            model: 0,
            print: 0,
            fee: 0,
            delivery: 0,
            all: 0
        }
    };

    /**
     * @type {*[]}
     */
    $discount.settings = discountSettings;

    $discount.setRealPrices = function(prices){
        $discount.realPrices = prices;
    };

    $discount.getDiscountByPriceType = function(price, priceType) {
        var discountAmountTotal = 0;
        $discount.settings.forEach(function(discountData){
            var discountAmount = 0;
            if (discountData.discount_for == priceType) {
                if(discountData.discount_type == 'fixed'){
                    discountAmount = parseFloat(discountData.discount_amount);
                }else {
                    discountAmount = parseFloat(price * discountData.discount_amount / 100);
                }
                discountAmountTotal+=discountAmount;
            }
        });
        return discountAmountTotal > price ? price : discountAmountTotal;
    };

    $discount.getTotalDiscount = function() {
        var discountTotal = 0;
        $discount.priceTypes.forEach(function(priceType){
            var discount = $discount.getDiscountByPriceType($discount.realPrices[priceType], priceType);
            discountTotal = discountTotal + parseFloat(discount);
        });
        return discountTotal > $discount.realPrices.all ? $discount.realPrices.all : discountTotal;
    };

    return $discount;
});