
app.controller('PrinterShipping', function ($scope, controllerParams)
{
    $scope.printers = controllerParams.printersShipping;

    $scope.selectedPrinter = controllerParams.printersShipping[0];
});