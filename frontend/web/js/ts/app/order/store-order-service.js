"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.factory('$storeOrderService', function ($http, $q, $modal, $user, $notifyHttpErrors, declineReasons, declineProductReasons, cncPreorderDeclineReasons, cncPreorderDeclineCustomerReasons, preorderDeclineReasons, $notify, $router) {

    var $orderService = {};

    $orderService.gettingPostalLabel = false;
    $orderService.parcel = {
        width: 0,
        height: 0,
        length: 0,
        weight: 0
    };


    $orderService.setOrderStatus = function (attempId, status) {
        if ($orderService.gettingPostalLabel === true) {
            $notify.error('Please wait...');
            return;
        }
        return $http.post('/workbench/service-order/set-order-status', {status: status}, {params: {attempId: attempId}})
            .then(function (response) {
                if (status == 'accepted') {
                    window.location.href = '/workbench/service-order/print?attempId=' + attempId;
                } else {
                    $router.toPsPrintRequests(response.data.statusGroup);
                }
            })
            .catch($notifyHttpErrors);
    };

    /**
     * set order sent with tracking number
     *
     * @param attempId
     * @param isPreorder
     * @param formData
     */
    $orderService.setOrderSentWithTrackingNumber = function (attempId, isPreorder, formData) {
        $modal.open({
            template: '/app/ps/orders/order-request-tracking-number.html',
            controller: function ($scope) {
                /**
                 * @type {*}
                 */
                $scope.form = {
                    shipper: formData ? formData.shipper : undefined,
                    trackingNumber: formData ? formData.trackingNumber : undefined,
                    withoutTracking: formData ? formData.withoutTracking : false,
                    isPreorder: isPreorder ? isPreorder : false
                };

                $scope.validateForm = function () {
                    var errors = [];

                    if (!$scope.form.shipper) {
                        errors.push(_t('site.ps', 'Please enter shipping company field'));
                    }

                    if (!$scope.form.withoutTracking && !$scope.form.trackingNumber) {
                        errors.push(_t('site.ps', 'Please enter tracking number field'));
                    }

                    return errors;
                };

                /**
                 * @returns {boolean}
                 */
                $scope.saveTrackingNumber = function () {
                    var errors = $scope.validateForm();

                    if (!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }

                    return $http.post('/workbench/service-order/save-tracking-number', $scope.form, {params: {attempId: attempId}})
                        .then(function () {
                            $router.toPsPrintRequests('sent')
                        })
                        .catch($notifyHttpErrors);
                };
            }
        });
    };


    $orderService.getPostalLabel = function (attempId, l, w, h, weight) {
        $http.post('/workbench/service-order/has-postal-label', {}, {params: {attempId: attempId}})
            .then(function (response) {
                if (response.data.success) {
                    return $orderService.printPostalLabel(attempId);
                } else {
                    return $orderService.configPostalLabelParcel(attempId, l, w, h, weight)
                }
            })
            .catch(function (data) {
                console.log(data);
            });
    };


    /**
     *
     * @param attempId
     * @param l  - length
     * @param w - width
     * @param h - height
     * @param weight
     */
    $orderService.configPostalLabelParcel = function (attempId, l, w, h, weight) {
        var parentScope = $orderService;
        $modal.open({
            template: '/app/ps/orders/config-postal-label.html',
            controller: function ($scope) {
                $scope.currentMeasure = $user.metricMeasure == 'cm' ? 'mm' : 'in';
                $scope.form = {
                    length: l,
                    width: w,
                    height: h,
                    weight: weight,
                    measure: $scope.currentMeasure
                };
                $scope.parent = parentScope;

                $scope.validateForm = function () {
                    var errors = [];
                    if (!$scope.form.length || !$scope.form.width || !$scope.form.height) {
                        errors.push(_t('site.ps', 'Please enter size'));
                    }
                    if (!$scope.form.weight) {
                        errors.push(_t('site.ps', 'Please enter weight'));
                    }
                    // USPS limits (inches)
                    if (($scope.form.length + $scope.form.width + $scope.form.height) > 108 || $scope.form.weight > 13) {
                        //errors.push(_t('site.ps', 'Parcel is too big. Please contact support.'));
                    }
                    return errors;
                };
                $scope.printPostalLabel = function () {
                    var errors = $scope.validateForm();
                    if (!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }
                    parentScope.parcel = $scope.form;
                    return parentScope.printPostalLabel(attempId, $scope); // todo - $scope better callback?
                };

                $scope.cancelPrintLabel = function () {
                    if (!parentScope.gettingPostalLabel) {
                        $scope.$dismiss();
                    } else {
                        $notify.error('Please wait...generating label...');
                    }

                }
            }
        });
    };

    $orderService.printPostalLabel = function (attempId, parentScope) {

        if ($orderService.gettingPostalLabel === true) {
            $notify.error('Please wait...');
            return;
        }
        $orderService.gettingPostalLabel = true;
        return $http.post('/workbench/service-order/load-postal-image', {parcel: $orderService.parcel}, {params: {attempId: attempId}})
            .then(function (response) {
                if (parentScope) parentScope.$dismiss();
                $modal.open({
                    template: '/app/ps/orders/postal-label-modal.html',
                    scope: {
                        postalLabelUrl: response.data.postalLabelUrl,
                        postalLabelPrintUrl: response.data.postalLabelPrintUrl
                    }
                });
                $orderService.gettingPostalLabel = false;
            })
            .catch(function (data) {
                $orderService.gettingPostalLabel = false;
                var text = data.data.errors || data.data.message || (_.isString(data.data) && data.data) || 'Error. Please, try again.';
                new TS.Notify({
                    type: 'error',
                    text: text,
                    target: '.configPostalLabel',
                    automaticClose: true,
                    automaticCloseTimer: 10000
                });
                if (data.data.code === 'ParcelCalcFailed') {
                    if (parentScope) parentScope.$dismiss();
                    $orderService.showSupportModal(text, data.data.data['id']);
                }
            });
    };

    $orderService.showSupportModal = function (text, orderId) {
        new TS.confirm(text, function (result) {
            if (result == 'ok') {
                $router.to($router.createSupportTopicOrder(orderId));
            }
        }, {confirm: _t('site.ps', 'Report a Problem'), dismiss: _t('site.ps', 'Cancel')});
    }

    $orderService.cancelAddonPosition = function (additionalPositionId) {
        $modal.confirm(_t('site.preorder', 'Are you sure you want to cancel additional service offer?'), _t('site', 'Yes'), _t('site', 'No')).then(function () {
            $http.post('/workbench/service-order/cancel-addon-position', {addonPositionId: additionalPositionId})
                .then(function (response) {
                    $notify.success('Additional service canceled', true);
                    $router.reload();
                })
                .catch($notifyHttpErrors)
        });
    };

    $orderService.requestMoreTimeModal = function (attempId, $event) {
        $modal.open({
            template: '/app/ps/orders/reuqest-more-time-modal.html',
            controller: function ($scope) {
                /**
                 *
                 * @type {*}
                 */
                $scope.form = {
                    plan_printed_at: $($event.target).data('date'),
                    request_reason: undefined
                };

                /**
                 *
                 */
                $scope.validateForm = function () {
                    var errors = [];

                    if (!$scope.form.plan_printed_at) {
                        errors.push(_t('site.ps', 'Please enter time field'));
                    }

                    if (!$scope.form.request_reason) {
                        errors.push(_t('site.ps', 'Please enter comment field'));
                    }

                    return errors;
                };

                /**
                 *
                 * @returns {boolean}
                 */
                $scope.requestMoreTime = function () {
                    var errors = $scope.validateForm();

                    if (!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }

                    return $http.post('/workbench/service-order/save-request-more-time', $scope.form, {params: {attempId: attempId}})
                        .then(function () {
                            $router.reload();
                        })
                        .catch($notifyHttpErrors);
                }
            }

        });
    };


    $orderService.requestScheduledTimeModal = function (attempId, $event) {
        $modal.open({
            template: '/app/ps/orders/request-scheduled-time-modal.html',
            controller: function ($scope) {
                $scope.form = {
                    scheduled_to_sent_at: $($event.target).data('date'),
                    request_reason: undefined
                };

                $scope.validateForm = function () {
                    var errors = [];

                    if (!$scope.form.scheduled_to_sent_at) {
                        errors.push(_t('site.ps', 'Please enter time field'));
                    }

                    if (!$scope.form.request_reason) {
                        errors.push(_t('site.ps', 'Please enter comment field'));
                    }

                    return errors;
                };

                $scope.requestMoreTime = function () {
                    var errors = $scope.validateForm();

                    if (!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }

                    return $http.post('/workbench/service-order/save-scheduled-sent-time', $scope.form, {params: {attempId: attempId}})
                        .then(function () {
                            $router.reload();
                        })
                        .catch($notifyHttpErrors);
                }
            }

        });
    };


    $orderService.openDeclineOrderModal = function (attempId, declineReasons) {
        $modal.open({
            template: '/app/ps/orders/order-decline-modal.html',
            controller: function ($scope, $http, $router) {
                /**
                 * Decline reasons
                 * @type {*}
                 */
                $scope.declineReasons = declineReasons;

                /**
                 * Decline form
                 * @type {*}
                 */
                $scope.form = {
                    reasonId: undefined,
                    reasonDescription: undefined
                };

                /**
                 *
                 */
                $scope.validateForm = function () {
                    var errors = [];
                    if (!$scope.form.reasonId) {
                        errors.push(_t('site.ps', 'Please select decline reason'));
                    }
                    return errors;
                };

                /**
                 *
                 * @returns {*}
                 */
                $scope.decline = function () {
                    var errors = $scope.validateForm();

                    if (!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }

                    return $http.post('/workbench/service-order/decline-order', $scope.form, {params: {attempId: attempId}})
                        .then(function () {
                            $router.toPsPrintRequests('canceled');
                        })
                        .catch($notifyHttpErrors);
                };
            }

        });
    };

    $orderService.declineOrderByType = function (attempId, type) {
        var declineReasonsList = {};

        if (type === 'productPreorder') {
            declineReasonsList = declineProductReasons;
        } else if (type === 'cncPreorder') {
            declineReasonsList = cncPreorderDeclineReasons;
        } else {
            declineReasonsList = declineReasons;
        }

        $orderService.openDeclineOrderModal(attempId, declineReasonsList)
    };

    $orderService.setAsReadyQuery = function (attempId) {
        return $http.post('/workbench/service-order/set-order-status', {status: 'ready_send'}, {
            params: {attempId: attempId}
        })
            .then(function (response) {
                /** @namespace response.data.statusGroup */
                $router.toPsPrintRequests(response.data.statusGroup);
            })
            .catch($notifyHttpErrors);

    };

    return $orderService;
});