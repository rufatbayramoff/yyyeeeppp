"use strict";


app.factory('$objectStorage', ['$injector',
    function ($injector) {
        var values = {};
        var $objectStorage = {};
        $objectStorage.putList = function (objectsList) {
            for (var objectName in objectsList) {
                var objects = objectsList[objectName];
                for (var objectId in objects) {
                    var objectValue = objects[objectId];
                    $objectStorage.put(objectName, objectValue);
                }
            }
        };

        $objectStorage.put = function (objectName, objectValue) {
            var objectId = objectValue.id;
            var object = new window[objectName](objectValue);
            if ((typeof values[objectName]) == 'undefined') {
                values[objectName] = {};
            }
            values[objectName][objectId] = object;
        };

        $objectStorage.get = function (objectName, objectId) {
            if (typeof values[objectName] === 'undefined') {
                return {};
            }
            if (parseInt(objectId).toString() == (objectId).toString()) {
                objectId = parseInt(objectId);
            }
            if (typeof values[objectName][objectId] === 'undefined') {
                return {};
            }
            return values[objectName][objectId];
        };

        var objectStorageData = null;
        try {
            objectStorageData = $injector.get('objectStorageData');
        } catch (e) {
        }

        if (objectStorageData) {
            $objectStorage.putList(objectStorageData);
        }

        return $objectStorage;
    }]
).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push(['$objectStorage', function ($objectStorage) {
        return {
            response: function (response) {
                if ((response.data) && (response.data.objectStorage)) {
                    $objectStorage.putList(response.data.objectStorage);
                }
                return response;
            }
        }
    }]);
}]);

var objectStorage = function () {
    return angular.element(document.body).injector().get('$objectStorage');
};

