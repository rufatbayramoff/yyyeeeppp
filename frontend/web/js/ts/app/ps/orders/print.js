"use strict";

/**
 * Controller printing model
 */
app.controller('PsPrintController', function ($scope, $notify, $router, $modal, $http, $notifyHttpErrors, $storeOrderService, $psDownloadModel, controllerParams) {
    /** @namespace $scope.attemp */
    /** @namespace $scope.attemp.id */

    $scope.attemp = controllerParams.attemp;

    /**
     * Click on set as printed
     */
    $scope.setAsPrinted = function () {
        if (!this.canSubmitResult()) {
            new TS.Notify({
                type: 'error',
                text: _t('site.ps', 'Please "Upload results" at first'),
                target: '.messageBox',
                automaticClose: true
            });
            return ;
        }

        TS.confirm(_t('site.ps', 'Are you sure you want to submit your results for moderation?'),
            function (result) {
                if (result == 'ok') {
                    return $http.post('/workbench/service-order/set-order-status', {status: 'printed'}, {
                        params: {attempId: $scope.attemp.id}
                    })
                        .then(function (response) {
                            /** @namespace response.data.statusGroup */
                            $router.toPsPrintRequests(response.data.statusGroup);
                        })
                        .catch($notifyHttpErrors);
                }
            },
            {confirm: _t('site.ps', 'Yes'), dismiss: _t('site.ps', 'No')}
        );
    };

    $scope.setAsReady = function () {
        if ($scope.attemp.moderation.files.length === 0) {
            TS.confirm(_t('site.ps', 'Would you like to continue without uploading images? If your order was manufactured on demand, you can upload images to show the client before dispatch.'),
                function (result) {
                    if (result === 'ok') {
                        return $storeOrderService.setAsReadyQuery($scope.attemp.id);
                    }

                    $('.js-dropzone-button-click').trigger('click');
                },
                {confirm: _t('site.ps', 'Yes'), dismiss: _t('site.ps', 'No')}
            );

            return;
        }

        $storeOrderService.setAsReadyQuery($scope.attemp.id);
    };

    $scope.setAsReadyQuery = function () {
        $storeOrderService.setAsReadyQuery($scope.attemp.id);
    };

    /**
     * On download file click
     */
    $scope.downloadFiles = function (attempId) {
        return $psDownloadModel.downloadFiles(controllerParams.company, attempId, undefined, true);
    };

    $scope.declineOrderByType = function (attempId, type)
    {
        $storeOrderService.declineOrderByType(attempId, type);
    };

    /**
     *
     */
    $scope.offerDoCertification = function () {
        TS.confirm(_t('site.ps', 'Only certified 3D printers can print models by downloading the files. Please certify your printer.'),
            function (result) {
                if (result == 'ok') {
                    /** @namespace controllerParams.certificationUrl */
                    $router.to(controllerParams.certificationUrl);
                }
            },
            {confirm: _t('site.ps', 'Get Certification'), dismiss: _t('site.ps', 'No Thanks')}
        );
    };


    /**
     * Add moderation file
     * @param file
     */
    $scope.onAddResultFile = function (file) {
        var fileItem = {isLoading: true};
        $scope.attemp.moderation.files.push(fileItem);

        $http.post('/workbench/service-order/add-result-image', {file: file}, {params: {attemptId: $scope.attemp.id}})
            .then(function (response) {
                fileItem.isLoading = false;
                angular.extend(fileItem, response.data);
            })
            .catch($notifyHttpErrors)
    };


    /**
     * Delete moderation file
     * @param moderationFile
     */
    $scope.deleteModerationFile = function (moderationFile) {
        _.remove($scope.attemp.moderation.files, moderationFile);

        $http.post('/workbench/service-order/delete-result-image', {}, {
            params: {
                attemptId: $scope.attemp.id,
                moderationFileId: moderationFile.id
            }
        })
            .catch($notifyHttpErrors)
    };

    $scope.cancelAddonPosition = function (additionalPositionId) {
        $storeOrderService.cancelAddonPosition(additionalPositionId);
    };

    /**
     * Can sumbit result
     * @returns {boolean}
     */
    $scope.canSubmitResult = function () {
        return !_.isEmpty($scope.attemp.moderation.files);
    };

    $scope.requestMoreTimeModal = function (attempId, $event) {
        $modal.open({
            template: '/app/ps/orders/reuqest-more-time-modal.html',
            controller: function ($scope) {
                /**
                 *
                 * @type {*}
                 */
                $scope.form = {
                    plan_printed_at: $($event.target).data('date'),
                    request_reason: undefined
                };

                /**
                 *
                 */
                $scope.validateForm = function () {
                    var errors = [];

                    if (!$scope.form.plan_printed_at) {
                        errors.push(_t('site.ps', 'Please enter time field'));
                    }

                    if (!$scope.form.request_reason) {
                        errors.push(_t('site.ps', 'Please enter comment field'));
                    }

                    return errors;
                };

                /**
                 *
                 * @returns {boolean}
                 */
                $scope.requestMoreTime = function () {
                    var errors = $scope.validateForm();

                    if (!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }

                    return $http.post('/workbench/service-order/save-request-more-time', $scope.form, {params: {attempId: attempId}})
                        .then(function () {
                            $router.reload();
                        })
                        .catch($notifyHttpErrors);
                }
            }

        });
    };

    $scope.partialRefund = function (attemptId, printAmount) {
        $modal.open({
            template: '/attempt/partitalRefund.html',
            controller: 'OfferPartialRefundController',
            scope: { attemptId: attemptId, printAmount: printAmount}
        });
    };
});