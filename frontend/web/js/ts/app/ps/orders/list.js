"use strict";

/**
 * Controller for manage printservice printer materials
 *
 * @property controllerParams.declineReasons
 * @property controllerParams.cncPreorderDeclineReasons
 * @property controllerParams.servicesPreorderDeclineReasons
 * @property controllerParams.productPreorderDeclineReasons
 * @property controllerParams.declineProductReasons
 *
 */
app.controller('PsOrdersListController', function ($scope, $notify, $notifyHttpErrors, $router, $modal, $http, $user, $measure, $measureConverter, $storeOrderService, $preorderService,
                                                   productPreorderDeclineReasons, servicesPreorderDeclineReasons, controllerParams) {
    /**
     * Set order status
     * @param attempId
     * @param status
     * @returns {*}
     */
    $scope.setOrderStatus = function (attempId, status) {
        return $storeOrderService.setOrderStatus(attempId, status);
    };

    /**
     * set order sent with tracking number
     *
     * @param attempId
     * @param isPreorder
     * @param formData
     */
    $scope.setOrderSentWithTrackingNumber = function (attempId, isPreorder, formData) {
        $storeOrderService.setOrderSentWithTrackingNumber(attempId, isPreorder, formData);
    };

    $scope.popupImageView = function ($event) {
        let src = $event.currentTarget.src;
        if (src) {
            $modal.open({
                template: '/static/templates/viewImage.html',
                scope: {
                    src: src,
                },
            });
        }
    };

    /**
     *
     * @param attempId
     * @returns {*}
     */
    $scope.getPostalLabel = function (attempId, l, w, h, weight) {
        return $storeOrderService.getPostalLabel(attempId, l, w, h, weight);
    };

    $scope.printPostalLabel = function (attempId) {
        return $storeOrderService.printPostalLabel(attempId);
    };

    $scope.cancelAddonPosition = function (additionalPositionId) {
        $storeOrderService.cancelAddonPosition(additionalPositionId);
    };

    /**
     * Open request more time dialog
     * @param attempId
     * @param $event
     */
    $scope.requestMoreTimeModal = function (attempId, $event) {
        $storeOrderService.requestMoreTimeModal(attempId, $event);
    };

    /**
     * Open scheduled time dialog
     * @param attempId
     * @param $event
     */
    $scope.requestScheduledTimeModal = function (attempId, $event) {
        $storeOrderService.requestScheduledTimeModal(attempId, $event);
    }


    /**
     * Open decline modal
     * @param {number} attempId
     * @param {boolean} isCncOrder
     */
    $scope.declineOrder = function (attempId, isCncOrder) {
        $storeOrderService.declineOrderByType(attempId, isCncOrder ? 'cncPreorder' : '')
    };

    $scope.deletePreorder = function (preorderId) {
        $preorderService.deletePreorder(preorderId);
    };

    $scope.declineOrderByType = function (attempId, type) {
        $storeOrderService.declineOrderByType(attempId, type);
    };


    $scope.createQuote = function () {
        $router.toCreateQuoteByPs();
    };

    /**
     * Decline preorder
     * @param preorderId
     * @param isForProduct
     */
    $scope.declinePreorder = function (preorderId, isForProduct) {
        return $preorderService
            .declinePreorder(preorderId, true, (isForProduct ? productPreorderDeclineReasons : servicesPreorderDeclineReasons))
            .then(function () {
                    $router.toPsQuotesRequests();
                },
                $notifyHttpErrors);
    };

    $scope.partialRefund = function (attemptId, printAmount) {
        $modal.open({
            template: '/attempt/partitalRefund.html',
            controller: 'OfferPartialRefundController',
            scope: {attemptId: attemptId, printAmount: printAmount}
        });
    };

    $scope.initOrdersFilterForm = function () {
        var form = $('#orders-filter-form');
        $('#orders-filter-form input, #orders-filter-form select').each(function () {
            $(this).on('change', function () {
                form.yiiActiveForm('submitForm');
            })
        });
    };
});