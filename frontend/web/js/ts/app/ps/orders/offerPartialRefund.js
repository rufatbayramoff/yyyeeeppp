"use strict";


app.controller('OfferPartialRefundController', function ($scope, $notify, $router, $modal, $http, $notifyHttpErrors, $psDownloadModel, controllerParams) {

    $scope.refundAmount = '';
    $scope.refundComment = '';

    $scope.isFullRefund = function() {
        return (Math.round($scope.printAmount * 10) / 10) === (Math.round($scope.refundAmount * 10) / 10);
    }

    /**
     * Click on set as printed
     */
    $scope.submitRefund = function () {
        $http.post($router.psRefundRequest(), {
            attemptId: $scope.attemptId,
            refundAmount: $scope.refundAmount,
            refundComment: $scope.refundComment
        })
            .then(function (response) {
                $notify.success(response.data.message);
                $router.to($router.viewPrintAttemp($scope.attemptId));
            })
            .catch($notifyHttpErrors);
        return false;
    }
});