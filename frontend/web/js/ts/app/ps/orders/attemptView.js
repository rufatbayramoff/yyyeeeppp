"use strict";

app.controller('AttemptViewController', function ($scope, $notify, $notifyHttpErrors, $router, $modal, $http, $psDownloadModel, $storeOrderService, $cncService2, controllerParams) {

    $scope.setOrderStatus = function (attempId, status) {
        return $storeOrderService.setOrderStatus(attempId, status);
    };

    $scope.declineOrderByType = function (attempId, type) {
        $storeOrderService.declineOrderByType(attempId, type);
    };

    /**
     * Open decline modal
     * @param attempId
     * @param fileId
     */
    $scope.downloadFiles = function (attempId, fileId) {
        return $psDownloadModel.downloadFiles(controllerParams.company, attempId, fileId)
            .then(undefined, $notifyHttpErrors);
    };

    /**
     * set order sent with tracking number
     *
     * @param attempId
     * @param isPreorder
     * @param formData
     */
    $scope.setOrderSentWithTrackingNumber = function (attempId, isPreorder, formData) {
        $storeOrderService.setOrderSentWithTrackingNumber(attempId, isPreorder, formData);
    };


    /**
     * Download preorder file or all perorder model files (if fileId not setted)
     * @param {int} preorderId
     * @param {int} [fileId]
     */
    $scope.downloadPreorderFile = function (preorderId, fileId)
    {
        $psDownloadModel.acceptDownloadPolisy(controllerParams.company)
            .then(function () {

                var fileUri = fileId
                    ? '/workbench/cncm/preorder/download-file?preorderId=' + preorderId + '&fileId=' + fileId
                    : '/workbench/cncm/preorder/download-model-files?preorderId=' + preorderId;

                window.open(fileUri, '_self');
            });
    };

    /**
     * Decline preorder
     * @param preorderId
     */
    $scope.declineCncPreorder = function (preorderId)
    {
        $cncService2.openDeclineModal(preorderId, false, false)
            .then($router.reload, function (response) {
                if (response) {
                    $notifyHttpErrors(response);
                }
            });
    };

    $scope.getPostalLabel = function (attempId, l, w, h, weight) {
        return $storeOrderService.getPostalLabel(attempId, l, w, h, weight);
    };

    $scope.printPostalLabel = function (attempId) {
        return $storeOrderService.printPostalLabel(attempId);
    };

    $scope.partialRefund = function (attemptId, printAmount) {
        $modal.open({
            template: '/attempt/partitalRefund.html',
            controller: 'OfferPartialRefundController',
            scope: { attemptId: attemptId, printAmount: printAmount}
        });
    };
});