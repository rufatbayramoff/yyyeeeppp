"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('PsCatalogController', function ($scope, $preorderService, $notify, controllerParams) {


    $scope.preorder = {};
    $scope.uploadedFilesUuids = controllerParams['uploadedFilesUuids'];
    $scope.description = controllerParams['description'];

    /**
     *
     * @param psId
     */
    $scope.openCreatePreorder = function (psId, preorder, productUuid, serviceId) {
        if (preorder) {
            $scope.preorder = preorder;
        }
        return $preorderService.openCreatePreorder(psId, $scope.preorder, productUuid, serviceId, $scope.description, $scope.uploadedFilesUuids);
    }

    $scope.setCurrency = function (currency) {
        $scope.preorder.currency = currency;
    }
});