"use strict";

/**
 * Controller for manage embed
 */
app.controller('PsPrinterEmbedController', function ($scope, $http, $notifyHttpErrors, $router, PsPrinter, $facebookApi, controllerParams, $modal)
{
    $scope.selectedPrinterId = null;

    $('.ps-share__info-show').click(function () {
        var scroll_elTarget = $('.ps-share__info-target');
        var scroll_el = $(this).attr('href');
        if ($(scroll_el).length !== 0) {
            setTimeout(function () {
                $(scroll_el).collapse('show');
            }, 200);
            $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 130}, 500);

        }
        return false;
    });

    if (controllerParams.ps) {
        window.updateEmbedPs = function(){

            var frame = document.getElementById('embedframe');
            var publicLink =  document.getElementById('publicLink');

            if (frame && publicLink) {
                frame.innerHTML = publicLink.value;
            }
        };

        updateEmbedPs();
    }

    var server = controllerParams.server;
    var origUrl = controllerParams.userUrl;

    if (document.getElementById('model-embed__code-input')) {
        document.getElementById('model-embed__code-input').value = '<link href="' + server + '/css/embed-store.css" rel="stylesheet" >'
            + document.getElementById('embedframe').innerHTML.replace(/&amp;/g, '&').trim();
    }

    window.updateEmbed = function(){

        var count = document.getElementById('embed-count').value;
        var emf = $('#embedframe iframe')[0];
        if(!origUrl){
            origUrl = emf.src;
        }
        function addParam(url, param, value) {
            var a = document.createElement('a'), regex = /(?:\?|&amp;|&)+([^=]+)(?:=([^&]*))*/g;
            var match, str = []; a.href = url; param = encodeURIComponent(param);
            while (match = regex.exec(a.search))
                if (param !== match[1]) str.push(match[1]+(match[2]?"="+match[2]:""));

            str.push(param+(value?"="+ encodeURIComponent(value):"=0"));
            a.search = str.join("&");
            return a.href;
        }

        var src = origUrl;

        if(count){
            src = addParam(origUrl, 'count', count);
        }
        emf.src = src;

        document.getElementById('model-embed__code-input').value =
            '<link href="' + server + '/css/embed-store.css" rel="stylesheet" >'
            + document.getElementById('embedframe').innerHTML.replace(/&amp;/g, '&');
    };

    /**
     * Open change ps url popver
     */
    $scope.openChangeUrlPopver = function ()
    {
        $modal.open({
            template : '/app/ps/set-ps-url-modal.html',
            scope : {
                url : controllerParams.ps.url,
                save : function()
                {
                    var scope = this;
                    return $http.post('/mybusiness/company/update-ps-url', {url : scope.url})
                        .then(function(){
                            $router.reload();
                            scope.$dismiss();
                        })
                        .catch($notifyHttpErrors);
                }
            }
        });
    };

    /**
     * @param psPrinterId
     * @param hideReviews
     */
    $scope.showEmbedScript = function (psPrinterId, hideReviews)
    {
        var $reviewsBlock = $('.js-reviews-block'),
            reviewsBlockProp = $reviewsBlock.prop('checked'),
            showObject;

        $reviewsBlock.parent()[hideReviews ? 'hide' : 'show']();

        $scope.selectedPrinterId = psPrinterId;

        $('.js-embed').attr('id', 'publicLink_old').addClass('hidden');

        if (psPrinterId) {
            showObject = reviewsBlockProp ? $('.js-embed-printer-' + psPrinterId) : $('.js-embed-printer-dr-' + psPrinterId)
        } else {
            showObject = reviewsBlockProp ? $('.js-embed-ps') : $('.js-embed-ps-dr');
        }

        showObject.removeClass('hidden').attr('id', "publicLink");

        if(window.updateEmbedPs){
            updateEmbedPs();
        }
    };

    $scope.showEmbedScriptCheckbox = function () {
        $scope.showEmbedScript($scope.selectedPrinterId);
    }
})
;

