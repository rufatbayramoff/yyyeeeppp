"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('OrderExampleController', function ($scope, $measure, $measureConverter)
{
    /**
     * Test models params in cm dimen
     */
    $scope.testModel = {
        filamentVolume: 122.3,
        renderUrl: '/static/order-example.jpg?v=3',
        size: {
            width: 8.9,
            height: 12.1,
            length: 10
        }
    };

    /**
     *
     */
    $scope.printerMeasure = $measure.getByMetric($scope.psPrinter.measure);
    $scope.$watch('psPrinter.measure', function (newValue) {
        $scope.printerMeasure =  $measure.getByMetric(newValue);
    });

    /**
     * Selected material
     */
    $scope.material = $scope.getMaterial($scope.psMaterial.material_id);

    /**
     * Selected color
     */
    $scope.color = $scope.getColor($scope.psMaterial.material_id, $scope.psColor.color_id);

    /**
     * Return weight in printer dimen
     * @returns {number}
     */
    $scope.getWeight = function ()
    {
        return $measureConverter.convert($measure.MEASURE_GR, $measure.getByMetric($scope.printerMeasure.system.defaultTypes.weight), $scope.testModel.filamentVolume * $scope.material.density);
    };

    /**
     * Return cost in printer currency
     * @returns {number}
     */
    $scope.getCost = function ()
    {
        var printPrice;
        if ($scope.psPrinter.measure == 'ml') {
            printPrice = $scope.testModel.filamentVolume * $scope.psColor.price;
        } else {
            printPrice = $scope.getWeight() * $scope.psColor.price;
        }
        return printPrice;
    };

    /**
     * Return true if calculated cost include min order price
     * @returns {boolean}
     */
    $scope.getIsCostIncludeMinOrderPrice = function ()
    {
        if(!$scope.psPrinter.min_order_price){
            return false;
        }
        var printPrice = $scope.getCost();
        return printPrice < $scope.psPrinter.min_order_price;
    };
    
    /**
     * Return size value in printer dimen
     * @param {number} value
     * @returns {number}
     */
    $scope.getSizeDimenValue = function (value)
    {
        return $measureConverter.convert($measure.MEASURE_CM, $measure.getByMetric($scope.printerMeasure.system.defaultTypes.length), value);
    };
});