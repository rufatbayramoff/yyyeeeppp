"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('CertificationController', function ($scope, $resource, $router, $notifyHttpErrors, $http, $modal, controllerParams) {
    /**
     * Printer
     */
    $scope.companyService = controllerParams.companyService;
    if (!$scope.company) {
        $scope.company = controllerParams.company;
    }
    $scope.phoneValidator = {
        validatePhone: function () {
            // stub
        }
    };


    /**
     * Professional test
     */
    $scope.professionalTest = {
        testedPhotos: []
    };


    /**
     *
     */
    $scope.showUploadDocumentModal = function () {
        var modalScope = {

            /**
             * Form for upload document file
             * @type {{documentFile: undefined}}
             */
            form: {
                documentFile: undefined
            },

            /**
             * Upload user docuemnt file
             */
            uploadDocumentFile: function () {
                return $http.post('/mybusiness/company/upload-document', modalScope.form)
                    .then($router.reload)
                    .catch($notifyHttpErrors);
            }
        };

        $modal.open({
            template: '/app/ps/printers/test/upload-document-modal.html',
            scope: modalScope
        });
    };

    /**
     * Save test result
     */
    $scope.saveTest = function (test) {
        return $http.post($router.getPsServiceTestSave($scope.companyService.id), $scope.professionalTest)
            .then(function (response) {
                if (response.data.success) {
                    $router.reload();
                }
            }).catch($notifyHttpErrors);
    };

});