/**
 * Created by analitic on 14.07.17.
 */

"use strict";

app.controller('PsCncCreateController', function ($scope, $q, $notify, $notifyHttpErrors, $router, $http, $timeout, controllerParams, PsCnc, PsMachine, baseSteps) {

    $scope.psCnc = new PsCnc(controllerParams.psCnc);
    $scope.psCnc.json = controllerParams.psCncJson;
    $scope.psMachine = new PsMachine(controllerParams.psMachine);
    $scope.psCnc.formShortMachinesList();

    var steps = baseSteps;
    steps.currentStep = controllerParams.step;
    steps.stepsCount = 4;

    steps.onBeforeStepChange = function (newStep, oldStep) {
        var errors = [];
        switch (oldStep) {
            case 1:
                $scope.psCnc.shortMachinesListToJson();
                errors = $scope.psCnc.validateMachinesShortList();
                break;
            case 2:
                $scope.psCnc.setMaterialsFromJson(editorMaterials.getValue());
                errors = $scope.psCnc.validateMaterials();
                var errorsJson = $scope.jsonEditorMaterial.validate();
                if (errorsJson && errorsJson.length) {
                    errors = [_t('site.printers', 'Please fill in all required fields')];
                }
                break;
            case 3:
                break;
            case 4:
                $scope.psCnc.setValuesFromJson(editor.getValue());
                break;

        }

        if (newStep < oldStep) {
            return true;
        }

        if (!_.isEmpty(errors)) {
            $notify.error(errors);
            return false;
        }
        return true;
    };


    /**
     * On step change
     * @param {int} newStep
     */
    steps.onStepChanged = function (newStep) {
        if (newStep == 1) {
            $scope.psCnc.formShortMachinesList()
        }

        if (newStep == 2) {
            $scope.jsonEditorMaterial.setValue($scope.psCnc.getJsonValue());
        }
        if (newStep == 4) {
            $scope.jsonEditor.setValue($scope.psCnc.getJsonValue());
        }
        if (newStep == 3) {
            $scope.jsonEditor.validate();
            $scope.$broadcast('redrawGoogleMap');
        }
        $scope.currentStep = newStep;
    };


    $scope.steps = steps;

    /**
     *
     * @param suggestAddress
     */
    $scope.openSuggestAddressModal = function (suggestAddress) {
        $modal.open({
            template: '/app/ps/printers/validate-address-modal.html',
            scope: {
                address: suggestAddress,
                confirmAddress: function () {
                    angular.extend($scope.psPrinter.location, suggestAddress);
                    $timeout(function () {
                        $('.js-save-printer').trigger('click');
                    });
                    this.$dismiss();
                }
            }
        });
    };


    var materialsElement = document.getElementById("psCncJsonContainerMaterials");
    var element = document.getElementById("psCncJsonContainer");

    window.jQuery.fn.select2 = false;

    var editorMaterials = new JSONEditor(materialsElement, {
        startval: $scope.psCnc.getJsonValue(),
        theme: 'bootstrapts',
        iconlib: 'bootstrap3',
        disable_collapse: true,
        disable_edit_json: true,
        disable_properties: true, //
        disable_array_delete_all_rows: true,
        disable_array_delete_last_row: true,
        no_additional_properties: true,
        schema: controllerParams.cncSchemaMaterials,
        show_errors: 'always',
        template: 'jsSupport'

    });

    var editor = new JSONEditor(element, {
        startval: $scope.psCnc.getJsonValue(),
        theme: 'bootstrapts',
        iconlib: 'bootstrap3',
        disable_collapse: true,
        disable_edit_json: true,
        disable_properties: true, //
        disable_array_delete_all_rows: true,
        disable_array_delete_last_row: true,
        no_additional_properties: true,
        schema: controllerParams.cncSchema,
        show_errors: 'always',
        template: 'jsSupport'

    });

    $scope.jsonEditorMaterial = editorMaterials;
    $scope.jsonEditor = editor;

    /**
     *
     * @returns {boolean}
     */
    steps.isSaveStep = function () {
        return this.currentStep === 3;
    };


    $scope.saveCnc = function () {
        if (this.currentStep == 4) {
            $scope.psCnc.setValuesFromJson(editor.getValue());
        }
        window.onbeforeunload = null;
        var jsonFormErrors = [];
        var errorsJson = [];
        if (jsonFormErrors && jsonFormErrors.length) {
            errorsJson = [_t('site.printers', 'Please fill in all required fields')];
        }
        var errors = _.union($scope.psCnc.validate(), $scope.psMachine.validateLocation(), errorsJson);
        if (!_.isEmpty(errors)) {
            $notify.error(errors);
            return false;
        }
        return $http.post('/mybusiness/services/save-cnc', {
            psMachine: $scope.psMachine,
            psCnc: $scope.psCnc.getJsonValue()
        })
            .then(function (response) {
                if (typeof(ga) != 'undefined' && ga) {
                    ga('send', 'pageview', '/virtual/cnc/created');
                }

                if (response.data.redirect) {
                    $router.to(response.data.redirect);
                    return;
                }

                $router.toPsPrintersList();
            })
            .catch(function (data) {
                if (data.data.code == "HaveSuggestAddress") {
                    $scope.openSuggestAddressModal(data.data.data);
                    return $q.reject(data);
                }
                $notifyHttpErrors(data);
                return $q.reject(data);
            });
    };

    $scope.addMachine = function () {
        $scope.psCnc.shortMachinesList.push({
            title: '',
            type: 'milling'
        });
    };

    $scope.deleteMachine = function (machineInfo) {
        TS.confirm(_t('site.ps', 'Are you sure you want to delete this item?'), function (result) {
            if (result == 'ok') {
                $scope.psCnc.shortMachinesList = _.reject($scope.psCnc.shortMachinesList, function (el) {
                    return el === machineInfo
                });
                $scope.$apply();
            }
        });
    };

    $scope.upMachine = function (machineInfo) {
        $scope.psCnc.shortMachinesList.moveUp(machineInfo);
    };

    $scope.downMachine = function (machineInfo) {
        $scope.psCnc.shortMachinesList.moveDown(machineInfo);
    };
});
