/**
 * Created by A1 on 09.08.2017.
 */

JSONEditor.plugins.select2.enable = false;

JSONEditor.defaults.themes.bootstrapts = JSONEditor.AbstractTheme.extend({
    getSelectInput: function (options) {
        var el = this._super(options);
        el.className += 'form-control';
        //el.style.width = 'auto';
        return el;
    },
    setGridColumnSize: function (el, size) {
        el.className = 'col-md-' + size;
    },
    afterInputReady: function (input) {
        if (input.controlgroup) return;
        input.controlgroup = this.closest(input, '.form-group');
        if (this.closest(input, '.compact')) {
            input.controlgroup.style.marginBottom = 0;
        }

        // TODO: use bootstrap slider
    },
    getTextareaInput: function () {
        var el = document.createElement('textarea');
        el.className = 'form-control';
        return el;
    },
    getRangeInput: function (min, max, step) {
        // TODO: use better slider
        return this._super(min, max, step);
    },
    //getTableHeaderCell: function (t) {
    //    return this._super(t);
    //},
    getFormInputField: function (type) {
        var el = this._super(type);
        if (type !== 'checkbox') {
            el.className += 'form-control';
        }
        return el;
    },
    getFormControl: function (label, input, description) {
        var group = document.createElement('div');

        if (label && input.type === 'checkbox') {
            group.className += ' checkbox';
            label.appendChild(input);
            label.style.fontSize = '14px';
            group.style.marginTop = '0';
            group.appendChild(label);
            input.style.position = 'relative';
            input.style.cssFloat = 'left';
        }
        else {
            group.className += ' form-group';
            if (label) {
                label.className += ' control-label';
                group.appendChild(label);
            }
            group.appendChild(input);
        }

        if (description) group.appendChild(description);

        return group;
    },
    getIndentedPanel: function () {
        var el = document.createElement('div');
        el.className = 'cnc-edit';
        el.style.paddingBottom = 0;
        return el;
    },
    getFormInputDescription: function (text) {
        var el = document.createElement('p');
        el.className = 'help-block';
        el.innerHTML = text;
        return el;
    },
    getHeader: function (el) {
        var returnValue = JSONEditor.AbstractTheme.prototype.getHeader(el);
        if ((el === '-') ||(el.innerText && el.innerText.substr(0, 1) === '-')) {
            returnValue.className = 'hidden';
        }
        return returnValue;
    },
    //getFormInputLabel: function(el) {
    //    console.log("Q: ", el);
    //    return JSONEditor.AbstractTheme.prototype.getFormInputLabel(el);
    //},
    //getCheckboxLabel: function(el) {
    //    console.log("W: ", el);
    //    return JSONEditor.AbstractTheme.prototype.getCheckboxLabel(el);
    //},
    getHeaderButtonHolder: function () {
        var el = this.getButtonHolder();
        el.style.marginLeft = '10px';
        return el;
    },
    getButtonHolder: function () {
        var el = document.createElement('div');
        el.className = 'cnc-edit__btn-group';
        return el;
    },
    getButton: function (text, icon, title) {
        var el = this._super(text, icon, title);
        el.className += 'btn btn-default';
        return el;
    },
    getTable: function () {
        var el = document.createElement('table');
        el.className = 'table table-bordered';
        return el;
    },

    addInputError: function (input, text) {
        if (!input.controlgroup) return;
        input.controlgroup.className += ' has-error';
        if (!input.errmsg) {
            input.errmsg = document.createElement('p');
            input.errmsg.className = 'help-block errormsg';
            input.controlgroup.appendChild(input.errmsg);
        }
        else {
            input.errmsg.style.display = '';
        }

        input.errmsg.textContent = text;
    },
    removeInputError: function (input) {
        if (!input.errmsg) return;
        input.errmsg.style.display = 'none';
        input.controlgroup.className = input.controlgroup.className.replace(/\s?has-error/g, '');
    },
    getTabHolder: function () {
        var el = document.createElement('div');
        el.innerHTML = "<div class='tabs list-group col-md-2'></div><div class='col-md-10'></div>";
        el.className = 'rows';
        return el;
    },
    getTab: function (text) {
        var el = document.createElement('a');
        el.className = 'list-group-item';
        el.setAttribute('href', '#');
        el.appendChild(text);
        return el;
    },
    markTabActive: function (tab) {
        tab.className += ' active';
    },
    markTabInactive: function (tab) {
        tab.className = tab.className.replace(/\s?active/g, '');
    },
    getProgressBar: function () {
        var min = 0, max = 100, start = 0;

        var container = document.createElement('div');
        container.className = 'progress';

        var bar = document.createElement('div');
        bar.className = 'progress-bar';
        bar.setAttribute('role', 'progressbar');
        bar.setAttribute('aria-valuenow', start);
        bar.setAttribute('aria-valuemin', min);
        bar.setAttribute('aria-valuenax', max);
        bar.innerHTML = start + "%";
        container.appendChild(bar);

        return container;
    },
    updateProgressBar: function (progressBar, progress) {
        if (!progressBar) return;

        var bar = progressBar.firstChild;
        var percentage = progress + "%";
        bar.setAttribute('aria-valuenow', progress);
        bar.style.width = percentage;
        bar.innerHTML = percentage;
    },
    updateProgressBarUnknown: function (progressBar) {
        if (!progressBar) return;

        var bar = progressBar.firstChild;
        progressBar.className = 'progress progress-striped active';
        bar.removeAttribute('aria-valuenow');
        bar.style.width = '100%';
        bar.innerHTML = '';
    }
});


var $each = function (obj, callback) {
    if (!obj || typeof obj !== "object") return;
    var i;
    if (Array.isArray(obj) || (typeof obj.length === 'number' && obj.length > 0 && (obj.length - 1) in obj)) {
        for (i = 0; i < obj.length; i++) {
            if (callback(i, obj[i]) === false) return;
        }
    }
    else {
        if (Object.keys) {
            var keys = Object.keys(obj);
            for (i = 0; i < keys.length; i++) {
                if (callback(keys[i], obj[keys[i]]) === false) return;
            }
        }
        else {
            for (i in obj) {
                if (!obj.hasOwnProperty(i)) continue;
                if (callback(i, obj[i]) === false) return;
            }
        }
    }
};

JSONEditor.defaults.editors.array.prototype.addRow = function (value, initial) {
    var self = this;
    var i = this.rows.length;

    self.rows[i] = this.getElementEditor(i);
    self.row_cache[i] = self.rows[i];

    if (self.tabs_holder) {
        self.rows[i].tab_text = document.createElement('span');
        self.rows[i].tab_text.textContent = self.rows[i].getHeaderText();
        self.rows[i].tab = self.theme.getTab(self.rows[i].tab_text);
        self.rows[i].tab.addEventListener('click', function (e) {
            self.active_tab = self.rows[i].tab;
            self.refreshTabs();
            e.preventDefault();
            e.stopPropagation();
        });

        self.theme.addTab(self.tabs_holder, self.rows[i].tab);
    }

    var controls_holder = self.rows[i].title_controls || self.rows[i].array_controls;

    // Buttons to delete row, move row up, and move row down
    if (!self.hide_delete_buttons) {
        var amsg = this.translate('button_delete_row_title', [self.getItemTitle()]) + '?';
        self.rows[i].delete_button = this.getButton(self.getItemTitle(), 'delete', this.translate('button_delete_row_title', [self.getItemTitle()]));
        self.rows[i].delete_button.className += ' delete';
        self.rows[i].delete_button.setAttribute('data-i', i);
        self.rows[i].delete_button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (!confirm(amsg)) return;
            var i = this.getAttribute('data-i') * 1;

            var value = self.getValue();

            var newval = [];
            var new_active_tab = null;
            $each(value, function (j, row) {
                if (j === i) {
                    // If the one we're deleting is the active tab
                    if (self.rows[j].tab === self.active_tab) {
                        // Make the next tab active if there is one
                        // Note: the next tab is going to be the current tab after deletion
                        if (self.rows[j + 1]) new_active_tab = self.rows[j].tab;
                        // Otherwise, make the previous tab active if there is one
                        else if (j) new_active_tab = self.rows[j - 1].tab;
                    }

                    return; // If this is the one we're deleting
                }
                newval.push(row);
            });
            self.setValue(newval);
            if (new_active_tab) {
                self.active_tab = new_active_tab;
                self.refreshTabs();
            }

            self.onChange(true);
        });

        if (controls_holder) {
            controls_holder.appendChild(self.rows[i].delete_button);
        }
    }

    if (i && !self.hide_move_buttons) {
        self.rows[i].moveup_button = this.getButton('', 'moveup', this.translate('button_move_up_title'));
        self.rows[i].moveup_button.className += ' moveup';
        self.rows[i].moveup_button.setAttribute('data-i', i);
        self.rows[i].moveup_button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var i = this.getAttribute('data-i') * 1;

            if (i <= 0) return;
            var rows = self.getValue();
            var tmp = rows[i - 1];
            rows[i - 1] = rows[i];
            rows[i] = tmp;

            self.setValue(rows);
            self.active_tab = self.rows[i - 1].tab;
            self.refreshTabs();

            self.onChange(true);
        });

        if (controls_holder) {
            controls_holder.appendChild(self.rows[i].moveup_button);
        }
    }

    if (!self.hide_move_buttons) {
        self.rows[i].movedown_button = this.getButton('', 'movedown', this.translate('button_move_down_title'));
        self.rows[i].movedown_button.className += ' movedown';
        self.rows[i].movedown_button.setAttribute('data-i', i);
        self.rows[i].movedown_button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var i = this.getAttribute('data-i') * 1;

            var rows = self.getValue();
            if (i >= rows.length - 1) return;
            var tmp = rows[i + 1];
            rows[i + 1] = rows[i];
            rows[i] = tmp;

            self.setValue(rows);
            self.active_tab = self.rows[i + 1].tab;
            self.refreshTabs();
            self.onChange(true);
        });

        if (controls_holder) {
            controls_holder.appendChild(self.rows[i].movedown_button);
        }
    }

    if (value) self.rows[i].setValue(value, initial);
    self.refreshTabs();
}

JSONEditor.defaults.editors.table.prototype.addRow = function (value) {
    var self = this;
    var i = this.rows.length;

    self.rows[i] = this.getElementEditor(i);

    var controls_holder = self.rows[i].table_controls;

    if (i && !this.hide_move_buttons) {
        self.rows[i].moveup_button = this.getButton('', 'moveup', this.translate('button_move_up_title'));
        self.rows[i].moveup_button.className += ' moveup';
        self.rows[i].moveup_button.setAttribute('data-i', i);
        self.rows[i].moveup_button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var i = this.getAttribute('data-i') * 1;

            if (i <= 0) return;
            var rows = self.getValue();
            var tmp = rows[i - 1];
            rows[i - 1] = rows[i];
            rows[i] = tmp;

            self.setValue(rows);
            self.onChange(true);
        });
        controls_holder.appendChild(self.rows[i].moveup_button);
    }

    if (!this.hide_move_buttons) {
        self.rows[i].movedown_button = this.getButton('', 'movedown', this.translate('button_move_down_title'));
        self.rows[i].movedown_button.className += ' movedown';
        self.rows[i].movedown_button.setAttribute('data-i', i);
        self.rows[i].movedown_button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var i = this.getAttribute('data-i') * 1;
            var rows = self.getValue();
            if (i >= rows.length - 1) return;
            var tmp = rows[i + 1];
            rows[i + 1] = rows[i];
            rows[i] = tmp;

            self.setValue(rows);
            self.onChange(true);
        });
        controls_holder.appendChild(self.rows[i].movedown_button);
    }

    // Buttons to delete row, move row up, and move row down
    if (!this.hide_delete_buttons) {
        var amsg = this.translate('button_delete_row_title_short') + '?';
        self.rows[i].delete_button = this.getButton('', 'delete', this.translate('button_delete_row_title_short'));
        self.rows[i].delete_button.className += ' delete';
        self.rows[i].delete_button.setAttribute('data-i', i);
        self.rows[i].delete_button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (!confirm(amsg)) return;
            var i = this.getAttribute('data-i') * 1;

            var value = self.getValue();

            var newval = [];
            $each(value, function (j, row) {
                if (j === i) return; // If this is the one we're deleting
                newval.push(row);
            });
            self.setValue(newval);
            self.onChange(true);
        });
        controls_holder.appendChild(self.rows[i].delete_button);
    }

    if (value) self.rows[i].setValue(value);
}


JSONEditor.defaults.editors.object.prototype.build = function () {
    var self = this;

    // If the object should be rendered as a table row
    if (this.options.table_row) {
        this.editor_holder = this.container;
        $each(this.editors, function (key, editor) {
            var holder = self.theme.getTableCell();
            self.editor_holder.appendChild(holder);

            editor.setContainer(holder);
            editor.build();
            editor.postBuild();

            if (self.editors[key].options.hidden) {
                holder.style.display = 'none';
            }
            if (self.editors[key].options.input_width) {
                holder.style.width = self.editors[key].options.input_width;
            }
        });
    }
    // If the object should be rendered as a table
    else if (this.options.table) {
        // TODO: table display format
        throw "Not supported yet";
    }
    // If the object should be rendered as a div
    else {
        this.header = document.createElement('span');
        this.header.textContent = this.getTitle();
        this.title = this.theme.getHeader(this.header);
        this.container.appendChild(this.title);
        this.container.style.position = 'relative';

        // Info
        if (this.schema.info) {
            this.info = document.createElement('span');
            this.info.innerHTML = this.schema.info;
            this.title.appendChild(this.info);
        }

        // Edit JSON modal
        this.editjson_holder = this.theme.getModal();
        this.editjson_textarea = this.theme.getTextareaInput();
        this.editjson_textarea.style.height = '170px';
        this.editjson_textarea.style.width = '300px';
        this.editjson_textarea.style.display = 'block';
        this.editjson_save = this.getButton('Save', 'save', 'Save');
        this.editjson_save.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            self.saveJSON();
        });
        this.editjson_cancel = this.getButton('Cancel', 'cancel', 'Cancel');
        this.editjson_cancel.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            self.hideEditJSON();
        });
        this.editjson_holder.appendChild(this.editjson_textarea);
        this.editjson_holder.appendChild(this.editjson_save);
        this.editjson_holder.appendChild(this.editjson_cancel);

        // Manage Properties modal
        this.addproperty_holder = this.theme.getModal();
        this.addproperty_list = document.createElement('div');
        this.addproperty_list.style.width = '295px';
        this.addproperty_list.style.maxHeight = '160px';
        this.addproperty_list.style.padding = '5px 0';
        this.addproperty_list.style.overflowY = 'auto';
        this.addproperty_list.style.overflowX = 'hidden';
        this.addproperty_list.style.paddingLeft = '5px';
        this.addproperty_list.setAttribute('class', 'property-selector');
        this.addproperty_add = this.getButton('add', 'add', 'add');
        this.addproperty_input = this.theme.getFormInputField('text');
        this.addproperty_input.setAttribute('placeholder', 'Property name...');
        this.addproperty_input.style.width = '220px';
        this.addproperty_input.style.marginBottom = '0';
        this.addproperty_input.style.display = 'inline-block';
        this.addproperty_add.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (self.addproperty_input.value) {
                if (self.editors[self.addproperty_input.value]) {
                    window.alert('there is already a property with that name');
                    return;
                }

                self.addObjectProperty(self.addproperty_input.value);
                if (self.editors[self.addproperty_input.value]) {
                    self.editors[self.addproperty_input.value].disable();
                }
                self.onChange(true);
            }
        });
        this.addproperty_holder.appendChild(this.addproperty_list);
        this.addproperty_holder.appendChild(this.addproperty_input);
        this.addproperty_holder.appendChild(this.addproperty_add);
        var spacer = document.createElement('div');
        spacer.style.clear = 'both';
        this.addproperty_holder.appendChild(spacer);


        // Description
        if (this.schema.description) {
            this.description = this.theme.getDescription(this.schema.description);
            this.container.appendChild(this.description);
        }

        // Validation error placeholder area
        this.error_holder = document.createElement('div');
        this.container.appendChild(this.error_holder);

        // Container for child editor area
        this.editor_holder = this.theme.getIndentedPanel();
        this.container.appendChild(this.editor_holder);

        // Container for rows of child editors
        this.row_container = this.theme.getGridContainer();
        this.editor_holder.appendChild(this.row_container);

        $each(this.editors, function (key, editor) {
            var holder = self.theme.getGridColumn();
            self.row_container.appendChild(holder);

            editor.setContainer(holder);
            editor.build();
            editor.postBuild();
        });

        // Control buttons
        this.title_controls = this.theme.getHeaderButtonHolder();
        this.editjson_controls = this.theme.getHeaderButtonHolder();
        this.addproperty_controls = this.theme.getHeaderButtonHolder();
        this.title.appendChild(this.title_controls);
        this.title.appendChild(this.editjson_controls);
        this.title.appendChild(this.addproperty_controls);

        // Show/Hide button
        this.collapsed = false;
        this.toggle_button = this.getButton('', 'collapse', this.translate('button_collapse'));
        this.title_controls.appendChild(this.toggle_button);
        this.toggle_button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (self.collapsed) {
                self.editor_holder.style.display = '';
                self.collapsed = false;
                self.setButtonText(self.toggle_button, '', 'collapse', self.translate('button_collapse'));
            }
            else {
                self.editor_holder.style.display = 'none';
                self.collapsed = true;
                self.setButtonText(self.toggle_button, '', 'expand', self.translate('button_expand'));
            }
        });

        // If it should start collapsed
        if (this.options.collapsed) {
            $trigger(this.toggle_button, 'click');
        }

        // Collapse button disabled
        if (this.schema.options && typeof this.schema.options.disable_collapse !== "undefined") {
            if (this.schema.options.disable_collapse) this.toggle_button.style.display = 'none';
        }
        else if (this.jsoneditor.options.disable_collapse) {
            this.toggle_button.style.display = 'none';
        }

        // Edit JSON Button
        this.editjson_button = this.getButton('JSON', 'edit', 'Edit JSON');
        this.editjson_button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            self.toggleEditJSON();
        });
        this.editjson_controls.appendChild(this.editjson_button);
        this.editjson_controls.appendChild(this.editjson_holder);

        // Edit JSON Buttton disabled
        if (this.schema.options && typeof this.schema.options.disable_edit_json !== "undefined") {
            if (this.schema.options.disable_edit_json) this.editjson_button.style.display = 'none';
        }
        else if (this.jsoneditor.options.disable_edit_json) {
            this.editjson_button.style.display = 'none';
        }

        // Object Properties Button
        this.addproperty_button = this.getButton('Properties', 'edit', 'Object Properties');
        this.addproperty_button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            self.toggleAddProperty();
        });
        this.addproperty_controls.appendChild(this.addproperty_button);
        this.addproperty_controls.appendChild(this.addproperty_holder);
        this.refreshAddProperties();
    }

    // Fix table cell ordering
    if (this.options.table_row) {
        this.editor_holder = this.container;
        $each(this.property_order, function (i, key) {
            self.editor_holder.appendChild(self.editors[key].container);
        });
    }
    // Layout object editors in grid if needed
    else {
        // Initial layout
        this.layoutEditors();
        // Do it again now that we know the approximate heights of elements
        this.layoutEditors();
    }
}

JSONEditor.defaults.editors.table.prototype.build = function () {
    var self = this;
    this.table = this.theme.getTable();
    this.container.appendChild(this.table);
    this.thead = this.theme.getTableHead();
    this.table.appendChild(this.thead);
    this.header_row = this.theme.getTableRow();
    this.thead.appendChild(this.header_row);
    this.row_holder = this.theme.getTableBody();
    this.table.appendChild(this.row_holder);

    // Determine the default value of array element
    var tmp = this.getElementEditor(0, true);
    this.item_default = tmp.getDefault();
    this.width = tmp.getNumColumns() + 2;

    if (!this.options.compact) {
        this.title = this.theme.getHeader(this.getTitle());
        this.container.appendChild(this.title);
        this.title_controls = this.theme.getHeaderButtonHolder();
        this.title.appendChild(this.title_controls);

        // Info
        if (this.schema.info) {
            this.info = document.createElement('span');
            this.info.innerHTML = this.schema.info;
            this.title.appendChild(this.info);
        }

        if (this.schema.description) {
            this.description = this.theme.getDescription(this.schema.description);
            this.container.appendChild(this.description);
        }
        this.panel = this.theme.getIndentedPanel();
        this.container.appendChild(this.panel);
        this.error_holder = document.createElement('div');
        this.panel.appendChild(this.error_holder);
    }
    else {
        this.panel = document.createElement('div');
        this.container.appendChild(this.panel);
    }

    this.panel.appendChild(this.table);
    this.controls = this.theme.getButtonHolder();
    this.panel.appendChild(this.controls);

    if (this.item_has_child_editors) {
        var ce = tmp.getChildEditors();
        var order = tmp.property_order || Object.keys(ce);
        for (var i = 0; i < order.length; i++) {
            var th = self.theme.getTableHeaderCell(ce[order[i]].getTitle());
            if (ce[order[i]].options.hidden) th.style.display = 'none';
            // Info
            if (ce[order[i]].schema.info) {
                var info = document.createElement('span');
                info.innerHTML = ce[order[i]].schema.info;
                th.appendChild(info);
            }

            self.header_row.appendChild(th);
        }
    }
    else {
        self.header_row.appendChild(self.theme.getTableHeaderCell(this.item_title));
    }

    tmp.destroy();
    this.row_holder.innerHTML = '';

    // Row Controls column
    this.controls_header_cell = self.theme.getTableHeaderCell(" ");
    self.header_row.appendChild(this.controls_header_cell);

    // Add controls
    this.addControls();
}

JSONEditor.defaults.editors.array.prototype.build = function () {
    var self = this;

    if (!this.options.compact) {
        this.header = document.createElement('span');
        this.header.textContent = this.getTitle();
        this.title = this.theme.getHeader(this.header);
        this.container.appendChild(this.title);
        this.title_controls = this.theme.getHeaderButtonHolder();
        this.title.appendChild(this.title_controls);

        // Info
        if (this.schema.info) {
            this.info = document.createElement('span');
            this.info.innerHTML = this.schema.info;
            this.title.appendChild(this.info);
        }

        if (this.schema.description) {
            this.description = this.theme.getDescription(this.schema.description);
            this.container.appendChild(this.description);
        }
        this.error_holder = document.createElement('div');
        this.container.appendChild(this.error_holder);

        if (this.schema.format === 'tabs') {
            this.controls = this.theme.getHeaderButtonHolder();
            this.title.appendChild(this.controls);
            this.tabs_holder = this.theme.getTabHolder();
            this.container.appendChild(this.tabs_holder);
            this.row_holder = this.theme.getTabContentHolder(this.tabs_holder);

            this.active_tab = null;
        }
        else {
            this.panel = this.theme.getIndentedPanel();
            this.container.appendChild(this.panel);
            this.row_holder = document.createElement('div');
            this.panel.appendChild(this.row_holder);
            this.controls = this.theme.getButtonHolder();
            this.panel.appendChild(this.controls);
        }
    }
    else {
        this.panel = this.theme.getIndentedPanel();
        this.container.appendChild(this.panel);
        this.controls = this.theme.getButtonHolder();
        this.panel.appendChild(this.controls);
        this.row_holder = document.createElement('div');
        this.panel.appendChild(this.row_holder);
    }

    // Add controls
    this.addControls();
}

JSONEditor.defaults.editors.string.prototype.build = function () {
    var self = this, i;
    if (!this.options.compact) this.header = this.label = this.theme.getFormInputLabel(this.getTitle());
    if (this.schema.description) this.description = this.theme.getFormInputDescription(this.schema.description);

    // Info
    if (this.schema.info) {
        this.info = document.createElement('span');
        this.info.innerHTML = this.schema.info;
        this.label.appendChild(this.info);
    }

    this.format = this.schema.format;
    if (!this.format && this.schema.media && this.schema.media.type) {
        this.format = this.schema.media.type.replace(/(^(application|text)\/(x-)?(script\.)?)|(-source$)/g, '');
    }
    if (!this.format && this.options.default_format) {
        this.format = this.options.default_format;
    }
    if (this.options.format) {
        this.format = this.options.format;
    }

    // Specific format
    if (this.format) {
        // Text Area
        if (this.format === 'textarea') {
            this.input_type = 'textarea';
            this.input = this.theme.getTextareaInput();
        }
        // Range Input
        else if (this.format === 'range') {
            this.input_type = 'range';
            var min = this.schema.minimum || 0;
            var max = this.schema.maximum || Math.max(100, min + 1);
            var step = 1;
            if (this.schema.multipleOf) {
                if (min % this.schema.multipleOf) min = Math.ceil(min / this.schema.multipleOf) * this.schema.multipleOf;
                if (max % this.schema.multipleOf) max = Math.floor(max / this.schema.multipleOf) * this.schema.multipleOf;
                step = this.schema.multipleOf;
            }

            this.input = this.theme.getRangeInput(min, max, step);
        }
        // Source Code
        else if ([
                'actionscript',
                'batchfile',
                'bbcode',
                'c',
                'c++',
                'cpp',
                'coffee',
                'csharp',
                'css',
                'dart',
                'django',
                'ejs',
                'erlang',
                'golang',
                'groovy',
                'handlebars',
                'haskell',
                'haxe',
                'html',
                'ini',
                'jade',
                'java',
                'javascript',
                'json',
                'less',
                'lisp',
                'lua',
                'makefile',
                'markdown',
                'matlab',
                'mysql',
                'objectivec',
                'pascal',
                'perl',
                'pgsql',
                'php',
                'python',
                'r',
                'ruby',
                'sass',
                'scala',
                'scss',
                'smarty',
                'sql',
                'stylus',
                'svg',
                'twig',
                'vbscript',
                'xml',
                'yaml'
            ].indexOf(this.format) >= 0
        ) {
            this.input_type = this.format;
            this.source_code = true;

            this.input = this.theme.getTextareaInput();
        }
        // HTML5 Input type
        else {
            this.input_type = this.format;
            this.input = this.theme.getFormInputField(this.input_type);
        }
    }
    // Normal text input
    else {
        this.input_type = 'text';
        this.input = this.theme.getFormInputField(this.input_type);
    }

    // minLength, maxLength, and pattern
    if (typeof this.schema.maxLength !== "undefined") this.input.setAttribute('maxlength', this.schema.maxLength);
    if (typeof this.schema.pattern !== "undefined") this.input.setAttribute('pattern', this.schema.pattern);
    else if (typeof this.schema.minLength !== "undefined") this.input.setAttribute('pattern', '.{' + this.schema.minLength + ',}');

    if (this.options.compact) {
        this.container.className += ' compact';
    }
    else {
        if (this.options.input_width) this.input.style.width = this.options.input_width;
    }

    if (this.schema.readOnly || this.schema.readonly || this.schema.template) {
        this.always_disabled = true;
        this.input.disabled = true;
    }

    this.input
        .addEventListener('change', function (e) {
            e.preventDefault();
            e.stopPropagation();

            // Don't allow changing if this field is a template
            if (self.schema.template) {
                this.value = self.value;
                return;
            }

            var val = this.value;

            // sanitize value
            var sanitized = self.sanitize(val);
            if (val !== sanitized) {
                this.value = sanitized;
            }

            self.is_dirty = true;

            self.refreshValue();
            self.onChange(true);
        });

    if (this.options.input_height) this.input.style.height = this.options.input_height;
    if (this.options.expand_height) {
        this.adjust_height = function (el) {
            if (!el) return;
            var i, ch = el.offsetHeight;
            // Input too short
            if (el.offsetHeight < el.scrollHeight) {
                i = 0;
                while (el.offsetHeight < el.scrollHeight + 3) {
                    if (i > 100) break;
                    i++;
                    ch++;
                    el.style.height = ch + 'px';
                }
            }
            else {
                i = 0;
                while (el.offsetHeight >= el.scrollHeight + 3) {
                    if (i > 100) break;
                    i++;
                    ch--;
                    el.style.height = ch + 'px';
                }
                el.style.height = (ch + 1) + 'px';
            }
        };

        this.input.addEventListener('keyup', function (e) {
            self.adjust_height(this);
        });
        this.input.addEventListener('change', function (e) {
            self.adjust_height(this);
        });
        this.adjust_height();
    }

    if (this.format) this.input.setAttribute('data-schemaformat', this.format);

    this.control = this.theme.getFormControl(this.label, this.input, this.description);
    this.container.appendChild(this.control);

    // Any special formatting that needs to happen after the input is added to the dom
    window.requestAnimationFrame(function () {
        // Skip in case the input is only a temporary editor,
        // otherwise, in the case of an ace_editor creation,
        // it will generate an error trying to append it to the missing parentNode
        if (self.input.parentNode) self.afterInputReady();
        if (self.adjust_height) self.adjust_height(self.input);
    });

    // Compile and store the template
    if (this.schema.template) {
        this.template = this.jsoneditor.compileTemplate(this.schema.template, this.template_engine);
        this.refreshValue();
    }
    else {
        this.refreshValue();
    }
}

JSONEditor.defaults.editors.select.prototype.build = function () {
    var self = this;
    if (!this.options.compact) this.header = this.label = this.theme.getFormInputLabel(this.getTitle());
    if (this.schema.description) this.description = this.theme.getFormInputDescription(this.schema.description);

    if (this.options.compact) this.container.className += ' compact';

    this.input = this.theme.getSelectInput(this.enum_options);
    this.theme.setSelectOptions(this.input, this.enum_options, this.enum_display);

    if (this.schema.readOnly || this.schema.readonly) {
        this.always_disabled = true;
        this.input.disabled = true;
    }

    this.input.addEventListener('change', function (e) {
        e.preventDefault();
        e.stopPropagation();
        self.onInputChange();
    });

    this.control = this.theme.getFormControl(this.label, this.input, this.description);
    this.container.appendChild(this.control);

    this.value = this.enum_values[0];
}