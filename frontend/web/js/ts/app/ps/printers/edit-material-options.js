"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('PsPrinterEditMaterialsController', function ($scope, $q, $notify, $currency, $modal, $measure, $measureType, Printer)
{
    /**
     * Controller
     */
    var ctrl = this;

    /**
     * Is loaded all needed data
     * @type {boolean}
     */
    $scope.isLoaded = false;

    $scope.$currency = $currency;
    $scope.avaliableMeasures = [$measure.MEASURE_GR, $measure.MEASURE_OZ, $measure.MEASURE_ML];
    $scope.$measure = $measure;
    $scope.$measureType = $measureType;

    if(!$scope.printer || $scope.printer.id != $scope.psPrinter.printer_id)
    {
        /**
         * Information about printer, allowed colors and materials for printer
         * @type {*}
         * @namespace printer.materials
         */
        $scope.printer =  Printer.info({id : $scope.psPrinter.printer_id}, function()
        {
            if($scope.psPrinter.getIsNew() && _.isEmpty($scope.psPrinter.materials))
            {
                $scope.addMaterial();
            }

            $scope.isLoaded = true;
        });
    }
    else
    {
        $scope.isLoaded = true;
    }

    /**
     * Return printer material by id
     * @param materialId
     * @todo move to Printer resource
     */
    $scope.getMaterial = function(materialId)
    {
        return _.find($scope.printer.materials, function(material){ return material.id == materialId});
    };

    /**
     * Return printer color of material
     * @param materialId
     * @param colorId
     * @todo move to Printer resource
     */
    $scope.getColor = function(materialId, colorId)
    {
        var material = $scope.getMaterial(materialId);

        if(!material)
        {
            return undefined;
        }

        return _.find(material.colors, function(color){ return color.id == colorId});
    };

    /**
     * On change material on material model
     * @param psMaterial
     */
    $scope.changeMaterialId = function(psMaterial)
    {
        var material = $scope.getMaterial(psMaterial.material_id);

        psMaterial.colors = [];

        _.each(material.colors, function(color)
        {
            psMaterial.colors.push({
                color_id: color.id
            });
        });
    };

    /**
     * Can add material. Return false if all allowed materials already added
     * @returns {boolean}
     */
    $scope.canAddMaterial = function()
    {
        return !_.isEmpty(ctrl.getUnaddedMaterials());
    };

    /**
     * Add material
     */
    $scope.addMaterial = function()
    {
        var material = _.first(ctrl.getUnaddedMaterials()),
            psMaterial = {
                material_id : material.id,
                colors : [],
                $$new : true
            };
        var firstColor = _.first(material.colors);
        let colorId = firstColor.id;
        for (var key in material.colors) {
            if (!material.colors.hasOwnProperty(key)) {
                continue;
            }
            let color = material.colors[key];
            if (color.code === 'White') {
                colorId = color.id;
            }
        }

        psMaterial.colors.push({
            color_id: colorId
        });

        $scope.psPrinter.materials.push(psMaterial);
    };

    /**
     * Return array of materials, who may be selected for new material (dropdown of color)
     * @param psMaterial
     * @returns {Array}
     */
    $scope.getSelectableMaterials = function(psMaterial)
    {
        var addedMaterialIds = _.without(_.pluck($scope.psPrinter.materials, 'material_id'), psMaterial.material_id);
        var r = _.filter($scope.printer.materials, function(material){return !_.contains(addedMaterialIds, material.id) });
        return _.sortBy(r, 'filament_title');
    };

    /**
     * Can add color to psMaterial
     * @param psMaterial
     */
    $scope.canAddColor = function (psMaterial)
    {
        return !_.isEmpty(ctrl.getUnaddedColors(psMaterial));
    };

    /**
     * Add color to psMaterial
     * @param psMaterial
     */
    $scope.addColor = function(psMaterial)
    {
        var psColor = {
            color_id: null
        };

        psMaterial.colors.push(psColor);
    };

    /**
     * Return array of colors, who may be selected for new color (dropdown of color)
     * @param psMaterial
     * @param psColor
     * @returns {Array}
     */
    $scope.getSelectableColors = function(psMaterial, psColor)
    {
        var addedColorsIds = _.without(_.pluck(psMaterial.colors, 'color_id'), psColor.color_id);
        return _.filter($scope.getMaterial(psMaterial.material_id).colors, function(color){return !_.contains(addedColorsIds, color.id) });
    };

    /**
     * Delete material
     * @param psMaterial
     */
    $scope.deleteMaterial = function (psMaterial)
    {
        _.remove($scope.psPrinter.materials, psMaterial);
    };

    /**
     * Delete color
     * @param psMaterial
     * @param psColor
     */
    $scope.deleteColor = function (psMaterial, psColor)
    {
        _.remove(psMaterial.colors, psColor);

        if(_.isEmpty(psMaterial.colors))
        {
            $scope.deleteMaterial(psMaterial)
        }
    };

    /**
     * Open modal for set price for all materials and colors
     */
    $scope.openSetPriceModal = function(psMaterial)
    {
        $modal.open({
            template : '/app/ps/printers/set-price-modal.html',
            scope : {

                /**
                 * Price input value
                 */
                price : null,

                /**
                 * Apply price to all colors of printer and close modal
                 */
                applyPrice : function()
                {
                    var price = this.price,
                        colors = _.flatten(_.pluck($scope.psPrinter.materials, 'colors'));
                    if(psMaterial){
                        colors = psMaterial.colors;
                    }

                    _.each(colors, function(color)
                    {
                        color.price = price;
                    });

                    this.$dismiss();
                }
            }
        });
    };

    /**
     * Return array of unadded materials
     * @returns {*}
     */
    ctrl.getUnaddedMaterials = function()
    {
        var addedMaterialsIds = _.pluck($scope.psPrinter.materials, 'material_id');
        return _.filter($scope.printer.materials, function(material)
        {
            return !_.contains(addedMaterialsIds, material.id);
        });
    };

    /**
     * Return array of unadded colors for psMaterial
     * @param psMaterial
     */
    ctrl.getUnaddedColors = function (psMaterial)
    {
        var addedColorsIds = _.pluck(psMaterial.colors, 'color_id');
        return _.filter($scope.getMaterial(psMaterial.material_id).colors, function(color)
        {
            return !_.contains(addedColorsIds, color.id);
        });
    };

    /**
     * @todo Refactore after improve UI
     */
    $scope.psPrinter.$$minOrderPriceEnabled = !!$scope.psPrinter.min_order_price;

    /**
     * @todo Refactore after improve UI
     */
    $scope.$watch('psPrinter.$$minOrderPriceEnabled', function(value)
    {
        if(!value && $scope.psPrinter)
        {
            $scope.psPrinter.min_order_price = null;
        }
    });

    /**
     * Update printer on saved
     * @todo Refactore after improve UI
     */
    $scope.$on('psPrinterSaved', function ()
    {
        $scope.psPrinter.$$minOrderPriceEnabled = !!$scope.psPrinter.min_order_price;
    })

    /**
     * Group of objects for order example
     * @type {null}
     */
    var orderExampleCurrentElement = null;
    $scope.orderExamples = {
        needShowOrderExample : function (psColor)
        {
            return this.currentColor === psColor && psColor.color_id && psColor.price;
        },
        showOrderExampleTrigger : function ($event, psColor)
        {
            this.currentColor = psColor;
            orderExampleCurrentElement = $($event.target).closest('.oneColorRow');
        },
        currentColor : null
    };

    /**
     * Click on body for hide order example
     */
    $('body').click(function (event) {
        if(orderExampleCurrentElement){
            var target = $(event.target);

            if(target.is(orderExampleCurrentElement) || orderExampleCurrentElement.find(event.target).length > 0){
                return;
            }

            $scope.orderExamples.currentColor = null;
            $scope.$digest();
        }
    });
});