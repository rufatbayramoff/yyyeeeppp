"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('PsPrinterEditPrinterController', function ($rootScope, $scope, $q, $notify, Printer, controllerParams)
{
    /**
     * Controller
     */
    var ctrl = this;

    /**
     * Is loaded all needed data
     * @type {boolean}
     */
    $scope.isLoaded = true;

    /**
     * Information about all allowed printers
     * @type {*}
     */
    $scope.printers = _.map(controllerParams.printers, function(printer){ return new Printer(printer)});

    /**
     * Current selected model of printer
     * @type {Printer|undefined}
     */
    $scope.currentPrinter = undefined;

    $scope.form = {};
    $scope.PROPERTY_ID_BUILD_VOLUME = Printer.PROPERTY_ID_BUILD_VOLUME;

    /**
     * Change printer from allowed list
     */
    $scope.onPrinterChange = function()
    {
        ctrl.updateCurrentPrinter();
        $scope.psPrinter.title = $scope.currentPrinter.title;
        $scope.psPrinter.materials = [];
    };


    /**
     *
     */
    this.updateCurrentPrinter = function()
    {
        if($scope.psPrinter.printer_id)
        {
            $rootScope.selectedPrinter = $scope.currentPrinter = _.find($scope.printers, function (printer) { return printer.id == $scope.psPrinter.printer_id;});
            ctrl.updatePrinterProperties();

            // if new printer
            if(!$scope.psPrinter.id) {
                $scope.psPrinter.redifinedProperties = [];
                $scope.psPrinter.positional_accuracy = $scope.currentPrinter.maxPositionalAccuracy;
            }

        }
    };


    /**
     * Update printer properties
     */
    this.updatePrinterProperties = function()
    {
        var layerResolutionHightProperty = $scope.currentPrinter.getPropertyById(Printer.PROPERTY_ID_LAYER_RESOLUTION_HIGHT),
            buildVolumeProperty = $scope.currentPrinter.getPropertyById(Printer.PROPERTY_ID_BUILD_VOLUME);

        $scope.printerProperties = {
            build_volume: buildVolumeProperty ? buildVolumeProperty.value : undefined,
            materials: $scope.currentPrinter.materialsList,
            layer_resolution_hight: layerResolutionHightProperty ? layerResolutionHightProperty.value : undefined,
            technology: $scope.currentPrinter.technology.title
        };

        $scope.redefinedPorperties = {};

        _.each($scope.currentPrinter.getEditableProperties(), function(printerProperty)
        {
            var redefinedProperty = $scope.psPrinter.getRedifinedPropertyByPropertyId(printerProperty.property_id);
            $scope.redefinedPorperties[printerProperty.property_id] = redefinedProperty ? redefinedProperty.value : printerProperty.value;
        });

        $scope.form.showRedefinedProperties = !_.isEmpty($scope.psPrinter.redifinedProperties);

    };


    /**
     * Update redefined properties on change
     * @param printerProperty
     */
    $scope.onRedefinedPropertyChange = function(printerProperty)
    {
        var value = $scope.redefinedPorperties[printerProperty.property_id],
            isNoDefault = value != printerProperty.value,
            psPrinterProperty = $scope.psPrinter.getRedifinedPropertyByPropertyId(printerProperty.property_id);

        if(isNoDefault)
        {
            if(psPrinterProperty)
            {
                psPrinterProperty.value = value;
            }
            else
            {
                $scope.psPrinter.redifinedProperties.push({property_id : printerProperty.property_id, value : value});
            }
        }
        else
        {
            if(psPrinterProperty)
            {
                _.remove($scope.psPrinter.redifinedProperties, psPrinterProperty);
            }
        }
    };


    ctrl.updateCurrentPrinter();

})

.directive('buildVolumeInput', function ($timeout) {

    var pattern = /^(\d{1,4}) x (\d{1,4}) x (\d{1,4}) mm$/;

    return {
        restrict : 'E',
        template : `
           <div class="form-control">

                <input ng-model="width"
                       type="number"
                       min="1"
                       max="9999"
                       step="1"
                       maxlength="4"
                       class="form-control input-sm"> x
    
                <input ng-model="height"
                       type="number"
                       min="1"
                       max="9999"
                       step="1"
                       maxlength="4"
                       class="form-control input-sm"> x
    
                <input ng-model="length"
                       type="number"
                       min="1"
                       max="9999"
                       step="1"
                       maxlength="4"
                       class="form-control input-sm"> mm
    
            </div>
        `,
        scope : {
            volume : '=',
            change : '&'
        },
        link : function (scope, element) {

            scope.width = scope.height = scope.length = undefined;

            element.find('input').on('keyup', function () {
                if (this.value.length > 4) {
                    $(this).val(this.value.slice(0, 4));
                    scope.$apply();
                }
            });


            var match = scope.volume
                ? scope.volume.match(pattern)
                : undefined;

            if (match) {

                scope.width = +match[1];
                scope.height = +match[2];
                scope.length = +match[3];
            }

            scope.$watchGroup(['width', 'height', 'length'], function () {

                var value = scope.width + ' x ' + scope.height + ' x ' + scope.length + ' mm';

                if (pattern.test(value)) {
                    scope.volume = value;
                    element.removeClass('has-error')
                }
                else {
                    scope.volume = undefined;
                    element.addClass('has-error')
                }

                $timeout(scope.change);
            });
        }
    };



});