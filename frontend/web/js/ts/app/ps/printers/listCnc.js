"use strict";

/**
 * Controller for manage company printer materials
 */
app.controller('PsCncListController', function ($scope, $http, $notifyHttpErrors, $notifyHttp, $router, controllerParams) {

    $scope.isVisible = controllerParams.psVisibility;
    $scope.onPsMachineVisibilityChange = function (psMachineId) {
        var visibilitySelected = $scope.isVisible['m'+psMachineId];
        $http.post('/mybusiness/services/change-visibility?psMachineId='+psMachineId, {visibility: visibilitySelected})
            .then(function (response) {
                $notifyHttp(response);
            })
            .catch(function (response) {
                $notifyHttpErrors(response);
            });
    };



    $scope.readedCncHits = function ()
    {
        return $http.post('/mybusiness/services/read-cnc-hints', {})
            .then(function () {
                $('.cnc_hints').remove();
            }, $notifyHttpErrors);
    }
});