"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('PsPrinterListController', function ($scope, $http, $notifyHttpErrors, $notifyHttp, $router, PsPrinter, $element, $modal, controllerParams) {
    /**
     *
     * @type {{}}
     */
    $scope.prinerEnabled = {};

    $scope.isVisible = {};

    $('[data-toggle="popover"]').popover({
            content: function () {
                var content = $(this).find('.popover-content').clone();
                return content.removeClass('hide');
            }
        }
    );

    /**
     * @param psPrinterId
     */
    $scope.onPrinterEnabledChange = function (psPrinterId) {
        var status = $scope.prinerEnabled[psPrinterId];

        $scope.printerStatusChangeView(psPrinterId, status);

        $http.post('/mybusiness/edit-printer/change-printer-status', {status: status}, {params: {id: psPrinterId}})
            .then(function (response) {
                    $notifyHttp(response);
                }
            )
            .catch(function (response) {
                $notifyHttpErrors(response);
                $scope.prinerEnabled[psPrinterId] = $scope.prinerEnabled[psPrinterId] == PsPrinter.USTATUS_ACTIVE ? PsPrinter.USTATUS_INACTIVE : PsPrinter.USTATUS_ACTIVE;
            });
    };

    /**
     * @param psPrinterId
     * @param status
     */
    $scope.printerStatusChangeView = function(psPrinterId, status) {
        var myEl = angular.element( document.querySelector(('.cardprinter-'+psPrinterId)) );
        if(status=='active'){
            if(myEl.hasClass('printer-status-rejected') || myEl.hasClass('printer-status-banned') || myEl.hasClass('printer-status-new')){

            }else{
                myEl.removeClass('my-ps-printers__card--disabled');
            }
        }else{
            myEl.addClass('my-ps-printers__card--disabled');
        }
    };

    /**
     * @param status
     */
    $scope.onPrinterEnabledChangeAll = function(status) {
        for (var psPrinterId in $scope.prinerEnabled) {
            if (!$scope.prinerEnabled.hasOwnProperty(psPrinterId)) {
                continue;
            }

            $scope.prinerEnabled[psPrinterId] = status;
            $scope.printerStatusChangeView(psPrinterId, status);
        }

        $http.post('/mybusiness/edit-printer/change-printer-status-all', {status: status})
            .then(function (response) {
                $notifyHttp(response);
            })
            .catch(function (response) {
                $notifyHttpErrors(response);
            });
    };

    /**
     * Send test order
     * @param psPrinterId
     */
    $scope.sendTestOrder = function (psPrinterId) {
        return $http.post('/mybusiness/edit-printer/send-test-order', {}, {params: {id: psPrinterId}})
            .then(function () {
                $router.toPsPrintRequests();
            })
            .catch($notifyHttpErrors);
    };

    /**
     * Open modal for offer test order
     */
    $scope.showTestOrderOfferModal = function () {
        $modal.open({
            template: '/app/ps/printers/test-order-offer.html',
            scope: {
                createTestOrder: function () {
                    /** @namespace controllerParams.firstPrinterWithoutTestOrderId */
                    return $scope.sendTestOrder(controllerParams.firstPrinterWithoutTestOrderId);
                }
            }
        });
    };

    $scope.showReviewModal = function (psPrinterId, printerTitle, resultFunction) {

        var cancelText = _t('site.ps', 'Cancel');

        if (typeof (resultFunction) == 'undefined') {
            resultFunction = function ($result) {
                if ($result) {
                    $router.reload();
                }
            }
        }
        $modal.open({
            template: '/app/services/reviewPrinterModal',
            scope: {
                psPrinterId: psPrinterId,
                printerTitle: printerTitle,
                form: {
                    rating_speed: undefined,
                    rating_quality: undefined,
                    rating_communication: undefined,
                    comment: undefined,
                    cancelText: cancelText
                },
            },
            controller: function ($scope) {
                /**
                 *
                 */
                $scope.validateForm = function () {
                    var errors = [];

                    if (!$scope.form.rating_speed) {
                        errors.push(_t('site.ps', 'Please enter speed rating'));
                    }

                    if (!$scope.form.rating_quality) {
                        errors.push(_t('site.ps', 'Please enter quality rating'));
                    }

                    if (!$scope.form.rating_communication) {
                        errors.push(_t('site.ps', 'Please enter communication rating'));
                    }

                    if ($scope.form.files.length > 10) {
                        errors.push(_t('site.ps', 'You can upload max 10 images'));
                    }

                    if ($scope.form.comment && $scope.form.comment.length > 1000) {
                        errors.push(_t('site.ps', 'Comment can contain no more than 1000 characters'));
                    }

                    return errors;
                };

                $scope.save = function () {
                    var errors = $scope.validateForm();

                    if (!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }

                    return $http.post('/workbench/order/add-review', $scope.form, {params: {orderId: orderId}})
                        .then(function (response) {
                            $scope.$dismiss();
                            return response.data.rating >= 4
                                ? $shareService.shareReview(response.data.id)
                                : $q.resolve(true);
                        })
                        .then(resultFunction)
                        .catch($notifyHttpErrors);
                };
                $scope.cancel = function () {
                    resultFunction(false);
                }
            }
        });
    }

    /** @namespace controllerParams.needShowTestOrderOffer */
    if (controllerParams.needShowTestOrderOffer) {
        $scope.showTestOrderOfferModal();
    }
})
    .controller('PsPrinterListHeaderController', function ($scope, $user, controllerParams, $facebookApi) {

        $scope.facebookWidget = function () {
            $facebookApi.addApplicationToPageModal();
        }
    });
