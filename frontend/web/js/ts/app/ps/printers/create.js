"use strict";

/**
 * Controller for manage printservice printer materials
 * @param {{psPrinter: Object,previousPsMachine: ?Object}} controllerParams
 */
app.controller('PsPrinterCreateController', function ($rootScope, $scope, $q, $notify, $notifyHttpErrors, $router, $modal, $timeout, $http, controllerParams, PsPrinter, PsMachine, baseSteps)
{

    $scope.psPrinter = new PsPrinter(controllerParams.psPrinter);
    $scope.psMachine = new PsMachine(!_.isEmpty(controllerParams.previousPsMachine) ? controllerParams.previousPsMachine : controllerParams.psMachine);

    var stepUrlHashes = {
        1 : "printer",
        2 : "material",
        3 : "delivery"
    };


    var steps = baseSteps;
    steps.stepsCount = 3;

    steps.onBeforeStepChange = function(newStep, oldStep)
    {
        if(newStep < oldStep)
        {
            return true;
        }

        var errors = [];

        switch (oldStep)
        {
            case 1:
                errors = $scope.psPrinter.validatePrinter($rootScope.selectedPrinter);
                break;

            case 2:
                errors = $scope.psPrinter.validateMaterialsAndColors();
                break;
        }

        if(!_.isEmpty(errors))
        {
            $notify.error(errors);
            return false;
        }

        return true;
    };

    /**
     * On step change
     * @param {int} newStep
     */
    steps.onStepChanged = function (newStep)
    {
        $router.setHash(stepUrlHashes[newStep]);
        if(window.__insp){
            window.__insp.push(["virtualPage"]);
        }
    };

    $scope.steps = steps;
    steps.onStepChanged(steps.currentStep);

    /**
     * Save printer
     * @returns {boolean}
     */
    $scope.savePrinter = function()
    {
        var errors = _.union($scope.psMachine.validateDelvieryTypes(), $scope.psMachine.validateLocation());

        if(!_.isEmpty(errors))
        {
            $notify.error(errors);
            return false;
        }

        return $http.post('/mybusiness/services/create-printer', {
            psMachine: $scope.psMachine,
            psPrinter: $scope.psPrinter
        })
            .then(
                function () {
                    if (typeof(ga) !== 'undefined' && ga) {
                        ga('send', 'pageview', '/virtual/printer/created');
                    }
                    $router.toPsPrintersList();
                },
                function (data){

                    if(data.data.code === "HaveSuggestAddress"){
                        $scope.openSuggestAddressModal(data.data.data);
                        return $q.reject(data);
                    }

                    if(data.data.code === "DeliveryAddressFailed"){
                        $scope.deliveryAddressFailed(data.data.message);
                        return $q.reject(data);
                    }

                    $notifyHttpErrors(data);
                    return $q.reject(data);
                }
            );

    };


    /**
     *
     * @param suggestAddress
     */
    $scope.openSuggestAddressModal = function (suggestAddress)
    {
        $modal.open({
            template : '/app/ps/printers/validate-address-modal.html',
            scope : {
                address : suggestAddress,
                confirmAddress : function ()
                {
                    if (typeof($scope.psMachine.location) === 'undefined') {
                        $scope.psMachine.location = {};
                    }
                    angular.extend($scope.psMachine.location, suggestAddress);
                    $timeout(function () {
                        $('.js-save-printer').trigger('click');
                    });
                    this.$dismiss();
                }
            }
        });
    };

    /**
     * @param errorMessage
     */
    $scope.deliveryAddressFailed = function (errorMessage)
    {
        $modal.open({
            template : '/app/ps/printers/delivery-address-failed-modal.html',
            scope : {
                errorMessage : errorMessage
            }
        });
    }

});