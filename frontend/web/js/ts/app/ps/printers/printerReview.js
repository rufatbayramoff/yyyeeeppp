app.controller('PrinterReview', function ($scope, $http, $notifyHttpErrors, $notify, $notifyHttp, $router, $modal, controllerParams) {

    $scope.psPrinterId = controllerParams['psPrinterId'];
    $scope.printerTitle = controllerParams['printerTitle'];
    $scope.form = controllerParams['form'];


    var validateScores = [
        'print_quality'
        , 'reliability'
        , 'ease_of_use'
        , 'failure_rate'
        , 'running_expenses'
        , 'software'
        , 'build_quality'
        , 'value'
        , 'customer_service'
        , 'community'
    ]
    $scope.validateForm = function () {
        var errors = [];

        if (!$scope.psPrinterId) {
            errors.push(_t('site.ps', 'Printer not specified'));
        }
        _.each(validateScores, function(code){
            if (!$scope.form[code] && errors.length==0) {
                errors.push(_t('site.ps', 'Please enter all star ratings'));
            }
        });
        if (!$scope.form.comments) {
            errors.push(_t('site.ps', 'Please enter comments or feedback'));
        }
        if ($scope.form.comments && $scope.form.comments.length > 4000) {
            errors.push(_t('site.ps', 'Comments can contain no more than 4000 characters'));
        }
        return errors;
    };

    $scope.save = function (resultFunction) {
        if (typeof (resultFunction) == 'undefined') {
            resultFunction = function ($result) {
                if ($result) {
                    $router.to('/mybusiness/services');
                }
            }
        }
        var errors = $scope.validateForm();

        if (!_.isEmpty(errors)) {
            $notify.error(errors);
            return false;
        }

        var psPrinterId = $scope.psPrinterId;

        return $http.post('/mybusiness/services/add-review', $scope.form, {params: {psPrinterId: psPrinterId}})
            .then(function (response, $q) {
                if(response.data.success){
                    $notify.success(response.data.message);
                }else{
                    $notify.error(response.data);
                }
                return true;
            })
            .then(resultFunction)
            .catch($notifyHttpErrors);
    };

});