"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('PsPrinterEditController', function ($scope, $q, $notify, $notifyHttpErrors, $router, $modal, $timeout, controllerParams, PsMachine, PsPrinter, Printer)
{
    $scope.ACTION_EDIT_PRINTER = 'edit-printer';
    $scope.ACTION_TEST = 'test';

    $scope.psPrinter = new PsPrinter(controllerParams.psPrinter);
    $scope.psMachine = new PsMachine(controllerParams.psMachine);
    $scope.printer = new Printer(controllerParams.printer);
    $scope.action = controllerParams.action;


    /**
     * Delete printer
     */
    $scope.deletePrinter = function()
    {
        TS.confirm(_t('site.ps', 'Are you sure you want to delete this printer?'), function(btn){
            if(btn=='ok'){
                return $scope.psPrinter.$delete()
                    .then(function()
                    {
                        $router.toPsPrintersList();
                    })
                    .catch($notifyHttpErrors);
            }
        });
        return null;
    };

    /**
     * Save printer
     * @returns {boolean}
     */
    $scope.savePrinter = function()
    {
        var errors, promise;

        switch(controllerParams.action)
        {
            case $scope.ACTION_EDIT_PRINTER :
                errors = $scope.psPrinter.validatePrinter($scope.printer);
                break;

            case 'edit-material-options':
                errors = $scope.psPrinter.validateMaterialsAndColors();
                break;

            case 'edit-delivery-options':
                errors = _.union($scope.psMachine.validateDelvieryTypes(), $scope.psMachine.validateLocation());
                break;

            default:
                throw "Undefined action";
        }

        if(!_.isEmpty(errors))
        {
            $notify.error(errors);
            return false;
        }
        // if printer is not enabled
        if($scope.psPrinter.user_status === PsPrinter.USTATUS_INACTIVE || $scope.psPrinter.user_status===null)
        { 
            TS.confirm(_t('site.ps', 'Your printer status is currently inactive.  Make it active?'), function(btn){
                if(btn==='ok'){
                    $scope.psPrinter.user_status = PsPrinter.USTATUS_ACTIVE;
                }else{
                     $scope.psPrinter.user_status = PsPrinter.USTATUS_INACTIVE;
                }
                return updatePromise();
            }, {
                confirm : _t('site.common', 'Yes'),
                dismiss: _t('site.common', 'No')
            });
        }else{
            return updatePromise();
        }
        
        function updatePromise()
        {
            switch(controllerParams.action)
            {
                case $scope.ACTION_EDIT_PRINTER :
                    promise = $scope.psPrinter.$updatePrinter();
                    break;

                case 'edit-material-options':
                    promise = $scope.psPrinter.$updateMaterials();
                    break;

                case 'edit-delivery-options':
                    promise = $scope.psMachine.$updateDelivery();
                    break;

                default:
                    throw "Undefined action";
            }
            promise
                .then(function()
                {
                    $scope.$broadcast('psPrinterSaved');
                    $router.toPsPrintersList();
                })
                .catch(function (data)
                {
                    if(data.data.code == "HaveSuggestAddress"){
                        $scope.openSuggestAddressModal(data.data.data);
                        return $q.reject(data);
                    }

                    if(data.data.code === "DeliveryAddressFailed"){
                        $scope.deliveryAddressFailed(data.data.message);
                        return $q.reject(data);
                    }

                    $notifyHttpErrors(data);
                    return $q.reject(data);
                });

            return promise;
        }
    };

    /**
     *
     * @param suggestAddress
     */
    $scope.openSuggestAddressModal = function (suggestAddress)
    {
        $modal.open({
            template : '/app/ps/printers/validate-address-modal.html',
            scope : {
                address : suggestAddress,
                confirmAddress : function ()
                {
                    angular.extend($scope.psMachine.location, suggestAddress);
                    $timeout(function () {
                        $('.js-save-printer').trigger('click');
                    });
                    this.$dismiss();
                }
            }
        });
    };

    /**
     * @param errorMessage
     */
    $scope.deliveryAddressFailed = function (errorMessage)
    {
        $modal.open({
            template : '/app/ps/printers/delivery-address-failed-modal.html',
            scope : {
                errorMessage : errorMessage
            }
        });
    }
});
