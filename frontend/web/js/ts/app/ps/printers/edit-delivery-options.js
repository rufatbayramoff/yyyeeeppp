"use strict";

/**
 * Controller for manage printservice printer delivery options
 * @param {*} $scope.psMachine
 */
app.controller('PsMachineEditDeliveryOptionsController', function ($scope, $element, $timeout, $http, $geo, $notify, $notifyHttpErrors, $q,
                                                                   $currency, $maps, deliveryTypes, carriersDomestic, carriersIntl, defaultCarrier, controllerParams) {
    var DELIVERY_TYPE_INTERNATIONAL_ID = 5;
    var DELIVERY_TYPE_DOMESTIC_ID = 2;
    var CARRIER_MYSELF = 'myself';
    var CARRIER_TREATSTOCK = 'treatstock';

    // reporting of Google maps error
    $timeout(function () {
        if (window.ga && (!window.google || !window.google.maps)) {
            window.ga('send', {
                hitType: 'event',
                eventCategory: "errors",
                eventAction: "ps.nomap",
                eventLabel: "Google map not loaded"
            });
        }
    }, 10000);

    /**
     * Controller
     */

    $scope.$geo = $geo;

    $scope.country = controllerParams.country;

    $timeout(function () {
        $scope.countries = $scope.fillEuropaCountries(controllerParams.deliveryCountries);
    }, 100);

    var ctrl = this;
    $scope.$currency = $currency;
    $scope.$watch('psMachine.location.country_id', function () {
        $scope.updateCarriers();
    });

    $scope.updateCarriers = function () {
        $scope.country = $geo.getCountryById($scope.psMachine.location.country_id);
        // carrier myself
        /** @namespace country.is_easypost_domestic */
        carriersDomestic[0].is_active = $scope.country.is_easypost_domestic;
        // carrier treatstock
        /** @namespace country.is_easypost_intl */
        carriersIntl[0].is_active = $scope.country.is_easypost_intl;
    };
    $scope.getCarriers = function (deliveryType) {
        if (!carriersDomestic || _.isEmpty(carriersDomestic)) {
            throw "carriers not loaded";
        }
        if (deliveryType.code === 'standard') {
            return carriersDomestic;
        }
        return carriersIntl;
    };
    /**
     * Show locality on map
     */
    $scope.showOnMap = function () {
        $maps.geocode($geo.stringifyAddress($scope.psMachine.location)).$promise
            .then(function (addresses) {
                var address = addresses[0];

                $scope.psMachine.location.lat = address.lat;
                $scope.psMachine.location.lon = address.lon;

                $scope.map.setAddress($scope.psMachine.location);
                $scope.autocomplete.setAddress($scope.psMachine.location);

                if (address.$$approximately) {
                    var approximateAddresses = _.filter(addresses, function (address) {
                        return address.$$homePrecision
                    });

                    if (!_.isEmpty(approximateAddresses)) {
                        $scope.approximateAddresses = approximateAddresses;
                    } else {
                        $scope.approximateAddresses = undefined;
                    }
                } else {
                    $scope.approximateAddresses = undefined;
                }
            })
            .catch(function () {
                $notify.error(_t('site.ps', 'Incorrect address'));
            });
    };

    /**
     * On change address on map
     * @param address
     */
    $scope.onMapMarkerPositionChange = function (address) {
        ctrl.cancelShowmapTimeout();
        angular.extend($scope.psMachine.location, address);
        $scope.autocomplete.setAddress(address);
    };

    /**
     * On change address from atocomplete
     * @param address
     */
    $scope.onAutocompleteAddressChange = function (address) {
        ctrl.cancelShowmapTimeout();
        angular.extend($scope.psMachine.location, address);
        $scope.map.setAddress(address);
    };

    /**
     * On change address from form
     */
    $scope.onLocationFormChange = function () {

        $scope.psMachine.location.lat = $scope.psMachine.location.lon = undefined;
        $scope.updateCarriers();
        ctrl.startShowmapTimeout();
    };

    /**
     * Set address from approximate list
     * @param address
     */
    $scope.selectApproximateAddress = function (address) {
        ctrl.cancelShowmapTimeout();
        angular.extend($scope.psMachine.location, address);
        $scope.map.setAddress(address);
        $scope.autocomplete.setAddress(address);
        $scope.approximateAddresses = undefined;
    };

    /**
     * Start timeout for make show on map after address form change
     */
    ctrl.startShowmapTimeout = function () {
        this.cancelShowmapTimeout();
        this.showmapTimeout = $timeout(function () {
            $scope.showOnMap();
            ctrl.showmapTimeout = undefined;
        }, 3000);
    };

    /**
     * Cancel timeout for make show on map
     */
    ctrl.cancelShowmapTimeout = function () {
        if (ctrl.showmapTimeout) {
            $timeout.cancel(ctrl.showmapTimeout);
        }
        ctrl.showmapTimeout = undefined;
    };


    /**
     * Event on exist delivery type enabled
     * @param deliveryType
     * @param enabled
     */
    $scope.onDeliveryTypeEnabledChange = function (deliveryType, enabled) {
        if (enabled) {
            $scope.psMachine.deliveryTypes.push({
                delivery_type_id: deliveryType.id,
                carrier: defaultCarrier
            });
            $scope.psMachine.getDeliveryTypeById(deliveryType.id).$$requireCarrierPrice = defaultCarrier === CARRIER_MYSELF;

            // If it interantional and not enabled domestic, enable domestic
            if (deliveryType.id === DELIVERY_TYPE_INTERNATIONAL_ID && !$scope.$$allowedDeliveryTypes[DELIVERY_TYPE_DOMESTIC_ID]) {

                $scope.$$allowedDeliveryTypes[DELIVERY_TYPE_DOMESTIC_ID] = true;
                $scope.onDeliveryTypeEnabledChange(_.find($scope.allowedDeliveryTypes, function (deliveryType) {
                    return deliveryType.id === DELIVERY_TYPE_DOMESTIC_ID;
                }), true)
            }
        } else {
            if (deliveryType.id === DELIVERY_TYPE_DOMESTIC_ID && $scope.$$allowedDeliveryTypes[DELIVERY_TYPE_INTERNATIONAL_ID]) {
                $scope.$$allowedDeliveryTypes[DELIVERY_TYPE_DOMESTIC_ID] = true;
                $notify.error(_t('site.ps', "Sorry, you cannot disable domestic shipping while you have international shipping available. If you feel this is wrong, please contact Treatstock support."));
            } else {
                $scope.psMachine.getDeliveryTypeById(deliveryType.id).$$requireCarrierPrice = false;
                _.remove($scope.psMachine.deliveryTypes, $scope.psMachine.getDeliveryTypeById(deliveryType.id));
            }
        }

        ctrl.updateAllowedDeliveryTypes();
    };


    /**
     * @todo remove after improve ui
     */
    ctrl.updateAllowedDeliveryTypes = function () {
        $scope.$$allowedDeliveryTypes = {};
        _.each($scope.psMachine.deliveryTypes, function (psDeliveryType) {
            $scope.$$allowedDeliveryTypes[psDeliveryType.delivery_type_id] = true;
        });
    };

    /**
     * @todo remove after improve ui
     */
    ctrl.processpsMachine = function () {
        _.each($scope.psMachine.deliveryTypes, function (psDeliveryType) {
            if (psDeliveryType.free_delivery) {
                psDeliveryType.$$requireFreeDelivery = true;
            }

            if (psDeliveryType.carrier && psDeliveryType.carrier === CARRIER_MYSELF) {
                psDeliveryType.$$requireCarrierPrice = true;
            }

            if (psDeliveryType.packing_price) {
                psDeliveryType.$$requirePackagingFee = true;
            }
        })
    };

    /**
     * @todo remove after improve ui
     * @param psDeliveryType
     */
    $scope.onChangeFreeDeliveryTypeRequired = function (psDeliveryType) {
        if (!psDeliveryType.$$requireFreeDelivery) {
            psDeliveryType.free_delivery = null;
        }
    };

    /**
     * @todo remove after improve ui
     * @param psDeliveryType
     */
    $scope.onChangePackingFeeRequired = function (psDeliveryType) {
        if (!psDeliveryType.$$requirePackagingFee) {
            psDeliveryType.packing_price = null;
        }
    };

    /**
     * @param  psDeliveryType
     */
    $scope.onCarrierChange = function (psDeliveryType) {
        if (psDeliveryType.carrier === CARRIER_MYSELF || psDeliveryType.carrier === null) {
            psDeliveryType.carrier = CARRIER_MYSELF;
            psDeliveryType.$$requireCarrierPrice = true;
        } else {
            psDeliveryType.$$requireCarrierPrice = false;
        }

        if (psDeliveryType.carrier !== CARRIER_TREATSTOCK) {
            psDeliveryType.$$requirePackagingFee = false;
            psDeliveryType.packing_price = null;
        }
    };


    /**
     *
     * @param deliveryType
     */
    $scope.isShowPackingPriceInput = function (deliveryType) {
        if (!deliveryType.is_carrier) {
            return false;
        }

        var psDeliveryType = $scope.psMachine.getDeliveryTypeById(deliveryType.id);
        if (psDeliveryType.carrier !== CARRIER_TREATSTOCK) {
            return false;
        }

        return true;
    };

    $scope.isInternational = function (deliveryType) {
        return deliveryType.id === DELIVERY_TYPE_INTERNATIONAL_ID;
    };
    $scope.isEuropa = function () {
        return $scope.country.is_europa == 1;
    };

    $scope.isCarrierMyself = function (deliveryType) {
        var psDeliveryType = $scope.psMachine.getDeliveryTypeById(deliveryType.id);
        return psDeliveryType.carrier === CARRIER_MYSELF;
    }

    $scope.isNeedDeliveryEuropa = function (deliveryType) {
        return $scope.isInternational(deliveryType) && $scope.isEuropa() && $scope.isCarrierMyself(deliveryType);
    }


    /**
     * Is loaded all needed data
     * @type {boolean}
     */
    $scope.isLoaded = true;
    $scope.isLoadedDeliveryAll = false;

    $scope.$geo = $geo;
    $scope.allowedDeliveryTypes = deliveryTypes;

    ctrl.updateAllowedDeliveryTypes();  /* @todo remove after improve ui */
    ctrl.processpsMachine();      /* @todo remove after improve ui */

    $timeout(function () {
        $scope.autocomplete
            .bindToMapBounds($scope.map.getMap())
            .setAddress($scope.psMachine.location);

        $scope.map.setAddress($scope.psMachine.location);
    });


    if ($scope.psMachine.getIsNew()) {
        $maps.getCurrentAddress().$promise.then(function (address) {
            angular.extend($scope.psMachine.location, address);
            $scope.map.setAddress(address);
            $scope.autocomplete.setAddress(address);
        });
    }

    $scope.changeEuropaPrice = function () {
        $scope.psMachine.updateEuropaPrice($scope.countries);
    };

    $scope.allEuropaDelivery = function (value) {
        var europaCountries = $scope.getCountryEuropa();
        _.forEach(europaCountries, function (country, key) {
            $scope.countries[key] = Object.assign({}, $scope.countries[key], {price: parseFloat(value, 10)});
        });
        $scope.psMachine.updateEuropaPrice($scope.countries);
    }

    $scope.getCountryEuropa = function () {
        return _.filter($geo.getCountries(), function (c) {
            return $scope.psMachine.location.country_id !== c.id && c.is_europa;
        });
    };

    $scope.fillEuropaCountries = function (deliveryCountries) {
        var europaCountries = $scope.getCountryEuropa();
        _.forEach(europaCountries, function (c, key) {
            var price = deliveryCountries && deliveryCountries.hasOwnProperty(c.id) ? deliveryCountries[c.id].price : '';
            europaCountries[key] = Object.assign({}, c, {price: parseFloat(price, 10)})
        });
        $scope.psMachine.updateEuropaPrice(europaCountries);
        return europaCountries;
    }


    $scope.$on('redrawGoogleMap', function (event) {
        $timeout(function () {
            google.maps.event.trigger(map, 'resize');
            $scope.map.setAddress($scope.psMachine.location);
        });

    });

    $scope.deliveryUpdate = function () {
        $scope.isLoadedDeliveryAll = true;
        var errors = _.union($scope.psMachine.validateDelvieryTypes(), $scope.psMachine.validateLocation());
        if (!_.isEmpty(errors)) {
            $notify.error(errors);
            $scope.isLoadedDeliveryAll = false;
            return false;
        }
        $http.post('/mybusiness/services/update-delivery-printer', {
            psMachine: $scope.psMachine
        }).then(function (response) {
            $scope.isLoadedDeliveryAll = false;
            if (response.data.success === false) {
                $notify.error(_t('site.ps', response.data.message));
                return;
            }
            $notify.success(_t('site.ps', 'Delivery updated'));
        }).catch(function (data) {
            $scope.isLoadedDeliveryAll = false;
            if (data.data.code === "DeliveryAddressFailed") {
                $scope.deliveryAddressFailed(data.data.message);
                return $q.reject(data);
            }
            if(data.data.code == "HaveSuggestAddress"){
                $notify.error(_t('site.ps', data.data.data.address+ ' The address was unable to be verified.'));
                return $q.reject(data);
            }
            $notifyHttpErrors(data);
            return $q.reject(data);
        });
    };

})

    /**
     *
     */
    .filter('locationBody', ['$sce', function ($sce) {
        return function (address) {
            var addressStr = (address.address || '') + " " + (address.address2 || '') + '<br/>'
                + (address.city || '') + " " + (address.region || '') + " " + (address.zip_code || '') + '<br/>'
                + (address.country || '');

            return $sce.trustAsHtml(addressStr);
        };
    }]);
