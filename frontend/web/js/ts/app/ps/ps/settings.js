"use strict";
/** @namespace controllerParams.redirectOnPhoneValidate */
/** @namespace controllerParams.smsGateways */


/**
 * Controller for manage printservice printer materials
 */
app.controller('PsSettingsController', function ($scope, $geo, $notify, $router, $http, $notifyHttpErrors, $interval, controllerParams)
{

    $scope.$geo = $geo;
    $scope.loading = false;
    $scope.currencies = controllerParams.currencies;
    $scope.phoneValidator = {
        validatePhone: function () {
            // stub
        }
    };


    /**
     * Is new ps
     * @returns {boolean}
     */
    $scope.getIsNew = function()
    {
        return !$scope.company.id;
    };

    /**
     *
     */
    $scope.company = $scope.ps = controllerParams.ps;

    $scope.allowIncomingQuotesSelect = [
        {id: 1, title:'Yes'},
        {id: 0, title:'No'},
    ];


    $scope.reportSupport = function (){
        return $http.post('/mybusiness/company/report-support')
            .then(function(response)
            {
                $notify.success(response.data);
                $scope.sendAgainTryCount = 0;
                $scope.sendAgainOver = 0;
            })
            .catch($notifyHttpErrors);
    };

    /**
     * Validate ps
     * @returns {Array}
     */
    $scope.validatePs = function()
    {
        var errors = [];
        errors = _.union(errors, $scope.checkPaypalPayoutAvailability(true));
        return _.union(errors,$scope.phoneValidator.validatePhone());
    };

    $scope.checkPaypalPayoutAvailability = function(returnErrors)
    {
        var errors = [];
        var country = $geo.getCountryById($scope.company.country_id);
        if(country.is_bank_transfer_payout || country.paypal_payout) {
            return [];
        }
        if(!country.paypal_payout){
            errors.push(_t('site.ps', 'Paypal payouts not available for this country.'));
        }
        if(!country.is_bank_transfer_payout){
            errors.push(_t('site.ps', 'Bank transfer payouts not available for this country.'));
        }
        if(!_.isEmpty(errors) && !returnErrors){
            $notify.error(errors);
        }
        return errors;
    };


    /**
     *
     * @returns {boolean}
     */
    $scope.savePs = function ()
    {
        $scope.loading = true;
        var errors = $scope.validatePs();

        if(!_.isEmpty(errors))
        {
            $notify.error(errors);
            $scope.loading = false;
            return false;
        }

        return $http.post('/mybusiness/settings/save', $scope.company)
            .then(function(response) {
                if(!response.data.success) {
                    $notify.error(response.data.errors);
                    $scope.loading = false;
                    return;
                }
                $notify.success(response.data.message);
                $scope.loading = false;
                $router.to('/mybusiness/settings');
            })
            .catch($notifyHttpErrors);
    };

    $scope.changeCurrency = function (newValue, oldValue)
    {
        TS.confirm(
            _t('site.ps', 'You change company currency. All service prices will be in new currency. Please check it.'),
            function (btn) {
                if (btn === 'ok') {
                    // Do nothing
                } else {
                    $scope.company.currency = oldValue;
                    $scope.$digest();
                }
            },
            {
                confirm: _t('site.order', 'Yes, I understand'),
                dismiss: _t('site.order', 'No')
            }
        );

        console.log('newVal'+newValue);
        console.log('oldVal'+oldValue);
    }
});