"use strict";

app.controller('PsServiceController', function ($scope, $http, $notify, $router, $notifyHttpErrors, controllerParams) {

    $scope.loaded = false;


    $scope.merge = function (services, companyCategories) {
        return  _.map(services, function (service){
            service.active = _.some(companyCategories, function (category) {
                return category.company_service_category_id == service.id;
            });
            return service;
        });
    };

    $scope.services = $scope.merge(controllerParams.services,controllerParams.companyCategories);

    $scope.saveDisable = function () {
        var actives = $scope.activeService();
        return actives.length === 0 || $scope.loaded;
    }

    $scope.activeService = function () {
        return _.filter($scope.services, (function (service) {
            return service.active;
        }));
    }

    $scope.serviceClick = function (service) {
        service.active = !service.active;
    }

    $scope.save = function () {
        $scope.loaded = true;
        var actives = $scope.activeService();
        if (actives.length === 0) {
            $scope.loaded = false;
            return false;
        }

        return $http.post('/mybusiness/company/create-services', {'ids': _.map(actives, 'id')})
            .then(function (response) {
                if (!response.data.success) {
                    $notify.error(response.data.errors);
                    $scope.loaded = false;
                    return;
                }
                $notify.success(response.data.message);
                $scope.loaded = false;
                $router.to('/mybusiness/services');
            })
            .catch(function (response) {
                $scope.loaded = false;
                $notifyHttpErrors(response);
            });
    }
});