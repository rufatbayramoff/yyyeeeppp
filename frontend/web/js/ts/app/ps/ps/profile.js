"use strict";

app.controller('PsProfileController', function ($scope, $http,$notify, $notifyHttpErrors, controllerParams) {
    $scope.ps = controllerParams.ps;
    $scope.ownershipList = controllerParams.ownershipList;
    $scope.businessTypeList = controllerParams.businessTypeList;
    $scope.totalEmployeesList = controllerParams.totalEmployeesList;
    $scope.incotermsList = controllerParams.incoterms;
    $scope.totalEmployeesList = controllerParams.totalEmployeesList;

    $scope.opts = {
        containment: '.js-sortable-container'
    };

    /**
     * Set logo data from dropzone-image-crop
     * @param imageFile
     * @param circleCrop
     * @param squareCrop
     */
    $scope.setLogoData = function (imageFile, circleCrop, squareCrop) {
        $scope.ps.logoFile = imageFile;
        $scope.ps.circleCrop = circleCrop;
        $scope.ps.squareCrop = squareCrop;
    };

    /**
     * Delete selected photo
     */
    $scope.deletePicture = function (file) {
        $scope.ps.pictures.splice($scope.ps.pictures.indexOf(file), 1);
    };

    $scope.$watchCollection('ps.pictures', function () {
        _.each($scope.ps.pictures, function (file) {
            if (!(file instanceof File)) {
                return;
            }

            if (PicturesHelper.isVideo(file)) {
                PicturesHelper.loadLocalVideoImage(file, $scope);
                file.isVideo = true;
            } else {
                PicturesHelper.loadLocalImage(file, $scope);
            }
        });
    });

    /**
     * @type {{
     *     loadLocalImage: loadLocalImage,
     *     loadLocalVideoImage: loadLocalVideoImage,
     *     isVideo: (function(*): boolean)
     * }}
     */
    var PicturesHelper = {
        loadLocalImage: function (imageFile, scope)
        {
            var reader = new FileReader();

            reader.onload = function (e) {
                imageFile.fileUrl = e.target.result;
                scope.$apply();
            };

            reader.readAsDataURL(imageFile);
        },
        loadLocalVideoImage: function (image, scope)
        {
            FrameGrab.blob_to_video(image).then(
                function videoRendered(videoEl) {
                    var frameGrab = new FrameGrab({video: videoEl});
                    frameGrab.grab('img', 2, 160).then(
                        function (itemEntry) {
                            image.fileUrl = itemEntry.container.src;
                            scope.$apply();
                            scope.ps.videoPreviews[image.name] =itemEntry.container.src;
                        },
                        function (reason) {
                            console.log("Can't grab the video frame from file: " + image.name + ". Reason: " + reason);
                        }
                    );
                },

                function videoFailedToRender(reason) {
                    console.log("Can't convert the file to a video element: " + image.name + ". Reason: " + reason);
                }
            );
        },
        isVideo: function (image) {
            var comps = image.name.split(".");
            var ext = comps.pop().toLowerCase();
            return ext === 'mov' || ext === 'mpeg' || ext === 'mp4' || ext === 'wmv';
        }
    };

    $scope.save = function () {
        $scope.ps.optionFileUploads = [];

        $scope.ps.pictures.forEach(function (item, i) {
            if (item instanceof File && typeof item.fileOptions !== "undefined") {
                $scope.ps.optionFileUploads.push({'fileOptions': item.fileOptions, 'name': item.name});
            }
        });
        return $http.post('/mybusiness/company/save-profile', $scope.ps)
            .then(function (response) {
                if (!response.data.success) {
                    $notify.error(response.data.errors);
                    return;
                }
                $notify.success(response.data.message);
            })
            .catch(function (response) {
                $scope.loaded = false;
                $notifyHttpErrors(response);
            });
    };
});