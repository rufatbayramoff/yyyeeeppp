"use strict";
/** @namespace controllerParams.redirectOnPhoneValidate */
/** @namespace controllerParams.smsGateways */


/**
 * Controller for manage printservice printer materials
 */
app.controller('PsCreateController', function ($scope, $notify, $router, $http, $notifyHttpErrors, $interval, controllerParams)
{
    $scope.loaded = false;

    /**
     * Is new ps
     * @returns {boolean}
     */
    $scope.getIsNew = function()
    {
        return !$scope.ps.id;
    };

    $scope.ps = controllerParams.ps;

    $scope.opts =  {
        containment: '.js-sortable-container'
    };

    $scope.locationTrigger = function () {
        $('body').on('changeLocation',function(e, location){
            $scope.ps.location = location;
        });
    };

    $scope.locationTrigger();

    /**
     * Validate ps
     * @returns {Array}
     */
    $scope.validatePs = function()
    {
        var errors = [];

        if(!$scope.ps.title)
        {
            errors.push(_t('site.ps', 'Please enter company name'));
        }
        if(!$scope.ps.description)
        {
            errors.push(_t('site.ps', 'Please provide a business description'));
        }
        if(!$scope.ps.location)
        {
            errors.push(_t('site.ps', 'Please enter location'));
        }
        return errors;
    };

    /**
     *
     * @returns {boolean}
     */
    $scope.savePs = function ()
    {
        $scope.loaded = true;
        var errors = $scope.validatePs();

        if(!_.isEmpty(errors))
        {
            $notify.error(errors);
            $scope.loaded = false;
            return false;
        }

        var url = $scope.getIsNew() ? '/mybusiness/company/new-ps' : '/mybusiness/company/update-ps';
        return $http.post(url, $scope.ps)
            .then(function(response) {
                if(!response.data.success) {
                    $notify.error(response.data.errors);
                    $scope.loaded = false;
                    return;
                }
                $notify.success(response.data.message);
                $scope.loaded = false;
                $router.to('/mybusiness/company/services');
            })
            .catch(function(response){
                $scope.loaded = false;
                $notifyHttpErrors(response);
            });
    };



    /**
     *
     */
    $scope.cancel = function()
    {
        $router.toPsPrintersList()
    };


});