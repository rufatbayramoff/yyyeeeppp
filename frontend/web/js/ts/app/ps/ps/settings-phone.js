"use strict";

app.controller('SettingsPhoneController', function ($scope, $resource, $router, $notifyHttpErrors, $http, $modal, $interval, $notify, controllerParams) {

    /**
     *
     * @type {number}
     */
    var SEND_AGAIN_OVER_DURATION = 60;

    /**
     * Seconds over user can again send sms
     * @type {number}
     */
    $scope.sendAgainOver = 0;


    /**
     *
     * @type {number}
     */
    $scope.sendAgainTryCount = controllerParams.sendAgainTryCount;


    /**
     * Is old number id verifyed
     * @type {boolean}
     */
    $scope.haveVerifyedPhone = $scope.company.phone_status === 'checked';


    /**
     * On phone change click
     */
    $scope.changePhoneNumber = function ()
    {
        $scope.company.phone_status = 'new';
    };

    /**
     * On cancel of changing number click
     */
    $scope.cancelOfChangingPhoneNumber = function ()
    {
        $scope.company.phone_status = 'checked';
    };

    $scope.confirmVerifyPhoneCode = function()
    {
        var errors = $scope.validateVerifyCode();

        if(!_.isEmpty(errors))
        {
            $notify.error(errors);
            return false;
        }

        return $http.post('/mybusiness/company/verify-phone', {phone : $scope.company.phone, phone_country_iso : $scope.company.phone_country_iso, verifyCode : $scope.company.$$phoneVerifyCode})
            .then(function(response)
            {
                $notify.success(response.data);
                $scope.company.phone_status = 'checked';

                /**
                 * Go back to printer sertification
                 * @link https://redmine.tsdev.work/issues/2176
                 */
                if (controllerParams.redirectOnPhoneValidate){
                    $router.to(controllerParams.redirectOnPhoneValidate);
                }

            })
            .catch($notifyHttpErrors);
    };

    /**
     *
     * @returns {*}
     */
    $scope.sendVerifyPhoneCode = function()
    {
        var errors = $scope.phoneValidator.validatePhone();

        if(!_.isEmpty(errors)){
            $notify.error(errors);
            return false;
        }

        return $http.post('/mybusiness/company/send-verify-phone', {
            phone : $scope.company.phone,
            phone_country_iso : $scope.company.phone_country_iso,
        })
            .then(function(response)
            {
                $notify.success(response.data);
                $scope.company.phone_status = 'checking';
                $scope.sendAgainTryCount++;
                sendVerifySmsAgainTimerStart($scope.sendAgainTryCount * SEND_AGAIN_OVER_DURATION);
            })
            .catch($notifyHttpErrors);
    };

    /**
     * Validate phone number
     * @returns {Array}
     */
    $scope.phoneValidator.validatePhone = function()
    {
        var errors = [];

        if(!$scope.company.phone){
            errors.push(_t('site.ps', 'Please enter phone number'));
        }

        if(!$scope.company.phone_country_iso){
            errors.push(_t('site.ps', 'Please enter phone number'));
        }

        return errors;
    };


    /**
     * Validate phone number
     * @returns {Array}
     */
    $scope.validateVerifyCode = function()
    {
        /** @namespace $scope.company.$$phoneVerifyCode */
        var errors = $scope.phoneValidator.validatePhone();
        if(!$scope.company.$$phoneVerifyCode){
            errors.push(_t('site.ps', 'Please enter verification code'));
        }

        return errors;
    };


    /**
     * Start timer for send again show button
     * @param {int} waitDuration Wait duration in seconds
     */
    function sendVerifySmsAgainTimerStart(waitDuration)
    {
        $scope.sendAgainOver = waitDuration;
        $interval(function () {
            $scope.sendAgainOver--;
        }, 1000, $scope.sendAgainOver);
    }

    /**
     * On phone change click
     */
    $scope.changePhoneNumber = function ()
    {
        $scope.company.phone_status = 'new';
    };

    /**
     * On cancel of changing number click
     */
    $scope.cancelOfChangingPhoneNumber = function ()
    {
        $scope.company.phone_status = 'checked';
    };


    // if sms will send on previos reloaded page - continue timer
    if(controllerParams.sendAgainOver){
        sendVerifySmsAgainTimerStart(controllerParams.sendAgainOver);
    }

});