"use strict";

app.controller('CatalogController', function ($scope, $router, controllerParams) {
//    debugger;
    $scope.categories = controllerParams.categories;
    $scope.categorySlug = controllerParams.categorySlug;
    $scope.location = controllerParams.locationString;
    $scope.international = controllerParams.international > 0;
    $scope.technology = controllerParams.technology;
    $scope.usage      = controllerParams.usage;
    $scope.material   = controllerParams.material;
    $scope.sort       = controllerParams.sort;
    $scope.text       = controllerParams.text;

    $scope.initCategories = function () {
        var select = $('.js-category-select').select2({
            placeholder: "Any",
            allowClear: false,
            data: $scope.categories,
            dropdownAutoWidth: true,
            theme: 'krajee',
            width: '100%',
            containerCssClass: ':all:',
        });
        select.on('select2:select', function (e) {
            var currentUrl = new URI(window.location.href);
            var newUrl = new URI(e.params.data.url);
            newUrl.search(currentUrl.search(true));
            $router.to(newUrl);
        });
        if ($scope.categorySlug) {
            setTimeout(function () {
                select.val($scope.categorySlug).trigger('change');
            }, 100);
        }
    };

    $scope.updateUrl = function () {
        $router.setUrl($router.printingCatalog($scope.location, $scope.international, $scope.technology, $scope.usage, $scope.material, $scope.sort, $scope.text));
    };

    $scope.onChangedFilter = function () {
        $scope.updateUrl();
    };

    $(window).on('onChangedFilter', function() {
        $scope.onChangedFilter();
    });

    $("#catalog").on("changeFilter", function (event, changedData) {
        if (changedData['material']) {
            $scope.material = $('#material').val().join('_');
        }
        if (changedData['location']) {
            $scope.location = TS.Locator.getLocationString(changedData['location']);
        }
        $scope.onChangedFilter();
    });


});