"use strict";

app.controller('CuttingDeliveryController', function ($scope, $q, $notify, $notifyHttpErrors, $router, $http, $timeout, $formValidator,$cuttingService, PsMachine, controllerParams) {
    $scope.cuttingMachine = new CuttingMachine(controllerParams.cuttingMachine);
    $scope.companyService = $scope.cuttingMachine.companyService;
    $scope.psMachine = new PsMachine(controllerParams.psMachine);
    $scope.psMachineOriginal = commonJs.clone($scope.psMachine);
    $scope.showNextProgress = 0;

    $scope.goToPage = function (url) {
        if (!$scope.isNeedSave()) {
            $router.to(url);
            return;
        }

        $scope.saveDeliveryDetails().then(function () {
            $router.to(url);
        });
    };

    $scope.prevStep = function () {
        $scope.goToPage($router.editCuttingProcessing($scope.cuttingMachine.id));
    };

    $scope.nextStep = function () {
        $scope.saveDeliveryDetails()
    };

    $scope.isNeedSave = function () {
        let needSave = !angular.equals($scope.psMachine, $scope.psMachineOriginal);
        return needSave;
    };

    $scope.saveDeliveryDetails = function () {
        var defer = $q.defer();
        $formValidator.clearErrors();
        $scope.showNextProgress = 1;
        $http.post($router.saveAjaxCuttingDelivery(), {
            'cattingMachineId': $scope.cuttingMachine.id,
            'psMachine': $scope.psMachine
        }).then(function (data) {
            $scope.showNextProgress = 0;
            var answer = data['data'];
            if (answer['success']) {
                let isNewRecord = !$scope.cuttingMachine.id;
                $notify.success(_t('mybusiness.service', 'Delivery details saved'));
                defer.resolve(answer);
            }
        }).catch(function (response) {
            $scope.showNextProgress = 0;
            $notifyHttpErrors(response);
            defer.reject();
        });
        return defer.promise;
    };

    $scope.publish = function(){
        if(!$scope.cuttingMachine.id){
            $notify.error(_t('mybusiness.service', 'Save machine'));
            return;
        }
        if($scope.isNeedSave) {
            $scope.saveDeliveryDetails().then(function () {
                $cuttingService.sendToModeration($scope.cuttingMachine).then(function () {
                    $router.to($router.getMybusinessServices());
                });
            });
        } else {
            $cuttingService.sendToModeration($scope.cuttingMachine).then(function () {
                $router.to($router.getMybusinessServices());
            });
        }
    };
});