"use strict";

app.controller('CuttingWorkpieceMaterialsController', function ($scope, $q, $notify, $notifyHttpErrors, $router, $http, $timeout,$cuttingService, $formValidator, controllerParams) {
    $scope.cuttingMachine = new CuttingMachine(controllerParams.cuttingMachine);
    $scope.companyService = $scope.cuttingMachine.companyService;
    $scope.cuttingMaterials = new CuttingMaterialList(controllerParams.cuttingMaterials);
    $scope.workpieceMaterialGroups = new CuttingWorkpieceMaterialGroupsList(controllerParams.workpieceMaterialGroups);
    $scope.workpieceMaterialsGroupsOriginal = commonJs.clone($scope.workpieceMaterialGroups);
    $scope.showNextProgress = 0;
    $scope.newEmptyColor = new WorkpieceMaterialGroupColor({'code': '', 'title': ''});

    $scope.goToPage = function (url) {
        if (!$scope.isNeedSave()) {
            $router.to(url);
            return;
        }

        $scope.saveWorkpieceMaterials().then(function () {
            $router.to(url);
        });
    };

    $scope.prevStep = function () {
        $scope.goToPage($router.editCuttingMachine($scope.cuttingMachine.id));
    };

    $scope.nextStep = function () {
        $scope.goToPage($router.editCuttingProcessing($scope.cuttingMachine.id));
    };

    $scope.isNeedSave = function () {
        let needSave = !angular.equals($scope.workpieceMaterialGroups, $scope.workpieceMaterialsGroupsOriginal);
        return needSave;
    };

    $scope.changeEmptyColorId = function (materialSize, emptyColorCode) {
        materialSize.addColor(emptyColorCode);
        $scope.newEmptyColor.code = '';
    };


    /**
     * Materials not already selected
     */
    $scope.getSelectableMaterials = function (workpieceMaterialGroup) {
        let currentMaterials = $scope.workpieceMaterialGroups.getMaterials();
        let list = $scope.cuttingMaterials.excludeList(workpieceMaterialGroup, currentMaterials);
        return list;
    };

    $scope.getSelectableColors = function (materialGroup, materialSize, materialColor) {
        return $scope.cuttingMaterials.getColorsForMaterialId(materialGroup.materialId, materialSize, materialColor);
    };

    /**
     * Material
     */
    $scope.changeMaterialId = function () {

    };

    $scope.canAddMaterial = function () {
        let existsNotSelectedMaterials = ($scope.cuttingMaterials.list.length > $scope.workpieceMaterialGroups.list.length);
        return existsNotSelectedMaterials;
    };

    $scope.addMaterial = function () {
        $scope.workpieceMaterialGroups.addEmptyMaterial();
    };

    $scope.getAddMaterialTitle = function () {
        let existsNotSelectedMaterials = ($scope.cuttingMaterials.list.length > $scope.workpieceMaterialGroups.list.length);
        if (!existsNotSelectedMaterials) {
            return 'No more possible materials for add to list'
        }
        return '';
    };

    /**
     * Size
     */
    $scope.canAddSize = function () {
        return true;
    };

    $scope.addSize = function (materialGroup) {
        materialGroup.addEmptySize();
    };


    /**
     *  Color
     */
    $scope.canAddColor = function (materialSize, materialId) {
        if (!materialId) {
            return false;
        }
        let materialElement = $scope.cuttingMaterials.findMaterialElement(materialId);
        return materialSize.hasSelectedColor() && !materialSize.allColorsSelected(materialElement.colors);
    };

    $scope.changeColorId = function (material) {

    };

    /**
     * Delete material
     */
    $scope.deleteMaterial = function (workpieceMaterialGroup, materialSize, materialColor) {
        $scope.workpieceMaterialGroups.delete(workpieceMaterialGroup, materialSize, materialColor);
    };

    /**
     * Group workpiece materials
     */
    $scope.getWorkpieceMaterialGroups = function () {

    };


    $scope.saveWorkpieceMaterials = function () {
        var defer = $q.defer();
        $formValidator.clearErrors();
        $scope.showNextProgress = 1;
        return $http.post($router.saveAjaxWorkpieceMaterials(), {
            'workpieceMaterialGroups': $scope.workpieceMaterialGroups.list
        }).then(function (data) {
            $scope.showNextProgress = 0;
            var answer = data['data'];
            if (answer['success']) {
                let isNewRecord = !$scope.cuttingMachine.id;
                $notify.success(_t('mybusiness.service', 'Workpiece materials saved'));
            }
        }).catch(function (response) {
            $scope.showNextProgress = 0;
            $notifyHttpErrors(response);
            defer.reject();
            return defer.promise;
        });
    };

    $scope.publish = function(){
        if(!$scope.cuttingMachine.id){
            $notify.error(_t('mybusiness.service', 'Save machine'));
            return;
        }
        if($scope.isNeedSave && $scope.workpieceMaterialGroups.list.length) {
            $scope.saveWorkpieceMaterials().then(function () {
                $cuttingService.sendToModeration($scope.cuttingMachine);
            });
        } else {
            $cuttingService.sendToModeration($scope.cuttingMachine);
        }
    };
});