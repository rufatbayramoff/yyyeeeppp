"use strict";

/**
 * @property {string} id
 * @property {float} max_with
 * @property {float} max_length
 * @property {float} max_length
 * @property {float} min_order_price
 * @property {CompanyService} companyService
 *
 * @param data
 * @constructor
 */
function CuttingMachine(data) {
    this.load(data);
}

CuttingMachine.prototype.load = function (data) {
    this.id = parseInt(data['id'] ?? 0);
    this.max_width = parseFloat(data['maxWidth']);
    this.max_length = parseFloat(data['maxLength']);
    this.min_order_price = parseFloat(data['minOrderPrice']);
    this.technology = data['technology'];
    if (data['companyService']) {
        if (!this.companyService) {
            this.companyService = new CompanyService(data['companyService']);
        } else {
            this.companyService.load(data['companyService']);
        }
    }
};


function CuttingProcessingList(data) {
    this.load(data)
}

CuttingProcessingList.prototype.load = function (data) {
    this.list = [];
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        let dataElement = data[key];
        let cuttingProcessing = new CuttingProcessing(dataElement);
        this.list.push(cuttingProcessing);
    }
};

CuttingProcessingList.prototype.getWorkpieceMaterialUuids = function () {
    let uuids = [];
    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        let cuttingProcessing = this.list[key];
        if (cuttingProcessing.workpieceMaterialUuid) {
            uuids.push(cuttingProcessing.workpieceMaterialUuid);
        }
    }
    return uuids;
};

CuttingProcessingList.prototype.addEmpty = function () {
    let cuttingProcessing = new CuttingProcessing({});
    this.list.push(cuttingProcessing);
};

CuttingProcessingList.prototype.delete = function (uuid) {
    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        let dataElement = this.list[key];
        if (uuid == dataElement.uuid) {
            this.list.splice(key, 1);
            break;
        }
    }
};


/**
 * @property {int} id
 * @property {int} cuttingMachineId
 * @property {string} workpieceMaterialProcessingUuid
 * @property {float} cuttingPrice
 * @property {float} engravingPrice
 *
 * @param data
 * @constructor
 */
function CuttingProcessing(data) {
    this.uuid = uuidHelper.generateUuid();
    this.load(data)
}

CuttingProcessing.prototype.load = function (data) {
    this.workpieceMaterialProcessingUuid = data['workpieceMaterialProcessingUuid'];

    if (data['cuttingPricePerM']) {
        this.cuttingPrice = Number(data['cuttingPricePerM']);
    }
    if (data['engravingPricePerM']) {
        this.engravingPrice = Number(data['engravingPricePerM']);
    }
    if (data['uuid']) {
        this.uuid = data['uuid'];
    }
};


function CuttingWorkpieceMaterialsProcessingList(data) {
    this.load(data)
}

CuttingWorkpieceMaterialsProcessingList.prototype.load = function (data) {
    this.list = [];
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        let dataElement = data[key];
        let workpieceMaterial = new CuttingWorkpieceMaterialProcessing(dataElement);
        this.list.push(workpieceMaterial);
    }
};

CuttingWorkpieceMaterialsProcessingList.prototype.excludeFromList = function (uuid, selectedWorkpieceMaterials) {
    let filteredList = [];
    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        let workpieceMaterial = this.list[key];
        if (workpieceMaterial.uuid === uuid) {
            filteredList.push(workpieceMaterial);
            continue;
        }
        let isSelected = false;
        for (var selectedKey in selectedWorkpieceMaterials) {
            if (!selectedWorkpieceMaterials.hasOwnProperty(selectedKey)) continue;
            let selectedUuid = selectedWorkpieceMaterials[selectedKey];
            if (selectedUuid === workpieceMaterial.uuid) {
                isSelected = true;
                break;
            }
        }
        if (!isSelected) {
            filteredList.push(workpieceMaterial);
        }
    }
    return filteredList;
};

/**
 * @property {string} uuid
 * @property {int} materialId
 * @property {int} thickness
 * @property {string} title
 *
 * @param data
 * @constructor
 */
function CuttingWorkpieceMaterialProcessing(data) {
    this.load(data)
}

CuttingWorkpieceMaterialProcessing.prototype.load = function (data) {
    angular.extend(this, data);
};

/**
 * @property {WorkpieceMaterialGroup[]} list
 * @param data
 * @constructor
 */
function CuttingWorkpieceMaterialGroupsList(data) {
    this.load(data);
}

CuttingWorkpieceMaterialGroupsList.prototype.load = function (data) {
    this.list = [];
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        let dataElement = data[key];
        let workpieceMaterialGroup = new WorkpieceMaterialGroup(dataElement);
        this.list.push(workpieceMaterialGroup);
    }
};

/**
 *
 * @returns {CuttingWorkpieceMaterialsGroup[]}
 */
CuttingWorkpieceMaterialGroupsList.prototype.getGroups = function () {
    let list = [];

    return this;
};

CuttingWorkpieceMaterialGroupsList.prototype.getMaterials = function () {
    let materials = [];
    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        let dataElement = this.list[key];
        materials[dataElement.materialId] = dataElement.materialId;
    }
    return materials;
};

CuttingWorkpieceMaterialGroupsList.prototype.addEmptyMaterial = function () {
    let emptyMaterial = new WorkpieceMaterialGroup({'sizes': [{}]});
    this.list.push(emptyMaterial);
};

/**
 *
 * @param workpieceMaterialGroup
 * @param materialSize
 * @param materialColor
 */
CuttingWorkpieceMaterialGroupsList.prototype.delete = function (workpieceMaterialGroup, materialSize, materialColor) {
    materialSize.deleteColor(materialColor);
    if (!materialSize.colors.length) {
        workpieceMaterialGroup.deleteSize(materialSize);
        if (!workpieceMaterialGroup.sizes.length) {
            this.deleteGroup(workpieceMaterialGroup);
        }
    }
};

CuttingWorkpieceMaterialGroupsList.prototype.deleteGroup = function (workpieceMaterialGroup) {
    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        let dataElement = this.list[key];
        if (workpieceMaterialGroup.uuid == dataElement.uuid) {
            this.list.splice(key, 1);
            break;
        }
    }
};

/**
 *
 * @param data
 * @constructor
 */
function CuttingWorkpieceMaterialsGroup(data) {
    angular.extend(this, data);
}

/**
 * @property {string} uuid
 * @property {int} materialId
 * @property {int} sizes
 *
 * @param data
 * @constructor
 */
function WorkpieceMaterialGroup(data) {
    this.uuid = uuidHelper.generateUuid();
    this.load(data);
}

WorkpieceMaterialGroup.prototype.load = function (data) {
    this.materialId = data['materialId'];
    this.sizes = [];
    for (var key in data['sizes']) {
        if (!data['sizes'].hasOwnProperty(key)) continue;
        let dataElement = data['sizes'][key];
        let groupSize = new WorkpieceMaterialGroupSize(dataElement);
        this.sizes.push(groupSize);
    }
};


WorkpieceMaterialGroup.prototype.addEmptySize = function () {
    let groupSize = new WorkpieceMaterialGroupSize([]);
    this.sizes.push(groupSize);
};

WorkpieceMaterialGroup.prototype.deleteSize = function (size) {
    for (var key in this.sizes) {
        if (!this.sizes.hasOwnProperty(key)) continue;
        let dataElement = this.sizes[key];
        if (size.uuid == dataElement.uuid) {
            this.sizes.splice(key, 1);
            break;
        }
    }
};


/**
 * @property {string} uuid
 * @property {int} width
 * @property {int} height
 * @property {int} thickness
 * @property {WorkpieceMaterialGroupColor} colors
 *
 * @param data
 * @constructor
 */
function WorkpieceMaterialGroupSize(data) {
    this.uuid = uuidHelper.generateUuid();
    this.load(data);
}

WorkpieceMaterialGroupSize.prototype.load = function (data) {
    this.width = data['width'];
    this.height = data['height'];
    this.thickness = data['thickness'];
    this.colors = [];
    for (var key in data.colors) {
        if (!data.colors.hasOwnProperty(key)) continue;
        let dataElement = data.colors[key];
        let groupSize = new WorkpieceMaterialGroupColor(dataElement);
        this.colors.push(groupSize);
    }
    if (!this.colors.length) {
        let groupSize = new WorkpieceMaterialGroupColor([]);
        this.colors.push(groupSize);
    }
};

WorkpieceMaterialGroupSize.prototype.deleteColor = function (color) {
    for (var key in this.colors) {
        if (!this.colors.hasOwnProperty(key)) continue;
        let dataElement = this.colors[key];
        if (dataElement.uuid == color.uuid) {
            this.colors.splice(key, 1);
            break;
        }
    }
};

WorkpieceMaterialGroupSize.prototype.addColor = function (colorCode) {
    let color = new WorkpieceMaterialGroupColor({'code': colorCode});
    this.colors.push(color);
};

WorkpieceMaterialGroupSize.prototype.hasSelectedColor = function (code) {
    for (var key in this.colors) {
        if (!this.colors.hasOwnProperty(key)) continue;
        let dataElement = this.colors[key];
        if (code) {
            if (code == dataElement.code) {
                return true;
            }
        } else {
            if (dataElement.code) {
                return true;
            }
        }
    }
    return false;
};

WorkpieceMaterialGroupSize.prototype.allColorsSelected = function (colors) {
    if (!colors || !colors.length) {
        return false;
    }
    for (var key in colors) {
        if (!colors.hasOwnProperty(key)) continue;
        let dataElement = colors[key];
        if (!this.hasSelectedColor(dataElement.code)) {
            return false;
        }
    }
    return true;
};

/**
 *  COLOR
 *
 * @param {string} uuid
 * @param {int} code
 * @param {int} id
 * @param {float} price
 * @constructor
 */


function WorkpieceMaterialGroupColor(data) {
    this.uuid = uuidHelper.generateUuid();
    this.load(data);
}

WorkpieceMaterialGroupColor.prototype.load = function (data) {
    if (data['uuid']) {
        this.uuid = data['uuid'];
    }
    this.code = data['code'];
    this.price = data['price'];
}


function CuttingMaterialList(data) {
    this.load(data);
}

CuttingMaterialList.prototype.load = function (data) {
    this.list = [];
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        let dataElement = data[key];
        let cuttingMaterial = new CuttingMaterial(dataElement);
        this.list.push(cuttingMaterial);
    }
};

CuttingMaterialList.prototype.excludeList = function (workpieceMaterialGroup, excludeList) {
    let list = [];

    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        let dataElement = this.list[key];
        if (workpieceMaterialGroup) {
            if ((workpieceMaterialGroup.materialId != dataElement.id) && excludeList[dataElement.id]) {
                continue;
            }
        } else {
            if (excludeList[dataElement.id]) {
                continue;
            }
        }

        list.push(dataElement);
    }
    return list;
};

CuttingMaterialList.prototype.findMaterialElement = function (materialId) {
    let findMaterialElement = null;
    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        let dataElement = this.list[key];
        if (dataElement.id == materialId) {
            findMaterialElement = dataElement;
            break;
        }
    }
    return findMaterialElement;
};

CuttingMaterialList.prototype.getColorsForMaterialId = function (materialId, sizeGroup, materialColor) {
    let findMaterialElement = this.findMaterialElement(materialId);
    if (!findMaterialElement) {
        return [];
    }

    let resColors = [];

    for (var keyColors in findMaterialElement.colors) {
        if (!findMaterialElement.colors.hasOwnProperty(keyColors)) continue;
        let colorElement = findMaterialElement.colors[keyColors];
        let exists = false;
        if (!materialColor || materialColor.code != colorElement.code) {
            for (var keySizeColor in sizeGroup.colors) {
                if (!sizeGroup.colors.hasOwnProperty(keySizeColor)) continue;
                let existsColorElement = sizeGroup.colors[keySizeColor];
                if (existsColorElement.code === colorElement.code) {
                    exists = true;
                    break;
                }
            }
        }
        if (!exists) {
            resColors.push(colorElement);
        }
    }
    return resColors;
};

/**
 * @param {int} id
 * @param {string} code
 * @param {string} title
 * @param {string} defaultThickness
 * @param {string} defaultWidth
 * @param {string} defaultLength
 * @param {string} defaultLength
 *
 * @param data
 * @constructor
 */
function CuttingMaterial(data) {
    this.load(data);
}

CuttingMaterial.prototype.load = function (data) {
    angular.extend(this, data);
};

CuttingMaterial.prototype.setEmpty = function (data) {
    this.load({'id': -1, 'code': '', 'title': 'Add color'});
};


/**
 * @param {string} code
 * @param {string} rgb
 * @param data
 * @constructor
 */
function CuttingMaterialColor(data) {
    this.load(data);
}

CuttingMaterialColor.prototype.load = function (data) {
    angular.extend(this, data);
};



