"use strict";

app.controller('CuttingProcessingController', function ($scope, $q, $notify, $notifyHttpErrors,$cuttingService, $router, $http, $timeout, $formValidator, controllerParams) {
    $scope.cuttingMachine = new CuttingMachine(controllerParams.cuttingMachine);
    $scope.companyService = $scope.cuttingMachine.companyService;
    $scope.workpieceMaterials = new CuttingWorkpieceMaterialsProcessingList(controllerParams.cuttingWorkpieceMaterials);
    $scope.processing = new CuttingProcessingList(controllerParams.cuttingProcessing);
    $scope.processingOriginal = commonJs.clone($scope.processing);
    $scope.showNextProgress = 0;
    //$scope.newEmptyColor = new WorkpieceMaterialGroupColor({'code': '', 'title': ''});

    $scope.goToPage = function (url) {
        if (!$scope.isNeedSave()) {
            $router.to(url);
            return;
        }

        $scope.saveProcessing().then(function () {
            $router.to(url);
        });
    };

    $scope.prevStep = function () {
        $scope.goToPage($router.editCuttingWorkpieceMaterials($scope.cuttingMachine.id));
    };

    $scope.nextStep = function () {
        $scope.goToPage($router.editCuttingDelivery($scope.cuttingMachine.id));
    };

    $scope.isNeedSave = function () {
        let needSave = !angular.equals($scope.processing, $scope.processingOriginal);
        return needSave;
    };

    $scope.getSelectableWorkpieceMaterials = function (processPrice) {
        let selectedWorkpieceMaterials = $scope.processing.getWorkpieceMaterialUuids();
        return $scope.workpieceMaterials.excludeFromList(processPrice, selectedWorkpieceMaterials);
    };

    /**
     * Processing
     */
    $scope.canAddProcessing = function () {
        let workpieceMaterialsCanAdd = ($scope.workpieceMaterials.list.length >$scope.processing.list.length);
        return workpieceMaterialsCanAdd;
    };

    $scope.addProcessing = function () {
        $scope.processing.addEmpty();
    };

    $scope.getAddProcessingTitle = function () {
        let workpieceMaterialsCanAdd = ($scope.workpieceMaterials.list.length >$scope.processing.list.length);
        if (!workpieceMaterialsCanAdd) {
            return 'All workpiece materials already added'
        }
        return '';
    };

    $scope.deleteProcessing = function(processPrice) {
        $scope.processing.delete(processPrice.uuid);
    };

    $scope.saveProcessing = function () {
        var defer = $q.defer();
        $formValidator.clearErrors();
        $scope.showNextProgress = 1;
        return $http.post($router.saveAjaxCuttingProcessing(), {
            'cattingMachineId': $scope.cuttingMachine.id,
            'processing': $scope.processing.list
        }).then(function (data) {
            var answer = data['data'];
            if (answer['success']) {
                if (answer['warning']) {
                    $notify.warning(answer['warning']);
                    setTimeout(function(){
                        $scope.showNextProgress = 0;
                        defer.resolve();
                    }, 3000);
                } else {
                    $scope.showNextProgress = 0;
                    $notify.success(_t('mybusiness.service', 'Processing saved'));
                    defer.resolve();
                }
            }
            return defer.promise;
        }).catch(function (response) {
            $scope.showNextProgress = 0;
            $notifyHttpErrors(response);
            defer.reject();
            return defer.promise;
        });
    };

    $scope.publish = function(){
        if(!$scope.cuttingMachine.id){
            $notify.error(_t('mybusiness.service', 'Save machine'));
            return;
        }
        if($scope.isNeedSave) {
            $scope.saveProcessing().then(function () {
                $cuttingService.sendToModeration($scope.cuttingMachine);
            });
        } else {
            $cuttingService.sendToModeration($scope.cuttingMachine);
        }
    };
});