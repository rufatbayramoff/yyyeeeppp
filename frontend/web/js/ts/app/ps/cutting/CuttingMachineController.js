"use strict";

app.controller('CuttingMachineController', ['$scope', '$q', '$notify', '$formValidator', '$notifyHttpErrors', '$router', '$http', '$timeout', '$cuttingService', 'controllerParams',function ($scope, $q, $notify, $formValidator, $notifyHttpErrors, $router, $http, $timeout, $cuttingService, controllerParams) {
    $scope.cuttingMachine = new CuttingMachine(controllerParams.cuttingMachine);
    $scope.cuttingMachineOriginal = commonJs.clone($scope.cuttingMachine);
    $scope.companyService = $scope.cuttingMachine.companyService;
    $scope.showNextProgress = 0;


    $scope.nextStep = function() {
        if (!$scope.isNeedSave()) {
            $router.to($router.editCuttingWorkpieceMaterials($scope.cuttingMachine.id));
            return;
        }
        $scope.saveGeneralInfo().then(function () {
            $router.to($router.editCuttingWorkpieceMaterials($scope.cuttingMachine.id));
        });
    };

    $scope.$watchCollection('companyService.images', function (newNames, oldNames) {
        _.each($scope.companyService.images, function (file) {
            if (!(file instanceof File)) {
                return;
            }
            var reader = new FileReader();
            reader.onload = function (e) {
                file.src = e.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(file);
        });
    });

    $scope.isNeedSave = function() {
        let needSave = !$scope.cuttingMachine.id || !angular.equals($scope.cuttingMachine, $scope.cuttingMachineOriginal);
        return needSave;
    };

    $scope.saveGeneralInfo = function () {
        var defer = $q.defer();
        $formValidator.clearErrors();
        $scope.showNextProgress = 1;
        return $http.post($router.saveAjaxCuttingMachine(), {
            'id': $scope.cuttingMachine.id,
            'CuttingMachine': {
                'maxWidth': $scope.cuttingMachine.max_width,
                'maxLength': $scope.cuttingMachine.max_length,
                'minOrderPrice': $scope.cuttingMachine.min_order_price,
                'technology': $scope.cuttingMachine.technology
            },
            'CompanyService': {
                'title': $scope.companyService.title,
                'description': $scope.companyService.description,
                'images': $scope.companyService.images,
            }
        }).then(function (data) {
            $scope.showNextProgress = 0;
            var answer = data['data'];
            if (answer['success']) {
                let isNewRecord = !$scope.cuttingMachine.id;
                $scope.cuttingMachine.load(answer['cuttingMachine']);
                if (isNewRecord) {
                    $router.changeUrl($router.editCuttingMachine($scope.cuttingMachine.id), null);
                }
                $notify.success(_t('mybusiness.service', 'Service saved'));
            }
        }).catch(function (response) {
            $scope.showNextProgress = 0;
            $notifyHttpErrors(response);
            defer.reject();
            return defer.promise;
        });
    };

    $scope.publish = function(){
        if(!$scope.cuttingMachine.id){
            $notify.error(_t('mybusiness.service', 'Save machine'));
            return;
        }
        $cuttingService.sendToModeration($scope.cuttingMachine);
    };
}]);
