"use strict";

app.controller('ReviewController', function ($scope, $http, controllerParams) {
    $scope.filter = {
        'category': controllerParams.category,
        'material': controllerParams.material,
        'technology': controllerParams.technology
    };

    $scope.init = function () {
        setTimeout(function () {
            $scope.filterMaterial = $('#material-filter').find('select');
            $scope.filterMaterial.on('select2:select', $scope.filterMaterialSelect);
            $scope.filterTechnology = $('#technology-filter').find('select');
            $scope.filterTechnology.on('select2:select', $scope.filterTechnologySelect);
            $scope.filterCategory = $('#category-filter').find('select');
            $scope.filterCategory.on('select2:select', $scope.filterCategorySelect);
            $scope.path = window.location.protocol + "//" + window.location.host + '/reviews';
        });
    };

    $scope.filterCategorySelect = function (e) {
        $scope.filter.category = e.params.data.id;
        $scope.changeLocation();
    }

    $scope.fixLoading = function () {
        $('#loadingProgress').removeClass('hidden');
        $('body').addClass('bodyCursorWait');
    };
    $scope.fixLoaded = function () {
        $('#loadingProgress').addClass('hidden');
        $('body').removeClass('bodyCursorWait');
    };

    $scope.filterMaterialSelect = function (e) {
        $scope.filter.material = e.params.data.id;
        $scope.changeLocation();
    };

    $scope.filterTechnologySelect = function (e) {
        $scope.filter.technology = e.params.data.id;
        $scope.changeLocation();
    };

    $scope.changeLocation = function () {
        var urls = [];
        var url = '';
        _.each($scope.filter, function (element, index) {
            if (element && element !== '0') {
                urls.push(index + "-" + element);
            }
        });
        if (urls.length) {
            $scope.updateReview($scope.path + '/' + $scope.queryParams(urls.join('/')));
            return;
        }
        $scope.updateReview($scope.path + $scope.queryParams(url));
    }

    $scope.queryParams = function (url) {
        if (window.location.search) {
            url = url + window.location.search;
        }
        let returnUrl = new URI(url);
        returnUrl.removeQuery('page');
        return returnUrl.toString();
    };

    $scope.updateReview = function (url) {
        $scope.fixLoading();
        $http.get(url).then(function (response) {
            history.pushState({}, '', url);
            $('#reviewList').replaceWith(response.data.htmlBlock);
            $scope.fixLoaded();
        }).catch(function (response) {
            new TS.Notify({
                type: 'error',
                text: response.message || 'Error loading reviews',
                automaticClose: false
            });
            $scope.fixLoaded();
        });
    }

});