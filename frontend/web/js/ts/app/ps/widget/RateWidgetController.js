"use strict";

app.controller('RateWidgetController', function ($scope, $router, controllerParams) {
    $scope.wideType = false;
    $scope.widgetCode = controllerParams.widgetCode;

    $scope.widgetShow = function () {
        let key ='';
        if ($scope.wideType) {
            key='wide';
        }
        $('#publicLink').val($scope.widgetCode[key]);
        $('#embedframe').html($scope.widgetCode[key]);
    };

    $scope.wideCheckbox = function () {
        $scope.wideType = !$scope.wideType;
        $scope.widgetShow();
    }

});