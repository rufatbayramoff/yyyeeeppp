"use strict";

/**
 * Controller for receipt
 */
app.controller('ReceiptController', function ($scope, $router, $http, $notify, $notifyHttp, $notifyHttpErrors, controllerParams)
{

    $scope.editAdditionalBuyerDetails = 0;
    $scope.editComment = 0;

    $scope.invoices = controllerParams.invoices;
    $scope.receipt = controllerParams.receipt;


    $scope.startEditBuyerDetails = function(invoiceUuid)
    {
        $scope.editAdditionalBuyerDetails = 1;
        setTimeout(function(){
            $('#editAdditionalBuyerDetails_'+invoiceUuid).focus();
        },100);
    };

    $scope.saveBuyerDetails = function(invoiceUuid)
    {
        $scope.editAdditionalBuyerDetails = 0;
        var buyerDetails = $scope.invoices[invoiceUuid].additionalBuyerDetails;

        return $http.post($router.getSaveReceiptBuyerAdditionDetails(invoiceUuid, $scope.receipt.id), {
            buyerDetails: buyerDetails
        }).then(function (response) {
            $notify.success('Saved');
        }).catch(function (response) {
            $notifyHttpErrors(response);
        });
    };

    $scope.startEditComment = function(invoiceUuid)
    {
        $scope.editComment = 1;
        setTimeout(function(){
            $('#editComment_'+invoiceUuid).focus();
        },100);
    };

    $scope.saveComment = function(invoiceUuid)
    {
        $scope.editComment = 0;
        var comment = $scope.invoices[invoiceUuid].comment;

        return $http.post($router.getSaveReceiptComment(invoiceUuid, $scope.receipt.id), {
            comment: comment
        }).then(function (response) {
            $notify.success('Saved');
        }).catch(function (response) {
            $notifyHttpErrors(response);
        });
    };

});