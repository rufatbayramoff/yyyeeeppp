/**
 * Created by analitic on 14.07.17.
 */
"use strict";

/**
 * Printservice cnc resource
 */
app.factory('CncSchema', function ($resource) {
    var CncSchema = $resource('',  {});

    return CncSchema;
});
