"use strict";

/**
 * Printservice cnc resource
 *
 * @property {array} materials
 * @property {array} shortMachinesList
 */
app.factory('PsCnc', function ($resource) {
    var PsCnc = $resource('',
        {id: '@id'},
        {schemaValues: {method: 'get', url: '/mybusiness/edit-printer/ps-cnc'}},
        {json: {}}
    );

    /**
     * Is new printer
     * @returns {boolean}
     */
    PsCnc.prototype.getIsNew = function () {
        return !this.id;
    };


    PsCnc.prototype.setMaterialsFromJson = function (json_value) {
        var self = this;
        if (!this.json) {
            this.json = {};
        }

        this.json.materials = json_value.materials;
        if (this.json.millingMachines && (!this.json.millingInstruments || (this.json.millingInstruments.length === 0))) {
            for (var key in this.json.materials) {
                if (!this.json.materials.hasOwnProperty(key)) continue;
                let material = this.json.materials[key];

                let item = {
                    volumeW: 100,
                    volumeH: 100,
                    volumeL: 100,
                    d: 7,
                    l: 3,
                    s: 80,
                    fr: 0.12,
                    delta: 1.5,
                    material: material.title
                };
                this.json.millingInstruments.push(item);
            }
        }
        if (this.json.cuttingMachines && this.json.cuttingMachines[0] && (!this.json.cuttingMachines[0].speeds || (this.json.cuttingMachines[0].speeds.length === 0))) {
            this.json.cuttingMachines[0].speeds = [];
            for (var key in this.json.materials) {
                if (!this.json.materials.hasOwnProperty(key)) continue;
                let material = this.json.materials[key];

                let item = {
                    material: material.title,
                    cuttingSpeed: 80,
                    maxThickness: 40
                };
                this.json.cuttingMachines[0].speeds.push(item);
            }
        }
        if (this.json.stocks.freeform && (this.json.stocks.freeform.length === 1) && (this.json.stocks.freeform[0].material === null)) {
            this.json.stocks.freeform = [];
            for (var key in this.json.materials) {
                if (!this.json.materials.hasOwnProperty(key)) continue;
                let material = this.json.materials[key];
                let item = {
                    material: material.title,
                };
                this.json.stocks.freeform.push(item);
            }
        }
        if (this.json.stocks.slabs && (this.json.stocks.slabs.length === 1) && (this.json.stocks.slabs[0].material === null)) {
            this.json.stocks.slabs = [];
            for (var key in this.json.materials) {
                if (!this.json.materials.hasOwnProperty(key)) continue;
                let material = this.json.materials[key];
                let item = {
                    material: material.title,
                    thickness: 80,
                    price: 25
                };
                this.json.stocks.slabs.push(item);
            }
        }
    };

    PsCnc.prototype.setValuesFromJson = function (json_value) {
        this.json = json_value;
    };

    PsCnc.prototype.validate = function () {
        return false;
    };

    PsCnc.prototype.addMachineToJson = function (machine) {
        var self = this;
        if (machine.type === 'milling') {
            self.json.millingMachines.push({
                title: machine.title
            });
        }
        if (machine.type === 'cutting') {
            self.json.cuttingMachines.push({
                title: machine.title
            });
        }
    };

    PsCnc.prototype.shortMachinesListToJson = function () {
        var self = this;
        angular.forEach(this.shortMachinesList, function (machine) {
            if (machine.link) {
                if (machine.type !== machine.typeOriginal) {
                    if (machine.typeOriginal === 'cutting') {
                        self.json.cuttingMachines = _.reject(self.json.cuttingMachines, function (el) {
                            return el === machine.link
                        });
                    }
                    if (machine.typeOriginal === 'milling') {
                        self.json.millingMachines = _.reject(self.json.millingMachines, function (el) {
                            return el === machine.link
                        });
                    }
                    self.addMachineToJson(machine);
                } else {
                    machine.link.title = machine.title;
                }
            } else {
                self.addMachineToJson(machine);
            }
        });
    };

    PsCnc.prototype.validateMachinesShortList = function () {
        if (this.shortMachinesList.length < 1) {
            return [_t('site.ps', 'Please add at least one machine.')];
        }
    };

    PsCnc.prototype.validateMaterials = function () {
        return [];
    };

    PsCnc.prototype.formShortMachinesList = function () {
        var self = this;
        var shortList = [];
        angular.forEach(this.json.cuttingMachines, function (cuttingMachine) {
            var item = {
                title: cuttingMachine.title,
                type: 'cutting',
                typeOriginal: 'cutting',
                link: cuttingMachine
            };
            shortList.push(item);
        });
        angular.forEach(this.json.millingMachines, function (millingMachine) {
            var item = {
                title: millingMachine.title,
                type: 'milling',
                typeOriginal: 'milling',
                link: millingMachine
            };
            shortList.push(item);
        });
        if (!shortList) {
            shortList.push({
                title: '',
                type: 'milling'
            });
        }
        this.shortMachinesList = shortList;
    };

    PsCnc.prototype.getJsonValue = function () {
        return this.json;
    };

    return PsCnc;
});
