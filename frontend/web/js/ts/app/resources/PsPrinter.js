"use strict";

/**
 * Printservice Printer resource
 */
app.factory('PsPrinter', function($resource)
{
    /**
     * @property {number} positional_accuracy
     */
    var PsPrinter = $resource('', {id : '@id'},{
        info : {method: 'get', url : '/mybusiness/edit-printer/ps-printer'},
        updatePrinter : {method : 'post', url : '/mybusiness/edit-printer/update-printer'},
        updateMaterials : {method : 'post', url : '/mybusiness/edit-printer/update-materials'},
        create : {method : 'post', url : '/mybusiness/services/create-printer'},
        delete : {method : 'delete', url : '/mybusiness/edit-printer/delete-printer/:id'}
    });

    PsPrinter.USTATUS_ACTIVE = 'active';
    PsPrinter.USTATUS_INACTIVE = 'inactive';

    PsPrinter.TEST_STATUS_NO = 'no';
    PsPrinter.TEST_STATUS_TESTED = 'tested';
    PsPrinter.TEST_STATUS_PROFESSIONAL = 'professional';

    PsPrinter.DELIVERY_TYPE_PICKUP_ID = 1;

    /**
     * Validate printer params
     * @todo This code like Paul - need change it to angular + server validation
     * @returns {string[]} Array of errors
     * @param {Printer} printer
     */
    PsPrinter.prototype.validatePrinter = function(printer)
    {
        var errors = [];

        if(!this.printer_id)
        {
            errors.push(_t('site.ps', 'Please choose printer'));
        }

        if(_.isEmpty(this.title))
        {
            errors.push(_t('site.ps', 'Please enter your printer name'));
        }

        if (!this.positional_accuracy) {
            errors.push(_t('site.ps', 'Please fill Positional Accuracy'));
        }
        else {

            if(this.positional_accuracy > printer.maxPositionalAccuracy) {
                errors.push(_t('site.ps', 'Positional Accuracy can\'t be more than ' + printer.maxPositionalAccuracy));
            }

            if(this.positional_accuracy < 0.001) {
                errors.push(_t('site.ps', 'Positional Accuracy can\'t be less than 0.001'));
            }
        }

        return errors;
    };

    /**
     * Validate materials and colors
     * @todo This code like Paul - need change it to angular + server validation
     * @returns {string[]}
     */
    PsPrinter.prototype.validateMaterialsAndColors = function()
    {
        var errors = [];

        if(_.isEmpty(this.materials))
        {
            errors.push(_t('site.ps', 'Please select printer materials'));
        }

        var colorErrors = [];
        _.each(_.flatten(_.pluck(this.materials, 'colors')), function(psColor)
        {
            if(!psColor.color_id)
            {
                colorErrors.push(_t('site.ps', 'Please set colors for each material'));
            }

            if(!psColor.price)
            {
                colorErrors.push(_t('site.ps', 'Please set correct prices for chosen materials'));
            }

            if(psColor.price == 0)
            {
                errors.push(_t('site.ps', 'Please set price more then 0'));
            }

            if(psColor.price > 1000)
            {
                errors.push(_t('site.ps', 'Please set price less then 1000'));
            }

        });
        errors = errors.concat(_.uniq(colorErrors));

        this.$$minOrderPriceEnabled = true;

        if (this.min_order_price<1)
        {
            errors.push(_t('site.ps', 'Please set the minimum printer fee to be greater than 1'));
        }

        return errors;
    };

    /**
     *
     * @param {int} deliveryTypeId
     */
    PsPrinter.prototype.getDeliveryTypeById = function(deliveryTypeId)
    {
        return _.find(this.deliveryTypes, function(deliveryType){ return deliveryType.delivery_type_id == deliveryTypeId});
    };

    /**
     * Return redefined property by property_id
     * @param id
     */
    PsPrinter.prototype.getRedifinedPropertyByPropertyId = function(id)
    {
        if(!this.redifinedProperties)
        {
            throw "Redefined properties for psPrinter not loaded";
        }
        return _.find(this.redifinedProperties, function(property){ return property.property_id == id});
    };

    /**
     * Is new printer
     * @returns {boolean}
     */
    PsPrinter.prototype.getIsNew = function()
    {
        return !this.id;
    };

    return PsPrinter;
});
