"use strict";

/**
 * Printer resource
 */
app.factory('Printer', function($resource)
{
    /**
     * @property {number} maxPositionalAccuracy
     */
    var Printer = $resource('', {},{

        info : {method: 'get', url : '/mybusiness/edit-printer/printer'},
        list : {method: 'get', url : '/mybusiness/edit-printer/printers', isArray : true}

    });

    Printer.PROPERTY_ID_BUILD_VOLUME = 1;
    Printer.PROPERTY_ID_LAYER_RESOLUTION_LOW = 5;
    Printer.PROPERTY_ID_LAYER_RESOLUTION_HIGHT = 6;

    /**
     * Check that properties is loaded
     */
    Printer.prototype.checkPropertiesLoaded = function()
    {
        if(!this.properties)
        {
            throw new Error("Properties for printer not loaded");
        }
    };

    /**
     * Return property by id
     * @param id
     */
    Printer.prototype.getPropertyById = function(id)
    {
        this.checkPropertiesLoaded();
        return _.find(this.properties, function(property){ return property.property_id == id});
    };


    /**
     * Return editable properties
     * @returns {*}
     */
    Printer.prototype.getEditableProperties = function()
    {
        this.checkPropertiesLoaded();
        return _.filter(this.properties, function(property){ return property.is_editable == true});
    };

    /**
     * Is printer have editable properties
     * @returns {boolean}
     */
    Printer.prototype.haveEditableProperties = function()
    {
        return !_.isEmpty(this.getEditableProperties());
    };

    return Printer;
});



