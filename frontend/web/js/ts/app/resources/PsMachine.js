"use strict";

/**
 * Printservice Printer resource
 */
app.factory('PsMachine', function($resource)
{
    var PsMachine = $resource('', {id : '@id'},{
        info : {method: 'get', url : '/mybusiness/edit-printer/ps-printer'},
        updateDelivery : {method : 'post', url : '/mybusiness/edit-printer/update-delivery'},
        create : {method : 'post', url : '/mybusiness/services/create-printer'},
        delete : {method : 'delete', url : '/mybusiness/edit-printer/delete-printer/:id'}
    });

    PsMachine.DELIVERY_TYPE_PICKUP_ID = 1;
    PsMachine.DELIVERY_TYPE_INTERNATIONAL_ID = 5;



    /**
     *
     * @param {int} deliveryTypeId
     */
    PsMachine.prototype.getDeliveryTypeById = function(deliveryTypeId)
    {
        return _.find(this.deliveryTypes, function(deliveryType){ return deliveryType.delivery_type_id == deliveryTypeId});
    };

    /**
     * Is new printer
     * @returns {boolean}
     */
    PsMachine.prototype.getIsNew = function()
    {
        return !this.id;
    };

    PsMachine.prototype.updateCountryPrice = function (countryId, value) {
        var index;
        this.updateInternationType(function (deliveryType){
            if(!Array.isArray(deliveryType.countries)) {
                deliveryType.countries = [];
            }
            index = _.findIndex(deliveryType.countries,function(c){ return c.id == countryId});
            if(index === -1) {
                deliveryType.countries.push({id: countryId, price: value});
            } else {
                deliveryType.countries[index].price = value;
            }
            return deliveryType;
        });
    }

    PsMachine.prototype.updateEuropaPrice = function (countries) {
        this.updateInternationType(function (deliveryType){
            deliveryType.countries = countries;
            return deliveryType;
        });
    }

    PsMachine.prototype.updateInternationType = function (cb) {
        this.deliveryTypes = this.deliveryTypes.map(function (deliveryType){
            if(deliveryType.delivery_type_id === PsMachine.DELIVERY_TYPE_INTERNATIONAL_ID) {
                deliveryType = cb(deliveryType);
            }
            return deliveryType;
        });
    }

    /**
     * Validate printer location
     * @todo This code like Paul - need change it to angular + server validation
     * @returns {string[]} Array of errors
     */
    PsMachine.prototype.validateLocation = function()
    {
        var errors = [];

        if(!this.location.lat || !this.location.lon)
        {
            errors.push(_t('site.ps', 'Please choose place on map by clicking "Shop on map" button'));
        }

        if(!this.location.address || this.location.address.length < 5)
        {
            errors.push(_t('site.ps', 'Please enter address'));
        }

        if(_.isEmpty(this.location.city))
        {
            errors.push(_t('site.ps', 'Please enter name of city'));
        }

        if(_.isEmpty(this.location.region))
        {
            // errors.push(_t('site.ps', 'Please enter region name'));
        }

        if(_.isEmpty(this.location.zip_code)) {
            errors.push(_t('site.ps', 'Please enter your ZIP code'));
        }

        return errors;
    };

    /**
     * Validate printer location
     * @todo This code like Paul - need change it to angular + server validation
     * @returns {string[]} Array of errors
     */
    PsMachine.prototype.validateDelvieryTypes = function()
    {
        var errors = [];

        if(_.isEmpty(this.deliveryTypes))
        {
            errors.push(_t('site.delivery', 'Please check at least one delivery option'));
        }

        var isNotSettedRequiredFreeDelivery = false;
        var isNotSettedCarrierPrice = false;
        _.each(this.deliveryTypes, function(deliveryType)
        {
            if(deliveryType.$$requireFreeDelivery && !deliveryType.free_delivery)
            {
                isNotSettedRequiredFreeDelivery = true;
            }
            if(deliveryType.$$requireCarrierPrice && deliveryType.delivery_type_id != PsMachine.DELIVERY_TYPE_PICKUP_ID)
            {
                if(deliveryType.carrier === 'myself' &&  isNaN(parseInt(deliveryType.carrier_price))){
                    isNotSettedCarrierPrice = true;
                }
            }

            if(!deliveryType.carrier){
                isNotSettedCarrierPrice = true;
            }

            if (deliveryType.delivery_type_id == PsMachine.DELIVERY_TYPE_PICKUP_ID && !deliveryType.comment){
                errors.push(_t('site.ps', 'Please fill out Working hours'));
            }

            if(deliveryType.$$requirePackagingFee && !deliveryType.packing_price) {
                errors.push(_t('site.delivery', 'Please Insert a Packaging fee.'));
            }
        });

        if(isNotSettedRequiredFreeDelivery)
        {
            errors.push(_t('site.ps', 'Please set correct amount required for free delivery of orders'));
        }
        if(isNotSettedCarrierPrice)
        {
            errors.push(_t('site.ps', 'Please set correct shipping cost for carrier'));
        }

        return errors;
    };

    return PsMachine;
});
