app.directive('checkoutPaymentMethods', function ($compile)
{
    return {
        restrict: "AE",
        link: function($scope, $el) {
            $scope.$watch('content', function() {
                var $q = $($scope.content);
                $el.html($q);
                $compile($q)($scope);
            });

            $scope.choosePaymentMethod = function (method) {
                if (typeof $scope.$parent.choosePaymentMethod === "function") {
                    $scope.$parent.choosePaymentMethod(method);
                } else {
                    console.log('method choosePaymentMethod() not fount');
                }
            };

            $scope.isSelectedPaymentMethod = function (method) {
                if (typeof $scope.$parent.isSelectedPaymentMethod === "function") {
                    return $scope.$parent.isSelectedPaymentMethod(method);
                }

                console.log('method isSelectedPaymentMethod() not fount');
                return false;
            };
        },
        scope: {
            content:'=',
            paymentData: '='
        }
    };
});
