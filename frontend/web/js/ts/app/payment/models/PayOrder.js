/**
 * @property {number} id
 * @property {boolean} isPickupDelivery
 * @property {PaymentInvoice} primaryPaymentInvoice
 * @property {objects} shipAddress
 * @property {boolean} hasDelivery
 * @property {array} availablePaymentMethods
 *
 * @property {objects} machine
 * @property {string} machine.layerResolution
 *
 * @property {objects} model3d
 * @property {objects|null} model3d.texture
 * @property {string} model3d.texture.materialTitle
 * @property {string} model3d.texture.fillPercent
 * @property {string} model3d.texture.colorTitle
 *
 * @param data
 * @constructor
 */
function PayOrder(data) {
    angular.extend(this, data);

    if (this.primaryPaymentInvoice) {
        this.primaryPaymentInvoice = new PaymentInvoice(this.primaryPaymentInvoice);
    }
}

/**
 * @returns {string|null}
 */
PayOrder.prototype.getFirstPaymentMethod = function () {
    if (this.availablePaymentMethods && this.availablePaymentMethods.length > 0) {
        return this.availablePaymentMethods[0];
    }

    return null;
};