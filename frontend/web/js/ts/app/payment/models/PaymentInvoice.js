/**
 * @property {string} uuid
 * @property {string} currency
 * @property {string} amountTotal
 * @property {bool} allowPromoCode
 * @property {string} accruedBonus
 * @property {PaymentInvoiceItem[]} invoiceItems
 * @property {objects} paymentInvoicePaymentMethods
 * @property {StoreOrderPromocode} storeOrderPromocode
 *
 * @param data
 * @constructor
 */
function PaymentInvoice(data) {
    this.load(data);
}

PaymentInvoice.prototype.load = function (data) {
    angular.extend(this, data);
    this.invoiceItems = [];

    if (data.invoiceItems) {
        data.invoiceItems.forEach(function (item) {
            this.invoiceItems.push(new PaymentInvoiceItem(item));
        }, this);
    }

    if (data.storeOrderPromocode) {
        this.storeOrderPromocode = new StoreOrderPromocode(data.storeOrderPromocode);
    }
};

/**
 * @returns {string|null}
 */
PaymentInvoice.prototype.getFirstPaymentMethod = function () {
    if (this.paymentInvoicePaymentMethods && this.paymentInvoicePaymentMethods.length > 0) {
        return this.paymentInvoicePaymentMethods[0].vendor;
    }

    return null;
};

/**
 * @returns {boolean}
 */
PaymentInvoice.prototype.hasPromoCode = function () {
    return this.storeOrderPromocode !== null;
};

/**
 * @property {string} title
 * @property {string} description
 * @property {string} total_line
 * @property {string} type
 *
 * @param data
 * @constructor
 */
function PaymentInvoiceItem(data) {
    angular.extend(this, data);
}

/**
 * @property {string} amountTotal
 * @property {string} promocode
 *
 * @constructor
 */
function StoreOrderPromocode(data) {
    angular.extend(this, data);
}