"use strict";

app.controller('BalanceInputController', function ($scope, controllerParams)
{
    $scope.isSubmit = false;
    $scope.minInput = controllerParams.minInput;
    $scope.currency = 'USD';

    $scope.checkout = function () {
        $scope.isSubmit = true;
    }
});