"use strict";

app.controller('PayOrderController', function ($scope, $router, $notifyHttpErrors, $notify, $http, $sce, $modelRender, controllerParams)
{
    /**
     * @type {PayOrder|null}
     */
    $scope.payOrder = null;
    $scope.paymentInvoice = null;
    if (controllerParams['model3dState']) {
        $scope.model3dTextureState = new ModelTextureState(controllerParams['model3dState']);
    }

    $scope.paymentData = {
        selectedMethod: null
    };

    /**
     * @returns {PayOrder|objects}
     */
    $scope.hasAddress = function ()
    {
        return $scope.payOrder &&
            ($scope.paymentInvoice.paymentFor == 'pay_for_print_model3d') &&
            ($scope.payOrder.shipAddress)
    };

    $scope.getModel3dPartPreviewUrl = function (model3dPart) {
        return $modelRender.getFullUrl(model3dPart, $scope.model3dTextureState.getColorForRendering(model3dPart));
    };


    /**
     * @param method
     */
    $scope.choosePaymentMethod = function (method)
    {
        $scope.paymentData.selectedMethod = method;
        return $http.post($router.getInvoiceBillingDetails($scope.paymentInvoice.uuid), {
            vendor: $scope.paymentData.selectedMethod
        }).then(function (response) {
            $scope.paymentInvoice.load(response.data.PaymentInvoice);
        }).catch(function (response) {
            $notifyHttpErrors(response);
        });
    };

    /**
     * @param payOrder
     */
    $scope.loadPayOrder = function(paymnetInvoice, payOrder)
    {
        $scope.paymentInvoice = new PaymentInvoice(paymnetInvoice);
        $scope.payOrder = new PayOrder(payOrder);
    };

    /**
     * @param method
     * @returns {boolean}
     */
    $scope.isSelectedPaymentMethod = function (method)
    {
        return $scope.paymentData.selectedMethod === method
    };

    /**
     * Start page
     */
    $scope.init = function()
    {
        $scope.loadPayOrder(controllerParams.invoice, controllerParams.PayOrder);
    };

    $scope.init();
}).filter('safeHtml', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
});;