"use strict";

app.controller('EarningController', function ($scope, $modal)
{
    $scope.showPopUp = function () {
        $modal.open({
            template: '/static/templates/popUpPay.html'
        });
    }
});