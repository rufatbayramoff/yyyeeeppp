"use strict";

app.controller('PayOrderPositionController', function ($scope, $router, $notifyHttpErrors, $notify, $http, $sce, controllerParams)
{
    /**
     * @param method
     */
    $scope.choosePaymentMethod = function (method)
    {
        $scope.paymentData.selectedMethod = method;

        return $http.post($router.getAdditionServiceBillingDetails($scope.paymentInvoice.uuid), {
            vendor: $scope.paymentData.selectedMethod
        }).then(function (response) {
           $scope.loadPaymentInvoice(response.data.PaymentInvoice);
        }).catch(function (response) {
            $notifyHttpErrors(response);
        });
    };

    /**
     * @param paymentInvoice
     */
    $scope.loadPaymentInvoice = function(paymentInvoice)
    {
        $scope.paymentInvoice = new PaymentInvoice(paymentInvoice);

        if (!$scope.paymentData.selectedMethod) {
            $scope.paymentData.selectedMethod = $scope.paymentInvoice.getFirstPaymentMethod();
        }
    };

    /**
     * @param method
     * @returns {boolean}
     */
    $scope.isSelectedPaymentMethod = function (method)
    {
        return $scope.paymentData.selectedMethod === method
    };

    /**
     * Start page
     */
    $scope.init = function()
    {
        $scope.loadPaymentInvoice(controllerParams.PaymentInvoice);
    };

    $scope.init();
});