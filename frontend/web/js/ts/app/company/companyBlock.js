"use strict";
app.controller('CompanyBlockController',
    ['$scope', '$user', '$router', '$http', '$notify', '$notifyHttpErrors', 'controllerParams',
        function ($scope, $user, $router, $http, $notify, $notifyHttpErrors, controllerParams) {
            $scope.companyBlock = new CompanyBlock(controllerParams['companyBlock']);

            $scope.beforeSubmit = function () {
                var el = document.getElementById('companyblock-videos');
                if(el)
                    el.value = angular.toJson($scope.companyBlock.videos);
                window.onbeforeunload = null;
            };


        }
    ]
);

function CompanyBlock(data) {
    this.load(data);
}

CompanyBlock.prototype.load = function (data) {
    angular.extend(this, data);
    if (typeof(this.videos) === 'undefined' || !this.videos) {
        this.videos = [];
    }
    if(data['videos']){
        this.videos = this.videos.map(function (raw) {
            return new CompanyBlockVideo(raw);
        });
    }
    this.addVideo = function (link) {
        var video = new CompanyBlockVideo({url: link});
        if(video.isValidUrl()){
            var isDublicated = this.videos.filter(function(item) {
                return item.url === link;
            })[0];
            if(!isDublicated)
                this.videos.push(video);
        }
        link = null;
    }
    this.removeVideo = function(index){
        this.videos.splice(index, 1);
    }
    this.showVideoPreview = function(video, el){
        document.getElementById(el).innerHTML = video.getEmbedHtml();
    }
};

/**
 * @property {string} url
 * @property {string} thumb
 * @param data
 * @constructor
 */
function CompanyBlockVideo(data) {
    angular.extend(this, data);
    var videoId = getYoutubeVideoId(this.url);
    this.videoId = videoId;
    this.thumb = 'https://img.youtube.com/vi/' + videoId + '/default.jpg';

    function getYoutubeVideoId(url) {
        var videoId = url.split('v=')[1];
        if(!videoId) return null;
        var ampersandPosition = videoId.indexOf('&');
        if(ampersandPosition != -1) {
            videoId = videoId.substring(0, ampersandPosition);
        }
        return videoId;
    }
    this.isValidUrl = function(){
        if(this.videoId){
            return true;
        }
        return false;
    }
    this.getEmbedHtml = function(){
        // return '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+this.videoId+'?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        return '<object style="width: 560px; height: 315px; float: none; clear: both; margin: 2px auto;" data="https://www.youtube.com/embed/'+this.videoId+'"></object>';
    }
}