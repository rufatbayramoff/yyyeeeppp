"use strict";

/**
 * @property {string} uuid
 * @property {string} title
 * @property {string} description
 * @property {array} tags
 * @property {array} images
 * @property {number} single_price
 * @property {string} unitType
 * @property {array} moqPrices
 * @property {array} customProperties
 * @param data
 * @constructor
 */
function CompanyService(data) {
    this.load(data);
}

CompanyService.prototype.load = function (data) {
    angular.extend(this, data);
    if (typeof(this.videos) === 'undefined' || !this.videos) {
        this.videos = [];
    }

    if (data['videos']) {
        this.videos = this.videos.map(function (raw) {
            return new UserVideo(raw);
        });
    }
    if (typeof(this.images) === 'undefined' || !this.images) {
        this.images = [];
    }
    if (data['images']) {
        this.images = this.images.map(function (raw) {
            return new CompanyServiceImage(raw);
        });
    }
    if (typeof(this.customProperties) === 'undefined' || !this.customProperties) {
        this.customProperties = new UserDefinedProperties( { 'customProperties': [{ title: '', value: '', position: 1}] });
    }else {
        this.customProperties = new UserDefinedProperties({'customProperties': data['customProperties']});
    }
    if (typeof(this.productPrices) === 'undefined' || !this.productPrices) {
        this.productPrices = [{ moq: 1 }];
    }

    this.addVideo = function (link) {
        var video = new UserVideo({url: link});
        if (video.isValidUrl()) {
            var isDublicated = this.videos.filter(function (item) {
                return item.url === link;
            })[0];
            if (!isDublicated)
                this.videos.push(video);
        }
        link = null;

        window.onbeforeunload = function () {
            if (document.getElementById('productform-videos').value == '') {
                return "Changes you made may not be saved.";
            }
            return true;
        }
    }
    this.removeVideo = function (index) {
        this.videos.splice(index, 1);
    }
    this.showVideoPreview = function (video, el) {
        document.getElementById(el).innerHTML = video.getEmbedHtml();
    }

    this.removeImage  = function (file) {
        var picture = this.images.indexOf(file);
        this.images.splice( picture, 1);
    };

    this.addPrice = function() {
        var price = {moq: 1};
        this.productPrices.push(price);
    }

    this.getData = function(){
        var result = {};
        var self = this;
        for(var j in self) {
            if (self.hasOwnProperty(j) && self[j]!=null && typeof(self[j])!=='function') {
                var res2 = undefined;
                if(typeof(self[j])==='object'){
                    res2 = {};
                    for(var i in self[j]){
                        if (self[j].hasOwnProperty(i) && typeof(self[j][i])!=='function') {
                            res2[i] = self[j][i];
                        }
                    }
                }else{
                    res2 = self[j];
                }
                result[j] = res2;
            }
        }
        result.customProperties = result.customProperties.customProperties;
        if(result.images[0]){
            result.CompanyService = {};
            result.CompanyService.images = result.images;
            result.CompanyService.imagesOptions = [];

            for (var key in result.CompanyService.images) {
                if (result.CompanyService.images[key].hasOwnProperty('fileOptions')) {
                    result.CompanyService.imagesOptions[key] = result.CompanyService.images[key].fileOptions;
                } else {
                    result.CompanyService.imagesOptions[key] = [];
                }
            }

            delete result.images;
        }
        return result;
    }
};

/**
 * @property {string} uuid
 * @property {string} src
 * @property {string} fileUrl
 * @property {string} cssClass
 */
function CompanyServiceImage(data) {
    angular.extend(this, data);
}
