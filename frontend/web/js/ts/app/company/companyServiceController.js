"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('CompanyServiceCreateController', function ($scope, $notify, $router, $http, $notifyHttpErrors, $interval, $formValidator, $googleLocationApi, controllerParams) {
    /**
     * Is new ps
     * @returns {boolean}
     */
    $scope.getIsNew = function () {
        return !$scope.companyService.id;
    };
    $scope.unitTypes = controllerParams.unitTypes;

    $scope.companyService = new CompanyService(controllerParams.companyService);
    $googleLocationApi.init();
    $googleLocationApi.registerUpdateProperty($scope.companyService, 'shipFrom');
    $scope.$watchCollection('companyService.images', function (newNames, oldNames) {
        _.each($scope.companyService.images, function (file) {
            if (!(file instanceof File)) {
                return;
            }
            var reader = new FileReader();
            reader.onload = function (e) {
                file.src = e.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(file);
        });
    });

    $scope.$watch('companyService.unitType', function(){
        updateServiceUnitLabel();
    });
    $scope.$watch('companyService.productPrices', function(){
        updateServiceUnitLabel();
    }, true);
    function updateServiceUnitLabel(){
        if($scope.companyService.productPrices[0].moq > 1){
            $scope.companyService.unitTypeLabel = $scope.unitTypes[$scope.companyService.unitType];
        }else{
            $scope.companyService.unitTypeLabel = $scope.companyService.unitType;
        }
    }
    /**
     * Validate ps
     * @returns {Array}
     */
    $scope.validate = function (action) {
        var errors = [];
        if(action=='unpublish'){
            return errors;
        }
        if (!$scope.companyService.title) {
            errors.push(_t('company.service', 'Please enter service title'));
        }
        if (!$scope.companyService.description) {
            errors.push(_t('company.service', 'Please provide a description for the service'));
        }
        if ($scope.companyService.images.length > 20) {
            errors.push(_t('company.service', "Portfolio can't have more than 20 images"));
        }
        if($scope.companyService.images.length < 1){
            errors.push(_t('site.product', 'Please upload at least one photo'))
        }
        return errors;
    };

    $scope.importProperties = function () {
        var lines = $scope.propsDump.trim().split("\n");
        var result = detectProperties(lines);
        for (var key in result) {
            if (!result.hasOwnProperty(key) || key === '') {
                continue;
            }
            var position = $scope.companyService.customProperties.customProperties.length + 1;

            $scope.companyService.customProperties.customProperties.push({title: key, value: result[key], position: position});
        }
        $scope.propsDump = '';
    }
    /**
     * @returns {boolean}
     */
    $scope.submitForm = function (action) {
        var errors = $scope.validate(action);
        if (!_.isEmpty(errors)) {
            $notify.error(errors);
            return false;
        }
        var url = $scope.getIsNew() ? '/mybusiness/company-services/company-service/create' : '/mybusiness/company-services/company-service/update';
        // set list of tags
        $scope.companyService.tags = $('#companyservice-tags').val();
        $scope.companyService.submitMode = action;
        return $http.post(url, $scope.companyService.getData())
            .then(function (data) {
                var answer = data['data'];
                if (answer['success']) {
                    if ($scope.companyService.isNew) {
                        $router.setUrl('/mybusiness/company-services/company-service/edit?id=' + answer['model']['id']);
                        return;
                    } else {
                        $notify.success(_t('mybusiness.service', 'Service saved'));
                        window.onbeforeunload = null;
                    }
                    $scope.companyService = new CompanyService(answer['model']);
                    //$router.reload();
                    return;
                }else if(answer['message']){
                    $notify.error(answer['message']);
                }

                if (answer['validationErrors']) {
                    $scope.processValidationErrors(answer['validationErrors']);
                }
                return;
            })
            .catch($notifyHttpErrors);

    };

    $scope.processValidationErrors = function (validationErrors) {
        $notify.error(validationErrors);
        $formValidator.applyValidateById(validationErrors, 'companyservice-');
    };
}) ;