"use strict";

/**
 * Controller cashback controll
 */
app.controller('CashbackController', function ($scope, $notify, $router, $http, $notifyHttpErrors, controllerParams) {
    $scope.companyCashbackPercent = controllerParams.companyCashbackPercent;
    $scope.percentPlus = $scope.companyCashbackPercent * 2;
    $scope.saveTimeout = 0;

    $scope.updatePercent = function () {
        $scope.percentPlus = $scope.companyCashbackPercent * 2;
        $scope.saveCompanyCashback()
    };


    $scope.saveCompanyCashbackTimeout = function () {
        return $http.post('/mybusiness/cashback/settings-save', {
            'percent': $scope.companyCashbackPercent
        })
            .then(function (data) {
                if (data.data.success) {
                    new TS.Notify({
                        type: 'success',
                        text: _t('site.common', 'Saved'),
                        automaticClose: true
                    });
                }
            })
            .catch(function (data) {
                $notifyHttpErrors(data);
                return $q.reject(data);
            });
    };

    $scope.saveCompanyCashback = function () {
        clearTimeout($scope.saveTimeout);
        $scope.saveTimeout = setTimeout(function() {
            $scope.saveCompanyCashbackTimeout();
        }, 800);
    };

});
