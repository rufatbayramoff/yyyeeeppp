"use strict";

/**
 * @property {CsWindowQuoteItem[]} quoteItems
 * @property {null|CsWindowQuoteItem} quoteItemActive
 * @property {string} measurement
 * @property {string} baseMeasurement
 * @property {string|null} phone
 * @property {string|null} email
 * @property {string|null} cs_window_snapshot_uid
 * @property {string|null} contact_name
 * @property {string|null} address
 * @property {string|null} notes
 * @property {string} status
 *
 * @property {Money} total_price
 *
 * @param data
 * @constructor
 */
function CsWindowQuote(data) {
    this.quoteItems = [];
    this.quoteItemActive = null;
    this.measurement = 'mm';
    this.baseMeasurement = 'mm';
    this.phone = null;
    this.email = null;
    this.cs_window_snapshot_uid = null;
    this.contact_name = null;
    this.address = null;
    this.notes = null;
    this.total_price = 0;
    angular.extend(this, data);

    this.total_price = new Money({
        amount: 0,
        currency: 'USD'
    });

    this._nextTempId = 1;

    /**
     * @param data
     * @param setActive
     * @returns {CsWindowQuoteItem}
     */
    this.createQuote = function (data, setActive) {
        var quoteItem = new CsWindowQuoteItem(data);

        setActive = setActive || false;

        quoteItem.id = ++this._nextTempId;
        quoteItem.quote = this;
        quoteItem.init();

        this.quoteItems.push(quoteItem);

        if (setActive) {
            this.setQuoteActive(quoteItem);
        }

        return quoteItem;
    };

    /**
     * @returns {number}
     */
    this.getNextTempId = function () {
        if (this.quoteItems.length === 0) {
            this._nextTempId = 1;
        }

        return this._nextTempId;
    };

    /**
     * @returns {CsWindowQuoteItem|null}
     */
    this.getLastQuote = function () {
        if (this.quoteItems.hasOwnProperty(this.quoteItems.length - 1)) {
            return this.quoteItems[this.quoteItems.length - 1];
        }

        return null;
    };

    /**
     * @param {CsWindowQuoteItem|null} quoteItem
     */
    this.setQuoteActive = function (quoteItem) {
        this.quoteItemActive = quoteItem;
    };

    /**
     * @param {CsWindowQuoteItem} quoteItem
     * @returns {boolean}
     */
    this.isQuoteActive = function (quoteItem) {
        if (!quoteItem || !this.quoteItemActive) {
            return false;
        }

        return this.quoteItemActive.id === quoteItem.id;
    };

    /**
     * @param {CsWindowQuoteItem} quoteItem
     * @returns {boolean}
     */
    this.removeQuote = function (quoteItem) {
        var index = this.quoteItems.indexOf(quoteItem);

        if (index !== -1) {
            this.quoteItems.splice(index, 1);

            if (this.isQuoteActive(quoteItem)) {
                this.setQuoteActive(this.getLastQuote());
            }

            return true;
        }

        return false;
    };

    /**
     * @returns {boolean}
     */
    this.hasQuotes = function () {
        return this.quoteItems && this.quoteItems.length > 0;
    };

    this.getTotalCostFormat = function () {
        this.total_price.amount = 0;

        if (this.hasQuotes()) {
            this.quoteItems.forEach(function (quoteItem) {
                /** @namespace {CsWindowQuoteItem} */
                this.total_price.amount += quoteItem.calculation.getCost() * quoteItem.qty;
            }, this);
        }

        return _t('csWindowCalculator', 'Total cost = {totalCost}', {totalCost: this.total_price.amountWithCurrency()});
    };

    /**
     * @returns {string}
     */
    this.getMeasurement = function () {
        return this.measurement;
    };

    /**
     * @param {string} value
     */
    this.setMeasurement = function (value) {
        if(['mm', 'inch'].indexOf(value) !== -1) {
            this.measurement = value;
        } else {
            this.measurement = 'mm';
        }

        if (this.hasQuotes()) {
            this.quoteItems.forEach(function (quoteItem) {
                quoteItem.view.changeMeasurement(this.measurement);
                quoteItem.calculation.updateFull();
            }, this);
        }
    };

    /**
     * @param measurement
     */
    this.setBaseMeasurement = function (measurement) {
        if(['mm', 'inch'].indexOf(measurement) !== -1) {
            this.baseMeasurement = measurement;
            this.setMeasurement(this.baseMeasurement);
        } else if(['m', 'ft'].indexOf(measurement) !== -1) {
            this.baseMeasurement = measurement === 'm' ? 'mm' : 'inch';
            this.setMeasurement(this.baseMeasurement);
        }
    };

    /**
     * @param value
     */
    this.setCsWindowSnapshotUid = function (value) {
        this.cs_window_snapshot_uid = value;
    };

    /**
     *
     * @returns {{phone: null, email: null, cs_window_snapshot_uid: null, contact_name: null, address: null, notes: null, total_price: number, currency: *, measurement: *, items: Array}}
     */
    this.getQuoteData = function () {
        var items = [];

        if (this.hasQuotes()) {
            /** @namespace {CsWindowQuoteItem} quoteItem */
            this.quoteItems.map(function (quoteItem) {
                items.push(quoteItem.getData());
            }, this);
        }

        return {
            phone: this.phone,
            email: this.email,
            cs_window_snapshot_uid: this.cs_window_snapshot_uid,
            contact_name: this.contact_name,
            address: this.address,
            notes: this.notes,
            total_price: +(this.total_price.amount).toFixed(2),
            currency: this.total_price.currency,
            measurement: this.measurement,
            items: items
        };
    };

    /**
     * @param data
     */
    this.loadData = function (data) {
        angular.extend(this, data);

        this.total_price = new Money({
            amount: data.total_price,
            currency: data.currency
        });
    };

    /**
     * @returns {CsWindowQuoteItem[]}
     */
    this.getHasErrorItem = function () {
        return this.quoteItems.filter(function (item) {
            return item.hasErrors();
        }, this);
    };

    this.clearQuote = function () {
        this.quoteItems = [];
        this.total_price.amount = 0;
        this.setQuoteActive(null);
    };

    /**
     * @returns {boolean}
     */
    this.isNew = function () {
        return this.status === 'new';
    };

    /**
     * @returns {boolean}
     */
    this.isViewed = function () {
        return this.status === 'viewed';
    };

    /**
     * @returns {boolean}
     */
    this.isDeleted = function () {
        return this.status === 'deleted';
    };

    /**
     * @returns {boolean}
     */
    this.isResolved = function () {
        return this.status === 'resolved';
    };
}

/**
 * @property {CsWindowQuote} quote
 *
 * @property {number|null} id
 * @property {string|null} title
 * @property {boolean} windowsill
 * @property {boolean} lamination
 * @property {boolean} slopes
 * @property {boolean} tinting
 * @property {boolean} energySaver
 * @property {boolean} installation
 * @property {number} qty
 * @property {Money} cost
 *
 * @property {CsWindowQuoteFrame} frame
 * @property {CsWindowQuoteView} view
 * @property {CsWindowQuoteCalculation} calculation
 *
 * @property {CsWindow.getOptionsPrice} optionsPrice
 * @property {CsWindowProfile} profile
 * @property {CsWindowGlass} glass
 * @property {CsWindowFurniture} furniture
 *
 * @param data
 * @constructor
 */
function CsWindowQuoteItem(data) {
    this.quote = null;

    this.id = null;
    this.title = null;
    this.cost = null;
    this.optionsPrice = null;
    this.windowsill = false;
    this.lamination = false;
    this.slopes = false;
    this.tinting = false;
    this.energySaver = false;
    this.installation = false;
    this.qty = 1;

    this.quoteParameters = {
        profile: {
            width: null,
            height: null
        },
        frame: null
    };

    this._errors = {};

    angular.extend(this, data);

    /**
     * @param key
     * @param value
     */
    this.addError = function (key, value) {
        this._errors[key] = value;
    };

    /**
     * @param key
     * @returns {string|null}
     */
    this.getError = function (key) {
        return this._errors.hasOwnProperty(key) ? this._errors[key] : null;
    };

    /**
     * @param key
     */
    this.clearError = function (key) {
        if (this._errors.hasOwnProperty(key)) {
            delete this._errors[key];
        }
    };

    /**
     * @returns {boolean}
     */
    this.hasErrors = function () {
        return !_.isEmpty(this._errors);
    };

    /**
     * @param frame
     */
    this.setFrame = function (frame) {
        this.frame.load(frame);
        this.calculation.updateFull();
    };

    /**
     * @param {CsWindowProfile|null} profile
     */
    this.setProfile = function (profile) {
        this.profile = profile;
        this.view.resetSliderCeil();
        this.calculation.updateFull();
    };

    /**
     * @param {CsWindowGlass|null} glass
     */
    this.setGlass = function (glass) {
        this.glass = glass;
        this.calculation.updateFull();
    };

    /**
     * @param {CsWindowFurniture|null} furniture
     */
    this.setFurniture = function (furniture) {
        this.furniture = furniture;
        this.calculation.updateFull();
    };

    this.optionsOnChange = function () {
        this.calculation.calcOptions();
    };

    this.sizeOnChange = function () {
        this.quoteParameters.profile.height = +this.quoteParameters.profile.height;
        this.quoteParameters.profile.width = +this.quoteParameters.profile.width;

        if (!this.quoteParameters.profile.height) {
            this.quoteParameters.profile.height = 0;
        }

        if (!this.quoteParameters.profile.width) {
            this.quoteParameters.profile.width = 0;
        }

        var maxW = this.view.convertMeasurement(this.profile.getMaxWidth(), this.quote.baseMeasurement, this.quote.measurement),
            maxH = this.view.convertMeasurement(this.profile.getMaxHeight(), this.quote.baseMeasurement, this.quote.measurement);

        if (maxH < this.quoteParameters.profile.height) {
            this.quoteParameters.profile.height = maxH;
        }

        if (maxW < this.quoteParameters.profile.width) {
            this.quoteParameters.profile.width = maxW;
        }

        this.calculation.updateFull();
    };

    this.getData = function () {
        this.quoteParameters.frame = this.frame.sections;

        return {
            id: this.id,
            title: this.title,
            windowsill: this.windowsill,
            lamination: this.lamination,
            slopes: this.slopes,
            tinting: this.tinting,
            energy_saver: this.energySaver,
            installation: this.installation,
            qty: this.qty,
            cost: this.cost.amount,
            profile_id: this.profile ? this.profile.id : null,
            glass_id: this.glass ? this.glass.id : null,
            furniture_id: this.frame.hasDirectionParams() && this.furniture ? this.furniture.id : null,
            frame_id: this.frame.id,
            quote_parameters: this.quoteParameters
        };
    };

    this.init = function () {
        this.cost = new Money({
            amount: this.cost,
            currency: this.quote.total_price.currency
        });

        // calculator view helper
        this.view = new CsWindowQuoteView(this);
        this.view.init();

        this.frame = new CsWindowQuoteFrame(this);
        if (data['frame']) {
            this.frame.load(data['frame']);

            if (this.quoteParameters.frame) {
                this.frame.setSections(this.quoteParameters.frame);
            }
        }

        this.calculation = new CsWindowQuoteCalculation(this);
        this.calculation.updateFull();
    }
}

/**
 * @property {CsWindowQuoteItem} quoteItem
 *
 * @param {CsWindowQuoteItem} data
 * @constructor
 */
function CsWindowQuoteView(data) {
    this.quoteItem = data;

    // view measurement
    this._currentMeasurement = null;

    var _self = this;

    this.sliderSizeWidthOptions = {
        vertical: false,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showSelectionBar: true,
        ceil: null,
        step: 1,
        precision: 0,
        onEnd: function (sliderId, modelValue, highValue, pointerType) {
            _self.quoteItem.calculation.updateFull();
        }
    };

    this.sliderSizeHeightOptions = {
        vertical: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showSelectionBar: true,
        ceil: null,
        step: 1,
        precision: 0,
        onEnd: function (sliderId, modelValue, highValue, pointerType) {
            _self.quoteItem.calculation.updateFull();
        }
    };

    /**
     * update values for view when switching measurement type
     *
     * @param {string} measurement
     */
    this.changeMeasurement = function (measurement) {
        var fromMeasurement = this._currentMeasurement;
        this._currentMeasurement = measurement;

        if (this._currentMeasurement === 'mm') {
            this.sliderSizeWidthOptions.step = 1;
            this.sliderSizeWidthOptions.precision = 0;
            this.sliderSizeHeightOptions.step = 1;
            this.sliderSizeHeightOptions.precision = 0;
        }

        if (this._currentMeasurement === 'inch') {
            this.sliderSizeWidthOptions.step = 0.001;
            this.sliderSizeWidthOptions.precision = 3;
            this.sliderSizeHeightOptions.step = 0.001;
            this.sliderSizeHeightOptions.precision = 3;
        }

        // width
        this.sliderSizeWidthOptions.ceil = this.convertMeasurement(this.quoteItem.profile.getMaxWidth(), this.quoteItem.quote.baseMeasurement, this._currentMeasurement);

        this.setQuoteProfileWidth(this.convertMeasurement(
            (this.getQuoteProfileWidth() == null ? this.sliderSizeWidthOptions.ceil : this.getQuoteProfileWidth()),
            fromMeasurement,
            this._currentMeasurement
        ));

        if (this.getQuoteProfileWidth() > this.sliderSizeWidthOptions.ceil) {
            this.setQuoteProfileWidth(this.sliderSizeWidthOptions.ceil);
        }
        // ---

        // height
        this.sliderSizeHeightOptions.ceil = this.convertMeasurement(this.quoteItem.profile.getMaxHeight(), this.quoteItem.quote.baseMeasurement, this._currentMeasurement);

        this.setQuoteProfileHeight(this.convertMeasurement(
            (this.getQuoteProfileHeight() == null ? this.sliderSizeHeightOptions.ceil : this.getQuoteProfileHeight()),
            fromMeasurement,
            this._currentMeasurement
        ));

        if (this.getQuoteProfileHeight() > this.sliderSizeHeightOptions.ceil) {
            this.setQuoteProfileHeight(this.sliderSizeHeightOptions.ceil);
        }
        // ---
    };

    /**
     * Write to current quota value width
     *
     * @param value
     */
    this.setQuoteProfileWidth = function (value) {
        this.quoteItem.quoteParameters.profile.width = value;
    };

    /**
     * Current quota value width
     *
     * @returns {null|number}
     */
    this.getQuoteProfileWidth = function () {
        return this.quoteItem.quoteParameters.profile.width;
    };

    /**
     * Write to current quota value height
     *
     * @param value
     */
    this.setQuoteProfileHeight = function (value) {
        this.quoteItem.quoteParameters.profile.height = value;
    };

    /**
     * Current quota value height
     *
     * @returns {null|number}
     */
    this.getQuoteProfileHeight = function () {
        return this.quoteItem.quoteParameters.profile.height;
    };

    /**
     * @param {string|number} value
     * @param {string} measurementFrom
     * @param {string} measurementTo
     * @returns {number}
     */
    this.convertMeasurement = function (value, measurementFrom, measurementTo) {
        if (measurementFrom === measurementTo) {
            return value;
        }

        // convert to inch
        if (measurementFrom === 'mm') {
            return +(value / 25.4).toFixed(2);
        }

        // convert to mm
        if(measurementFrom === 'inch') {
            return +(value * 25.4).toFixed(0);
        }

        return value;
    };

    /**
     * @param value
     * @returns {string}
     */
    this.getProfileSizeConvertRevers = function (value) {
        return '~'+this.convertMeasurement(
            value,
            this._currentMeasurement,
            (this._currentMeasurement === 'mm' ? 'inch' : 'mm')
        )+' '+(this._currentMeasurement === 'mm' ? 'inch' : 'mm')+' clear';
    };

    /**
     * Reset slider when switching profile
     */
    this.resetSliderCeil = function () {
        this.sliderSizeWidthOptions.ceil = null;
        this.sliderSizeHeightOptions.ceil = null;
        this.changeMeasurement(this._currentMeasurement);
    };

    /**
     * @returns {string}
     */
    this.getProfileSize = function () {
        return this.getQuoteProfileWidth() + 'x' + this.getQuoteProfileHeight();
    };

    this.init = function () {
        this._currentMeasurement = this.quoteItem.quote.measurement;
        this.changeMeasurement(this._currentMeasurement);
    };

    this.getCost = function () {
        return this.quoteItem.cost.amountWithCurrency();
    }
}

/**
 * @property {CsWindowQuoteItem} quoteItem
 * @property {number} id
 *
 * @param {CsWindowQuoteItem} data
 * @constructor
 */
function CsWindowQuoteFrame(data) {
    this.quoteItem = data;

    this.id = null;
    this.sections = [];

    var WINDOW_TYPES = ['swivel', 'folding'];

    /**
     * @param data
     */
    this.load = function (data) {
        this.id = data['id'];
        this.sections = commonJs.clone(data['sections']);

        this._treeSections(this.sections, function (section) {
            section.openDirection = [];
        });
    };

    /**
     * @param section
     */
    this.setSections = function (section) {
        this.sections = section;
    };

    /**
     * @param sectionId
     */
    this.changeWindowType = function (sectionId) {
        var section = this.getSectionById(sectionId);

        if (section) {
            if (WINDOW_TYPES.hasOwnProperty(section.openDirection.length)) {
                section.openDirection.push(WINDOW_TYPES[section.openDirection.length])
            } else {
                section.openDirection = [];
            }
        }

        this.quoteItem.calculation.updateFull();
    };

    /**
     *
     * @param sectionId
     *
     * @returns {objects}
     */
    this.getSectionById = function (sectionId) {
        return this._treeSections(this.sections, function (section) {
            if (section.id === sectionId) {
                return section;
            }

            return null;
        });
    };

    /**
     * @param sectionId
     * @param type
     * @returns {boolean}
     */
    this.hasDirection = function(sectionId, type) {
        var param = this.getSectionById(sectionId);

        if (!param) {
            return false;
        }

        return param.openDirection.indexOf(type) !== -1;
    };

    /**
     * @param sectionId
     * @returns {boolean|string}
     */
    this.getTextDirection = function(sectionId) {
        var param = this.getSectionById(sectionId);

        if (!param) {
            return false;
        }
        var result = [];
        param.openDirection.forEach(function(val){
            if(val=='swivel'){
                result.push('tilt');
            }else if(val=='folding'){
                result.push('turn');
            }
        });
        var text = result.join(' ');
        return text;
    };

    /**
     * @returns {boolean}
     */
    this.hasDirectionParams = function () {
        return this._treeSections(this.sections, function (section) {
            return section.openDirection.length > 0;
        });
    };

    this._treeSections = function (sections, callBack) {
        for (var i in sections) {
            if (!sections.hasOwnProperty(i)) {
                continue;
            }

            var res;

            if (typeof callBack === "function") {
                res = callBack(sections[i]);

                if (res) {
                    return res;
                }
            }

            if (sections[i]['sections'] && sections[i]['sections'].length > 0) {
                res = this._treeSections(sections[i]['sections'], callBack);
            }

            if (res) {
                return res;
            }
        }

        return null;
    };
}

/**
 * @property {CsWindowQuoteItem} quoteItem
 *
 * @param {CsWindowQuoteItem} data
 * @constructor
 */
function CsWindowQuoteCalculation(data) {
    this.quoteItem = data;

    this.sectionsSize = [];

    this.profilePrice = 0;
    this.glassPrice = 0;
    this.furniturePrice = 0;
    this.windowsillPrice = 0;
    this.laminationPrice = 0;
    this.slopesPrice = 0;
    this.tintingPrice = 0;
    this.energySaverPrice = 0;
    this.installationPrice = 0;

    this.getCost = function (notInstallation) {
        var price = this.profilePrice
            + this.glassPrice
            + this.furniturePrice
            + this.windowsillPrice
            + this.laminationPrice
            + this.slopesPrice
            + this.tintingPrice
            + this.energySaverPrice;

        if (!notInstallation) {
            price += this.installationPrice;
        }

        return price;
    };

    this.updateCost = function () {
        this.quoteItem.cost.amount = +(this.getCost()).toFixed(2);
    };

    this.updateFull = function () {
        this.calcSectionsSize(this.quoteItem.frame.sections, this.quoteItem.quoteParameters.profile.width, this.quoteItem.quoteParameters.profile.height, true);
        this.calcProfile();
        this.calcGlass();
        this.calcFurniture();
        this.calcOptions();
    };

    /**
     * $prof(2X+2Y) * (*1.5|*1.6)
     */
    this.calcProfile = function () {
        this.profilePrice = 0;

        if (this.sectionsSize.length === 0) {
            return;
        }

        this.sectionsSize.map(function (section) {
            this.profilePrice += this._measurementPriceConvert(this.quoteItem.profile.price)
                * (this._measurementValueConvert(section.width) * 2 + this._measurementValueConvert(section.height) * 2)
                * this.getProfileCoefficient(section.sectionId);
        }, this);

        this.updateCost();
    };

    /**
     * @param sectionId
     * @returns {number}
     */
    this.getProfileCoefficient = function (sectionId) {
        var param = this.quoteItem.frame.getSectionById(sectionId);

        if (!param || !this.quoteItem.furniture) {
            return 1;
        }

        if (param.openDirection.length === 2) {
            return 1.6;
        } else if (param.openDirection.length === 1) {
            return 1.5;
        }

        return 1;
    };

    /**
     * $glass*X*Y
     */
    this.calcGlass = function () {
        this.glassPrice = 0;

        if (this.sectionsSize.length === 0 || !this.quoteItem.glass) {
            return;
        }

        this.sectionsSize.map(function (section) {
            this.glassPrice += this._measurementValueConvert(section.width) * this._measurementValueConvert(section.height) * this._measurementPriceConvert(this.quoteItem.glass.price, true);
        }, this);

        this.updateCost();
    };

    /**
     * 0
     * fur*(2X+2Y) + $swivel
     * fur*(2X+2Y) + $folding
     */
    this.calcFurniture = function () {
        this.furniturePrice = 0;

        if (this.sectionsSize.length === 0 ||
            !this.quoteItem.frame.hasDirectionParams() ||
            !this.quoteItem.furniture
        ) {
            return;
        }

        this.sectionsSize.map(function (section) {
            var param = this.quoteItem.frame.getSectionById(section.sectionId);

            if (param.openDirection.length > 0) {
                this.furniturePrice += this._measurementPriceConvert(this.quoteItem.furniture.price)
                    * (2 * this._measurementValueConvert(section.width) + 2 * this._measurementValueConvert(section.height))
                    + this._calcFurniturePriceSwivelFolding(param);
            }
        }, this);

        this.updateCost();
    };

    /**
     * @param param
     * @returns {number}
     * @private
     */
    this._calcFurniturePriceSwivelFolding = function (param) {
        if (param.openDirection.length === 2) {
           return +this.quoteItem.furniture.price_folding;
        } else if(param.openDirection.length === 1) {
            return +this.quoteItem.furniture.price_swivel;
        }

        return  0;
    };

    /**
     *
     * Get the dimensions of all sections of the window
     *
     * @param sections
     * @param w
     * @param h
     * @param sectionsSizeClear - clear old calculations?
     */
    this.calcSectionsSize = function (sections, w, h, sectionsSizeClear) {
        if (sectionsSizeClear) {
            this.sectionsSize = [];
        }

        sections.map(function (section) {
            if (section['sections'] && section['sections'].length > 0) {
                this.calcSectionsSize(section['sections'], (w / 100) * section.width, (h / 100) * section.height, false);
            } else {
                this.sectionsSize.push({
                    sectionId: section.id,
                    pWidth: section.width,
                    pHeight: section.height,
                    width: (w / 100) * section.width,
                    height: (h / 100) * section.height
                });
            }
        }, this);
    };

    this.calcOptions = function () {
        // $windowsill*X
        if (this.quoteItem.windowsill) {
            this.windowsillPrice = this._measurementValueConvert(this.quoteItem.quoteParameters.profile.width)
                * this._measurementPriceConvert(this.quoteItem.optionsPrice.windowsill);
        } else {
            this.windowsillPrice = 0;
        }

        // $lam(2X+2Y)
        if (this.quoteItem.lamination) {
            this.laminationPrice = (
                    2 * this._measurementValueConvert(this.quoteItem.quoteParameters.profile.width)
                    + 2 * this._measurementValueConvert(this.quoteItem.quoteParameters.profile.height)
                ) * this._measurementPriceConvert(this.quoteItem.optionsPrice.lamination);
        } else {
            this.laminationPrice = 0;
        }

        // $slopes*X
        if (this.quoteItem.slopes) {
            this.slopesPrice = this._measurementValueConvert(this.quoteItem.quoteParameters.profile.width) * this._measurementPriceConvert(this.quoteItem.optionsPrice.slopes);
        } else {
            this.slopesPrice = 0;
        }

        // $tint*X*Y
        if (this.quoteItem.tinting) {
            this.tintingPrice = (
                    this._measurementValueConvert(this.quoteItem.quoteParameters.profile.width)
                    * this._measurementValueConvert(this.quoteItem.quoteParameters.profile.height)
                ) * this._measurementPriceConvert(this.quoteItem.optionsPrice.tinting, true);
        } else {
            this.tintingPrice = 0;
        }

        // $energy*X*Y
        if (this.quoteItem.energySaver) {
            this.energySaverPrice = this._measurementValueConvert(this.quoteItem.quoteParameters.profile.width)
                * this._measurementValueConvert(this.quoteItem.quoteParameters.profile.height)
                * this._measurementPriceConvert(this.quoteItem.optionsPrice.energy_saver, true);
        } else {
            this.energySaverPrice = 0;
        }

        // price * $install
        if (this.quoteItem.installation) {
            var optionPrices = this.quoteItem.optionsPrice;
            this.installationPrice = this.getCost(true) / 100 * optionPrices.installation;
        } else {
            this.installationPrice = 0;
        }

        this.updateCost();
        this._debug();
    };

    /**
     * @returns {number}
     * @private
     */
    this._measurementValueConvert = function (value) {
        return +(value / (this.quoteItem.quote.measurement === 'mm' ? 1000 : 12));
    };

    /**
     * 1000 mm = 3.28 ft
     *
     * @param {number} price
     * @param {boolean} square
     * @returns {number}
     * @private
     */
    this._measurementPriceConvert = function (price, square) {
        if (this.quoteItem.quote.baseMeasurement === this.quoteItem.quote.measurement) {
            return price;
        }

        if (this.quoteItem.quote.baseMeasurement === 'inch') {
            if (square) {
                return +(price * Math.pow(3.28,2));
            }

            return +(price * 3.28);
        }

        if (this.quoteItem.quote.baseMeasurement === 'mm') {
            if (square) {
                return +(price / Math.pow(3.28,2));
            }

            return +(price / 3.28);
        }

        return price;
    };

    this._debug = function () {
        return;
        console.log(' ');
        console.log('--- Calc: ' + this.quoteItem.title + ' ---');
        console.log('Measurement: base ('+ this.quoteItem.quote.baseMeasurement+'), convert ('+this.quoteItem.quote.measurement+')');
        console.log('Profile: ' +  this.profilePrice);
        console.log('Glass: ' +  this.glassPrice);
        console.log('Furniture: ' +  this.furniturePrice);
        console.log('Options:');
        console.log(' - windowsill: ' + this.windowsillPrice);
        console.log(' - lamination: ' + this.laminationPrice);
        console.log(' - slopes: ' + this.slopesPrice);
        console.log(' - tinting: ' + this.tintingPrice);
        console.log(' - energySaver: ' + this.energySaverPrice);
        console.log(' - installation: ' + this.installationPrice);
        console.log('Total Price: ' +  this.getCost());
    }
}