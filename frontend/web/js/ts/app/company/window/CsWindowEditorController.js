"use strict";

app.controller('CsWindowEditorController', function ($scope, $notify, $router, $http, $notifyHttpErrors, $formValidator,  controllerParams) {

    var data = controllerParams['csWindow'] || {};

    $scope.csWindow = new CsWindow(data);

    /**
     * @param obj
     */
    $scope.onAutocompleteAddressChange = function(obj){
        obj.csLocation.location = new GoogleLocation(obj.address);
        obj.csLocation.updated = true;
    };

    /**
     * @param submitMode
     */
    $scope.submitForm = function(submitMode){
        $formValidator.clearErrors();
        $http.post('/mybusiness/cs-window/manage/save', {'csWindow' : angular.toJson($scope.csWindow), 'submitMode' : submitMode })
            .then(function (data) {
                var answer = data['data'];
                if (answer['success']) {
                    $notify.success(_t('service.window', 'Service information saved'));
                    if(answer['csWindow']){
                        $scope.csWindow = new CsWindow(answer['csWindow']);
                    }
                    console.log($scope.csWindow);
                }
                if (answer['validationErrors']) {
                    $scope.processValidationErrors(answer['validationErrors']);
                }
            })
            .catch($notifyHttpErrors);
    };

    /**
     * @param validationErrors
     */
    $scope.processValidationErrors = function (validationErrors) {
        $notify.error(validationErrors);
        $formValidator.applyValidateById(validationErrors, 'csWindow-');
    };
});
