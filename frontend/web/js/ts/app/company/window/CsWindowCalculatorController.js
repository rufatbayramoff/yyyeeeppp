"use strict";

app.controller('CsWindowCalculatorController', function ($scope, $notify, $router, $http, $notifyHttpErrors, $csWindowCalculatorService, $formValidator, controllerParams) {

    var csWindowData       = controllerParams['csWindow'] || {},
        csWindowQuoteData  = controllerParams['csWindowQuote'] || {};

    /**
     * @type {CsWindow}
     */
    $scope.csWindow = new CsWindow(csWindowData);

    /**
     * @type {CsWindowQuote}
     */
    $scope.csWindowQuote = new CsWindowQuote();

    /**
     * @type {CsWindowFrameBundle}
     */
    $scope.csWindowFrame = new CsWindowFrameBundle(controllerParams['windowFrames']);

    $scope.calculatorParam = {
        clearOldCalcMessege: false,
        showAddWindowButton: true,
        step: 'calculator'
    };

    /**
     * @param step
     *
     * @returns {boolean}
     */
    $scope.checkCalculatorStep = function (step) {
        return $scope.calculatorParam.step === step;
    };

    $scope.stepProceedToCheckout = function () {
        $scope.calculatorParam.showAddWindowButton = false;
        $scope.calculatorParam.step = 'form';
        $scope.csWindowQuote.setQuoteActive(null);
    };

    $scope.stepCalculator = function (clearQuote) {
        $scope.calculatorParam.showAddWindowButton = true;
        $scope.calculatorParam.step = 'calculator';

        if (!$scope.csWindowQuote.quoteItemActive) {
            $scope.csWindowQuote.setQuoteActive($scope.csWindowQuote.getLastQuote());
        }

        if (clearQuote) {
            $scope.csWindowQuote.clearQuote();
            $scope.createQuote();
        }
    };

    $scope.stepSendQuoteSuccess = function () {
        $scope.calculatorParam.step = 'success';
    };

    /**
     * create new window quote
     */
    $scope.createQuote = function () {
        $csWindowCalculatorService.createQuote($scope.csWindow, $scope.csWindowQuote, $scope.csWindowFrame);
    };

    /**
     * @param {CsWindowQuoteItem} windowQuote
     */
    $scope.setQuoteActive = function (windowQuote) {
        $scope.csWindowQuote.setQuoteActive(windowQuote);
        $scope.stepCalculator();
    };

    /**
     * @param {CsWindowQuoteItem} windowQuote
     */
    $scope.removeWindowQuote = function (windowQuote) {
        $scope.csWindowQuote.removeQuote(windowQuote);
        $scope.saveCalcQuoteInSession();
    };

    /**
     * @param {CsWindowProfile} profile
     */
    $scope.setQuoteProfile = function(profile) {
        $csWindowCalculatorService.setQuoteProfile($scope.csWindow, $scope.csWindowQuote.quoteItemActive, profile);
    };

    /**
     * @param {CsWindowGlass} glass
     */
    $scope.setCorrectGlass = function(glass) {
        $csWindowCalculatorService.setCorrectGlass($scope.csWindow, $scope.csWindowQuote.quoteItemActive, glass);
    };

    $scope.saveCalcQuoteInSession = function() {
        if (!$scope.checkCalculatorStep('calculator')) {
            return false;
        }

        return $http.post($router.windowCalculatorQuoteInSession($scope.csWindow.uid), {
            csWindowQuote: $scope.csWindowQuote.getQuoteData()
        }).catch($notifyHttpErrors);
    };

    /**
     * Validate and save quote
     *
     * @returns {*}
     */
    $scope.saveQuote = function() {
        var errors = $csWindowCalculatorService.validationQuoteData($scope.csWindow, $scope.csWindowQuote);

        if (!_.isEmpty(errors.quote)) {
            $notify.error(errors.quote);
            return false;
        }

        if (!_.isEmpty(errors.form)) {
            $formValidator.applyValidateById(errors.form, 'windowCalculator-');
            return false;
        }

        if (errors.hasItemsErrors) {
            $scope.csWindowQuote.setQuoteActive($scope.csWindowQuote.getHasErrorItem()[0]);
            $scope.stepCalculator();
            return false;
        }

        return $http.post($router.windowCalculatorSaveQuote($scope.csWindow.uid), {
            csWindowQuote: $scope.csWindowQuote.getQuoteData()
        }).then(function (response) {
            var data = response.data;

            if (data['success'] === true) {
                $scope.stepSendQuoteSuccess();
            } else {
                $notify.error(data['validationErrors']);
            }
        }).catch($notifyHttpErrors);
    };

    /**
     * full init calculator widget
     */
    $scope.initCalculator = function () {
        $scope.csWindowQuote.setCsWindowSnapshotUid($scope.csWindow.uid);
        $scope.csWindowQuote.setBaseMeasurement($scope.csWindow.measurement);

        if (csWindowQuoteData.items && csWindowQuoteData.items.length > 0) {
            if (csWindowQuoteData.cs_window_snapshot_uid === $scope.csWindow.uid) {
                $csWindowCalculatorService.loadQuoteData($scope.csWindow, $scope.csWindowQuote, $scope.csWindowFrame, csWindowQuoteData);
                return;
            } else {
                $scope.calculatorParam.clearOldCalcMessege = true;
                $notify.error(_t('csWindowCalculator', 'Manufacturer made updates and your session was reset.'), false);
            }
        }

        $scope.createQuote();
    };

    // Function calls
    $scope.initCalculator();

    setInterval(function () {
        $scope.saveCalcQuoteInSession();
    }, 30000);
});
