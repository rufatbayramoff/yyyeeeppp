"use strict";

app.controller('CsWindowQuoteViewController', function ($scope, $notify, $router, $http, $formValidator, $notifyHttpErrors, $csWindowCalculatorService, controllerParams) {

    var csWindowData       = controllerParams['csWindow'] || {},
        csWindowQuoteData  = controllerParams['csWindowQuote'] || {};

    /**
     * @type {CsWindow}
     */
    $scope.csWindow = new CsWindow(csWindowData);

    /**
     * @type {CsWindowQuote}
     */
    $scope.csWindowQuote = new CsWindowQuote();

    /**
     * @type {CsWindowFrameBundle}
     */
    $scope.csWindowFrame = new CsWindowFrameBundle(controllerParams['windowFrames']);

    /**
     * @param {string} status
     *
     * @returns {*}
     */
    $scope.statusChange = function (status)
    {
        return $http.post($router.windowQuoteStatusChange($scope.csWindowQuote.uid), {
            status: status
        }).then(function (response) {
            var data = response.data;

            if (data['success'] === true) {
                $scope.csWindowQuote.status = data.csWindowQuote.status;
            } else {
                $notify.error(data['validationErrors']);
            }
        }).catch($notifyHttpErrors);
    };

    $scope.initQuoteView = function () {
        $scope.csWindowQuote.setCsWindowSnapshotUid($scope.csWindow.uid);
        $scope.csWindowQuote.setBaseMeasurement($scope.csWindow.measurement);
        $csWindowCalculatorService.loadQuoteData($scope.csWindow, $scope.csWindowQuote, $scope.csWindowFrame, csWindowQuoteData);
    };

    // Function calls
    $scope.initQuoteView();
});
