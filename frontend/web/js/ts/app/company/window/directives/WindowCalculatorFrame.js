app.directive('windowCalculatorFrame', function () {
    'use strict';

    return {
        restrict: 'AE',
        scope: {
            quote: '=?',
            tplId: '@',
            windowTypeSwitchClick: '=?'
        },
        template: '<ng-include src="getTemplate()"/>',
        link: function($scope, elem) {
            $scope.getTemplate = function() {
                return $scope.tplId;
            };

            if ($scope.windowTypeSwitchClick === true) {
                $scope.$watch('tplId', function() {
                    angular.element(function () {
                        $('.js-windowChangeOpenDirection', elem).on('click', function (e) {
                            $scope.quote.frame.changeWindowType($(this).data('section-id'));
                            $scope.$apply();
                        });
                    });
                });
            }
        }
    }
});