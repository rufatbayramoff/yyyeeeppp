/**
 * @property {string} measurement
 * @property {string} windowsill
 * @property {string} lamination
 * @property {string} slopes
 * @property {string} tinting
 * @property {string} energy_saver
 * @property {string} installation
 * @property {string} status
 * @property {Array} profile
 * @property {int} is_active
 *
 * @param data
 * @constructor
 */
function CsWindow(data) {
    this.uid = null;
    this.isActive = false;
    this.measurement = 'm';
    this.sizeMeasurement = 'mm';
    this.currency = '$';
    this.profile = [new CsWindowProfile()];
    this.glass = [];
    this.furniture = [];
    this.locations = [];

    angular.extend(this, data);

    this.sizeMeasurement = this.measurement === 'm' ? 'mm' : 'inch';

    if (data['profile']) {
        this.profile = this.profile.map(function (raw) {
            if (raw['isDeleted']) {
                return false;
            }

            return new CsWindowProfile(raw);
        }).filter(function (r) {
            return r;
        });
    }

    if (data['glass']) {
        this.glass = this.glass.map(function (raw) {
            if (raw['isDeleted']) {
                return false;
            }

            return new CsWindowGlass(raw);
        });
    }

    if (data['furniture']) {
        this.furniture = this.furniture.map(function (raw) {
            if (raw['isDeleted']) {
                return false;
            }

            return new CsWindowFurniture(raw);
        });
    }

    if (data['locations']) {
        this.locations = this.locations.map(function (raw) {
            if (raw['isDeleted']) {
                return false;
            }

            return new CsLocation(raw);
        });
    }

    if (data['companyService']) {
        this.companyService = new CsWindowService(data['companyService']);
    }

    // functions
    this.addProfile = function () {
        var profile = new CsWindowProfile();
        this.profile.push(profile);
    };

    this.removeProfile = function (item) {
        var index = this.profile.indexOf(item);
        this.profile.splice(index, 1);
    };

    this.addGlass = function () {
        var line = new CsWindowGlass();
        this.glass.push(line);
        this.resetGlassListByThicknessCache();
    };

    this.removeGlass = function (item) {
        var index = this.glass.indexOf(item);
        this.glass.splice(index, 1);
    };

    this.addFurniture = function () {
        var line = new CsWindowFurniture();
        this.furniture.push(line);
    };

    this.removeFurniture = function (item) {
        var index = this.furniture.indexOf(item);
        this.furniture.splice(index, 1);
    };

    this.addLocation = function (address) {
        var csLocation = new CsLocation();
        this.locations.push(csLocation);
    };

    this.removeLocation = function (item) {
        var index = this.locations.indexOf(item);
        this.locations.splice(index, 1);
    };

    /**
     * @returns {boolean}
     */
    this.haveProfile = function () {
        return this.profile && this.profile.length > 0
    };

    /**
     * @param thickness
     * @returns {boolean}
     */
    this.haveGlassByThickness = function (thickness) {

        if (!thickness) {
            return false;
        }

        var list = this.getGlassListByThickness(thickness);
        return list && list.length > 0
    };

    /**
     * this.getGlassListByThickness() result cache
     *
     * @type {{}}
     * @private
     */
    this._glassListByThicknessCache = {};

    /**
     * @param thickness
     */
    this.getGlassListByThickness = function (thickness) {
        if (this._glassListByThicknessCache.hasOwnProperty(thickness)) {
            return this._glassListByThicknessCache[thickness];
        }

        this._glassListByThicknessCache[thickness] = this.glass.filter(function (glass) {
            return glass.thickness <= thickness;
        });

        return this._glassListByThicknessCache[thickness];
    };

    this.resetGlassListByThicknessCache = function () {
        this._glassListByThicknessCache = {};
    };

    /**
     * @returns {boolean}
     */
    this.haveFurniture = function () {
        return this.furniture && this.furniture.length > 0
    };

    /**
     * @returns {CsWindowProfile|null}
     */
    this.getFirstProfile = function () {
        return this.profile[0];
    };

    /**
     * @returns {CsWindowFurniture|null}
     */
    this.getFirstFurniture = function () {
        return this.furniture[0];
    };

    /**
     * @returns {{windowsill: number, lamination: number, slopes: number, tinting: number, energy_saver: number, installation: number}}
     */
    this.getOptionsPrice = function () {
        return {
            windowsill: this.windowsill,
            lamination: this.lamination,
            slopes: this.slopes,
            tinting: this.tinting,
            energy_saver: this.energy_saver,
            installation: this.installation
        }
    };

    this.getProfileById = function (id) {
        for (var key in this.profile) {
            if (!this.profile.hasOwnProperty(key)) {
                continue;
            }

            if (this.profile[key].id === id) {
                return this.profile[key]
            }
        }

        return null;
    };

    this.getGlassById = function (id) {
        for (var key in this.glass) {
            if (!this.glass.hasOwnProperty(key)) {
                continue;
            }

            if (this.glass[key].id === id) {
                return this.glass[key]
            }
        }

        return null;
    };

    this.getFurnitureById = function (id) {
        for (var key in this.furniture) {
            if (!this.furniture.hasOwnProperty(key)) {
                continue;
            }

            if (this.furniture[key].id === id) {
                return this.furniture[key]
            }
        }

        return null;
    };

    this.setMeasurement = function (measurement) {
        this.measurement = measurement;
        this.sizeMeasurement = this.measurement === 'm' ? 'mm' : 'inch';
    }
}

/**
 * @property {boolean} isPublished
 * @property {string} visibility
 * @property {string} visibilityLabel
 * @property {string} moderatorStatusLabel
 *
 * @param data
 * @constructor
 */
function CsWindowService(data) {
    angular.extend(this, data);
}

/**
 * @property {number} id
 * @property {string} title
 * @property {string} thickness
 * @property {string} chambers
 * @property {number} max_width
 * @property {number} max_height
 * @property {string} noise_reduction
 * @property {string} thermal_resistance
 * @property {string} price
 * @property {number} max_glass
 * @param data
 * @constructor
 */
function CsWindowProfile(data) {
    angular.extend(this, data);

    /**
     * @returns {number}
     */
    this.getMaxGlass = function () {
        return this.max_glass ? this.max_glass : 0
    };

    /**
     * @returns {number}
     */
    this.getMaxWidth = function () {
        return this.max_width;
    };

    /**
     * @returns {number}
     */
    this.getMaxHeight = function () {
        return this.max_height;
    };
}

/**
 * @property {string} title
 * @property {string} thickness
 * @property {string} chambers
 * @property {string} noise_reduction
 * @property {string} thermal_resistance
 * @property {string} price
 * @param data
 * @constructor
 */
function CsWindowGlass(data) {
    angular.extend(this, data);
}

/**
 * @property {string} title
 * @property {string} price
 * @property {string} price_swivel
 * @property {string} price_folding
 * @param data
 * @constructor
 */
function CsWindowFurniture(data) {
    angular.extend(this, data);
}

/**
 * @property {GoogleLocation} location
 * @property {integer} radius
 * @property {string} locationFormatted
 * @param data
 * @constructor
 */
function CsLocation(data) {
    this.location = null;
    this.locationFormatted = '';
    this.radius = 10;
    angular.extend(this, data);
    if (this.location) {
        this.locationFormatted = this.location.formatted_address;
    }

}

/**
 * @property {string} street
 * @property {string} street2
 * @property {string} city
 * @property {string} country
 * @property {string} state
 * @property {string} zip
 * @property {string} lat
 * @property {string} lon
 * @property {string} formatted_address
 * @param address
 * @constructor
 */
function GoogleLocation(address) {
    var formattedAddress = address.$$rawPlace ? address.$$rawPlace.formatted_address : address.formatted_address;
    this.street = address.address;
    this.street2 = address.address2;
    this.city = address.city;
    this.country = this.countryIso = address.country_iso;
    this.state = address.region;
    this.zip_code = address.zip_code;
    this.lat = address.lat;
    this.lon = address.lon;
    this.formatted_address = formattedAddress;
}

/**
 * @property {[
 * {
 *      id: number,
 *      sections: objects
 * ]} frame
 *
 * @param data
 * @constructor
 */
function CsWindowFrameBundle(data) {
    this.frame = [];

    this.frame = data.map(function (raw) {
        return {
            id: raw['id'],
            sections: raw['sections']
        };
    });

    this.getFirstFrame = function () {
        return this.frame[0];
    };

    /**
     * @param id
     * @returns {string}
     */
    this.getHtmlId = function (id) {
        return 'windowPreview_'+id;
    };

    /**
     *
     * @param id
     * @returns {*}
     */
    this.getFrameById = function (id) {
        for (var key in this.frame) {
            if (!this.frame.hasOwnProperty(key)) {
                continue;
            }

            if (this.frame[key].id === id) {
                return this.frame[key]
            }
        }

        return null;
    };
}