"use strict";

/**
 * Controller CsWindowCalculatorController
 */
app.factory('$csWindowCalculatorService', function ($modal, $notifyHttpErrors) {

    var $csWindowCalculatorService = {};

    /**
     *
     * @param {CsWindow} csWindow
     * @param {CsWindowQuote} csWindowQuotes
     * @param {CsWindowFrameBundle} csWindowFrame
     *
     * @returns {CsWindowQuoteItem}
     */
    $csWindowCalculatorService.createQuote = function (csWindow, csWindowQuotes, csWindowFrame) {
        var quoteItem = csWindowQuotes.createQuote({
            title: _t('csWindowCalculator', 'window {nextNum}', {nextNum: (csWindowQuotes.getNextTempId())}),
            frame: csWindowFrame.getFirstFrame(),
            profile: csWindow.getFirstProfile(),
            furniture: csWindow.getFirstFurniture(),
            optionsPrice: csWindow.getOptionsPrice()
        }, true);

        $csWindowCalculatorService.setCorrectGlass(csWindow, quoteItem, null);

        return quoteItem;
    };

    /**
     * @param {CsWindow} csWindow
     * @param {CsWindowQuoteItem} quoteItem
     * @param {CsWindowProfile} profile
     */
    $csWindowCalculatorService.setQuoteProfile = function(csWindow, quoteItem, profile) {
        quoteItem.setProfile(profile);
        $csWindowCalculatorService.setCorrectGlass(csWindow, quoteItem, quoteItem.glass);
    };

    /**
     * @param {CsWindow} csWindow
     * @param {CsWindowQuoteItem} quoteItem
     * @param {CsWindowGlass} glass
     */
    $csWindowCalculatorService.setCorrectGlass = function(csWindow, quoteItem, glass) {
        if (glass && quoteItem.profile.getMaxGlass() < glass.thickness) {
            glass = null;
        }

        if (!glass) {
            glass = $csWindowCalculatorService.getDefaultGlassToProfile(csWindow, quoteItem.profile);
        }

        quoteItem.setGlass(glass);
    };

    /**
     *
     * @param {CsWindow} csWindow
     * @param {CsWindowProfile} profile
     * @returns {CsWindowGlass|null}
     */
    $csWindowCalculatorService.getDefaultGlassToProfile = function (csWindow, profile) {
        if (csWindow.haveGlassByThickness(profile.getMaxGlass())) {
            return csWindow.getGlassListByThickness(profile.getMaxGlass())[0];
        }

        return null;
    };

    /**
     * @param {CsWindow} csWindow
     * @param {CsWindowQuote} csWindowQuote
     * @param {CsWindowFrameBundle} csWindowFrame
     * @param quoteData
     */
    $csWindowCalculatorService.loadQuoteData = function (csWindow, csWindowQuote, csWindowFrame, quoteData) {
        csWindowQuote.loadData(quoteData);

        quoteData.items.map(function (item) {
            var profile = csWindow.getProfileById(item.profile_id),
                glass = csWindow.getGlassById(item.glass_id),
                furniture = csWindow.getFurnitureById(item.furniture_id),
                frame = csWindowFrame.getFrameById(item.frame_id);

            if (!profile || !glass || !frame) {
                return;
            }

            var quoteItem = csWindowQuote.createQuote({
                title: item.title,
                quoteParameters: item.quote_parameters,
                profile: profile,
                furniture: furniture ? furniture : csWindow.getFirstFurniture(),
                optionsPrice: csWindow.getOptionsPrice(),
                frame: frame,
                cost: item.cost,
                qty: item.qty,
                energySaver: item.energy_saver,
                installation: item.installation,
                lamination: item.lamination,
                slopes: item.slopes,
                tinting: item.tinting,
                windowsill: item.windowsill,
            }, true);

            $csWindowCalculatorService.setCorrectGlass(csWindow, quoteItem, glass);
        });
    };

    /**
     * @param {CsWindow} csWindow
     * @param {CsWindowQuote} csWindowQuote
     *
     * @returns {{quote: Array, form: {}, hasItemsErrors: boolean}}
     */
    $csWindowCalculatorService.validationQuoteData = function (csWindow, csWindowQuote) {
        var errors = {
            quote: [],
            form: {},
            hasItemsErrors: false,
        };

        if (!csWindowQuote.contact_name) {
            errors.form.contact_name = _t('csWindowCalculator', 'Please enter contact name');
        }

        if (!csWindowQuote.email) {
            errors.form.email = _t('csWindowCalculator', 'Please enter email');
        }

        if (csWindowQuote.quoteItems.length === 0) {
            errors.quote.push(_t('csWindowCalculator', 'Please add windows'));
        } else {
            csWindowQuote.quoteItems.forEach(function (item) {
                if (!item.qty) {
                    item.addError('qty', _t('csWindowCalculator', 'Please enter the qty of windows'));
                }

                if (!item.title) {
                    item.addError('title', _t('csWindowCalculator', 'Please enter window name'));
                }

                var profile = csWindow.getProfileById(item.profile.id);

                if (profile.getMaxWidth() < item.quoteParameters.profile.width) {
                    item.addError('profileWidth', _t('csWindowCalculator', 'Profile width too large'));
                }

                if (profile.getMaxHeight() < item.quoteParameters.profile.height) {
                    item.addError('profileHeight', _t('csWindowCalculator', 'Profile Height too large'));
                }

                if (item.hasErrors()) {
                    errors.hasItemsErrors = true;
                }
            });
        }

        return errors;
    };

    return $csWindowCalculatorService;
});