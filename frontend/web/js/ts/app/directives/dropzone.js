"use strict";

var x = 1;

app.directive('dropzone', function(){

    return {
        restrict : 'A',
        scope : {
            onAccept : '&',
            onError : '&',
            acceptedFiles : '@'
        },
        link : function (scope) {
            console.log("dropzone " + x++);
        },
        controller : function () {
            console.log("dropzone controller  " + x++);
        }
    };
})

.directive('dropzoneClickable', function () {

    return {
        restrict : 'A',
        require : '^dropzone',
        link : function (scope) {
            console.log("dropzoneClickable " + x++);
        },
        controller : function () {
            console.log("dropzoneClickable controller  " + x++);
        }
    }
})

.directive('dropzoneDropable', function () {

    return {
        restrict : 'A',
        require : '^dropzone',
        link : function (scope) {

            console.log("dropzoneDropable " + x++);
        },
        controller : function () {
            console.log("dropzoneDropable controller " + x++);
        }
    }
});