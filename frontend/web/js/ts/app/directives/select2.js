"use strict";

app.directive('select2', function () {
    return {
        restrict: 'A',
        // Todo: add parameters to configure directive
        link: function (scope, element) {
            //var rand = Math.floor(Math.random() * 9999999);
            //var s2optionsName = 's2options_d' + rand;
            //window[s2optionsName] = {"themeCss": ".select2-container--krajee", "doReset": true, "doToggle": false, "doOrder": false};
            var select2Options = {"allowClear": false, "theme": "krajee","containerCssClass": "input-sm"};
            var title = '';
            $(element).select2(select2Options);//.done(initS2Loading(title, s2optionsName));
        }
    };
});
