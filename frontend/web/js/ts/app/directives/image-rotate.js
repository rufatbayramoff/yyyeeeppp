"use strict";

var ImageRotate = function ($main) {
    this.$img = null;
    this.$button = null;
    this.$main = null;

    var nextRotatePos = 0,
        changeImgSrc = 0;

    this.init = function () {
        this.$main = $main;
        this.getImg();
        this.getButton();
    };

    this.getImg = function () {
        if (!this.$img || (this.$img && this.$img.length === 0)) {
            this.$img = this.$main.find('[data-image-rotate-flag="img"]');
        }

        return this.$img;
    };

    this.getButton = function () {
        if (!this.$button || (this.$button && this.$button.length === 0)) {
            this.$button = this.$main.find('[data-image-rotate-flag="button"]');
        }

        return this.$button;
    };

    this.rotate = function () {
        this.getImg();

        var maxH = this.$main.outerWidth(), w, h,
            x = 0, y = 0,
            d = this.getRotateNextPos(),
            or = (d % 180 === 90);

        if (or) {
            w = this.getImg().width() * (maxH / this.getImg().height());
            h = maxH;

            y = (w / 2) - (h / 2);
            x = (h / 2) - (w / 2);
        } else {
            h = this.getImg().height() * (maxH / this.getImg().width());
            w = maxH;
        }

        this.getImg().css({
            width: w + 'px',
            height: h + 'px',
            transform: 'translateX(' + x + 'px) translateY(' + y + 'px) rotate(' + d + 'deg)',
            transformOrigin: 'center center'
        });

        this.$main.css({
            width: maxH + 'px',
            height: (or ? w : h) + 'px',
        });
    };

    /**
     * @returns {number}
     */
    this.getRotateNextPos = function () {
        return nextRotatePos = (nextRotatePos >= 270) ? 0 : nextRotatePos + 90;
    };

    /**
     * @returns {number}
     */
    this.getRotatePos = function () {
        return nextRotatePos;
    };

    /**
     * @param {Boolean} status
     */
    this.buttonDisabled = function (status) {
        this.getButton().prop('disabled', status);
    };

    this.changeImgSrc = function (src) {
        changeImgSrc++;
        src += (src.indexOf('?') === -1 ? '?' : '&') + 'changeimgsrc=' + changeImgSrc;
        this.getImg().attr('src', src);
    };
};

app.directive('imageRotate', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var rotate = new ImageRotate(element),
                file = null;

            rotate.init();

            if (typeof scope[attrs.imageRotate] !== "undefined") {
                file = scope[attrs.imageRotate];
            }

            rotate.getButton().on('click', function () {
                rotate.rotate();

                if (file) {
                    file.fileOptions = {'rotate': rotate.getRotatePos()};
                }
            })
        },
    };
});

app.directive('imageRotateAjax', function ($http, $router, $notifyHttpErrors) {
    return {
        restrict: 'A',
        scope : {
            fileUuid : '@',
            maxSizeW: '@',
            maxSizeH: '@'
        },
        link: function (scope, element) {
            var rotate = new ImageRotate(element),
                fileUuid = scope.fileUuid;

            rotate.init();

            rotate.getButton().on('click', function () {
                rotate.buttonDisabled(true);

                $http.post($router.getImageRotate(fileUuid), {
                    direction: 'right',
                    resizeW: scope.maxSizeW,
                    resizeH: scope.maxSizeH
                }).then(function (data) {
                    var res = data['data'];

                    if (res['success']) {
                        rotate.changeImgSrc(res['url']);
                    }

                    rotate.buttonDisabled(false);
                }).catch($notifyHttpErrors);
            });
        },
    };
});