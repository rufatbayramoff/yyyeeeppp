"use strict";

/**
 * Directive for google maps
 */
app.directive('dropzoneImage', function($parse, $notify)
{
    return {
        restrict : 'A',
        link : function(scope, element, attrs)
        {
            var clickElement = element.find('[dropzone-image-click]');
            var previewImage = element.find('[dropzone-image-preview]');
            var previewTxt = element.find('[dropzone-txt-preview]');
            var modelSetter =  $parse(attrs.dropzoneImage).assign || angular.noop;
            var canChangeAfterSelect = $parse(attrs.canChangeAfterSelect)();

            new Dropzone(element[0], {
                url: '#',
                autoProcessQueue: false,
                autoDiscover: false,
                previewsContainer : false,
                maxFiles : 1,
                clickable: clickElement[0],
                acceptedFiles: "image/jpeg,image/png,image/gif",

                accept: function(file, done)
                {
                    if(!canChangeAfterSelect){
                        clickElement.hide();
                    }
                    done();

                    if(previewImage && window.FileReader)
                    {
                        var reader = new FileReader();
                        reader.onload = function (e)
                        {
                            previewImage.attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);
                    }
                    if(previewTxt){
                        previewTxt.html(file.name);
                    }

                    modelSetter(scope, file);
                    scope.$apply();
                },

                error: function(file, message)
                {
                    $notify.error(message);
                },

                addedfile: function ()
                {
                    this.removeAllFiles();
                }
            });
        }
    }
});