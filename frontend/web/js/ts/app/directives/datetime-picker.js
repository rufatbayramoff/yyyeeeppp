"use strict";

/**
 * Directive date tim picker
 */
app.directive('datetimePicker', function()
{
    return {
        restrict : 'A',
        scope : {
            datetimePicker : '='
        },
        link : function(scope, element)
        {
            var $elem = $(element);


            $elem.datetimepicker({
                    locale: 'en',
                    sideBySide: true,
                    minDate : moment(),
                    format : 'YYYY-MM-DD HH:mm:ss'
                })
                .on('dp.change', function(e)
                {
                    scope.datetimePicker =  e.date.format('YYYY-MM-DD HH:mm:ss');
                    element.data("DateTimePicker").hide();
                });

            scope.$watch('datetimePicker', function (newVal) {
                $elem.data("DateTimePicker").date(newVal)
            });
        }
    }
});
