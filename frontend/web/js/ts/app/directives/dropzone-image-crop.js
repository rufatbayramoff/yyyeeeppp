"use strict";

/**
 * Directive for google maps
 */
app.directive('dropzoneImageCrop', function($parse, $notify, $modal, $timeout)
{
    return {
        restrict : 'A',
        scope : {
            canChangeAfterSelect : '=',
            dropzoneImageCrop : '&'
        },

        link : function(scope, element)
        {
            var clickElement = element.find('[dropzone-image-click]');
            var previewImage = element.find('[dropzone-image-preview]');

            var setData = function(imageFile, circleCrop, squareCrop)
            {
                var reader = new FileReader();
                reader.onload = function (e) {
                    previewImage[0].src = e.target.result;
                };
                reader.readAsDataURL(imageFile);
                scope.dropzoneImageCrop({imageFile : imageFile, circleCrop : circleCrop, squareCrop : squareCrop});
            };

            new Dropzone(element[0], {
                url: '#',
                autoProcessQueue: false,
                autoDiscover: false,
                previewsContainer : false,
                maxFiles : 1,
                clickable: clickElement[0],
                acceptedFiles: "image/jpeg,image/png,image/gif",

                accept: function(file, done)
                {
                    if(!scope.canChangeAfterSelect){
                        clickElement.hide();
                    }
                    done();


                    if(window.FileReader)
                    {
                        var reader = new FileReader();
                        reader.onload = function (readerLoadEvent)
                        {
                            var image = new Image();
                            image.onload = function()
                            {
                                if(image.width < 200 || image.height < 64)
                                {
                                    $notify.error(_t('user.profile', 'Business logo image should not be less than 200 x 64 with a maximum file size of 1.5 MB.'));
                                    return;
                                }

                                var circleCropper;
                                var squreCropper;


                                $modal.open({

                                    //TODO MOVE TO PARAM
                                    template : '/app/ps/ps/change-logo-modal.html',
                                    scope : {

                                        save : function ()
                                        {
                                            this.$dismiss();
                                            setData(file, circleCropper.cropper('getData', true), squreCropper.cropper('getData', true));
                                        }
                                    },
                                    onShown : function ()
                                    {
                                        $timeout(function()
                                        {
                                            var cropImg = $('#circle-image'),
                                                cropImgOrig = cropImg[0];

                                            cropImgOrig.onload = function ()
                                            {
                                                var scale = cropImgOrig.width / cropImgOrig.naturalWidth;
                                                circleCropper = cropImg.cropper({
                                                    aspectRatio: 1,
                                                    guides : false,
                                                    zoomable : false,
                                                    minCropBoxWidth : 64 * scale,
                                                    minCropBoxHeight : 64 * scale,
                                                    preview : '.circle-image-preview'
                                                });
                                            };

                                            cropImgOrig.src = image.src;

                                        });


                                        $timeout(function()
                                        {
                                            var cropImg = $('#square-image'),
                                                cropImgOrig = cropImg[0];

                                            cropImgOrig.onload = function ()
                                            {
                                                var scale = cropImgOrig.width / cropImgOrig.naturalWidth;
                                                squreCropper = cropImg.cropper({
                                                    guides : false,
                                                    zoomable : false,
                                                    minCropBoxWidth : 240 * scale,
                                                    minCropBoxHeight : 64 * scale,
                                                    preview : '.square-image-preview'
                                                });
                                            };

                                            cropImgOrig.src = image.src;
                                        });
                                    }
                                });
                            };
                            image.src = readerLoadEvent.target.result;
                        };
                        reader.readAsDataURL(file);
                    }
                    else{
                        $notify.error(_t("site.common", "Please update browser"));
                    }
                },

                error: function(file, message)
                {
                    $notify.error(message);
                },

                addedfile: function ()
                {
                    this.removeAllFiles();
                }
            });
        }
    };
});







