"use strict";

/**
 * Directive for google maps
 */
app.directive('googleMap', function ($parse, $rootScope, $maps) {
    return {

        require: 'googleMap',
        scope: {
            address: '=googleMap',
            onMarkerPositionChange: '&'
        },

        controller: function ($element, $scope) {
            var ctrl = this;

            /**
             * Return google map instance
             * @returns {google.maps.Map}
             */
            this.getMap = function () {
                return map;
            };

            /**
             * Set address (like locality) to map
             * @param {{country_id: int, region: string, city: string, address: string, address2: string|undefined, lat: string|undefined, lon: string|undefined, zip_code: string}} address
             */
            this.setAddress = function (address) {
                function setMarker(coords) {
                    if (useMarker) {
                        marker.setPosition(coords);
                        marker.setVisible(true);

                        var title = _.filter([address.address, address.city], function (val) {
                            return !_.isEmpty(val);
                        }).join(', ');

                        if (!_.isEmpty(title)) {
                            infowindow.setContent('<strong>' + title + '</strong>');
                            infowindow.open(map, marker);

                        }
                        else {
                            infowindow.close();
                        }

                        map.setCenter(coords);
                    }
                }

                if (!address.lat && !address.lon) {
                    // make query for get coordinates
                    var geocoder = new google.maps.Geocoder();
                    var addressString = address.country + ', ' + address.city;
                    geocoder.geocode({'address': addressString}, function (results, status) {
                        //console.log(results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
                        setMarker(results[0].geometry.location);
                    });
                } else {
                    var coords = new google.maps.LatLng(address.lat, address.lon);
                    setMarker(coords);
                }
                return this;
            };

            /**
             * Will use marker
             * @todo move it to scope
             * @type {boolean}
             */
            var useMarker = true;

            /**
             * Marker can drag
             * @todo do this as evaluted filed based on onMarkerPositionChange
             * @type {boolean}
             */
            var markerDraggable = true;

            /**
             * Default map options
             * @type {*}
             */
            var mapOptions = {
                center: new google.maps.LatLng(0, 0),
                zoom: 12,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.BOTTOM_CENTER
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                scaleControl: true,
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                }
            };

            /**
             * Google map
             * @type {google.maps.Map}
             */
            var map = new google.maps.Map($element[0], mapOptions);

            if (useMarker) {
                var infowindow = new google.maps.InfoWindow();
                var marker = new google.maps.Marker({
                    map: map,
                    draggable: markerDraggable,
                    anchorPoint: new google.maps.Point(0, 0),
                    visible: false
                });
            }

            if (useMarker && markerDraggable) {
                google.maps.event.addListener(marker, 'dragend', function () {
                    map.setCenter(marker.getPosition());
                    $maps.geodecode(marker.getPosition().lat(), marker.getPosition().lng(), 1).$promise.then(function (addresses) {
                        var address = addresses[0];
                        $scope.onMarkerPositionChange({address: address});
                        ctrl.setAddress(address);
                    });
                });
            }

            if ($scope.address) {
                this.setAddress($scope.address);
            }
        },

        link: function (scope, element, attrs, ctrl) {
            if (attrs.id) {
                ($parse(attrs.id).assign || angular.noop)($rootScope, ctrl);
            }
        }
    }
});