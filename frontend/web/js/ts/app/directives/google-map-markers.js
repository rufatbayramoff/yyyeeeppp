"use strict";

/**
 * Directive for google maps
 */
app.directive('googleMapMarkers', function ($parse, $rootScope, $maps, locations) {
    return {

        controller: function ($element, $scope) {
            var ctrl = this;
            /**
             * Return google map instance
             * @returns {google.maps.Map}
             */
            this.getMap = function () {
                return map;
            };

            var mapOptions = {
                center: { lat: parseFloat(locations.locations[0].position.lat), lng: parseFloat(locations.locations[0].position.lng) },
                zoom: 12,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.BOTTOM_CENTER
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                scaleControl: true,
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                }
            };

            var map = new google.maps.Map($element[0], mapOptions);
            locations.locations.map(function (point) {
                new google.maps.Marker({
                    map: map,
                    position: { lat: parseFloat(point.position.lat), lng: parseFloat(point.position.lng) },
                    title:point.title
                });
            })

        },
        link: function (scope, element, attrs, ctrl) {
            if (attrs.id) {
                ($parse(attrs.id).assign || angular.noop)($rootScope, ctrl);
            }
        }
    }
});