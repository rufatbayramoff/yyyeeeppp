"use strict";

/**
 *
 */
app.directive('dropzoneImages', function($parse, $notify, $timeout)
{
    return {
        restrict : 'A',
        link : function(scope, element, attrs)
        {
            var clickElement = element.find('[dropzone-images-click]');
            var previewContainer = element.find('[dropzone-images-preview]');
            var canChangeAfterSelect = $parse(attrs.canChangeAfterSelect)();

            var getter =  $parse(attrs.dropzoneImages),
                setter = getter.assign || angular.noop;

            if(!getter(scope))
            {
                setter(scope, []);
            }

            var filesArray = getter(scope);
            new Dropzone(element[0], {
                url: '#',
                autoProcessQueue: false,
                autoDiscover: false,
                previewsContainer : false,
               // maxFiles : 1,
                clickable: clickElement.length > 0 ? clickElement[0] : element[0],
                acceptedFiles: "image/jpeg,image/png,image/gif,.mp4,.webm,.ogg,.3gpp",

                accept: function(file, done)
                {
                    if(!canChangeAfterSelect){
                        clickElement.hide();
                    }
                    done();
                    if(previewContainer.length > 0 && window.FileReader)
                    {
                        var reader = new FileReader();
                        reader.onload = function (e)
                        {
                            var container = $(`
                                <div class="ps-upload-photo__pic">
                                    <img>
                                    <button type="button" class="ps-upload-photo__btn-del" title="Delete">&times;</button>
                                </div>
                            `);

                            container
                                .find('img')
                                .attr('src', e.target.result);



                            container
                                .find('button')
                                .click(function () {
                                    $timeout(function () {
                                        var index = filesArray.indexOf(file);
                                        filesArray.splice(index, 1);
                                    });
                                    container.remove();

                                });

                            container.appendTo(previewContainer);



                        };
                        reader.readAsDataURL(file);
                    }
                    filesArray.push(file);
                    scope.$apply();
                },

                error: function(file, message)
                {
                    $notify.error(message);
                }
            });
        }
    }
});