"use strict";

/**
 * Directive for google autocomplete input
 */
app.directive('googleAddressInput', function($parse, $rootScope, $maps, $geo)
{
    return {
        require : 'googleAddressInput',
        scope : {
            onAddressChange : '&'
        },
        controller : function ($element, $scope)
        {
            /**
             * Google autocomplete
             * @type {google.maps.places.Autocomplete}
             */
            var autocomplete = new google.maps.places.Autocomplete($element[0]);

            google.maps.event.addListener(autocomplete, 'place_changed', function()
            {
                var address = $maps.createAddressObject(autocomplete.getPlace());
                $scope.onAddressChange({address: address});
                $scope.$apply();
            });

            /**
             * Bind autocomplete to map bounds
             */
            this.bindToMapBounds = function(map)
            {
                autocomplete.bindTo('bounds', map);
                return this;
            };

            /**
             * Set input value (address as string)
             * @param addressString
             */
            this.setValue = function(addressString)
            {
                $element[0].value = addressString;
                return this;
            };

            /**
             * Set addres as value
             * @param addressObjct
             */
            this.setAddress = function(addressObjct)
            {
                this.setValue($geo.stringifyAddress(addressObjct));
                return this;
            }
        },
        link : function(scope, element, attrs, ctrl)
        {
            if(attrs.id)
            {
                ($parse(attrs.id).assign || angular.noop)($rootScope, ctrl);
            }
        }
    }
});