"use strict";

/**
 * Derective which help handle enter press
 */
app.directive('enterClick', function ()
{
    /**
     *
     */
    return function (scope, element, attrs)
    {
        element.bind("keydown keypress", function (event)
        {
            if(event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.enterClick);
                });
                event.preventDefault();
            }
        });
    };
})

.directive('ctrlEnterClick', function ()
{
    /**
     *
     */
    return function (scope, element, attrs)
    {
        element.bind("keydown keypress", function (event)
        {

            if((event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey) {
                scope.$apply(function () {
                    scope.$eval(attrs.ctrlEnterClick);
                });
                event.preventDefault();
            }
        });
    };
});