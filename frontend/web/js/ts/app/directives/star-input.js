"use strict";

/**
 *
 */
app.directive('starInput', function ()
{
    return {
        restrict : 'A',
        scope : {
            starInput : '='
        },

        link : function(scope, element)
        {
            $(element)
                .attr("value", scope.starInput)
                .rating({
                    showCaption : false,
                    showClear: false,
                    min : 0,
                    max : 5
                })
                .on('rating.change', function (event, value) {
                    scope.starInput = +value;
                    scope.$apply();
                })
        }
    }
});