"use strict";

/**
 * Directive autocomplete
 */
app.directive('autocomplete', function () {
    return {
        restrict: 'E',
        scope: {
            inputClass: '@',
            autocompleteElements: '=',
            autocompleteModel: '=',
            autocompleteTitle: '='
        },
        template: '<input ng-class="inputClass"  type="text" ng-model="autocompleteTitle">',
        link: function (scope, element) {
            var $elem = $(element).find('input');
            $elem.autocomplete({
                minLength: 1,
                source: scope.autocompleteElements,

                select: function (event, ui) {
                    $elem.val(ui.item.label); // display the selected text
                    scope.autocompleteTitle = ui.item.label;
                    scope.autocompleteModel = ui.item.id; // save selected id to hidden input
                    scope.$apply();
                    return false;
                },
                change: function (event, ui) {
                    scope.autocompleteModel = (ui.item ? ui.item.id : null);
                    if (scope.autocompleteModel) {
                        scope.autocompleteTitle = ui.item.label;
                    }
                    scope.$apply();
                }
            });
        }
    }
});
