"use strict";

/**
 * Directive for google maps
 */
app.directive('dropzoneButton', function($parse, $notify)
{
    return {
        restrict : 'A',
        scope : {
            files : '=dropzoneButton',
            maxFiles : '@',
            acceptedFiles : '@',
            onAccept : '&'
        },
        link : function(scope, element, attrs)
        {
            var hasFileArray = !!attrs.dropzoneButton;
            if (hasFileArray) {
                scope.files = scope.files || [];
            }

            scope.acceptedFiles = scope.acceptedFiles || "image/jpeg,image/png,image/gif,.stl,.ply";
            scope.maxFiles = scope.maxFiles ? parseInt(scope.maxFiles) : 1;
            scope.onAccept = scope.onAccept || angular.noop;


            new Dropzone(element[0], {
                url: '#',
                autoProcessQueue: false,
                autoDiscover: false,
                previewsContainer : false,
                maxFiles : parseInt(scope.maxFiles) || 1,
                clickable: [element[0]].concat(element.find('*').get()),
                acceptedFiles: scope.acceptedFiles,

                accept: function(file, done)
                {
                    if (hasFileArray && scope.files.length >= scope.maxFiles) {
                        done(_t('site', 'You can add no more than {cnt} files', {cnt : scope.maxFiles} ));
                        return;
                    }

                    done();
                    scope.onAccept({file : file});

                    if (hasFileArray) {
                        scope.files.push(file);
                    }

                    scope.$apply();
                },

                error: function(file, message)
                {
                    $notify.error(message);
                },

                addedfile: function ()
                {
                    this.removeAllFiles();
                }
            });
        }
    }
});