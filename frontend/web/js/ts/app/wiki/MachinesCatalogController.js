"use strict";

/**
 * Wiki machines catalog controller
 */
app.controller('MachinesCatalogController', function ($scope, $notify, $router, controllerParams) {

    $scope.category = controllerParams['category'];
    $scope.technology = controllerParams['technology'];
    $scope.brand = controllerParams['brand'];
    $scope.material = controllerParams['material'];
    $scope.search = controllerParams['search'];

    $scope.updateUrl = function () {
        $router.setUrl($router.wikiMachinesCatalog($scope.category, $scope.technology, $scope.brand, $scope.material, $scope.search));
    };

    /**
     *
     */
    $scope.onChangedFilter = function () {
        $scope.updateUrl();
    }
});