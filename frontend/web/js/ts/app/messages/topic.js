"use strict";

/**
 * Controller for topic view
 */
app.controller('TopicController', function ($scope, $sce, $timeout, $notifyHttpErrors, $http, controllerParams, $modal, $notify)
{
    /**
     * Refresh message period
     * @type {number}
     */
    var REFRESH_TIME = 5000;

    $scope.showOrderInfo = true;

    /** @namespace controllerParams.topic */
    var topic = controllerParams.topic;

    /**
     * Current message
     * @type {string}
     */
    $scope.currentMessage = '';
    $scope.files = [];

    /**
     * Messages
     * @type {Array}
     */
    $scope.messages = [];

    /**
     * Return id of last message
     */
    var getLastMessage = function () {
        return _.isEmpty($scope.messages)
            ? undefined
            : $scope.messages[$scope.messages.length - 1].id;
    };

    /**
     * Is send button enabled
     * @type {boolean}
     */
    $scope.sendButtonEnabled = true;

    /**
     *
     * @param newMessages
     */
    var updateMessages = function (newMessages)
    {

        if(!_.isEmpty(newMessages)){

            var messagesIds = [];

            angular.forEach($scope.messages, function (message) {
                messagesIds.push(message.id);
            });

            newMessages = _.filter(newMessages, function (message) {
               return messagesIds.indexOf(message.id) === -1;
            });

            angular.forEach(newMessages, function (message) {
                message.user.avatar =  $sce.trustAsHtml(message.user.avatar);
                message.text =  $sce.trustAsHtml(message.text);
            });

            $scope.messages = $scope.messages.concat(newMessages);

            $timeout(function () {
                var chatArea = $(".direct-chat-messages")[0];
                chatArea.scrollTop = chatArea.scrollHeight;
            });
        }
    };

    /**
     * Send message
     */
    $scope.sendMessage = function ()
    {
        if(!$scope.currentMessage && _.isEmpty($scope.files)){
            $notify.warning(_t('site.messages', 'Message is empty'));
            return;
        }
        if(!$scope.sendButtonEnabled){
            return;
        }
        $scope.sendButtonEnabled = false;

        return $http.post('/workbench/messages/add-message', {message : $scope.currentMessage, files : $scope.files}, {params : {topicId : topic.id, lastMessageId : getLastMessage()}})
            .then(function (response) {
                $scope.currentMessage = '';
                $scope.files = [];
                updateMessages(response.data);
                $scope.sendButtonEnabled = true;
            })
            .catch(function(response){
                $notifyHttpErrors(response);
                $scope.sendButtonEnabled = true;
            });
    };

    /**
     * Delete file
     * @param message
     * @param file
     */
    $scope.deleteFile = function (message, file)
    {
        return $http.post('/workbench/messages/delete-file', {}, {params : {fileId : file.id}})
            .then(function () {
                _.remove(message.files, file);
                $notify.success(_t("site.messages", "File was successfully deleted"));
            })
            .catch($notifyHttpErrors);
    };

    /**
     * Delete attached file
     * @param attachedFile
     */
    $scope.deleteAttachedFile = function (attachedFile)
    {
        _.remove($scope.files, attachedFile);
    };

    /**
     * Load new messages from server
     */
    var refreshMessages = function ()
    {
        $http.get('/workbench/messages/topic-messages', {params : {topicId : topic.id, lastMessageId : getLastMessage()}})
            .then(function (response) {
                if (response.data && response.status == 200) {
                    if (response.data.length != 0 && !response.data[0].id) {
                        window.top.document.location = '/user/login';
                        return;
                    }
                }
                updateMessages(response.data);
                $timeout(refreshMessages, REFRESH_TIME);
            })
            .catch(function(response){
                $notifyHttpErrors(response);
                $timeout(refreshMessages, REFRESH_TIME);
            });
    };

    /**
     * Init
     */
    updateMessages(controllerParams.messages);
    $timeout(refreshMessages, REFRESH_TIME);

    $scope.instantPayment = function () {

    };

    /**
     * Report message
     * @param message
     */
    $scope.reportMessage = function (message)
    {
        $modal.open({
            template : '/app/messages/report-message.html',
            controller : function($scope, $http, $router, $notify)
            {
                /**
                 * Report reasons
                 * @type {*}
                 */
                $scope.reportMessageReasons = controllerParams.reportMessageReasons;

                /**
                 * Report form
                 * @type {*}
                 */
                $scope.form = {
                    reasonId : undefined,
                    reasonDescription  : undefined
                };

                /**
                 *
                 */
                $scope.validateForm = function()
                {
                    var errors = [];
                    if(!$scope.form.reasonId){
                        errors.push(_t('site.ps', 'Please select report reason'));
                    }
                    return errors;
                };

                /**
                 *
                 * @returns {*}
                 */
                $scope.report = function ()
                {
                    var errors = $scope.validateForm();

                    if(!_.isEmpty(errors)){
                        $notify.error(errors);
                        return false;
                    }

                    return $http.post('/workbench/messages/report-message', $scope.form, {params : {topicId : topic.id}})
                        .then(function(){
                            $scope.$dismiss();
                            $notify.success(_t('frontend.message', "Report submitted"));
                        })
                        .catch($notifyHttpErrors);
                };
            }
        });
    }
});


function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (25+o.scrollHeight)+"px";
}