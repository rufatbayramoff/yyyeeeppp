"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('MyOrdersListController', function ($scope, $notify, $notifyHttpErrors, $modal, $http, $router, $q) {
    $scope.popupImageView = function ($event) {
        let src = $event.currentTarget.src;
        if (src) {
            $modal.open({
                template: '/static/templates/viewImage.html',
                scope: {
                    src: src,
                },
            });
        }
    };
});