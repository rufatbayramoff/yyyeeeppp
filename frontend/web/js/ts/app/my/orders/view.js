"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('PurchaseOrderViewController', function ($scope, $notify, $notifyHttpErrors, $modal, $http, $router, $preorderService, $q, $shareService) {
    $scope.setAsReceivedAndShowReviewModal = function (orderId, allowedAttachFiles, canCreateOrderReview) {
        canCreateOrderReview = (typeof canCreateOrderReview === "undefined" ? true : canCreateOrderReview);

        return $modal.confirm(_t('site.order',
            'Are you sure you want to set your order as received? This will confirm that you’re satisfied with your order and payment will be sent to the supplier.')
            , _t('site', 'Yes'), _t('site', 'No'))
            .then(function () {
                $http.post('/workbench/order/receive-ajax/' + orderId).then(function(){
                    if (!canCreateOrderReview) {
                        window.location.href = '/workbench/order/view/' + orderId;
                        return;
                    }

                    $scope.showReviewModal(orderId, allowedAttachFiles, _t('site.ps', 'Not now'), function ($result) {
                        window.location.href = '/workbench/order/view/' + orderId;
                    });
                },function (response) {
                    response ? $notifyHttpErrors(response) : '';
                    return $q.reject(response);
                });
            });
    };

    $scope.showReviewModalAndSetReceived = function (orderId, allowedAttachFiles) {
        $scope.showReviewModal(orderId, allowedAttachFiles, _t('site.ps', 'Not now'), function ($result) {
            window.location.href = '/workbench/order/view/' + orderId;
        });
    };

    /**
     *
     * @param preorderId
     */
    $scope.declinePreorder = function (preorderId) {
        return $modal.confirm(_t('site.preorder', 'Are you sure you want to cancel your request for a quote?'), _t('site', 'Yes'), _t('site', 'No'))
            .then(function () {
                return $preorderService.declinePreorder(preorderId, false)
            })
            .then($router.reload, function (response) {
                response ? $notifyHttpErrors(response) : '';
                return $q.reject(response)
            });
    };

    $scope.popupImageView = function ($event) {
        let src = $event.currentTarget.src;
        if (src) {
            $modal.open({
                template: '/static/templates/viewImage.html',
                scope: {
                    src: src,
                },
            });
        }
    };

    $scope.showReviewModal = function (orderId, allowedAttachFiles, cancelText, resultFunction) {


        if (typeof (cancelText) == 'undefined') {
            cancelText = _t('site.ps', 'Cancel');
        }
        if (typeof (resultFunction) == 'undefined') {
            resultFunction = function ($result) {
                if ($result) {
                    $router.reload();
                }
            }
        }

        $modal.open({
            template: '/app/my/orders/review-modal.html',
            scope: {
                form: {
                    rating_speed: undefined,
                    rating_quality: undefined,
                    rating_communication: undefined,
                    comment: undefined,
                    files: [],
                    printFiles: Object.keys(allowedAttachFiles),
                    cancelText: cancelText
                },
                allowedAttachFiles: allowedAttachFiles
            },
            controller: function ($scope) {
                /**
                 *
                 */
                $scope.validateForm = function () {
                    var errors = [];

                    if (!$scope.form.rating_speed) {
                        errors.push(_t('site.ps', 'Please enter speed rating'));
                    }

                    if (!$scope.form.rating_quality) {
                        errors.push(_t('site.ps', 'Please enter quality rating'));
                    }

                    if (!$scope.form.rating_communication) {
                        errors.push(_t('site.ps', 'Please enter communication rating'));
                    }

                    if ($scope.form.files.length > 10) {
                        errors.push(_t('site.ps', 'You can upload max 10 images'));
                    }

                    if ($scope.form.comment && $scope.form.comment.length > 1000) {
                        errors.push(_t('site.ps', 'Comment can contain no more than 1000 characters'));
                    }

                    return errors;
                };

                $scope.save = function () {
                    var errors = $scope.validateForm();

                    if (!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }

                    return $http.post('/workbench/order/add-review', $scope.form, {params: {orderId: orderId}})
                        .then(function (response) {
                            $scope.$dismiss();
                            return response.data.rating >= 4
                                ? $shareService.shareReview(response.data.id)
                                : $q.resolve(true);
                        })
                        .then(resultFunction)
                        .catch($notifyHttpErrors);
                };
                $scope.cancel = function () {
                    resultFunction(false);
                }
            }
        });
    }
});