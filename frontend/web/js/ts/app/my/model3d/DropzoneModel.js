"use strict";

/**
 * DropzoneModel class.
 *
 * @constructor
 * @param {String} id - The id.
 * @param {Object} options - The options
 */
function DropzoneModel(id,options) {
    this.id = id;
    this.dropZone = new Dropzone('#'+this.id, options);
    this.progressbar = $("#dz-total-progress");
    this.dropZone.on("totaluploadprogress",this.totalUpdateProgress.bind(this));
    this.dropZone.on("sending",this.sending.bind(this));
    this.dropZone.on("queuecomplete",this.queueComplete.bind(this));
    this.dropZone.on("error",function (file, response) {
        if (!response.success && response.message) {
            new TS.Notify({
                type: 'error',
                text: response.message,
                automaticClose: true
            });
        }
    });
}

DropzoneModel.prototype.totalUpdateProgress = function (progress) {
    this.progressbar.find('.progress-bar').css('width', progress + "%");
}

DropzoneModel.prototype.sending = function () {
    this.progressbar.css('opacity', "1");
}

DropzoneModel.prototype.queueComplete = function () {
    this.progressbar.css('opacity', "0");
}

/**
 * @param {Function} successCallback - The successCallback.
 */
DropzoneModel.prototype.success = function(successCallback) {
    this.dropZone.on("success",successCallback);
}

DropzoneModel.prototype.destroy = function () {
    this.dropZone.destroy();
}