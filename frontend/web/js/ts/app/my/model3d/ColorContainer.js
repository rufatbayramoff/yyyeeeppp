"use strict";

/**
 * ColorContainer class.
 *
 * @constructor
 * @param {Array} colors - The id.
 */
function ColorContainer(colors) {
    this.colors = colors;
}

/**
 * @param {Number|String} colorId
 * @return string
 * */
ColorContainer.prototype.hexByColor = function (colorId) {
    var color = this.findColor(colorId);
    return this.rgbToHex.apply(null, color.split(','));
}

/**
 * @param {Number|String} color
 * @return string
 * */
ColorContainer.prototype.findColor = function (color) {
    return this.colors[color];
}


/**
 * @param {Number|String} r
 * @param {Number|String} g
 * @param {Number|String} b
 * @return string
 * */
ColorContainer.prototype.rgbToHex = function (r, g, b) {
    var bin = r << 16 | g << 8 | b;
    return (function (h) {
        return new Array(7 - h.length).join("0") + h
    })(bin.toString(16).toUpperCase());
}