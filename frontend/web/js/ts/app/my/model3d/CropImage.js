"use strict";

function CropImage(activeFrame) {
    this.frame = activeFrame;
    this.container = this.frame.active();
    this.imageClass = '.js-crop-image';
    this.crop = this.crop.bind(this);
    this.success = this.success.bind(this);
}

CropImage.prototype.run = function () {
    if (!this.frame.isImage()) {
        return;
    }
    this.container.find(this.imageClass).off('click touchstart').on('click touchstart', this.crop);
}

CropImage.prototype.createTemplate = function (target) {
    $(TS.generateModal(target, _t('site.model', 'Crop image'))).insertAfter('footer');
}

CropImage.prototype.showModal = function (target, imageData) {
    $('#' + target).modal('show').find('.modal-body').load('/my/model/crop-image/' + imageData.id);
    $('#' + target + ' .modal-dialog').css("min-width", "60%");
    $('#' + target).on('modalAjaxSuccess',this.success);
}

CropImage.prototype.success = function (event, result) {
    if ($('#js-image-crop').length > 0) {
        $('#js-image-crop').cropper('destroy');
    }
    if(result.url) {
        var newImg = result.url + (result.url.lastIndexOf("?") > 0 ? '&' : '?') + 'p' + Math.random() + '=1';
        this.frame.updateImage(newImg);
    }
}

CropImage.prototype.crop = function () {
    var image = this.container.find('[data-img]').data();
    var index = this.frame.index();
    var target = 'jscropimage' + index;
    if (!image.id) {
        return;
    }
    if ($(target).length === 0) {
        this.createTemplate(target);
    }
    this.showModal(target, image);
}

