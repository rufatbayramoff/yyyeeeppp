"use strict";

/**
 * @property {int} id
 * @property {string} title
 * @property {int} categoryId
 * @property {string} categoryLabel
 * @param data
 * @constructor
 */
function Model3dForm(data) {
    this.load(data);
};

Model3dForm.prototype.load = function (data) {
    this.id = data['id'];
    this.categoryId = data['categoryId'];
    this.categoryLabel = data['categoryLabel'];

};


Model3dForm.prototype.getCategoryLabel = function (data) {
    if (this.categoryLabel) {
        return this.categoryLabel;
    }
    return _t('site.model3d', 'Not select category');
};

Model3dForm.prototype.setOneMaterialKit = function (data) {
    this.modelMaterial = {
        materialGroupId: data['materialGroupId'],
        colorId: data['colorId']
    };
    delete this['filesMaterial'];
};

Model3dForm.prototype.setFileColor = function (fileId, data) {
    if (typeof this.filesMaterial === "undefined") {
        this.filesMaterial = {};
    }
    if (typeof this.filesMaterial[fileId] === "undefined") {
        this.filesMaterial[fileId] = {};
    }
    this.filesMaterial[fileId]['materialGroupId'] = data['materialGroupId'];
    this.filesMaterial[fileId]['colorId'] = data['colorId'];

    delete this['modelMaterial'];
};
