"use strict";

/**
 * Model3d edit controller
 */
app.controller('Model3dEditController', function ($scope, $notify, $notifyHttpErrors, $modal, $http, $router, $q, controllerParams) {
    $scope.model3d = new Model3dForm(controllerParams.model3d);
    $scope.currentFileId = null;
    $scope.currentFileType = null;
    $scope.categorySelect = {};
    $scope.fotoramaWidget = null;


    $scope.init = function () {
        setTimeout(function () {
            $scope.fotoramaWidget = $('#fotoramaWidgetDiv');

            $scope.fotoramaWidget.on('fileChange', function (event, data) {
                $scope.onChangeModelFile(data.activeFrame, data.fotorama.data);
            });

            $('#model3dEditForm').on('changeModelColor', function (event, data) {
                $scope.onChangeModelColor(data);
            });

            var activeFrame = $scope.fotoramaWidget.data('fotorama').activeFrame;
            if (activeFrame) {
                $scope.currentFileId = activeFrame.fileid;
                $scope.currentFileType = activeFrame.type;
            }
        }, 5);
    };


    $scope.onChangeModelColor = function (data) {
        $scope.$broadcast("changeModelColor", data);
        if (data['isOneMaterialForKit']) {
            $scope.model3d.setOneMaterialKit(data);
        } else {
            $scope.model3d.setFileColor($scope.currentFileId, data);
        }
    };

    $scope.removeFile = function () {
        $scope.$broadcast("removeFile");
    }

    $scope.onChangeModelFile = function (selectedItem, allData) {
        $scope.currentFileId = selectedItem['fileid'];
        colorSelectorWidgetObj.onChangeModelFile(selectedItem, allData);

    };

    $scope.changeCategoryPressed = function () {
        $modal.open({
            template: '/product/select-product-category.html',
            scope: {
                categorySelect: {},
                changeCategoryOnSubmit: $scope.changeCategory,
                categories: controllerParams.categories,
                productCategoryId: $scope.model3d.productCategoryId,
            }
        });
    };

    $scope.changeCategory = function (categoryId, categoryLabel) {
        $scope.model3d.categoryId = categoryId;
        $scope.model3d.categoryLabel = categoryLabel;
    };

    $scope.publish = function () {
        $scope.model3d.submitMode = 'publish';
        $scope.save();
    };

    $scope.unpublish = function () {
        var defer = $q.defer();
        $modal.confirm(_t('site.preorder', 'Are you sure you want to remove this 3d model from marketplace?'), _t('site', 'Yes'), _t('site', 'No'))
            .then(function () {
                $scope.model3d.submitMode = 'unpublish';
                $scope.save();
            }, defer.reject);
    };

    $scope.save = function () {
        let url = $router.getSaveModel3d($scope.model3d.id);

        let params = {
            'Model3dEditForm': $('#model3dEditForm').serializeForm()['Model3dEditForm']
        };
        params["Model3dEditForm"]['submitForm'] = 1;
        for (var key in $scope.model3d) {
            if (!$scope.model3d.hasOwnProperty(key)) continue;
            params["Model3dEditForm"][key] = $scope.model3d[key];
        }

        $http.post(url, params).then(function (response) {
            if (!response.data || !response.data.success) {
                $notify.error('Error. Please, try again.');
            } else {
                $notify.success(response.data.message);
                setTimeout(function () {
                    $router.setUrl($router.getBusinessProducts('model3d'));
                }, 1000);
            }
        }).catch($notifyHttpErrors);
    };

    $scope.delete = function () {
        var defer = $q.defer();
        $modal.confirm(_t('site.preorder', 'Do you want to delete this model, its description, and all its images?'), _t('site', 'Yes'), _t('site', 'No'))
            .then(function () {
                $http.post($router.getDeleteModel3d($scope.model3d.id)).then(function (response) {
                    if (!response.data || !response.data.success) {
                        $notify.error('Error. Please, try again.');
                    } else {
                        $notify.success(response.data.message);
                        setTimeout(function () {
                            $router.setUrl($router.getBusinessProducts());
                        }, 1000);
                    }
                }).catch($notifyHttpErrors);
            }, defer.reject);
    };

    $scope.$on('deleteModel',function(){
        let url = $router.deleteModel($scope.model3d.id);
        $http.post(url).then(function (response) {
            if(response.data.success === true) {
                window.location.href = '/mybusiness/products';
            } else {
                $notify.error(response.message || _t('site.model', 'Error deleting'));
            }
        }).catch($notifyHttpErrors);
    });

    $scope.$on('deleteImage',function (e, data){
        var url = $router.deleteImage(data.imageId);
        $http.post(url).then(function (result) {
            if (result.data.success === true) {
                data.callback();
                $notify.success(result.message || _t('site.model', 'Item deleted'));
            }
            else {
                $notify.error(result.message || _t('site.model', 'Error deleting'));
            }
        }).catch($notifyHttpErrors);
    });

    $scope.$on('fotoramaChange', function(e , data) {

    });

    /**
    * @param {{modelPartId: number,callback: function}} data
    * */
    $scope.$on('deleteFile',function (e, data){
        let url = $router.deletePart(data.modelPartId);
        $http.post(url).then(function (result) {
            if (result.data.success === true) {
                data.callback();
                $notify.success(result.message || _t('site.model', 'Item deleted'));
            }
            else {
                $notify.error(result.message || _t('site.model', 'Error deleting'));
            }
        }).catch($notifyHttpErrors);
    });

    $scope.$on('setCover', function (e, data){
        let url = $router.setCover($scope.model3d.id);
        $http.post(url,{'uid': 'MI:' + data.imageId}).then(function (result) {
            if (result.data.success === true) {
                data.callback(data.imageId);
                $notify.success(result.message || _t('site.model', 'Done'));
            }
            else {
                $notify.error(result.message || _t('site.model', 'Error. Please try again.'));
            }
        }).catch($notifyHttpErrors);
    })

    $scope.init();

});
