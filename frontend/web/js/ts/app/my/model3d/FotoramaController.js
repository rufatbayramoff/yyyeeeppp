"use strict";

app.controller('FotoramaController', function (
    $scope, $notify, $modal, $q, $modelService, $rootScope, $templateRequest, $compile,$timeout,controllerParams
) {
    $scope.colorContainer = new ColorContainer(controllerParams.colors);
    $scope.fotorama = null;
    $scope.dropZone = null;
    $scope.activeFrame = null;
    $scope.modelType = 'model';
    $scope.model3d = new Model3dForm(controllerParams.model3d);
    $scope.uploadFiles = [];
    $scope.coverImageId = null;

    $scope.init = function (container) {
        $scope.fotoramaContainer = $('#' + container || 'fotorama');
        $scope.fotoramaContainer.on('fotorama:load', $scope.initFotorama);
        $scope.fotoramaContainer.on('fotorama:show fotorama:showend', $scope.initFotorama);
    }

    /**
     * @param {{activeFrame: Object}} fotorama
     * */
    $scope.initFotorama = function (e, fotorama) {
        $scope.dropZoneInit();
        $scope.initFrame(fotorama);
        $scope.initRemove(fotorama);
        $scope.initModelQty(fotorama);
        $scope.initCover(fotorama);
    }

    $scope.initCover = function (fotorama) {
        var container = $scope.activeFrame.active();
        var isCover = fotorama.activeFrame.iscover;
        var crop = container.find('.js-set-cover');
        if (isCover) {
            crop.hide();
            return;
        }

        crop.off('click touchstart').on('click touchstart', function () {
            var coverImage = $scope.activeFrame.frame.iscover;
            var image = container.find('[data-img]').data();
            if (coverImage || image.id === $scope.coverImageId) {
                $notify.success(_t('site.model', 'This already is cover'));
                return false;
            }

            $scope.$emit('setCover', {
                imageId: image.id,
                callback: $scope.coverSuccess
            });
        });
    }

    $scope.coverSuccess = function (imageId) {
        var fotorama = $scope.fotoramaApi();
        var data = fotorama.data;
        var container = $scope.activeFrame.active();
        for (var i = 0; i < data.length; i++) {
            data[i].iscover = false;
        }
        $scope.activeFrame.frame.iscover = true;
        $scope.coverImageId = imageId;
        $('.js-set-cover').show();
        container.find('.js-set-cover').hide();
    }

    $scope.initFrame = function (fotorama) {
        $scope.activeFrame = new ActiveFrameModel(fotorama.activeFrame);
        $scope.activeFrame.rotate();
        $scope.activeFrame.updateColor(fotorama.activeFrame.colorrgb);
        $scope.crop = new CropImage($scope.activeFrame);
        $scope.crop.run();
        $scope.fotoramaContainer.trigger('fileChange', {activeFrame: fotorama.activeFrame, fotorama: fotorama});
    };

    $scope.dropZoneInit = function () {
        if($scope.dropZone !== null) {
            $scope.dropZone.destroy();
        }
        $scope.dropZone = new DropzoneModel($scope.fotoramaContainer.attr('id'),{
            autoQueue: true,
            paramName: "Model3dEditForm[file]",
            uploadMultiple: true,
            parallelUploads: 10,
            maxFiles: 10,
            clickable: '.js-add-image',
            url: "/my/model/add-object/" + $scope.model3d.id
        });
        $scope.dropZone.success($scope.successUpload);
    }

    $scope.initModelQty = function (fotorama) {
        var findInput = function (e) {
            var fileId = $(e.currentTarget).data('fileid');
            return $(".js-fileqty-" + fileId + " input");
        }
        $('.js-model3dqty').hide();
        if ($('.js-model3dqty div.form-group').length > 0) {
            var containerQty = $('.js-fileqty-' + $scope.field(fotorama));
            var plus = containerQty.find('.tsi-plus-c');
            var minus = containerQty.find('.tsi-minus-c');
            containerQty.show();
            plus.off('click').on('click', function (e) {
                e.preventDefault();
                var el1 = findInput(e);
                el1.val(Math.min(parseInt(el1.val()) + 1, 100));
            });
            minus.off('click').on('click', function (e) {
                e.preventDefault();
                var el1 = findInput(e);
                el1.val(Math.max(parseInt(el1.val()) - 1, 1));
            });
        }
    }
    $scope.initRemove = function (fotorama) {
        $('.js-remove-file').off().on('click touchstart', function () {
            $scope.removeFile(fotorama);
        });
        $('.js-remove-image').off().on('click touchstart', function (){
            $scope.removeImage(fotorama);
        });
    }

    $scope.removeImage = function (fotorama) {
        var container = $scope.activeFrame.active();
        var isCover = fotorama.activeFrame.iscover;
        var image = container.find('[data-img]').data();
        if (isCover && fotorama.data.length > 1) {
            $notify.info(_t('site.model', "You can't delete cover image"));
            return false;
        }
        if (!confirm(_t('site.model', 'Are you sure you want to delete this image?'))) {
            return false;
        }
        $scope.$emit('deleteImage', {
            imageId: image.id,
            callback: $scope.fotoramaRemoveFile(fotorama)
        });
        return;
    }

    $scope.fotoramaRemoveFile = function (fotorama) {
        return function () {
            fotorama.splice(fotorama.activeIndex, 1);
            if(!fotorama.data) {
                window.location.href = '/my/store/category/id/-1';
                return;
            }
        };
    };

    /**
     * @param {{type: string,isparsed: number, img: string}} item
     * @return boolean
     * */
    $scope.isNotRender = function (item) {
        var isModel = function () {
            return item.type === $scope.modelType;
        };

        var isNotParse = function () {
            return item.isparsed === false;
        };

        var isDefaultIcon = function () {
            return item.img === "/static/images/3dicon.png";
        }
        return isModel() && isNotParse() && isDefaultIcon();
    }

    /**
     * @param {{
     * model3dTextureState: {kitTexture: {printerColor: {rgbHex: string}}},
     * isComplited: boolean,
     * model3d: {id:number, coverImgUrl: string,parts:Array.<{renderUrlBase:string,originalModel3dPartId:number,hash:string,caption: string,fileExt:string,fileId:number,previewUrl:string,render:{isSuccess: boolean}}>}}} response
     * */
    $scope.tickSubscribe = function (response) {
        if (response.isComplited === false) {
            return;
        }
        if (response.model3d.parts.length === 0) {
            return;
        }
        /**
         * @type Array
         * */
        var notRender = response.model3d.parts.filter(function (item) {
            return item.render.isSuccess === false;
        });
        if (notRender.length > 0) {
            return;
        }

        $scope.imageLoad($scope.serializeResponseModel(response));
    }

    /**
     * @param {{
     * model3dTextureState: {kitTexture: {printerColor: {rgbHex: string}}},
     * isComplited: boolean,
     * model3d: {id:number, coverImgUrl: string,parts:Array.<{renderUrlBase:string,originalModel3dPartId:number,hash:string,caption: string,fileExt:string,fileId:number,previewUrl:string,render:{isSuccess: boolean}}>}}} response
     * */
    $scope.serializeResponseModel = function (response) {
        var fotoramaData = $scope.fotoramaApi().data;
        return fotoramaData.map(function (item) {
            if(item.type !== $scope.modelType) {
                return {
                    img: item.img,
                    fileid: item.fileid,
                    thumb: item.img,
                    type: item.type,
                    html: item.html,
                    id: item.id,
                    iscover: item.iscover
                };
            }
            var model = response.model3d.parts.find(function (element) {
                return element.fileId == item.fileid;
            });
            var color = model.colorrgb || response.model3dTextureState.kitTexture.printerColor.rgbHex;
            return {
                caption: model.caption,
                img: model.previewUrl,
                fileid: model.fileId,
                thumb: model.previewUrl,
                type: $scope.modelType,
                fileext: model.fileExt,
                md5sum: model.hash,
                modelpartid: model.originalModel3dPartId,
                model3did: response.model3d.id,
                isparsed: true,
                renderurlbase: model.renderUrlBase,
                colorrgb: color,
                html: item.html
            };

        });
    }

    $scope.initSubscribe = function () {
        var fotorama = $scope.fotoramaApi();
        var data = fotorama.data;
        for (var i = 0; i < data.length; i++) {
            if ($scope.isNotRender(data[i])) {
                $modelService.checkJobs.waitComplited(data[i].model3did, data[i].modelpartid)
                    .tickSubscribe($scope.tickSubscribe);
            }
        }
    }

    $scope.$on('changeModelColor', function (event, data) {
        let color = $scope.colorContainer.hexByColor(data.colorId).toLowerCase();
        let isOneMaterialForKit = data.isOneMaterialForKit?1:0;
        if (isOneMaterialForKit) {
            $('.fotorama__thumb.fotorama__loaded.fotorama__loaded--img').find('img').each(function (id, img) {
                let pos = img.src.indexOf('_color_');
                if (pos > 1) {
                    let newUrl = img.src.substring(0, pos + 7) + color + img.src.substring(pos + 13, 1024);
                    img.src = newUrl;
                }

            });
            let fotoramaData = $scope.fotoramaApi().data;
            for (var key in fotoramaData) {
                if (!fotoramaData.hasOwnProperty(key)) {
                    continue;
                }
                let item = fotoramaData[key];
                if (item.type === $scope.modelType) {
                    item.printercolorid=data.colorId;
                    item.colorrgb = color;
                    item.printercolorrgb = color;
                }
            }
            fotoramaData.map(function (item) {

            });
        }
        $scope.activeFrame.updateColor(color);
    });

    $scope.removeFile = function (fotorama) {
        var modelCount = $scope.modelCount(fotorama.data);
        if (modelCount === 1 && fotorama.activeFrame.type != 'image') {
            $modal.confirm(
                _t('site.model', 'Do you want to delete this model, its description, and all its images?'),
                _t('site', 'Yes'),
                _t('site', 'No'))
                .then(function () {
                    $scope.$emit('deleteModel');
                });
        } else {
            if (fotorama.activeFrame.type == 'image') {
                return $scope.removeImage(fotorama);
            }
            $modal.confirm(
                _t('site.model', 'Are you sure you want to delete this file?'),
                _t('site', 'Yes'),
                _t('site', 'No'))
                .then(function () {
                    $scope.$emit('deleteFile', {
                        modelPartId: $scope.modelPartId(fotorama),
                        callback: $scope.fotoramaRemoveFile(fotorama)
                    });
                });
        }
        $scope.$apply();
        return false;
    };

    $scope.modelCount = function (data) {
        return data.reduce(function(acc,model){
            if(model.type === $scope.modelType) {
                acc = acc + 1;
            }
            return acc;
        },0);
    };

    $scope.successUpload = function (e,response) {
        if(!Array.isArray(response)) {
            $notify.error(_t('site.model', 'Error. Please try again.'));
            return;
        }

        _.each(response, function (responseJson) {
            if (!responseJson.success) {
                $notify.error(responseJson.message || _t('site.model', 'Error. Please try again.'));
                return;
            }
            if (responseJson.message === 'reload') {
                window.location.reload();
                return;
            }
            if ($scope.fileIsExist(responseJson)) {
                return;
            }
            $scope.appendToFileUpload(responseJson);
            $scope.fotoramaHtml(responseJson,'/product/model-preview.html',$scope.previewTemplate);
        });
    }

    $scope.previewTemplate = function (html, data) {
        $scope.fotoramaHtml(data,'/product/model-qty.html',function (qtyHtml) {
            $(".js-model3dqty").last().after(qtyHtml);
        });
        $scope.appendImage(data, html);
        $scope.initSubscribe();
        $notify.success(data.message);
    }

    /**
     * @param {{fileId}} file
     * */
    $scope.fileIsExist = function (file) {
        return $scope.uploadFiles.find(function (id) {
            return id === file.fileId;
        });
    }

    /**
     * @param {{fileId}} file
     * */
    $scope.appendToFileUpload = function (file) {
        return $scope.uploadFiles.push(file.fileId);
    }

    /**
     * @param {{success:boolean,message:string,fileUrl:string,fileId,fileType,fileExt,md5sum,modelpartid,model3dId:number}} params
     * */
    $scope.appendImage = function (params, html) {
        var fotorama = $scope.fotoramaApi();
        fotorama.push({
            img: params.fileUrl,
            fileid: params.fileId,
            thumb: params.fileUrl,
            type: params.fileType,
            fileext: params.fileExt,
            md5sum: params.md5sum,
            modelpartid: params.modelpartid,
            model3did: params.model3did,
            html: html,
            isparsed: false
        });
    }

    $scope.imageLoad = function (data) {
        var fotorama = $scope.fotoramaApi();
        fotorama.load(data);
    }

    $scope.fotoramaApi = function () {
        return $scope.fotoramaContainer.data('fotorama');
    }

    $scope.field = function (fotorama) {
        return fotorama.activeFrame.fileid;
    }

    $scope.modelPartId = function (fotorama) {
        return fotorama.activeFrame.modelpartid;
    }

    $scope.fotoramaHtml = function (params, name, callback) {
        $templateRequest(name).then(function (templateHtml) {
            var template = $compile(templateHtml);
            var content = template(angular.extend($scope.$new(), {model:params}));
            $timeout(function (){
                callback(content.prop('outerHTML'), params);
            });
        });
    }
})