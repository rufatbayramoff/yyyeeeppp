"use strict";

/**
 * ActiveFrameModel class.
 *
 * @constructor
 * @param {{activeIndex,$stageFrame:Object,fileid: string, html: string, md5sum: string, fileext: string,renderurlbase:string, colorrgb: string}} frame
 */


function ActiveFrameModel(frame) {
    this.frame = frame;
    this.imageClass = '.fotorama__active';
    this.modelRenderClass = 'model3dRenderLiveImg';
    this.render = commonJs.clone(model3dRenderClass);
    this.defaultCoord = 0;
    this.allowAutoRotate = false;
    this.thumbClass = '.fotorama__nav__frame.fotorama__nav__frame--thumb.fotorama__active';
    this.imageType = 'image';
}

ActiveFrameModel.prototype.rotate = function () {
    if (!this.isFileLoad() && this.isImage()) {
        return;
    }
    this.renderInit();
}

ActiveFrameModel.prototype.isImage = function () {
    return this.frame.type === this.imageType;
}

ActiveFrameModel.prototype.active = function () {
    return this.frame.$stageFrame;
}
ActiveFrameModel.prototype.renderInit = function () {
    this.render.init(
        {
            imgUrl: this.renderUrl(),
            imgElementId: this.findImage(),
            fileUid: this.fileUuid(),
            color: this.colorRgb(),
            ax: this.defaultCoord, ay: 30, az: this.defaultCoord,
            pb: this.defaultCoord,
            allowAutoRotate: this.allowAutoRotate
        }
    );
}

ActiveFrameModel.prototype.updateColor = function (color) {
    if (!this.isFileLoad()) {
        return;
    }
    this.frame.colorrgb = color;
    this.render.updateColor(color);
    this.updateThumbColor();
}


ActiveFrameModel.prototype.isFileLoad = function () {
    return this.frame.md5sum && this.frame.fileext
}

/**
 * @return Object
 * */
ActiveFrameModel.prototype.findImage = function () {
    var image = this.findActiveImage();
    image.attr('id', this.imageId());
    image.addClass(this.modelRenderClass);
    return image;
}

/**
 * @return string
 * */
ActiveFrameModel.prototype.imageId = function () {
    return 'fotoramaBackgroundImage' + this.frame.fileid;
}

/**
 * @return Object
 * */
ActiveFrameModel.prototype.findActiveImage = function () {
    return this.frame.$stageFrame.find('img');
}

ActiveFrameModel.prototype.updateThumbColor = function () {
    var self = this;
    var image = self.findActiveImage();
    image.on('renderImageLoad', function (event, imageUrl) {
        var thumb = self.findActiveThumb();
        thumb.attr('src', imageUrl)
    });
}

ActiveFrameModel.prototype.updateImage = function (imageUrl) {
    this.frame.img = imageUrl;
    this.frame.thumb = imageUrl;
    if (this.frame.$navThumbFrame) {
        this.frame.$navThumbFrame.find('img').attr('src', imageUrl);
    }
    if (this.frame.$stageFrame) {
        this.frame.$stageFrame.find('img').attr('src', imageUrl);
    }
}

ActiveFrameModel.prototype.findActiveThumb = function () {
    return $(this.thumbClass).find('img');
}

ActiveFrameModel.prototype.renderUrl = function () {
    return this.frame.renderurlbase;
}

ActiveFrameModel.prototype.fileUuid = function () {
    return this.frame.md5sum + '_' + this.frame.fileext.toLowerCase();
}

ActiveFrameModel.prototype.colorRgb = function () {
    return this.frame.colorrgb;
}

ActiveFrameModel.prototype.index = function () {
    return this.frame.activeIndex;
}

