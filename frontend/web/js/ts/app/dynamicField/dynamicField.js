"use strict";

/*
* @property {int} id
* @property {string} code
* @property {string} bindedModel
* @property {string} title
* @property {string} type
* @property {object} typeParams
* @property {string} defaultValue
* @property {string} description
* @property {bool} isActive
*/
function DynamicField(data) {
    angular.extend(this, data);
}

DynamicField.prototype.someStub = function () {
    return null;
};