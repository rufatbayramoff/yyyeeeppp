"use strict";

var l = function () {
    angular.forEach(arguments, function (l) {
        console.log(l);
    });
};

var angularModules = ['ngResource', 'ngLocale', 'ui.mask', 'ui.router'];
if (window.angularAdditionalModules) {
    angularModules = angularModules.concat(window.angularAdditionalModules);
}

var app = angular.module('app', angularModules);

app
    .config(['$locationProvider', '$provide', function ($locationProvider, $provide) {
        $locationProvider
            .html5Mode({
                enabled: true,
                requireBase: false,
                rewriteLinks: false
            });
        /* TODO: REMOVE IT FROM HEARE
                $provide.decorator('$browser', ['$delegate', function ($delegate)
                {
                    $delegate.onUrlChange = function () {};
                    $delegate.url = function () { return "/"};
                    return $delegate;
                }]);
        */
    }])


    /**
     *
     */
    .config(['$httpProvider', function ($httpProvider) {
        // configure yii csrf token
        $httpProvider.defaults.headers.common['X-CSRF-Token'] = function () {
            return yii.getCsrfToken();
        };

        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        // upload file use multipart/form-data
        $httpProvider.interceptors.push(function () {
            return {
                request: function (config) {
                    if (config.data) {
                        var haveFile = false;

                        Object.traverse(config.data, function (obj, node) {
                            if (obj instanceof File || obj instanceof FileList || node instanceof File || node instanceof FileList) {
                                haveFile = true;
                            }
                        }, undefined, undefined, true);

                        if (haveFile) {
                            config.data = Object.toFormData(config.data);
                            config.headers = config.headers || {};
                            config.headers['Content-Type'] = undefined;
                        }
                    }
                    return config;
                }
            };
        });
    }])

    /**
     * Handler for show http errors
     */
    .factory('$notifyHttpErrors', ['$notify', '$router', '$user', '$formValidator', '$q', function ($notify, $router, $user, $formValidator, $q) {
        return function (data) {
            if (data.status == 401 || data.status == 403) {
                $user.openLoginModal($router.getCurrentLocation());
            }
            else {
                if (data.data && data.data.validateErrors) {
                    $formValidator.applyValidate(data.data.validateErrors);
                } else {
                    var message = 'Error. Please, try again.';
                    if (data.data) {
                        message = data.data.errors || data.data.message || (_.isString(data.data) && data.data);
                    }
                    $notify.error(message);
                }
            }
            return $q.reject(data);
        }
    }])

    /**
     * Handler for show notifiers
     */
    .factory('$notifyHttp', ['$notify', '$q', function ($notify, $q) {
        return function (data) {
            $notify.success(data.data.message || (_.isString(data.data) && data.data) || 'Successfully saved.');
            return $q.reject(data);
        }
    }])

    /**
     * Translate service wrapper
     */
    .factory('$t', function () {
        return _t;
    })

    .run(['$flushMessageManager', function ($flushMessageManager) {
        /**
         * Remove underscore function
         * @param array
         * @param element
         */
        _.remove = function (array, element) {
            var index = array.indexOf(element);
            if (index !== -1) {
                array.splice(index, 1);
            }
        };

        $flushMessageManager.showMessages();
    }])

    /**
     * Directive for loader button.
     * It disabled button while not resolved returned from click promise.
     *
     */
    .directive('loaderClick', ['$parse', '$q', '$timeout', function ($parse, $q, $timeout) {
        var inLoadText = '<i class="tsi tsi-refresh tsi-spin-custom"></i> ' + _t('front.user', 'Please wait...');

        return {
            restrict: 'A',
            compile: function ($element, attr) {
                var fn = $parse(attr['loaderClick'], null, true);
                return function ngEventHandler(scope, element) {
                    element.on('click', function (event) {
                        var promise = scope.$apply(function () {
                            return fn(scope, {$event: event});
                        });

                        // if it not promise - do nothing
                        if (!promise || !angular.isFunction(promise.then)) {
                            return;
                        }

                        var content = element.html();
                        element.prop('disabled', true);
                        element.html(attr['loaderClickText'] || inLoadText);

                        var restoreFn = function () {

                            var disabled = false;

                            if (attr['ngDisabled']) {
                                disabled = !!$parse(attr['ngDisabled'])(scope);
                            }

                            $timeout(function () {
                                element.prop('disabled', disabled);
                                element.html(content);
                            });
                        };

                        promise.then(function () {
                            var needRestore = !$parse((attr['loaderClickUnrestored']))(scope);
                            if (needRestore) {
                                restoreFn();
                            }
                        })
                            .catch(restoreFn);
                    });
                };
            }
        };
    }])
    .directive('checkList', function () {
        return {
            scope: {
                list: '=checkList',
                value: '@'
            },
            link: function (scope, elem) {
                var handler = function (setup) {
                    var checked = elem.prop('checked');
                    var index = scope.list.indexOf(scope.value);

                    if (checked && index == -1) {
                        if (setup) elem.prop('checked', false);
                        else scope.list.push(scope.value);
                    } else if (!checked && index != -1) {
                        if (setup) elem.prop('checked', true);
                        else scope.list.splice(index, 1);
                    }
                };

                var setupHandler = handler.bind(null, true);
                var changeHandler = handler.bind(null, false);

                elem.bind('change', function () {
                    scope.$apply(changeHandler);
                });
                scope.$watch('list', setupHandler, true);
            }
        };
    })


    .directive('checkListObject', function () {
        return {
            scope: {
                list: '=checkListObject',
                value: '='
            },
            link: function (scope, elem) {
                var handler = function (setup) {
                    var checked = elem.prop('checked');
                    var index = scope.list.indexOf(scope.value);

                    if (checked && index == -1) {
                        if (setup) elem.prop('checked', false);
                        else scope.list.push(scope.value);
                    } else if (!checked && index != -1) {
                        if (setup) elem.prop('checked', true);
                        else scope.list.splice(index, 1);
                    }
                };

                var setupHandler = handler.bind(null, true);
                var changeHandler = handler.bind(null, false);

                elem.bind('change', function () {
                    scope.$apply(changeHandler);
                });
                scope.$watch('list', setupHandler, true);
            }
        };
    })

    .filter('html', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }])

    .filter('url', ['$sce', function ($sce) {
        return function (url) {
            return $sce.trustAsResourceUrl(url);
        };
    }])

    .filter('numkeys', function () {
        return function (object) {

            if (_.isArray(object)) {
                return object.length;
            }

            return object ? Object.keys(object).length : 0;
        }
    })
    .filter('int', function () {
        return function (num) {
            return toInteger(num)
        };
    })
    .filter('intFormatted', function() {
        return function (num) {
            return toInteger(num).toLocaleString('en-EN').replace(',', ' ');
        }
    })
    .filter('money', ['$filter', function ($filter) {
        return function (value) {
            return $filter('number')(value, 2);
        };
    }])

    .filter('debugger', function () {

        return function (data) {
            return typeof data;
        }
    })
    .filter('scalarArray', function () {
        return function (data) {
            var returnValue = new Array(Math.round(data));
            return returnValue;
        }
    })

    /**
     * Validator for email input
     * Require ng-model derective
     */
    .directive('emailValidator', function () {
        var EMAIL_REGEXP = /^[a-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/;

        return {
            require: '?ngModel',
            link: function (scope, elm, attrs, ctrl) {
                if (ctrl && ctrl.$validators.email) {
                    ctrl.$validators.email = function (modelValue) {
                        return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
                    };
                }
            }
        };
    })

    .filter('nl2br', ['$sce', function($sce){
        return function(msg,is_xhtml) {
            is_xhtml = is_xhtml || true;
            var breakTag = (is_xhtml) ? '<br />' : '<br>';
            msg = (msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
            return $sce.trustAsHtml(msg);
        }
    }]);

