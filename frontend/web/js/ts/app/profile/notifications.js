"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('ProfileNotificationController', function ($scope, $notify, $notifyHttpErrors, $http, controllerParams) {
    /**
     * Notification params
     * @type {*}
     */
    $scope.notification = controllerParams.notification;

    $scope.form = {};
    $scope.form.noCheckOnline = controllerParams.noCheckOnline;
    $scope.form.allowDialogAutoCreate = controllerParams.allowDialogAutoCreate;
    $scope.form.allowCompanyDialogAutoCreate = controllerParams.allowCompanyDialogAutoCreate;

    /**
     * User config of notifications
     */
    $scope.userConfig = controllerParams.userConfig;

    /**
     * User have senders
     * @type {boolean}
     */
    $scope.haveSenders = !_.isEmpty(controllerParams.notification.senders);

    /**
     * Chenage notification type for group and sender
     * @param group
     * @param sender
     */
    $scope.changeType = function (group, sender) {
        var times = _.keys($scope.notification.times);
        var currentIndex = times.indexOf($scope.userConfig[group][sender]);
        var nextIndex = (currentIndex + 1) % times.length;

        $scope.userConfig[group][sender] = times[nextIndex];
    };

    /**
     * Save notification config
     */
    $scope.saveNotificationConfig = function () {
        return $http.post('/profile/notifications/save-notification-config',
            {notify_config: $scope.userConfig, noCheckOnline: $scope.form.noCheckOnline,allowDialogAutoCreate: $scope.form.allowDialogAutoCreate,allowCompanyDialogAutoCreate: $scope.form.allowCompanyDialogAutoCreate})
            .then(function (response) {
                $notify.success(response.data);
            })
            .catch($notifyHttpErrors);
    }
});