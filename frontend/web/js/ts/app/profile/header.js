"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.controller('ProfileHeaderController', function ($scope, $notify, $notifyHttpErrors, $http, $modal, $timeout, $router)
{
    /**
     * Show change avatar modal
     */
    $scope.showChangeAvatarModal = function ()
    {
        var cropper, avatarFile;

        $modal.open({
            template : '/app/profile/change-avatar-modal.html',
            scope : {
                form : {
                    isAvatarLoaded : false
                },

                /**
                 * On user load avatar
                 * @param event
                 */
                onAvatarFileChange : function (event)
                {
                    var reader = new FileReader(),
                        scope = this;

                    reader.onload = function (readerLoadEvenr)
                    {
                        var image = new Image();
                        image.onload = function ()
                        {
                            if(image.width < 100 || image.height < 100)
                            {
                                $notify.error(_t('user.profile', 'Please upload pictures with a size that is not less than 100x100 pixels. Max size of file is 1.5 MB.'));
                                return;
                            }

                            scope.form.isAvatarLoaded = true;
                            scope.$apply();

                            $timeout(function()
                            {
                                var cropImg = $('#crop-image'),
                                    cropImgOrig = cropImg[0];

                                cropImgOrig.src = readerLoadEvenr.target.result;
                                cropImgOrig.onload = function ()
                                {
                                    var scale = cropImgOrig.width / cropImgOrig.naturalWidth;
                                    cropper = cropImg.cropper({
                                        aspectRatio: 1,
                                        guides : false,
                                        zoomable : false,
                                        minCropBoxWidth : 100 * scale,
                                        minCropBoxHeight : 100 * scale,
                                        preview : '.avatar-preview__img'
                                    });
                                };
                            });
                        };
                        
                        image.src = readerLoadEvenr.target.result;
                    };

                    avatarFile = event.target.files[0];
                    reader.readAsDataURL(avatarFile);
                },

                /**
                 * Save avatar
                 */
                saveAvatar : function ()
                {
                    return $http.post('/profile/profile/change-avatar', {
                        crop : cropper.cropper('getData', true),
                        file : avatarFile
                    })
                        .then($router.toUserProfile)
                        .catch($notifyHttpErrors);
                }
            }
        });
    }
    
});