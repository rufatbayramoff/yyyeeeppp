"use strict";

/**
 * Service for working witch geo
 */
app.factory('$deliveryValidator', ['$http', '$router', '$notify', '$modal', '$q', '$notifyHttpErrors', '$formValidator', function ($http, $router, $notify, $modal, $q, $notifyHttpErrors, $formValidator) {
    var $deliveryStepValidator = {};

    var confirmValidatedAddress = function (validatedAddress) {
        var deffer = $q.defer();
        $modal.open({
            template: '/app/store/checkuot/delivery/validate-address-modal.html',
            scope: {
                address: validatedAddress,
                confirmAddress: function () {
                    //noinspection JSUnresolvedFunction
                    this.$dismiss();
                    deffer.resolve();
                },

                declineAddress: function () {
                    //noinspection JSUnresolvedFunction
                    this.$dismiss();
                    deffer.reject();
                }
            }
        });
        return deffer.promise;
    };

    var confirmInvalidAddress = function (deliveryForm) {
        var deffer = $q.defer();
        $modal.open({
            template: '/app/store/checkuot/delivery/invalid-address-modal.html',
            scope: {
                deliveryForm: deliveryForm,
                confirmAddress: function () {
                    //noinspection JSUnresolvedFunction
                    this.$dismiss();
                    deffer.resolve();
                },

                declineAddress: function () {
                    //noinspection JSUnresolvedFunction
                    this.$dismiss();
                    deffer.reject();
                }
            }
        });
        return deffer.promise;
    }

    $deliveryStepValidator.validate = function (deliveryForm, psMachineDeliveryInfo, onSuccessCheckout, onError, onRetry) {
        var params = {
            'deliveryForm': deliveryForm
        };
        $formValidator.clearErrors();
        $http.post($router.getAjaxCuttingCheckAndSaveDeliveryInfo(), params)
            .then(function (response) {
                // console.log('ajax finished');
                if (!response.data) {
                    $notify.error('Error. Please, try again.');
                    onError();
                    return;
                }
                if (!response.data.success) {
                    $notify.error('Error. Please, try again.');
                    onError();
                    return;
                }
                onSuccessCheckout();
            })
            .catch(function (data) {
                if (data.status === 418) {
                    onError();
                    $router.toPrintModel3dIndex();
                    return;
                }
                if (data.data.validatedAddress) {
                    var validatedAddress = data.data.validatedAddress;
                    confirmValidatedAddress(validatedAddress)
                        .then(
                            function () {
                                deliveryForm.load(validatedAddress);
                                onRetry();
                            },
                            function () {
                                $notify.warning(_t('site.order', 'The address you have entered is invalid. Please re-enter a valid shipping address.'));
                            }
                        );
                    return;
                }
                if (data.data.validateErrors && deliveryForm.street) {
                    let deliveryformStreetError = data.data.validateErrors['deliveryform-street'] ? data.data.validateErrors['deliveryform-street'] : '';
                    delete (data.data.validateErrors['deliveryform-street']);
                    if (!Object.keys(data.data.validateErrors).length && deliveryformStreetError) {  // Only one error with street
                        // Possible correct not validated address
                        confirmInvalidAddress(deliveryForm)
                            .then(
                                function () {
                                    deliveryForm.forceInvalidAddress = 1;
                                    onRetry();
                                },
                                function () {
                                    deliveryForm.forceInvalidAddress = 0;
                                    data.data.validateErrors['deliveryform-street'] = deliveryformStreetError; // Restore show error
                                    $notify.warning(_t('site.order', 'The address you have entered is invalid. Please re-enter a valid shipping address.'));
                                    onError();
                                    $notifyHttpErrors(data);
                                }
                            );
                        return;
                    }
                }
                $notifyHttpErrors(data);
                onError();
            });
    };


    return $deliveryStepValidator;
}])
    .filter('addressBody', ['$sce', function ($sce) {
        return function (address) {
            var addressStr = (address.street || '') + " " + (address.street2 || '') + '<br/>'
                + (address.city || '') + " " + (address.state || '') + " " + (address.zip || '') + '<br/>'
                + (address.country || '');

            if (address.phone) {
                addressStr += '<br/>' + _t('site.store', 'Phone') + ': ' + address.phone;
            }
            //noinspection JSUnresolvedFunction
            return $sce.trustAsHtml(addressStr);
        };
    }])