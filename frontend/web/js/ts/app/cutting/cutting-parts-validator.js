"use strict";

/**
 * Service for working witch geo
 */
app.factory('$cuttingPartsValidator', ['$http', '$router', '$notify', '$modal', '$q', '$notifyHttpErrors', '$formValidator', function ($http, $router, $notify, $modal, $q, $notifyHttpErrors, $formValidator) {
    var $cuttingPartsValidator = {};

    $cuttingPartsValidator.validate = function (cuttingPack, onError, onRetry) {
        if (!cuttingPack.hasActivePackParts()) {
            return false;
        }
        return true;
    };

    return $cuttingPartsValidator;
}]);
