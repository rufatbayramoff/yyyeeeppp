"use strict";

app.controller('offers-step', ['$scope', '$timeout', '$router', '$modal', '$rootScope', '$http', '$notifyHttpErrors', 'controllerParams', function ($scope, $timeout, $router, $modal, $rootScope, $http, $notifyHttpErrors, controllerParams) {
    $scope.sortOptions = {
        default: _t('site.store', 'Default'),
        rating: _t('site.store', 'Best Rating'),
        price: _t('site.store', 'Lowest print price'),
        distance: _t('site.store', 'Nearest to me')
    };
    $scope.currencies = {
        usd: 'USD',
        eur: 'EUR',
    };

    $scope.fullScreen(0);

    $scope.updateOffersListBackground = function (forceUpdate) {
        /*
        console.log('updateOffersListBackground');
        console.log('updateOffersListTimeout: '+$scope.updateOffersListTimeout);
        console.log('startUpdateOffersOnGetResponse:'+$scope.startUpdateOffersOnGetResponse);
        console.log('startedUpdateOffersRequest:'+$scope.startedUpdateOffersRequest);
        */

        if ($scope.updateOffersListTimeout) {
            clearTimeout($scope.updateOffersListTimeout);
            $scope.updateOffersListTimeout = 0;
        }

        $scope.updateOffersListTimeout = setTimeout(function () {
            $scope.offersPageInfo.currentPage = 0;
            $scope.updateOffersListTimeout = 0;
            $scope.offersPageInfo.currentPage = 0;
            $scope.updateOffersList();
        }, 300);

    };

    $scope.updateOffersList = function () {
/*
        console.log('updateOffersList');
        console.log('updateOffersListTimeout: '+$scope.updateOffersListTimeout);
        console.log('startUpdateOffersOnGetResponse:'+$scope.startUpdateOffersOnGetResponse);
        console.log('startedUpdateOffersRequest:'+$scope.startedUpdateOffersRequest);
*/
        if ($scope.startedUpdateOffersRequest) {
            $scope.startUpdateOffersOnGetResponse = true;
            return;
        }
        $scope.startedUpdateOffersRequest = true;


        var params = {
            intlOnly: $scope.offersBundleData.intlOnly,
            cuttingPack: $scope.cuttingPack.offersSerializer()
        };

        //$http.get($router.getPrintModel3dOffersList(), {params: params})
        $http.post($router.getAjaxCuttingOffers(
            $scope.cuttingPack.id,
            $scope.offersPageInfo.currentPage,
            $scope.offersPageInfo.pageSize,
            $scope.offersBundleData.sortOffersBy,
            $scope.isWidget(),
            $scope.offersBundleData.psIdOnly
        ), params)
            .then(function (response) {
                // console.log('ajax finished');
                $scope.startedUpdateOffersRequest = false;
                if (!response.data || !response.data.success) {
                    $notify.error('Error. Please, try again.');
                } else {
                    $scope.offersBundleData.offersBundle = new OffersBundle(response.data.offersBundle);
                    var bestOffer = $scope.offersBundleData.offersBundle.getBestOffer();
                    $scope.offersPageInfo.numPages = Math.ceil($scope.offersBundleData.offersBundle.totalCount / $scope.offersPageInfo.pageSize);
                    $scope.offersBundleData.allowedMaterials = new AllowedMaterialsList(response.data.offersBundle.allowedMaterialsColors);
                    if(bestOffer) {
                        $scope.selectMachine(bestOffer);
                    }
                    if (!$scope.offersBundleData.selectedMaterialId && $scope.offersBundleData.allowedMaterials.isEmptyList()) {
                        $scope.offersBundleData.selectedMaterialId = $scope.offersBundleData.allowedMaterials.getFirstMaterial().material.id;
                    }
                    $scope.updateThicknessAndColorForCurrentMaterial();
                    if (!$scope.offersBundleData.offersBundle.totalCount && !$scope.offersBundleData.intlOnly) {
                        $scope.offersBundleData.intlOnly = true;
                        $scope.updateOffersList();
                    }
                }
                if ($scope.startUpdateOffersOnGetResponse) {
                    $scope.updateOffersList();
                    $scope.startUpdateOffersOnGetResponse = false;
                }
            })
            .catch(function (data) {
                if (data.status === 418) {
                    $router.setUrl($router.getCuttingPackUpload());
                    return;
                }
                $scope.startedUpdateOffersRequest = false;
                if (data.status === 419) {
                    setTimeout(function () {
                        $scope.updateOffersListBackground();
                    }, 1000);
                    return;
                }
                $notifyHttpErrors(data);
            });
    };

    $scope.previewParts = function () {
        $modal.open({
            template: '/laser-cutting/templates/previewParts.html',
            onShown: function () {
                $rootScope.$broadcast('cuttingPartsPreview:modalOpen');
            },
            onClose: function () {
                $scope.updateOffersListBackground();
            },
            scope: {
                cuttingPack: $scope.cuttingPack,
            }
        });
    };

    $scope.changeQty = function (cuttingPart) {
        $scope.updateOffersListBackground();
    };

    $scope.updateThicknessAndColorForCurrentMaterial = function () {
        if (!$scope.offersBundleData.selectedMaterialId) {
            return ;
        }

        let materialItem = $scope.offersBundleData.allowedMaterials.getMaterialItem($scope.offersBundleData.selectedMaterialId);
        if (!materialItem) {
            return ;
        }

        if (!$scope.offersBundleData.selectedThickness || !materialItem.isThicknessExists($scope.offersBundleData.selectedThickness)) {
            $scope.offersBundleData.selectedThickness = materialItem.getFirstThickness().thickness;
        }

        if (!$scope.offersBundleData.selectedMaterialColorId || !materialItem.getThickness($scope.offersBundleData.selectedThickness).isColorExists($scope.offersBundleData.selectedMaterialColorId)) {
            let m = materialItem.getThickness($scope.offersBundleData.selectedThickness);
            $scope.offersBundleData.selectedMaterialColorId = m.getFirstColor().id;
        }

        if ($scope.offersBundleData.selectedMaterialId !== $scope.cuttingPack.materialId) {
            $scope.cuttingPack.materialId = $scope.offersBundleData.selectedMaterialId;
            $scope.updateOffersListBackground();
        }

        if ($scope.offersBundleData.selectedThickness !== $scope.cuttingPack.thickness) {
            $scope.cuttingPack.thickness = $scope.offersBundleData.selectedThickness;
            $scope.updateOffersListBackground();
        }

        if ($scope.offersBundleData.selectedMaterialColorId !== $scope.cuttingPack.colorId) {
            $scope.cuttingPack.colorId = $scope.offersBundleData.selectedMaterialColorId;
            $scope.updateOffersListBackground();
        }
    };

    $scope.changeMaterial = function (materialId) {
        $scope.offersBundleData.selectedMaterialId = materialId;
        $scope.updateThicknessAndColorForCurrentMaterial();
    };

    $scope.changeThickness = function () {
        $scope.cuttingPack.setThickness($scope.offersBundleData.selectedThickness);
        $scope.updateOffersListBackground();
    };

    $scope.changeColor = function (colorId) {
        $scope.offersBundleData.selectedMaterialColorId = colorId;
        $scope.cuttingPack.setColor($scope.offersBundleData.selectedMaterialColorId);
        $scope.updateOffersListBackground();
    };

    $scope.onChangeWithInternationalDelivery = function() {
        $scope.updateOffersListBackground();
    };

    $scope.setPsCountry = function () {
        if ($scope.offersBundleData.psIdOnly) {
            return $http.post($router.setGeoLocation(), {'UserLocator': {'country': $scope.psCountryIsoOnly}})
                .then(function (response) {
                    if (!response.data || !response.data.success) {
                        $notify.error('Error. Please, try again.');
                    } else {
                        $router.reload();
                    }
                })
                .catch(function (data) {
                    if (data.status === 418) {
                        $router.toPrintModel3dIndex();
                        return;
                    }
                    $notifyHttpErrors(data);
                });

        }
    };

    $scope.selectMachine = function(offerItem) {
        $scope.offersBundleData.activeOfferItemId = offerItem.serviceMachine.id;
        var params = {
            cuttingPack: $scope.cuttingPack,
            offerServiceId: $scope.offersBundleData.activeOfferItemId,
        };
        $scope.deliveryForm.needLoad = false;
        $scope.deliveryForm.loaded = false;

        $http.post($router.getAjaxCuttingSelectOffer(), params)
            .then(function (response) {
                // console.log('ajax finished');
                if (!response.data || !response.data.success) {
                    $notify.error('Error. Please, try again.');
                } else {
                    $scope.psMachineDeliveryInfo.load(response.data.psMachineDeliveryInfo);
                    $scope.deliveryForm.load(response.data.deliveryForm);
                }
            })
            .catch(function (data) {
                if (data.status === 418) {
                    $router.toPrintModel3dIndex();
                    return;
                }
                $notifyHttpErrors(data);
            });
    };

    $scope.offerHere = function (offerItem) {
        $scope.selectMachine(offerItem);
        $scope.nextStep();
        return '';
    };

    $scope.getCurrentMaterial = function () {
        if (!$scope.offersBundleData.allowedMaterials.list) {
            return null;
        }
        if (!$scope.offersBundleData.selectedMaterialId) {
            return null;
        }
        return $scope.offersBundleData.allowedMaterials.list[$scope.offersBundleData.selectedMaterialId];
    };

    $scope.getCurrentMaterialColors = function () {
        if (!$scope.offersBundleData.allowedMaterials.list) {
            return null;
        }
        if (!$scope.offersBundleData.selectedMaterialId) {
            return null;
        }
        if (!$scope.offersBundleData.selectedThickness) {
            return null;
        }
        return $scope.offersBundleData.allowedMaterials.list[$scope.offersBundleData.selectedMaterialId]['thickness'][$scope.offersBundleData.selectedThickness].colors;
    };

    $scope.materialsGuide = function () {
        $modal.open({
            template: '/wiki/materials-guide.html',
            onShown: function () {
                $rootScope.$broadcast('materialsGuide:modalOpen');
            },
            scope: {
                type: 'laser_cutting',
                allowedMaterialsList: [], // typeof $scope.offersBundleData.allowedMaterials.getMaterialsList === 'function' ? $scope.offersBundleData.allowedMaterials.getMaterialsList() : [],
            }
        });
    };

    $scope.updateOffersListBackground();


}]);