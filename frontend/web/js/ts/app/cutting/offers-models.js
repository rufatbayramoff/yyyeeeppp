/*
* @property {Offer[]} offers
* @property {Offer} bestOffer;
* @property {string[]} messages
* @property {string} emptyReason
*/
function OffersBundle(data) {
    angular.extend(this, data);
    this.offers = {};
    for (var key in data.offers) {
        if (!data.offers.hasOwnProperty(key)) continue;
        var dataElement = data.offers[key];
        this.offers[key] = new Offer(dataElement);
    }
}

/**
 * @param id
 */
OffersBundle.prototype.getOfferById = function (id) {
    var returnValue = this.offers.find(function (offer) {
        return offer.serviceMachine.id == id;
    });
    return returnValue;
};

OffersBundle.prototype.isNoDeliveryReason = function () {
    if (this.emptyReason == 'no_delivery') {
        return true;
    }
    return false;
};

OffersBundle.prototype.getEmptyListComment = function () {
    if (this.emptyReason == 'no_cutting_parts') {
        return _t('site.ps', 'No cutting parts.');
    }
    if (this.emptyReason == 'no_delivery') {
        return _t('site.ps', 'Sorry, the company does not/cannot ship to your area.');
    }
    if (this.emptyReason == 'no_texture') {
        return _t('site.ps', 'No manufacturers were found with the combination of materials and colors you have selected.');
    }
    if (this.emptyReason == 'large_size') {
        return _t('site.ps', 'No manufacturers were found. Too big model size to produce your order.');
    }
    return _t('site.ps', 'No manufacturers were found. Please try selecting different materials and/or colors.');
};

OffersBundle.prototype.getBestOffer = function () {
    if (this.offers) {
        if(Array.isArray(this.offers)) {
            return _.first(this.offers);
        }
        return this.offers[_.keys(this.offers)[0]];
    }
    return null;
};

/**
 * @property {ServiceMachine[]} serviceMachine
 * @property {Money} $manufacturePrice
 * @property {Money} $packingPrice
 * @property {Money} $deliveryPrice
 * @property {float} tsBonusAmount
 * @property {bool} isPickup
 *
 * @param data
 * @constructor
 */
function Offer(data) {
    angular.extend(this, data);
    this.manufacturePrice = new Money(data['manufacturePrice']);
    this.packingPrice = new Money(data['packingPrice']);
    if(data['deliveryPrice'] === 'TBC') {
        this.deliveryPrice = data['deliveryPrice'];
    } else if(data['deliveryPrice']) {
        this.deliveryPrice = new Money(data['deliveryPrice']);
    }
    if (data['estimateDeliveryCost']) {
        this.estimateDeliveryCost = new Money(data['estimateDeliveryCost']);
    } else {
        this.estimateDeliveryCost = null;
    }
    this.reviewsInfo = this.serviceMachine.reviewsInfo || {};
    this.reviewsInfo.rating = this.reviewsInfo.rating || 0;
    this.reviewsInfo.reviewsCount = this.reviewsInfo.reviewsCount || 0;
}

Offer.prototype.hasDelivery = function () {
    if ((this.deliveryTypes.indexOf('standard') !== -1) || (this.deliveryTypes.indexOf('intl') != -1)) {
        return true;
    }
    return false;
}

Offer.prototype.getDeliveriesString = function () {
    var retVal = '';
    if (this.deliveryPrice) {
        retVal += _t('site.ps', 'Delivery');
    }
    if (this.allowPickup) {
        if (retVal) {
            retVal = _t('site.ps', 'Pickup') + ', ' + retVal;
        } else {
            retVal = _t('site.ps', 'Pickup');
        }
    }
    return retVal;
};

Offer.prototype.isFreeDelivery = function () {
    return this.deliveryPrice && this.deliveryPrice.amount === 0;
}

Offer.prototype.isOwnPrintPrice = function () {
    if (this.userPrice.amount !== this.anonimysPrice.amount) {
        return true;
    }
    return false;
};

Offer.prototype.getProducePrice = function () {
    let producePrice = this.manufacturePrice;
    return producePrice;
};

Offer.prototype.companyRating = function () {
    return this.reviewsInfo.rating+'/5 ('+this.reviewsInfo.reviewsCount+')';
};

Offer.prototype.topCheckNoInfo = function () {
    return this.serviceMachine.peliability === 'no info';
}


/**
 * @property {string} id
 * @property {string} title
 * @property {string} publicLink
 * @property {string} companyLogoUrl
 * @property {string} companyTitle
 * @property {string} technology
 *
 * @param data
 * @constructor
 */
function ServiceMachine(data) {
    angular.extend(this, data);
}

/**
 * @property {GroupMaterialsItem[]} list
 * @param data
 * @constructor
 */
function AllowedMaterialsList(data) {
    this.list = {};
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        var dataElement = data[key];
        var element = new MaterialItem(dataElement);
        this.list[element.material.id] = element;
    }
}

AllowedMaterialsList.prototype.isEmpty = function () {
    return this.list.length === 0;
};

AllowedMaterialsList.prototype.getFirstMaterial = function () {
    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        var dataElement = this.list[key];
        return dataElement;
    }
    return null;
};

/**
 * @param materialId
 * @returns {{id, group_id, title}|boolean}
 */
AllowedMaterialsList.prototype.getAllowedMaterialById = function (materialId) {
    return this.list[materialId] || false;
};

AllowedMaterialsList.prototype.isEmptyList = function () {
    return Object.keys(this.list).length;
};

AllowedMaterialsList.prototype.getMaterialItem = function (materialId) {
    return this.list[materialId];
};

/**
 *
 * @property MaterialInfo material
 * @property ColorsInfo[] colors
 * @property float[]      thickness
 * @param data
 * @constructor
 */
function MaterialItem(data) {
    this.material = new MaterialInfo(data['material']);
    this.thickness = {};
    for (var key in data.thickness) {
        if (!data.thickness.hasOwnProperty(key)) continue;
        let dataElement = data.thickness[key];

        let thicknessInfo = new ThicknessInfo(dataElement);
        this.thickness[key] = thicknessInfo;
    }
}

/**
 *
 * @returns {bool}
 */
MaterialItem.prototype.isThicknessExists = function (thickness) {
    return this.thickness[thickness] ? true : false;
};


MaterialItem.prototype.getThickness = function (thickness) {
    return this.thickness[thickness] ? this.thickness[thickness] : null;
};


/**
 *
 * @returns {float}
 */
MaterialItem.prototype.getFirstThickness = function () {
    for (var key in this.thickness) {
        if (!this.thickness.hasOwnProperty(key)) continue;
        var dataElement = this.thickness[key];
        return dataElement;
    }
    return null;
};

/**
 * @property string id
 * @property string title
 *
 * @param data
 * @constructor
 */
function MaterialInfo(data) {
    angular.extend(this, data);
}

/**
 * @property string thickness
 * @property ColorInfo[] colors
 *
 * @param data
 * @constructor
 */
function ThicknessInfo(data) {
    angular.extend(this, data);
}

ThicknessInfo.prototype.isColorExists = function (colorId) {
    return this.colors[colorId] ? true : false;
};

ThicknessInfo.prototype.getFirstColor = function () {
    for (var key in this.colors) {
        if (!this.colors.hasOwnProperty(key)) continue;
        var dataElement = this.colors[key];
        return dataElement;
    }
    return null;
};

ThicknessInfo.prototype.getColor = function (colorId) {
    return this.colors[colorId] ? this.colors[colorId] : null;
};


/**
 * @property string id
 * @property string title
 *
 * @param data
 * @constructor
 */
function ColorInfo(data) {
    angular.extend(this, data);
}

