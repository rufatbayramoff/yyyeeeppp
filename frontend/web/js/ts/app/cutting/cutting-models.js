"use strict";

/**
 * @property {int} uuid
 * @property {string} title
 * @property {bool} isSameMaterialForAllParts
 * @property {int} materialId
 * @property {number} thickness
 * @property {int} colorId
 * @property {bool} isConfirmedCutAll
 * @property {CuttingPackFile[]} cuttingPackFiles
 * @param data
 * @constructor
 */
function CuttingPack(data) {
    this.load(data);
}

CuttingPack.prototype.load = function (data) {
    this.uuid = data['uuid'];
    this.title = data['title'];
    this.isSameMaterialForAllParts = data['isSameMaterialForAllParts'];
    this.materialId = data['materialId'];
    this.thickness = data['thickness'];
    this.colorId = data['colorId'];
    this.cuttingPackFiles = [];
    for (var key in data['cuttingPackFiles']) {
        if (!data['cuttingPackFiles'].hasOwnProperty(key)) continue;
        let packFileInfo = data['cuttingPackFiles'][key];
        let packFile = new CuttingPackFile(packFileInfo);
        packFile.cuttingPack = this;
        this.cuttingPackFiles.push(packFile);
    }
    this.loadSvg = false;
};

CuttingPack.prototype.update = function (data) {
    if (data.hasOwnProperty('uuid')) {
        this.uuid = data['uuid'];
    }
    if (data.hasOwnProperty('title')) {
        this.title = data['title'];
    }
    if (data.hasOwnProperty('isSameMaterialForAllParts')) {
        this.isSameMaterialForAllParts = data['isSameMaterialForAllParts'];
    }
    if (data.hasOwnProperty('materialId')) {
        this.materialId = data['materialId'];
    }
    if (data.hasOwnProperty('thickness')) {
        this.thickness = data['thickness'];
    }
    if (data.hasOwnProperty('colorId')) {
        this.colorId = data['colorId'];
    }

    for (var key in this.cuttingPackFiles) {
        if (!this.cuttingPackFiles.hasOwnProperty(key)) continue;
        let cuttingPackFile = this.cuttingPackFiles[key];
        cuttingPackFile.isCanceled = 1;
    }

    for (var key in data['cuttingPackFiles']) {
        if (!data['cuttingPackFiles'].hasOwnProperty(key)) continue;
        let packFileInfo = data['cuttingPackFiles'][key];
        let existsCuttingPackFile = this.getCuttingPackFile(packFileInfo['uuid']);
        if (existsCuttingPackFile) {
            existsCuttingPackFile.update(packFileInfo);
            existsCuttingPackFile.isCanceled = 0;
        } else {
            let packFile = new CuttingPackFile(packFileInfo);
            packFile.cuttingPack = this;
            this.cuttingPackFiles.push(packFile);
        }
    }
};

CuttingPack.prototype.getCuttingPackFile = function (uuid) {
    for (var key in this.cuttingPackFiles) {
        if (!this.cuttingPackFiles.hasOwnProperty(key)) continue;
        let packFile = this.cuttingPackFiles[key];
        if (packFile.uuid === uuid) {
            return packFile;
        }
    }
    return null;
};

CuttingPack.prototype.isNeedPing = function () {
    return this.isConverting();
};

/**
 * Has select on any cutting page
 */
CuttingPack.prototype.hasActivePackParts = function () {
    let activePart = this.getActivePackParts();
    return activePart.length > 0;
};

CuttingPack.prototype.getNextViewPage = function () {
    let activePages = this.getActivePages();
    for (var key in activePages) {
        if (!activePages.hasOwnProperty(key)) continue;
        let packPage = activePages[key];
        if ((packPage.isConfirmedCutAll !== 0) && (packPage.isConfirmedCutAll !== 1)) {
            return packPage;
        }
    }
    return null;
};

CuttingPack.prototype.isEmpty = function () {
    for (var key in this.cuttingPackFiles) {
        if (!this.cuttingPackFiles.hasOwnProperty(key)) continue;
        var packFile = this.cuttingPackFiles[key];
        if (packFile.isCanceled) {
            continue;
        }
        return false;
    }
    return true;
};

CuttingPack.prototype.isConverting = function () {
    for (var key in this.cuttingPackFiles) {
        if (!this.cuttingPackFiles.hasOwnProperty(key)) continue;
        let packFile = this.cuttingPackFiles[key];
        if (packFile.isModel() && !packFile.isConverted && !packFile.isCanceled) {
            return true;
        }
    }
    return false;
};

CuttingPack.prototype.isUploading = function () {
    for (var key in this.cuttingPackFiles) {
        if (!this.cuttingPackFiles.hasOwnProperty(key)) continue;
        var packFile = this.cuttingPackFiles[key];
        if (packFile.isCanceled) {
            continue;
        }
        if (packFile.uploadPercent > 1 && packFile.uploadPercent < 100 && !packFile.isCanceled) {
            return true;
        }
    }
    return false;
};

CuttingPack.prototype.hasModels = function () {
    for (var key in this.cuttingPackFiles) {
        if (!this.cuttingPackFiles.hasOwnProperty(key)) continue;
        var packFile = this.cuttingPackFiles[key];
        if (packFile.isCanceled) {
            continue;
        }
        if (packFile.isModel()) {
            return true;
        }
    }
    return false;
};

CuttingPack.prototype.getActivePages = function () {
    let activeFiles = this.getActivePackFiles();
    let packPages = [];
    for (var key in activeFiles) {
        if (!activeFiles.hasOwnProperty(key)) continue;
        var packFile = activeFiles[key];
        if (!packFile.isModel()) {
            continue;
        }
        packPages = packPages.concat(packFile.cuttingPackPages)
    }
    return packPages;
};

CuttingPack.prototype.getActivePackFiles = function () {
    let packFiles = [];
    for (var key in this.cuttingPackFiles) {
        if (!this.cuttingPackFiles.hasOwnProperty(key)) continue;
        var packFile = this.cuttingPackFiles[key];
        if (packFile.isCanceled || !packFile.qty) {
            continue;
        }
        packFiles.push(packFile)
    }
    return packFiles;
};

CuttingPack.prototype.getFirstCuttingPack = function () {
    let activeParts = this.getActivePackFiles();
    return activeParts[0];
};

CuttingPack.prototype.getFirstCuttingPage = function () {
    let activeParts = this.getActivePages();
    return activeParts[0];
};

CuttingPack.prototype.hasSeveralFiles = function () {
    let activeParts = this.getActivePages();
    return activeParts.length > 1;
};

CuttingPack.prototype.hasSeveralPages = function () {
    let activeParts = this.getActivePages();
    return activeParts.length > 1;
};


CuttingPack.prototype.getItemByMd5NameSize = function (md5NameSize) {
    if (!this.id) {
        return null;
    }
    for (var key in this.cuttingPackFiles) {
        if (!this.cuttingPackFiles.hasOwnProperty(key)) continue;
        var packFile = this.cuttingPackFiles[key];
        if (packFile.fileMd5NameSize === md5NameSize) {
            return packFile;
        }
    }
    return null;
};

CuttingPack.prototype.addPart = function (partInfo) {
    partInfo.type = 'model';
    this.cuttingPackFiles.push(partInfo);
};

CuttingPack.prototype.addImage = function (imageInfo) {
    imageInfo.type = 'image';
    this.cuttingPackFiles.push(imageInfo);
};

CuttingPack.prototype.getActivePackParts = function () {
    let activeParts = [];
    let activePartFiles = this.getActivePackFiles();
    for (var key in activePartFiles) {
        if (!activePartFiles.hasOwnProperty(key)) continue;
        var packFile = activePartFiles[key];

        for (var pageKey in packFile.cuttingPackPages) {
            if (!packFile.cuttingPackPages.hasOwnProperty(pageKey)) continue;
            var packPage = packFile.cuttingPackPages[pageKey];
            activeParts = activeParts.concat(packPage.cuttingPackParts);
        }
    }
    return activeParts;
};


CuttingPack.prototype.setColor = function (colorId) {
    this.colorId = colorId;
};

CuttingPack.prototype.setMaterial = function (materialId) {
    this.materialId = materialId;
};

CuttingPack.prototype.setThickness = function (thickness) {
    this.thickness = thickness;
};

CuttingPack.prototype.offersSerializer = function (thickness) {
    let info = {
        'uuid': this.uuid,
        'isSameMaterialForAllParts': this.isSameMaterialForAllParts,
        'materialId': this.materialId,
        'colorId': this.colorId,
        'thickness': this.thickness,
        'cuttingParts': []
    };

    let activeParts = this.getActivePackParts();

    for (var key in activeParts) {
        if (!activeParts.hasOwnProperty(key)) continue;
        let cuttingPart = activeParts[key];
        let cuttingPartSerialized = cuttingPart.offersSerializer();
        info.cuttingParts.push(cuttingPartSerialized);
    }

    return info;
};

CuttingPack.prototype.setLoadSvg = function (loadSvg) {
    this.loadSvg = loadSvg;
};

CuttingPack.prototype.isLoadSvg = function () {
    return this.loadSvg === true;
};


/**
 * @property {string}    uuid
 * @property {number}    qty
 * @property {string}    type
 * @property {string}    title
 * @property {string}    fileMd5NameSize
 * @property {CuttingPackPage}  cuttingPackPages
 * @param data
 * @constructor
 */
function CuttingPackFile(data) {
    this.uuid = uuidHelper.generateUuid();
    this.qty = 1;
    this.load(data);
}

CuttingPackFile.prototype.load = function (data) {
    angular.extend(this, data);
    this.type = data['type'];
    this.cuttingPackPages = [];
    for (var key in data['cuttingPackPages']) {
        if (!data['cuttingPackPages'].hasOwnProperty(key)) continue;
        let packFilePageArray = data['cuttingPackPages'][key];
        let cuttingPackFilePage = new CuttingPackPage(packFilePageArray);
        cuttingPackFilePage.cuttingPackFile = this;
        this.cuttingPackPages.push(cuttingPackFilePage);
    }
};

CuttingPackFile.prototype.update = function (data) {
    this.qty = data['qty'];
    this.isConverted = data['isConverted'];
    this.previewUrl = data['previewUrl'];
    this.format = data['format'];
    this.type = data['type'];

    for (var key in data['cuttingPackPages']) {
        if (!data['cuttingPackPages'].hasOwnProperty(key)) continue;
        let packFilePageArray = data['cuttingPackPages'][key];
        let cuttingPackFilePage = this.getCuttingPackPage(packFilePageArray['uuid']);
        if (cuttingPackFilePage) {
            cuttingPackFilePage.update(packFilePageArray);
        } else {
            let cuttingPackFilePage = new CuttingPackPage(packFilePageArray);
            cuttingPackFilePage.cuttingPackFile = this;
            this.cuttingPackPages.push(cuttingPackFilePage);
        }
    }
};

CuttingPackFile.prototype.getCuttingPackPage = function (uuid) {
    for (var key in this.cuttingPackPages) {
        if (!this.cuttingPackPages.hasOwnProperty(key)) continue;
        let cuttingPackFilePage = this.cuttingPackPages[key];
        if (cuttingPackFilePage.uuid === uuid) {
            return cuttingPackFilePage;
        }
    }
    return null;
};

CuttingPackFile.prototype.isImage = function (data) {
    return this.type == 'image';
};

CuttingPackFile.prototype.isModel = function (data) {
    return this.type == 'model';
};

CuttingPackFile.prototype.isConverting = function (data) {
    return (this.type == 'model') && (this.isConverted == 0);
};

CuttingPackFile.prototype.getPreviewUrl = function () {
    return this.isConverting() ? '/static/images/preloader80.gif' : this.previewUrl;
};

CuttingPackFile.prototype.getPartPosition = function (uuid) {
    let position = 1;
    let cuttingPackParts = this.getCuttingPackParts();
    for (var key in cuttingPackParts) {
        if (!cuttingPackParts.hasOwnProperty(key)) continue;
        let packPart = cuttingPackParts[key];
        if (packPart.uuid == uuid) {
            return position;
        }
        position++;
    }
    return '';
};

CuttingPackFile.prototype.getCuttingPackParts = function () {
    let cuttingPackParts = [];
    for (var key in this.cuttingPackPages) {
        if (!this.cuttingPackPages.hasOwnProperty(key)) continue;
        let packPage = this.cuttingPackPages[key];
        cuttingPackParts = cuttingPackParts.concat(packPage.cuttingPackParts);
    }
    return cuttingPackParts;
};


CuttingPackFile.prototype.toJSON = function () {
    return {
        'fileMd5NameSize': this.fileMd5NameSize,
        'format': this.format,
        'qty': this.qty,
        'selections': this.selections,
        'type': this.type,
        'uuid': this.uuid,
        'cuttingPackPages': this.cuttingPackPages
    }
};

/**
 * CuttingPackPage
 *
 * @param data
 * @property {string}  title
 * @constructor
 */
function CuttingPackPage(data) {
    this.uuid = uuidHelper.generateUuid();
    this.selections = null;
    this.load(data);
}

CuttingPackPage.prototype.load = function (data) {
    angular.extend(this, data);
    this.title = data['title'] + '-' + this.uuid;
    this.cuttingPackParts = [];
    for (var key in data['cuttingPackParts']) {
        if (!data['cuttingPackParts'].hasOwnProperty(key)) continue;
        let packFilePageArray = data['cuttingPackParts'][key];
        let cuttingPackPart = new CuttingPackPart(packFilePageArray);
        cuttingPackPart.cuttingPackPage = this;
        this.cuttingPackParts.push(cuttingPackPart);
    }
};

CuttingPackPage.prototype.update = function (data) {
    this.title = data['title'] + '-' + this.uuid;
    for (var key in data['cuttingPackParts']) {
        if (!data['cuttingPackParts'].hasOwnProperty(key)) continue;
        let packFilePageArray = data['cuttingPackParts'][key];
        let cuttingPackPart = this.getCuttingPackPart(packFilePageArray['uuid']);
        if (cuttingPackPart) {
            cuttingPackPart.load(data)
        } else {
            let cuttingPackPart = new CuttingPackPart(packFilePageArray);
            cuttingPackPart.cuttingPackPage = this;
            this.cuttingPackParts.push(cuttingPackPart);
        }
    }
};

CuttingPackPage.prototype.resetParts = function () {
    this.cuttingPackParts = [];
    this.isConfirmedCutAll = null;
};

CuttingPackPage.prototype.getCuttingPackPart = function (uuid) {
    for (var key in this.cuttingPackParts) {
        if (!this.cuttingPackParts.hasOwnProperty(key)) continue;
        let packPart = this.cuttingPackParts[key];
        if (packPart['uuid'] == uuid) {
            return packPart;
        }
    }
    return null;
};


/*
INFO EXAMPLE

let info = {
    blocks: [
        {
            'md5': '1c7BMLeAaw0uwP',
            'w': 300,
            'h': 600,
            'image': '/static/images/cutting-format-template.svg',
            'cutting': 6,
            'engrave': 3,
        },
        {
            'md5': '1c7BMLenLW2XMc',
            'w': 400,
            'h': 500,
            'image': '/static/images/cutting-format-template.svg',
            'cutting': 4,
            'engrave': 2,
        }
    ],
    'selections': {
        'a': 'b'
    }
};
*/
CuttingPackPage.prototype.importPartsWt = function (infoIn) {
    let info = infoIn.info;

    this.selections = infoIn['svg_state'];
    this.cuttingPackParts = [];
    for (var key in info['blocks']) {
        if (!info['blocks'].hasOwnProperty(key)) continue;
        let infoBlockArray = info['blocks'][key];
        //Skip zerro
        if (!((Math.round(infoBlockArray['w']) > 0) && (Math.round(infoBlockArray['h']) > 0))) {
            continue;
        }

        let cuttingPackFilePart = new CuttingPackPart({});
        cuttingPackFilePart.cuttingPackFile = this.cuttingPackFile;
        cuttingPackFilePart.cuttingPackPage = this;
        cuttingPackFilePart.loadInfoBlock(infoBlockArray);
        this.cuttingPackParts.push(cuttingPackFilePart);
    }
};

CuttingPackPage.prototype.toJSON = function () {
    let parts = [];
    let selections = "" +this.selections;

    for (var key in this.cuttingPackParts) {
        if (!this.cuttingPackParts.hasOwnProperty(key)) continue;
        let infoBlockArray = this.cuttingPackParts[key];
        let jsonInfoBlockArray = infoBlockArray.toJSON();
        parts.push(jsonInfoBlockArray);
    }

    return {
        'uuid': this.uuid,
        'selections': selections,
        'cuttingPackParts': parts,
    };
};


/**
 * @property {int} id
 * @property {int} qty
 * @property {string} image
 * @property {int} width
 * @property {int} height
 * @property {int} cuttingLength
 * @property {int} engravingLength
 * @property {int} materialId
 * @property {number} thickness
 * @property {int}  colorId
 * @param data
 * @constructor
 */
function CuttingPackPart(data) {
    this.uuid = uuidHelper.generateUuid();
    this.load(data);
}

CuttingPackPart.prototype.toJSON = function () {
    return {
        'uuid': this.uuid,
        'qty': this.qty,
        'image': this.image,
        'width': this.width,
        'height': this.height,
        'cuttingLength': this.cuttingLength,
        'engravingLength': this.engravingLength,
        'materialId': this.materialId,
        'number': this.number,
        'thickness': this.thickness,
        'colorId': this.colorId
    };
};

CuttingPackPart.prototype.offersSerializer = function () {
    return {
        'uuid': this.uuid,
        'qty': this.qty,
        'width': this.width,
        'height': this.height,
        'cuttingLength': this.cuttingLength,
        'engravingLength': this.engravingLength,
        'materialId': this.materialId,
        'thickness': this.thickness,
        'colorId': this.colorId
    };
};

CuttingPackPart.prototype.load = function (data) {
    angular.extend(this, data);
};

CuttingPackPart.prototype.viewTitle = function () {
    return this.cuttingPackPage.cuttingPackFile.getPartPosition(this.uuid) + ': ' + this.cuttingPackPage.cuttingPackFile.title;
};

CuttingPackPart.prototype.loadInfoBlock = function (data) {
    this.uuid = $.md5(data['data']['svg']);
    this.width = Math.round(data['w']);
    this.height = Math.round(data['h']);
    this.image = data['data']['png'];
    this.cuttingLength = Math.round(data['lengths']['lencut']);
    this.engravingLength = Math.round(data['lengths']['lenengrave']);
    this.qty = 1;
};

