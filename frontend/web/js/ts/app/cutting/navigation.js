app.config(['$stateProvider', '$routerProvider', function ($stateProvider, router) {

    $stateProvider
        .state('upload', {
            url: router.getCuttingPackUpload(),
            templateUrl: '/laser-cutting/templates/upload.html',
            controller: 'upload-step'
        })
        .state('parts', {
            url: router.getCuttingPackParts(),
            templateUrl: '/laser-cutting/templates/cutting-parts.html',
            controller: 'select-parts-step'
        })
        .state('offers', {
            url: router.getCuttingOffers(),
            templateUrl: '/laser-cutting/templates/offers.html',
            controller: 'offers-step'
        })
        .state('delivery', {
            url: router.getCuttingDelivery(),
            templateUrl: '/common/templates/delivery.html',
            controller: 'delivery-step'
        })
        .state('checkout', {
            url: router.getCuttingCheckout(),
            templateUrl: '/common/templates/checkout.html',
            controller: 'checkout-step'
        })
}]);


app.controller('laser-cutting-navigation', [
    '$scope', '$rootScope', '$router', '$state', '$cuttingPartsValidator', '$deliveryValidator',
    '$modal', 'controllerParams', function ($scope, $rootScope, $router, $state, $cuttingPartsValidator, $deliveryValidator, $modal, controllerParams) {

        $scope.validateErrors = {};
        $scope.posUid = controllerParams['posUid'] || '';
        $router.setPosUid($scope.posUid);
        $scope.cuttingPack = new CuttingPack(controllerParams['cuttingPack']);
        $scope.offersBundleData = {
            offerBundle: {},
            activeOfferItemId: -1,
            sortOffersBy: 'default',
            currency: 'usd',
            allowedMaterials: {},
            selectedMaterialId: null, // Use string because angular work in select correctly only with string
            selectedMaterialColorId: null,
            selectedThickness: null,
            timeAround: null,
            psIdOnly: controllerParams['psId'] || null
        };
        $scope.deliveryForm = new DeliveryForm();
        $scope.psMachineDeliveryInfo = new PsMachineDeliveryInfo();
        $scope.psCountryOnly = controllerParams['psCountry'];
        $scope.psCountryIsoOnly = controllerParams['psCountryIso'];
        $scope.showProgress = false;
        $scope.showProgressTop = {
            'status': 0
        };
        $scope.checkoutValidatedStatus = {
            validated: false
        };
        $scope.needCheckOnTabTap = false;

        // Cutting machines step controller
        $scope.offersPageInfo = {
            pageSize: 10,
            currentPage: 0,
            numPages: 1
        };

        $scope.steps = {
            'upload': {
                next: 'parts',
                validate: function () {
                },
            },
            'parts': {
                prev: 'upload',
                next: 'offers',
                validate: function () {
                    if ($scope.cuttingPack.isConverting()) {
                        return [
                            _t('site.printModel3d', 'Please wait for files to be converted')
                        ];
                    }
                    if ($scope.cuttingPack.isUploading()) {
                        return [
                            _t('site.printModel3d', 'Please wait for files to be uploaded')
                        ];
                    }

                    if (!$scope.cuttingPack.hasModels()) {
                        return [
                            _t('site.printModel3d', 'Please upload an  CDR, DXF, EPS, PDF or SVG file.')
                        ]
                    }
                },
            },
            'offers': {
                prev: 'parts',
                next: 'delivery',
                validate: function () {
                    if ($scope.cuttingPack.isConverting()) {
                        return [
                            _t('site.printModel3d', 'Please wait for files to be converted')
                        ];
                    }
                    if ($scope.cuttingPack.isUploading()) {
                        return [
                            _t('site.printModel3d', 'Please wait for files to be uploaded')
                        ];
                    }

                    if (!$scope.cuttingPack.hasModels()) {
                        return [
                            _t('site.printModel3d', 'Please upload an  CDR, DXF, EPS, PDF or SVG file.')
                        ]
                    }
                    if (!$cuttingPartsValidator.validate($scope.cuttingPack)) {
                        return [
                            _t('site.cutting', 'Please, mark the lines you want to cut or engrave'),
                        ];
                    }
                },
            },
            'delivery': {
                prev: 'offers',
                next: 'checkout',
                saveState: function () {
                    /*
                    $deliveryValidator.validate($scope.deliveryForm, $scope.psMachineDeliveryInfo, function () {

                    }, function () {

                    });*/
                },
                validate: function () {
                    if ($scope.offersBundleData.activeOfferItemId < 1) {
                        return [
                            _t('site.printModel3d', 'Please select a manufacturer.')
                        ]
                    }
                }
            },
            'checkout': {
                prev: 'delivery',
                next: 'done',
                validate: function () {
                    if ($scope.offersBundleData.activeOfferItemId < 1) {
                        return [
                            _t('site.printModel3d', 'Please select a manufacturer.')
                        ]
                    }

                    if ($scope.checkoutValidatedStatus.validated) {
                        return true;
                    }

                    function onSuccessCheckout() {
                        $scope.checkoutValidatedStatus.validated = true;
                        $state.go('checkout');
                    }

                    function onError(validateErrors) {
                        $scope.showProgress = false;
                        if (validateErrors && validateErrors.length) {
                            new TS.Notify({
                                type: 'error',
                                text: commonJs.prepareMessage(validateErrors || 'Error'),
                                automaticClose: true
                            });
                        }
                    }

                    function onRetry() {
                        $scope.needCheckOnTabTap = true;
                        $state.go('checkout');
                    }

                    $scope.showProgress = true;
                    $deliveryValidator.validate($scope.deliveryForm, $scope.psMachineDeliveryInfo, onSuccessCheckout, onError, onRetry);
                    return false;
                }
            },
            'done': {
                next: '',
                validate: function () {
                }
            }
        };

        $scope.stopShowProgress = function () {
            $scope.showProgress = false;
        };

        $scope.checkStep = function (stepName, showNotifies) {
            if (typeof showNotifies === 'undefined') {
                showNotifies = false;
            }
            var stateStep = $scope.steps[stepName];
            var validateErrors = stateStep.validate();

            if (validateErrors === false) {
                return false;
            }

            if (validateErrors && validateErrors.length) {
                new TS.Notify({
                    type: 'error',
                    text: commonJs.prepareMessage(validateErrors || 'Error'),
                    automaticClose: true
                });
                return false;
            }
            return true;
        };

        $scope.prevStep = function () {
            $scope.needCheckOnTabTap = false;
            var stepName = $state.current.name;
            var stateStep = $scope.steps[stepName];
            if (stateStep) {
                if (stateStep.saveState) {
                    stateStep.saveState();
                }
                $state.go(stateStep.prev);
                TS.NotifyHelper.hideAll();
                window.scrollTo(0, 0);
            }
        };

        $scope.nextStep = async function () {
            $scope.needCheckOnTabTap = false;
            var stepName = $state.current.name;
            var stateStep = $scope.steps[stepName];
            if (stateStep) {
                if (stateStep.saveState) {
                    await stateStep.saveState();
                }
                if ($scope.checkStep(stateStep.next, true)) {
                    $state.go(stateStep.next);
                    TS.NotifyHelper.hideAll();
                    window.scrollTo(0, 0);
                }
            }
        };

        $scope.fullScreen = function (turnOn) {
            if (turnOn) {
                $('body').addClass('cutting-full-screen-mode');
                if ($scope.cuttingPack.hasSeveralFiles()) {
                    $('body').addClass('cutting-several-files');
                }
            } else {
                $('body').removeClass('cutting-full-screen-mode');
                $('body').removeClass('cutting-several-files');
            }
        };

        $rootScope.$on('$stateChangeStart',
            async function (event, toState, toParams, fromState, fromParams) {
                if ($scope.posUid) {
                    toParams.posUid = $scope.posUid;
                }
                if (!$scope.needCheckOnTabTap) {
                    $scope.needCheckOnTabTap = true;
                    return;
                }
                if (fromState.name) {
                    var fromStep = $scope.steps[fromState.name];
                    if (fromStep.saveState) {
                        // Save state
                        await fromStep.saveState();
                    }
                }
                if (toState.name) {
                    if (!$scope.checkStep(toState.name)) {
                        if (fromState.name) {
                            event.preventDefault();
                            $state.go(fromState.name);
                        }
                    }
                }
            }
        );

        $scope.isWidget = function () {
            return window.location.href.indexOf('/widget') !== -1;
        };

        $scope.getModel3dPartPreviewUrl = function (model3dPart) {
            return $modelRender.getFullUrl(model3dPart, $scope.model3dTextureState.getColorForRendering(model3dPart));
        };

        $scope.setShowProgressTop = function (flag) {
            setTimeout(function () {
                $scope.showProgressTop.status = flag;
                $scope.$digest();
            }, 100);
        };

        $scope.$on('loadSvg',function(event,data){
            setTimeout(function () {
                $scope.cuttingPack.setLoadSvg(data);
                $scope.$digest();
            }, 100);
        });

        // Set init step
        setTimeout(function () {
            $state.go(controllerParams.currentStepName);
        }, 0);
    }]);

