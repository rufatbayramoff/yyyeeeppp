"use strict";

app.controller('select-parts-step', ['$scope', '$timeout', '$router', '$rootScope', '$http', '$notifyHttpErrors', 'controllerParams', function ($scope, $timeout, $router, $rootScope, $http, $notifyHttpErrors, controllerParams) {
    $scope.fullScreen(1);

    $scope.currentPackPage = $scope.cuttingPack.getFirstCuttingPage();
    $scope.selectedArea = null;
    $scope.cutAllFiles = 0;

    window.wt = $scope.wt = new WorkTable("#worktable", "#worktable_menu", function (selectedArea) {
        $scope.selectedArea = selectedArea;
        if (selectedArea) {
            // console.log('Selected area box: ' + selectedArea.sizeX + selectedArea.units + ' x ' + selectedArea.sizeY + selectedArea.units);
        }
        setTimeout(function () {
            $scope.$digest();
        }, 100);
    });
    $scope.setStats = function (stats) {
        // console.log(stats);
        $scope.$emit('loadSvg',false);
    };

    $scope.nextStep = async function () {
        $scope.setShowProgressTop(1);
        setTimeout(function () {
            $scope.saveCurrentPage().then(function () {
                console.log('save current page');
                if ($scope.cuttingPack.hasSeveralPages()) {
                    let viewNextPage = $scope.cuttingPack.getNextViewPage();
                    if (viewNextPage) {
                        $scope.currentPackPage = viewNextPage;
                        $scope.reOpenFile($scope.currentPackPage.uuid);
                        $scope.setShowProgressTop(0);
                        return;
                    }

                }
                $scope.setShowProgressTop(0);
                if (!$scope.cuttingPack.hasActivePackParts()) {
                    new TS.Notify({
                        type: 'error',
                        text: _t('site.cutting', 'Please, mark the lines you want to cut or engrave'),
                        automaticClose: true
                    });
                } else {
                    return $scope.$parent.nextStep();
                }
            }).catch(function () {
                $scope.setShowProgressTop(0);
            });
        }, 100);
    };

    $scope.saveCurrentPage = function () {
        return new Promise((resolve, reject) => {
            let currentPage = $scope.currentPackPage;
            $scope.wt.export().then(function (exportInfo) {
                if (!exportInfo['info']['blocks'].length) {
                    if (currentPage.isConfirmedCutAll === 0) {
                        resolve();
                        return;
                    }
                    if (currentPage.isConfirmedCutAll === 1) {
                        $scope.wt.export(true).then(function (exportInfo) {
                            currentPage.importPartsWt(exportInfo);
                            $scope.saveCurrentPackFileParts(currentPage).then(function () {
                                resolve();
                            });
                        });
                        return;
                    }
                    if ((currentPage.isConfirmedCutAll === null) || (typeof currentPage.isConfirmedCutAll === 'undefined')) {
                        let dismissText = _t('site.order', 'Skip');
                        let confirmText = _t('site.order', 'Yes, cut all lines');
                        let cutAllConfirm = function () {
                            $scope.currentPackPage.isConfirmedCutAll = 1;
                            $scope.wt.export(true).then(function (exportInfo) {
                                currentPage.importPartsWt(exportInfo);
                                $scope.saveCurrentPackFileParts(currentPage).then(function () {
                                    resolve();
                                });
                            });
                        };

                        if ($scope.cutAllFiles===1) {
                            confirmText = _t('site.order', 'Yes, cut all remaining files');
                        }
                        if ($scope.cutAllFiles===2) {
                            cutAllConfirm();
                        } else {
                            TS.confirm(
                                _t('site.cutting', 'The lines will be cut all the way through. If you want some lines to be engraved instead, select them manually.'),
                                function (btn) {
                                    if (btn === 'ok') {
                                        $scope.cutAllFiles++;
                                        cutAllConfirm();
                                    } else {
                                        $scope.currentPackPage.isConfirmedCutAll = 0;
                                        currentPage.cuttingPackParts = [];
                                        $scope.saveCurrentPackFileParts(currentPage).then(function () {
                                            resolve();
                                        });
                                    }
                                },
                                {
                                    confirm: confirmText,
                                    dismiss: dismissText,
                                    canClose: false
                                }
                            );
                            return;
                        }
                    }
                } else {
                    $scope.currentPackPage.isConfirmedCutAll = 0;
                }
                currentPage.importPartsWt(exportInfo);
                $scope.saveCurrentPackFileParts(currentPage).then(function () {
                    resolve();
                });
            });
        });
    };

    $scope.reOpenFile = function (uuid, forceOriginal) {
        $scope.wt.cleanUp();
        $scope.$emit('loadSvg',true);
        $scope.wt.openFile($router.getAjaxCuttingSvgDownload(uuid, forceOriginal), $scope.setStats);
    };

    $scope.resetState = function () {
        $scope.reOpenFile($scope.currentPackPage.uuid, true);
        $scope.currentPackPage.resetParts();
    };

    $scope.saveCurrentPackFileParts = function (cuttingPackPage) {
        let page = cuttingPackPage.toJSON();
        return $http.post($router.getAjaxCuttingSaveParts(), {
            'cuttingPackPage': page
        })
            .then(function (response) {
                $("#offers-navigation").trigger('updateOffersList');
            })
            .catch($notifyHttpErrors);
    };

    $scope.changeCurrentCuttingPage = function (item) {
        let oldPage = $scope.currentPackPage;
        $scope.saveCurrentPage().then(function () {
            $scope.currentPackPage = item;
            $scope.reOpenFile(item.uuid);
            $scope.setShowProgressTop(0);
        }).catch(function () {
            // Cancel change - do nothing
        })
    };

    if ($scope.currentPackPage) {
        $scope.reOpenFile($scope.currentPackPage.uuid);
    }

}]);