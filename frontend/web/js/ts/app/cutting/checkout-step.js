"use strict";

app.controller('checkout-step', ['$scope', '$router', '$http', '$notifyHttpErrors', '$notify', 'controllerParams', '$sce', '$compile', function ($scope, $router, $http, $notifyHttpErrors, $notify, controllerParams, $sce, $compile) {
    /**
     * @type {PayOrder|null}
     */
    $scope.payOrder = null;

    /**
     * @type {string|null}
     */
    $scope.cardHtml = null;


    $scope.paymentData = {
        selectedMethod: controllerParams['selectedMehtod']
    };

    /**
     * @type {{code: null}}
     */
    $scope.promocodeData = {
        code: null
    };

    $scope.getPayOrderData = function () {
        $http.get($router.getAjaxCuttingPayOrder())
            .then(function (response) {
                $scope.loadPayOrderResponse(response);
            });
    };

    /**
     * @returns {PayOrder|PaymentInvoice}
     */
    $scope.hasBillingDetails = function () {
        return $scope.payOrder
            && $scope.payOrder.primaryPaymentInvoice
            && $scope.paymentData.selectedMethod !== null
    };

    /**
     * @returns {PayOrder|objects}
     */
    $scope.hasAddress = function () {
        return $scope.payOrder && $scope.payOrder.shipAddress
    };

    /**
     * @param method
     */
    $scope.choosePaymentMethod = function (method) {
        $scope.paymentData.selectedMethod = method;

        return $http.post($router.getAjaxCuttingBillingDetailsPrint(), {vendor: $scope.paymentData.selectedMethod}).then(function (response) {
            $scope.payOrder.primaryPaymentInvoice.load(response.data.PaymentInvoice);
        }).catch(function (response) {
            $notifyHttpErrors(response);
        });
    };

    /**
     * @param response
     */
    $scope.loadPayOrderResponse = function (response) {
        $scope.payOrder = new PayOrder(response.data.payOrder);
        $scope.payOrder.shipAddress.address = $sce.trustAsHtml($scope.payOrder.shipAddress.address);

        if (response.data.checkoutCard) {
            $scope.cardHtml = response.data.checkoutCard;
        }
    };

    /**
     * @param method
     * @returns {boolean}
     */
    $scope.isSelectedPaymentMethod = function (method) {
        return $scope.paymentData.selectedMethod === method
    };

    $scope.getTestData = function () {
        console.log($scope.payOrder);
        console.log($scope.paymentData);
    };

    /**
     * Start page
     */
    $scope.init = function () {
        $(window).scrollTop(0);
        $scope.getPayOrderData();
        setTimeout(function(){
            $scope.checkoutValidatedStatus.validated = false;
        },100);
    };

    $scope.init();
}]);