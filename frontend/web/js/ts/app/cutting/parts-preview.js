"use strict";

app.controller('parts-preview', ['$scope', '$router', '$http', '$timeout', 'controllerParams', function ($scope, $router, $http, $timeout, controllerParams) {
    $scope.cuttingPack;
    $scope.selectedPartUuid;
    $scope.activeParts = $scope.cuttingPack.getActivePackParts();

    $scope.viewPrev = function () {
        var prev = null;
        var last = null;
        for (var partKey in $scope.activeParts) {
            if (!$scope.activeParts.hasOwnProperty(partKey)) continue;
            var part = $scope.activeParts[partKey];
            last = part;
        }
        for (var partKey in $scope.activeParts) {
            if (!$scope.activeParts.hasOwnProperty(partKey)) continue;
            var part = $scope.activeParts[partKey];
            if (part.uuid == $scope.selectedPartUuid) {
                break;
            }
            prev = part;
        }
        if (prev) {
            $scope.setSelectedCuttingPartUuId(prev.uuid);
        } else {
            $scope.setSelectedCuttingPartUuId(last.uuid);
        }

    };
    $scope.viewNext = function () {
        var prev = null;
        var first = null;
        var next = null;
        for (var partKey in $scope.activeParts) {
            if (!$scope.activeParts.hasOwnProperty(partKey)) continue;
            var part = $scope.activeParts[partKey];
            if (prev && prev.uuid == $scope.selectedPartUuid) {
                next = part;
                break;
            }
            prev = part;
            if (!first) {
                first = part;
            }
        }
        if (next) {
            $scope.setSelectedCuttingPartUuId(next.uuid);
        } else {
            $scope.setSelectedCuttingPartUuId(first.uuid);
        }
    };
    $scope.setSelectedCuttingPartUuId = function (uuid) {
        $scope.selectedPartUuid = uuid;
    };

    $scope.updateVisibleElement = function () {

    };

    $scope.$on('cuttingPartsPreview:modalOpen', function () {
        if (!$scope.selectedPartUuid) {
            $scope.selectedPartUuid = $scope.activeParts[0]['uuid'];
        }
        $scope.$digest();
    });

}]);
