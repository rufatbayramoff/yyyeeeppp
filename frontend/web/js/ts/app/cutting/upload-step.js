"use strict";

app.controller('upload-step', ['$scope', '$timeout', '$router', '$rootScope', '$http', '$notifyHttpErrors', 'controllerParams', function ($scope, $timeout, $router, $rootScope, $http, $notifyHttpErrors, controllerParams) {
    $scope.fullScreen(0);
    $scope.cancelUploadItem = function (uploadFileInfo) {
        if ((typeof uploadFileInfo.uploadItem) !== 'undefined') {
            uploadFileInfo.uploadItem.cancel();
        }

        uploadFileInfo.isCanceled = true;

        var data = {
            'uid': uploadFileInfo.id,
            'fileMd5NameSize': uploadFileInfo.fileMd5NameSize
        };
        $http.post($router.getAjaxCuttingPackCancel(), data)
            .then(function (response) {

            })
            .catch($notifyHttpErrors);

        $scope.offersBundleData.updatePrintersTextureSetted = false;
        $scope.offersBundleData.activeOfferItemId = -1;
    };

    $scope.pingPartsFlag = null;

    $scope.pingPartsStatus = function () {
        if ($scope.pingPartsFlag) {
            return;
        }
        let checkJobsRequest = function () {
            if ($scope.cuttingPack.isNeedPing()) {
                $scope.pingPartsFlag = true;
                $http.get($router.getAjaxPingCuttingPack()).then(function (response) {
                        if (response.data && response.data.success == true) {
                            $scope.cuttingPack.update(response.data.cuttingPack);
                            setTimeout(
                                checkJobsRequest, 1000
                            );
                        }
                    }
                )
            } else {
                $scope.pingPartsFlag = false;
            }
        };
        checkJobsRequest();
    };

    $scope.restoreUploadItem = function (uploadFileInfo) {
        uploadFileInfo.isCanceled = false;
        var data = {
            'uid': uploadFileInfo.id,
        };
        $http.post($router.getAjaxCuttingPackRestore(), data)
            .then(function (response) {
            })
            .catch($notifyHttpErrors);
    };

    $scope.createCuttingPack = function (addFileFunction) {
        return $http.post($router.getAjaxCreateCuttingPack()).then(function (response) {
                if (response.data && response.data.success == true) {
                    $scope.cuttingPack.load(response.data.cuttingPack);
                } else {
                    new TS.Notify({
                        type: 'error',
                        text: _t('site.printModel3d', 'Failed to load cut model. Please try to refresh the page.'),
                        target: '.messageBox',
                        automaticClose: true
                    });
                    return $q.reject();
                }
            }
        )
    };

    $scope.updateCuttingFileQty = function (cuttingPackFile) {
        if (cuttingPackFile.qty === '' || cuttingPackFile.qty === null) {
            return;
        }
        $scope.offersBundleData.updatePrintersTextureSetted = false;
        $scope.offersBundleData.activeOfferItemId = -1;
        var data = {
            'uid': cuttingPackFile.id,
            'qty': cuttingPackFile.qty
        };
        return $http.post($router.getAjaxCuttingChangeFileQty(), data)
            .then(function (response) {
                $("#offers-navigation").trigger('updateOffersList');
            })
            .catch($notifyHttpErrors);
    };

    $scope.uploaderConfig = {
        'existsUid': $scope.cuttingPack.id,
        'allowedExtensions': controllerParams['upload.allowedExtensions'].join(',.'),
        'fileAdded': function () {
            $scope.offersBundleData.updatePrintersTextureSetted = false;
            $scope.offersBundleData.activeOfferItemId = -1;
        },
        'uploadFileUrl': $router.getAjaxCuttingPackUpload(),
        'cancelUploadItem': $scope.cancelUploadItem,
        'getItemByMd5NameSize': function (uuid) {
            return $scope.cuttingPack.getItemByMd5NameSize(uuid);
        },
        'restoreUploadItem': $scope.restoreUploadItem,
        'applyChanges': function () {
            setTimeout(function () {
                $scope.$digest();
            }, 0);
        },
        'modelImage': CuttingPackFile,
        'modelPart': CuttingPackFile,
        'pushImage': function (imageInfo) {
            $scope.cuttingPack.addImage(imageInfo);
        },
        'pushPart': function (partInfo) {
            $scope.cuttingPack.addPart(partInfo);
        },
        'completeCallback': function (success, data, status, file) {
            if (success) {
                if ($scope.uploader.isArchiveFile(file)) {
                    $router.setUrl($router.getCuttingPackUpload());
                    return;
                }
                $scope.cuttingPack.update(data['cuttingPack']);
                $scope.pingPartsStatus();
            } else {
                if (status == 418) {
                    $router.setUrl($router.getCuttingPackUpload());
                    return;
                }
            }
            setTimeout(function () {
                $scope.$digest();
            }, 0);
        },
        'modelCreated': function () {
            return $scope.cuttingPack.id;
        },
        'modelCreatePromise': $scope.createCuttingPack,
    };
    $scope.uploader = new CommonUploadPage($scope.uploaderConfig);

    if ($scope.cuttingPack.isNeedPing()) {
        $scope.pingPartsStatus();
    }

}]);
