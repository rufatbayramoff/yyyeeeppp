app.controller('ProductCategorySelectController', function ($scope, $rootScope, $notify, $notifyHttpErrors, $http, $router, $timeout, $q, controllerParams) {

    $scope.validateText = '';
    $scope.categorySelect.productRootCategories = null;
    $scope.categorySelect.productSubCategories = null;
    $scope.categorySelect.productSubSubCategories = null;

    $scope.categorySelect.mainCategoryId = null;
    $scope.categorySelect.subCategoryId = null;
    $scope.categorySelect.subSubCategoryId = null;
    $scope.categorySelect.currentCategoryId = null;
    $scope.categorySelect.currentCategoryTitle = '';
    $scope.updateShortListTitleTimer = null;


    $scope.init = function (params) {
        $scope.categorySelect.mainCategoryId = params['categorySelect.mainCategoryId'] + '';
        $scope.categorySelect.subCategoryId = params['categorySelect.subCategoryId'] + '';
        $scope.categorySelect.subSubCategoryId = params['categorySelect.subSubCategoryId'] + '';
        $scope.categorySelect.productRootCategories = params['productRootCategories'];
        $scope.categorySelect.productSubCategories = params['productSubCategories'];
        $scope.categorySelect.productSubSubCategories = params['productSubSubCategories'];
        $scope.categorySelect.currentCategoryId = params['categorySelect.currentCategoryId'];
        $scope.categorySelect.currentCategoryTitle = params['categorySelect.currentCategoryTitle'];
        $scope.shortList = {};
    };

    $scope.updateShortList = function (title) {
        $http.get($router.getProductShotCategoryList(title)).then(function (response) {
            $scope.categorySelect.shortList = response.data.categories;
        });
    };

    $rootScope.$on('productCategory.updateTitle', function (event, data) {
        let newTitle = data['title'];
        if ($scope.updateShortListTitleTimer) {
            clearTimeout($scope.updateShortListTitleTimer);
            $scope.updateShortListTitleTimer = null;
        }
        $scope.updateShortListTitleTimer = setTimeout(function () {
            $scope.updateShortList(newTitle);
        }, 1000);
    });


    $scope.updateCategory = function () {
        $scope.categorySelect.subCategoryId = null;
        $scope.categorySelect.subSubCategoryId = null;
        $scope.categorySelect.productSubSubCategories = null;
        $scope.categorySelect.currentCategoryId = null;
        $scope.categorySelect.currentCategoryTitle = '';
        // load subcategories
        $scope.getSubCategories($scope.categorySelect.mainCategoryId).then(function (categories) {
            $scope.categorySelect.productSubCategories = categories;
        });
    };

    $scope.selectCategory = function (category) {
        $scope.setCurrentCategory(category.id, category.fullTitle);
    };

    $scope.updateSubcategory = function () {
        var self = this;

        $scope.categorySelect.subSubCategoryId = null;
        $scope.categorySelect.currentCategoryId = null;
        $scope.categorySelect.currentCategoryTitle ='';

        $scope.getSubCategories($scope.categorySelect.subCategoryId).then(function (categories) {
            $scope.categorySelect.productSubSubCategories = categories;
        });
    };

    $scope.update = function () {
        let categoryFullTitle = $scope.getCurrentCategoryLabel();
        $scope.setCurrentCategory($scope.categorySelect.subSubCategoryId, categoryFullTitle);
    };

    $scope.getCurrentCategoryLabel = function () {
        let label = '';
        for (var key in $scope.categorySelect.productRootCategories) {
            if (!$scope.categorySelect.productRootCategories.hasOwnProperty(key)) {
                continue;
            }
            let category = $scope.categorySelect.productRootCategories[key];
            if (category.id == $scope.categorySelect.mainCategoryId) {
                label += category.title + ' \\ ';
                break;
            }
        }

        for (var key in $scope.categorySelect.productSubCategories) {
            if (!$scope.categorySelect.productSubCategories.hasOwnProperty(key)) {
                continue;
            }
            let category = $scope.categorySelect.productSubCategories[key];
            if (category.id == $scope.categorySelect.subCategoryId) {
                label += category.title + ' \\ ';
                break;
            }
        }

        for (var key in $scope.categorySelect.productSubSubCategories) {
            if (!$scope.categorySelect.productSubSubCategories.hasOwnProperty(key)) {
                continue;
            }
            let category = $scope.categorySelect.productSubSubCategories[key];
            if (category.id == $scope.categorySelect.subSubCategoryId) {
                label += category.title;
                break;
            }
        }
        return label;
    };

    $scope.setCurrentCategory = function (categoryId, categoryFullTitle) {
        $scope.categorySelect.currentCategoryId = categoryId;
        $scope.categorySelect.currentCategoryTitle = categoryFullTitle;
        if ($scope.changeCategory) {
            $scope.changeCategory(categoryId, categoryFullTitle);
        }
    };

    $scope.getSubCategories = function (categoryId) {
        var deferred = $q.defer();
        return $http.get('/mybusiness/products/product/sub-categories/', {params: {categoryId: categoryId}}).then(function (response) {
                if (response.data && response.data.success == true) {
                    deferred.resolve(response.data.categories);
                } else {
                    new TS.Notify({
                        type: 'error',
                        text: _t('site.product', 'Failed to load categories. Please try to refresh the page.'),
                        automaticClose: true
                    });
                    deferred.reject();
                }
                return deferred.promise;
            }
        ).catch($notifyHttpErrors);
    };

    var hasOver = false;

    $scope.updateColumnWidth = function (event, flag) {
        let el = event.currentTarget;

        $(el).parent().find('div').removeClass('col-md-4');
        $(el).parent().find('div').addClass('col-md-3');
        if (flag) {
            hasOver = true;
            $(el).removeClass('col-md-3'); //, 'col-md-6', 500, 'easeInOutQuad');
            $(el).addClass('col-md-6');
        } else {
            hasOver = false;
            $(el).removeClass('col-md-6');

            $(el).parent().find('div').removeClass('col-md-3');
            $(el).parent().find('div').addClass('col-md-4');
        }

    };

    $scope.updateColumnWidth2 = function (event, flag) {
        let el = event.currentTarget;
        $(el).parent().find('div').switchClass('col-md-4', 'col-md-3', 1);

        if (flag) {
            hasOver = true;
            $(el).switchClass('col-md-3', 'col-md-6', 500);
        } else {
            hasOver = false;
            $(el).switchClass('col-md-6', 'col-md-4', 1);
            $(el).parent().find('div').switchClass('col-md-3', 'col-md-4', 500);
        }

    };

    $scope.validate = function () {
        if (!$scope.categorySelect.currentCategoryId) {
            $scope.validateText = _t('site.product', 'Please, select category in right panel');
            $timeout(function () {
                $scope.validateText = '';
            }, 1500);
            return false;
        }
        return true;
    };

    $scope.cancel = function () {
        $scope.$dismiss();
    };

    $scope.submit = function () {
        if (!$scope.validate()) {
            return;
        }
        if ($scope.changeCategoryOnSubmit) {
            let label = $scope.getCurrentCategoryLabel();
            $scope.changeCategoryOnSubmit($scope.categorySelect.currentCategoryId, label);
        }
        $scope.$dismiss();
    };

    $scope.init(controllerParams);
});
