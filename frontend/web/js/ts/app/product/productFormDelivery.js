"use strict";
app.controller('ProductFormDeliveryController',
    ['$scope', '$user', '$router', '$http', '$notify', '$geo', '$notifyHttpErrors', 'controllerParams',
        function ($scope, $user, $router, $http, $notify, $geo, $notifyHttpErrors, controllerParams) {

            $scope.product = $scope.$parent.product;
            $scope.$geo = $geo;
        }
    ]
);
