"use strict";
app.controller('ProductFormController', ['$scope', '$router', '$http', '$notify', '$notifyHttpErrors', 'controllerParams', function ($scope, $router, $http, $notify, $notifyHttpErrors, controllerParams) {
    $scope.product = controllerParams['product'];


    $scope.clickAllowExpressDelivery = function()
    {
        var isChecked = $('#allow-express-delivery').prop('checked');
        $scope.setAllowExpressDelivery(isChecked);
    };

    $scope.setAllowExpressDelivery = function(isAllowDelivery) {
        if (isAllowDelivery) {
            $('#exress-delivery-form').show();
        }else {
            $('#exress-delivery-form').hide();
        }
    };

    $scope.clickAllowWholesaleDelivery = function()
    {
        var isChecked = $('#allow-wholesale-delivery').prop('checked');
        $scope.setAllowWholesaleDelivery(isChecked);
    };

    $scope.setAllowWholesaleDelivery = function(isAllowDelivery) {
        if (isAllowDelivery) {
            $('#wholesale-delivery-form').show();
        }else {
            $('#wholesale-delivery-form').hide();
        }
    };
}]);