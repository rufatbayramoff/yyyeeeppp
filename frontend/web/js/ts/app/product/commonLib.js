

function arrayCombine(keys, values) {
    var newArray = {}, keycount = keys.length, i;
    if (!keys || !values || keys.constructor !== Array || values.constructor !== Array) {
        return [];
    }
    if (keycount != values.length) {
        return [];
    }
    for (i = 0; i < keycount; i++) {
        newArray[keys[i]] = values[i];
    }
    return newArray;
}

function detectProperties(lines) {
    var result = [];
    var delimeter = findPropertyDelimeter(lines);
    var prevKey = null;
    if (lines.length == 2 && lines[0].split(delimeter).length > 2) {
        var keys = lines[0].split(delimeter);
        var vals = lines[1].split(delimeter);
        result = arrayCombine(keys, vals);
    } else {
        _.each(lines, function (line) {
            var row = line.split(delimeter);
            var key = row[0];
            var val = row[1] || false;
            if (val === false && prevKey && result[prevKey] === false) {
                result[prevKey] = key;
                delimeter = "\n";
            } else {
                result[key] = val;
                prevKey = key;
            }
        });
    }
    return result;
}

function findPropertyDelimeter(lines) {
    var delimeters = [':', '-', "\t", ',', ';', "\n"];
    var found = [];
    var maxVal = 0;
    var maxChar = null;
    _.each(lines, function (line) {
        _.each(delimeters, function (del) {
            var c = found[del] || 0;
            found[del] = c + (line.split(del).length - 1);
            if (found[del] > maxVal) {
                maxVal = found[del];
                maxChar = del;
            }
        });
    });
    return maxChar;
}

function UserDefinedProperties(data) {
    this.customProperties = [];
    angular.extend(this, data);

    this.addProductUserField = function () {
        var position = this.customProperties.length + 1;
        this.customProperties.push({title: "", value: "", position: position});
    };

    this.deleteProductUserField = function (userField) {
        var i = this.customProperties.indexOf(userField);
        this.customProperties.splice(i, 1);
    };

    this.productUserFieldMove = function (userField, direction) {
        this.customProperties.sort(function (a, b) {
            if (a.position < b.position) return -1;
            if (a.position > b.position) return 1;
            return 0;
        });
        var swithDone = false, switchWithNext = false, switchField = null;
        this.customProperties.forEach(function (i) {
            if (swithDone) {
                return;
            }
            if (userField == i) {
                if (direction == 'down') {
                    switchWithNext = true;
                } else {
                    swithDone = true;
                }
                return;
            }
            switchField = i;
            if (switchWithNext) {
                swithDone = true;
            }
        });
        var tmpPos = userField.position;
        if (switchField) {
            userField.position = switchField.position;
            switchField.position = tmpPos;
        }
    }
}