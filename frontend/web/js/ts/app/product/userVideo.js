

/**
 * @property {string} url
 * @property {string} thumb
 * @param data
 * @constructor
 */
function UserVideo(data) {
    angular.extend(this, data);
    var videoId = getYoutubeVideoId(this.url);
    this.videoId = videoId;
    this.thumb = 'https://img.youtube.com/vi/' + videoId + '/default.jpg';

    function getYoutubeVideoId(url) {
        var videoId = url.split('v=')[1];
        if (!videoId) return null;
        var ampersandPosition = videoId.indexOf('&');
        if (ampersandPosition != -1) {
            videoId = videoId.substring(0, ampersandPosition);
        }
        return videoId;
    }

    this.isValidUrl = function () {
        if (this.videoId) {
            return true;
        }
        return false;
    }
    this.getEmbedHtml = function () {
        // return '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+this.videoId+'?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        return '<object style="width: 560px; height: 315px; float: none; clear: both; margin: 2px auto;" data="https://www.youtube.com/embed/' + this.videoId + '"></object>';
    }
}
