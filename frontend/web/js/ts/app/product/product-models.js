/*
* @property {string}    uuid
* @property {string}    title
* @property {string}    description
* @property {string}    coverUrl
* @property {SiteTag[]} productTags
* @property {SiteTag[]} expressDeliveries
* @property {string}    productType
* @property {string}    productStatusLabel
*/
function Product(data) {
    angular.extend(this, data);
    this.productTags = [];
    for (var key in data.productTags) {
        if (!data.productTags.hasOwnProperty(key)) continue;
        var dataElement = data.productTags[key];
        var element = new SiteTag(dataElement);
        this.productTags.push(element);
    }
    this.expressDeliveries = [];
    for (var key in data.expressDeliveries) {
        if (!data.expressDeliveries.hasOwnProperty(key)) continue;
        var dataElement = data.expressDeliveries[key];
        var element = new ProductExpressDelivery(dataElement);
        this.expressDeliveries.push(element);
    }

}

/**
 * Function stub
 * @param id
 */
Product.prototype.getSomething = function (id) {
    var returnValue = '';
    return returnValue;
};

Product.prototype.addExpressDelivery = function (productExpressDelivery) {
    this.expressDeliveries.push(productExpressDelivery);
};


function SiteTag(data) {
    angular.extend(this, data);
}

/**
 * @property {string}  $uuid
 * @property {integer} $countryId
 * @property {string}  $firstItem
 * @property {string}  $followingItem
 * @property {integer} $isDeleted
 * @constructor
 */
function ProductExpressDelivery(data) {
    angular.extend(this, data);
    if (!this.uuid) {
        this.uuid = uuidHelper.generateUuid();
    }
}

