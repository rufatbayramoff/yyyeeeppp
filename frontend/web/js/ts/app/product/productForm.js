"use strict";
app.controller('ProductFormController',
    ['$scope', '$user', '$router', '$rootScope', '$http', '$notify', '$googleLocationApi', '$notifyHttpErrors', '$q', '$formValidator', 'controllerParams', 'orderByFilter',
        function ($scope, $user, $router, $rootScope, $http, $notify, $googleLocationApi, $notifyHttpErrors, $q, $formValidator, controllerParams, orderBy) {

            var feePercent = parseFloat(controllerParams['feePercent']);
            $scope.product = new Product(controllerParams['product']);

            $scope.categorySelect = {
                isShortList: 1,
                shortList: {}
            };

            $scope.attachments = [];
            if (controllerParams['product']['attachments']) {
                $scope.attachments = controllerParams['product']['attachments'];
            }
            $scope.productPrices = $scope.product.productPrices;
            $scope.isSwitchQuote = $scope.product.switchQuoteQty != null || isNaN($scope.product.switchQuoteQty);
            if ($scope.product.category) {
                $scope.formCategoryId = '' + $scope.product.category.id;
            }

            $scope.updateButtonsText = function () {
                $scope.saveButton = _t('site.product', 'Save');
                $scope.unpublishButtonText = _t('site.product', 'Unpublish');
                if (!$scope.product.isPublished()) {
                    $scope.saveButton = _t('site.product', 'Publish');
                    $scope.unpublishButtonText = _t('site.product', 'Save unpublished');
                }
            };

            $scope.updateButtonsText();

            $scope.$watch('isSwitchQuote', function () {
                var el = document.getElementById('productform-switch_quote_qty');
                if (el)
                    el.disabled = !$scope.isSwitchQuote;
            });

            $scope.isShortTitle = function() {
                if (!$scope.product.title || $scope.product.title.length<2) {
                    return true;
                }
                return false;
            }

            $scope.$watch('product.title', function () {
                if ($scope.isShortTitle()) {
                    return ;
                }

                $rootScope.$emit('productCategory.updateTitle', {
                    title: $scope.product.title
                });
            });


            /**
             * Delete attached file
             * @param attachedFile
             */
            $scope.deleteAttachedFile = function (attachedFile) {
                _.remove($scope.attachments, attachedFile);
            };

            $scope.changeCategory = function (categoryId) {
                var newCategoryId = categoryId || $('#product_category_id').val();
                var dynamicFieldProperties = {
                    'categoryId': newCategoryId,
                    'dynamicFieldValues': $("[name*='ProductForm[dynamicFields]']").serializeArray()
                };
                return $http.post($router.getDynamicFieldCategories(), dynamicFieldProperties)
                    .then(function (response) {
                        $('#productDynamicValues').html(response.data);
                    });
            };

            // product prices
            $scope.addProductPrice = function () {
                var price = {moq: 1};
                $scope.calculatePrice(price);
                $scope.productPrices.push(price);
            };

            $scope.processValidationErrors = function (validationErrors) {
                $notify.error(validationErrors);
                $formValidator.applyValidateById(validationErrors, 'productform-');
            };

            $scope.submitFormData = function ($mode, redir) {
                var el = document.getElementById('productform-moq_prices');
                if (el)
                    document.getElementById('productform-moq_prices').value = angular.toJson($scope.productPrices);
                var el = document.getElementById('productform-custom_properties');

                if (el)
                    el.value = angular.toJson($scope.product.customProperties.customProperties);
                el = document.getElementById('productform-videos');
                if (el)
                    el.value = angular.toJson($scope.product.videos);

                $formValidator.clearErrors();
                var formData = $("#ProductForm").serializeForm();
                if (!formData) {
                    window.location.href = redir;
                    return;
                }

                if ($scope.product.images && $scope.product.images.length > 0) {
                    formData['ProductForm']['images'] = $scope.product.images;
                } else {
                    if ($mode == 'publish') {
                        $notify.error(_t('site.product', 'Please upload at least one photo'));
                        return;
                    }
                }
                if ($mode) {
                    formData['ProductForm']['submitMode'] = $mode;
                }
                if ($scope.attachments) {
                    formData['ProductForm']['attachments'] = $scope.attachments;
                }

                formData['ProductForm']['imagesOptions'] = [];

                $scope.product.images.forEach(function (item, i) {
                    formData['ProductForm']['imagesOptions'][i] = (typeof item.fileOptions !== "undefined") ? item.fileOptions : [];
                });

                var currentUrl = window.location.href;
                $http.post(currentUrl, formData)
                    .then(function (data) {
                        var answer = data['data'];
                        if (answer['success']) {
                            if ($scope.product.isNew) {
                                $router.setUrl($router.getProductEdit(answer['uid']));
                            } else {
                                if (!redir) {
                                    $notify.success(_t('site.product', 'Product saved'));
                                }
                                if (answer['product']) {
                                    $scope.product.load(answer['product']);
                                    $scope.updateButtonsText();
                                }
                                window.onbeforeunload = null;
                            }
                        }
                        if (answer['productFill']) {
                            document.getElementById('productFillPercent').outerHTML = answer['productFill'];
                        }
                        if (answer['validationErrors']) {
                            $scope.processValidationErrors(answer['validationErrors']);
                        }
                        if (redir) {
                            window.location.href = redir;
                        }
                        return;
                    })
                    .catch($notifyHttpErrors);
            };

            $scope.submitForm = function ($mode, redir) {
                if ($mode == 'unpublish' && !$scope.product.isNew) {
                    TS.confirm(_t('site.product', 'Are you sure you want to save unpublished?'),
                        function (btn) {
                            if (btn === 'ok') {
                                $scope.submitFormData($mode, redir);
                            }
                        },
                        {confirm: _t('site.ps', 'Yes'), dismiss: _t('site.ps', 'No')}
                    );
                    return;
                }
                $scope.submitFormData($mode, redir);
            };

            $scope.deleteProductPrice = function (prodPrice) {
                var i = $scope.productPrices.indexOf(prodPrice);
                $scope.productPrices.splice(i, 1);
            };

            $scope.calculatePrice = function (productPrice) {
                productPrice.priceWithFee = Math.round(productPrice.priceWithFee * 100, 0) / 100;
                var priceWithFee = parseFloat(productPrice.priceWithFee);
                productPrice.price = priceWithFee - Math.floor10((feePercent * 100 * priceWithFee / (100 + feePercent * 100)), -2);

            };
            $scope.sortPrices = function (e, productPrice) {
                if (productPrice.moq <= 0) {
                    $notify.error(_t('site.product', 'Quantity must be greater than 0'));
                    e.target.focus();
                    return;
                }
                $scope.productPrices = orderBy($scope.productPrices, 'moq', false);
            }

            $scope.calculatePriceWithFee = function (productPrice) {
                var price = parseFloat(productPrice.price);
                productPrice.priceWithFee = price + Math.floor10(price * feePercent, -2);
            };

            $scope.$watchCollection('product.images', function (newNames, oldNames) {
                _.each($scope.product.images, function (file) {
                    if (!(file instanceof File)) {
                        return;
                    }
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        file.src = e.target.result;
                        $scope.$apply();
                    };
                    reader.readAsDataURL(file);
                });
            });

            $scope.removeImage = function (file) {
                var picture = $scope.product.images.indexOf(file);
                $scope.product.images.splice(picture, 1);
            };

            if ($scope.productPrices.length === 0 && $scope.product.isNew) {
                $scope.addProductPrice();
            }
            // init
            $googleLocationApi.init();

            // custom Properties
            $scope.addProductUserField = function () {
                var position = $scope.customProperties ? $scope.customProperties.length + 1 : 1;
                $scope.customProperties.push({title: "", value: "", position: position});
            };

            $scope.deleteProductUserField = function (userField) {
                var i = $scope.customProperties.indexOf(userField);
                $scope.customProperties.splice(i, 1);
            };

            $scope.productUserFieldMove = function (userField, direction) {
                $scope.customProperties.sort(function (a, b) {
                    if (a.position < b.position) return -1;
                    if (a.position > b.position) return 1;
                    return 0;
                });
                var swithDone = false, switchWithNext = false, switchField = null;
                $scope.customProperties.forEach(function (i) {
                    if (swithDone) {
                        return
                    }
                    if (userField == i) {
                        if (direction == 'down') {
                            switchWithNext = true;
                        } else {
                            swithDone = true;
                        }
                        return;
                    }
                    switchField = i;
                    if (switchWithNext) {
                        swithDone = true;
                    }
                });
                var tmpPos = userField.position;
                if (switchField) {
                    userField.position = switchField.position;
                    switchField.position = tmpPos;
                }
            };

            $scope.propsDump = '';

            $scope.importProperties = function () {
                var lines = $scope.propsDump.trim().split("\n");
                var result = detectProperties(lines);
                for (var key in result) {
                    if (!result.hasOwnProperty(key) || key === '') {
                        continue;
                    }
                    var position = $scope.product.customProperties.customProperties.length + 1;
                    $scope.product.customProperties.customProperties.push({
                        title: key,
                        value: result[key],
                        position: position
                    });
                }
                $scope.propsDump = '';
            };

            $scope.showAllCategories = function () {
                $scope.categorySelect.isShortList = 0;
            };

            // init buttons
            $('#productFormControllerEl li > a').on('click', function (e, a) {
                e.preventDefault();
                angular.element(document.getElementById('productFormControllerEl')).scope().submitForm('publish', e.currentTarget.href);
                return false;
            });

        }
    ]
);