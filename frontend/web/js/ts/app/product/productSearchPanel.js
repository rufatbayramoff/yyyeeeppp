"use strict";
app.controller('ProductSearchPanelController',
    ['$scope', '$user', '$router', '$http', '$notify', '$notifyHttpErrors', '$location', '$q', 'controllerParams',
        function ($scope, $user, $router, $http, $notify, $notifyHttpErrors, $location, $q, controllerParams) {
            $scope.searchParams = controllerParams['searchParams'];
            $scope.categoryCode = controllerParams['categoryCode'];

            var productRoute = $router.getProductsCatalogList();
            if (controllerParams['productSearchRoute']) {
                productRoute = controllerParams['productSearchRoute'];
            }
            $scope.updateSearchParams = function () {
                if ($('#listsort').val()) {
                    $scope.searchParams['listSort'] = $('#listsort').val();
                } else {
                    delete $scope.searchParams['listSort'];
                }
                if ($('#searchText input[name ="search"]').val()) {
                    $scope.searchParams['search'] = $('#searchText input[name ="search"]').val();
                } else {
                    delete $scope.searchParams['search'];
                }
            };

            $scope.fixLoading = function () {
                $('#loadingProgress').removeClass('hidden');
                $('body').addClass('bodyCursorWait');
            };
            $scope.fixLoaded = function () {
                $('#loadingProgress').addClass('hidden');
                $('body').removeClass('bodyCursorWait');
            };
            $scope.fixUrlHistory = function () {
                var search = {};
                for (var key in $scope.searchParams) {
                    if (!$scope.searchParams.hasOwnProperty(key)) continue;
                    search[key] = $scope.searchParams[key];
                }
                $location.search(search);
            };

            var canceler = $q.defer();


            $scope.submitSearch = function () {
                // Cancel prev request;
                canceler.resolve();
                canceler = $q.defer();

                $scope.updateSearchParams();
                $scope.fixLoading();
                $scope.fixUrlHistory();
                let productsUrl = '';
                if($scope.categoryCode) {
                    productsUrl = $router.getProductsList()
                        + ($scope.categoryCode ? '/' + $scope.categoryCode : '');
                } else {
                    productsUrl = productRoute
                        + ($scope.categoryCode ? '/' + $scope.categoryCode : '');
                }
                let addParams = ''
                    + ($scope.searchParams['listSort'] ? 'listSort=' + $scope.searchParams['listSort'] : '')
                    + ($scope.searchParams['search'] ? 'search=' + encodeURIComponent($scope.searchParams['search']) : '');
                if (addParams) {
                    productsUrl += '?' + addParams;
                }

                $http.get(productsUrl, {timeout: canceler.promise}).then(function (response) {
                    setTimeout(function() {
                        history.pushState({}, '', productsUrl.replace(/%20/g, "+"));
                    }, 100);
                    $scope.fixLoaded();
                    $('#productsList').html(response.data.htmlBlock);
                }).catch(function (response) {
                    if (response.status !== -1) {
                        new TS.Notify({
                            type: 'error',
                            text: response.message || 'Error loading products',
                            automaticClose: false
                        });
                        $scope.fixLoaded();
                    }
                });
            };

            $scope.changeSortDirection = function () {
                $scope.searchParams['sortDirection'] = $scope.searchParams['sortDirection'] ? 0 : 1;
                $('#sortDirectionIcon').removeClass('tsi-arrow-up-l').removeClass('tsi-arrow-down-l');
                if ($scope.searchParams['sortDirection']) {
                    $('#sortDirectionIcon').addClass('tsi-arrow-up-l');
                } else {
                    $('#sortDirectionIcon').addClass('tsi-arrow-down-l');
                }
                $('#sortDirection').val($scope.searchParams['sortDirection']);
                if (!$scope.searchParams['sortDirection']) {
                    delete $scope.searchParams['sortDirection'];
                }

                $scope.submitSearch();
            };

            $scope.changeCategory = function (categoryCode) {
                var url = new URI($router.getProductsList() + (categoryCode ? '/' + categoryCode : ''));
                url.setSearch($scope.searchParams);
                window.location.href = url.toString();
            };

            function subscribeOnChange() {
                // For old style elements
                $('#listsort').on('change', function () {
                        $scope.submitSearch();
                    }
                );
                $('#searchText input[name ="search"]').on('change', function () {
                    $scope.submitSearch();
                });
                $('#searchPanel').on('submitSearch', function (event, searchValues) {
                    //console.log('submitSearch');
                    //console.log(searchValues);
                    for (var key in searchValues) {
                        if (!searchValues.hasOwnProperty(key)) continue;
                        $scope.searchParams[key] = searchValues[key];
                    }
                    $scope.submitSearch();
                });

                $('#searchPanel').on('typeahead:selected', function (e, datum) {
                    $scope.submitSearch();
                });

                $('#searchPanel').on('dfEnum', function (event, fieldCode) {
                    var values = [];
                    $('input[name="dynamicFields[' + fieldCode + ']"]').each(function () {
                        if ($(this).is(':checked')) {
                            values.push($(this).val())
                        }
                    });
                    var keyValue = "df_" + fieldCode;
                    var valueStr = values.join(',');

                    var event = [];
                    var eventParam = {};
                    eventParam[keyValue] = valueStr;
                    event.push(eventParam);

                    $("#searchPanel").trigger("submitSearch", event);
                });

            }

            subscribeOnChange();

        }
    ]
);