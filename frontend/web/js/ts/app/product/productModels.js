"use strict";

/**
 * @property {string} uuid
 * @property {string} title
 * @property {string} coverUrl
 * @property {string} description
 * @property {string} productStatusLabel
 * @property {array} productTags
 * @property {array} images
 * @property {number} single_price
 * @property {number} feePercent
 * @property {string} unitType
 * @property {ProductPrice[]} productPrices
 * @property {array} customProperties
 * @property {array} productCertifications
 * @property {ProductCategory} category
 * @param data
 * @constructor
 */
function Product(data) {
    this.load(data);
}

Product.prototype.load = function (data) {
    angular.extend(this, data);
    if (data['category']) {
        this.category = new ProductCategory(data['category']);
    }
    if (typeof(this.videos) === 'undefined' || !this.videos) {
        this.videos = [];
    }
    if (data['videos']) {
        this.videos = this.videos.map(function (raw) {
            return new UserVideo(raw);
        });
    }
    if (typeof(this.images) === 'undefined' || !this.images) {
        this.images = [];
    }
    if (data['images']) {
        this.images = this.images.map(function (raw) {
            return new ProductImage(raw);
        });
    }

    this.productExpressDelivery = [];
    if (data['expressDeliveries']) {
        this.productExpressDelivery = this.productExpressDelivery.map(function (raw) {
            return new ProductExpressDelivery(raw);
        });
    }

    if (typeof(this.customProperties) === 'undefined' || !this.customProperties) {
        this.customProperties = new UserDefinedProperties([]);
    }else {
        this.customProperties = new UserDefinedProperties({'customProperties': data['customProperties']});
    }

    if (data['productPrices']) {
        this.productPrices = data['productPrices'].map(function (raw) {
            return new ProductPrice(raw);
        });
    }
    if (typeof(this.productPrices) === 'undefined' || !this.productPrices) {
        this.productPrices = [];
    }
    if (data['deliveryDetails']) {
        this.deliveryDetails = new ProductDeliveryDetails(data['deliveryDetails']);
    }
    if (data['deliveryDetails']) { //     ProductPrice
        this.deliveryDetails = new ProductDeliveryDetails(data['deliveryDetails']);
    }

    this.addVideo = function (link) {
        var video = new UserVideo({url: link});
        if (video.isValidUrl()) {
            var isDublicated = this.videos.filter(function (item) {
                return item.url === link;
            })[0];
            if (!isDublicated)
                this.videos.push(video);
        }
        link = null;

        window.onbeforeunload = function () {
            if (document.getElementById('productform-videos').value == '') {
                return "Changes you made may not be saved.";
            }
            return true;
        }
    }
    this.removeVideo = function (index) {
        this.videos.splice(index, 1);
    }
    this.showVideoPreview = function (video, el) {
        document.getElementById(el).innerHTML = video.getEmbedHtml();
    }
};
Product.prototype.getUnitTypeByPrice = function(qty){
    var t = this.unitType;
    if(qty > 1){
        t = t  + 's';
    }
    return t;
}
Product.prototype.getTotalPrice = function (qty) {
    // Reverse sort productPrice by moq
    var productPrices = commonJs.clone(this.productPrices);
    productPrices =productPrices.sort(function(a, b) {return a.moq<b.moq});
    var maxPrice = 9999999999999999;
    var minPrice = maxPrice;
    for (var key in productPrices) {
        if (!productPrices.hasOwnProperty(key)) continue;
        var productPrice = productPrices[key];
        if (productPrice.moq<=qty && productPrice.priceWithFee<minPrice) {
            minPrice = productPrice.priceWithFee*qty;
            break;
        }
    }
    if (minPrice==maxPrice) {
        return '';
    }
    return minPrice;
};

Product.prototype.isPublished = function () {
    var publicStatuses = [
        'published_public',
        'publish_pending',
        'published_updated',
        'published_directly'
    ];
    if (publicStatuses.indexOf(this.productStatus) > -1) {
        return true;
    }
    return false;
};


/**
 * @property {string} uuid
 * @property {string} src
 * @property {string} fileUrl
 * @property {string} cssClass
 */
function ProductImage(data) {
    angular.extend(this, data);
}

/**
 * @property {integer} id
 * @property {string} code
 * @property {string} title
 */
function ProductCategory(data) {
    angular.extend(this, data);
}

/**
 * @property {string} incoterms
 * @property {string} shipFrom
 * @param data
 * @constructor
 */
function ProductDeliveryDetails(data) {
    angular.extend(this, data);
}

/**
 * @property {integer} moq
 * @property {float} price
 * @property {float} priceWithFee
 * @param data
 * @constructor
 */
function ProductPrice(data) {
    angular.extend(this, data);
}

ProductPrice.prototype.priceWithFormatted = function () {
    return '$' + this.priceWithFee;
}


/**
 * @property {string} country
 * @property {number} firstItemPrice
 * @property {number} followingItemPrice
 * @param data
 * @constructor
 */
function ProductExpressDelivery(data) {
    angular.extend(this, data);
}


/**
 * @property {Product[]} products
 *
 * @param data
 * @constructor
 */
function ProductsList(data) {
    this.products = data.map(function (raw) {
        return new Product(raw);
    });
}

/**
 *
 * @param uuid
 * @returns {Product|null}
 */
ProductsList.prototype.getProductByUuid = function (uuid) {
    for (var key in this.products) {
        if (!this.products.hasOwnProperty(key)) continue;
        var product = this.products[key];
        if (product.uuid == uuid) {
            return product;
        }
    }
    return null;
}


/**
 * @property {CompanyService[]} services
 *
 * @param data
 * @constructor
 */
function CompanyServicesList(data) {
    this.services = data.map(function (raw) {
        return new CompanyService(raw);
    });
}

CompanyServicesList.prototype.getServiceById = function (id) {
    for (var key in this.services) {
        if (!this.services.hasOwnProperty(key)) continue;
        var service = this.services[key];
        if (service.id == id) {
            return service;
        }
    }
    return null;
}

/**
 * @property {string} title
 * @property {string} description
 * @property {objects} images
 *
 * @param data
 * @constructor
 */
function CompanyService(data) {
    angular.extend(this, data);


}

/**
 * @returns {string|null}
 */
CompanyService.prototype.getCoverUrl = function () {
    if (this.images && !this.isVideo && this.images.length > 0) {
        return this.images[0].src
    }
    return null;
};

