"use strict";

app.controller('ProductExpressDeliveryController', ['$scope', '$router', '$http', '$notify', '$notifyHttpErrors', 'controllerParams', function ($scope, $router, $http, $notify, $notifyHttpErrors, controllerParams) {
    $scope.product = new Product(controllerParams['product']);

    $scope.initSelect2 = function () {
        $('.js-country-select-ajax').select2({
            minimumInputLength: 0,
            ajax: {
                url: $router.getProductCountrySelect(),
                dataType: "json",
                type: "GET",
                processResults: function (data) {
                    return {
                        results: data.items
                    };
                }
            }
        });
    };
    $scope.addLine = function () {
        var expressDelivery = new ProductExpressDelivery();
        $scope.product.addExpressDelivery(expressDelivery);
    }
}]);
