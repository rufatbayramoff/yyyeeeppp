"use strict";
app.controller('ProductCartController', ['$scope', '$router', '$http', '$notify', '$notifyHttpErrors', 'controllerParams', function ($scope, $router, $http, $notify, $notifyHttpErrors, controllerParams) {
    $scope.products = controllerParams['products'];
    $scope.productCart = {
        items: controllerParams['productCart']
    };
    $scope.productTotalPrice = [];
    $scope.invalidNext = false;
    $scope.switchToQuoteMode = false;
    $scope.validateProductQty = function(uuid) {
        var qty = $scope.productCart.items[uuid].qty;
        TS.NotifyHelper.hideAll();
        if(qty >= $scope.products[uuid].switchQuoteQty){
            $scope.switchToQuoteMode = true;
            $scope.invalidNext  = true;
            return;
        }
        if(qty < $scope.products[uuid].minOrderQty){
            $notify.error(_t("cart.product", "Min.order quantity for this product is ") + $scope.products[uuid].minOrderQty);
            $scope.invalidNext  = true;
            return;
        }
        var productPrice = $scope.getProductPriceByQty(uuid, qty);
        $scope.productCart.items[uuid].price = productPrice;
        $scope.productCart.items[uuid].priceTotal = productPrice * qty;
        $scope.invalidNext  = false;
        $scope.switchToQuoteMode  = false;
    }

    $scope.getProductPriceByQty = function(uuid, qty)
    {
        var result = null;
        _.each($scope.products[uuid].productPrices, function(productPrice){
            if (qty >= productPrice.moq) {
                 if (result === null || productPrice['priceWithFee'] < result) {
                     result = productPrice['priceWithFee'];
                 }
             }
         });
        return result;
    }
    $scope.goToDelivery = function () {
        var data = $scope.productCart;
        $http.post($router.getProductSaveCart(), data)
            .then(function (response) {
                if (response.data.success) {
                    $router.setUrl($router.getProductBuyDelivery());
                }
            })
            .catch($notifyHttpErrors);
    };
}]);