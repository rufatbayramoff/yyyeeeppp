"use strict";
app.controller('ProductDeliveryController', ['$scope', '$router', '$http', '$notify', '$notifyHttpErrors', '$formValidator', 'controllerParams', function ($scope, $router, $http, $notify, $notifyHttpErrors, $formValidator, controllerParams) {
    $scope.products = controllerParams['products'];
    $scope.deliveryForm = new DeliveryForm();
    $scope.deliveryForm.load(controllerParams['deliveryForm']);


    $scope.goToPayment = function () {
        var data = {
            'deliveryForm': $scope.deliveryForm
        };
        $http.post($router.getProductSaveDeliveryInfo(), data)
            .then(function (response) {
                if (response.data.success) {
                    $router.setUrl($router.getProductBuyPayment());
                }
            })
            .catch($notifyHttpErrors);
    };

    $scope.allowShowCountryIsoSelect = function (counrtyIso) {
        if ($scope.deliveryForm.allowToSendToCountry(counrtyIso)) {
            return true;
        }
        return false;
    };

    var ctrl = this;

    /**
     * Start timeout for make show on map after address form change
     */
    ctrl.startShowmapTimeout = function () {
        this.cancelShowmapTimeout();
        this.showmapTimeout = $timeout(function () {
            $scope.showOnMap();
            ctrl.showmapTimeout = undefined;
        }, 3000);
    };

    /**
     * Cancel timeout for make show on map
     */
    ctrl.cancelShowmapTimeout = function () {
        if (ctrl.showmapTimeout) {
            $timeout.cancel(ctrl.showmapTimeout);
        }
        ctrl.showmapTimeout = undefined;
    };


    /**
     * On change address on map
     * @param address
     */
    $scope.onMapMarkerPositionChange = function (address) {
        ctrl.cancelShowmapTimeout();
        $scope.deliveryForm.setGoogleAddress(address);
        $scope.autocomplete.setAddress(address);
    };

    /**
     * On change address from atocomplete
     * @param address
     */
    $scope.onAutocompleteAddressChange = function (address) {
        ctrl.cancelShowmapTimeout();
        $scope.deliveryForm.setGoogleAddress(address);
        $scope.map.setAddress(address);
    };
}]);