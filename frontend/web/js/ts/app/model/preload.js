"use strict";

app.controller('PreloadModelController',['$scope', '$modelService', '$router', '$flushMessageManager', '$model3dParseProgress', 'controllerParams', function ($scope, $modelService, $router, $flushMessageManager, $model3dParseProgress, controllerParams) {
    /**
     *
     * @type {boolean}
     */
    $scope.isLoading = true;

    /**
     *
     * @type {string}
     */

    $scope.errorMessage = undefined;

    $scope.model3dParseProgress = $model3dParseProgress;
    $scope.model3dParseProgress.setTotalFilesCount(controllerParams.totalFiles);

    $scope.rednerError = function () {
        $scope.isLoading = false;
        $scope.errorMessage = _t('site.model3d', 'A calculating error occurred for this model.');
    };

    $modelService.checkJobs.waitComplited(controllerParams.modelId).tickSubscribe(function (response) {
        $scope.model3dParseProgress.showProgress(response.parts);
        if ($scope.model3dParseProgress.processFinished) {
            if ($scope.model3dParseProgress.isGoodPartsExists) {
                $router.to(controllerParams.redirectUrl);
            } else {
                // Flush message: Not exists files able to print.
                $flushMessageManager.flush(_t('site.upload', 'No valid files for printing'), 'error', false);
                $scope.model3dParseProgress.goToUploadPage();
            }
        }
    });
}]);
