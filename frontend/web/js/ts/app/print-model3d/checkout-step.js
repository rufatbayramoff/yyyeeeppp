"use strict";

app.controller('checkout-step', ['$scope', '$router', '$http', '$notifyHttpErrors', '$notify', 'controllerParams', '$sce', '$compile', function ($scope, $router, $http, $notifyHttpErrors, $notify, controllerParams, $sce, $compile)
{
    /**
     * @type {PayOrder|null}
     */
    $scope.payOrder = null;

    /**
     * @type {string|null}
     */
    $scope.cardHtml = null;
    $scope.paymentData = {
        selectedMethod: null
    };

    /**
     * @type {{code: null}}
     */
    $scope.promocodeData = {
        code: null
    };

    $scope.getPayOrderData = function ()
    {
        $http.get($router.getPrintModel3dPayOrder())
            .then(function (response) {
                $scope.loadPayOrderResponse(response);
            });
    };

    /**
     * @returns {PayOrder|PaymentInvoice}
     */
    $scope.hasBillingDetails = function()
    {
        return $scope.payOrder
            && $scope.payOrder.primaryPaymentInvoice
            && $scope.paymentData.selectedMethod !== null
    };

    /**
     * @returns {PayOrder|objects}
     */
    $scope.hasAddress = function ()
    {
        return $scope.payOrder && $scope.payOrder.shipAddress
    };

    /**
     * @param method
     */
    $scope.choosePaymentMethod = function (method)
    {
        $scope.paymentData.selectedMethod = method;

        return $http.post($router.getBillingDetailsPrint(), {vendor: $scope.paymentData.selectedMethod}).then(function (response) {
            $scope.payOrder.primaryPaymentInvoice.load(response.data.PaymentInvoice);
        }).catch(function (response) {
            $notifyHttpErrors(response);
        });
    };

    /**
     * @param response
     */
    $scope.loadPayOrderResponse = function(response)
    {
        $scope.payOrder = new PayOrder(response.data.payOrder);
        $scope.payOrder.shipAddress.address = $sce.trustAsHtml($scope.payOrder.shipAddress.address);

        if (response.data.checkoutCard) {
            $('#checkout-step-loading').addClass('hidden');
            $scope.cardHtml = response.data.checkoutCard;
        }
    };

    /**
     * @param method
     * @returns {boolean}
     */
    $scope.isSelectedPaymentMethod = function (method)
    {
        return $scope.paymentData.selectedMethod === method
    };

    $scope.ajaxPromoCode = function (data, $event) {
        var $button = $($event.target),
            text = $button.html();

        $button.html('<i class="tsi tsi-refresh tsi-spin-custom"></i>').prop('disabled', true);

        return $http.post($router.getPrintModelApplyPromoCode(), data).then(function (response) {
            $scope.loadPayOrderResponse(response);
            $button.html(text).prop('disabled', false);
        }).catch(function(data) {
            if (data.data){
                $notifyHttpErrors(data);
            }

            $button.html(text).prop('disabled', false);
        });
    };

    /**
     * @param $event
     * @returns {*}
     */
    $scope.applyPromoCode = function ($event)
    {
        return $scope.ajaxPromoCode({
            promocode: $scope.promocodeData.code,
            paymentMethod: $scope.paymentData.selectedMethod
        }, $event);
    };

    /**
     * @param $event
     * @returns {*}
     */
    $scope.removePromoCode = function ($event)
    {
        $scope.promocodeData.code = null;

        return $scope.ajaxPromoCode({
            deletePromoCode: 1,
            paymentMethod: $scope.paymentData.selectedMethod
        }, $event);
    };

    $scope.getTestData = function ()
    {
        console.log($scope.payOrder);
        console.log($scope.paymentData);
    };

    /**
     * Start page
     */
    $scope.init = function()
    {
        $(window).scrollTop(0);
        $scope.getPayOrderData();
        if((typeof(gtag) != 'undefined') && gtag) {
            gtag('event', 'conversion', {
                'send_to': 'AW-873098269/K5NnCPvj68kCEJ3YqaAD',
                'transaction_id': ''
            });
        }

    };

    $scope.init();
}]);