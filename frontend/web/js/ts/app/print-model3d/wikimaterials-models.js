"use strict";

/**
 * @property WikiMaterial[] materials
 * @param $sce
 * @param data
 * @constructor
 */
function WikiMaterialsList($sce, data) {
    this.load($sce, data);
}

WikiMaterialsList.prototype.load = function ($sce, data) {
    this.list = [];
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        var dataElement = data[key];
        var material = new WikiMaterial($sce, dataElement);
        this.list.push(material);
    }
};

/*
* @property string code
* @property string title
* @property string about
* @property string advantages
* @property string disadvantages
* @property string url
* @property int show_widget
* @param $sce
* @param data
* @constructor
*/
function WikiMaterial($sce, data) {
    angular.extend(this, data);
    this.about = $sce.trustAsHtml(this.about);
    this.advantages = $sce.trustAsHtml(this.advantages);
    this.disadvantages = $sce.trustAsHtml(this.disadvantages);
}
