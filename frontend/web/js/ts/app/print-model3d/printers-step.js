"use strict";
app.controller('printers-step', ['$scope', '$router', '$http', '$notify', '$notifyHttpErrors', '$modelRender', '$colorService', '$modal', '$rootScope', 'controllerParams',
    function ($scope, $router, $http, $notify, $notifyHttpErrors, $modelRender, $colorService, $modal, $rootScope, controllerParams) {

        $scope.sortOptions = {
            default: _t('site.store', 'Default'),
            rating: _t('site.store', 'Best Rating'),
            price: _t('site.store', 'Low price'),
            distance: _t('site.store', 'Nearest to me')
        };

        $scope.currencies = {
            usd: 'USD',
            eur: 'EUR',
        };

        $scope.isColorsShortMode = 1;

        $scope.setModel3dActivePart = function (model3dPart) {
            // console.log('Set active model3dPart: ' + model3dPart.id);
            $scope.offersBundleData.selectedModel3dPartId = model3dPart.id;
            var model3dPart = $scope.model3d.getPartById($scope.offersBundleData.selectedModel3dPartId);
            var partTexture = $scope.model3dTextureState.getTexture(model3dPart);
            $scope.offersBundleData.selectedMaterialGroupId = partTexture.printerMaterialGroup.id;
            $scope.offersBundleData.selectedMaterialColorId = partTexture.printerColor.id;
            if (partTexture.infill) {
                $scope.offersBundleData.currentInfill = partTexture.infill;
            }
            $scope.calcShortColorsList();
            $scope.updateInfillSelector();
        };

        $scope.changeMaterialGroup = function (materialGroupId) {
            $scope.offersBundleData.selectedMaterialGroupId = materialGroupId;
            var newMaterialColorItem = $scope.offersBundleData.allowedMaterials.getGroupMaterialsItemByGroupId(materialGroupId);
            var colorItem = $colorService.newColorForMaterialColorsItem(newMaterialColorItem, $scope.offersBundleData.selectedMaterialColorId);

            $scope.model3dTextureState.isAnyTextureAllowed = false;
            $scope.offersBundleData.selectedMaterialColorId = colorItem.colorId;
            $scope.offersBundleData.selectedMaterialId = colorItem.materialId ? '' + colorItem.materialId : '0';
            $scope.offersBundleData.activeOfferItemId = -1;

            var infill = $scope.getCurrentInfill();
            $scope.model3dTextureState.setColorAndMaterialGroup(
                colorItem.colorId,
                colorItem.materialId,
                $scope.offersBundleData.selectedMaterialGroupId,
                infill,
                $scope.offersBundleData.selectedModel3dPartId);

            $scope.updatePrintersListBackground();
            $scope.calcShortColorsList();
            $scope.updateInfillSelector();
        };

        $scope.changeMaterial = function () {
            var selectedMaterialId = $scope.offersBundleData.selectedMaterialId;
            //debugger;
            //console.log('Change material: ' + selectedMaterialId);
            var materialColors = $scope.offersBundleData.allowedMaterials.getColorsMaterialItem($scope.offersBundleData.selectedMaterialGroupId, $scope.offersBundleData.selectedMaterialId);
            // Check current color in colors list
            var colorExists = false;
            for (var key in materialColors.longList) {
                if (!materialColors.longList.hasOwnProperty(key)) continue;
                var materialColorItem = materialColors.longList[key];
                if (materialColorItem['colorId'] == $scope.offersBundleData.selectedMaterialColorId) {
                    colorExists = true;
                }
            }
            if (!colorExists) {
                var newCurrentColor = materialColors.longList[0];
                $scope.offersBundleData.selectedMaterialColorId = newCurrentColor.colorId;

            }

            $scope.model3dTextureState.setColorAndMaterialGroup(
                $scope.offersBundleData.selectedMaterialColorId,
                $scope.offersBundleData.selectedMaterialId,
                $scope.offersBundleData.selectedMaterialGroupId,
                $scope.offersBundleData.currentInfill,
                $scope.offersBundleData.selectedModel3dPartId);

            $scope.model3dTextureState.isAnyTextureAllowed = false;
            $scope.offersBundleData.activeOfferItemId = -1;
            $scope.updatePrintersListBackground();
            $scope.calcShortColorsList();
        };

        $scope.changeColor = function (printerColorId) {
            // console.log('Change Color: ' + printerColorId);
            $scope.model3dTextureState.setColorAndMaterialGroup(
                printerColorId,
                $scope.offersBundleData.selectedMaterialId,
                $scope.offersBundleData.selectedMaterialGroupId,
                $scope.offersBundleData.currentInfill,
                $scope.offersBundleData.selectedModel3dPartId);
            $scope.model3dTextureState.isAnyTextureAllowed = false;
            $scope.offersBundleData.selectedMaterialColorId = printerColorId;
            $scope.offersBundleData.activeOfferItemId = -1;
            $scope.updatePrintersListBackground();
        };

        $scope.updatePrintersListTimeout = 0;
        $scope.startUpdatePrintersOnGetResponse = false;
        $scope.startedUpdatePrintersRequest = false;
        $scope.updatePrintersListBackground = function (forceUpdate) {
            /*
            console.log('updatePrintersListBackground');
            console.log('updatePrintersListTimeout: '+$scope.updatePrintersListTimeout);
            console.log('startUpdatePrintersOnGetResponse:'+$scope.startUpdatePrintersOnGetResponse);
            console.log('startedUpdatePrintersRequest:'+$scope.startedUpdatePrintersRequest);
            */
            if (forceUpdate === true) {
                $scope.offersBundleData.updatePrintersTextureSetted = false;
            }

            if ($scope.updatePrintersListTimeout) {
                clearTimeout($scope.updatePrintersListTimeout);
                $scope.updatePrintersListTimeout = 0;
            }

            $scope.updatePrintersListTimeout = setTimeout(function () {
                $scope.printersPageInfo.currentPage = 0;
                $scope.updatePrintersListTimeout = 0;
                $scope.printersPageInfo.currentPage = 0;
                $scope.updatePrintersList();
            }, 300);

        };

        $scope.updatePrintersList = function () {
            /*
            console.log('updatePrintersList');
            console.log('updatePrintersListTimeout: '+$scope.updatePrintersListTimeout);
            console.log('startUpdatePrintersOnGetResponse:'+$scope.startUpdatePrintersOnGetResponse);
            console.log('startedUpdatePrintersRequest:'+$scope.startedUpdatePrintersRequest);
            */
            if ($scope.startedUpdatePrintersRequest) {
                $scope.startUpdatePrintersOnGetResponse = true;
                return;
            }
            $scope.startedUpdatePrintersRequest = true;

            var params = {
                intlOnly: $scope.offersBundleData.intlOnly,
                certOnly: $scope.offersBundleData.certOnly,
                modelTextureInfo: $scope.model3dTextureState.getTexturesWordyFormat($scope.model3d)
            };

            //$http.get($router.getPrintModel3dPrintersList(), {params: params})
            $http.post($router.getPrintModel3dPrintersList(
                $scope.model3d.uid,
                $scope.printersPageInfo.currentPage,
                $scope.printersPageInfo.pageSize,
                $scope.offersBundleData.currency,
                $scope.offersBundleData.sortPrintersBy,
                $scope.isWidget(),
                $scope.offersBundleData.psIdOnly,
                $scope.offersBundleData.psPrinterIdOnly
                ), params)
                .then(function (response) {
                    // console.log('ajax finished');
                    $scope.startedUpdatePrintersRequest = false;
                    $scope.offersBundleData.timeAround = response.data.timeAround;
                    if (!response.data || !response.data.success) {
                        $notify.error('Error. Please, try again.');
                    } else {
                        $scope.offersBundleData.offersBundle = new OffersBundle(response.data.offerBundle);
                        var bestOffer = $scope.offersBundleData.offersBundle.getBestOffer();
                        if (bestOffer) {
                            $scope.selectPrinter(bestOffer);
                        }
                        $scope.printersPageInfo.numPages = Math.ceil($scope.offersBundleData.offersBundle.totalCount / $scope.printersPageInfo.pageSize);
                        $scope.offersBundleData.allowedMaterials = new AllowedMaterialsList(response.data.allowedMaterials);
                        if (!$scope.offersBundleData.updatePrintersTextureSetted) {
                            $scope.offersBundleData.updatePrintersTextureSetted = true;

                            $scope.model3dTextureState.load(response.data.model3dState);
                            if ($scope.model3dTextureState.isOneTextureForKit) {
                                $scope.offersBundleData.selectedMaterialGroupId = $scope.model3dTextureState.kitTexture.printerMaterialGroup.id;
                                $scope.offersBundleData.selectedMaterialColorId = $scope.model3dTextureState.kitTexture.printerColor.id;
                                $scope.offersBundleData.selectedMaterialId = $scope.model3dTextureState.kitTexture.getPrinterMaterialId() ? '0' : $scope.model3dTextureState.kitTexture.getPrinterMaterialId() + '';
                            } else {
                                var firstPart = $scope.model3d.parts[0];
                                if (typeof (firstPart) !== 'undefined') {
                                    $scope.setModel3dActivePart(firstPart);
                                }
                            }
                            if (response.data.activeOfferItemId) {
                                $scope.offersBundleData.activeOfferItemId = response.data.activeOfferItemId;
                            }
                            $scope.calcShortColorsList();
                            $scope.updateInfillSelector();
                        }
                        if ($scope.offersBundleData.allowedMaterials.isEmpty() && !$scope.offersBundleData.intlOnly) {
                            $scope.offersBundleData.intlOnly = true;
                            $scope.offersBundleData.updatePrintersTextureSetted = false;
                            $scope.updatePrintersListBackground();
                        }
                        $scope.updateToolTips();
                    }
                    if ($scope.startUpdatePrintersOnGetResponse) {
                        $scope.updatePrintersList();
                        $scope.startUpdatePrintersOnGetResponse = false;
                    }
                })
                .catch(function (data) {
                    if (data.status === 418) {
                        $router.toPrintModel3dIndex();
                        return;
                    }
                    $scope.startedUpdatePrintersRequest = false;
                    if (data.status === 419) {
                        setTimeout(function () {
                            $scope.updatePrintersListBackground();
                        }, 1000);
                        return;
                    }
                    $notifyHttpErrors(data);
                });
        };

        $scope.isLastPageSelected = function () {
            return ($scope.printersPageInfo.currentPage + 1) == $scope.printersPageInfo.numPages;
        }

        $scope.prevPage = function () {
            $scope.printersPageInfo.currentPage = $scope.printersPageInfo.currentPage - 1;
            if ($scope.printersPageInfo.currentPage < 0) {
                $scope.printersPageInfo.currentPage = 0;
            }
            $scope.updatePrintersList();
        };

        $scope.nextPage = function () {
            $scope.printersPageInfo.currentPage = $scope.printersPageInfo.currentPage + 1;
            if (($scope.printersPageInfo.currentPage + 1) > $scope.printersPageInfo.numPages) {
                $scope.printersPageInfo.currentPage = $scope.printersPageInfo.numPages - 1;
            }
            $scope.updatePrintersList();
        };

        $scope.selectPage = function (pageId) {
            // console.log('Change page: ' + pageId);
            $scope.printersPageInfo.currentPage = pageId;
            $scope.updatePrintersList();
        };

        $scope.selectPrinter = function (offerItem) {
            $scope.offersBundleData.activeOfferItemId = offerItem.psPrinter.id;
            var params = {
                modelTextureInfo: $scope.model3dTextureState.getTexturesWordyFormat($scope.model3d),
                psPrinterId: offerItem.psPrinter.id
            };
            $scope.deliveryForm.needLoad = false;
            $scope.deliveryForm.loaded = false;

            return $http.post($router.getPrintModel3dSaveCart(), params)
                .then(function (response) {
                    if (!response.data || !response.data.success) {
                        $notify.error('Error. Please, try again.');
                    } else {
                        $scope.psMachineDeliveryInfo.load(response.data.psMachineDeliveryInfo);
                        $scope.deliveryForm.load(response.data.deliveryForm);
                    }
                })
                .catch(function (data) {
                    if (data.status === 418) {
                        $router.toPrintModel3dIndex();
                        return;
                    }
                    $notifyHttpErrors(data);
                });
        }

        $scope.printHere = function (offerItem) {
            $scope.selectPrinter(offerItem).then(function () {
                $scope.nextStep();
            });
        };

        $scope.getColorById = function (colorId) {
            var color = objectStorage().get('PrinterColor', colorId);
            return color;
        };


        $scope.isShortColors = function () {
            return $scope.isColorsShortMode;
        };

        $scope.longColorsNeed = function () {
            var materialColors = $scope.offersBundleData.allowedMaterials.getColorsMaterialItem($scope.offersBundleData.selectedMaterialGroupId, $scope.offersBundleData.selectedMaterialId);
            return materialColors && (materialColors.shortList.length !== materialColors.longList.length);
        };

        /**
         * Short colors list may be changed, if current color only on long colors list:
         *   replace last color with current color
         *
         */
        $scope.calcShortColorsList = function () {
            var materialColors = $scope.offersBundleData.allowedMaterials.getColorsMaterialItem($scope.offersBundleData.selectedMaterialGroupId, $scope.offersBundleData.selectedMaterialId);
            if (materialColors) {

            } else {
                $scope.offersBundleData.selectedMaterialId = '0';
            }
        };

        $scope.updateInfillSelector = function () {
            let infill = $scope.getCurrentInfill();
            if (infill) {
                $scope.offersBundleData.currentInfill = infill;
            }
        };

        $scope.updateToolTips = function () {
            setTimeout(function () {
                $('[data-toggle="tooltip"]').tooltip();
            }, 500);
        };

        $scope.setColorsShortMode = function (mode) {
            if (mode) {
                $scope.calcShortColorsList();
            }
            return $scope.isColorsShortMode = mode;
        };

        $scope.materialsGuide = function () {
            $modal.open({
                template: '/wiki/materials-guide.html',
                onShown: function () {
                    $rootScope.$broadcast('materialsGuide:modalOpen');
                },
                scope: {
                    type: '3dprinting',
                    allowedMaterialsList: typeof $scope.offersBundleData.allowedMaterials.getMaterialsList === 'function' ? $scope.offersBundleData.allowedMaterials.getMaterialsList() : [],
                }
            });
        };

        $scope.setPsCountry = function () {
            if ($scope.offersBundleData.psIdOnly) {
                return $http.post($router.setGeoLocation(), {'UserLocator': {'country': $scope.psCountryIsoOnly}})
                    .then(function (response) {
                        if (!response.data || !response.data.success) {
                            $notify.error('Error. Please, try again.');
                        } else {
                            $router.reload();
                        }
                    })
                    .catch(function (data) {
                        if (data.status === 418) {
                            $router.toPrintModel3dIndex();
                            return;
                        }
                        $notifyHttpErrors(data);
                    });

            }
        };

        $scope.setOneTextureForKit = function () {
            $scope.model3dTextureState.kitTexture = new Texture(
                {
                    printer_color_id: $scope.offersBundleData.selectedMaterialColorId,
                    printer_material_group_id: $scope.offersBundleData.selectedMaterialGroupId,
                    infill: $scope.offersBundleData.currentInfill
                }
            );
            $scope.model3dTextureState.setOneTextureForKit();
            $scope.offersBundleData.selectedModel3dPartId = null;
            $scope.updatePrintersListBackground();
        };

        $scope.onChangePrintersListSort = function () {
            $scope.updatePrintersListBackground();
        };

        $scope.onChangeWithInternationalDelivery = function () {
            $scope.updatePrintersListBackground();
        };

        if (!$scope.offersBundleData.updatePrintersTextureSetted) {
            $scope.updatePrintersListBackground();
        }

        $scope.isShowMaterialColors = function () {
            return $scope.getMaterialsColors() && ($scope.getMaterialsColors().length > 2);
        }

        $scope.getMaterialsColors = function () {
            if (!$scope.offersBundleData.allowedMaterials || !$scope.offersBundleData.allowedMaterials.getGroupMaterialsItemByGroupId) {
                return null;
            }
            var materials = [];
            if ($scope.offersBundleData.allowedMaterials.getGroupMaterialsItemByGroupId($scope.offersBundleData.selectedMaterialGroupId)) {
                materials = $scope.offersBundleData.allowedMaterials.getGroupMaterialsItemByGroupId($scope.offersBundleData.selectedMaterialGroupId).materials;
            }
            return materials;
        };

        $scope.getColors = function () {
            if (!$scope.offersBundleData.allowedMaterials || !$scope.offersBundleData.allowedMaterials.getGroupMaterialsItemByGroupId) {
                return null;
            }
            var materialColors = $scope.offersBundleData.allowedMaterials.getColorsMaterialItem($scope.offersBundleData.selectedMaterialGroupId, $scope.offersBundleData.selectedMaterialId);

            if (materialColors && $scope.isColorsShortMode) {
                var currentColor = materialColors.shortList.find(function (colorInfo) {
                    return colorInfo.colorId === $scope.offersBundleData.selectedMaterialColorId;
                });
                if (!currentColor) {
                    // Replace first color
                    var materialColorsCopy = materialColors;
                    materialColorsCopy.shortList[0].colorId = $scope.offersBundleData.selectedMaterialColorId;

                    materialColorsCopy.shortColorsList[0] = objectStorage().get('PrinterColor', $scope.offersBundleData.selectedMaterialColorId);
                    materialColors = materialColorsCopy;
                }
            }

            return materialColors;
        };

        $scope.getInfill = function () {
            if (!$scope.offersBundleData.allowedMaterials || !$scope.offersBundleData.allowedMaterials.getGroupMaterialsItemByGroupId) {
                return false;
            }
            var materialGroupItem = $scope.offersBundleData.allowedMaterials.getGroupMaterialsItemByGroupId($scope.offersBundleData.selectedMaterialGroupId);
            if (materialGroupItem && materialGroupItem.materialGroup && materialGroupItem.materialGroup.infill) {
                return materialGroupItem.materialGroup.infill;
            }
            return false;
        };

        $scope.getCurrentInfill = function () {
            let infill = $scope.getInfill();
            if (infill) {
                let infill = $scope.offersBundleData.currentInfill;
                var materialGroupItem = $scope.offersBundleData.allowedMaterials.getGroupMaterialsItemByGroupId($scope.offersBundleData.selectedMaterialGroupId);
                if (infill < materialGroupItem.materialGroup.infill.min || infill > materialGroupItem.materialGroup.infill.max) {
                    infill = materialGroupItem.materialGroup.infill.min;
                }
                return infill;
            } else {
                return null;
            }
        };

        $scope.allVendors = function() {
            // All vendors
            $scope.offersBundleData.psIdOnly = null;
            $scope.updatePrintersListBackground();
        };

        $scope.onChangeInfill = function () {
            $scope.model3dTextureState.setColorAndMaterialGroup(
                $scope.offersBundleData.selectedMaterialColorId,
                $scope.offersBundleData.selectedMaterialId,
                $scope.offersBundleData.selectedMaterialGroupId,
                $scope.offersBundleData.currentInfill,
                $scope.offersBundleData.selectedModel3dPartId);
            $scope.updatePrintersListBackground();
        };

        $("#print-navigation").on('updatePrintersList', function () {
            //console.log('updatePrintersList event');
            $scope.updatePrintersListBackground(true);
        });

        $scope.updateToolTips();
        /*
        $scope.testClick = function () {
            $scope.offersBundleData.selectedMaterialId = '2';
            console.log('Test click');
        }*/
    }]);
