"use strict";

app.controller('delivery-step', ['$scope', '$router', '$modal', '$http', '$notifyHttpErrors', '$geo', 'controllerParams', function (
    $scope, $router, $modal, $http, $notifyHttpErrors, $geo, controllerParams) {

    $scope.phoneCountryCodes = controllerParams.phoneCountryCodes;

    $scope.timeAroundDay = function () {
        var day = Object.values($scope.offersBundleData.timeAround);
        return day.reduce(function (a, b) {
            return a + b
        }, 0) + 1;
    };

    $scope.timeAroundModal = function () {
        $modal.open({
            template: '/print/time-around-modal.html',
            scope: {
                timeAround: $scope.offersBundleData.timeAround
            }
        });
    };

    $scope.updateDeliveryInfoBackground = function () {
        setTimeout(function () {
            $scope.updateDeliveryInfo();
        }, 100);
    };

    $scope.updateDeliveryInfo = function () {

        var params = {};

        $http.post($router.getGetDeliveryInfo(), params)
            .then(function (response) {
                // console.log('ajax finished');
                $scope.psMachineDeliveryInfo.load(response.data.psMachineDeliveryInfo);
                $scope.deliveryForm.load(response.data.deliveryForm);
                if (!response.data || !response.data.success) {
                    $notify.error('Error. Please, try again.');
                    return;
                }
            })
            .catch(function (data) {
                if (data.status === 418) {
                    $router.toPrintModel3dIndex();
                    return;
                }
                $notifyHttpErrors(data);
            });

    };
    $scope.resetDeliveryForm = function () {
        $scope.deliveryForm.city = null;
        $scope.deliveryForm.state = null;
        $scope.deliveryForm.zip = null;
        $scope.deliveryForm.lat = null;
        $scope.deliveryForm.lon = null;
        $scope.deliveryForm.country = $scope.deliveryForm.countryIso;
        var country = $geo.getCountryByIsoCode($scope.deliveryForm.countryIso);
        if (country) {
            $scope.map.setAddress({country: country.title});
            $scope.map.getMap().setZoom(7);
        }
    };

    $scope.allowShowCountryIsoSelect = function (counrtyIso) {
        //console.log('countryIso: ' + counrtyIso);
        if ($scope.psMachineDeliveryInfo.haveIntl()) {
            return true;
        }
        if ($scope.psMachineDeliveryInfo.location.countryIso === counrtyIso) {
            return true;
        }
        return false;
    };

    var ctrl = this;

    /**
     * Start timeout for make show on map after address form change
     */
    ctrl.startShowmapTimeout = function () {
        this.cancelShowmapTimeout();
        this.showmapTimeout = $timeout(function () {
            $scope.showOnMap();
            ctrl.showmapTimeout = undefined;
        }, 3000);
    };

    /**
     * Cancel timeout for make show on map
     */
    ctrl.cancelShowmapTimeout = function () {
        if (ctrl.showmapTimeout) {
            $timeout.cancel(ctrl.showmapTimeout);
        }
        ctrl.showmapTimeout = undefined;
    };


    /**
     * On change address on map
     * @param address
     */
    $scope.onMapMarkerPositionChange = function (address) {
        ctrl.cancelShowmapTimeout();
        $scope.deliveryForm.setGoogleAddress(address);
        $scope.autocomplete.setAddress(address);
    };

    /**
     * On change address from atocomplete
     * @param address
     */
    $scope.onAutocompleteAddressChange = function (address) {
        ctrl.cancelShowmapTimeout();
        $scope.deliveryForm.setGoogleAddress(address);
        $scope.map.setAddress(address);
    };

    if ($scope.deliveryForm.needLoad) {
        $scope.deliveryForm.needLoad = false;
        $scope.updateDeliveryInfoBackground();
    }

    $scope.DELIVERY_TYPE_PICKUP = 'pickup';
    $scope.DELIVERY_TYPE_DELIVERY = 'standard';
    $scope.DELIVERY_TYPE_INTL = 'intl';

    $scope.initSelectPhone = function () {
        let selectPhone = $('.js-phone-code-select').select2({
            allowClear: false,
            dropdownAutoWidth: true,
            templateResult: function (state) {
                return $('<span><span class="phone-flag-icon flag-icon-' + String(state.id).toLowerCase() + '"></span>' + state.id + ' ' + state.text + '</span>');
            },
            templateSelection: function (state) {
                return $('<span><span class="phone-flag-icon flag-icon-' + String(state.id).toLowerCase() + '"></span>' + state.text.substr(state.text.indexOf('+'), 100) + '</span>');
            },
            theme: 'krajee',
            data: $scope.phoneCountryCodes,
        });
        setTimeout(function () {
            selectPhone.val($scope.deliveryForm.phoneCountyIso).trigger('change');
        }, 100);
    };

    $scope.stopShowProgress();

    $scope.isRateLoad = false;

    $scope.standardShipping = 0;
    $scope.expressShipping = 0;

    $scope.loadRate = function (){
        $http.post($router.getGetDeliveryRate())
            .then(function (response) {
                var data = response.data;
                $scope.standardShipping = new Money(data.standardShipping);
                if(data.expressShipping) {
                    $scope.expressShipping = new Money(data.expressShipping);
                }
                $scope.isRateLoad = true;
            })
    };

    $scope.loadRate();

}]);
