app.config(['$stateProvider', '$routerProvider', function ($stateProvider, router) {

    $stateProvider
        .state('upload', {
            url: router.getPrintModel3dIndex(),
            templateUrl: '/print-model3d/templates/upload.html',
            controller: 'upload-step'
        })
        .state('printers', {
            url: router.getPrintModel3dPrinters(),
            templateUrl: '/print-model3d/templates/printers.html',
            controller: 'printers-step'
        })
        .state('delivery', {
            url: router.getPrintModel3dDelivery(),
            templateUrl: '/print-model3d/templates/delivery.html',
            controller: 'delivery-step'
        })
        .state('checkout', {
            url: router.getPrintModel3dCheckout(),
            templateUrl: '/print-model3d/templates/checkout.html',
            controller: 'checkout-step'
        })
}]);


app.controller('print-model3d-navigation', ['$scope', '$rootScope', '$router', '$state', '$deliveryValidator', '$modal', '$modelRender', 'controllerParams', function ($scope, $rootScope, $router, $state, $deliveryValidator, $modal, $modelRender, controllerParams) {
    $scope.validateErrors = {};
    $scope.posUid = controllerParams['posUid'] ? controllerParams['posUid'] : '';
    $router.setPosUid($scope.posUid);
    $scope.psId = controllerParams['psId'] ? controllerParams['psId'] : null;
    $scope.model3d = new Model(controllerParams['upload.model3d']);
    $scope.model3d.sortPartsById();
    $scope.model3dTextureState = new ModelTextureState(controllerParams['upload.model3dState']);
    $scope.offersBundleData = {
        offerBundle: {},
        activeOfferItemId: -1,
        allowedMaterials: {},
        selectedMaterialGroupId: null,
        selectedMaterialId: '0', // Use string because angular work in select correctly only with string
        selectedMaterialColorId: null,
        currentInfill: null,
        selectedModel3dPartId: null,
        updatePrintersTextureSetted: false,
        currency: 'usd',
        sortPrintersBy: 'default',
        shortColorsList: [],
        intlOnly: false,
        certOnly: false,
        timeAround: null,
        psIdOnly: controllerParams['psId'],
        psPrinterIdOnly: controllerParams['psPrinterId']

    };
    if ((typeof controllerParams['printer.activeOfferItemId'] !== 'undefined') && (controllerParams['printer.activeOfferItemId']) > 0) {
        $scope.offersBundleData.activeOfferItemId = controllerParams['printer.activeOfferItemId'];
    }
    $scope.deliveryForm = new DeliveryForm();
    $scope.psMachineDeliveryInfo = new PsMachineDeliveryInfo();
    $scope.showProgress = false;
    $scope.checkoutValidated = false;
    $scope.psCountryOnly = controllerParams['psCountry'];
    $scope.psCountryIsoOnly = controllerParams['psCountryIso'];

    // Printers step controller
    $scope.printersPageInfo = {
        pageSize: 10,
        currentPage: 0,
        numPages: 1
    };

    // TODO: MOVER VALIDATE TO scope of controllers
    $scope.steps = {
        'upload': {
            next: 'printers',
            validate: function () {

            }
        },
        'printers': {
            prev: 'upload',
            next: 'delivery',
            validate: function () {
                if ($scope.model3d.isUploading()) {
                    return [
                        _t('site.printModel3d', 'Please wait for files to be uploaded')
                    ];
                }

                if ($scope.model3d.getActiveModel3dParts().length < 1) {
                    return [
                        _t('site.printModel3d', 'Please upload an STL, PLY, OBJ or 3MF file.')
                    ]
                }

                if (!$scope.model3d.isParsed()) {
                    return [
                        _t('site.printModel3d', 'Processing model. Please wait...')
                    ];
                }

                if (!$scope.model3d.isCalculated()) {
                    return [
                        _t('site.printModel3d', 'Parsing failed. Possible invalid file format.')
                    ];
                }
            }
        },
        'delivery': {
            prev: 'printers',
            next: 'checkout',
            saveState: function () {

                $deliveryValidator.validate($scope.deliveryForm, $scope.psMachineDeliveryInfo, function () {
                }, function () {
                });
            },
            validate: function () {
                if (!$scope.offersBundleData.updatePrintersTextureSetted) {
                    return [
                        _t('site.printModel3d', 'Please select a manufacturer.')
                    ]
                }
                if ($scope.offersBundleData.activeOfferItemId < 1) {
                    return [
                        _t('site.printModel3d', 'Please select a manufacturer.')
                    ]
                }
            }
        },
        'checkout': {
            prev: 'delivery',
            next: 'done',
            validate: function () {
                if (!$scope.offersBundleData.updatePrintersTextureSetted) {
                    return [
                        _t('site.printModel3d', 'Please select a manufacturer.')
                    ]
                }
                if ($scope.offersBundleData.activeOfferItemId < 1) {
                    return [
                        _t('site.printModel3d', 'Please select a manufacturer.')
                    ]
                }

                if ($scope.checkoutValidated) {
                    $scope.checkoutValidated = false;
                    return true;
                }

                function onSuccessCheckout() {
                    $scope.checkoutValidated = true;
                    $state.go('checkout');
                }

                function onError(validateErrors) {
                    $scope.showProgress = false;
                    if (validateErrors && validateErrors.length) {
                        new TS.Notify({
                            type: 'error',
                            text: commonJs.prepareMessage(validateErrors || 'Error'),
                            automaticClose: true
                        });
                    }
                }

                function onRetry() {
                    $state.go('checkout');
                }

                $scope.showProgress = true;
                $deliveryValidator.validate($scope.deliveryForm, $scope.psMachineDeliveryInfo, onSuccessCheckout, onError, onRetry);
                return false;
            }
        },
        'done': {
            next: '',
            validate: function () {
            }
        }
    };

    $scope.stopShowProgress = function () {
        $scope.showProgress = false;
    };

    $scope.checkStep = function (stepName, showNotifies) {
        if (typeof showNotifies === 'undefined') {
            showNotifies = false;
        }
        var stateStep = $scope.steps[stepName];
        var validateErrors = stateStep.validate();

        if (validateErrors === false) {

            return false;
        }

        if (validateErrors && validateErrors.length) {
            new TS.Notify({
                type: 'error',
                text: commonJs.prepareMessage(validateErrors || 'Error'),
                automaticClose: true
            });
            return false;
        }
        return true;
    };

    $scope.prevStep = function () {
        var stepName = $state.current.name;
        var stateStep = $scope.steps[stepName];
        if (stateStep) {
            $state.go(stateStep.prev);
            TS.NotifyHelper.hideAll();
            window.scrollTo(0, 0);
        }
    };

    $scope.nextStep = function () {
        var stepName = $state.current.name;
        var stateStep = $scope.steps[stepName];
        if (stateStep) {
            if ($scope.checkStep(stateStep.next, true)) {
                $state.go(stateStep.next);
                TS.NotifyHelper.hideAll();
                window.scrollTo(0, 0);
            }
        }
    };

    $scope.parentNextStep = $scope.nextStep;

    $scope.firstPageLoadCheck = true;
    $rootScope.$on('$stateChangeStart',
        async function (event, toState, toParams, fromState, fromParams) {
            if ($scope.posUid) {
                toParams.posUid = $scope.posUid;
            }
            if ($scope.firstPageLoadCheck) {
                $scope.firstPageLoadCheck = false;
                return;
            }
            if (fromState.name && toState.name) {
                var fromStep = $scope.steps[fromState.name];
                if (fromStep.saveState && (fromStep.next !== toState.name)) {
                    // Backward, save state
                    await fromStep.saveState();
                }
            }
            if (toState.name) {
                if (!$scope.checkStep(toState.name)) {
                    if (fromState.name) {
                        event.preventDefault();
                        $state.go(fromState.name);
                    }
                }
            }
        }
    );

    $scope.preview3d = function () {
        $modal.open({
            template: '/model3d/preview3d.html',
            onShown: function () {
                $rootScope.$broadcast('model3dPreview:modalOpen');
            },
            scope: {
                model3d: $scope.model3d,
                model3dTextureState: $scope.model3dTextureState,
                selectedModel3dPartId: $scope.offersBundleData.selectedModel3dPartId
            }
        });
    };

    $scope.isWidget = function () {
        return window.location.href.indexOf('/widget') !== -1;
    };

    $scope.getModel3dPartPreviewUrl = function (model3dPart) {
        return $modelRender.getFullUrl(model3dPart, $scope.model3dTextureState.getColorForRendering(model3dPart));
    };

    // Set init step
    setTimeout(function () {
        $state.go(controllerParams.currentStepName);
    }, 0);
}]);

