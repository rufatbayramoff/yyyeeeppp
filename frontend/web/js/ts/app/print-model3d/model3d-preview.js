"use strict";

app.controller('model3d-preview', ['$scope', '$router', '$http', '$timeout', 'controllerParams', function ($scope, $router, $http, $timeout, controllerParams) {
    $scope.selectedModel3dPartId = null;
    $scope.renders = {};
    $scope.viewPrev = function () {
        var prev = null;
        var last = null;
        for (var partKey in $scope.model3d.parts) {
            if (!$scope.model3d.parts.hasOwnProperty(partKey)) continue;
            var part = $scope.model3d.parts[partKey];
            if (part.isCanceled) {
                continue;
            }
            last = part;
        }
        for (var partKey in $scope.model3d.parts) {
            if (!$scope.model3d.parts.hasOwnProperty(partKey)) continue;
            var part = $scope.model3d.parts[partKey];
            if (part.isCanceled) {
                continue;
            }
            if (part.id == $scope.selectedModel3dPartId) {
                break;
            }
            prev = part;
        }
        if (prev) {
            $scope.setSelectedModel3dPartId(prev.id);
        } else {
            $scope.setSelectedModel3dPartId(last.id);
        }

    };
    $scope.viewNext = function () {
        var prev = null;
        var first = null;
        var next = null;
        for (var partKey in $scope.model3d.parts) {
            if (!$scope.model3d.parts.hasOwnProperty(partKey)) continue;
            var part = $scope.model3d.parts[partKey];
            if (part.isCanceled) {
                continue;
            }
            if (prev && prev.id == $scope.selectedModel3dPartId) {
                next = part;
                break;
            }
            prev = part;
            if (!first) {
                first = part;
            }
        }
        if (next) {
            $scope.setSelectedModel3dPartId(next.id);
        } else {
            $scope.setSelectedModel3dPartId(first.id);
        }
    };
    $scope.setSelectedModel3dPartId = function (partId) {
        if ($scope.renders[$scope.selectedModel3dPartId]) {
            $scope.renders[$scope.selectedModel3dPartId].setIsVisible(false);
        }
        $scope.selectedModel3dPartId = partId;
        if ($scope.renders[$scope.selectedModel3dPartId]) {
            $scope.renders[$scope.selectedModel3dPartId].setIsVisible(true);
            $scope.renders[$scope.selectedModel3dPartId].autoRotateStart();
        }
    };

    $scope.initModel3dRender = function () {
        $scope.renders = {};
        for (var partKey in $scope.model3d.parts) {
            if (!$scope.model3d.parts.hasOwnProperty(partKey)) continue;
            var part = $scope.model3d.parts[partKey];
            if (!part.hash || part.isCanceled) continue;

            var render = commonJs.clone(model3dRenderClass);
            var fileExt = part.fileExt.toLowerCase();
            if (fileExt === 'step' || fileExt === 'stp' || fileExt === 'iges' || fileExt === 'igs') {
                fileExt = 'stl';
            }
            render.init({
                imgElementId: 'modal_model3dpart_img_' + part.id,
                fileUid: part.hash + '_' + fileExt,
                isMulticolorFormat: part.isMulticolorFormat,
                renderUrlBase: part.renderUrlBase,
                color: $scope.model3dTextureState.getColorForRendering(part),
                ax: 0,
                ay: 30,
                az: 0,
                pb: part.pb,
                allowAutoRotate: true
            });
            render.placeImg();
            render.setIsVisible(false);
            $scope.renders[part.id] = render;
        }
    };

    $scope.$on('model3dPreview:modalOpen', function () {
        $scope.initModel3dRender();
        if (!$scope.selectedModel3dPartId) {
            for (var partKey in $scope.model3d.parts) {
                if (!$scope.model3d.parts.hasOwnProperty(partKey)) continue;
                var part = $scope.model3d.parts[partKey];
                if (!part.hash || part.isCanceled) continue;
                $scope.setSelectedModel3dPartId(part.id);
                $scope.$digest();
                break;
            }
        }
    });

}]);
