"use strict";

function CommonUploadPage(config) {
    let commonUploadPage = this;

    // Temp array anti garbage collector
    this.fileInputs = [];
    this.modelCreatePromiseStarted = null;

    this.fileAdded = function (uploadItem) {
        var file = uploadItem.file;

        if (file.size <= 0) {
            new TS.Notify({
                type: 'error',
                text: '"' + file.name + '" ' + _t('site.printModel3d', 'File is empty'),
                target: '.messageBox',
                automaticClose: true
            });

            return false;
        }

        var fileMd5NameSize = $.md5(file.name.normalize('NFC') + '_' + file.size);
        var uploadFileInfo = config.getItemByMd5NameSize(fileMd5NameSize);
        if (uploadFileInfo && uploadFileInfo.uid) {
            // File already in list
            if ((typeof uploadFileInfo.qty) != 'undefined') {
                if (uploadFileInfo.isCanceled === true) {
                    config.restoreUploadItem(uploadFileInfo);
                } else {
                    new TS.Notify({
                        type: 'success',
                        text: _t('site.printModel3d', 'File already in the list.'),
                        target: '.messageBox',
                        automaticClose: true
                    });
                }
                config.applyChanges();
            } else {
                if (uploadFileInfo.isCanceled === true) {
                    config.restoreUploadItem(uploadFileInfo);
                } else {
                    new TS.Notify({
                        type: 'success',
                        text: _t('site.printModel3d', 'File already in the list.'),
                        target: '.messageBox',
                        automaticClose: true
                    });
                }
            }
            return false;
        }

        let fileExt = file.name.split('.').pop();
        uploadFileInfo = new UploadFileInfo({
            'previewUrl': '/static/images/preloader80.gif',
            'format': fileExt,
            'title': file.name,
            'fileExt': fileExt,
            'uploadPercent': 1,
            'uploadItem': uploadItem,
            'fileMd5NameSize': fileMd5NameSize
        });
        if (commonUploadPage.isImgFile(file)) {
            // Is image
            var imageInfo = new config.modelImage(uploadFileInfo);
            uploadFileInfo = imageInfo;
            if (imageInfo.uuid) {
                uploadItem.uuid = imageInfo.uuid;
            }
            uploadItem.readAs('DataURL', function (e) {
                imageInfo.previewUrl = URL.createObjectURL(file);
                config.applyChanges();
            });
            config.pushImage(imageInfo);
        } else {
            var partInfo = new config.modelPart(uploadFileInfo);
            if (partInfo.uuid) {
                uploadItem.uuid = partInfo.uuid;
            }
            uploadFileInfo = partInfo;
            partInfo.qty = 1;
            partInfo.isParsed = false;
            config.pushPart(partInfo);
        }

        uploadItem.progressCallback = function (percent) {
            if (percent > 99) {
                percent = 99;
            }
            uploadFileInfo.uploadPercent = percent;
            config.applyChanges();
        };

        uploadItem.completeCallback = function (success, data, status) {
            if (success) {
                uploadFileInfo.uploadPercent = 100;
            } else {
                if (uploadFileInfo.uploadItem.cancelled) {
                    return;
                }
                uploadFileInfo.uploadPercent = 0;
                uploadFileInfo.uploadFailedReason = _t('site.printModel3d', 'File upload failed. Please try again later.');
                let popupMessage = _t('site.printModel3d', 'File upload failed: ') + file.name;
                let json = JSON.parse(uploadFileInfo.uploadItem.xhr.response);
                if (json && json.message) {
                    uploadFileInfo.uploadFailedReason = json.message;
                    popupMessage+="<br>\n"+json.message;
                }
                new TS.Notify({
                    type: 'error',
                    text: popupMessage,
                    target: '.messageBox',
                    automaticClose: true
                });
            }
            config.completeCallback(success, data, status, file);
        };

        config.applyChanges();
        return true;
    };

    this.isImgFile = function (file) {
        let isImage = file.type.match(/image.*/) && (
            (file.type.indexOf('jpg') != -1) ||
            (file.type.indexOf('jpeg') != -1) ||
            (file.type.indexOf('gif') != -1) ||
            (file.type.indexOf('png') != -1)
        );
        return isImage;
    };

    this.isArchiveFile = function (file) {
        return file.type.indexOf('zip') != -1;
    };

    this.initDropDownUploader = function () {
        var input = $(document.createElement('input'));
        window.test.getWidgetUploadInput = function () {
            return input;
        };
        input.attr('type', 'file');
        input.attr('multipile', 'true');
        $('.upload-drop-file-zone').on('drop dragdrop', function (event) {
            $(this).removeClass('upload-drop-file-zone-hover');
        }).on('dragenter', function (event) {
            event.preventDefault();
            $(this).addClass('upload-drop-file-zone-hover');
        }).on('dragleave', function () {
            event.preventDefault();
            $(this).removeClass('upload-drop-file-zone-hover');
        }).on('dragover', function (event) {
            event.preventDefault();
            $(this).addClass('upload-drop-file-zone-hover');
        });
        commonUploadPage.initUploader(input, '.upload-drop-file-zone')
    };

    this.initUploader = function (element, dropBox) {
        var fileInput = $(element);
        var withDrop = false;
        var acceptType = '.' + config.allowedExtensions;
        var d = fileInput.damnUploader({
            dropping: withDrop,
            url: config.uploadFileUrl,
            dropBox: dropBox,
            dataType: 'json',
            acceptType: acceptType
        });

        var isValidFile = function (file, acceptedFiles) {
            var baseMimeType, mimeType, validType, _i, _len;
            if (!acceptedFiles) {
                return true;
            }
            acceptedFiles = acceptedFiles.split(",");
            mimeType = file.type;
            baseMimeType = mimeType.replace(/\/.*$/, "");
            for (_i = 0, _len = acceptedFiles.length; _i < _len; _i++) {
                validType = acceptedFiles[_i];
                validType = validType.trim();
                if (validType.charAt(0) === ".") {
                    if (file.name.toLowerCase().indexOf(validType.toLowerCase(), file.name.length - validType.length) !== -1) {
                        return true;
                    }
                } else if (/\/\*$/.test(validType)) {
                    if (baseMimeType === validType.replace(/\/.*$/, "")) {
                        return true;
                    }
                } else {
                    if (mimeType === validType) {
                        return true;
                    }
                }
            }
            return false;
        };

        if (!withDrop) {
            $(dropBox).on('drop', function (e) {
                var denyUploadWithFolder = false;
                if (e.originalEvent.dataTransfer.items instanceof DataTransferItemList) {
                    $.each(e.originalEvent.dataTransfer.items, function (i, item) {
                        var entry = item.webkitGetAsEntry();
                        if (entry.isDirectory) {
                            denyUploadWithFolder = true;
                        }
                    });
                }
                if (denyUploadWithFolder) {
                    new TS.Notify({
                        type: 'warning',
                        text: _t('site.printModel3d', 'Cannot upload folder. Please upload supported files only'),
                        target: '.messageBox',
                        automaticClose: true
                    });
                } else {
                    var denyFormat = false;
                    if (e.originalEvent.dataTransfer.files instanceof FileList) {
                        $.each(e.originalEvent.dataTransfer.files, function (i, item) {
                            if (!isValidFile(item, acceptType)) {
                                denyFormat = true;
                            }
                        });
                    }
                    if (denyFormat) {
                        new TS.Notify({
                            type: 'warning',
                            text: _t('site.printModel3d', 'Please upload supported files only'),
                            target: '.messageBox',
                            automaticClose: true
                        });
                    } else {
                        d._duAddItemsToQueue(e.originalEvent.dataTransfer.files);
                    }
                }
                return false;
            });
        }

        fileInput.on('du.add', function (e) {
            var addFileFunction = function () {
                config.fileAdded();
                if (!commonUploadPage.fileAdded(e.uploadItem)) {
                    return false;
                }
                window.setTimeout(function () {
                    fileInput.duStart();
                }, 100);
            };

            if (config.modelCreated()) {
                addFileFunction();
            } else {
                if (commonUploadPage.modelCreatePromiseStarted) {
                    commonUploadPage.modelCreatePromiseStarted.then(addFileFunction);
                } else {
                    commonUploadPage.modelCreatePromiseStarted = config.modelCreatePromise().then(addFileFunction);
                }
            }
        });
        commonUploadPage.fileInputs.push(fileInput);
    };

    this.addFileClick = function () {
        var uploadForm = document.createElement('form');
        var input = $(uploadForm.appendChild(document.createElement('input')));
        input.attr('type', 'file');
        input.attr('multipile', 'true');
        commonUploadPage.initUploader(input, '');
        input.trigger('click'); // opening dialog
    };
};

/**
 * @property {string} previewUrl
 * @property {string} title
 * @property {int} uploadPercent
 **/
function UploadFileInfo(data) {
    angular.extend(this, data);
}
