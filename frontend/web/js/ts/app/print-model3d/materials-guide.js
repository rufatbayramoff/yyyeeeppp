"use strict";

app.controller('materials-guide', ['$scope', '$router', '$http', '$timeout', '$notify', '$sce', 'controllerParams', function ($scope, $router, $http, $timeout, $notify, $sce, controllerParams) {
    $scope.wikiMaterialsList = null;
    $scope.currentMaterial = null;

    $scope.selectMaterial = function (materialItem) {
        $scope.currentMaterial = materialItem;
    };

    $scope.$on('materialsGuide:modalOpen', function () {
        $http.get($router.getMaterialsGuide($scope.type, $scope.allowedMaterialsList))
            .then(function (response) {
                if (!response.data || !response.data.success) {
                    $notify.error('Error. Please, try again.');
                } else {
                    $scope.wikiMaterialsList = new WikiMaterialsList($sce, response.data.wikiMaterials);
                    $scope.currentMaterial = _.first($scope.wikiMaterialsList.list);
                    if (!$scope.currentMaterial) {
                        $scope.currentMaterial = false;
                    }
                }
            });
    });
}]);

