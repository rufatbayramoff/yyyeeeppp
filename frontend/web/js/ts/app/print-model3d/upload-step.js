"use strict";

app.controller('upload-step', ['$scope', '$timeout', '$router', '$modelService', '$model3dParseProgress', '$rootScope', '$http', 'controllerParams', function ($scope, $timeout, $router, $modelService, $model3dParseProgress, $rootScope, $http, controllerParams) {

    $scope.reviews = new ReviewsBundle(controllerParams.reviews);

    $scope.hasStepFiles = function () {
        return $modelService.testHasStepFiles($scope.model3d);
    };

    $scope.testIsEmptyModel = function () {
        return $modelService.testIsEmptyModel($scope.model3d);
    };

    $scope.testIsPrintablePartsExists = function () {
        return $modelService.testIsPrintablePartsExists($scope.model3d);
    };

    $scope.updateModel3dPartQty = function (model3dPart) {
        if (model3dPart.qty === '' || model3dPart.qty === null) {
            return;
        }
        $scope.offersBundleData.updatePrintersTextureSetted = false;
        $scope.offersBundleData.activeOfferItemId = -1;
        $modelService.updatePartQty(model3dPart).then(function (response) {
            $scope.model3d.fixParserParts(response.data.model3d.parts);
            $("#print-navigation").trigger('updatePrintersList');
        });
    };

    $scope.createModel3d = function (addFileFunction) {
        return $http.get($router.getCreateModel3d()).then(function (response) {
                if (response.data && response.data.success == true) {
                    var model3dInfo = response.data.model3d;
                    var model3dStateInfo = response.data.model3dState;
                    $scope.model3d.load(model3dInfo);
                    $scope.model3dTextureState.load(model3dStateInfo);
                } else {
                    new TS.Notify({
                        type: 'error',
                        text: _t('site.printModel3d', 'Failed to load 3D model. Please try to refresh the page.'),
                        target: '.messageBox',
                        automaticClose: true
                    });
                    return $q.reject();
                }
            }
        )
    };

    $scope.pingPartsStatus = function () {
        $modelService.checkJobs2($scope.model3d.uid, function (response) {
            var modelIsParsed = true;
            var model3dResponse = response.model3d;

            $scope.model3d.loadCancelParts(model3dResponse.canceledParts);
            $scope.model3d.fixParserParts(model3dResponse.parts);
            var savedAnyTextureAllowed = $scope.model3dTextureState.isAnyTextureAllowed;
            $scope.model3dTextureState.load(response.model3dTextureState);
            $scope.model3dTextureState.isAnyTextureAllowed = savedAnyTextureAllowed;

            if (response.isComplited) {
                return true;
            } else {
                return false;
            }
        });
    };

    $scope.changeScale = function (scaleUnit) {
        return $modelService.switchScaleUnit($scope.model3d, scaleUnit, function () {
            $scope.offersBundleData.updatePrintersTextureSetted = false;
            $scope.offersBundleData.activeOfferItemId = -1;
            $rootScope.$broadcast('filepage:setModelMeasure', scaleUnit);
        });
    };

    $scope.$on('filepage:updateScale', function (event, newSize) {
        var firstPart = $scope.model3d.getFirstActiveModel3dPart();
        $modelService.updatePartScale($scope.model3d, firstPart, newSize, function () {
            $scope.offersBundleData.updatePrintersTextureSetted = false;
            $scope.offersBundleData.activeOfferItemId = -1;
            $rootScope.$broadcast('filepage:scaleUpdated');
        });
    });



    $scope.duplicate = _.throttle(function(model3dPart){
        $modelService.duplicate({'uid':model3dPart.uid,'qty': model3dPart.qty}, function (response) {
            $scope.model3d.load(response.data.model3d);
            $scope.pingPartsStatus();
            $("#print-navigation").trigger('updatePrintersList');
        });
    }, 1000);

    $scope.cancelUploadItem = function (uploadFileInfo) {
        $modelService.cancelUploadItem(uploadFileInfo);
        if ($scope.model3d.isEmpty()) {
            $scope.model3d.uid = '';
        }
        $scope.offersBundleData.updatePrintersTextureSetted = false;
        $scope.offersBundleData.activeOfferItemId = -1;
    };

    $scope.uploaderConfig = {
        'existsUid': $scope.model3d.uid,
        'allowedExtensions': controllerParams['upload.allowedExtensions'].join(',.'),
        'fileAdded': function () {
            $scope.offersBundleData.updatePrintersTextureSetted = false;
            $scope.offersBundleData.activeOfferItemId = -1;
        },
        'uploadFileUrl': $router.getPrintModel3dUpload(),
        'cancelUploadItem': $scope.cancelUploadItem,
        'getItemByMd5NameSize': function (uuid) {
            return $scope.model3d.getItemByMd5NameSize(uuid);
        },
        'restoreUploadItem': function (uploadFileInfo) {
            $modelService.restoreUploadItem(uploadFileInfo, $scope.pingPartsStatus);
        },
        'applyChanges': function () {
            setTimeout(function () {
                $scope.$digest();
            }, 0);
        },
        'modelImage': ModelImage,
        'modelPart': ModelPart,
        'pushImage': function(imageInfo) {
            $scope.model3d.images.push(imageInfo);
        },
        'pushPart': function(partInfo) {
            $scope.model3d.parts.push(partInfo);
        },
        'completeCallback': function (success, data, status, file) {
            if (success) {
                if ($scope.uploader.isArchiveFile(file)) {
                    $router.toPrintModel3dIndex();
                    return;
                }
                $modelService.updateModel3dInfo($scope.model3d, data['model3d']);
                $scope.pingPartsStatus();
            } else {
                if (status == 418) { // TODO: Move to $notifyHttpErrors
                    $router.toPrintModel3dIndex();
                    return;
                }
            }
            setTimeout(function () {
                $scope.$digest();
            }, 0);
        },
        'modelCreated': function () {
            return $scope.model3d.uid;
        },
        'modelCreatePromise': $scope.createModel3d,
    };
    $scope.uploader = new CommonUploadPage($scope.uploaderConfig);
    $scope.uploader.initDropDownUploader();

    if (($scope.model3d.id) && ($scope.model3d.parts.length > 0) && (!$scope.model3d.isParsed())) {
        $scope.pingPartsStatus();
    }
}]);
