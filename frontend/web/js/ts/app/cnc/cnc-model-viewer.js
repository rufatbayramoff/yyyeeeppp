"use strict";

app.directive("modelViewer", function ($q, $timeout, $treeJs, $cncService, machineId)
{
    if (!Detector.webgl)
        Detector.addGetWebGLMessage();

    return {
        restrict : 'E',
        templateUrl : '/app/cnc/model-viewer.html',
        replace : true,
        scope : {
            part : '=',
            costing : '=',
            preset : '='
        },
        link : function (scope, element)
        {
            var camera,
                intersected,
                scene,
                renderer,
                raycaster,
                controls,
                material,
                materialColored,
                meshMain,
                objects = [],
                instrMaterial,
                theInstrumentMesh;

            var light1, light2, light3, light4;

            var mouse = new THREE.Vector2(),
                vivid = true;

            function restoreAxis(axisCode) {
                if (axisCode == 1) { return new THREE.Vector3( 1, 0, 0); }
                else if (axisCode == 2) { return new THREE.Vector3(-1, 0, 0); }
                else if (axisCode == 3) { return new THREE.Vector3( 0, 1, 0); }
                else if (axisCode == 4) { return new THREE.Vector3( 0,-1, 0); }
                else if (axisCode == 5) { return new THREE.Vector3( 0, 0, 1); }
                else if (axisCode == 6) { return new THREE.Vector3( 0, 0,-1); }
                ; return null;
            }

            function init() {
                var $renderElement = element.find('.cnc_model_view_viewer_render');
                var w = $renderElement.width();
                var h = $renderElement.height();

                // camera
                camera = new THREE.PerspectiveCamera(35, w / h, 1, 10000);

                // scene
                scene = new THREE.Scene();
                raycaster = new THREE.Raycaster();
                // renderer
                renderer = new THREE.WebGLRenderer({antialias: true, preserveDrawingBuffer: true});
                renderer.setClearColor(0xfcfcfc);
                renderer.setSize(w, h);
                renderer.gammaInput = true;
                renderer.gammaOutput = true;
                renderer.shadowMap.enabled = true;
                renderer.shadowMap.renderReverseSided = false;
                // container

                $renderElement.get(0).appendChild(renderer.domElement);
                // controls
                controls = new THREE.OrbitControls(camera, renderer.domElement);
                controls.target.set(0, 0, 0);
                controls.zoomSpeed = 0.5;
                controls.update();
                // lights
                scene.add(new THREE.HemisphereLight(0x443333, 0x111122));
                var r = 1;
                light1 = addShadowedLight(2*r, 2*r, 0, 0xffffff, 1);
                light2 = addShadowedLight(-1*r, r, r, 0xffffff, 0.8);
                light3 = addShadowedLight(-1*r, 0.5*r, -1*r, 0xffffff, 0.6);
                light4 = addShadowedLight(0.5*r, -0.5*r, -2*r, 0xffffff, 0.6);
                // material
                material = new THREE.MeshPhongMaterial({
                    color: 0xff5533, specular: 0x111111, shininess: 200,
                    side: THREE.DoubleSide // , vertexColors: THREE.VertexColors
                });
                materialColored = new THREE.MeshPhongMaterial({
                    color: 0xcccccc, specular: 0x111111, shininess: 200,
                    side: THREE.DoubleSide , vertexColors: THREE.VertexColors
                });
                instrMaterial = new THREE.MeshStandardMaterial( { color: 0x000088, opacity: 0.5, transparent: true } );
                //window.addEventListener('resize', onWindowResize, false);
                document.addEventListener( 'mousemove', onDocumentMouseMove, false );
                renderer.domElement.ondblclick=onDoubleClick;
            }


            function addShadowedLight(x, y, z, color, intensity)
            {
                var directionalLight = new THREE.DirectionalLight(color, intensity);
                directionalLight.position.set(x, y, z);
                scene.add(directionalLight);
                directionalLight.castShadow = true;
                var d = 1;
                directionalLight.shadow.camera.left = -d;
                directionalLight.shadow.camera.right = d;
                directionalLight.shadow.camera.top = d;
                directionalLight.shadow.camera.bottom = -d;
                directionalLight.shadow.camera.near = 1;
                directionalLight.shadow.camera.far = 15;
                directionalLight.shadow.mapSize.width = 1024;
                directionalLight.shadow.mapSize.height = 1024;
                directionalLight.shadow.bias = -0.005;
                return directionalLight;
            }

            function showStl(geometry, isColored)
            {
                if (meshMain) scene.remove(meshMain);
                geometry.computeVertexNormals();
                geometry.computeBoundingSphere();
                var sphere = geometry.boundingSphere;
                geometry.computeBoundingBox();
                //geometry.center();
                var r =  sphere.radius;
                meshMain = new THREE.Mesh(geometry, (isColored ? materialColored : material) );
                makeInstrument(geometry.instrumentDiameter);
                meshMain.castShadow = true;
                meshMain.receiveShadow = true;
                // add to scene
                scene.add(meshMain);
                objects.push(meshMain);
                // Lights
                light1.position.set(2*r, 2*r, 0);
                light2.position.set(-1*r, 1*r, 1*r);
                light3.position.set(-1*r, 0.5*r, -1*r);
                light4.position.set(0.5*r, -0.5*r, -2*r);
                controls.target.copy(sphere.center);

                camera.position.set(sphere.center.x, sphere.center.y, sphere.center.z + 4*r);
            }


            function makeInstrument(diameter)
            {
                if (theInstrumentMesh) {
                    theInstrumentMesh.visible = false;
                    scene.remove( theInstrumentMesh );
                }
                var instrRadius = diameter/2;
                var instrLen = 1000; // sphere.radius * 2;
                theInstrumentMesh = new THREE.Mesh( new THREE.CylinderGeometry( instrRadius, instrRadius, instrLen, 100/*rsegm*/, 5/*hsegm*/ ), instrMaterial );
                theInstrumentMesh.position.set( 0, 0, 0 );
                theInstrumentMesh.visible = false;
                scene.add( theInstrumentMesh );
            }

            function onDoubleClick() {
                vivid = !vivid;
            }

            function onDocumentMouseMove( event )
            {
                event.preventDefault();
                if (!meshMain || !meshMain.geometry || !meshMain.geometry.instrumentDiameter) return;
                if (!vivid) return;
                var rect = renderer.domElement.getBoundingClientRect();
                mouse.x =  (event.clientX - rect.left) * 2.0 / (rect.right - rect.left) - 1;
                mouse.y =  (event.clientY - rect.bottom) * 2.0 / (rect.top - rect.bottom) - 1;
                raycaster.setFromCamera( mouse, camera );
                var intersections = raycaster.intersectObjects( objects );

                if ( intersections.length > 0 ) {
                    intersected = intersections[ 0 ].object;
                    var instrumentPos = new THREE.Vector3(
                        meshMain.geometry.instrumentPositions[intersections[0].faceIndex*3],
                        meshMain.geometry.instrumentPositions[intersections[0].faceIndex*3+1],
                        meshMain.geometry.instrumentPositions[intersections[0].faceIndex*3+2]
                    );
                    var instrumentAxis = restoreAxis(meshMain.geometry.instrumentAxis[intersections[ 0 ].faceIndex * 3]);
                    if (instrumentAxis) {
                        var axisOriginal = new THREE.Vector3(0, 1, 0);
                        theInstrumentMesh.quaternion.setFromUnitVectors(axisOriginal, instrumentAxis.clone().normalize());
                        var p = instrumentPos.add(instrumentAxis.multiplyScalar(1000/2));
                        theInstrumentMesh.position.set( p.x, p.y, p.z );
                        theInstrumentMesh.visible = true;
                    } else {
                        theInstrumentMesh.visible = false;
                    }
                }
            }

            var animate = function ()
            {
                requestAnimationFrame(animate);
                controls.update();
                renderer.render(scene, camera);
            };

            var rendererGeometry = function (loadStlResponse)
            {
                scope.view.isLoading = false;
                $timeout(function () {
                    init();
                    animate();
                    showStl(loadStlResponse.geometry, loadStlResponse.isColored);
                });

                return $q.resolve(loadStlResponse);
            };

            var showLoadingError = function (response) {
                scope.view.isLoading = false;
                scope.view.loadingError = response.data.message;
                return $q.reject(response);
            };

            scope.view = {
                isLoading : true,
                isModeling : false,
                loadingError : undefined
            };

            scope.$watch('part.cncParams', function (cncParams) {

                if(cncParams) {
                    $treeJs.loadStl("/workbench/cncm/filepage/load-stl?partId=" + scope.part.uid, false)
                        .then(rendererGeometry, showLoadingError);
                }
            });

            scope.$watch('view.isModeling', function (newVal, oldVal) {

                if (newVal === oldVal) {
                    return;
                }

                scope.view.isLoading = true;
                scope.view.loadingError = undefined;

                if (newVal) {
                    $cncService.modeling(scope.part, machineId, scope.preset, scope.costing)
                        .then(function (response) {
                            return $treeJs.loadStl("/workbench/cncm/filepage/load-modeling-file?uri=" + encodeURI(response.plyColored), true);
                        }, showLoadingError)
                        .then(rendererGeometry, showLoadingError);

                }
                else {
                    $treeJs.loadStl("/workbench/cncm/filepage/load-stl?partId=" + scope.part.uid, false)
                        .then(rendererGeometry, showLoadingError);

                }
            });

            scope.$watch('preset', function(){
                scope.view.isModeling = false;
            }, true);
        }
    }
})


.factory('$treeJs', function ($q) {

    var $treeJs = {};

    /**
     * Load STL files
     * @param {string} url
     * @param {boolean} isColored
     * @return {Promise}
     */
    $treeJs.loadStl = function (url, isColored)
    {
        var defer = $q.defer();
        var loader = isColored? new THREE.ST2Loader() : new THREE.STLLoader();

        loader.load(
            url,
            function (geometry) {defer.resolve({geometry : geometry, isColored : isColored})},
            undefined,
            function () {defer.reject({data : {message : _t('site.ps', 'Error processing model.')}})});

        return defer.promise;
    };


    return $treeJs;
});