"use strict";

/**
 * @property {int} id
 * @property {string} status
 * @property {bool} isEditable
 * @property {bool} isSelfPreorder
 * @property {Model} replica
 * @property {CncPreorderWork[]} works
 * @property {CncPreorderPreset[]} presets
 * @property {CncPreorderRequest} request
 * @property {FileModel[]} additionalFiles
 * @property {int} version
 * @property {String} customer_comment
 *
 * @property {bool} isWaitCustomerAccepting
 * @property {bool} isWaitPsOffer
 * @property {bool} isCanDecline
 *
 * @param {*} data
 * @constructor
 */
function CncPreorder(data)
{
    angular.extend(this, data);

    this.replica = new Model(this.replica);
    this.request = new CncPreorderRequest(this.request);

    this.works = this.works.map(function (raw) {
        return new CncPreorderWork(raw)
    });

    this.presets = this.presets.map(function (raw) {
        return new CncPreorderPreset(raw)
    });


    // this need for angular ng-repeat support

    this.$$worksForPart = [];
    this.$$additionalWorks = [];

    var self = this;
    this.works.forEach(function (work) {

        if(work.part_replica_id) {

            if(!self.$$worksForPart[work.part_replica_id]) {
                self.$$worksForPart[work.part_replica_id] = [];
            }
            self.$$worksForPart[work.part_replica_id].push(work);
        }
        else {
            self.$$additionalWorks.push(work);
        }
    });
}

/**
 *
 * @param {ModelPart} part
 * @return {CncPreorderWork[]}
 */
CncPreorder.prototype.getWorksForPart = function(part)
{
    return this.$$worksForPart[part.partReplicaId];
};

/**
 *
 * @param {ModelPart} part
 * @return {CncPreorderPreset}
 */
CncPreorder.prototype.getPresetForPart = function(part)
{
    return _.find(this.presets, function (preset) {
        return preset.part_replica_id === part.partReplicaId;
    });
};

/**
 *
 * @return {CncPreorderWork[]}
 */
CncPreorder.prototype.getAddtionalWorks = function()
{
    return this.$$additionalWorks;
};


/**
 *
 * @param {CncPreorderWork} work
 */
CncPreorder.prototype.deleteWork = function(work)
{
    _.remove(this.works, work);
    work.isForPart()
        ? _.remove(this.$$worksForPart[work.part_replica_id], work)
        : _.remove(this.$$additionalWorks, work);
};

/**
 *
 * @param {CncPreorderWork} work
 */
CncPreorder.prototype.addWork = function(work)
{
    work.isForPart()
        ? this.$$worksForPart[work.part_replica_id].push(work)
        : this.$$additionalWorks.push(work);
};

/**
 *
 * @return {string|undefined}
 */
CncPreorder.prototype.getCurrency = function()
{
    var work = _.first(this.works);
    return work ? work.currency : undefined;
};

/**
 *
 * @return {string|undefined}
 */
CncPreorder.prototype.isDeclined = function()
{
    return this.status === 'rejected';
};

/**
 *
 * @param {CncPreorder} preorder
 * @return {string[]}
 */
CncPreorder.validate = function (preorder)
{
    var errors = [];

    preorder.works.forEach(function (work) {
        if (work.$$isEdit) {
            errors.push(_t('site.cnc', "Please complete editing the quote"));
        }
        if(!work.id){
            errors.push(_t('site.cnc', "Please complete editing the quote"));
        }
    });
    preorder.$$worksForPart.forEach(function (works) {
        works.forEach(function(work) {
            if (work.$$isEdit) {
                errors.push(_t('site.cnc', "Please complete editing the quote or delete unsaved items"));
            }
        });
    });
    preorder.$$additionalWorks.forEach(function (work) {
        if (work.$$isEdit) {
            errors.push(_t('site.cnc', "Please complete editing additional works or delete unsaved items"));
        }
    });
    preorder.presets.forEach(function (preset) {
        if (preset.$$isEdit) {
            errors.push(_t('site.cnc', "Please complete editing the quote or delete unsaved items"));
        }
    });

    return _.uniq(errors);
};


/**
 * @property {int} id
 * @property {string} title
 * @property {int} part_replica_id
 * @property {number} cost
 * @property {string} currency
 * @property {string} per
 * @property {string} source
 * @property {string} checker
 *
 * @property {bool} $$isEdit
 * @property {bool} $$loading
 *
 * @param {*} [data]
 * @constructor
 */
function CncPreorderWork(data)
{
    angular.extend(this, data);
}

CncPreorderWork.PER_PART = 'part';
CncPreorderWork.PER_BATCH = 'batch';
CncPreorderWork.SOURCE_MANUAL = 'manual';

/**
 *
 * @param {CncPreorderWork} work
 * @return {string[]}
 */
CncPreorderWork.validate = function (work)
{
    var errors = [];

    if (!work.title) {
        errors.push(_t('site.cnc', "Fill title"));
    }

    if (!work.cost) {
        errors.push(_t('site.cnc', "Enter a cost"));
    }

    return errors;
};

/**
 *
 * @return {boolean}
 */
CncPreorderWork.prototype.perBatch = function ()
{
    return this.per === CncPreorderWork.PER_BATCH;
};

/**
 *
 * @return {boolean}
 */
CncPreorderWork.prototype.perPart = function ()
{
    return this.per === CncPreorderWork.PER_PART;
};

/**
 *
 * @return {boolean}
 */
CncPreorderWork.prototype.isForPart = function ()
{
    return !!this.part_replica_id;
};


/**
 *
 * @return {boolean}
 */
CncPreorderWork.prototype.isNew = function ()
{
    return !this.id;
};


/**
 * @property {int} id
 * @property {int} part_replica_id
 * @property {string} material
 * @property {int} qty
 * @property {string[]} postprocessing
 * @property {bool} $$isEdit
 *
 * @param {*} data
 * @constructor
 */
function CncPreorderPreset(data)
{
    angular.extend(this, data);
}

/**
 * @property {string} name
 * @property {string} description
 * @property {string} estimateTime
 *
 * @param {*} data
 * @constructor
 */
function CncPreorderRequest(data)
{
    angular.extend(this, data);
}


/**
 * @property {int} id
 * @property {string} name
 *
 * @param {*} data
 * @constructor
 */
function FileModel(data)
{
    angular.extend(this, data);
}