"use strict";


app.directive('filesList', function($http, $notifyHttpErrors, $q, preorderId)
{
    return {
        restrict : 'E',
        templateUrl : '/app/cnc/files-list.html',
        scope : {
            title :'@',
            files : '=',
            editable : '='
        },

        link : function ($scope)
        {
            /**
             *
             */
            var checkEditable = function ()
            {
                if(!$scope.editable) {
                    throw new Error("File list cannot be edited");
                }
            };

            /**
             *
             * @type {Array}
             */
            $scope.uploads = [];


            /**
             * Download additional or part file
             * @param {int} fileId
             */
            $scope.downloadFile = function (fileId)
            {
                window.open('/workbench/cncm/preorder/download-file?preorderId=' + preorderId + '&fileId=' + fileId, '_self');
            };

            /**
             *
             * @param {FileModel} file
             */
            $scope.removeFile = function (file)
            {
                checkEditable();

                $http.post('/workbench/cncm/preorder/remove-file', {}, {params : {preorderId : preorderId, fileId : file.id}})
                    .then(function () { _.remove($scope.files, file); }, $notifyHttpErrors);
            };

            /**
             *
             * @param {File} file
             */
            $scope.uploadFile = function (file)
            {
                checkEditable();

                var uploadObject = {name : file.name};
                $scope.uploads.push(uploadObject);

                $http.post('/workbench/cncm/preorder/upload-file', {file : file}, {params : {preorderId : preorderId}})
                    .then(function (response) {$scope.files.push(new FileModel(response.data));}, $notifyHttpErrors)
                    .finally(function () {
                        _.remove($scope.uploads, uploadObject);
                    })
            }
        }
    };
});