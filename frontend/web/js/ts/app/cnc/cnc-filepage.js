"use strict";


app.controller("CncFilepageController", function ($scope, $q, $http, $notifyHttpErrors, $modal, $router, $user, $cncService, $sce, $preorderService,modelId, machineId) {
    /**
     * Costing bundle
     * @type {CostingBundle}
     */
    $scope.isLoading = true;

    /**
     * Model
     * @type {Model}
     */
    $scope.model = undefined;

    /**
     * CNC machine
     * @type {CncMachine}
     */
    $scope.machine = undefined;


    /**
     * HashMap of presets by part id
     * @type {Preset[]}
     */
    $scope.presets = {};

    /**
     * Costing bundle
     * @type {CostingBundle}
     */
    $scope.costingBundle = undefined;

    /**
     * Analyze errors map
     * @type {String[]}
     */
    $scope.analyzeErrors = {};

    var ctrl = this;

    /**
     * Map current costing promises by part
     * @type {Promise[]}
     */
    ctrl.costingRequests = {};

    /**
     *
     * @param part
     */
    ctrl.createPresetWatcher = function (part) {
        $scope.$watch('presets[' + part.id + ']', function (preset) {
            /** @var preset Preset */
            $scope.costingBundle.resetCosting(part);

            var promise = $cncService.costing(part, machineId, preset);

            ctrl.costingRequests[part.id] = promise;

            promise
                .then(
                    function (response) {
                        if (ctrl.costingRequests[part.id] !== promise) {
                            return;
                        }
                        $scope.costingBundle.putCosting(part, response)
                    },
                    function (response) {
                        var message = $sce.trustAsHtml(commonJs.prepareMessage(response.errors || response.message));
                        $scope.costingBundle.putError(part, message)
                    }
                );
        }, true);
    };


    $q
        .all([
            $http.get('/workbench/cncm/filepage/machine', {params: {machineId: machineId}}),
            $http.get('/workbench/cncm/filepage/model', {params: {modelId: modelId}})
        ])
        .then(function (responses) {

            $scope.isLoading = false;

            $scope.machine = new CncMachine(responses[0].data);
            $scope.model = new Model(responses[1].data);
            $scope.costingBundle = new CostingBundle($scope.model.parts);

            // create presets
            $scope.model.parts.forEach(function (part) {
                var preset = new Preset();
                preset.count = 1;
                preset.material = $scope.machine.settings.defaultMaterial;
                preset.postprocessing = [];
                $scope.presets[part.id] = preset;
            });

            // load cncParams  for parts
            $scope.model.parts.forEach(function (part) {
                if (part.cncParams) {
                    ctrl.createPresetWatcher(part);
                    return;
                }

                $cncService.cncParams(part)
                    .then(
                        function (response) {
                            part.cncParams = response;
                            ctrl.createPresetWatcher(part);
                        },
                        function (response) {
                            $scope.analyzeErrors[part.id] = response.data.message;
                            $notifyHttpErrors(response);
                        }
                    );
            });

        }, $notifyHttpErrors);


    /**
     * Create preorder
     */
    $scope.openPreorderModal = function () {
        var modalScope = $scope.$new(true);

        /**
         * Preorder form
         * @type {*}
         */

        modalScope.preorder = {
            totalQty: $scope.getTotalPresetQty(),
            budget: $scope.costingBundle.getTotal(),
            currency: $scope.machine.currency,
            isGuest: TS.User.isGuest
        };

        /**
         * Create preorder and redirect to preorder view
         * @return {Promise}
         */
        modalScope.create = function () {

            var data = {
                modelId: modelId,
                machineId: machineId,
                presets: $scope.presets,
                preorder: this.preorder
            };

            return $http.post('/workbench/cncm/preorder/create', data)
                .then(function (response) {
                    if (window.top)
                        window.top.location.href = '/workbench/preorder/view-customer?preorderId=' + response.data.id;
                    else
                        $router.to('/workbench/preorder/view-customer?preorderId=' + response.data.id)
                })
                .catch($notifyHttpErrors)
        };

        $modal.open({
            template: '/app/cnc/create-preorder-modal.html',
            scope: modalScope
        });
    };

    $scope.getTotalPresetQty = function () {
        var totalQty = 0;
        for (var partId in $scope.presets) {
            var part = $scope.presets[partId];
            totalQty = part.count + totalQty;
        }
        return totalQty;
    };
    /**
     *
     * @param priceElement
     * @param preset
     * @return {number}
     */
    $scope.getPreiceElementCount = function (priceElement, preset) {
        return priceElement.isPerPart ? preset.count : 1;
    };

    /**
     *
     * @return {number}
     */
    $scope.getTotalPriceElementCost = function (priceElement, preset) {
        return priceElement.price * $scope.getPreiceElementCount(priceElement, preset);
    };

    $scope.openCreatePreorder = function(psId, serviceId)
    {
        if($scope.model && $scope.model.parts && Array.isArray($scope.model.parts)){
            return $preorderService.openCreatePreorderWithFile(psId, serviceId,$scope.model.parts.map(function (p){
                return p.fileUuid;
            }));
        }
        return $preorderService.openCreatePreorder(psId, null, null, serviceId);
    };
})
    .factory('$cncService', function ($http, $q, $timeout) {
        var $cncService = {};

        /**
         * @param {ModelPart} part
         */
        $cncService.cncParams = function (part) {
            var load = function () {
                return $http.get('/workbench/cncm/filepage/cnc-params', {params: {partId: part.uid}})
                    .then(onSuccessRequest, onErrorRequest)
            };

            var defer = $q.defer();

            var onSuccessRequest = function (response) {
                if (response.data) {
                    var cncParams = new CncParams(response.data);
                    defer.resolve(cncParams);
                    return;
                }

                $timeout(load, 5000);
            };

            var onErrorRequest = function (response) {
                defer.reject(response);
            };

            load();
            return defer.promise;
        };

        /**
         *
         * @param {ModelPart} part
         * @param {int} machineId
         * @param {Preset} preset
         */
        $cncService.costing = function (part, machineId, preset) {
            return $http.post('/workbench/cncm/filepage/costing', preset, {
                params: {
                    partId: part.uid,
                    machineId: machineId
                }
            })
                .then(
                    function (response) {
                        return $q.resolve(response.data);
                    },
                    function (response) {
                        return $q.reject(response.data);
                    }
                );
        };

        /**
         *
         * @param {ModelPart} part
         * @param {int} machineId
         * @param {Preset} preset
         * @param {*} costing
         */
        $cncService.modeling = function (part, machineId, preset, costing) {
            var data = {
                preset: preset,
                costing: costing
            };

            return $http.post('/workbench/cncm/filepage/modeling', data, {
                params: {
                    partId: part.uid,
                    machineId: machineId
                }
            })
                .then(function (response) {
                    return $q.resolve(response.data);
                });
        };

        return $cncService;
    });