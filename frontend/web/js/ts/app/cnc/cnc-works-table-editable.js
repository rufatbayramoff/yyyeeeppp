"use strict";


app.directive('worksTableEditable', function($http, $user, $notify, $cncCostCalculator, $notifyHttpErrors, preorderId)
{
    return {
        restrict : 'E',
        templateUrl : '/app/cnc/works-table-editable.html',
        scope : {
            title :'@',
            works : '=',
            preset : '=',
            preorder : '=',
            editable : '=',
            showQtyColumn : '=?',
            addButtonTitle : '@?'
        },

        /**
         *
         * @param {{preset : CncPreorderPreset, works : CncPreorderWork[], editable : bool, preorder : CncPreorder,
         *      getWorkQty, addWork, deleteWork, editWork, saveWork, getTotalCost, getCurrency, getWorkCost }} $scope
         */
        link : function ($scope)
        {

            if($scope.showQtyColumn === undefined) {
                $scope.showQtyColumn = true;
            }

            if($scope.addButtonTitle === undefined) {
                $scope.addButtonTitle = _t('site.cnc', 'Add item');
            }


            /**
             *
             */
            var checkEditable = function ()
            {
                if(!$scope.editable) {
                    throw new Error("Works table list not editable");
                }
            };


            /**
             * @param {CncPreorderWork} work
             * @return {integer}
             */
            $scope.getWorkQty = function (work)
            {
                return $scope.preset && work.perPart() ? $scope.preset.qty : 1;
            };

            /**
             * @param {CncPreorderWork} work
             * @return {number}
             */
            $scope.getWorkCost = function (work)
            {
                return $cncCostCalculator.costForWork(work, $scope.preset);
            };

            /**
             *
             */
            $scope.addWork = function ()
            {
                checkEditable();

                var work = new CncPreorderWork();
                work.$$isEdit = true;
                work.per = CncPreorderWork.PER_BATCH;
                work.source = CncPreorderWork.SOURCE_MANUAL;
                work.currency = $user.currency;

                if ($scope.preset) {
                    work.part_replica_id = $scope.preset.part_replica_id;
                }
                $scope.preorder.addWork(work);
            };

            /**
             *
             * @param {CncPreorderWork} work
             */
            $scope.deleteWork = function (work)
            {
                checkEditable();

                work.$$loading = true;
                if(work.id) {
                    $http.post('/workbench/cncm/preorder/delete-work', {}, {params: {workId: work.id, preorderId: preorderId}})
                        .then(function () {
                            $scope.preorder.deleteWork(work);
                        }, $notifyHttpErrors)
                        .finally(function () {
                            work.$$loading = undefined;
                        });
                }else{
                    $scope.preorder.deleteWork(work);
                }
            };

            /**
             *
             * @param {CncPreorderWork} work
             */
            $scope.editWork = function (work)
            {
                checkEditable();

                work.$$isEdit = true;
            };

            /**
             *
             * @param {CncPreorderWork} work
             */
            $scope.saveWork = function (work)
            {
                checkEditable();

                var errors = CncPreorderWork.validate(work);
                if (errors.length) {
                    $notify.error(errors);
                    return;
                }

                work.$$loading = true;
                var uri = work.isNew() ? '/workbench/cncm/preorder/add-work' : '/workbench/cncm/preorder/update-work';

                $http.post(uri, work, {params : {workId : work.id, preorderId : preorderId}})
                    .then(function (response) {
                            angular.extend(work, response.data);
                            work.$$isEdit = false;
                        }, $notifyHttpErrors)
                    .finally(function () {
                        work.$$loading = undefined;
                    });
            };

            /**
             *
             * @return {number}
             */
            $scope.getTotalCost = function ()
            {
                return $cncCostCalculator.costForWorks($scope.works, $scope.preset);
            };
        }

    };
});