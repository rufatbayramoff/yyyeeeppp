"use strict";


app.factory('$cncCostCalculator', function ()
{
    var $cncCostCalculator = {};

    /**
     *
     * @param {CncPreorderWork} work
     * @param {CncPreorderPreset} [preset]
     * @return {number}
     */
    $cncCostCalculator.costForWork = function(work, preset)
    {
        if (work.perPart() && !preset) {
            throw new Error("Work not specified for part")
        }

        return work.perPart() ? work.cost * preset.qty : work.cost;
    };

    /**
     *
     * @param {CncPreorderWork[]} works
     * @param {CncPreorderPreset} [preset]
     * @return {number}
     */
    $cncCostCalculator.costForWorks = function(works, preset)
    {
        return works.reduce(function (totalCost, work) {
            return totalCost + $cncCostCalculator.costForWork(work, preset);
        }, 0);
    };

    /**
     *
     * @param {CncPreorder} preorder
     * @return {number}
     */
    $cncCostCalculator.costForPreorder = function(preorder)
    {
        var totalCost = 0;

        preorder.replica.parts.forEach(function (part) {
            totalCost += $cncCostCalculator.costForWorks(preorder.getWorksForPart(part), preorder.getPresetForPart(part));
        });

        totalCost +=  $cncCostCalculator.costForWorks(preorder.getAddtionalWorks());

        return totalCost;
    };

    return $cncCostCalculator;
});