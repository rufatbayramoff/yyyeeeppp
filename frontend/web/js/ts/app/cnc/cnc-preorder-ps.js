"use strict";

/**
 * TODO move CncPreorder to resource
 */
app.controller("CncPreorderPsController", function ($modal, $router, $q, $scope, $http, $notifyHttpErrors, $notify, $cncCostCalculator, preorderId, machineId, isCustomer, $cncService2)
{
    /**
     * Costing bundle
     * @type {CostingBundle}
     */
    $scope.isLoading = true;

    /**
     *
     * @type {bool}
     */
    $scope.isCustomer = isCustomer;

    $q
        .all([
            $http.get('/workbench/cncm/filepage/machine', {params : {machineId : machineId}}),
            $http.get('/workbench/cncm/preorder/preorder', {params : {preorderId : preorderId}})
        ])
        .then(function (responses) {

            $scope.isLoading = false;
            $scope.machine = new CncMachine(responses[0].data);
            $scope.preorder = new CncPreorder(responses[1].data);

        }, $notifyHttpErrors);

    /**
     * Return totla cost of preorder
     * @return {number}
     */
    $scope.getTotalCost = function()
    {
        return $cncCostCalculator.costForPreorder($scope.preorder);
    };

    /**
     * Download additional or part file
     * @param {int} fileId
     */
    $scope.downloadFile = function (fileId)
    {
        window.open('/workbench/cncm/preorder/download-file?preorderId=' + preorderId + '&fileId=' + fileId, '_self');
    };

    /**
     *
     * @param preset
     */
    $scope.editPreset = function (preset)
    {
        preset.$$isEdit = true;
    };

    /**
     *
     * @param preset
     */
    $scope.savePreset = function (preset)
    {
        return $http.post('/workbench/cncm/preorder/update-preset', preset, {params : {preorderId : preorderId, presetId : preset.id}})
            .then(function (response) {
                angular.extend(preset, response.data);
                preset.$$isEdit = undefined;
            }, $notifyHttpErrors);
    };

    /**
     *
     * @return {string}
     */
    $scope.getDialogUrl = function ()
    {
        if($scope.isSelfPreorder) {
            throw new Error("Dialogue not possible");
        }

        return '/workbench/messages/embed-topic?bindId=' + $scope.preorder.id + '&bindTo=cnc_preorder';
    };

    /**
     *
     * @return {bool}
     */
    $scope.isEditable = function ()
    {
        return !isCustomer && $scope.preorder && $scope.preorder.isEditable;
    };

    /**
     *
     */
    $scope.isCanMakeOffer = function ()
    {
        return !isCustomer && $scope.preorder.isWaitPsOffer;
    };

    /**
     *
     */
    $scope.makeOffer = function ()
    {
        var errors = CncPreorder.validate($scope.preorder);

        if(errors.length) {
            $notify.error(errors);
            return;
        }
        return $http.post('/workbench/cncm/preorder/make-offer', {}, {params : {preorderId : preorderId}})
            .then(function (response) {
                $scope.preorder = new CncPreorder(response.data);
                $notify.info(_t('site.cnc', 'Your offer has been submitted to the customer'));
            }, $notifyHttpErrors);
    };

    /**
     *
     */
    $scope.decline = function ()
    {
        $cncService2.openDeclineModal(preorderId, true, isCustomer)
            .then(function (preorder) {
                $scope.preorder = preorder;
                $notify.info(_t('site.cnc', 'Declined'));
            }, function (response) {
                if (response) {
                    $notifyHttpErrors(response);
                }
            });
    };


    /**
     *
     */
    $scope.isCanAcceptOffer = function ()
    {
        return isCustomer && $scope.preorder.isWaitCustomerAccepting;
    };

    /**
     *
     */
    $scope.acceptOffer = function ()
    {
        return $http.post('/workbench/cncm/preorder/accept', {}, {params : {preorderId : preorderId, preorderVersion : $scope.preorder.version}})
            .then(function (response) {
                $router.toInvoicePayment(response.data.invoiceUuid);
            }, $notifyHttpErrors);
    };


    $scope.openReturnToServiceModal = function ()
    {
        $modal.open({
            template : '/app/cnc/return-to-service-modal.html',
            scope : {
                comment : '',
                returnToService : function()
                {
                    var modalScope = this;

                    return $http.post('/workbench/cncm/preorder/return-to-service', {comment : modalScope.comment}, {params : {preorderId : preorderId}})
                        .then(function (response) {
                            $scope.preorder = new CncPreorder(response.data);
                            $notify.info(_t('site.cnc', 'Successful return to service'));
                            modalScope.$dismiss();
                        }, $notifyHttpErrors);
                },
            }
        });
    };
});