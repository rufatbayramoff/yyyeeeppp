"use strict";
app.controller('PreorderViewCustomreController', function ($scope, $preorderService, $notifyHttpErrors, $router, $q, $modal) {


    $scope.repeatPreorder = function (preorderId) {
        return $modal.confirm(_t('site.preorder', 'Are you sure you want to repeat your request for a quote?'), _t('site', 'Yes'), _t('site', 'No'))
            .then(function () {
                return $preorderService.repeatPreorder(preorderId, false)
            })
            .then(function (response) {
                    if (response.data.message) {
                        new TS.Notify({
                            type: 'success',
                            text: response.data.message,
                            automaticClose: false
                        });
                    }
                    setTimeout(function () {
                        $router.reload();
                    }, 1500)
                }
                , function (response) {
                    response ? $notifyHttpErrors(response) : '';
                    return $q.reject(response)
                });
    };

    /**
     *
     * @param preorderId
     */
    $scope.declinePreorder = function (preorderId) {
        return $preorderService.declineOfferByClient(preorderId)
            .then($router.toMyPreorders, function (response) {
                response ? $notifyHttpErrors(response) : '';
                return $q.reject(response)
            });
    };

    /**
     *
     * @param preorderId
     */
    $scope.cancelPreorder = function (preorderId) {
        return $modal.confirm(_t('site.preorder', 'Are you sure you want to cancel your request for a quote?'), _t('site', 'Yes'), _t('site', 'No'))
            .then(function () {
                return $preorderService.cancelByClient(preorderId)
            })
            .then($router.toMyPreorders, function (response) {
                response ? $notifyHttpErrors(response) : '';
                return $q.reject(response)
            });
    };

    $scope.deletePreorder = function(preorderId) {
        return $modal.confirm(_t('site.preorder', 'Are you sure you want to delete it?'), _t('site', 'Yes'), _t('site', 'No'))
            .then(function () {
                return $preorderService.deleteByClient(preorderId)
            })
            .then($router.toMyPreorders, function (response) {
                response ? $notifyHttpErrors(response) : '';
                return $q.reject(response)
            });
    }

});