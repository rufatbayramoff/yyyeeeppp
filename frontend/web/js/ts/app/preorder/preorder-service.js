"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.factory('$preorderService', function ($http, $q, $modal, $user, $notifyHttpErrors, controllerParams, $notify, $router) {
    var $preorderService = {};

    $preorderService.openCreatePreorder = function (psId, inlinePreorder, productUuid, serviceId, description, uploadedFilesUuids) {
        var url = new URI($router.getCreateRequestForQuote());
        url.addQuery('psId', psId);
        if (productUuid) {
            url.addQuery('productUuid', productUuid);
        }
        if (serviceId) {
            url.addQuery('serviceId', serviceId);
        }
        if (inlinePreorder) {
            if (inlinePreorder.budget) {
                url.addQuery('budget', inlinePreorder.budget);
            }
            if (inlinePreorder.currency) {
                url.addQuery('currency', inlinePreorder.currency);
            }
            if (inlinePreorder.estimateTime) {
                url.addQuery('estimateTime', inlinePreorder.estimateTime);
            }
            if (inlinePreorder.description) {
                url.addQuery('description', inlinePreorder.description);
            }
        }
        if (description) {
            url.addSearch('description', description);
        }
        if (uploadedFilesUuids) {
            url.addSearch('uploadedFilesUuids', uploadedFilesUuids);
        }

        $router.to(url);
    };

    $preorderService.openCreatePreorderWithFile = function (psId, serviceId, fileUuids) {
        var url = new URI($router.getCreateRequestForQuote());
        url.addQuery('psId', psId);
        url.addQuery('serviceId', serviceId);
        if (fileUuids.length) {
            url.addQuery('uploadedFilesUuids', fileUuids.join('-'));
        }
        $router.to(url);
    };

    /**
     *
     * @param {PreorderForm} preorder
     */
    $preorderService.create = function (preorder) {
        return $http.post($router.getCreateRequestForQuoteAjax(), preorder)
            .then(function (response) {
                if (response.data.success) {
                    return $q.resolve(new Preorder(response.data.preorder));
                } else {
                    return $q.reject();
                }
            });
    };

    /**
     *
     * @param {Preorder} preorder
     * @param {PreorderWork} work
     */
    $preorderService.saveWork = function (preorder, work) {
        work.$$loading = true;
        var url = work.id ? '/workbench/preorder/update-work' : '/workbench/preorder/add-work';
        return $http.post(url, work, {params: {preorderId: preorder.id, workId: work.id}})
            .then(function (response) {
                var updatedWork = new PreorderWork(response.data);
                var index = preorder.works.indexOf(work);
                preorder.works[index] = updatedWork;
                return $q.resolve(updatedWork);
            })
            .finally(function () {
                work.$$loading = false;
            });
    };

    /**
     *
     * @param {Preorder} preorder
     * @param {PreorderWork} work
     */
    $preorderService.deleteWork = function (preorder, work) {

        if (!work.id) {
            _.remove(preorder.works, work);
            return $q.resolve();
        }

        work.$$loading = true;
        return $http.post('/workbench/preorder/delete-work', work, {params: {preorderId: preorder.id, workId: work.id}})
            .then(function () {
                _.remove(preorder.works, work);
            })
            .finally(function () {
                work.$$loading = false;
            });
    };

    /**
     *
     * @param {Preorder} preorder
     * @param {PreorderFile} file
     */
    $preorderService.getDownloadFileLink = function (preorder, file) {
        if (!file.id) {
            return undefined;
        }
        return '/workbench/preorder/download-preorder-file?preorderId=' + preorder.id + "&fileId=" + file.id;
    };

    /**
     *
     * @param {Preorder} preorder
     * @param {PreorderWork} work
     * @param {PreorderFile} file
     */
    $preorderService.getDownloadWorkFileLink = function (preorder, work, file) {
        if (!work.id || !file.id) {
            return undefined;
        }
        return '/workbench/preorder/download-work-file?preorderId=' + preorder.id + "&workId=" + work.id + "&fileId=" + file.id;
    };

    /**
     *
     * @param {Preorder} preorder
     */
    $preorderService.makeOffer = function (preorder) {
        var defer = $q.defer();

        var errors = preorder.validateMakeOffer();

        if (errors.length) {
            defer.reject({data: {errors: errors}});
            return defer.promise;
        }

        function innerMakeOffer() {
            var unsavedWorksPromises = preorder.works
                .filter(function (work) {
                    return work.$$isEdit;
                })
                .map(function (work) {
                    return $preorderService.saveWork(preorder, work)
                });
            $q.all(unsavedWorksPromises).then(function () {
                $http.post('/workbench/preorder/make-offer', {'Preorder': preorder}).then(function (response) {
                    defer.resolve(new Preorder(response.data));
                }).catch(function (response) {
                    defer.reject(response);
                })

            }, defer.reject);
        }

        if (preorder.offered) {
            $modal.confirm(_t('site.preorder', 'Are you sure you want to submit the updated offer?'), _t('site', 'Yes'), _t('site', 'No'))
                .then(function () {
                    innerMakeOffer();
                }, defer.reject);
        } else {
            innerMakeOffer();
        }

        return defer.promise;
    };

    /**
     *
     * @param {Preorder} preorder
     */
    $preorderService.saveDraft = function (preorder) {
        var defer = $q.defer();

        var unsavedWorksPromises = preorder.works
            .filter(function (work) {
                return work.$$isEdit;
            })
            .map(function (work) {
                return $preorderService.saveWork(preorder, work)
            });
        $q.all(unsavedWorksPromises).then(function () {
            $http.post('/workbench/preorder/save-draft', {'Preorder': preorder}).then(function (response) {
                defer.resolve(new Preorder(response.data));
            }).catch(function (response) {
                defer.reject({data: {errors: response.data.message}});
            })

        }, defer.reject);
        return defer.promise;
    };

    /**
     *
     * @param {Preorder} preorder
     */
    $preorderService.withdrawOffer = function (preorder) {
        var defer = $q.defer();

        var unsavedWorksPromises = preorder.works
            .filter(function (work) {
                return work.$$isEdit;
            })
            .map(function (work) {
                return $preorderService.saveWork(preorder, work)
            });
        $q.all(unsavedWorksPromises).then(function () {
            $http.post('/workbench/preorder/withdraw-offer', {'Preorder': preorder}).then(function (response) {
                defer.resolve(new Preorder(response.data));
            }).catch(function (response) {
                defer.reject({data: {errors: response.data.message}});
            })

        }, defer.reject);
        return defer.promise;
    };

    $preorderService.repeatPreorder = function (preorderId) {
        var defer = $q.defer();
        $http.post('/workbench/preorder/repeat', {preorderId: preorderId})
            .then(
                function (response) {
                    defer.resolve(response);
                },
                function (response) {
                    defer.reject(response);
                }
            );
        return defer.promise;
    };

    $preorderService.declineOfferByClient = function (preorderId) {
        let declineReasons = controllerParams.preorderCustomerDeclineReasons;

        var defer = $q.defer();

        var makeRequest = function (declineForm) {
            if (!declineForm) {
                declineForm = {};
            }
            declineForm.preorderId = preorderId;

            return $http.post('/workbench/preorder/decline-offer-by-client', declineForm)
                .then(
                    function (response) {
                        defer.resolve(response);
                        return $q.resolve(response)
                    },
                    function (response) {
                        defer.reject(response);
                        return $q.reject(response)
                    }
                );
        };

        $modal.open({
            template: '/app/ps/orders/order-decline-modal.html',
            onClose: function () {
                defer.reject()
            },
            controller: function ($scope) {
                $scope.declineReasons = declineReasons;

                $scope.form = {
                    reasonId: undefined,
                    reasonDescription: undefined
                };

                $scope.validateForm = function () {
                    var errors = [];
                    if (!$scope.form.reasonId) {
                        errors.push(_t('site.ps', 'Please select a decline reason'));
                    }
                    return errors;
                };


                $scope.decline = function () {
                    var errors = $scope.validateForm();

                    if (!_.isEmpty(errors)) {
                        $notify.error(errors);
                        return false;
                    }

                    return makeRequest($scope.form).then(function () {
                        $scope.$dismiss();
                    }, function (response) {
                        $scope.$dismiss();
                    });
                };
            }
        });

        return defer.promise;
    };

    $preorderService.cancelByClient = function (preorderId) {
        return $http.post('/workbench/preorder/cancel-offer-by-client', {preorderId: preorderId});
    };

    $preorderService.deleteByClient = function (preorderId) {
        return $http.post('/workbench/preorder/delete-offer-by-client', {preorderId: preorderId});
    };

    /**
     *
     * @param {int} preorderId
     * @param {boolean} [showDeclineModal]
     * @param {objects} declineReasons
     */
    $preorderService.declinePreorder = function (preorderId, showDeclineModal, declineReasons) {

        declineReasons = declineReasons || controllerParams.preorderDeclineReasons;

        var defer = $q.defer();

        var makeRequest = function (declineForm) {
            if (!declineForm) {
                declineForm = {};
            }

            return $http.post('/workbench/preorder/decline', declineForm, {params: {preorderId: preorderId}})
                .then(
                    function (response) {
                        defer.resolve(new Preorder(response.data));
                        return $q.resolve(response)
                    },
                    function (response) {
                        defer.reject(response);
                        return $q.reject(response)
                    }
                );
        };

        if (!showDeclineModal) {
            makeRequest();
        } else {
            $modal.open({
                template: '/app/ps/orders/order-decline-modal.html',
                onClose: function () {
                    defer.reject()
                },
                controller: function ($scope) {
                    $scope.declineReasons = declineReasons;

                    $scope.form = {
                        reasonId: undefined,
                        reasonDescription: undefined
                    };

                    $scope.validateForm = function () {
                        var errors = [];
                        if (!$scope.form.reasonId) {
                            errors.push(_t('site.ps', 'Please select a decline reason'));
                        }
                        return errors;
                    };


                    $scope.decline = function () {
                        var errors = $scope.validateForm();

                        if (!_.isEmpty(errors)) {
                            $notify.error(errors);
                            return false;
                        }

                        return makeRequest($scope.form).then(function () {
                            $scope.$dismiss();
                        }, function (response) {
                            $scope.$dismiss();
                        });
                    };
                }
            });


        }
        return defer.promise;
    };

    $preorderService.deletePreorder = function (preorderId) {
        $http.post('/workbench/service-order/delete-draft-preorder-by-ps', {'preorderId': preorderId})
            .then(function () {
                $router.toPsQuotesRequests();
            })
            .catch($notifyHttpErrors);
    };


    return $preorderService;
});