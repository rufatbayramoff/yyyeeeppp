"use strict";

app.controller('RequestQuoteController', ['$scope', '$router', '$http', '$user', '$timeout', '$notify', '$notifyHttpErrors', '$preorderService', '$q', 'controllerParams', function ($scope, $router, $http, $user, $timeout, $notify, $notifyHttpErrors, $preorderService, $q, controllerParams) {
    $scope.preorder = new PreorderForm(controllerParams.preorderForm);
    $scope.unitTypes = controllerParams.unitTypes;
    $scope.form = {acceptTerms: false};
    $scope.isGuest = $user.isGuest;

    $scope.isShowName = function () {
        return !$scope.preorder.product && !$scope.preorder.createdByPs;
    };
    $scope.isShowSelectClient = function () {
        return $scope.preorder.createdByPs && !$scope.preorder.product && !$scope.preorder.companyService;
    };

    $scope.isShowMessageToManufacturer = function () {
        return !$scope.preorder.createdByPs;
    };

    $scope.isShowProduct = function () {
        return !!$scope.preorder.product;
    };

    $scope.isShowCompanyService = function () {
        return !!$scope.preorder.companyService;
    };

    $scope.isShowQuantity = function () {
        return !!$scope.preorder.product;
    };
    $scope.isShowTotalPrice = function () {
        return !!$scope.preorder.product;
    };
    $scope.isShowShipTo = function () {
        return !!$scope.preorder.product;
    };
    $scope.isShowProjectDescription = function () {
        return !$scope.preorder.product;
    };
    $scope.isShowAttachFiles = function () {
        return !$scope.preorder.product;
    };
    $scope.isShowBudget = function () {
        return !$scope.preorder.product;
    };
    $scope.initClientsAutocomplete = function () {
        var elem = $('#clientNameAutocomplete');
        if (elem.length) {
            elem.autocomplete({
                minLength: controllerParams.minClientNameLen,
                source: $router.getQuoteClientsAutocompleteSource(),
                select: function (event, ui) {
                    $scope.preorder.clientId = ui.item.id;
                    $scope.preorder.name = ui.item.value; // save selected id to hidden input
                    $scope.$apply();
                    elem.val(ui.item.value); // display the selected text
                    return false;
                },
                change: function (event, ui) {
                    if (ui.item) {
                        elem.val(ui.item.value); // display the selected text
                        $scope.preorder.clientId = (ui.item ? ui.item.id : null);
                        $scope.preorder.name = ui.item.value;
                        $scope.$apply();
                    }
                },
            });
        }
    };

    $scope.removeFile = function (index) {
        $scope.preorder.removeFile(index);
    };

    $scope.removeExistFile = function (index) {
        $scope.preorder.removeExistFile(index);
    };

    $scope.getUnitTypeMeasurement = function () {
        var result;
        if ($scope.preorder.quantity > 1) {
            result = $scope.unitTypes[$scope.preorder.product.unitTypeLabel];
        } else {
            result = $scope.preorder.product.unitTypeLabel;
        }
        return result;
    };

    $scope.setCurrency = function (currency) {
        $scope.preorder.currency = currency;
    };

    $scope.submitForm = function () {
        if (!document.querySelector('#preorderForm').checkValidity()) {
            return;
        }

        if ($user.isGuest) {
            if (!$scope.form.acceptTerms) {
                $notify.error("Please agree to our Terms and Conditions");
                return;
            }

            if (!$scope.preorder.email) {
                $notify.error("Please enter an email address.");
                return;
            }
        }

        if (typeof grecaptcha !== 'undefined') {
            $scope.preorder['g-recaptcha-response'] = grecaptcha.getResponse();
        }

        return $preorderService.create($scope.preorder)
            .then(
                function (preorder) {
                    if ($scope.preorder.createdByPs) {
                        $router.to($router.getViewPreorderOrderPs(preorder.id));
                    } else {
                        $router.to($router.getViewPreorderOrderCustomer(preorder.id));
                    }
                },
                function (response) {
                    $notifyHttpErrors(response);
                    return $q.reject(response);
                }
            );
    };

    if ($scope.preorder.createdByPs) {
        $timeout(function () {
            $scope.initClientsAutocomplete();
        }, 100);
    }

    $("body").on('setPreorderAddress', function (event, location) {
        $scope.preorder.shipTo.setAddressFromLocation(location);
    });
}
]);