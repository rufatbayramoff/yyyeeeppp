"use strict";

/**
 * Controller for manage printservice printer materials
 */
app.factory('$preorderFee', function ($http, $q, $user, $notifyHttpErrors, controllerParams, $notify, $router) {
    var $preorderFee = {};
    $preorderFee.findPercent = function (totalPrice, currency) {
        var defer = $q.defer();
        $http.get($router.getPreorderFeeCalc(), {'params': {'totalPrice': totalPrice, 'currency': currency}})
            .then(function (response) {
                defer.resolve(response['data']['youWillGet']);
            })
            .catch(defer.reject);
        return defer.promise;
    };

    return $preorderFee;
});