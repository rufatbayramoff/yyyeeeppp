"use strict";

/**
 * @property {int} psId
 * @property {String} name
 * @property {int} clientId
 * @property {String} description
 * @property {File[]} files
 * @property {File[]} existsFiles
 * @property {String} message
 * @property {number} budget
 * @property {number} estimateTime
 * @property {String} email
 * @property {UserAddress} shipTo
 * @property {String} quantity
 * @property {Product} product
 * @property {bool} createdByPs
 * @property {CompanyService} companyService
 *
 * @constructor
 */
function PreorderForm(data) {
    if (data) {
        angular.extend(this, data);
    }
    if (data['product']) {
        this['product'] = new Product(data['product']);
    }
    if (data['shipTo']) {
        this['shipTo'] = new UserAddress(data['shipTo']);
    }
    if (data['companyService']) {
        this['companyService'] = new CompanyService(data['companyService']);
    }
}

PreorderForm.prototype.totalPriceFormatted = function (data) {
    if (!this.quantity) {
        return '';
    }
    if (this.product) {
        return this.currency +' '+ this.product.getTotalPrice(this.quantity).toFixed(2);
    }
    return;
}


PreorderForm.prototype.quantityMeasure = function () {
    var t = this.product.unitType;
    return t;
}

PreorderForm.prototype.getMinQty = function () {
    if (this.product && this.product.minOrderQty) {
        return this.product.minOrderQty;
    }
    return 0;
}

PreorderForm.prototype.removeFile = function (index) {
    if(Array.isArray(this.files)) {
        this.files.splice(index, 1);
    }
}

PreorderForm.prototype.removeExistFile = function (index) {
    if(Array.isArray(this.existsFiles)) {
        this.existsFiles.splice(index, 1);
    }
}

/**
 * @property {int} id
 * @property {int} ps_id
 * @property {String} status
 * @property {String} name
 * @property {String} description
 * @property {String} serviceType
 * @property {File[]} files
 * @property {String} message
 * @property {number} budget
 * @property {number} estimate_time
 * @property {String} email
 * @property {String} created_at
 * @property {String} offer_description
 * @property {String} shipAddress
 * @property {int} offer_estimate_time
 * @property {boolean} isForProduct
 * @property {boolean} hasBudgetError
 *
 * @property {PreorderUser} user
 * @property {PreorderFile[]} files
 * @property {PreorderWork[]} works
 * @property {ProductSnapshot} productSnapshot
 * @property {Money} productPrice
 * @property {Money} productTotalPrice
 * @constructor
 */
function Preorder(data) {
    if (data) {
        angular.extend(this, data);
        this.user = new PreorderUser(this.user);
        this.files = this.files.map(function (file) {
            return new PreorderFile(file);
        });
        this.works = this.works.map(function (work) {
            return new PreorderWork(work);
        });
        if (data['productSnapshot']) {
            this.productSnapshot = new ProductSnapshot(data['productSnapshot']);
        }
        this.productPrice = new Money(data['productPrice']);
        this.productTotalPrice = new Money(data['productTotalPrice']);
        if (data['shipTo']) {
            this['shipTo'] = new UserAddress(data['shipTo']);
        }
    }
}

Preorder.STATUS_WAIT_CONFIRM = 'wait_confirm';

/**
 *
 * @return {string[]}
 */
Preorder.prototype.validateMakeOffer = function () {
    var errors = [];

    // if (!this.offer_description) {
    //     errors.push(_t('site.cnc', "Enter description"));
    // }

    if (!this.offer_estimate_time) {
        errors.push(_t('site.cnc', "Set estimated time"));
    }

    if (!this.works.length) {
        errors.push(_t('site.cnc', "Create a list of services to issue a quote"));
    } else {
        for (var key in this.works) {
            if (!this.works.hasOwnProperty(key)) {
                continue;
            }
            var work = this.works[key];
            var workErrors = work.validate();
            errors = errors.concat(workErrors);
        }
    }

    return _.uniq(errors);
};

/**
 * @returns {boolean}
 */
Preorder.prototype.isDeclined = function () {
    return this.status === 'rejected';
};

Preorder.prototype.isReoffer = function () {
    return this.status === 'rejected_by_client';
};

Preorder.prototype.isCnc = function () {
    return this.serviceType === 'cnc';
};

Preorder.prototype.isCalculate = function () {
  return this.isCnc() && !this.hasBudgetError;
};

Preorder.prototype.canSaveDraft = function () {
    return ((this.status === 'new') || (this.status === 'draft'));
};

Preorder.prototype.isCanMakeOffer = function () {
    return ((this.status === 'new') || (this.status === 'draft') || (this.status === 'rejected_by_client'));
};


/**
 * @returns {boolean}
 */
Preorder.prototype.canPsCancel = function () {
    return this.status === 'wait_confirm';
};

/**
 * @returns {boolean}
 */
Preorder.prototype.canDelete = function () {
    return this.status === 'draft';
};


/**
 * @returns {boolean}
 */
Preorder.prototype.canDecline = function () {
    return (this.status !== 'rejected_by_company') && (this.status !== 'canceled_by_client') && (this.status !== 'deleted_by_client') && (this.status !== 'wait_confirm') && (this.status !== 'draft');
};

/**
 * @property {String} uuid
 * @property {String} productUuid
 * @property {String} title
 * @property {String} authorName
 * @property {String} createdAt
 * @property {String} productStatus
 * @property {String} productStatusLabel
 * @property {String} coverUrl
 * @property {String} description
 * @property {String} publicPageUrl
 * @property {String} unitType
 * @property {ProductSnapshotImage[]} images
 * @property {ProductTag[]} productTags
 *
 * @param data
 * @constructor
 */
function ProductSnapshot(data) {
    if (data) {
        angular.extend(this, data);
        this.images = this.images.map(function (image) {
            return new ProductSnapshotImage(image);
        });
        this.productTags = this.productTags.map(function (tag) {
            return new ProductTag(tag);
        });
    }
}

/**
 * @property {String} name
 * @property {String} url
 * @param data
 * @constructor
 */
function ProductSnapshotImage(data) {
    angular.extend(this, data);
}

/**
 * @property {String} text
 * @param data
 * @constructor
 */
function ProductTag(data) {
    angular.extend(this, data);
}

/**
 * @property {int} id
 * @property {String} username
 * @property {String} avatarUrl
 * @constructor
 */
function PreorderUser(data) {
    angular.extend(this, data);
}

PreorderUser.prototype.isUnactive = function () {
    return this.status == 5;
}

/**
 * @property {int} id
 * @property {String} name
 * @property {String} size
 * @constructor
 */
function PreorderFile(data) {
    angular.extend(this, data);
}

/**
 *
 * @property {int} id
 * @property {String} title
 * @property {String} productUuid
 * @property {int} companyServiceId
 * @property {String} type
 * @property {number} cost
 * @property {PreorderFile[]} files
 *
 * @property {bool} $$isEdit
 * @property {bool} $$loading
 * @param data
 * @constructor
 */
function PreorderWork(data) {
    if (data) {
        angular.extend(this, data);
        this.files = this.files.map(function (file) {
            return new PreorderFile(file);
        });
    }
}

/**
 *
 * @return {string[]}
 */
PreorderWork.prototype.validate = function () {
    var errors = [];
    if (this.isProductType()) {
        if (!this.productUuid) {
            errors.push(_t('site.cnc', "Specify product"));
        }
        if (!this.qty || this.qty <= 0) {
            errors.push(_t('site.cnc', "Specify quantity"));
        }
    } else {
        if (!this.companyServiceId && !this.title) {
            errors.push(_t('site.cnc', "Specify service"));
        }
    }
    if (!this.qty || this.qty <= 0) {
        errors.push(_t('site.cnc', "Specify quantity"));
    }
    if (!this.cost) {
        errors.push(_t('site.cnc', "Specify cost")+': '+this.title);
    }

    return errors;
};

PreorderWork.prototype.isProductType = function () {
    return this.type == 'product';
};

PreorderWork.prototype.isServiceType = function () {
    return this.type == 'service';
};

PreorderWork.prototype.isTextType = function () {
    return this.type == '';
};

/**
 * @property {String} countryIso
 * @property {String} region
 * @property {String} city
 * @property {String} address
 * @property {String} extended_address
 * @property {String} lat
 * @property {String} lon
 * @property {String} zipCode
 * @property {String} formattedAddress
 * @property {String} contact_name
 * @property {String} company
 *
 * @param data
 * @constructor
 */
function UserAddress(data) {
    angular.extend(this, data);
}


UserAddress.prototype.setAddressFromLocation = function (location) {
    this.countryIso = location.country;
    this.city = location.city;
    this.region = location.region;
    this.address = '';
    this.extended_address = '';
    this.lat = location.lat;
    this.lon = location.lon;
    this.zipCode = location.zip;
    this.formattedAddress = location.formattedAddress;
};