"use strict";
app.controller('PreorderViewPsController', function ($scope, $notify, $router, $preorderCalculator, $preorderService, $preorderFee, $notifyHttpErrors, $sce, $modal, $formValidator, controllerParams,
                                                     productPreorderDeclineReasons, servicesPreorderDeclineReasons,
                                                     preorder) {

    var INVITE_EMAIL = 1;
    var INVITE_USER = 2
    /**
     *
     * @type {Preorder}
     */
    $scope.preorder = new Preorder(preorder);
    $scope.customers = [
        {"id": INVITE_EMAIL, "name": _t("site.preorder", "Email address")},
        {"id": INVITE_USER, "name": _t("site.preorder", "Username")},
    ];
    $scope.invitePreorder = INVITE_EMAIL;
    $scope.youWillGet = new Money();
    $scope.company = controllerParams['company'];
    $scope.products = new ProductsList(controllerParams['products']);
    $scope.services = new CompanyServicesList(controllerParams['services']);
    $scope.productTypesPlural = controllerParams['productTypesPlural'];
    $scope.productTypesSingular = controllerParams['productTypesSingular'];
/*
    setTimeout(function () {
        debugger;
        $('[data-toggle="tooltip"]').tooltip();
    }, 1500);
*/
    $scope.onUpdateProductCost = function () {
        $scope.updateCostFee();
    };

    $scope.inviteChange = function (value) {
        $scope.invitePreorder = value;
        $scope.preorder.user.id = null;
        $scope.preorder.user.name = null;
        $scope.preorder.email = null;
    };

    $scope.isInviteEmail = function () {
        return $scope.invitePreorder == INVITE_EMAIL;
    };

    $scope.isInviteUser = function () {
        return $scope.invitePreorder == INVITE_USER;
    };

    $scope.quoteHasBeenEdited = false;

    /**
     * Add product
     */
    $scope.addProduct = function () {
        $scope.addWork('product');
    };

    /**
     * Add service
     */
    $scope.addService = function () {
        $scope.addWork('service');
    };

    /**
     * Add new work
     */
    $scope.addWork = function (type) {
        var work = new PreorderWork();
        work.$$isEdit = true;

        if (typeof (type) === 'undefined' || type === '') {
            if ($scope.preorder.productSnapshot) {
                type = 'product';
            } else {
                type = 'service';
            }
        }
        if (type === 'product') {
            work.productUuid = $scope.getDefaultProductUuid();
        }
        if (type === 'service') {
            work.companyServiceId = $scope.getDefaultServiceId();
        }
        work.type = type;
        work.files = [];
        $scope.preorder.works.push(work);
    };

    $scope.getDefaultProductUuid = function () {
        if ($scope.preorder.productSnapshot) {
            return $scope.preorder.productSnapshot.productUuid
        } else {
            return null;
        }
    };

    $scope.getDefaultServiceId = function () {
        return null;
    };

    $scope.initSelect2 = function () {
        $('.js-product-ajax').select2({
            ajax: {
                url: $router.getCurrentUserProductsAjax(),
                dataType: 'json'
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    };

    $scope.initCustomerSearch = function () {
        var options = {
            theme: 'krajee',
            width: '100%',
            containerCssClass: ':all:',
            ajax: {
                url: $router.getSearchClients(),
                method: 'GET',
                dataType: 'json',
                processResults: function (data) {
                    return {
                        "results": _.map(data, function (user) {
                            return {
                                'id': user['id'],
                                'text': user['username'],
                            }
                        })
                    };
                }
            }
        };

        $('.js-select-user-search').select2(options).on('select2:select', function (e) {
            var param = e.params.data;
            $scope.preorder.user.id = param['id'];
            $scope.preorder.user.name = param['text'];
        });

    };

    /**
     *
     * @param {PreorderWork} work
     */
    $scope.saveWork = function (work) {
        var errors = work.validate();
        if (errors.length) {
            $notify.error(errors);
            return;
        }

        return $preorderService.saveWork($scope.preorder, work)
            .then(function () {
                $scope.updateCostFee();
                $scope.quoteHasBeenEdited = true;
            })
            .catch($notifyHttpErrors);
    };

    /**
     *
     * @param {PreorderWork} work
     */
    $scope.editWork = function (work) {
        work.$$isEdit = true;
    };

    /**
     *
     * @param {PreorderWork} work
     */
    $scope.deleteWork = function (work) {
        return $preorderService.deleteWork($scope.preorder, work)
            .then(function () {
                $scope.updateCostFee();
                $scope.quoteHasBeenEdited = true;
            })
            .catch($notifyHttpErrors);
    };

    /**
     *
     * @return {*|Promise|Promise.<T>}
     */
    $scope.makeOffer = function () {
        if (!$scope.youWillGet) {
            $notify.error(_t('site.preorder', "Total cost should be more than ") + $scope.getMinCost() + " " + preorder.currency);
            return;
        }

        return $preorderService.makeOffer($scope.preorder)
            .then(function () {
                $notify.success(_t('site.cnc', "Offer sent to customer."));
                setTimeout(function () {
                    $router.reload();
                }, 2000);
            }).catch(function (response) {
                if (response) {
                    if (response.data.validationErrors) {
                        $formValidator.applyValidateById(response.data.validationErrors, 'preorder');
                        response.data.errors = response.data.validationErrors;
                    }
                    return $notifyHttpErrors(response);
                } else {
                    $router.reload();
                }
            });
    };

    $scope.saveDraft = function () {
        return $preorderService.saveDraft($scope.preorder)
            .then(function () {
                $notify.success(_t('site.cnc', "Draft was saved."));
                setTimeout(function () {
                    $router.reload();
                }, 2000);
            }).catch(function (response) {
                if (response) {
                    return $notifyHttpErrors(response);
                } else {
                    $router.reload();
                }
            });
    };

    $scope.withdrawOffer = function () {
        return $preorderService.withdrawOffer($scope.preorder)
            .then(function () {
                $notify.success(_t('site.cnc', 'Offer withdrew'));
                setTimeout(function () {
                    $router.reload();
                }, 2000);
            }).catch(function (response) {
                if (response) {
                    return $notifyHttpErrors(response);
                } else {
                    $router.reload();
                }
            });
    };

    $scope.deletePreorder = function () {
        new TS.confirm(_t('site.preorder', 'Restore will be impossible. Are you sure?'), function (result) {
            if (result == 'ok') {
                $preorderService.deletePreorder(preorder.id);
            }
        }, {confirm: _t('site.ps', 'Yes, delete it'), dismiss: _t('site.ps', 'Cancel')});
    };

    $scope.declinePreorder = function () {
        debugger;
        new TS.confirm(_t('site.preorder', 'Are you sure?'), function (result) {
            if (result == 'ok') {

                $preorderService
                    .declinePreorder(preorder.id, preorder.isShowDeclineReasonModal, (preorder.isForProduct ? productPreorderDeclineReasons : servicesPreorderDeclineReasons))
                    .then(function () {
                            $router.toPsQuotesRequests();
                        },
                        $notifyHttpErrors);
            }
        }, {confirm: _t('site.ps', 'Yes, decline it'), dismiss: _t('site.ps', 'Cancel')});
    };


    /**
     *
     */
    $scope.getCost = function () {
        return $preorderCalculator.calculate($scope.preorder);
    };

    $scope.getBonusAccrued = function () {
        if ($scope.company.cashbackPercent) {
            return Math.round($preorderCalculator.calculate($scope.preorder)*$scope.company.cashbackPercentPlus)/100;
        }
    };


    $scope.getMinCost = function () {
        return 4;
    };

    $scope.updateCostFee = function () {
        var totalCost = $preorderCalculator.calculate($scope.preorder);
        $preorderFee.findPercent(totalCost, $scope.preorder.currency)
            .then(function (data) {
                $scope.youWillGet = data['amount'];
                if ($scope.youWillGet < 1) {
                    $scope.youWillGet = 0;
                }
            });
    };

    /**
     *
     * @returns {string}
     * @param preorderWork
     */
    $scope.getWorkTextTitle = function (preorderWork) {
        if (preorderWork.isProductType()) {
            var product = $scope.products.getProductByUuid(preorderWork.productUuid);
            return _t('site.product', 'Product') + ': ' + product.title;
        }
        if (preorderWork.isServiceType()) {
            var service = $scope.services.getServiceById(preorderWork.companyServiceId);
            return _t('site.service', 'Service') + ': ' + service.title;
        }
        return preorderWork.title;
    };

    $scope.getWorkTextUnit = function (preorderWork, qty) {
        if (preorderWork.isProductType()) {
            var product = $scope.products.getProductByUuid(preorderWork.productUuid);
            return $scope.getUnitTypeLabel(product.unitType, qty);
        }
        return '';
    };

    $scope.getUnitTypeLabel = function (unitType, qty) {
        if (qty > 1 && $scope.productTypesPlural[unitType]) {
            unitType = $scope.productTypesPlural[unitType];
        }
        if ($scope.productTypesSingular[unitType]) {
            unitType = $scope.productTypesSingular[unitType];
        }
        return unitType;
    }

    var workServicesAutoComplete = null;
    $scope.getWorkServicesAutocomplete = function () {
        if (workServicesAutoComplete === null) {
            workServicesAutoComplete = _.map($scope.services.services, function (obj) {
                return {id: obj.id, label: obj.title}
            });
        }
        return workServicesAutoComplete;
    };

    $scope.changeProduct = function (preorderWork) {
        var product = $scope.products.getProductByUuid(preorderWork.productUuid);
        if (product) {
            if (!preorderWork.qty) {
                preorderWork.qty = 1;
            }
            preorderWork.cost = product.getTotalPrice(preorderWork.qty);
        }
    };

    $scope.changeQty = function (preorderWork) {
        if (preorderWork.type == 'product') {
            var product = $scope.products.getProductByUuid(preorderWork.productUuid);
            if (product) {
                preorderWork.cost = product.getTotalPrice(preorderWork.qty);
            }
        }
    };

    $scope.shipAddressHtml = function () {
        return $sce.trustAsHtml(preorder.shipAddress);
    };

    /**
     *
     */
    $scope.declinePreorder = function () {
        function decline() {
            $preorderService
                .declinePreorder($scope.preorder.id, $scope.preorder.isShowDeclineReasonModal, ($scope.preorder.isForProduct ? productPreorderDeclineReasons : servicesPreorderDeclineReasons))
                .then(function () {
                    $router.toPsQuotesRequests();
                }, $notifyHttpErrors);
        }

        if ($scope.preorder.isShowDeclineReasonModal) {
            decline();
        } else {
            $modal.confirm(_t('site.preorder', 'Are you sure you want to decline?'), _t('site', 'Yes'), _t('site', 'No'))
                .then(function () {
                    return decline();
                });
        }
    };

    $scope.getDownloadFileLink = $preorderService.getDownloadFileLink;
    $scope.getDownloadWorkFileLink = $preorderService.getDownloadWorkFileLink;
    $scope.updateCostFee();
})


    .factory('$preorderCalculator', function () {

        var $preorderCalculator = {};

        /**
         *
         * @param {Preorder} preorder
         */
        $preorderCalculator.calculate = function (preorder) {
            var cost = 0;
            preorder.works.forEach(function (work) {
                if (work.cost) {
                    cost += work.cost;
                }
            });
            return cost;
        };

        return $preorderCalculator;
    });
