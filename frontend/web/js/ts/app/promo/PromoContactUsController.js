"use strict";


app.controller('PromoContactUsController', function($scope, $http, $notifyHttpErrors, $notify, controllerParams)
{
    $scope.contactForm = {};

    $scope.isSended = false;

    $scope.sendContactForm = function () {

        if(!$scope.contactForm.email) {
            $notify.error(_t('site.promo', 'Please enter a valid email'));
            return;
        }

        $scope.contactForm.type = controllerParams.type;

        return $http.post('/l/contact-us',  $scope.contactForm)
            .then(function () {
                $scope.isSended = true;
                $scope.contactForm = {};
                $notify.success(_t('front.site', 'Thank you for contacting us.'));
            }, $notifyHttpErrors);
    };
});