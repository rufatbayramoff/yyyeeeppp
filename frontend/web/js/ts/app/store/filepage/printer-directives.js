/**
 * Printers rating directive with starts
 */
app.directive('printerRating', function ($timeout) {
    var txtReview = _t('site.common', 'review');
    var txtReviews = _t('site.common', 'reviews');

    var getCountText = function (count) {
        return (count > 1 ? txtReviews : txtReview);
    };

    return {
        template: '<div><div ng-if="showRating" class="star-rating-block" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"><meta itemprop="ratingValue" content="{{rating.rating}}"/><meta itemprop="worstRating" content="1"/><meta itemprop="bestRating" content="5"/><input value="{{rating.rating}}" type="text" class="star-rating" data-min=0 data-max=5 data-step=0.1 data-size="xs" data-symbol="&#xea4b;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>' +
        '<span class="star-rating-count">(<span itemprop="reviewCount">{{rating.reviewsCount}}</span> {{countText}})</span></div>',
        restrict: 'E',
        scope: {
            rating: '='
        },
        replace: true,
        link: function (scope, element) {
            scope.countText = getCountText(scope.rating.reviewsCount);
            scope.showRating = scope.rating.reviewsCount > 2;

            if (scope.showRating) {
                $timeout(function () {
                    element.find('.star-rating')
                        .rating({
                            showCaption: false,
                            showClear: false,
                            min: 0,
                            max: 5,
                            readOnly: true,
                            starCaptions: {
                                1: _t('site.common', 'Very Bad'),
                                2: _t('site.common', 'Bad'),
                                3: _t('site.common', 'Good'),
                                4: _t('site.common', 'Very Good'),
                                5: _t('site.common', 'Perfect')
                            }
                        });
                });
            }
        }
    }
});

