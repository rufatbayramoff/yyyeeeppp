"use strict";

app.factory('$modelRender', function (globalConfig)
{
    var $modelRender = {};

    /**
     * Generate thumb url for part and color
     * @param {ModelPart} part
     * @param colorHex
     * @returns {string}
     */
    $modelRender.getFullUrl = function (part, colorHex)
    {
        var renderParams = commonJs.clone(model3dRenderParams);
        renderParams.fileUid = part.hash;
        renderParams.colorHex = colorHex;
        renderParams.ax = 0;
        renderParams.ay = 30;
        renderParams.pb = part.pb;
        renderParams.isMulticolorFormat = part.isMulticolorFormat;
        renderParams.renderUrlBase = part.renderUrlBase;

        var imgUrl = model3dRenderRoute.getUrl(renderParams);
        return imgUrl;
    };

    return $modelRender;
});