"use strict";

app.directive('modelSocialButtons', function ($timeout, globalConfig)
{
    return {
        restrict : 'E',
        scope : {
            model : '=',
            link : '@',
            title : '@'
        },
        templateUrl : '/js/ts/app/store/filepage/filepage-social.html',
        replace : true,
        link : function (scope, element)
        {
            scope.link = scope.link || window.location.href;
            scope.title = scope.title || document.title;

            /**
             * Init facbook SDK
             */
            this.initFacebookSDK = function ()
            {
                window.fbAsyncInit = function () {
                    FB.init({
                        appId: globalConfig.facebook_app_id,
                        xfbml: true,
                        version: 'v2.5'
                    });
                };

                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {
                        return;
                    }
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            };

            /**
             * get share link pop-up
             *
             * @param $doc
             * @param link
             * @returns {string}
             */
            var getShareLink = function ($doc, link) {
                return "<div class='text-center'><input onfocus='$(this).select()' class='form-control m-b10 share-input' type='text' value='" + link
                    + "' /></div>";
            };

            /**
             * get embed link. adds widget=1 param to given link
             *
             * @param $doc
             * @param link
             * @returns {string}
             */
            var getShareEmbed = function ($doc, link) {
                if (link.indexOf('?') > 0) {
                    link = link + '&widget=1';
                } else {
                    link = link + '?widget=1';
                }
                var iframe = '<iframe width="auto" height="550" src="' + link + '" frameborder="0"></iframe>';
                return "<div class='text-center'><input onfocus='$(this).select();' class='form-control m-b10 share-input' size=55 type='text' value='"
                    + iframe + "' /></div>";
            };

            /**
             *
             */
            scope.openFacebookModal = function ()
            {
                FB.ui({
                    method: 'send',
                    link: scope.link
                }, angular.noop);
            };


            var self = this;

            $timeout(function () {

                self.initFacebookSDK();

                $('.js-social-likes', element).socialLikes({
                    counters : true,
                    zeroes:  false,
                    title: scope.title
                });

                var shareLinkContent = getShareLink($('body'),scope.link);
                var shareEmbedContent = getShareEmbed($('body'),scope.link);

                $('.sharelink', element).popover({trigger: 'focus', html: true, content: $(shareLinkContent)});
                $('.shareembed', element).popover({trigger: 'focus', html: true, content: $(shareEmbedContent)});


                $('.shareembed, .sharelink', element).on('click', function () {
                    $(this).popover('toggle');
                });

                $(document).on('popup_opened.social-likes', function(event, service) {
                    if ((typeof(ga) != 'undefined') && ga){
                        ga('send', 'event', {
                            eventCategory: 'SocialShare',
                            eventAction:  service,
                            eventLabel: location.href
                        });
                    }
                });

            });
        }
    };
});