"use strict";

/**
 * Filepage controller
 */
app.controller('FilePageController', ['$scope', 'controllerParams', '$modal', '$user', function ($scope, controllerParams, $modal, $user) {

    $scope.model = new Model(controllerParams.model);
    $scope.model3dTextureState = new ModelTextureState(controllerParams.model3dTextureState);
    $scope.originalModel3dTextureState = controllerParams.model3dTextureState;

    /**
     * If current selected fotorama object is image, selectedModel3dPartId is setup to null
     *
     * @type {null}
     */
    $scope.selectedModel3dPartId = null;


    /**
     *
     * @param $event
     */
    $scope.goToEditModel = function ($event) {
        if ($user.isGuest) {
            $event.stopPropagation();
            $event.preventDefault();
            $user.openLoginModal('/my/model/edit/' + $scope.model.id);
        }
    };

    /**
     * If fotorama change current image
     */
    $scope.$on('filepage:changeSelectedPart', function (event, selectedPart) {
        if (selectedPart) {
            $scope.selectedModel3dPartId = selectedPart.id;
            var texture = $scope.model3dTextureState.getTexture(selectedPart);
        } else {
            $scope.selectedModel3dPartId = null;
        }

        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
            $scope.$apply();
        }
    });
}]);
