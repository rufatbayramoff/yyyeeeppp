"use strict";

/**
 * @property {int} storeUnitId
 * @property {float} weight
 * @property {Array} images
 * @property {ModelPart[]} parts
 * @property {ModelImage[]} images
 * @property {string} coverImgUrl
 * @property {float} scale
 * @param data
 * @constructor
 */
function Model(data) {
    this.load(data);
}

Model.prototype.load = function (data) {
    angular.extend(this, data);
    if (typeof (this.parts) === 'undefined') {
        this.parts = [];
    }
    if (typeof (this.images) === 'undefined') {
        this.images = [];
    }
    this.parts = this.parts.map(function (raw) {
        return new ModelPart(raw);
    });
    this.images = this.images.map(function (raw) {
        return new ModelImage(raw);
    });
};

Model.prototype.loadCancelParts = function (cancelPartsList) {
    for (var partId in cancelPartsList) {
        if (!cancelPartsList.hasOwnProperty(partId)) continue;
        var partUid = cancelPartsList[partId];
        var canceledPart = this.getPartByUid(partUid);
        if (canceledPart) {
            canceledPart.isCanceled = true;
        }
    }
};

Model.prototype.fixParserPart = function (partInfo) {
    var part = this.findPartByUid(partInfo.uid);
    if (!part) {
        var newPart = new ModelPart(partInfo);
        this.parts.push(newPart);
        return;
    }
    if (partInfo.size) {
        part.weight = partInfo.weight;
        part.size = new Size(partInfo.size);
        part.isMulticolorFormat = partInfo.isMulticolorFormat;
        part.fileExt = partInfo.fileExt;
    }
    if (partInfo.parser) {
        if (partInfo.parser.isSuccess) {
            part.isParsed = true;
        }
        if (partInfo.parser.isError) {
            part.isParsed = true;
        }
    }
};

Model.prototype.fixParserParts = function (partsList) {
    for (var partId in partsList) {
        if (!partsList.hasOwnProperty(partId)) continue;
        var partInfo = partsList[partId];
        this.fixParserPart(partInfo);
    }
};


Model.prototype.findPartByMd5NameSize = function (fileMd5NameSize) {
    for (var partKey in this.parts) {
        var part = this.parts[partKey];
        if (part.fileMd5NameSize === fileMd5NameSize) {
            return part;
        }
    }
    return null;
};

Model.prototype.findPartByUid = function (uid) {
    if (!uid) {
        return null;
    }
    for (var partKey in this.parts) {
        var part = this.parts[partKey];
        if (part.uid === uid) {
            return part;
        }
    }
    return null;
};


Model.prototype.sortPartsById = function () {
    this.parts = _.sortBy(this.parts, 'id');
};

/**
 *
 * @returns {boolean}
 */
Model.prototype.hasImages = function () {
    return this.images && this.images.length > 0;
};

/**
 *
 * @returns {boolean}
 */
Model.prototype.hasParts = function () {
    return this.parts && this.parts.length > 0;
};

/**
 *
 * @returns {boolean}
 */
Model.prototype.isEmpty = function () {
    var activeParts = this.getActiveModel3dParts();
    var activeImages = this.getActiveModelImages();
    var notEmpty = (activeParts && (activeParts.length > 0)) || (activeImages && (activeImages.length > 0));
    return !notEmpty;
};

/**
 *
 * @returns {boolean}
 */
Model.prototype.isKit = function () {
    var activeParts = this.getActiveModel3dParts();
    return activeParts && (activeParts.length > 1);
};

/**
 *
 * @return {boolean}
 */
Model.prototype.isCalculated = function () {
    for (var partKey in this.parts) {
        if (!this.parts.hasOwnProperty(partKey)) continue;
        var part = this.parts[partKey];

        if (part && !part.isCanceled && !part.isCalculated()) {
            return false;
        }
    }
    return true;
};

/**
 *
 * @return {boolean}
 */
Model.prototype.isParsed = function () {
    for (var partKey in this.parts) {
        if (!this.parts.hasOwnProperty(partKey)) continue;
        var part = this.parts[partKey];
        if (!part.isCanceled) {
            if (part.isParsed) {
            } else {
                return false;
            }
        }
    }
    return true;
};


/**
 * Model is in calculating progress
 *
 * @return {boolean}
 */
Model.prototype.isCalculating = function () {
    var calculated = this.isCalculated();
    var parsed = this.isParsed();
    var calculating = !(calculated && parsed);
    return calculating;
};

/**
 * Model has model3dpart with allowed preview
 *
 * @return {boolean}
 */
Model.prototype.hasPreview3d = function () {
    for (var partKey in this.parts) {
        if (!this.parts.hasOwnProperty(partKey)) continue;
        var part = this.parts[partKey];
        if (part && part.hash) {
            return true;
        }
    }
    return false;
};

/**
 *
 * @returns {ModelPart}
 */
Model.prototype.getFirstActiveModel3dPart = function () {
    for (var partKey in this.parts) {
        if (!this.parts.hasOwnProperty(partKey)) continue;
        var part = this.parts[partKey];
        if (part && !part.isCanceled && part.qty) {
            return part;
        }
    }
    return null;
};

/**
 *
 * @return {ModelPart[]}
 */
Model.prototype.getActiveModel3dParts = function () {
    var activeParts = [];
    for (var partKey in this.parts) {
        if (!this.parts.hasOwnProperty(partKey)) continue;
        var part = this.parts[partKey];
        if (part && !part.isCanceled && part.qty) {
            activeParts.push(part);
        }
    }
    return activeParts;
};

Model.prototype.getActiveModelImages = function () {
    var activeImages = [];
    for (var imageKey in this.images) {
        if (!this.images.hasOwnProperty(imageKey)) continue;
        var image = this.images[imageKey];
        if (!image.isCanceled) {
            activeImages.push(image);
        }
    }
    return activeImages;
};


/**
 *
 * @param md5NameSize string
 * @returns {ModelPart|ModelImage|null}
 */
Model.prototype.getItemByMd5NameSize = function (md5NameSize) {
    if (!this.uid) {
        return null;
    }
    for (var k in this.images) {
        var image = this.images[k];
        if (image.fileMd5NameSize === md5NameSize) {
            return image;
        }
    }
    for (var k2 in this.parts) {
        var part = this.parts[k2];
        if (part.fileMd5NameSize === md5NameSize) {
            return part;
        }
    }
    return null;
};


/**
 *
 */
Model.prototype.getPartById = function (id) {
    for (var partKey in this.parts) {
        if (!this.parts.hasOwnProperty(partKey)) continue;
        var part = this.parts[partKey];
        if (part.id === id) {
            return part;
        }
    }
    return null;
};

Model.prototype.getPartByUid = function (uid) {
    for (var partKey in this.parts) {
        if (!this.parts.hasOwnProperty(partKey)) continue;
        var part = this.parts[partKey];
        if (part.uid === uid) {
            return part;
        }
    }
    return null;
};

/**
 * Return model files for all active parts
 * @return {FileModel[]}
 */
Model.prototype.getActivePartsModelFiles = function () {
    if (this.$$activePartsModelFiles === undefined) {
        this.$$activePartsModelFiles = this.getActiveModel3dParts().map(function (part) {
            return part.getFile();
        });
    }
    return this.$$activePartsModelFiles;
};

Model.prototype.isUploading = function () {
    for (var partKey in this.parts) {
        if (!this.parts.hasOwnProperty(partKey)) continue;
        var part = this.parts[partKey];
        if (part.uploadPercent > 1 && part.uploadPercent < 100) {
            return true;
        }
    }
    for (var imageKey in this.images) {
        if (!this.images.hasOwnProperty(imageKey)) continue;
        var image = this.images[imageKey];
        if (image.uploadPercent > 1 && image.uploadPercent < 100) {
            return true;
        }
    }
    return false;
};

/**
 * @property {int} id
 * @property {string} uid
 * @property {int} fileId
 * @property {string} name
 * @property {string} fileMd5NameSize
 * @property {int|null} partReplicaId
 * @property {int} originalModel3dPartId
 * @property {Size} size
 * @property {string} hash
 * @property {bool} isParsed
 * @property {bool} isConverted
 * @property {bool} isMulticolorFormat
 *
 * @property {int} pb some parameter for rendering
 * @property {CncParams} cncParams
 * @constructor
 */
function ModelPart(data) {
    this.load(data);
}

/**
 *
 * @return {boolean}
 */
ModelPart.prototype.isCalculated = function () {
    return this.size && this.size.height > 0;

};

ModelPart.prototype.getSizeTitle = function () {
    var measure = angular.injector(['app']).get("$measure");
    var measureConverter = angular.injector(['app']).get("$measureConverter");

    var sizeMM = measureConverter.convertSize(this.size, measure.MEASURE_MM, true);
    var sizeInch = measureConverter.convertSize(this.size, measure.MEASURE_IN, true);

    return sizeMM.width.toFixed(2) + ' x ' + sizeMM.height.toFixed(2) + ' x ' + sizeMM.length.toFixed(2) + ' mm ' +
        '(' + (sizeInch.width).toFixed(2) + ' x ' + (sizeInch.height).toFixed(2) + ' x ' + (sizeInch.length).toFixed(2) + ' in)';
};

ModelPart.prototype.getWeightTitle = function () {
    return this.weight + ' ' + _t('site.common', 'gr');
};

ModelPart.prototype.load = function (data) {
    angular.extend(this, data);
    this.size = new Size(data.size);
};

ModelPart.prototype.isStepFile = function () {
    if (!this.name) {
        return false;
    }
    let name = this.name.toLowerCase();
    return (name.indexOf('.step') > 0) || (name.indexOf('.stp') > 0);
};

/**
 *
 * @return {FileModel}
 */
ModelPart.prototype.getFile = function () {
    var file = new FileModel();
    file.id = this.fileId;
    file.name = this.name;
    return file;
};

/**
 * @property {int} id
 * @property {int} file_id
 * @property {string} title
 * @property {string} previewUrl
 * @constructor
 */
function ModelImage(data) {
    angular.extend(this, data);
}

/**
 * @property {number} width
 * @property {number} length
 * @property {number} height
 * @property {string} measure
 * @constructor
 */
function Size(data) {
    angular.extend(this, data);
}

/**
 *
 * @param {Size} newValue
 */
Size.prototype.set = function (newValue) {
    this.width = newValue.width;
    this.length = newValue.length;
    this.height = newValue.height;
    this.measure = newValue.measure;
};

/**
 *
 * @param {Size} size
 * @return {number}
 */
Size.prototype.getMinSide = function (size) {
    return Math.min(size.width, size.height, size.length);
};

/**
 *
 * @param size
 * @return {number}
 */
Size.prototype.getMaxSide = function (size) {
    return Math.max(size.width, size.height, size.length);
};

Size.prototype.isMoreThen = function (size) {
    let thisSizesData = [this.length, this.width, this.height];
    let otherSizesData = [size.length, size.width, size.height];

    thisSizesData.sort().reverse();
    otherSizesData.sort().reverse();

    let i = 0;
    for (i = 0; i < 3; i++) {
        if (thisSizesData[i] >= otherSizesData[i]) {
            return true;
        }
    }
    return false;
};


/**
 * @property {bool} isOneTextureForKit
 * @property {Texture} kitTexture
 * @property {Texture[]} textures
 * @property {bool} isAnyTextureAllowed
 * @param {*} data
 * @constructor
 */
function ModelTextureState(data) {
    this.isAnyTextureAllowed = true;
    this.load(data);
}

/**
 * @param {ModelPart} part
 * @return {string}
 */
ModelTextureState.prototype.getColorForRendering = function (part) {
    var texture = this.getTexture(part);
    return texture.printerColor.getColorForRendering(part.isMulticolorFormat);
};

/**
 * @param {int} index
 * @return {Texture}
 */
ModelTextureState.prototype.getTextureByIndex = function (index) {
    if (this.isOneTextureForKit) {
        return this.kitTexture;
    } else {
        if (typeof (this.textures[index]) === 'undefined') {
            return this.kitTexture;
        }
        return this.textures[index];
    }
};


/**
 * @param {ModelPart} part
 * @return {Texture}
 */
ModelTextureState.prototype.getTexture = function (part) {
    if (part === null) {
        return this.kitTexture;
    }
    return this.getTextureByIndex(part.id);
};


/**
 * Return all textures in state.
 * If it one texture for kit - return array with this texture,
 * else  - return all parts textures
 * @return {Texture[]}
 */
ModelTextureState.prototype.getTextures = function () {
    if (this.isOneTextureForKit) {
        var textures = [];
        textures.push(this.kitTexture);
        return textures;
    }

    return this.textures;
};

ModelTextureState.prototype.getTexturesWordyFormat = function (model3d) {

    if (this.isOneTextureForKit) {
        var retVal = {
            isOneMaterialForKit: true,
            modelTexture: this.kitTexture.serializeAttributes(this.isAnyTextureAllowed),
            isAnyTextureAllowed: this.isAnyTextureAllowed
        };
        return retVal;
    } else {
        var texturesSerialized = {};
        for (var partKey in model3d.parts) {
            if (!model3d.parts.hasOwnProperty(partKey)) continue;
            var part = model3d.parts[partKey];
            var texture = this.getTexture(part);
            texturesSerialized[part.uid] = texture.serializeAttributes(this.isAnyTextureAllowed);
        }

        var retVal = {
            isOneMaterialForKit: false,
            partsMaterial: texturesSerialized,
            isAnyTextureAllowed: this.isAnyTextureAllowed
        };
        return retVal;
    }
};

ModelTextureState.prototype.setOneTextureForKit = function () {
    this.isOneTextureForKit = true;
    this.textures = {};
};

ModelTextureState.prototype.getTextureForPart = function (model3dPartId) {
    if (typeof (model3dPartId) == "undefined") {
        model3dPartId = 0;
    }

    let texture = null;

    if (model3dPartId) {
        this.isOneTextureForKit = false;
        texture = this.textures[model3dPartId];
        if (typeof (texture) == 'undefined') {
            texture = new Texture({});
            this.textures[model3dPartId] = texture;
        }
    } else {
        this.isOneTextureForKit = true;
        texture = this.kitTexture;
    }
    return texture;
};

/**
 * @param {int} printerColorId
 * @param {int} printerMaterialId
 * @param {int} printerMaterialGroupId
 * @param {int} model3dPartId
 */
ModelTextureState.prototype.setColorAndMaterialGroup = function (printerColorId, printerMaterialId, printerMaterialGroupId, infill, model3dPartId) {
    let texture = this.getTextureForPart(model3dPartId);

    // Always change kit texture, this will be default texture for parts without textures
    texture.setPrinterColor(printerColorId);
    texture.setPrinterMaterialGroup(printerMaterialGroupId);
    texture.setPrinterMaterial(printerMaterialId);
    texture.setInfill(infill);
};

/**
 * @param {array} data
 */
ModelTextureState.prototype.load = function (data) {
    angular.extend(this, data);
    var kitTexture = [];
    this.textures = {};
    if (typeof (data) !== 'undefined') {
        if (typeof (data['kitTexture']) !== 'undefined') {
            kitTexture = data['kitTexture'];
        }
        for (var key in data['textures']) {
            var textureInfo = data['textures'][key];
            var texture = new Texture(textureInfo);
            this.textures[key] = texture;
        }
    }
    this.kitTexture = new Texture(kitTexture);
};


/**
 * @property {int} infill
 * @property {int} printer_color_id
 * @property {int} printer_material_group_id
 *
 * @property {PrinterColor} printerColor
 * @property {PrinterMaterialGroup} printerMaterialGroup
 * @constructor
 */
function Texture(data) {
    angular.extend(this, data);
    if (this.printer_color_id) {
        this.printerColor = objectStorage().get('PrinterColor', this.printer_color_id);
        this.printerMaterialGroup = objectStorage().get('PrinterMaterialGroup', this.printer_material_group_id);

        if (!_.isEmpty(this.printerColor) && !_.isEmpty(this.printerMaterialGroup)) {
            return;
        }
    }
    if (_.isEmpty(this.printerColor) || !data || !data['printerColor']) {
        this.printerColor = new PrinterColor({
            id: 90,
            code: 'white',
            title: _t('site.common', 'White'),
            rgbHex: 'ffffff'
        });
    } else {
        this.printerColor = new PrinterColor(data['printerColor']);
    }

    if (_.isEmpty(this.printerMaterialGroup) || !data || !data['printerMaterialGroup']) {
        this.printerMaterialGroup = new PrinterMaterialGroup({
            code: 'PLA',
            title: _t('site.common', 'PLA'),
            id: 16
        });
    } else {
        this.printerMaterialGroup = new PrinterMaterialGroup(data['printerMaterialGroup']);
    }
}

Texture.prototype.serializeAttributes = function (notUseMaterial) {
    return {
        printer_material_group_id: this.printerMaterialGroup.id,
        printer_color_id: this.printerColor.id,
        printer_material_id: notUseMaterial ? null : this.getPrinterMaterialId(),
        infill: this.infill
    }
};

Texture.prototype.setInfill = function (infill) {
    this.infill = infill;
};

Texture.prototype.setPrinterColor = function (printerColorId) {
    this.printerColor = objectStorage().get('PrinterColor', printerColorId);
    this.printer_color_id = printerColorId;
};

Texture.prototype.setPrinterMaterialGroup = function (printerMaterialGroupId) {
    this.printerMaterialGroup = objectStorage().get('PrinterMaterialGroup', printerMaterialGroupId);
    this.printer_material_group_id = printerMaterialGroupId;
};

Texture.prototype.setPrinterMaterial = function (printerMaterialId) {
    printerMaterialId = parseInt(printerMaterialId);
    this.printerMaterial = printerMaterialId ? objectStorage().get('PrinterMaterial', printerMaterialId) : null;
    this.printer_material_id = printerMaterialId ? printerMaterialId : printerMaterialId;
};

/**
 * @returns {int|null}
 */
Texture.prototype.getPrinterMaterialId = function () {
    return (this.printerMaterial ? this.printerMaterial.id : null)
};

/**
 * @returns {string}
 */
Texture.prototype.getPrinterMaterialTitle = function () {
    return (this.printerMaterial ? this.printerMaterial.title : this.printerMaterialGroup.title)
};

/**
 * @property {MaterialGroupGuide[]} list
 * @param data
 * @constructor
 */
function MaterialGroupGuideList(data) {
    $this.load(data);
}

MaterialGroupGuideList.prototype.load = function (data) {
    for (var id in data) {
        if (!data.hasOwnProperty(id)) continue;
        var groupDescription = data[id];
        var materialGroupDescription = new MaterialGroupGuide(data);
        this.list.push(materialGroupDescription);
    }

    angular.extend(this, data);
};


/**
 * @property {int} code
 * @property {string} title
 * @property {string} imageUrl
 * @property {string} about
 * @property {string[]} cons
 * @property {string[]} props
 */
function MaterialGroupGuide(data) {
    $this.load(data);
}

/**
 * @property {int} id
 */
MaterialGroupGuide.prototype.load = function () {
    angular.extend(this, data);
};


/**
 * @property {int} id
 * @property {string} code
 * @property {string} rgbHex
 * @property {string} title
 * @property {int} isShortColor
 * @param {*} data
 * @constructor
 */
function PrinterColor(data) {
    angular.extend(this, data);
}

PrinterColor.prototype.getColorForRendering = function (isMulticolorAllowed) {
    if ((typeof (isMulticolorAllowed) !== 'undefined') && (!isMulticolorAllowed)) {
        if (this.id === 102) {
            return 'ffffff';
        }
    }
    return this.id == 102 ? 'multicolor' : this.rgbHex;
};

PrinterColor.prototype.getRgbHex = function () {
    return this.rgbHex;
};


/**
 * @property {string} code
 * @property {string} title
 * @property {string} photoUrl
 * @property {string} shortDescription
 * @property {string} longDescription
 * @param {*} data
 * @constructor
 */
function PrinterMaterialGroup(data) {
    angular.extend(this, data);
}

function PrinterMaterial(data) {
    angular.extend(this, data);
}

/**
 * @property {int[]} partsQty
 * @param {*} data
 * @constructor
 */
function OrderForm(data) {
    angular.extend(this, data);
}


/**
 * @property {String} finefile
 * @param {*} data
 * @constructor
 */
function CncParams(data) {
    angular.extend(this, data);
}

/**
 * @property {int} count
 * @property {String} material
 * @property {String[]} postprocessing
 * @param {*} data
 * @constructor
 */
function Preset(data) {
    angular.extend(this, data);
}


/**
 *
 * @param {ModelPart[]} parts
 * @constructor
 */
function CostingBundle(parts) {
    /**
     * Model parts
     * @type {ModelPart[]}
     */
    this.parts = parts;

    /**
     * Costings map (by part id)
     * @type {*[]}
     */
    this.costings = {};

    /**
     * Errors map (by part id)
     * @type {String[]}
     */
    this.errors = {};
}

/**
 * Return costing result for part, or undefined if it not calculated yet
 * @param {ModelPart} part
 * @param {*} costing
 */
CostingBundle.prototype.putCosting = function (part, costing) {
    this.errors[part.id] = undefined;
    var existCosting = this.costings[part.id];

    existCosting
        ? angular.extend(existCosting, costing)
        : this.costings[part.id] = costing;
};

/**
 * Put error for costing calculate for part
 * @param {ModelPart} part
 * @param {String|*} error
 */
CostingBundle.prototype.putError = function (part, error) {
    this.costings[part.id] = undefined;
    this.errors[part.id] = error;
};

/**
 * Reset costing for part
 * @param {ModelPart} part
 */
CostingBundle.prototype.resetCosting = function (part) {
    this.costings[part.id] = undefined;
    this.errors[part.id] = undefined;
};

/**
 * Return costing result for part, or undefined if it not calculated yet
 * @param {ModelPart} part
 * @return {*}
 */
CostingBundle.prototype.getCostingForPart = function (part) {
    return this.costings[part.id];
};

/**
 * Return costing error for part
 * @param {ModelPart} part
 * @return {String|*}
 */
CostingBundle.prototype.getErrorForPart = function (part) {
    return this.errors[part.id];
};

/**
 * Is cost calculated for all models
 */
CostingBundle.prototype.isSuccesfulCalculated = function () {
    var self = this;
    return _.reduce(this.parts, function (val, part) {
        return val && self.getCostingForPart(part)
    }, true);
};

/**
 * Is cost calculated for all models
 */
CostingBundle.prototype.getCurrency = function () {
    var costing = _.first(_.values(this.costings));
    return costing ? costing.currency : undefined;
};

/**
 * Is cost calculated for all models
 */
CostingBundle.prototype.getTotal = function () {
    if (!this.isSuccesfulCalculated()) {
        return undefined;
    }
    return _.reduce(this.costings, function (val, costing) {
        return val + costing.total
    }, 0);
};

/**
 * @property {int} count
 * @property {CncMaterial[]} materials
 * @property {CncMaterial[]} postProcessing
 * @property {{defaultMaterial: String}} settings
 * @param {*} data
 * @constructor
 */
function CncMachine(data) {
    angular.extend(this, data);
}

/**
 * @return {CncMaterial}
 */
CncMachine.prototype.getDefaultMaterial = function () {
    var self = this;
    return _.find(this.materials, function (material) {
        return material.code = self.settings.defaultMaterial;
    })
};

/**
 * @property {String} title
 * @property {String} code
 * @param data
 * @constructor
 */
function CncMaterial(data) {
    angular.extend(this, data);
}


var globalConfg = {
    facebook_app_id: "",
    renderUrl: "",
    staticUrl: ""
};