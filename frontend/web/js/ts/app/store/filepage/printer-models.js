"use strict";

/*
* @property {Offer[]} offers
* @property {Offer} bestOffer;
* @property {string[]} messages
*/
function OffersBundle(data) {
    angular.extend(this, data);
    this.offers = [];
    for (var key in data.offers) {
        if (!data.offers.hasOwnProperty(key)) continue;
        var dataElement = data.offers[key];
        var element = new Offer(dataElement);
        this.offers.push(element);
    }
}

/**
 * @param id
 */
OffersBundle.prototype.getOfferById = function (id) {
    var returnValue = this.offers.find(function (offer) {
        return offer.psPrinter.id == id;
    });
    return returnValue;
};


OffersBundle.prototype.isNoDeliveryReason = function () {
    if (!this.messages) {
        return false;
    }
    var message = this.messages[0];
    if (typeof (message) === 'undefined') {
        return false;
    }
    if (message == 'no_delivery') {
        return true;
    }
    return false;
};

OffersBundle.prototype.noActiveMachines = function(psIdOnly) {
    if (!this.messages) {
        return false;
    }
    var message = this.messages[0];
    if (typeof (message) === 'undefined') {
        return false;
    }
    if (message == 'no_active_machines') {
        return true;
    }
    return false;
}

OffersBundle.prototype.getEmptyListComment = function (psIdOnly) {
    if (!this.messages) {
        return '';
    }
    var message = this.messages[0];
    if (typeof (message) === 'undefined') {
        return;
    }
    if (message == 'no_delivery') {
        return _t('site.ps', 'Sorry, the company does not/cannot ship to your area.');
    }
    if (message == 'no_texture') {
        return _t('site.ps', 'No manufacturers were found with the combination of materials and colors you have selected.');
    }
    if (message == 'large_size') {
        return _t('site.ps', 'No manufacturers were found with a build volume big enough to produce your order.');
    }
    if (message == 'no_active_machines' && psIdOnly) {
        return _t('site.ps', 'Sorry, the company has temporarily deactivated their machines');
    }

    return _t('site.ps', 'No manufacturers were found. Please try selecting different materials and/or colors.');
};

OffersBundle.prototype.isCantPrintInTexture = function () {
    if (this.messages[0] === 'no_texture') {
        return true;
    }
    return false;
};

OffersBundle.prototype.getBestOffer = function () {
    if (this.offers) {
        return _.first(this.offers);
    }
    return null;
};

/**
 * @property {PsPrinter[]} psPrinter
 * @property {Money} $anonimysPrice
 * @property {Money} $userPrice
 * @property {Money} $estimateDeliveryCost
 * @property {String|null} responseTime
 * @property {String|null} peliability
 * @property {String|null} certificatedMachine
 * @property {array} deliveryTypes
 * @param data
 * @constructor
 */
function Offer(data) {
    angular.extend(this, data);
    this.userPrice = new Money(data['userPrice']);
    this.anonimysPrice = new Money(data['anonimysPrice']);
    if (data['estimateDeliveryCost']) {
        this.estimateDeliveryCost = new Money(data['estimateDeliveryCost']);
    } else {
        this.estimateDeliveryCost = null;
    }
    this.reviewsInfo = this.psPrinter.reviewsInfo || {};
    this.reviewsInfo.rating = this.reviewsInfo.rating || 0;
    this.reviewsInfo.reviewsCount = this.reviewsInfo.reviewsCount || 0;
}


Offer.prototype.hasDelivery = function () {
    if ((this.deliveryTypes.indexOf('standard') !== -1) || (this.deliveryTypes.indexOf('intl') != -1)) {
        return true;
    }
    return false;
}

Offer.prototype.getDeliveriesString = function (showFreeDelivery) {
    var retVal = '';
    var isFreeDelivery = false;
    if ((this.deliveryTypes.indexOf('standard') !== -1) || (this.deliveryTypes.indexOf('intl') !== -1)) {
        if (this.estimateDeliveryCost && this.estimateDeliveryCost.amount === 0) {
            isFreeDelivery = true;
            if (showFreeDelivery) {
                retVal += _t('site.ps', 'Free delivery');
            }
        } else {
            retVal += _t('site.ps', 'Delivery');
        }
    }
    if (!isFreeDelivery && (this.deliveryTypes.indexOf('pickup') !== -1)) {
        if (retVal) {
            retVal = _t('site.ps', 'Pickup') + ', ' + retVal;
        } else {
            retVal = _t('site.ps', 'Pickup');
        }
    }
    return retVal;
};

Offer.prototype.companyRating = function (){
    return this.reviewsInfo.rating+'/5 ('+this.reviewsInfo.reviewsCount+')';
};

Offer.prototype.topCheckNoInfo = function () {
    return this.peliability === 'no info';
}

Offer.prototype.isOwnPrintPrice = function () {
    if (this.userPrice.amount !== this.anonimysPrice.amount) {
        return true;
    }
    return false;
};

Offer.prototype.certificatedMachineFormat = function () {
    if(this.certificatedMachine == 1) {
        return _t('site.ps', '1 certified machine');
    }
    return this.certificatedMachine + _t('site.ps',' certified machines ');
};

/**
 * @property {float} title
 * @property {string} logoUrl
 * @property {ReviewsInfo} reviewsInfo
 *
 * @param data
 * @constructor
 */
function Ps(data) {
    angular.extend(this, data);
}


/**
 * @property {int} id: 396
 * @property {Ps} ps
 * @property {bool} isStatusPro
 * @property {bool} isStatusCertificated
 * @property {string} address
 * @property {string} printerTitle
 * @property {string} publicLink
 *
 */
function PsPrinter(data) {
    angular.extend(this, data);
}


/**
 * @property {float} rating
 * @property {int} reviewsCount
 */
function ReviewsInfo(data) {
    angular.extend(this, data);
}

/**
 * @property {GroupMaterialsItem[]} list
 * @param data
 * @constructor
 */
function AllowedMaterialsList(data) {
    this.list = [];
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        var dataElement = data[key];
        var element = new GroupMaterialsItem(dataElement);
        this.list.push(element);
    }
}

AllowedMaterialsList.prototype.isEmpty = function () {
    return this.list.length === 0;
}

/**
 * @param materialsList
 */
AllowedMaterialsList.prototype.addMaterialsList = function (materialsList) {
    this.materialsList = materialsList || [];

    this.list.forEach(function (item) {
        for (var key in this.materialsList) {
            if (!this.materialsList.hasOwnProperty(key)) continue;
            var dataElement = this.materialsList[key];

            if (dataElement.group_id === item.materialGroupId) {
                item.materialList.push(dataElement);
            }
        }
    }, this);
};

AllowedMaterialsList.prototype.getGroupMaterialsItemByGroupId = function (id) {
    for (var key in this.list) {
        var groupColorItem = this.list[key];
        if (groupColorItem.materialGroupId == id) {
            return groupColorItem;
        }
    }
    return null;
};

/**
 *
 * @param groupId
 * @param materialId
 * @returns {null}
 */
AllowedMaterialsList.prototype.getColorsMaterialItem = function (groupId, materialId) {
    var groupMaterials = this.getGroupMaterialsItemByGroupId(groupId);
    if (!groupMaterials) {
        return null;
    }
    for (var key in groupMaterials.materials) {
        if (!groupMaterials.materials.hasOwnProperty(key)) continue;
        var materialInfo = groupMaterials.materials[key];

        if (!materialId || materialId == 'all') {
            // Send first material all
            return materialInfo;
        }
        if (materialId == materialInfo.materialId) {
            return materialInfo;
        }
    }
    return null;
};

/**
 * @param {int} id
 * @param {ModelTextureState} model3dTextureState
 * @param {int} selectedMaterialId
 * @returns {Array}
 */
AllowedMaterialsList.prototype.getColorsShortByGroupId = function (id, model3dTextureState, selectedMaterialId) {
    var materialColorItem = this.getGroupMaterialsItemByGroupId(id);
    if (materialColorItem) {
        var shortColorsList = materialColorItem.colorsShortList;

        if (selectedMaterialId) {
            if (materialColorItem.colorsShortListGroup.hasOwnProperty(selectedMaterialId)) {
                return materialColorItem.colorsShortListGroup[selectedMaterialId];
            }
        }

        // Add colors from model textures
        var textures = model3dTextureState.getTextures();
        for (var key in textures) {
            if (!textures.hasOwnProperty(key)) continue;
            var texture = textures[key];
            if (texture.printerMaterialGroup.id === id) {
                var colorExists = false;
                for (var key2 in shortColorsList) {
                    if (!shortColorsList.hasOwnProperty(key2)) continue;
                    var shortColor = shortColorsList[key2];
                    if (shortColor.id == texture.printerColor.id) {
                        colorExists = true;
                        break;
                    }
                }
                if (!colorExists) {
                    shortColorsList.push(texture.printerColor);
                }
            }
        }

        return shortColorsList;
    }
    return [];
};

AllowedMaterialsList.prototype.getColorsLongByGroupId = function (id, selectedMaterialId) {
    var materialColorItem = this.getColorsMaterialItem(id, selectedMaterialId);
    if (materialColorItem) {
        return materialColorItem.longColorsList;
    }
    return [];
};

/**
 * @param materialId
 * @returns {{id, group_id, title}|boolean}
 */
AllowedMaterialsList.prototype.getAllowedMaterialById = function (materialId) {
    return this.materialsList[materialId] || false;
};

/**
 * @returns {string[]}
 */
AllowedMaterialsList.prototype.getMaterialsList = function () {
    let list = {};
    for (var key in this.list) {
        if (!this.list.hasOwnProperty(key)) continue;
        var groupInfo = this.list[key];
        for (var materialKey in groupInfo.materials) {
            if (!groupInfo.materials.hasOwnProperty(materialKey)) continue;
            var materialInfo = groupInfo.materials[materialKey];
            if (materialInfo.materialId) {
                list[materialInfo.materialId] = materialInfo.materialId;
            }
        }
    }
    return Object.values(list);
};

/**
 * @property int materialGroupId
 * @property MaterialColorsItem[] materials
 * @param data
 * @constructor
 */
function GroupMaterialsItem(data) {
    this.materialGroupId = data['groupId'];
    this.materials = [];
    this.materialGroup = objectStorage().get('PrinterMaterialGroup', this.materialGroupId);

    for (var key in data.materials) {
        if (!data.materials.hasOwnProperty(key)) continue;
        var dataElement = data.materials[key];
        var material = new MaterialColorsItem(dataElement);
        this.materials[key] = material;
    }
}

/**
 *
 * @property int materialId
 * @property array materials
 *
 * @param data
 * @constructor
 */
function MaterialColorsItem(data) {
    angular.extend(this, data);
    if (this.materialId == 'all') {
        this.materialId = 0;
    }
    this.materialId = parseInt(this.materialId);

    this.shortColorsList = [];
    this.longColorsList = [];

    for (var key in this.shortList) {
        if (!this.shortList.hasOwnProperty(key)) continue;
        var dataElement = this.shortList[key];
        var element = objectStorage().get('PrinterColor', dataElement.colorId);
        this.shortColorsList.push(element);
    }

    for (var key in this.longList) {
        if (!this.longList.hasOwnProperty(key)) continue;
        var dataElement = this.longList[key];
        var element = objectStorage().get('PrinterColor', dataElement.colorId);
        this.longColorsList.push(element);
    }
}

/**
 *
 * @param id
 * @returns {objects|null}
 */
MaterialColorsItem.prototype.getMaterialById = function (id) {

    for (var i = 0; i < this.materialList.length; i++) {
        if (id === this.materialList[i].id) {
            return this.materialList[i];
        }
    }

    return null;
};

/**
 * @returns {string}
 */
MaterialColorsItem.prototype.getTitleSelectedMaterial = function (selectedMaterialId) {
    if (!this.materialId) {
        return _t('site.allmaterials', 'All');
    }
    var material = objectStorage().get('PrinterMaterial', this.materialId);
    if (!material) {
        return '';
    }
    return material.title;
};

/**
 *
 * @returns {boolean}
 */
MaterialColorsItem.prototype.isMaterialsList = function () {
    if (this.materialList && this.materialList.length > 0) {
        return !(this.materialList.length === 1 && this.materialList[0].title === this.materialGroup.title)
    }

    return false;
};

