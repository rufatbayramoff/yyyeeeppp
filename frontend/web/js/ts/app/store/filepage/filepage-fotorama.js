"use strict";


/**
 * Filpage fotorama controller
 */
app.directive('filepageFotorama', function (globalConfig, $objectStorage, $rootScope, $timeout, $modelRender) {
    /**
     *
     * @param model
     * @param {ModelTextureState} state
     * @returns {{allowfullscreen: string, ratio: string, loop: boolean, shadows: boolean, nav: string, swipe: boolean, click: boolean, thumbwidth: number, thumbheight: number, width: string, maxwidth: string, data: Array}}
     */
    var createInitConfig = function (model, state) {
        var config = {
            allowfullscreen: 'native',
            ratio: '4/3',
            loop: true,
            shadows: false,
            nav: 'thumbs',
            swipe: false,
            click: true,
            thumbwidth: 48,
            thumbheight: 48,
            width: '100%',
            maxwidth: '100%',
            data: []
        };

        angular.forEach(model.images, function (image) {
            config.data.push({
                img: image.previewUrl,
                thumb: image.previewUrl
            });
        });

        angular.forEach(model.parts, function (part) {

            var thumb = $modelRender.getFullUrl(part, state.getColorForRendering(part));

            config.data.push({
                img: thumb,
                thumb: thumb,
                caption: part.caption,
                part: part,
                reloadIfFailedTimeout: 2500
            });

        });

        return config;
    };

    /**
     * Fire part change event on root scope.
     * In data will actual paer or indefined if it image
     * @param fotorama
     */
    var firePartChange = function (fotorama) {
        var part = fotorama.activeFrame.part || undefined;
        $rootScope.$broadcast('filepage:changeSelectedPart', part);
    };

    /**
     * Change color on fotorama stage for part
     * @param {ModelTextureState} state
     * @param fotorama
     * @param {ModelPart} part
     * @param renders
     */
    var changeRenderColorForPart = function (state, fotorama, part, renders) {
        var fotoramaItem = _.find(fotorama.data, function (item) {
            return item.part == part;
        });

        var color = state.getColorForRendering(part);

        var thumb = $modelRender.getFullUrl(part, color);

        fotoramaItem.img = fotoramaItem.thumb = thumb;

        if (fotoramaItem.$navThumbFrame) {
            fotoramaItem.$navThumbFrame.find('img').attr('src', thumb);
        }

        if (renders[part.id]) {
            renders[part.id].updateColor(color);
        }
        else if (fotoramaItem.$stageFrame) {
            fotoramaItem.$stageFrame.find('img').attr('src', thumb);
        }
    };

    return {
        restrict: 'A',
        scope: {
            model: '=filepageFotorama',
            state: '=modelState',
            orderForm: '='
        },
        link: function (scope, element) {

            var inited = false;

            $timeout(function () {
                inited = true;
            });

            /**
             * Render objects
             * @type {Object.<int, model3dRenderClass>}
             */
            var renders = {};

            /**
             * @type {Fotorama}
             */
            var fotorama = element
                .fotorama(createInitConfig(scope.model, scope.state))
                .data('fotorama');

            firePartChange(fotorama, renders, scope.state);

            element.on('fotorama:showend', function () {
                firePartChange(fotorama, renders, scope.state);
            });

            element.on('fotorama:load', function (z, y, stage) {

                var part = stage.frame.part || undefined;
                var img = stage.frame.$stageFrame.find('img').addClass('model3dRenderLiveImg');

                if (part && !renders[part.id]) {
                    var render = commonJs.clone(model3dRenderClass);
                    render.init({
                        imgElementId: img,
                        fileUid: part.hash + '_' + part.fileExt.toLowerCase(),
                        isMulticolorFormat: part.isMulticolorFormat,
                        renderUrlBase: part.renderUrlBase,
                        color: scope.state.getColorForRendering(part),
                        ax: 0,
                        ay: 30,
                        az: 0,
                        pb: part.pb,
                        allowAutoRotate: false
                    });
                    renders[part.id] = render;
                }
            });


            var changeColorWatherUnbinders = [];

            scope.$watch('state.isOneTextureForKit', function (value) {
                // clear watchers
                angular.forEach(changeColorWatherUnbinders, function (unbinder) {
                    unbinder();
                });
                changeColorWatherUnbinders.length = 0;

                // add colro wather for all parts
                if (value) {
                    var unbinder = scope.$watch('state.kitTexture', function () {
                        if (!inited) {
                            return;
                        }
                        angular.forEach(fotorama.data, function (item) {
                            if (item.part) {
                                changeRenderColorForPart(scope.state, fotorama, item.part, renders)
                            }
                        });
                    });
                    changeColorWatherUnbinders.push(unbinder);
                }

                // add color wather per model
                else {
                    angular.forEach(scope.model.parts, function (part) {
                        var unbinder = scope.$watch('state.textures[' + part.id + ']', function () {
                            if (!inited) {
                                return;
                            }

                            changeRenderColorForPart(scope.state, fotorama, part, renders)
                        });
                        changeColorWatherUnbinders.push(unbinder);
                    });
                }
            });


            /**
             * Wath partsQtu for change caption
             */
            scope.$watch('orderForm.partsQty', function (partsQty) {
                if (!partsQty) {
                    return;
                }

                angular.forEach(fotorama.data, function (item) {
                    if (item.part && partsQty[item.part.fileId] != undefined && item.$stageFrame) {
                        item.$stageFrame.find('.qty-badge').text("x " + partsQty[item.part.fileId]);
                    }
                });

            }, true);
        }
    }
});

