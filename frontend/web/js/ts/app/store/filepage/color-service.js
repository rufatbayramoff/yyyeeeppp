"use strict";

app.factory('$colorService', function () {
    var $colorService = {};


    /**
     * @param {GroupMaterialsItem} newGroupMaterialsItem
     * @param {int} oldColorId
     * @returns {PrinterColor}
     */
    $colorService.newColorForMaterialColorsItem = function (newGroupMaterialsItem, oldColorId) {
        var allMaterials = newGroupMaterialsItem.materials[0];

        var oldColor = allMaterials.longList.find(function (printerColor) {
            return printerColor.colorId === oldColorId;
        });
        if (oldColor) {
            return oldColor;
        }
        return allMaterials.shortList[0]; // Return first color in group by default
    };
    return $colorService;
});