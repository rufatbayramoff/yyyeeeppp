"use strict";

app.directive('modelScale', ['$measure', '$measureConverter', '$user', '$mathUtils', '$timeout', '$rootScope', function ($measure, $measureConverter, $user, $mathUtils, $timeout, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            modelState: '=',
            maxModelArraySize: '=',
        },
        templateUrl: '/js/ts/app/store/filepage/filepage-scale.html',
        replace: true,
        link: function (scope, element) {
            var $scope = scope;
            $scope.scaleTypes = [
                {type: 'dimension',name: 'Dimensions'},
                {type: 'percent', name: 'Percentage'},
                {type: 'ration', name: 'Ratio'}
            ];

            $scope.maxModelSize = function() {
                return new Size({
                    width: $scope.maxModelArraySize[0],
                    length: $scope.maxModelArraySize[1],
                    height: $scope.maxModelArraySize[2],
                    measure: 'mm'
                });
            }

            $scope.scaleTypeChange = function (scale) {
                $scope.scaleType = scale;
            };

            $scope.initScale = function () {

                /**
                 *
                 * @type {*[]}
                 */
                $scope.allowedMeasures = [$measure.MEASURE_MM, $measure.MEASURE_IN];
                $scope.scaleType = $scope.scaleTypes[0];
                $scope.isSmall = false;
                $scope.isLarge = false;
                $scope.showProgress = false;

                /**
                 *
                 * @type {*}
                 */
                var metricMeasure = $scope.model.modelUnits;
                $scope.currentMeasure = $measure.getByMetric(metricMeasure);

                if (!$scope.model.getFirstActiveModel3dPart()) {
                    $scope.originalSize = new Size();
                    $scope.size = new Size();
                    return ;
                }
                /**
                 *
                 * @type {Size}
                 */
                var originalSize = $scope.model.getFirstActiveModel3dPart().originalSize;
                if (!originalSize) {
                    originalSize = $scope.model.getFirstActiveModel3dPart().size;
                }
                $scope.originalSize =  angular.copy(originalSize);

                /**
                 * @type {Size}
                 */
                $scope.size = new Size($scope.model.getFirstActiveModel3dPart().size);
                $scope.size = $measureConverter.roundSize($scope.size);
                if ($scope.size.measure != metricMeasure) {
                    $scope.size = $measureConverter.convertSize($scope.size, $scope.currentMeasure, true);
                    $scope.originalSize = $measureConverter.convertSize($scope.originalSize, $scope.currentMeasure);
                }

                $scope.initPercent();
                $scope.initRatio();
            };

            $scope.initPercent = function () {
                $scope.percent = 100;
            };

            $scope.initRatio = function () {
                $scope.ratio = 1;
            };

            $scope.initScale();

            $scope.onChangeWidth =  function () {
                $scope.setScalePercent($scope.size.width / $scope.originalSize.width);
            };

            $scope.onChangeHeight = function () {
                $scope.setScalePercent($scope.size.height / $scope.originalSize.height);
            };

            $scope.onChangeLength = function () {
                $scope.setScalePercent($scope.size.length / $scope.originalSize.length);
            };

            $scope.onChangeRatio = function (ratio) {
                $scope.ratio = ratio;
                $scope.setScalePercent($scope.ratio);
            };

            $scope.onChangePercent = function (percent) {
                $scope.percent = percent;
                $scope.setScalePercent($scope.percent / 100);
            };

            $scope.setScalePercent = function(percent)
            {
                $scope.size.width = $mathUtils.round($scope.originalSize.width * percent, 2);
                $scope.size.height = $mathUtils.round($scope.originalSize.height * percent, 2);
                $scope.size.length = $mathUtils.round($scope.originalSize.length * percent, 2);

                $scope.recalcIsSmallOrLarge();
            };

            $scope.recalcIsSmallOrLarge = function() {
                var allowedMin = $measureConverter.convert($measure.MEASURE_MM, $scope.currentMeasure, 10);
                $scope.isSmall = allowedMin > $scope.size.width && allowedMin > $scope.size.height && allowedMin > $scope.size.length;
                $scope.isLarge = $scope.size.isMoreThen(this.maxModelSize());
            };


            $scope.isSizeChanged = function()
            {
                return !(
                    $scope.size.width == $scope.originalSize.width &&
                    $scope.size.length == $scope.originalSize.length &&
                    $scope.size.height == $scope.originalSize.height
                );
            };


            /**
             *
             * @param measure
             */
            $scope.setCurrentMeasure = function (measure) {
                $scope.currentMeasure = measure;
                $scope.size = $measureConverter.convertSize($scope.size, $scope.currentMeasure, true);
                $scope.originalSize = $measureConverter.convertSize($scope.originalSize, $scope.currentMeasure);
            };

            $scope.getMaxModelSizeLabel = function () {
                let size = $scope.maxModelSize();
                let sizeInch = $measureConverter.convertSize(size, $measure.MEASURE_IN, true);
                return size.width + ' x ' + size.height + ' x ' + size.length + ' mm '
                    + '(' + sizeInch.width + ' x ' + sizeInch.height + ' x ' + sizeInch.length + ' in)';
            };


            /**
             *
             */
            $scope.apply = function () {
                $scope.showProgress = true;
                $rootScope.$broadcast('filepage:updateScale', $scope.size);
            };

            $rootScope.$on('filepage:scaleUpdated', function (params) {
                $scope.showProgress = false;
                $scope.recalcIsSmallOrLarge();
            });


            /**
             *
             */
            $scope.revertSize = function () {
                $scope.size = $measureConverter.roundSize(angular.copy($scope.originalSize));
                $scope.initPercent();
                $scope.initRatio();
                $scope.apply();
            };

            $rootScope.$on('filepage:setModelMeasure', function (params, measure) {
                $scope.currentMeasure = $measure.getByMetric(measure);
                $scope.size.measure = measure;
                $scope.recalcIsSmallOrLarge();
            });

            // ui init
            $timeout(function () {
                // disable dropdown links
                element.find('.js-measure-types li a').click(function (e) {
                    e.preventDefault(e);
                });

                // open scale if need
                if ($scope.isLarge || $scope.isSmall) {
                    element.find('#modelZoom').addClass('in');
                }
            });
        }
    };
}]);