"use strict";

DeliveryForm.prototype.load = function (data) {
    angular.extend(this, data);
    this.loaded = true;
};


/**
 * @property {bool} needLoad
 * @property {bool} loaded
 * @property {string} deliveryType
 * @property {string} contactName
 * @property {string} phone
 * @property {string} email
 * @property {string} comment
 * @property {string} phoneCountyIso
 * @property {string} phoneLineNumber
 *
 * @property {string} city
 * @property {string} country
 * @property {string} state
 * @property {string} street
 * @property {string} street2
 * @property {string} zip
 * @property {string} lat
 * @property {string} lon
 *
 * @property {bool}   forceInvalidAddress
 *
 * @property {array} allowedCountries
 *
 * @param data
 * @constructor
 */
function DeliveryForm(data) {
    if ((typeof (data) === 'undefined') || (Object.keys(data).length == 0)) {
        this.needLoad = true;
    }
    angular.extend(this, data);
}


DeliveryForm.prototype.load = function (data) {
    angular.extend(this, data);
    if (data['contact_name']) {
        this.contactName = data['contact_name'];
    }
    if (data['first_name']) {
        this.firstName = data['first_name'];
    }
    if (data['last_name']) {
        this.lastName = data['last_name'];
    }
    this.loaded = true;
};

DeliveryForm.prototype.needExpress = function () {
    return this.isTreatstock() && this.isUsa();
}

DeliveryForm.prototype.isTreatstock = function () {
    return this.deliveryTypeCarrier === 'treatstock' && this.deliveryType === 'standard';
}

DeliveryForm.prototype.isUsa = function () {
    return this.countryIso === 'US';
}

DeliveryForm.prototype.setGoogleAddress = function (address) {
    this.street = address.address;
    this.street2 = address.address2;
    this.city = address.city;
    this.country = this.countryIso = address.country_iso;
    this.state = address.region;
    this.zip = address.zip_code;
    this.lat = address.lat;
    this.lon = address.lon;
    this.forceInvalidAddress = 0;
    if(!this.needExpress()){
        this.shipping = 'standard';
    }
};

DeliveryForm.prototype.allowToSendToCountry = function (isoCode) {
    return (this.allowedCountries.indexOf(isoCode) > -1);
};


/**
 * @property {array} deliveryTypes
 * @property {string} pickupWorkTime
 * @property {bool} isInternational
 * @property {Location} location
 * @param data
 * @constructor
 */
function PsMachineDeliveryInfo(data) {
    this.load(data);
}

PsMachineDeliveryInfo.prototype.havePickup = function () {
    var returnValue = (typeof this.deliveryTypes !== 'undefined') && this.deliveryTypes.indexOf('pickup') !== -1;
    //console.log('HavePickup: '+returnValue);
    return returnValue;
};

PsMachineDeliveryInfo.prototype.haveDelivery = function () {
    var returnValue = (typeof this.deliveryTypes !== 'undefined') && this.deliveryTypes.indexOf('standard') !== -1;
    //console.log('HaveDelivery: '+returnValue);
    return returnValue;
};

PsMachineDeliveryInfo.prototype.haveIntl = function () {
    var returnValue = (typeof this.deliveryTypes !== 'undefined') && this.deliveryTypes.indexOf('intl') !== -1;
    //console.log('HaveDelivery: '+returnValue);
    return returnValue;
};

PsMachineDeliveryInfo.prototype.havePickupAndDelivery = function () {
    if (this.havePickup() && this.haveDelivery()) {
        return true;
    }
    return false;
};

PsMachineDeliveryInfo.prototype.load = function (data) {
    if (typeof (data) === 'undefined') {
        return;
    }
    angular.extend(this, data);
    if (typeof (data.location) !== 'undefined') {
        this.location = new Location(data['location']);
    }
};

PsMachineDeliveryInfo.prototype.isUsa = function () {
    if(!this.location){
        return false;
    }
    return this.location.countryIso === 'US';
}

/**
 * @property {string} address
 * @property {string} lat
 * @property {string} lon
 * @property {string} countryIso
 *
 */
function Location(data) {
    angular.extend(this, data);
}
