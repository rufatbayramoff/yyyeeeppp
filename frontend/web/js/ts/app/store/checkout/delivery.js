"use strict";

/**
 * Controller for manage printservice printer materials
 */
/** @namespace controllerParams.defaultDeliveryType */
/** @namespace controllerParams.hasDeliveryPickup */
/** @namespace controllerParams.onlyPickup */
/** @namespace controllerParams.modelPrice */
/** @namespace controllerParams.printPrice */
/** @namespace controllerParams.totalFeePrice */
/** @namespace controllerParams.mapConfig */

app.controller('OrderCheckoutDeliveryController', ['$scope', '$discount', '$notify', '$notifyHttpErrors', '$http', '$modal', '$q', '$user', 'controllerParams',
    function ($scope, $discount, $notify, $notifyHttpErrors, $http, $modal, $q, $user, controllerParams)
{
    $scope.DELIVERY_TYPE_PICKUP = 'pickup';
    $scope.DELIVERY_TYPE_DELIVERY = 'standard';

    $scope.view = {
        selectedDeliveryType : controllerParams.defaultDeliveryType,
        deliveryPrice : undefined,
        currencyLabel : controllerParams.currency,
        promocode: controllerParams.promocode || undefined
    };

    /**
     * Return form have delivery and pickup
     * @returns {boolean}
     */
    $scope.havePickupAndDelivery = function ()
    {
        return controllerParams.hasDeliveryPickup && !controllerParams.onlyPickup
    };

    /**
     *
     * @param price
     */
    $scope.updateDeliveryPrice = function (price)
    {
        $scope.view.deliveryPrice = price;
    };

    /**
     *
     */
    $scope.hasShippingPrice = function ()
    {
        return $scope.view.deliveryPrice !== undefined
            && $scope.view.deliveryPrice !== 0;
    };

    /**
     * Shipping price
     * @returns {string}
     */
    $scope.getShippingPrice = function()
    {
        if($scope.view.deliveryPrice === undefined){
            return _t('site.delivery', 'TBD');
        }
        return $scope.view.currencyLabel + $scope.view.deliveryPrice.toFixed(2);
    };

    function getDeliveryPriceInner(){
        return ($scope.view.deliveryPrice || 0);
    }
    function getTotalPriceInner(){

        return (controllerParams.modelPrice + controllerParams.printPrice + controllerParams.totalFeePrice + getDeliveryPriceInner());
    }
    function getTotalDiscountInner(){
        $discount.setRealPrices({
            model: controllerParams.modelPrice,
            print: controllerParams.printPrice,
            fee: controllerParams.totalFeePrice,
            delivery: getDeliveryPriceInner(),
            all: getTotalPriceInner()
        });
        return $discount.getTotalDiscount().toFixed(2);
    }

    $scope.getTotalDiscount = function()
    {
        if(getTotalDiscountInner()==0){
            return 'TBD';
        }
        return '-'  + $scope.view.currencyLabel + getTotalDiscountInner();
    };

    /**
     * Total price
     * @returns {string}
     */
    $scope.getTotalPrice = function ()
    {
        var totalPrice = getTotalPriceInner();
        var totalDiscount = getTotalDiscountInner();
        return $scope.view.currencyLabel + Math.abs(totalPrice - totalDiscount).toFixed(2);
    };

    /** apply promocodes **/

    $scope.getPromocode  = function()
    {
        return $scope.view.promocode;
    };
    $scope.applyPromocode = function()
    {
        var promocode = document.getElementById('promocode');
        return $http.post("/store/delivery/promo-apply", {'code' : promocode.value, 'prices': {
                model: controllerParams.modelPrice,
                print: controllerParams.printPrice,
                fee: controllerParams.totalFeePrice,
                delivery: getDeliveryPriceInner(),
                all: getTotalPriceInner()
            }}, {timeout: 10000})
            .then(function (response)
            {
                response = response.data;
                if (!response.success) {
                    var message = response.message || _t('site.order', 'Error');
                    $notify.warning(message, true);
                    promocode.value = '';
                    return;
                }
                //noinspection JSUnresolvedVariable
                if(response.discountSettings){
                    //noinspection JSUnresolvedVariable
                    $discount.settings = response.discountSettings;
                }
                $scope.view.promocode = response.promocode;
                $notify.success(response.message || 'Done', true);
            })
            .catch(function(data)
            {
                // if it no rejected by script - show errors
                if (data.data){
                    $notifyHttpErrors(data);
                }
            });
    };
    $scope.hasPromocode = function()
    {
        return $scope.view.promocode!=undefined;
    };
    $scope.noPromocode = function()
    {
        return $scope.view.promocode==undefined;
    };
    $scope.removePromocode = function()
    {
        if($scope.noPromocode()){
            return;
        }
        return $http.post("/store/delivery/promo-remove", {'code' : $scope.view.promocode}, {timeout: 10000})
            .then(function (response)
            {
                response = response.data;
                if (!response.success) {
                    var message = response.message || _t('site.order', 'Error');
                    $notify.warning(message, true);
                    return;
                }
                $notify.success(response.message || 'Done', true);
                $scope.view.promocode = undefined;
                $discount.settings = [];
            })
            .catch(function(data)
            {
                if (data.data){
                    $notifyHttpErrors(data);
                }
            });
    };

    /**
     * Check anonim user and submit form if all is ok
     * @param {String} userEmail User email
     * @param submitFormFn Function for submit form
     * @returns {*} Promise for buy button
     */
    $scope.checkAnonimUserAndSubmit = function (userEmail, submitFormFn)
    {
        /**
         *
         */
        var openOfferLoginModal = function ()
        {
            var proceedWithoutLogin = false;
            $modal.open({
                template : '/app/store/checkuot/delivery/offer-login-modal.html',
                scope : {
                    login : function()
                    {
                        //noinspection JSUnresolvedFunction
                        this.$dismiss();
                        $user.openLoginModal();
                        buyButtonDeffer.reject();
                    },

                    proceedWithoutLogin : function ()
                    {
                        proceedWithoutLogin = true;
                        //noinspection JSUnresolvedFunction
                        this.$dismiss();
                        submitFormFn();
                    }
                },
                onClose : function ()
                {
                    if(!proceedWithoutLogin){
                        buyButtonDeffer.reject();
                    }
                }
            });
        };

        var buyButtonDeffer = $q.defer();

        if ($user.isGuest) {

            $user.checkEmailExist(userEmail)
                .then(function (isExist)
                {
                    isExist ? openOfferLoginModal() : submitFormFn() ;
                }, $notifyHttpErrors);
        }
        else {
            submitFormFn();
        }

        return buyButtonDeffer.promise;
    };
}])

/**
 * Pickup controller
 */
.controller('OrderCheckoutDeliveryPickupController', ['$scope', '$notify', '$sessionStorage', '$user', 'controllerParams',
    function ($scope, $notify, $sessionStorage, $user, controllerParams)
{
    var SESSION_ADDRESS_STORAGE_KEY = "pickup_stored_address";

    var ctrl = this;

    $scope.view = {
        printerLocation : controllerParams.mapConfig
    };

    /**
     * Validae pickup form
     * @returns {Array}
     */
    ctrl.validateForm = function ()
    {
        var errors = [];

        if(!$scope.pickupDeliveryForm.phone) {
            errors.push(_t('store.order', 'Please, enter phone.'));
        }

        if(!$scope.pickupDeliveryForm.contact_name) {
            errors.push(_t('store.order', 'Please, enter full name.'));
        }

        if($user.isGuest && !$scope.pickupDeliveryForm.email) {
            errors.push(_t('store.order', 'Please, enter email.'));
        }

        return errors;
    };

    /**
     *
     * @param $event
     */
    $scope.buy = function ($event)
    {
        /**
         *
         */
        var submitForm = function()
        {
            $($event.currentTarget).closest('form').submit();
        };

        var errors = ctrl.validateForm();

        if (!_.isEmpty(errors)) {
            $notify.error(errors);
            return false;
        }

        $sessionStorage.set(SESSION_ADDRESS_STORAGE_KEY, $scope.pickupDeliveryForm);
        return $scope.checkAnonimUserAndSubmit($scope.pickupDeliveryForm.email, submitForm);
    };

    $scope.pickupDeliveryForm = $sessionStorage.get(SESSION_ADDRESS_STORAGE_KEY, controllerParams.pickupDeliveryForm) || {};

    $scope.updateDeliveryPrice(0);
}])

/** @namespace ratesResponse.validated_address */
/** @namespace ratesResponse.rates */
/** @namespace $scope.deliveryForm.contact_name */
/** @namespace $scope.deliveryForm.street */
/** @namespace $scope.deliveryForm.street2 */
/** @namespace $scope.deliveryForm.company */
/** @namespace cancelLoadRatesPromise.promise */
/**
 * Delivery controller
 */
.controller('OrderCheckoutDeliveryDeliveryController', [
    '$scope', '$notify', '$notifyHttpErrors', '$router', '$modal', '$http', '$sce', '$timeout', '$q', '$sessionStorage',
    '$deliveryRates', '$user', 'controllerParams',
    function ($scope, $notify, $notifyHttpErrors, $router, $modal, $http, $sce, $timeout, $q, $sessionStorage, $deliveryRates, $user, controllerParams)
{
    /**
     * Controller
     */
    var ctrl = self;

    var SESSION_ADDRESS_STORAGE_KEY = "delivery_stored_address";

    /**
     *
     * @type {string}
     */
    $scope.MODE_SELECTED_ADDRESS = 'selectedAddress';

    /**
     *
     * @type {string}
     */
    $scope.MODE_INPUT_ADDRESS = 'inputAddress';

    /**
     *
     * @type {{rates: undefined, deliveryRatesLoading: boolean, canBuy: boolean, mode: string}}
     */
    $scope.view = {
        rates : undefined,
        deliveryRatesLoading : false,
        canBuy : false,
        mode : $scope.MODE_INPUT_ADDRESS,
        currencyLabel : controllerParams.currency,
        error : undefined
    };

    /**
     *
     */
    $scope.deliveryForm = controllerParams.deliveryForm;
    $scope.currentLocation = controllerParams.currentLocation;

    /**
     *
     */
    $scope.userAddresses = controllerParams.userAddresses;

    /**
     * Have user stored address
     * @returns {boolean}
     */
    $scope.haveAddresses = function ()
    {
        return $scope.userAddresses.length > 0;
    };

    $scope.checkCurrentAndLastAddress = function()
    {

    };

    /**
     * Can press buy button
     * @returns {boolean}
     */
    $scope.canBuy = function ()
    {
        return !!$scope.deliveryForm.deliveryType;
    };

    /**
     *
     * @returns {boolean}
     */
    $scope.isCalculateShippingShowed = function ()
    {
        //noinspection JSUnresolvedVariable
        return !($scope.view.rates && $scope.view.rates[0].isEstimate);
    };

    /**
     *
     * @param errors
     */
    ctrl.onErrorLoadRates = function (errors)
    {
        $scope.view.deliveryRatesLoading = false;
        if (errors){
            var message = commonJs.prepareMessage(errors);
            $scope.view.error = $sce.trustAsHtml(message);
            $notify.warning(message, true);
        }
        return $q.reject(errors);
    };

    /**
     *
     * @param rates
     */
    ctrl.onSuccessLoadRates = function (rates)
    {
        $scope.view.deliveryRatesLoading = false;
        $notify.success(_t('site.store', 'Shipping address confirmed'), true);
        $scope.renderRates(rates);
    };

    /**
     * Validae pickup form
     * @returns {Array}
     */
    ctrl.validateForm = function ()
    {
        var errors = [];

        if($user.isGuest && !$scope.deliveryForm.email) {
            errors.push(_t('store.order', 'Please, enter email.'));
        }

        return errors;
    };

    /**
     *
     */
    $scope.buy = function ($event)
    {
        var submitFormFn = function() {
            $($event.currentTarget).closest('form').submit();
        };

        if(!$scope.canBuy()){
            return;
        }

        var errors = ctrl.validateForm();

        if (!_.isEmpty(errors)) {
            $notify.error(errors);
            return false;
        }

        //noinspection JSUnresolvedVariable
        if (!$scope.view.rates[0].isEstimate) {
            return $scope.checkAnonimUserAndSubmit($scope.deliveryForm.email, submitFormFn);
        }

        var onSuccessLoadRates = function (rates)
        {
            //noinspection JSUnresolvedVariable
            if(!rates[0].isEstimate){
                ctrl.onSuccessLoadRates(rates);
                $notify.info(_t('site.order', 'Shipping rates changed.'));
            }
            else {
                return $scope.checkAnonimUserAndSubmit($scope.deliveryForm.email, submitFormFn);
            }
        };

        return $deliveryRates.loadRates($scope.deliveryForm)
            .then(onSuccessLoadRates, ctrl.onErrorLoadRates);
    };


    /**
     * Save session address to session storage
     */
    ctrl.saveSessionAddress = function ()
    {
        if ($scope.view.mode == $scope.MODE_INPUT_ADDRESS){
            var sessionAddress = {
                country : $scope.deliveryForm.country,
                state : $scope.deliveryForm.state,
                city : $scope.deliveryForm.city,
                street : $scope.deliveryForm.street,
                street2 : $scope.deliveryForm.street2,
                zip : $scope.deliveryForm.zip,
                phone : $scope.deliveryForm.phone,
                email : $scope.deliveryForm.email,
                company : $scope.deliveryForm.company,
                contact_name : $scope.deliveryForm.contact_name
            };

            $sessionStorage.set(SESSION_ADDRESS_STORAGE_KEY, sessionAddress);
        }
    };


    /**
     * Update rates
     */
    $scope.updateRates = function ()
    {
        $scope.view.deliveryRatesLoading = true;
        $scope.resetRates();
        ctrl.saveSessionAddress();



        return $deliveryRates.loadRates($scope.deliveryForm)
            .then(ctrl.onSuccessLoadRates, ctrl.onErrorLoadRates);
    };

    /**
     *
     * @param rates
     */
    $scope.renderRates = function(rates)
    {
        $timeout(function () {
            $scope.view.rates = rates;
            $scope.deliveryForm.deliveryType = rates[0].code;
            $scope.updateDeliveryPrice(rates[0].cost.amount);
        });
    };

    /**
     *
     */
    $scope.resetRates = function ()
    {
        $deliveryRates.cancelLoadRates();
        $scope.view.rates = undefined;
        $scope.view.error = undefined;
        $scope.deliveryForm.deliveryType = undefined;
        $scope.updateDeliveryPrice(undefined);
    };

    /**
     *
     */
    $scope.changeSelectedAddress = function ()
    {
        $scope.view.mode = $scope.MODE_INPUT_ADDRESS;
    };

    /**
     *
     * @param address
     */
    $scope.setExistedAddress = function (address)
    {
        $scope.view.mode = $scope.MODE_SELECTED_ADDRESS;
        angular.extend($scope.deliveryForm, address);
        $timeout($scope.updateRates);
    };

    // watch address for reset rates on change address
    $scope.$watchGroup(['deliveryForm.country', 'deliveryForm.state', 'deliveryForm.city', 'deliveryForm.street', 'deliveryForm.street2',
        'deliveryForm.zip', 'deliveryForm.email'], function (newVal, oldVal)
    {
        // if we have estmate price and not change country - dont reset rates
        //noinspection JSUnresolvedVariable
        if ($scope.view.rates && $scope.view.rates.length > 0 && $scope.view.rates[0].isEstimate && newVal[0] == oldVal[0]) {
            return;
        }

        $scope.resetRates();
    });


    $scope.$on('$destroy', $deliveryRates.cancelLoadRates);

    // set stored address, from session storage or from earler saved addresses
    var sessionAddress = $sessionStorage.get(SESSION_ADDRESS_STORAGE_KEY);
    if (sessionAddress) {
        angular.extend($scope.deliveryForm, sessionAddress);
    }
    else if($scope.haveAddresses()){
        var userAddress = $scope.userAddresses[0];
        var needSetFlag = true;
        if ($scope.currentLocation ) {
            var currentLocation = $scope.currentLocation;
            if (currentLocation['city'] != userAddress['city'] || currentLocation['country'] != userAddress['country']) {
                $scope.changeSelectedAddress();
                userAddress = {'city': currentLocation['city'], 'region': currentLocation['region'], 'country': currentLocation['country']}
                needSetFlag = false;
            }
        }
        if(needSetFlag)
            $scope.setExistedAddress(userAddress);
    }

    // if we have estimate rates - render rates

    //noinspection JSUnresolvedVariable
    if (controllerParams.rates && controllerParams.rates.length > 0 && controllerParams.rates[0].isEstimate && $scope.deliveryForm.country == 'US') {
        $scope.renderRates(controllerParams.rates);
    }
}])



.factory('$deliveryRates', ['$http', '$q', '$modal', function ($http, $q, $modal)
{
    var $deliveryRates = {};

    var cancelLoadRatesPromise = undefined;

    var confirmValidatedAddress = function(validatedAddress)
    {
        var deffer = $q.defer();
        $modal.open({
            template : '/app/store/checkuot/delivery/validate-address-modal.html',
            scope : {
                address : validatedAddress,
                isResult : false,   // falg that user made decision
                confirmAddress : function()
                {
                    //noinspection JSUnresolvedFunction
                    this.$dismiss();
                    this.isResult = true;
                    deffer.resolve();
                },

                declineAddress : function () {
                    //noinspection JSUnresolvedFunction
                    this.$dismiss();
                    this.isResult = true;
                    deffer.reject();
                },
            },
            onClose : function () {
                if (!this.isResult) {
                    deffer.reject();
                }
            }
        });
        return deffer.promise;
    };


    /**
     *
     * @param deliveryForm
     * @returns {*}
     */
    $deliveryRates.loadRates = function (deliveryForm)
    {
        cancelLoadRatesPromise = $q.defer();

        var deffer = $q.defer();

        /**
         *
         * @param response
         */
        var onSuccessLoad = function (response)
        {
            var ratesResponse = response.data;

            if (!ratesResponse.success) {
                var message = ratesResponse.message || _t('site.order', 'Done');
                deffer.reject(message);
                return;
            }

            if(ratesResponse.validated_address){

                confirmValidatedAddress(ratesResponse.validated_address)
                    .then(
                        function () {
                            angular.extend(deliveryForm, ratesResponse.validated_address);
                            deffer.resolve(ratesResponse.rates);
                        },
                        function () {
                            deffer.reject(_t('site.order', "The address you have entered is invalid. Please re-enter a valid shipping address."));
                        }
                    );
                return;
            }

            deffer.resolve(ratesResponse.rates);
        };

        /**
         * On error on load rates
         * @param response
         */
        var onErrorLoad = function (response)
        {
            return deffer.reject(response.data);
        };

        $http.post("/store/delivery/load-rates", deliveryForm, {timeout: cancelLoadRatesPromise.promise})
            .then(onSuccessLoad, onErrorLoad);

        return deffer.promise;
    };


    /**
     * Cancel load rates
     */
    $deliveryRates.cancelLoadRates = function ()
    {
        if (cancelLoadRatesPromise){
            cancelLoadRatesPromise.resolve();
            cancelLoadRatesPromise = undefined;
        }
    };

    return $deliveryRates;
}])

/**
 *
 */
.filter('addressTitle', function ()
{
    return function (address)
    {
        return address.contact_name + (address.company || '');
    };
})

/**
 *
 */
.filter('addressBody', ['$sce', function ($sce)
{
    return function (address)
    {
        var addressStr = (address.street || '') + " " + (address.street2 || '') + '<br/>'
            + (address.city || '') + " " + (address.state || '') + " " + (address.zip || '') + '<br/>'
            + (address.country || '');

        if(address.phone){
            addressStr += '<br/>' + _t('site.store', 'Phone') + ': ' + address.phone;
        }
        //noinspection JSUnresolvedFunction
        return $sce.trustAsHtml(addressStr);
    };
}])

.directive('select2', function () {
    return {
        restrict: 'A',
        link : function (scope, element)
        {
            $(element).select2();
        }
    };
});

