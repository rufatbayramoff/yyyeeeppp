app.directive('printerLocationMap', function ()
{
    return {
        restrict : 'A',
        scope : {
            printerLocationMap : '='
        },
        link : function(scope, element)
        {
            var myOptions = {
                zoom : 15,
                center : new google.maps.LatLng(scope.printerLocationMap.lat, scope.printerLocationMap.lon),
                mapTypeId : google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(element[0], myOptions);
            var marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(scope.printerLocationMap.lat, scope.printerLocationMap.lon)});
            var infowindow = new google.maps.InfoWindow({content : scope.printerLocationMap.address});
            infowindow.open(map,marker);
        }
    }
});
