"use strict";

var socialLikesButtons = {
    reddit: {
        popupUrl: 'https://reddit.com/submit?url={url}',
        popupWidth: 650,
        popupHeight: 500
    },
    linkedin: {
        popupUrl: 'https://www.linkedin.com/shareArticle?mini=true&url={url}&source=treatstock',
        popupWidth: 650,
        popupHeight: 500
    }
};


/**
 * Controller for manage printservice printer materials
 */
app.controller('ShareReview', function ($scope, $shareService) {
    /**
     * Load share prototype and open share modal.
     *
     * @param {int} reviewId
     */
    $scope.shareReview = function (reviewId) {
        return $shareService.shareReview(reviewId);
    };

})


    .factory('$shareService', function ($modal, $http, $q, $notify, $notifyHttpErrors, globalConfig) {

        window.fbAsyncInit = function () {
            FB.init({
                appId: globalConfig.facebook_app_id,
                xfbml: true,
                version: 'v2.5'
            });
        };


        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        var getShareLink = function (link) {
            return "<div class='text-center'><input onfocus='$(this).select()' class='form-control m-b10 share-input' type='text' value='" + link
                + "' /> <button class='btn btn-primary'>OK</button> </div>";
        };


        var $shareService = {};


        /**
         * Load share prototype and open share modal.
         *
         * @param {int} reviewId
         */
        $shareService.shareReview = function (reviewId) {
            return $http.get('/reviews/share', {params: {reviewId: reviewId}})
                .then(function (repsonse) {
                    return $shareService.openCreateShareModal(repsonse.data);
                }, $notifyHttpErrors);
        };

        /**
         * Open create share modal.
         *
         * @param share
         */
        $shareService.openCreateShareModal = function (share) {

            var modalDeffer = $q.defer();

            $modal.open({
                template: '/app/review/review-share-modal.html',
                scope: {
                    share: share,
                    form: {
                        editState: false,
                        customPhoto: [],
                        customPhotoSrc: undefined
                    }
                },
                controller: function ($scope) {
                    /**
                     *
                     * @return {boolean}
                     */
                    $scope.isCanSelectPhoto = function () {
                        return $scope.share.canEdit || $scope.share.review.files.length > 1;
                    };

                    /**
                     *
                     * @return {boolean}
                     */
                    $scope.isPhotoBlockVisible = function () {
                        return $scope.share.canEdit || $scope.share.review.files.length > 0;
                    };

                    /**
                     *
                     * @return {boolean}
                     */
                    $scope.showSelectPhotoHeader = function () {
                        return $scope.form.customPhoto.length > 0 || $scope.share.review.files.length > 0;
                    };
                    /**
                     * Show edit text controls.
                     */
                    $scope.edit = function () {
                        $scope.form.editState = true;
                    };

                    /**
                     * Load new photo thumb.
                     *
                     * @param {File} file
                     */
                    $scope.loadCustomPhoto = function (file) {
                        if (file && window.FileReader) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $scope.$apply(function () {
                                    $scope.form.customPhotoSrc = e.target.result;
                                });
                            };
                            reader.readAsDataURL(file);
                        }
                    };

                    /**
                     * Clear uploaded photo,
                     */
                    $scope.removeUploadedImage = function () {
                        $scope.form.customPhoto = [];
                        $scope.form.customPhotoSrc = undefined;
                    };

                    /**
                     * Create share.
                     *
                     * @param {string} socialType
                     * @param $event
                     */
                    $scope.save = function (socialType, $event) {

                        var data = {
                            text: $scope.share.text,
                            photoId: $scope.form.customPhoto.length ? undefined : $scope.share.photo_id,
                            photo: $scope.form.customPhoto.length ? $scope.form.customPhoto[0] : undefined,
                            socialType: socialType
                        };

                        return $http.post('/reviews/create-share', data, {params: {reviewId: share.review.id}})
                            .then(function (response) {
                                return $scope.openSocialModal(response, $event);
                            })
                            .then($scope.$dismiss, $notifyHttpErrors)
                    };

                    $scope.copyAffiliateUrlToClipboard = function () {
                        $('#affiliateUrl').select();
                        document.execCommand("copy");
                    };


                    /**
                     * Open modal window woth share.
                     *
                     * @param {*} response
                     * @param $event
                     */
                    $scope.openSocialModal = function (response, $event) {
                        var share = response.data;
                        var deffer = $q.defer();

                        var socialType = share.social_type;

                        if (socialType === 'google') {
                            socialType = 'plusone';
                        }

                        if (socialType === 'facebook_im') {
                            FB.ui({
                                method: 'send',
                                link: share.url
                            }, function () {
                                deffer.resolve(socialType);
                            });

                            return deffer.promise;
                        }

                        if (socialType === 'facebook_im') {
                            FB.ui({
                                method: 'send',
                                link: share.url
                            }, function () {
                                deffer.resolve(socialType);
                            });
                            return deffer.promise;
                        }

                        if (socialType === 'link') {
                            var content = $(getShareLink(share.url));
                            content.find('button').click(function () {
                                deffer.resolve(socialType);
                            });

                            var shareButton = $($event.currentTarget).closest('button');
                            shareButton.popover({trigger: 'focus', html: true, content: content});
                            shareButton.popover('toggle');
                            return deffer.promise;
                        }

                        if (socialType === 'affiliate') {
                            var content = $(getShareLink(share.affiliateUrl));
                            content.find('button').click(function () {
                                deffer.resolve(socialType);
                            });

                            var shareButton = $($event.currentTarget).closest('button');
                            shareButton.popover({trigger: 'focus', html: true, content: content});
                            shareButton.popover('toggle');
                            return deffer.promise;
                        }

                        var el = $('<div><div class="' + socialType + '">' + socialType + '</div></div>');

                        el.socialLikes({url: share.url});

                        el.on('popup_opened.social-likes', function (event, service) {
                            if ((typeof (ga) != 'undefined') && ga) {
                                ga('send', 'event', {
                                    eventCategory: 'SocialShare',
                                    eventAction: service,
                                    eventLabel: location.href
                                });
                            }
                        });

                        el.on('popup_closed.social-likes', function (event, service) {
                            deffer.resolve(service);
                        });

                        el.find('div').trigger('click');

                        return deffer.promise;
                    }

                },
                onShown: function () {
                    new Swiper('.ps-profile-user-review__user-models', {
                        scrollbar: '.ps-profile-user-review__user-models-scrollbar',
                        scrollbarHide: true,
                        slidesPerView: 'auto',
                        grabCursor: true
                    });
                },
                onClose: function () {
                    modalDeffer.resolve(true);
                }
            });

            return modalDeffer.promise;
        };

        return $shareService;
    })




    /**
     * Directive for show ps rating as starts with some texts.
     */
    .directive('psRating', function () {
        return {
            restrict: 'E',
            template: "<div> <div style='display: inline-block'><input/></div> <span class='star-rating-range'><strong>{{rating}}</strong>/5</span> <span class='star-rating-count'>(<span>{{reviewsCount}}</span> {{getReviewCountHint()}})</span> </div>",
            replace: true,
            scope: {
                rating: '=',
                reviewsCount: '='
            },
            link: function (scope, element) {
                $(element).find('input').val(scope.rating).rating({
                    showCaption: false,
                    showClear: false,
                    min: 0,
                    max: 5,
                    size: "xs",
                    readonly: true,
                    starCaptions: {
                        1: _t('site.common', 'Very Bad'),
                        2: _t('site.common', 'Bad'),
                        3: _t('site.common', 'Good'),
                        4: _t('site.common', 'Very Good'),
                        5: _t('site.common', 'Perfect')
                    }
                });

                scope.getReviewCountHint = function () {
                    return scope.reviewsCount > 1 ? _t('site.common', 'reviews') : _t('site.common', 'review');
                }
            }
        }
    });