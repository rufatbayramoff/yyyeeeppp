"use strict";

/**
 * @property {Review[]} parts
 *
 * @param {array} data
 */
function ReviewsBundle(data) {
    this.parts = [];

    this.parts = data.map(function (part) {
        return new Review(part);
    });
}

/**
 * @returns {boolean}
 */
ReviewsBundle.prototype.haveReviews = function () {
    return this.parts.length > 0;
};

// ------------------

/**
 * @property {string} comment
 * @property {string} coverImgUrl
 * @property {string} urlPs
 * @property {string} createdAt
 * @property {string} answer
 * @property {ReviewUser} user
 * @property {RatingInfo} rating
 *
 * @param {array} data
 */
function Review(data) {
    angular.extend(this, data);
    this.user = new ReviewUser(data.user);
    this.rating = new RatingInfo(data.ratingInfo);
}

/**
 * @returns {boolean}
 */
Review.prototype.haveCoverImg = function () {
    return !!this.coverImgUrl;
};

/**
 * @returns {boolean}
 */
Review.prototype.haveComment = function () {
    return !!this.comment;
};

// ------------------

/**
 * @property {string} fullName
 * @property {string} avatarUrl
 * @property {string} publicProfileUrl
 *
 * @param {array} data
 */
function ReviewUser(data) {
    angular.extend(this, data);
}

/**
 * @returns {boolean}
 */
ReviewUser.prototype.haveAvatar = function () {
    return !!this.avatarUrl;
};

// ------------------

/**
 * @property {float} ratingAll
 * @property {float} ratingSpeed
 * @property {float} ratingQuality
 * @property {float} ratingCommunication
 *
 * @param {array} data
 */
function RatingInfo(data) {
    angular.extend(this, data);
}

/**
 * @returns {number}
 */
RatingInfo.prototype.getRatingAllPercent = function () {
    return this.ratingAll / 5 * 100;
};


