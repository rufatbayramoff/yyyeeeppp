$(function(){
    $('body').scrollspy({target: "#jsprod-nav", offset: 200});
    $('.ugc-content table').wrap('<div class="table-responsive m-t10"></div>');
    $(".productBlockTab").click(function (el) {
        var blockId = $(this).find('[role="tab"]').attr("href")
        $('.productBlockTabs li').removeClass('active');
        $(this).parent().addClass('active');

        $('html, body').animate({
            scrollTop: $(blockId).offset().top - 150
        }, 500);
    });
    setTimeout(function () {
        var navOffset = $('.product-page__nav').offset().top - $('.product-page__nav').outerHeight(true) + 30;
        $('#jsprod-nav').affix({offset: {top: navOffset}});
    }, 100);
});