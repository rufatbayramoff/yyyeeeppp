TS = TS || {};

/**
 * My Store 
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 * @type type
 */
TS.Store = {
    
    /**
     * callback function called after ajax request completed.
     * 
     * @param {string} btn - clicked button
     * @param {string} json - ajax json result     
     */
    itemRemoved : function(btn, json)
    {
        btn.parent('.panel-body').html(json.message);
        btn.hide();
    },
    
    /**
     * more logic after item published.
     * if no Tax information for user, show him notification message box
     * 
     * @param {string} btn
     * @param {string} json
     * @returns {boolean}
     */
    itemPublished : function (btn, json, modalId)
    {
        $(modalId).find('.modal-body').addClass('alert-success').html(json.message);
        setTimeout(function(){
            $(modalId).modal('hide');
            if(json.messageTax && json.messageTax!=""){
                TS.showNeedTaxInfo(json.messageTax);
            }
        }, 2000);
        return true;
    }  
}; 