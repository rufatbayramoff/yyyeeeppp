/**
 * Uploaded model
 * @type Model|function
 */
TS.Uploader.UploadModel = Backbone.Model.extend({
    defaults: {
        id: null,
        item: null,
        picture: null
    }
});

/**
 * Collection of uploaded models
 * @type Collection|function
 */
TS.Uploader.UploadCollection = Backbone.Collection.extend({
    model: TS.Uploader.UploadModel
});

/**
 * View for edit upload
 */
TS.Uploader.EditView = Backbone.View.extend({

    /** @namespace this.restrictions */
    /** @namespace this.restrictions.modelExtensions */
    /** @namespace this.restrictions.modelsLimit */
    /** @namespace this.restrictions.maxFileSizeLimit */
    /** @namespace this.restrictions.imagesLimit */
    /** @namespace this.restrictions.imageExtensions */
    /** @namespace this.restrictions.archiveExtensions */
    /** @namespace this.restrictions.totalFilesLimit */
    /** @namespace this.messages.unsupportedFileTypes */

    /**
     * Urls
     */
    urls : {
        loginUrl : '/user/login/?return=/upload',
        uploadUrl : '/upload/file-upload',
        uploadFinish : '/upload/draft'
    },

    /**
     * Events
     */
    events:
    {
        'click .ts-uploader-save' : 'onSaveClick'
    },

    /**
     * Init
     */
    initialize: function(options)
    {
        var self = this;
        this.$saveButton = this.$('.ts-uploader-save');

        _.extend(this, options);
        this.files.model3d = new TS.Uploader.UploadCollection(this.files.model3d);
        this.files.image = new TS.Uploader.UploadCollection(this.files.image);
        this.files.archives = new TS.Uploader.UploadCollection(this.files.archives);
        TS.Uploader.msg = this.messages;


        this.modelDropzone = TS.Uploader.getDropZone(
            '#ts-uploader1', this.createExtensionsString(this.restrictions.modelExtensions),
            this.files.model3d,
            {maxFiles: this.restrictions.modelsLimit, maxFilesize: this.restrictions.maxFileSizeLimit}
        );
        this.imageDropzone = TS.Uploader.getDropZone(
            '#ts-uploader2', this.createExtensionsString(this.restrictions.imageExtensions),
            this.files.image,
            {maxFiles: this.restrictions.imagesLimit, maxFilesize: this.restrictions.maxFileSizeLimit}
        );

        this.archiveDropzone = TS.Uploader.getDropZone(
            '#ts-uploader3', this.createExtensionsString(this.restrictions.archiveExtensions),
            this.files.archives,
            {maxFiles: this.restrictions.imagesLimit, maxFilesize: this.restrictions.maxFileSizeLimit}
        );

        this.commonDropzone = new Dropzone('#ts-uploader', {
            paramName: "UploadForm[file]",
            addRemoveLinks: false,
            uploadMultiple: false,
            parallelUploads: 15,
            autoQueue : false,
            dictFileTooBig: "File is too big ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
            maxFilesize: this.restrictions.maxFileSizeLimit,
            maxFiles: this.restrictions.totalFilesLimit,
            clickable : (this.files.image.length > 0 || this.files.model3d.length > 0) ? false : '.upload-start .fileinput-button',
            url: this.urls.uploadUrl,
            previewsContainer : '#ts-uploader-hidden',
            previewTemplate : '<div></div>',
            acceptedFiles : this.createExtensionsString(this.restrictions.imageExtensions, this.restrictions.modelExtensions, this.restrictions.archiveExtensions),
            init : function(){
                this.on("addedfile", function(file) {
                    if(file.status=='error'){
                        return false;
                    }
                });
                this.on("error", function(file, message) {
                    new TS.Notify({
                        type: 'error',
                        text: 'Error. ' + message,
                        target: '.messageBox'
                    });

                    setTimeout(function(){
						this.removeFile(file);
                        window.location.href = "/upload/edit";
                    }, 2000);
                });
            },
            accept: function(file)
            {
                var fileExt = file.name.split('.').pop().toLowerCase();

                $('.ts-uploader-content').show();

                if($('.upload-start')) $('.upload-start').hide();

                // detect file type
                if(_.contains(self.restrictions.archiveExtensions, fileExt)) {
                    self.archiveDropzone.addFile(file);
                }else if(_.contains(self.restrictions.modelExtensions, fileExt)){
                    self.modelDropzone.addFile(file);
                }else if(_.contains(self.restrictions.imageExtensions, fileExt)){
                    self.imageDropzone.addFile(file);
                }else{
                    new TS.Notify({
                        type: 'error',
                        text: self.messages.unsupportedFileTypes,
                        target: '.messageBox'
                    });
                    return false;
                }

                TS.Uploader.updateHeader('#ts-uploader1');
            }
        });

        this.addDropzoneCheckSaveListener([this.modelDropzone, this.imageDropzone, this.commonDropzone, this.archiveDropzone]);


        // init base functions
        $('#uploadform-title').change(function()
        {
            $(this).addClass('changed');
        });

        // cancel upload
        $('.ts-uploader-cancel').click(function()
        {
            if(confirm(TS.Uploader.msg.cancelUpload)){
                TS.Uploader.cancelUpload();
            }
        });

        if(TS.User.isGuest){
        }else{
            // if we have cookie, user request to submit file after login
            var t = TS.getCookie("afterlogin_uploadsubmit");
            if(t){
                $("#uploadform-title").val(TS.getCookie("afterlogin_uploadsubmit_title"));
                $("#uploadform-description").val(TS.getCookie("afterlogin_uploadsubmit_info"));
                // remove old cookies
                TS.setCookie("afterlogin_uploadsubmit", "");
                TS.setCookie("afterlogin_uploadsubmit_title", "");
                TS.setCookie("afterlogin_uploadsubmit_info", "");
                //$(".ts-uploader-save").click();
                window.location.href =  this.urls.uploadFinish + '?saveclick=2';
            }
        }
        // sortable items
        TS.Uploader.initSortable('#' + this.modelDropzone.element.id + '-preview');
        TS.Uploader.initSortable('#' + this.imageDropzone.element.id + '-preview');
    },

    /**
     * Create extensions for Dropbox from array of extensions
     * @returns {string}
     */
    createExtensionsString : function()
    {
        var extArray = [].concat.apply([], arguments);
        extArray = _.map(extArray, function(ext){ return '.' + ext; });
        return extArray.join(',').toLowerCase();
    },

    /**
     * On save button click
     * @param event
     * @returns {boolean}
     */
    onSaveClick : function(event)
    {
        var finishUrl = this.urls.uploadFinish + '?saveclick=1';
        if(TS.User.isGuest){
            event.preventDefault();
            // after login 
            TS.setCookie("afterlogin_uploadsubmit", "1");
            TS.setCookie("afterlogin_uploadsubmit_title", $("#uploadform-title").val());
            TS.setCookie("afterlogin_uploadsubmit_info", $("#uploadform-description").val());
            TS.Visitor.loginForm(finishUrl);
            return false;
        }
        else{
            window.location.href = finishUrl;
            $('#cancel_button_block').hide();
            $('#creating_model3d_progress').removeClass('hidden');
        }
    },


    // CHECK THAT WE CAN SAVE ------------------------------------------------------------------------------------------

    /**
     * Add listeners to dropzone for track that save button can be saved
     */
    addDropzoneCheckSaveListener : function(dropzones)
    {
        var self = this;

        /**
         * Resolve that upload can be save and set disabled to Save button
         */
        var updateSaveButtonDisable = function()
        {
            self.setDisabledSaveButton(!self.resolveCanBeSaved());
        };
        var updateSaveButtonDisableComplete = function()
        {
            if(self.resolveCanBeSaved()){
                setTimeout(function(){
                    //$(".ts-uploader-save").html("clicking...");
                    self.$saveButton.click();
                }, 200);
                //$( ".ts-uploader-save").trigger( "click" );
            }
        };
        _.each(dropzones, function(dropzone)
        {
            dropzone
                .on('sending', updateSaveButtonDisable)
                .on('error', updateSaveButtonDisable)
                .on('success', updateSaveButtonDisableComplete)
                .on('complete', updateSaveButtonDisable)
        });
    },

    /**
     * Set disabled attribute for Save button
     * @param disabled
     */
    setDisabledSaveButton : function (disabled)
    {
        if(disabled)
        {
            this.$saveButton
                .prop('disabled', true)
                .prop('title', 'Wait, loading file...');
        }
        else
        {
            var me = this;

            me.$saveButton
                        .prop('disabled', false)
                        .prop('title', '');
             //setTimeout(function(){}, 1500);
            // we can submit form now 
            //me.submitUploadForm();
        }
    },
    submitUploadForm: function()
    {
        $('.ts-uploader-form form').submit();
    },
    /**
     * Resolve that upload can be saved
     * @returns {boolean}
     */
    resolveCanBeSaved : function()
    {
        var result = true;
        _.each([this.commonDropzone, this.imageDropzone, this.modelDropzone, this.archiveDropzone], function(dropzone)
        {
            if(!_.isEmpty(dropzone.getUploadingFiles()))
            {
                result = false;
            }
        });
        return result;
    }

});