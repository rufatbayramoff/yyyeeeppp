/**
 * Uploader 
 * 
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
TS = TS || {};

TS.Uploader = {

    msg: {

    },
    urls : {
        loginUrl : '/user/login/?return=/upload',
        uploadUrl : '/upload/file-upload'
    },
    jobRequestLimit : 20,
    jobRequests: [],
    /**
     * local collection to view
     * 
     * @param myDz
     * @param  collection
     * @returns {undefined}
     */
    loadItems: function (myDz, collection)
    {
        collection.each(function (model)
        {
            var mockFile = model.attributes;
            mockFile.name = mockFile.filename;
            myDz.emit("addedfile", mockFile);
            myDz.emit("thumbnail", mockFile, mockFile.preview);
            myDz.emit("complete", mockFile);
        });
        var existingFileCount = collection.length;
        myDz.options.maxFiles = myDz.options.maxFiles - existingFileCount;
        this.moveDropZone(myDz);

   },

    /**
     * set cover class to first element in preview area
     * 
     * @param {string} el
     * @returns {undefined}
     */
    updateCover: function (el)
    {
        if (el == '#ts-uploader1-preview' || el=='#ts-uploader3-preview') { // turn off cover update for 3d
            return;
        }
        var elFirst = $(el).children('.uploader-item')[0];
        $('.upload-cover').removeClass('upload-cover');
        if (elFirst && !$(elFirst).hasClass('additem')) {
            $(elFirst).addClass('upload-cover');
        }
    },

    /**
     * update top header 3D Model or 3D Kit
     * also updates buttom form title
     *
     * @param {String} id
     * @returns {undefined}
     */
    updateHeader: function (id)
    {
        if(id==='#ts-uploader2'){
            return;
        }
        var d = $(id + '-preview .dz-preview'); 
        var firstEl;
        if (d.length > 1) {
            $('.ts-uploader-content h3.header1').html('3D Kit');
            firstEl = d[0];
        } else {
            $('.ts-uploader-content h3.header1').html('3D Model');
            firstEl = d;
        }        
        TS.Uploader.updateTitle($(firstEl).find('input.item-name').val());
    },

    /**
     * set sortable elements by element selector
     * 
     * @param {String} el
     * @returns {undefined}
     */
    initSortable: function (el)
    {
        $(el).sortable({
            handle: 'img'
        });

        $(el).sortable().bind('sortupdate', function (e, ui)
        {
            var filename = $(ui.item).find('.item img').attr('alt');
            var d = $(ui.item).find('.item-name');
            // uploadform-title
            TS.Uploader.updateTitle(d.val());
            $.post("/upload/change-position", {
                filename: filename, newPosition: ui.index, oldPosition: ui.oldindex
            });
            TS.Uploader.updateCover(el);
        });
    },
    
    /**
     * update title based on element
     * @returns {undefined}
     */
    updateTitle: function (d)
    {
        var titleEl = $('#uploadform-title');
        if (d && !titleEl.hasClass('changed')) {
            var dVal = d.replace(/\\/g, '/');
            dVal = dVal.substring(dVal.lastIndexOf('/') + 1, dVal.lastIndexOf('.'));
            dVal = dVal.replace(/([a-z](?=[A-Z]))/g, '$1 ');
            dVal = dVal.replace(/\_/g, ' ');
            dVal = dVal.replace(/\-/g, ' ');
            dVal = dVal.replace(/\[\]\(\)/g, ' ');
            $('#uploadform-title').val(dVal);
        }
    },

    /**
     * get template HTML by id
     * 
     * @param tplId
     * @returns string
     */
    getTemplate: function (tplId)
    {
        var p = document.querySelector(tplId);
        p.id = '';
        $(p).removeAttr('id');
        var tpl = p.parentNode.innerHTML;
        p.parentNode.removeChild(p);
        return tpl;
    },

    /**
     * rename file name
     * 
     * @param file
     * @param newValue
     * @returns {undefined}
     */
    changeFilename: function (file, newValue)
    {
        $.post("/upload/rename-file", {filename: file.filename, newName: newValue}, function( data ) {
        });
    },

    /**
     * cancel upload session
     */
    cancelUpload: function ()
    {
        $.post("/upload/cancel",  function()
        {
            document.location.href = '/upload';
        });
    },

    /**
     * preview 3D model
     *
     * @param  file
     * @returns {Boolean}
     */
    preview3d: function (file)
    {
        $('#model3d-screens').html('');
        window.thingiurlbase = "/js/viewer";
        var thingiview = new Thingiview("viewer");
        thingiview.setRotation = function (flag) {
        };
        thingiview.setObjectColor('#A0D8E0');
        thingiview.setBackgroundColor('#fefefe');
        thingiview.setObjectMaterial('solid'); // wireframe | solid
        thingiview.initScene();
        // detect url type and load string, url or json
        var file3dUrl = file.file3d ? file.file3d : file.url;
        thingiview.loadSTL(file3dUrl);
        // open preview
        var target = 'preview3ddiv';
        $('#' + target).modal('show');
        window.thingiview = thingiview;
        return false;
    },

    /**
     * get dropzone to work with.
     * events here - addedfile, success json request
     *
     * @param {string} tplId - id to identify all elements for this dropzone
     * @param filesExt - file extension to work with
     * @param toLoad - items to load to this dropzone
     * @param config
     *
     * @returns {Dropzone}
     */
    getDropZone: function (tplId, filesExt, toLoad, config)
    {
        var maxFiles = config.maxFiles || 10;
        var me = this;
        var modelDz = new Dropzone(tplId, $.extend({}, {
            paramName: "UploadForm[file]",
            url: me.urls.uploadUrl,
            previewsContainer: tplId + "-preview",
            clickable: tplId + " .fileinput-button",
            addRemoveLinks: false,
            acceptedFiles: filesExt,
            uploadMultiple: false,
            thumbnailWidth: 160,
            thumbnailHeight: 120,
            parallelUploads: 5,
            dictFileTooBig: _t('site.ps', 'File is too big ({{filesize}}MB). Max file size: {{maxFilesize}}MB.'),
            dictMaxFilesExceeded: _t('site.ps', 'You cannot upload more than ') + maxFiles + _t('site.ps', ' files for one model or KIT'),
            maxFiles: 20,
            dictRemoveFileConfirmation: TS.Uploader.msg.confirmRemove,
            previewTemplate: TS.Uploader.getTemplate(tplId + '-template')
        },  config));
        modelDz.on("addedfile", function (file)
        {
            var itemEl = file.previewElement;
            var itemName = $(itemEl).find('.item-name');
            itemName.val(file.displayFilename || file.name);
            itemName.change(function (e) {
                var newValue = e.currentTarget.value;
                TS.Uploader.changeFilename(file, newValue);
            });
            var elPreview = $(itemEl).find('.preview3d');
            elPreview.click(function () {
                TS.Uploader.preview3d(file);
            });
            me.moveDropZone(modelDz);
            me.updateCover(tplId + '-preview');
            //$('#' + modelDz.element.id + '-preview').sortable();
            TS.Uploader.updateHeader(tplId);
        });
        modelDz.on('error', function (file, responseJson)
        {
            if (file.status === 'error') {
                $(file.previewElement).find('.preview3d').fadeOut();
                modelDz.removeFile(file);
                new TS.Notify({
                    type: 'error',
                    text: _.isString(responseJson) ? responseJson : (responseJson.message || _t('site.ps', 'File type is not supported')),
                    target: '.messageBox',
                    automaticClose: true
                });
            }
            if (!file.accepted) {
                //$(file.previewElement.querySelector('.preview3d')).fadeOut();
                //modelDz.removeFile(file);
            }
        });
        modelDz.on('success', function (file, responseJson)
        {
            if (responseJson.success) {
                file.filename = responseJson.filename;
                file.url = responseJson.fileUrl;
                if (responseJson.fileType === 'model3d') {
                    
                    me.addToRender(this, file, responseJson);
                } else {
                    this.emit("thumbnail", file, responseJson.fileUrl);
                }
                $(file.previewElement).find('.preview img').attr('alt', responseJson.filename);
                new TS.Notify({
                    type: 'success',
                    text: TS.Uploader.msg.added || 'Done',
                    target: '.messageBox',
                    automaticClose: true
                });

                // Update sortable
                $('[data-sortable-id]').sortable();
            }
        });

        modelDz.on('complete', function (file) {
            var itemEl = file.previewElement;
            $(itemEl).find('.progress').remove();
            //itemEl.querySelector('.progress').remove();
        });
        modelDz.on("removedfile", function (file) {
            if (file && file.filename) {
                $.post("/upload/delete", {filename: file.filename});
                new TS.Notify({
                    type: 'success',
                    text: TS.Uploader.msg.removed || 'Removed',
                    target: '.messageBox',
                    automaticClose: true
                });
            }
            TS.Uploader.updateHeader(tplId);
            me.updateCover(tplId + '-preview');
        });
        if (toLoad && toLoad.length) {
            TS.Uploader.loadItems(modelDz, toLoad);
        }
        return modelDz;
    },
    /**
     * add job to render
     * 
     * @param dz DropZone
     * @param file
     * @param responseJson
     */
    addToRender : function(dz, file, responseJson)    
    {
        var self = this;
        dz.emit("thumbnail", file, '/static/images/3drender.png');
        var imgUrl = responseJson['preview']+'?'+Math.random();

        $('<img src="'+ imgUrl +'">').load(function() {
            dz.emit("thumbnail", file, responseJson['preview']);
            return true;
        }).bind('error', function() {
            setTimeout(function () {
                self.addToRender(dz, file, responseJson);
            }, 2500);
        });
    },

    /**
     * move dropzone element to the end of preview container
     * 
     * @param  myDz
     * @returns {undefined}
     */
    moveDropZone: function (myDz)
    {
        $('#' + myDz.element.id + "-preview .additem")
                .detach().appendTo('#' + myDz.element.id + "-preview");
    }
};