var CategoriesView = Backbone.View.extend({

    /**
     * Urls
     */
    urls : {
        unbindItems : '/profile/collection/unbind-items'
    },

    /**
     * Messages
     */
    messages : {
        mustSelectItems : _t('site.collections', 'You must select items'),
        acceptUnbind : _t('site.collections', 'Are you sure you want to delete this item?'),
        unbindError : _t('site.collections', 'An error has occurred while deleting the items')
    },

    /**
     * Events
     */
    events:
    {
        'click .js-unbind-checked-items' : 'unbindCheckedItems'
    },

    /**
     * Init
     */
    initialize: function()
    {
        this.$('.catalog-item__controls').click(function()
        {
            $(this).toggleClass("js-checked");
        });
    },

    /**
     * Return checked ids
     * @returns {number[]}
     */
    getCheckedIds : function ()
    {
        var checkboxes = this.$('.js-item-checkbox:checked'),
            result = [];

        checkboxes.each(function(i, el)
        {
            result.push(parseInt($(el).closest('[data-collection-item-id]').data().collectionItemId));
        });

        return result;
    },


    /**
     * Delete checked items
     */
    unbindCheckedItems : function()
    {
        var self = this,
            itemsIds = this.getCheckedIds();

        if(itemsIds.length == 0)
        {
            alert(this.messages.mustSelectItems);
            return false;
        }

        if(!confirm(this.messages.acceptUnbind))
        {
            return false;
        }

        $.post(this.urls.unbindItems, {ids : itemsIds})
            .done(function()
            {
                self.reload();
            })
            .fail(function()
            {
                new TS.Notify({
                    type : 'error',
                    text: self.messages.unbindError,
                    automaticClose: true
                });
            });
    },


    /**
     * Reload page
     */
    reload : function()
    {
        location.reload();
    }
});


var list = new CategoriesView({el : '.js-collection-view'});

