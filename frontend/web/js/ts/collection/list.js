var CategoriesList = Backbone.View.extend({
    /**
     * Urls
     */
    urls : {
        deleteCollections : '/profile/collection/delete-collections',
        deleteModels : '/profile/store/delete-models'
    },

    /**
     * Messages
     */
    messages : {
        mustSelectItems : _t('site.catalog', 'You must select collections'),
        acceptDelete : _t('site.catalog', 'Are you sure you want to delete these collections?'),
        deleteError : _t('site.catalog', 'Error occurred while deleting collections')
    },

    /**
     * Events
     */
    events:
    {
        'click .js-collections-delete' : 'deleteCollections',
        'click .js-model-delete' : 'deleteModel'
    },

    /**
     * Init
     */
    initialize: function()
    {
        this.$('.js-catalog-item-slider')
            .fotorama({
                loop : true,
               // autoplay : true,
                arrows : true,
                nav : false,
                shadows : false,
                width : "100%",
                click : false,
                swipe : true
            })
            // hide arrows
            .on('fotorama:ready', function ()
            {
                $('.fotorama__wrap').addClass('fotorama__wrap--no-controls');
            });
        var self = this;
        this.$('.catalog-item__controls').click(function()
        {
            $(this).toggleClass("js-checked");
            if(self.getCheckedIds().length > 0){
                $('.js-collections-delete').show();
            }else{
                $('.js-collections-delete').hide();
            }
        });

        $('.js-collections-delete').hide();

    },


    /**
     * Return checked ids
     * @returns {number[]}
     */
    getCheckedIds : function ()
    {
        var checkboxes = this.$('.js-item-checkbox:checked'),
            result = [];

        checkboxes.each(function(i, el)
        {
            result.push(parseInt($(el).closest('[data-id]').data().id));
        });

        return result;
    },

    deleteModel : function() 
    {
		var self = this, itemsIds = this.getCheckedIds();

		if (itemsIds.length == 0) {
			alert(this.messages.mustSelectItems);
			return false;
		}

		if (!confirm(this.messages.acceptUnbind)) {
			return false;
		}

		$.post(this.urls.deleteModels, {
			ids : itemsIds
		}).success(function() {
			self.reload();
		}).error(function() {
			new TS.Notify({
				type : 'error',
				text : self.messages.deleteError,
				automaticClose : true
			});
		});
	},
    
    /**
	 * Delete collections
	 */
    deleteCollections : function()
    {
        var self = this,
            itemsIds = this.getCheckedIds();

        if(itemsIds.length == 0)
        {
            alert(this.messages.mustSelectItems);
            return false;
        }

        // user can delete collection only if it empty
        // so, need check it

        var noEmptyCollectionsTitles = [];

        _.each(itemsIds, function(id)
        {
            var collectionData = self.getCollectionData(id);
            if(collectionData.size > 0)
            {
                noEmptyCollectionsTitles.push(collectionData.title);
            }
        });

        if(!_.isEmpty(noEmptyCollectionsTitles))
        {
            alert(_t('site.catalog', 'Collection can only be removed if it is empty. The following collection is not empty: ')+ noEmptyCollectionsTitles.join(', '));
            return false;
        }

        if(!confirm(this.messages.acceptDelete))
        {
            return false;
        }

        $.post(this.urls.deleteCollections, {ids : itemsIds}).done(function()
            {
                self.reload();
            })
            .fail(function()
            {
                new TS.Notify({
                    type : 'error',
                    text: self.messages.deleteError,
                    automaticClose: true
                });
            });
    },

    /**
     * Reload page
     */
    reload : function()
    {
        location.reload();
    },

    /**
     * Resovle collection data by id
     * @param collectionId
     */
    getCollectionData : function(collectionId)
    {
        return this.$('.js-collection[data-id='+collectionId+']').data();
    }
});

var list = new CategoriesList({el : '.js-collection-list'});
