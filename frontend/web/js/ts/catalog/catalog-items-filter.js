/**
 * View of list store units
 */
var StoreItemFilter = Backbone.View.extend({

    SORT_ASC : 'asc',
    SORT_DESC : 'desc',

    /**
     * Events
     */
    events:
    {
        'slideStop .slider' : 'onPriceRangeChange',
        'click [data-tag]' : 'onPopularTagClick',
        'change #tags' : 'onTagsChanged',
        'change #js-store-item-filter-category-select' : 'onCategoryChange',
        'change #sort' : 'onSortChange',
        'click .js-btn-sort' : 'onSortDirectionClick'
    },

    /**
     * Init
     */
    initialize: function(options)
    {
        _.extend(this, options);
        var self = this;

        this.$sortInput = this.$('#sort');
        this.$sortDirectionInput = this.$('#sortdirection');
        this.$sortDirectionButton = this.$('.js-btn-sort');
        this.$tags = this.$('#tags');
        this.$popularTags = this.$('[data-tag]');

        this.$popularTags.each(function(i,el)
        {
            var tagValue = $(el).attr("data-tag");
            self.$tags.append($('<option/>').attr('value', tagValue).text(tagValue));
        });
        this.bind('formChange', _.bind(this.onFormChange, this));
        this.renderSort();
    },

    /**
     * On price change range
     * @param event
     */
    onPriceRangeChange : function(event)
    {
        var min = event.value[0];
        var max = event.value[1];
        this.$el.find('#pricemin').val(min);
        this.$el.find('#priceMax').val(max);
        this.trigger('formChange');
    },

    /**
     * Add tag
     * @param event
     */
    onPopularTagClick : function(event)
    {
        var tagElement = $(event.currentTarget),
            tag = tagElement.data('tag')+'',
            tags = this.getTags();

        if(_.contains(tags, tag))
        {
            tags = _.without(tags, tag);
        }
        else
        {
            tags.push(tag);
        }

        this.$tags.data('select2').val(tags);
    },

    /**
     * On tags changed
     */
    onTagsChanged : function()
    {
        var tags = this.getTags();

        this.$popularTags.each(function(i,el)
        {
            var $el = $(el),
                tag = $el.attr('data-tag')+'';

            if(_.contains(tags, tag))
            {
                $el.addClass('js-store-item-tag-active');
            }
            else
            {
                $el.removeClass('js-store-item-tag-active');
            }
        });
        this.trigger('formChange');
    },

    /**
     * On change category
     */
    onCategoryChange : function(event, data)
    {
        this.$('#category').val(data.activeCategoryId);
        this.$('.js-category-header').text(data.title);
        this.trigger('formChange');
    },

    /**
     * On sort change
     */
    onSortChange : function()
    {
        /** @namespace this.isHaveDirection */
        /** @namespace this.defaultSortDirection */

        var sortValue = this.getSortValue();

        this.isHaveDirection[sortValue]
            ? this.setSortDirectionValue(this.defaultSortDirection[sortValue])
            : this.setSortDirectionValue(null);

        this.renderSort();
        this.onFormChange();
    },

    /**
     * On sort direction click
     */
    onSortDirectionClick : function()
    {
        var currentSortDirection = this.getSortDirectionValue();
        this.setSortDirectionValue(currentSortDirection == this.SORT_ASC ? this.SORT_DESC : this.SORT_ASC);
        this.renderSort();
        this.onFormChange();
    },

    /**
     * Return tags
     * @returns {*|Array}
     */
    getTags : function()
    {
        return this.$tags.val() || [];
    },

    /**
     * Return sort value
     * @returns {string}
     */
    getSortValue : function()
    {
        return this.$sortInput.val();
    },


    /**
     * Return sort direction value
     * @returns {string}
     */
    getSortDirectionValue : function()
    {
        return this.$sortDirectionInput.val();
    },

    /**
     * Set sort direction value
     * @returns {string}
     */
    setSortDirectionValue : function(value)
    {
        return this.$sortDirectionInput.val(value);
    },

    /**
     * On form value updated
     */
    onFormChange : function()
    {
        this.$el.submit();
    },

    /**
     * Render sort block
     */
    renderSort : function ()
    {
        var sortValue = this.getSortValue(),
            sortDirectionValue = this.getSortDirectionValue();

        if(this.isHaveDirection[sortValue])
        {
            this.$sortDirectionButton
                .find('span')
                    .removeClass('tsi tsi-arrow-down-l tsi-arrow-up-l')
                    .addClass('tsi ' + (sortDirectionValue == this.SORT_ASC ? 'tsi-arrow-up-l' : 'tsi-arrow-down-l'))
                .end()
                .show();
        }
        else
        {
            this.$sortDirectionButton.hide();
        }
    }
});