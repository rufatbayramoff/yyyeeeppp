/**
 * View of list store units
 */
var StoreItemList = Backbone.View.extend({

    /**
     * Events
     */
    events: {
        'pjax:complete': 'onPageRefresh'
    },

    /**
     * Init
     */
    initialize: function () {
        storeUnitJsPricesObj.calculatePrices();
    },

    /**
     * On page refresh, for example, when filter was changed
     */
    onPageRefresh: function () {
        TS.Directives.init();
        this.delegateEvents();
        storeUnitJsPricesObj.calculatePrices();
    },

});

$(function () {
    new StoreItemList({el: '#store-item-pjax'});
});
