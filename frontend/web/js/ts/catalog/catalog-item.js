/**
 * View of list store units
 */
var CatalogItem = Backbone.View.extend({

    /** @namespace this.model.model3dId */
    /** @namespace this.model.isLiked */

    directiveSelector : '.catalog-item',

    /**
     * Now run like request
     * @todo we must use button for like control and use disbaled attribute
     */
    inLikeRequest : false,

    /**
     * Messages
     */
    messages : {
        like : 'Like',
        unlike : 'Unlike',
        errorOnLike : 'Error on like'
    },

    /**
     * Urls
     */
    urls : {
        like : '/profile/collection/like',
        unlike : '/profile/collection/unlike'
    },

    /**
     * Events
     */
    events:
    {
        'click .js-store-item-like': 'onLikeClick',
        'click .js-store-item-likes-first': 'onLikeClick'
    },

    $hideLikesElement : undefined,

    /**
     * Init
     */
    initialize: function()
    {
        this.$likeButton = this.$('.js-store-item-like');
        this.$likeLabel = this.$('.js-store-item-like-label');
        this.$likesCount = this.$('.js-store-item-likes-count');
        this.$hideLikesElement = this.$('.js-hide-like-if-not-liked');

        if(this.$hideLikesElement && this.$hideLikesElement.data('likes') == 0){
            this.$hideLikesElement.hide();
        }


    },

    /**
     * Like button click
     */
    onLikeClick : function()
    {

        if(TS.User.isGuest)
        {
           
            // user-loginAjax-login
            // user-loginAjax-signup
            // set try to like after login
            var mid = this.model.get('model3dId');
            //$('#user-loginAjax-login form').append("<input type='hidden' name='model3d_like' value='" + mid + "' />");
            //$('#user-loginAjax-signup form').append("<input type='hidden' name='model3d_like' value='" + mid + "' />");            
            TS.setCookie("afterlogin_model3d_to_like", mid);
            TS.Visitor.loginForm();
            return;
        }

        if(this.inLikeRequest)
        {
            return;
        }
        this.inLikeRequest = true;

        this.postLike();
    },
    
    postLike: function()
    {
        var self = this;
        $.post(this.model.get('isLiked') ? this.urls.unlike : this.urls.like, {model3dId : this.model.get('model3dId')})
        .done(function(response)
        {
            /** @namespace data.liked */
            /** @namespace data.likeMessage */
            /** @namespace data.message */

            self.model.set('isLiked', response.liked);
            self.$likeLabel.text(response.liked ? self.messages.unlike : self.messages.like);
            self.$likesCount.text(response.likeMessage);

            response.liked
                ? self.$likeButton.addClass('js-store-item-like-showed')
                : self.$likeButton.removeClass('js-store-item-like-showed');

            if(response.likesCount == 0)
            {
                self.$likesCount.addClass('js-store-item-likes-first');
                if (self.$hideLikesElement){
                    self.$hideLikesElement.hide();
                }
            }
            else
            {
                self.$likesCount.removeClass('js-store-item-likes-first');
                if (self.$hideLikesElement){
                    self.$hideLikesElement.show();
                }
            }

            self.showSuccess(response.liked ? response.message : response.message);
            self.inLikeRequest = false;
        })
        .fail(function()
        {
            self.showError(self.messages.errorOnLike);
            self.inLikeRequest = false;
        });
    },

    /**
     * Show success message
     * @param text
     */
    showSuccess : function(text)
    {
        new TS.Notify({ type : 'success', text: text,  automaticClose: true});
    },

    /**
     * Show error message
     * @param text
     */
    showError : function(text)
    {
        new TS.Notify({ type : 'error', text: text,  automaticClose: true});
    }

});

TS.Directives.registerDirective('CatalogItem', CatalogItem);