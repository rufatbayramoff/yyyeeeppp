TS = TS || {};

/**
 * Notify messages
 *
 * usage example :
 *  new TS.Notify({ type : 'success', text: 'Item deleted',  automaticClose: true});
 *
 */
TS.NotifyHelper = {
    hideAll: function () {
        $('#notify-message').html('');
    }
}
TS.Notify = Backbone.View.extend({
    targetElement: '#notify-message',
    tagName: 'div',
    className: 'alert',
    defaultMessages: {
        'success': _t('site.common', 'Success!'),
        'error': _t('site.common', 'Sorry! An error occurred in the process'),
        'warning': _t('site.common', 'Are you sure you want to take this action?'),
        'information': _t('site.common', 'An unknown event occurred')
    },
    cssClasses: {
        'success': 'alert-success',
        'error': 'alert-danger',
        'warning': 'alert-warning',
        'information': 'alert-info'
    },
    events: {
        "click": "closeNotification"
    },
    automaticClose: true,
    automaticCloseTimer: 4000,
    initialize: function (options) {
        // defaults
        var type = 'information';
        var closeBtn = '<button type="button" class="close"><span aria-hidden="true">&times;</span></button>';
        var text = this.defaultMessages[type];
        var target = this.targetElement;
        if (options && options.hasOwnProperty('type'))
            type = options.type;
        if (options && options.hasOwnProperty('text'))
            text = options.text;
        if (options && options.hasOwnProperty('target'))
            target = options.target;

        if (options && options.hasOwnProperty('automaticClose')) {
            this.automaticClose = options.automaticClose;
            if (this.automaticClose === false) {

                text = closeBtn + text;
            } else {
                if (options && options.hasOwnProperty('automaticCloseTimer')) {
                    this.automaticCloseTimer = options.automaticCloseTimer;
                }
            }
        }
        if ($('.notification:contains(' + text + ')').length === 0) {
            this.render(type, text, target);
        }

    },

    render: function (type, text, target) {
        var self = this;
        this.$el.addClass(this.cssClasses[type]);
        this.$el.html(text);
        this.$el.prependTo(this.targetElement);
        if (this.automaticClose) {
            setTimeout(function () {
                self.closeNotification();
            }, this.automaticCloseTimer);
        }
    },

    closeNotification: function () {
        var self = this;
        $(this.el).fadeOut(function () {
            self.unbind();
            self.remove();
        });
    }
});
