var imageCropClass = {
    config: {
        imgId: ''
    },

    rotateAngle: 0,
    imgJquery: null,

    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
        self.imgJquery = $('#' + self.config.imgId);
        self.initCropper();
    },

    initCropper: function () {
        var self = this;
        setTimeout(function () {
            self.imgJquery.cropper({
                aspectRatio: 4 / 3,
                viewMode: '1',
                crop: self.crop
            });
        }, 0);

    },

    crop: function (e) {
        var x = e.x || e.detail.x;
        var width = e.width || e.detail.width;
        var y = e.y || e.detail.y;
        var height = e.height || e.detail.height;
        var rotate = e.rotate || e.detail.rotate;
        var scaleX = e.scaleX || e.detail.scaleX;
        var scaleY = e.scaleX || e.detail.scaleY;
        // Output the result data for cropping image.
        $('#x1').val(Math.round(x));
        $('#x2').val(Math.round(width - x));

        $('#y1').val(Math.round(y));
        $('#y2').val(Math.round(height - y));

        $('#h').val(Math.round(height));
        $('#w').val(Math.round(width));

        $('#rotate').val(Math.round(rotate));

        $('#scaleX').val(Math.round(scaleX));
        $('#scaleY').val(Math.round(scaleY));
    },


    zoomAndCenterCrop: function () {
        var self = this;
        var windowSizeX = $('.cropper-wrap-box').outerWidth();
        var windowSizeY = $('.cropper-wrap-box').outerHeight();
        var imageSizeX = Math.round(document.getElementById(self.config.imgId).width);
        var imageSizeY = Math.round(document.getElementById(self.config.imgId).height);
        var cropData = self.imgJquery.cropper('getData');
        if (self.rotateAngle === 90 || self.rotateAngle === 270 || self.rotateAngle === -90 || self.rotateAngle === -270) {
            // horizontal change
            var t = imageSizeX;
            imageSizeX = imageSizeY;
            imageSizeY = t;
        }

        var scaleX = windowSizeX / imageSizeX;
        var scaleY = windowSizeY / imageSizeY;
        var scale = Math.min(scaleX, scaleY);
        var newWidth = imageSizeX*scale;
        var newHeigh = imageSizeY*scale;
        if (cropData.width>newWidth)  {
            cropData.width =  newWidth;
            self.imgJquery.cropper('setData', cropData);
        }
        if (cropData.height>newHeigh)  {
            cropData.height =  newHeigh;
            self.imgJquery.cropper('setData', cropData);
        }
        self.imgJquery.cropper('zoomTo', scale);
    },

    rotateLeft: function () {
        var self = this;
        self.rotateAngle -= 90;
        if (self.rotateAngle === -360 ) self.rotateAngle= 0;
        self.imgJquery.cropper('reset');
        self.imgJquery.cropper('rotate', self.rotateAngle);
        self.zoomAndCenterCrop();
    },

    rotateRight: function () {
        var self = this;
        self.rotateAngle += 90;
        if (self.rotateAngle === 360 ) self.rotateAngle= 0;
        self.imgJquery.cropper('reset');
        self.imgJquery.cropper('rotate', self.rotateAngle);
        self.zoomAndCenterCrop();
    },
    flipHorizontal: function () {
        var self = this;
        self.imgJquery.cropper('scale', -1, 1);
    },
    flipVertical: function () {
        var self = this;
        self.imgJquery.cropper('scale', 1, -1);
    },
    resetImage: function () {
        var self = this;
        self.imgJquery.cropper('reset');
    }


};
