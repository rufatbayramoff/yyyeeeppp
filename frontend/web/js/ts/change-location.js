
TS.Locator = {

    componentForm : {
        street_number: 'short_name', // building number
        route: 'long_name', // street name
        locality: 'short_name', // city name
        administrative_area_level_1: 'long_name', // state
        country: 'short_name', // country
        postal_code: 'short_name' // postal code
    },
    filledForm : {},
    autocomplete : null,

    /**
     *
     */
    initAutocomplete : function()
    {
        TS.Locator.autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('userlocator-address')), {types: ['(cities)'], shouldFocus: true}
        );
        $(document).on("keypress", "#user-change-location-form", function(event) {
            return event.keyCode != 13;
        });
        TS.Locator.autocomplete.addListener('place_changed', TS.Locator.fillInAddress);
        document.getElementById('userlocator-address').focus();
    },

    /**
     *
     */
    fillInAddress : function()
    {
        var place = TS.Locator.autocomplete.getPlace();
        if(!place.address_components) {
            new TS.Notify({ type : "warning", text: _t('user.location', 'Please enter the name of your city')});
            return;
        }
        var componentForm = TS.Locator.componentForm;
        for (var i = 0; i < place.address_components.length; i++) {
            var addressCmp = place.address_components[i];
            var addressType = addressCmp.types[0];
            var addressType2 = false;
            if(addressCmp.types[1]){
                addressType2 = addressCmp.types[1];
            }
            if (componentForm[addressType]) {
                TS.Locator.filledForm[addressType] = addressCmp[componentForm[addressType]] || addressCmp['short_name'];
            }else if(addressType2){
                TS.Locator.filledForm[addressType2] = addressCmp[componentForm[addressType2]];
            }
            if(addressType=='administrative_area_level_2'){
                if(!TS.Locator.filledForm['administrative_area_level_1'])
                    TS.Locator.filledForm['administrative_area_level_1'] = addressCmp['long_name'];
            }
            // check second address type
        }
        TS.Locator.filledForm['lat'] = place.geometry.location.lat();
        TS.Locator.filledForm['formatted_address'] = place.formatted_address;
        TS.Locator.filledForm['lat'] = place.geometry.location.lat();
        TS.Locator.filledForm['lon'] = place.geometry.location.lng();
        TS.Locator.submitAddress();
        TS.UpdateLocation();
    },

    /**
     *
     */
    submitAddress: function()
    {
        TS.checkGlobalSession(function() {
            document.getElementById('userlocator-addressapi').value = JSON.stringify(TS.Locator.filledForm);
            $('#user-change-location-form .ts-ajax-submit').trigger('click');
        });
    },

    /**
     *
     */
    init : function ()
    {
        if (window.google){
            setTimeout(TS.Locator.initAutocomplete, 500);
        }
        else {
            setTimeout(TS.Locator.init, 500);
        }
    },

    getLocationString: function(location)
    {
        var country = location.country.replace(new RegExp(' ', 'g'), '-');
        var region = location.region ? location.region.replace(new RegExp(' ', 'g'), '-') : '';
        var city = location.city.replace(new RegExp(' ', 'g'), '-');
        var location = [city,region,country].join('--');
        return location;
    },

    getUrlPsCatalogLocationRedir : function(location)
    {
        var locationString = this.getLocationString(location);
        var href = '/3d-printing-services?location='+locationString;
        return this.addSearchText(href);
    },
    /**
     * converts /catalog/<location>/<filter> to /catalog/<newLocation>/<filter>
     * <location> converted to format <city>--<region>--<country>
     * example: New York, NY, US  -> New-York--NY--US
     *
     * @param currentHref
     * @param location
     */
    getUrlWithLocation : function(currentHref, location)
    {
        var newHref = currentHref.split('/');
        var locationString = this.getLocationString(location);
        newHref[4] = locationString;
        return this.addSearchText(newHref.join('/'));
    },

    /**
     * Add search text to url, if it inputed to form
     * @param search url for ps catalog page
     * @return {string} url with search text param
     */
    addSearchText : function (search) {
        var searchValue = $('#searchPsText').val();
        if (searchValue) {
            search+= "&text=" + encodeURIComponent(searchValue.replace(' ', '+'));
        }
        return search;
    }
};

/**
 * @return {boolean}
 */
TS.UpdateLocation = TS.UpdateLocation || function(btn, result, modalId)
{
    $(modalId).modal('hide');
    if(result && result.success){
        var url = document.location.href.substring(0, document.location.href.lastIndexOf("?") > 0? document.location.href.lastIndexOf("?"): document.location.href.length);
        if (typeof(ga) != 'undefined' && ga) { // #4473
            ga('send', 'pageview', url + '/' + result.message);
        }
        new TS.Notify({ type : 'success', text: 'Please wait...',  automaticClose: true})
        setTimeout(function(){
            document.location.replace(document.location.href)
        }, 500);
    }
    return true;
};
TS.RedirLocation = TS.RedirLocation || TS.UpdateLocation;