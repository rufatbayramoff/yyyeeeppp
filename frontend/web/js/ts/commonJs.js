/**
 * Common for front and backend part
 *
 * @type {{setObjectConfig: common.setObjectConfig}}
 */
var commonJs = {

    clone: function (obj) {
        var copy;

        if (null == obj) return obj;

        // Handle Date
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }

        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = commonJs.clone(obj[i]);
            }
            return copy;
        }

        // Handle function
        if (obj instanceof Function) {
            var copy = function() { return obj.apply(this, arguments); };
            return copy;
        }

        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = commonJs.clone(obj[attr]);
            }
            return copy;
        }
        return obj;
    },

    /**
     *  Set config to some object
     *
     * @param object
     * @param config
     */
    setObjectConfig: function (object, config) {
        for (var key in config) {
            object.config[key] = config[key];
        }
    },

    /**
     * Prepare message to string
     * @param {String|String[]}message
     * @returns {String}
     */
    prepareMessage: function (message) {
        if (_.isString(message)) {
            return message;
        }

        if (_.isObject(message) && message.message) {
            return message.message;
        }

        if (_.isArray(message) || _.isObject(message)) {
            return '<p>' + _t('site.common', 'Please fix the following errors:') + '</p><ul class="p-l20"><li>' + _.uniq(_.values(message)).join('</li><li>') + '</ul>'
        }

        throw "Bad type for message";
    },

    jsonSyntaxHighlight: function (json) {
        if (typeof json != 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    },

    checkAltSrcImage: function (img) {
        if ($(img).attr('alt-src')) {
            img.src = $(img).attr('alt-src');

            var checkImageTimer = function () {
                var testImage = new Image();
                testImage.src = $(img).attr('main-src');
                testImage.onload = function () {
                    img.src = $(img).attr('main-src');
                };
                testImage.onerror = function () {
                    if ($(img).is(":visible")) {
                        setTimeout(checkImageTimer, 1000);
                    }
                }
            };
            setTimeout(checkImageTimer, 1000);
        }
    }
};

if (typeof (window.test) === 'undefined') {
    window.test = {};
}

$(function () {
    $('.jsonHighlight').each(function (item, obj) {
        obj = $(obj);
        obj.html(commonJs.jsonSyntaxHighlight(obj.html()));
    });
});


Array.prototype.moveUp = function (value, by) {
    var index = this.indexOf(value),
        newPos = index - (by || 1);

    if (index === -1)
        throw new Error("Element not found in array");

    if (newPos < 0)
        newPos = 0;

    this.splice(index, 1);
    this.splice(newPos, 0, value);
};

Array.prototype.moveDown = function (value, by) {
    var index = this.indexOf(value),
        newPos = index + (by || 1);

    if (index === -1)
        throw new Error("Element not found in array");

    if (newPos >= this.length)
        newPos = this.length;

    this.splice(index, 1);
    this.splice(newPos, 0, value);
};

function toInteger(num) {
    if (typeof num == 'number' || typeof num == 'string') {
        num = num.toString();
        var dotLocation = num.indexOf('.');
        if (dotLocation > 0) {
            num = num.substr(0, dotLocation);
        }
        if (isNaN(Number(num))) {
            num = parseInt(num);
        }
        if (isNaN(num)) {
            return 0;
        }
        return Number(num);
    } else if (typeof num == 'boolean' && num === true) {
        return 1;
    }
    return 0;
}


(function () {
    /**
     * correct rounding
     *
     * @param {String}  type
     * @param {Number}  value
     * @param {Integer} exp
     * @returns {Number}
     */
    function decimalAdjust(type, value, exp) {
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})();