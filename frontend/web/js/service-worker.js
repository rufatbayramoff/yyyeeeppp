'use strict';

/*
self.addEventListener('install', function (event) {
    console.log('Install');
});
*/
/*
self.addEventListener('activate', function (event) {
    console.log('Activate');
    event.waitUntil(clients.claim());
});
self.addEventListener('fetch', (event) => {
    console.log('Fetch server');
});
*/

self.addEventListener('notificationclick', function (event) {
    event.notification.close();
    let url = event.notification.data.url;
    if (url) {
        clients.openWindow(url);
    }
});

self.addEventListener('push', function (event) {
    event.waitUntil(
        self.registration.pushManager.getSubscription()
            .then(function (subscription) {
                var endpoint = encodeURIComponent(subscription.endpoint);
                var url = '/browser-push/browser-push/get-notifications?endPoint=' + endpoint;
                return fetch(url)
                    .then(response => response.json())
                    .then(function (data) {
                        if (data.list) {
                            let notifications = [];
                            for (var key in data.list) {
                                if (!data.list.hasOwnProperty(key)) {
                                    continue;
                                }
                                var notify = data.list[key];
                                notifications.push(self.registration.showNotification(notify.title, {
                                    body: notify.text,
                                    icon: notify.icon,
                                    data: {
                                        url: notify.url
                                    }
                                }));
                            }
                            if (notifications) {
                                return Promise.all(notifications);
                            }
                        }
                    });
            })
    );
});
