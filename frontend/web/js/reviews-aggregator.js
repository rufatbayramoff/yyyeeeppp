{
    "@context": "http://schema.org",
    "@type": "LocalBusiness",
    "name": "Treatstock | Smart Manufacturing Platform",
    "url": "https://www.treatstock.com/",
    "image": "https://static.treatstock.com/static/images/ts-logo.svg",
    "priceRange": "$",
    "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": 4.9 ,
        "ratingCount": 19978
    },
    "review": [
        {
            "@type": "Review",
            "author": {
                "@type": "Person",
                "name": "jocelyn.e.ma"
            },
            "datePublished": "2020-12-15",
            "reviewBody": "At this point, there' not much to say, RDR does great work, and goes above and beyond when it comes to communication and service, just a joy to work with!"
        },

        {
            "@type": "Review",
            "author": {
                "@type": "Person",
                "name": "twittyc"
            },
            "datePublished": "2020-12-10",
            "reviewBody": "Wow, the quality blew me way. Thank you for the quick turnaround!"
        },

        {
            "@type": "Review",
            "author": {
                "@type": "Person",
                "name": "cashenandrew"
            },
            "datePublished": "2020-12-7",
            "reviewBody": "Very fast, great service, will be using again."
        },

        {
            "@type": "Review",
            "author": {
                "@type": "Person",
                "name": "1976aca"
            },
            "datePublished": "2020-12-4",
            "reviewBody": "Nice work as always"
        },

        {
            "@type": "Review",
            "author": {
                "@type": "Person",
                "name": "helgish"
            },
            "datePublished": "2020-11-30",
            "reviewBody": "Fantastic as always! Exceptional service and the casts are perfect."
        },

        {
            "@type": "Review",
            "author": {
                "@type": "Person",
                "name": "scott-jenson"
            },
            "datePublished": "2020-11-27",
            "reviewBody": "Excellent experience, quick to communicate, verified my choices and the part fits perfectly. Strongly recommend."
        },

        {
            "@type": "Review",
            "author": {
                "@type": "Person",
                "name": "schuyler"
            },
            "datePublished": "2020-11-23",
            "reviewBody": "Great communications throughout the production process."
        },

        {
            "@type": "Review",
            "author": {
                "@type": "Person",
                "name": "bryanluce"
            },
            "datePublished": "2020-11-20",
            "reviewBody": "Great communication on a difficult print. I'm very happy with the results and will use VectorPrints again."
        }

    ]
}
