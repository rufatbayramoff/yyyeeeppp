
function initTrust() {
    //Init slider for .trust-logo__slider
    var swiperTrust = new Swiper('.trust-logo__slider', {
        pagination: '.trust-logo__pagination',
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 60,
        grabCursor: true,
    });
}

initTrust();
$(window).resize(function () {initTrust()});

//Init slider for .main-page-promo
var swiperPromo = new Swiper('.main-page-promo__list', {
    pagination: '.main-page-promo__pagination',
    paginationClickable: true,
    slidesPerView: 1,
    spaceBetween: 30,
    grabCursor: true,
    autoplay: 3000,
    speed: 700,
    loop: true,

    breakpoints: {
        600: {
            slidesPerView: 1
        }
    }
});

$('a[data-target]').click(function () {
    var scroll_elTarget = $(this).attr('data-target');
    if ($(scroll_elTarget).length != 0) {
        $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 120}, 350);
    }
    return false;
});

$('.main-page-why__cta.btn-link').click(function (e) {
    e.preventDefault();
    $('html,body').animate({
        scrollTop: $('.main-page-how').offset().top - 100
    }, 350);
});

//Init slider for .products-promo-hero__banner
var swiperMainPageBanner = new Swiper('.products-promo-hero__banner-cont', {
    pagination: '.products-promo-hero__banner-pagination',
    paginationClickable: true,
    slidesPerView: 1,
    spaceBetween: 30,
    grabCursor: true,
    autoplay: 5000,
    speed: 700,
    loop: true
});

//Init slider for PS portfolio
var swiperFeaturedPS = new Swiper('.designer-card__ps-portfolio', {
    scrollbar: '.designer-card__ps-portfolio-scrollbar',
    scrollbarHide: true,
    slidesPerView: 'auto',
    grabCursor: true
});

function initPSSliders() {
    //Init slider for --ps
    var swiperPs = new Swiper('.main-page-models__list--ps', {
        slidesPerView: 'auto',
        scrollbar: '.main-page-models__ps-scrollbar',
        scrollbarHide: true,
        spaceBetween: 0,
        grabCursor: true,

        breakpoints: {
            500: {
                slidesPerView: 1.15,
                centeredSlides: false
            },
            767: {
                slidesPerView: 2.25
            }
        }
    });
}

initPSSliders();
$(window).resize(function () {initPSSliders()});

function initPrints() {
    //Init slider for .main-page-prints__slider
    var swiperPrints = new Swiper('.main-page-prints__slider', {
        scrollbar: '.main-page-prints__scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });
}

initPrints();
$(window).resize(function () {initPrints()});



function initReviews() {
    //Init slider for .main-page-prints__slider
    var swiperPrints = new Swiper('.user-choice-slider', {
        scrollbar: '.user-choice-slider__scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });
}

initReviews();
$(window).resize(function () {initReviews()});

// Turn on sliders on mobile devices
function initMobileSliders() {
    if ( $('html').hasClass('mobile') ) {

        //Init slider for .promo-blocks-6
        var swiperCats = new Swiper('.promo-blocks-6__list', {
            scrollbar: '.promo-blocks-6__scrollbar',
            scrollbarHide: true,
            slidesPerView: 6,
            spaceBetween: 30,
            grabCursor: true,

            breakpoints: {
                600: {
                    slidesPerView: 1.2,
                    spaceBetween: 15
                },
                900: {
                    slidesPerView: 3.3,
                    spaceBetween: 30
                }
            }
        });

        //Init slider for .promo-blocks-featured
        var swiperFeatCats = new Swiper('.promo-blocks-featured__slider', {
            scrollbar: '.promo-blocks-featured__scrollbar',
            scrollbarHide: true,
            slidesPerView: 4,
            spaceBetween: 15,
            grabCursor: true,

            breakpoints: {
                600: {
                    slidesPerView: 1.2,
                    spaceBetween: 15
                },
                900: {
                    slidesPerView: 3.3,
                    spaceBetween: 30
                }
            }
        });

        //Init slider for .main-page-models
        var swiperModels = new Swiper('.main-page-models__list--models', {
            slidesPerView: 3.25,
            spaceBetween: 0,
            grabCursor: true,

            breakpoints: {
                500: {
                    slidesPerView: 1.15,
                    spaceBetween: 0,
                    centeredSlides: false
                },
                700: {
                    slidesPerView: 2.25
                }
            }
        });

        //Init slider for .main-page-blog
        var swiperBlog = new Swiper('.post-list', {
            grabCursor: true,
            slidesPerView: 1.15,
            spaceBetween: 0,
            centeredSlides: false
        });

    }
}

initMobileSliders();
$(window).resize(function () {initMobileSliders()});


// Turn on sliders on tablet devices
function initTabletSliders() {
    if ( $('html').hasClass('tablet') ) {

        //Init slider for .main-page-models
        var swiperModels = new Swiper('.main-page-models__list--models', {
            slidesPerView: 3.25,
            spaceBetween: 0,
            grabCursor: true,

            breakpoints: {
                500: {
                    slidesPerView: 1.15,
                    spaceBetween: 0,
                    centeredSlides: false
                },
                700: {
                    slidesPerView: 2.25
                }
            }
        });
    }
}

initTabletSliders();
$(window).resize(function () {initTabletSliders()});


function mainPageVars() {
    var $varsShop = $(".main-page-vars--shop"),
        $varsHire = $(".main-page-vars--hire"),
        $varsFindps = $(".main-page-vars--findps"),
        //$varsShopBtn = $(".main-page-vars--shop .main-page-vars__btn"),
        //$varsHireBtn = $(".main-page-vars--hire .main-page-vars__btn"),
        //$varsFindpsBtn = $(".main-page-vars--findps .main-page-vars__btn"),
        $varsShopBg = $(".main-page-pic__bg--shop"),
        $varsHireBg = $(".main-page-pic__bg--hire"),
        $varsFindpsBg = $(".main-page-pic__bg--findps");

    $varsShop.mouseover(function() {
        $varsHireBg.removeClass("is-active");
        $varsFindpsBg.removeClass("is-active");
        $varsShopBg.addClass("is-active");
    });

    $varsHire.mouseover(function() {
        $varsShopBg.removeClass("is-active");
        $varsFindpsBg.removeClass("is-active");
        $varsHireBg.addClass("is-active");
    });

    $varsFindps.mouseover(function() {
        $varsShopBg.removeClass("is-active");
        $varsHireBg.removeClass("is-active");
        $varsFindpsBg.addClass("is-active");
    });
}

mainPageVars();


function initPrints() {
    //Init slider for .customer-reviews__slider
    var swiperPrints = new Swiper('.customer-reviews__slider', {
        scrollbar: '.customer-reviews__scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });
}

initPrints();
$(window).resize(function () {initPrints()});