/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.01.17
 * Time: 11:39
 */

var preloadModel3dClass = {
    config: {
        checkUrl: null,
        redirectUrl: null,
        checkTimeOut: 4000
    },

    init: function (config) {
        var self = this;

        commonJs.setObjectConfig(self, config);
        setTimeout(function () {
            self.checkModel3dRender();
        }, self.config.checkTimeOut);
        return self;
    },

    checkModel3dRender: function () {
        var self = this;
        $.ajax({
            type: 'POST',
            data: [],
            cache: false,
            url: self.config.checkUrl,
            success: function (response) {
                if (response.success && response.result) {
                    window.location.href = self.config.redirectUrl;
                } else {
                    setTimeout(function () {
                        self.checkModel3dRender();
                    }, self.config.checkTimeOut);
                }
            },
            error: function() {
                setTimeout(function () {
                    self.checkModel3dRender();
                }, 4000);
            }
        });
    }
};
