/**
 * Created by analitic on 23.06.17.
 */

/**
 * Global site app config
 *
 * @type {{config: {}, init: siteParamsClass.init}}
 */
var siteParamsClass = {
    config: {
    },

    init: function (config) {
        var self = this;
        commonJs.setObjectConfig(self, config);
    }
};