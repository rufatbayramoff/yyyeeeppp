<?php

$salt = 'VKs=d9apwksAKspowdja';

function filterReferrer($referrer)
{
    $len = strlen($referrer);
    if ($len > 300) {
        $referrer = substr($referrer, 0, 300);
    }
    $len = strlen($referrer);
    $filtered = '';
    for ($i = 0; $i < $len; $i++) {
        if (((ord($referrer[$i]) > 96) && (ord($referrer[$i]) < 123)) || ((ord($referrer[$i]) > 47) && (ord($referrer[$i]) < 58)) || ($referrer[$i] == '-')) {
            $filtered .= $referrer[$i];
        }
    }
    return $referrer;
}

function getCurrentScore()
{
    global $salt;
    if (!array_key_exists('dp', $_COOKIE) || !$_COOKIE['dp']) {
        return 0;
    }
    $dpString   = $_COOKIE['dp'];
    $remoteAddr = $_SERVER['REMOTE_ADDR'];

    list($md5, $timestamp, $randInt, $score) = explode('_', $dpString);
    $calcMd5 = $salt . '_' . $remoteAddr . '_' . $timestamp . '_' . $randInt . '_' . $score;
    if ($md5 !== $calcMd5) {
        return 0;
    }
    return (float)$score;
}

function markCookieScore($score)
{
    $currentScore = getCurrentScore();
    if ($score<$currentScore) {
        return ;
    }

    global $salt;
    $remoteAddr = $_SERVER['REMOTE_ADDR'];
    $timestamp  = time();
    $randInt    = random_int(10000, 99999);
    $md5Str     = $salt . '_' . $remoteAddr . '_' . $timestamp . '_' . $randInt . '_' . $score;
    $md5        = md5($md5Str);
    $expireDate = time() + 60 * 60 * 24 * 130;

    function getCookieDomain()
    {
        $mainDomain = $_SERVER['HTTP_HOST'];
        $tod1       = strrpos($mainDomain, '.');
        if ($tod1) {
            $shiftedDomain = substr($mainDomain, 0, $tod1);
            $tod2          = strrpos($shiftedDomain, '.');
            if ($tod2) {
                $mainDomain = substr($mainDomain, $tod2, 1024);
                if ($mainDomain === '.co.uk') { // Fix for main site for co.uk
                    $mainDomain = '.treatstock.co.uk';
                }
            }
        }
        return $mainDomain;
    }

    $cookieDomain = getCookieDomain();

    setcookie(
        'dp',
        $md5 . '_' . $timestamp . '_' . $randInt . '_' . $score,
        $expireDate,
        '/',
        $cookieDomain,
        false,
        true
    );
}

function addReferrerVar($referrer, $var)
{
    if (strpos($referrer, '?')) {
        return $referrer . '&' . $var;
    }
    return $referrer . '?' . $var;
}

function googleAnalyticsTurnOn()
{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ddos protection</title>
    <!-- Google Tag Manager -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        if (ga) {
            ga('create', 'UA-69396872-1', 'auto', {allowLinker: true});
            ga('require', 'linker');
            ga('set', 'dimension1', 'ts4realUsers');
            ga('send', 'pageview');
        }

        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-W6N6S7Q');
    </script>
</head>
<?php
}

function successV2Page($referer)
{
    ?>
    <script>
        if (ga) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'ddosPage',
                eventAction: 'capchaSuccess',
                eventLabel: 'capchaSuccess',
                hitCallback: function () {
                    location.href = '<?=$referer?>';
                }
            });
        } else {
            location.href = '<?=$referer?>';
        }
    </script>
    <?php
}

function failedV2Page()
{
?>
<script>
    if (ga) {
        ga('send', {
            hitType: 'event',
            eventCategory: 'ddosPage',
            eventAction: 'capchaError',
            eventLabel: 'capchaError'
        });
    }
    setTimeout(function () {
        location.href = '/';
    }, 3000);
</script>
<body>
<div id="notify-message"></div>
<section>
    <div>
        Invalid Capcha. Please try again.
    </div>
</section>
<?php
}


$referer = filterReferrer($_SERVER['HTTP_REFERER']);
$dirPath = __DIR__ . '/../../vendor/google/recaptcha/src/autoload.php';
require $dirPath;

if (array_key_exists('g-recaptcha-response', $_POST) && $_POST['g-recaptcha-response']) {
    $recaptchaSecret = '6LddAjUUAAAAADozCUilUH_NHMInarTceiLUSAY6';
    $recaptcha       = new \ReCaptcha\ReCaptcha($recaptchaSecret);
    $resp            = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
    if ($resp->isSuccess()) {
        googleAnalyticsTurnOn();
        successV2Page($referer);
        markCookieScore(1);
    } else {
        googleAnalyticsTurnOn();
        failedV2Page();
        markCookieScore(0);
    }
} elseif (array_key_exists('token-v3', $_POST) && $_POST['token-v3']) {
    $minScoreV3      = 0.5;
    $recaptchaSecret = '6Ld4i8gUAAAAADuVgSJK23FYMTWsrg-k8sFzcYL7';

    $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret);
    $resp      = $recaptcha->verify($_POST['token-v3'], $_SERVER['REMOTE_ADDR']);
    if ($resp && $resp->getScore() >= $minScoreV3) {
        markCookieScore($resp->getScore());
        echo 'success';
        exit;
    } else {
        markCookieScore($resp->getScore());
        echo 'failed';
        exit;
    }
} else {
    die('No recaptcha response');
}

?>


