User-agent: *

Allow: /my/print-model3d
Allow: /my/order-laser-cutting
Allow: /sitemap-xml
Disallow: /sitemap
Disallow: /my/
Disallow: /site/download-file
Disallow: /do-restore-session
Disallow: /mybusiness
Disallow: /user
Disallow: /guide/tag
Disallow: /workbench
Disallow: /profile
Disallow: /u/

Sitemap: https://www.treatstock.com/sitemap-xml/sitemap_index.xml