<?php
header('Content-Type: image/png');

$origRed = imagecreatefrompng('/var/www/treatstock/frontend/web/test/colorData/orig/ring_color_ff0000.png');
$res = imagecreatefrompng('/var/www/treatstock/frontend/web/test/colorData/orig/ring_white_ambient_40_color_ffffff.png');
//$res = imagecreatefrompng('/var/www/treatstock/frontend/web/test/colorData/orig/ring_ambient_40_color_ff0000.png');
$colorMap = [];
for ($i = 0; $i < 800; $i++) {
    for ($j = 0; $j < 600; $j++) {
        $oRgb = imagecolorat($origRed, $i, $j);
        $rRgb = imagecolorat($res, $i, $j);
        $oColors = imagecolorsforindex($origRed, $oRgb);
        $rColors = imagecolorsforindex($res, $rRgb);
        if ($oColors['red'] == $oColors['green'] && $oColors['green'] == $oColors['blue']) {
            continue;
        }
        $brightO = $oColors['red'].'_'.$oColors['green'].'_'.$oColors['blue'];
        $brightR = ($rColors['green'] + $rColors['blue']) / 2;
        if (array_key_exists($brightO, $colorMap)) {
            if ($colorMap[$brightO]>$brightR) {
                $colorMap[$brightO] = ($colorMap[$brightO] + $brightR) / 2;
            }
        } else {
            $colorMap[$brightO] = $brightR;
        }

    }
}
/*
ksort($colorMap);

foreach ($colorMap as $k=>$v) {
    echo $k.','.$v.'<br>';
}

exit;
*/
$newImage = imagecreatetruecolor(800, 600);
$backColor = imagecolorallocate($newImage, 255, 255, 255);
for ($i = 0; $i < 800; $i++) {
    for ($j = 0; $j < 600; $j++) {
        $oRgb = imagecolorat($origRed, $i, $j);
        $oColors = imagecolorsforindex($origRed, $oRgb);
        if ($oColors['red'] == $oColors['green'] && $oColors['green'] == $oColors['blue']) {
            $color = imagecolorallocate($newImage, $oColors['red'], $oColors['green'], $oColors['blue']);
            imagesetpixel($newImage, $i, $j, $color);
            continue;
        }
        $brightO = $oColors['red'].'_'.$oColors['green'].'_'.$oColors['blue'];
        $brightR = $colorMap[$brightO];

/*
        if ($brightO < 55) {
            $brightR = $brightO * 4.3;
        }
        if ($brightO >= 55 && $brightO < 60) {
            $brightR = 55*4.3+($brightO-55)*0.9;
        }
        if ($brightO >= 60 && $brightO < 257) {
            $brightR = 55*4.3+(60-55)*0.9 + ($brightO-60)*0.6;
        }
        /*
        if ($brightO>70 && $brightO<150) {
            $brightR = $brightO*3;
        }
        if ($brightO>150 && $brightO<255) {
        */
        //$brightR = $brightO*sqrt(250-$brightO)*0.27;
        //$brightR = $brightO +sqrt(250-$brightO)*4;
        //}
        if ($brightR > 255) {
            $brightR = 255;
        }
        $color = imagecolorallocate($newImage, $brightR, $brightR, $brightR);
        imagesetpixel($newImage, $i, $j, $color);
    }

}
imagepng($newImage);
imagedestroy($newImage);
//imagepng($newImage, '/var/www/treatstock/res.png');


?>