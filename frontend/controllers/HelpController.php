<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace frontend\controllers;


use common\components\ArrayHelper;
use common\components\BaseActiveQuery;
use common\components\exceptions\BusinessException;
use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use common\models\SiteHelpIntl;
use common\models\SiteHelpVote;
use common\models\SystemLang;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\InvalidCallException;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\sphinx\ActiveDataProvider;
use yii\sphinx\Query;

class HelpController extends \common\components\BaseController
{

    public function init()
    {
        parent::init();
        app()->view->headerSearchFormDisabled();
    }

    /**
     *
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        BaseActiveQuery::$staticCacheGlobalEnable = false;
        // $content = $this->view->renderFile('@app/views/help/defaultMain.php');
        $dbContent = SiteHelp::find()->where(['alias' => 'help.main'])->withoutStaticCache()->one();
        $content   = $dbContent->content;
        return $this->render('index', ['defaultContent' => $content]);
    }

    /**
     * @param $from
     * @param $q
     * @return ActiveDataProvider
     */
    private function getSphinxSearch($from, $q)
    {

        $querySphinx = (new Query())->from($from)->showMeta(true);
        $querySphinx->match($q);

        $dataProviderSphinx = new ActiveDataProvider([
            'query'      => $querySphinx,
            'pagination' => [
                'defaultPageSize' => 30,
                'pageSizeLimit'   => 100
            ]
        ]);
        $recordIds          = ArrayHelper::getColumn($dataProviderSphinx->getModels(), 'id');
        if ($recordIds) {
            $dataProvider = SiteHelp::getDataProvider(['id' => $recordIds, 'is_active' => 1], 100);
            if ($dataProvider->getCount() === 0) {
                $dataProvider = SiteHelpCategory::getDataProvider(['id' => $recordIds], 100);
            }
        } else {
            $query = SiteHelp::find()
                ->leftJoin('site_help_intl', 'site_help_intl.model_id=site_help.id')
                ->andWhere([
                    'or', ['like', 'site_help.title', $q], ['like', 'site_help.content', $q],
                    ['like', 'site_help_intl.title', $q], ['like', 'site_help_intl.content', $q],
                ])
                ->andWhere(['site_help.is_active' => 1]);
            $dataProvider = new \yii\data\ActiveDataProvider(
                [
                    'query'      => $query,
                    'pagination' => [
                        'pageSize' => 100,
                    ],

                ]
            );

        }


        return $dataProvider;
    }

    /**
     * @param bool $q
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function actionSearch($q = false)
    {
        if (empty($q)) {
            return $this->redirect(['index']);
        }
        $dataProvider = $this->getSphinxSearch('help', $q);
        return $this->render('helpSearch', ['dataProvider' => $dataProvider, 'q' => $q]);
    }

    /**
     * show category
     *
     * @param $id
     * @param bool $slug
     * @return string
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionCategory($id, $slug = false)
    {
        BaseActiveQuery::$staticCacheGlobalEnable = false;
        $category                                 = SiteHelpCategory::tryFindByPk($id);
        if (empty($slug)) {
            $path = Url::toRoute(array_merge(['help/category', 'id' => $id, 'slug' => $category->getSlug()]));
            return $this->redirect($path, 301);
        }
        $this->view->title = $category->title;
        return $this->render(
            'index',
            ['currentCategory' => $category]
        );
    }

    /**
     * show article
     *
     * @param $id
     * @param bool $alias
     * @return string
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionArticle($id, $alias = false)
    {
        BaseActiveQuery::$staticCacheGlobalEnable = false;
        $article                                  = SiteHelp::tryFind(['id' => $id, 'is_active' => 1]);
        if (empty($alias) || $alias !== $article->getSlug()) { // we redirect because we want to use slug from title
            $path = Url::toRoute(array_merge(['help/article', 'id' => $id, 'alias' => $article->getSlug()]));
            return $this->redirect($path, 301);
        }
        $this->registerLangDomains($article);
        $this->view->title = $article->title;
        $category          = $article->category;
        $isVoted           = $this->isUserVoted($article->id);
        return $this->render('index', ['currentArticle' => $article, 'currentCategory' => $category, 'isVoted' => $isVoted]);
    }

    protected function registerLangDomains(SiteHelp $article)
    {
        foreach (SystemLang::getAllActive() as $lang) {
            $siteHelpIntl = SiteHelpIntl::findOne(['model_id' => $article->id, 'lang_iso' => $lang->iso_code, 'is_active' => 1]);
            if ($siteHelpIntl) {
                if (!$siteHelpIntl->title) {
                    $title = $article->title;
                } else {
                    $title = $siteHelpIntl->title;
                }
                $path                                                 = Url::toRoute(['help/article', 'id' => $article->id, 'alias' => Inflector::slug($title)]);
                $langUrl                                              = Yii::$app->getModule('intlDomains')->domainManager->getUrlWithLangDomain($path, $lang->iso_code, false);
                $this->view->params['alternateLang'][$lang->iso_code] = $langUrl;
            }
        }
    }

    /**
     *
     * @param $id
     * @param string $flag
     * @return array
     * @throws \yii\base\InvalidCallException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionVote($id, $flag = null)
    {
        if (!app('request')->isPost) {
            throw new BusinessException('Invalid request method. Need post method.');
        }
        $article = SiteHelp::tryFindByPk($id);
        if ($flag == 'yes') {
            $article->is_ok = (int)$article->is_ok + 1;
        } else {
            $userId          = UserFacade::getCurrentUserId() ?: UserSessionFacade::getSessionIp();
            $article->is_bad = (int)$article->is_bad + 1;
            if ($flag != 'no') { // user answer
                $answer   = app('request')->post('answer');
                $votehash = md5($userId . $id);
                $voted    = SiteHelpVote::findOne(['vote_hash' => $votehash]);
                if (empty($voted)) {
                    SiteHelpVote::addRecord(
                        [
                            'site_help_id' => $id,
                            'vote_hash'    => $votehash,
                            'vote_answer'  => $answer
                        ]
                    );
                }
            }
        }
        $article->safeSave();
        $this->saveVote($id);
        return $this->jsonMessage(_t('site.help', 'Thank you for your feedback!'));
    }

    /**
     *
     * @param int $id
     * @return boolean
     */
    private function isUserVoted($id)
    {
        $key = 'helpvoted';
        $ids = !isset(app('request')->cookies[$key]) ? [] : explode(",", app('request')->cookies[$key]);
        if (!in_array($id, $ids)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param int $id
     * @return boolean
     * @throws \yii\base\InvalidCallException
     */
    private function saveVote($id)
    {
        $key = 'helpvoted';
        $ids = !isset(app('request')->cookies[$key]) ? [] : explode(',', app('request')->cookies[$key]);
        if (!in_array($id, $ids)) {
            $ids[] = $id;
            app('response')->cookies->add(
                new \yii\web\Cookie(
                    [
                        'name'  => $key,
                        'value' => implode(",", $ids)
                    ]
                )
            );
        }
    }
}