<?php

namespace frontend\controllers\store;

use common\components\BaseController;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\order\anonim\AnonimOrderHelper;
use common\components\order\PriceCalculator;
use common\components\serizaliators\porperties\DeliveryRateProperties;
use common\models\Cart;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\User;
use common\models\UserAddress;
use common\services\LocationService;
use frontend\components\UserSessionFacade;
use frontend\controllers\store\serializers\DeliveryAddressSerializer;
use frontend\models\delivery\DeliveryForm;
use frontend\models\model3d\PlaceOrderState;
use frontend\models\user\UserFacade;
use lib\delivery\delivery\models\DeliveryRate;
use lib\delivery\parcel\Parcel;
use lib\delivery\parcel\ParcelFactory;
use lib\money\Money;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\UserException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\ErrorAction;
use yii\web\Response;

/**
 * Delivery controller - goes here after cart checkout.
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class DeliveryController extends BaseController
{
    /**
     * @var PriceCalculator
     */
    private $priceCalculator;
    /**
     * @var string
     */
    protected $viewPath = '@frontend/views/store/';

    /**
     * @var LocationService
     */
    private $locationService;

    /**
     * @var PlaceOrderState
     */
    public $placeOrderState;


    /**
     * @param PriceCalculator $priceCalculator
     * @param LocationService $locationService
     * @param PlaceOrderState $placeOrderState
     */
    public function injectDependencies(PriceCalculator $priceCalculator, LocationService $locationService, PlaceOrderState $placeOrderState)
    {
        $this->priceCalculator = $priceCalculator;
        $this->locationService = $locationService;
        $this->placeOrderState = $placeOrderState;
    }

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->view->noindex();
        $this->view->title = _t('site.store', 'Delivery');
    }

    /**
     * External actions
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'deliverHere' => ['post'],
                    'loadRates'   => ['post']
                ]
            ],
        ];
    }

    /**
     * delivery form
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     * @throws UserException
     */
    public function actionIndex()
    {
        $lastCartLink = \Yii::$app->cart->getCartLink();
        if ($lastCartLink) {
            return $this->redirect($lastCartLink);
        }
        return $this->render('cartEmpty');
    }

    /**
     *
     */
    public function actionLoadRates()
    {
        $deliveryForm = $this->loadDeliveryForm();
        $deliveryForm->setScenario(DeliveryForm::SCENARIO_DELIVERY);

        try {
            if (!$deliveryForm->validate()) {
                return $this->jsonFailure(['message' => Html::errorSummary($deliveryForm, ['header' => '', 'encode' => false])]);
            }

            $userCurrency = UserSessionFacade::getCurrency();
            $cart         = Cart::tryFind(['user_session_id' => UserFacade::getUserSession()->id]);
            $machine      = $cart->getMachine();

            $printPrice = $this->priceCalculator->calculateModelAndPrintPriceByCart($cart->getModel3d(), $cart->getMachine()->asPrinter());
            $parcel = ParcelFactory::createFromCart($cart);

            $rate = $this->getShippingRate($machine, $deliveryForm, $printPrice, $parcel);

            if (!$rate) {
                $deliveryForm->reportNoRates($machine->asDeliveryParams()->getUserAddress(), $deliveryForm->getUserAddress(), $cart);
                $msg = _t('site.address', 'Unfortunately, we cannot provide delivery to this address.');
                return $this->jsonFailure([
                    'message' => $msg
                ]);
            }

            $result = [
                'rates' => DeliveryRateProperties::serialize([$rate])
            ];
            if ($deliveryForm->getValidatedAddress()->isEasyPost()) {
                $result['validated_address'] = DeliveryAddressSerializer::serialize($deliveryForm->getValidatedAddress());
            }
            $this->locationService->changeUserLocationByDelivery($deliveryForm);
            return $this->jsonSuccess($result);
        } catch (\Exception $e) {
            logException($e, 'tsdebug');
            throw new BusinessException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @return bool
     */
    public function actionCheckEmailExist()
    {
        $email = Yii::$app->request->getBodyParam('email');
        if (!$email) {
            throw new InvalidArgumentException();
        }
        $result                     = User::find()->where(['email' => $email])->exists();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    /**
     * @param CompanyService $machine
     * @param DeliveryForm $deliveryForm
     * @param Money $printPrice
     * @param \lib\delivery\parcel\Parcel $parcel
     * @return DeliveryRate|null
     */
    private function getShippingRate(CompanyService $machine, DeliveryForm $deliveryForm, Money $printPrice, Parcel $parcel)
    {
        $userCurrency = UserSessionFacade::getCurrency();
        $formKey      = serialize($deliveryForm->getAttributes(['contact_name', 'company', 'street', 'street2', 'city', 'state', 'zip', 'country']));
        $key          = md5('g1' . $machine->id . $machine->getDeliveryOptionsHash() . $formKey . serialize($parcel->attributes) . $userCurrency);
        if ($shippingCache = app('cache')->get($key)) {
            return $shippingCache;
        }
        $rate = Yii::$app->deliveryService->getOrderRate($machine->asDeliveryParams(), $deliveryForm, $printPrice, $parcel, $userCurrency);
        app('cache')->set($key, $rate);
        return $rate;
    }

    /**
     * Load delivery form from request
     *
     * @return DeliveryForm
     */
    private function loadDeliveryForm()
    {
        $cart         = Cart::tryFind(['user_session_id' => UserFacade::getUserSession()->id], _t('site.delivery', 'Session expired. Please reload the page.'));
        $post         = Yii::$app->request->getBodyParams();
        $data         = isset($post['DeliveryForm']) ? $post['DeliveryForm'] : $post;
        $deliveryForm = new DeliveryForm($cart->getMachine()->asDeliveryParams(), $cart->getModel3d());
        $deliveryForm->setScenario(isset($data['deliveryType']) && $data['deliveryType'] == DeliveryType::PICKUP ? DeliveryForm::SCENARIO_PICKUP : DeliveryForm::SCENARIO_DELIVERY);
        $deliveryForm->load($data, '');
        if (!empty($data['comment'])) {
            $deliveryForm->comment = $data['comment'];
        }
        $deliveryForm->userId = AnonimOrderHelper::resolveOrderUser()->id;
        return $deliveryForm;
    }

    /**
     * @return array
     */
    public static function getUserAddressSerializer()
    {
        return [
            UserAddress::class => [
                'id',
                'contact_name',
                'country' => 'country.iso_code',
                'state'   => 'region',
                'city',
                'street'  => 'address',
                'street2' => 'extended_address',
                'zip'     => 'zip_code',
                'comment',
                'phone',
                'email',
                'company'
            ],
        ];
    }
}
