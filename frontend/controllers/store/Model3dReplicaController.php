<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.08.16
 * Time: 15:16
 */

namespace frontend\controllers\store;

use common\components\BaseController;
use common\models\Model3dReplica;
use common\services\Model3dReplicaService;
use frontend\models\model3d\Model3dItemForm;
use Yii;
use yii\filters\AccessControl;

class Model3dReplicaController extends BaseController
{

    protected $viewPath = '@frontend/views/model3d/';


    public function init()
    {
        parent::init();
        $this->view->title = _t('site.store', 'Model3d');
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'index'
                        ],
                        'roles'   => [
                            '@'
                        ]
                    ],

                ]
            ]
        ];
    }

    /**
     *
     * @param integer $id Model3dReplica id
     * @return array
     * @throws \lib\message\exceptions\BadParamsException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex($id)
    {
        $model3dReplica = Model3dReplica::tryFindByPk($id);
        Model3dReplicaService::tryCheckModel3dReplicaViewRights($model3dReplica);

        $model3dItemForm = Model3dItemForm::create($model3dReplica);

        if (Yii::$app->request->get('fileId')) {
            $model3dItemForm->setCurrentSelectedFotoramaFileId(Yii::$app->request->get('fileId'));
        }

        return $this->render('model3dReplica', ['model3dItemForm' => $model3dItemForm]);
    }


}