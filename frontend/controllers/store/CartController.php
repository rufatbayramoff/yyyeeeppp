<?php

namespace frontend\controllers\store;

use common\components\BaseController;
use common\components\exceptions\BusinessException;
use common\components\order\PriceCalculator;
use common\models\Cart;
use common\models\CartItem;
use common\models\factories\Model3dReplicaFactory;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PsPrinter;
use common\models\repositories\Model3dReplicaRepository;
use common\models\repositories\Model3dRepository;
use common\models\StoreUnit;
use common\models\StoreUnitShoppingCandidate;
use common\modules\promocode\components\PromocodeApplyService;
use common\services\Model3dPartService;
use common\services\Model3dService;
use common\services\PrinterMaterialService;
use common\services\StoreUnitShoppingCandidateService;
use frontend\components\cart\CartFactory;
use frontend\components\cart\exceptions\CartEmptyException;
use frontend\models\model3d\Model3dFacade;
use frontend\models\model3d\Model3dItemForm;
use frontend\models\store\GetPrintedForm;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\ErrorException;
use yii\base\UserException;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Cart related controller.
 * When user clicks PRINT - this action goes here as cart/add
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class CartController extends BaseController
{
    protected $viewPath = '@frontend/views/store/';

    /** @var  StoreUnitShoppingCandidateService */
    public $shoppingCandidateService;

    public function injectDependencies(StoreUnitShoppingCandidateService $shoppingCandidateService)
    {
        $this->shoppingCandidateService = $shoppingCandidateService;
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'add' => ['post'],
                ]
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->view->noindex();
        return $this->render('cart.php', []);
    }

    /**
     * user adds model to cart using store unit and printer id
     * current realization will auto-create order and will redirect to delivery
     *
     * @return string
     * @throws HttpException
     * @throws UserException
     */
    public function actionAdd()
    {
        try {
            $printerId   = Yii::$app->request->post('psPrinterId') ?: Yii::$app->request->get('psPrinterId');
            $canChangePs = Yii::$app->request->post('GetPrintedForm') ? Yii::$app->request->post('GetPrintedForm')['canChangePs'] : true;

            $model3dEditForm = Yii::$app->request->post('Model3dEditForm');

            if (array_key_exists('model3dUId', $model3dEditForm)) { // Support model3dUid 
                $model3dUid = $model3dEditForm['model3dUId'];

                if (strpos($model3dUid, 'R:') === 0) { // Model3dReplica
                    $model3dEditForm['replicaId'] = substr($model3dUid, 2, 1024);
                } else {
                    $model3dEditForm['model3dId'] = $model3dUid;
                }
            }

            if (array_key_exists('replicaId', $model3dEditForm)) {
                $model3dReplicaId = $model3dEditForm['replicaId'];
                $model3dReplica   = Model3dReplica::tryFindByPk($model3dReplicaId);
                $model3dReplica->cleanEmptyQty();
                $storeUnit = $model3dReplica->storeUnit;
            } else {
                $storeUnitId = Yii::$app->request->post('storeUnitId') ?: Yii::$app->request->get('storeUnitId');

                if ($storeUnitId) {
                    $storeUnit = StoreUnit::tryFindByPk($storeUnitId);
                    $model3d   = $storeUnit->model3d;
                } elseif (array_key_exists('model3dId', $model3dEditForm)) {
                    $model3dId         = $model3dEditForm['model3dId'];
                    $model3dRepository = Yii::createObject(Model3dRepository::class);
                    $model3d           = $model3dRepository->getByUid($model3dId);
                    $storeUnit         = $model3d->storeUnit;
                }
                if (array_key_exists('modelTextureInfo', $model3dEditForm)) {
                    $modelTextureInfo = $model3dEditForm['modelTextureInfo'];
                    PrinterMaterialService::unSerializeModel3dTexture($model3d, $modelTextureInfo);
                }

                $model3dReplica = Model3dReplicaFactory::createModel3dReplica($storeUnit->model3d, $model3dEditForm['partsQty'] ?? null);
                $model3dReplica->setStoreUnit($storeUnit);
                $storeUnit->setModel3d($model3dReplica);

                if (array_key_exists('scaleBy', $model3dEditForm)) {
                    $scaleBy = $model3dEditForm['scaleBy'];
                    if ($scaleBy > 0 && ($scaleBy < 99 || $scaleBy > 101)) {
                        Model3dService::scaleModel3d($model3dReplica, $scaleBy / 100);
                    }
                }
            }

            if (!$storeUnit->isAvailableForBuy()) {
                throw new BusinessException(_t('site.store', 'This model cannot be added to cart'));
            }

            $this->shoppingCandidateService->formShoppingCandidate($storeUnit->model3d->getSourceModel3d(), StoreUnitShoppingCandidate::TYPE_PRINT_HERE);

            $printer = PsPrinter::getValidated($printerId);

            $model3dItemForm = Model3dItemForm::create($model3dReplica);
            $model3dItemForm->load(app('request')->post()['Model3dEditForm']);

            Model3dService::setCheapestMaterialInPrinter($model3dReplica, $printer);

            if (!Model3dFacade::isCanPrintModelByTextureAndSize($printer, $model3dReplica)) {
                throw new BusinessException("This printer cant't print model");
            }

            Model3dReplicaRepository::save($model3dReplica);

            $cart     = CartFactory::createCart();
            $cartItem = CartFactory::createCartItem($model3dReplica, $printer->companyService, $model3dItemForm->quantity, $canChangePs);

            $cart->addPosition($cartItem);
            if (!$cart->safeSave()) {
                throw new ErrorException('Cant save cart: ' . json_encode($cart->getErrors()));
            };
            $getPrintedForm = $this->createGetPrintedFormByCart($cart);
        } catch (UserException $e) {
            \Yii::error($e);
            throw $e;
        } catch (\Exception $e) {
            \Yii::error($e);
            throw new HttpException(500, 'Server error. Please contact us to fix this problem');
        }

        if (Yii::$app->request->get('redirectToDelivery')) {
            return $this->renderAdaptive('redirectToDelivery');
        }

        return $this->renderAdaptive(
            'getprintedModal',
            [
                'cart'           => $cart,
                'getPrintedForm' => $getPrintedForm,
            ]
        );
    }

    /**
     *
     * @return string
     * @throws UserException
     */
    public function actionChange()
    {
        $userSession = UserFacade::getUserSession();
        $cart        = Cart::tryFind(['user_session_id' => $userSession->id]);
        \yii\helpers\Url::remember('/store/delivery');
        return $this->renderAjax(
            'getprintedModal.php',
            [
                'cart'           => $cart,
                'getPrintedForm' => $this->createGetPrintedFormByCart($cart),
                'change'         => true,
            ]
        );
    }

    /**
     * update cart info, when user clicks color
     *
     * @param int $id Cart item id
     * @return string
     * @throws UserException
     *
     * TODO: update to post request
     */
    public function actionUpdateCart($id)
    {
        $cartItem    = CartItem::tryFindByPk($id);
        $userSession = UserFacade::getUserSession();
        $cart        = $cartItem->cart;
        if ($cart->user_session_id !== $userSession->id) {
            throw new BusinessException(_t('site.store', 'Can only change your own cart'));
        }
        $storeUnit = $cartItem->model3dReplica->storeUnit;
        if (!$storeUnit->isAvailableForBuy()) {
            throw new BusinessException(_t('site.store', 'This model cannot be added to cart'));
        }
        if (array_key_exists('materialId', app('request')->get())) {
            $materialId = app('request')->get()['materialId'];
            $colorId    = app('request')->get()['colorId'];
            $material   = PrinterMaterial::tryFindByPk($materialId);
            $color      = PrinterColor::tryFindByPk($colorId);
            $cartItem->model3dReplica->setKitModelMaterialAndColor($material, $color);
            $cartItem->model3dReplica->model3dTexture->safeSave();
        }

        if (array_key_exists('qty', app('request')->get())) {
            $qty           = app('request')->get()['qty'];
            $cartItem->qty = $qty;
        }

        $notifyMsg = '';
        if (array_key_exists('model3dPart', app('request')->get())) {
            $modelPartId = (int)app('request')->get('model3dPart');
            foreach ($cartItem->model3dReplica->model3dParts as $part) {
                if ($part->id === $modelPartId) {
                    $oldQty    = $part->qty;
                    $part->qty = (int)app('request')->get('partQty');
                    if ($cart->getTotalQty() === 0) {
                        $part->qty = $oldQty;
                        //$notifyMsg = _t('site.store', 'Cannot order empty model.');
                        continue;
                    }
                    $part->safeSave();
                    break;
                }
            }
            $storeUnit->setModel3d($cartItem->model3dReplica);
            $cartItem->price = PriceCalculator::calculateModel3dPrice($storeUnit->model3d);
        }
        $promoService = new PromocodeApplyService();
        $promoService->clearRepo();
        $cartItem->safeSave();
        return $this->renderAjax(
            'getprintedModal',
            [
                'cart'           => $cart,
                'notifyMsg'      => $notifyMsg,
                'getPrintedForm' => $this->createGetPrintedFormByCart($cart),
            ]
        );
    }

    public function actionClear()
    {
        return $this->redirect('/store/delivery');
    }

    /**
     * checkout action, after user clicks Procceed to checkout
     * on Get printed modal window page.
     *
     * @return array
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     * @throws \frontend\components\cart\exceptions\CartEmptyException
     */
    public function actionCheckout()
    {
        $userSession = UserFacade::getUserSession();
        $cart        = Cart::findOne(['user_session_id' => $userSession->id]);
        if (!$cart && UserFacade::getCurrentUserId()) {
            $cart = Cart::findOne(['user_id' => UserFacade::getCurrentUserId()]);
        }
        if (!$cart || !$cart->cartItems) {
            throw new CartEmptyException(_t('site.cart', 'Sorry but your cart is empty'));
        }
        if (array_key_exists('canChangePs', app('request')->post()['GetPrintedForm'])) {
            $isCanChangePs           = app('request')->post()['GetPrintedForm']['canChangePs'];
            $cartItems               = $cart->cartItems;
            $cartItem                = reset($cartItems);
            $cartItem->can_change_ps = $isCanChangePs;
            $cartItem->safeSave();
        }
        return $this->jsonSuccess(
            [
                'redir' => Url::toRoute(['store/delivery'])
            ]
        );
    }

    /**
     * Crate GetPrintedForm based on cart
     *
     * @param Cart $cart
     * @return GetPrintedForm
     */
    private function createGetPrintedFormByCart(Cart $cart)
    {
        $firstItem                      = $cart->getFirstItem();
        $getPrintedForm                 = new GetPrintedForm();
        $getPrintedForm->qty            = $firstItem->qty;
        $getPrintedForm->model3dReplica = $firstItem->model3dReplica;
        $getPrintedForm->printer        = $firstItem->machine->asPrinter();
        $getPrintedForm->canChangePs    = $firstItem->can_change_ps;
        return $getPrintedForm;
    }
}
