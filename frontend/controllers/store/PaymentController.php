<?php

namespace frontend\controllers\store;

use common\components\BaseActiveQuery;
use common\components\BaseController;
use common\components\DateHelper;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\OnlyPostRequestException;
use common\components\order\history\OrderHistoryService;
use common\models\loggers\PreorderLogger;
use common\models\PaymentBankInvoice;
use common\models\PaymentInvoice;
use common\models\PaymentPayPageLog;
use common\models\PaymentPayPageLogProcess;
use common\models\PaymentPayPageLogStatus;
use common\models\PaymentTransaction;
use common\models\PaymentTransactionRefund;
use common\models\StoreOrder;
use common\models\StoreOrderAttemp;
use common\models\User;
use common\modules\cutting\models\PlaceCuttingOrderState;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\components\PaymentUrlHelper;
use common\modules\payment\exception\PaymentException;
use common\modules\payment\factories\PaymentInvoiceFactory;
use common\modules\payment\gateways\vendors\StripeGateway;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\modules\payment\serializers\PaymentInvoiceSerializer;
use common\modules\payment\services\InvoiceBankService;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use common\modules\payment\services\PaymentStoreOrderService;
use common\modules\payment\services\RefundService;
use common\modules\payment\services\StoreOrderPositionService;
use common\modules\payment\widgets\PaymentBankViewWidget;
use common\services\StoreOrderService;
use frontend\models\model3d\PlaceOrderState;
use frontend\models\user\UserFacade;
use frontend\modules\preorder\components\PreorderEmailer;
use lib\money\Currency;
use Psr\Log\InvalidArgumentException;
use Yii;
use yii\base\BaseObject;
use yii\base\UserException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Payment controller
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 *
 * @property InvoiceBankService $invoiceBankService
 * @property PaymentAccountService $paymentAccountService
 */
class PaymentController extends BaseController
{
    protected $viewPath = '@frontend/views/store/';

    protected $invoiceBankService;

    /**
     * @var PaymentService
     */
    protected $paymentService;


    /**
     * @var StoreOrderService
     */
    protected $orderService;

    /**
     * @var PreorderLogger
     */
    protected $preorderLogger;

    /**
     * @var StoreOrderPositionService
     */
    protected $orderPositionService;

    /**
     * @var OrderHistoryService
     */
    protected $orderHistoryService;

    /**
     * @var PlaceOrderState
     */
    public $placeOrderState;

    /**
     * @var PlaceCuttingOrderState
     */
    public $cuttingOrderState;

    /**
     * @var StoreOrderService
     */
    public $storeOrderService;

    /**
     * @var PreorderEmailer
     */
    public $preorderEmailer;

    /**
     * @var RefundService
     */
    public $refundService;

    /** @var PaymentInvoiceFactory */
    protected $paymentInvoiceFactory;

    /** @var PaymentInvoiceRepository */
    protected $paymentInvoiceRepository;

    /** @var PaymentStoreOrderService */
    protected $paymentStoreOrderService;

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /**
     * @param InvoiceBankService $invoiceService
     * @param StoreOrderService $orderService
     * @param PaymentService $paymentService
     * @param PreorderLogger $preorderLogger
     * @param PlaceOrderState $placeOrderState
     * @param PreorderEmailer $preorderEmailer
     * @param RefundService $refundService
     * @param StoreOrderPositionService $orderPositionService
     * @param OrderHistoryService $orderHistoryService
     * @param PaymentInvoiceFactory $paymentInvoiceFactory
     * @param PaymentInvoiceRepository $paymentInvoiceRepository
     * @param StoreOrderService $storeOrderService
     * @param PaymentStoreOrderService $paymentStoreOrderService
     * @param PaymentAccountService $paymentAccountService
     */
    public function injectDependencies(
        InvoiceBankService        $invoiceService,
        StoreOrderService         $orderService,
        PaymentService            $paymentService,
        PreorderLogger            $preorderLogger,
        PlaceOrderState           $placeOrderState,
        PlaceCuttingOrderState    $cuttingOrderState,
        PreorderEmailer           $preorderEmailer,
        RefundService             $refundService,
        StoreOrderPositionService $orderPositionService,
        OrderHistoryService       $orderHistoryService,
        PaymentInvoiceFactory     $paymentInvoiceFactory,
        PaymentInvoiceRepository  $paymentInvoiceRepository,
        StoreOrderService         $storeOrderService,
        PaymentStoreOrderService  $paymentStoreOrderService,
        PaymentAccountService     $paymentAccountService
    )
    {
        $this->invoiceBankService       = $invoiceService;
        $this->paymentService           = $paymentService;
        $this->orderService             = $orderService;
        $this->placeOrderState          = $placeOrderState;
        $this->cuttingOrderState        = $cuttingOrderState;
        $this->preorderLogger           = $preorderLogger;
        $this->preorderEmailer          = $preorderEmailer;
        $this->refundService            = $refundService;
        $this->orderPositionService     = $orderPositionService;
        $this->orderHistoryService      = $orderHistoryService;
        $this->paymentInvoiceFactory    = $paymentInvoiceFactory;
        $this->paymentInvoiceRepository = $paymentInvoiceRepository;
        $this->storeOrderService        = $storeOrderService;
        $this->paymentStoreOrderService = $paymentStoreOrderService;
        $this->paymentAccountService    = $paymentAccountService;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'checkout' => ['post']
                ]
            ],
        ];
    }

    /**
     * Pay for invoice
     *
     * @param $invoiceUuid
     *
     * @return stringUserException
     * @throws NotFoundHttpException
     * @throws
     * @throws \yii\base\InvalidConfigException
     */
    public function actionPayInvoice($invoiceUuid)
    {
        /** @var PaymentInvoice $paymentInvoice */
        $paymentInvoice = $this->getPaymentInvoiceResolve($invoiceUuid);

        try {
            $paymentInvoice->canBePaidOrFail();
        } catch (BusinessException $businessException) {
            return $this->render('payInvoiceError.php', ['message' => $businessException->getMessage(), 'paymentInvoice' => $paymentInvoice]);
        }

        $logUuid           = \common\components\UuidHelper::generateUuid();
        $this->view->title = _t('site.payment', 'Payment. Invoice #{uuid}', ['uuid' => $paymentInvoice->uuid]);

        if ($paymentInvoice->instantPayment) {
            $this->view->title = _t('site.payment', 'Payment. Instant payment "{uuid}"', ['uuid' => $paymentInvoice->instantPayment->uuid]);
        } elseif ($paymentInvoice->orderPosition) {
            $this->view->title = _t('site.payment', 'Payment. Addition service "{name}"', ['name' => $paymentInvoice->orderPosition->title]);
        } elseif ($paymentInvoice->storeOrder) {
            $this->view->title = _t('site.payment', 'Payment. Order #{orderId}', ['orderId' => $paymentInvoice->storeOrder->id]);
        }

        $defaultPayCardVendor = param('default_pay_card_vendor');
        if (app('request')->get('defaultPayCardVendor') == 'braintree') {
            $defaultPayCardVendor = 'braintree';
        }

        $braintreeClientToken = app('session')->get('brt_token_' . $paymentInvoice->uuid);
        if (!$braintreeClientToken) {
            $braintreePaymentCheckout = new PaymentCheckout(PaymentTransaction::VENDOR_BRAINTREE);
            $braintreeClientToken     = $braintreePaymentCheckout->generateClientToken($paymentInvoice);
            app('session')->set('brt_token_' . $paymentInvoice->uuid, $braintreeClientToken);
        }
        $stripeClientToken = app('session')->get('stripe_token_' . $paymentInvoice->uuid);
        if (!$stripeClientToken && $paymentInvoice->getAmountTotal()->notZero()) {
            $stripePaymentCheckout = new PaymentCheckout(PaymentTransaction::VENDOR_STRIPE);
            if ($paymentInvoice->currency !== Currency::USD) {
                $stripePaymentCheckout->paymentProcessor->stripeGateway->paymentMethods = [StripeGateway::METHOD_TYPE_CARD];
            }
            $stripePaymentCheckout->paymentProcessor->stripeGateway->paymentLogger->payPageLogUuid = $logUuid;
            $stripeClientToken = $stripePaymentCheckout->generateClientToken($paymentInvoice);
            app('session')->set('stripe_token_' . $paymentInvoice->uuid, $stripeClientToken);
        }
        $stripeAlipayClientToken = '';
        if ($paymentInvoice->currency === Currency::USD && $paymentInvoice->getAmountTotal()->notZero()) {
            $stripeAlipayClientToken = app('session')->get('stripe_alipay_token_' . $paymentInvoice->uuid);
            if (!$stripeAlipayClientToken) {
                $stripePaymentCheckout                                                  = new PaymentCheckout(PaymentTransaction::VENDOR_STRIPE);
                $stripePaymentCheckout->paymentProcessor->stripeGateway->paymentMethods = [StripeGateway::METHOD_TYPE_ALIPAY];
                $stripePaymentCheckout->paymentProcessor->stripeGateway->captureMethod  = StripeGateway::CAPTURE_METHOD_AUTOMATIC;
                $stripePaymentCheckout->paymentProcessor->stripeGateway->paymentLogger->payPageLogUuid = $logUuid;
                $stripeAlipayClientToken                                                = $stripePaymentCheckout->generateClientToken($paymentInvoice);
                app('session')->set('stripe_alipay_token_' . $paymentInvoice->uuid, $stripeAlipayClientToken);
            }
        }

        if ($paymentInvoice->isInvoiceGroup()) {
            $invoiceBraintree    = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($paymentInvoice->invoice_group_uuid,
                PaymentTransaction::VENDOR_BRAINTREE);
            $invoiceTs           = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($paymentInvoice->invoice_group_uuid,
                PaymentTransaction::VENDOR_TS);
            $invoiceBonus        = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($paymentInvoice->invoice_group_uuid,
                PaymentTransaction::VENDOR_BONUS);
            $invoiceBankTransfer = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($paymentInvoice->invoice_group_uuid,
                PaymentTransaction::VENDOR_BANK_TRANSFER);
        } else {
            $invoiceBraintree    = $paymentInvoice;
            $invoiceTs           = $paymentInvoice;
            $invoiceBonus        = $paymentInvoice;
            $invoiceBankTransfer = $paymentInvoice;
        }

        $this->view->params['hidesearch'] = true;
        $this->view->params['hidenav']    = true;

        return $this->render('payInvoice.php', [
            'logUuid'                 => $logUuid,
            'storeOrder'              => $paymentInvoice->storeOrder,
            'storeOrderPosition'      => $paymentInvoice->orderPosition,
            'instantPayment'          => $paymentInvoice->instantPayment,
            'primaryInvoice'          => $paymentInvoice,
            'invoiceBraintree'        => $invoiceBraintree,
            'invoiceTs'               => $invoiceTs,
            'invoiceBonus'            => $invoiceBonus,
            'invoiceBankTransfer'     => $invoiceBankTransfer,
            'braintreeClientToken'    => $braintreeClientToken,
            'stripeClientToken'       => $stripeClientToken,
            'stripeAlipayClientToken' => $stripeAlipayClientToken,
            'amountNetIncome'         => $this->paymentAccountService->getUserMainAmount($paymentInvoice->user, $paymentInvoice->currency),
            'defaultPayCardVendor'    => $defaultPayCardVendor
        ]);
    }

    /**
     * Checkout.
     * Create payment transaction
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCheckout()
    {
        BaseActiveQuery::$staticCacheGlobalEnable = false;
        $invoiceUuid                              = app('request')->post('payment_invoice') ?? app('request')->get('payment_invoice');
        $logUuid                                  = app('request')->post('logUuid');
        $vendor                                   = app('request')->post('vendor');
        $this->actionCheckoutSuccess($vendor, $invoiceUuid, $logUuid);
    }


    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCheckoutDone()
    {
        $paymentInvoice = PaymentInvoice::tryFind(['uuid' => \Yii::$app->request->get('invoice')]);

        $isWidget = 0;
        if (isset($_GET['widget']) && $_GET['widget']) {
            $isWidget = 1;
        }

        if (!$paymentInvoice->isAvailableForUser()) {
            throw new InvalidArgumentException('Unavailable for current user');
        }
        if (!$paymentInvoice->isPayed()) {
            $this->setFlashMsg(false, _t('site.store', 'Payment failed'));
            return $this->redirect(PaymentUrlHelper::payInvoice($paymentInvoice));
        }

        $this->placeOrderState->resetState();
        $this->cuttingOrderState->resetState();
        if ($isWidget) {
            $this->layout = 'plain.php';
        }

        return $this->render('checkoutDone.php', compact('paymentInvoice', 'isWidget'));
    }

    public function actionCheckoutFailed($vendor, $paymentInvoiceUuid, $logUuid = '')
    {
        $paymentInvoice = PaymentInvoice::tryFind(['uuid' => $paymentInvoiceUuid]);
        if ($logUuid) {
            $payPageLogStatus              = new PaymentPayPageLogStatus();
            $payPageLogStatus->uuid        = $logUuid;
            $payPageLogStatus->status_date = DateHelper::now();
            $payPageLogStatus->status      = PaymentPayPageLog::STATUS_FAILED;
            $payPageLogStatus->vendor      = $vendor;
            $payPageLogStatus->safeSave();
        }

        if (!$paymentInvoice->isAvailableForUser()) {
            throw new InvalidArgumentException('Unavailable for current user');
        }
        $this->setFlashMsg(false, _t('site.store', 'Payment failed'));
        return $this->redirect(PaymentUrlHelper::payInvoice($paymentInvoice));
    }

    public function actionCheckoutSuccess($vendor, $paymentInvoiceUuid, $logUuid = '')
    {
        $isWidget       = app('request')->get('widget', 0);
        $paymentInvoice = PaymentInvoice::tryFind(['uuid' => $paymentInvoiceUuid]);

        $paymentToken = $this->getPaymentCheckoutToken($vendor);

        if ($logUuid) {
            PaymentPayPageLogStatus::logStatus($logUuid, $vendor, PaymentPayPageLog::STATUS_SERVER_PROCESS);
        }

        try {
            $paymentInvoice->canBePaidOrFail();

            if (!$paymentInvoice->canPaidByMethodType($vendor, $this->getCurrentUser())) {
                throw new BusinessException(_t('site.store', 'Unable to pay by current payment method'));
            }

            if (Yii::$app->redis->get('checkoutSuccessInvoice:invoice_' . $paymentInvoiceUuid)) {
                sleep(7);
                return $this->redirect('/store/payment/checkout-done?invoice=' . $paymentInvoice->uuid . ($isWidget ? '&widget=1' : ''));
            }
            Yii::$app->redis->set('checkoutSuccessInvoice:invoice_' . $paymentInvoiceUuid, 1);

            $paymentInvoice->setPaymentInProgress();
            $paymentInvoice->setPrimaryInvoiceForRelatedObjects();

            $paymentCheckout = new PaymentCheckout($vendor);
            if ($logUuid && $paymentCheckout?->getGateway()?->getLogger()) {
                $paymentCheckout->getGateway()->getLogger()->payPageLogUuid = $logUuid;
            }
            $paymentCheckout->pay($paymentInvoice, $paymentToken);

            if ($posUid = \Yii::$app->request->get('posUid', '')) {
                $this->placeOrderState->setStateUid($posUid);
                $this->cuttingOrderState->setStateUid($posUid);
            }
            $this->placeOrderState->resetState();
            $this->cuttingOrderState->resetState();
        } catch (UserException $e) {
            PaymentPayPageLogProcess::logProcess($logUuid, ['err' => json_encode(['authorize_failed' => $e->getMessage()])]);
            PaymentPayPageLogStatus::logStatus($logUuid, $vendor, PaymentPayPageLog::STATUS_AUTHORIZE_FAILED);
            $this->setFlashMsg(false, _t('site.store', $e->getMessage()));
            return $this->redirect(['store/payment/pay-invoice', 'invoiceUuid' => $paymentInvoice->uuid]);
        } catch (\Exception $e) {
            PaymentPayPageLogProcess::logProcess($logUuid, ['err' => json_encode(['authorize_failed' => $e->getMessage()])]);
            PaymentPayPageLogStatus::logStatus($logUuid, $vendor, PaymentPayPageLog::STATUS_AUTHORIZE_FAILED);
            logException($e, 'tsdebug');
            $this->setFlashMsg(false, _t('site.store', 'Payment failed. Please contact support@treatstock.com'));
            return $this->redirect(['store/payment/pay-invoice', 'invoiceUuid' => $paymentInvoice->uuid]);
        }

        if ($logUuid) {
            PaymentPayPageLogStatus::logStatus($logUuid, $vendor, PaymentPayPageLog::STATUS_DONE);
        }

        $this->redirect('/store/payment/checkout-done?invoice=' . $paymentInvoice->uuid . ($isWidget ? '&widget=1' : ''));
    }

    /**
     * @param $uuid - PaymentBankInvoice
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionInvoice($uuid)
    {
        $paymentBankInvoice = PaymentBankInvoice::tryFind([
            'uuid' => $uuid
        ]);

        $this->invoiceBankService->checkAccessToInvoice($paymentBankInvoice->paymentInvoice);
        $this->layout = 'plain.php';

        return $this->render('@common/modules/payment/views/invoice.php', [
            'bankInvoice' => $paymentBankInvoice,
        ]);
    }

    /**
     * Generate bank invoice
     *
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \yii\base\Exception
     */
    public function actionCheckoutInvoice()
    {
        $paymentInvoice = PaymentInvoice::tryFind([
            'uuid' => app()->request->post('payment_invoice')
        ]);

        $this->invoiceBankService->checkAccessToInvoice($paymentInvoice);

        $paymentInvoice->canBePaidOrFail();

        $paymentInvoice->setPrimaryInvoiceForRelatedObjects();

        $bankInvoice = $paymentInvoice->paymentBankInvoice ?: $this->invoiceBankService->createBankInvoiceByInvoice($paymentInvoice);
        $this->invoiceBankService->sendEmailBankInvoice($bankInvoice, $this->invoiceBankService->paymentBankInvoicePdf($bankInvoice));
        $this->placeOrderState->clearModel3d();
        $this->placeOrderState->resetState();
        $this->cuttingOrderState->resetState();

        return $this->redirect(['/store/payment/invoice', 'uuid' => $bankInvoice->uuid]);
    }

    /**
     * @param $id - order id
     *
     * @return array|string
     * @throws NotFoundHttpException
     * @throws PaymentException
     * @throws UserException
     * @throws \yii\base\ExitException
     */
    public function actionRefundRequest()
    {
        /** @var User $currentPsUser */
        $currentPsUser = $this->getCurrentUser();
        if (!app('request')->isPost) {
            throw new OnlyPostRequestException();
        }
        $printAttempt = StoreOrderAttemp::tryFindByPk(Yii::$app->request->post('attemptId'));
        if ($printAttempt->ps->user_id !== $currentPsUser->id) {
            throw new InvalidArgumentException('Invalid user');
        }

        $storeOrder = $printAttempt->order;
        $tr         = $this->paymentStoreOrderService->findOrderPaymentTransaction($storeOrder);
        if (!$tr) {
            throw new BusinessException('No transactions to refund.');
        }
        $refundModel = new PaymentTransactionRefund();
        $refundModel->initWithTransaction($tr);
        $refundModel->isFullRefund   = false;
        $refundModel->amount         = Yii::$app->request->post('refundAmount');
        $refundModel->comment        = Yii::$app->request->post('refundComment');
        $refundModel->from_user_id   = $currentPsUser->id;
        $refundModel->transaction_id = $tr->id;

        try {
            $this->refundService->addRequestRefund($storeOrder->getPrimaryInvoice(), $refundModel);
        } catch (\Exception $e) {
            return $this->jsonError($e->getMessage());
        }

        return $this->jsonSuccess(
            [
                'message' => _t('site.store', 'Refund submitted')
            ]
        );
    }

    public function actionInvoiceBillingDetails()
    {
        $paymentInvoice = $this->getPaymentInvoiceResolve(Yii::$app->request->get('invoiceUuid'));

        if (!$paymentInvoice->orderPosition && $paymentInvoice->storeOrder) {
            StoreOrder::validateBeforeCheckout($paymentInvoice->storeOrder);
        }

        $vendor = app()->request->post('vendor');
        if (!$vendor) {
            throw new InvalidArgumentException('vendor not defined');
        };

        if ($paymentInvoice->isInvoiceGroup()) {
            $invoice = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($paymentInvoice->invoice_group_uuid, $vendor);
        } else {
            $invoice = $paymentInvoice;
        }

        if (!$invoice || !$invoice->canPaidByMethodType($vendor, $this->getCurrentUser())) {
            throw new UserException(_t('site.store', 'Unable to pay by current payment method'));
        }

        return $this->jsonSuccess([
            'PaymentInvoice' => PaymentInvoiceSerializer::serialize($invoice),
        ]);

    }

    public function actionDownload()
    {
        $response = Yii::$app->getResponse();
        $uuid     = Yii::$app->request->get('uuid');

        $paymentBankInvoice = PaymentBankInvoice::tryFind([
            'uuid' => $uuid
        ]);

        $this->invoiceBankService->checkAccessToInvoice($paymentBankInvoice->paymentInvoice);

        $pdf = $this->invoiceBankService->paymentBankInvoicePdf($paymentBankInvoice);
        $response->setDownloadHeaders('invoice_' . $paymentBankInvoice->uuid . '.pdf', 'application/pdf', false);
        return $response->sendFile($pdf);
    }

    /**
     * @param $invoiceUuid
     *
     * @return PaymentInvoice
     *
     * @throws NotFoundHttpException
     * @throws UserException
     */
    protected function getPaymentInvoiceResolve($invoiceUuid): PaymentInvoice
    {
        $paymentInvoice = PaymentInvoice::tryFind(['uuid' => $invoiceUuid]);

        if (!$paymentInvoice->isAvailableForUser()) {
            throw new BusinessException('Unavailable for current user');
        }

        return $paymentInvoice;
    }

    /**
     * get token from POST request, basedd on vendor type
     *
     * @param string $vendor
     * @return string
     * @throws UserException
     */
    private function getPaymentCheckoutToken($vendor)
    {
        $paymentToken = '';
        if ($vendor == 'braintree') {
            $paymentToken = app('request')->post('payment_method_nonce');
            if (empty($paymentToken)) {
                throw new UserException(_t('front.store', 'Checkout unsuccessful due to incorrect payment method.'));
            }
        } elseif ($vendor == 'stripe') {
            $paymentIntent             = Yii::$app->request->get('payment_intent');
            $paymentIntentClientSecret = Yii::$app->request->get('payment_intent_client_secret');
            if (empty($paymentIntent)) {
                throw new UserException(_t('front.store', 'Checkout unsuccessful due to incorrect stripe payment method.'));
            }
            $paymentToken = [
                'paymentIntent'             => $paymentIntent,
                'paymentIntentClientSecret' => $paymentIntentClientSecret
            ];
        }
        return $paymentToken;
    }

    /**
     * Resolve order and check access for current user
     *
     * @param int $orderId
     * @return StoreOrder
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    protected function resolvePositionOrder($orderId)
    {
        AssertHelper::assert($orderId, 'Order id not set');

        if (!UserFacade::getCurrentUserId()) {
            throw new ForbiddenHttpException(_t('site.order', 'Please login'));
        }

        /** @var StoreOrder $order */
        $order = StoreOrder::findByPk($orderId);

        if (!$order) {
            throw new NotFoundHttpException();
        }

        if (!UserFacade::isObjectOwner($order)) {
            throw new ForbiddenHttpException(_t('site.order', 'Access denied for current order'));
        }
        return $order;
    }
}
