<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 29.01.18
 * Time: 11:37
 */

namespace frontend\controllers\store\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\UserAddress;

class DeliveryAddressSerializer extends AbstractProperties
{
    public function getProperties()
    {
        return [
            UserAddress::class => [
                'id',
                'first_name',
                'last_name',
                'country' => 'country.iso_code',
                'state'   => 'region',
                'city',
                'street'  => 'address',
                'street2' => 'extended_address',
                'zip'     => 'zip_code',
                'comment',
                'phone',
                'email',
                'company'
            ]
        ];
    }
}