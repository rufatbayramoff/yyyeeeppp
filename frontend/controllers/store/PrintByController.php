<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.12.16
 * Time: 17:32
 */

namespace frontend\controllers\store;

use common\components\BaseController;
use common\components\ps\locator\PrinterRepository;
use common\models\Ps;
use common\models\PsPrinter;
use frontend\widgets\PrintByPsPrinterWidget;
use yii\base\Controller;
use yii\base\InvalidParamException;

class PrintByController extends BaseController
{
    public function actionPsPrinter($id)
    {
        $psPrinter = PsPrinter::findByPk($id);
        return $this->renderAdaptive(
            'printByController',
            ['psPrinter' => $psPrinter]
        );
    }

    public function actionPs($id)
    {
        $ps = Ps::findByPk($id);
        if (!$ps->isActive()) {
            throw new InvalidParamException('Ps is not active');
        }
        return $this->renderAdaptive(
            'printByController',
            ['ps' => $ps]
        );
    }
}