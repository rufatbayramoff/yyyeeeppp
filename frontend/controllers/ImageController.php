<?php

namespace frontend\controllers;

use common\components\exceptions\BusinessException;
use common\models\File;
use common\models\repositories\FileRepository;
use frontend\components\image\ImageHtmlHelper;
use yii\base\InvalidParamException;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;

/**
 * Class ImageController
 * @package frontend\controllers
 *
 * @property FileRepository $fileRepository
 */
class ImageController extends \common\components\BaseController
{
    public $fileRepository;

    /**
     * @param FileRepository $fileRepository
     */
    public function injectDependencies(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param $fileUuid
     *
     * @return array
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRotate($fileUuid)
    {
        $params = app()->request->post();

        $direction = $params['direction'] ?? 'left';
        $resizeW = isset($params['resizeW']) ? (int) $params['resizeW'] : false;
        $resizeH = isset($params['resizeH']) ? (int) $params['resizeH'] : false;

        /** @var File $file */
        $file = $this->fileRepository->getByUuid($fileUuid);

        if (!$file) {
            throw new NotFoundHttpException('File not found.');
        }

        $angle = $direction === 'left' ? -90 : 90;

        ImageHtmlHelper::rotateImage($file, $angle);

        $path = $file->getFileUrl();

        if ($resizeW && $resizeH) {
            $path = ImageHtmlHelper::getThumbUrl($path, $resizeW, $resizeH);
        }

        return $this->jsonSuccess([
            'url' => $path
        ]);
    }
}