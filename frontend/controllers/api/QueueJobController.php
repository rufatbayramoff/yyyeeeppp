<?php
namespace frontend\controllers\api;

/**
 * Get job queue information
 *
 * @deprecated
 * @TODO Delete
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class QueueJobController extends \common\components\BaseController
{

    public function actionFile($token)
    {
        $jobInfo = \console\jobs\QueueGateway::getFileJob($token);
        $json = [
            'success' => true,
            'operation' => $jobInfo->operation,
            'token' => $jobInfo->token,
            'status' => $jobInfo->status
        ];
        $json['result'] = \yii\helpers\Json::decode($jobInfo->result);
        
        if ($json['operation'] == 'render') {
            if (! isset($json['result']) and $json['status'] != 'failed') {
                $json['success'] = false;
            }
            $preview = $json['result']['preview'];
            $file3d = $json['result']['file3d'];
            unset($json['result']);
            $json['result'] = [
                'preview' => $preview,
                'file3d' => $file3d
            ];
        }
        if ($json['status'] == 'failed') {
            $json['result'] = [
                'status' => 'failed'
            ];
        }
        return $this->jsonReturn($json);
    }
}