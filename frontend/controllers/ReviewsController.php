<?php
/**
 * User: nabi
 */

namespace frontend\controllers;


use common\components\BaseController;
use common\components\exceptions\AssertHelper;
use common\components\serizaliators\porperties\ReviewShareSerializer;
use common\components\serizaliators\Serializer;
use common\models\factories\ReviewShareFactory;
use common\models\StoreOrderReview;
use common\models\StoreOrderReviewShare;
use common\traits\TransactedControllerTrait;
use frontend\models\ps\PsFacade;
use frontend\models\review\ReviewShareForm;
use yii\web\NotFoundHttpException;

class ReviewsController extends BaseController
{
    use TransactedControllerTrait;

    /**
     * @var ReviewShareFactory
     */
    private $shareFactory;

    /**
     * @var Serializer
     */
    private $shareSerializer;


    public function actionAll()
    {
        return $this->render('reviews.php');
    }


    /**
     * @param ReviewShareFactory $shareFactory
     */
    public function injectDependencies(ReviewShareFactory $shareFactory) : void
    {
        $this->shareFactory = $shareFactory;
        $user = $this->getCurrentUser();
        $this->shareSerializer = new Serializer(new ReviewShareSerializer($user));
    }

    /**
     * Return data for view and create share.
     *
     * @param int $reviewId
     * @return array
     */
    public function actionShare(int $reviewId) : array
    {
        $review =  $this->loadReview($reviewId);

        $user = $this->getCurrentUser();
        $share = $this->shareFactory->create($review, $user);
        return $this->jsonReturn($this->shareSerializer->serialize($share));
    }

    /**
     * Create new share.
     *
     * @param int $reviewId
     * @return array
     *
     * @transacted
     */
    public function actionCreateShare(int $reviewId) : array
    {
        $user = $this->getCurrentUser();
        $review = $this->loadReview($reviewId);

        $share = $this->shareFactory->create($review, $user);

        $form = new ReviewShareForm($share, $user);

        $form->load(\Yii::$app->request->getBodyParams());
        AssertHelper::assertValidate($form);

        $form->process();

        return $this->shareSerializer->serialize($share);
    }

    /**
     * Share page.
     *
     * @param int $shareId
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewShare(int $shareId) : string
    {
        $share = $this->loadShare($shareId);

        if (!$share->isCanShow()) {
            $this->redirect(PsFacade::getPsReviewsRoute($share->review->ps));
        }

        return $this->render('share', ['share' => $share]);
    }

    /**
     * Load review for share purpose.
     *
     * @param int $reviewId
     * @return StoreOrderReview
     */
    private function loadReview(int $reviewId) : StoreOrderReview
    {
        $review = StoreOrderReview::findByPk($reviewId);
        if (!$review || !in_array($review->status, [StoreOrderReview::STATUS_NEW, StoreOrderReview::STATUS_MODERATED])) {
            throw new NotFoundHttpException(_t('share', 'Review not found or can\'t be shared.'));
        }
        return $review;
    }

    /**
     * @param int $shareId
     * @return StoreOrderReviewShare
     * @throws NotFoundHttpException
     */
    private function loadShare(int $shareId): StoreOrderReviewShare
    {
        $share = StoreOrderReviewShare::tryFind($shareId);
        if (!$share) {
            throw new NotFoundHttpException(_t('share', 'Share not found or can\'t be showed.'));
        }
        return $share;
    }
}