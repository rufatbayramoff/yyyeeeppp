<?php

namespace frontend\controllers;

use common\components\exceptions\AssertHelper;
use common\components\hiredesigner\HireDesignerRepository;
use common\models\base\SeoPage;
use common\models\BlogPost;
use common\modules\captcha\services\CaptchaService;
use common\modules\catalogPs\repositories\PrinterMaterialRepository;
use common\modules\catalogPs\repositories\PrinterTechnologyRepository;
use common\modules\catalogPs\repositories\UsageGroupsRepository;
use common\modules\homePage\repositories\HomePageProductRepository;
use common\modules\model3dRender\actions\ActionDownloadModel3d;
use common\modules\share\actions\ShareEmailAction;
use common\modules\translation\actions\ActionSetTranslateMode;
use common\modules\translation\actions\ActionTranslateDialog;
use common\modules\translation\actions\ActionTranslatePage;
use common\modules\translation\actions\ActionTranslateSave;
use Da\QrCode\Action\QrCodeAction;
use Da\QrCode\Contracts\ErrorCorrectionLevelInterface;
use Da\QrCode\Format\BookMarkFormat;
use Da\QrCode\Label;
use Da\QrCode\QrCode;
use frontend\assets\AppAsset;
use frontend\components\StaticPageAction;
use frontend\components\UserSessionFacade;
use frontend\controllers\actions\DownloadFileAction;
use frontend\models\site\ContactForm;
use frontend\models\site\HireDesignerForm;
use frontend\models\user\UserFacade;
use frontend\widgets\Search;
use frontend\widgets\TopFindServiceWidget;
use yii;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends \common\components\BaseController
{

    /**
     * External actions
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error'              => [
                'class' => yii\web\ErrorAction::class,
            ],
            'set-translate-mode' => [
                'class' => ActionSetTranslateMode::class,
            ],
            'translate-dialog'   => [
                'class' => ActionTranslateDialog::class,
            ],
            'translate-page'     => [
                'class' => ActionTranslatePage::class,
            ],
            'translate-save'     => [
                'class' => ActionTranslateSave::class,
            ],

            'download-model3d' => [
                'class' => ActionDownloadModel3d::class,
            ],
            'terms'            => [
                'class' => StaticPageAction::class,
            ],
            'about'            => [
                'class' => StaticPageAction::class,
            ],
            'return-policy'    => [
                'class' => StaticPageAction::class,
            ],
            'policy'           => [
                'class' => StaticPageAction::class,
            ],
            'download-file'    => [
                'class' => DownloadFileAction::class,
            ],
            'sendemail'        => [
                'class' => ShareEmailAction::class
            ],
            'qr' => [
                'class' => QrCodeAction::class,
                'param' => 'v'
            ]
        ];
    }

    protected $captchaService;

    /**
     * @param CaptchaService $captchaService
     */
    public function injectDependencies(CaptchaService $captchaService): void
    {
        $this->captchaService = $captchaService;
    }

    public function actionQr2($title, $link, $logo = null)
    {
        $format = new BookMarkFormat(['title' => $title, 'url' => $link]);

        /*
         $qrCode = new QrCode($format);

         header('Content-Type: ' . $qrCode->getContentType());

         echo $qrCode->writeString();
         exit;*/

        $label = (new Label($title))
            ->updateFontSize(24);

        $qrCode = (new QrCode($link))
            //->useLogo(__DIR__ . '/data/logo.png')
            //->useForegroundColor(51, 153, 255)
            //->useBackgroundColor(200, 220, 210)
            ->useEncoding('UTF-8')
            ->setErrorCorrectionLevel(ErrorCorrectionLevelInterface::HIGH)
            ->setLogoWidth(100)
            ->setSize(300)
            ->setMargin(5)
            ->setLabel($label);

        if (!empty($logo)) {
            $pathInfo = pathinfo($logo);
            $context  = stream_context_create(
                [
                    'ssl' => [
                        'verify_peer'      => false,
                        'verify_peer_name' => false,
                    ]
                ]
            );
            if (empty($pathInfo['extension'])) {
                die('Logo extension not found. Please specify URL with extension');
            }
            $logoExtension = strtolower($pathInfo['extension']);
            if (!in_array($logoExtension, ['png', 'gif', 'jpg'])) {
                die('Not valid logo extension');
            }

            $md5     = md5($logo);
            $tmpPath = Yii::getAlias('@runtime') . '/qrlogo_' . $md5 . '.' . $logoExtension;
            $content = file_get_contents($logo, false, $context);
            file_put_contents($tmpPath, $content);
            $qrCode = $qrCode->useLogo($tmpPath);
        }
        header('Content-Type: ' . $qrCode->getContentType());

        echo $qrCode->writeString();
        exit;

    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'translateSave' => ['post'],
                ]
            ]
        ];
    }

    public function actionDown()
    {
        if ($this->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $this->jsonError(_t('site.common', 'Treatstock is temporarily down for scheduled maintenance.'), 406);
        }
        header('HTTP/1.1 503 Service Temporarily Unavailable');
        header('Status: 503 Service Temporarily Unavailable');
        header('Retry-After: 30');//30 seconds

        AppAsset::register($this->view);
        $this->layout = 'plain';
        return $this->render('site_down.php');
    }

    /**
     * @return string
     */
    public function actionUi()
    {
        return $this->render('ui-kit.php');
    }

    public function actionHow()
    {
        return $this->redirect('/how-it-works', 301);
    }

    /**
     * @return string
     */
    public function actionHowItWorks()
    {
        return $this->render('how_works');
    }


    /**
     * accept cookie
     */
    public function actionAcceptCookie()
    {
        Yii::$app->response->getCookies()->add(
            new Cookie(
                [
                    'name'   => 'accepted_cookie',
                    'value'  => 1,
                    'expire' => time() + 86400 * 365
                ]
            )
        );
        return $this->jsonSuccess();
    }


    /**
     * @return string
     */
    public function actionIndex()
    {
        $materialRepo         = Yii::createObject(PrinterMaterialRepository::class);
        $technologyRepository = Yii::createObject(PrinterTechnologyRepository::class);
        $usageRepo            = Yii::createObject(UsageGroupsRepository::class);

        $catIds = \Yii::$app->setting->get('mainpage.showcategories', '1,2,3');

        $featuredLocations = \Yii::$app->setting->get(
            'settings.psCatalogFeaturedLocations',
            [
                "Los Angeles, CA, US",
                "San Francisco, CA, US",
                "New York, NY, US",
                "Philadelphia, PA, US",
                "London, UK",
                "Sydney, AU",
                "Boston, MA, US",
                "Detroit, MI, US",
                "Vancouver, BC, CA ",
                "Rome, IT",
                "Paris, FR",
                "Chicago, IL, US",
                "Newark, NJ, US",
                "Beijing, CN",
                "Amsterdam, NL",
                "Atlanta, GA, US",
                "San Diego, US",
                "Portland, OR, US"
            ]
        );

        $materialsCodes    = \Yii::$app->setting->get('settings.psCatalogMaterials', ["PLA", "ABS", "Nylon", "Resin", "Rubber (TPU)"]);
        $technologiesCodes = \Yii::$app->setting->get('settings.psCatalogTechnologies', ["FDM", "CJP", "MJM", "SLA", "SLS"]);
        $materials         = $technologies = [];

        foreach ($materialsCodes as $materialCode) {
            $materials[] = $materialRepo->getByCode($materialCode);
        }
        $materials = array_filter($materials);
        foreach ($technologiesCodes as $techCode) {
            $technologies[] = $technologyRepository->getByCode($techCode);
        }
        $technologies = array_filter($technologies);

        $usageTypes = [];
        foreach ($usageRepo->getAll() as $usageType):
            $usageTypes[$usageType['id']] = $usageType['title'];
        endforeach;
        $locationString = UserSessionFacade::getLocationAsString();

        $productCategoriesBlock = HomePageProductRepository::getActiveCategories();
        $productCategoryCard    = HomePageProductRepository::getActiveCardsCategories();
        $featuredCategories     = HomePageProductRepository::getActiveFeaturedCategories();
        $blogPosts              = BlogPost::getLatest(3);

        return $this->render(
            'index',
            [
                'cacheId'                => md5(
                    md5(implode(",", $technologiesCodes)) .
                    md5(implode(",", $materialsCodes)) .
                    md5(implode(",", $featuredLocations)) . Yii::$app->language . $locationString
                ),
                'featuredLocations'      => array_splice($featuredLocations, 0, 7),
                'materials'              => $materials,
                'technologies'           => $technologies,
                'usageTypes'             => $usageTypes,
                'productCategoriesBlock' => $productCategoriesBlock,
                'productCategoryCard'    => $productCategoryCard,
                'featuredCategories'     => $featuredCategories,
                'blogPosts'              => $blogPosts
            ]
        );
    }

    /**
     * @return string|yii\web\Response
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->captchaService->checkGoogleRecaptcha2Response();
            if ($model->sendEmail(param('noreplayEmail', 'no-replay@treatstock.com'))) {
                Yii::$app->session->setFlash('success', _t('front.site', 'Thank you for contacting us. We will respond as soon as possible.'));
            } else {
                Yii::$app->session->setFlash('error', _t('front.site', 'There was an error sending email.'));
            }
            return $this->refresh();
        }
        return $this->render(
            'contact',
            [
                'model' => $model
            ]
        );
    }

    /**
     * @param bool $alias
     * @return string|yii\web\Response
     */
    public function actionHelp($alias = false)
    {
        if (!$alias) {
            return $this->redirect(['help/index'], 301);
        }
        $currentLang = \frontend\components\UserSessionFacade::getLanguage();
        $help        = \common\models\SiteHelp::findOne(
            [
                'alias'     => $alias,
                'is_active' => 1
            ]
        );
        if (empty($help)) {
            $this->view->title = _t('site.help', 'Help not found') . ' - Treatstock';
            return $this->renderAdaptive(
                'help_notfound',
                [
                    'title'   => _t('site.help', 'Help not found'),
                    'content' => _t(
                        'site.help',
                        'Help is not yet ready for this context. <br /> <br /><small>Requested help alias: {link}</small>',
                        [
                            'link' => $alias
                        ]
                    )
                ]
            );
        }
        $this->view->title = $help->title . ' - Treatstock';
        $help->updateClicks();
        return $this->renderAdaptive(
            'help',
            [
                'content' => $help->content,
                'title'   => $help->title
            ]
        );
    }

    /**
     * Hire designer form
     */
    public function actionHireDesigner()
    {
        $model              = new HireDesignerForm();
        $model->agreePolicy = 1;

        if (Yii::$app->request->isPost) {

            $model->load(Yii::$app->request->post());
            $model->files = UploadedFile::getInstances($model, 'files');

            if ($model->validate()) {

                $mailer = \Yii::$app->mailer;

                $subject = "Hire Designer Request";

                $message = $mailer
                    ->compose(
                        ['html' => 'blankEmailHtml', 'text' => 'blankEmail'],
                        [
                            'content'     => $model->asString(false),
                            'subject'     => $subject,
                            'contentHtml' => $model->asString(true)
                        ]
                    )
                    ->setFrom(\Yii::$app->params['supportEmail'])
                    ->setTo(param('adminEmail'))
                    ->setSubject($subject);

                foreach ($model->files as $file) {
                    $message->attach($file->tempName, ['fileName' => $file->name]);
                }

                AssertHelper::assert($mailer->send($message), 'Hire designer form cant send');
                return $this->render('hireDesignerSuccess');
            }
        } else {
            if (!$model->email && !is_guest()) {
                $model->email = UserFacade::getCurrentUser()->email;
            }
        }

        $hireDesigner = (new HireDesignerRepository())->load();
        return $this->render('hireDesigner', ['model' => $model, 'hireDesigner' => $hireDesigner]);
    }

    /**
     * Autocomplete for explore search
     *
     * @param string $query
     * @return string[];
     */
    public function actionExploreAutocomplete($query, $category='')
    {
        $data = $query ? TopFindServiceWidget::getRelatedList($query, $category) : [];
        return $this->jsonReturn($data);
    }

    /**
     * seo page
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSeoPage()
    {
        if (!empty($this->seo) && $this->seo->url == app('request')->pathInfo) {
            $pageTitle   = $this->seo->header;
            $pageContent = $this->seo->header_text;
        } else {
            // try to find with '/'
            $seo = SeoPage::findOne(['url' => app('request')->pathInfo . '/']);
            if ($seo) {
                return $this->redirect(app('request')->pathInfo . '/', 301);
            }
            throw new NotFoundHttpException(_t('site', 'Page not found'));
        }
        return $this->render('page', ['pageTitle' => $pageTitle, 'pageContent' => $pageContent]);
    }
}
