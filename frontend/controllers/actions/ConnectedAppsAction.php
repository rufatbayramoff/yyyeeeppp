<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.01.18
 * Time: 13:38
 */

namespace frontend\controllers\actions;

use backend\models\search\ConnectedAppSearch;
use common\components\BaseController;
use yii\base\Action;

/**
 * Class ConnectedAppsAction
 *
 * @package frontend\controllers\actions
 * @property BaseController $controller
 */
class ConnectedAppsAction extends Action
{
    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function appList()
    {
        $connectedApp = \Yii::createObject(ConnectedAppSearch::class);
        $provider = $connectedApp->search([]);
        return $this->controller->renderAdaptive(
            'index',
            [
                'provider' => $provider
            ]
        );
    }

    /**
     * @param $code
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function viewApp($code)
    {
        $connectedApp = ConnectedAppSearch::tryFind(['code' => $code]);
        return $this->controller->renderAdaptive(
            'view',
            [
                'connectedApp' => $connectedApp
            ]
        );
    }

    /**
     * @param null $code
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($code = null)
    {
        $this->controller->setViewPath('@frontend/views/connectedApps');
        if ($code) {
            return $this->viewApp($code);
        }
        return $this->appList();
    }
}