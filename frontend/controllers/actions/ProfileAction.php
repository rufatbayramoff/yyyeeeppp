<?php


namespace frontend\controllers\actions;


use common\models\Company;
use common\models\Model3dViewedState;
use common\models\PsPrinter;
use common\models\repositories\CompanyRepository;
use common\models\repositories\FileRepository;
use common\models\SeoPage;
use common\models\user\UserIdentityProvider;
use common\services\Model3dService;
use frontend\controllers\c\PublicController;
use frontend\models\user\CompanyPublicPageEntity;
use Yii;
use yii\base\Action;

/**
 * @property PublicController $controller
 * */
class ProfileAction extends Action
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;
    private FileRepository $fileRepository;

    /** @var UserIdentityProvider */
    protected $userIdentityProvider;

    public function __construct(
        $id,
        $controller,
        CompanyRepository $companyRepository,
        UserIdentityProvider $userIdentityProvider,
        FileRepository $fileRepository,
        $config = []
    )
    {
        parent::__construct($id, $controller, $config);
        $this->companyRepository = $companyRepository;
        $this->fileRepository = $fileRepository;
        $this->userIdentityProvider = $userIdentityProvider;
    }


    public function run(string $username)
    {
        $company = $this->companyRepository->getPublicByName($username, $this->userIdentityProvider->getUser());
        $user = $company->user;
        $about = $user->userProfile->info_about . '. ' . $user->userProfile->info_skills;
        if (!empty($user->userProfile->info_about)) {
            $this->controller->view->params['meta_description'] = $about;
        } else {
            $this->controller->view->params['meta_description'] = _t('site.seo', 'View {psTitle} on Treatstock.', ['psTitle' => \H($company->title)]);
        }

        $model3dViewedState = Model3dViewedState::find()
            ->where(['id' => (int)Yii::$app->request->get('model3dViewedStateId')])
            ->one();
        /** @var Model3dService $model3dService */
        $model3dService = Yii::createObject(Model3dService::class);
        if ($model3dViewedState && !$model3dService->isAvailableForCurrentUser($model3dViewedState->model3d)) {
            $model3dViewedState = null;
        }
        /** @var PsPrinter $selectedPrinter */
        $selectedPrinter = PsPrinter::find()
            ->where(['id' => (int)Yii::$app->request->get('selectedPrinterId')])
            ->one();
        if ($selectedPrinter && ($selectedPrinter->companyService->ps_id !== $company->id)) {
            throw new  \yii\base\InvalidArgumentException('Invalid selected printer');
        }

        if (empty($this->seo)) {
            $seoPage = new SeoPage();
            $seoPage->title = _t('site.public', 'About {ps} - Treatstock', ['ps' => $company->title]);
            $seoPage->meta_keywords = _t('site.public', '{ps}, reviews, work examples, 3d printers, cnc, 3d models, 3d design', ['ps' => $company->title]);
            $seoPage->meta_description = _t('site.public', '{psDescription} - about {ps}', ['psDescription' => $company->description, 'ps' => $company->title]);
            $this->controller->seo = $seoPage;
        }
        if ($company->user->getIsSystem()) {
            $tpl = 'support';
        } else {
            $tpl = 'about';
        }
        return $this->controller->render($tpl, [
            'ps'                      => $company,
            'companyPublicPageEntity' => CompanyPublicPageEntity::fill($company),
            'selectedPrinter'         => $selectedPrinter,
            'model3dViewedState'      => $model3dViewedState,
            'lastReviewFiles'         => $this->fileRepository->lastReviewFiles($company)
        ]);
    }
}