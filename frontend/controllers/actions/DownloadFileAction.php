<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.03.18
 * Time: 15:20
 */

namespace frontend\controllers\actions;

use common\components\FileTypesHelper;
use common\models\FileDownloadHash;
use DateTime;
use DateTimeZone;
use frontend\components\image\ImageHtmlHelper;
use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DownloadFileAction extends Action
{
    /**
     * @param $fileHash
     * @param bool $image
     *
     * @throws NotFoundHttpException
     */
    public function run($fileHash, $image = false)
    {
        $currentDate = new DateTime('now', new DateTimeZone('UTC'));
        $currentDate = $currentDate->format('Y-m-d H:i:s');
        $fileHash = FileDownloadHash::findByHash($fileHash);
        if (!$fileHash) {
            throw  new NotFoundHttpException('File not found');
        }
        $file = $fileHash->file;
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_RAW;

        if (($imgWeight = Yii::$app->request->get('w')) && ($imgHeight = Yii::$app->request->get('h'))) {
            $path = ImageHtmlHelper::getThumbPathForFile($file, $imgWeight, $imgHeight);
        } else {
            $path = $file->getLocalTmpFilePath();
        }

        $isInline = false;
        if (FileTypesHelper::isImage($file)) {
            $isInline = true;
        }
        $file->getFileExtension();

        Yii::$app->response->getHeaders()->add('X-Robots-Tag', 'noindex, nofollow');
        if ($image) {
            $type = 'image/' . $file->extension;
            header('Content-Type:' . $type);
            header('Content-Length: ' . $file->size);
            readfile($path);
        } else {
            $response->sendFile($path, $file->getFileName(), ['inline' => $isInline]);
        }
    }
}