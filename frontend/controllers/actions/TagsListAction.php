<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.04.18
 * Time: 11:56
 */

namespace frontend\controllers\actions;

use yii\base\Action;

class TagsListAction extends Action
{
    public function run()
    {
        $q = \Yii::$app->getRequest()->get('q', '');
        $res = \common\models\SiteTag::find()->select('id, text')->where(
            [
                'status' => [
                    'active',
                    'new'
                ]
            ]
        );
        if (!empty($q)) {
            // ['like', 'name', '%tester', false]
            $res = $res->andWhere(
                [
                    'like',
                    'text',
                    $q . '%',
                    false
                ]
            );
        }
        $tags = $res->asArray()->all();
        if (empty($tags)) {
            $tagsAll = explode(",", $q);

            $tags = [];
            foreach($tagsAll as $k1=>$v1){
                if(strlen(trim($v1)) < 2){
                    continue;
                }
                $tags[] = [
                    'id'   => $v1,
                    'text' => trim($v1)
                ];
            };
        } else {
            foreach ($tags as $key => $tagInfo) {
                if(strlen(trim($tagInfo['text'])) < 2){
                    continue;
                }
                $tags[$key]['id'] = trim($tagInfo['text']);
            }
        }
        return $this->controller->jsonReturn(
            [
                'results' => $tags
            ]
        );
    }
}