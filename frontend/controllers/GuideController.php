<?php
/**
 * @author Dmitry Strukov <d.strukov@treatstock.com>
 */

namespace frontend\controllers;


use common\components\ArrayHelper;
use common\models\SiteHelp;
use common\models\SiteHelpCategory;
use frontend\views\guide\HelpUrlStrategy;
use yii\data\ActiveDataProvider;

class GuideController extends \common\components\BaseController
{
    /**
     * @var HelpUrlStrategy
     */
    private $urlStrategy;

    /**
     * @var SiteHelpCategory
     */
    private $topCategory;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->urlStrategy = new HelpUrlStrategy('/guide');
        $this->topCategory = SiteHelpCategory::findByPk(\Yii::$app->setting->get('settings.guides_category_id'));
    }

    /**
     *
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        return $this->render('index', ['topCategory' => $this->topCategory, 'urlStrategy' => $this->urlStrategy]);
    }

    /**
     * @param bool $q
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function actionSearch($q = false)
    {
        if(empty($q)){
            return $this->redirect(['index']);
        }
        $dataProvider = $this->getSphinxSearch('help', $q);
        if($dataProvider->getCount()===0){
            $dataProvider = $this->getSphinxSearch('helpcategory', $q);
        }
        return $this->render('search', [
            'dataProvider' => $dataProvider,
            'q' => $q,
            'urlStrategy' => $this->urlStrategy
        ]);
    }

    /**
     * show category
     *
     * @param string $slug
     * @return string
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionCategory($slug)
    {
        $category = SiteHelpCategory::tryFind(['slug' => $slug]);

        return $this->render('category', [
            'category' => $category,
            'urlStrategy' => $this->urlStrategy,
            'topCategory' => $this->topCategory,
        ]);
    }

    /**
     * show article
     *
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionArticle($id)
    {
        $article = SiteHelp::tryFind(['id' => $id, 'is_active' => 1]);
        
        return $this->render('article', [
            'article' => $article,
            'urlStrategy' => $this->urlStrategy,
            'topCategory' => $this->topCategory
        ]);
    }


    public function actionTag($tag)
    {
        /** @var int[] $subCategoriesIds */
        $subCategoriesIds = ArrayHelper::getColumn($this->topCategory->siteHelpCategories, 'id');

        $query = SiteHelp::find()
            ->forTag($tag)
            ->active()
            ->forCategoriesIds($subCategoriesIds)
            ->orderByProprity();

        $dataProvider= new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'defaultPageSize' => 10000,
                'pageSizeLimit'   => 10000
            ]
        ]);

        return $this->render('tag', [
            'dataProvider' => $dataProvider,
            'tag' => $tag,
            'urlStrategy' => $this->urlStrategy,
            'topCategory' => $this->topCategory
        ]);
    }

    /**
     * @param $from
     * @param $q
     * @return \yii\data\ActiveDataProvider
     */
    private function getSphinxSearch($from, $q)
    {
        $querySphinx = (new \yii\sphinx\Query())->from($from)->showMeta(true);
        $querySphinx->match($q);

        $dataProviderSphinx = new \yii\sphinx\ActiveDataProvider([
            'query'      => $querySphinx,
            'pagination' => [
                'defaultPageSize' => 30,
                'pageSizeLimit'   => 100
            ]
        ]);
        $recordIds = ArrayHelper::getColumn($dataProviderSphinx->getModels(), 'id');

        if($from=='help'){
            $dataProvider = SiteHelp::getDataProvider(['id'=>$recordIds, 'is_active'=>1], 100);
        }else if($from=='helpcategory'){
            $dataProvider = SiteHelpCategory::getDataProvider(['id'=>$recordIds], 100);
        }
        return $dataProvider;
    }




}