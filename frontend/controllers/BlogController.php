<?php
/**
 * Date: 07.07.16
 *
 * @author Nabi Ibatulin <n.ibatulin@treatstock.com>
 */

namespace frontend\controllers;

use Yii;
use common\components\BaseController;
use common\models\BlogPost;
use yii\data\ActiveDataProvider;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use Zelenin\Feed;
use Zelenin\yii\extensions\Rss\RssView;

/**
 * Class BlogController
 * @package frontend\controllers
 */
class BlogController extends BaseController
{
    /**
     * List of blog posts
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BlogPost::find()->active()->orderBy('date DESC'),
            'pagination' => [
                'pageSize' => 12,
            ],
        ]);
        $this->view->title = _t('site.blog', 'Treatstock Blog');
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionRss()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => BlogPost::find()->active()->orderBy('date DESC'),
            'pagination' => [
                'pageSize' => 12,
            ],
        ]);

        $response = \Yii::$app->getResponse();
        $headers = $response->getHeaders();

        $headers->set('Content-Type', 'application/rss+xml; charset=utf-8');

        return RssView::widget([
            'dataProvider' => $dataProvider,
            'channel' => [
                'title' => function ($widget, Feed $feed) {
                    $feed->addChannelTitle(Yii::$app->name);
                },
                'link' => Url::toRoute('/blog', true),
                'description' => _t('site.blog', 'Treatstock Blog'),
                'language' => function ($widget, Feed $feed) {
                    return Yii::$app->language;
                },
            ],
            'items' => [
                'title' => function ($model, $widget, Feed $feed) {
                    return $model->title;
                },
                'description' => function ($model, $widget, Feed $feed) {
                    return StringHelper::truncateWords($model->content, 50);
                },
                'link' => function ($model, $widget, Feed $feed) {
                    return Url::toRoute(['/blog/view', 'alias' => $model->alias], true);
                },

                'guid' => function ($model, $widget, Feed $feed) {
                    $date = \DateTime::createFromFormat('Y-m-d', $model->date);
                    return Url::toRoute(['/blog/view', 'alias' => $model->alias], true) . ' ' . $date->format(DATE_RSS);
                },
                'pubDate' => function ($model, $widget, Feed $feed) {
                    $date = \DateTime::createFromFormat('Y-m-d', $model->date);
                    return $date->format(DATE_RSS);
                }
            ]
        ]);
    }
    /**
     * View post
     * @param string $alias Alias of post
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias)
    {
        $post = BlogPost::findOne(['alias' => $alias]);
        if(!$post){
            throw new NotFoundHttpException(_t('site.blog', 'Post not found'));
        }
        $this->view->title = $post->title . ' - Treatstock Blog';
        return $this->render('view', ['post' => $post]);
    }
}