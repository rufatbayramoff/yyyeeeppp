<?php
namespace frontend\controllers\c;

use common\components\BaseController;
use common\models\repositories\CompanyRepository;

class WidgetController extends BaseController
{
    public $viewPath = '@frontend/views/widget';

    /** @var CompanyRepository */
    protected $companyRepository;

    public function injectDependencies(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function actionRate(string $username)
    {
        $company = $this->companyRepository->getPublicByName($username, $this->getCurrentUser());
        return $this->renderAjax('rate', ['company' => $company]);
    }

    public function actionRateWide(string $username)
    {
        $company = $this->companyRepository->getPublicByName($username, $this->getCurrentUser());
        return $this->renderAjax('rate-wide', ['company' => $company]);
    }

}
