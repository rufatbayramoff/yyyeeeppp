<?php
/**
 * User: nabi
 */

namespace frontend\controllers\c;


use common\models\Model3dReplica;
use common\models\Model3dViewedState;
use common\models\PsPrinter;
use common\models\SeoPageAutofillTemplate;
use common\modules\catalogPs\models\CatalogSearchForm;
use common\modules\catalogPs\repositories\criterias\FilterPsCriteria;
use common\modules\catalogPs\repositories\PrinterMaterialRepository;
use common\modules\catalogPs\repositories\PrinterTechnologyRepository;
use common\modules\catalogPs\repositories\PsPrinterEntityRepository;
use common\modules\seo\factories\SeoPageFactory;
use common\modules\seo\placeholders\PrintServicePlaceholder;
use common\services\Model3dService;
use common\services\Model3dViewedStateService;
use frontend\models\model3d\Model3dItemForm;
use Yii;
use yii\base\Action;
use yii\web\HttpException;

/**
 * Class PrintServicesAction
 *
 * @property PublicController $controller
 * @package frontend\controllers\c
 */
class PrintServicesAction extends Action
{

    /**
     * @var CatalogSearchForm
     */
    public $searchForm;
    /**
     * @var PrinterMaterialRepository
     */
    public $materialRepo;

    /**
     * @var PrinterTechnologyRepository
     */
    public $technologyRepository;


    public function injectDependencies(CatalogSearchForm $searchForm, PrinterMaterialRepository $materialRepo, PrinterTechnologyRepository $technologyRepository)
    {
        $this->searchForm = $searchForm;
        $this->materialRepo = $materialRepo;
        $this->technologyRepository = $technologyRepository;
    }

    /**
     *
     * @param $username
     * @return string
     */
    public function run($username, $withTop = true)
    {
        try {
            $this->controller->initPs($username);
        } catch (HttpException $e) {
            if ($e->statusCode == 404) {
                return $this->controller->render(
                    '3dprinting/404.php'
                );
            }
            throw $e;
        }

        $this->searchForm->groupByPs = false;
        $this->searchForm->withMeta = true;
        $this->searchForm->psLinks = [$username];

        $model3dViewedState = Model3dViewedState::find()
            ->where(['id' => (int)Yii::$app->request->get('model3dViewedStateId')])
            ->one();
        /** @var Model3dService $model3dService */
        $model3dService = Yii::createObject(Model3dService::class);
        if ($model3dViewedState && !$model3dService->isAvailableForCurrentUser($model3dViewedState->model3d)) {
            $model3dViewedState = null;
        }
        if (app('request')->get('model3dReplicaId')) {
            $replicaId = app('request')->get('model3dReplicaId');
            $model3dReplica = Model3dReplica::findByPk($replicaId);
            if ($model3dReplica) {
                $model3dForm = Model3dItemForm::create($model3dReplica);

                /** @var Model3dViewedStateService $model3dViewedStateService */
                $model3dViewedStateService = Yii::createObject(Model3dViewedStateService::class);
                $res = $model3dViewedStateService->registerModel3dViewForm($model3dForm);
                if ($res) {
                    $model3dViewedState = Model3dViewedState::find()
                        ->where(['id' => $res->id])
                        ->one();
                }
            }
        }
        $selectedPrinter = PsPrinter::findByPk(Yii::$app->request->get('selectedPrinterId'));
        if (!$selectedPrinter || !$selectedPrinter->ps || !$this->controller->ps || ($selectedPrinter->ps->id != $this->controller->ps->id)) {
            $selectedPrinter = null;
            $model3dViewedState = null;
        }

        /* @var $psPrintersRepo PsPrinterEntityRepository */
        $psPrintersRepo = \Yii::createObject(
            [
                'class' => PsPrinterEntityRepository::class,
                'sort'  => $this->searchForm->sort,
                'criteria' => new FilterPsCriteria($this->controller->ps)
            ]
        );
        $dataProviderPrinters = $this->searchForm->getDataProvider($psPrintersRepo);

        if (empty($this->controller->seo)) {
            $psPlaceholder = new PrintServicePlaceholder();
            $psPlaceholder->setDataObject($this->controller->ps);
            $seoAutofill = SeoPageAutofillTemplate::findOne(['type' => SeoPageAutofillTemplate::TYPE_PS, 'is_default' => 1, 'lang_iso' => app()->language]);
            if ($seoAutofill) {
                $seoPage = SeoPageFactory::createBySeoTemplate(
                    $seoAutofill,
                    $psPlaceholder->getFilledPlaceholders()
                );
                $this->controller->seo = $seoPage;
            }
        }
        $viewTpl = '3dprinting/3dprinting.php';
        if (!$withTop) {
            $this->controller->layout = 'empty';
            $viewTpl = 'services/3dprinting_inline.php';
        }
        return $this->controller->render(
            $viewTpl,
            [
                'withTop'                 => $withTop,
                'ps'                      => $this->controller->ps,
                'companyPublicPageEntity' => $this->controller->companyPublicPageEntity,
                'searchForm'              => $this->searchForm,
                'dataProvider'            => $dataProviderPrinters,
                'model3dViewedState'      => $model3dViewedState,
                'selectedPrinter'         => $selectedPrinter,
            ]
        );
    }
}