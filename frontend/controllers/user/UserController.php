<?php

namespace frontend\controllers\user;

use common\components\exceptions\BusinessException;
use common\models\Preorder;
use common\models\User;
use common\models\user\ChangeEmailException;
use common\models\user\ChangeEmailRequest;
use common\models\UserEmailLogin;
use common\models\UserLike;
use common\models\UserLog;
use common\models\UserOsn;
use common\modules\company\services\CompanyVerifyService;
use common\modules\promocode\components\PromocodeApplyService;
use common\services\LikeService;
use common\services\UserEmailLoginService;
use frontend\models\osn\OsnGateway;
use frontend\models\ps\PsFacade;
use frontend\models\user\FrontUser;
use frontend\models\user\LoginForm;
use frontend\modules\preorder\components\PreorderService;
use Yii;
use \frontend\models\user\UserFacade;
use yii\authclient\AuthAction;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * signup, login, email, logout, resend email actions
 */
class UserController extends \common\components\BaseController
{

    protected $viewPath = '@frontend/views/user/';

    /**
     * @var PreorderService
     */
    private $preorderService;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $needsLogin = [
            'logout',
            'profile',
            'profile-update',
            'change-password',
            'osn'
        ];
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only'  => $needsLogin,
                'rules' => [
                    [
                        'actions' => $needsLogin,
                        'allow'   => true,
                        'roles'   => [
                            '@'
                        ]
                    ]
                ]
            ],
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'like'   => [
                        'post'
                    ],
                    'unlike' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     * @param PreorderService $preorderService
     */
    public function injectDependencies(PreorderService $preorderService)
    {
        $this->preorderService = $preorderService;
    }

    /**
     * default action to view user profile
     *
     * @return Response
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        return $this->redirect('@web/user-profile');
    }

    /**
     * @param $key
     * @throws NotFoundHttpException
     * @throws \yii\base\UserException
     */
    public function actionLoginAs($key)
    {
        sleep(5);
        if (Yii::$app->cache->get('loginAsKey')) {
            $loginKey = Yii::$app->cache->get('loginAsKey');
            if ($loginKey['uuid'] === $key) {
                $user = FrontUser::tryFindByPk($loginKey['userId']);
                if ($user->status === \common\models\User::STATUS_DELETED) {
                    throw new \yii\base\UserException('Your account is not active. If you think this is an error, please contact us.');
                }
                if ($user->company && $user->company->is_backend) {
                    // Allow if backend
                } else {
                    if (!param('allow_loginas_any_user')) {
                        return 'Allow login as any user!';
                    }
                }
                echo 'Login as user ' . $user->id;
                \Yii::warning('Login as user ' . $user->id);
                Yii::$app->user->login($user, 600);
                return $this->redirect('/');
            };
        }
        throw new ForbiddenHttpException('Invalid login as key');
    }

    public function beforeAction($action)
    {
        if (Yii::$app->request->get('isExternalWidget')) {
            $this->layout                            = 'plain.php';
            $this->module->layout                    = 'plain.php';
            Yii::$app->params['additionalBodyClass'] = 'item-rendering-external-widget';
        }
        $this->view->noindex();
        return parent::beforeAction($action);
    }

    /**
     *
     * @return Response
     */
    public function actionProfile()
    {
        return $this->redirect('@web/user-profile');
    }

    protected function filterRedirect($redirectTo)
    {
        if (!$redirectTo) {
            $redirectTo = Url::previous() ?: '/';
        }
        if ($redirectTo && strpos($redirectTo, 'user/signup') !== false) {
            $redirectTo = '/';
        }
        return $redirectTo;
    }

    protected function redirectBackFromLogin()
    {
        $this->layout         = 'plain.php';
        $this->module->layout = 'plain.php';

        if (\Yii::$app->request->referrer && Yii::$app->getModule('intlDomains')->domainManager->isTreatstockDomain(\Yii::$app->request->referrer)) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->redirect('/');
    }

    /**
     * user registration process
     *
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function actionSignup()
    {
        $model = new \frontend\models\user\SignupForm();
        if (!is_guest()) {
            return $this->redirectBackFromLogin();
        }
        // try login
        $loginForm = new \frontend\models\user\LoginForm();
        if ($this->tryLogin($loginForm, 'SignupForm')) {
            $redirectTo  = $this->filterRedirect($loginForm->redirectTo);
            $currentUser = $this->getCurrentUser();
            if ($currentUser && $currentUser->userProfile && $currentUser->userProfile->current_lang) {
                $redirectTo = \Yii::$app->getModule('intlDomains')->domainManager->getUrlWithLangDomain($redirectTo, $currentUser->userProfile->current_lang);
            }
            return $this->redirect($redirectTo);
        }

        if (!empty(app('request')->get('redirectTo'))) {
            $model->redirectTo = app('request')->get('redirectTo');
        }

        if ($model->load(app('request')->post())) {

            if (UserFacade::hasDanyUsernameWords($model->email)) {
                throw new ForbiddenHttpException('Username contains forbidden word');
            }

            $user = $model->signup();
            if ($user && Yii::$app->getUser()->login($user)) {
                $redirectTo  = $this->filterRedirect($model->redirectTo);
                $currentUser = $this->getCurrentUser();
                if ($currentUser && $currentUser->userProfile && $currentUser->userProfile->current_lang) {
                    $redirectTo = \Yii::$app->getModule('intlDomains')->domainManager->getUrlWithLangDomain($redirectTo, $currentUser->userProfile->current_lang);
                }
                $this->checkAfterLoginAction();
                return $this->redirect($redirectTo);
            }
        }
        return $this->render(
            'signup',
            [
                'model' => $model
            ]
        );
    }

    /**
     * @param LoginForm $loginForm
     * @param null $htmlFormName
     * @return bool
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\UserException
     */
    protected function tryLogin($loginForm, $htmlFormName = null)
    {
        if (app('request')->isPost && $loginForm->load(app('request')->post(), $htmlFormName)) {
            // after login action
            if ($loginForm->login()) {
                $this->checkAfterLoginAction();
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $hash
     * @param bool $confirmed
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     */
    public function actionEmailLogin($hash = '', $confirmed = false)
    {
        $emailLoginService = \Yii::createObject(UserEmailLoginService::class);
        $newRedirectTo     = app('request')->get('redirectTo', false);
        if (!empty($hash) && $confirmed) {
            $emailLogin    = UserEmailLogin::tryFind(['hash' => $hash]);
            $newRedirectTo = $emailLogin->requested_url;
            // sign out if current user and login user is different
            if (!is_guest() && !$emailLogin->equalUser($this->getCurrentUser())) {
                Yii::$app->user->logout();
            }
            if (!$emailLogin || !$emailLoginService->isValidEmailLogin($emailLogin)) {
                $this->setFlashMsg(false, _t('site.user', 'Link is expired/invalid. Please request a new sign-in link.'));
                return $this->redirect('/user/email-login');
            }
            $canSignIn = Yii::$app->request->post('agree') || $emailLogin->user->isUnconfirmed();
            if ($canSignIn && $emailLoginService->isValidEmailLogin($emailLogin)) {
                return $emailLoginService->signInByUserEmailLogin($emailLogin);
            }
        }

        $emailLogin = new UserEmailLogin();
        if (app('request')->isPost) {
            $emailLogin->load(app('request')->post());
            $user = $emailLoginService->findValidUserByEmail($emailLogin->email);
            if ($user) {
                if (empty($emailLogin->requested_url)) {
                    $emailLogin->requested_url = param('server') . '/workbench/orders';
                }
                $emailLoginService->requestLoginByEmail($user, $emailLogin->email, $emailLogin->requested_url);
                $this->setFlashMsg(
                    true,
                    _t(
                        'site.user',
                        'We sent an email to you at {email}. ',
                        ['email' => $emailLogin->email]
                    )
                );
                return $this->redirect('/user/email-login');
            } else {
                $this->setFlashMsg(false, _t('site.user', 'Email is not correct.')); // brute-forcing?
            }
        }
        if ($newRedirectTo) {
            $emailLogin->requested_url = $newRedirectTo;
        } else {
            if (empty($emailLogin->requested_url)) {
                $emailLogin->requested_url = param('server') . '/workbench/orders';
            }
        }
        return $this->render(
            'loginByEmail.php',
            ['emailLogin' => $emailLogin, 'hash' => $hash, 'confirmed' => $confirmed]
        );
    }

    /**
     * user log in action
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!is_guest()) {
            return $this->redirectBackFromLogin();
        }
        $model = new \frontend\models\user\LoginForm();

        if (!empty(app('request')->get('redirectTo'))) {
            $model->redirectTo = app('request')->get('redirectTo');
        }

        try {
            if ($this->tryLogin($model)) {
                $redirectTo  = $this->filterRedirect($model->redirectTo);
                $currentUser = $this->getCurrentUser();
                if ($currentUser && $currentUser->userProfile && $currentUser->userProfile->current_lang) {
                    $redirectTo = \Yii::$app->getModule('intlDomains')->domainManager->getUrlWithLangDomain($redirectTo, $currentUser->userProfile->current_lang);
                }
                return $this->redirect($redirectTo);
            }
        } catch (\Exception $e) {
            $this->setFlashMsg(false, $e->getMessage());
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax(
                'loginAjax',
                [
                    'model'       => $model,
                    'modelSignup' => new \frontend\models\user\SignupForm(),
                ]
            );
        }
        if (app('request')->isPost) {
            if ($model->hasErrors()) {
                $this->setFlashMsg(false, _t('site.user', 'Incorrect email or password.'));
            }
            return $this->redirect('/user/login');
        }
        return $this->render(
            'login',
            [
                'model' => $model
            ]
        );
    }

    /**
     * Post info to remote login (google, facebook)
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAuthRemote()
    {
        $redirectTo = Yii::$app->request->get('redirectTo');
        $redirectTo = $this->filterRedirect($redirectTo);

        Yii::$app->session->set('remoteLoginRedirect', $redirectTo);
        Yii::$app->session->set('remoteLoginRememberMe', app('request')->get('rememberMe', false));
        /** @var AuthAction $action */
        $action = new AuthAction(
            'afterRemoteLogin', $this, [
                'successCallback' => [
                    $this,
                    'authRemoteSuccessCallback'
                ]
            ]
        );
        try {
            $result = $action->run();
        } catch (HttpException $exception) {
            throw new BusinessException($exception->getMessage());
        }
        return $result;
    }

    /**
     * Remote login answer
     *
     * @param $authclient
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAfterRemoteLogin($authclient)
    {
        $_GET['authclient'] = $authclient;
        $action             = new AuthAction(
            'afterRemoteLogin', $this, [
                'successCallback' => [
                    $this,
                    'authRemoteSuccessCallback'
                ]
            ]
        );
        try {
            $result = $action->run();
        } catch (HttpException $exception) {
            throw new BusinessException($exception->getMessage());
        }
        return $result;
    }

    /**
     * callback for auth
     * checks if user with email exists - logins and checks user_osn table
     * if user with new email - creates user and logins
     *
     * @param \yii\authclient\ClientInterface $client
     * @return \yii\console\Response|Response
     * @throws NotFoundHttpException
     */
    public function authRemoteSuccessCallback($client)
    {
        Yii::$app->getSession()->remove('osn_login');
        $osnUser = OsnGateway::prepareUserData($client);
        /** @var FrontUser $user */
        $user       = FrontUser::find()->where(
            [
                'email' => $osnUser['email']
            ]
        )->one();
        $rememberMe = Yii::$app->session->get('remoteLoginRememberMe');

        if (!empty($user)) {
            // check if bined used
            $userOsn = UserOsn::find()->where(['email' => $osnUser['email'], 'osn_code' => $osnUser['osn']['osn_code']])->oldest();
            if ($userOsn) {
                $user = FrontUser::findOne(['id' => $userOsn->user_id]);
            } else {
                // check if osn is bined to user
                UserFacade::bindOsn($user->id, $osnUser['osn']);
            }
        } else {
            // check if bined used
            $userOsn = UserOsn::findOne(['email' => $osnUser['email'], 'osn_code' => $osnUser['osn']['osn_code']]);
            if ($userOsn) {
                $user = FrontUser::findOne(['id' => $userOsn->user_id]);
            } else {
                // new user
                $user = UserFacade::createUserByOsn($osnUser);
            }
            $rememberMe = true;
        }
        if ($user) {
            if (empty($user->userProfile->avatar_url) && !empty($osnUser['profile']['avatar_url'])) {
                \common\models\UserProfile::updateRow(
                    ['user_id' => $user->id],
                    [
                        'avatar_url' => $osnUser['profile']['avatar_url']
                    ]
                );
            }
            UserFacade::loginByOsn($user, $rememberMe);
        }
        $this->checkAfterLoginAction();

        $redirectUrl = Yii::$app->session->get('remoteLoginRedirect');
        if ($user && $user->userProfile && $user->userProfile->current_lang) {
            $redirectUrl = \Yii::$app->getModule('intlDomains')->domainManager->getUrlWithLangDomain($redirectUrl, $user->userProfile->current_lang);
        }
        $response          = Yii::$app->getResponse();
        $this->layout      = 'empty';
        $response->content = $this->render(
            'afterRemoteLogin',
            [
                'redirectUrl' => $redirectUrl
            ]
        );
        return $response;
    }

    /**
     *
     * @param string $code
     * @return string
     * @throws \yii\base\UserException
     */
    public function actionRestore($code)
    {
        $userRequest = \common\models\UserRequest::findOne(
            [
                'code' => $code
            ]
        );
        if ($userRequest) {
            UserFacade::confirmRestore($userRequest);
            $this->setFlashMsg(false, _t("user", "Your account restored. You can try to login now."));
        } else {
            $this->setFlashMsg(false, _t("user", "The link for restoring the account is not correct. If you think this is an error, please contact us"));
        }
        return $this->redirect(
            [
                '/user/login'
            ]
        );
    }

    /**
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    private function checkAfterLoginAction()
    {
        $modelId = isset($_COOKIE['afterlogin_model3d_to_like']) ? $_COOKIE['afterlogin_model3d_to_like'] : 0;
        if ($modelId) {
            $model = \common\models\Model3d::tryFindByPk($modelId);
            $user  = $this->getCurrentUser();
            LikeService::likeModel($model, $user);
            unset($_COOKIE['afterlogin_model3d_to_like']);
            setcookie('afterlogin_model3d_to_like', null, -1, '/');
        }
        $psPrinterId = isset($_COOKIE['afterlogin_ps_to_like']) ? $_COOKIE['afterlogin_ps_to_like'] : 0;
        if ($psPrinterId) {
            LikeService::likeObject(UserLike::TYPE_PS_PRINTER, $psPrinterId, $this->getCurrentUser());
            unset($_COOKIE['afterlogin_ps_to_like']);
            setcookie('afterlogin_ps_to_like', null, -1, '/');
        }
    }

    /**
     * user logout - keeps sessionId()
     *
     * @return string
     */
    public function actionLogout()
    {
        if (app('request')->isPost) {
            $promoService = new PromocodeApplyService();
            $promoService->clearRepo();
            Yii::$app->asyncSession->reset();
            Yii::$app->user->logout();
            Yii::$app->session->destroy();
        }
        return $this->redirect('/');
    }

    /**
     * handles ajax request to resend email confirmation
     *
     * @return array|string|Response
     * @throws \Exception
     */
    public function actionResendEmail()
    {
        if (\Yii::$app->request->isAjax) {
            /** @var FrontUser $currentUser */
            $currentUser = Yii::$app->user->getIdentity();
            if ($currentUser == null) {
                return $this->goHome();
            }
            if ($currentUser->needConfirm()) {
                $resendModel = new \frontend\models\user\ResendEmailForm($currentUser);
                $resendModel->resend();
                $resultJson = [
                    'message' => _t('front', 'E-mail sent')
                ];
            } else {
                $resultJson = [
                    'message' => _t('front', 'E-mail already confirmed')
                ];
            }
            return $this->jsonSuccess($resultJson);
        }
        return $this->render('user/resendConfirm');
    }

    /**
     * Confirm email by key,
     * if confirmed redirected
     * if not confirmed - show error message
     *
     * @param string $key
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionEmailConfirm($key, CompanyVerifyService $verifyService)
    {
        $user = UserFacade::getCurrentUser();
        if (!$user) {
            #$this->setFlashMsg(false, _t('site', 'Please login and refresh this page to confirm your email.'));
            #return $this->render('_blank');
        }
        $msg = _t('front', 'E-mail cannot be confirmed. Wrong key.');
        if (is_guest()) {
            $user = FrontUser::find()->where(['auth_key' => $key])->one();
        }
        if ($user) { // confirmed
            $oldKey = $user->auth_key;
            if (is_guest()) {
                Yii::$app->user->login($user, 0);
                $user->generateAuthKey();
            }

            if ($user->status == User::STATUS_DRAFT_COMPANY) {
                $verifyService->verifiedByUserRegister(PsFacade::getPsByUserId($user->id));
            }
            $user->status = User::STATUS_ACTIVE;
            if ($user->save()) {
                // update companies

                // update preorders
                $this->preorderService->confirmAllNotConfirmed($user);

                UserLog::log($user->id, 'email', 'unconfirmed', 'confirmed');
                $msg = _t('front', 'E-mail confirmed');
                if ($oldKey == $key) {
                    Yii::$app->getSession()->setFlash('success', $msg);
                }
                return $this->goHome();
            }
        }
        Yii::$app->getSession()->setFlash('error', $msg);
        return $this->render(
            '_blank',
            [
                'msg' => $msg
            ]
        );
    }

    /**
     * Confirm on change email
     *
     * @param string $key
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionChangeEmailConfirm($key)
    {
        if (is_guest()) {
            Yii::$app->getSession()->setFlash('error', _t('site.user', 'Please login and open link again to confirm your new email.'));
            return $this->render('_blank');
        }
        $user = $this->getCurrentUser(true);

        /** @var ChangeEmailRequest $request */
        $request = ChangeEmailRequest::find()->unconfirmed()
            ->forUser($user)
            ->byKey($key)
            ->one();

        if (!$request) {
            throw new NotFoundHttpException('Cant find change email request');
        }

        try {
            UserFacade::applyChangeEmailRequest($user, $request);
            Yii::$app->getSession()->setFlash('success', _t('front', 'E-mail confirmed'));
            return $this->goHome();
        } catch (ChangeEmailException $e) {
            $msg = _t('front', $e->getMessage());
            Yii::$app->getSession()->setFlash('error', $msg);
            return $this->render(
                '_blank',
                [
                    'msg' => $msg
                ]
            );
        }
    }

    /**
     * show forgot form and send reset link with token
     *
     * @return string|Response
     */
    public function actionForgotPassword()
    {
        $model = new \frontend\models\user\PasswordResetRequestForm();
        if ($this->getCurrentUser()) {
            return $this->redirect('/');
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', _t('front.site', 'Check your email for further instructions.'));
                return $this->redirect(
                    \yii\helpers\Url::to(
                        [
                            'user/forgot-password'
                        ]
                    )
                );
            } else {
                Yii::$app->getSession()->setFlash('error', _t('front.site', 'Sorry, we are unable to reset password for email provided.'));
            }
        }
        return $this->render(
            'forgotPassword',
            [
                'model' => $model
            ]
        );
    }

    /**
     * action to reset password - link from email with token
     *
     * @param string $token
     * @return string|Response
     */
    public function actionResetPassword($token)
    {
        $model = new \frontend\models\user\ResetPasswordForm($token);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', _t('front', 'New password was saved.'));
            return $this->goHome();
        }
        return $this->render(
            'forgotPasswordReset',
            [
                'model' => $model
            ]
        );
    }

    /**
     * user liked object
     *
     * @return array
     */
    public function actionLike()
    {
        $objectType = Yii::$app->request->post('object_type');
        $objectId   = Yii::$app->request->post('object_id');
        LikeService::likeObject($objectType, $objectId, $this->getCurrentUser());
        $total = LikeService::getObjectLikesCount($objectType, $objectId);
        return $this->jsonReturn(
            [
                'success' => true,
                'message' => LikeService::getLikesCountString($total),
                'liked'   => true,
                'action'  => '/user/unlike'
            ]
        );
    }

    /**
     * user unliked object
     *
     * @return array
     */
    public function actionUnlike()
    {
        $objectType = Yii::$app->request->post('object_type');
        $objectId   = Yii::$app->request->post('object_id');
        LikeService::unlikeObject($objectType, $objectId, $this->getCurrentUser());
        $total = LikeService::getObjectLikesCount($objectType, $objectId);
        return $this->jsonReturn(
            [
                'success' => true,
                'message' => LikeService::getLikesCountString($total),
                'liked'   => false,
                'action'  => '/user/like'
            ]
        );
    }

    /**
     * change current measurement
     * used by switcher widget
     *
     * @return array
     */
    public function actionChangeMeasurement()
    {
        $result = app('request')->post('newValue');
        if (!empty($result)) {
            \frontend\components\UserSessionFacade::setMeasurement($result);
        }
        return $this->jsonSuccess(
            [
                'message' => 'updated'
            ]
        );
    }
}
