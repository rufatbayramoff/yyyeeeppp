<?php

namespace frontend\controllers\user;

use common\components\BaseController;
use common\components\exceptions\BusinessException;
use common\models\Model3d;
use common\models\ProductCommon;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\StoreUnit;
use common\models\User;
use common\models\UserCollection;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use frontend\components\UserUtils;
use frontend\models\ps\PsFacade;
use frontend\models\store\StoreFacade;
use frontend\models\store\StoreSearchForm;
use frontend\models\user\UserFacade;
use lib\collection\CollectionDb;
use yii\data\Pagination;
use yii\web\GoneHttpException;
use yii\web\HttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;

/**
 * Users catalog controler
 * - view users
 * - view user public profile
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class UController extends BaseController
{
    const USER_LOAD_BY_ID       = 1;
    const USER_LOAD_BY_USERNAME = 2;

    protected $viewPath = '@frontend/views/user/';

    public function beforeAction($action)
    {
        $this->view->noindex();
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }


    /**
     * get list of active and confirmed users
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['/']);
    }

    /**
     * @param $username
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProfile($username)
    {
        $user = $this->loadUser($username);
        if ($user->hasActiveCompany()) {
            return $this->redirectByPs($user->ps[0]);
        }

        if ($user->userProfile->is_hidden_public_page) {
            throw new BusinessException('Public page is hidden', 451);
        }

        $storeUnits = [];
        $allModels = Model3d::find()->joinWith('productCommon')->andWhere(['product_common.user_id' => $user->id])->all();
        foreach ($allModels as $oneModel) {
            if (!$oneModel->storeUnit) {
                continue;
            }
            $storeUnits[] = $oneModel->storeUnit;
        }

        $params = ['user' => $user];
        $about = $user->userProfile->info_about . '. ' . $user->userProfile->info_skills;
        if (!empty($user->userProfile->info_about)) {
            $this->view->params['meta_description'] = $about;
        } else {
            $this->view->params['meta_description'] = _t('site.seo', 'View {username} user profile at Treatstock.', ['username' => \H($user->username)]);
        }

        return $this->render('public/publicProfile', compact('params', 'storeUnits'));
    }

    /**
     *
     * @TODO - replace all logic to use StoreSearchForm with StoreFacade::getStoreUnits($searchForm)
     *
     * @param string $username
     * @param bool $widget If render widget image for embed
     * @param int $count
     * @param null $categoryId
     * @param null $sort
     * @return string
     * @throws GoneHttpException
     */
    public function actionStore($username, $widget = false, $count = 120, $categoryId = null, $sort = null)
    {
        $userModel = $this->loadUser($username);
        if ($userModel->ps) {
            return $this->redirectByPs($userModel->ps[0], 'store');
        }
        $count = (int)$count;
        if ($count < 1) {
            $count = 1;
        }
        if ($count > 120) {
            $count = 120;
        }

        $params = [
            'userModel' => $userModel
        ];
        $tpl = 'public/store.php';
        if ($widget) {
            $this->layout = 'plain.php';
            $tpl = 'public/storeEmbed.php';
        }

        $storeUnitsQuery = Model3d::find()->storePublished($userModel);
        if ($categoryId) {
            $storeUnitsQuery->inCategory($categoryId);
        }
        if ($sort) {
            $sortColumn = StoreUnit::column('rating');
            $storeUnitsQuery->joinWith('productCommon');
            if ($sort === StoreSearchForm::SORT_MODEL_DATE) {
                $sortColumn = ProductCommon::column('created_at');
            }
            $storeUnitsQuery->orderBy($sortColumn . ' desc');
        }
        $pages = new Pagination(['totalCount' => $storeUnitsQuery->count(), 'defaultPageSize' => $count]);
        $storeUnits = $storeUnitsQuery->offset($pages->offset)->limit($pages->limit)->all();

        $username = UserFacade::getFormattedUserName($userModel, true);
        $title = _t('site.store', '{user} - Shop 3D Models To Print', ['user' => $username]);

        if (empty($this->view->params['meta_description'])) {
            $this->view->params['meta_description'] = _t(
                'site.seo',
                'Shop 3D Printable Models by {user}',
                ['user' => $username]
            );
        }
        if (empty($this->view->params['meta_keywords'])) {
            $items = CollectionDb::getColumn($storeUnits, 'title');
            $items = $items ? ', ' . implode(', ', $items) : '';

            $this->view->params['meta_keywords'] = _t(
                    'site.seo',
                    '{user}, sell your 3d models',
                    ['user' => $username]
                ) . $items;
        }

        return $this->render(
            $tpl,
            compact(
                'title',
                'params',
                'userModel',
                'storeUnits',
                'title',
                'sort',
                'pages'
            )
        );
    }

    /**
     * @param $username
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCollections($username)
    {
        $user = $this->loadUser($username);

        $items = \frontend\models\model3d\CollectionFacade::getUserCollections($user->id, false);
        $params = [
            'userModel' => $user,
            'items'     => $items
        ];

        $collections = implode(', ', CollectionDb::getColumn($items, 'title'));
        if (count($items) > 2) {
            $collections = str_replace(['My Favorites,'], '', $collections);
        }

        if (empty($this->view->params['meta_description'])) {
            $this->view->params['meta_description'] = _t(
                'site.seo',
                '3D Printable Models Collection by {username}. {collections}.',
                ['username' => $username, 'collections' => $collections]
            );
        }
        if (empty($this->view->params['meta_keywords'])) {
            $this->view->params['meta_keywords'] = _t(
                    'site.seo',
                    'collections, 3d collections, 3d models, {username}, ',
                    ['username' => $username]
                ) . $collections;
        }
        return $this->render('public/collection/collectionList', compact('params', 'items', 'user'));
    }

    /**
     *
     * @param $username
     * @param int $collectionId
     * @return string
     * @internal param int $id
     */
    public function actionCollection($username, $collectionId)
    {
        $user = $this->loadUser($username);
        $collection = UserCollection::tryFindByPk($collectionId);
        $params = [
            'userModel'  => $user,
            'collection' => $collection,
            'items'      => $collection->userCollectionModel3ds,
        ];
        $this->view->params['meta_description'] = _t(
            'site.seo',
            '{collection} - Printable models collection by {username}',
            ['username' => $username, 'collection' => $collection->title]
        );
        $this->view->params['meta_keywords'] = _t(
            'site.seo',
            '{collection}, 3d collections, 3d models, {username}',
            ['username' => $username, 'collection' => $collection->title]
        );
        return $this->render('public/collection/collection', ['params' => $params]);
    }

    private function redirectByPs(Ps $ps, string $type = '3dprinting')
    {
        if ($type == 'store') {
            return $this->redirect(UserUtils::getUserStoreUrl($ps->user), 301);
        }
        return $this->redirect(['/c/public/profile', 'username' => $ps->url], 301);
    }

    /**
     *
     * @param int $id PS id
     * @param null $psUrl
     * @return string
     * @throws NotFoundHttpException
     * @internal param string $userlink
     */
    public function actionPrintservice($id = null, $psUrl = null)
    {
        if (strpos($psUrl, 'id') === 0) {
            $id = str_replace('id', '', $psUrl);
            $userModel = User::findOne($id);
            if (!$userModel) {
                return $this->redirect(CatalogPrintingUrlHelper::landingPage(), 301);
            }
            $ps = Ps::tryFind(['user_id' => $userModel->id], 'Printer not found');
            return $this->redirectByPs($ps);

        }
        if ($id) {
            $ps = Ps::tryFindByPk((int)$id);
            return $this->redirectByPs($ps);
        }

        $ps = Ps::tryFind(['url' => $psUrl], _t('public.ps', 'Print service not found.'));
        return $this->redirectByPs($ps);
    }

    /**
     * @param $psUrl
     * @return string
     * @throws GoneHttpException
     * @throws NotFoundHttpException
     */
    public function actionPrintserviceNew($psUrl)
    {
        $ps = Ps::findOne(['url' => $psUrl]);

        if (!$ps) {
            $ps = Ps::findOne(['url_old' => $psUrl]);
            if (!$ps) {
                throw new NotFoundHttpException(_t('site.user', 'Print Service not found'));
            }
        }

        return $this->redirect(PsFacade::getPsLink($ps), 301);
    }

    /**
     * @param $username
     * @param $printerId
     * @return string
     * @throws NotAcceptableHttpException
     * @throws NotFoundHttpException
     */
    public function actionPsMap($username, $printerId)
    {
        $user = $this->loadUser($username);
        $printer = PsPrinter::tryFindByPk($printerId);

        if ($printer->ps->user_id != $user->id) {
            throw new NotAcceptableHttpException('Wrong URL');
        }
        $oneLocation = $printer->getLocation()->one();
        $data['id'] = $oneLocation['id'];
        $data['title'] = $oneLocation['address'];
        $data['lat'] = $oneLocation['lat'];
        $data['lon'] = $oneLocation['lon'];
        $markers[] = $data;
        $dataLocations = [
            'status'  => 'OK',
            'markers' => $markers
        ];
        return $this->renderAdaptive('public/printserviceMap', ['printer' => $printer, 'dataLocations' => $dataLocations]);
    }

    /**
     * @param $username
     * @return User
     * @throws NotFoundHttpException
     */
    private function loadUser($username)
    {
        $user = User::findOne(['username' => $username]);

        if (!$user || $user->status == User::STATUS_DELETED) {
            throw new NotFoundHttpException(_t("site.error", "User not found"));
        }
        return $user;
    }
}
