<?php
/**
 * User: nabi
 */

namespace frontend\controllers;


use frontend\components\StaticPageAction;

class StaticPageController extends \common\components\BaseController
{

    public function actions()
    {
        $pageAlias = app('request')->getPathInfo();
        // we remove site/ for now
        $pageAlias = str_replace('site/', '', $pageAlias);
        return [
            'index' => [
                'class' => StaticPageAction::class,
                'pageAlias' => $pageAlias
            ]
        ];
    }
}