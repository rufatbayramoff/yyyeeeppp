<?php

namespace frontend\controllers;

use common\components\Emailer;
use common\components\exceptions\AssertHelper;
use common\components\exceptions\BusinessException;
use common\components\ps\promo\PsPromoRepository;
use frontend\controllers\actions\ConnectedAppsAction;
use Yii;
use yii\base\DynamicModel;
use yii\base\ViewNotFoundException;
use yii\web\NotFoundHttpException;

class PromoController extends \common\components\BaseController
{
    /**
     * @var Emailer
     */
    private $emailer;

    /**
     * @param Emailer $emailer
     */
    public function injectDependencies(Emailer $emailer)
    {
        $this->emailer = $emailer;
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'apps' => [
                'class' => ConnectedAppsAction::class,
            ]
        ];
    }

    /**
     * @param string $id
     * @param array $params
     * @return mixed|string
     */
    public function runAction($id, $params = [])
    {
        if ($this->hasViewPage($id)) {
            return $this->actionPage($id, $params);
        }
        return parent::runAction($id, $params);
    }

    /**
     * @param $id
     * @return bool
     */
    private function hasViewPage($id)
    {

        if (file_exists(\Yii::getAlias('@frontend/views/promo/' . $id . '.php'))) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @param $params
     * @return string
     */
    public function actionPage($id, $params = [])
    {
        $currentUrl = app('request')->pathInfo;
        if (strpos($currentUrl, 'promo/') !== false) {
            // redirect
            $newLink = str_replace('promo/', 'l/', $currentUrl);
            return $this->redirect('/' . $newLink, 301);
        }
        $this->registerJsSiteParams();
        $this->view->params['currentUrl'] = app('request')->url;
        try {
            $html = $this->render($id, $params);
        } catch (ViewNotFoundException $viewException) {
            throw new NotFoundHttpException('Not exists promo page');
        }
        return $html;
    }


    /**
     *
     */
    public function actionContactUs()
    {
        $model = DynamicModel::validateData(Yii::$app->request->post(), [
            ['email', 'email', 'message' => _t('site.promo', 'Please enter a valid email')],
            ['email', 'required'],
        ]);

        AssertHelper::assertValidate($model);

        $data = Yii::$app->request->post();

        $subjects = [
            'partnership' => 'Message from promo/partnership',
            'business'    => 'Message from promo/business'
        ];

        $title = $subjects[$data['type']];

        $msg = '';

        foreach ($data as $label => $value) {
            $msg .= ucfirst(H($label)) . ": " . H($value) . '<br/>';
        }

        if (!$msg) {
            return;
        }

        $this->emailer->sendSupportMessage($title, $msg);
    }
}