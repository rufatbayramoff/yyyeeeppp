<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.06.17
 * Time: 10:46
 */

namespace frontend\controllers;

use common\components\HttpRequestHelper;
use common\models\HttpRequestLog;
use frontend\widgets\GoogleAnalyticsWidget;
use yii;

/**
 * Site controller
 */
class SessionController extends \common\components\BaseController
{
    protected function initMultidomain()
    {
        \Yii::$app->getModule('intlDomains')->domainManager->sendAllowAjaxIntlDomainHeaders();
        if (\Yii::$app->request->getIsOptions()) {
            \Yii::$app->end();
        }
    }

    public function actionPingGlobalSession()
    {
        Yii::$app->sessionManager->pingGlobalSessionImg();
    }

    protected function warnMessage($warnMessage)
    {
        if (!HttpRequestHelper::isBot(Yii::$app->request)) {
            app()->httpRequestLogger->initRequestLog(HttpRequestLog::REQUEST_TYPE_RESTORE_SESSION);
            app()->httpRequestLogger->closeRequest($warnMessage);
            Yii::warning($warnMessage);
        }
        sleep(1);
        echo GoogleAnalyticsWidget::widget();
        echo $warnMessage;
    }

    public function actionDoRestoreSession($redirectParams)
    {
        $this->initMultidomain();
        Yii::$app->response->getHeaders()->add('X-Robots-Tag', 'noindex, nofollow');
        if (!Yii::$app->sessionManager->doRestoreSession($redirectParams)) {
            $warnMessage = 'Invalid restore session params. ' . date('Y-m-d H:i:s') . '_' . random_int(0, 99999999);
            $this->warnMessage($warnMessage);
        }
    }

    public function actionSetSession($sessionParams)
    {
        $this->initMultidomain();
        if (!Yii::$app->sessionManager->setSession($sessionParams)) {
            $warnMessage = 'Invalid set session params. ' . date('Y-m-d H:i:s') . '_' . random_int(0, 99999999);
            $this->warnMessage($warnMessage);
        }
    }
}