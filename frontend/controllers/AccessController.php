<?php
/**
 * User: nabi
 */

namespace frontend\controllers;


use common\components\BaseController;

class AccessController extends BaseController
{
    public function actionBackend($key = null)
    {
        $path = \Yii::getAlias('@backend/runtime/ips.txt');
        $userIp = $_SERVER['REMOTE_ADDR'];
        $keyParam = param('backendAccessKey');

        if ($key === $keyParam) {
            $ips = file_get_contents($path);
            file_put_contents($path, ''); // clear file and write new data
            $ipRows = explode("\n", $ips);
            $ipList = [];
            foreach ($ipRows as $ip) {
                $ip = str_replace('allow', '', $ip);
                if (strpos($ip, '#') > 0) {
                    $ip = substr($ip, 0, strpos($ip, '#'));
                }
                $ip = trim($ip, " \t\n\r\x0B;");
                if (empty($ip)) {
                    continue;
                }
                $ipList[$ip] = $ip;
            }
            $ipList[$userIp] = $userIp;
            $ipList = array_map(
                function ($item) {
                    return 'allow ' . $item . ';';
                },
                $ipList
            );

            $data = implode("\n", $ipList);
            file_put_contents($path, $data);
            echo('OK. ');
        }
        echo('Done');
        \Yii::$app->end();
    }
}