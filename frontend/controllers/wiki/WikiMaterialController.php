<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.11.17
 * Time: 14:53
 */

namespace frontend\controllers\wiki;

use backend\models\search\WikiMaterialSearch;
use common\components\BaseController;
use common\models\Ps;
use common\models\WikiMaterial;
use common\modules\cnc\repositories\PsCncMachineRepository;
use frontend\models\user\CompanyPublicPageEntity;
use Yii;

class WikiMaterialController extends BaseController
{
    public $viewPath = '@frontend/views/wiki';

    /**
     * @var Ps
     */
    public $ps;
    /**
     * @var CompanyPublicPageEntity
     */
    public $publicServicePage;

    public function injectDependencies(PsCncMachineRepository $psCncMachineRepository)
    {

    }

    public function actionIndex()
    {
        $wikiMaterialSearch = new WikiMaterialSearch();
        $searchParams = Yii::$app->request->get();
        $searchParams[$wikiMaterialSearch->formName()]['is_active'] = 1;
        $provider = $wikiMaterialSearch->search($searchParams);
        $provider->query->notWidget();
        return $this->render(
            'wikiMaterialsList',
            [
                'wikiMaterialSearch' => $wikiMaterialSearch,
                'dataProvider' => $provider
            ]
        );
    }

    public function actionViewMaterial($code)
    {
        $wikiMaterial = WikiMaterial::find()->notWidget()->andWhere(['code' => $code])->tryOne();
        return $this->render(
            'wikiMaterialItem',
            [
                'wikiMaterial' => $wikiMaterial
            ]
        );
    }
}