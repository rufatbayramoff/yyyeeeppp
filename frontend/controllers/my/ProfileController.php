<?php

namespace frontend\controllers\my;

use common\components\exceptions\AssertHelper;
use common\models\UserDeactivatingLog;
use common\services\StoreOrderService;
use common\services\UserProfileService;
use common\traits\TransactedControllerTrait;
use frontend\models\user\UploadDocumentForm;
use frontend\models\user\UserDeleteRequestForm;
use frontend\models\user\UserFacade;
use frontend\models\user\verifyPhone\ConfirmForm;
use frontend\models\user\verifyPhone\SendForm;
use Yii;
use yii\base\UserException;

/**
 * user public profile controller
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class ProfileController extends \common\components\BaseController
{
    use TransactedControllerTrait;

    protected $viewPath = '@frontend/views/user/my';

    /**
     * @var UserProfileService
     */
    protected $userProfileService;

    /**
     * @var StoreOrderService
     */
    protected $storeOrderService;

    public function injectDependencies(
        UserProfileService $userProfileService,
        StoreOrderService $storeOrderService
    )
    {
        $this->userProfileService = $userProfileService;
        $this->storeOrderService = $storeOrderService;
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'sendVerifyPhone' => ['post'],
                    'verifyPhone'     => ['post'],
                    'uploadDocument'  => ['post'],
                    'delete'          => ['post']
                ]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $user = $this->getCurrentUser(true);

        $params = [
            'user' => $user
        ];
        $this->view->noindex();
        return $this->render('publicProfile', [
            'params' => $params
        ]);
    }

    /**
     * @return array|string
     * @throws \yii\base\InvalidParamException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDeleteRequest()
    {
        $this->layout = 'plain.php';
        $user = UserFacade::getCurrentUser();
        $openOrdersCount = StoreOrderService::getClientProductionOrdersCount($user) + StoreOrderService::getCompanyProductionAndNewOrdersCount($user);
        $allowClaimOrdersCount = StoreOrderService::getCompanyAllowClaimOrdersCount($user);

        if ((\Yii::$app->request->isPost && \Yii::$app->request->get('deleteSubmit')) &&
            ($openOrdersCount == 0) && ($allowClaimOrdersCount == 0)) {
            $this->userProfileService->deleteAccountSendConfirmation($user);
            $message = _t('user.profile', 'Code was sent to your email');
            \Yii::$app->getSession()->setFlash('success', $message);
            return $this->jsonReturn(['success' => true, 'message' => $message]);
        }
        $deleteAccountConfirmationCode = $this->userProfileService->getLastDeleteAccountConfirmationCode($user);

        /** @var UserDeactivatingLog $userDeactivatingLog */
        $userDeactivatingLog = Yii::createObject(UserDeactivatingLog::class);
        $userDeactivatingLog->setScenario(UserDeactivatingLog::SCENARIO_NOT_VALIDATED);

        return $this->render('deleteRequest', [
            'confirmationCodeExists' => ((bool)$deleteAccountConfirmationCode),
            'openOrdersCount'        => $openOrdersCount,
            'allowClaimOrdersCount'  => $allowClaimOrdersCount,
            'userDeactivatingLog'    => $userDeactivatingLog,
            'currentUser'            => $user
        ]);
    }

    /**
     * @return \yii\web\Response
     * @throws UserException
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDeleteConfirm()
    {
        sleep(3); // Protect by brut force
        $user = UserFacade::getCurrentUser();
        $deleteAccountConfirmationCode = $this->userProfileService->getLastDeleteAccountConfirmationCode($user);
        $this->storeOrderService->checkActiveOrder($user);
        // By enter code in form
        $profileDeleteRequestForm = \Yii::$app->request->post('UserProfileDeleteRequest');
        $code = $profileDeleteRequestForm['approve_code'] ?? '';
        if ($deleteAccountConfirmationCode && ($code === $deleteAccountConfirmationCode->approve_code)) {
            Yii::$app->user->logout();
            $this->userProfileService->deleteAccount($user, true);
            $this->userProfileService->addDeleteAccountReason($user, Yii::$app->request->post('UserDeactivatingLog'));
            \Yii::$app->getSession()->setFlash('success', _t('user.profile', 'Account was deleted'));
            return $this->redirect('/');
        } else {
            \Yii::$app->getSession()->setFlash('error', _t('user.profile', 'Confirmation code invalid'));
            return $this->redirect('/profile');
        }
    }

    /**
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDeleteConfirmByUrl()
    {
        // By enter code in form
        $approveCode = \Yii::$app->request->get('approveCode');

        /** @var UserDeactivatingLog $userDeactivatingLog */
        $userDeactivatingLog = Yii::createObject(UserDeactivatingLog::class);
        $userDeactivatingLog->setScenario(UserDeactivatingLog::SCENARIO_NOT_VALIDATED);

        return $this->render(
            'deleteConfirmByUrl', [
            'approveCode'         => $approveCode,
            'userDeactivatingLog' => $userDeactivatingLog
        ]);
    }

}
