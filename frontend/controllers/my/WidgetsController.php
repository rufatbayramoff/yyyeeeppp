<?php

namespace frontend\controllers\my;

use common\components\exceptions\AssertHelper;
use frontend\models\user\UserFacade;
use Yii;
use yii\helpers\Json;

/**
 *
 */
class WidgetsController extends \common\components\BaseController
{

    protected $viewPath = '@frontend/views/user/my';

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Show widgets
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('widgets');
    }
}
