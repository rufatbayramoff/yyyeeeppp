<?php

namespace frontend\controllers\my;

use common\components\exceptions\BusinessException;
use common\models\PaymentTransaction;
use common\models\StoreOrder;
use common\modules\payment\components\PaymentCheckout;
use common\modules\payment\components\PaymentInfoHelper;
use common\modules\payment\gateways\vendors\StripeGateway;
use common\modules\payment\serializers\PaymentInvoiceSerializer;
use frontend\models\user\UserFacade;
use lib\money\Currency;
use lib\money\Money;
use Twilio\TwiML\Voice\Pay;
use yii\base\UserException;

trait CheckoutCardViewTrait
{
    /**
     * @param StoreOrder $storeOrder
     * @param bool $widget
     *
     * @return string
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    protected function getCheckoutCardView(StoreOrder $storeOrder, $widget = false): string
    {
        $paymentInvoice       = $storeOrder->getPrimaryInvoice();
        $logUuid              = \common\components\UuidHelper::generateUuid();
        $defaultPayCardVendor = param('default_pay_card_vendor');

        $braintreeClientToken    = '';
        $stripeClientToken       = '';
        $stripeAlipayClientToken = '';

        if ($storeOrder->getPrimaryInvoice()->total_amount !== 0) {
            $braintreeClientToken = app('session')->get('brt_token_' . $paymentInvoice->uuid);
            if (!$braintreeClientToken) {
                $braintreePaymentCheckout = new PaymentCheckout(PaymentTransaction::VENDOR_BRAINTREE);
                $braintreePaymentCheckout->paymentProcessor->braintreeGateway->paymentLogger->payPageLogUuid = $logUuid;
                $braintreeClientToken     = $braintreePaymentCheckout->generateClientToken($paymentInvoice);
                app('session')->set('brt_token_' . $paymentInvoice->uuid, $braintreeClientToken);
            }

            $stripeClientToken = app('session')->get('stripe_token_' . $paymentInvoice->uuid);
            if (!$stripeClientToken) {
                $stripePaymentCheckout                                                  = new PaymentCheckout(PaymentTransaction::VENDOR_STRIPE);
                $stripePaymentCheckout->paymentProcessor->stripeGateway->paymentMethods = [StripeGateway::METHOD_TYPE_CARD];
                $stripePaymentCheckout->paymentProcessor->stripeGateway->paymentLogger->payPageLogUuid = $logUuid;
                $stripeClientToken                                                      = $stripePaymentCheckout->generateClientToken($paymentInvoice);
                app('session')->set('stripe_token_' . $paymentInvoice->uuid, $stripeClientToken);
            }

            if ($paymentInvoice->currency === Currency::USD) {
                $stripeAlipayClientToken = app('session')->get('stripe_alipay_token_' . $paymentInvoice->uuid);
                if (!$stripeAlipayClientToken) {
                    $stripePaymentCheckout                                                  = new PaymentCheckout(PaymentTransaction::VENDOR_STRIPE);
                    $stripePaymentCheckout->paymentProcessor->stripeGateway->paymentMethods = [StripeGateway::METHOD_TYPE_ALIPAY];
                    $stripePaymentCheckout->paymentProcessor->stripeGateway->captureMethod  = StripeGateway::CAPTURE_METHOD_AUTOMATIC;
                    $stripePaymentCheckout->paymentProcessor->stripeGateway->paymentLogger->payPageLogUuid = $logUuid;
                    $stripeAlipayClientToken                                                = $stripePaymentCheckout->generateClientToken($paymentInvoice);
                    app('session')->set('stripe_alipay_token_' . $paymentInvoice->uuid, $stripeAlipayClientToken);
                }
            }
        }

        if ($paymentInvoice->invoice_group_uuid) {
            $invoiceBraintree    = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($paymentInvoice->invoice_group_uuid,
                PaymentTransaction::VENDOR_BRAINTREE);
            $invoiceTs           = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($paymentInvoice->invoice_group_uuid,
                PaymentTransaction::VENDOR_TS);
            $invoiceBankTransfer = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($paymentInvoice->invoice_group_uuid,
                PaymentTransaction::VENDOR_BANK_TRANSFER);
            $invoiceBonus        = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($paymentInvoice->invoice_group_uuid,
                PaymentTransaction::VENDOR_BONUS);
        } else {
            $invoiceBraintree    = $paymentInvoice;
            $invoiceTs           = $paymentInvoice;
            $invoiceBankTransfer = $paymentInvoice;
            $invoiceBonus        = $paymentInvoice;
        }

        $this->layout = 'plain.php';
        if (UserFacade::getCurrentUser()) {
            $amountNetIncome = $this->paymentAccountService->getUserMainAmount($storeOrder->user, $invoiceTs->currency);
        } else {
            $amountNetIncome = Money::zero($invoiceTs->currency);
        }

        return $this->render('@common/modules/payment/views/checkoutCard', [
            'storeOrder'              => $storeOrder,
            'invoiceBraintree'        => $invoiceBraintree,
            'invoiceTs'               => $invoiceTs,
            'invoiceBonus'            => $invoiceBonus,
            'invoiceBankTransfer'     => $invoiceBankTransfer,
            'braintreeClientToken'    => $braintreeClientToken,
            'stripeClientToken'       => $stripeClientToken,
            'stripeAlipayClientToken' => $stripeAlipayClientToken,
            'widget'                  => $widget,
            'logUuid'                 => $logUuid,
            'posUid'                  => $this->placeOrderState->uid,
            'amountNetIncome'         => $amountNetIncome,
            'defaultPayCardVendor'    => $defaultPayCardVendor
        ]);
    }

    protected function billingDetailsPrint()
    {
        /** @var StoreOrder $storeOrder */
        $storeOrder = $this->placeOrderState->getOrder();

        if (!$storeOrder) {
            throw new BusinessException(_t('site.store', 'Order not found'));
        }

        StoreOrder::validateBeforeCheckout($storeOrder);

        $vendor         = app()->request->post('vendor');
        $primaryInvoice = $storeOrder->getPrimaryInvoice();


        if ($primaryInvoice->isInvoiceGroup()) {
            $invoice = $this->paymentInvoiceRepository->getNewInvoiceByGroupUuidPaymentMethod($storeOrder->getPrimaryInvoice()->invoice_group_uuid, $vendor);
        } else {
            $invoice = $primaryInvoice;
        }

        if (!$invoice || !$invoice->canPaidByMethodType($vendor, $this->getCurrentUser())) {
            throw new BusinessException(_t('site.store', 'Unable to pay by current payment method'));
        }

        return $this->jsonSuccess([
            'PaymentInvoice' => PaymentInvoiceSerializer::serialize($invoice),
        ]);
    }
}