<?php

namespace frontend\controllers\my;

use backend\modules\statistic\reports\ReportExcelWriter;
use common\components\BaseController;
use common\components\DateHelper;
use common\components\FileDirHelper;
use common\models\PaymentDetail;
use common\models\PaymentPayPageLog;
use common\models\PaymentPayPageLogProcess;
use common\models\PaymentPayPageLogStatus;
use common\models\PaymentTransaction;
use common\models\UserPaypal;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use frontend\models\payment\PaymentsReport;
use lib\pdf\HtmlToPdf;
use Yii;
use yii\base\BaseObject;
use yii\web\ForbiddenHttpException;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PaymentsController extends BaseController
{

    protected $viewPath = '@frontend/views/user/my/payments';

    protected $mode = 'payments';

    protected $enableSeo = false;

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /**
     * @param PaymentService $paymentService
     */
    public function injectDependencies(PaymentAccountService $paymentAccountService)
    {
        $this->paymentAccountService = $paymentAccountService;
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'out' => ['post'],
                ]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['log-payment-button-press', 'log-payment-process-status'],
                        'roles'   => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->redirect('/workbench/payments/?old=1');
        $paymentModel = new PaymentTransaction();
        $user         = $this->getCurrentUser(true);
        $params       = [
            'user' => $user
        ];
        $where        = [
            'user_id' => $user->id
        ];
        // $summary = '';

        $where['type'] = [
            'plus',
            'minus',
            'refund',
            'payment'
        ];
        $summary       = PaymentTransaction::getSummary($user->id);

        //$paymentsTransactions = PaymentTransaction::getDataProvider($where);
        // $paymentsTransactions->sort = ['defaultOrder' => ['created_at' => SORT_DESC]];

        $typesAwards    = PaymentDetail::getTypesNeedTax();
        $typesAwards[]  = PaymentDetail::TYPE_PAYOUT;
        $typesAwards[]  = PaymentDetail::TYPE_TAX;
        $paymentDetails = $this->getPaymentDetails($user);
        $userPaypal     = UserPaypal::findOne([
            'user_id' => $user->id
        ]);
        if ($user->thingiverseUser) {
            return $this->renderContent('');
        }
        return $this->render(
            'payments',
            [
                'params'         => $params,
                'paymentDetails' => $paymentDetails,
                'paymentModel'   => $paymentModel,
                'mode'           => $this->mode,
                'summary'        => $summary,
                'userPaypal'     => $userPaypal,
            ]
        );
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionTransactions()
    {
        $this->mode = 'transaction';
        $user       = $this->getCurrentUser('/');
        if ($user->thingiverseUser) {
            return $this->renderContent('');
        }
        if (is_guest()) {
            throw new ForbiddenHttpException('No access. Please sign in.');
        }
        if (app('request')->get('error', false)) { // if paypal gives error
            \Yii::warning(app('request')->get('error_description', "Error") . ". " . app('request')->get('error'), "paypal");
            return ("<html><head></head><body>
                <h2>" . app('request')->get('error_description', "Error") . "</h2>
                    <p>" . app('request')->get('error', "Error Request.") . "</p>                
                <script>//window.close();</script></body></html>");
        } else

            if (app('request')->get('code', false)) { // if user paypal confirms email

                $authCode = app('request')->get('code');
                try {
                    UserPaypal::validateByAuth($user->id, $authCode);
                    $this->layout = '@frontend/views/layouts/plain';
                    return $this->renderContent("<html><head></head><body><script>if (window.opener) {window.opener.location.reload(false);window.close();} else {window.location='/workbench/payments'}</script></body></html>");
                } catch (\Exception $ex) {
                    logException($ex, 'tsdebug');
                    //$this->setFlashMsg(false, $ex->getMessage());
                    throw $ex;
                }
            }
        return $this->redirect('/workbench/payments');
    }

    public function actionReport($from, $to, $pdf = false)
    {
        return $this->redirect('/workbench/payments/?old=1');
        $this->view->title = _t('site.payments', 'Financial Report');
        $user              = $this->getCurrentUser(true);
        $this->layout      = '@frontend/views/layouts/plain.php';
        $limit             = 100000;

        $paymentDetails     = $this->getPaymentDetails($user, 'income', $from, $to, $limit);
        $paymentsOutDetails = $this->getPaymentDetails($user, 'payout', $from, $to, $limit);

        $fromDate = date("Y-m-d", strtotime($from));
        $toDate   = date("Y-m-d", strtotime($to));

        $to = null; // disable total to filter by date.
        return $this->render('paymentsReport', [
            'user'               => $user,
            'paymentDetails'     => $paymentDetails,
            'paymentsOutDetails' => $paymentsOutDetails,
            'fromDate'           => $fromDate,
            'toDate'             => $toDate,
            'pdf'                => $pdf,
            'totalIncome'        => displayAsCurrency(PaymentDetail::getTotalIncome($user->id, $fromDate, $toDate), 'USD'),
            'totalWithdraw'      => displayAsCurrency(abs(PaymentDetail::getTotalWithdraw($user->id, $fromDate, $toDate)), 'USD'),
            'totalBalance'       => $this->paymentAccountService->getUserMainAmount($user)
        ]);
    }

    public function actionDownloadReport($from, $to)
    {
        return $this->redirect('/workbench/payments/?old=1');
        $user   = $this->getCurrentUser(true);
        $data   = $this->getPaymentDetails($user, 'all', $from, $to, 100000);
        $report = new PaymentsReport();
        $report->setRows($data->getModels());
        $report->setColumnNames([
            'id'              => 'ID',
            'created_at'      => _t('site.user', 'Created On'),
            'original_amount' => _t('site.user', 'Amount'),
            'type'            => _t('app', 'Type'),
            'description'     => _t('app', 'Description')
        ]);

        $reportWriter = new ReportExcelWriter('earnings');
        $reportWriter->setFileName($report->getFileName());
        return $reportWriter->write($report);
    }

    public function actionDownloadPdf($from, $to)
    {
        return $this->redirect('/workbench/payments/?old=1');
        $user     = $this->getCurrentUser();
        $result   = $this->actionReport($from, $to, true);
        $result   = str_replace('href="/css', 'href="' . param('server') . '/css', $result);
        $result   = str_replace('href="/assets', 'href="' . param('server') . '/assets', $result);
        $cacheDir = \Yii::$app->getRuntimePath() . '/payments/';
        FileDirHelper::createDir($cacheDir);
        $fileHtmlCache = $cacheDir . $user->id . '_' . strtotime($from) . '_' . strtotime($to) . '.html';
        $oldPdfFile    = $cacheDir . $user->id . '_' . strtotime($from) . '_' . strtotime($to) . '.pdf';
        if (file_exists($oldPdfFile)) {
            $filePdf = $oldPdfFile;
        } else {
            file_put_contents($fileHtmlCache, $result);
            $filePdf = HtmlToPdf::convert($fileHtmlCache);
        }
        app('response')->setDownloadHeaders(sprintf('Report %s - %s.pdf', $from, $to), 'application/pdf', false);
        return app('response')->sendFile($filePdf);
    }

    /**
     * request to payout to paypal
     *
     * @return string
     */
    public function actionOut()
    {
        return $this->redirect('/workbench/payments/?old=1');
    }

    private function getPaymentDetails($user, $type = 'all', $from = null, $to = null, $limit = 20)
    {
        $paymentsWhere = [
            "and",
            'user_id=' . $user->id,
            'amount!=0',
            "type NOT IN('order')"
            //"type IN('" . implode("','", $typesAwards) . "')"
        ];
        if ($type == 'payout') {
            $paymentsWhere[] = 'amount < 0';
        } else if ($type == 'income') {
            $paymentsWhere[] = 'amount > 0';
        }
        if ($from && $to) {
            $fromDate        = date("Y-m-d 00:00:01", strtotime($from));
            $toDate          = date("Y-m-d 23:59:59", strtotime($to));
            $paymentsWhere[] = sprintf("created_at BETWEEN '%s' AND '%s'", $fromDate, $toDate);
        }
        $paymentDetails       = PaymentDetail::getDataProvider($paymentsWhere, $limit);
        $paymentDetails->sort = [
            'defaultOrder' => [
                'id' => SORT_DESC
            ]
        ];
        return $paymentDetails;
    }

    public function actionLogPaymentButtonPress($uuid)
    {
        PaymentPayPageLog::register($uuid, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], Yii::$app->request->post('invoiceUuid'));
        PaymentPayPageLogStatus::logStatus($uuid, Yii::$app->request->post('vendor'), PaymentPayPageLog::STATUS_PRESS_BUTTON);

        return $this->jsonSuccess();
    }

    public function actionLogPaymentProcessStatus($uuid)
    {
        PaymentPayPageLogProcess::logProcess($uuid, Yii::$app->request->post());
        PaymentPayPageLogStatus::logStatus($uuid, Yii::$app->request->post('vendor'), PaymentPayPageLog::STATUS_PROCESSED);
        return $this->jsonSuccess();
    }
}
