<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.09.17
 * Time: 14:23
 */

namespace frontend\controllers\my;

use common\components\ArrayHelper;
use common\components\BaseController;
use common\components\ErrorListHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\NoActiveModel3d;
use common\components\exceptions\NoCalculatedModel3d;
use common\components\order\anonim\AnonimOrderHelper;
use common\components\order\builder\OrderBuilder;
use common\components\order\predicates\NeedCertificateForDownloadModelPredicate;
use common\components\order\PriceCalculator;
use common\components\orderOffer\OrderOfferLocatorService;
use common\components\ps\locator\Size;
use common\components\serizaliators\Serializer;
use common\interfaces\Model3dBaseInterface;
use common\models\CompanyService;
use common\models\DeliveryType;
use common\models\factories\DeliveryFormFactory;
use common\models\factories\FileFactory;
use common\models\factories\Model3dFactory;
use common\models\factories\Model3dReplicaFactory;
use common\models\factories\PsMachineDeliveryInfoFactory;
use common\models\factories\UserLocationFactory;
use common\models\Model3d;
use common\models\Model3dPart;
use common\models\PaymentInvoice;
use common\models\Promocode;
use common\models\PsPrinter;
use common\models\repositories\Model3dReplicaRepository;
use common\models\repositories\Model3dRepository;
use common\models\serializers\WikiMaterialWidgetSerializer;
use common\models\StoreOrder;
use common\models\UserLocation;
use common\models\WikiMaterial;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\modules\payment\serializers\PayOrderSerializer;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentInvoiceService;
use common\modules\printersList\models\RequestInfo;
use common\modules\promocode\components\PromocodeApplyService;
use common\modules\promocode\components\PromocodeDiscountService;
use common\services\LocationService;
use common\services\Model3dPartService;
use common\services\Model3dService;
use common\services\Model3dViewedStateService;
use common\services\PrinterMaterialService;
use common\services\ReviewService;
use ErrorException;
use frontend\components\cart\CartFactory;
use frontend\components\cart\CartService;
use frontend\components\UserSessionFacade;
use frontend\controllers\catalog\actions\serializers\MaterialColorItemSerializer;
use frontend\controllers\catalog\actions\serializers\Model3dSerializer;
use frontend\controllers\catalog\actions\serializers\ModelStateLiteSerializer;
use frontend\controllers\catalog\actions\serializers\OffersBundleSerializerExt;
use frontend\controllers\catalog\actions\serializers\PrinterColorSerializer;
use frontend\controllers\catalog\actions\serializers\PrinterMaterialGroupSerializer;
use frontend\controllers\catalog\actions\serializers\PrinterMaterialSerializer;
use frontend\controllers\store\serializers\DeliveryAddressSerializer;
use frontend\models\delivery\DeliveryForm;
use frontend\models\model3d\Model3dFacade;
use frontend\models\model3d\Model3dItemForm;
use frontend\models\model3d\PlaceOrderState;
use frontend\models\user\UserFacade;
use lib\delivery\delivery\models\DeliveryRate;
use lib\delivery\parcel\Parcel;
use lib\delivery\parcel\ParcelFactory;
use lib\MeasurementUtil;
use lib\money\Money;
use Psr\Log\InvalidArgumentException;
use Yii;
use yii\base\InvalidValueException;
use yii\base\UserException;
use yii\web\GoneHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class PrintModel3dAjaxController
 *
 * @package frontend\controllers\my
 *
 * @property PromocodeApplyService $promocodeApplyService
 * @property PromocodeDiscountService $promocodeDiscountService
 * @property PaymentAccountService $paymentAccountService
 * @property PaymentInvoiceService $paymentInvoiceService
 * @property PaymentInvoiceRepository $paymentInvoiceRepository
 */
class PrintModel3dAjaxController extends BaseController
{
    use CheckoutCardViewTrait;

    /** @var  Model3dService */
    public $model3dService;

    /** @var FileFactory */
    public $fileFactory;

    /** @var  PlaceOrderState */
    public $placeOrderState;

    /** @var  Model3dRepository */
    public $model3dRepository;

    /** @var Model3dFactory */
    public $model3dFactory;

    /** @var  Model3dViewedStateService */
    public $model3dViewedStateService;

    /** @var  ReviewService */
    public $reviewService;

    /** @var  CartService */
    public $cartService;

    /** @var  PsMachineDeliveryInfoFactory */
    public $psMachineDeliveryInfoFactory;

    /** @var DeliveryFormFactory */
    public $deliveryFormFactory;

    /** @var PriceCalculator */
    public $priceCalculator;

    /**
     * @var string
     */
    public $posUid;

    /**
     * @var LocationService
     */
    private $locationService;

    protected $promocodeApplyService;

    protected $promocodeDiscountService;

    protected $paymentAccountService;

    protected $paymentInvoiceService;

    protected $paymentInvoiceRepository;

    /**
     * @param Model3dService $model3dService
     * @param FileFactory $fileFactory
     * @param PlaceOrderState $model3dPrintState
     * @param Model3dRepository $model3dRepository
     * @param Model3dViewedStateService $model3dViewedStateService
     * @param ReviewService $reviewService
     * @param CartService $cartService
     * @param PsMachineDeliveryInfoFactory $psMachineDeliveryInfoFactory
     * @param DeliveryFormFactory $deliveryFormFactory
     * @param PriceCalculator $priceCalculator
     * @param LocationService $locationService
     * @param Model3dFactory $model3dFactory
     * @param PromocodeApplyService $promocodeApplyService
     * @param PromocodeDiscountService $promocodeDiscountService
     * @param PaymentAccountService $paymentAccountService
     * @param PaymentInvoiceService $paymentInvoiceService
     * @param PaymentInvoiceRepository $paymentInvoiceRepository
     */
    public function injectDependencies(
        Model3dService               $model3dService,
        FileFactory                  $fileFactory,
        PlaceOrderState              $model3dPrintState,
        Model3dRepository            $model3dRepository,
        Model3dViewedStateService    $model3dViewedStateService,
        ReviewService                $reviewService,
        CartService                  $cartService,
        PsMachineDeliveryInfoFactory $psMachineDeliveryInfoFactory,
        DeliveryFormFactory          $deliveryFormFactory,
        PriceCalculator              $priceCalculator,
        LocationService              $locationService,
        Model3dFactory               $model3dFactory,
        PromocodeApplyService        $promocodeApplyService,
        PromocodeDiscountService     $promocodeDiscountService,
        PaymentAccountService        $paymentAccountService,
        PaymentInvoiceService        $paymentInvoiceService,
        PaymentInvoiceRepository     $paymentInvoiceRepository
    )
    {
        $this->model3dService               = $model3dService;
        $this->fileFactory                  = $fileFactory;
        $this->placeOrderState              = $model3dPrintState;
        $this->model3dRepository            = $model3dRepository;
        $this->model3dViewedStateService    = $model3dViewedStateService;
        $this->reviewService                = $reviewService;
        $this->cartService                  = $cartService;
        $this->psMachineDeliveryInfoFactory = $psMachineDeliveryInfoFactory;
        $this->deliveryFormFactory          = $deliveryFormFactory;
        $this->priceCalculator              = $priceCalculator;
        $this->locationService              = $locationService;
        $this->model3dFactory               = $model3dFactory;
        $this->promocodeApplyService        = $promocodeApplyService;
        $this->promocodeDiscountService     = $promocodeDiscountService;
        $this->paymentAccountService        = $paymentAccountService;
        $this->paymentInvoiceService        = $paymentInvoiceService;
        $this->paymentInvoiceRepository     = $paymentInvoiceRepository;
    }

    public function init()
    {
        app()->httpRequestLogger->initCommonRequestLog();
        parent::init();
    }

    public function beforeAction($action)
    {
        if ($posUid = Yii::$app->request->get('posUid', '')) {
            $this->placeOrderState->setStateUid($posUid);
        }
        $this->posUid                = $posUid;
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    /**
     * @return Model3dBaseInterface
     * @throws ErrorException
     * @throws GoneHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \HttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    protected function getOrCreateModel3d()
    {
        try {
            $model3d = $this->placeOrderState->getModel3d();
        } catch (NoActiveModel3d $exception) {
            $sourceDetails = $this->placeOrderState->getModel3dSourceDetails();
            $model3d       = $this->model3dFactory->createModel3dByFilesList([], $sourceDetails);
            $model3d->setScenario(Model3dBaseInterface::SCENARIO_EMPTY_MODEL);
            $model3d->setAnyTextureAllowed(true);
            $this->model3dRepository->save($model3d);
            $this->placeOrderState->setModel3d($model3d);
        }
        return $model3d;
    }

    public function actionCreateModel3d()
    {
        try {
            $model3d = $this->placeOrderState->getModel3d();
            if ($model3d->isEmpty()) {
                $this->placeOrderState->clearModel3d();
                $this->placeOrderState->setModel3dSourceDetails([]);
            }
        } catch (NoActiveModel3d $exception) {
        }
        $model3d = $this->getOrCreateModel3d();
        return $this->jsonReturn(['success' => true, 'model3d' => Model3dSerializer::serialize($model3d), 'model3dState' => ModelStateLiteSerializer::serialize($model3d)]);
    }

    public function actionUploadFile()
    {
        $model3d       = $this->placeOrderState->getModel3d();
        $uploadedFiles = UploadedFile::getInstancesByName('file');
        foreach ($uploadedFiles as $uploadedFile) {
            $file = $this->fileFactory->createFileFromUploadedFile($uploadedFile);
            $this->model3dService->addFile($model3d, $file);
        }
        $this->placeOrderState->setModel3d($model3d);

        return $this->jsonReturn(
            [
                'success' => true,
                'model3d' => Model3dSerializer::serialize($model3d)
            ]
        );
    }

    public function actionDuplicateFile()
    {
        $qty         = (int)\Yii::$app->request->post('qty', 1);
        $model3dPart = $this->getModel3dPart();
        $model3d     = $this->model3dService->duplicateModel3dPart($model3dPart, $qty);
        $this->placeOrderState->setModel3d($model3d);
        return $this->jsonReturn(
            [
                'success' => true,
                'model3d' => Model3dSerializer::serialize($model3d)
            ]
        );
    }

    /**
     * Cancel model3d file, image or part.
     *
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\GoneHttpException
     * @throws \yii\base\Exception
     * @throws \HttpException
     */
    public function actionCancelFile()
    {
        $uid             = \Yii::$app->request->post('uid');
        $fileMd5NameSize = \Yii::$app->request->post('fileMd5NameSize');
        if (!$uid && !$fileMd5NameSize) {
            throw new InvalidArgumentException('Should be setted Uid or fileMd5NameSize');
        }

        $model3d = $this->placeOrderState->getModel3d();
        if ($uid) {
            $object = $this->model3dService->deleteFileByUid($uid);
        } else {
            $object = $this->model3dService->deleteFileByMd5NameSize($model3d, $fileMd5NameSize);
        }

        return $this->jsonReturn(
            ['success' => $object ? true : false]
        );
    }

    /**
     * @return array
     * @throws GoneHttpException
     * @throws NoActiveModel3d
     * @throws NotFoundHttpException
     * @throws \HttpException
     * @throws \backend\components\NoAccessException
     * @throws \yii\base\Exception
     */
    public function actionRestoreFile()
    {
        $uid             = \Yii::$app->request->post('uid');
        $fileMd5NameSize = \Yii::$app->request->post('fileMd5NameSize');
        if (!$uid && !$fileMd5NameSize) {
            throw new NotFoundHttpException('Should be setted Uid.');
        }
        $object = $this->model3dService->restoreFileByUid($uid);
        return $this->jsonReturn([
            'success' => $object ? true : false
        ]);
    }

    /**
     * @param Model3dBaseInterface $model3d
     * @throws \HttpException
     * @throws \yii\web\GoneHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    protected function checkOwner(Model3dBaseInterface $model3d)
    {
        $this->model3dService->tryCanBeOrderedByCurrentUser($model3d);
    }

    /**
     * @return \common\interfaces\Model3dBasePartInterface
     * @throws \HttpException
     * @throws NoActiveModel3d
     * @throws \backend\components\NoAccessException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\GoneHttpException
     */
    protected function getModel3dPart()
    {
        $uid = \Yii::$app->request->post('uid');
        if ($uid) {
            $model3dPart = $this->model3dRepository->getPartByUid($uid);
            $model3d     = $model3dPart->getAttachedModel3d();
            $this->checkOwner($model3d);
            return $model3dPart;
        }

        // Check uploaded files
        $fileMd5NameSize = \Yii::$app->request->post('fileMd5NameSize');

        $model3d = $this->placeOrderState->getModel3d();
        foreach ($model3d->getActiveModel3dParts() as $part) {
            if ($fileMd5NameSize && ($part->file->getMd5NameSize() == $fileMd5NameSize)) {
                return $part;
            }
        }
        throw new NotFoundHttpException('Cant find model3dpart. Should be setted corrent fileId or fileMd5NameSize');
    }

    /**
     * @return array
     * @throws GoneHttpException
     * @throws NoActiveModel3d
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \HttpException
     * @throws \backend\components\NoAccessException
     */
    public function actionQtyChange()
    {
        /** @var Model3dPart $model3dPart */
        $model3dPart      = $this->getModel3dPart();
        $qty              = (int)\Yii::$app->request->post('qty');
        $model3dPart->qty = $qty;
        $model3dPart->safeSave();

        if ((count($model3dPart->model3d->getActiveModel3dParts()) > 1) && ($model3dPart->model3d->model_units === MeasurementUtil::MM)) {
            Model3dService::resetScale($model3dPart->model3d);
            $this->model3dRepository->save($model3dPart->model3d);
        }

        return $this->jsonReturn(
            [
                'success' => true,
                'model3d' => Model3dSerializer::serialize($model3dPart->model3d)
            ]
        );
    }

    /**
     * Scale model3d. Change scale unit from mm to inch, or set scale directly
     *
     * @return array
     * @throws NoActiveModel3d
     * @throws \Exception
     * @throws \backend\components\NoAccessException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionChangeScale()
    {
        $scaleUnit  = \Yii::$app->request->post('scaleUnit', null);
        $scale      = \Yii::$app->request->post('scale', null);
        $model3dUid = \Yii::$app->request->post('model3dUid');

        if ($model3dUid) {
            $model3d = $this->model3dRepository->getByUid($model3dUid);
            $this->checkOwner($model3d);
        } else {
            /** @var Model3d $model3d */
            $model3d = $this->placeOrderState->getModel3d();
        }
        if (!$model3d->getActiveModel3dParts()) {
            return [
                'success' => true,
            ];
        };
        if ($scaleUnit) {
            $this->placeOrderState->setScaleUnit($scaleUnit);
            if ($model3d->model_units !== $scaleUnit) {
                $scale = $model3d->model_units === MeasurementUtil::IN ? MeasurementUtil::INCH_TO_MM : MeasurementUtil::MILLIMETERS_TO_INCHES;
                Model3dService::scaleModel3d($model3d, $scale);
                $model3d->model_units = $scaleUnit;
                $this->model3dRepository->save($model3d);
            }
        } elseif ($scale) {
            Model3dService::resetScale($model3d);
            Model3dService::scaleModel3d($model3d, $scale / 100);
            $this->model3dRepository->save($model3d);
        }
        return $this->jsonReturn(
            [
                'success'       => true,
                'scaledModel3d' => Model3dSerializer::serialize($model3d)
            ]
        );
    }

    /**
     * Set new model3dpart size
     *
     * @return array
     * @throws GoneHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \HttpException
     * @throws \common\components\operations\PostValidateException
     * @throws \common\components\operations\PreValidateException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionChangePartsSize()
    {
        $model3dParts      = \Yii::$app->request->post('parts', []);
        $scaledModel3dList = [];

        foreach ($model3dParts as $model3dPartInfo) {
            $model3dPartUid = $model3dPartInfo['model3dPartUid'];
            $newSizeArray   = $model3dPartInfo['size'];
            $model3dPart    = $this->model3dRepository->getPartByUid($model3dPartUid);
            $model3d        = $model3dPart->model3d;
            $newSize        = Size::create($newSizeArray['width'], $newSizeArray['height'], $newSizeArray['length'], $newSizeArray['measure']);
            $newSize->validateOrFail();
            $newSize = $newSize->convertToMeasurement(MeasurementUtil::MM);

            $this->model3dService->tryCanBeOrderedByCurrentUser($model3d);
            $this->model3dService->setPartSize($model3dPart, $newSize);
            $model3dPart->safeSave();
            $model3d->refresh();
            $scaledModel3dList[$model3d->getUid()] = Model3dSerializer::serialize($model3d);
        }

        return $this->jsonReturn(
            [
                'success'           => true,
                'scaledModel3dList' => array_values($scaledModel3dList)
            ]
        );
    }

    /**
     * @param int $model3dUid
     * @param int $page
     * @param int $pageSize
     * @param string $sortBy
     * @param int $widget
     * @param int $manualSettedPsId
     * @return array
     * @throws GoneHttpException
     * @throws NoActiveModel3d
     * @throws NoCalculatedModel3d
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \Exception
     * @throws \HttpException
     * @throws \backend\components\NoAccessException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionGetPrintersList($model3dUid = 0, $page = 0, $pageSize = 10, $currency = 'usd', $sortBy = 'default', $widget = 0, $manualSettedPsId = 0, $manualSettedPsPrinterId = 0)
    {
        $model3d = $this->model3dRepository->getByUid($model3dUid);
        $this->checkOwner($model3d);

        if (!$model3d->getActiveModel3dParts()) {
            throw new NoActiveModel3d('No active model3dparts');
        }

        if (!$model3d->isCalculatedProperties()) {
            throw new NoCalculatedModel3d('Model3d is not calculated');
        }

        $model3dStateString = \Yii::$app->request->get('modelTextureInfo', '');
        if ($model3dStateString) {
            $modelState = json_decode($model3dStateString, true);
        } else {
            $modelState = \Yii::$app->request->post('modelTextureInfo', []);
        }

        if ($modelState) {
            if (array_key_exists('isAnyTextureAllowed', $modelState)) {
                if ($modelState['isAnyTextureAllowed']) {
                    $model3d->isAnyTextureAllowed = true;
                } else {
                    $model3d->isAnyTextureAllowed = false;
                }
            }
            $this->placeOrderState->setModel3dIsAnyTextureAllowed($model3d->isAnyTextureAllowed);
            PrinterMaterialService::unSerializeModel3dTexture($model3d, $modelState, true);
        }

        $activeOfferItemId = $this->placeOrderState->getActiveOfferItem();

        /** @var OrderOfferLocatorService $offersLocator */
        $offersLocator = \Yii::createObject(OrderOfferLocatorService::class);

        if (Yii::$app->request->post('intlOnly', false) === true) {
            $offersLocator->onlyInternational = true;
        }
        $certOnly = Yii::$app->request->post('certOnly', false);

        if ($widget) {
            $offersLocator->initAnonymousBundle(UserSessionFacade::getLocation());
        } else {
            $predicateOnlyCertificated = new NeedCertificateForDownloadModelPredicate();
            $onlyCertificated          = $predicateOnlyCertificated->test($model3d);
            $offersLocator->initPrintersBundle(UserFacade::getCurrentUser(), UserSessionFacade::getLocation(), [], $onlyCertificated || $certOnly);
        }

        $affiliateSource = $this->placeOrderState->getAffiliateSource();
        //$offersLocator->fixGamebodyResin($this->placeOrderState->getAffiliateSource());

        if ($manualSettedPsId) {
            $offersLocator->manualSettedPsId = $manualSettedPsId;
        }
        if ($manualSettedPsPrinterId) {
            $offersLocator->manualSettedPsPrinterId = $manualSettedPsPrinterId;
        }

        $offersLocator->setCurrency($currency);
        $sortBy = $this->modifySort($sortBy);
        $offersLocator->setSortMode($sortBy);
        $offersLocator->formOffersListForModel($model3d);

        Model3dService::resetTextureMatreialInfo($model3d);
        $allOffersList        = $offersLocator->getAllOffersList();
        $paginationOffersList = $offersLocator->getPaginationList($page * $pageSize, $pageSize);
        $offersLocator->formCosts($paginationOffersList, $model3d, true);

        $model3dItemForm    = Model3dItemForm::create($model3d);
        $model3dViewedState = $this->model3dViewedStateService->registerModel3dViewForm($model3dItemForm);
        $obs                = new Serializer(new OffersBundleSerializerExt(UserSessionFacade::getLocation(), $model3dViewedState, $this->reviewService));
        $offerBundleData    = [
            'offers'     => $obs->serialize($paginationOffersList),
            'totalCount' => count($allOffersList),
        ];

        if ($offersLocator->emptyPrintersListReason) {
            $offerBundleData['messages'] = [$offersLocator->emptyPrintersListReason];
        }

        $allowedMaterials   = $offersLocator->availableColorGroupsForCurrentPrintersList;
        $usedColors         = PrinterMaterialService::getMaterialColorsItemUsedColors($allowedMaterials);
        $usedMaterials      = PrinterMaterialService::getMaterialColorsItemUsedMaterials($allowedMaterials);
        $usedMaterialGroups = PrinterMaterialService::getMaterialColorsItemUsedMaterialGroups($allowedMaterials);

        $allowedMaterialsSerialized = MaterialColorItemSerializer::serialize($allowedMaterials);

        $objectStorage                         = [];
        $objectStorage['PrinterColor']         = PrinterColorSerializer::serialize($usedColors);
        $objectStorage['PrinterMaterialGroup'] = PrinterMaterialGroupSerializer::serialize($usedMaterialGroups);
        $objectStorage['PrinterMaterial']      = PrinterMaterialSerializer::serialize($usedMaterials);

        $model3dState = ModelStateLiteSerializer::serialize($model3d);
        $this->model3dRepository->save($model3d);

        if ($activeOfferItemId && !array_key_exists($activeOfferItemId, $allOffersList)) {
            $activeOfferItemId = null;
            $this->placeOrderState->setActiveOfferItem(null);
        }

        return $this->jsonReturn(
            [
                'success'           => true,
                'offerBundle'       => $offerBundleData,
                'allowedMaterials'  => $allowedMaterialsSerialized,
                'objectStorage'     => $objectStorage,
                'model3dState'      => $model3dState,
                'activeOfferItemId' => $activeOfferItemId,
                'modelWeight'       => $model3d->getWeight(),
                'timeAround'        => $this->model3dService->timeAround($model3d, $offersLocator?->location?->geoCountry, $offersLocator->onlyInternational,)
            ]
        );
    }

    /**
     * @return array
     * @throws ErrorException
     * @throws GoneHttpException
     * @throws NoActiveModel3d
     * @throws NotFoundHttpException
     * @throws UserException
     * @throws \Exception
     * @throws \HttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionSaveCart()
    {
        $modelState  = \Yii::$app->request->post('modelTextureInfo', []);
        $psPrinterId = \Yii::$app->request->post('psPrinterId', []);
        /** @var Model3d $model3d */
        $model3d = $this->placeOrderState->getModel3d();

        $printer = PsPrinter::getValidated($psPrinterId);
        $this->placeOrderState->setActiveOfferItem($psPrinterId);

        if ($modelState) {
            PrinterMaterialService::unSerializeModel3dTexture($model3d, $modelState);
        }

        $this->model3dRepository->save($model3d);
        $model3dReplica = Model3dReplicaFactory::createModel3dReplica($model3d);

        $model3dReplica->setStoreUnit($model3d->storeUnit);
        if (PrinterMaterialService::isNotSettedPrinterMaterial($model3dReplica)) {
            Model3dService::setCheapestMaterialInPrinter($model3dReplica, $printer);
        }
        Model3dReplicaRepository::save($model3dReplica);

        $cart     = CartFactory::createCart($this->posUid);
        $cartItem = CartFactory::createCartItem($model3dReplica, $printer->companyService, 1, true);
        $cart->addPosition($cartItem);
        if (!$cart->safeSave()) {
            throw new ErrorException('Save cart error: ' . json_encode($cart->getErrors()));
        };

        $deliveryInfo            = $this->getDeliveryInfo($printer->companyService, $model3dReplica);
        $deliveryInfo['success'] = true;

        return $this->jsonReturn($deliveryInfo);
    }

    public function getDeliveryInfo(CompanyService $psMachine, Model3dBaseInterface $model3dReplica)
    {
        $psMachineDeliveryInfo = $this->psMachineDeliveryInfoFactory->createByPsMachine($psMachine);
        $deliveryForm          = $this->deliveryFormFactory->createByPsMachine($psMachine);

        $this->placeOrderState->getDeliveryFormState($deliveryForm);
        if (!$deliveryForm->phoneCountyIso) {
            $deliveryForm->phoneCountyIso = $deliveryForm->country;
        }
        if ($deliveryForm->phone && !$deliveryForm->phoneLineNumber) {
            $deliveryForm->phoneLineNumber = $deliveryForm->phone;
        }

        $psMachineDeliveryInfoSerialized       = ArrayHelper::toArray($psMachineDeliveryInfo);
        $deliveryFormSerialized                = ArrayHelper::toArray($deliveryForm);
        $deliveryFormSerialized['contactName'] = $deliveryFormSerialized['contact_name'];
        $deliveryFormSerialized['countryIso']  = $deliveryFormSerialized['country'];


        if (!in_array($deliveryForm->deliveryType, $psMachineDeliveryInfo->deliveryTypes)) {
            $deliveryFormSerialized['deliveryType'] = reset($psMachineDeliveryInfo->deliveryTypes);
        }
        $deliveryFormSerialized['deliveryTypeCarrier'] = $psMachineDeliveryInfo->getCarrierByDeliveryType($deliveryForm->deliveryType);

        return [
            'psMachineDeliveryInfo' => $psMachineDeliveryInfoSerialized,
            'deliveryForm'          => $deliveryFormSerialized
        ];
    }

    public function actionGetDeliveryInfo()
    {
        $cart = $this->cartService->getSessionCart($this->posUid);
        if (!$cart) {
            throw new BusinessException('Please select printer. Cart is empty.', 418);
        }
        $psMachine      = $cart->getMachine();
        $model3dReplica = $cart->getModel3d();

        $deliveryInfo            = $this->getDeliveryInfo($psMachine, $model3dReplica);
        $deliveryInfo['success'] = true;

        return $this->jsonReturn($deliveryInfo);
    }

    /**
     * @return array
     * @throws HttpException
     * @throws UserException
     * @throws \Exception
     * @throws \common\components\exceptions\IllegalStateException
     * @throws \frontend\components\cart\exceptions\CartEmptyException
     * @throws \lib\geo\GeoNamesException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSaveDeliveryInfo()
    {
        $cart = $this->cartService->getSessionCart($this->posUid);
        if (!$cart) {
            throw new BusinessException('Please select printer. Cart is empty.', 418);
        }
        $deliveryInfo = \Yii::$app->request->post('deliveryForm', []);

        $psMachine = $cart->getMachine();

        $deliveryForm = $this->deliveryFormFactory->createByPsMachine($psMachine);
        $deliveryForm->loadInfo($deliveryInfo);

        $this->placeOrderState->setDeliveryFormState($deliveryForm);
        if ($deliveryForm->isInternational()) {
            $deliveryForm->deliveryType = DeliveryType::INTERNATIONAL;
        }
        $deliveryForm->validate();
        $errors = $deliveryForm->getErrors();
        if ($errors) {
            $validateErrors = ErrorListHelper::addErrorsFormPrefix($errors, 'deliveryform');
            return $this->jsonReturn(
                [
                    'validateErrors' => $validateErrors
                ],
                422
            );
        }

        if ($deliveryForm->getValidatedAddress() && $deliveryForm->getValidatedAddress()->isEasyPost()) {
            return $this->jsonReturn(
                [
                    'validatedAddress' => DeliveryAddressSerializer::serialize($deliveryForm->getValidatedAddress())
                ],
                422
            );
        }

        $user    = AnonimOrderHelper::resolveOrderUser();
        $machine = $cart->getMachine();

        if ($machine->isPrinter() && !Model3dFacade::isCanPrintModelByTextureAndSize($machine->asPrinter(), $cart->getModel3d())) {
            throw new BusinessException("This printer cant't print model");
        }

        $printPrice = $this->priceCalculator->calculateModel3dPrintPriceWithPackageFee($cart->getModel3d(), $cart->getMachine()->asPrinter());
        $rate       = $this->getShippingRate(
            $machine,
            $deliveryForm,
            $printPrice,
            ParcelFactory::createFromCart($cart)
        );
        if (!$rate) {
            throw new BusinessException("Invalid delivery rate params");
        }

        $rate->deliveryForm   = $deliveryForm;
        $rate->deliveryParams = $machine->asDeliveryParams();

        $order = OrderBuilder::create()
            ->customer($user, $deliveryForm->email)
            ->comment($deliveryForm->comment)
            ->canChangeSupplier($cart->canChangePs())
            ->cart($cart)
            ->manualPs($this->placeOrderState->getPsIdOnly())
            ->delivery($rate)
            ->affiliateSource($this->placeOrderState->getAffiliateSource())
            ->sortMode($this->placeOrderState->getSortMode())
            ->buildByItems();
        $this->placeOrderState->setOrderId($order->id);
        $this->locationService->changeUserLocationByDelivery($deliveryForm);
        return $this->jsonReturn(
            [
                'success' => true
            ]
        );
    }

    /**
     * @param CompanyService $machine
     * @param DeliveryForm $deliveryForm
     * @param Money $printPrice
     * @param \lib\delivery\parcel\Parcel $parcel
     *
     * @return DeliveryRate|null
     * @throws UserException
     * @throws \yii\base\InvalidConfigException
     */
    private function getShippingRate(CompanyService $machine, DeliveryForm $deliveryForm, Money $printPrice, Parcel $parcel)
    {
        $userCurrency = UserSessionFacade::getCurrency();
        $formKey      = $deliveryForm->hashKey();
        $key          = md5('g1' . $machine->id . $machine->getDeliveryOptionsHash() . $formKey . serialize($parcel->attributes) . $userCurrency);
        if (($shippingCache = app('cache')->get($key))) {
            return $shippingCache;
        }
        $rate = Yii::$app->deliveryService->getOrderRate($machine->asDeliveryParams(), $deliveryForm, $printPrice, $parcel, $userCurrency);
        app('cache')->set($key, $rate);
        return $rate;
    }

    /**
     * @throws GoneHttpException
     * @throws UserException
     * @throws \HttpException
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPromoCode()
    {
        try {
            $model3d = $this->placeOrderState->getModel3d();
            $model3d->refresh();
        } catch (NoActiveModel3d $e) {
            throw new BusinessException(_t('site.store', 'Order not found. Refresh the page.'));
        }

        if (\count($model3d->getCalculatedModel3dParts()) < 1) {
            throw new BusinessException(_t('site.store', 'Order not found. Refresh the page.'));
        }

        $storeOrder = $this->placeOrderState->getOrder();
        $cart       = $this->cartService->getSessionCart($this->posUid);

        if (!$cart || !$storeOrder) {
            throw new BusinessException(_t('site.store', 'Order not found. Refresh the page.'));
        }

        $promocodeString = Yii::$app->request->post('promocode');

        if ($promocodeString) {
            $this->addPromoCodeStoreOrder($storeOrder, $promocodeString);
        } elseif (Yii::$app->request->post('deletePromoCode')) {
            $this->deletePromocodeStoreOrder($storeOrder);
        } else {
            return $this->jsonError(_t('site.store', 'Promo code not found or not active.'));
        }

        return $this->jsonSuccess([
            'payOrder'     => PayOrderSerializer::serialize($storeOrder),
            'checkoutCard' => $this->getCheckoutCardView($storeOrder, Yii::$app->request->get('widget') === 'widget')
        ]);
    }

    /**
     * @param StoreOrder $storeOrder
     * @param $promocodeString
     *
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function addPromoCodeStoreOrder(StoreOrder $storeOrder, $promocodeString)
    {
        $promocode = Promocode::findOne(['code' => $promocodeString]);

        if (!$promocode || !$promocode->isActive()) {
            throw new BusinessException(_t('site.store', 'Promo code not found or not active.'));
        }

        $invoice = $storeOrder->getPrimaryInvoice();

        if ($invoice->storeOrderPromocode) {
            return;
        }

        if ($promocode->discount_currency !== $invoice->currency) {
            throw new BusinessException(_t('site.store', 'Promo code only for ') . $promocode->discount_currency);
        }

        $res = $this->promocodeApplyService->validatePromoByPrices($promocode, $invoice);

        if (!$res) {
            throw new BusinessException(_t('site.store', 'This promo code could not be applied for this order.'));
        }

        // try validation
        $this->promocodeApplyService->validateByTotalPrice($promocode, $invoice);
        $this->promocodeApplyService->validatePromo($promocode);

        // set Discounts
        $this->promocodeDiscountService->applyPromocodeDiscounts($storeOrder, $promocode);
    }

    /**
     * @param StoreOrder $storeOrder
     *
     * @throws UserException
     * @throws \Throwable
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\db\StaleObjectException
     */
    protected function deletePromocodeStoreOrder(StoreOrder $storeOrder)
    {
        /** @var PaymentInvoice $invoice */
        $invoice = $storeOrder->getPrimaryInvoice();

        // try validation
        $this->promocodeApplyService->validateBeforeRemove($invoice);

        // remove  Discounts
        $this->promocodeDiscountService->removePromocodeDiscounts($storeOrder);
    }

    /**
     * @return array
     * @throws UserException
     * @throws \common\components\exceptions\InvalidModelException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionPayOrder()
    {
        /** @var StoreOrder $storeOrder */
        $storeOrder = $this->placeOrderState->getOrder();

        if (!$storeOrder) {
            throw new BusinessException(_t('site.store', 'Order not found'));
        }

        StoreOrder::validateBeforeCheckout($storeOrder);

        return $this->jsonSuccess([
            'payOrder'     => PayOrderSerializer::serialize($storeOrder),
            'checkoutCard' => $this->getCheckoutCardView($storeOrder, Yii::$app->request->get('widget') === 'widget')
        ]);
    }

    /**
     * @return array
     * @throws UserException
     */
    public function actionBillingDetailsPrint()
    {
        return $this->billingDetailsPrint();
    }

    public function actionMaterialsGuide()
    {
        $allowsMaterials    = Yii::$app->request->get('materials');
        $type               = Yii::$app->request->get('type');
        $wikiMaterialsQuery = WikiMaterial::find()->active();
        if ($allowsMaterials) {
            $allowsMaterials = explode(',', $allowsMaterials);
            $wikiMaterialsQuery->joinWith('printerMaterials')
                ->andWhere(['printer_material.id' => $allowsMaterials]);
        }
        if ($type) {
            $wikiMaterialsQuery->andWhere(['wiki_material.type_of_manufacturer' => $type]);
        }

        $wikiMaterialsQuery->showWidget();

        $wikiMaterials = $wikiMaterialsQuery->all();
        return $this->jsonSuccess(['wikiMaterials' => WikiMaterialWidgetSerializer::serialize($wikiMaterials)]);
    }

    public function actionGetDeliveryRate()
    {
        $cart = $this->cartService->getSessionCart($this->posUid);
        if (!$cart) {
            throw new BusinessException('Please select printer. Cart is empty.', 418);
        }
        $psMachine    = $cart->getMachine();
        $deliveryForm = $this->deliveryFormFactory->createByPsMachine($psMachine);
        $this->placeOrderState->getDeliveryFormState($deliveryForm);
        $deliveryForm->shipping = DeliveryType::STANDARD; // Separated standard and express
        $printPrice             = $this->priceCalculator->calculateModel3dPrintPriceWithPackageFee($cart->getModel3d(), $cart->getMachine()->asPrinter());
        if (!$printPrice) {
            throw new BusinessException("Price not set");
        }
        $model3d = $this->placeOrderState->getModel3d();
        $rate    = $this->getShippingRate(
            $psMachine,
            $deliveryForm,
            $printPrice,
            ParcelFactory::createFromModel($model3d)
        );
        if (!$rate) {
            throw new BusinessException("Invalid delivery rate params");
        }
        $rateMoney = $rate->getCost();
        $rates     = [
            'standardShipping' => $rateMoney
        ];
        if ($deliveryForm->isUsaToUsa($psMachine) && $rateMoney->notZero()) {
            $rates['expressShipping'] = Money::create(DeliveryRate::EXPRESS_RATE, UserSessionFacade::getCurrency());
        }
        return $this->jsonSuccess($rates);
    }

    /**
     * @param string $sortBy
     * @return mixed
     */
    protected function modifySort(string $sortBy): mixed
    {
        if ($sortBy === RequestInfo::SORT_STRATEGY_2021_DEFAULT) {
            $sortBy = RequestInfo::SORT_STRATEGY_DEFAULT;
        }
        $this->placeOrderState->setSortMode($sortBy);
        return $sortBy;
    }
}