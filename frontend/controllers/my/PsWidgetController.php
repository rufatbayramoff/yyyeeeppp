<?php
namespace frontend\controllers\my;

use common\components\exceptions\BusinessException;
use common\components\FaceBookHelper;
use common\components\WidgetStatisticsRegistrar;
use common\interfaces\Model3dBaseInterface;
use common\models\PsCncMachine;
use common\models\PsFacebookPage;
use common\models\CompanyService;
use common\modules\cnc\widgets\CncWidget;
use common\services\PsPrinterService;
use common\models\WidgetStatistics;
use common\traits\TransactedControllerTrait;
use common\models\Ps;
use common\models\PsPrinter;
use frontend\models\ps\PsFacade;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\InvalidParamException;
use yii\base\UserException;
use yii\web\HttpException;
use yii\web\NotAcceptableHttpException;

/**
 * Print Service External Widget Controller
 *
 */
class PsWidgetController extends \common\components\BaseController
{
    use TransactedControllerTrait {
        beforeAction as beforeActionTransactionStart;
    }

    protected $viewPath = '@frontend/views/ps/psWidget';

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                    ]
                ]
            ]
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return $this->beforeActionTransactionStart($action);
    }

    /**
     *
     */
    protected function initLayout()
    {
        $this->layout = 'plain.php';
        $this->module->layout = 'plain.php';
        Yii::$app->params['additionalBodyClass'] = 'item-rendering-external-widget';
    }

    /**
     * @return string|\yii\web\Response
     * @throws BusinessException
     * @throws UserException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionFacebook()
    {
        $this->initLayout();
        $signedRequest = Yii::$app->request->post('signed_request', Yii::$app->request->get('signed_request', null));
        if (!$signedRequest) {
            throw new InvalidParamException('Signed request not setted');
        }
        $signedRequestInfo = FaceBookHelper::decodeSignedRequest($signedRequest, Yii::$app->params['facebook_ps_secrect']);
        $pageId = $signedRequestInfo['page']['id'];
        $facebookPage = PsFacebookPage::findOne(['facebook_page_id' => $pageId]);
        if (!$facebookPage) {
            if (!$signedRequestInfo['page']['admin']) {
                throw new BusinessException('Page owner should connect this application to his print service.');
            }

            // Not signed PS
            $currentUser = UserFacade::getCurrentUser();
            $ps = null;
            if ($currentUser) {
                $ps = $currentUser->ps;
                $ps = reset($ps);
            }

            if (!$ps) {
                $validateMessage = null;
                if ($psLink = Yii::$app->request->post('psLink', null)) {
                    $ps = PsPrinterService::getPsByLink($psLink);
                    if ($ps) {
                        PsPrinterService::addFacebookPage($ps, $pageId);
                        return $this->render(
                            'psLinked',
                            [
                                'ps' => $ps
                            ]
                        );
                    } else {
                        $validateMessage = _t('site.ps', 'We couldn’t find a print service with this link on Treatstock. Please try again or contact support@treatstock.com.');
                    }
                } elseif ($psLink !== null) {
                    $validateMessage = _t('site.ps', 'Print service link cannot be empty');
                }

                return $this->render(
                    'psLinkManual',
                    [
                        'psLink'          => $psLink,
                        'validateMessage' => $validateMessage,
                        'signedRequest'   => $signedRequest
                    ]
                );
            }

            /** @var Ps $ps */
            if (Yii::$app->request->post('psLinkApprove', null)) {
                PsPrinterService::addFacebookPage($ps, $pageId);
                return $this->render(
                    'psLinked',
                    [
                        'ps' => $ps
                    ]
                );
            } else {
                return $this->render(
                    'psLink',
                    [
                        'ps'            => $ps,
                        'signedRequest' => $signedRequest
                    ]
                );
            }
        }
        $ps = $facebookPage->ps;
        $hostingPage = 'https://www.facebook.com/'.$facebookPage->facebook_page_id.'/app/'.param('facebook_ps_print_application_id');
        WidgetStatisticsRegistrar::registerWidgetStatistics(WidgetStatistics::TYPE_PS_FACEBOOK, $ps, $hostingPage);
        $viewVars['ps'] = $ps;
        $viewVars['psPrinter'] = null;
        $viewVars['model3dSource'] = Model3dBaseInterface::SOURCE_PS_FACEBOOK;
        return $this->renderAdaptive(
            'psWidgetUploadPage',
            $viewVars
        );
    }

    /**
     * @return array
     * @throws BusinessException
     */
    protected function initPsViewVars($psId, $psPrinterId)
    {
        $psPrinter = null;
        $ps = null;

        if ($psId) {
            $ps = Ps::tryFindByPk($psId);
            if ((!$ps->isActive()) || (!$ps->user->getIsActive())) {
                throw new BusinessException(_t('common.ps', 'The widget is currently unavailable due to the manufacturer being inactive on Treatstock.'));
            }
        } elseif ($psPrinterId) {
            $psPrinter = PsPrinter::tryFindByPk($psPrinterId);
            if (!$psPrinter->isCanPrintInStore()) {
                throw new BusinessException(_t('common.ps', 'Widget is currently unavailable as the 3D printer is inactive on Treatstock.'));
            }
        } else {
            throw new InvalidParamException('Please set psId or psPrinterId parameter');
        }

        return [
            'ps'            => $ps,
            'psPrinter'     => $psPrinter,
            'model3dSource' => Model3dBaseInterface::SOURCE_PS_WIDGET
        ];
    }

    public function actionHelpCenter()
    {
        $this->initLayout();
        $psId = Yii::$app->request->get('psId', false);
        if ($psId !== false) {
            if (is_guest()) {
                $psId = 1;
            } else {
                $user = UserFacade::getCurrentUser();
                $psId = count($user->ps) > 0 ? $user->ps[0]->id : null;
                $this->view->title = 'Print service #' . $psId . '. Treatstock';
            }
            if ($psId === null) {
                $psId = 1;
            }
        }
        $psPrinterId = Yii::$app->request->get('psPrinterId', false);
        $viewVars = $this->initPsViewVars($psId, $psPrinterId);

        return $this->renderAdaptive(
            'psWidgetUploadPage',
            $viewVars
        );
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $psId = Yii::$app->request->get('psId', false);
        $psPrinterId = Yii::$app->request->get('psPrinterId', false);
        if ($psId && !$psPrinterId) {
            $ps = Ps::tryFindByPk($psId);
            return $this->redirect(PsPrinterService::getPsWidgetUrl($ps));
        }
        $this->initLayout();
        $viewVars = $this->initPsViewVars($psId, $psPrinterId);

        if ($viewVars['ps']) {
            WidgetStatisticsRegistrar::registerWidgetStatistics(WidgetStatistics::TYPE_PS_SITE, $viewVars['ps']);
        } elseif ($viewVars['psPrinter']) {
            WidgetStatisticsRegistrar::registerWidgetStatistics(WidgetStatistics::TYPE_PRINTER_SITE, $viewVars['psPrinter']);
        }

        return $this->renderAdaptive(
            'psWidgetUploadPage',
            $viewVars
        );
    }

    public function actionCnc($widgetCncId)
    {
        $this->initLayout();
        $cncService = CompanyService::tryFindByPk($widgetCncId);
        if($cncService->type!=CompanyService::TYPE_CNC){
            throw new HttpException(400, 'Wrong id');
        }
        return $this->renderContent(CncWidget::widget([
            'cncMachine' => $cncService->psCncMachine
            ])
        );
    }
}
