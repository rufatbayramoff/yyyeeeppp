<?php
/**
 * User: nabi
 */

namespace frontend\controllers\my;


use yii\web\Controller;

class CompanyOrderController extends Controller
{
    public function beforeAction($action)
    {

        $this->redirect('/workbench/service-orders');
        return true;
    }
    public function actionPrintRequests()
    {
        $this->redirect('/workbench/service-orders');
        return;
    }
}