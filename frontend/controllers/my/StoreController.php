<?php

namespace frontend\controllers\my;

use common\components\BaseController;
use common\models\Model3d;
use frontend\models\model3d\Model3dPublishForm;
use frontend\models\store\StoreFacade;
use frontend\models\user\UserFacade;
use yii\base\Exception;
use yii\web\NotFoundHttpException;

/**
 * user shop controller - user actions to publish, unpublish models to store
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class StoreController extends BaseController
{

    protected $viewPath = '@frontend/views/user/my/collection';

    public $layout = '@frontend/views/user/my/collection/layout.php';

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'publish' => ['post'],
                    'remove'  => ['post'],
                ]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            // If you want to change it only in one or few actions, add additional check
            \Yii::info('redir', sprintf('From  %s', app('request')->referrer));
            return $this->redirect('/mybusiness/products');
            exit;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        $user = $this->getCurrentUser('/');
        $params = [
            'user' => $user
        ];

        $query = Model3d::find()->myStore($user->id);
        $query->hasActiveModel3dParts();
        $query->orderBy('updated_at DESC');
        $items = $query->all();
        $items = StoreFacade::convertToCollections($items);
        return $this->render('store', compact('params', 'items'));
    }


    /**
     * Publish 3D Model action
     * - check model3d ready to be published
     * - publish model3d
     * - add to moderation
     * - subscribe user to be notified after moderation
     *
     * @param type $id
     * @return type
     * @throws NotFoundHttpException
     */
    public function actionPublish($id)
    {
        $user = $this->getCurrentUser(true);
        $params = [
            'user'      => $user,
            'published' => false,
            'redirect'  => false
        ];

        $model3dObj = Model3d::tryFindByPk($id);
        $publishForm = new Model3dPublishForm();
        $msg = _t('front.publish', 'Your model is published and will be available in store after moderation. Thank you!');
        $params['published'] = $publishForm->validatePublish($model3dObj);
        if ($params['published'] == false && app('request')->isPost) {
            $res = true;
            $msgTax = '';
            try {
                $post = app('request')->post();
                $publishForm->load($post);
                if (!empty($publishForm->pricePerPrint)) {
                    // TURNED OFF issue #1308
                    // $msgTax = UserFacade::getTaxMessage($user);
                }
                $publishForm->model3dId = $model3dObj->id;
                $publishForm->publish();
                $this->setFlashMsg(true, $msg);
            } catch (Exception $ex) {
                $res = false;
                $msg = $ex->getMessage();
                $this->setFlashMsg(false, $msg);
            }

            if ($this->isAjax) {
                return $this->jsonReturn([
                    'success'    => $res,
                    'message'    => $msg,
                    'messageTax' => $msgTax
                ]);
            }
        }
    }

    /**
     * remove model3d from store position
     *
     * @param type $modelId
     * @return type
     */
    public function actionRemove($modelId)
    {
        $result = true;
        $msg = _t('front.store', ' model removed from store');

        /**
         *
         * @var Model3d $model3dObj
         */
        $model3dObj = Model3d::tryFindByPk($modelId);

        $msg = $model3dObj->title . $msg;

        if (!$model3dObj->storeUnit) {
            $result = false;
            $msg = _t('front.store', 'Model is not published to store');
        }
        try {
            if (!UserFacade::isObjectOwner($model3dObj)) {
                throw new \yii\base\UserException("Sorry, but you don't have access to perform this operation.");
            }
            $userId = UserFacade::getCurrentUserId();
            $result = StoreFacade::removeUnit($model3dObj);
        } catch (\Exception $e) {
            logException($e, 'store');
            $result = false;
            $msg = _t("site.store", "This store unit cannot be removed from store, because orders exists");
        }

        if ($this->isAjax) {
            return $this->jsonReturn([
                'success' => $result,
                'message' => $msg
            ]);
        }
        $this->setFlashMsg($result, $msg);
        return $this->redirect(app('request')->referrer);
    }

    public function actionWidget()
    {
        return $this->render('widget', [
        ]);
    }
}
