<?php

namespace frontend\controllers\my;

use common\components\ArrayHelper;
use common\components\BaseController;
use common\components\ErrorListHelper;
use common\components\exceptions\BusinessException;
use common\components\exceptions\OnlyPostRequestException;
use common\components\order\anonim\AnonimOrderHelper;
use common\components\order\builder\OrderBuilder;
use common\models\CompanyService;
use common\models\CuttingMachine;
use common\models\CuttingPack;
use common\models\CuttingPackFile;
use common\models\CuttingPackPage;
use common\models\CuttingPackPageImage;
use common\models\DeliveryType;
use common\models\factories\DeliveryFormFactory;
use common\models\factories\FileFactory;
use common\models\factories\PsMachineDeliveryInfoFactory;
use common\models\PaymentInvoice;
use common\models\StoreOrder;
use common\models\User;
use common\modules\affiliate\services\AffiliateSessionService;
use common\modules\cutting\factories\CuttingPackFactory;
use common\modules\cutting\models\PlaceCuttingOrderState;
use common\modules\cutting\repositories\CuttingPackRepository;
use common\modules\cutting\serializers\CuttingPackSerializer;
use common\modules\cutting\serializers\OffersBundleSerializer;
use common\modules\cutting\services\CuttingMachineService;
use common\modules\cutting\services\CuttingOffersLocatorService;
use common\modules\cutting\services\CuttingPackService;
use common\modules\payment\repositories\PaymentInvoiceRepository;
use common\modules\payment\serializers\PayOrderSerializer;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use common\services\LocationService;
use frontend\components\FrontendWebView;
use frontend\components\UserSessionFacade;
use frontend\controllers\store\serializers\DeliveryAddressSerializer;
use frontend\models\delivery\DeliveryForm;
use frontend\models\user\UserFacade;
use lib\delivery\parcel\Parcel;
use lib\money\Money;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\UserException;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class OrderLaserCuttingAjaxController
 *
 * @package frontend\controllers\my
 *
 * @property FrontendWebView $view
 */
class OrderLaserCuttingAjaxController extends BaseController
{
    use CheckoutCardViewTrait;

    /** @var  PlaceCuttingOrderState */
    protected $placeOrderState;

    /** @var  PaymentService */
    protected $paymentService;

    /** @var CuttingPackFactory */
    protected $cuttingPackFactory;

    /** @var FileFactory */
    protected $fileFactory;

    /** @var CuttingPackRepository */
    protected $cuttingPackRepository;

    /** @var CuttingPackService */
    protected $cuttingPackService;

    /** @var CuttingOffersLocatorService */
    protected $cuttingOffersService;

    /** @var CuttingMachineService */
    protected $cuttingMachineService;

    /** @var  AffiliateSessionService */
    protected $affiliateSessionService;

    /** @var PsMachineDeliveryInfoFactory */
    protected $psMachineDeliveryInfoFactory;

    /** @var DeliveryFormFactory */
    protected $deliveryFormFactory;

    /** @var LocationService */
    protected $locationService;

    /** @var string */
    protected $posUid = '';

    /** @var PaymentAccountService */
    protected $paymentAccountService;

    /** @var PaymentInvoiceRepository */
    protected $paymentInvoiceRepository;

    public function injectDependencies(
        FileFactory $fileFactory,
        PlaceCuttingOrderState $placeOrderState,
        PaymentService $paymentService,
        CuttingPackFactory $cuttingPackFactory,
        CuttingPackRepository $cuttingPackRepository,
        CuttingPackService $cuttingPackService,
        CuttingOffersLocatorService $cuttingOffersService,
        CuttingMachineService $cuttingMachineService,
        PsMachineDeliveryInfoFactory $psMachineDeliveryInfoFactory,
        DeliveryFormFactory $deliveryFormFactory,
        LocationService $locationService,
        PaymentAccountService $paymentAccountService,
        PaymentInvoiceRepository $paymentInvoiceRepository
    )
    {
        $this->placeOrderState              = $placeOrderState;
        $this->paymentService               = $paymentService;
        $this->cuttingPackFactory           = $cuttingPackFactory;
        $this->cuttingPackRepository        = $cuttingPackRepository;
        $this->cuttingPackService           = $cuttingPackService;
        $this->fileFactory                  = $fileFactory;
        $this->cuttingOffersService         = $cuttingOffersService;
        $this->cuttingMachineService        = $cuttingMachineService;
        $this->psMachineDeliveryInfoFactory = $psMachineDeliveryInfoFactory;
        $this->deliveryFormFactory          = $deliveryFormFactory;
        $this->locationService              = $locationService;
        $this->paymentAccountService        = $paymentAccountService;
        $this->paymentInvoiceRepository     = $paymentInvoiceRepository;
    }

    public function beforeAction($action)
    {
        if ($posUid = Yii::$app->request->get('posUid', '')) {
            $this->placeOrderState->setStateUid($posUid);
        }
        $this->posUid                = $posUid;
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    public function actionCreate()
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $cuttingPack = $this->placeOrderState->getCuttingPack();
        if (!$cuttingPack) {
            $cuttingPack = $this->cuttingPackFactory->createForCurrentUser();
            $cuttingPack->validateOrFailed();
            $this->cuttingPackRepository->save($cuttingPack);
            $this->placeOrderState->setCuttingPack($cuttingPack);
        }

        return $this->jsonReturn([
            'success'     => true,
            'cuttingPack' => CuttingPackSerializer::serialize($cuttingPack)
        ]);
    }

    public function actionUploadFile()
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $cuttingPack   = $this->resolveCuttingPack();
        $uploadedFiles = UploadedFile::getInstancesByName('file');
        foreach ($uploadedFiles as $uploadedFile) {
            $file = $this->fileFactory->createFileFromUploadedFile($uploadedFile);
            $this->cuttingPackService->validateFile($file);
            $this->cuttingPackService->addFile($cuttingPack, $file, Yii::$app->request->post('uuid'));
        }

        return $this->jsonReturn(
            [
                'success'     => true,
                'cuttingPack' => CuttingPackSerializer::serialize($cuttingPack)
            ]
        );
    }

    public function actionRestoreFile()
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $cuttingPack     = $this->resolveCuttingPack();
        $cuttingPackFile = $this->cuttingPackService->restoreFileByUidOrMd5($cuttingPack, Yii::$app->request->post('uid'), Yii::$app->request->post('fileMd5NameSize'));
        return $this->jsonReturn(
            [
                'success'         => true,
                'cuttingPackFile' => CuttingPackSerializer::serialize($cuttingPackFile)
            ]
        );
    }

    public function actionCancelFile()
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $cuttingPack     = $this->resolveCuttingPack();
        $cuttingPackFile = $this->cuttingPackService->cancelFileByUidOrMd5($cuttingPack, Yii::$app->request->post('uid'), Yii::$app->request->post('fileMd5NameSize'));
        return $this->jsonReturn(
            [
                'success'         => true,
                'cuttingPackFile' => CuttingPackSerializer::serialize($cuttingPackFile)
            ]
        );
    }

    public function actionQtyChange()
    {
        if (!Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }
        $cuttingPack     = $this->resolveCuttingPack();
        $cuttingPackFile = $this->cuttingPackService->updateFileQty($cuttingPack, Yii::$app->request->post('uid'), Yii::$app->request->post('qty'));
        return $this->jsonReturn(
            [
                'success'         => true,
                'cuttingPackFile' => CuttingPackSerializer::serialize($cuttingPackFile)
            ]
        );
    }

    public function actionPing()
    {
        $cuttingPack = $this->resolveCuttingPack();
        return $this->jsonReturn(
            [
                'success'     => true,
                'cuttingPack' => CuttingPackSerializer::serialize($cuttingPack)
            ]
        );
    }

    public function actionSvgDownload(string $packPageUuid)
    {
        $forceOriginal   = Yii::$app->request->get('forceOriginal', false);
        $cuttingPackPage = CuttingPackPage::tryFind(['uuid' => $packPageUuid]);
        $cuttingPackFile = $cuttingPackPage->cuttingPackFile;
        if (!UserFacade::isObjectOwner($cuttingPackFile->cuttingPack)) {
            throw new UserException('No access permission');
        }

        $response         = Yii::$app->response;
        $response->format = Response::FORMAT_RAW;
        $path             = $cuttingPackPage->file->getLocalTmpFilePath();

        if ($cuttingPackPage->activeCuttingPackParts && $cuttingPackPage->selections && !$forceOriginal) {
            return $response->sendContentAsFile($cuttingPackPage->selections, $cuttingPackPage->file->getFileName());
        } else {
            return $response->sendFile($path, $cuttingPackPage->file->getFileName());
        }
    }

    public function actionPartImageDownload(string $uuid)
    {
        $cuttingPackPageImage = CuttingPackPageImage::tryFind(['uuid' => $uuid]);
        if (!UserFacade::isObjectOwner($cuttingPackPageImage->cuttingPackPage->cuttingPackFile->cuttingPack)) {
            throw new UserException('No access permission');
        }
        $response         = Yii::$app->response;
        $response->format = Response::FORMAT_RAW;
        $path             = $cuttingPackPageImage->file->getLocalTmpFilePath();

        return $response->sendFile($path, $cuttingPackPageImage->file->getFileName());
    }

    public function actionSaveParts()
    {
        $cuttingPackPageInfo = Yii::$app->request->post('cuttingPackPage');
        if (!array_key_exists('uuid', $cuttingPackPageInfo)) {
            throw new InvalidArgumentException('Invalid parameter cuttingPackPage');
        }
        $cuttingPackPageUuid = $cuttingPackPageInfo['uuid'];
        $cuttingPackPage     = CuttingPackPage::tryFind(['uuid' => $cuttingPackPageUuid]);

        $cuttingPack = $this->resolveCuttingPack();
        if ($cuttingPack->uuid != $cuttingPackPage->cuttingPackFile->cuttingPack->uuid) {
            throw new InvalidArgumentException('Invalid current place order cutting pack');
        }

        $this->cuttingPackService->saveFileParts($cuttingPack, $cuttingPackPageInfo);
        return $this->jsonSuccess(
            [
                'cuttingPackPage' => CuttingPackSerializer::serialize($cuttingPackPage)
            ]
        );
    }

    public function actionOffers($manualSettedPsId = '')
    {
        $cuttingPackInfo = Yii::$app->request->post('cuttingPack');
        $cuttingPack     = $this->resolveCuttingPack();
        if ($cuttingPack->uuid != $cuttingPackInfo['uuid']) {
            throw new InvalidArgumentException('Invalid current place order cutting pack');
        }

        if ($manualSettedPsId) {
            $this->cuttingOffersService->manualSettedPsId = (int)$manualSettedPsId;
        }

        $this->cuttingPackService->saveCuttingPackMaterialInfo($cuttingPack, $cuttingPackInfo);
        $this->cuttingOffersService->setLocation(UserSessionFacade::getLocation());
        $this->cuttingOffersService->allowedInternational = Yii::$app->request->post('intlOnly');
        $offersBundle                                     = $this->cuttingOffersService->formOffersBundle($cuttingPack);
        $offersBundleSerialized                           = OffersBundleSerializer::serialize($offersBundle);
        $cuttingPackSerialized                            = CuttingPackSerializer::serialize($cuttingPack);

        return $this->jsonSuccess([
            'offersBundle' => $offersBundleSerialized,
            'cuttingPack'  => $cuttingPackSerialized
        ]);
    }

    protected function processDelivery(CuttingMachine $cuttingMachine)
    {
        $psMachineDeliveryInfo = $this->psMachineDeliveryInfoFactory->createByPsMachine($cuttingMachine->companyService);
        $deliveryForm          = $this->deliveryFormFactory->createByPsMachine($cuttingMachine->companyService);

        $this->placeOrderState->getDeliveryFormState($deliveryForm);
        if (!$deliveryForm->phoneCountyIso) {
            $deliveryForm->phoneCountyIso = $deliveryForm->country;
        }
        if ($deliveryForm->phone && !$deliveryForm->phoneLineNumber) {
            $deliveryForm->phoneLineNumber = $deliveryForm->phone;
        }

        $psMachineDeliveryInfoSerialized       = ArrayHelper::toArray($psMachineDeliveryInfo);
        $deliveryFormSerialized                = ArrayHelper::toArray($deliveryForm);
        $deliveryFormSerialized['contactName'] = $deliveryFormSerialized['contact_name'];
        $deliveryFormSerialized['countryIso']  = $deliveryFormSerialized['country'];


        if (!in_array($deliveryForm->deliveryType, $psMachineDeliveryInfo->deliveryTypes)) {
            $deliveryFormSerialized['deliveryType'] = reset($psMachineDeliveryInfo->deliveryTypes);
        }
        $deliveryFormSerialized['deliveryTypeCarrier'] = $psMachineDeliveryInfo->getCarrierByDeliveryType($deliveryForm->deliveryType);

        return [
            'psMachineDeliveryInfo' => $psMachineDeliveryInfoSerialized,
            'deliveryForm'          => $deliveryFormSerialized
        ];
    }

    protected function resolveCuttingMachine(): CuttingMachine
    {
        $cuttingMachineId = $this->placeOrderState->getActiveOfferItem();
        if (!$cuttingMachineId) {
            throw new BusinessException('Please select offer', 418);
        }
        $cuttingMachine = CuttingMachine::tryFindByPk($cuttingMachineId);
        if (!$this->cuttingMachineService->isAvailableForOffers($cuttingMachine)) {
            throw new InvalidArgumentException('Is not available for offers');
        };
        return $cuttingMachine;
    }

    public function actionSelectOffer()
    {
        $cuttingPackInfo = Yii::$app->request->post('cuttingPack');
        $offerServiceId  = Yii::$app->request->post('offerServiceId');
        $cuttingPack     = $this->resolveCuttingPack();
        if ($cuttingPack->uuid != $cuttingPackInfo['uuid']) {
            throw new InvalidArgumentException('Invalid current place order cutting pack');
        }
        $this->cuttingPackService->saveCuttingPackMaterialInfo($cuttingPack, $cuttingPackInfo);
        $cuttingMachine = CuttingMachine::tryFindByPk($offerServiceId);
        if (!$this->cuttingMachineService->isAvailableForOffers($cuttingMachine)) {
            throw new InvalidArgumentException('Is not available for offers');
        };
        $this->placeOrderState->setActiveOfferItem($cuttingMachine->id);
        return $this->jsonSuccess($this->processDelivery($cuttingMachine));
    }

    public function actionDelivery()
    {
        $cuttingMachine = $this->resolveCuttingMachine();
        return $this->jsonSuccess($this->processDelivery($cuttingMachine));
    }

    public function actionSaveDeliveryInfo()
    {
        if (!\Yii::$app->request->isPost) {
            throw new OnlyPostRequestException();
        }

        $cuttingPack    = $this->placeOrderState->getCuttingPack();
        $cuttingMachine = $this->resolveCuttingMachine();
        $deliveryInfo   = \Yii::$app->request->post('deliveryForm', []);

        $deliveryForm = $this->deliveryFormFactory->createByPsMachine($cuttingMachine->companyService);
        $deliveryForm->loadInfo($deliveryInfo);
        $this->placeOrderState->setDeliveryFormState($deliveryForm);
        if ($deliveryForm->isInternational()) {
            $deliveryForm->deliveryType = DeliveryType::INTERNATIONAL;
        }
        $deliveryForm->validate();
        $errors = $deliveryForm->getErrors();
        if ($errors) {
            $validateErrors = ErrorListHelper::addErrorsFormPrefix($errors, 'deliveryform');
            return $this->jsonReturn(
                [
                    'validateErrors' => $validateErrors
                ],
                422
            );
        }

        if ($deliveryForm->getValidatedAddress() && $deliveryForm->getValidatedAddress()->isEasyPost()) {
            return $this->jsonReturn(
                [
                    'validatedAddress' => DeliveryAddressSerializer::serialize($deliveryForm->getValidatedAddress())
                ],
                422
            );
        }

        $deliveryRate                 = $this->cuttingPackService->getOrderRateCached($cuttingMachine, $cuttingPack, $deliveryForm);
        $deliveryRate->deliveryForm   = $deliveryForm;
        $deliveryRate->deliveryParams = $cuttingMachine->companyService->asDeliveryParams();


        $user = AnonimOrderHelper::resolveOrderUser();

        $order = OrderBuilder::create()
            ->customer($user, $deliveryForm->email)
            ->comment($deliveryForm->comment)
            ->cuttingMachine($cuttingMachine)
            ->cuttingPack($cuttingPack)
            ->manualPs($this->placeOrderState->getPsIdOnly())
            ->delivery($deliveryRate)
            ->buildByItems();

        $this->placeOrderState->setOrderId($order->id);
        $this->locationService->changeUserLocationByDelivery($deliveryForm);

        return $this->jsonSuccess(['orderId' => $order->id]);
    }

    public function actionPayOrder()
    {
        /** @var StoreOrder $storeOrder */
        $storeOrder = $this->placeOrderState->getOrder();

        if (!$storeOrder) {
            throw new BusinessException(_t('site.store', 'Order not found'), 422);
        }

        StoreOrder::validateBeforeCheckout($storeOrder);
        $payOrderSerialized = PayOrderSerializer::serialize($storeOrder);

        return $this->jsonSuccess([
            'payOrder'     => $payOrderSerialized,
            'checkoutCard' => $this->getCheckoutCardView($storeOrder, Yii::$app->request->get('widget') === 'widget')
        ]);
    }

    public function actionBillingDetailsPrint()
    {
        return $this->billingDetailsPrint();
    }

    protected function resolveCuttingPack(): ?CuttingPack
    {
        $cuttingPack = $this->placeOrderState->getCuttingPack();
        if (!$cuttingPack) {
            throw new BusinessException('Please upload model', 418);
        }
        return $cuttingPack;
    }

}