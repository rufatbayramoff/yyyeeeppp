<?php

namespace frontend\controllers\my;

use common\components\ArrayHelper;
use common\components\BaseController;
use common\components\exceptions\InvalidModelException;
use common\components\exceptions\NoActiveModel3d;
use common\interfaces\Model3dBaseInterface;
use common\models\CuttingPack;
use common\models\CuttingPackPart;
use common\models\factories\Model3dFactory;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\repositories\Model3dRepository;
use common\models\repositories\ReviewRepository;
use common\models\repositories\UserSessionRepository;
use common\models\SystemLang;
use common\models\User;
use common\models\UserSession;
use common\modules\affiliate\services\AffiliateSessionService;
use common\modules\cutting\factories\CuttingPackFactory;
use common\modules\cutting\helpers\CuttingUrlHelper;
use common\modules\cutting\models\PlaceCuttingOrderState;
use common\modules\cutting\services\CuttingPackService;
use common\modules\payment\services\PaymentAccountService;
use common\modules\payment\services\PaymentService;
use common\modules\promocode\components\PromocodeApplyService;
use common\modules\promocode\components\PromocodeDiscountService;
use common\services\Model3dService;
use frontend\components\cart\CartService;
use frontend\components\FrontendWebView;
use frontend\models\delivery\DeliveryForm;
use frontend\models\model3d\PlaceOrderState;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\UserException;
use yii\caching\DbDependency;
use yii\web\GoneHttpException;

/**
 * Class OrderLaserCuttingController
 *
 * @package frontend\controllers\my
 *
 * @property FrontendWebView $view
 */
class OrderLaserCuttingController extends BaseController
{
    /** @var  PlaceCuttingOrderState */
    protected $placeOrderState;

    /** @var  PaymentService */
    protected $paymentService;

    /** @var CuttingPackFactory */
    protected $cuttingPackFactory;

    /** @var CuttingPackService */
    protected $cuttingPackService;

    /** @var  AffiliateSessionService */
    protected $affiliateSessionService;

    /** @var  Model3dRepository */
    protected $model3dRepository;

    /** @var  Model3dService */
    protected $model3dService;

    /** @var string */
    protected $posUid = '';

    /**
     * @param PlaceCuttingOrderState $placeOrderState
     * @param PaymentService $paymentService
     * @param AffiliateSessionService $affiliateSessionService
     * @param CuttingPackFactory $cuttingPackFactory
     * @param CuttingPackService $cuttingPackService
     * @param Model3dRepository $model3dRepository
     * @param Model3dService $model3dService
     */
    public function injectDependencies(
        PlaceCuttingOrderState $placeOrderState,
        PaymentService $paymentService,
        AffiliateSessionService $affiliateSessionService,
        CuttingPackFactory $cuttingPackFactory,
        CuttingPackService $cuttingPackService,
        Model3dRepository $model3dRepository,
        Model3dService $model3dService
    )
    {
        $this->placeOrderState         = $placeOrderState;
        $this->paymentService          = $paymentService;
        $this->affiliateSessionService = $affiliateSessionService;
        $this->cuttingPackFactory      = $cuttingPackFactory;
        $this->cuttingPackService      = $cuttingPackService;
        $this->model3dRepository       = $model3dRepository;
        $this->model3dService          = $model3dService;
    }

    /**
     * @param $action
     *
     * @return bool
     * @throws UserException
     */
    public function beforeAction($action)
    {
        $posUid = '';
        if ($posUidParam = Yii::$app->request->get('posUid', '')) {
            $posUid = $posUidParam;
        }
        if ($posUid) {
            $this->placeOrderState->setStateUid($posUid);
        }

        $this->posUid                      = $this->view->params['posUid'] = $posUid;
        if (Yii::$app->request->get('psId')) {
            $this->placeOrderState->setPsIdOnly(Yii::$app->request->get('psId'));
        }
        $this->view->params['psId']        = $this->placeOrderState->getPsIdOnly();
        return parent::beforeAction($action);
    }

    protected function fixLayout()
    {
        if ($this->isWidget()) {
            $this->view->setWidgetMode(true);
            $this->layout = 'plain.php';
            Yii::$app->params['additionalBodyClass'] = 'item-rendering-external-widget';
        }
    }

    /**
     * @return bool
     */
    protected function isWidget(): bool
    {
        return \Yii::$app->request->get('widget') === 'widget';
    }

    public function actionDownloadImage($name, $page, $name2, $imgName)
    {
        die('r');
    }

    public function actionUploadFile()
    {
        $this->fixLayout();
        $currentStepName = 'upload';
        Yii::$app->sessionManager->checkRestoreSession(true);


        if (Yii::$app->request->get('affiliate')) {
            $affiliateSource = $this->affiliateSessionService->reInitAffiliateSource(Yii::$app->request, Yii::$app->response);
            $affiliateSource->followers_count++;
            $affiliateSource->safeSave();
            $this->placeOrderState->setAffiliateSource($affiliateSource);
        } else {
            $affiliateSource = $this->affiliateSessionService->getAffiliateSource(Yii::$app->request);
            if ($affiliateSource) {
                $this->placeOrderState->setAffiliateSource($affiliateSource);
            }
        }
        if($packUid = Yii::$app->request->get('packUid', false)) {
            $pack = CuttingPack::tryFind(['uuid'=>$packUid]);
            $this->placeOrderState->setCuttingPack($pack);
        }


        $cuttingPack = $this->placeOrderState->getCuttingPack();
        if (!$cuttingPack) {
            $cuttingPack         = $this->cuttingPackFactory->createForCurrentUser();
            $cuttingPack->source = CuttingPack::SOURCE_WEBSITE;
        }

        return $this->render(
            'index.php',
            [
                'currentStepName' => $currentStepName,
                'stateData'       => [
                    'cuttingPack' => $cuttingPack,
                ],
            ]
        );
    }

    protected function doStep($currentStepName)
    {
        $cuttingPack = $this->placeOrderState->getCuttingPack();
        if (!$cuttingPack) {
            return $this->redirect(CuttingUrlHelper::makeOrderUploadFiles());
        }

        return $this->render(
            'index.php',
            [
                'currentStepName' => $currentStepName,
                'stateData'       => [
                    'cuttingPack' => $cuttingPack,
                ],
            ]
        );
    }

    public function actionSelectParts()
    {
        return $this->doStep('parts');
    }

    public function actionOffers()
    {
        return $this->doStep('offers');
    }

    public function actionDelivery()
    {
        return $this->doStep('delivery');
    }

    public function actionCheckout()
    {
        return $this->doStep('checkout');
    }

    /**
     * @param $uuid CuttingPack uuid
     */
    public function actionPreviewPack($uuid)
    {
        $cuttingPack = CuttingPack::tryFind(['uuid' => $uuid]);
        if (!$this->cuttingPackService->allowViewForCurrentUser($cuttingPack)) {
            throw new UserException('Not allowed for current user');
        }
        $previewCuttingFile = $cuttingPack->getPreviewFile();
        if ($previewCuttingFile->isImage()) {
            return \Yii::$app->response->sendFile($previewCuttingFile->file->getLocalTmpFilePath(), $previewCuttingFile->getTitle());
        } else {
            $parts = $previewCuttingFile->cuttingPackParts;
            /** @var CuttingPackPart $firstPart */
            $firstPart = reset($parts);
            if ($firstPart->image) {
                $rawImage= $firstPart->getImageContent();
                return \Yii::$app->response->sendContentAsFile($rawImage, $previewCuttingFile->getTitle().'.svg', [
                    'mimeType' => $firstPart->getImageType(),
                    'inline' => 1
                ]);
            }
        }
        $defaultSvgPath = Yii::getAlias('@static') . '/images/widget-page-icons/formats.svg';
        return \Yii::$app->response->sendFile($defaultSvgPath, $previewCuttingFile->getTitle());
    }
}