<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.07.17
 * Time: 17:34
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

class SitemapController extends Controller
{
    protected function getWebRootPath()
    {
        return \Yii::getAlias(param('sitemapAlias'));
    }

    protected function getIndexFile($index = '')
    {
        $currentDomain = $_SERVER['HTTP_HOST'];
        $currentLangIso = Yii::$app->getModule('intlDomains')->domainManager->getDefaultLangIsoForDomain($currentDomain);
        $indexFilePath = $this->getWebRootPath() . '/sitemap_' . $index . $currentLangIso . '.xml';
        if (!file_exists($indexFilePath)) {
            $indexFilePath = $this->getWebRootPath() . '/sitemap_' . $index . 'en-US.xml';
        }
        $response = Yii::$app->response;
        $response->sendFile($indexFilePath, 'sitemap.xml', ['inline'=>true, 'mimeType'=>'application/xml']);
    }

    public function actionIndexList()
    {
        return $this->getIndexFile('index_');
    }

    public function actionMainList()
    {
        return $this->getIndexFile();
    }
}