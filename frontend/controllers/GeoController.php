<?php
namespace frontend\controllers;

use common\models\GeoCountry;
use frontend\components\UserSessionFacade;
use frontend\models\user\UserLocator;
use yii\helpers\Html;

/**
 * Geo Controller
 *
 * @author Pavel Ustinov <p.ustinov@headrobotics.com>
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.com>
 */
class GeoController extends \common\components\BaseController
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'set-location' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     *
     * @param string $q            
     * @param string $k            
     * @return array
     */
    public function actionSearchLocation($q, $k)
    {
        app('response')->format = \yii\web\Response::FORMAT_JSON;
        $kCol = 'id';
        $vCol = 'title';
        $columns = [];
        if (! empty($k)) {
            list ($kCol, $vCol) = explode(",", $k);
            $columns = [
                $kCol,
                $vCol
            ];
        }
        $tableName = 'geo_city';
        $out = [
            'results' => [
                'id' => '',
                'text' => ''
            ]
        ];
        if (! is_null($q)) {
            $query = new \yii\db\Query();
            $search = $vCol . ' LIKE "' . $q . '%"';
            if (is_numeric($q)) {
                $search = $kCol . "=" . intval($q);
            }
            $query->select(implode(",", $columns) . " AS text")
                ->from($tableName)
                ->where($search)
                ->limit(30);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

    /**
     * request to change location
     *
     * @return string
     */
    public function actionChangeLocation()
    {
        $userLocator = new UserLocator();
        return $this->renderAdaptive('changeLocation', [
            'jsCallback' => 'TS.UpdateLocation',
            'locator' => $userLocator
        ]);
    }

    public function actionChangeLocationRedir()
    {
        $userLocator = new UserLocator();
        return $this->renderAdaptive('changeLocation', [
            'jsCallback' => 'TS.RedirLocation',
            'locator' => $userLocator
        ]);
    }

    /**
     * set location
     *
     * @return string
     */
    public function actionSetLocation()
    {
        $userLocator = new UserLocator();
        $post = app('request')->post();
        $userLocator->load($post);
        if (! $userLocator->validate()) {
            $fullError = Html::errorSummary($userLocator);
            return $this->jsonFailure([
                'message' => _t('site.error', 'Please specify only the name of the city'),
                'fullError' => $fullError
            ]);
        }
        UserSessionFacade::setLocationByLocator($userLocator);
        $result = $userLocator->toString();
        return $this->jsonSuccess([
            'message' => $result,
            'location' => [
                'country' => $userLocator->country, // $geoCountry ? $geoCountry->title : 'World',
                'countryCode' => $userLocator->country,
                'region' => $userLocator->getState(),
                'city' => $userLocator->getCity(),
                'lat'  => $userLocator->lat,
                'lon'  => $userLocator->lon,
                'zip'  => $userLocator->getPostalCode(),
                'formattedAddress' => $userLocator->formattedAddress
            ]
        ]);
    }
}
