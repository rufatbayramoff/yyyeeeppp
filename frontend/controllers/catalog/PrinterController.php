<?php

namespace frontend\controllers\catalog;

use common\components\BaseController;
use common\models\PsPrinter;
use frontend\models\ps\PsFacade;

/**
 * PrinterController
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class PrinterController extends BaseController
{

    private $defaultRenderColor;

    private $defaultMaterial;

    public function init()
    {
        parent::init();
        $this->defaultRenderColor = param('render_color', 'White');
        $this->defaultMaterial = param('default_material', 'PLA');
    }

    public function actionIndex($id)
    {
        $printer  = PsPrinter::getValidated($id);
        $link =     PsFacade::getPsLink($printer->ps, ['printer'=>$printer->id]);
        return $this->redirect($link, 301);
    }
}
