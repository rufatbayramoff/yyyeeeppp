<?php

namespace frontend\controllers\catalog;

use common\components\BaseController;
use common\components\exceptions\BusinessException;
use common\components\exceptions\InvalidModelException;
use common\components\FileTypesHelper;
use common\interfaces\Model3dBasePartInterface;
use common\models\factories\FileFactory;
use common\models\File;
use common\models\Model3d;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\repositories\FileRepository;
use common\models\ThingiverseThing;
use common\modules\cutting\models\PlaceCuttingOrderState;
use common\modules\cutting\services\CuttingPackService;
use common\modules\model3dRender\models\RenderFileParams;
use common\services\Model3dService;
use DateTime;
use DateTimeZone;
use Exception;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;
use frontend\models\model3d\PlaceOrderState;
use frontend\widgets\Model3dUploadWidget;
use Throwable;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\base\UserException;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\validators\ImageValidator;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\Session;
use yii\web\UploadedFile;

/**
 * Controller to handle file uploads
 */
class UploadModel3dController extends BaseController
{
    protected $viewPath = '@frontend/views/upload';

    /**
     * @var Model3dService
     */
    private $model3dService;

    /**
     * @var Session
     */
    private $session;

    /**
     * @param Model3dService $model3dService
     */
    public function injectDependencies(Model3dService $model3dService): void
    {
        $this->model3dService = $model3dService;
        $this->session = Yii::$app->session;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => [
                    'file-upload'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'file-upload'
                        ],
                        'allow'   => true
                    ]
                ]
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'file-upload' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     * @param $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if ($action->id == 'add-file' || $action->id == 'create-model3d') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * @param int|null $psId
     * @param int|null $psPrinterId
     * @param int|null $ps_printer_id
     * @param string|null $utmSource
     * @param string|null $utm_source
     * @param string|null $usage
     * @param int|null $psPrinterIdOnly
     * @return string
     * @todo add arguments comments
     */
    public function actionIndex(
        int $psId = null,
        int $psPrinterId = null,
        int $ps_printer_id = null,
        string $utmSource = null,
        string $utm_source = null,
        string $usage = null,
        int $psPrinterIdOnly = null
    ) {
        $utmSource = $utmSource ?? $utm_source;
        $psPrinterId = $psPrinterId ?? $ps_printer_id;
        if (!app('request')->get('noredir', false)) {
            $url = '/my/print-model3d?utm_source=upload';
            if (!empty($psId)) {
                $url = $url . '&psId=' . $psId;
            }
            if (!empty($psPrinterId)) {
                $url = $url . '&psPrinterId=' . $psPrinterId;
            }
            return $this->redirect($url, 301);
        }
        Yii::$app->sessionManager->checkRestoreSession(true);


        $ps = $psId ? Ps::findByPk($psId) : null;
        $psPrinter = $psPrinterId ? PsPrinter::findByPk($psPrinterId) : null;

        $this->session->set('upload_usage', $usage);

        $link = $this->detectSourceUrl();

        return $this->renderAdaptive('uploadModel3d', [
                'utmSource'       => $utmSource,
                'ps'              => $ps,
                'psPrinter'       => $psPrinter,
                'psPrinterIdOnly' => $psPrinterIdOnly,
                'link'            => $link
            ]
        );
    }

    /**
     * @param $redirectUrl
     * @param $additionalParams
     * @return string
     */
    protected function addAdditionalParamsToUrl($redirectUrl, $additionalParams)
    {
        if (!$additionalParams) {
            $redirectUrl .= '&editModel3dMode=true';
            return $redirectUrl;
        }
        if ($additionalParams && array_key_exists('utmSource', $additionalParams) && $additionalParams['utmSource']) {
        } else {
            $redirectUrl .= '&editModel3dMode=true';
        }
        foreach ($additionalParams as $key => $value) {
            $redirectUrl .= '&' . $key . '=' . $value;
        }
        return $redirectUrl;
    }

    /**
     * @return array
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     * @throws InvalidModelException
     */
    public function actionCreateModel3d()
    {
        $filesUuidsList = $this->request->post('uploadedFilesUuids');
        $additionalParams = $this->request->post('additionalParams')??[];
        $isWidget = 0;
        if ($additionalParams && array_key_exists('widget', $additionalParams)) {
            $isWidget = 1;
        }
        if ($filesUuidsList) {
            $files = [];
            foreach ($filesUuidsList as $fileUuid) {
                $file = $this->getFileByUuid($fileUuid);
                $files[] = $file;
            }

            $utmSource = $this->request->get('utm_source');
            if (!$utmSource && $additionalParams && array_key_exists('utmSource', $additionalParams)) {
                $utmSource = $additionalParams['utmSource'];
            }
            if (!empty($this->request->get('widgetCncId'))) {
                $utmSource = 'cnc';
            }

            if ($utmSource === 'cnc') {
                $model3d = $this->createModel3dByFiles($additionalParams, $files);
                $redirectUrl = $this->cncPack($model3d, $additionalParams);
            } else if($utmSource === 'cutting') {
                $redirectUrl = $this->cuttingPack($files, $additionalParams);
            } else if ($this->request->post('isPlaceOrderState')) {
                $model3d = $this->createModel3dByFiles($additionalParams, $files);
                $redirectUrl = $this->model3dPack($model3d, $additionalParams, $isWidget, $utmSource);
            } else {
                $model3d = $this->createModel3dByFiles($additionalParams, $files);
                $redirectUrl = '/model3d/preload?id=' . $model3d->id;
                $redirectUrl = $this->addAdditionalParamsToUrl($redirectUrl, $additionalParams);
            }

            return $this->jsonReturn([
                'success'     => true,
                'model3dId'   => $model3d->id ?? null,
                'redirectUrl' => $redirectUrl
            ]);
        }

        return $this->jsonReturn(
            [
                'success' => false,
                'message'  => 'Empty files list'
            ]
        );
    }

    /**
     * @return array
     * @throws BusinessException
     * @throws InvalidConfigException
     * @throws InvalidModelException
     * @throws \yii\base\ErrorException
     */
    public function actionAddFile()
    {
        $uploadedFiles = UploadedFile::getInstancesByName('files');
        $uuids = [];
        $fileFactory = Yii::createObject(FileFactory::class);
        $fileRepository = Yii::createObject(FileRepository::class);
        foreach ($uploadedFiles as $uploadedFile) {
            $file = $fileFactory->createFileFromUploadedFile($uploadedFile);

            // TODO Merge with Model3dImg::validate
            if (FileTypesHelper::isImage($file)) {
                $imageValidator = new ImageValidator(['minWidth' => 1]);
                if (!$imageValidator->validate($uploadedFile, $errorMessage)) {
                    Yii::error('Validate model3d: ' . $errorMessage, 'app');
                    throw new BusinessException($errorMessage);
                }
            }
            if (FileTypesHelper::isFileInExt($file, [Model3dBasePartInterface::FORMAT_3MF]) && ($file->size>Model3dBasePartInterface::MAX_3MF_SIZE)) {
               throw new BusinessException('Your file size is too big');
            }

            $file->expireBySeconds(60 * 60 * 24);
            $file->setOwner(Model3dUploadWidget::class, 'uploadFile');
            $fileRepository->save($file);
            $uuids[$file->getFileName()] = $file->uuid;
        }
        $resJson = json_encode(['success' => true, 'uuids' => $uuids], JSON_UNESCAPED_UNICODE);
        return $this->jsonReturn($resJson);
    }

    /**
     * @param $uuid
     * @return Response
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionFilePreview($uuid)
    {
        $file = $this->getFileByUuid($uuid);
        $renderParams = Yii::createObject(
            [
                'class' => RenderFileParams::class,
                'color' => 'ffffff'
            ]
        );
        $url = Yii::$app->getModule('model3dRender')->renderer->getRenderImageUrl($file, $renderParams);
        $viewUrl = ImageHtmlHelper::getThumbUrl($url, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
        return $this->redirect($viewUrl);
    }

    /**
     * @param $uuid
     * @return array|File|mixed|null|ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function getFileByUuid($uuid)
    {
        $nowDate = (new DateTime('now', new DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $file = File::find()->where(
            [
                'uuid'       => $uuid,
                'ownerClass' => Model3dUploadWidget::class,
                'ownerField' => 'uploadFile',
            ]
        )->andWhere(['>=', 'expire', $nowDate])->one();
        if (!$file) {
            throw new NotFoundHttpException();
        }
        return $file;
    }

    /**
     * If link from referer to upload page, which include link to original website,
     * we display this link to user to download.
     * If source is thingiverse, check and redirect to this model page
     *
     * @return bool|null|string
     */
    private function detectSourceUrl()
    {
        $link = null;
        $fullLink = $this->request->getUrl();
        if (!empty($this->request->get('link'))) {
            $link = substr($fullLink, strpos($fullLink, '&link=') + strlen('&link='));
        } else {
            if (!empty($this->request->get('source_url'))) {
                $link = substr($fullLink, strpos($fullLink, '&source_url=') + strlen('&source_url=')); //, strlen($fullLink));
            }
        }
        if (!empty($link) && filter_var($link, FILTER_VALIDATE_URL) === false) {
            $link = urldecode($link);
        }
        if (strpos($link, 'thingiverse.com') !== false) {
            $thingId = (int)str_replace([
                'http://www.thingiverse.com/thing:',
                'https://www.thingiverse.com/thing:',
                'https://thingiverse.com/thing:',
                'http://thingiverse.com/thing:'
            ], '', $link);
            $hasThing = ThingiverseThing::findOne(['thing_id' => $thingId]);
            if ($hasThing) {
                $modelLink = Model3dFacade::getStoreUrl($hasThing->model3d);
                if ($modelLink) {
                    return $this->redirect($modelLink);
                }
            }
        }
        return $link;
    }

    /**
     * @param array $files
     * @param array $additionalParams
     * @return string
     * @throws InvalidConfigException
     */
    public function cuttingPack(array $files, array $additionalParams): string
    {
        $cuttingPackService = Yii::createObject(CuttingPackService::class);
        $cuttingPack = $cuttingPackService->createCuttingPackByFilesList($files);
        $posUid      = $cuttingPack->uuid;
        $placeOrderState = Yii::createObject(PlaceCuttingOrderState::class);
        $placeOrderState->setStateUid($posUid);
        $placeOrderState->setCuttingPack($cuttingPack);
        if ($additionalParams && array_key_exists('psId', $additionalParams)) {
            $placeOrderState->setPsIdOnly($additionalParams['psId']);
        }
        $route = ['/my/order-laser-cutting/upload-file', 'posUid' => $posUid];
        return Url::to($route);
    }

    /**
     * @param Model3d $model3d
     * @param array $additionalParams
     * @return string
     */
    public function cncPack(Model3d $model3d, array $additionalParams): string
    {
        $route = ['/workbench/cncm/filepage/index', 'modelId' => $model3d->id];
        if ($this->request->get('machineId')) {
            $route['machineId'] = $this->request->get('machineId');
        }
        if (array_key_exists('machineId', $additionalParams)) {
            $route['machineId'] = $additionalParams['machineId'];
        }
        if ($this->request->get('widgetCncId')) {
            $route['viewType'] = 'widget';
        }
        return Url::to($route);
    }

    /**
     * @param Model3d $model3d
     * @param array $additionalParams
     * @param int $isWidget
     * @param string $utmSource
     * @return string
     * @throws InvalidConfigException
     */
    public function model3dPack(Model3d $model3d, array $additionalParams, int $isWidget, string $utmSource): string
    {
        $posUid = 'MU' . $model3d->id;
        $placeOrderState = Yii::createObject(PlaceOrderState::class);
        $placeOrderState->setStateUid($posUid);
        $placeOrderState->setModel3d($model3d);
        if ($additionalParams && array_key_exists('psId', $additionalParams)) {
            $placeOrderState->setPsIdOnly($additionalParams['psId']);
        }

        try {
            if ($additionalParams && array_key_exists('psPrinterId', $additionalParams)) {
                /** @var PsPrinter $printer */
                $printer = PsPrinter::findByPk($additionalParams['psPrinterId']);
                if ($printer) {
                    $placeOrderState->setPsIdOnly($printer->ps->id);
                }
            }
        } catch (Throwable $ignored) {
        }

        return '/my/print-model3d' . ($isWidget ? '/widget' : '') . '?posUid=' . $posUid . '&utmSource=' . $utmSource;
    }

    /**
     * @param array|null $additionalParams
     * @param array $files
     * @return Model3d
     * @throws InvalidModelException
     */
    private function createModel3dByFiles(?array $additionalParams, array $files): Model3d
    {
        $model3dProperties = [];
        if ($additionalParams && array_key_exists('model3dSource', $additionalParams)) {
            $model3dProperties['source'] = $additionalParams['model3dSource'];
        }

        return $this->model3dService->createModel3dByFilesList($model3dProperties, $files);
    }
}
