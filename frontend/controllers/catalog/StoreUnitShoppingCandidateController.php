<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.11.16
 * Time: 16:54
 */

namespace frontend\controllers\catalog;

use common\models\StoreUnitShoppingCandidate;
use common\services\StoreUnitShoppingCandidateService;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\VerbFilter;

class StoreUnitShoppingCandidateController  extends \common\components\BaseController
{
    protected $viewPath = '@frontend/views/upload';

    /** @var  StoreUnitShoppingCandidateService */
    public $shoppingCandidateService;

    public function injectDependencies(StoreUnitShoppingCandidateService $shoppingCandidateService)
    {
        $this->shoppingCandidateService = $shoppingCandidateService;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     * init to use UploadFacade mode remote or local
     */
    public function init()
    {
    }

    /**
     * Upload model3d form for files [images,stl]
     * checks if in session we have uploaded files, and redirs to edit if it has
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionDelete($id)
    {
        $storeUnitShoppingCandidate = StoreUnitShoppingCandidate::tryFindByPk($id);

        if (!$this->shoppingCandidateService->isAvailableForCurrentUser($storeUnitShoppingCandidate)) {
            throw new InvalidParamException('Shopping candidate '.$id.' is unavalible for current user');
        }
        $storeUnitShoppingCandidate->delete();
        return $this->jsonSuccess(
            [
                'message' => _t('site.store', 'Deleted.')
            ]
        );
    }
}