<?php

namespace frontend\controllers\catalog;


use common\models\ApiPrintablePack;
use common\models\File;
use common\models\Model3d;
use common\models\Model3dPart;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\repositories\Model3dRepository;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\models\StoreUnitShoppingCandidate;
use common\modules\api\v2\services\ApiService;
use common\services\Model3dPartService;
use common\services\Model3dService;
use common\services\PrinterMaterialService;
use common\services\StoreUnitShoppingCandidateService;
use console\jobs\model3d\ParserJob;
use console\jobs\QueueGateway;
use frontend\controllers\actions\TagsListAction;
use frontend\controllers\catalog\actions\FilepageAction;
use frontend\controllers\catalog\actions\PrintersListAction;
use frontend\controllers\catalog\actions\serializers\Model3dSerializer;
use frontend\controllers\catalog\actions\serializers\Model3dTextureStateSerializer;
use frontend\models\model3d\Model3dFacade;
use frontend\models\model3d\Model3dForm;
use frontend\models\model3d\PlaceOrderState;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\Exception;

/**
 * 3d model controller - view model item
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class Model3dController extends \common\components\BaseController
{

    /**
     * @var Model3dService
     */
    public $model3dService = null;

    /** @var  StoreUnitShoppingCandidateService */
    public $shoppingCandidateService;

    /** @var Model3dRepository */
    public $model3dRepository;

    /** @var ApiService */
    public $apiService;

    /**
     * @param StoreUnitShoppingCandidateService $shoppingCandidateService
     * @param Model3dService $model3dService
     * @param Model3dRepository $model3dRepository
     * @param ApiService $apiService
     */
    public function injectDependencies(
        StoreUnitShoppingCandidateService $shoppingCandidateService,
        Model3dService $model3dService,
        Model3dRepository $model3dRepository,
        ApiService $apiService
    )
    {
        $this->shoppingCandidateService = $shoppingCandidateService;
        $this->model3dService           = $model3dService;
        $this->model3dRepository        = $model3dRepository;
        $this->apiService               = $apiService;
    }


    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'item'              => ['class' => FilepageAction::class],
            'get-printers-list' => ['class' => PrintersListAction::class],
            'tags'              => ['class' => TagsListAction::class]
        ];
    }


    protected $viewPath = '@frontend/views/model3d';

    protected function formPreloadRedirectUrlParams()
    {
        $allowedRedirectParams = [
            'printerMaterialGroupId',
            'printerColorId',
            'psId',
            'psPrinterId',
            'psPrinterIdOnly',
        ];
        $redirectUrlParams     = [];

        if (Yii::$app->request->get('isExternalWidget')) {
            $redirectUrlParams['widget'] = Model3dForm::WIDGET_TYPE_PS_PRINTER;
        }
        foreach ($allowedRedirectParams as $redirectParam) {
            if (Yii::$app->request->get($redirectParam)) {
                $redirectUrlParams[$redirectParam] = Yii::$app->request->get($redirectParam);
            }
        }
        return $redirectUrlParams;
    }

    public function actionPreload($id, $akey = null)
    {

        $model3d = Model3dFacade::getCatalogItem($id);
        if ($akey) {
            $this->model3dService->tryValidateModelStore($model3d, $akey);
            $this->shoppingCandidateService->formShoppingCandidate($model3d, StoreUnitShoppingCandidate::TYPE_API);
        }
        if (app('request')->get('editModel3dMode', false)) {
            if (UserFacade::getCurrentUser()) {
                return $this->redirect('/my/model/edit/' . $model3d->id);
            }
        }

        $redirectUrlParams = $this->formPreloadRedirectUrlParams();
        if (Yii::$app->request->get('akey')) {
            $redirectUrlParams['akey'] = Yii::$app->request->get('akey');
        }
        $toPs = null;
        if (array_key_exists('widget', $redirectUrlParams) && $redirectUrlParams['widget']) {
            if (Yii::$app->request->get('psPrinterId')) {
                $psPrinter = PsPrinter::tryFind(Yii::$app->request->get('psPrinterId'));
                $toPs      = $psPrinter->ps;
            } elseif (Yii::$app->request->get('psId')) {
                $toPs = Ps::tryFind(Yii::$app->request->get('psId'));
            }
            $isExternalWidget                        = true;
            $this->layout                            = 'plain.php';
            Yii::$app->params['additionalBodyClass'] = 'item-rendering-external-widget';
        } else {
            $isExternalWidget = false;
        }

        $redirectUrl = Model3dFacade::getStoreUrl(
            $model3d,
            $redirectUrlParams
        );

        return $this->renderAdaptive(
            'itemRendering.php',
            [
                'redirectUrl'      => $redirectUrl,
                'isExternalWidget' => $isExternalWidget,
                'toPs'             => $toPs,
                'id'               => $id,
                'totalFiles'       => count($model3d->getActiveModel3dParts())
            ]
        );
    }

    /**
     * @param $packPublicToken
     * @return string
     * @throws Exception
     * @throws \ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\UserException
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPreloadPrintablePack($packPublicToken)
    {
        $printablePack = ApiPrintablePack::tryFind(['public_token' => $packPublicToken]);

        $this->apiService->prepareForPrint($printablePack);

        if (($printerMaterialGroupId = Yii::$app->request->get('printerMaterialGroupId')) && ($printerColorId = Yii::$app->request->get('printerColorId'))) {
            $printerMaterialGroupRepository = Yii::createObject(PrinterMaterialGroupRepository::class);
            $printerColorRepository         = Yii::createObject(PrinterColorRepository::class);
            $model3dRepository              = Yii::createObject(Model3dRepository::class);

            $group = $printerMaterialGroupRepository->getById($printerMaterialGroupId);
            $color = $printerColorRepository->getById($printerColorId);

            PrinterMaterialService::setMaterialGroupAndColor($printablePack->model3d, $group, $color);
            $printablePack->model3d->setAnyTextureAllowed(false);
            $model3dRepository->save($printablePack->model3d);
        };


        $placeOrderState = Yii::createObject(PlaceOrderState::class);
        $posUid          = 'PP' . $printablePack->public_token; // Same token for same printable token if reload page
        $placeOrderState->setStateUid($posUid);
        $placeOrderState->setModel3d($printablePack->model3d);
        $placeOrderState->setModel3dIsAnyTextureAllowed($printablePack->model3d->isAnyTextureAllowed());

        if ($affiliateSource = $printablePack->apiExternalSystem->getAffiliateSource()) {
            $affiliateSource->followers_count++;
            $affiliateSource->safeSave();
            $placeOrderState->setAffiliateSource($affiliateSource);
        }
        $redirectUrl = '/my/print-model3d/?posUid=' . $posUid;
        $this->redirect($redirectUrl);
    }


    /**
     * get preview 360 (used from backend)
     *
     * @param $model3dPartId
     * @return string
     * @internal param int $model3d
     * @internal param $fileId
     */
    public function actionPreview360($model3dPartId)
    {
        /** @var Model3d $model3dObj */
        $model3dPart  = Model3dPart::tryFindByPk($model3dPartId);
        $this->layout = 'plain';
        return $this->render(
            'preview360Modal',
            [
                'model3dPart' => $model3dPart
            ]
        );
    }

    public function actionPreview360File($fileId)
    {
        $file         = File::tryFindByPk($fileId);
        $this->layout = 'plain';
        return $this->render(
            'preview360ModalFile',
            [
                'file' => $file
            ]
        );
    }

    /**
     * Checks job result
     *
     * @param string $modelId
     * @param string $partId
     * @return array
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCheckJobs($modelId, $partId = null): array
    {
        $model = $this->model3dRepository->getByUid($modelId);

        $response        = [];
        $hasParserErrors = false;
        $complited       = true;

        $parts = $model->getActiveModel3dParts();

        $response['model3d']             = Model3dSerializer::serialize($model);
        $response['model3dTextureState'] = Model3dTextureStateSerializer::serialize($model);

        foreach ($parts as $model3dPart) {
            $renderImg    = Model3dPartService::getModel3dPartRenderUrlIfExists($model3dPart);
            $isParseError = false;
            $isParsed     = $model3dPart->isCaclulatedProperties();
            if (!$isParsed) {
                if (ParserJob::isCanRun($model3dPart->file)) {
                    QueueGateway::addFileJob(ParserJob::create($model3dPart->file));
                } elseif (ParserJob::isFailed($model3dPart->file)) {
                    $isParseError = true;
                }
            }
            $hasParserErrors = $hasParserErrors || $isParseError;

            $itemKey = false;

            foreach ($response['model3d']['parts'] as $partKey => $partItem) {
                if ($partItem['uid'] === $model3dPart->getUid()) {
                    $itemKey = $partKey;
                    break;
                }
            }
            if ($itemKey === false) {
                throw new  Exception('not found part exception');
            }

            $response['model3d']['parts'][$itemKey]['render'] = [
                'isSuccess' => (bool)$renderImg,
                'renderImg' => $renderImg
            ];

            $response['model3d']['parts'][$itemKey]['parser'] = [
                'isSuccess' => $isParsed,
                'isError'   => $isParseError,
                'result'    => $isParsed ? Model3dPartService::getModel3dPartCaption($model3dPart) : null
            ];

            $complited = $complited && ($isParsed || $isParseError);
        }

        $response['isError']     = $hasParserErrors;
        $response['isComplited'] = $complited;
        return $this->jsonReturn($response);
    }

    /**
     * Generate short files list
     *
     * @param $modelFiles
     * @param $currentFileId
     * @return array
     */
    protected function generateShortModelFilesList($modelFiles, $currentFileId)
    {
        $modelFilesList = [];
        foreach ($modelFiles as $modelFileId => $modeFile) {
            if ($currentFileId == $modelFileId) {
                $modelFilesList[$modelFileId] = [
                    'isActive' => true
                ];
            } else {
                $modelFilesList[$modelFileId] = [
                    'isActive' => false
                ];
            }
        }
        return $modelFilesList;
    }
}

