<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 09.01.18
 * Time: 17:00
 */

namespace frontend\controllers\catalog\actions;

use common\components\BaseController;
use common\components\order\predicates\NeedCertificateForDownloadModelPredicate;
use common\components\orderOffer\OrderOfferLocatorService;
use common\components\serizaliators\Serializer;
use common\interfaces\Model3dBaseInterface;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\models\PrinterMaterialGroup;
use common\models\repositories\Model3dRepository;
use common\models\repositories\PrinterColorRepository;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\models\repositories\PrinterMaterialRepository;
use common\services\Model3dService;
use common\services\Model3dViewedStateService;
use common\services\PrinterMaterialService;
use common\services\ReviewService;
use frontend\components\UserSessionFacade;
use frontend\controllers\catalog\actions\serializers\MaterialColorsItemSerializer;
use frontend\controllers\catalog\actions\serializers\Model3dTextureStateSerializer;
use frontend\controllers\catalog\actions\serializers\OffersBundleSerializerExt;
use frontend\controllers\catalog\actions\serializers\PrinterColorSerializer;
use frontend\controllers\catalog\actions\serializers\PrinterMaterialGroupSerializer;
use frontend\models\model3d\Model3dFacade;
use frontend\models\model3d\Model3dItemForm;
use frontend\models\user\UserFacade;
use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;

/**
 * Class PrintersListAction
 *
 * @package frontend\controllers\catalog\actions
 * @property BaseController $controller
 */
class PrintersListAction extends Action
{
    /**
     * @var Model3dService
     */
    protected $model3dService;

    /** @var Model3dViewedStateService */
    protected $model3dViewedStateService;

    /** @var ReviewService */
    protected $reviewService;

    /** @var PrinterMaterialGroupRepository */
    protected $printerMaterialGroupRepository;

    /** @var PrinterMaterialRepository */
    protected $printerMaterialRepository;

    /** @var PrinterColorRepository */
    protected $printerColorRepository;

    /** @var  NeedCertificateForDownloadModelPredicate */
    protected $predicateOnlyCertificated;

    /**
     * @param Model3dService $model3dService
     * @param Model3dViewedStateService $model3dViewedStateService
     * @param ReviewService $reviewService
     * @param PrinterMaterialGroupRepository $printerMaterialGroupRepository
     * @param PrinterMaterialRepository $printerMaterialRepository
     * @param PrinterColorRepository $printerColorRepository
     * @param NeedCertificateForDownloadModelPredicate $predicateOnlyCertificated
     */
    public function injectDependencies(
        Model3dService $model3dService,
        Model3dViewedStateService $model3dViewedStateService,
        ReviewService $reviewService,
        PrinterMaterialGroupRepository $printerMaterialGroupRepository,
        PrinterMaterialRepository $printerMaterialRepository,
        PrinterColorRepository $printerColorRepository,
        NeedCertificateForDownloadModelPredicate $predicateOnlyCertificated
    ) {
        $this->model3dService = $model3dService;
        $this->model3dViewedStateService = $model3dViewedStateService;
        $this->reviewService = $reviewService;
        $this->printerMaterialGroupRepository = $printerMaterialGroupRepository;
        $this->printerMaterialRepository = $printerMaterialRepository;
        $this->printerColorRepository = $printerColorRepository;
        $this->predicateOnlyCertificated = $predicateOnlyCertificated;
    }


    /**
     * @param $usageTypeId
     * @param $model3d
     * @param $offersLocator
     */
    protected function initUsageFilter($usageTypeId, Model3dBaseInterface $model3d, OrderOfferLocatorService $offersLocator)
    {
        $usageMaterialGroups = $this->printerMaterialGroupRepository->getMaterialsGroupsByUsageFilter($usageTypeId);
        $usageMaterials = $this->printerMaterialRepository->getMaterialsByGroups($usageMaterialGroups);

        if ($usageMaterials) {
            $offersLocator->setAllowedMaterials($usageMaterials);
        }
        $model3dTextures = PrinterMaterialService::getModelTextures($model3d);
        foreach ($model3dTextures as $texture) {
            $isInSameGroup = false;
            foreach ($usageMaterialGroups as $usageMaterialGroup) {
                if ($usageMaterialGroup->code == $texture->calculatePrinterMaterialGroup()->code) {
                    $isInSameGroup = true;
                    break;
                }
            }
            if (!$isInSameGroup) {
                $model3d->isAnyTextureAllowed = true;
            }
        }
    }

    protected function loadModelState(Model3dBaseInterface $model3d)
    {
        $scaleBy = Yii::$app->request->get('scaleBy', Yii::$app->request->post('scaleBy', 100));
        $partsQty = Yii::$app->request->get('partsQty', Yii::$app->request->post('partsQty', []));
        if ($partsQty && is_string($partsQty)) {
            $partsQty = json_decode($partsQty, true);
        }
        $model3dStateString = \Yii::$app->request->get('modelTextureInfo', '');
        if ($model3dStateString) {
            $modelState = json_decode($model3dStateString, true);
        } else {
            $modelState = \Yii::$app->request->post('modelTextureInfo', []);
        }

        if ($modelState) {
            if (array_key_exists('isAnyTextureAllowed', $modelState)) {
                if ($modelState['isAnyTextureAllowed']) {
                    $model3d->isAnyTextureAllowed = true;
                } else {
                    $model3d->isAnyTextureAllowed = false;
                }
            }
            PrinterMaterialService::unSerializeModel3dTexture($model3d, $modelState);
        }


        if ($scaleBy > 0 && ($scaleBy < 99 || $scaleBy > 101)) {
            Model3dService::scaleModel3d($model3d, $scaleBy / 100);
        }
        foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
            if (array_key_exists($model3dPart->file_id, $partsQty)) {
                $model3dPart->qty = (int)$partsQty[$model3dPart->file_id];
            }
        }
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run()
    {
        $model3dUId = Yii::$app->request->get('model3dUId', Yii::$app->request->post('model3dUId'));
        $page = Yii::$app->request->get('page', Yii::$app->request->post('page', 0));
        $pageSize = Yii::$app->request->get('pageSize', Yii::$app->request->post('pageSize', 99999999));
        $manualSettedPsId = Yii::$app->request->get('manualSettedPsId', Yii::$app->request->post('manualSettedPsId'));
        $manualSettedPsPrinterId = Yii::$app->request->get('manualSettedPsPrinterId', Yii::$app->request->post('manualSettedPsPrinterId'));

        if ($model3dUId) {
            $model3dRepository = Yii::createObject(Model3dRepository::class);
            $model3d = $model3dRepository->getByUid($model3dUId);
        }

        $this->checkOwner($model3d);
        $this->loadModelState($model3d);

        $offersLocator = \Yii::createObject(OrderOfferLocatorService::class);
        $offersLocator->initPrintersBundle(UserFacade::getCurrentUser(), UserSessionFacade::getLocation());
        if ($usageTypeId = Yii::$app->request->get('usageTypeId', Yii::$app->request->post('usageTypeId'))) {
            $this->initUsageFilter($usageTypeId, $model3d, $offersLocator);
        }
        if ($sortMode = Yii::$app->request->get('sort', Yii::$app->request->post('sort'))) {
            $offersLocator->setSortMode($sortMode);
        }
        $offersLocator->onlyCertificated = $this->predicateOnlyCertificated->test($model3d);
        $offersLocator->manualSettedPsId = $manualSettedPsId;
        $offersLocator->manualSettedPsPrinterId = $manualSettedPsPrinterId;
        $offersLocator->formOffersListForModel($model3d);
        Model3dService::resetTextureMatreialInfo($model3d);
        $allOffersList = $offersLocator->getAllOffersList();
        $paginationOffersList = $offersLocator->getPaginationList($page * $pageSize, $pageSize);
        $offersLocator->formCosts($paginationOffersList, $model3d);

        $model3dItemForm = Model3dItemForm::create($model3d);
        $model3dViewedState = $this->model3dViewedStateService->registerModel3dViewForm($model3dItemForm);
        $obs = new Serializer(new OffersBundleSerializerExt(UserSessionFacade::getLocation(), $model3dViewedState, $this->reviewService));
        $offerBundleData = [
            'offers'     => $obs->serialize($paginationOffersList),
            'totalCount' => count($allOffersList),
        ];

        if ($offersLocator->emptyPrintersListReason) {
            $offerBundleData['messages'] = [$offersLocator->emptyPrintersListReason];
        }

        $allowedMaterials = $offersLocator->availableColorGroupsForCurrentPrintersList;
        $usedColors = PrinterMaterialService::getMaterialColorsItemUsedColors($allowedMaterials);
        $usedMaterialGroups = PrinterMaterialService::getMaterialColorsItemUsedMaterialGroups($allowedMaterials);
        $groupMaterials = PrinterMaterialService::formingGroupMaterialsListForColorSelector($allowedMaterials);
        $groupMaterials = array_values($groupMaterials);

        if ($usageTypeId) {
            $usageGroupTypes = PrinterMaterialGroup::getUsageGroupsByMaterials($offersLocator->allPossibleMaterials);
        } else {
            // Use all group materials
            $usageGroupTypes = PrinterMaterialGroup::getUsageGroups($groupMaterials);
        }

        $usageTypeSerialized = ArrayHelper::toArray($usageGroupTypes);
        $groupMaterialsSerialized = MaterialColorsItemSerializer::serialize($groupMaterials);
        $objectStorage = [];
        $objectStorage['PrinterColor'] = PrinterColorSerializer::serialize($usedColors);
        $objectStorage['PrinterMaterialGroup'] = PrinterMaterialGroupSerializer::serialize(array_values($usedMaterialGroups));
        $model3dState = Model3dTextureStateSerializer::serialize($model3d);

        return $this->controller->jsonReturn(
            [
                'success'          => true,
                'offersBundle'     => $offerBundleData,
                'allowedMaterials' => $groupMaterialsSerialized,
                'usageTypes'       => $usageTypeSerialized,
                'objectStorage'    => $objectStorage,
                'model3dState'     => $model3dState,
            ]
        );
    }

    protected function checkOwner(Model3dBaseInterface $model3d)
    {
        if (UserFacade::isObjectOwner($model3d)) {
            return;
        }
        $printablePackPublicToken = Yii::$app->session->get('printablePackPublicToken');
        $this->model3dService->tryValidateModelStore($model3d, null, $printablePackPublicToken);
    }
}