<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.10.17
 * Time: 17:10
 */

namespace frontend\controllers\catalog\actions\serializers;

use common\components\ArrayHelper;
use common\components\helpers\ColorHelper;
use common\components\ps\locator\materials\MaterialColorsItem;
use common\components\serizaliators\AbstractProperties;
use common\models\PrinterColor;

class MaterialColorsItemSerializer extends AbstractProperties
{

    public $shortListSize = 8;

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            MaterialColorsItem::class => [
                'id'              => 'materialGroup.id',
                'materialGroupId' => function (MaterialColorsItem $item) {
                    return $item->materialGroup->id;
                },
                'shortList'       => function (MaterialColorsItem $item) {
                    $colorRatings = $item->colorRatings;
                    arsort($colorRatings);
                    return \array_slice(array_keys($colorRatings), 0, $this->shortListSize);
                },
                'shortListGroup' => function (MaterialColorsItem $item) {
                    $shortListGroup = [];

                    foreach ($item->colorsGroupedByMaterialId as $k => $colors) {
                        /** @var PrinterColor $color */
                        foreach ($colors as $color) {
                            $shortListGroup[$k][$color->id] = $item->colorRatings[$color->id] ?? 0;
                        }

                        arsort($shortListGroup[$k]);
                        $shortListGroup[$k] = \array_slice(array_keys($shortListGroup[$k]), 0, $this->shortListSize);
                    }

                    return $shortListGroup;
                },
                'longList'        => function (MaterialColorsItem $item) {
                    $colorsByHSVSorted = [];
                    foreach ($item->colors as $printerColor) {
                        $colorsByHSVSorted[$printerColor->id] = ColorHelper::getColorSortCoefficient($printerColor->rgb);
                    }
                    asort($colorsByHSVSorted);
                    return array_keys($colorsByHSVSorted);
                },
                'longListGroup' => function (MaterialColorsItem $item) {
                    $shortListGroup = [];

                    foreach ($item->colorsGroupedByMaterialId as $k => $colors) {
                        /** @var PrinterColor $color */
                        foreach ($colors as $color) {
                            $shortListGroup[$k][$color->id] = ColorHelper::getColorSortCoefficient($color->rgb);
                        }

                        asort( $shortListGroup[$k]);
                        $shortListGroup[$k] = array_keys($shortListGroup[$k]);
                    }

                    return $shortListGroup;
                },
            ],
        ];
    }
}