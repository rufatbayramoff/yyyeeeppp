<?php

namespace frontend\controllers\catalog\actions\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\PrinterMaterial;

class PrinterMaterialSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            PrinterMaterial::class => [
                'id',
                'title',
                'group_id'
            ]
        ];
    }
}