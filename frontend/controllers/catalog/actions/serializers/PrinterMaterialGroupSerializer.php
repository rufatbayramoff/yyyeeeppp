<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.10.17
 * Time: 16:52
 */

namespace frontend\controllers\catalog\actions\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\PrinterMaterialGroup;

class PrinterMaterialGroupSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            PrinterMaterialGroup::class => [
                'id',
                'code',
                'title'            => 'title',
                'shortDescription' => 'short_description',
                'longDescription'  => 'long_description',
                'infill'           => function (PrinterMaterialGroup $printerMaterialGroup) {
                    return $printerMaterialGroup->getInfillArray();
                },
                'photoUrl'         => function (PrinterMaterialGroup $printerMaterialGroup) {
                    return $printerMaterialGroup->photoFile ? $printerMaterialGroup->photoFile->getFileUrl() : $printerMaterialGroup->getDefaultPhotoFileUrl();
                }
            ]
        ];
    }
}