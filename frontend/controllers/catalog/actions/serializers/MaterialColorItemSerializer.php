<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.12.18
 * Time: 16:08
 */

namespace frontend\controllers\catalog\actions\serializers;

use common\components\ArrayHelper;
use common\components\helpers\ColorHelper;
use common\components\ps\locator\materials\MaterialColorItem;
use common\components\serizaliators\AbstractProperties;
use common\models\PrinterColor;

class MaterialColorItemSerializer
{

    public static $shortListSize = 8;

    /**
     * @param MaterialColorItem[] $materialColorItems
     * @return array
     */
    public static function serialize($materialColorItems): array
    {
        $groups = [];

        // Reform array
        foreach ($materialColorItems as $key => $materialColorItem) {
            if ($materialColorItem->groupId) {
                if (array_key_exists($materialColorItem->groupId, $groups)) {
                    $groups[$materialColorItem->groupId]['groupId']            = $materialColorItem->groupId;
                    $groups[$materialColorItem->groupId]['materials']['all'][] = $materialColorItem;
                }
            } elseif ($materialColorItem->materialId) {
                $groupId                                                         = $materialColorItem->getMaterial()->group_id;
                $groups[$groupId]['groupId']                                     = $groupId;
                $groups[$groupId]['materials'][$materialColorItem->materialId][] = $materialColorItem; // Add m to save sort mode
            }
        }

        // Sort Colors by ratings for short list
        foreach ($groups as $groupId => $group) {
            foreach ($group['materials'] as $materialId => $materialInfo) {
                $longList = $shortList = $materialInfo;
                uasort($shortList, function (MaterialColorItem $a, MaterialColorItem $b) {
                    return $b->rating <=> $a->rating;
                });
                $shortListSerialized = [];
                foreach ($shortList as $key => $materialColorItem) {
                    $shortListSerialized[$key] = [
                        'colorId'    => $materialColorItem->colorId,
                        'materialId' => $materialColorItem->materialId
                    ];
                }
                $shortListSerialized = array_values(array_splice($shortListSerialized, 0, self::$shortListSize));

                uasort($longList, function (MaterialColorItem $a, MaterialColorItem $b) {
                    $coefficientA = ColorHelper::getColorSortCoefficient($a->getColor()->rgb);
                    $coefficientB = ColorHelper::getColorSortCoefficient($b->getColor()->rgb);
                    return $coefficientB <=> $coefficientA;
                });
                $longListSerialized = [];
                foreach ($longList as $key => $materialColorItem) {
                    $longListSerialized[$key] = [
                        'colorId'    => $materialColorItem->colorId,
                        'materialId' => $materialColorItem->materialId
                    ];
                }
                $longListSerialized = array_values($longListSerialized);

                $groups[$groupId]['materials'][$materialId] = [
                    'materialId' => $materialId,
                    'shortList'  => $shortListSerialized,
                    'longList'   => $longListSerialized
                ];
                $groups[$groupId]['groupId']                = $groupId;
            }
        }

        // Sort Materials
        foreach ($groups as $groupId => $group) {
            uasort($group['materials'], function ($a, $b) {
                if ($a['materialId'] == 'all') {
                    return -1;
                }
                if ($b['materialId'] == 'all') {
                    return 1;
                }
                return count($b['longList']) <=> count($a['longList']);
            });
            $groups[$groupId]['materials'] = array_values($group['materials']);
        }

        return $groups;
    }
}