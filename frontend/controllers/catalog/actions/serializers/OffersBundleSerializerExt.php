<?php
/**
 * Created by mitaichik
 */

namespace frontend\controllers\catalog\actions\serializers;


use common\components\orderOffer\OrderOffer;
use common\components\serizaliators\AbstractProperties;
use common\models\Company;
use common\models\Model3dViewedState;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\UserAddress;
use common\services\ReviewService;
use frontend\controllers\catalog\actions\models\Rating;
use frontend\models\ps\PsFacade;
use lib\geo\models\Location;

class OffersBundleSerializerExt extends AbstractProperties
{
    /**
     * @var Location
     */
    public $location;

    /**
     * @var Model3dViewedState
     */
    public $model3dViewedState;


    /**
     * @var \common\services\ReviewService
     */
    public $reviewService;

    public function __construct(Location $location, Model3dViewedState $model3dViewedState = null, ReviewService $reviewService = null)
    {
        $this->location           = $location;
        $this->reviewService      = $reviewService;
        $this->model3dViewedState = $model3dViewedState;
        parent::__construct([]);
    }

    /**
     * @return array
     * @throws \yii\base\InvalidParamException
     *
     *
     */
    public function getProperties()
    {
        return [
            OrderOffer::class => [
                'psPrinter'           => function (OrderOffer $orderOffer) {
                    return $orderOffer->getPrinter();
                },
                'anonimysPrice',
                'userPrice',
                'tsBonusAmount'       => function (OrderOffer $orderOffer) {
                    return $orderOffer->tsBonusAmount;
                },
                'estimateDeliveryCost',
                'deliveryTypes',
                'certificatedMachine' => function (OrderOffer $orderOffer) {
                    return $orderOffer->certificatedMachine();
                },
                'peliability'         => function (OrderOffer $orderOffer) {
                    return $orderOffer->peliabilityFormat();
                },
                'responseTime'        => function (OrderOffer $orderOffer) {
                    return $orderOffer->responseTimeFormat();
                },
                'isOnlyPickup'        => function (OrderOffer $orderOffer) {
                    return $orderOffer->hasPickupOnly();
                }
            ],
            PsPrinter::class  => [
                'id',
                'ps',
                'isStatusCertificated' => function (PsPrinter $psPrinter) {
                    return $psPrinter->isCertificated();
                },
                'address'              => function (PsPrinter $printer) {
                    return UserAddress::formatPrinterAddress($printer, $this->getAddressTemplate($printer));
                },
                'title',
                'publicLink'           => function (PsPrinter $printer) {
                    if (!$this->model3dViewedState) {
                        return null;
                    }
                    return PsFacade::getPsLink(
                        $printer->company,
                        ['selectedPrinterId' => $printer->id, 'model3dViewedStateId' => $this->model3dViewedState->id ?? null]
                    );
                },
                'topService'           => function (PsPrinter $printer) {
                    return $printer?->company?->psCatalog?->country_rating ?? 0;
                },
                'reviewsInfo'          => function (PsPrinter $printer) {
                    return $printer?->company?->psCatalog ?
                        [
                            'rating'       => $printer?->company?->psCatalog?->rating_avg,
                            'reviewsCount' => $printer?->company?->psCatalog?->rating_count
                        ] : null;
                }
            ],
            Company::class    => [
                'title',
                'logoUrl'     => function (Ps $ps) {
                    return Ps::getCircleImageByPs($ps);
                },
                'reviewsInfo' => function (Ps $ps) {
                    if (!$this->reviewService) {
                        return null;
                    }
                    $count = $ps->psCatalog->rating_count ?? 0;

                    if (!$count) {
                        return null;
                    }

                    $rating = Rating::create($count, $ps->psCatalog->rating_avg);

                    return $rating;
                }
            ],
        ];
    }

    /**
     * @param PsPrinter $printer
     * @return string
     */
    private function getAddressTemplate(PsPrinter $printer): string
    {
        return $this->location->country != $printer->companyService->location->country->iso_code
            ? '%city%, %region%, %country%'
            : '%city%, %region%';
    }
}
