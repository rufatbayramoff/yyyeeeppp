<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.10.17
 * Time: 16:46
 */

namespace frontend\controllers\catalog\actions\serializers;

use common\components\serizaliators\AbstractProperties;
use common\models\PrinterColor;

class PrinterColorSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            PrinterColor::class => [
                'id',
                'code'   => 'render_color',
                'rgbHex',
                'title'  => 'title'
            ]
        ];
    }
}
