<?php
/**
 * Created by mitaichik
 */

namespace frontend\controllers\catalog\actions\serializers;


use common\components\serizaliators\AbstractProperties;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBaseInterface;
use common\models\Model3d;
use common\models\Model3dImg;
use common\models\Model3dPart;
use common\models\Model3dReplica;
use common\models\Model3dReplicaImg;
use common\models\Model3dReplicaPart;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;

class Model3dSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        $model = [
            'id',
            'uid'         => function (Model3dBaseInterface $model3d) {
                return $model3d->getUid();
            },
            'title',
            'modelUnits'  => 'model_units',
            'storeUnitId' => function (Model3dBaseInterface $model3d) {
                return $model3d->storeUnit->id ?? null;
            },
            'weight',
            'parts'       => function (Model3dBaseInterface $model3d) {
                return array_reverse(
                    array_values($model3d->getActiveModel3dParts())
                ); // DON`t change It. If you need only calculated parts, create other field, calculated parts or use post filter.
            },
            'canceledParts' => function (Model3dBaseInterface $model3d) {
                return array_keys($model3d->getCanceledParts());
            },
            'images'      => function (Model3dBaseInterface $model3d) {
                return array_values($model3d->getActiveModel3dImages());
            },
            'coverImgUrl' => function (Model3dBaseInterface $model3d) {
                return Model3dFacade::getCover($model3d)['image'];
            },
        ];

        $img = [
            'id'              => 'id',
            'fileId'          => 'file_id',
            'fileMd5NameSize' => function (Model3dBaseImgInterface $img) {
                return $img->file->getMd5NameSize();
            },
            'title'           => function (Model3dBaseImgInterface $img) {
                return $img->getTitle();
            },
            'previewUrl'      => function (Model3dBaseImgInterface $img) {
                return ImageHtmlHelper::getThumbUrlForFile($img->file, ImageHtmlHelper::DEFAULT_WIDTH, ImageHtmlHelper::DEFAULT_HEIGHT);
            }
        ];

        return [
            Model3d::class            => $model,
            Model3dReplica::class     => $model,
            Model3dImg::class         => $img,
            Model3dReplicaImg::class  => $img,
            Model3dPart::class        => Model3dPartSerializer::class,
            Model3dReplicaPart::class => Model3dPartSerializer::class
        ];
    }
}