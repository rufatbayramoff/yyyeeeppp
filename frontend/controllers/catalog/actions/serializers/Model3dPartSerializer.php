<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 31.01.18
 * Time: 17:32
 */

namespace frontend\controllers\catalog\actions\serializers;

use common\components\serizaliators\AbstractProperties;
use common\interfaces\Model3dBasePartInterface;
use common\models\Model3dPart;
use common\models\Model3dReplicaPart;
use common\services\Model3dPartService;

class Model3dPartSerializer extends AbstractProperties
{
    public function getProperties()
    {
        $part = [
            'id'                    => 'id',
            'uid'                   => function (Model3dBasePartInterface $part) {
                return $part->getUid();
            },
            'fileId'                => 'file_id',
            'name',
            'partReplicaId'         => function (Model3dBasePartInterface $part) {
                return $part instanceof Model3dReplicaPart ? $part->id : null;
            },
            'originalModel3dPartId' => function (Model3dBasePartInterface $part) {
                return $part->getOriginalModel3dPartObj()->id;
            },
            'title'                 => function (Model3dBasePartInterface $part) {
                return $part->getTitle();
            },
            'qty'                   => function (Model3dBasePartInterface $part) {
                return (int)$part->qty;
            },
            'fileMd5NameSize'       => function (Model3dBasePartInterface $part) {
                return $part->file->getMd5NameSize();
            },
            'caption'               => function (Model3dBasePartInterface $part) {
                return Model3dPartService::getModel3dPartCaption($part, true);
            },
            'hash'                  => function (Model3dBasePartInterface $part) {
                return $part->file->md5sum;
            },
            'fileUuid'                  => function (Model3dBasePartInterface $part) {
                return $part->file->uuid;
            },
            'previewUrl'            => function (Model3dBasePartInterface $part) { // Always return url, js check if file exists should be
                return Model3dPartService::getModel3dPartRenderUrl($part);
            },
            'size',
            'originalSize',
            'weight'                => function (Model3dBasePartInterface $part) {
                return round($part->getWeight(), 2);
            },
            'pb'                    => function (Model3dBasePartInterface $part) {
                return $part->model3d->getCadFlag();
            },
            'isParsed'              => function (Model3dBasePartInterface $part) {
                return $part->isParsed();
            },
            'cncParams'             => function(Model3dBasePartInterface $part){
                if($part->model3dPartCncParams) {
                    return $part->model3dPartCncParams->toArray(['analyze','filepath','finefile','id','roughfile']);
                }
                return null;
            },
            'isMulticolorFormat'    => function (Model3dBasePartInterface $part) {
                return $part->isMulticolorFormat();
            },
            'fileExt'               => function (Model3dBasePartInterface $part) {
                return $part->file->extension;
            },
            'renderUrlBase'         => function (Model3dBasePartInterface $part) {
                return \Yii::$app->getModule('model3dRender')->renderer->getRenderImageUrlBase($part->file);
            },
            'isConverted'
        ];
        return [
            Model3dPart::class        => $part,
            Model3dReplicaPart::class => $part
        ];
    }
}