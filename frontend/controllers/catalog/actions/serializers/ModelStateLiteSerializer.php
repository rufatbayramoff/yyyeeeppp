<?php
/**
 * Created by mitaichik
 */

namespace frontend\controllers\catalog\actions\serializers;


use common\components\serizaliators\AbstractProperties;
use common\interfaces\Model3dBaseInterface;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\models\Model3dTexture;
use common\models\PrinterColor;
use common\models\PrinterMaterial;
use common\models\PrinterMaterialGroup;
use frontend\models\model3d\Model3dItemForm;

class ModelStateLiteSerializer extends AbstractProperties
{
    /**
     * @return array
     */
    public function getProperties()
    {
        $model = [
            'id',

            'isOneTextureForKit' => function (Model3dBaseInterface $model3d) {
                return $model3d->isOneTextureForKit();
            },

            'textures' => function (Model3dBaseInterface $model3d) {

                if ($model3d->isOneTextureForKit()) {
                    return null;
                }

                $textures = [];

                foreach ($model3d->getActiveModel3dParts() as $part) {
                    $textures[$part->id] = $part->getTexture();
                }

                return $textures;
            },

            'kitTexture'          => function (Model3dBaseInterface $model3d) {
                return $model3d->getKitTexture();
            },
            'usageType'           => function () {
                return '';
            },
            'isAnyTextureAllowed' => function (Model3dBaseInterface $model3d) {
                return $model3d->isAnyTextureAllowed();
            }
        ];


        return [
            Model3d::class              => $model,
            Model3dReplica::class       => $model,
            PrinterColor::class         => PrinterColorSerializer::class,
            PrinterMaterialGroup::class => PrinterMaterialGroupSerializer::class,
            PrinterMaterial::class      => PrinterMaterialSerializer::class,
            Model3dTexture::class       => [
                'printerColor',
                'printerMaterial',
                'printerMaterialGroup' => function (Model3dTexture $model3dTexture) {
                    return $model3dTexture->calculatePrinterMaterialGroup();
                },
                'infill'
            ]
        ];
    }
}