<?php
/**
 * Created by mitaichik
 */

namespace frontend\controllers\catalog\actions\serializers;


use common\components\serizaliators\AbstractProperties;
use frontend\models\model3d\Model3dItemForm;

class OrderFormSerializer extends AbstractProperties
{

    /**
     * @return array
     */
    public function getProperties()
    {
        return [
            Model3dItemForm::class => [
                'partsQty',
            ]
        ];
    }
}