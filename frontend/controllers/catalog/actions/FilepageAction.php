<?php
/**
 * Created by mitaichik
 */

namespace frontend\controllers\catalog\actions;


use backend\components\NoAccessException;
use common\components\BaseController;
use common\components\exceptions\InvalidModelException;
use common\components\WidgetStatisticsRegistrar;
use common\models\factories\Model3dReplicaFactory;
use common\models\Model3d;
use common\models\Model3dReplica;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\repositories\PrinterMaterialGroupRepository;
use common\models\repositories\PrinterMaterialRepository;
use common\models\WidgetStatistics;
use common\services\Model3dService;
use common\services\PrinterMaterialService;
use frontend\components\UserSessionFacade;
use frontend\controllers\catalog\actions\serializers\PrinterColorSerializer;
use frontend\controllers\catalog\actions\serializers\PrinterMaterialGroupSerializer;
use frontend\models\model3d\Model3dFacade;
use frontend\models\model3d\Model3dForm;
use frontend\models\model3d\Model3dItemForm;
use frontend\models\user\UserFacade;
use lib\MeasurementUtil;
use Yii;
use yii\base\Action;
use yii\web\GoneHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class FilepageAction
 *
 * @package frontend\controllers\catalog\actions
 * @property BaseController $controller
 */
class FilepageAction extends Action
{
    /**
     * @var Model3dService
     */
    protected $model3dService;

    /**
     * @var PrinterMaterialRepository
     */
    protected $printerMaterialRepository;

    /**
     * @var PrinterMaterialGroupRepository
     */
    protected $printerMaterialGroupRepository;

    /**
     * @param Model3dService $model3dService
     * @param PrinterMaterialRepository $printerMaterialRepository
     * @param PrinterMaterialGroupRepository $printerMaterialGroupRepository
     */
    public function injectDependencies(
        Model3dService $model3dService,
        PrinterMaterialRepository $printerMaterialRepository,
        PrinterMaterialGroupRepository $printerMaterialGroupRepository
    )
    {
        $this->model3dService                 = $model3dService;
        $this->printerMaterialRepository      = $printerMaterialRepository;
        $this->printerMaterialGroupRepository = $printerMaterialGroupRepository;
    }

    /**
     * @param $id
     * @return string|array
     * @throws NoAccessException
     * @throws NotFoundHttpException
     * @throws \HttpException
     * @throws \common\components\exceptions\BusinessException
     * @throws \common\components\operations\PostValidateException
     * @throws \common\components\operations\PreValidateException
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function run($id)
    {
        $model3d = Model3dFacade::getCatalogItem($id);

        if ($model3d->company && !$model3d->company->isActive() && !UserFacade::isObjectOwner($model3d)) {
            throw new NotFoundHttpException('The company that published the model is not active.');
        }

        $this->checkForRedirect($model3d);

        if ($checkOwnerResponse = $this->checkOwner($model3d)) {
            return $checkOwnerResponse;
        }

        $model3dItemForm = $this->createModel3dEditForm($model3d);

        if ($model3dReplicaId = Yii::$app->request->get('model3dReplicaId')) {
            $model3dReplica = Model3dReplica::tryFindByPk($model3dReplicaId);
            if (!UserFacade::isObjectOwner($model3dReplica)) {
                throw new NoAccessException('Unavailible model3dReplica for current user');
            }
            $model3dReplica->setStoreUnit($model3d->storeUnit);
            $model3dParts = $model3dReplica->getActiveModel3dParts();
            // Return qty parts to form TODO: REMOVE THIS BLOCK AFTER REMOVE partsQty var from Model3dItemForm class
            foreach ($model3dParts as $model3dPart) {
                $model3dItemForm->partsQty[$model3dPart->file->id] = $model3dPart->qty;
            }
            $model3d = $model3dReplica;
            $model3d->storeUnit->setModel3d($model3dReplica);
            $model3dItemForm->model3d = $model3dReplica;
        }
        if (Yii::$app->request->isPost) {
            $formData = Yii::$app->request->get('prices')
                ? Yii::$app->request->getBodyParams()
                : Yii::$app->request->getBodyParams()['Model3dEditForm'];

            $model3dItemForm->load($formData);
            $scaleBy                       = isset($formData['scaleBy']) ? (float)$formData['scaleBy'] : 0;
            $model3dItemForm->scaleMeasure = isset($formData['measure']) ? $formData['measure'] : $model3dItemForm->scaleMeasure;

            $isNeedScale = $scaleBy > 0.1 && (round($scaleBy, 1) != 100);

            if ($isNeedScale || $model3dItemForm->partsQty) {

                //$model3dReplica = Model3dReplicaFactory::createModel3dReplica($model3d, $model3dItemForm->partsQty);

                if ($isNeedScale) {
                    $model3dItemForm->scaleBy = $scaleBy;
                    $model3dReplica           = Model3dService::scaleModel3d($model3dReplica, $scaleBy / 100);
                }

                $model3dReplica->setStoreUnit($model3d->storeUnit);
                $model3d = $model3dReplica;
                $model3d->storeUnit->setModel3d($model3dReplica);
                $model3dItemForm->model3d = $model3dReplica;
            }
        }
        $model3d->isAnyTextureAllowed = true; // Allow first open page reset texture

        $externalWidgetType = $this->getWidgetType();

        $usedColors         = [];
        $usedMaterialGroups = [];
        foreach (PrinterMaterialService::getModelTextures($model3d) as $texture) {
            $usedColors[$texture->printerColor->id]                            = $texture->printerColor;
            $usedMaterialGroups[$texture->calculatePrinterMaterialGroup()->id] = $texture->calculatePrinterMaterialGroup();
        }

        $objectStorage                         = [];
        $objectStorage['PrinterColor']         = PrinterColorSerializer::serialize($usedColors);
        $objectStorage['PrinterMaterialGroup'] = PrinterMaterialGroupSerializer::serialize(array_values($usedMaterialGroups));


        $data = [
            'model3dItemForm'       => $model3dItemForm,
            'isExternalWidget'      => (bool)$externalWidgetType,
            'manualSettedPs'        => $this->getManualPs() ? $this->getManualPs() : ($this->getManualPrinter() ? $this->getManualPrinter()->ps : null),
            'manualSettedPsPrinter' => $this->getManualPrinter(),
            'seo'                   => $this->controller->seo,
            'objectStorage'         => $objectStorage
        ];


        $tpl = 'item3.php';

        if ($externalWidgetType === Model3dForm::WIDGET_TYPE_PS_PRINTER) {
            // Inserted in iframe as embed block to user site
            $this->controller->layout                = 'plain.php';
            Yii::$app->params['additionalBodyClass'] = 'item-rendering-external-widget';

        }
        if (
            $externalWidgetType === Model3dForm::WIDGET_TYPE_VIEW_MODEL ||
            $externalWidgetType === Model3dForm::WIDGET_TYPE_VIEW_MODEL_BUY_WIDGET
        ) {
            // Inserted in iframe as embed block to user site
            WidgetStatisticsRegistrar::registerWidgetStatistics(WidgetStatistics::TYPE_DESIGNER_SITE, $model3d->user);
            $tpl                      = 'itemEmbed.php';
            $this->controller->layout = 'plain.php';
            $data['showImage']        = \Yii::$app->request->get('img', true);
            $data['showPrice']        = \Yii::$app->request->get('price', true);
            $data['buyWidget']        = $externalWidgetType === Model3dForm::WIDGET_TYPE_VIEW_MODEL_BUY_WIDGET;
        }

        return $this->controller->renderAdaptive($tpl, $data);
    }


    /**
     * @return null|string
     */
    private function getWidgetType()
    {
        if (Yii::$app->request->get('widget') === Model3dForm::WIDGET_TYPE_PS_PRINTER) {
            return Model3dForm::WIDGET_TYPE_PS_PRINTER;
        } elseif (Yii::$app->request->get('widget') === Model3dForm::WIDGET_TYPE_VIEW_MODEL_BUY_WIDGET) {
            return Model3dForm::WIDGET_TYPE_VIEW_MODEL_BUY_WIDGET;
        } elseif (Yii::$app->request->get('widget')) {
            return Model3dForm::WIDGET_TYPE_VIEW_MODEL;
        }
        return null;
    }

    /**
     * @param Model3d $model
     * @return Model3dItemForm
     */
    private function createModel3dEditForm(Model3d $model)
    {
        $form               = Model3dItemForm::create($model);
        $form->scaleMeasure = UserSessionFacade::getMeasurement() === MeasurementUtil::IN ? MeasurementUtil::IN : MeasurementUtil::MM;

        $currentFileId = null;
        if (Yii::$app->request->post('Model3dEditForm') && array_key_exists('fileId', Yii::$app->request->post('Model3dEditForm'))) {
            $currentFileId = (int)Yii::$app->request->post('Model3dEditForm')['fileId'];
        }
        if ($currentFileId) {
            $form->setCurrentSelectedFotoramaFileId($currentFileId);
        } else {
            $form->setFirstModel3dPartAsFotoramaFileId();
        }

        if (Yii::$app->request->get('materialGroupUsageFilter')) {
            $form->materialGroupUsageFilter = Yii::$app->request->get('materialGroupUsageFilter');
        } else {
            if (Yii::$app->session->get('upload_usage', false)) {
                $form->materialGroupUsageFilter = (string)Yii::$app->session->get('upload_usage');
                // now unset
                Yii::$app->session->set('upload_usage', null);
            }
        }
        if (Yii::$app->request->post('Model3dEditForm') && array_key_exists('materialGroupUsageFilter', Yii::$app->request->post('Model3dEditForm'))) {
            $form->materialGroupUsageFilter = Yii::$app->request->post('Model3dEditForm')['materialGroupUsageFilter'];
        }

        if (Yii::$app->request->get('printerColorId') || Yii::$app->request->get('printerMaterialGroupId')) {
            $form->loadGetParamsColors(Yii::$app->request->queryParams);
        }

        return $form;
    }

    /**
     * Check url and redirect to right seo page
     *
     * @param Model3d $model3d
     * @throws \yii\base\ExitException
     */
    private function checkForRedirect(Model3d $model3d)
    {
        $link = Model3dFacade::getStoreUrl($model3d);
        if ('/' . app('request')->pathInfo !== $link) {
            $this->controller->redirect($link, 301);
            \Yii::$app->end();
        }
    }

    /**
     * @return null|Ps
     */
    private function getManualPs()
    {
        if ($psId = Yii::$app->request->get('psId')) {
            return Ps::findByPk($psId);
        }
        return null;
    }

    /**
     * @return null|PsPrinter
     */
    private function getManualPrinter()
    {
        if ($printerId = Yii::$app->request->get('psPrinterId')) {
            return PsPrinter::findByPk((int)$printerId);
        }
        return null;
    }

    /**
     * @return bool
     */
    private function getIsManualSettedPsPrinterOnly()
    {
        return (bool)Yii::$app->request->get('psPrinterIdOnly');
    }


    /**
     * @param Model3d $model3d
     * @return null|string|Response
     * @throws NotFoundHttpException
     * @throws \HttpException
     */
    private function checkOwner(Model3d $model3d)
    {
        if (UserFacade::isObjectOwner($model3d)) {
            return null;
        }

        $akey                     = Yii::$app->request->get('akey');
        $printablePackPublicToken = Yii::$app->request->get('printablePackPublicToken');

        $session    = Yii::$app->session;
        $akeyTested = $akey ? $akey : $session->get('model3d_akey', $akey);
        if ($akey) {
            $session->set('model3d_akey', $akeyTested);
            $session->set('printablePackPublicToken', null);
        } else {
            $printablePackPublicToken = $printablePackPublicToken ?: $session->get('printablePackPublicToken');
            $session->set('printablePackPublicToken', $printablePackPublicToken);
            $session->set('model3d_akey', null);
        }

        try {
            $this->model3dService->tryValidateModelStore($model3d, $akeyTested, $printablePackPublicToken);
        } catch (GoneHttpException $e) {

            if ($this->getWidgetType() == Model3dForm::WIDGET_TYPE_PS_PRINTER) {

                if ($manualPs = $this->getManualPs()) {
                    return $this->controller->redirect('/my/ps-widget/?psId=' . $manualPs->id);
                } elseif ($manualPrinter = $this->getManualPrinter()) {
                    return $this->controller->redirect('/my/ps-widget/?psPrinterId=' . $manualPrinter->id);
                }
            } else {
                return $this->controller->renderAdaptive('itemNotPublished', ['model3d' => $model3d]);
            }
        }
        return null;
    }

    public function actionGetPrinters($id)
    {

    }
}