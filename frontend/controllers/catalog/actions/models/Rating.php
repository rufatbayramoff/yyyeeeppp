<?php
/**
 * Created by mitaichik
 */

namespace frontend\controllers\catalog\actions\models;


class Rating
{
    /**
     * @var int
     */
    public $reviewsCount;

    /**
     * @var float
     */
    public $rating;

    public static function create(int $count,float $rating)
    {
        $self = new self();
        $self->reviewsCount = $count;
        $self->rating = $rating;
        return $self;
    }
}