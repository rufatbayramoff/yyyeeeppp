<?php

namespace frontend\assets;

use frontend\components\angular\AngularAsset;

class AngularJsSliderAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@vendor/angular-slider/angularjs-slider/dist';

    public $js = [
        'rzslider.min.js',
    ];

    public $css = [
        'rzslider.min.css',
    ];

    public $depends = [
        AngularAsset::class,
    ];
}