<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.09.17
 * Time: 14:10
 */

namespace frontend\assets;

use backend\assets\CommonAsset;
use yii\web\AssetBundle;

class ImageCropClassAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/ts/imageCropClass.js',
    ];

    public $depends = [
        ImageCropAsset::class,
        CommonAsset::class,
    ];
}