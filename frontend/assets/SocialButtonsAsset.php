<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.08.16
 * Time: 14:40
 */

namespace frontend\assets;

use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;

class SocialButtonsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';


    public $js = [
        'js/ts/socialButtons.js',
    ];

    public $depends = [
        AppAsset::class,
        BootstrapPluginAsset::class,
    ];
}