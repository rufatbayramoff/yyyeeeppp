<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.07.17
 * Time: 11:38
 */

namespace frontend\assets;

use beowulfenator\JsonEditor\JsonEditorAsset;
use yii\web\AssetBundle;

class JsonEditorAssetJsTemplate extends AssetBundle
{
    public $sourcePath = '@frontend/web/js';

    public $js = [
        'jsoneditorTemplateProcessor.js',
    ];

    public $depends = [
        JsonEditorAsset::class,
    ];
}
