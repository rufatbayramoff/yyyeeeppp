<?php
/**
 * Created by mitaichik
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class CollectionAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/ts/collection/list.js',
        'js/ts/collection/view.js',
    ];

    public $depends = [
        AppAsset::class
    ];
}