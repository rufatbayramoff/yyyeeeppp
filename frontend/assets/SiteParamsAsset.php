<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.06.17
 * Time: 11:35
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class SiteParamsAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/js';

    public $js = [
        'siteParams.js',
    ];
}