<?php


namespace frontend\assets;


use yii\web\AssetBundle;

class BoostrapRequireFieldAsset  extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/boostrap-require-field.css'
    ];

    public $depends = [
        AppAsset::class
    ];
}