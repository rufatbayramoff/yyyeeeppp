<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.07.17
 * Time: 11:38
 */

namespace frontend\assets;

use beowulfenator\JsonEditor\JsonEditorAsset;
use yii\web\AssetBundle;

class JsonEditorAssetJsTemplateTheme extends AssetBundle
{
    public $sourcePath = '@frontend/web/js';

    public $js = [
        'ts/app/ps/printers/theme.js',
    ];

    public $depends = [
        JsonEditorAsset::class,
    ];
}
