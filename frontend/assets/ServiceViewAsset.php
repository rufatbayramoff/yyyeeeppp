<?php


namespace frontend\assets;


use yii\web\AssetBundle;

class ServiceViewAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/js/ts';

    public $js = [
        'serviceView.js',
    ];
}