<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 11.01.17
 * Time: 11:30
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class PreloadModel3dRendering extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';


    public $js = [
        'js/preloadModel3dClass.js',
    ];

    public $css = [
        'css/preloadModel3dClass.css',
    ];

    public $depends = [
        AppAsset::class,
    ];
}