<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 04.07.16
 * Time: 14:19
 */

namespace frontend\assets;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class JsURIAsset extends AssetBundle
{
    public $sourcePath = '@vendor/medialize/URI/src';
    public $js = [
        'URI.min.js',
        'jquery.URI.min.js'
    ];

    public $depends = [
        JqueryAsset::class,
        YiiAsset::class
    ];
}