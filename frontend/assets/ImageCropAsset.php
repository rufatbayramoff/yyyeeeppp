<?php
namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class ImageCropAsset extends AssetBundle
{

    public $sourcePath = '@vendor/fengyuanchen/cropper/dist';

    public $depends = [
        YiiAsset::class,
        JqueryAsset::class,
    ];

    public function init()
    {
        $this->css[] = YII_DEBUG ? 'cropper.css' : 'cropper.min.css';
        $this->js[] = YII_DEBUG ? 'cropper.js' : 'cropper.min.js';
    }
}