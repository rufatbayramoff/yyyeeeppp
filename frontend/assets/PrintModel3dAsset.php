<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.09.17
 * Time: 15:12
 */

namespace frontend\assets;

use common\modules\model3dRender\assets\Model3dRenderer2Asset;
use yii\web\AssetBundle;

class PrintModel3dAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/print-model3d/print-model3d.css',
    ];

    public $depends = [
        AppAsset::class,
        Model3dRenderer2Asset::class
    ];
}