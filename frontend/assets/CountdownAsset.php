<?php
/**
 * User: nabi
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class CountdownAsset extends AssetBundle
{
    public $sourcePath = 'js';
    public $css = [];
    public $js = ['jquery.countdown.min.js'];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}