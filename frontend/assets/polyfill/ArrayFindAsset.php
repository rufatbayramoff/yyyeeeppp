<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 26.10.17
 * Time: 11:05
 */


namespace frontend\assets\polyfill;


use yii\web\AssetBundle;

class ArrayFindAsset extends AssetBundle
{
    public $sourcePath = '@vendor/jsPolyfill/array';

    public $js = [
        'find.js',
    ];
}