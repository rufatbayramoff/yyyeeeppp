<?php


namespace frontend\assets;

use yii\web\AssetBundle;

class FlagsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/components/flag-icon-css';

    public $css = [
        'css/flag-icon.css'
    ];

    public $depends = [
        AppAsset::class
    ];
}
