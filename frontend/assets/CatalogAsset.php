<?php
/**
 * Created by mitaichik
 */

namespace frontend\assets;


use frontend\widgets\assets\StoreUnitWidgetAssets;
use yii\web\AssetBundle;

class CatalogAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/ts/catalog/catalog-items.js',
        'js/ts/catalog/catalog-item.js',
        'js/ts/catalog/catalog-items-filter.js',
    ];

    public $depends = [
        AppAsset::class,
        StoreUnitWidgetAssets::class
    ];
}