<?php
/**
 * Created by mitaichik
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class ModelViewerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/viewer/Three.js',
        'js/viewer/plane.js',
        'js/viewer/thingiview.js',
    ];
}