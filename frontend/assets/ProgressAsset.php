<?php


namespace frontend\assets;


use yii\web\AssetBundle;

class ProgressAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/progress.css',
    ];

    public $depends = [
        AppAsset::class,
    ];
}