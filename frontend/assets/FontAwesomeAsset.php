<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class FontAwesomeAsset extends AssetBundle
{

    public $sourcePath = '@bower/font-awesome';

    public $css = [
        'css/font-awesome.min.css'
    ];

    public $publishOptions = [
        'only' => [
            'fonts/',
            'css/'
        ]
    ];
}
