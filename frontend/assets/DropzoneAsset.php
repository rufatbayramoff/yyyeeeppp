<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.12.16
 * Time: 12:24
 */


namespace frontend\assets;

use yii\web\AssetBundle;

class DropzoneAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/rsvp.js',
        'js/frame-grab.js',
        'js/dropzone.js'
    ];

    public $css = [
        'css/dropzone.css'
    ];

    public $depends = [
        AppAsset::class
    ];
}
