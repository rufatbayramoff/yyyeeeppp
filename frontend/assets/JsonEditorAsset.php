<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.08.18
 * Time: 17:22
 */

namespace frontend\assets;

use backend\assets\CommonAsset;
use common\modules\comments\assets\CommentsAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class JsonEditorAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/js';

    public $js = [
        'jsoneditor.min.js',
    ];

    public $depends = [
        CommentsAsset::class,
    ];
}