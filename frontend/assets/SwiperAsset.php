<?php
/**
 * Created by mitaichik
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class SwiperAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/swiper.jquery.min.js',
    ];

    public $css = [
        'css/swiper.min.css'
    ];

    public $depends = [
        AppAsset::class,
    ];
}