<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 03.08.16
 * Time: 12:27
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class Model3dItemFormAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';


    public $js = [
        'js/ts/model3dItemForm.js',
    ];

    public $depends = [
        AppAsset::class,
    ];
}