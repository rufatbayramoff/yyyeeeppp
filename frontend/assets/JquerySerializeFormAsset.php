<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 *
 * @author Nabi Ibatulin <n.ibatulin@headrobotics.org>
 */
class JquerySerializeFormAsset extends AssetBundle
{
    public $sourcePath = '@vendor/danheberden/jquery-serializeForm/dist';

    public $js = [
        'jquery-serializeForm.min.js'
    ];

    public $depends = [
        JqueryAsset::class
    ];
}
