<?php
/**
 * Created by mitaichik
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class CncModelViewerAsset extends AssetBundle
{
    public $sourcePath = "@app/../tools/node/www/cloudcam/js";

    public $js = [
        'three.js',
        'Detector.js',
        'STLLoader.js',
        'ST2Loader.js',
        'OrbitControls.js',
    ];
}