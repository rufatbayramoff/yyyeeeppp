<?php
/**
 * Created by mitaichik
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class DatetimePickerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/moment-with-locales.js',
        'js/bootstrap-datetimepicker.js',
    ];

    public $css = [
        'css/bootstrap-datetimepicker.css'
    ];

    public $depends = [
        AppAsset::class
    ];
}