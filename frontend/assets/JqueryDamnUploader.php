<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.09.17
 * Time: 10:24
 */

namespace frontend\assets;

use yii\web\AssetBundle;


class JqueryDamnUploader extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        '/js/jquery.damnUploader.js'
    ];

    public $depends = [
        AppAsset::class
    ];
}