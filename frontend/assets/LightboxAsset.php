<?php
/**
 *
 */
namespace frontend\assets;


use yii\web\AssetBundle;

class LightboxAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';


    public $js = [
        'js/lightbox.min.js'
    ];

    public $css = [
        'css/lightbox.min.css'
    ];

    public $depends = [
        AppAsset::class,
    ];
}