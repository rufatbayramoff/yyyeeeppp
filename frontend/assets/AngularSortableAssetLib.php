<?php

namespace frontend\assets;

use frontend\components\angular\AngularAsset;

/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.07.18
 * Time: 16:00
 */

class AngularSortableAssetLib extends \yii\web\AssetBundle
{
    public $sourcePath = '@vendor/kamilkp/angular-sortable-view/src';

    public $js = [
        'angular-sortable-view.js',
    ];

    public $depends = [
        AngularAsset::class,
    ];
}