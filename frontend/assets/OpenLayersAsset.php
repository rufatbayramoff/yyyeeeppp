<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 13.04.17
 * Time: 16:50
 */
namespace frontend\assets;

use yii\web\AssetBundle;

class OpenLayersAsset extends  AssetBundle
{
    public $sourcePath = '@vendor/openlayers/openlayers';

    public $js = [
        'OpenLayers.js',
    ];

    public $css = [
    ];
}