<?php
namespace frontend\assets;

use backend\assets\CommonAsset;
use common\modules\model3dRender\assets\Model3dRenderer2Asset;
use common\modules\translation\assets\TranslateFunctionTAsset;
use frontend\assets\polyfill\ArrayFindAsset;
use frontend\components\angular\AngularAsset;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/normalize.css',
        'css/bootstrap-social.css',
        'css/checkbox.css',
        'css/site.css',
        'css/search-block.css',
        'css/helper.css',
        'css/main-page.css',
        'css/ts-icons.css',
        'css/product-card.css',
        'css/vr-card.css',
        'css/model-page.css',
        'css/ps.css',
        'css/store.css',
        'css/catalog-item.css',
        'css/model-card.css',
        'css/category-page.css',
        'css/3dprototyping.css',
        'css/designers-catalog.css',
        'css/private-profile.css',
        'css/ps-pub-profile.css',
        'css/product-page.css',
        'css/product-card.css',
        'css/product-edit.css',
        'css/collection.css',
        'css/order.css',
        'css/delivery.css',
        'css/error-pages.css',
        'css/about-page.css',
        'css/how-works.css',
        'css/upload-page.css',
        'css/help-page.css',
        'css/blog.css',
        'css/guides.css',
        'css/apps.css',
        'css/ps-land.css',
        'css/affiliates-lp.css',
        'css/soc-icons-embedded.css',
        'css/fotorama.css',
        'css/c-delivery-schedule.css',
        'css/ps-profile.css',
        'css/mat-catalog.css',
        'css/star-rating.min.css',
        'css/animate.css',
        'css/chat.css',
        'css/invoice.css'
    ];

    public $js = [
        'js/underscore-min.js',
        'js/backbone-min.js',
        // own TS library js files
        'js/matchMedia.js',
        'js/device.min.js',
        'js/ts/site.js',
        'js/ts/commonJs.js',
        'js/ts/directives.js',
        'js/ts/visitor.js',
        'js/ts/loginForm.js',
        'js/ts/user.js',
        'js/ts/notify.js',
        'js/fotorama.js',
        'js/star-rating.min.js',
        'js/ts/widgets/search.js',
        'js/ts/widgets/explore-button.js',
        'js/social-likes.min.js'
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
        JsURIAsset::class,
        JquerySerializeFormAsset::class,
        AngularAsset::class,
        TranslateFunctionTAsset::class,
        Model3dRenderer2Asset::class,
        SiteParamsAsset::class,
        ArrayFindAsset::class,
        CommonAsset::class,
    ];
}
