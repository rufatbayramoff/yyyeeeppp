<?php
/**
 * Created by mitaichik
 */

namespace frontend\assets;


use yii\web\AssetBundle;

/**
 * Class GoogleMapAsset
 * @package frontend\assets
 */
class GoogleMapAsset extends AssetBundle
{

    public $lang = 'en';

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->js[] = 'https://maps.googleapis.com/maps/api/js?key='.param('google_maps_api_key').'&v=3.exp&libraries=places&language='.$this->lang;
    }
}
