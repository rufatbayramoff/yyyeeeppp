<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.09.16
 * Time: 15:46
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class Model3dFormAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';


    public $js = [
        'js/ts/model3dForm.js',
    ];

    public $depends = [AppAsset::class];
}