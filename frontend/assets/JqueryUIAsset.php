<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.08.18
 * Time: 17:22
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class JqueryUIAsset extends AssetBundle
{
    public $sourcePath = '@vendor/danheberden/jquery-serializeForm/dist';

    public $js = [
        'jquery-serializeForm.min.js'
    ];

    public $depends = [
        JqueryAsset::class
    ];
}