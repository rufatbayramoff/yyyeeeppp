<?php
/**
 * Created by mitaichik
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class PhotoSwipeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';



    public $js = [
        'js/photoswipe.min.js',
        'js/photoswipe-ui-default.min.js',
        'js/photo-swipe-dom.js'
    ];

    public $css = [
        'css/photoswipe.css',
        'css/photoswipe-default-skin.css',
    ];

    public $depends = [
        AppAsset::class,
    ];
}