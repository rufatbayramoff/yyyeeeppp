<?php
/**
 * Created by mitaichik
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class UploaderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/ts/upload/uploader.js',
        'js/ts/upload/edit.js',
        'js/html.sortable.min.js',
    ];

    public $depends = [
        AppAsset::class,
        DropzoneAsset::class,
    ];
}