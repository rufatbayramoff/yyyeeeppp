<?php

use common\models\model3d\CoverInfo;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\StoreUnitUtils;
use frontend\components\UserUtils;
use frontend\models\model3d\Model3dFacade;

/** @var \frontend\models\site\HireDesignerForm $model */
/** @var \yii\web\View $this */
/** @var yii\bootstrap\ActiveForm $form */
/** @var \common\components\hiredesigner\HireDesigner $hireDesigner */

$this->title = _t('front.site', 'Hire a Designer');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h1 class="text-center m-t30 m-b20"><?= _t('site.custom3d', 'Hire a Designer'); ?></h1>

    <h3 class="designers-cat-2heading">
        <?= _t('site.custom3d', 'If you want to create something unique or need a prototype for your invention, hire a designer to exclusively create your 3D model idea.'); ?>
    </h3>
</div>

<div class="container">
    <div class="responsive-container-list responsive-container-list--3">

        <?php foreach ($hireDesigner->items as $item): ?>

            <div class="responsive-container">
                <div class="designer-card">
                    <div class="designer-card__userinfo">
                        <a class="designer-card__avatar" href="<?= UserUtils::getUserPublicProfileUrl($item->designer)?>">
                            <img src="<?= UserUtils::getAvatarUrl($item->designer, 128); ?>" alt="" align="left">
                        </a>
                        <div class="designer-card__username">
                            <?= H($item->designer->userProfile->full_name ?: $item->designer->username)?>
                        </div>
                    </div>
                    <div class="designer-card__models-list">
                        <?php foreach ($item->models as $model3d):?>
                            <a class="designer-card__model" href="<?= Model3dFacade::getStoreUrl($model3d)?>" title="<?= H($model3d->title)?>">
                                <img src="<?= ImageHtmlHelper::getThumbUrl((new CoverInfo(Model3dFacade::getCover($model3d)))->image, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);?>" alt="<?= H($model3d->title)?>">
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <div class="designer-card__about">
                        <?= StoreUnitUtils::displayMore($item->designer->userProfile->info_about, 30)?>
                    </div>
                    <div class="designer-card__btn-block">
                        <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="<?= UserUtils::getUserStoreUrl($item->designer)?>"><?= _t('site', 'Hire Designer')?></a>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>

    </div>
</div>