<div class="container default-page">
    <div class="row">
        <div class="col-xs-12 post-content__body">
            <h1><?php echo $pageTitle; ?></h1>

            <?php echo $pageContent; ?>
        </div>
    </div>
</div>
