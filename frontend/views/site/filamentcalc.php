<style>
.fil_radio, .fil_div {
    width: 100%;
    clear: both;
    float: left;
    margin-bottom: 20px;
}
.fil_label {
    width: 33%;
    float: left;
    font-weight: bold;
}
.fil_field {
    width: 66%;
    float: left;
}
</style>
<div class="fil_cal">
    <div id="error_div"></div>
    <div class="fil_radio" id="fil_type">
        <div class="fil_label"><?= _t('front.site', 'Type Of Filament')?>:</div>
        <div class="fil_field">
            <p><input type="radio" name="filament" value="ABS"> ABS - 1.03 <?= _t('front.site', 'g/cm^3')?></p>
            <p><input type="radio" name="filament" value="PLA"> PLA - 1.24 <?= _t('front.site', 'g/cm^3')?></p>
            <p id="fil_type_p"><input type="radio" name="filament" value="OTR"> <?= _t('front.site', 'Other')?> &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="density" id="density" placeholder="<?= _t('front.site', 'Enter Density')?>"><?= _t('front.site', 'g/cm^3')?>
            </p>
        </div>
    </div> 
    <div class="fil_radio" id="fil_size">
        <div class="fil_label"><?= _t('front.site', 'Size Of Filament')?>:</div>
        <div class="fil_field">
            <p><input type="radio" name="size" value="1.75"> 1.75mm</p>
            <p><input type="radio" name="size" value="2.90"> 2.90mm</p>
            <p id="fil_size_p"><input type="radio" name="size" value="OTR"> <?= _t('front.site', 'Other')?> &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="Msize" id="Msize" placeholder="<?= _t('front.site', 'Enter Size')?>"><?= _t('front.site', 'mm')?></p>
        </div>
    </div>
    <div class="fil_div" id="fil_cost">
        <div class="fil_label"><?= _t('front.site', 'Filament Cost')?>:</div>
        <div class="fil_field">
            $&nbsp;&nbsp;<input type="text" name="cost" id="cost" value="34.99" placeholder="<?= _t('front.site', 'Enter Cost')?>">
            Per <input type="text" name="Psize" id="Psize" value="2" placeholder="<?= _t('front.site', 'Enter Per Size')?>">
            <select name="unit" id="unit" class="fil_select">
                <option value="kg"><?= _t('front.site', 'Kg')?></option>
                <option selected="selected" value="lb"><?= _t('front.site', 'Lb')?></option>
            </select>
        </div>
    </div>
    <div class="fil_div" id="fil_length">
        <div class="fil_label"><?= _t('front.site', 'Length Of Filament')?>:</div>
        <div class="fil_field">
            <input type="text" name="length" id="length" placeholder="<?= _t('front.site', 'Filament Length')?>">&nbsp;&nbsp;<?= _t('front.site', 'mm')?>
        </div>
    </div>
    <div class="fil_div" id="fil_time">
        <div class="fil_label"><?= _t('front.site', 'Job Time')?>:</div>
        <div class="fil_field">
            <input type="text" name="time" id="time" placeholder="<?= _t('front.site', 'Job Time')?>">&nbsp;&nbsp;<?= _t('front.site', 'min')?>
        </div>
    </div>
    <div class="fil_div" id="fil_Chour">
        <div class="fil_label"><?= _t('front.site', 'Cost Per Hour')?>:</div>
        <div class="fil_field">
            <input type="text" name="Chour" id="Chour" placeholder="<?= _t('front.site', 'Cost Per Hour')?>">&nbsp;&nbsp;$
        </div>
    </div>
    <div class="fil_div">
        <div class="fil_label"><?= _t('front.site', 'Mark-Up (Optional)')?>:</div>
        <div class="fil_field">
            <input type="text" name="markup" id="markup" placeholder="<?= _t('front.site', 'Mark-up Percentage')?>">&nbsp;&nbsp;%
        </div>
    </div>
    <div class="fil_btndiv"><a onclick="return check_calculation();" class="fil_btn"><?= _t('front.site', 'CALCULATE')?></a>
        <span id="fil_load" style="display: none;"><?= _t('front.site', 'Loading')?>...</span></div>
</div>