<?php

use yii\helpers\Html;

$this->title = _t('site.common', 'Treatstock is temporarily down for scheduled maintenance.');
$name = 'Scheduled Maintenance';
$ee = app('errorHandler')->exception;
$message =
    _t('site.page', 'We will be back up and running within a few minutes. 
    Thanks for your patience.');
?>


    <div class="error-page__down">
        <div class="error-page__logo"></div>
        <h1 class="error-page__down-title"><?= $name ?></h1>

        <h3 class="error-page__down-subtitle"><?= nl2br(\H($message)) ?></h3>
    </div>


<?php $this->registerJs(' $("body").addClass("error-page--404"); setTimeout(function(){ location.reload(); }, 15000);'); ?>