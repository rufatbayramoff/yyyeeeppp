<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\HomePageCategoryBlock[] $productCategoriesBlock
 * @var \common\models\HomePageCategoryCard[] $productCategoryCard
 * @var \common\models\HomePageFeatured[] $featuredCategories
 * @var \common\models\BlogPost[] $blogPosts
 */

use frontend\components\UserSessionFacade;
use yii\helpers\Url;
use yii\web\View;

$this->registerAssetBundle(\frontend\assets\IndexPageAsset::class);
$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

$this->params['script']['type'] = 'application/ld+json';
$this->params['script']['content'] = json_decode(file_get_contents(Yii::getAlias('@frontend').'/web/js/reviews-aggregator.js'));

$additionalParams = [
    'psId' => $this->params['psId'] ?? ''
];

?>

<?= $this->render('/common/ie-alert.php'); ?>

<?php /*
<div class="hero-banner hero-banner--white-text">
    <div class="container container--wide">

        <div class="row">
            <div class="col-sm-12 text-center">
                <h1 class="hero-banner__title">
                    <?= _t('front', 'Treatstock powers online manufacturing'); ?>
                </h1>
                <p class="hero-banner__lead lead m-b30 p-b20">
                    <?= _t('front', 'Online tools for'); ?>
                    <a href="<?=Url::toRoute(['/catalogps/ps-catalog/index', 'type'     => '3d-printing-services',
                        'location' => UserSessionFacade::getLocationAsString(UserSessionFacade::getLocation())]); ?>" target="_blank">
                        <?= _t('front', '3D printing'); ?></a>,
                    <a href="<?php echo Url::to(['/catalogps/catalog/category', 'category' => 'cnc-manufacturing'])?>" target="_blank">
                        <?= _t('front', 'CNC Machining'); ?>
                    </a>
                    <?= _t('front', 'and'); ?>
                    <a href="<?php echo Url::to(['/catalogps/catalog/category', 'category' => 'cutting'])?>" target="_blank">
                        <?= _t('front', 'Cutting'); ?>
                    </a>
                </p>

                <a href="/order-upload" class="btn btn-primary m-t30 m-b10"><?= _t('front', 'Get instant quote'); ?></a>

                <p class="hero-banner__hint m-b30">
                    <span class="tsi tsi-lock m-r5"></span>
                    <?= _t('front.upload', 'All files are protected by Treatstock security'); ?>
                </p>

            </div>
        </div>
    </div>

    <div class="hero-banner__fade-bg"></div>
    <div class="hero-banner__bg" style="background-image: url('https://static.treatstock.com/static/images/common/business-page/head-bg-min.jpg');"></div>

</div>
 */?>

<div class="hero-banner hero-banner--2col">
    <div class="container container--wide">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="hero-banner__title">
                    <?= _t('front', 'Treatstock powers online manufacturing'); ?>
                </h1>
                <p class="hero-banner__lead lead m-b30">
                    <?= _t('front', 'Online tools for'); ?>
                    <a href="<?=Url::toRoute(['/catalogps/ps-catalog/index', 'type'     => '3d-printing-services',
                        'location' => UserSessionFacade::getLocationAsString(UserSessionFacade::getLocation())]); ?>" target="_blank">
                        <?= _t('front', '3D printing'); ?></a>,
                    <a href="<?php echo Url::to(['/catalogps/catalog/category', 'category' => 'cnc-manufacturing'])?>" target="_blank">
                        <?= _t('front', 'CNC Machining'); ?>
                    </a>
                    <?= _t('front', 'and'); ?>
                    <a href="<?php echo Url::to(['/catalogps/catalog/category', 'category' => 'cutting'])?>" target="_blank">
                        <?= _t('front', 'Cutting'); ?>
                    </a>
                </p>
                <a href="/my/print-model3d?utm_source=main-page" class="btn btn-primary m-r20 m-b20"><?= _t('front', 'Order 3D print'); ?></a>
                <a href="/l/start-3dprintservice" class="btn btn-primary btn-ghost m-b20"><?= _t('front', 'Become a manufacturer'); ?></a>

            </div>
            <div class="col-sm-6">
                <div class="hero-banner__video">

                    <video class="hero-banner__video-source" autoplay loop muted playsinline autoplay="autoplay" poster="https://static.treatstock.com/static/images/main-page-temp/mp-workfkow-cover.png">
                        <source src="https://static.treatstock.com/static/images/main-page-temp/mp-workfkow.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                        <?= _t('front', 'Your browser doesn\'t support embedded videos.'); ?>
                        <a href="https://static.treatstock.com/static/images/main-page-temp/mp-workfkow.mp4"><?= _t('front', 'Download video'); ?></a>.
                    </video>

                    <img class="hero-banner__video-frame" src="https://static.treatstock.com/static/images/main-page-temp/mp-mcbook-holder.png">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container trust-logo-container">
    <h3 class="trust-logo-title">
        <?= _t('front', 'Trusted by'); ?>
    </h3>

    <div class="trust-logo__slider swiper-container">
        <div class="swiper-wrapper">
            <div class="trust-logo__slider-item swiper-slide">
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/apple.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/tesla.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/stanford.png"/>
            </div>
            <div class="trust-logo__slider-item swiper-slide">
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/google.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/ames.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/samsung.png"/>
            </div>
            <div class="trust-logo__slider-item swiper-slide">
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/adobe.png"/
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/aerospace.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/whirlpool.png"/>
            </div>
            <div class="trust-logo__slider-item swiper-slide">
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/huawei.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/verizon.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/bayer.png"/>
            </div>
            <div class="trust-logo__slider-item swiper-slide">
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/atos.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/3dsys.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/bmw.png"/>
            </div>
            <div class="trust-logo__slider-item swiper-slide">
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/cmu.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/columbia.png"/>
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/deloitte.png"/>
            </div>
            <div class="trust-logo__slider-item swiper-slide">
                <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/sjsu.png"/>
            </div>
        </div>

        <div class="trust-logo__pagination"></div>
    </div>
</div>

<div class="site-index">

    <?php /*
    <div class="promo-page-services container container--wide p-t30 p-b30">
        <h2 class="text-center animated m-t30" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
            <?= _t('front', 'Our services'); ?>
        </h2>
        <div class="row p-b30">
            <div class="col-sm-4">
                <a class="promo-page-services__card p-t30 p-b30" href="<?php echo Url::toRoute(['/catalogps/ps-catalog/index', 'type'     => '3d-printing-services',
                    'location' => UserSessionFacade::getLocationAsString(UserSessionFacade::getLocation())])?>" target="_blank">
                    <img class="promo-page-services__icon" src="https://static.treatstock.com/static/images/common/service-cat/3dprinting.svg">
                    <div class="promo-page-services__title text-center">
                        <?= _t('front', '3D Printing'); ?>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a class="promo-page-services__card p-t30 p-b30" href="<?php echo Url::to(['/catalogps/catalog/category', 'category' => 'cnc-manufacturing'])?>" target="_blank">
                    <img class="promo-page-services__icon" src="https://static.treatstock.com/static/images/common/service-cat/cnc-machining.svg">
                    <div class="promo-page-services__title text-center">
                        <?= _t('front', 'CNC Machining'); ?>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a class="promo-page-services__card p-t30 p-b30" href="<?php echo Url::to(['/catalogps/catalog/category', 'category' => 'cutting'])?>" target="_blank">
                    <img class="promo-page-services__icon" src="https://static.treatstock.com/static/images/common/service-cat/cutting-laser.svg">
                    <div class="promo-page-services__title text-center">
                        <?= _t('front', 'Laser Cutting'); ?>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a class="promo-page-services__card p-t30 p-b30" href="<?php echo Url::to(['/catalogps/catalog/category', 'category' => 'cnc'])?>" target="_blank">
                    <img class="promo-page-services__icon" src="https://static.treatstock.com/static/images/common/service-cat/cnc-routing.svg">
                    <div class="promo-page-services__title text-center">
                        <?= _t('front', 'CNC Routing'); ?>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a class="promo-page-services__card p-t30 p-b30" href="<?php echo Url::to(['/catalogps/catalog/category', 'category' => 'injection-molding'])?>" target="_blank">
                    <img class="promo-page-services__icon" src="https://static.treatstock.com/static/images/common/service-cat/injection-molding.svg">
                    <div class="promo-page-services__title text-center">
                        <?= _t('front', 'Injection Molding'); ?>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a class="promo-page-services__card p-t30 p-b30" href="<?php echo Url::to(['/catalogps/catalog/category', 'category' => 'metal-casting'])?>" target="_blank">
                    <img class="promo-page-services__icon" src="https://static.treatstock.com/static/images/common/service-cat/printing-metal.svg">
                    <div class="promo-page-services__title text-center">
                        <?= _t('front', 'Metal Printing'); ?>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="customer-reviews">
        <div class="container container--wide">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="promo-page-section__title m-b30 animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp"><?= _t('front', 'Average rating of our services 4.9/5'); ?></h2>
                    <h2 class="customer-reviews__title animated p-t30" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'What our customers are saying'); ?>
                    </h2>
                </div>
            </div>
        </div>

        <div class="container container--wide main-page-reviews-list">
            <div class="row fb-grid">
                <div class="col-xs-12 col-sm-6 col-md-3 m-b30">
                    <div class="main-page-reviews animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <img class="main-page-reviews__avatar"
                             src="https://secure.gravatar.com/avatar/e83e2496ad31501002461f799301c8bb?r=g&s=64&d=retro">
                        <div class="main-page-reviews__user-data">
                            <span class="main-page-reviews__username">mazzemotto</span>
                        </div>
                        <div class="main-page-reviews__rating">
                            <div id="star-rating-block-13955" class="star-rating-block">
                                <div class="star-rating rating-xs rating-disabled">
                                    <div class="rating-container tsi" data-content="">
                                        <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                        <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                               step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                               data-rating-class="tsi" data-readonly="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-reviews__text">
                            The print was an excellent job. A fast print, but more importantly are really good quality. I am a happy customer and would buy again.
                        </div>
                        <div class="main-page-reviews__date">
                            <span>Jan 11, 2021</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 m-b30">
                    <div class="main-page-reviews animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <img class="main-page-reviews__avatar"
                             src="https://secure.gravatar.com/avatar/75d9273ff688b23a26baeb1d242bf61b?r=g&s=64&d=retro">
                        <div class="main-page-reviews__user-data">
                            <span class="main-page-reviews__username">helgish</span>
                        </div>
                        <div class="main-page-reviews__rating">
                            <div id="star-rating-block-13955" class="star-rating-block">
                                <div class="star-rating rating-xs rating-disabled">
                                    <div class="rating-container tsi" data-content="">
                                        <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                        <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                               step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                               data-rating-class="tsi" data-readonly="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-reviews__text">
                            Like always, the quality is unreal. Great communication, open and forthright, definitely one of the best!
                        </div>
                        <div class="main-page-reviews__date">
                            <span>Jan 6, 2021</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 m-b30">
                    <div class="main-page-reviews animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <img class="main-page-reviews__avatar"
                             src="https://static.treatstock.com/static/user/google_114025802608639125611/avatar_64x64.jpg">
                        <div class="main-page-reviews__user-data">
                            <span class="main-page-reviews__username">m</span>
                        </div>
                        <div class="main-page-reviews__rating">
                            <div id="star-rating-block-13955" class="star-rating-block">
                                <div class="star-rating rating-xs rating-disabled">
                                    <div class="rating-container tsi" data-content="">
                                        <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                        <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                               step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                               data-rating-class="tsi" data-readonly="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-reviews__text">
                            Very happy with the delivered parts, exactly as ordered.
                        </div>
                        <div class="main-page-reviews__date">
                            <span>Jan 6, 2021</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 m-b30">
                    <div class="main-page-reviews animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                        <img class="main-page-reviews__avatar"
                             src="https://static.treatstock.com/static/user/google_104722062749600656013/avatar_64x64.jpg">
                        <div class="main-page-reviews__user-data">
                            <span class="main-page-reviews__username">blacksadify</span>
                        </div>
                        <div class="main-page-reviews__rating">
                            <div id="star-rating-block-13955" class="star-rating-block">
                                <div class="star-rating rating-xs rating-disabled">
                                    <div class="rating-container tsi" data-content="">
                                        <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                        <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                               step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                               data-rating-class="tsi" data-readonly="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-reviews__text">
                            Really good quality and quick shipping. Would highly recommend!
                        </div>
                        <div class="main-page-reviews__date">
                            <span>Dec 29, 2020</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center">
            <a href="/reviews" class="btn btn-primary m-t30 m-b30 animated" data-animation-in="fadeInUp" data-animation-out="fadeOutDown">
                <?= _t('front', 'See all reviews'); ?>
            </a>
        </div>
    </div>
    */?>


    <section class="promo-page-section promo-page-section--grey" id="manufacturing">
        <div class="container container--wide">
            <div class="row promo-page-section__row">
                <div class="col-xs-12 col-sm-6 promo-page-section__visual">
                    <img src="https://static.treatstock.com/static/images/main-page-temp/main-page-product.png"
                         style="display: block;max-width: 100%;margin: 0 auto;border-radius: 5px;">
                </div>

                <div class="col-xs-12 col-sm-6 promo-page-section__data">
                    <h2 class="promo-page-section__title"><?= _t('front', 'Get quality parts fast and safe'); ?></h2>
                    <ul class="m-t20 m-b30 p-l20">
                        <li class=""><?= _t('front', 'Upload your CAD file.'); ?></li>
                        <li class=""><?= _t('front', 'Choose technology, material, and service.'); ?></li>
                        <li class=""><?= _t('front', 'Get an instant quote and place an order.'); ?></li>
                    </ul>


                    <div class="row">
                        <div class="col-md-6 m-b30 main-page-become__list">
                            <span class="main-page-become__icon">
                                <img src="https://static.treatstock.com/static/images/common/big-icons/save-time.svg">
                            </span>
                            <span class="main-page-become__label"><?= _t('front', 'Rapid Prototyping'); ?></span>
                        </div>
                        <div class="col-md-6 m-b30 main-page-become__list">
                            <span class="main-page-become__icon">
                                <img src="https://static.treatstock.com/static/images/common/big-icons/compare.svg">
                            </span>
                            <span class="main-page-become__label"><?= _t('front', 'Compare Suppliers'); ?></span>
                        </div>
                    </div>
                    <div class="row m-b10">
                        <div class="col-md-6 m-b30 main-page-become__list">
                            <span class="main-page-become__icon">
                                <img src="https://static.treatstock.com/static/images/common/big-icons/trade-assurance.svg">
                            </span>
                            <span class="main-page-become__label"><?= _t('front', 'Buyer Protection'); ?></span>
                        </div>
                        <div class="col-md-6 m-b30 main-page-become__list">
                            <span class="main-page-become__icon">
                                <img src="https://static.treatstock.com/static/images/common/big-icons/instant-quotes.svg">
                            </span>
                            <span class="main-page-become__label"><?= _t('front', 'Instant Requests'); ?></span>
                        </div>
                    </div>

                    <a href="/order-upload?utm_source=main-page" class="btn btn-primary m-b20"><?= _t('front', 'Place an instant order'); ?></a>

                </div>

            </div>
        </div>

    </section>


    <!-- Become a Manufacturer -->
    <div class="main-page-become">
        <div class="main-page-become__pic"></div>

        <div class="container container--wide">
            <div class="col-sm-6 col-sm-offset-6 main-page-become__data">
                <div class="animated-none" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <h2 class="main-page-become__heading">
                        <a class="t-main-page-become__heading" href="/mybusiness/company" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company', 'signUp');return false;" : "return true;" ?>">
                            <?= _t('front', 'Become a manufacturer on Treatstock'); ?>
                        </a>
                    </h2>
                    <p class="main-page-become__text">
                        <?= _t('front', 'Use our online manufacturing tools for easy price calculation, order management and safe deals. Generate leads with business customers from all over the world. '); ?>
                    </p>
                    <div class="row">
                        <div class="col-md-6 m-b20 main-page-become__list">
                            <span class="main-page-become__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><defs><style>.cls-1-iq,.cls-2-iq{fill:none;stroke-linecap:round;stroke-linejoin:round;}.cls-1-iq{stroke:currentColor;}.cls-2-iq{stroke:#2d8ee0;}</style></defs><title>instant-quotes</title><g id="instant-quotes"><g id="v1"><path class="cls-1-iq" d="M38.11,7.5H30a1.34,1.34,0,0,0-1,.4L12.43,24.16a1.39,1.39,0,0,0,0,1.95l8.14,8.43a1.39,1.39,0,0,0,2,0L39.09,18a1.4,1.4,0,0,0,.41-1V8.89A1.39,1.39,0,0,0,38.11,7.5Z"></path><circle class="cls-1-iq" cx="35.5" cy="11.5" r="2"></circle><line class="cls-1-iq" x1="0.5" y1="25.5" x2="5.5" y2="25.5"></line><line class="cls-1-iq" x1="5.5" y1="19.5" x2="10.5" y2="19.5"></line><line class="cls-1-iq" x1="11.5" y1="13.5" x2="16.5" y2="13.5"></line><line class="cls-1-iq" x1="5.5" y1="31.5" x2="10.5" y2="31.5"></line><line class="cls-1-iq" x1="17.5" y1="7.5" x2="22.5" y2="7.5"></line><g id="S"><path class="cls-2-iq" d="M27.5,18.5h-3a1.5,1.5,0,0,0,0,3c1.75,0,.43,0,2.18,0s1.82,3-.18,3h-3"></path><line class="cls-2-iq" x1="25.52" y1="17.05" x2="25.5" y2="18.5"></line><line class="cls-2-iq" x1="25.5" y1="24.5" x2="25.52" y2="26"></line></g></g></g></svg>
                            </span>
                            <span class="main-page-become__label"><?= _t('front', 'Instant Requests'); ?></span>
                        </div>
                        <div class="col-md-6 m-b20 main-page-become__list">
                            <span class="main-page-become__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><defs><style>.cls-1-promo,.cls-2-promo,.cls-3-promo,.cls-4-promo{fill:none;}.cls-1-promo,.cls-2-promo,.cls-3-promo{stroke:#ffffff;}.cls-1-promo,.cls-2-promo{stroke-linecap:square;}.cls-1-promo{stroke-linejoin:bevel;}.cls-2-promo,.cls-3-promo{stroke-miterlimit:10;}.cls-3-promo,.cls-4-promo{stroke-linecap:round;}.cls-4-promo{stroke:#2d8ee0;stroke-linejoin:round;}</style></defs><title>promotion-tools</title><g id="promotion-tools"><rect class="cls-1-promo" x="6.45" y="20.8" width="12" height="11" transform="translate(-14.95 16.51) rotate(-45)"></rect><path class="cls-2-promo" d="M33.31,17.46S25.25,21.28,20.59,26l-7.78-7.78c4.66-4.67,8.48-12.73,8.48-12.73Z"></path><polygon class="cls-2-promo" points="26.24 35.85 22.71 39.38 13.87 32.67 18.46 28.07 26.24 35.85"></polygon><path class="cls-2-promo" d="M34.73,18.88,19.88,4a2.15,2.15,0,0,1,2.83,0l12,12A2.15,2.15,0,0,1,34.73,18.88Z"></path><path class="cls-1-promo" d="M10,36.56l2.12-2.12L4.32,26.66,2.2,28.78A3,3,0,0,0,2.2,33l3.54,3.54A3,3,0,0,0,10,36.56Z"></path><line class="cls-3-promo" x1="9.98" y1="26.66" x2="12.81" y2="23.83"></line><line class="cls-3-promo" x1="12.1" y1="28.78" x2="14.93" y2="25.95"></line><line class="cls-3-promo" x1="32.61" y1="6.86" x2="36.14" y2="3.32"></line><line class="cls-3-promo" x1="28.83" y1="4.39" x2="31.08" y2="0.49"></line><line class="cls-3-promo" x1="38.97" y1="8.38" x2="35.08" y2="10.63"></line><g id="S"><path class="cls-4-promo" d="M5.5,13.5h-3a1.5,1.5,0,0,0,0,3c1.75,0,.43,0,2.18,0s1.82,3-.18,3h-3"></path><line class="cls-4-promo" x1="3.52" y1="12.05" x2="3.5" y2="13.5"></line><line class="cls-4-promo" x1="3.5" y1="19.5" x2="3.52" y2="21"></line></g><g id="S-2" data-name="S"><path class="cls-4-promo" d="M12.5,3.5h-3a1.5,1.5,0,0,0,0,3c1.75,0,.43,0,2.18,0s1.82,3-.18,3h-3"></path><line class="cls-4-promo" x1="10.52" y1="2.05" x2="10.5" y2="3.5"></line><line class="cls-4-promo" x1="10.5" y1="9.5" x2="10.52" y2="11"></line></g><g id="S-3" data-name="S"><path class="cls-4-promo" d="M35.5,27.5h-3a1.5,1.5,0,0,0,0,3c1.75,0,.43,0,2.18,0s1.82,3-.18,3h-3"></path><line class="cls-4-promo" x1="33.52" y1="26.05" x2="33.5" y2="27.5"></line><line class="cls-4-promo" x1="33.5" y1="33.5" x2="33.52" y2="35"></line></g></g></svg>
                            </span>
                            <span class="main-page-become__label"><?= _t('front', 'Promotional tools'); ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 m-b20 main-page-become__list">
                            <span class="main-page-become__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path fill="#ffffff" d="M10 36.19a3.75 3.75 0 0 1-1.07-.3 1.1 1.1 0 0 1-.51-.82 5.63 5.63 0 0 0 1.51-3.85c0-2.64-1.23-4.16-3.39-4.16s-3.39 1.59-3.39 4.16a5.9 5.9 0 0 0 1.51 3.84 1.08 1.08 0 0 1-.51.83 3.75 3.75 0 0 1-1.07.3A3.74 3.74 0 0 0 1 37a3.75 3.75 0 0 0-.9 2.4.49.49 0 0 0 .49.5.51.51 0 0 0 .5-.49 2.77 2.77 0 0 1 .61-1.71 3.07 3.07 0 0 1 1.55-.56 4.25 4.25 0 0 0 1.4-.43 2 2 0 0 0 .85-1 2.27 2.27 0 0 0 1 .26 2.37 2.37 0 0 0 1-.28 2 2 0 0 0 .86 1 4.18 4.18 0 0 0 1.4.43 3.07 3.07 0 0 1 1.54.56 2.81 2.81 0 0 1 .62 1.71.49.49 0 0 0 .5.49.5.5 0 0 0 .5-.5A3.73 3.73 0 0 0 12 37a3.71 3.71 0 0 0-2-.81zM6.5 35c-1 0-2.39-1.83-2.39-3.76 0-.95.23-3.16 2.39-3.16 2 0 2.39 1.72 2.39 3.16 0 2-1.31 3.76-2.39 3.76zM37 36.19a3.75 3.75 0 0 1-1.07-.3 1.1 1.1 0 0 1-.51-.82 5.63 5.63 0 0 0 1.51-3.85c0-2.64-1.23-4.16-3.39-4.16s-3.39 1.59-3.39 4.16a5.9 5.9 0 0 0 1.51 3.84 1.08 1.08 0 0 1-.51.83 3.75 3.75 0 0 1-1.07.3A3.74 3.74 0 0 0 28 37a3.75 3.75 0 0 0-.9 2.4.49.49 0 0 0 .49.5.51.51 0 0 0 .5-.49 2.77 2.77 0 0 1 .61-1.71 3.07 3.07 0 0 1 1.55-.56 4.25 4.25 0 0 0 1.4-.43 2 2 0 0 0 .85-1 2.27 2.27 0 0 0 1 .26 2.37 2.37 0 0 0 1-.28 2 2 0 0 0 .86 1 4.18 4.18 0 0 0 1.4.43 3.07 3.07 0 0 1 1.54.56 2.81 2.81 0 0 1 .62 1.71.49.49 0 0 0 .5.49.5.5 0 0 0 .5-.5A3.73 3.73 0 0 0 39 37a3.71 3.71 0 0 0-2-.81zM33.5 35c-1 0-2.39-1.83-2.39-3.76 0-.95.23-3.16 2.39-3.16 2 0 2.39 1.72 2.39 3.16 0 2-1.31 3.76-2.39 3.76zM.56 12.93a.51.51 0 0 0 .5-.49 2.77 2.77 0 0 1 .61-1.71 3.07 3.07 0 0 1 1.55-.56 4.25 4.25 0 0 0 1.4-.43 2 2 0 0 0 .85-1A2.27 2.27 0 0 0 6.5 9a2.37 2.37 0 0 0 1-.28 2 2 0 0 0 .86 1 4.18 4.18 0 0 0 1.4.43 3.07 3.07 0 0 1 1.54.56 2.81 2.81 0 0 1 .62 1.71.49.49 0 0 0 .5.49.5.5 0 0 0 .5-.5A3.73 3.73 0 0 0 12 10a3.71 3.71 0 0 0-2-.81 3.75 3.75 0 0 1-1.07-.3 1.1 1.1 0 0 1-.51-.82 5.63 5.63 0 0 0 1.47-3.85C9.89 1.58 8.66.06 6.5.06S3.11 1.65 3.11 4.22a5.9 5.9 0 0 0 1.51 3.84 1.08 1.08 0 0 1-.51.83 3.75 3.75 0 0 1-1.11.3A3.74 3.74 0 0 0 1 10a3.75 3.75 0 0 0-.9 2.4.49.49 0 0 0 .46.53zm3.55-8.71c0-.95.23-3.16 2.39-3.16 2 0 2.39 1.72 2.39 3.16C8.89 6.24 7.58 8 6.5 8S4.11 6.15 4.11 4.22zm27 4.67a3.75 3.75 0 0 1-1.07.3A3.74 3.74 0 0 0 28 10a3.75 3.75 0 0 0-.9 2.4.49.49 0 0 0 .49.5.51.51 0 0 0 .5-.49 2.77 2.77 0 0 1 .61-1.71 3.07 3.07 0 0 1 1.55-.56 4.25 4.25 0 0 0 1.4-.43 2 2 0 0 0 .85-1 2.27 2.27 0 0 0 1 .26 2.37 2.37 0 0 0 1-.28 2 2 0 0 0 .86 1 4.18 4.18 0 0 0 1.4.43 3.07 3.07 0 0 1 1.54.56 2.81 2.81 0 0 1 .62 1.71.49.49 0 0 0 .5.49.5.5 0 0 0 .5-.5A3.73 3.73 0 0 0 39 10a3.71 3.71 0 0 0-2-.81 3.75 3.75 0 0 1-1.07-.3 1.1 1.1 0 0 1-.51-.82 5.63 5.63 0 0 0 1.51-3.85c0-2.64-1.23-4.16-3.39-4.16s-3.39 1.59-3.39 4.16a5.9 5.9 0 0 0 1.51 3.84 1.08 1.08 0 0 1-.55.83zm0-4.67c0-.95.23-3.16 2.39-3.16 2 0 2.39 1.72 2.39 3.16 0 2-1.31 3.76-2.39 3.76s-2.39-1.83-2.39-3.76zM3.42 23.79a.5.5 0 0 1 .34-.62.49.49 0 0 1 .62.34 16.76 16.76 0 0 0 .85 2.27.51.51 0 0 1-.46.71.49.49 0 0 1-.45-.29 17.47 17.47 0 0 1-.9-2.41zm.4-6.32c0 .5-.07 1-.07 1.53s0 1 .07 1.51a.51.51 0 0 1-.45.55h-.05a.52.52 0 0 1-.5-.46c0-.53-.07-1.07-.07-1.6s0-1.09.08-1.63a.5.5 0 1 1 1 .1zm1.42-5.27a15.52 15.52 0 0 0-.85 2.27.49.49 0 0 1-.48.36.34.34 0 0 1-.14 0 .49.49 0 0 1-.34-.62 17.47 17.47 0 0 1 .9-2.41.49.49 0 0 1 .67-.26.5.5 0 0 1 .24.66zm6.34-7.69a.49.49 0 0 1 .2-.68 16.5 16.5 0 0 1 2.35-1.05.5.5 0 1 1 .33.94 16.07 16.07 0 0 0-2.21 1 .46.46 0 0 1-.23.06.49.49 0 0 1-.44-.27zm6.48-1.65a.51.51 0 0 1-.5-.44.51.51 0 0 1 .44-.56 17.74 17.74 0 0 1 4 0 .51.51 0 0 1 .44.56.51.51 0 0 1-.56.44 15.68 15.68 0 0 0-3.75 0zm7.48.86a.51.51 0 0 1-.31-.64.52.52 0 0 1 .65-.3 16.9 16.9 0 0 1 2.34 1.05.5.5 0 0 1-.24.94.46.46 0 0 1-.23-.06 16.07 16.07 0 0 0-2.21-.99zM35 12.46a.49.49 0 0 1 .24-.66.5.5 0 0 1 .67.24 17.43 17.43 0 0 1 .89 2.41.5.5 0 0 1-.34.62.34.34 0 0 1-.14 0 .49.49 0 0 1-.48-.36 16.71 16.71 0 0 0-.84-2.25zm1.85 8.86a.51.51 0 0 1-.45-.55c.05-.51.07-1 .07-1.53s0-1-.07-1.51a.51.51 0 0 1 .46-.55.52.52 0 0 1 .54.45c.05.53.07 1.07.07 1.61s0 1.09-.07 1.63a.5.5 0 0 1-.5.45zM35 26a15.47 15.47 0 0 0 .84-2.27.5.5 0 0 1 1 .28 18.87 18.87 0 0 1-.9 2.41.52.52 0 0 1-.46.29.54.54 0 0 1-.21-.05A.49.49 0 0 1 35 26zm-6.54 8.37a17.57 17.57 0 0 1-2.35 1.05.51.51 0 0 1-.17 0 .48.48 0 0 1-.47-.33.5.5 0 0 1 .3-.64 15.55 15.55 0 0 0 2.21-1 .5.5 0 0 1 .48.88zm-5.79 1.41a.52.52 0 0 1-.44.56 19 19 0 0 1-2 .11 18.62 18.62 0 0 1-2-.11.5.5 0 0 1 .11-1 16.3 16.3 0 0 0 3.76 0 .5.5 0 0 1 .57.48zM15 35.16a.5.5 0 0 1-.47.33.47.47 0 0 1-.17 0A17.57 17.57 0 0 1 12 34.41a.5.5 0 1 1 .48-.88 16.2 16.2 0 0 0 2.21 1 .5.5 0 0 1 .31.63z"></path><path fill="#2d8ee0" d="M25.5 15h-11a2.5 2.5 0 0 0-2.5 2.5v9a.49.49 0 0 0 .36.48h.14a.52.52 0 0 0 .42-.22C13.43 26 15.08 24 16.5 24h9a2.5 2.5 0 0 0 2.5-2.5v-4a2.5 2.5 0 0 0-2.5-2.5zm1.5 6.5a1.5 1.5 0 0 1-1.5 1.5h-9c-1.36 0-2.67 1.14-3.5 2.07V17.5a1.5 1.5 0 0 1 1.5-1.5h11a1.5 1.5 0 0 1 1.5 1.5zm-2-3a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1 0-1h9a.5.5 0 0 1 .5.5zm-2 2a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1 0-1h7a.5.5 0 0 1 .5.5z"></path></svg>
                            </span>
                            <span class="main-page-become__label"><?= _t('front', 'Advertising Network'); ?></span>
                        </div>
                        <div class="col-md-6 m-b20 main-page-become__list">
                            <span class="main-page-become__icon">
                                <svg id="financial-tools" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><defs><style>.cls-1-fin{fill:#ffffff;}.cls-2-fin,.cls-3-fin,.cls-4-fin{fill:none;stroke-linecap:round;}.cls-2-fin,.cls-3-fin{stroke:#ffffff;}.cls-2-fin{stroke-miterlimit:10;}.cls-3-fin,.cls-4-fin{stroke-linejoin:round;}.cls-4-fin{stroke:#2d8ee0;}</style></defs><title>financial-tools</title><path class="cls-1-fin" d="M39,32v1a.91.91,0,0,1-1,1H2a.89.89,0,0,1-1-1V32H16.25a1.82,1.82,0,0,0,.32.44A2,2,0,0,0,18,33h4a2,2,0,0,0,1.43-.56,1.82,1.82,0,0,0,.32-.44H39m1-1H23a1,1,0,0,1-1,1H18a1,1,0,0,1-1-1H0v2a1.92,1.92,0,0,0,2,2H38a2,2,0,0,0,2-2V31ZM0,31Z"></path><path class="cls-2-fin" d="M1.5,31.5v-23a2,2,0,0,1,2.06-2H36.44a2,2,0,0,1,2.06,2v23"></path><line class="cls-2-fin" x1="22.5" y1="10.5" x2="34.5" y2="10.5"></line><line class="cls-2-fin" x1="22.5" y1="12.5" x2="34.5" y2="12.5"></line><line class="cls-2-fin" x1="22.5" y1="14.5" x2="34.5" y2="14.5"></line><line class="cls-2-fin" x1="22.5" y1="16.5" x2="34.5" y2="16.5"></line><polyline class="cls-3-fin" points="5.5 27.5 9.5 24.5 12.5 26.5 18 21"></polyline><g id="S"><path class="cls-4-fin" d="M13.5,10.5h-3a1.5,1.5,0,0,0,0,3c1.75,0,.43,0,2.18,0s1.82,3-.18,3h-3"></path><line class="cls-4-fin" x1="11.52" y1="9.05" x2="11.5" y2="10.5"></line><line class="cls-4-fin" x1="11.5" y1="16.5" x2="11.52" y2="18"></line></g><path class="cls-1-fin" d="M27,26v1H25V26h2m1-1H24v3h4V25Z"></path><path class="cls-1-fin" d="M30,24v3H28V24h2m1-1H27v5h4V23Z"></path><path class="cls-1-fin" d="M33,22v5H31V22h2m1-1H30v7h4V21Z"></path></svg>
                            </span>
                            <span class="main-page-become__label"><?= _t('front', 'Manufacturing Tools'); ?></span>
                        </div>
                    </div>

                    <p class="m-t20">
                        <a href="/l/start-3dprintservice" class="btn btn-primary ps-promo__btn t-ps-promo--start-ps-btn">
                            <?= _t('front', 'Start now'); ?>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- // Become a Manufacturer -->



</div>



<?php #echo ShoppingCandidateWidget::widget();
function minifyJS($buffer) {

    $buffer = preg_replace("/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))/", "", $buffer);
    $buffer = str_replace(["\r\n","\r","\t","\n",'  ','    ','     '], '', $buffer);
    $buffer = preg_replace(['(( )+\))','(\)( )+)'], ')', $buffer);

    return $buffer;
}
$content = file_get_contents(Yii::getAlias('@frontend/web/js/mainpage.js'));

?>
<?php $this->registerJs(minifyJS($content)); ?>

