<?php
?>
<div class="covid-warning">
    <div class="container">
        <a href="/l/covid19" target="_blank">
            <?= _t('front', 'Treatstock vs COVID-2019: get 3D printed respirators and masks'); ?>
        </a>
    </div>
</div>
<style>
    .covid-warning {
        position: relative;
        background: rgba(0, 0, 0, 0.65);
        color: #ffffff;
        overflow: hidden;
    }
    .covid-warning:after {
        content: '';
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: url(https://static.treatstock.com/static/images/covid/covid.jpg) no-repeat 50% 50% transparent;
        background-size: cover;
    }
    .covid-warning a {
        display: block;
        margin: 0;
        padding: 30px 0;
        text-align: center;
        font-size: 25px;
        line-height: 30px;
        text-decoration: none;
        color: #ffffff;
    }
    .covid-warning a:hover {
        text-decoration: underline;
    }
</style>
