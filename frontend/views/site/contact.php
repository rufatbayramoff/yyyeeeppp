<?php

use common\modules\captcha\widgets\GoogleRecaptcha2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\site\ContactForm */

$this->title = _t('front.site', 'Contact Us');
$this->params['breadcrumbs'][] = $this->title;
if(!is_guest()){
    $user = frontend\models\user\UserFacade::getCurrentUser();
    $model->name = $user->username;
    $model->email = $user->email;
}
$model->subject = app('request')->get('subject', null);

$this->registerLinkTag(['rel' => 'canonical', 'href' => \yii\helpers\Url::canonical()]);

?>
<div class="site-contact">
    <div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 m-t30">

            <div class="panel box-shadow">
                <div class="panel-body p-l30 p-r30">

                    <h1 class="text-center m-t0"><?= \H($this->title) ?></h1>

                    <table class="table">
                        <tr>
                            <th class="p-l0">Address</th>
                            <td>40 East Main Street Suite 900<br />
                                Newark DE 19711</td>
                        </tr>
                    </table>

                    <p><?=_t('site.page', 'Send us an email at')?>
                        <script>
                            var parts = ["support", "treatstock", "com", "&#46;", "&#64;"];
                            var mailAddress = parts[0] + parts[4] + parts[1] + parts[3] + parts[2];
                            document.write('<A href="mailto:' + mailAddress + '">' + mailAddress + '</a>');
                        </script>
                    </p>

                    <hr />

                    <p>
                        <?php
                        $helpLink = yii\helpers\Html::a("Help Center", ['/help']);
                        echo  _t('front.site', 'Before you contact us, you should have a look in our {help} to see if you can find an answer to your question there. If you have a business inquiry or any other questions, please fill out the following form to contact us.', ['help'=>$helpLink]);
                        ?>
                    </p>

                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                        <?= $form->field($model, 'name')->label(_t('site.page', 'Name')) ?>
                        <?= $form->field($model, 'email')->label(_t('site.page', 'Email')) ?>
                        <?= $form->field($model, 'subject')->label(_t('site.page', 'Subject')) ?>
                        <?= $form->field($model, 'orderId')->label(_t('site.page', 'Order or Invoice Id'))?>
                        <?= $form->field($model, 'body')->textArea(['rows' => 6])->label(_t('site.page', 'Message'))?>
                        <div class="form-group">
                            <?php echo GoogleRecaptcha2::widget()?>
                        </div>
                        <div class="form-group">
                            <?= Html::submitButton(_t('front.site', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
    </div>
</div>

<?php
$this->registerJs("$('#contact-form').submit(function(){  if ((typeof(ga) != 'undefined') && ga) {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'contact',
                    eventAction:  'submit',
                    eventLabel: 'contact'
                });
            } });");