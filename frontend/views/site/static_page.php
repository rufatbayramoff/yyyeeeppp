<?php
/* @var $this yii\web\View */
/* @var $page \common\models\StaticPage */
$this->title = $page->title . ' - Treatstock';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container static-page-wrap">
    <div class="row wide-padding">
        <div class="col-xs-12">
            <h1 class=" page-header text-center"><?= \H($page->title) ?></h1>
            <?= $page->content; ?>
        </div>
    </div>
</div>
