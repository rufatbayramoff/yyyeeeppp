<?php

/** @var \yii\web\View $this */
/** @var yii\bootstrap\ActiveForm $form */

$this->title = _t('front.site', 'Hire Designer');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 m-t30">
                <div class="panel panel-default box-shadow">
                    <div class="panel-body p-l30 p-r30">
                        <h1 class="text-center m-t0"><?= \H($this->title) ?></h1>
                        <p class="text-center"><?= _t('front.site', 'Form submitted successfully. A designer will be in contact with you shortly.'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
