<?php

use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use yii\helpers\Html;
$this->title = _t('site.page', 'How it Works for Customers');

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container">
<div class="row wide-padding">

    <div class="col-xs-12 site-how-to">
        <h1><?= \H($this->title) ?></h1>

        <?php /*
        <div class="how-to-order">

            <h2 class="how-to-order__title">
                <?=_t('site.page', 'How to Place an Order')?>
            </h2>

            <div class="how-to-order__steps">
                <h3 class="how-to-order__steps-title">
                    <a class="how-to-order__steps-btn" href="#how-to__3" data-toggle="collapse" data-parent="#how-to-accordion" data-target="#how-to__3">
                        <?=_t('site.page', 'Shop for 3D Models')?>
                    </a>
                </h3>
                <p class="how-to-order__steps-text">
                    <?=_t('site.page', 'Browse through our catalog of 3D printable models to find what you are looking for and get them printed using our 3D printing network.')?>
                </p>
            </div>

            <div class="how-to-order__steps">
                <h3 class="how-to-order__steps-title">
                    <a class="how-to-order__steps-btn" href="#how-to__1" data-toggle="collapse" data-parent="#how-to-accordion" data-target="#how-to__1">
                        <?=_t('site.page', 'Create a Custom 3D Design')?>
                    </a>
                </h3>
                <p class="how-to-order__steps-text">
                    <?=_t('site.page', 'If you want to create something unique or need a prototype for your invention, hire a designer to exclusively create your 3D model idea.')?>
                </p>
            </div>

            <div class="how-to-order__steps">
                <h3 class="how-to-order__steps-title">
                    <a class="how-to-order__steps-btn" href="#how-to__2" data-toggle="collapse" data-parent="#how-to-accordion" data-target="#how-to__2">
                        <?=_t('site.page', 'Need your 3D File Printed?')?>
                    </a>
                </h3>
                <p class="how-to-order__steps-text">
                    <?=_t('site.page', 'If you had a custom 3D design created for you or have your own 3D file, upload it here and choose a print service to have it made.')?>
                </p>
            </div>
        </div>
        */?>

        <div id="how-to-accordion">

            <div class="panel how-to__panel">

                <h2 class="how-to__title" data-toggle="collapse" data-parent="#how-to-accordion" data-target="#how-to__4">
                    <span class="tsi tsi-cube"></span>
                    <span class="tsi tsi-up-c"></span>
                    <?=_t('site.page', 'REQUEST A QUOTE FROM A MANUFACTURER')?>
                </h2>

                <div class="how-to__pane collapse in" id="how-to__4">

                    <div class="how-to__pic how-to__pic--findps">
                        <h3 class="how-to__subtitle">&nbsp;</h3>
                    </div>

                    <div class="how-to-steps-wrap" data-width="100%" data-height="350" data-arrows="true" data-click="true">
                        <div class="how-to-steps">
                            <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-1.png" alt="<?=_t('site.page', 'Create a digital model')?>" title="<?=_t('site.page', 'Create a digital model')?>">
                                    */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count how-to-steps__count--one">1</div>
                                <h3>
                                    <a href="<?=CatalogPrintingUrlHelper::printing3dCatalog()?>" data-method="get">
                                        <?=_t('site.page', 'Browse directory')?>
                                    </a>
                                </h3>
                                <p><?=_t('site.page', 'Browse thousands of manufacturers from all over the world and select a material, technology or product application to narrow your search.')?></p>
                            </div>
                        </div>

                        <div class="how-to-steps">
                            <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-2.png" alt="<?=_t('site.page', 'Publish your model')?>" title="<?=_t('site.page', 'Publish your model')?>">
                                    */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count">2</div>
                                <h3><?=_t('site.page', 'Compare manufacturers')?></h3>
                                <p><?=_t('site.page', 'Sort by best rating, lowest price, and distance depending on which is more important for you. Then, compare prices, customer reviews, and delivery options to select a manufacturer of your choice.')?></p>
                            </div>
                        </div>

                        <div class="how-to-steps">
                            <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-3.png" alt="<?=_t('site.page', 'Earn & Enjoy!')?>" title="<?=_t('site.page', 'Earn & Enjoy!')?>">
                                    */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count">3</div>
                                <h3><?=_t('site.page', 'Get a Quote')?></h3>
                                <p><?=_t('site.page', 'Enter your name, project description, attach files, comments, budget, deadline and email address and submit your request to the manufacturer.')?></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="panel how-to__panel">

                <h2 class="how-to__title collapsed" data-toggle="collapse" data-parent="#how-to-accordion" data-target="#how-to__2">
                    <span class="tsi tsi-bag"></span>
                    <span class="tsi tsi-up-c"></span>
                    <?=_t('site.page', 'PLACE AN ORDER WITH A MANUFACTURER')?>
                </h2>

                <div class="how-to__pane collapse" id="how-to__2">

                    <div class="how-to__pic how-to__pic--findps">
                        <h3 class="how-to__subtitle">&nbsp;</h3>
                    </div>

                    <div class="how-to-steps-wrap" data-width="100%" data-height="350" data-arrows="true" data-click="true">
                        <div class="how-to-steps">
                            <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-1.png" alt="<?=_t('site.page', 'Create a digital model')?>" title="<?=_t('site.page', 'Create a digital model')?>">
                                    */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count how-to-steps__count--one">1</div>
                                <h3>
                                    <a href="/upload?utm_source=ts_main" data-method="get">
                                        <?=_t('site.page', 'Upload your files')?>
                                    </a>
                                </h3>
                                <p><?=_t('site.page', 'Upload your files to our secure platform, toggle between mm and inches to get the right sizes, and set the quantities you would like for each file.')?></p>
                            </div>
                        </div>

                        <div class="how-to-steps">
                            <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-2.png" alt="<?=_t('site.page', 'Publish your model')?>" title="<?=_t('site.page', 'Publish your model')?>">
                                    */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count">2</div>
                                <h3><?=_t('site.page', 'Customize and select manufacturer')?></h3>
                                <p><?=_t('site.page', 'Choose the materials and colors to customize your order. Then, compare prices and customer reviews and select a manufacturer of your choice.')?></p>
                            </div>
                        </div>

                        <div class="how-to-steps">
                            <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-3.png" alt="<?=_t('site.page', 'Earn & Enjoy!')?>" title="<?=_t('site.page', 'Earn & Enjoy!')?>">
                                    */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count">3</div>
                                <h3><?=_t('site.page', 'Place your order')?></h3>
                                <p><?=_t('site.page', 'Enter your delivery details, leave comments for the manufacturer, and complete the checkout process to place your order.')?></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="panel how-to__panel">

                <h2 class="how-to__title collapsed" data-toggle="collapse" data-parent="#how-to-accordion" data-target="#how-to__1">
                    <span class="tsi tsi-pencil"></span>
                    <span class="tsi tsi-up-c"></span>
                    <?=_t('site.page', 'HIRE A DESIGNER')?>
                </h2>

                <div class="how-to__pane collapse" id="how-to__1">

                    <div class="how-to__pic how-to__pic--custom">
                        <h3 class="how-to__subtitle">&nbsp;</h3>
                    </div>

                    <div class="how-to-steps-wrap" data-width="100%" data-height="350" data-arrows="true" data-click="true">
                        <div class="how-to-steps">
                            <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-1.png" alt="<?=_t('site.page', 'Create a digital model')?>" title="<?=_t('site.page', 'Create a digital model')?>">
                                    */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count how-to-steps__count--one">1</div>
                                <h3>
                                    <a href="/site/hire-designer" data-method="get">
                                        <?=_t('site.page', 'Select a 3D Designer')?>
                                    </a>
                                </h3>
                                <p><?=_t('site.page', 'Choose from one of our talented designers based on their experience and expertise to have your idea created for you.')?></p>
                            </div>
                        </div>

                        <div class="how-to-steps">
                            <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-2.png" alt="<?=_t('site.page', 'Publish your model')?>" title="<?=_t('site.page', 'Publish your model')?>">
                                    */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count">2</div>
                                <h3><?=_t('site.page', 'Place an Order')?></h3>
                                <p><?=_t('site.page', 'Discuss the deliverables, cost and deadline required to produce your design and make the payment.')?></p>
                            </div>
                        </div>

                        <div class="how-to-steps">
                            <?php /*
                                    <img class="how-to-steps__img" src="/static/images/howto-pic-designer-3.png" alt="<?=_t('site.page', 'Earn & Enjoy!')?>" title="<?=_t('site.page', 'Earn & Enjoy!')?>">
                                    */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count">3</div>
                                <h3><?=_t('site.page', 'Custom 3D Model')?></h3>
                                <p>
                                    <?php
                                    $getPrinted = yii\helpers\Html::a("get it manufactured", ['/upload?utm_source=ts_main']);
                                    $sell = yii\helpers\Html::a("sell", ['/upload']);
                                    echo _t('site.page', 'Once you receive your custom 3D model, you can {getPrinted} or publish and {sell} it on Treatstock.', ['getPrinted' => $getPrinted, 'sell' => $sell]);?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



            <div class="panel how-to__panel">

                <h2 class="how-to__title collapsed" data-toggle="collapse" data-parent="#how-to-accordion" data-target="#how-to__3">
                    <span class="tsi tsi-shopping-cart"></span>
                    <span class="tsi tsi-up-c"></span>
                    <?=_t('site.page', 'BUY 3D MODELS')?>
                </h2>

                <div class="how-to__pane collapse" id="how-to__3">

                    <div class="how-to__pic how-to__pic--shop">
                        <h3 class="how-to__subtitle">&nbsp;</h3>
                    </div>

                    <div class="how-to-steps-wrap" data-width="100%" data-height="350" data-arrows="true" data-click="true">

                        <div class="how-to-steps">
                            <?php /*
                            <img class="how-to-steps__img" src="/static/images/howto-pic-2.png" alt="<?=_t('site.page', 'Choose a print service')?>" title="<?=_t('site.page', 'Choose a print service')?>">
                            */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count">2</div>
                                <h3><?=_t('site.page', 'Select a manufacturer')?></h3>
                                <p><?=_t('site.page', 'Customize your order by choosing the materials, colors, and quantities. Then, compare prices, reviews and delivery options and select a manufacturer of your choice.')?></p>
                            </div>
                        </div>

                        <div class="how-to-steps">
                            <?php /*
                            <img class="how-to-steps__img" src="/static/images/howto-pic-3.png" alt="<?=_t('site.page', 'Enjoy!')?>" title="<?=_t('site.page', 'Enjoy!')?>">
                            */?>
                            <div class="how-to-steps__text">
                                <div class="how-to-steps__count">3</div>
                                <h3><?=_t('site.page', 'Treat yourself!')?></h3>
                                <p><?=_t('site.page', 'Now for the best part, enjoying your newly manufactured products! If you would like, you can leave a review for the manufacturer to share your experience with others.')?></p>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
    
</div>
</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    function makeSliders() {
        var isMobile = window.matchMedia("only screen and (max-width: 767px)");

        if (isMobile.matches) {
            $('.how-to-steps-wrap').addClass('fotorama');
            $('.fotorama').fotorama();
        }
    }

    makeSliders();

    $(function () {

        var hash = window.location.hash;

        if (hash){
            var hashQueryPos = hash.indexOf('?');
            if(hashQueryPos != -1){
                hash = hash.substr(0, hashQueryPos);
            }
        }

        if(hash != null && hash != ""){
            $('.collapse').removeClass('in');
            $('.how-to__title').addClass('collapsed');
            $(hash + '.collapse').collapse('toggle');

            $(".how-to__title").click(function (e) {

                if (window.location.hash != $(this).attr("data-target")) {
                    $('.collapse').removeClass('in');
                    $('.how-to__title').addClass('collapsed');
                    $('.collapse').collapse('hide');
                }
                window.location.hash = $(this).attr("data-target");
            });
        }

        $(".how-to__title").click(function (e) {
            window.location.hash = $(this).attr("data-target");
        });
    });

    $(function () {
        $('#how-to-accordion').on('shown.bs.collapse', function (e) {
            var offset = $('.how-to__panel > .collapse.in').offset();
            if (offset) {
                $('html,body').animate({
                    scrollTop: $('.collapse.in').siblings('.how-to__title').offset().top - 120
                }, 300);
            }
        });
    });

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>