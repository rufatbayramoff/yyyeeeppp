<?php
$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);
?>
<style>
    pre {
        font-size: 14px !important;
    }
    .example-block {
        position: relative;
        padding: 20px;
        border-radius: 5px 5px 0 0;
        border: 1px solid #dfdfdf;
    }

    .code-block {
        padding: 10px 20px;
        border-radius: 0 0 5px 5px;
        background: #f7f9fa;
        border: 1px solid #d7dcde;
        margin-bottom: 20px;
        font-family: "Droid Sans Mono", Menlo, Monaco, monospace;
        color: #333b40;
    }

    .example-block+.code-block {
        border-top: none;
    }

    .bs-component .modal {
        position: relative;
        top: auto;
        right: auto;
        left: auto;
        bottom: auto;
        z-index: 1;
        display: block;
    }
    .bs-component .modal-dialog {
        width: 90%;
    }
    .icons-list {
        margin: 0 -10px;
        padding: 0;
        list-style: none;
        overflow: hidden;
    }

    .icons-list li {
        display: inline-block;
        float: left;
        width: 20%;
        margin: 0 0 20px;
        padding: 0 10px;
    }

    .icons-list .tsi {
        margin: 0 0 10px;
        font-size: 30px;
    }

    .icons-list input {
        width: 100%;
        border-radius: 5px;
        padding: 5px;
        background: #ffffff;
        border: 1px solid #CCCCCC;
        cursor: text;
    }
</style>

<div class="container">
<div class="row">
    <div class="col-xs-12">
        <h1 class="text-center"><strong>Treatstock UI Kit</strong></h1>

        <h1 class="page-header">Typography</h1>
        <div class="row">
            <div class="col-lg-4">
                <div class="bs-component">
                    <h1>h1. Heading</h1>
                    <h2>h2. Heading</h2>
                    <h3>h3. Heading</h3>
                    <h4>h4. Heading</h4>
                    <h5>h5. Heading</h5>
                    <h6>h6. Heading</h6>
                    <p class="lead">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                </div>
            </div>
            <div class="col-lg-4">
                <h2>Example body text</h2>
                <p>Nullam quis risus eget <a href="#">urna mollis ornare</a> vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                <p><small>This line of text is meant to be treated as fine print.</small></p>
                <p>The following snippet of text is <strong>rendered as bold text</strong>.</p>
                <p>The following snippet of text is <em>rendered as italicized text</em>.</p>
                <p>An abbreviation of the word attribute is <abbr title="attribute">attr</abbr>.</p>
            </div>
            <div class="col-lg-4">
                <h2>Emphasis classes</h2>
                <p class="text-muted">Fusce dapibus, tellus ac cursus commodo, tortor mauris nibh.</p>
                <p class="text-primary">Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p class="text-warning">Etiam porta sem malesuada magna mollis euismod.</p>
                <p class="text-danger">Donec ullamcorper nulla non metus auctor fringilla.</p>
                <p class="text-success">Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                <p class="text-info">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            </div>
        </div>


        <h1>Blockquote's</h1>

        <div class="row">
            <div class="col-xs-12">
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                </blockquote>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <blockquote class="pull-right">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                </blockquote>
            </div>
        </div>

        <h1>Checked list</h1>

        <p>Use <code>.checked-list</code> for visiual list</p>

        <div class="example-block">
            <ul class="checked-list">
                <li>Choose from over 100 materials and colors</li>
                <li>Compare instant prices to find the best deals online</li>
                <li>Fast turnarounds with orders generally delivered within 5 days</li>
                <li>All machines are tested and orders pass a quality control check</li>
            </ul>
        </div>

        <h1>Page header</h1>

        <p>Add bottom border to divide blocks</p>

        <div class="example-block">
            <h1 class="page-header">Example header</h1>
        </div>

        <h1>Tables</h1>

        <p>Use the <code>.table</code> to simple tables. Add special classes to style simple table.</p>

        <div class="example-block">
            <div class="row">
                <div class="col-sm-4">
                    <h4>Class <code>.table</code></h4>
                    <table class="table">
                        <tr>
                            <th>Header 1</th>
                            <th>Header 2</th>
                            <th>Header 3</th>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                    </table>
                </div>

                <div class="col-sm-4">
                    <h4>Class <code>.table-striped</code></h4>
                    <table class="table table-striped">
                        <tr>
                            <th>Header 1</th>
                            <th>Header 2</th>
                            <th>Header 3</th>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                    </table>
                </div>

                <div class="col-sm-4">
                    <h4>Class <code>.table-hover</code></h4>
                    <table class="table table-hover">
                        <tr>
                            <th>Header 1</th>
                            <th>Header 2</th>
                            <th>Header 3</th>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                    </table>
                </div>

                <div class="col-sm-6">
                    <h4>Class <code>.table-bordered</code>. Add borders.</h4>
                    <table class="table table-bordered">
                        <tr>
                            <th>Header 1</th>
                            <th>Header 2</th>
                            <th>Header 3</th>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                    </table>
                </div>

                <div class="col-sm-6">
                    <h4>Class <code>.table-striped</code> + <code>.table-hover</code></h4>
                    <table class="table table-striped table-hover">
                        <tr>
                            <th>Header 1</th>
                            <th>Header 2</th>
                            <th>Header 3</th>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                        <tr>
                            <td>Text</td>
                            <td>Text</td>
                            <td>Text</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <h1>Buttons</h1>

        <p>Use the <code>.btn</code> class on a <code>&lt;a&gt;</code>, <code>&lt;button&gt;</code>, or <code>&lt;input&gt;</code> element.</p>

        <div class="example-block">
            <a class="btn btn-default" href="#" role="button">Link</a>
            <button class="btn btn-default" type="submit">Button</button>
            <input class="btn btn-default" type="button" value="Input">
            <input class="btn btn-default" type="submit" value="Submit">
        </div>

        <div class="code-block">
            &lt;a class=&quot;btn btn-default&quot; href=&quot;#&quot; role=&quot;button&quot;&gt;Link&lt;/a&gt;<br>
            &lt;button class=&quot;btn btn-default&quot; type=&quot;submit&quot;&gt;Button&lt;/button&gt;<br>
            &lt;input class=&quot;btn btn-default&quot; type=&quot;button&quot; value=&quot;Input&quot;&gt;<br>
            &lt;input class=&quot;btn btn-default&quot; type=&quot;submit&quot; value=&quot;Submit&quot;&gt;
        </div>

        <h2>Variants</h2>

        <div class="example-block">
            <button type="button" class="btn btn-default">Default</button>
            <button type="button" class="btn btn-primary">Primary</button>
            <button type="button" class="btn btn-success">Success</button>
            <button type="button" class="btn btn-info">Info</button>
            <button type="button" class="btn btn-warning">Warning</button>
            <button type="button" class="btn btn-danger">Danger</button>
            <button type="button" class="btn btn-link">Link Btn</button>
        </div>

        <div class="code-block">
            &lt;button type=&quot;button&quot; class=&quot;btn btn-default&quot;&gt;Default&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-primary&quot;&gt;Primary&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-success&quot;&gt;Success&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-info&quot;&gt;Info&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-warning&quot;&gt;Warning&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-danger&quot;&gt;Danger&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-link&quot;&gt;Link Btn&lt;/button&gt;
        </div>

        <p>Main button sizes.</p>

        <div class="example-block">
            <p>
                <button type="button" class="btn btn-lg btn-default">Default</button>
                <button type="button" class="btn btn-lg btn-primary">Primary</button>
                <button type="button" class="btn btn-lg btn-success">Success</button>
                <button type="button" class="btn btn-lg btn-info">Info</button>
                <button type="button" class="btn btn-lg btn-warning">Warning</button>
                <button type="button" class="btn btn-lg btn-danger">Danger</button>
            </p>
            <p>
                <button type="button" class="btn btn-default">Default</button>
                <button type="button" class="btn btn-primary">Primary</button>
                <button type="button" class="btn btn-success">Success</button>
                <button type="button" class="btn btn-info">Info</button>
                <button type="button" class="btn btn-warning">Warning</button>
                <button type="button" class="btn btn-danger">Danger</button>
            </p>
            <p>
                <button type="button" class="btn btn-sm btn-default">Default</button>
                <button type="button" class="btn btn-sm btn-primary">Primary</button>
                <button type="button" class="btn btn-sm btn-success">Success</button>
                <button type="button" class="btn btn-sm btn-info">Info</button>
                <button type="button" class="btn btn-sm btn-warning">Warning</button>
                <button type="button" class="btn btn-sm btn-danger">Danger</button>
            </p>
            <p>
                <button type="button" class="btn btn-xs btn-default">Default</button>
                <button type="button" class="btn btn-xs btn-primary">Primary</button>
                <button type="button" class="btn btn-xs btn-success">Success</button>
                <button type="button" class="btn btn-xs btn-info">Info</button>
                <button type="button" class="btn btn-xs btn-warning">Warning</button>
                <button type="button" class="btn btn-xs btn-danger">Danger</button>
            </p>
        </div>

        <h2>Ghost buttons</h2>

        <p>Add <code>.btn-ghost</code> to <code>.btn</code> block to make ghost button</p>

        <div class="example-block">
            <p>
                <button type="button" class="btn btn-lg btn-ghost btn-default">Default</button>
                <button type="button" class="btn btn-lg btn-ghost btn-primary">Primary</button>
                <button type="button" class="btn btn-lg btn-ghost btn-success">Success</button>
                <button type="button" class="btn btn-lg btn-ghost btn-info">Info</button>
                <button type="button" class="btn btn-lg btn-ghost btn-warning">Warning</button>
                <button type="button" class="btn btn-lg btn-ghost btn-danger">Danger</button>
            </p>
            <p>
                <button type="button" class="btn btn-ghost btn-default">Default</button>
                <button type="button" class="btn btn-ghost btn-primary">Primary</button>
                <button type="button" class="btn btn-ghost btn-success">Success</button>
                <button type="button" class="btn btn-ghost btn-info">Info</button>
                <button type="button" class="btn btn-ghost btn-warning">Warning</button>
                <button type="button" class="btn btn-ghost btn-danger">Danger</button>
            </p>
            <p>
                <button type="button" class="btn btn-sm btn-ghost btn-default">Default</button>
                <button type="button" class="btn btn-sm btn-ghost btn-primary">Primary</button>
                <button type="button" class="btn btn-sm btn-ghost btn-success">Success</button>
                <button type="button" class="btn btn-sm btn-ghost btn-info">Info</button>
                <button type="button" class="btn btn-sm btn-ghost btn-warning">Warning</button>
                <button type="button" class="btn btn-sm btn-ghost btn-danger">Danger</button>
            </p>
            <p>
                <button type="button" class="btn btn-xs btn-ghost btn-default">Default</button>
                <button type="button" class="btn btn-xs btn-ghost btn-primary">Primary</button>
                <button type="button" class="btn btn-xs btn-ghost btn-success">Success</button>
                <button type="button" class="btn btn-xs btn-ghost btn-info">Info</button>
                <button type="button" class="btn btn-xs btn-ghost btn-warning">Warning</button>
                <button type="button" class="btn btn-xs btn-ghost btn-danger">Danger</button>
            </p>
        </div>

        <h2>Sizes</h2>

        <p>Use classes <code>.btn-xs</code>, <code>.btn-sm</code>, <code>.btn-lg</code> for special sizes</p>

        <div class="example-block">
            <p>
                <button type="button" class="btn btn-primary btn-lg">Large button</button>
                <button type="button" class="btn btn-default btn-lg">Large button</button>
            </p>
            <p>
                <button type="button" class="btn btn-primary">Default button</button>
                <button type="button" class="btn btn-default">Default button</button>
            </p>
            <p>
                <button type="button" class="btn btn-primary btn-sm">Small button</button>
                <button type="button" class="btn btn-default btn-sm">Small button</button>
            </p>
            <p>
                <button type="button" class="btn btn-primary btn-xs">Extra small button</button>
                <button type="button" class="btn btn-default btn-xs">Extra small button</button>
            </p>
        </div>

        <div class="code-block">
            &lt;p&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-primary btn-lg&quot;&gt;Large button&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-default btn-lg&quot;&gt;Large button&lt;/button&gt;<br>
            &lt;/p&gt;<br>
            &lt;p&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-primary&quot;&gt;Default button&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-default&quot;&gt;Default button&lt;/button&gt;<br>
            &lt;/p&gt;<br>
            &lt;p&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-primary btn-sm&quot;&gt;Small button&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-default btn-sm&quot;&gt;Small button&lt;/button&gt;<br>
            &lt;/p&gt;<br>
            &lt;p&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-primary btn-xs&quot;&gt;Extra small button&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-default btn-xs&quot;&gt;Extra small button&lt;/button&gt;<br>
            &lt;/p&gt;
        </div>

        <h2>Circle buttons</h2>

        <p>Add <code>.btn-circle</code> to <code>.btn</code> block to make circle. Use for control buttons.</p>

        <div class="example-block">
            <p>
                <button type="button" class="btn btn-lg btn-circle btn-default"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-lg btn-circle btn-primary"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-lg btn-circle btn-success"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-lg btn-circle btn-info"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-lg btn-circle btn-warning"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-lg btn-circle btn-danger"><span class="tsi tsi-printer"></span></button>
            </p>
            <p>
                <button type="button" class="btn btn-circle btn-default"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-circle btn-primary"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-circle btn-success"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-circle btn-info"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-circle btn-warning"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-circle btn-danger"><span class="tsi tsi-printer"></span></button>
            </p>
            <p>
                <button type="button" class="btn btn-sm btn-circle btn-default"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-sm btn-circle btn-primary"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-sm btn-circle btn-success"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-sm btn-circle btn-info"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-sm btn-circle btn-warning"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-sm btn-circle btn-danger"><span class="tsi tsi-printer"></span></button>
            </p>
            <p>
                <button type="button" class="btn btn-xs btn-circle btn-default"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-xs btn-circle btn-primary"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-xs btn-circle btn-success"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-xs btn-circle btn-info"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-xs btn-circle btn-warning"><span class="tsi tsi-printer"></span></button>
                <button type="button" class="btn btn-xs btn-circle btn-danger"><span class="tsi tsi-printer"></span></button>
            </p>
        </div>

        <h3>Active state</h3>

        <p><code>:active</code> state or add <code>.active</code> class</p>

        <div class="example-block">
            <button type="button" class="btn btn-primary btn-lg active">Primary button</button>
            <button type="button" class="btn btn-default btn-lg active">Button</button>
        </div>

        <div class="code-block">
            &lt;button type=&quot;button&quot; class=&quot;btn btn-primary btn-lg active&quot;&gt;Primary button&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-default btn-lg active&quot;&gt;Button&lt;/button&gt;
        </div>

        <h4>Anchor element</h4>

        <p><code>:active</code> state or add <code>.active</code> class</p>

        <div class="example-block">
            <a href="#" class="btn btn-primary btn-lg active" role="button">Primary link</a>
            <a href="#" class="btn btn-default btn-lg active" role="button">Link</a>
        </div>

        <div class="code-block">
            &lt;a href=&quot;#&quot; class=&quot;btn btn-primary btn-lg active&quot; role=&quot;button&quot;&gt;Primary link&lt;/a&gt;<br>
            &lt;a href=&quot;#&quot; class=&quot;btn btn-default btn-lg active&quot; role=&quot;button&quot;&gt;Link&lt;/a&gt;
        </div>

        <h2>Disabled state</h2>

        <h3>Button element</h3>

        <p><code>disabled</code> attribute or add <code>.disabled</code> class</p>

        <div class="example-block">
            <button type="button" class="btn btn-lg btn-primary" disabled="disabled">Primary button</button>
            <button type="button" class="btn btn-default btn-lg" disabled="disabled">Button</button>
        </div>

        <div class="code-block">
            &lt;button type=&quot;button&quot; class=&quot;btn btn-lg btn-primary&quot; disabled=&quot;disabled&quot;&gt;Primary button&lt;/button&gt;<br>
            &lt;button type=&quot;button&quot; class=&quot;btn btn-default btn-lg&quot; disabled=&quot;disabled&quot;&gt;Button&lt;/button&gt;
        </div>

        <h3>Anchor element</h3>

        <p>Add <code>.disabled</code> class</p>

        <div class="example-block">
            <a href="#" class="btn btn-primary btn-lg disabled" role="button">Primary link</a>
            <a href="#" class="btn btn-default btn-lg disabled" role="button">Link</a>
        </div>

        <div class="code-block">
            &lt;a href=&quot;#&quot; class=&quot;btn btn-primary btn-lg disabled&quot; role=&quot;button&quot;&gt;Primary link&lt;/a&gt;<br>
            &lt;a href=&quot;#&quot; class=&quot;btn btn-default btn-lg disabled&quot; role=&quot;button&quot;&gt;Link&lt;/a&gt;
        </div>

        <h1>File inputs</h1>

        <div class="example-block">
            <div class="row">
                <div class="col-sm-6">
                    <h4>Default <code>.inputfile</code></h4>
                    <p>
                        <input type="hidden" name="[file]" value="">
                        <input type="file"
                               id="inputID"
                               class="inputfile"
                               name="[file]"
                               value=""
                               multiple=""
                               data-multiple-caption="{count} files selected"
                               accept=".stl,.ply">
                        <label class="uploadlabel" for="inputID">
                        <span>
                            <i class="tsi tsi-upload-l"></i>
                            <?= _t('site.main', 'Upload Files'); ?>
                        </span>
                        </label>
                    </p>
                    <p>Wide inputfile <code>.inputfile--wide</code></p>
                    <p>
                        <input type="hidden" name="ChangeCoverForm[file]" value="">
                        <input type="file"
                               id="inpitID2"
                               class="inputfile inputfile--wide"
                               name="ChangeCoverForm[file]"
                               multiple=""
                               data-multiple-caption="{count} files selected"
                               value=""
                               accept="image/jpeg,image/png,image/gif"
                               aria-required="true">
                        <label class="uploadlabel" for="inpitID2">
                            <span>0 files selected</span>
                            <strong>
                                <i class="tsi tsi-upload-l"></i> Choose a picture
                            </strong>
                        </label>
                    </p>
                </div>
                <div class="col-sm-6">
                    <h4>Small <code>.inputfile--sm</code></h4>
                    <p>
                        <input type="hidden" name="[file]" value="">
                        <input type="file"
                               id="inputID3"
                               class="inputfile inputfile--sm"
                               name="[file]"
                               value=""
                               multiple=""
                               data-multiple-caption="{count} files selected"
                               accept=".stl,.ply">
                        <label class="uploadlabel" for="inputID3">
                        <span>
                            <i class="tsi tsi-upload-l"></i>
                            <?= _t('site.main', 'Upload Files'); ?>
                        </span>
                        </label>
                    </p>
                    <p>Wide inputfile <code>.inputfile--wide</code></p>
                    <p>
                        <input type="hidden" name="ChangeCoverForm[file]" value="">
                        <input type="file"
                               id="inpitID4"
                               class="inputfile inputfile--wide inputfile--sm"
                               name="ChangeCoverForm[file]"
                               multiple=""
                               data-multiple-caption="{count} files selected"
                               value=""
                               accept="image/jpeg,image/png,image/gif"
                               aria-required="true">
                        <label class="uploadlabel" for="inpitID4">
                            <span>0 files selected</span>
                            <strong>
                                <i class="tsi tsi-upload-l"></i> Choose a picture
                            </strong>
                        </label>
                    </p>
                </div>
            </div>
        </div>

        <h1>File links</h1>

        <div class="example-block">
            <div class="row">
                <div class="col-sm-6">
                    <h4>Used for download file link. Simple <code>a</code> tag with <code>.simple-file-link</code></h4>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>some file name.doc</span>
                            <i>3.5 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>some picture.png</span>
                            <i>1.2 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>model.stl</span>
                            <i>11.3 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>1.stl</span>
                            <i>1.13 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>i.stl</span>
                            <i>781.87 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>eshe_kakoy-to_file.iges</span>
                            <i>23.4 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>issues.txt</span>
                            <i>0.3 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>some file.doc</span>
                            <i>1.2 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>picture-result.jpg</span>
                            <i>2 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>file name.doc</span>
                            <i>4.6 Mb</i>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h4>Download file link with delete button</h4>
                    <div class="simple-file-link">
                        <button title="<?= _t('site.ps', 'Delete File'); ?>">&times;</button>
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>PS_added_file.doc</span>
                            <i>0.6 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <button title="<?= _t('site.ps', 'Delete File'); ?>">&times;</button>
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>service_added_file.png</span>
                            <i>2.8 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <button title="<?= _t('site.ps', 'Delete File'); ?>">&times;</button>
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>l.stl</span>
                            <i>1.1 Mb</i>
                        </a>
                    </div>
                    <div class="simple-file-link">
                        <button title="<?= _t('site.ps', 'Delete File'); ?>">&times;</button>
                        <a class="simple-file-link__link" href="#linkToDownload">
                            <span>i.stl</span>
                            <i>8791.79 Mb</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <h1>Colors</h1>

        <h2>Text colors</h2>

        <div class="example-block">
            <p class="text-muted">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            <p class="text-primary">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            <p class="text-success">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            <p class="text-info">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            <p class="text-warning">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            <p class="text-danger">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        </div>

        <div class="code-block">
            &lt;p class=&quot;text-muted&quot;&gt;...&lt;/p&gt;<br>
            &lt;p class=&quot;text-primary&quot;&gt;...&lt;/p&gt;<br>
            &lt;p class=&quot;text-success&quot;&gt;...&lt;/p&gt;<br>
            &lt;p class=&quot;text-info&quot;&gt;...&lt;/p&gt;<br>
            &lt;p class=&quot;text-warning&quot;&gt;...&lt;/p&gt;<br>
            &lt;p class=&quot;text-danger&quot;&gt;...&lt;/p&gt;
        </div>

        <h2>Background colors</h2>

        <div class="example-block bg-color-examples">
            <p class="bar bg-primary">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            <p class="bar bg-success">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            <p class="bar bg-info">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            <p class="bar bg-warning">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
            <p class="bar bg-danger">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        </div>

        <div class="code-block">
            &lt;p class=&quot;bar bg-primary&quot;&gt;...&lt;/p&gt;<br>
            &lt;p class=&quot;bar bg-success&quot;&gt;...&lt;/p&gt;<br>
            &lt;p class=&quot;bar bg-info&quot;&gt;...&lt;/p&gt;<br>
            &lt;p class=&quot;bar bg-warning&quot;&gt;...&lt;/p&gt;<br>
            &lt;p class=&quot;bar bg-danger&quot;&gt;...&lt;/p&gt;
        </div>

        <h1>Dropdowns</h1>

        <p>Add <code>.dropdown-primary</code> class to <code>.dropdown</code> with Primary Button</p>
        <p>Add <code>.dropdown-danger</code> class to <code>.dropdown</code> with Danger Button</p>
        <p>Add <code>.dropdown-warning</code> class to <code>.dropdown</code> with Warning Button</p>
        <p>Default Button don't need to additional class. It's works by default =)</p>

        <div class="example-block">
            <div class="row">
                <div class="col-md-2">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="dropdown dropdown-primary">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="dropdown dropdown-success">
                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="dropdown dropdown-danger">
                        <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="dropdown dropdown-warning">
                        <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <h3>Right aligned dropdown menu</h3>

        <p>Add <code>.dropdown-menu-right</code> class to <code>.dropdown-menu</code> block and add <code>.dropdown-menu-right-toggle</code> class to <code>.dropdown-toggle</code> button.</p>
        <p><span class="text-danger">Attention!</span> If you use right dropdown menu, add to <code>.dropdown</code> block <code>float: right;</code> CSS style. Make it contextually.</p>

        <div class="example-block">
            <div class="row">
                <div class="col-md-2">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="dropdown dropdown-primary">
                        <button class="btn btn-primary dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="dropdown dropdown-success">
                        <button class="btn btn-success dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="dropdown dropdown-danger">
                        <button class="btn btn-danger dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="dropdown dropdown-warning">
                        <button class="btn btn-warning dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <h3>Dropup</h3>

        <div class="example-block">
            <div class="dropup">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropup
                    <span class="tsi tsi-up"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>
        </div>

        <h3>Hover dropdown</h3>

        <p>Add new class <code>.dropdown-onhover</code> to <code>.dropdown</code> block.</p>

        <div class="example-block">
            <div class="row">
                <div class="col-md-2">
                    <div class="dropdown dropdown-onhover">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="dropdown dropdown-primary dropdown-onhover">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="dropdown dropdown-success dropdown-onhover">
                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="dropdown dropdown-danger dropdown-onhover">
                        <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="dropdown dropdown-warning dropdown-onhover">
                        <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Dropdown
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <h3>Split button with dropdown</h3>

        <p>Button with additional dropdown.</p>

        <div class="example-block">
            <div class="row">
                <div class="col-md-2">
                    <div class="btn-group dropdown dropdown-default">
                        <button type="button" class="btn btn-default">Button</button>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="btn-group dropdown dropdown-primary">
                        <button type="button" class="btn btn-primary">Button</button>
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="btn-group dropdown dropdown-success">
                        <button type="button" class="btn btn-success">Button</button>
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="btn-group dropdown dropdown-danger">
                        <button type="button" class="btn btn-danger">Button</button>
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="btn-group dropdown dropdown-warning">
                        <button type="button" class="btn btn-warning">Button</button>
                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <h1>Datalist</h1>
        <input list="datalist1" name="result"  class="form-control" />
        <datalist id="datalist1">
            <option value="Test">Test</option>
            <option value="Abs">Abs</option>
            <option value="Fer">Fer</option>
            <option value="Der">Der</option>
        </datalist>

        <h1>Color switch dropdown</h1>

        <div class="example-block">
            <div class="row">
                <div class="col-sm-4">

                    <div class="model-page__sidebar-title">MATERIAL AND COLOR</div>

                    <div class="color-switcher-tab">
                        <a href="#" class="color-switcher-tab__tab-item">Author’s choice</a>
                        <a href="#" class="color-switcher-tab__tab-item color-switcher-tab__tab-item--active">Customize</a>
                    </div>

                    <div class="checkbox color-switcher-checkbox">
                        <input id="checkboxColor" type="checkbox">
                        <label for="checkboxColor">
                            Set one material for the whole kit
                        </label>
                    </div>

                    <div class="dropdown color-switcher-block">
                        <div class="dropdown color-switcher">
                            <button class="btn btn-default color-switcher__btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <div class="color-switcher__pic">
                                    <img src="/static/images/material-plastic.jpg">
                                </div>

                                <div class="color-switcher__title">
                                    Heat resistant plastic
                                    <div class="color-switcher__title-hint">
                                        Durable prototypes that behave and look like polypropilene. <a href="#">Learn more</a>
                                    </div>
                                </div>
                                <span class="tsi tsi-down"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuColorSwitcher">
                                <li>
                                    <a href="#">
                                        <div class="color-switcher__pic">
                                            <img src="/static/images/material-bronze.jpg">
                                        </div>

                                        <div class="color-switcher__title">
                                            Composite
                                            <div class="color-switcher__title-hint">
                                                Wood, bronze, copper and other strange materials
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="color-switcher__pic">
                                            <img src="/static/images/material-plasticABS.jpg">
                                        </div>

                                        <div class="color-switcher__title">
                                            ABS
                                            <div class="color-switcher__title-hint">
                                                Durable plastic, need to be strongman to break it
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="color-switcher__pic">
                                            <img src="/static/images/material-plastic-flexible.jpg">
                                        </div>

                                        <div class="color-switcher__title">
                                            FlexPLA
                                            <div class="color-switcher__title-hint">
                                                Flexible plastic, you can bend it in all ways and stretch it
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="color-switcher__material-list">
                            <input type="checkbox" class="showhideColorSwitcher" id="color-switcher1">
                            <label class="color-switcher__mobile" for="color-switcher1">
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(0,204,51)"></div>
                                    <div class="material-item__label">Green</div>
                                </div>
                                <span class="tsi tsi-down"></span>
                            </label>

                            <div class="color-switcher__color-list">
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(140,120,26)"></div>
                                    <div class="material-item__label">Bronze</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item material-item--active">
                                    <div class="material-item__color" style="background-color: rgb(184,115,51)"></div>
                                    <div class="material-item__label">Copper</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: #2f4f4d"></div>
                                    <div class="material-item__label">DarkSlateGray</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(0,204,51)"></div>
                                    <div class="material-item__label">Green</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(0,102,204)"></div>
                                    <div class="material-item__label">Blue</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(204,0,0)"></div>
                                    <div class="material-item__label">Red</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: #2f4f4d"></div>
                                    <div class="material-item__label">DarkSlateGray</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(255, 204, 0)"></div>
                                    <div class="material-item__label">Yellow</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(56,176,204)"></div>
                                    <div class="material-item__label">Cyan</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(255,255,255)"></div>
                                    <div class="material-item__label">White</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(184, 100, 0)"></div>
                                    <div class="material-item__label">Wood</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(0,0,0)"></div>
                                    <div class="material-item__label">Black</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(121,121,121)"></div>
                                    <div class="material-item__label">Gray</div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-sm-4 m-t30 m-b30"></div>

                <div class="col-sm-4">

                    <div class="model-page__sidebar-title">MATERIAL AND COLOR</div>

                    <div class="color-switcher-tab">
                        <a href="#" class="color-switcher-tab__tab-item color-switcher-tab__tab-item--active">Author’s choice</a>
                        <a href="#" class="color-switcher-tab__tab-item">Customize</a>
                    </div>

                    <div class="color-multiple-warning">
                        <div class="color-multiple-warning__title">
                            Multiple materials and colors
                        </div>
                        <div class="color-multiple-warning__hint">
                            Author recommended different materials and colors.
                            <br>
                            Select a model to view them.
                        </div>
                    </div>

                    <div class="color-switcher-block color-switcher-block--multiple">
                        <div class="color-switcher">
                            <div class="color-switcher__btn">
                                <div class="color-switcher__pic">
                                    <img src="/static/images/material-plastic.jpg">
                                </div>

                                <div class="color-switcher__title">
                                    Heat resistant plastic
                                    <div class="color-switcher__title-hint">
                                        Durable prototypes that behave and look like polypropilene. <a href="#">Learn more</a>
                                    </div>
                                </div>
                            </div>

                            <div class="color-switcher__material-list">
                                <input type="checkbox" class="showhideColorSwitcher" id="color-switcher2">
                                <label class="color-switcher__mobile" for="color-switcher2">
                                    <div title="Biodegradable and flexible plastic" class="material-item">
                                        <div class="material-item__color" style="background-color: rgb(0,204,51)"></div>
                                        <div class="material-item__label">Green</div>
                                    </div>
                                    <span class="tsi tsi-down"></span>
                                </label>

                                <div class="color-switcher__color-list">
                                    <div title="Biodegradable and flexible plastic" class="material-item material-item--active">
                                        <div class="material-item__color" style="background-color: rgb(204,0,0)"></div>
                                        <div class="material-item__label">Red</div>
                                    </div>
                                    <div title="Biodegradable and flexible plastic" class="material-item">
                                        <div class="material-item__color" style="background-color: rgb(255, 204, 0)"></div>
                                        <div class="material-item__label">Yellow</div>
                                    </div>
                                    <div title="Biodegradable and flexible plastic" class="material-item">
                                        <div class="material-item__color" style="background-color: rgb(56,176,204)"></div>
                                        <div class="material-item__label">Cyan</div>
                                    </div>
                                    <div title="Biodegradable and flexible plastic" class="material-item">
                                        <div class="material-item__color" style="background-color: rgb(255,255,255)"></div>
                                        <div class="material-item__label">White</div>
                                    </div>
                                    <div title="Biodegradable and flexible plastic" class="material-item">
                                        <div class="material-item__color" style="background-color: rgb(184, 100, 0)"></div>
                                        <div class="material-item__label">Wood</div>
                                    </div>
                                    <div title="Biodegradable and flexible plastic" class="material-item">
                                        <div class="material-item__color" style="background-color: rgb(0,0,0)"></div>
                                        <div class="material-item__label">Black</div>
                                    </div>
                                    <div title="Biodegradable and flexible plastic" class="material-item">
                                        <div class="material-item__color" style="background-color: rgb(0,204,51)"></div>
                                        <div class="material-item__label">Green</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <h4>Simple color block in document. Can replace <code>a</code> tag with <code>div</code> tag.</h4>

                    <div class="m-b30">
                        Color:
                        <div title="Biodegradable and flexible plastic" class="material-item">
                            <div class="material-item__color" style="background-color: rgb(0,204,51)"></div>
                            <div class="material-item__label">Green</div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-4">

                    <div class="model-page__sidebar-title">MATERIAL AND COLOR (Slider mode)</div>

                    <div class="color-switcher-block">

                        <div class="color-switcher fotorama" data-maxheight="80">

                            <div class="color-switcher__btn">
                                <div class="color-switcher__pic">
                                    <img src="/static/images/material-plastic.jpg">
                                </div>

                                <div class="color-switcher__title">
                                    Heat resistant plastic
                                    <div class="color-switcher__title-hint">
                                        Durable prototypes that behave and look like polypropilene. <a href="#">Learn more</a>
                                    </div>
                                </div>
                            </div>

                            <div class="color-switcher__btn">
                                <div class="color-switcher__pic">
                                    <img src="/static/images/material-bronze.jpg">
                                </div>

                                <div class="color-switcher__title">
                                    Composite
                                    <div class="color-switcher__title-hint">
                                        Wood, bronze, copper and other strange materials
                                    </div>
                                </div>
                            </div>

                            <div class="color-switcher__btn">
                                <div class="color-switcher__pic">
                                    <img src="/static/images/material-plasticABS.jpg">
                                </div>

                                <div class="color-switcher__title">
                                    ABS
                                    <div class="color-switcher__title-hint">
                                        Durable plastic, need to be strongman to break it
                                    </div>
                                </div>
                            </div>

                            <div class="color-switcher__btn">
                                <div class="color-switcher__pic">
                                    <img src="/static/images/material-plastic-flexible.jpg">
                                </div>

                                <div class="color-switcher__title">
                                    FlexPLA
                                    <div class="color-switcher__title-hint">
                                        Flexible plastic, you can bend it in all ways and stretch it
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="color-switcher__material-list">

                            <input type="checkbox" class="showhideColorSwitcher" id="color-switcher3">
                            <label class="color-switcher__mobile" for="color-switcher3">
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(0,204,51)"></div>
                                    <div class="material-item__label">Green</div>
                                </div>
                                <span class="tsi tsi-down"></span>
                            </label>

                            <div class="color-switcher__color-list">
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(140,120,26)"></div>
                                    <div class="material-item__label">Bronze</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item material-item--active">
                                    <div class="material-item__color" style="background-color: rgb(184,115,51)"></div>
                                    <div class="material-item__label">Copper</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: #2f4f4d"></div>
                                    <div class="material-item__label">DarkSlateGray</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(0,204,51)"></div>
                                    <div class="material-item__label">Green</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(0,102,204)"></div>
                                    <div class="material-item__label">Blue</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(204,0,0)"></div>
                                    <div class="material-item__label">Red</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: #2f4f4d"></div>
                                    <div class="material-item__label">DarkSlateGray</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(255, 204, 0)"></div>
                                    <div class="material-item__label">Yellow</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(56,176,204)"></div>
                                    <div class="material-item__label">Cyan</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(255,255,255)"></div>
                                    <div class="material-item__label">White</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(184, 100, 0)"></div>
                                    <div class="material-item__label">Wood</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(0,0,0)"></div>
                                    <div class="material-item__label">Black</div>
                                </div>
                                <div title="Biodegradable and flexible plastic" class="material-item">
                                    <div class="material-item__color" style="background-color: rgb(121,121,121)"></div>
                                    <div class="material-item__label">Gray</div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

        <h1>Printer Service List</h1>

        <div class="example-block">

            <div class="sample-container" style="width: 100%; max-width: 720px;">

                <div class="model-printers-title">
                    <span class="model-printers-title__text">Where to print</span>

                    <span class="model-printers-title__sort"></span>

                    <span class="model-printers-title__map">
                        <span class="tsi tsi-map-marker"></span>
                        <a id="ts-user-location" class="ts-ajax-modal" href="/geo/change-location" title="Shipping address" data-target="#changelocation">Washington, US</a>
                    </span>
                </div>

                <div class="row model-printers-available">
                    <div class="model-printers-available__wrap">
                        <div class="model-printers-available__info">
                            <div class="model-printers-available__info__img">
                                <img src="/static//images/3drender.png" width="30" height="30" alt="" align="left">
                            </div>

                            <div class="model-printers-available__info__ps">
                                <div class="model-printers-available__info__ps-title">
                                    <span>Ilnur's PS</span>                    </div>
                                <div class="model-printers-available__info__ps-printer">
                                    Printer:
                                    <a href="/catalog/printer?id=2436">FlashForge Ilnur's</a>
                                    <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                                        Professional
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="model-printers-available__delivery">
                            <div class="model-printers-available__delivery-title">Adelphi, Maryland</div>
                            Pickup, Free Delivery
                        </div>
                        <div class="model-printers-available__price">
                            <span class="ts-print-price">
                                $1,000.00
                            </span>
                            <div class="min-print-price"></div>
                        </div>
                        <div class="model-printers-available__color">
                            <span class="btn btn-sm ts-material-color" title="Blue" style="background-color: rgb(0,102,204)"></span>
                        </div>
                    </div>

                    <div class="model-printers-available__print-btn">
                        <a class="btn btn-warning btn-block ts-ajax-modal" href="/store/cart/add/165/2436/PLA/Blue" title="Get printed" data-target="#modal-getPrinted">
                            <span class="tsi tsi-printer3d"></span> Print
                        </a>
                    </div>
                </div>

                <div class="row model-printers-available">
                    <div class="model-printers-available__wrap">
                        <div class="model-printers-available__info">
                            <div class="model-printers-available__info__img">
                                <img src="/static/images/images.jpg" width="30" height="30" alt="" align="left">
                            </div>
                            <div class="model-printers-available__info__ps">
                                <div class="model-printers-available__info__ps-title">
                                    <span>Rafa's 3D print service</span>
                                </div>
                                <div class="model-printers-available__info__ps-printer">
                                    Printer:
                                    <a href="/catalog/printer?id=2425">Ultimaker 2</a>
                                    <div class="cert-label cert-label--common" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This printer has a 'Common Certificate' that confirms it is capable of printing average models to a high quality.">
                                        <span class="tsi tsi-checkmark"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="model-printers-available__delivery">
                            <div class="model-printers-available__delivery-title">Cincinnati, Ohio</div>
                            Pickup, Delivery
                        </div>
                        <div class="model-printers-available__price">
                            <span class="ts-print-price">
                                $0.05
                            </span>
                            <div class="min-print-price"></div>
                        </div>
                        <div class="model-printers-available__color">
                            <span class="btn btn-sm ts-material-color" title="Blue" style="background-color: rgb(0,102,204)"></span>
                        </div>
                    </div>

                    <div class="model-printers-available__print-btn">
                        <a class="btn btn-warning btn-block ts-ajax-modal" href="/store/cart/add/165/2425/PLA/Blue" title="Get printed" data-target="#modal-getPrinted">
                            <span class="tsi tsi-printer3d"></span> Print
                        </a>
                    </div>
                </div>

                <div class="row model-printers-available">
                    <div class="model-printers-available__wrap">
                        <div class="model-printers-available__info">
                            <div class="model-printers-available__info__img">
                                <img src="/static/images/ps-pic3.jpg" width="30" height="30" alt="" align="left">
                            </div>
                            <div class="model-printers-available__info__ps">
                                <div class="model-printers-available__info__ps-title">
                                    <span>PS (Ilnur)</span>
                                </div>
                                <div class="model-printers-available__info__ps-printer">
                                    Printer:
                                    <a href="/catalog/printer?id=2439"> Last PS Ilnur</a>
                                </div>
                            </div>
                        </div>

                        <div class="model-printers-available__delivery">
                            <div class="model-printers-available__delivery-title">Adelphi, Maryland</div>
                            Pickup, Free Delivery
                        </div>
                        <div class="model-printers-available__price">
                            <span class="ts-print-price">
                                $129.60
                            </span>
                            <div class="min-print-price"></div>
                        </div>
                        <div class="model-printers-available__color">
                            <span class="btn btn-sm ts-material-color" title="Blue" style="background-color: rgb(0,102,204)"></span>
                        </div>
                    </div>

                    <div class="model-printers-available__print-btn">
                        <a class="btn btn-warning btn-block ts-ajax-modal" href="/store/cart/add/165/2439/PLA/Blue" title="Get printed" data-target="#modal-getPrinted">
                            <span class="tsi tsi-printer3d"></span> Print
                        </a>
                    </div>
                </div>

                <a class="tsrm-toggle model-printers-available__show-more" href="#">Show all print services</a>
            </div>

        </div>

        <h1>Delivery options</h1>

        <div class="example-block">
            <div class="row">
                <div class="col-md-9 wide-padding--right">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="delivery-options-right-header">
                                Delivery options
                            </div>

                            <div class="right-delivery-block">

                                <div class="right-delivery-block__item">

                                    <div class="checkbox">

                                        <input type="checkbox" autocomplete="off" class="ng-valid ng-dirty ng-valid-parse ng-not-empty ng-touched" checked="checked">

                                        <label>Customer Pickup</label>

                                        <div class="delivery-option-field" style="display: block;">

                                            <!-- Delivery Schedule -->
                                            <div class="delivery-schedule">
                                                <div class="delivery-schedule__title">Working hours:</div>
                                                <div class="delivery-schedule__add-more">
                                                    <span class="tsi tsi-plus"></span>
                                                </div>

                                                <div class="delivery-schedule__day-list">
                                                    <div class="delivery-schedule__day">
                                                        Mo
                                                    </div>
                                                    <div class="delivery-schedule__day is-active">
                                                        Tu
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        We
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        Th
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        Fr
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        Sa
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        Su
                                                    </div>
                                                </div>

                                                <div class="delivery-schedule__time">
                                                    <div class="delivery-schedule__time-range">
                                                        <input type="time" class="form-control delivery-schedule__time-value">

                                                        <?php
                                                        echo kartik\select2\Select2::widget([
                                                            'name' => 'delivery-schedule__time-system',
                                                            'value' => '0',
                                                            'data' => ['AM','PM'],
                                                            'hideSearch' => true,
                                                            'options' => [
                                                                'multiple' => false,
                                                                'class' => 'delivery-schedule__time-system',
                                                            ]
                                                        ]);
                                                        ?>
                                                    </div>

                                                    <div class="delivery-schedule__time-divider">—</div>

                                                    <div class="delivery-schedule__time-range">
                                                        <input type="time" class="form-control delivery-schedule__time-value">

                                                        <?php
                                                        echo kartik\select2\Select2::widget([
                                                            'name' => 'delivery-schedule__time-system',
                                                            'value' => '0',
                                                            'data' => ['AM','PM'],
                                                            'hideSearch' => true,
                                                            'options' => [
                                                                'multiple' => false,
                                                                'class' => 'delivery-schedule__time-system',
                                                            ]
                                                        ]);
                                                        ?>
                                                    </div>
                                                </div>

                                                <!-- Additional time block -->

                                                <div class="delivery-schedule__day-list">
                                                    <div class="delivery-schedule__day">
                                                        Mo
                                                    </div>
                                                    <div class="delivery-schedule__day is-active">
                                                        Tu
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        We
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        Th
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        Fr
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        Sa
                                                    </div>
                                                    <div class="delivery-schedule__day">
                                                        Su
                                                    </div>
                                                </div>

                                                <div class="delivery-schedule__time">
                                                    <div class="delivery-schedule__time-range">
                                                        <input type="time" class="form-control delivery-schedule__time-value">

                                                        <?php
                                                        echo kartik\select2\Select2::widget([
                                                            'name' => 'delivery-schedule__time-system',
                                                            'value' => '0',
                                                            'data' => ['AM','PM'],
                                                            'hideSearch' => true,
                                                            'options' => [
                                                                'multiple' => false,
                                                                'class' => 'delivery-schedule__time-system',
                                                            ]
                                                        ]);
                                                        ?>
                                                    </div>

                                                    <div class="delivery-schedule__time-divider">—</div>

                                                    <div class="delivery-schedule__time-range">
                                                        <input type="time" class="form-control delivery-schedule__time-value">

                                                        <?php
                                                        echo kartik\select2\Select2::widget([
                                                            'name' => 'delivery-schedule__time-system',
                                                            'value' => '0',
                                                            'data' => ['AM','PM'],
                                                            'hideSearch' => true,
                                                            'options' => [
                                                                'multiple' => false,
                                                                'class' => 'delivery-schedule__time-system',
                                                            ]
                                                        ]);
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- // Delivery Schedule -->

                                        </div>
                                    </div>

                                </div>

                                <div class="right-delivery-block__item">

                                    <div class="checkbox">

                                        <input type="checkbox" autocomplete="off" class="ng-valid ng-empty ng-dirty ng-valid-parse ng-touched">

                                        <label class="ng-binding">Domestic shipping</label>

                                    </div>

                                </div>
                                <div class="right-delivery-block__item ng-scope">

                                    <div class="checkbox">

                                        <input type="checkbox" autocomplete="off" class="ng-valid ng-empty ng-dirty ng-valid-parse ng-touched">

                                        <label class="ng-binding">International shipping</label>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h1>Input groups</h1>

        <div class="example-block">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">@</span>
                <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
            </div>
            <br>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">@example.com</span>
            </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                <span class="input-group-addon">.00</span>
            </div>
            <br>
            <label for="basic-url">Your vanity URL</label>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon3">https://example.com/users/</span>
                <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">
            </div>
        </div>

        <h2>Checkboxes and radio addons</h2>

        <div class="example-block">
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <div class="checkbox">
                                <input type="checkbox">
                                <label></label>
                            </div>
                        </span>
                        <input type="text" class="form-control">
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->

                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <div class="radio">
                                <input type="radio">
                                <label></label>
                            </div>
                        </span>
                        <input type="text" class="form-control">
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
        </div>

        <h2>Button addons</h2>

        <div class="example-block">
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                        <input type="text" class="form-control" placeholder="Search for...">
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
        </div>

        <h1>Tabs</h1>

        <p>U can add <code>.border-0</code> class to the <code>.nav .nav-tabs</code> block to remove bottom border</p>

        <div class="example-block">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab">Home</a></li>
                <li><a href="#profile" data-toggle="tab">Profile</a></li>
                <li class="disabled"><a>Disabled</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Dropdown
                        <span class="tsi tsi-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#dropdown1" data-toggle="tab">Action</a></li>
                        <li class="divider"></li>
                        <li><a href="#dropdown2" data-toggle="tab">Another action</a></li>
                    </ul>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="home">
                    <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                </div>
                <div class="tab-pane fade" id="profile">
                    <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit.</p>
                </div>
                <div class="tab-pane fade" id="dropdown1">
                    <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork.</p>
                </div>
                <div class="tab-pane fade" id="dropdown2">
                    <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater.</p>
                </div>
            </div>
        </div>

        <h1>Breadcrumbs</h1>

        <div class="example-block">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Printer Store</a></li>
                <li><a href="#">My Orders</a></li>
            </ul>
        </div>

        <h1>Pagination</h1>

        <p>Use <code>.disabled</code> and <code>.active</code> classes to <code>li</code> blocks</p>

        <div class="example-block">
            <nav>
                <ul class="pagination">
                    <li class="prev">
                        <a href="#" aria-label="Previous">
                            <span class="tsi tsi-left"></span>
                        </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li class="disabled"><a href="#">2</a></li>
                    <li class="active"><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li class="next">
                        <a href="#" aria-label="Next">
                            <span class="tsi tsi-right"></span>
                        </a>
                    </li>
                </ul>
            </nav>

            <nav>
                <ul class="pagination">
                    <li class="prev disabled">
                        <a href="#" aria-label="Previous">
                            <span class="tsi tsi-left"></span>
                        </a>
                    </li>
                    <li class="active"><a href="#1">1</a></li>
                    <li><a href="#2">2</a></li>
                    <li><a href="#3">3</a></li>
                    <li>
                        <a class="pagination__dots" href="#dots">
                            <span class="pagination__dots-body"></span>
                        </a>
                    </li>
                    <li><a href="#8">8</a></li>
                    <li><a href="#9">9</a></li>
                    <li><a href="#10">10</a></li>
                    <li class="next">
                        <a href="#" aria-label="Next">
                            <span class="tsi tsi-right"></span>
                        </a>
                    </li>
                </ul>
            </nav>

            <nav>
                <ul class="pagination">
                    <li class="prev">
                        <a href="#" aria-label="Previous">
                            <span class="tsi tsi-left"></span>
                        </a>
                    </li>
                    <li><a href="#1">1</a></li>
                    <li>
                        <a class="pagination__dots" href="#dots">
                            <span class="pagination__dots-body"></span>
                        </a>
                    </li>
                    <li class="active"><a href="#4">4</a></li>
                    <li><a href="#5">5</a></li>
                    <li><a href="#6">6</a></li>
                    <li>
                        <a class="pagination__dots" href="#dots">
                            <span class="pagination__dots-body"></span>
                        </a>
                    </li>
                    <li><a href="#10">10</a></li>
                    <li class="next">
                        <a href="#" aria-label="Next">
                            <span class="tsi tsi-right"></span>
                        </a>
                    </li>
                </ul>
            </nav>

            <nav>
                <ul class="pagination">
                    <li class="prev">
                        <a href="#" aria-label="Previous">
                            <span class="tsi tsi-left"></span>
                        </a>
                    </li>
                    <li><a href="#1">1</a></li>
                    <li><a href="#2">2</a></li>
                    <li><a href="#3">3</a></li>
                    <li>
                        <a class="pagination__dots" href="#dots">
                            <span class="pagination__dots-body"></span>
                        </a>
                    </li>
                    <li class="active"><a href="#8">8</a></li>
                    <li><a href="#9">9</a></li>
                    <li><a href="#10">10</a></li>
                    <li class="next">
                        <a href="#" aria-label="Next">
                            <span class="tsi tsi-right"></span>
                        </a>
                    </li>
                </ul>
            </nav>

            <nav>
                <ul class="pagination">
                    <li class="prev">
                        <a href="#" aria-label="Previous">
                            <span class="tsi tsi-left"></span>
                        </a>
                    </li>
                    <li><a href="#1">1</a></li>
                    <li><a href="#2">2</a></li>
                    <li><a href="#3">3</a></li>
                    <li>
                        <a class="pagination__dots" href="#dots">
                            <span class="pagination__dots-body"></span>
                        </a>
                    </li>
                    <li><a href="#8">8</a></li>
                    <li><a href="#9">9</a></li>
                    <li class="active"><a href="#10">10</a></li>
                    <li class="next disabled">
                        <a href="#" aria-label="Next">
                            <span class="tsi tsi-right"></span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <h1>Labels</h1>

        <div class="example-block">
            <h1>Example heading <span class="label label-default">New</span></h1>
            <h2>Example heading <span class="label label-default">New</span></h2>
            <h3>Example heading <span class="label label-default">New</span></h3>
            <h4>Example heading <span class="label label-default">New</span></h4>
            <h5>Example heading <span class="label label-default">New</span></h5>
            <h6>Example heading <span class="label label-default">New</span></h6>
        </div>

        <h2>Colors</h2>

        <div class="example-block">
            <span class="label label-default">Default</span>
            <span class="label label-primary">Primary</span>
            <span class="label label-success">Success</span>
            <span class="label label-info">Info</span>
            <span class="label label-warning">Warning</span>
            <span class="label label-danger">Danger</span>
        </div>

        <h1>Badges</h1>

        <div class="example-block">
            <a href="#">Inbox <span class="badge">42</span></a>

            <br>
            <br>

            <button class="btn btn-primary" type="button">
                Messages <span class="badge">4</span>
            </button>
        </div>

        <h1>Alerts</h1>

        <div class="example-block">
            <div class="alert alert-success" role="alert">Alert block</div>
            <div class="alert alert-primary" role="alert">Alert block</div>
            <div class="alert alert-info" role="alert">Alert block</div>
            <div class="alert alert-warning" role="alert">Alert block</div>
            <div class="alert alert-danger" role="alert">Alert block</div>

            <br>

            <div class="alert alert-success alert--text-normal" role="alert">Alert block with class=".alert--text-normal"</div>
            <div class="alert alert-primary alert--text-normal" role="alert">alert--text-normal</div>
        </div>

        <h2>Dismissible alerts</h2>

        <p>Build on any alert by adding an optional <code>.alert-dismissible</code> and <code>close</code> button.</p>

        <div class="example-block">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Success!</strong> Better check yourself, you're not looking too good.
            </div>
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Info!</strong> Better check yourself, you're not looking too good.
            </div>
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> Better check yourself, you're not looking too good.
            </div>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> Better check yourself, you're not looking too good.
            </div>

            <br>

            <div class="alert alert-success alert-dismissible alert--text-normal" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Normal text!</strong> additional class=".alert--text-normal" make inner text normal as on site page.
            </div>
        </div>

        <h2>Links in alerts</h2>
        <p>Use the <code>.alert-link</code> utility class to quickly provide matching colored links within any alert.</p>

        <div class="example-block">
            <div class="alert alert-primary" role="alert">
                <strong>Well done!</strong> You successfully read <a href="#" class="alert-link">this important alert message</a>.
            </div>
            <div class="alert alert-success" role="alert">
                <strong>Well done!</strong> You successfully read <a href="#" class="alert-link">this important alert message</a>.
            </div>
            <div class="alert alert-info" role="alert">
                <strong>Heads up!</strong> This <a href="#" class="alert-link">alert needs your attention</a>, but it's not super important.
            </div>
            <div class="alert alert-warning" role="alert">
                <strong>Warning!</strong> Better check yourself, you're <a href="#" class="alert-link">not looking too good</a>.
            </div>
            <div class="alert alert-danger" role="alert">
                <strong>Oh snap!</strong> <a href="#" class="alert-link">Change a few things up</a> and try submitting again.
            </div>
        </div>

        <h1>Panels</h1>
        <p>Use <code>.panel</code> class.</p>
        <p>U can add <code>.box-shadow</code> helper class to add shadow effect or <code>.box-border</code> to make 2px borders</p>

        <div class="example-block">
            <div class="panel panel-default box-shadow">
                <div class="panel-heading">Panel heading without title</div>
                <div class="panel-body">
                    Basic panel example
                </div>
                <div class="panel-footer">Panel footer</div>
            </div>

            <br>

            <div class="panel panel-default box-border">
                <div class="panel-heading">Panel heading without title</div>
                <div class="panel-body">
                    Basic panel example
                </div>
                <div class="panel-footer">Panel footer</div>
            </div>
        </div>

        <h1>Checkbox and radio</h1>

        <h2>Checkbox</h2>

        <p>Supports bootstrap brand colors: <code>.checkbox-primary</code>, <code>.checkbox-info</code> etc.</p>

        <div class="example-block">
            <div class="row">
                <div class="col-xs-6">

                    <div class="checkbox">
                        <input id="checkbox1" type="checkbox">
                        <label for="checkbox1">
                            Default
                        </label>
                    </div>

                    <div class="checkbox checkbox-primary">
                        <input id="checkbox2" type="checkbox" checked="">
                        <label for="checkbox2">
                            Primary
                        </label>
                    </div>
                    <div class="checkbox checkbox-success">
                        <input id="checkbox3" type="checkbox">
                        <label for="checkbox3">
                            Success
                        </label>
                    </div>
                    <div class="checkbox checkbox-info">
                        <input id="checkbox4" type="checkbox">
                        <label for="checkbox4">
                            Info
                        </label>
                    </div>
                    <div class="checkbox checkbox-warning">
                        <input id="checkbox5" type="checkbox" checked="">
                        <label for="checkbox5">
                            Warning
                        </label>
                    </div>
                    <div class="checkbox checkbox-danger">
                        <input id="checkbox6" type="checkbox" checked="">
                        <label for="checkbox6">
                            Check me out
                        </label>
                    </div>

                    <br>

                    <p>Checkboxes without label text</p>
                    <div class="checkbox">
                        <input type="checkbox" id="singleCheckbox1" value="option1" aria-label="Single checkbox One">
                        <label></label>
                    </div>
                    <div class="checkbox checkbox-primary">
                        <input type="checkbox" id="singleCheckbox2" value="option2" checked="" aria-label="Single checkbox Two">
                        <label></label>
                    </div>

                    <br>

                    <p>Inline checkboxes</p>
                    <div class="checkbox checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" value="option1">
                        <label for="inlineCheckbox1"> Inline One </label>
                    </div>
                    <div class="checkbox checkbox-success checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox2" value="option1" checked="">
                        <label for="inlineCheckbox2"> Inline Two </label>
                    </div>
                    <div class="checkbox checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox3" value="option1">
                        <label for="inlineCheckbox3"> Inline Three </label>
                    </div>

                </div>
                <div class="col-xs-6">
                    <p>
                        Disabled state also supported.
                    </p>
                    <div class="checkbox">
                        <input id="checkbox9" type="checkbox" disabled="">
                        <label for="checkbox9">
                            Can't check this
                        </label>
                    </div>
                    <div class="checkbox checkbox-success">
                        <input id="checkbox10" type="checkbox" disabled="" checked="">
                        <label for="checkbox10">
                            This too
                        </label>
                    </div>
                    <div class="checkbox checkbox-warning checkbox-circle">
                        <input id="checkbox11" type="checkbox" disabled="" checked="">
                        <label for="checkbox11">
                            And this
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <h2>Radio</h2>

        <div class="example-block">
            <div class="radio">
                <input type="radio" name="radio1" id="radio1" value="option1" checked="">
                <label for="radio1">
                    Variant 1
                </label>
            </div>
            <div class="radio radio-primary">
                <input type="radio" name="radio1" id="radio2" value="option2">
                <label for="radio2">
                    Variant 2
                </label>
            </div>
            <div class="radio radio-success">
                <input type="radio" name="radio1" id="radio3" value="option2">
                <label for="radio3">
                    Variant 3
                </label>
            </div>
            <div class="radio radio-info">
                <input type="radio" name="radio1" id="radio4" value="option2">
                <label for="radio4">
                    Variant 4
                </label>
            </div>
            <div class="radio radio-warning">
                <input type="radio" name="radio1" id="radio5" value="option2">
                <label for="radio5">
                    Variant 5
                </label>
            </div>
            <div class="radio radio-danger">
                <input type="radio" name="radio1" id="radio6" value="option2">
                <label for="radio6">
                    Variant 6
                </label>
            </div>
        </div>

        <h2>Switch (checkbox)</h2>

        <div class="example-block">
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary active">
                    <input type="checkbox" checked="checked" autocomplete="off">
                    Checkbox 1 (pre-checked)
                </label>
                <label class="btn btn-primary">
                    <input type="checkbox" autocomplete="off">
                    Checkbox 2
                </label>
                <label class="btn btn-primary">
                    <input type="checkbox" autocomplete="off">
                    Checkbox 3
                </label>
            </div>
        </div>

        <h2>Switch (radio)</h2>

        <div class="example-block">
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary active">
                    <input type="radio" name="options" id="option1" autocomplete="off" checked="checked">
                    Radio 1 (preselected)
                </label>
                <label class="btn btn-primary">
                    <input type="radio" name="options" id="option2" autocomplete="off">
                    Radio 2
                </label>
                <label class="btn btn-primary">
                    <input type="radio" name="options" id="option3" autocomplete="off">
                    Radio 3
                </label>
            </div>

            <h3>Price switch</h3>

            <div>
                Price per
                <div class="btn-group btn-switcher" data-toggle="buttons">
                    <label class="btn btn-default active">
                        <input type="radio" name="optionsPrice" id="optionsPrice" autocomplete="off" checked="checked">
                        oz
                    </label>
                    <label class="btn btn-default">
                        <input type="radio" name="optionsPrice" id="optionsPrice" autocomplete="off">
                        g
                    </label>
                </div>
            </div>

            <h3>Toggle switch (checkbox)</h3>

            <h4>Sizes — default, <code>.checkbox-switch--sm</code>, <code>.checkbox-switch--xs</code></h4>
            <label class="checkbox-switch">
                <input type="checkbox">
                <span class="slider"></span>
            </label>

            <label class="checkbox-switch checkbox-switch--sm">
                <input type="checkbox">
                <span class="slider"></span>
            </label>

            <label class="checkbox-switch checkbox-switch--xs">
                <input type="checkbox">
                <span class="slider"></span>
            </label>

            <h4>With text label</h4>
            <label class="checkbox-switch">
                <input type="checkbox">
                <span class="slider"></span>
                <span class="text">Label text</span>
            </label>

            <label class="checkbox-switch checkbox-switch--sm">
                <input type="checkbox">
                <span class="slider"></span>
                <span class="text">Label text</span>
            </label>

            <label class="checkbox-switch checkbox-switch--xs">
                <input type="checkbox">
                <span class="slider"></span>
                <span class="text">Label text</span>
            </label>

            <h4>Colors — default, <code>.checkbox-switch--success</code>, <code>.checkbox-switch--danger</code></h4>
            <label class="checkbox-switch">
                <input type="checkbox">
                <span class="slider"></span>
            </label>

            <label class="checkbox-switch checkbox-switch--success">
                <input type="checkbox">
                <span class="slider"></span>
            </label>

            <label class="checkbox-switch checkbox-switch--danger">
                <input type="checkbox">
                <span class="slider"></span>
            </label>
        </div>

        <h1>Progress</h1>

        <div class="row">
            <div class="col-xs-12">
                <div class="example-block">

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%">
                            <span class="sr-only">60% Complete</span>
                        </div>
                    </div>

                    <div class="progress"> <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%"> 60% </div> </div>

                    <div class="progress"> <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"> <span class="sr-only">40% Complete (success)</span> </div> </div> <div class="progress"> <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%"> <span class="sr-only">20% Complete</span> </div> </div> <div class="progress"> <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%"> <span class="sr-only">60% Complete (warning)</span> </div> </div> <div class="progress"> <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%"> <span class="sr-only">80% Complete (danger)</span> </div> </div>

                    <h2>Striped</h2>

                    <div class="progress"> <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%"> <span class="sr-only">40% Complete (success)</span> </div> </div> <div class="progress"> <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%"> <span class="sr-only">20% Complete</span> </div> </div> <div class="progress"> <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%"> <span class="sr-only">60% Complete (warning)</span> </div> </div> <div class="progress"> <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%"> <span class="sr-only">80% Complete (danger)</span> </div> </div>

                    <h2>Animated</h2>

                    <div class="progress"> <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width:45%"><span class="sr-only">45% Complete</span></div> </div>

                    <h2>Stacked</h2>

                    <div class="progress"> <div class="progress-bar progress-bar-success" style="width:35%"> <span class="sr-only">35% Complete (success)</span> </div> <div class="progress-bar progress-bar-warning progress-bar-striped" style="width:20%"> <span class="sr-only">20% Complete (warning)</span> </div> <div class="progress-bar progress-bar-danger" style="width:10%"> <span class="sr-only">10% Complete (danger)</span> </div> </div>

                </div>

            </div>
        </div>

        <h1>Forms</h1>

        <div class="row">
            <div class="col-lg-6">
                <form class="form-horizontal">
                        <fieldset>
                            <legend>Legend</legend>
                            <div class="form-group">
                                <label for="inputEmail" class="col-lg-3 control-label">Email</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-lg-3 control-label">Password</label>
                                <div class="col-lg-9">
                                    <input type="password" class="form-control" id="inputPassword" placeholder="Password">

                                    <div class="checkbox">
                                        <input id="checkbox21" type="checkbox">
                                        <label for="checkbox21">
                                            Checkbox
                                        </label>
                                    </div>

                                    <div class="checkbox checkbox-danger">
                                        <input id="checkbox22" type="checkbox">
                                        <label for="checkbox22">
                                            Check me out
                                        </label>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="textArea" class="col-lg-3 control-label">Textarea</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" rows="3" id="textArea"></textarea>
                                    <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Radios</label>
                                <div class="col-lg-9">
                                    <div class="radio">
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                                        <label for="optionsRadios1">
                                            Option one is this
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        <label for="optionsRadios2">
                                            Option two can be something else
                                        </label>
                                    </div>
                                    <div class="radio radio-danger">
                                        <input type="radio" name="optionsRadios" id="optionsRadios3" value="option1" checked="">
                                        <label for="optionsRadios3">
                                            Option one is this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="select" class="col-lg-3 control-label">Selects</label>
                                <div class="col-lg-9">
                                    <select class="form-control" id="select">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                    <br>
                                    <select multiple="" class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-9 col-lg-offset-3">
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
            </div>
            <div class="col-lg-5 col-lg-offset-1">

                <form>
                    <div class="form-group">
                        <label class="control-label" for="focusedInput">Focused input</label>
                        <input class="form-control" id="focusedInput" type="text" value="This is focused...">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="disabledInput">Disabled input</label>
                        <input class="form-control" id="disabledInput" type="text" placeholder="Disabled input here..." disabled="">
                    </div>

                    <div class="form-group has-warning">
                        <label class="control-label" for="inputWarning">Input warning</label>
                        <input type="text" class="form-control" id="inputWarning">
                    </div>

                    <div class="form-group has-error">
                        <label class="control-label" for="inputError">Input error</label>
                        <input type="text" class="form-control" id="inputError">
                    </div>

                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Input success</label>
                        <input type="text" class="form-control" id="inputSuccess">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputLarge">Large input</label>
                        <input class="form-control input-lg" type="text" id="inputLarge">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputDefault">Default input</label>
                        <input type="text" class="form-control" id="inputDefault">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputSmall">Small input</label>
                        <input class="form-control input-sm" type="text" id="inputSmall">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Input addons</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Button</button>
                    </span>
                        </div>
                    </div>
                </form>

            </div>
        </div>

        <h2>Custom horizontal form table</h2>

        <p><code>.form-table</code> custom horizontal form used for label-input blocks layout.</p>

        <div class="example-block">

            <table class="form-table">
                <tr class="form-table__row">
                    <td class="form-table__label">
                        <label class="control-label"><?= _t('front.user', 'Year Established')?></label>
                    </td>
                    <td class="form-table__data">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="" autocomplete="off">
                        </div>
                    </td>
                </tr>
                <tr class="form-table__row">
                    <td class="form-table__label">
                        Just text
                    </td>
                    <td class="form-table__data">
                        <input type="text" class="form-control" placeholder="" autocomplete="off">
                    </td>
                </tr>
                <tr class="form-table__row">
                    <td class="form-table__label">
                        <b>Bold text</b>
                    </td>
                    <td class="form-table__data">
                        <input type="text" class="form-control" placeholder="" autocomplete="off">
                    </td>
                </tr>
                <tr class="form-table__row">
                    <td class="form-table__label">
                        Dropdown
                    </td>
                    <td class="form-table__data">
                        <div class="col-sm-4">
                            <select class="form-control">
                                <option value="">Var Odin</option>
                                <option value="">Var Dva</option>
                                <option value="">Var Tree</option>
                            </select>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <h1>Nav</h1>

        <h2>Breadcrumbs</h2>

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Library</a></li>
            <li class="active">Data</li>
        </ol>

        <div class="model-breadcrumbs">
            <a href="#" class="model-breadcrumbs__item">3D Models</a>
            <span class="tsi tsi-right model-breadcrumbs__item"></span>
            <a href="#2" class="model-breadcrumbs__item">Home Goods</a>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h2 id="nav-tabs">Base tabbed nav</h2>
                <p>Used for main layout navigation</p>
                <div class="bs-component">
                    <div class="over-nav-tabs-header">
                        <div class="container">
                            <h1>Printer Details</h1>
                        </div>
                    </div>
                    <div class="nav-tabs__container">
                        <div class="container">
                            <ul id="tabs" class="nav nav-tabs">
                                <li><a href="#first">Printer</a></li>
                                <li class="active"><a href="#second">Material Options</a></li>
                                <li><a href="#third">Delivery Options</a></li>
                                <li><a href="#4th">Certification</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h2 id="nav-tabs">Complicated nav block</h2>
                <p>Used for complicated nav. Just duplicate whole <code>.nav-tabs__container</code> block.</p>
                <div class="bs-component">
                    <div class="over-nav-tabs-header">
                        <div class="container">
                            <h1>Profile</h1>
                        </div>
                    </div>
                    <div class="nav-tabs__container">
                        <div class="container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#1st">Profile</a></li>
                                <li><a href="#2nd">Delivery</a></li>
                                <li><a href="#3rd">Earnings</a></li>
                                <li><a href="#4th">Taxes</a></li>
                                <li><a href="#5th">Settings</a></li>
                                <li><a href="#6">Notifications</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="nav-tabs__container">
                        <div class="container">
                            <ul id="tabs" class="nav nav-tabs">
                                <li class="active"><a href="#first">First block</a></li>
                                <li><a href="#second">Second block</a></li>
                                <li><a href="#third">Third block</a></li>
                                <li><a href="#4th">Fourth block</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h2 id="nav-tabs">Secondary nav block</h2>
                <p>Used for secondary nav. Add <code>.nav-tabs--secondary</code> to <code>.nav-tabs</code> block.</p>
                <div class="bs-component">
                    <ul class="nav nav-tabs nav-tabs--secondary" role="tablist">
                        <li role="presentation" class="active"><a href="#widget-store" aria-controls="widget-store" role="tab" data-toggle="tab" aria-expanded="true">Store Widget</a></li>
                        <li role="presentation" class=""><a href="#widget-model" aria-controls="widget-model" role="tab" data-toggle="tab" aria-expanded="false">Model Widget</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <h2 id="pagination">Pagination</h2>
                <div class="bs-component">
                    <ul class="pagination">
                        <li class="disabled"><a href="#"><span class="tsi tsi-left"></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><span class="tsi tsi-right"></span></a></li>
                    </ul>

                    <br>

                    <ul class="pagination pagination-lg">
                        <li class="disabled"><a href="#"><span class="tsi tsi-left"></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><span class="tsi tsi-right"></span></a></li>
                    </ul>

                    <br>

                    <ul class="pagination pagination-sm">
                        <li class="disabled"><a href="#"><span class="tsi tsi-left"></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><span class="tsi tsi-right"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6">
                <h2 id="pager">Pager</h2>
                <div class="bs-component">
                    <ul class="pager">
                        <li><a href="#">Previous</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>

                    <ul class="pager">
                        <li class="previous disabled"><a href="#">← Older</a></li>
                        <li class="next"><a href="#">Newer →</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <h1>Panels</h1>

        <div class="row">
            <div class="col-lg-4">
                <div class="bs-component">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            Basic panel
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Panel heading</div>
                        <div class="panel-body">
                            Panel content
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            Panel content
                        </div>
                        <div class="panel-footer">Panel footer</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="bs-component">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel primary</h3>
                        </div>
                        <div class="panel-body">
                            Panel content
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel success</h3>
                        </div>
                        <div class="panel-body">
                            Panel content
                        </div>
                    </div>

                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel warning</h3>
                        </div>
                        <div class="panel-body">
                            Panel content
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="bs-component">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel danger</h3>
                        </div>
                        <div class="panel-body">
                            Panel content
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel info</h3>
                        </div>
                        <div class="panel-body">
                            Panel content
                        </div>
                    </div>
                    <div id="source-button" class="btn btn-primary btn-xs" style="display: none;">&lt; &gt;</div></div>
            </div>
        </div>


        <h1>Modals</h1>

        <div class="row">
            <div class="col-lg-6">
                <div class="bs-component">
                    <div class="modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 class="modal-title">Modal title</h3>
                                </div>
                                <div class="modal-body">
                                    <p>One fine body…</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#demoModal">
                    Show demo modal
                </button>

                <!-- Modal -->
                <div class="modal fade" id="demoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">Demo modal title</h3>
                            </div>
                            <div class="modal-body">
                                <h2 class="m-t10">Text in a modal</h2>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>

                                <h3>Popover in a modal</h3>
                                <p>This <a href="#" role="button" class="btn btn-default" data-toggle="popover" title="" data-content="And here's some amazing content. It's very engaging. right?" data-original-title="A Title">button</a> should trigger a popover on click.</p>

                                <h3>Tooltips in a modal</h3>
                                <p><a href="#" data-toggle="tooltip" title="" data-original-title="Tooltip">This link</a> and <a href="#" data-toggle="tooltip" title="" data-original-title="Tooltip">that link</a> should have tooltips on hover.</p>

                                <hr>

                                <h3>Overflowing text to show scroll behavior</h3>
                                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                                <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <p>Add <code>modal-lg</code> to <code>modal-dialog</code> class</p>

                <!-- Large modal -->
                <button type="button" class="btn btn-default" data-toggle="modal" data-target=".example-modal-lg">Large modal</button>

                <div class="modal fade example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">Large modal</h3>
                            </div>
                            <div class="modal-body">
                                <h2 class="m-t0">Text in a modal</h2>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <p>Add <code>modal-sm</code> to <code>modal-dialog</code> class</p>
                <!-- Small modal -->
                <button type="button" class="btn btn-default" data-toggle="modal" data-target=".example-modal-sm">Small modal</button>

                <div class="modal fade example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">Small modal</h3>
                            </div>
                            <div class="modal-body">
                                <h2 class="m-t0">Text in a modal</h2>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <p>Primary modal styling modal header. Add <code>.modal-primary</code> to <code>.modal</code> class</p>
                <!-- Small modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".example-modal-primary">Primary modal</button>

                <div class="modal modal-primary fade example-modal-primary" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">Primary modal</h3>
                            </div>
                            <div class="modal-body">
                                <h2 class="m-t0">Text in a modal</h2>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <p>Danger modal styling modal header. Add <code>.modal-success</code> to <code>.modal</code> class</p>
                <!-- Small modal -->
                <button type="button" class="btn btn-success" data-toggle="modal" data-target=".example-modal-success">Success modal</button>

                <div class="modal modal-success fade example-modal-success" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">Success modal</h3>
                            </div>
                            <div class="modal-body">
                                <h2 class="m-t0">Text in a modal</h2>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <p>Danger modal styling modal header. Add <code>.modal-danger</code> to <code>.modal</code> class</p>
                <!-- Small modal -->
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".example-modal-danger">Danger modal</button>

                <div class="modal modal-danger fade example-modal-danger" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">Danger modal</h3>
                            </div>
                            <div class="modal-body">
                                <h2 class="m-t0">Text in a modal</h2>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <p>Warning modal styling modal header. Add <code>.modal-warning</code> to <code>.modal</code> class</p>
                <!-- Small modal -->
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".example-modal-warning">Warning modal</button>

                <div class="modal modal-warning fade example-modal-warning" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">Warning modal</h3>
                            </div>
                            <div class="modal-body">
                                <h2 class="m-t0">Text in a modal</h2>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <hr>

                <br>

                <p>Printer deadline settings modal</p>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalDeadline">
                    Deadline for a printer
                </button>

                <!-- Modal -->
                <div class="modal modal-primary fade" id="modalDeadline" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Set new deadline for printing</h4>
                            </div>
                            <form>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="inputDate">Date:</label>
                                                <input type="text" class="form-control" id="inputDate" placeholder="12-12-2015">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="inputTime">Time:</label>
                                                <input type="text" class="form-control" id="inputTime" placeholder="12:30">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <label for="inputComments">Comments</label>
                                            <textarea class="form-control" id="inputComments" placeholder="Comments"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <br>

                <hr>

                <p>Picture load preview modal</p>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPictureload">
                    Load picture
                </button>

                <!-- Modal -->
                <div class="modal modal-primary fade" id="modalPictureload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Picture load preview</h4>
                            </div>
                            <form>
                                <div class="modal-body">
                                    <p>Upload a example picture for</p>
                                    <p>ABS material
                                        <span title="Full color gypsum" class="btn btn-sm ts-material-color composite m-l20" style="background-color: rgb(0,255,0)"></span>
                                        Green color
                                    </p>

                                    <div class="text-center ps-upload-photo ps-upload-photo--modal-sm">
                                        <div id="psImagePreview"></div>
                                        <div id="dropYourPhoto" class="dz-clickable">
                                            <div>
                                                <span class="drop-photo-icon">
                                                    <span class="tsi tsi-upload-l"></span>
                                                </span>
                                                Browse
                                            </div>
                                            or drop your<br>
                                            logo here
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button type="button" class="btn btn-danger">Upload</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <br>
                <br>

            </div>

            <div class="col-lg-6">
                <h2>Popovers</h2>
                <div class="bs-component">
                    <button type="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-original-title="" title="">Left</button>

                    <button type="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-original-title="" title="">Top</button>

                    <button type="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus
              sagittis lacus vel augue laoreet rutrum faucibus." data-original-title="" title="">Bottom</button>

                    <button type="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-original-title="" title="">Right</button>
                </div>

                <h2>Popover on hover with HTML content</h2>
                <p>For HOVER effect add <code>data-trigger="hover focus"</code> or with an initialization option <code>$("#popover").popover({ trigger: "hover" });</code></p>
                <p>For HTML content add <code>data-html="true"</code> and <code>data-content="&lt;div&gt;&lt;b&gt;Popover Example&lt;/b&gt; 1 - Content&lt;/div&gt;" "title="Popover Example &lt;b&gt;1&lt;/b&gt; - Title"</code></p>
                <br>
                <div class="bs-component">
                    <button type="button" class="btn btn-default" data-container="body" data-toggle="popover" data-trigger="hover focus" data-html="true"  data-placement="right" data-content="<div><b>Some Content</b><br><a href='#'>Link</a></div>" title="<i>Popover Title <b>1</b></i>" data-original-title="Test">Popover on Hover</button>
                </div>

                <h2>Tooltips</h2>
                <div class="bs-component">
                    <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">Left</button>

                    <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tooltip on top">Top</button>

                    <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Tooltip on bottom">Bottom</button>

                    <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="" data-original-title="Tooltip on right">Right</button>
                </div>
            </div>
        </div>

        <h1>Preloader</h1>

        <p>Use svg image to get preloader. Has 2 versions. Use v1 as primary. <strong>This animated SVG's don't work on IE and FF.</strong></p>

        <div class="example-block">
            <h3>Preloader GIF (use only on white backgrounds)</h3>

            <image src="/static/images/preloader.gif" width="60" height="60"/>

            <h3>Preloader SVG v1</h3>

            <svg width="60" height="60">
                <image xlink:href="/static/images/preloader.svg" src="/static/images/preloader.gif" width="60" height="60"/>
            </svg>

            <h3>Preloader SVG v2. Dashed.</h3>

            <svg width="60" height="60">
                <image xlink:href="/static/images/preloader2.svg" src="/static/images/preloader.gif" width="60" height="60"/>
            </svg>
        </div>


        <h1>Rating Stars</h1>

        <p>Used <a href="https://github.com/kartik-v/bootstrap-star-rating">https://github.com/kartik-v/bootstrap-star-rating</a></p>

        <div class="row">
            <div class="col-md-12">
                <input class="star-rating star-rating--uikit" min="0" max="5" step="1.0" data-symbol="&#xe118;" data-glyphicon="false" data-rating-class="rating-fa">

                <br>

                <input type="number" class="star-rating star-rating--uikit" min="0" max="5" step="0.5" data-size="xl"
                       data-symbol="&#xe005;" data-default-caption="{rating} hearts" data-star-captions="{}">

                <hr>

                <input id="input-2d" class="rating" min="0" max="5" step="0.5" data-size="sm"
                       data-symbol="&#xf0fc;" data-glyphicon="false" data-rating-class="rating-fa" data-default-caption="{rating} drinks" data-star-captions="{}">
                <hr>
                <input value="0" type="number" class="star-rating star-rating--uikit" data-symbol="*" min=0 max=5 step=0.5 data-size="xl" >
                <hr>
                <p>Sizes</p>
                <input value="0" type="number" class="star-rating star-rating--uikit" min=0 max=8 step=0.5 data-size="xl" data-stars="8">
                <hr>
                <input value="4" type="number" class="star-rating star-rating--uikit" min=0 max=5 step=0.2 data-size="lg">
                <hr>
                <input value="0" type="number" class="star-rating star-rating--uikit" min=0 max=5 step=0.1 data-size="md" >
                <hr>
                <input value="2" type="number" class="star-rating star-rating--uikit" min=0 max=5 step=0.5 data-size="sm">
                <hr>
                <p>Minimal size. used on site.</p>
                <input value="0" type="number" class="star-rating star-rating--uikit" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea4b;" data-glyphicon="false" data-rating-class="tsi">
                
                <p>Readonly version with attribute <code>data-readonly="true"</code>.</p>
                <input value="4" type="number" class="star-rating star-rating--uikit" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea4b;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">

            </div>
        </div>

        <h1>Select 2 styling</h1>

        <div class="row">
            <div class="col-md-6">

                <p>Small size</p>

                <?php
                    echo kartik\select2\Select2::widget([
                    'name' => 'state_2',
                    'value' => '1',
                    'data' => [1,2,3],
                    'size' => kartik\select2\Select2::SMALL,
                    'options' => ['multiple' => false, 'placeholder' => 'Select states ...']
                    ]);
                ?>

                <br>

                <p>Normal Size</p>

                <?php
                echo kartik\select2\Select2::widget([
                    'name' => 'state_3',
                    'value' => '1',
                    'data' => [1,2,3],
                    'options' => ['multiple' => false, 'placeholder' => 'Select states ...']
                ]);
                ?>

                <br>

                <p>Large size</p>

                <?php
                echo kartik\select2\Select2::widget([
                    'name' => 'state_4',
                    'value' => '1',
                    'data' => [1,2,3],
                    'size' => kartik\select2\Select2::LARGE,
                    'options' => ['multiple' => false, 'placeholder' => 'Select states ...']
                ]);
                ?>
            </div>

            <div class="col-md-6">

                <p>Small multiple</p>

                <?php
                echo kartik\select2\Select2::widget([
                    'name' => 'state_5',
                    'value' => '1',
                    'data' => [1,2,3],
                    'size' => kartik\select2\Select2::SMALL,
                    'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
                ]);
                ?>

                <br>

                <p>Normal multiple</p>

                <?php
                echo kartik\select2\Select2::widget([
                    'name' => 'state_6',
                    'value' => '1',
                    'data' => [1,2,3],
                    'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
                ]);
                ?>

                <br>

                <p>Large multiple</p>

                <?php
                echo kartik\select2\Select2::widget([
                    'name' => 'state_7',
                    'value' => '1',
                    'data' => [1,2,3],
                    'size' => kartik\select2\Select2::LARGE,
                    'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
                ]);
                ?>
            </div>
        </div>

        <h1>Responsive columns — responsive-container</h1>
        <p>Use this columns, when col-*-* has some troubles. Use classes: <code>.responsive-container-list</code> and <code>.responsive-container</code>.</p>
        <p>Works like <code>class="col-xs-12 col-sm-4 col-md-3"</code> Watch example.</p>

        <style>
            .example-block .example-panel {
                background-color: #f4f7fa;
                padding: 0 10px;
                border-radius: 10px;
                overflow: hidden;
                margin-bottom: 20px;
            }
        </style>

        <div class="example-block">
            <div class="responsive-container-list">

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>Bigger content to make high block</p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <h4>Use <code>.responsive-container-list--3</code> to make 3 row on full screen and 2 rows on <992px resolutions.</h4>
        <p>Works like <code>class="col-xs-12 col-sm-6 col-md-4"</code></p>

        <div class="example-block">
            <div class="responsive-container-list responsive-container-list--3">

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>Bigger content to make high block</p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>

                <div class="responsive-container">
                    <div class="example-panel">
                        <h4>responsive-container</h4>

                        <p>
                            Some content text
                        </p>

                        <p>
                            <a class="btn btn-danger btn-ghost" href="#"><span class="tsi tsi-bin"></span> Button</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <h1>Flexbox grid</h1>
        <p>Use <code>.fb-grid</code> for <code>.row</code> elements and <code>.fb-grid__item</code> for <code>.col-*-*</code> elements</p>

        <div class="example-block">
            <div class="row fb-grid">
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 150px;margin-bottom: 20px;">height 150px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 200px;margin-bottom: 20px;">height 200px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 100px;margin-bottom: 20px;">height 100px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 100px;margin-bottom: 20px;">height 100px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 150px;margin-bottom: 20px;">height 150px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 200px;margin-bottom: 20px;">height 200px block</div>
                </div>
            </div>
        </div>

        <h2>Flexbox aligning</h2>
        <h3>Top aligning</h3>
        <p>Use <code>.fb-grid--va-top</code> for <code>.fb-grid</code> block. <strong>Default option</strong>.</p>

        <div class="example-block">
            <div class="row fb-grid fb-grid--va-top">
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 100px;margin-bottom: 20px;">height 100px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 150px;margin-bottom: 20px;">height 150px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 100px;margin-bottom: 20px;">height 100px block</div>
                </div>
            </div>
        </div>

        <h3>Center aligning</h3>
        <p>Use <code>.fb-grid--va-center</code> for <code>.fb-grid</code> block.</p>

        <div class="example-block">
            <div class="row fb-grid fb-grid--va-center">
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 100px;margin-bottom: 20px;">height 100px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 150px;margin-bottom: 20px;">height 150px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 100px;margin-bottom: 20px;">height 100px block</div>
                </div>
            </div>
        </div>

        <h3>Bottom aligning</h3>
        <p>Use <code>.fb-grid--va-bottom</code> for <code>.fb-grid</code> block.</p>

        <div class="example-block">
            <div class="row fb-grid fb-grid--va-bottom">
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 100px;margin-bottom: 20px;">height 100px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 150px;margin-bottom: 20px;">height 150px block</div>
                </div>
                <div class="fb-grid__item col-sm-4">
                    <div style="background:#eee;height: 100px;margin-bottom: 20px;">height 100px block</div>
                </div>
            </div>
        </div>

        <h1>2 column promo grid</h1>
        <p>Use this grid for promo information</p>

        <div class="example-block">
            <div class="promo-grid">
                <div class="promo-grid__row">
                    <div class="promo-grid__item">
                        <img class="promo-grid__pic" src="https://static.treatstock.com/static/uploads/ck_20170222_upload, print and earn!.png" alt="" style="width:100%; max-width:244px">
                    </div>
                    <div class="promo-grid__item">
                        <h2 class="m-t0">Upload, Print & Earn!</h2>
                        <p>Take your website to a whole new level by allowing visitors to upload models and printing them on Treatstock. Every order through the API will generate an additional source of income for your website!</p>
                    </div>
                </div>
                <div class="promo-grid__row">
                    <div class="promo-grid__item">
                        <img class="promo-grid__pic" src="https://static.treatstock.com/static/uploads/ck_20170222_Sell 3D printed products.png" alt="" style="width:100%; max-width:240px">
                    </div>
                    <div class="promo-grid__item">
                        <h2 class="m-t0">Sell 3D Printed Products</h2>
                        <p>If you have a web shop, stand out from the crowd with our API and sell your designs as printed products to customers who don’t own a 3D printer. Receive payment each time your model is printed on Treatstock!</p>
                    </div>
                </div>
                <div class="promo-grid__row">
                    <div class="promo-grid__item">
                        <img class="promo-grid__pic" src="https://static.treatstock.com/static/uploads/ck_20170221_compare prices.png" alt="" style="width:100%; max-width:230px">
                    </div>
                    <div class="promo-grid__item">
                        <h2 class="m-t0">Compare Prices</h2>
                        <p>Integrate our API to calculate and display prices for printing models uploaded on your website. Your users can select different materials and colors and compare prices without having to leave your site. Be rewarded each time a user prints a model on Treatstock!</p>
                    </div>
                </div>
            </div>
        </div>

        <h1>Picture slider</h1>
        <p>Common picture slider for blog, guides and other pages.</p>
        <p>Use Swiper and LightBox assets and init JS code before use it.</p>

        <div class="example-block">
            <div class="picture-slider swiper-container">
                <div class="swiper-wrapper">
                    <a class="picture-slider__item swiper-slide"
                        href="/static/images/cat-animals.png"
                        data-lightbox="picture-slider1">
                        <img src="/static/images/cat-animals.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-art.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-fashion.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-games.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-games.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-home.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-home.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-sport.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-sport.png" alt="">
                    </a>
                    <a class="picture-slider__item swiper-slide"
                       href="/static/images/cat-technology.png"
                       data-lightbox="picture-slider1">
                        <img src="/static/images/cat-technology.png" alt="">
                    </a>
                </div>
                <div class="picture-slider__scrollbar swiper-scrollbar"></div>
            </div>
        </div>
        <div class="code-block">
            <pre>
&lt;div class="picture-slider swiper-container"&gt;
    &lt;div class="swiper-wrapper"&gt;
        &lt;a class="picture-slider__item swiper-slide"
            href="bigPictureLink"
            data-lightbox="pictureSliderId"&gt;
            &lt;img src="smallPictureLink" alt="pictureDescription"&gt;
        &lt;/a&gt;
        &lt;a class="picture-slider__item swiper-slide"
            href="bigPictureLink"
            data-lightbox="pictureSliderId"&gt;
            &lt;img src="smallPictureLink" alt="pictureDescription"&gt;
        &lt;/a&gt;
    &lt;/div&gt;
    &lt;div class="picture-slider__scrollbar swiper-scrollbar"&gt;&lt;/div&gt;
&lt;/div&gt;</pre>

        <h4>JS code for init</h4>
        <pre>
//Init Picture Slider
var swiperPictureSlider = new Swiper('.picture-slider', {
    scrollbar: '.picture-slider__scrollbar',
    scrollbarHide: true,
    slidesPerView: 'auto',
    grabCursor: true
});</pre>
        </div>

        <h1><code>designer-card</code> block elements</h1>
        <p>Basic <code>designer-card</code> and <code>designer-card--ps-cat</code> for service card</p>

        <div class="example-block">
            <div class="row">
                <div class="col-sm-4">
                    <div class="designer-card">
                        <h2 class="designer-card__title">
                            <?= _t('site.ps', 'designer-card__title'); ?>
                        </h2>

                        <div class="designer-card__userinfo">
                            <a class="designer-card__avatar" href="https://www.treatstock.com/u/tomislav-veg">
                                <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/avatar_128x128.jpg" alt="" align="left">
                            </a>

                            <div class="designer-card__username">
                                designer-card__username
                            </div>
                        </div>

                        <div class="designer-card__models-list">
                            <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/753-leonardo-di-caprio" title="Leonardo  Di Caprio">
                                <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/model/7763_358x269.jpg" alt="Leonardo  Di Caprio">
                            </a>

                            <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/1763-donald-trump-sandwich-prick" title="Donald Trump Sandwich Prick">
                                <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/model/7787_358x269.jpg" alt="Donald Trump Sandwich Prick">
                            </a>

                            <a class="designer-card__model" href="https://www.treatstock.com/3d-printable-models/1764-hillary-clinton-sandwich-prick" title="Hillary Clinton Sandwich Prick">
                                <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/model/7791_358x269.jpg" alt="Hillary Clinton Sandwich Prick">
                            </a>
                        </div>

                        <div class="designer-card__about">
                            designer-card__about I'm self learned digital sculptor and 3D artist. Sculpting and designing models for 3D print
                        </div>

                        <div class="designer-card__btn-block">
                            <a class="btn btn-primary btn-ghost ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="https://www.treatstock.com/3d-printable-models-store/1453">Hire Designer</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="designer-card designer-card--ps-cat">
                        <div class="designer-card__ps-rating">
                            <style>
                                /* Generated by Rating Stars widget*/
                                .star-rating-block,
                                .star-rating-count {
                                    display: inline-block;
                                }
                                .star-rating-block {
                                    position: relative;
                                }
                                .star-rating-block .star-rating {
                                    position: relative;
                                    top: -2px;
                                    display: inline-block;
                                }
                                .star-rating-range {
                                    padding: 0 5px;
                                    font-size: 14px;
                                    font-weight: normal;
                                }
                                .star-rating-count {
                                    margin-left: 5px;
                                    font-size: 14px;
                                    color: #909498;
                                    font-weight: normal;
                                }
                            </style>
                            <div id="star-rating-block-67392" class="star-rating-block" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                <meta itemprop="ratingValue" content="4.8915662647">
                                <meta itemprop="worstRating" content="1">
                                <meta itemprop="bestRating" content="5">
                                <div class="star-rating rating-xs rating-disabled"><div class="rating-container tsi" data-content=""><div class="rating-stars" data-content="" style="width: 97.8313%;"></div><input value="4.8915662647" type="text" class="star-rating form-control hide" data-min="0" data-max="5" data-step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div></div>

                                <span class="star-rating-range"><strong>4.9</strong>/5</span>
                                <span class="star-rating-count">(<span itemprop="reviewCount">83</span> reviews)</span></div>

                        </div>

                        <div class="cert-label cert-label--pro" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This printer has a 'Professional Certificate' that confirms it is capable of printing complex models, with many small details, to a high quality.">
                            Pro        </div>

                        <div class="designer-card__userinfo">
                            <a class="designer-card__avatar" href="#" target="_blank"><img src="http://ts.vcap.me/static/user/0bf727e907c5fc9d5356f11e4c45d613/ps_logo_circle_1479155239_64x64.jpg"></a>
                            <h3 class="designer-card__username">
                                <a href="#" target="_blank" title="Laughing Monkey Labs LLC">
                                    Laughing Monkey Labs LLC                </a>
                            </h3>
                        </div>
                        <div class="designer-card__ps-pics">
                            <div class="designer-card__ps-portfolio swiper-container">
                                <div class="swiper-wrapper">
                                    <a class="designer-card__ps-portfolio-item swiper-slide" href="https://static.treatstock.com/static/files/4a/12/27123_2474_7e65cc2d0817213fd89f844662762220_720x540.jpg" data-lightbox="ps396"><img src="https://static.treatstock.com/static/files/4a/12/27123_2474_7e65cc2d0817213fd89f844662762220_160x90.jpg" alt="IMG_1898.JPG"></a><a class="designer-card__ps-portfolio-item swiper-slide" href="https://static.treatstock.com/static/files/72/56/27124_2474_fa693105d69411a841515d9e7a1a2ba8_720x540.jpg" data-lightbox="ps396"><img src="https://static.treatstock.com/static/files/72/56/27124_2474_fa693105d69411a841515d9e7a1a2ba8_160x90.jpg" alt="IMG_1908.JPG"></a><a class="designer-card__ps-portfolio-item swiper-slide" href="https://static.treatstock.com/static/files/53/d1/27126_2474_7dc16553790691562c7a3f4886917788_720x540.jpeg" data-lightbox="ps396"><img src="https://static.treatstock.com/static/files/53/d1/27126_2474_7dc16553790691562c7a3f4886917788_160x90.jpeg" alt="bowl.jpeg"></a>                        </div>
                                <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar"></div>
                            </div>
                        </div>
                        <div class="designer-card__ps-loc" title="Lawton, Michigan, US">
                            <span class="tsi tsi-map-marker"></span>
                            Lawton, Michigan, US                </div>

                        <div class="designer-card__about">
                            Your source for fast accurate 3D printed parts and prototypes. <a class="tsrm-toggle" href="#" style="display:inline">...Show more</a><span class="tsrm-content hide">

Experienced certified 3D design services available.</span>        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="designer-card__data">
                                    <span class="designer-card__data-label">Materials:</span>
                                    PLA, FLEX, Wood PLA                                    </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="designer-card__data">
                                    <span class="designer-card__data-label">Minimum Charge:</span>
                                    $4.50                                    </div>
                                <div class="designer-card__data">
                                    <span class="designer-card__data-label">Shipping:</span>
                                    Standard Flat Rate                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="designer-card__btn-block">
                                    <a class="btn btn-danger btn-ghost ts-ga" data-categoryga="PublicPs" data-actionga="PrintHere" href="#">
                                        <span class="tsi tsi-printer3d"></span>
                                        Buy Here                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <h1>Responsive video (16/9 ratio)</h1>
        <p>Wrap default YouTube <code>iframe</code> with special <code>&lt;div class="video-container"&gt;</code>. Example below.</p>

        <div class="example-block">
            <div class="row">
                <div class="col-sm-4 m-b30">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/CYijJ4rrktw?rel=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-3">
                    <div class="video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/YwaX8fNAeyo?rel=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="code-block">
            <pre>&lt;div class="video-container"&gt;
    &lt;iframe width="560" height="315" src="https://www.youtube.com/embed/CYijJ4rrktw?rel=0" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;
&lt;/div&gt;</pre>
        </div>

        <h1>Img CSS colorizing</h1>
        <p>Add specila classes like <code>.colorize-yellow</code> to <code>img</code> tag to colorize picture.</p>

        <style>
            .colorize-yellow {
                -webkit-filter: grayscale(100%) sepia(1) hue-rotate(0deg) saturate(4);
                filter:grayscale(100%) sepia(1) hue-rotate(0deg) saturate(4);
            }

            .colorize-green {
                -webkit-filter: grayscale(100%) sepia(1) hue-rotate(50deg) saturate(4);
                filter: grayscale(100%) sepia(1) hue-rotate(50deg) saturate(4);
            }

            .colorize-blue {
                -webkit-filter: grayscale(100%) sepia(1) hue-rotate(160deg) saturate(4);
                filter: grayscale(100%) sepia(1) hue-rotate(160deg) saturate(4);
            }

            .colorize-violet {
                -webkit-filter: grayscale(100%) sepia(1) hue-rotate(200deg) saturate(4);
                filter: grayscale(100%) sepia(1) hue-rotate(200deg) saturate(4);
            }

            .colorize-magenta {
                -webkit-filter: grayscale(100%) sepia(1) hue-rotate(260deg) saturate(4);
                filter: grayscale(100%) sepia(1) hue-rotate(260deg) saturate(4);
            }
            .colorize-red {
                -webkit-filter: grayscale(100%) sepia(1) hue-rotate(310deg) saturate(4);
                filter: grayscale(100%) sepia(1) hue-rotate(310deg) saturate(4);
            }

            .bs-example-css-colors img {
                width: 100%;
                margin-bottom: 30px;
            }
        </style>

        <div class="row bs-example-css-colors">
            <div class="col-sm-4">
                <p><strong>Original</strong></p>
                <img src="/static/images/1017_Silver.png">
            </div>
            <div class="col-sm-4">
                <p><code>.colorize-yellow</code></p>
                <img class="colorize-yellow" src="/static/images/1017_Silver.png">
            </div>
            <div class="col-sm-4">
                <p><code>.colorize-green</code></p>
                <img class="colorize-green" src="/static/images/1017_Silver.png">
            </div>
            <div class="col-sm-4">
                <p><code>.colorize-blue</code></p>
                <img class="colorize-blue" src="/static/images/1017_Silver.png">
            </div>
            <div class="col-sm-4">
                <p><code>.colorize-violet</code></p>
                <img class="colorize-violet" src="/static/images/1017_Silver.png">
            </div>
            <div class="col-sm-4">
                <p><code>.colorize-magenta</code></p>
                <img class="colorize-magenta" src="/static/images/1017_Silver.png">
            </div>
            <div class="col-sm-4">
                <p><code>.colorize-red</code></p>
                <img class="colorize-red" src="/static/images/1017_Silver.png">
            </div>
        </div>

        <h1>TS icons</h1>
        <p>Project icons</p>
        <ul class="icons-list">
            <li>
                <div class="tsi tsi-aim"></div>
                <input type="text" readonly="readonly" value="tsi tsi-aim">
            </li>
            <li>
                <div class="tsi tsi-aircraft"></div>
                <input type="text" readonly="readonly" value="tsi tsi-aircraft">
            </li>
            <li>
                <div class="tsi tsi-alarm-clock"></div>
                <input type="text" readonly="readonly" value="tsi tsi-alarm-clock">
            </li>
            <li>
                <div class="tsi tsi-album"></div>
                <input type="text" readonly="readonly" value="tsi tsi-album">
            </li>
            <li>
                <div class="tsi tsi-ambulance-car"></div>
                <input type="text" readonly="readonly" value="tsi tsi-ambulance-car">
            </li>
            <li>
                <div class="tsi tsi-anchor"></div>
                <input type="text" readonly="readonly" value="tsi tsi-anchor">
            </li>
            <li>
                <div class="tsi tsi-archive"></div>
                <input type="text" readonly="readonly" value="tsi tsi-archive">
            </li>
            <li>
                <div class="tsi tsi-archive-box"></div>
                <input type="text" readonly="readonly" value="tsi tsi-archive-box">
            </li>
            <li>
                <div class="tsi tsi-archive-data"></div>
                <input type="text" readonly="readonly" value="tsi tsi-archive-data">
            </li>
            <li>
                <div class="tsi tsi-archive-in"></div>
                <input type="text" readonly="readonly" value="tsi tsi-archive-in">
            </li>
            <li>
                <div class="tsi tsi-archive-out"></div>
                <input type="text" readonly="readonly" value="tsi tsi-archive-out">
            </li>
            <li>
                <div class="tsi tsi-arrow-back-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-back-l">
            </li>
            <li>
                <div class="tsi tsi-arrow-diagonal"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-diagonal">
            </li>
            <li>
                <div class="tsi tsi-arrow-down"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-down">
            </li>
            <li>
                <div class="tsi tsi-arrow-down-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-down-l">
            </li>
            <li>
                <div class="tsi tsi-arrow-forward-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-forward-l">
            </li>
            <li>
                <div class="tsi tsi-arrow-left"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-left">
            </li>
            <li>
                <div class="tsi tsi-arrow-left-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-left-l">
            </li>
            <li>
                <div class="tsi tsi-arrow-left-right"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-left-right">
            </li>
            <li>
                <div class="tsi tsi-arrow-left-right-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-left-right-l">
            </li>
            <li>
                <div class="tsi tsi-arrow-right"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-right">
            </li>
            <li>
                <div class="tsi tsi-arrow-right-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-right-l">
            </li>
            <li>
                <div class="tsi tsi-arrow-up"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-up">
            </li>
            <li>
                <div class="tsi tsi-arrow-up-down"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-up-down">
            </li>
            <li>
                <div class="tsi tsi-arrow-up-down-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-up-down-l">
            </li>
            <li>
                <div class="tsi tsi-arrow-up-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrow-up-l">
            </li>
            <li>
                <div class="tsi tsi-arrows-left-right"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrows-left-right">
            </li>
            <li>
                <div class="tsi tsi-arrows-right-left-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrows-right-left-l">
            </li>
            <li>
                <div class="tsi tsi-arrows-up-down"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrows-up-down">
            </li>
            <li>
                <div class="tsi tsi-arrows-up-down-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-arrows-up-down-l">
            </li>
            <li>
                <div class="tsi tsi-at"></div>
                <input type="text" readonly="readonly" value="tsi tsi-at">
            </li>
            <li>
                <div class="tsi tsi-atom"></div>
                <input type="text" readonly="readonly" value="tsi tsi-atom">
            </li>
            <li>
                <div class="tsi tsi-automobile"></div>
                <input type="text" readonly="readonly" value="tsi tsi-automobile">
            </li>
            <li>
                <div class="tsi tsi-awareness-ribbon"></div>
                <input type="text" readonly="readonly" value="tsi tsi-awareness-ribbon">
            </li>
            <li>
                <div class="tsi tsi-baby-bottle"></div>
                <input type="text" readonly="readonly" value="tsi tsi-baby-bottle">
            </li>
            <li>
                <div class="tsi tsi-backspace"></div>
                <input type="text" readonly="readonly" value="tsi tsi-backspace">
            </li>
            <li>
                <div class="tsi tsi-bag"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bag">
            </li>
            <li>
                <div class="tsi tsi-ban-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-ban-c">
            </li>
            <li>
                <div class="tsi tsi-bank"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bank">
            </li>
            <li>
                <div class="tsi tsi-basket"></div>
                <input type="text" readonly="readonly" value="tsi tsi-basket">
            </li>
            <li>
                <div class="tsi tsi-battery"></div>
                <input type="text" readonly="readonly" value="tsi tsi-battery">
            </li>
            <li>
                <div class="tsi tsi-battery-charge"></div>
                <input type="text" readonly="readonly" value="tsi tsi-battery-charge">
            </li>
            <li>
                <div class="tsi tsi-bed"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bed">
            </li>
            <li>
                <div class="tsi tsi-bicycle"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bicycle">
            </li>
            <li>
                <div class="tsi tsi-bin"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bin">
            </li>
            <li>
                <div class="tsi tsi-blank"></div>
                <input type="text" readonly="readonly" value="tsi tsi-blank">
            </li>
            <li>
                <div class="tsi tsi-blocks-1"></div>
                <input type="text" readonly="readonly" value="tsi tsi-blocks-1">
            </li>
            <li>
                <div class="tsi tsi-blocks-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-blocks-2">
            </li>
            <li>
                <div class="tsi tsi-blocks-3"></div>
                <input type="text" readonly="readonly" value="tsi tsi-blocks-3">
            </li>
            <li>
                <div class="tsi tsi-bluetooth"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bluetooth">
            </li>
            <li>
                <div class="tsi tsi-book-open"></div>
                <input type="text" readonly="readonly" value="tsi tsi-book-open">
            </li>
            <li>
                <div class="tsi tsi-bookmark"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bookmark">
            </li>
            <li>
                <div class="tsi tsi-bookmarks"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bookmarks">
            </li>
            <li>
                <div class="tsi tsi-bricks"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bricks">
            </li>
            <li>
                <div class="tsi tsi-briefcase"></div>
                <input type="text" readonly="readonly" value="tsi tsi-briefcase">
            </li>
            <li>
                <div class="tsi tsi-broadcast"></div>
                <input type="text" readonly="readonly" value="tsi tsi-broadcast">
            </li>
            <li>
                <div class="tsi tsi-browser"></div>
                <input type="text" readonly="readonly" value="tsi tsi-browser">
            </li>
            <li>
                <div class="tsi tsi-brush"></div>
                <input type="text" readonly="readonly" value="tsi tsi-brush">
            </li>
            <li>
                <div class="tsi tsi-bullhorn"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bullhorn">
            </li>
            <li>
                <div class="tsi tsi-bus"></div>
                <input type="text" readonly="readonly" value="tsi tsi-bus">
            </li>
            <li>
                <div class="tsi tsi-cabinet"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cabinet">
            </li>
            <li>
                <div class="tsi tsi-cable"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cable">
            </li>
            <li>
                <div class="tsi tsi-calculator"></div>
                <input type="text" readonly="readonly" value="tsi tsi-calculator">
            </li>
            <li>
                <div class="tsi tsi-calendar"></div>
                <input type="text" readonly="readonly" value="tsi tsi-calendar">
            </li>
            <li>
                <div class="tsi tsi-call"></div>
                <input type="text" readonly="readonly" value="tsi tsi-call">
            </li>
            <li>
                <div class="tsi tsi-call-ban"></div>
                <input type="text" readonly="readonly" value="tsi tsi-call-ban">
            </li>
            <li>
                <div class="tsi tsi-call-coonect"></div>
                <input type="text" readonly="readonly" value="tsi tsi-call-coonect">
            </li>
            <li>
                <div class="tsi tsi-call-end"></div>
                <input type="text" readonly="readonly" value="tsi tsi-call-end">
            </li>
            <li>
                <div class="tsi tsi-call-incoming"></div>
                <input type="text" readonly="readonly" value="tsi tsi-call-incoming">
            </li>
            <li>
                <div class="tsi tsi-call-list"></div>
                <input type="text" readonly="readonly" value="tsi tsi-call-list">
            </li>
            <li>
                <div class="tsi tsi-call-off"></div>
                <input type="text" readonly="readonly" value="tsi tsi-call-off">
            </li>
            <li>
                <div class="tsi tsi-call-outgoing"></div>
                <input type="text" readonly="readonly" value="tsi tsi-call-outgoing">
            </li>
            <li>
                <div class="tsi tsi-call-s"></div>
                <input type="text" readonly="readonly" value="tsi tsi-call-s">
            </li>
            <li>
                <div class="tsi tsi-calling"></div>
                <input type="text" readonly="readonly" value="tsi tsi-calling">
            </li>
            <li>
                <div class="tsi tsi-camera"></div>
                <input type="text" readonly="readonly" value="tsi tsi-camera">
            </li>
            <li>
                <div class="tsi tsi-cardiogram"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cardiogram">
            </li>
            <li>
                <div class="tsi tsi-cash"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cash">
            </li>
            <li>
                <div class="tsi tsi-cd"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cd">
            </li>
            <li>
                <div class="tsi tsi-cds"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cds">
            </li>
            <li>
                <div class="tsi tsi-certificate"></div>
                <input type="text" readonly="readonly" value="tsi tsi-certificate">
            </li>
            <li>
                <div class="tsi tsi-chart-1"></div>
                <input type="text" readonly="readonly" value="tsi tsi-chart-1">
            </li>
            <li>
                <div class="tsi tsi-chart-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-chart-2">
            </li>
            <li>
                <div class="tsi tsi-chat"></div>
                <input type="text" readonly="readonly" value="tsi tsi-chat">
            </li>
            <li>
                <div class="tsi tsi-check-list"></div>
                <input type="text" readonly="readonly" value="tsi tsi-check-list">
            </li>
            <li>
                <div class="tsi tsi-checkmark"></div>
                <input type="text" readonly="readonly" value="tsi tsi-checkmark">
            </li>
            <li>
                <div class="tsi tsi-checkmark-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-checkmark-c">
            </li>
            <li>
                <div class="tsi tsi-checkmark-s"></div>
                <input type="text" readonly="readonly" value="tsi tsi-checkmark-s">
            </li>
            <li>
                <div class="tsi tsi-chef-hat"></div>
                <input type="text" readonly="readonly" value="tsi tsi-chef-hat">
            </li>
            <li>
                <div class="tsi tsi-chip"></div>
                <input type="text" readonly="readonly" value="tsi tsi-chip">
            </li>
            <li>
                <div class="tsi tsi-clipboard"></div>
                <input type="text" readonly="readonly" value="tsi tsi-clipboard">
            </li>
            <li>
                <div class="tsi tsi-clipboard-checkmark"></div>
                <input type="text" readonly="readonly" value="tsi tsi-clipboard-checkmark">
            </li>
            <li>
                <div class="tsi tsi-clock"></div>
                <input type="text" readonly="readonly" value="tsi tsi-clock">
            </li>
            <li>
                <div class="tsi tsi-cloud"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cloud">
            </li>
            <li>
                <div class="tsi tsi-cloud-in"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cloud-in">
            </li>
            <li>
                <div class="tsi tsi-cloud-lock"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cloud-lock">
            </li>
            <li>
                <div class="tsi tsi-cloud-out"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cloud-out">
            </li>
            <li>
                <div class="tsi tsi-clover"></div>
                <input type="text" readonly="readonly" value="tsi tsi-clover">
            </li>
            <li>
                <div class="tsi tsi-coffee-cup"></div>
                <input type="text" readonly="readonly" value="tsi tsi-coffee-cup">
            </li>
            <li>
                <div class="tsi tsi-cogwheel"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cogwheel">
            </li>
            <li>
                <div class="tsi tsi-columns"></div>
                <input type="text" readonly="readonly" value="tsi tsi-columns">
            </li>
            <li>
                <div class="tsi tsi-comment"></div>
                <input type="text" readonly="readonly" value="tsi tsi-comment">
            </li>
            <li>
                <div class="tsi tsi-compass"></div>
                <input type="text" readonly="readonly" value="tsi tsi-compass">
            </li>
            <li>
                <div class="tsi tsi-components"></div>
                <input type="text" readonly="readonly" value="tsi tsi-components">
            </li>
            <li>
                <div class="tsi tsi-compress-s"></div>
                <input type="text" readonly="readonly" value="tsi tsi-compress-s">
            </li>
            <li>
                <div class="tsi tsi-copy"></div>
                <input type="text" readonly="readonly" value="tsi tsi-copy">
            </li>
            <li>
                <div class="tsi tsi-credit-card"></div>
                <input type="text" readonly="readonly" value="tsi tsi-credit-card">
            </li>
            <li>
                <div class="tsi tsi-crop"></div>
                <input type="text" readonly="readonly" value="tsi tsi-crop">
            </li>
            <li>
                <div class="tsi tsi-crown"></div>
                <input type="text" readonly="readonly" value="tsi tsi-crown">
            </li>
            <li>
                <div class="tsi tsi-cube"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cube">
            </li>
            <li>
                <div class="tsi tsi-cursor"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cursor">
            </li>
            <li>
                <div class="tsi tsi-cut"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cut">
            </li>
            <li>
                <div class="tsi tsi-cutlery"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cutlery">
            </li>
            <li>
                <div class="tsi tsi-database"></div>
                <input type="text" readonly="readonly" value="tsi tsi-database">
            </li>
            <li>
                <div class="tsi tsi-desktop"></div>
                <input type="text" readonly="readonly" value="tsi tsi-desktop">
            </li>
            <li>
                <div class="tsi tsi-diamond"></div>
                <input type="text" readonly="readonly" value="tsi tsi-diamond">
            </li>
            <li>
                <div class="tsi tsi-direction"></div>
                <input type="text" readonly="readonly" value="tsi tsi-direction">
            </li>
            <li>
                <div class="tsi tsi-direction-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-direction-2">
            </li>
            <li>
                <div class="tsi tsi-doc"></div>
                <input type="text" readonly="readonly" value="tsi tsi-doc">
            </li>
            <li>
                <div class="tsi tsi-down"></div>
                <input type="text" readonly="readonly" value="tsi tsi-down">
            </li>
            <li>
                <div class="tsi tsi-down-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-down-c">
            </li>
            <li>
                <div class="tsi tsi-download"></div>
                <input type="text" readonly="readonly" value="tsi tsi-download">
            </li>
            <li>
                <div class="tsi tsi-download-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-download-l">
            </li>
            <li>
                <div class="tsi tsi-drop"></div>
                <input type="text" readonly="readonly" value="tsi tsi-drop">
            </li>
            <li>
                <div class="tsi tsi-dropper"></div>
                <input type="text" readonly="readonly" value="tsi tsi-dropper">
            </li>
            <li>
                <div class="tsi tsi-duplicate"></div>
                <input type="text" readonly="readonly" value="tsi tsi-duplicate">
            </li>
            <li>
                <div class="tsi tsi-edit"></div>
                <input type="text" readonly="readonly" value="tsi tsi-edit">
            </li>
            <li>
                <div class="tsi tsi-eject"></div>
                <input type="text" readonly="readonly" value="tsi tsi-eject">
            </li>
            <li>
                <div class="tsi tsi-eraser"></div>
                <input type="text" readonly="readonly" value="tsi tsi-eraser">
            </li>
            <li>
                <div class="tsi tsi-expand-s"></div>
                <input type="text" readonly="readonly" value="tsi tsi-expand-s">
            </li>
            <li>
                <div class="tsi tsi-external"></div>
                <input type="text" readonly="readonly" value="tsi tsi-external">
            </li>
            <li>
                <div class="tsi tsi-eye"></div>
                <input type="text" readonly="readonly" value="tsi tsi-eye">
            </li>
            <li>
                <div class="tsi tsi-eye-off"></div>
                <input type="text" readonly="readonly" value="tsi tsi-eye-off">
            </li>
            <li>
                <div class="tsi tsi-fast-forward"></div>
                <input type="text" readonly="readonly" value="tsi tsi-fast-forward">
            </li>
            <li>
                <div class="tsi tsi-female"></div>
                <input type="text" readonly="readonly" value="tsi tsi-female">
            </li>
            <li>
                <div class="tsi tsi-film"></div>
                <input type="text" readonly="readonly" value="tsi tsi-film">
            </li>
            <li>
                <div class="tsi tsi-filter"></div>
                <input type="text" readonly="readonly" value="tsi tsi-filter">
            </li>
            <li>
                <div class="tsi tsi-flag"></div>
                <input type="text" readonly="readonly" value="tsi tsi-flag">
            </li>
            <li>
                <div class="tsi tsi-flag-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-flag-2">
            </li>
            <li>
                <div class="tsi tsi-flame"></div>
                <input type="text" readonly="readonly" value="tsi tsi-flame">
            </li>
            <li>
                <div class="tsi tsi-flashlight"></div>
                <input type="text" readonly="readonly" value="tsi tsi-flashlight">
            </li>
            <li>
                <div class="tsi tsi-flask"></div>
                <input type="text" readonly="readonly" value="tsi tsi-flask">
            </li>
            <li>
                <div class="tsi tsi-folder"></div>
                <input type="text" readonly="readonly" value="tsi tsi-folder">
            </li>
            <li>
                <div class="tsi tsi-folder-add"></div>
                <input type="text" readonly="readonly" value="tsi tsi-folder-add">
            </li>
            <li>
                <div class="tsi tsi-folder-delete"></div>
                <input type="text" readonly="readonly" value="tsi tsi-folder-delete">
            </li>
            <li>
                <div class="tsi tsi-folder-in"></div>
                <input type="text" readonly="readonly" value="tsi tsi-folder-in">
            </li>
            <li>
                <div class="tsi tsi-folder-lock"></div>
                <input type="text" readonly="readonly" value="tsi tsi-folder-lock">
            </li>
            <li>
                <div class="tsi tsi-folder-open"></div>
                <input type="text" readonly="readonly" value="tsi tsi-folder-open">
            </li>
            <li>
                <div class="tsi tsi-folder-out"></div>
                <input type="text" readonly="readonly" value="tsi tsi-folder-out">
            </li>
            <li>
                <div class="tsi tsi-frame"></div>
                <input type="text" readonly="readonly" value="tsi tsi-frame">
            </li>
            <li>
                <div class="tsi tsi-fuel-pump"></div>
                <input type="text" readonly="readonly" value="tsi tsi-fuel-pump">
            </li>
            <li>
                <div class="tsi tsi-gift"></div>
                <input type="text" readonly="readonly" value="tsi tsi-gift">
            </li>
            <li>
                <div class="tsi tsi-glass"></div>
                <input type="text" readonly="readonly" value="tsi tsi-glass">
            </li>
            <li>
                <div class="tsi tsi-globe"></div>
                <input type="text" readonly="readonly" value="tsi tsi-globe">
            </li>
            <li>
                <div class="tsi tsi-group"></div>
                <input type="text" readonly="readonly" value="tsi tsi-group">
            </li>
            <li>
                <div class="tsi tsi-guy-fawkes-mask"></div>
                <input type="text" readonly="readonly" value="tsi tsi-guy-fawkes-mask">
            </li>
            <li>
                <div class="tsi tsi-hat"></div>
                <input type="text" readonly="readonly" value="tsi tsi-hat">
            </li>
            <li>
                <div class="tsi tsi-hd"></div>
                <input type="text" readonly="readonly" value="tsi tsi-hd">
            </li>
            <li>
                <div class="tsi tsi-hdd"></div>
                <input type="text" readonly="readonly" value="tsi tsi-hdd">
            </li>
            <li>
                <div class="tsi tsi-headphones"></div>
                <input type="text" readonly="readonly" value="tsi tsi-headphones">
            </li>
            <li>
                <div class="tsi tsi-heart"></div>
                <input type="text" readonly="readonly" value="tsi tsi-heart">
            </li>
            <li>
                <div class="tsi tsi-helicopter"></div>
                <input type="text" readonly="readonly" value="tsi tsi-helicopter">
            </li>
            <li>
                <div class="tsi tsi-help"></div>
                <input type="text" readonly="readonly" value="tsi tsi-help">
            </li>
            <li>
                <div class="tsi tsi-history"></div>
                <input type="text" readonly="readonly" value="tsi tsi-history">
            </li>
            <li>
                <div class="tsi tsi-home"></div>
                <input type="text" readonly="readonly" value="tsi tsi-home">
            </li>
            <li>
                <div class="tsi tsi-hourglass"></div>
                <input type="text" readonly="readonly" value="tsi tsi-hourglass">
            </li>
            <li>
                <div class="tsi tsi-id-card"></div>
                <input type="text" readonly="readonly" value="tsi tsi-id-card">
            </li>
            <li>
                <div class="tsi tsi-infinity"></div>
                <input type="text" readonly="readonly" value="tsi tsi-infinity">
            </li>
            <li>
                <div class="tsi tsi-info-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-info-c">
            </li>
            <li>
                <div class="tsi tsi-key"></div>
                <input type="text" readonly="readonly" value="tsi tsi-key">
            </li>
            <li>
                <div class="tsi tsi-lamp"></div>
                <input type="text" readonly="readonly" value="tsi tsi-lamp">
            </li>
            <li>
                <div class="tsi tsi-laptop"></div>
                <input type="text" readonly="readonly" value="tsi tsi-laptop">
            </li>
            <li>
                <div class="tsi tsi-launch"></div>
                <input type="text" readonly="readonly" value="tsi tsi-launch">
            </li>
            <li>
                <div class="tsi tsi-leaf"></div>
                <input type="text" readonly="readonly" value="tsi tsi-leaf">
            </li>
            <li>
                <div class="tsi tsi-left"></div>
                <input type="text" readonly="readonly" value="tsi tsi-left">
            </li>
            <li>
                <div class="tsi tsi-left-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-left-c">
            </li>
            <li>
                <div class="tsi tsi-left-right"></div>
                <input type="text" readonly="readonly" value="tsi tsi-left-right">
            </li>
            <li>
                <div class="tsi tsi-lightbulb"></div>
                <input type="text" readonly="readonly" value="tsi tsi-lightbulb">
            </li>
            <li>
                <div class="tsi tsi-link"></div>
                <input type="text" readonly="readonly" value="tsi tsi-link">
            </li>
            <li>
                <div class="tsi tsi-list-1"></div>
                <input type="text" readonly="readonly" value="tsi tsi-list-1">
            </li>
            <li>
                <div class="tsi tsi-list-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-list-2">
            </li>
            <li>
                <div class="tsi tsi-list-3"></div>
                <input type="text" readonly="readonly" value="tsi tsi-list-3">
            </li>
            <li>
                <div class="tsi tsi-lock"></div>
                <input type="text" readonly="readonly" value="tsi tsi-lock">
            </li>
            <li>
                <div class="tsi tsi-loop"></div>
                <input type="text" readonly="readonly" value="tsi tsi-loop">
            </li>
            <li>
                <div class="tsi tsi-loudspeakers"></div>
                <input type="text" readonly="readonly" value="tsi tsi-loudspeakers">
            </li>
            <li>
                <div class="tsi tsi-mail"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mail">
            </li>
            <li>
                <div class="tsi tsi-mail-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mail-2">
            </li>
            <li>
                <div class="tsi tsi-male"></div>
                <input type="text" readonly="readonly" value="tsi tsi-male">
            </li>
            <li>
                <div class="tsi tsi-man"></div>
                <input type="text" readonly="readonly" value="tsi tsi-man">
            </li>
            <li>
                <div class="tsi tsi-map"></div>
                <input type="text" readonly="readonly" value="tsi tsi-map">
            </li>
            <li>
                <div class="tsi tsi-map-marker"></div>
                <input type="text" readonly="readonly" value="tsi tsi-map-marker">
            </li>
            <li>
                <div class="tsi tsi-marker"></div>
                <input type="text" readonly="readonly" value="tsi tsi-marker">
            </li>
            <li>
                <div class="tsi tsi-mask"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mask">
            </li>
            <li>
                <div class="tsi tsi-medical-cross"></div>
                <input type="text" readonly="readonly" value="tsi tsi-medical-cross">
            </li>
            <li>
                <div class="tsi tsi-medical-kit"></div>
                <input type="text" readonly="readonly" value="tsi tsi-medical-kit">
            </li>
            <li>
                <div class="tsi tsi-menu-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-menu-c">
            </li>
            <li>
                <div class="tsi tsi-message"></div>
                <input type="text" readonly="readonly" value="tsi tsi-message">
            </li>
            <li>
                <div class="tsi tsi-mic"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mic">
            </li>
            <li>
                <div class="tsi tsi-mic-off"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mic-off">
            </li>
            <li>
                <div class="tsi tsi-microphone"></div>
                <input type="text" readonly="readonly" value="tsi tsi-microphone">
            </li>
            <li>
                <div class="tsi tsi-minus-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-minus-c">
            </li>
            <li>
                <div class="tsi tsi-mobile"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mobile">
            </li>
            <li>
                <div class="tsi tsi-money-stack"></div>
                <input type="text" readonly="readonly" value="tsi tsi-money-stack">
            </li>
            <li>
                <div class="tsi tsi-mood-sad"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mood-sad">
            </li>
            <li>
                <div class="tsi tsi-mood-smile"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mood-smile">
            </li>
            <li>
                <div class="tsi tsi-moon"></div>
                <input type="text" readonly="readonly" value="tsi tsi-moon">
            </li>
            <li>
                <div class="tsi tsi-mortarboard"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mortarboard">
            </li>
            <li>
                <div class="tsi tsi-mouse"></div>
                <input type="text" readonly="readonly" value="tsi tsi-mouse">
            </li>
            <li>
                <div class="tsi tsi-music"></div>
                <input type="text" readonly="readonly" value="tsi tsi-music">
            </li>
            <li>
                <div class="tsi tsi-nav-controls"></div>
                <input type="text" readonly="readonly" value="tsi tsi-nav-controls">
            </li>
            <li>
                <div class="tsi tsi-new-window"></div>
                <input type="text" readonly="readonly" value="tsi tsi-new-window">
            </li>
            <li>
                <div class="tsi tsi-newspaper"></div>
                <input type="text" readonly="readonly" value="tsi tsi-newspaper">
            </li>
            <li>
                <div class="tsi tsi-nodes"></div>
                <input type="text" readonly="readonly" value="tsi tsi-nodes">
            </li>
            <li>
                <div class="tsi tsi-note"></div>
                <input type="text" readonly="readonly" value="tsi tsi-note">
            </li>
            <li>
                <div class="tsi tsi-paper"></div>
                <input type="text" readonly="readonly" value="tsi tsi-paper">
            </li>
            <li>
                <div class="tsi tsi-paperclip"></div>
                <input type="text" readonly="readonly" value="tsi tsi-paperclip">
            </li>
            <li>
                <div class="tsi tsi-parameters-h"></div>
                <input type="text" readonly="readonly" value="tsi tsi-parameters-h">
            </li>
            <li>
                <div class="tsi tsi-parameters-v"></div>
                <input type="text" readonly="readonly" value="tsi tsi-parameters-v">
            </li>
            <li>
                <div class="tsi tsi-pause"></div>
                <input type="text" readonly="readonly" value="tsi tsi-pause">
            </li>
            <li>
                <div class="tsi tsi-pen"></div>
                <input type="text" readonly="readonly" value="tsi tsi-pen">
            </li>
            <li>
                <div class="tsi tsi-pencil"></div>
                <input type="text" readonly="readonly" value="tsi tsi-pencil">
            </li>
            <li>
                <div class="tsi tsi-percent"></div>
                <input type="text" readonly="readonly" value="tsi tsi-percent">
            </li>
            <li>
                <div class="tsi tsi-picture"></div>
                <input type="text" readonly="readonly" value="tsi tsi-picture">
            </li>
            <li>
                <div class="tsi tsi-pie-chart"></div>
                <input type="text" readonly="readonly" value="tsi tsi-pie-chart">
            </li>
            <li>
                <div class="tsi tsi-pill"></div>
                <input type="text" readonly="readonly" value="tsi tsi-pill">
            </li>
            <li>
                <div class="tsi tsi-pin"></div>
                <input type="text" readonly="readonly" value="tsi tsi-pin">
            </li>
            <li>
                <div class="tsi tsi-play"></div>
                <input type="text" readonly="readonly" value="tsi tsi-play">
            </li>
            <li>
                <div class="tsi tsi-playlist"></div>
                <input type="text" readonly="readonly" value="tsi tsi-playlist">
            </li>
            <li>
                <div class="tsi tsi-plug"></div>
                <input type="text" readonly="readonly" value="tsi tsi-plug">
            </li>
            <li>
                <div class="tsi tsi-plus"></div>
                <input type="text" readonly="readonly" value="tsi tsi-plus">
            </li>
            <li>
                <div class="tsi tsi-plus-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-plus-c">
            </li>
            <li>
                <div class="tsi tsi-power"></div>
                <input type="text" readonly="readonly" value="tsi tsi-power">
            </li>
            <li>
                <div class="tsi tsi-printer"></div>
                <input type="text" readonly="readonly" value="tsi tsi-printer">
            </li>
            <li>
                <div class="tsi tsi-projector"></div>
                <input type="text" readonly="readonly" value="tsi tsi-projector">
            </li>
            <li>
                <div class="tsi tsi-projector-screen"></div>
                <input type="text" readonly="readonly" value="tsi tsi-projector-screen">
            </li>
            <li>
                <div class="tsi tsi-question"></div>
                <input type="text" readonly="readonly" value="tsi tsi-question">
            </li>
            <li>
                <div class="tsi tsi-question-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-question-c">
            </li>
            <li>
                <div class="tsi tsi-quotes"></div>
                <input type="text" readonly="readonly" value="tsi tsi-quotes">
            </li>
            <li>
                <div class="tsi tsi-radiation"></div>
                <input type="text" readonly="readonly" value="tsi tsi-radiation">
            </li>
            <li>
                <div class="tsi tsi-radio"></div>
                <input type="text" readonly="readonly" value="tsi tsi-radio">
            </li>
            <li>
                <div class="tsi tsi-rec"></div>
                <input type="text" readonly="readonly" value="tsi tsi-rec">
            </li>
            <li>
                <div class="tsi tsi-redo"></div>
                <input type="text" readonly="readonly" value="tsi tsi-redo">
            </li>
            <li>
                <div class="tsi tsi-refresh"></div>
                <input type="text" readonly="readonly" value="tsi tsi-refresh">
            </li>
            <li>
                <div class="tsi tsi-reminder"></div>
                <input type="text" readonly="readonly" value="tsi tsi-reminder">
            </li>
            <li>
                <div class="tsi tsi-reminder-off"></div>
                <input type="text" readonly="readonly" value="tsi tsi-reminder-off">
            </li>
            <li>
                <div class="tsi tsi-remove"></div>
                <input type="text" readonly="readonly" value="tsi tsi-remove">
            </li>
            <li>
                <div class="tsi tsi-remove-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-remove-c">
            </li>
            <li>
                <div class="tsi tsi-remove-s"></div>
                <input type="text" readonly="readonly" value="tsi tsi-remove-s">
            </li>
            <li>
                <div class="tsi tsi-repeat"></div>
                <input type="text" readonly="readonly" value="tsi tsi-repeat">
            </li>
            <li>
                <div class="tsi tsi-resize"></div>
                <input type="text" readonly="readonly" value="tsi tsi-resize">
            </li>
            <li>
                <div class="tsi tsi-rewind"></div>
                <input type="text" readonly="readonly" value="tsi tsi-rewind">
            </li>
            <li>
                <div class="tsi tsi-right"></div>
                <input type="text" readonly="readonly" value="tsi tsi-right">
            </li>
            <li>
                <div class="tsi tsi-right-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-right-c">
            </li>
            <li>
                <div class="tsi tsi-router"></div>
                <input type="text" readonly="readonly" value="tsi tsi-router">
            </li>
            <li>
                <div class="tsi tsi-ruller"></div>
                <input type="text" readonly="readonly" value="tsi tsi-ruller">
            </li>
            <li>
                <div class="tsi tsi-save"></div>
                <input type="text" readonly="readonly" value="tsi tsi-save">
            </li>
            <li>
                <div class="tsi tsi-scales"></div>
                <input type="text" readonly="readonly" value="tsi tsi-scales">
            </li>
            <li>
                <div class="tsi tsi-screen-max"></div>
                <input type="text" readonly="readonly" value="tsi tsi-screen-max">
            </li>
            <li>
                <div class="tsi tsi-screen-min"></div>
                <input type="text" readonly="readonly" value="tsi tsi-screen-min">
            </li>
            <li>
                <div class="tsi tsi-search"></div>
                <input type="text" readonly="readonly" value="tsi tsi-search">
            </li>
            <li>
                <div class="tsi tsi-send"></div>
                <input type="text" readonly="readonly" value="tsi tsi-send">
            </li>
            <li>
                <div class="tsi tsi-server"></div>
                <input type="text" readonly="readonly" value="tsi tsi-server">
            </li>
            <li>
                <div class="tsi tsi-share"></div>
                <input type="text" readonly="readonly" value="tsi tsi-share">
            </li>
            <li>
                <div class="tsi tsi-share-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-share-2">
            </li>
            <li>
                <div class="tsi tsi-shield-protection"></div>
                <input type="text" readonly="readonly" value="tsi tsi-shield-protection">
            </li>
            <li>
                <div class="tsi tsi-shield-restriction"></div>
                <input type="text" readonly="readonly" value="tsi tsi-shield-restriction">
            </li>
            <li>
                <div class="tsi tsi-shield-verified"></div>
                <input type="text" readonly="readonly" value="tsi tsi-shield-verified">
            </li>
            <li>
                <div class="tsi tsi-ship"></div>
                <input type="text" readonly="readonly" value="tsi tsi-ship">
            </li>
            <li>
                <div class="tsi tsi-shopping-cart"></div>
                <input type="text" readonly="readonly" value="tsi tsi-shopping-cart">
            </li>
            <li>
                <div class="tsi tsi-shower"></div>
                <input type="text" readonly="readonly" value="tsi tsi-shower">
            </li>
            <li>
                <div class="tsi tsi-shuffle"></div>
                <input type="text" readonly="readonly" value="tsi tsi-shuffle">
            </li>
            <li>
                <div class="tsi tsi-sign-in"></div>
                <input type="text" readonly="readonly" value="tsi tsi-sign-in">
            </li>
            <li>
                <div class="tsi tsi-sign-out"></div>
                <input type="text" readonly="readonly" value="tsi tsi-sign-out">
            </li>
            <li>
                <div class="tsi tsi-signal-disconnect"></div>
                <input type="text" readonly="readonly" value="tsi tsi-signal-disconnect">
            </li>
            <li>
                <div class="tsi tsi-signal-receiving"></div>
                <input type="text" readonly="readonly" value="tsi tsi-signal-receiving">
            </li>
            <li>
                <div class="tsi tsi-signal-searching"></div>
                <input type="text" readonly="readonly" value="tsi tsi-signal-searching">
            </li>
            <li>
                <div class="tsi tsi-sim-card"></div>
                <input type="text" readonly="readonly" value="tsi tsi-sim-card">
            </li>
            <li>
                <div class="tsi tsi-spinner"></div>
                <input type="text" readonly="readonly" value="tsi tsi-spinner">
            </li>
            <li>
                <div class="tsi tsi-split"></div>
                <input type="text" readonly="readonly" value="tsi tsi-split">
            </li>
            <li>
                <div class="tsi tsi-spy"></div>
                <input type="text" readonly="readonly" value="tsi tsi-spy">
            </li>
            <li>
                <div class="tsi tsi-stack"></div>
                <input type="text" readonly="readonly" value="tsi tsi-stack">
            </li>
            <li>
                <div class="tsi tsi-star"></div>
                <input type="text" readonly="readonly" value="tsi tsi-star">
            </li>
            <li>
                <div class="tsi tsi-star-of-life"></div>
                <input type="text" readonly="readonly" value="tsi tsi-star-of-life">
            </li>
            <li>
                <div class="tsi tsi-stats"></div>
                <input type="text" readonly="readonly" value="tsi tsi-stats">
            </li>
            <li>
                <div class="tsi tsi-stop"></div>
                <input type="text" readonly="readonly" value="tsi tsi-stop">
            </li>
            <li>
                <div class="tsi tsi-stopwatch"></div>
                <input type="text" readonly="readonly" value="tsi tsi-stopwatch">
            </li>
            <li>
                <div class="tsi tsi-structure-1"></div>
                <input type="text" readonly="readonly" value="tsi tsi-structure-1">
            </li>
            <li>
                <div class="tsi tsi-structure-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-structure-2">
            </li>
            <li>
                <div class="tsi tsi-suitcase"></div>
                <input type="text" readonly="readonly" value="tsi tsi-suitcase">
            </li>
            <li>
                <div class="tsi tsi-sun"></div>
                <input type="text" readonly="readonly" value="tsi tsi-sun">
            </li>
            <li>
                <div class="tsi tsi-tablet"></div>
                <input type="text" readonly="readonly" value="tsi tsi-tablet">
            </li>
            <li>
                <div class="tsi tsi-tag"></div>
                <input type="text" readonly="readonly" value="tsi tsi-tag">
            </li>
            <li>
                <div class="tsi tsi-target"></div>
                <input type="text" readonly="readonly" value="tsi tsi-target">
            </li>
            <li>
                <div class="tsi tsi-thumb-list"></div>
                <input type="text" readonly="readonly" value="tsi tsi-thumb-list">
            </li>
            <li>
                <div class="tsi tsi-thumbs-down"></div>
                <input type="text" readonly="readonly" value="tsi tsi-thumbs-down">
            </li>
            <li>
                <div class="tsi tsi-thumbs-up"></div>
                <input type="text" readonly="readonly" value="tsi tsi-thumbs-up">
            </li>
            <li>
                <div class="tsi tsi-track-next"></div>
                <input type="text" readonly="readonly" value="tsi tsi-track-next">
            </li>
            <li>
                <div class="tsi tsi-track-prev"></div>
                <input type="text" readonly="readonly" value="tsi tsi-track-prev">
            </li>
            <li>
                <div class="tsi tsi-train"></div>
                <input type="text" readonly="readonly" value="tsi tsi-train">
            </li>
            <li>
                <div class="tsi tsi-tram"></div>
                <input type="text" readonly="readonly" value="tsi tsi-tram">
            </li>
            <li>
                <div class="tsi tsi-truck"></div>
                <input type="text" readonly="readonly" value="tsi tsi-truck">
            </li>
            <li>
                <div class="tsi tsi-tv"></div>
                <input type="text" readonly="readonly" value="tsi tsi-tv">
            </li>
            <li>
                <div class="tsi tsi-umbrella"></div>
                <input type="text" readonly="readonly" value="tsi tsi-umbrella">
            </li>
            <li>
                <div class="tsi tsi-undo"></div>
                <input type="text" readonly="readonly" value="tsi tsi-undo">
            </li>
            <li>
                <div class="tsi tsi-unlink"></div>
                <input type="text" readonly="readonly" value="tsi tsi-unlink">
            </li>
            <li>
                <div class="tsi tsi-unlock"></div>
                <input type="text" readonly="readonly" value="tsi tsi-unlock">
            </li>
            <li>
                <div class="tsi tsi-up"></div>
                <input type="text" readonly="readonly" value="tsi tsi-up">
            </li>
            <li>
                <div class="tsi tsi-up-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-up-c">
            </li>
            <li>
                <div class="tsi tsi-up-down"></div>
                <input type="text" readonly="readonly" value="tsi tsi-up-down">
            </li>
            <li>
                <div class="tsi tsi-upload-l"></div>
                <input type="text" readonly="readonly" value="tsi tsi-upload-l">
            </li>
            <li>
                <div class="tsi tsi-usb"></div>
                <input type="text" readonly="readonly" value="tsi tsi-usb">
            </li>
            <li>
                <div class="tsi tsi-user"></div>
                <input type="text" readonly="readonly" value="tsi tsi-user">
            </li>
            <li>
                <div class="tsi tsi-users"></div>
                <input type="text" readonly="readonly" value="tsi tsi-users">
            </li>
            <li>
                <div class="tsi tsi-video"></div>
                <input type="text" readonly="readonly" value="tsi tsi-video">
            </li>
            <li>
                <div class="tsi tsi-video-cam"></div>
                <input type="text" readonly="readonly" value="tsi tsi-video-cam">
            </li>
            <li>
                <div class="tsi tsi-voicemail"></div>
                <input type="text" readonly="readonly" value="tsi tsi-voicemail">
            </li>
            <li>
                <div class="tsi tsi-volume"></div>
                <input type="text" readonly="readonly" value="tsi tsi-volume">
            </li>
            <li>
                <div class="tsi tsi-volume-mute"></div>
                <input type="text" readonly="readonly" value="tsi tsi-volume-mute">
            </li>
            <li>
                <div class="tsi tsi-wallet"></div>
                <input type="text" readonly="readonly" value="tsi tsi-wallet">
            </li>
            <li>
                <div class="tsi tsi-warning-c"></div>
                <input type="text" readonly="readonly" value="tsi tsi-warning-c">
            </li>
            <li>
                <div class="tsi tsi-webcam"></div>
                <input type="text" readonly="readonly" value="tsi tsi-webcam">
            </li>
            <li>
                <div class="tsi tsi-webcam-off"></div>
                <input type="text" readonly="readonly" value="tsi tsi-webcam-off">
            </li>
            <li>
                <div class="tsi tsi-weigher"></div>
                <input type="text" readonly="readonly" value="tsi tsi-weigher">
            </li>
            <li>
                <div class="tsi tsi-weight"></div>
                <input type="text" readonly="readonly" value="tsi tsi-weight">
            </li>
            <li>
                <div class="tsi tsi-wheelchair"></div>
                <input type="text" readonly="readonly" value="tsi tsi-wheelchair">
            </li>
            <li>
                <div class="tsi tsi-wi-fi"></div>
                <input type="text" readonly="readonly" value="tsi tsi-wi-fi">
            </li>
            <li>
                <div class="tsi tsi-winner-cup"></div>
                <input type="text" readonly="readonly" value="tsi tsi-winner-cup">
            </li>
            <li>
                <div class="tsi tsi-wrench"></div>
                <input type="text" readonly="readonly" value="tsi tsi-wrench">
            </li>
            <li>
                <div class="tsi tsi-yin-yang"></div>
                <input type="text" readonly="readonly" value="tsi tsi-yin-yang">
            </li>
            <li>
                <div class="tsi tsi-zoom-in"></div>
                <input type="text" readonly="readonly" value="tsi tsi-zoom-in">
            </li>
            <li>
                <div class="tsi tsi-zoom-out"></div>
                <input type="text" readonly="readonly" value="tsi tsi-zoom-out">
            </li>

            <li>
                <div class="tsi tsi-rating-star"></div>
                <input type="text" readonly="readonly" value="tsi tsi-rating-star">
            </li>

            <li>
                <div class="tsi tsi-printer3d"></div>
                <input type="text" readonly="readonly" value="tsi tsi-printer3d">
            </li>

            <li>
                <div class="tsi tsi-printer3d-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-printer3d-2">
            </li>

            <li>
                <div class="tsi tsi-cnc"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cnc">
            </li>

            <li>
                <div class="tsi tsi-cnc-2"></div>
                <input type="text" readonly="readonly" value="tsi tsi-cnc-2">
            </li>
        </ul>


    </div>
</div>
</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    $('[data-toggle="popover"]').popover();
//    $('[data-toggle="tooltip"]').tooltip();

    $(".star-rating--uikit").rating({
        starCaptions: {1: "Very Bad", 2: "Bad", 3: "Good", 4: "Very Good", 5: "Perfect"},
        showClear: false
    });

    //init PS card portfolio pics
    var swiperFeaturedPS = new Swiper('.designer-card__ps-portfolio', {
        scrollbar: '.designer-card__ps-portfolio-scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });

    //Init Picture Slider
    var swiperPictureSlider = new Swiper('.picture-slider', {
        scrollbar: '.picture-slider__scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>
