<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta name="robots" content="noindex, nofollow" />
</head>
<?php if (is_guest()): ?>
    Please login to send email
<?php else: ?>

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'email') ?>

    <div class="form-group">
    <?= Html::submitButton(_t('front.site', 'Send email'), ['class' => 'btn btn-primary ts-ajax-submit js-clickProtect']) ?>
    </div>
    <?php ActiveForm::end(); ?>

<?php endif; ?>
</html>
