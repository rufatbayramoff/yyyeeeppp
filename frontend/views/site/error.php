<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */


/**
 * Resolve title for exception page
 * @return string
 */
$nameFn = function()
{
    $homeLink = Html::a(_t('site.common', 'homepage'), ['/']);
    if ($referrer = Yii::$app->request->referrer){
        return _t('site.common', 'Oops! Something went wrong. <br/>You could go back to {backLink} or head on over to our {hompageLink}', [
            'backLink' => Html::a(_t('site.common', 'where you were'), $referrer),
            'hompageLink' => $homeLink,
        ]);
    }
    return _t('site.common', 'Oops! Something went wrong. <br/> You could head on over to our {hompageLink}', [
        'hompageLink' => $homeLink,
    ]);
};


$this->title = _t('site.common', 'Oops! Something went wrong.');
$name = $nameFn();
if ($this->context->module->layout == 'plain.php') {
    $isShortForm = true;
} else {
    $isShortForm = false;
}
$ee = app('errorHandler')->exception;








 
?>
<div class="container-fluid">

    <div class="error-page__pic error-page__pic--404">
        <?php if(!empty($ee->statusCode)): ?>
        <div class="error-page__error-code">
            <?php echo $ee->statusCode; ?>
        </div>
        <?php else: ?>

        <?php endif; ?>
        <div class="site-error">
            <?php if ($message): ?>
                <div class="alert alert-danger alert--error-page">
                    <?= nl2br(\H($message)) ?>
                </div>
            <?php endif; ?>
            <?php if (!$isShortForm) { ?>
                <h1><?= $name ?></h1>
            <?php } ?>

        </div>
        <?php if (!$isShortForm) {
            ?>


            <div class="error-page__action-btns">
                <?php if (is_guest()): ?>
                    <a href="#" onclick="TS.Visitor.signUpForm();return false;" class="btn btn-danger">
                        <?php echo _t('site.page', 'Sign Up for Free'); ?></a>
                <?php endif; ?>
                <span class="error-page__action-btns-or"></span>
                <a href="<?php echo yii\helpers\Url::toRoute(['/company-services/']); ?>" class="btn btn-primary">
                    <?php echo _t("site.page", "Find a Service"); ?></a>
                <span class="error-page__action-btns-or"></span>
                <a href="<?php echo yii\helpers\Url::toRoute(['/products']); ?>" class="btn btn-primary btn-ghost">
                    <?php echo _t("site.page", "View more products"); ?></a>
            </div>
            <?php
        }
        ?>
    </div>

</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    $("body").addClass("error-page--404");

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>