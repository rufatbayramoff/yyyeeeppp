<?php
use yii\helpers\Json;

/* @var $this \yii\base\View */
/* @var $url string */
/* @var $enforceRedirect boolean */

?>
<!DOCTYPE html>
<html>
<head>
    <script>
        function  getCookie (name) {
            var matches = document.cookie.match(new RegExp(
              "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
        function popupWindowRedirect(url, enforceRedirect)
        {
            if (window.opener && !window.opener.closed) {
                var redir = getCookie('afterlogin_redir');                
                if(redir){
                    document.cookie = 'afterlogin_redir=0;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    window.opener.location = redir;
                }else if (enforceRedirect === undefined || enforceRedirect) {
                    window.opener.location = url;
                } else{
                    window.opener.location.reload(false);                    
                }
                window.opener.focus();
                window.close();
            } else {
                window.location = url;
            }
        }
        popupWindowRedirect(<?= Json::htmlEncode($url) ?>, <?= Json::htmlEncode($enforceRedirect) ?>);
    </script>
</head>
<body>
<h2 id="title" style="display:none;">Redirecting back to the &quot;<?= Yii::$app->name ?>&quot;...</h2>
<h3 id="link"><a href="<?= $url ?>">Click here to return to the &quot;<?= Yii::$app->name ?>&quot;.</a></h3>
<script type="text/javascript">
    document.getElementById('title').style.display = '';
    document.getElementById('link').style.display = 'none';
</script>
</body>
</html>