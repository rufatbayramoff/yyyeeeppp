<?php

use common\models\Company;
use common\models\repositories\ReviewRepository;
use common\models\StoreOrderReview;
use frontend\components\UserUtils;
use frontend\models\review\StoreOrderReviewSearch;
use frontend\models\user\UserFacade;
use frontend\widgets\ReviewStarsWidget;

/* @var $company Company */
/* @var StoreOrderReview $reviews */

$this->registerCssFile('@web/css/ts-rating-widget.css');

$storeOrderReviewSearch        = Yii::createObject(StoreOrderReviewSearch::class);
$storeOrderReviewSearch->forPs = $company;
$reviewsProvider = $storeOrderReviewSearch->search([]);
$reviews = $reviewsProvider->getModels();

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
?>

<div class="ts-rating-wgt ts-rating-wgt--slider">
    <div class="ts-rating-wgt__card">
        <a href="https://www.treatstock.com/" class="ts-rating-wgt__logo" target="_blank" title="Treatstock Smart Manufacturing Platform">Treatstock</a>
        <div class="ts-rating-wgt__stars">
            <?php echo ReviewStarsWidget::widget(
                [
                    'rating'     => 5,
                    'withSchema' => true,
                    'company'    => $company,
                    'html'       => sprintf(
                        '<div class="ts-rating-wgt__score">%s • <a href="%s" target="_blank">%s</a></div>',
                        $company->psCatalog->rating_avg ?? 0,
                        $company->getPublicCompanyLink(),
                        ($company->psCatalog->rating_count ?? 0) . ' ' . _t('site.common', 'reviews')
                    )
                ]);
            ?>
        </div>
    </div>
    <div class="ts-rating-wgt__slider swiper-container">
        <div class="swiper-wrapper">
        <?php foreach ($reviews as $review) { ?>
            <div class="ts-rating-wgt__slider-item swiper-slide">
                <div class="ts-rating-wgt__review">
                    <div class="ts-rating-wgt__review-content">
                        <?php echo ReviewStarsWidget::widget(
                            [
                                'rating'     => $review->rating_speed ,
                                'withSchema' => true,
                                'company'    => $company,
                            ]);
                        ?>
                        <?php if (($review->comment)) { ?>
                            <div class="ts-rating-wgt__review-body">
                                <?=H($review->comment)?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="ts-rating-wgt__review-author">
                        <div class="ts-rating-wgt__review-ava">
                            <?php $userProfileLink = $link = UserUtils::getUserPublicProfileUrl($review->order->user); ?>

                            <?= UserUtils::getAvatarForUser($review->order->user, 'store'); ?>

                        </div>
                        <div class="ts-rating-wgt__review-text">
                            <div class="ts-rating-wgt__review-name">
                                <?= \H(UserFacade::getFormattedUserName($review->order->user)); ?>
                            </div>
                            <div class="ts-rating-wgt__review-date"><?= Yii::$app->formatter->asDate($review->created_at) ?></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
        <div class="ts-rating-wgt__slider-pagination swiper-pagination"></div>
        <div class="ts-rating-wgt__slider-btn swiper-button-next"></div>
        <div class="ts-rating-wgt__slider-btn swiper-button-prev"></div>
    </div>
</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    function initWgtSlider() {
        //init review cards slider
        var swiperRatingWgt = new Swiper('.ts-rating-wgt__slider', {
            pagination: '.ts-rating-wgt__slider-pagination',
            paginationClickable: true,
            slidesPerView: 2,
            spaceBetween: 30,
            grabCursor: true,

            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',

            breakpoints: {
                600: {
                    slidesPerView: 1,
                },
                767: {
                    slidesPerView: 1,
                }
            }
        });
    }

    initWgtSlider();
    $(window).resize(function () {initWgtSlider()});

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>