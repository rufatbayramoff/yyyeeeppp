<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.11.17
 * Time: 16:21
 */

use frontend\components\image\ImageHtmlHelper;
use yii\widgets\LinkPager;

/** @var $dataProvider ActiveDataProvider */

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

$this->title = _t('site.material', 'Materials Catalog for Manufacturing');
$seo = $this->context->seo ?? null;
?>

<div class="over-nav-tabs-header">
    <div class="container container--wide">
        <h1><?= $seo->header ?? _t('site.material', 'Materials Catalog for Manufacturing'); ?></h1>
        <?= $seo->header_text ?? ''; ?>
    </div>
</div>
<div class="nav-tabs__container">
    <div class="container container--wide">
        <div class="nav-filter">
            <div class="nav-filter__group nav-filter__group--search">
                <label for=""><?= _t('site.material', 'Search:'); ?></label>
                <div class="nav-filter__input nav-filter__search">
                    <form method="get" action="#">
                        <input class="form-control nav-filter__search-input" placeholder="Search" name="search"
                               value="<?= H($wikiMaterialSearch->search) ?>">
                        <button class="btn btn-default nav-filter__search-btn" type="submit"><span
                                    class="tsi tsi-search"></span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container container--wide">
    <div class="row">
        <div class="col-xs-12 col-sm-9 col-lg-10">

            <div class="responsive-container-list responsive-container-list--3">
                <?php if ($dataProvider->getCount() > 0): ?>
                    <?php $wikiMaterials = $dataProvider->getModels(); ?>
                    <?php foreach ($wikiMaterials as $wikiMaterial): ?>
                        <div class="responsive-container">
                            <a href="<?= $wikiMaterial->getPublicUrl(); ?>" class="mat-cat-card">
                                <img class="mat-cat-card__pic"
                                     src="<?= $wikiMaterial->getFirstPhotoFile() ? ImageHtmlHelper::getThumbUrlForFile($wikiMaterial->getFirstPhotoFile()) : '/static/images/3drender_720x540.png' ?>"
                                     alt="<?= $wikiMaterial->title ?>">
                                <h3 class="mat-cat-card__title"><?= $wikiMaterial->title ?></h3>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="responsive-container">
                        <?= _t('site.material', 'No materials found.'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <?php echo LinkPager::widget([
                        'pagination' => $dataProvider->getPagination()
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-3 col-lg-2 p-l0">
            <div class="tsadelement">
                <div id="amzn-assoc-ad-f75163cb-f6d9-470a-8686-adc365ba9c2c"></div>
                <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=f75163cb-f6d9-470a-8686-adc365ba9c2c"></script>
            </div>
        </div>

    </div>
</div>