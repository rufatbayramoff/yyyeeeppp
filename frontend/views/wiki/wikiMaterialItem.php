<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 28.11.17
 * Time: 11:12
 */

use common\components\ArrayHelper;
use common\models\factories\LocationFactory;
use common\models\PrinterMaterial;
use common\modules\comments\widgets\Comments;
use common\services\WikiMaterialService;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserSessionFacade;

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

/** @var $wikiMaterial \common\models\WikiMaterial */
$this->title = "Material Catalog";
?>


<div class="over-nav-tabs-header">
    <div class="container container--wide">
        <ol class="breadcrumb m-t10 m-b0">
            <li><a href="/materials"><?= _t('site.material', 'Materials Catalog'); ?></a></li>
            <li><a href="<?= $wikiMaterial->getPublicUrl() ?>"><?= $wikiMaterial->title ?></a></li>
        </ol>
        <h1 class="m-t10"><?= $wikiMaterial->title ?></h1>
    </div>
</div>

<div class="container container--wide">
    <div class="row">
        <div class="col-xs-12 col-sm-9 col-lg-10">

            <div class="row">
                <div class="col-sm-4">
                    <div class="designer-card p-t0 p-l0 p-r0 p-b0 m-b30">
                        <div class="fotorama"
                             data-width="100%"
                             data-height="auto"
                             data-arrows="true"
                             data-click="true"
                             data-nav="thumbs"
                             data-thumbmargin="10"
                             data-thumbwidth="80px"
                             data-thumbheight="60px"
                             data-allowfullscreen="true">
                            <?php
                            foreach ($wikiMaterial->wikiMaterialPhotoFiles as $photoFile) {
                                echo '<img src="' . ImageHtmlHelper::getThumbUrlForFile($photoFile) . '" alt="' . H($photoFile->getFileName()) . '">';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8  wide-padding wide-padding--left">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="m-t0"><?= _t('site.material', 'Features'); ?></h4>
                        </div>
                        <div class="col-sm-8">
                            <ul class="mat-cat-list">
                                <?php
                                foreach ($wikiMaterial->wikiMaterialFeatures as $wikiMaterialFeature) {
                                    echo '<li><span class="tsi tsi-checkmark"></span>' . $wikiMaterialFeature->title . '</li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <h2><?= _t('public.ps', 'About '); ?> <?= $wikiMaterial->title ?></h2>

                    <p><?= $wikiMaterial->about ?></p>

                    <a href="<?= WikiMaterialService::formPrintersUrl($wikiMaterial, null) ?>"
                       class="btn btn-primary m-b20"
                       insp-label="order in material">
                        <?= _t('site.material', 'Order in'); ?> <?= $wikiMaterial->title ?>
                    </a>
                </div>
            </div>

        </div>

        <div class="col-xs-12 col-sm-3 col-lg-2 p-l0">
            <div class="tsadelement">
                <div id="amzn-assoc-ad-f75163cb-f6d9-470a-8686-adc365ba9c2c"></div>
                <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=f75163cb-f6d9-470a-8686-adc365ba9c2c"></script>
            </div>
        </div>

    </div>

</div>

<div class="mat-cat-nav">
    <div class="container container--wide">
        <span class="mat-cat-nav__item"><?= _t('site.material', 'Application'); ?></span>
        <?php if ($wikiMaterial->wikiMaterialProperties) {
            ?>
            <a class="mat-cat-nav__item" href="#props" data-target="#props"><?= _t('site.material', 'Properties'); ?></a>
        <?php } ?>
        <a class="mat-cat-nav__item" href="#descr" data-target="#descr"><?= _t('site.material', 'Description'); ?></a>
        <a class="mat-cat-nav__item" href="#rev" data-target="#rev"><?= _t('site.material', 'Reviews'); ?></a>
    </div>
</div>

<!-- Application -->
<div class="container container--wide">
    <div class="row mat-cat-application m-t10 fb-grid">
        <?php
        foreach ($wikiMaterial->wikiMaterialIndustries as $wikiMaterialIndustry) {
            ?>
            <div class="fb-grid__item col-xs-6 col-sm-3">
                <div class="designer-card designer-card--industry">
                    <img class="designer-card__pic" src="https://www.treatstock.com<?= $wikiMaterialIndustry->bizIndustry->photo_url ?>"
                         alt="<?= $wikiMaterialIndustry->bizIndustry->title ?>">
                    <h2 class="designer-card__title">
                        <?= $wikiMaterialIndustry->bizIndustry->title ?>
                    </h2>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<!-- Properties -->
<?php if ($wikiMaterial->wikiMaterialProperties) {
    ?>
    <div class="container container--wide" id="props">
        <div class="row">
            <div class="col-sm-8">
                <h3><?= _t('site.material', 'Properties'); ?></h3>
                <table class="table table-bordered m-b10">
                    <tr>
                        <th>
                            <?= _t('site.material', 'Property'); ?>
                        </th>
                        <th>
                            <?= _t('site.material', 'Value'); ?>
                        </th>
                    </tr>
                    <?php
                    $comments = [];
                    $asteriskCount = 0;
                    foreach ($wikiMaterial->wikiMaterialProperties as $wikiMaterialProperty) {
                        $asterisks = '';
                        if ($wikiMaterialProperty->comment) {
                            if (array_search($wikiMaterialProperty->comment, $comments)) {
                                $asteriskCount = array_search($wikiMaterialProperty->comment, $comments);
                            } else {
                                $comments[$asteriskCount + 1] = $wikiMaterialProperty->comment;
                                $asteriskCount = count($comments);
                            }
                            $asterisks = implode('', array_fill(1, $asteriskCount, '*'));
                        }

                        echo '<tr><td>' . $wikiMaterialProperty->title . '</td><td>' . $wikiMaterialProperty->value . ' ' . $asterisks . ' </td></tr>';
                    }
                    ?>
                </table>
                <p class="text-muted">
                    <?php
                    foreach ($comments as $asteriskCount => $comment) {
                        $asterisks = implode('', array_fill(1, $asteriskCount, '*'));
                        echo $asterisks . ' ' . $comment;
                        if ($comment !== end($comments)) { // Is last element
                            echo ', ';
                        }
                    }
                    ?>
                </p>
            </div>
        </div>
    </div>
<?php } ?>

<!-- Description -->
<div class="container container--wide mat-cat-text" id="descr">
    <div class="row">
        <div class="col-sm-8">
            <h3><?= _t('site.material', 'Description'); ?></h3>
            <p><?= $wikiMaterial->description ?></p>
            <!-- Documents Link block -->
            <div class="m-b20">
                <?php
                foreach ($wikiMaterial->wikiMaterialFileFiles as $file) {
                    echo '<a href="' . $file->getFileUrl() . '" target="_blank"><span class="tsi tsi-doc m-r10"></span>' . $file->getFileName() . '</a><br>';
                }
                ?>
            </div>
        </div>
    </div>
</div>


<!-- Review -->
<div class="container container--wide" id="rev">
    <div class="row">
        <div class="col-sm-8">
            <h3><?= _t('site.material', 'Reviews'); ?></h3>

            <?php
            if (Yii::$app->user->isGuest) {
                ?>
                <?= _t('public.ps', 'Work with this material?'); ?>
                <a href="<?= $wikiMaterial->getPublicUrl() ?>#modalSendReview"
                   onclick="TS.Visitor.loginForm('<?= $wikiMaterial->getPublicUrl() ?>#modalSendReview'); return false;">
                    <?= _t('public.ps', 'Leave a review'); ?>
                </a>
                <?php
            }
            ?>
            <?= Comments::widget(['model' => 'wiki_material', 'model_id' => $wikiMaterial->id]); ?>
        </div>
    </div>
</div>

<!-- Related Links -->
<div class="container container--wide m-b30">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="m-b20"><?= _t('public.ps', 'Related Links'); ?></h3>
            <?php
            foreach ($wikiMaterial->wikiMaterialRelatedObjects as $wikiMaterialRelated) {
                echo '<a href="' . $wikiMaterialRelated->getPublicUrl() . '" class="btn btn-primary btn-ghost btn-sm m-r20 m-b10">' . $wikiMaterialRelated->title . '</a>';
            }
            ?>
        </div>
    </div>
</div>

<div class="page-link-blocks">
    <div class="container container--wide">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="m-t10 m-b20"><?= _t('public.ps', 'Print Services in my city'); ?></h3>
            </div>
            <div class="col-sm-3">
                <ul>
                    <?php
                    foreach ($wikiMaterial->geoCities as $geoCity) {
                        echo '<li><a href="' . WikiMaterialService::formPrintersUrl($wikiMaterial, $geoCity) . '">' . $geoCity->title . '</a></li>';
                    } ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    $('a[data-target]').click(function () {
        var scroll_elTarget = $(this).attr('data-target');
        if ($(scroll_elTarget).length != 0) {
            $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 110}, 400);
        }
        return false;
    });

    <?php $this->endBlock(); ?>
</script>
<?php $this->registerJs($this->blocks['js1']); ?>
