<?php
use yii\bootstrap\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $locator \frontend\models\user\UserLocator */

$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);
$this->registerJsFile('/js/ts/change-location.js', ['position' => View::POS_BEGIN]);

?>
<div class="user-change-location">    <?php
    $form = ActiveForm::begin([
        'id' => 'user-change-location-form',
        'action' => ['geo/set-location']
    ]);
    ?> 
    <div class="row">
        <div class="col-xs-12">
        <?php 
          echo $form->field($locator, 'address')->label(_t('site.user', 'Enter the name of your city'));
          echo $form->field($locator, 'addressApi')->hiddenInput()->label(false);
        ?> 
        </div>
        <div class="col-xs-12 hide">
            <br />
            <button
                type="submit"
                class="btn btn-primary ts-ajax-submit"
                name="submit-button"
                jsCallback="<?=$jsCallback;?>"><?=_t('front', 'Change')?></button>
        </div>
    </div>
   
  
<?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">


TS.Locator.init();


</script>

<style type="text/css">
.pac-container {
    z-index: 1100;
}
</style>


