<?php

/** @var $cuttingWorkpieceMaterialsGroup \common\modules\cutting\models\CuttingWorkpieceMaterialGroup */

use lib\money\Money;

?>

<div class="ps-pub-profile__workpiece">
    <div class="designer-card p-t10 p-b10 m-b0">

        <h3 class="m-t0 m-b10">
            <?= H($cuttingWorkpieceMaterialsGroup->material->title); ?>
        </h3>

        <div class="designer-card__cutting-materials">
            <?php
            foreach ($cuttingWorkpieceMaterialsGroup->sizes as $groupSize) {
                ?>
                <div class="designer-card__cutting-materials-item">

                    <div class="designer-card__cutting-materials-sizes clearfix">
                        <div class="pull-left p-r20">

                            <div class="text-muted"><?= _t('site.cutting', 'Sizes') ?></div>

                            <b><?= H($groupSize->size->width) ?>x<?= H($groupSize->size->length) ?> mm</b>

                        </div>

                        <div class="pull-right text-right">

                            <div class="text-muted"><?= _t('site.cutting', 'Thickness') ?></div>

                            <b><?= H($groupSize->size->thickness) ?> mm</b>

                        </div>

                    </div>

                    <?php
                    foreach ($groupSize->groupColors as $groupColors) {
                        ?>
                        <div class="designer-card__cutting-materials-colors">
                            <?php
                            foreach ($groupColors->colors as $groupColor) {
                                ?>
                                <div title="<?= _t('site.cutting', 'Color') ?> <?= $groupColor->color->title ?>" class="material-item">
                                    <div class="material-item__color" style="background-color: #<?= $groupColor->color->getRgbHex(); ?>"></div>
                                    <div class="material-item__label"><?= $groupColor->color->title ?></div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        if ($groupColors->price) { ?>
                            <h3 class="designer-card__cutting-materials-price" title="<?= _t('site.cutting', 'Price') ?>">
                                <?= displayAsMoney($groupColors->price); ?>
                            </h3>
                            <?php
                        }
                        ?>
                        <?php
                    }
                    ?>
                    <?php
                    if ($groupSize->price) {
                        ?>
                        <h3 class="designer-card__cutting-materials-price" title="<?= _t('site.cutting', 'Price') ?>">
                            <?= displayAsMoney($groupSize->price) ?>
                        </h3>
                        <?php
                    }
                    if ($cuttingWorkpieceMaterialsGroup->price) {
                        ?>
                        <h3 class="designer-card__cutting-materials-price" title="<?= _t('site.cutting', 'Price') ?>">
                            <?= displayAsMoney($cuttingWorkpieceMaterialsGroup->price) ?>
                        </h3>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>

    </div>

</div>
