<?php
/**
 * @var \frontend\models\review\StoreOrderReviewSearch $searchModel
 * @var string $action
 */

use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>
<?php $form = ActiveForm::begin([
    'method' => 'GET',
    'action' => $action,
    'enableClientValidation' => false,
    'options' => [
        'class' => 'js-ReviewSearchPanel'
    ]
]); ?>

<div class="ps-profile-user-review-filter">
    <label for="service_id"><?= _t('reviews.searchPanel', 'Filter by') ;?></label>
    <div class="ps-profile-user-review-filter__select">
        <?php echo $form->field($searchModel, 'service_id')
            ->label(false)
            ->widget(
            Select2::class, [
                'data' => ArrayHelper::merge(['' => _t('reviews.searchPanel', 'All')], $searchModel->getReviewPrinterListForPs()),
                'size' => kartik\select2\Select2::SMALL,
                'pluginEvents'  => [
                    'select2:select' => "function() { $('.js-ReviewSearchPanel').submit() }",
                ]
            ]
        ) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
