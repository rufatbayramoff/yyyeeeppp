<?php
/**
 * User: nabi
 */

use common\components\ArrayHelper;
use common\models\DeliveryType;
use common\models\factories\LocationFactory;
use common\models\Ps;
use common\models\PsMachineDeliveryCountry;
use common\models\PsPrinter;
use common\models\UserAddress;
use common\modules\catalogPs\components\CatalogSearchService;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use common\modules\catalogPs\models\PsPrinterEntity;
use frontend\assets\PhotoSwipeAsset;
use frontend\widgets\Model3dUploadWidget;
use frontend\widgets\Model3dViewedStateWidget;
use lib\money\Money;
use yii\helpers\Url;


/** @var $ps \common\models\Ps */
/** @var $companyPublicPageEntity \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\Model3dViewedState $model3dViewedState */
/** @var PsPrinter $selectedPrinter */
/** @var PS $ps */
/** @var \yii\web\View $this */

$user = $ps->user;
$sendMessageUrl = $companyPublicPageEntity->sendMessageUrl;
echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $companyPublicPageEntity]);

$cncMachine = null;
$this->registerAssetBundle(PhotoSwipeAsset::class);
$this->registerAssetBundle(\frontend\assets\PhotoSwipeAsset::class);


$listView = Yii::createObject([
    'class'        => \yii\widgets\ListView::class,
    'dataProvider' => $dataProvider,
    'itemOptions'  => ['tag' => null],
    'itemView'     => 'listItem',
    'viewParams'   => [
        'searchForm'       => $searchForm,
        'printedFilesRepo' => new \common\modules\catalogPs\repositories\PrintedFileRepository($dataProvider->getModels()),
    ]
]);
$printersShipping = [];
$prevShipping = [];
$sameDeliveryHash = [];
/** @var PsPrinterEntity $printerEntity */
foreach ($dataProvider->getModels() as $printerEntity) {
    $psMachineDeliveries = $printerEntity->getDeliveryTypes();
    $deliveries = [
        DeliveryType::PICKUP        => null,
        DeliveryType::STANDARD      => null,
        DeliveryType::INTERNATIONAL => null,
    ];
    /** @var \common\models\PsMachineDelivery $psDelivery */
    foreach ($psMachineDeliveries as $psDelivery) {
        $deliveryType = DeliveryType::findByPk($psDelivery->delivery_type_id);
        if ($deliveryType->code == DeliveryType::PICKUP) {
            continue;
        }
        $price = $psDelivery->carrier_price
            ? displayAsCurrency($psDelivery->carrier_price, $ps->currency)
            : _t('ps.profile', 'Postal Service Flat Rate');

        if ($psDelivery->carrier == DeliveryType::CARRIER_TS) {
            $price = _t('ps.profile', 'Postal Service Flat Rate');
        }
        if ($psDelivery->deliveryType->code == DeliveryType::PICKUP) {
            $price = _t('site.ps', 'Working hours: {hours}', ['hours' => \H($psDelivery->comment)]);
        }
        $iconHtml = '<span class="tsi ' . DeliveryType::DELIVERY_ICONS[$psDelivery->deliveryType->code] . '"></span>';
        $deliveries[$deliveryType->code] = [
            'icon'          => $iconHtml,
            'price'         => $price,
            'type'          => $deliveryType->getTitle(),
            'type_code'     => $deliveryType->code,
            'comment'       => $psDelivery->comment,
            'carrier'       => $psDelivery->carrier,
            'carrier_price' => $psDelivery->carrier_price,
            'free_delivery' => $psDelivery->free_delivery,
            'packing_price' => $psDelivery->packing_price,
            'countries'     => array_filter($psDelivery->psMachineDeliveryCountries,function (PsMachineDeliveryCountry $deliveryCountry){
                return !empty($deliveryCountry->price) && $deliveryCountry->price > 0;
            })
        ];
    }
    array_filter($deliveries);

    $printersShipping[$printerEntity->id] = [
        'printerEntity' => $printerEntity,
        'printer_id'    => $printerEntity->id,
        'title'         => $printerEntity->title,
        'delivery'      => $deliveries
    ];
}


$resolveCityBreadcrumbFn = function () use ($ps) {
    $cityes = ArrayHelper::index(ArrayHelper::getColumn($ps->notDeletedPsMachines, 'location.city'), 'id');
    if (!$cityes || count($cityes) > 1) {
        return null;
    }
    /** @var CatalogSearchService $catalogSearchService */
    $catalogSearchService = Yii::$container->get(CatalogSearchService::class);
    $city = reset($cityes);
    return [
        $city->title,
        CatalogPrintingUrlHelper::printing3dCatalog(LocationFactory::createFromGeoCity($city))
    ];
};

$cityBreadcrumb = $resolveCityBreadcrumbFn();
?>
    <div class="container ps-profile">

        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb m-t10">
                    <li><a href="<?=CatalogPrintingUrlHelper::landingPage();?>"><?= _t('site.user', '3D Printers'); ?></a></li>
                    <?php if ($cityBreadcrumb): ?>
                        <li><a href="<?= $cityBreadcrumb[1]; ?>"><?= H($cityBreadcrumb[0]); ?></a></li>
                    <?php endif; ?>
                    <li><a href="<?= $companyPublicPageEntity->psUrl; ?>"><?= H($ps->title); ?></a></li>
                    <li><a href="<?= $companyPublicPageEntity->printServiceUrl; ?>"><?= _t('site.user', '3D Printing'); ?></a></li>
                </ol>
            </div>
        </div>
        <div class="row ps-pub-profile">
            <div class="col-sm-8 wide-padding wide-padding--right">
                <h1 class="ps-pub-profile__header">
                    <?= H($ps->title); ?>
                </h1>

                <div class="designer-card ps-profile__loc-card">
                    <h2 class="designer-card__title">
                        <?php if (count($printersShipping) > 1): ?>
                            <?= _t('site.user', 'Shipping options for'); ?>
                            <select class="form-control input-sm ps-profile__loc-card-select"
                                    onchange="$('.js-printer-shipping').hide();$('#printerDelivery-'+this.value).show();">
                                <?php foreach ($printersShipping as $id => $printer): ?>
                                    <option value="<?= $id; ?>"><?= $printer['title']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        <?php else: ?>
                            <?= _t('site.user', 'Shipping options'); ?>
                        <?php endif; ?>
                    </h2>
                    <?php
                    $shippingBlockStyle = '';
                    $i = 0;
                    foreach ($printersShipping as $k => $printerShipping): $i++; ?>
                        <div class="row js-printer-shipping" id="printerDelivery-<?= $k ?>" style="<?= $shippingBlockStyle; ?>">
                            <div class="col-md-12">
                                <p>
                                    <?= _t('site.user', 'Printer location'); ?>
                                    <span class="tsi tsi-map-marker"></span> <?= UserAddress::formatPrinterAddress(PsPrinter::findByPk($k)); ?>
                                </p>
                            </div>
                            <?php foreach ($printerShipping['delivery'] as $deliveryCode => $delivery): ?>
                                <?php if(!isset($delivery['icon'],$delivery['type'])):?>
                                    <?php continue;?>
                                <?php endif;?>
                                <div class="col-md-4">
                                    <div class="designer-card__data">
                                    <span class="designer-card__data-label">
                                        <?= $delivery['icon']; ?>  <?= $delivery['type']; ?>
                                    </span>
                                    </div>
                                    <div class="designer-card__data">
                                        <?= $delivery['price']; ?> <br>

                                        <?php if (!empty($delivery['free_delivery'])):
                                            $feeHelper = new \common\modules\payment\fee\FeeHelper();
                                            $freeDelivery = Money::create($delivery['free_delivery'], $ps->currency);
                                            $percent = $feeHelper->getTsCommonFee($freeDelivery);
                                            $freeDeliveryPrice = \lib\money\MoneyMath::sum($freeDelivery, $percent);
                                            ?>
                                            <?= _t('site.ps', 'Free Delivery from {price}', ['price' => displayAsMoney($freeDeliveryPrice)]); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <?php if (!empty($delivery['countries'])): ?>
                                <div class="col-md-12">
                                    <div class="designer-card__data m-t10 m-b0">
                                        <span class="designer-card__data-label">
                                            <span class="tsi tsi-aircraft m-r5"></span><?= _t('site.user', 'Shipping by countries'); ?>
                                        </span>
                                    </div>
                                    <div class="row">
                                    <?php /** @var PsMachineDeliveryCountry $country */
                                    foreach ($delivery['countries'] as $country):?>
                                        <div class="col-sm-6 col-md-4 m-b0">
                                            <div class="designer-card__data-row">
                                                <div class="designer-card__data-row-left">
                                                    <?php echo $country->geoCountry->title ?>
                                                </div>
                                                <div class="designer-card__data-row-right">
                                                    <?php echo displayAsCurrency($country->price, $ps->currency) ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    </div>
                                </div>
                                <?php endif; ?>

                            <?php endforeach; ?>
                        </div>
                        <?php $shippingBlockStyle = 'display:none';
                    endforeach; ?>

                </div>


                <h3><?= _t('site.user', 'Printers'); ?></h3>

                <div class="pub-machine__list">
                    <?= $listView->renderItems() ?>
                </div>
            </div>
            <div class="col-sm-4 ps-pub-profile__sidebar">

                <?php
                if (!empty($model3dViewedState)) {
                    echo Model3dViewedStateWidget::widget(['model3dViewedState' => $model3dViewedState, 'psPrinter' => $selectedPrinter]);
                } elseif ($companyPublicPageEntity->printersCount) {
                    echo Model3dUploadWidget::widget([
                        'viewType'          => 'short',
                        'isPlaceOrderState' => true,
                        'additionalParams'  => ['utmSource' => 'fromPrinter', 'psId' => $ps->id]
                    ]);
                }
                ?>

                <h3>
                    <?= _t('site.user', 'Need something special?'); ?>
                </h3>

                <?= $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-inline.php', ['ps' => $ps]); ?>

            </div>
        </div>
    </div>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        PrinterShipping = function ($scope) {

        };

        <?php $this->endBlock(); ?>
    </script>

<?php $this->registerJs($this->blocks['js1']); ?>