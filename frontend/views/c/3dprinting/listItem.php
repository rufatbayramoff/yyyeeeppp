<?php

use common\components\FileTypesHelper;
use common\models\DeliveryType;
use common\models\Ps;
use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\ps\PsFacade;
use frontend\widgets\SwipeGalleryWidget;
use yii\helpers\Url;

$this->registerAssetBundle(\frontend\assets\PhotoSwipeAsset::class);

/**
 * @var $printedFilesRepo \common\modules\catalogPs\repositories\PrintedFileRepository
 * @var $printer \common\modules\catalogPs\models\PsPrinterEntity
 * @var $searchForm \common\modules\catalogPs\models\CatalogSearchForm
 */
$printer = $model;
/** @var $ps Ps */
$ps = $printer->getPs();


$materials          = [];
$materialsDisplay   = [];
$certLevel          = $printer->companyService->getCertificationLabel();
$materials          = $printer->getMaterials();
$moreMaterialsCount = 0;

$psLink    = PsFacade::getPsLink($ps);
$psPicture = Ps::getCircleImageByPs($ps);
?>

<div class="pub-machine__list-item">
    <div class="designer-card designer-card--pub-machine">
        <?php if ($printer->meta->getTechnology()): ?>
            <span class="designer-card__tech-label label label-info" data-toggle="tooltip" data-placement="bottom" title=""
                  data-original-title="<?= H($printer->meta->getTechnology()->getTitleInfo()); ?>"> <?= H($printer->meta->getTechnology()->getTitleCode()); ?></span>
        <?php endif; ?>

        <h2 class="designer-card__title">
            <?= H($printer->title); ?>&nbsp;<a class="small" target="_blank" href="<?= Url::toRoute(['machines/default/item', 'slug' => $printer->meta->getSlug()]) ?>"><i
                        class="tsi tsi-info-c"></i></a>
        </h2>
        <?php
        $printerLocation = sprintf("%s, %s, %s", $printer->city, $printer->region, $printer->country_iso);
        ?>
        <div class="designer-card__ps-loc" title="<?= $printerLocation; ?>">
            <span class="tsi tsi-map-marker"></span>
            <?php if ($printer->location) {
                ?>
                <a href="<?= CatalogPrintingUrlHelper::printing3dCatalog($printer->location); ?>" target="_blank"><?= $printerLocation; ?></a>
                <?php
            } else {
                ?>
                <?= $printerLocation; ?>
                <?php
            }
            ?>
        </div>
        <div class="designer-card__cert">
            <p class="designer-card__cert-label">

                <strong><?= _t('site.ps', 'Certification'); ?></strong>
                <?php

                if ($printer->getPsPrinter() && $printer->getPsPrinter()->isCertificated()): ?>
                    <?php $tsCertificationClass = $printer->getTsCertificationClass() ?>
                    <span class="designer-card__cert-status">
                         <span class="cert-label"
                               data-toggle="tooltip"
                               data-placement="bottom"
                               title=""
                               data-original-title="<?= _t(
                                   'site.ps',
                                   $tsCertificationClass->description
                               ); ?>">
                             <span class="cert-label cert-label--common m-r5">
                                <i class="tsi tsi-checkmark"></i>
                             </span>
                             <?= $tsCertificationClass->title; ?>
                         </span>
                    </span>
                <?php else: ?>
                    <?= _t('site.ps', 'Not verified'); ?>
                <?php endif; ?>
            </p>
            <div class="designer-card__cert-data">
                <div class="designer-card__cert-pic">
                    <?php
                    $testOrderImages = $printer->meta->getTestOrderImages();
                    if ($testOrderImages) {

                        $isFirst = true;
                        foreach ($testOrderImages as $firstImage) {
                            if (FileTypesHelper::isVideo($firstImage)) {
                                $videoUrl = $firstImage->getFileUrl();
                                echo sprintf(
                                    '<a  href="%s" data-lightbox="%s" style="' . ($isFirst ? '' : 'display : none') . '"><video src="%s" alt="%s"></a>',
                                    $videoUrl, 'ps' . $printer->id, $videoUrl, \H($firstImage->name)
                                );
                            } else {
                                $img      = ImageHtmlHelper::getThumbUrl($firstImage->getFileUrl());
                                $imgThumb = ImageHtmlHelper::getThumbUrl($firstImage->getFileUrl(), 160, 90);
                                echo sprintf(
                                    '<a  href="%s" data-lightbox="%s" style="' . ($isFirst ? '' : 'display : none') . '"><img src="%s" alt="%s"></a>',
                                    $img, 'ps' . $printer->id, $imgThumb, \H($firstImage->name)
                                );
                            }
                            $isFirst = false;
                        }
                    } else {
                        ?>
                        <div class="designer-card__cert-pic-empty">
                            No image
                        </div>
                        <?php
                    }
                    ?>
                    </a>
                </div>
                <div class="designer-card__cert-text">
                    <div class="designer-card__data">
                        <div class="designer-card__data-label"><?= _t('site.ps', 'Build area'); ?></div>
                        <?php
                        $size = $printer->meta->getBuildVolume();
                        echo $size[0] . ' x ' . $size[1] . ' x ' . $size[2] . ' mm';
                        ?>
                    </div>
                    <div class="designer-card__data m-b0">
                        <div class="designer-card__data-label"><?= _t('site.ps', 'Layer resolution (highest)'); ?></div>
                        <?= $printer->meta->getLayerResolution('high'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row designer-card__data">
            <div class="col-sm-4 col-md-6 col-lg-4 designer-card__data-label"><?= _t('site.ps', 'Minimum Charge:'); ?></div>
            <div class="col-sm-8 col-md-6 col-lg-8">
                <?php
                $feeHelper     = new \common\modules\payment\fee\FeeHelper();
                $minOrderPrice = \lib\money\Money::create($printer->min_order_price, $printer->companyService->company->currency);
                $percent       = $feeHelper->getTsCommonFee($minOrderPrice);
                if ($minOrderPrice->getAmount()): ?>
                    <?= displayAsMoney(\lib\money\MoneyMath::sum($minOrderPrice, $percent)); ?>
                <?php else: ?>
                    -
                <?php endif; ?>
            </div>
        </div>
        <div class="row designer-card__data border-t p-t10">
            <div class="col-xs-12 designer-card__data-label m-b10"><?= _t('site.ps', '{Materials} and Colors',
                    ['Materials' => \yii\helpers\Html::a(_t('site.ps', 'Materials'), '/materials', ['target' => '_blank'])]); ?></div>
            <div class="col-xs-12">
                <?php foreach ($materials as $printerMaterial):
                    $firstColor = $printerMaterial->getCheapestColor();
                    $filamentTitle = $printerMaterial->material->title;
                    if ($wikiMaterial = $printerMaterial->material->getWikiMaterials()->one()) {
                        $filamentTitle = \yii\helpers\Html::a($filamentTitle, $wikiMaterial->getPublicUrl(), ['target' => '_blank']);
                    } else {
                        $filamentTitle = H($filamentTitle);
                    }

                    ?>
                    <div class="designer-card__material">
                        <span class="designer-card__material-title"><?= $filamentTitle ?></span>
                        <?php foreach ($printerMaterial->colors as $printerColor): ?>
                            <div title="<?= $printerColor->color->title; ?>" class="material-item">
                                <div class="material-item__color" style="background-color: #<?= $printerColor->color->getRgbHex(); ?>"></div>
                                <div class="material-item__label"><?= $printerColor->color->title; ?></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>


        <div class="designer-card__ps-pics">
            <div class="designer-card__ps-pics-label"><?= _t('site.ps', '3D printed examples'); ?></div>
            <?=
            SwipeGalleryWidget::widget([
                'files'            => (array)$printedFilesRepo->getByPrinterId($printer->id),
                'maxCount'         => 3,
                'thumbSize'        => [160, 90],
                'assetsByClass'    => true,
                'containerOptions' => ['class' => 'designer-card__ps-portfolio'],
                'itemOptions'      => ['class' => 'designer-card__ps-portfolio-item'],
                'scrollbarOptions' => ['class' => 'designer-card__ps-portfolio-scrollbar'],
                'emptyOptions'     => ['class' => 'designer-card__ps-pics-empty'],
                'emptyText'        => _t('site.catalog', 'Images not uploaded'),
                'alt'              => H($ps->title) . _t('site.catalog', ' 3D printing photo'),
                'title'            => H($ps->title) . _t('site.catalog', ' 3D printing photo')
            ]);
            ?>
        </div>

        <?php if ($printer->description): ?>
            <div class="designer-card__about">
                <?= \frontend\components\StoreUnitUtils::displayMore($printer->description); ?>
            </div>
        <?php else: ?>
            <!-- Remove empty gap -->
            <div style="margin-top: -25px;"></div>
        <?php endif; ?>
        <div>
            <?php
            $deliveryTypesIntl = PsFacade::getDeliveryTypesIntl();
            $deliveryHtml      = [];
            foreach ($printer->getDeliveryTypes() as $psDelivery):
                $price = $psDelivery->carrier_price
                    ? displayAsCurrency($psDelivery->carrier_price, 'USD')
                    : _t('ps.profile', 'Postal Service Flat Rate');

                if ($psDelivery->carrier == DeliveryType::CARRIER_TS) {
                    $price = _t('ps.profile', 'Postal Service Flat Rate');
                }
                $iconHtml = '<span class="tsi ' . DeliveryType::DELIVERY_ICONS[$psDelivery->deliveryType->code] . '"></span>';
                if ($psDelivery->deliveryType->code == DeliveryType::PICKUP) {
                    $deliveryHtml[] = $iconHtml . ' ' . $deliveryTypesIntl[$psDelivery->deliveryType->code];
                    continue;
                }
                $deliveryHtml[] = $iconHtml . ' ' . $deliveryTypesIntl[$psDelivery->deliveryType->code] . ' — ' . $price;
            endforeach;
            //echo implode('<br />', $deliveryHtml);
            ?>
        </div>
    </div>
</div>