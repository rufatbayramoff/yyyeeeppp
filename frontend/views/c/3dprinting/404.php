<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 16.11.17
 * Time: 14:37
 */

use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

?>
<div class="container">
    <div class="error-page__pic text-center">
        <h1><?=_t('site.user', 'This manufacturer is currently unavailable')?></h1>
        <div class="error-page__error-info">
            <?=_t('site.user', 'Please select another manufacturer')?>
        </div>
    </div>
</div>

<div class="main-page-models main-page-models--ps m-t30" style="margin-bottom: -40px;">
    <div class="container js-main-page-featured-print-services">
        <div class="main-page-models__list main-page-models__list--ps swiper-container p-b0">
            <div class="swiper-wrapper">
                <?php

                if($this->beginCache('psmain'.md5(implode(',',app('setting')->get('mainpage.ps', []))), ['duration' => 36000, 'variations'=>[\Yii::$app->language]])){
                    echo \frontend\widgets\PsCardWidget::widget([
                        'perPage' => 20,
                        'containerClass' => 'main-page-models__item swiper-slide',
                        'psLinks'=> \Yii::$app->setting->get('mainpage.ps', [
                            'laughing-monkey-labs-llc',
                            'modern-blacksmith-3d-design-printing-and-scanning',
                            'technologics-group',
                            'grapha-print',
                            'seaside-3d', 'pfactory',  'koekjesstekernl'
                        ])
                    ]);
                    $this->endCache();
                }
                ?>
            </div>
            <div class="main-page-models__ps-scrollbar swiper-scrollbar"></div>
        </div>
    </div>

    <div class="container">
        <div class="col-xs-12">
            <a class="btn btn-primary m-b20" href="<?=CatalogPrintingUrlHelper::printing3dCatalog();?>">
                <?= _t('front', 'All manufacturers'); ?>
            </a>
        </div>
    </div>
</div>

<script>
    <?php $this->beginBlock('js1', false); ?>

    //Init slider for PS portfolio
    var swiperFeaturedPS = new Swiper('.designer-card__ps-portfolio', {
        scrollbar: '.designer-card__ps-portfolio-scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });


    function initPSSliders() {
    //Init slider for --ps
        var swiperPs = new Swiper('.main-page-models__list--ps', {
            slidesPerView: 'auto',
            scrollbar: '.main-page-models__ps-scrollbar',
            scrollbarHide: true,
            spaceBetween: 0,
            grabCursor: true,

            breakpoints: {
                500: {
                    slidesPerView: 1.15,
                    centeredSlides: false
                },
                767: {
                    slidesPerView: 2.25
                }
            }
        });
    }

    initPSSliders();
    $(window).resize(function () {initPSSliders()});

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>