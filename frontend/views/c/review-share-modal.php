<?php

use common\components\FileTypesHelper;

?>
<script type="text/ng-template" id="/app/review/review-share-modal.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?= _t('share', 'Close') ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title"><?= _t("site.review", "Share review"); ?></h3>
                </div>
                <div class="modal-body">

                    <div style="margin-bottom: 15px; ">

                        <p ng-if="!form.editState" class="m-b0" ng-bind-html="share.text | nl2br"></p>

                        <textarea ng-if="form.editState" ng-model="share.text" class="form-control" rows="4" style="overflow:hidden; resize:none"></textarea>

                        <div ng-if="share.canEdit"
                             style="text-align:right; margin-top: 15px">

                            <button ng-if="!form.editState"
                                    class="btn btn-default btn-sm"
                                    ng-click="edit()">
                                <span class="tsi tsi-pencil"></span> <?= _t('share', 'Edit') ?>
                            </button>
                        </div>

                    </div>

                    <div ng-if="isPhotoBlockVisible()"
                         style="background-color: #f6f8fa; margin: 0 -15px; padding: 10px 15px;">

                        <!-- PHOTO HEADER -->


                        <div ng-if="isCanSelectPhoto()" class="clearfix" style="margin: 5px 0">

                            <h4 ng-if="showSelectPhotoHeader()" class="m-t0 pull-left"><?= _t('share', 'Select photo') ?></h4>

                            <!-- UPLOAD PHOTO CONTROLS-->


                            <button ng-if="share.canEdit && !form.customPhoto.length"
                                    dropzone-button="form.customPhoto"
                                    accepted-files="<?= FileTypesHelper::dropzoneAcceptStr(FileTypesHelper::IMAGES_EXTENSIONS) ?>"
                                    on-accept="loadCustomPhoto(file)"
                                    max-files="1"
                                    class="btn btn-link btn-sm pull-right"
                                    style="padding-top: 0; margin-top: -4px; position: relative; padding-right: 0">
                                <span class="tsi tsi-plus"></span> <?= _t('share', 'Upload photo') ?>
                            </button>


                            <button ng-if="share.canEdit && form.customPhoto.length"
                                    ng-click="removeUploadedImage()"
                                    class="btn btn-link btn-sm pull-right"
                                    style="padding-top: 0; margin-top: -4px; position: relative; padding-right: 0">
                                <span class="tsi tsi-plus"></span> <?= _t('share', 'Remove photo') ?>
                            </button>

                        </div>

                        <!-- CUSTOM UPLOAD PHOTO -->

                        <div ng-if="form.customPhoto.length"
                             style="padding-bottom: 15px">

                            <img style="border-radius: 5px"
                                 ng-src="{{form.customPhotoSrc}}" width="120">

                        </div>


                        <!-- REVIEW FILES -->


                        <div ng-if="!form.customPhoto.length"
                             class="ps-profile-user-review__user-models swiper-container">
                            <div class="swiper-wrapper">
                                <div ng-repeat="file in share.review.files" class="ps-profile-user-review__user-models-pic swiper-slide" style="position: relative">

                                    <!--                                    <div ng-if="isCanSelectPhoto()"-->
                                    <!--                                         style="position: absolute; top: 5px; left: 5px; background: white; border: 5px solid white; border-radius: 50%; line-height: 0">-->
                                    <!--                                        <input ng-model="share.photo_id"-->
                                    <!--                                               ng-value="file.id"-->
                                    <!--                                               type="radio" style="margin: 0; padding: 0;"-->
                                    <!--                                               name="photo-select-radio">-->
                                    <!--                                    </div>-->

                                    <div ng-if="isCanSelectPhoto()"
                                         class="radio radio-primary"
                                         style="position: absolute;
                                                top: 5px;
                                                left: 0;
                                                right: 0;
                                                bottom: 0;
                                                padding-left: 35px;
                                                margin: 0;">
                                        <input
                                                ng-model="share.photo_id"
                                                ng-value="file.id"
                                                type="radio"
                                                name="photo-select-radio"
                                                style="
                                                    position: absolute;
                                                    width: 100%;
                                                    height: 100%;
                                                    top: 0;
                                                    left: 0;
                                                    margin: 0;">
                                        <label for="photo-select-radio"></label>
                                    </div>


                                    <img ng-src="{{file.previewUrl}}">
                                </div>
                            </div>
                            <div class="ps-profile-user-review__user-models-scrollbar swiper-scrollbar"></div>
                        </div>


                    </div>


                    <!-- SHARE BUTTONS -->

                    <div class="clearfix">
                        <div class="pull-right m-t20">

                            <h4 style="display: inline-block;vertical-align: top;margin: 5px 15px 5px 0;"><?= _t('share', 'Share on') ?></h4>

                            <button loader-click="save('facebook', $event)" class="btn btn-primary btn-ghost btn-sm m-r10 m-b10" title="Facebook">
                                <svg style="width: 16px;height: 16px;fill: currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                                    <path d="M5.9 3.1v2.2H4.3V8h1.6v8h3.3V8h2.2s.2-1.3.3-2.7H9.2V3.5c0-.3.4-.6.7-.6h1.8V.1H9.2c-3.4-.1-3.3 2.6-3.3 3z"/>
                                </svg>
                            </button>

                            <button loader-click="save('linkedin', $event)" class="btn btn-primary btn-ghost btn-sm m-r10 m-b10" title="LinkedIn">
                                <svg style="width: 16px;height: 16px;fill: currentColor" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 430.117 430.117">
                                    <path d="M430.117 261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477V420.56h-92.219s1.242-251.285 0-277.32h92.21v39.309c-.187.294-.43.611-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zM52.183 9.558C20.635 9.558 0 30.251 0 57.463c0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zM5.477 420.56h92.184V143.24H5.477v277.32z"/>
                                </svg>
                            </button>

                            <button loader-click="save('reddit')" class="btn btn-primary btn-ghost btn-sm m-r10 m-b10" title="Reddit">
                                <svg style="width: 16px;height: 16px; fill: currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                                    <circle cx="5.6" cy="9" r="1.1"/>
                                    <path d="M16 8.2c0-1.3-1-2.4-2.3-2.4-.5 0-.9.1-1.2.4-1.1-.6-2.5-1-4-1.1l.9-2.5 1.8.4v.2c0 1 .8 1.8 1.8 1.8s1.8-.8 1.8-1.8S14 1.4 13 1.4c-.7 0-1.2.3-1.5.9l-2.7-.8L7.5 5c-1.5.1-2.9.5-4 1.2h.1c-.4-.3-.8-.4-1.3-.4C1 5.8 0 6.8 0 8.2c0 .9.5 1.7 1.2 2.1.3 2.5 3.3 4.5 6.9 4.5 3.6 0 6.6-2 6.9-4.5.6-.5 1-1.3 1-2.1zm-3.1-6.1c.6 0 1 .5 1 1 0 .6-.5 1-1 1-.6 0-1-.5-1-1s.4-1 1-1zM2.3 6.6c.2 0 .4 0 .5.1-.8.7-1.4 1.6-1.6 2.5-.2-.2-.4-.6-.4-1 0-.9.7-1.6 1.5-1.6zM8 13.9c-3.4 0-6.1-1.8-6.1-4s2.7-4 6.1-4c3.4 0 6.1 1.8 6.1 4s-2.7 4-6.1 4zm6.9-4.7c-.2-1-.8-1.8-1.7-2.5.1 0 .3-.1.5-.1.8 0 1.5.7 1.5 1.6 0 .3-.1.7-.3 1z"/>
                                    <circle cx="10.3" cy="9" r="1.1"/>
                                    <path d="M10.1 11.4c-2.3 1.5-4.1.1-4.2 0-.2-.1-.4-.1-.6.1-.1.2-.1.4.1.6 0 0 1.1.8 2.6.8.8 0 1.6-.2 2.6-.8.2-.1.2-.4.1-.6-.2-.1-.4-.2-.6-.1z"/>
                                </svg>
                            </button>

                            <button loader-click="save('facebook_im')" class="btn btn-primary btn-ghost btn-sm m-r10 m-b10" title="Facebook Messenger">
                                <svg style="width: 16px;height: 16px;fill: currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                                    <path d="M8 .6C3.6.6 0 3.3 0 7.4c0 2.3 1.2 4.4 2.9 5.8V16l2.8-1.5c.7.2 1.5.3 2.3.3 4.4 0 8-3.3 8-7.4S12.4.6 8 .6zm.8 9.4l-2-2.2-4 2.2 4.4-4.6 2.1 2.2 3.9-2.2L8.8 10z"/>
                                </svg>
                            </button>

                            <button loader-click="save('link', $event)" class="btn btn-primary btn-ghost btn-sm m-b10" title="Link">
                                <svg style="width: 16px;height: 16px;fill: currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                                    <path d="M14.9 2.5c-.9-.9-1.9-1.2-3-1-.9.1-1.8.7-2.7 1.5L7.4 4.9c-.3.3-.5.6-.8.9-1.1 0-2.2.6-3.2 1.6L1.6 9.2c-1 1-1.6 2.2-1.6 3.3 0 .9.4 1.7 1.1 2.4.7.7 1.6 1.1 2.4 1.1H4c.9-.1 1.9-.7 2.7-1.6l1.8-1.8c.3-.3.5-.6.8-.9 1.1 0 2.2-.6 3.2-1.6l1.8-1.8c1-1 1.6-2.2 1.6-3.3.1-.9-.3-1.7-1-2.5zm-1.4 4.8l-1.8 1.8c-.5.5-1.1.9-1.6 1.1 0-.1.1-.2.1-.3.2-1-.1-2-.9-2.9l-1 .9c1.3 1.3.4 2.8-.6 3.8l-1.8 1.8c-.7.7-1.4 1.1-2 1.2-.7.1-1.3-.1-1.9-.7-.5-.5-.7-1-.7-1.5 0-.9.7-1.8 1.2-2.4l1.8-1.8c.5-.6 1.1-1 1.6-1.2 0 .1-.1.2-.1.3-.2 1 .1 2 .9 2.9l.9-.8c-1.2-1.3-.3-2.8.7-3.8l1.8-1.8c.7-.7 1.4-1.1 2-1.2.7-.1 1.3.1 1.9.7.5.5.7 1 .7 1.5.1 1-.6 1.9-1.2 2.4z"/>
                                </svg>
                            </button>
                        </div>

                    </div>

                    <div class="clearfix">
                        <div class="pull-right m-t20">
                            <?= _t('share', 'Share the affiliate link and earn a 50% recurring commission from each completed order. Make money while you sleep.') ?>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" id='affiliateUrl' class="form-control" ng-model="share.affiliateUrl" readonly>
                            </div>
                            <div class="col-md-4">
                                <button ng-click="copyAffiliateUrlToClipboard()" class="btn btn-primary btn-ghost btn m-b10" title="Link">
                                    <?= _t('share', 'Copy'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
