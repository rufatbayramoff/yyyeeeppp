<?php

use common\components\ArrayHelper;
use common\models\GeoCountry;
use common\models\Ps;
use common\models\User;
use common\modules\company\repositories\OrderRepository;
use common\modules\company\serializers\CompanyServiceLocationSerializer;
use frontend\assets\DropzoneAsset;
use frontend\assets\SocialButtonsAsset;
use frontend\models\user\CompanyPublicPageEntity;
use lib\geo\GeoNames;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Menu;

/** @var User $userModel */
/** @var Ps $ps */
/** @var View $this */
/** @var $companyPublicPageEntity CompanyPublicPageEntity */
/** @var ActiveDataProvider $dataProvider */
if (array_key_exists('isWidget', $this->params) && $this->params['isWidget']) {
    return;
}
$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router', 'geo', 'maps'])
    ->constant(
        'globalConfig',
        [
            'facebook_app_id' => param('facebook_app_id')
        ]
    )
    ->directive(['google-map-markers', 'google-address-input', 'dropzone-button'])
    ->constants([
        'locations' => CompanyServiceLocationSerializer::serialize($ps),
        'countries' => ArrayHelper::toArray(
            GeoNames::getAllCountries(),
            [GeoCountry::class => ['id', 'iso_code', 'title', 'is_easypost_domestic', 'is_easypost_intl', 'is_europa']]
        ),
    ])
    ->controllerParams([
        'x'           =>  'y',
        'location'    =>  $ps->calculatedLocation,
    ])
    ->controller([
        'ps/PsCatalogController',
        'ps/PsProfileController',
        'store/common-models',
        'review/shareReview',
        'preorder/preorder-models',
        'preorder/preorder-service',
        'product/productForm',
        'product/productModels',
    ]);

echo $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-modal.php');

$this->registerAssetBundle(SocialButtonsAsset::class);
$this->registerAssetBundle(DropzoneAsset::class);

$this->params['fluid-layout'] = true;


$userLink = $companyPublicPageEntity->psUrl;
$bgUrl = $companyPublicPageEntity->bgUrl;
$avatar = $companyPublicPageEntity->avatarUrl;
$subTitle = $companyPublicPageEntity->psTitle;
$printersCount = $companyPublicPageEntity->printersCount;
$storeModelsCount = $companyPublicPageEntity->storeModelsCount;
$sendMessageUrl = $companyPublicPageEntity->sendMessageUrl;
$orderRepository = Yii::createObject(OrderRepository::class);
$ogTags = [
    'og:url'         => param('server') . $companyPublicPageEntity->psUrl,
    'og:title'       => H($companyPublicPageEntity->psTitle),
    'og:description' => H($companyPublicPageEntity->psTitle),
    'og:image'       => $companyPublicPageEntity->bgUrl,
    'og:type'        => 'website'
];
$this->params['ogtags'] = $ogTags;
$this->registerLinkTag(
    [
        'rel'  => 'image_src',
        'href' => $companyPublicPageEntity->bgUrl
    ],
    true
);
?>
<div class="user-profile">
    <div class="user-profile__cover" style="background-image: url('<?= $bgUrl; ?>')"></div>
    <div class="container relative">
        <div class="row">
            <div class="col-sm-9">
                <div class="user-profile__userinfo">
                    <a class="user-profile__avatar" href="<?= $companyPublicPageEntity->psUrl; ?>">
                        <img src="<?php echo $companyPublicPageEntity->avatarUrl;?>" alt="<?= H($ps->title); ?> Logo">
                    </a>
                    <div class="user-profile__username">
                        <?= $subTitle; ?>
                    </div>
                    <div class="user-profile__cert">
<!--                        <div class="label label-success">Pro</div> service-->
                    </div>
                    <div class="user-profile__user-rating">
                    </div>

                    <div class="user-profile__stats">

                        <?php if($companyPublicPageEntity->reviewsCount > 0):?>
                            <div class="user-profile__stats-item">
                                <div class="user-profile__stats-value"><?php echo $companyPublicPageEntity->reviewsCount ?></div>
                                <a href="<?php echo Url::to(['c/public/reviews', 'username' => $companyPublicPageEntity->url()]) ?>"><?= _t('public.store', '{reviews,plural,=1{Review} other{Reviews}}',['reviews' => $companyPublicPageEntity->reviewsCount]) ?></a>
                            </div>
                        <?php endif?>
                        <div class="user-profile__stats-item">
                            <div class="user-profile__stats-value"><?php echo $orderRepository->completedOrder($ps)?></div>
                            <span class="m-r5"><?php echo _t('public.store', '{orders,plural,=0{Order} =1{Order} other{Orders}} done',['orders' => $orderRepository->completedOrder($ps)]) ?></span>
                            <span class="tsi tsi-info-c" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= _t('public.store', 'Orders over the last 12 months') ?>"></span>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($ps->canPreorder()):?>
                <div ng-controller="PsCatalogController" class="col-sm-3">
                    <div class="user-profile__actions">
                        <a loader-click="openCreatePreorder(<?= $ps->id; ?> )" class="btn btn-danger"><?= _t('public.store', 'Get a Quote') ?></a>
                    </div>
                </div>
            <?php endif;?>
        </div>

    </div>
</div>
<div class="nav-tabs__container nav-tabs__container--ps-pub">
    <div class="container">
        <?php echo Menu::widget([
            'options' => [
                'class' => 'nav nav-tabs'
            ],
            'items'   => [
                [
                    'url'     => ['c/public/profile','username' => $companyPublicPageEntity->url()],
                    'label'   => _t('front.my', 'About'),
                    'visible' => true
                ],
                [
                    'url'     => ['c/public/reviews','username' => $companyPublicPageEntity->url()],
                    'label'   => _t('front.my', 'Reviews'),
                    'visible' => $companyPublicPageEntity->reviewsCount > 0
                ],
                [
                    'url'     => ['c/public/print-services', 'username' => $companyPublicPageEntity->url()],
                    'label'   => _t('front.my', '3D Printing'),
                    'visible' => $printersCount > 0
                ],
                [
                    'url'     => ['c/public/cnc','username' => $companyPublicPageEntity->url()],
                    'label'   => _t('front.my', 'CNC Machining'),
                    'visible' => $companyPublicPageEntity->cncCount > 0
                ],
                [
                    'url'     => ['c/public/cutting','username' => $companyPublicPageEntity->url()],
                    'label'   => _t('front.my', 'Cutting'),
                    'visible' => $companyPublicPageEntity->cuttingCount > 0
                ],
                [
                    'url'     => ['c/public/designer', 'username' => $companyPublicPageEntity->url()],
                    'label'   => _t('front.my', '3D Models'),
                    'visible' => $storeModelsCount > 0
                ],
                [
                    'url'     => ['c/public/products','username' => $companyPublicPageEntity->url()],
                    'label'   => _t('front.my', 'Store'),
                    'visible' => $companyPublicPageEntity->productsCount > 0
                ],
                [
                    'url'     => ['c/public/services','username' => $companyPublicPageEntity->url()],
                    'label'   => _t('front.my', 'Services'),
                    'visible' => $companyPublicPageEntity->companyServicesCount > 0,
                    'active'=> (Yii::$app->controller->action->id === 'services' || Yii::$app->controller->action->id === 'service-view')
                ]
            ]
        ]) ?>
    </div>
</div>