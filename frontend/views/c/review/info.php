<?php
/** @var StoreOrderReview $review */

use common\models\StoreOrderReview;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;

?>
<div class="ps-profile-user-review__user-info">
    <?php
    if ($review->order->user->allowViewPublicProfile()) {
        ?>
        <?php $userProfileLink = $link = UserUtils::getUserPublicProfileUrl($review->order->user); ?>
        <a href="<?= $userProfileLink ?>" class="ps-profile-user-review__user-avatar">
            <?= UserUtils::getAvatarForUser($review->order->user, 'store'); ?>
        </a>
        <a href="<?= $userProfileLink ?>" class="ps-profile-user-review__user-name">
            <?= \H(UserFacade::getFormattedUserName($review->order->user)); ?>
        </a>
        <?php
    } elseif ($review->order->user->isDeleted()) {
        ?>
        <div class="ps-profile-user-review__user-name">
            <?= _t('site.common', 'Deleted'); ?>
        </div>
        <?php
    } else { ?>
        <div class="ps-profile-user-review__user-avatar">
            <?= UserUtils::getAvatarForUser($review->order->user, 'store'); ?>
        </div>
        <div class="ps-profile-user-review__user-name">
            <?= \H(UserFacade::getFormattedUserName($review->order->user)); ?>
        </div>
        <?php
    }
    ?>
</div>
<div class="ps-profile-user-review__user-date">
    <?= Yii::$app->formatter->asDate($review->created_at) ?>
</div>