<?php
/** @var StoreOrderReview $review */
use frontend\widgets\ReviewStarsWidget;

?>
<?= ReviewStarsWidget::widget(['rating' => $review->getRating(), 'dataSize' => 'xs']); ?>
<button type="button" class="btn btn-info btn-xs star-rating__dropdown collapsed" data-toggle="collapse" data-target="#rating-<?=$review->id;?>" aria-expanded="false">
    <span class="tsi tsi-down"></span>
</button>

<div id="rating-<?=$review->id;?>" class="star-rating__dropdown-body collapse">
    <table class="form-table">
        <?php $ratingDetails = $review->getRatingDetails(); ?>
        <?php foreach ($ratingDetails as $label => $value): ?>

            <tr class="form-table__row">
                <td class="form-table__label">
                    <?=$label;?>
                </td>
                <td class="form-table__data">
                    <?= ReviewStarsWidget::widget(['rating' => $value]); ?>
                </td>
            </tr>

        <?php endforeach; ?>
    </table>
</div>