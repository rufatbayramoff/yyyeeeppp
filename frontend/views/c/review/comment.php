<?php
/** @var StoreOrderReview $review */
use common\models\StoreOrderReview;
?>

<?php if ($review->comment): ?>
    <div class="ps-profile-user-review__text">
        <?= H($review->comment) ?>
    </div>
<?php endif; ?>