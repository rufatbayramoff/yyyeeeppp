<?php

use common\components\FileTypesHelper;
use common\models\CompanyBlock;
use common\models\CompanyService;
use common\models\Ps;
use common\models\PsCncMachine;
use common\modules\company\serializers\CompanyServiceSerializer;
use frontend\assets\DropzoneAsset;
use frontend\assets\ServiceViewAsset;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\user\CompanyPublicPageEntity;
use frontend\models\user\UserFacade;
use frontend\modules\product\widgets\ProductSchemaWidget;
use frontend\widgets\SwipeGalleryWidget;
use yii\db\ActiveRecord;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var $companyService CompanyService */
/* @var $this View */
/* @var $seeAlsoProducts array */
/* @var $model array|null|ActiveRecord */
/* @var $seeAlso array */

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->directive('dropzone-button')
    ->controller([
        'preorder/preorder-models',
        'preorder/preorder-service'
    ]);
$this->registerAssetBundle(DropzoneAsset::class);
ServiceViewAsset::register($this);
echo $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-modal.php');
$user = UserFacade::getCurrentUser();
if ($this->context->seo) {
    $this->title = $this->context->seo->title;
}
$this->title     = $companyService->getTitleLabel() . ' - Treatstock';
$categories      = [];
$currentCategory = $companyService->category;
$categories      = array_reverse($categories);

$curCategory        = $companyService->category;
$defaultDescription = '';
if ($curCategory) {
    $currentCategory = $curCategory;
    do {
        $categories[]    = $currentCategory;
        $currentCategory = $currentCategory->parentCategory;
    } while ($currentCategory);
    $categories = array_reverse($categories);

}
$defaultTitle = H($companyService->title);

/** @var $ps Ps */
/** @var $companyPublicPageEntity CompanyPublicPageEntity */
/** @var PsCncMachine[] $psCncMachines */

echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $companyService->ps->user, 'ps' => $companyService->company, 'companyPublicPageEntity' => $companyPublicPageEntity]);

$videos           = $companyService->getProductVideos();
$certDataProvider = $companyService->getCertificationsDataProvider();
$publicBlocks     = $companyService->getPublicBlocks();

echo ProductSchemaWidget::widget([
    'product' => $companyService,
    'price'   => $companyService->getSinglePriceMoney(),
    'rating'  => $companyService->company->psCatalog ?
        \frontend\controllers\catalog\actions\models\Rating::create($companyService->company->psCatalog->rating_count, $companyService->company->psCatalog->rating_avg) :
        null
]);

$breadcrumps                                                        = [];
$breadcrumps[ $companyService->ps->title] = $companyPublicPageEntity->psUrl;
$breadcrumps[ _t('site.user', 'Services')] = $companyPublicPageEntity->psUrl.'/services';
$breadcrumps[$companyService->title] = \Yii::$app->request->url;
?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb m-t10">
                <?php
                foreach ($breadcrumps as $breadcrumpText => $breadcrumpUrl) {
                    echo '<li>';
                    echo $breadcrumpUrl ? '<a href="' . HL($breadcrumpUrl) . '">' : '';
                    echo H($breadcrumpText);
                    echo $breadcrumpUrl ? '</a>' : '';
                    echo '</li>';
                }
                ?>
            </ol>
            <?php
            echo \frontend\widgets\BreadcrumbsSchemaWidget::widget([
                'breadcrumpsItems' => $breadcrumps
            ]);
            ?>
        </div>
    </div>
</div>
<div class="container product-page" id="productBlockSection0">
    <h1 class="ps-pub-profile__header">
        <?= $companyService->title ?>
    </h1>
    <div class="row">
        <div class="col-sm-7">
            <div class="designer-card p-t0 p-l0 p-r0 p-b0 m-b30">
                <div class="fotorama"
                     data-width="100%"
                     data-height="450px"
                     data-arrows="true"
                     data-click="true"
                     data-nav="thumbs"
                     data-thumbmargin="10"
                     data-thumbwidth="80px"
                     data-thumbheight="60px"
                     data-allowfullscreen="true">
                    <?php
                    if ($companyImages = $companyService->getImages()) {
                        foreach ($companyImages as $imageFile) {
                            echo '<img src="' . HL($imageFile->getFileUrl()) . '" alt="' . H($imageFile->getFileName()) . '">';
                        }
                    } else {
                        ?>
                        <div class="product-page__empty-prod-img">
                            <?= _t('public.product', 'Images not uploaded'); ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="model-tags">
                    <?php
                    foreach ($companyService->tags as $tag) {
                        echo ' <span class="label label-default">#' . H($tag->text) . '</span>';
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-5 wide-padding wide-padding--left">

            <div class="designer-card product-page-info">
                <table class="form-table form-table--top-label">
                    <?php $companyServiceSerialize = CompanyServiceSerializer::serialize($companyService) ?>
                    <?php foreach ($companyServiceSerialize['properties'] as $k => $v):
                        if (empty($v)) {
                            continue;
                        }
                        ?>
                        <tr class="form-table__row">
                            <td class="form-table__label">
                                <b><?= H($k); ?></b>
                            </td>
                            <td class="form-table__data">
                                <?= H($v); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>

                <div class="m-b10" ng-controller="PsCatalogController">

                    <a class="btn btn-danger" href="<?= HL($companyService->getCreateQuoteUrl()); ?>"><?= _t('site.ps', 'Get a Quote'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="product-page__nav">
    <div class="product-page__nav-inner" id="jsprod-nav">
        <div class="container ">
            <div id="jsprod-nav-tabs">
                <ul class="nav nav-tabs nav-tabs--secondary productBlockTabs" role="tablist">
                    <li role="presentation" class="productBlockTab productBlockTab0 active" data-block-id="0">
                        <a href="#infoBlockSection" role="tab">Service information</a>
                    </li>
                    <?php if ($videos): ?>
                        <li class="productBlockTab productBlockTab1" data-block-id="1" role="presentation">
                            <a href="#videoBlockSection" role="tab">Videos</a>
                        </li>
                    <?php endif; ?>
                    <?php if ($certDataProvider->getTotalCount() > 0): ?>
                        <li class="productBlockTab productBlockTab2" data-block-id="2" role="presentation">
                            <a href="#certBlockSection" role="tab">Certifications</a>
                        </li>
                    <?php endif; ?>
                    <?php if (!empty($publicBlocks)): ?>
                        <?php foreach ($publicBlocks as $key => $companyServiceBlock): ?>
                            <li class="productBlockTab productBlockTab3" data-block-id="<?php echo $key + 3 ?>" role="presentation">
                                <a href="#publicBlockSection<?php echo $key ?>" role="tab"><?= H($companyServiceBlock->title) ?></a>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container ">
    <div class="row">
        <div class="col-sm-9 wide-padding wide-padding--right">
            <div class="product-page__section ugc-content productBlock0 m-t0">
                <?php

                $userProperties       = $companyService->getCustomProperties();
                $userPropertiesChunks = $userProperties ? array_chunk($userProperties, ceil(count($userProperties) / 2), true) : [];
                ?>

                <div class="row m-b10">
                    <?php foreach ($userPropertiesChunks as $userPropertiesChunk): ?>

                        <div class="col-md-6">
                            <?php foreach ($userPropertiesChunk as $propertyKey => $propertyValue): ?>
                                <div class="row">
                                    <div class="col-xs-5 col-sm-4 col-md-5 p-r0 m-b10"><b><?= H($propertyKey); ?></b></div>
                                    <div class="col-xs-7 col-sm-8 col-md-7 m-b10"><?= H($propertyValue); ?></div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                    <?php endforeach; ?>
                </div>
                <h3 id="infoBlockSection" class="m-b0"><?= _t('public.ps', 'Description'); ?></h3>
                <div>
                    <?= H(strip_tags($companyService->getDescription())) ?>
                </div>
            </div>

            <?php if ($videos): ?>
                <div id="videoBlockSection" class="product-page__section">
                    <h3 class="product-page__section-title"><?= _t('public.product', 'Videos'); ?></h3>
                    <div class="row">
                        <?php foreach ($videos as $video): ?>
                            <div class="col-sm-4">
                                <div class="video-container">
                                    <object data="https://www.youtube.com/embed/<?= $video['videoId']; ?>"></object>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php
            if ($certDataProvider->getTotalCount() > 0): ?>
                <div class="product-page__section">
                    <h3 id="certBlockSection" class="product-page__section-title"><?= _t('company.public', 'Certifications'); ?></h3>
                    <div class="table-responsive">
                        <?php echo GridView::widget(
                            [
                                'dataProvider' => $certDataProvider,
                                'columns'      => [
                                    [
                                        'format' => 'raw',
                                        'value'  => function ($cert) {

                                            if (FileTypesHelper::isImage($cert->file)) {
                                                $downloadLink = ImageHtmlHelper::getClickableThumb($cert->file);
                                            } else {
                                                $imgHtml      = "<img width='85' src='/static/images/certificate.svg' style='margin-bottom: -5px;'/> ";
                                                $downloadLink = Html::a($imgHtml, $cert->file->getFileUrl());
                                            }
                                            return $downloadLink;
                                        }
                                    ],
                                    [
                                        'attribute' => 'title',
                                        'format'    => 'raw',
                                        'label'     => _t('mybusiness.public', 'Certification'),
                                        'value'     => function ($cert) {
                                            $downloadLink = Html::a(_t('site.product', 'Download'), $cert->file->getFileUrl());
                                            if (FileTypesHelper::isImage($cert->file)) {
                                                $downloadLink = '<br />' . ImageHtmlHelper::getClickableThumb($cert->file);
                                            }

                                            return '<b>' . H($cert->title) . '</b>';
                                        }
                                    ],
                                    'certifier',
                                    'application',
                                    'issue_date:date',
                                    'expire_date:date',
                                ],
                                'tableOptions' => [
                                    'class' => 'table table-bordered product__cert-table'
                                ],
                            ]
                        ); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php foreach ($publicBlocks as $key => $companyServiceBlock): ?>
                <div class="product-page__section ugc-content">
                    <h3 id="publicBlockSection<?php echo $key ?>" class="product-page__section-title"><?= H($companyServiceBlock->title); ?></h3>
                    <?= $companyServiceBlock->content; ?>
                    <div class="m-t10">
                        <?= $companyServiceBlock->imageFiles ?
                            SwipeGalleryWidget::widget([
                                'files'            => $companyServiceBlock->imageFiles,
                                'thumbSize'        => [230, 180],
                                'containerOptions' => ['class' => 'picture-slider'],
                                'itemOptions'      => ['class' => 'picture-slider__item'],
                                'scrollbarOptions' => ['class' => 'picture-slider__scrollbar'],
                            ]) : '';
                        ?>
                    </div>
                    <?php if ($companyServiceBlock instanceof CompanyBlock && $videos = $companyServiceBlock->getCompanyBlockVideos()): ?>
                        <div class="row">
                            <?php foreach ($videos as $video): ?>
                                <div class="col-sm-4">
                                    <div class="video-container">
                                        <object data="https://www.youtube.com/embed/<?= $video['videoId']; ?>"></object>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
