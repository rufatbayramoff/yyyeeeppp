<?php

use common\components\ArrayHelper;
use common\components\DateHelper;
use common\components\FileTypesHelper;
use common\components\helpers\ResponseTimeHelper;
use common\models\Company;
use common\models\CompanyBlock;
use common\models\CompanyCertification;
use common\models\CompanyServiceCertification;
use common\models\File;
use common\models\Model3dViewedState;
use common\models\ProductCertification;
use common\models\StoreOrderReview;
use common\models\StoreUnit;
use common\models\UserLocation;
use common\modules\payment\components\BonusCalculator;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\user\CompanyPublicPageEntity;
use frontend\models\user\UserFacade;
use frontend\widgets\Model3dViewedStateWidget;
use frontend\widgets\PsPrintServiceReviewStarsWidget;
use frontend\widgets\SwipeGalleryWidget;
use frontend\widgets\UploadWidget;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var $user common\models\User */
/** @var $companyPublicPageEntity CompanyPublicPageEntity */
/** @var Model3dViewedState $model3dViewedState */
/** @var $storeUnits StoreUnit[] */
/** @var array $params */
/** @var int|null $selectedPrinter */
/** @var Company $ps */
/** @var File[] $lastReviewFiles */

$user          = $ps->user;
$currentUser   = UserFacade::getCurrentUser();
$companyBlocks = $ps->getCompanyBlocks()
    ->orderBy('position')
    ->andWhere(['is_public_profile' => 1, 'is_visible' => 1])
    ->all();
$userProfile   = $user->userProfile;
echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $companyPublicPageEntity]);
echo $this->render('@frontend/views/c/review-share-modal.php');
?>

<div ng-controller="PsProfileController" class="container" itemscope itemtype="http://schema.org/Service">

    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb m-t10">

            </ol>
        </div>
    </div>

    <div class="row ps-pub-profile">

        <div class="col-sm-8 ps-pub-profile__data-container wide-padding wide-padding--right">
            <?php echo UploadWidget::widget(
                [
                    'additionalParams' => ['utmSource' => 'fromPrinter', 'psId' => $ps->id],
                    'needNewWindow'    => true,
                    'isWidget'         => 0
                ]
            ); ?>
            <div class="row">
                <div class="col-sm-5">
                    <?php if($model3dViewedState !== null):?>
                        <?php echo  Model3dViewedStateWidget::widget(['model3dViewedState' => $model3dViewedState, 'psPrinter' => $selectedPrinter])?>
                    <?php endif;?>
                </div>
            </div>

            <h1 class="ps-pub-profile__header">
                <?= _t('public.ps', 'About') . ' ' . H($ps->title); ?>
            </h1>

            <div class="ps-pub-profile__ps-text" itemprop="description">
                <?= nl2br(H($ps->description)); ?>
            </div>

            <div class="footer__social m-b20">
                <?php if ($ps->facebook): ?>
                    <a class="social-ico-fb footer__social-item t-footer-follow__fb"
                       href="<?= $ps->getFacebookFormatLink(); ?>" target="_blank" title="Facebook">
                        <svg class="soc-ico" xmlns="http://www.w3.org/2000/svg"
                             width="96.124px" height="96.123px"
                             viewBox="0 0 96.124 96.123">
                            <path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803   c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654   c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246   c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z"></path>
                        </svg>
                        <!-- Facebook-->
                    </a>
                <?php endif; ?>
                <?php if ($ps->twitter): ?>
                    <a class="social-ico-tw footer__social-item t-footer-follow__twit"
                       href="<?= $ps->getTwitterFormatLink(); ?>" target="_blank" title="Twitter">
                        <svg class="soc-ico" xmlns="http://www.w3.org/2000/svg"
                             version="1.1" viewBox="0 0 612 612">
                            <path d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411    c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513    c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101    c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104    c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194    c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485    c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z"></path>
                        </svg>
                        <!-- Twitter-->
                    </a>
                <?php endif; ?>
                <?php if ($ps->instagram): ?>
                    <a class="social-ico-insta footer__social-item t-footer-follow__inst"
                       href="<?= $ps->getInstagramFormatLink(); ?>" target="_blank" title="Instagram">
                        <svg class="soc-ico" xmlns="http://www.w3.org/2000/svg"
                             version="1.1" width="169.063px"
                             height="169.063px"
                             viewBox="0 0 169.063 169.063">
                            <path d="M122.406,0H46.654C20.929,0,0,20.93,0,46.655v75.752c0,25.726,20.929,46.655,46.654,46.655h75.752   c25.727,0,46.656-20.93,46.656-46.655V46.655C169.063,20.93,148.133,0,122.406,0z M154.063,122.407   c0,17.455-14.201,31.655-31.656,31.655H46.654C29.2,154.063,15,139.862,15,122.407V46.655C15,29.201,29.2,15,46.654,15h75.752   c17.455,0,31.656,14.201,31.656,31.655V122.407z"></path>
                            <path d="M84.531,40.97c-24.021,0-43.563,19.542-43.563,43.563c0,24.02,19.542,43.561,43.563,43.561s43.563-19.541,43.563-43.561   C128.094,60.512,108.552,40.97,84.531,40.97z M84.531,113.093c-15.749,0-28.563-12.812-28.563-28.561   c0-15.75,12.813-28.563,28.563-28.563s28.563,12.813,28.563,28.563C113.094,100.281,100.28,113.093,84.531,113.093z"></path>
                            <path d="M129.921,28.251c-2.89,0-5.729,1.17-7.77,3.22c-2.051,2.04-3.23,4.88-3.23,7.78c0,2.891,1.18,5.73,3.23,7.78   c2.04,2.04,4.88,3.22,7.77,3.22c2.9,0,5.73-1.18,7.78-3.22c2.05-2.05,3.22-4.89,3.22-7.78c0-2.9-1.17-5.74-3.22-7.78   C135.661,29.421,132.821,28.251,129.921,28.251z"></path>
                        </svg>
                        <!-- Instagram-->
                    </a>
                <?php endif; ?>
            </div>

            <?php if ($ps->calculatedLocation): ?>
            <h3 class="m-t30 m-b10"><?= _t('mybusiness.public', 'Shipping options'); ?></h3>
            <div class="row">
                <div class="col-md-5 col-lg-4">

                        <div class="m-b30">
                            <div>
                                <span class="tsi tsi-map-marker m-r5"></span><b><?= _t('mybusiness.public', 'Location'); ?></b>
                            </div>
                            <?= UserLocation::formatLocation($ps->calculatedLocation, '%city%, %region%, %country%'); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php
            $companyProperties = [
                _t('mybusiness.public', 'Business type')       => H($ps->area_of_activity),
                _t('mybusiness.public', 'Ownership')           => H($ps->ownership),
                _t('mybusiness.public', 'Incoterms')           => H($ps->incoterms),
                _t('mybusiness.public', 'Total Employees')     => H($ps->total_employees),
                _t('mybusiness.public', 'Year established')    => H($ps->year_established),
                _t('mybusiness.public', 'Annual turnover ($)') => H($ps->annual_turnover),
            ];
            ?>
            <table class="form-table">
                <tbody>
                <?php foreach ($companyProperties as $propK => $propV): if (empty($propV)) {
                    continue;
                } ?>
                    <tr class="form-table__row">
                        <td class="form-table__label">
                            <b><?= $propK ?></b>
                        </td>
                        <td class="form-table__data">
                            <?= ($propV) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            $certDataProvider = CompanyCertification::getDataProvider(['company_id' => $ps->id], 100, ['sort' => false]);
            if ($certDataProvider->getTotalCount() > 0):

                ?>
                <h2><?= _t('company.public', 'Certifications'); ?></h2>
                <div class="table-responsive">
                    <?php echo GridView::widget(
                        [
                            'dataProvider' => $certDataProvider,
                            'columns'      => [
                                [
                                    'attribute' => 'file_id',
                                    'label'     => 'File',
                                    'format'    => 'raw',
                                    'value'     => function ($cert) use ($ps) {
                                        $certTitle = H($ps->title) . " - " . H($cert->title);
                                        if (FileTypesHelper::isImage($cert->file)) {
                                            $downloadLink = Html::a(
                                                Html::img(
                                                    ImageHtmlHelper::getThumbUrlForFile(
                                                        $cert->file,
                                                        ImageHtmlHelper::THUMB_SMALL,
                                                        ImageHtmlHelper::THUMB_SMALL
                                                    ),
                                                    ['alt' => $certTitle, 'title' => $certTitle]
                                                ),
                                                $cert->file->getFileUrl(), ['target' => '_blank']
                                            );
                                        } else {
                                            $imgHtml      = "<img width='85' src='/static/images/certificate.svg' title='$certTitle' style='margin-bottom: -5px;'/> ";
                                            $downloadLink = Html::a($imgHtml, $cert->file->getFileUrl(), ['title' => $cert->title]);
                                        }
                                        return $downloadLink;
                                    }
                                ],
                                [
                                    'attribute' => 'title',
                                    'label'     => _t('mybusiness.public', 'Certification')
                                ],
                                'certifier',
                                'application',
                                'issue_date:date',
                                'expire_date:date',
                            ],
                            'tableOptions' => [
                                'class' => 'table table-bordered product__cert-table'
                            ],
                        ]
                    ); ?>
                </div>
            <?php endif; ?>
            <?php
            $companyProducts  = ArrayHelper::getColumn($ps->products, 'uuid');
            $certDataProvider = ProductCertification::getDataProvider(['product_uuid' => $companyProducts], 100, ['sort' => false]);
            if ($certDataProvider->getTotalCount() > 0):
                ?>
                <h2><?= _t('company.public', 'Product Certifications'); ?></h2>
                <div class="table-responsive">
                    <?php echo GridView::widget(
                        [
                            'dataProvider' => $certDataProvider,
                            'columns'      => [
                                [
                                    'attribute' => 'file_id',
                                    'label'     => 'File',
                                    'format'    => 'raw',
                                    'value'     => function ($cert) use ($ps) {

                                        $certTitle = H($ps->title) . " - " . H($cert->title);
                                        if (FileTypesHelper::isImage($cert->file)) {
                                            $downloadLink = Html::a(
                                                Html::img(
                                                    ImageHtmlHelper::getThumbUrlForFile(
                                                        $cert->file,
                                                        ImageHtmlHelper::THUMB_SMALL,
                                                        ImageHtmlHelper::THUMB_SMALL
                                                    ),
                                                    ['alt' => $certTitle, 'title' => $certTitle, 'width' => ImageHtmlHelper::THUMB_SMALL]
                                                ),
                                                $cert->file->getFileUrl(), ['target' => '_blank']
                                            );
                                        } else {
                                            $imgHtml      = "<img width='85' src='/static/images/certificate.svg' title='$certTitle' style='margin-bottom: -5px;'/> ";
                                            $downloadLink = Html::a($imgHtml, $cert->file->getFileUrl(), ['title' => $cert->title]);
                                        }
                                        return $downloadLink;
                                    }
                                ],
                                [
                                    'attribute' => 'title',
                                    'format'    => 'raw',
                                    'label'     => _t('mybusiness.public', 'Certification'),
                                    'value'     => function (ProductCertification $cert) {
                                        return Html::a(H($cert->product->title),
                                                $cert->product->getPublicPageUrl(), ['target' => '_blank']) . '<br>' . $cert->title;
                                    }
                                ],
                                'certifier',
                                'application',
                                'issue_date:date',
                                'expire_date:date',
                            ],
                            'tableOptions' => [
                                'class' => 'table table-bordered product__cert-table'
                            ]
                        ]
                    ); ?>
                </div>
            <?php endif; ?>

            <?php
            $companyServices  = ArrayHelper::getColumn($ps->companyServices, 'id');
            $certDataProvider = CompanyServiceCertification::getDataProvider(['company_service_id' => $companyServices], 100, ['sort' => false]);
            if ($certDataProvider->getTotalCount() > 0):
                ?>
                <h2><?= _t('company.public', 'Services Certifications'); ?></h2>
                <div class="table-responsive">
                    <?php echo GridView::widget(
                        [
                            'dataProvider' => $certDataProvider,
                            'columns'      => [
                                [
                                    'attribute' => 'file_id',
                                    'label'     => 'File',
                                    'format'    => 'raw',
                                    'value'     => function ($cert) use ($ps) {

                                        $certTitle = H($ps->title) . " - " . H($cert->title);
                                        if (FileTypesHelper::isImage($cert->file)) {
                                            $downloadLink = Html::a(
                                                Html::img(
                                                    ImageHtmlHelper::getThumbUrlForFile(
                                                        $cert->file,
                                                        ImageHtmlHelper::THUMB_SMALL,
                                                        ImageHtmlHelper::THUMB_SMALL
                                                    ),
                                                    ['alt' => $certTitle, 'title' => $certTitle, 'width' => ImageHtmlHelper::THUMB_SMALL]
                                                ),
                                                $cert->file->getFileUrl(), ['target' => '_blank']
                                            );
                                        } else {
                                            $imgHtml      = "<img width='85' src='/static/images/certificate.svg' title='$certTitle' style='margin-bottom: -5px;'/> ";
                                            $downloadLink = Html::a($imgHtml, $cert->file->getFileUrl(), ['title' => $cert->title]);
                                        }
                                        return $downloadLink;
                                    }
                                ],
                                [
                                    'attribute' => 'title',
                                    'format'    => 'raw',
                                    'label'     => _t('mybusiness.public', 'Certification'),
                                    'value'     => function (CompanyServiceCertification $cert) {
                                        return Html::a(H($cert->companyService->title),
                                                $cert->companyService->getPublicPageUrl(), ['target' => '_blank']) . '<br>' . $cert->title;
                                    }
                                ],
                                'certifier',
                                'application',
                                'issue_date:date',
                                'expire_date:date',
                            ],
                            'tableOptions' => [
                                'class' => 'table table-bordered product__cert-table'
                            ]
                        ]
                    ); ?>
                </div>
            <?php endif; ?>

            <h2 class="m-t30 m-b10">
                <?= _t('site.user', 'Reviews'); ?>
                <div class="ps-profile-rating">
                </div>
            </h2>

            <?php
            /** @var StoreOrderReview[] $reviews */
            $reviews = StoreOrderReview::find()
                ->with('order.user')
                ->forPs($ps)
                ->isCanShow()
                ->orderBy('id DESC')
                ->limit(1)
                ->all();
            ?>
            <?php foreach ($reviews as $review): ?>
                <?php echo $this->render('reviewItem', ['model' => $review]); ?>
            <?php endforeach; ?>
            <?php
            if (count($reviews) > 0):
                ?>
                <a href="<?= $companyPublicPageEntity->psUrl; ?>/reviews" class="btn btn-link btn-sm p-l0 m-b20">
                    <?= _t('site.user', 'More reviews'); ?>
                </a>
            <?php else: ?>
                <p class="m-b30 text-muted"><b><?= _t('site.ps', 'No reviews found'); ?></b></p>
            <?php endif; ?>

            <?php foreach ($companyBlocks as $companyBlock): ?>
                <div class="product-page__section ugc-content">
                    <h3 class="product-page__section-title"><?= H($companyBlock->title); ?></h3>

                    <?= $companyBlock->content; ?>

                    <div class="m-t10">
                        <?= $companyBlock->imageFiles ?
                            SwipeGalleryWidget::widget([
                                'files'            => $companyBlock->imageFiles,
                                'thumbSize'        => [230, 180],
                                'containerOptions' => ['class' => 'picture-slider'],
                                'itemOptions'      => ['class' => 'picture-slider__item'],
                                'scrollbarOptions' => ['class' => 'picture-slider__scrollbar'],
                            ]) : '';
                        ?>
                    </div>
                    <?php if ($companyBlock instanceof CompanyBlock && $videos = $companyBlock->getCompanyBlockVideos()): ?>
                        <div class="row">
                            <?php foreach ($videos as $video): ?>
                                <div class="col-sm-4">
                                    <div class="video-container">
                                        <object data="https://www.youtube.com/embed/<?= $video['videoId']; ?>"></object>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

        </div>
        <div class="col-sm-4 ps-pub-profile__sidebar">
            <?php if ($ps->needClaim()): ?>
                <h3><?= _t('company.public', 'Is this your business?'); ?></h3>
                <p class="m-b30">
                    <a href="/companies/verify?id=<?= $ps->id; ?>" class="btn btn-primary btn-ghost btn-sm"
                       rel="nofollow"> <span class="tsi tsi-briefcase"></span> Claim this business</a>
                </p>
            <?php endif; ?>

            <div class="designer-card">

                <h2 class="designer-card__title"><?= _t('public.ps', 'Treatstock Achievement'); ?></h2>

                <table class="form-table form-table--top-label m-b0">

                    <tbody>
                    <?php if ($companyPublicPageEntity->reviewsCount > 0 && $ps?->psCatalog?->rating_count > 0): ?>
                        <tr class="form-table__row">
                            <td class="form-table__label">
                                <b><?= _t('public.ps', 'Customer reviews'); ?></b>
                            </td>

                            <td class="form-table__data">
                                <?php echo PsPrintServiceReviewStarsWidget::widget(['ps' => $ps, 'withSchema' => false, 'fromReviewCount' => 0]); ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php $diff = DateHelper::diffNow($ps->created_at) ?>
                    <tr class="form-table__row">
                        <td class="form-table__label">
                            <?= _t('company.public', 'Registration'); ?>
                        </td>
                        <td class="form-table__data">
                            <b><?php echo _t('front.site', '{year,plural,=0{<1 year} =1{<1 year} other{# years}}', ['year' => $diff->y]) ?></b>
                        </td>
                    </tr>
                    <?php
                    if ($ps->getCashbackPercentPlus() && BonusCalculator::isAllowBonusForUser($currentUser)) { ?>
                        <tr class="form-table__row">
                            <td class="form-table__label">
                                <?= _t('company.public', 'Bonus reward'); ?>
                            </td>
                            <td class="form-table__data">
                                <div class="order-types__bonus"><?= _t('company.public', 'Up to') ?> <?= $ps->getCashbackPercentPlus() ?>%</div>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if ($responseTime = ResponseTimeHelper::response($ps->response_time)): ?>
                        <tr class="form-table__row">
                            <td class="form-table__label p-b0">
                                <?= _t('company.public', 'Avg. response time'); ?>
                            </td>
                            <td class="form-table__data p-b0">
                                <b><?php echo $responseTime ?></b>
                            </td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>

            </div>

            <?php if ($currentUser && $ps->isActive()) {
                ?>
                <a class="btn btn-danger btn-ghost btn-block m-b30"
                   href="<?= \common\modules\instantPayment\helpers\InstantPaymentUrlHelper::getUrlForCompany($ps); ?>"><?= _t('site.ps', 'Instant payment'); ?></a>
                <?php
            }
            ?>

            <?php if (count($lastReviewFiles) > 0): ?>
                <?php $reviewsImageFiles = array_chunk($lastReviewFiles, 2) ?>
                <h3 class="m-t0"><?= _t('public.ps', 'Last produced on Treatstock'); ?></h3>
                <div class="row fb-grid">
                    <?php foreach ($reviewsImageFiles as $files): ?>
                        <div class="fb-grid__item col-xs-6">
                            <?php foreach ($files as $file): ?>
                                <?php $hash = md5($file->created_at) ?>
                                <div class="ps-pub-profile__last-prod">
                                    <a data-lightbox="review_pic" href="<?php echo ImageHtmlHelper::getThumbUrlForFile($file) ?>">
                                        <img src="<?php echo ImageHtmlHelper::getThumbUrlForFile($file, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT) . $hash ?>"
                                             class="ps-pub-profile__last-prod-img">
                                    </a>
                                    <div class="ps-pub-profile__last-prod-date"><?php echo Yii::$app->formatter->asDate($file->created_at, 'long') ?></div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

</div>


<?php if ($pictures = $ps->getPicturesFiles()): ?>
    <div class="ps-pub-portfolio">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3>
                        <?= _t('public.ps', 'Work Examples / Production Process'); ?>
                    </h3>
                </div>
            </div>
        </div>
        <div class="container p-l0 p-r0">


            <?=
            SwipeGalleryWidget::widget([
                'files'            => $pictures,
                'thumbSize'        => [230, 180],
                'containerOptions' => ['class' => 'ps-pub-portfolio__slider'],
                'itemOptions'      => ['class' => 'ps-pub-portfolio__item'],
                'scrollbarOptions' => ['class' => 'ps-pub-portfolio__scrollbar'],
            ]);
            ?>
        </div>
    </div>
<?php endif; // Work examples end ?>
