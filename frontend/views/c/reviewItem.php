<?php
/** @var StoreOrderReview $model */

/** @var StoreOrderReview $review */

use common\models\Model3dReplica;
use common\models\StoreOrderReview;
use common\services\Model3dService;
use frontend\models\user\UserFacade;
use frontend\widgets\SwipeGalleryWidget;

$review = $model;
$printer = '-';
if ($review->order->currentAttemp) {
    $orderAttemp = $review->order->currentAttemp;
    $printer = $orderAttemp->machine ? $orderAttemp->machine->getTitleLabel() : $printer;
}

/** @var Model3dReplica|null $model3dReplica */
$model3dReplica = $review->order->getFirstItem() && $review->order->getFirstItem()->hasModel() ? $review->order->getFirstItem()->model3dReplica : null;

$material = $color = null;

if ($model3dReplica && $model3dReplica->isOneTextureForKit()) {
    $material = $model3dReplica->model3dTexture->printerMaterial;
    $color = $model3dReplica->model3dTexture->printerColor;
}
$allowOrder = false;
if ($orderItem = $review->order->getFirstReplicaItem()) {
    /** @var Model3dService $model3dService */
    $model3dService = Yii::createObject(['class' => Model3dService::class, 'user' => UserFacade::getCurrentUser()]);
    $allowOrder = $model3dService->isAvailableForCurrentUser($orderItem->storeUnit->model3d);
}
?>

<div class="ps-profile-user-review" id="user-review-<?php echo $review->id; ?>">
    <div class="ps-profile-user-review__user-bar">
        <?php echo $this->render('review/info',['review' => $review])?>
        <div class="ps-profile-user-review__user-rate">
            <?php echo $this->render('review/rate',['review' => $review])?>
        </div>
    </div>
    <?php echo $this->render('review/comment',['review' => $review])?>
    <?php if ($review->canShowAnswer()): ?>
        <div class="ps-profile-user-review__text">
            <b><?= _t('site.review', 'Response'); ?></b>: <?= H($review->answer) ?>
            <?php if ($review->isAnswerRejected()): ?>
                <p class="alert alert-warning">
                    <?= _t('site.review', 'Your response was not approved by a moderator. '); ?>
                    <?php if ($review->answer_reject_comment): ?>
                        <?php echo _t('site.review', 'Reason: {reason}', ['reason' => H($review->answer_reject_comment)]); ?>
                    <?php endif; ?>
                </p>
            <?php endif; ?>
        </div>
    <?php endif; ?>


    <div class="ps-profile-user-review__data">
        <?php if ($model3dReplica): ?>
            <div class="ps-profile-user-review__data-ps">
                <table>
                    <tbody>
                    <tr>
                        <td class="ps-profile-user-review__data-label">
                            <?= _t('ps.profile', 'Printed on') ?>:
                        </td>
                        <td><?= H($printer); ?></td>
                    </tr>
                    <tr>
                        <td class="ps-profile-user-review__data-label">
                            <?= _t('ps.profile', 'Material') ?>:
                        </td>
                        <td>
                            <?= $material ? H($material->title) : _t('site.model3d', 'Different materials'); ?>
                            <?php if ($color) { ?>
                                <div title="<?=$material?H($material->title):'';?>" class="material-item">

                                    <div class="material-item__color" style="background-color: rgb(<?= $color->rgb; ?>)"></div>
                                    <div class="material-item__label"><?= H($color->title) ?></div>

                                </div>
                            <?php } else { ?>
                                <?= _t('site.model3d', 'Different colors'); ?>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>

        <?=
        SwipeGalleryWidget::widget(
            [
                'files'            => $review->reviewFilesSort,
                'thumbSize'        => [120, 120],
                'assetsByClass'    => true,
                'containerOptions' => ['class' => 'ps-profile-user-review__user-models'],
                'itemOptions'      => ['class' => 'ps-profile-user-review__user-models-pic'],
                'scrollbarOptions' => ['class' => 'ps-profile-user-review__user-models-scrollbar'],
                'alt'              => _t('ps.public', 'Review image #{id}')
            ]
        );
        ?>

    </div>

    <?php if ($review->isOwnCompanyToCurrentUser()): ?>
        <button type="button" class="btn btn-link btn-sm p-l0 m-b10 ts-ajax-modal"
                data-url="<?php echo \yii\helpers\Url::to(['/workbench/reviews/answer-review', 'review_id' => $review->id]); ?>"
                data-original-title="<?= _t('site.review', 'Reply to review'); ?>"
                data-modal-dialog-class="">
            <span class="tsi tsi-comment m-r10"></span><?= _t('site.review', 'Reply'); ?>
        </button>
    <?php endif; ?>

    <div ng-controller="ShareReview" style="display: inline-block;">

        <button type="button" class="btn btn-link btn-sm p-l0 m-b10" loader-click="shareReview(<?= $review->id ?>)">
            <span class="tsi tsi-share-2 m-r10"></span><?= _t('share', 'Share review'); ?>
        </button>


        <?php if ($allowOrder): ?>
            <a class="btn btn-link btn-sm p-l0 m-b10" href="/my/print-model3d?model3d=M:<?= $review->order->getFirstReplicaItem()->storeUnit->model3d->id; ?>">
                <span class="tsi tsi-shopping-cart"></span>
                <?= _t('share', 'Buy Now') ?></a>
        <?php endif; ?>
    </div>


</div>