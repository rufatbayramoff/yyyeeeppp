<?php

use common\models\CuttingPack;
use common\models\UserAddress;
use common\modules\cutting\models\CuttingWorkpieceMaterialGroup;
use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\widgets\Model3dUploadWidget;
use frontend\widgets\Model3dViewedStateWidget;
use lib\delivery\delivery\DeliveryFacade;
use yii\helpers\Html;

/** @var $ps \common\models\Ps */
/** @var $companyPublicPageEntity \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\CuttingMachine[] $cuttingMachines */
/** @var CuttingWorkpieceMaterialGroup[] $cuttingWorkpieceMaterialGroups */

$currentUser    = \frontend\models\user\UserFacade::getCurrentUser();

echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $companyPublicPageEntity]);
?>

<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb m-t10">
                <li><a href="<?= $companyPublicPageEntity->psUrl; ?>"><?= H($ps->title); ?></a></li>
                <li><a href="#"><?= _t('site.user', 'Cutting'); ?></a></li>
            </ol>
        </div>
    </div>

    <div class="row ps-pub-profile">
        <div class="col-sm-8 wide-padding wide-padding--right">
            <h1 class="ps-pub-profile__header">
                <?=_t('site.cutting', 'Workpiece materials')?>
            </h1>

            <div class="ps-pub-profile__workpiece-list">
                <?php
                foreach ($cuttingWorkpieceMaterialGroups as $cuttingWorkpieceMaterialsGroup) {
                    echo $this->render('cuttingWorkpieceMaterialItem', ['cuttingWorkpieceMaterialsGroup' => $cuttingWorkpieceMaterialsGroup]);
                };
                ?>
            </div>

            <h3><?=_t('site.cutting', 'Machines')?></h3>
            <div class="pub-machine__list">
                <?php
                foreach ($cuttingMachines as $cuttingMachine) {
                    echo $this->render('cuttingMachineItem', ['cuttingMachine' => $cuttingMachine]);
                };
                ?>
            </div>

        </div>
        <div class="col-sm-4 ps-pub-profile__sidebar">
            <?php if ($companyPublicPageEntity->cuttingCount) {
                echo Model3dUploadWidget::widget([
                    'viewType'          => 'cutting',
                    'isPlaceOrderState' => true,
                    'additionalParams'  => ['utmSource' => 'cutting', 'psId' => $ps->id],
                    'model3dExtensions' => CuttingPack::ALLOWED_FORMATS
                ]);
            } ?>
            <?php if ($ps->needClaim()): ?>
                <h3><?= _t('company.public', 'Is this your business?'); ?></h3>
                <p>
                    <a href="/companies/verify?id=<?= $ps->id; ?>" class="btn btn-primary btn-ghost btn-sm"
                       rel="nofollow"> <span class="tsi tsi-briefcase"></span> Claim this business</a>
                </p>
            <?php endif; ?>

            <h3><?= _t('site.user', 'Have a project?'); ?></h3>


            <?php if ($ps): ?>
                <?= $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-inline.php', ['ps' => $ps]); ?>
            <?php endif; ?>

            <?php if ($currentUser && $currentUser->company && $currentUser->company->canMakeOffer() && ($ps->user->allow_incoming_quotes)) {
                ?>
                <a class="btn btn-danger btn-ghost m-r10 m-b20"
                   href="<?= HL(PreorderUrlHelper::getCreateOfferUrl($ps->user)); ?>"><?= _t('site.ps', 'Make offer'); ?></a>
                <?php
            }
            ?>
            <?php if ($currentUser && $ps->isActive()) {
                ?>
                <a class="btn btn-danger btn-ghost m-b20"
                   href="<?= \common\modules\instantPayment\helpers\InstantPaymentUrlHelper::getUrlForCompany($ps); ?>"><?= _t('site.ps', 'Instant payment'); ?></a>
                <?php
            }
            ?>
        </div>
    </div>

</div>