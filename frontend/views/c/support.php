<?php
/** @var Company $ps */
/** @var $companyPublicPageEntity CompanyPublicPageEntity */

use common\models\Company;
use common\models\message\helpers\UrlHelper;
use frontend\models\user\CompanyPublicPageEntity;
use frontend\models\user\UserFacade;
use yii\helpers\Url;


$bgUrl = $companyPublicPageEntity->bgUrl;
$sendMessageUrl = Url::to(UrlHelper::supportMessageRoute());
$online = UserFacade::getUserOnlineStatus($ps->user->userStatistics);
?>

<div class="user-profile">

    <div class="user-profile__cover" style="background-image: url('<?php echo $bgUrl; ?>')" title="<?php echo H($ps->title) ?>"></div>

    <div class="container relative">
        <div class="row">
            <div class="col-sm-9">
                <div class="user-profile__userinfo">
                    <a class="user-profile__avatar" href="<?php echo $companyPublicPageEntity->psUrl?>">
                        <img class="img-circle" src="<?php echo $ps->getCircleImageByPs($ps, 100,100);?>" alt="<?php echo H($ps->title) ?>" align="left">                                            </a>
                    <div class="user-profile__username">
                        <?php echo H($ps->title) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-sm-4 sidebar">

            <div class="panel panel-default m-t20">
                <div class="panel-heading"><?php echo _t('front.site', 'User statistics'); ?></div>
                <table class="table detail-view m-l5 m-r5">
                    <tr>
                        <th><?php echo _t('front.site', 'Status'); ?></th>
                        <td class="text-success"><?php echo $online;?></td>
                    </tr>
                </table>
            </div>

            <?php if(is_guest()):?>
                <a onclick="TS.Visitor.signUpForm('<?= app('request')->url; ?>');return false;" href="#" class="btn btn-block btn-danger public-profile__msg-btn">
                    <span class="tsi tsi-message"></span>
                    <?php echo _t('public.store', 'Send message') ?>
                </a>
            <?php else:?>
                <a href="<?php echo $sendMessageUrl?>" class="btn btn-block btn-danger public-profile__msg-btn">
                    <span class="tsi tsi-message"></span>
                    <?php echo _t('public.store', 'Send message') ?>
                </a>
            <?php endif;?>
        </div>

        <div class="col-sm-8 wide-padding wide-padding--left">

            <h1 class="page-header"> <?php echo _t('public.ps', 'About') . ' ' . H($ps->title); ?></h1>

            <h3> <?php echo nl2br(H($ps->description))?></h3>

            <div class="public-profile__about"></div>

        </div>

    </div>
</div>
