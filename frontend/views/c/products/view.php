<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 15:59
 */

use frontend\components\UserUtils;
use yii\helpers\Html;

/** @var $product \common\models\Product */

$linkAuthorStore = UserUtils::getUserStoreUrl($product->user);
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">

            <div class="model-breadcrumbs">
                <a href="/products" class="model-breadcrumbs__item">
                    <?= _t('site.store', 'Products') ?>
                </a>
                <span class="tsi tsi-right model-breadcrumbs__item"></span>
                <a href="/products?category_id=<?= $product->category_id ?>" class="model-breadcrumbs__item">
                    <?= H($product->category->title) ?>
                </a>
            </div>

            <h1 class="model-title">
                <?= H($product->getTitle()) ?>
            </h1>

            <div class="model-info">
                <div class="model-author">
                    <div><?= _t('site.store', 'by') ?></div>
                    <div class="model-author__userpic">
                        <?php
                        $ava = HL(UserUtils::getAvatar($product->user->userProfile->avatar_url, $product->user->email));
                        echo Html::a($ava, $linkAuthorStore); ?>
                        ?>
                    </div>
                    <div class="model-author__username">
                        <a href="<?=H($linkAuthorStore)?>"><?= H($product->user->getFullNameOrUsername()) ?></a>
                    </div>
                </div>
            </div>


            <div class="model-tags">
                <?php
                foreach ($product->siteTags as $tag) {
                    echo ' <span class="label label-default">#' . H($tag->text) . '</span>';
                }
                ?>
            </div>

        </div>
    </div>
</div>
<div class="container product-page">
    <div class="row">
        <div class="col-sm-5">
            <div class="designer-card p-t0 p-l0 p-r0 p-b0 m-b30">
                <div class="fotorama"
                     data-width="100%"
                     data-height="auto"
                     data-arrows="true"
                     data-click="true"
                     data-nav="thumbs"
                     data-thumbmargin="10"
                     data-thumbwidth="80px"
                     data-thumbheight="60px"
                     data-allowfullscreen="true">
                    <?php
                    if ($product->getCoverUrl()) {
                        echo '<img src="' . HL($product->getCoverUrl()) . '" alt="' . H($product->getTitle()) . '">';
                    }
                    foreach ($product->imageFiles as $imageFile) {
                        echo '<img src="' . HL($imageFile->getFileUrl()) . '" alt="' . H($imageFile->getFileName()) . '">';
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-7 wide-padding wide-padding--left">

            <div class="designer-card">
                <div class="row">
                    <div class="col-md-4">
                        <p class="m-b0"><?= _t('site.store', 'Product price') ?></p>
                        <p><strong><?=$product->productCommon->company->currency?> <?= $product->single_price ?></strong></p>
                    </div>
                    <div class="col-md-4">
                        <p class="m-b0"><?= _t('site.store', 'Delivery first item price') ?></p>
                        <p><strong>$<?= $product->productDelivery->express_delivery_first_item ?></strong></p>
                    </div>
                    <div class="col-md-4">
                        <p class="m-b0"><?= _t('site.store', 'Delivery following item price') ?></p>
                        <p><strong>$<?= $product->productDelivery->express_delivery_following_item ?></strong></p>
                    </div>
                    <div class="col-md-4">
                        <p class="m-b0"><?= _t('site.store', 'Express delivery country') ?></p>
                        <p><strong><?= $product->productDelivery->expressDeliveryCounrty->title ?></strong></p>
                    </div>
                </div>

                <div class="m-b10">
                    <a href="/product/buy/<?= $product->uuid ?>" class="btn btn-danger ts-ga">
                        <span class="tsi tsi-shopping-cart m-r10"></span>
                        <?= _t('front.store', ' Order now '); ?>
                        <span class="tsi tsi-right m-l20" style="margin-right: -10px;"></span>
                    </a>
                </div>
            </div>

            <div class="model-share__list m-t20 m-b10">

                <div class="model-share__btn"><a href="/site/sendemail" title="Send email" class="ts-ajax-modal">
                        <span class="social-likes__icon social-likes__icon_mail"></span></a>
                </div>

                <div class="model-share__btn sharelink" id="sharelink" data-placement="bottom" data-container="body" data-toggle="popover"
                     data-original-title="Share this link">
                    <span class="social-likes__icon social-likes__icon_link" title="Share this link"></span>
                </div>

                <div class="model-share__btn facebook-send" title="Facebook Messenger">
                    <span class="social-likes__icon social-likes__icon_facebook-m"></span> Facebook Send
                </div>

                <div class="model-share__btn social-likes__widget social-likes__widget_facebook" title="Share this link" data-display="popup"><span
                        class="social-likes__button social-likes__button_facebook"><span class="social-likes__icon social-likes__icon_facebook"></span>Facebook</span><span
                        class="social-likes__counter social-likes__counter_facebook social-likes__counter_empty"></span></div>
                <div class="model-share__btn social-likes__widget social-likes__widget_twitter" title="Tweet this link" data-via="treatstock"><span
                        class="social-likes__button social-likes__button_twitter"><span class="social-likes__icon social-likes__icon_twitter"></span> Twitter</span></div>
                <div class="model-share__btn social-likes__widget social-likes__widget_pinterest" title="Pin this model"
                     data-media="http://ts5.vcap.me/static/files/15/98/113879_15_de56ea67eadda21371bae310ff3d8d81.jpg?date=1504157919"><span
                        class="social-likes__button social-likes__button_pinterest"><span
                            class="social-likes__icon social-likes__icon_pinterest"></span> Pinterest</span><span
                        class="social-likes__counter social-likes__counter_pinterest social-likes__counter_empty"></span></div>
            </div>

            <h3><?= _t('public.ps', 'Description'); ?></h3>
            <div>
                <?= $product->getDescription() ?>
            </div>
        </div>
    </div>
</div>

<div class="mat-cat-nav">
    <div class="container">

    </div>
</div>


