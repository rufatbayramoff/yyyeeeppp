<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 14:46
 */

use frontend\widgets\CompanyTitleWidget;
use yii\helpers\Html;

$listView = Yii::createObject([
    'class'        => \yii\widgets\ListView::class,
    'dataProvider' => $dataProvider,
    'pager'        => ['linkOptions' => ['target'=>'_self']],
    'itemView'     => 'productListElement',
]);

$isWidget = array_key_exists('isWidget', $this->params) && $this->params['isWidget'];

$company = $ps;
$baseUrl = $company->getProductsStoreUrl($isWidget);
if (strpos($baseUrl, '?')) {
    $baseUrl .= '&';
} else {
    $baseUrl .= '?';
}

if ($isWidget) {
    ?>
    <base target='_parent'>
    <?php
} else {
    echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $companyPublicPageEntity]);
    ?>
    <div class="store-filter__container m-b30">
        <div class="container">

            <div class="store-filter">

                <h1 class="store-filter__categories-title ">
                    <?= _t('site.store', 'Store') ?>
                </h1>

            </div>

        </div>
    </div>
    <?php
}
?>

<div class="container">
    <?php
    if ($isWidget) {
        ?>
        <div class="relative">
            <h3 class="m-t0 p-t10 item-rendering-external-widget__head-title"><a href="<?= $company->getProductsStoreUrl(); ?>"><?= $company->title  ?></a></h3>
             <?= CompanyTitleWidget::widget(['company' => $company]); ?>
        </div>
        <?php
    }
    if (count($productsCategories) > 1) {
        ?>
        <div class="row">
            <div class="col-sm-5 col-md-4">
                <div class="form-group m-b0">
                    <?= HTML::dropDownList(
                        'ProductForm[categoryId]',
                        $selectedCategory,
                        ['' => _t('site.store', 'All categories')] + $productsCategories,
                        [
                            'class'    => 'form-control',
                            'id'       => 'product_category_id',
                            'onchange' => 'window.location.href="' . $baseUrl . 'category="+this.value',
                            'label'    => false
                        ]
                    )
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>


    <div class="row p-t20">
        <?php if ($dataProvider->count) {
            echo $listView->renderItems();
        } else { ?>
            <div class="product-list-empty">
                <?= _t('site.store', 'No products found.') ?>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
    echo $listView->renderPager();
    ?>
</div>
</base>
