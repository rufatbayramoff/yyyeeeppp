<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 18.04.18
 * Time: 15:08
 */

use common\models\Product;


/** @var Product $product */
$product = $model;
$productPriceFrom = $product->getMoqFromPriceMoney();
?>

<div class="col-xs-6 col-sm-4 col-md-3 product-card-container">
    <div class="product-card">
        <a class="product-card__pic" href="<?=$product->getPublicPageUrl()?>" title="<?= H($product->getTitle()) ?>" alt="<?= H($product->getTitle()) ?>">
            <?php if ($product->getCoverUrl()) {?>
                <img src="<?= $product->getCoverUrl() ?>" alt="<?= H($product->getTitle()) ?>">
            <?php } else {?>
                <span class="product-card__pic-empty">
                    <?= _t('site.catalog', 'Images not uploaded') ?>
                </span>
            <?php }?>
        </a>
        <a class="product-card__title" href="<?=HL($product->getPublicPageUrl())?>" title="<?= H($product->getTitle()) ?>"><?= H($product->getTitle()) ?></a>
        <a href="<?=HL($product->company->getPublicProductsCompanyLink());?>" target="_blank" class="product-card__supplier"><?=H($product->company->title);?></a>
        <?php if ($productPriceFrom): ?>
            <div class="product-card__price"><?= displayAsMoney($productPriceFrom); ?><span class="product-card__price-unit">/<?=$product->getUnitTypeTitle(1);?></span></div>
        <?php else: ?>
            <div class="product-card__price"><?=_t('site.product', 'Price on request');?></div>
        <?php endif; ?>
        <div class="product-card__min-qty"><?=$product->getMinOrderQty(); ?> <?=$product->getUnitTypeTitle($product->getMinOrderQty());?> (<abbr title="<?= _t('site.catalog', 'Minimum order quantity') ?>">MOQ</abbr>)</div>
    </div>
</div>
