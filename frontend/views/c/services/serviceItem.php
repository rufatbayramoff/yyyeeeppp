<?php
/**
 * User: nabi
 */

use common\models\UserAddress;
use lib\delivery\delivery\DeliveryFacade;
use yii\helpers\Html;

/** @var $ps \common\models\Ps */
/** @var $companyPublicPageEntity \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\PsCncMachine[] $psCncMachines */

echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $companyPublicPageEntity]);
?>
<div class="container ps-profile">

    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb m-t10">
                <li><a href="<?=$companyPublicPageEntity->psUrl;?>"><?=H($ps->title);?></a></li>
                <li><a href="<?=$companyPublicPageEntity->psUrl;?>/services"><?=_t('site.user', 'Services');?></a></li>
                <li><a href="<?=$model->getPublicServiceUrl();?>"><?=$model->getTitleLabel();?></a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 wide-padding wide-padding--right">

            <h1 class="ps-pub-profile__header">
                <?=$model->getTitleLabel();?>
            </h1>

            <div class="m-b20">
                <?php if($model->category_id): ?>
                    <span class="label label-info"><?=$model->category->title;?></span>
                <?php endif; ?>
            </div>

            <div class="ps-pub-profile__ps-text ugc-content">
                <?=$model->getDescription();?>
            </div>


            <h3><?=_t('site.user', 'Work Examples');?></h3>

            <div>
                <?php
                $images = $model->getImages(100, true);
                ?>
                <?=
                \frontend\widgets\SwipeGalleryWidget::widget([
                    'files' => $images,  'assetsByClass' => true,
                    'thumbSize' => [320, 180],
                    'containerOptions' => ['class' => 'picture-slider'],
                    'itemOptions' => ['class' => 'picture-slider__item'],
                    'scrollbarOptions' => ['class' => 'picture-slider__scrollbar'],
                ]);
                ?>
            </div>

            <?php foreach ($model->getBindedCompanyBlocks() as $blockBind):
                    $block = $blockBind->block;
                    if (empty($blockBind->block->is_visible)) {
                        continue;
                    }
                ?>
                <div class="product-page__section ugc-content">
                    <h3 class="product-page__section-title"><?= H($block->title); ?></h3>

                    <?= $block->content; ?>

                    <div class="m-t10">
                        <?= $block->imageFiles ?
                            \frontend\widgets\SwipeGalleryWidget::widget([
                                'files'            => $block->imageFiles,
                                'thumbSize'        => [230, 180],
                                'containerOptions' => ['class' => 'picture-slider'],
                                'itemOptions'      => ['class' => 'picture-slider__item'],
                                'scrollbarOptions' => ['class' => 'picture-slider__scrollbar'],
                            ]) : '';
                        ?>
                    </div>
                    <?php if ($videos = $block->getCompanyBlockVideos()): ?>
                        <div class="row">
                            <?php foreach ($videos as $video): ?>
                                <div class="col-sm-4">
                                    <div class="video-container">
                                        <object data="https://www.youtube.com/embed/<?= $video['videoId']; ?>"></object>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

        </div>

        <div class="col-sm-4 ps-pub-profile__sidebar">
            <?= $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-inline.php', ['ps' => $ps]); ?>
        </div>
    </div>

</div>


<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.ugc-content table').wrap('<div class="table-responsive m-t10"></div>');
    });
</script>
