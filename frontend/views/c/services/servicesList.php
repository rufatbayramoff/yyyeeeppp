<?php
/**
 * User: nabi
 */

use common\models\UserAddress;
use lib\delivery\delivery\DeliveryFacade;
use yii\helpers\Html;

/** @var $ps \common\models\Ps */
/** @var $companyPublicPageEntity \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\PsCncMachine[] $psCncMachines */
echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $companyPublicPageEntity]);
?>
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb m-t10">
                    <li><a href="<?=$companyPublicPageEntity->psUrl;?>"><?=H($ps->title);?></a></li>
                    <li><a href="<?=$companyPublicPageEntity->psUrl;?>/services"><?=_t('site.user', 'Services');?></a></li>
                </ol>
            </div>
        </div>

        <h1 class="ps-pub-profile__header">
            <?= _t('public.services', 'Services'); ?>
        </h1>

        <?php if ($psPrintersHtml && $companyPublicPageEntity->printersCount > 0): ?>
        <div class="row m-b10">
            <div class="col-lg-6">    <h3 class="m-l10"><?= _t('public.services', '3D Printing Services'); ?></h3>
            </div>
            <div class="col-lg-6 text-right p-b10 p-t20 p-r20">
            <a class="m-r20 btn btn-danger" href='/my/print-model3d?utm_source=profile_ps&psId=<?=$companyPublicPageEntity->psId;?>'>
                <?=_t('public.services', 'Instant 3D printing order');?></a>
            </div>
        </div>
            <?= $psPrintersHtml; ?>
        <?php endif; ?>

        <?php if ($cncServicesHtml && $companyPublicPageEntity->cncCount > 0): ?>
            <div class="row m-b10">
                <div class="col-lg-6"> <h3 class="m-l10"><?=_t('public.services', 'CNC Services'); ?></h3>
                </div>
                <div class="col-lg-6 text-right p-b10 p-t20 p-r20">
                    <a class="m-r20 btn btn-danger" href='<?=$companyPublicPageEntity->cncUrl;?>'>
                        <?=_t('public.services', 'Online CNC Quote');?></a>
                </div>
            </div>

            <?=$cncServicesHtml; ?>
        <?php endif; ?>


        <?php
        $listView = Yii::createObject(
            [
                'class'        => \yii\widgets\ListView::class,
                'dataProvider' => $servicesDataProvider,
                'itemOptions'  => ['tag' => null],
                'itemView'     => function($model, $key, $index, $widget) use ($companyPublicPageEntity){
                    return $this->render('serviceListItem_' . $model->type, ['model'=>$model, 'companyPublicPageEntity'=> $companyPublicPageEntity]);
                },
                'viewParams'   => [
                    'companyPublicPageEntity' => $companyPublicPageEntity
                ]
            ]
        );
        if ($listView->dataProvider->getCount() > 0): ?>
            <div ng-controller="PsCatalogController" class="row fb-grid">
                <?= $listView->renderItems() ?>
            </div>
            <div class="row"><div class="col-sm-12"><?= $listView->renderPager() ?></div></div>
        <?php endif; ?>

    </div>

