<?php

use common\models\DeliveryType;
use common\models\Ps;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\ps\PsFacade;
use frontend\widgets\SwipeGalleryWidget;
use yii\helpers\Url;

$this->registerAssetBundle(\frontend\assets\PhotoSwipeAsset::class);

/**
 * @var $printedFilesRepo \common\modules\catalogPs\repositories\PrintedFileRepository
 * @var $printer \common\modules\catalogPs\models\PsPrinterEntity
 * @var $searchForm \common\modules\catalogPs\models\CatalogSearchForm
 */
$printer = $model;
/** @var $ps Ps */
$ps = $printer->getPs();


$materials = [];
$materialsDisplay = [];
$certLevel = $printer->companyService->getCertificationLabel();
$materials =  $printer->getMaterials();
$moreMaterialsCount = 0;

$psLink = PsFacade::getPsLink($ps);
$psPicture = Ps::getCircleImageByPs($ps);

?>

<div class="pub-machine__list-item pub-machine__list-item3">
    <div class="designer-card designer-card--pub-machine">
        <?php if($printer->meta->getTechnology()): ?>
        <span class="designer-card__tech-label label label-info"  data-toggle="tooltip" data-placement="bottom" title=""
              data-original-title="<?=H($printer->meta->getTechnology()->getTitleInfo());?>"> <?=H($printer->meta->getTechnology()->getTitleCode());?></span>
        <?php endif; ?>

        <h2 class="designer-card__title" style="font-size: 16px;">
            <?=H($printer->title);?>

            <a class="m-l10 small" href="<?= Url::toRoute(['machines/default/item', 'slug' => $printer->meta->getSlug()])?>">
                <i class="tsi tsi-question"></i>
            </a>
        </h2>
        <?php
        $printerLocation = sprintf("%s, %s, %s", $printer->city, $printer->region, $printer->country_iso);
        $locationLink = \frontend\components\UserSessionFacade::getLocationAsString(\Yii::createObject(
            [
                'class'   => \lib\geo\models\Location::class,
                'city'    => $printer->city,
                'region'  => $printer->region,
                'country' => $printer->country_iso
            ]
        ));
        ?>
        <div class="designer-card__ps-loc" title="<?=$printerLocation;?>">
            <span class="tsi tsi-map-marker"></span>
            <a href="<?= Url::toRoute(['/catalogps/ps-catalog/index', 'location'=>$locationLink, 'type'=>'3d-printing-services']);?>"><?=$printerLocation;?></a>
        </div>

        <div class="designer-card__cert">
            <p class="designer-card__cert-label">
                <strong><?=_t('site.ps', 'Certification');?></strong>
                <?=$certLevel; ?>
            </p>
            <div class="designer-card__cert-data">
                <div class="designer-card__cert-pic">
                    <?php
                    $testOrderImages = $printer->meta->getTestOrderImages();
                    if($testOrderImages) {

                        $isFirst = true;
                        foreach ($testOrderImages  as $firstImage) {


                            $img = ImageHtmlHelper::getThumbUrl($firstImage->getFileUrl());
                            $imgThumb = ImageHtmlHelper::getThumbUrl($firstImage->getFileUrl(), 160, 90);
                            echo sprintf(
                                '<a  href="%s" data-lightbox="%s" style="' . ($isFirst ? '' : 'display : none') .'"><img src="%s" alt="%s"></a>',
                                $img, 'ps'.$printer->id, $imgThumb, \H($firstImage->name)
                            );
                            $isFirst = false;
                        }
                    }else{
                        ?>
                        <div class="designer-card__cert-pic-empty">
                            No image
                        </div>
                        <?php
                    }
                    ?>
                    </a>
                </div>
                <div class="designer-card__cert-text">
                    <div class="designer-card__data">
                        <div class="designer-card__data-label"><?=_t('site.ps', 'Build area');?></div>
                        <?php
                        $size = $printer->meta->getBuildVolume();
                        echo $size[0] . ' x ' . $size[1] . ' x ' . $size[2] . ' mm';
                        ?>
                    </div>
                    <div class="designer-card__data m-b0">
                        <div class="designer-card__data-label"><?=_t('site.ps', 'Layer resolution (highest)');?></div>
                        <?=$printer->meta->getLayerResolution('high');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row designer-card__data">
            <div class="col-sm-4 col-md-6 col-lg-6 designer-card__data-label"><?= _t('site.ps', 'Minimum Charge:'); ?></div>
            <div class="col-sm-8 col-md-6 col-lg-6">
                <?php if((float)$printer->min_order_price > 0): ?>
                    <?=displayAsCurrency($printer->min_order_price, $printer->companyService->company->currency);?>
                <?php else: ?>
                    -
                <?php endif; ?>
            </div>
        </div>
        <div class="row designer-card__data border-t p-t10">
            <div class="col-xs-12 designer-card__data-label m-b10"><?=_t('site.ps', 'Materials and Colors');?>
                <button class="btn btn-default btn-xs btn-circle" type="button" data-toggle="collapse" data-target="#materialsColors<?=$printer->id;?>" aria-expanded="false" aria-controls="materialsColors<?=$printer->id;?>">
                    <span class="tsi tsi-down"></span>
                </button>
            </div>
            <div class="col-xs-12 collapse" id="materialsColors<?=$printer->id;?>">
                <?php foreach($materials as $printerMaterial):
                            $firstColor = $printerMaterial->getCheapestColor();
                    ?>
                <div class="designer-card__material">
                    <span class="designer-card__material-title"><?=H($printerMaterial->material->title);?></span>
                    <span class="designer-card__material-price">
                        <?=_t('site.ps', 'from {minPrice}', ['minPrice'=>displayAsCurrency($firstColor->price, $printer->companyService->company->currency)]); ?>/<?=$firstColor->price_measure;?>
                    </span>
                    <?php foreach($printerMaterial->colors as $printerColor): ?>
                    <div title="<?=$printerColor->color->title;?> - <?=displayAsCurrency($printerColor->price, $printerColor->price_currency_iso);?>/<?=$printerColor->price_measure;?>" class="material-item">
                        <div class="material-item__color" style="background-color: #<?=$printerColor->color->getRgbHex();?>"></div>
                        <div class="material-item__label"><?=H($printerColor->color->title);?></div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php endforeach; ?>
            </div>
        </div>






        <div class="designer-card__ps-pics">
            <div class="designer-card__ps-pics-label"><?=_t('site.ps', 'Printed examples');?></div>
            <?=
                SwipeGalleryWidget::widget([
                    'files' => (array)$printedFilesRepo->getByPrinterId($printer->id),
                    'maxCount' => 3,
                    'thumbSize' => [160, 90],
                    'assetsByClass' => true,
                    'containerOptions' => ['class' => 'designer-card__ps-portfolio'],
                    'itemOptions' => ['class' => 'designer-card__ps-portfolio-item'],
                    'scrollbarOptions' => ['class' => 'designer-card__ps-portfolio-scrollbar'],
                    'emptyOptions' => ['class' => 'designer-card__ps-pics-empty'],
                    'emptyText' => _t('site.catalog', 'Images not uploaded')
                ]);
            ?>
        </div>

        <?php if ($printer->description): ?>
            <div class="designer-card__about">
                <?=\frontend\components\StoreUnitUtils::displayMore($printer->description);?>
            </div>
        <?php else: ?>
            <!-- Remove empty gap -->

        <?php endif; ?>
        <div>
            <?php
            $deliveryTypesIntl = PsFacade::getDeliveryTypesIntl();
            $deliveryHtml = [];
            foreach ($printer->getDeliveryTypes() as $psDelivery):
                $price = $psDelivery->carrier_price
                    ? displayAsCurrency($psDelivery->carrier_price, 'USD')
                    : _t('ps.profile', 'Postal Service Flat Rate');

                if ($psDelivery->carrier == DeliveryType::CARRIER_TS) {
                    $price = _t('ps.profile', 'Postal Service Flat Rate');
                }
                $iconHtml = '<span class="tsi ' . DeliveryType::DELIVERY_ICONS[$psDelivery->deliveryType->code] . '"></span>';
                if ($psDelivery->deliveryType->code == DeliveryType::PICKUP) {
                    $deliveryHtml[] = $iconHtml . ' ' . $deliveryTypesIntl[$psDelivery->deliveryType->code];
                    continue;
                }
                $deliveryHtml[] = $iconHtml . ' ' . $deliveryTypesIntl[$psDelivery->deliveryType->code] . ' — ' . $price;
            endforeach;
             echo implode('<br />', $deliveryHtml);
            ?>
        </div>
    </div>
</div>