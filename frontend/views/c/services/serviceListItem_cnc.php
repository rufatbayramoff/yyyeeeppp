<?php
/**
 * User: nabi
 */

/* @var $model \common\models\CompanyService */
?>
<div class="col-sm-4">
    <div class="designer-card">
        <?php if($model->category_id): ?>
            <span class="label label-warning"><?=$model->category->title;?></span>
        <?php endif; ?>
        <div class="designer-card__userinfo">
            <a class="designer-card__avatar" href="https://www.treatstock.com/u/tomislav-veg">
                <img src="https://static.treatstock.com/static/user/536a76f94cf7535158f66cfbd4b113b6/avatar_128x128.jpg" alt="" align="left">
            </a>

            <div class="designer-card__username">
                <?=$model->ps->title; ?>
            </div>

            <div class="designer-card__author"><?=$model->getTitleLabel();?></div>
        </div>

        <div class="designer-card__models-list">
            <?php
            $images = $model->getImages();
            ?>
            <?=
            \frontend\widgets\SwipeGalleryWidget::widget([
                'files' => $images,
                'thumbSize' => [320, 180],
                'containerOptions' => ['class' => 'ps-pub-portfolio__slider'],
                'itemOptions' => ['class' => 'ps-pub-portfolio__item'],
                'scrollbarOptions' => ['class' => 'ps-pub-portfolio__scrollbar'],
                'emptyOptions' => ['class' => 'designer-card__ps-pics-empty'],
                'emptyText' => _t('site.catalog', 'Images not uploaded')
            ]);
            ?>
        </div>

        <div class="designer-card__about">
            <?=\yii\helpers\StringHelper::truncateWords(strip_tags($model->getDescription()), 20);?>
        </div>

        <div class="designer-card__btn-block">
            <a class="btn btn-primary btn-ghost" href="/3d-printable-models-store/1453">Hire Designer</a>
        </div>
    </div>
</div>