<?php
/**
 * User: nabi
 */

use frontend\components\image\ImageHtmlHelper;

/* @var $model \common\models\CompanyService */
/* @var $companyPublicPageEntity \frontend\models\user\CompanyPublicPageEntity */
$logo = $model->getLogoImage();
?>
<div class="fb-grid__item col-xs-12 col-sm-6 col-md-4">
    <div class="designer-card designer-card--services">
        <?php if ($model->category_id): ?>
            <span class="designer-card__tech-label label label-info"><?= H($model->category->title); ?></span>
        <?php endif; ?>

        <div class="designer-card__userinfo">
            <a class="designer-card__avatar" href="#">
                <?php if ($logo): ?>
                    <img src="<?= HL($logo); ?>"/>
                <?php endif; ?>

            </a>
            <div class="designer-card__username p-t10">
                <a href="<?= $model->getPublicServiceUrl(); ?>">
                    <?= $model->getTitleLabel(); ?></a>
            </div>
        </div>

        <div class="designer-card__models-list">
            <?php
            $images = $model->getImages();
            ?>
            <?=
            \frontend\widgets\SwipeGalleryWidget::widget([
                'files'            => $images,
                'assetsByClass'    => true,
                'thumbSize'        => [320, 180],
                'containerOptions' => ['class' => 'ps-pub-portfolio__slider'],
                'itemOptions'      => ['class' => 'ps-pub-portfolio__item'],
                'scrollbarOptions' => ['class' => 'ps-pub-portfolio__scrollbar'],
                'emptyOptions'     => ['class' => 'designer-card__ps-pics-empty'],
                'emptyText'        => _t('site.catalog', 'Images not uploaded')
            ]);
            ?>
        </div>

        <?php /*
        <div class="designer-card__about">
            <?=\yii\helpers\StringHelper::truncateWords(strip_tags($model->getDescription()), 14);?>
        </div>
        */ ?>

        <div class="designer-card__btn-block">
            <a class="btn btn-danger btn-ghost m-b20" href="<?= HL($model->getPublicPageUrl()); ?>">
                <?= _t('site.ps', 'Get a Quote'); ?>
            </a>
        </div>
    </div>
</div>
