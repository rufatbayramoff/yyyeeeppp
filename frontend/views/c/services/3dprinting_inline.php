<?php
/**
 * User: nabi
 */

use common\components\ArrayHelper;
use common\models\DeliveryType;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\UserAddress;
use common\modules\catalogPs\components\CatalogSearchService;
use common\modules\catalogPs\models\PsPrinterEntity;
use frontend\assets\PhotoSwipeAsset;
use frontend\widgets\Model3dUploadWidget;
use frontend\widgets\Model3dViewedStateWidget;
use yii\helpers\Url;


/** @var $ps \common\models\Ps */
/** @var $companyPublicPageEntity \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\Model3dViewedState $model3dViewedState */
/** @var PsPrinter $selectedPrinter */
/** @var PS $ps */
/** @var \yii\web\View $this */

$user = $ps->user;
$sendMessageUrl = $companyPublicPageEntity->sendMessageUrl;

$cncMachine = null;

$listView = Yii::createObject([
    'class'        => \yii\widgets\ListView::class,
    'dataProvider' => $dataProvider,
    'itemOptions'  => ['tag' => null],
    'itemView'     => '3dprinting_inline_listItem',
    'viewParams'   => [
        'searchForm'       => $searchForm,
        'printedFilesRepo' => new \common\modules\catalogPs\repositories\PrintedFileRepository($dataProvider->getModels()),
    ]
]);
?>
        <div class="row ps-pub-profile">
            <div class="col-sm-12 wide-padding wide-padding--right">
                <div class="pub-machine__list">
                    <?= $listView->renderItems() ?>
                </div>
            </div>
        </div>
