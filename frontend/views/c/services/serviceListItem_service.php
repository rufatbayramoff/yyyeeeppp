<?php

use common\modules\company\serializers\CompanyServiceSerializer;
use yii\helpers\Html;

?>
<div class="fb-grid__item col-xs-12 col-sm-6 col-md-4">
    <div class="designer-card designer-card--services">
        <span class="designer-card__tech-label label label-info"><?php echo H($model->category->title ?? '');?></span>
        <h2 class="designer-card--services-title">
            <?php echo Html::a(H($model->getTitleLabel()),$model->getPublicServiceUrl())?>
        </h2>

        <div class="row designer-card--services-body">
            <div class="col-sm-12">
                <div class="designer-card--services-content">
                    <div class="designer-card--services-text">
                        <p class="designer-card--services-descript">
                            <?php echo \yii\helpers\StringHelper::truncateWords(strip_tags($model->getDescription()), 20);?>
                        </p>
                        <?php echo Html::a(_t('public.services', 'Show more'),HL($model->getPublicServiceUrl()),['class' => 'designer-card--services-more'])?>
                        <p class="designer-card--services-tags">
                            <b><?=_t('site.common', 'Tags') ?></b>: <?php echo H($model->tagsList());?>
                        </p>
                        <div class="product-page-info m-b0">
                            <table class="form-table form-table--top-label">
                                <tbody>
                                    <?php $companyServiceSerialize = CompanyServiceSerializer::serialize($model)?>
                                    <?php foreach ($companyServiceSerialize['properties'] as $propertyKey => $propertyValue): ?>
                                        <?php if(empty($propertyValue)) {
                                            continue;
                                        }
                                        ?>
                                        <tr class="form-table__row">
                                            <td class="form-table__label">
                                                <b><?php echo H($propertyKey)?></b>
                                            </td>
                                            <td class="form-table__data">
                                                <?php echo H($propertyValue)?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="designer-card--services-btns">
                        <?php echo Html::a(_t('public.services', 'Get a Quote'),$model->getCreateQuoteUrl(),['class' => 'btn btn-danger btn-block m-b20'])?>
                        <?php echo Html::a( _t('public.services', 'See more'),HL($model->getPublicServiceUrl()),['class' => 'btn btn-default btn-block m-b20 m-t0'])?>
                    </div>
                </div>

            </div>

        </div>


    </div>
</div>