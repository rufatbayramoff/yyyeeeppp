<?php
/**
 * User: nabi
 */

use frontend\assets\DropzoneAsset;
use frontend\components\StoreUnitUtils;
use frontend\components\UserUtils;

use frontend\models\store\StoreSearchForm;
use frontend\models\user\UserFacade;
use frontend\widgets\CategoryWithTotalSelectWidget;
use frontend\widgets\StoreUnitWidget;
use frontend\widgets\SwipeGalleryWidget;

/** @var $ps \common\models\Ps */
/** @var $companyPublicPageEntity \frontend\models\user\CompanyPublicPageEntity */

echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $companyPublicPageEntity]);

$user        = $ps->user;
$categoryId  = !empty($currentCategory) ? $currentCategory->id : null;
$usernameTxt = UserFacade::getFormattedUserName($user, true);

Yii::$app->angular
    ->service(['modal', 'user', 'notify', 'router'])
    ->directive('dropzone-button')
    ->controller([
        'store/common-models',
        'preorder/preorder-models',
        'preorder/preorder-service'
    ])
    ->controller('preorder/request-design-quote-directive')
    ->controllerParam('ps', ['id' => $ps->id]);

$this->registerAssetBundle(DropzoneAsset::class);

$listView = Yii::createObject([
    'class'        => \yii\widgets\ListView::class,
    'dataProvider' => $dataProvider,
    'pager'        => ['linkOptions' => ['target' => '_self']],
    'itemView'     => 'products/productListElement',
]);

?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb m-t10">
                <li><a href="<?= $companyPublicPageEntity->psUrl; ?>"><?= H($ps->title); ?></a></li>
                <li><a href="<?= $companyPublicPageEntity->storeUrl; ?>"><?= _t('site.user', '3D Models') ?></a></li>
            </ol>
        </div>
    </div>
</div>
<div class="store-filter__container store-filter--public-store0 m-b30">
    <div class="container">

        <div class="store-filter">
            <div class="store-filter__sort-by">
                <h4><?= _t('public.store', 'Sort by') ?>:</h4>
                <?php
                echo kartik\select2\Select2::widget([
                    'name'         => 'delivery-schedule__time-system',
                    'value'        => \Yii::$app->request->getUrl(),
                    'data'         => [
                        UserUtils::getUserStoreUrl($user, false, 120, $categoryId)                                   => _t('public.store', 'Best rated'),
                        UserUtils::getUserStoreUrl($user, false, 120, $categoryId, StoreSearchForm::SORT_MODEL_DATE) => _t('public.store', 'Upload date')
                    ],
                    'hideSearch'   => true,
                    'pluginEvents' => [
                        'change' => 'function(t){ location.href = t.target.value; }'
                    ],
                    'options'      => [
                        'multiple' => false,
                        'class'    => 'delivery-schedule__time-system',
                    ]
                ]);
                ?>
            </div>
        </div>

    </div>
</div>

<div class="container">

    <div class="row public-store-container">

        <div class="col-sm-8 wide-padding wide-padding--right">

            <h1 class="public-store__h1 h2">
                <?= _t('front.my', '3D Models by ') ?>
                <?php echo $usernameTxt; ?>
            </h1>

            <div class="row">
                <?= $listView->renderItems(); ?>
            </div>
            <?= $listView->renderPager(); ?>

        </div>

        <div class="col-sm-4 ps-pub-profile__sidebar">
            <h3 class="m-t0"><?= _t('front.user', 'Want to hire this designer?'); ?></h3>
            <?= $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-inline.php', ['ps' => $ps]); ?>
        </div>
    </div>

</div>


<script>
    <?php $this->beginBlock('js1', false); ?>
    var baseStoreUrl = '<?=UserUtils::getUserStoreUrl($user, false, 120, null, $sort);?>';
    var withSort = <?=!empty($sort) ? 'true' : 'false';?>;
    $("#js-public-store-category-select").on('change', function (a, b) {
        if (parseInt(b.activeCategoryId) > 0) {
            location.href = baseStoreUrl + (withSort ? '&' : '?') + 'categoryId=' + b.activeCategoryId;
        } else {
            location.href = baseStoreUrl
        }
    });

    //Init slider for PS portfolio
    var swiperPubPortfolio = new Swiper('.ps-pub-portfolio__slider', {
        scrollbar: '.ps-pub-portfolio__scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });
    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>

