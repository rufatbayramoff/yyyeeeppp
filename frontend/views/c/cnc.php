<?php
/**
 * User: nabi
 */

use common\models\UserAddress;
use lib\delivery\delivery\DeliveryFacade;
use yii\helpers\Html;

/** @var $ps \common\models\Ps */
/** @var $companyPublicPageEntity \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\PsCncMachine[] $psCncMachines */

echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $companyPublicPageEntity]);
?>

<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb m-t10">
                <li><a href="<?= $companyPublicPageEntity->psUrl; ?>"><?= H($ps->title); ?></a></li>
                <li><a href="#"><?= _t('site.user', 'CNC Machining'); ?></a></li>
            </ol>
        </div>
    </div>

    <div class="row ps-pub-profile">
        <div class="col-sm-12 wide-padding wide-padding--right">

            <h1 class="ps-pub-profile__header">
                <?= _t('site.ps', 'Online cnc service'); ?>
            </h1>

            <?php
            foreach ($psCncMachines as $psCncMachine) {
                ?>
                <div class="my-ps-printers__card">

                    <div class="my-ps-printers__row my-ps-printers__row--loc-mat">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="my-ps-printers__header">
                                    <h4 class="my-ps-printers__heading my-ps-printers__heading--loc">
                                        <?= _t('site.ps', 'Location & Delivery'); ?>
                                    </h4>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="my-ps-printers__block my-ps-printers__block--loc">
                                                <span class="tsi tsi-map-marker"></span>
                                                <?php if ($psCncMachine->companyService->getLocation()->one()): ?>
                                                    <?php
                                                    echo H(UserAddress::formatMachineAddress($psCncMachine->companyService));
                                                    ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="my-ps-printers__block my-ps-printers__block--delivery">
                                                <span class="tsi tsi-truck"></span>
                                                <?php echo implode(', ',
                                                    DeliveryFacade::getAvailableDeliveriesLabels2($psCncMachine->companyService->asDeliveryParams(), 0)); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="my-ps-printers__header">
                                    <h4 class="my-ps-printers__heading my-ps-printers__heading--materials">
                                        <?= _t('site.ps', 'CNC Machines'); ?>
                                    </h4>
                                </div>

                                <div class="row">
                                    <?php if ($psCncMachine->psCnc->millingMachines): ?>
                                        <div class="col-md-6">
                                            <div class="my-ps-printers__block my-ps-printers__block--material">
                                                <strong><?= _t('site.ps', 'Milling Machines'); ?></strong><br>
                                                <?php
                                                foreach ($psCncMachine->psCnc->millingMachines as $millingMachine) {
                                                    ?>
                                                    <?= H($millingMachine->title) ?><br>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($psCncMachine->psCnc->cuttingMachines): ?>
                                        <div class="col-md-6">
                                            <div class="my-ps-printers__block my-ps-printers__block--material">
                                                <strong><?= _t('site.ps', 'Cutting Machines'); ?></strong><br>
                                                <?php
                                                foreach ($psCncMachine->psCnc->cuttingMachines as $cuttingMachine) {
                                                    ?>
                                                    <?= H($cuttingMachine->title) ?><br>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="my-ps-printers__row my-ps-printers__row--loc-mat border-t">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="m-t10">
                                    <strong><?= _t('site.ps', 'Materials'); ?></strong>:
                                    <?php
                                    $materials = [];
                                    foreach ($psCncMachine->psCnc->materials as $material) {
                                        $materials[] = $material->title;
                                    }
                                    if (!empty($materials)) {
                                        echo implode(", ", $materials);
                                    } else {
                                        echo _t('site.ps', 'No materials available');
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="my-ps-printers__row p-b10">
                        <?= \common\modules\cnc\widgets\CncWidget::widget(['cncMachine' => $psCncMachine, 'showPsTitle' => false]) ?>
                    </div>
                </div>
                <?php
            };
            ?>
        </div>
    </div>
</div>