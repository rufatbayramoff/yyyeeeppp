<?php
/** @var $cuttingMachine \common\models\CuttingMachine */

use common\models\UserAddress;
use frontend\components\image\ImageHtmlHelper;
use lib\delivery\delivery\DeliveryFacade;
use lib\money\Money;

$companyService = $cuttingMachine->companyService;
?>

<div class="pub-machine__list-item">
    <div class="designer-card designer-card--pub-machine">
        <h2 class="designer-card__title">
            <?= H($companyService->title); ?>
        </h2>

        <div class="row">
            <div class="col-md-6">
                <div class="my-ps-printers__block my-ps-printers__block--loc">
                    <span class="tsi tsi-map-marker"></span>
                    <?php if ($companyService->getLocation()->one()): ?>
                        <?= H(UserAddress::formatMachineAddress($companyService)); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="my-ps-printers__block my-ps-printers__block--delivery">
                    <span class="tsi tsi-truck"></span>
                    <?php echo implode(', ',
                        DeliveryFacade::getAvailableDeliveriesLabels2($companyService->asDeliveryParams(), 0)); ?>
                </div>
            </div>
        </div>
        <div class="designer-card__cert">
            <div class="designer-card__cert-data">
                <div class="designer-card__cert-pic">
                    <?php
                    $testOrderImages = $companyService->imageFiles;
                    if ($testOrderImages) {
                        $isFirst = true;
                        foreach ($testOrderImages as $firstImage) {
                            $img      = ImageHtmlHelper::getThumbUrl($firstImage->getFileUrl());
                            $imgThumb = ImageHtmlHelper::getThumbUrl($firstImage->getFileUrl(), 160, 90);
                            echo sprintf(
                                '<a  href="%s" data-lightbox="%s" style="' . ($isFirst ? '' : 'display : none') . '"><img src="%s" alt="%s"></a>',
                                $img, 'ps' . $cuttingMachine->id, $imgThumb, \H($firstImage->name)
                            );
                            $isFirst = false;
                        }
                    } else {
                        ?>
                        <div class="designer-card__cert-pic-empty">
                            No image
                        </div>
                        <?php
                    }
                    ?>
                    </a>
                </div>
                <div class="designer-card__cert-text">
                    <div class="designer-card__data">
                        <div class="designer-card__data-label"><?= _t('site.ps', 'Build area'); ?></div>
                        <?php
                        $size = $cuttingMachine->getWorkSize();
                        echo $size->width . ' x ' . $size->length . ' mm';
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row designer-card__data">
            <div class="col-sm-4 col-md-6 col-lg-4 designer-card__data-label"><?= _t('site.ps', 'Minimum Charge:'); ?></div>
            <div class="col-sm-8 col-md-6 col-lg-8">
                <?php
                if ($cuttingMachine->min_order_price): ?>
                    <?= displayAsMoney(Money::create($cuttingMachine->min_order_price, $companyService->company->currency)); ?>
                <?php else: ?>
                    -
                <?php endif; ?>
            </div>
        </div>
        <div class="row border-t p-t10">
            <div class="col-xs-12 designer-card__data-label m-b10"><?= _t('site.ps', 'Materials and Prices'); ?></div>

            <?php foreach ($cuttingMachine->cuttingMachineProcessings as $machineProcessing): ?>
                <div class="col-xs-6 m-b10">
                    <span class="designer-card__material-title"><?= H($machineProcessing->cuttingMaterial->title) ?></span>
                    <div><?= _t('site.ps', 'Thickness') ?>: <?= H($machineProcessing->thickness) ?>mm</div>
                    <div><?= _t('site.ps', 'Cutting price') ?> :
                        <?= displayAsMoney(Money::create($machineProcessing->getCuttingPricePerM(), $companyService->company->currency)) ?> <?= _t('site.ps', ' per m') ?></div>
                    <div><?= _t('site.ps', 'Engrave price') ?> :
                        <?= displayAsMoney(Money::create($machineProcessing->getEngravingPricePerM(), $companyService->company->currency)) ?> <?= _t('site.ps', ' per m') ?></div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

