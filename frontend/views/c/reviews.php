<?php
/**
 * User: nabi
 */

use frontend\assets\DropzoneAsset;
use frontend\assets\SocialButtonsAsset;
use frontend\models\ps\PsFacade;
use frontend\widgets\PsPrintServiceReviewStarsWidget;
use frontend\widgets\Model3dUploadWidget;
use frontend\widgets\Model3dViewedStateWidget;
use yii\helpers\Url;

/** @var $ps \common\models\Ps */
/** @var $this \yii\web\View */
/** @var $companyPublicPageEntity \frontend\models\user\CompanyPublicPageEntity */
/** @var \common\models\Model3dViewedState $model3dViewedState */
/** @var $reviewsProvider \yii\data\ActiveDataProvider */
/** @var $storeOrderReviewSearch \frontend\models\review\StoreOrderReviewSearch */

echo $this->renderFile('@frontend/views/c/_top.php', ['user' => $ps->user, 'ps' => $ps, 'companyPublicPageEntity' => $companyPublicPageEntity]);
echo $this->render('review-share-modal');

$this->registerAssetBundle(DropzoneAsset::class);

Yii::$app->angular
    ->service(['modal', 'user', 'router'])
    ->controller('review/shareReview')
    ->directive('dropzone-button');

$this->registerJs("var facebook_app_id = '".param('facebook_app_id')."';", \yii\web\View::POS_HEAD);
$this->registerAssetBundle(SocialButtonsAsset::class);

$emptyReviewsCount = 0;
$sendMessageUrl = $companyPublicPageEntity->sendMessageUrl;

$listView = \yii\widgets\ListView::widget([
    'dataProvider' => $reviewsProvider,
    'itemOptions' => ['tag'=>null],
    'itemView' => 'reviewItem'
]);


?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb m-t10">
                <li><a href="<?=$companyPublicPageEntity->psUrl;?>"><?=H($ps->title);?></a></li>
                <li><a href="<?=$companyPublicPageEntity->psUrl;?>/reviews"><?=_t('site.user', 'Reviews');?></a></li>
            </ol>
        </div>
    </div>
    <div class="row ps-pub-profile">
        <div class="col-sm-8 wide-padding wide-padding--right">
            <?php if ($reviewsProvider->count > 0): ?>

                <h1 class="ps-profile-user-review-title h2">
                    <?= _t('ps.profile', 'Reviews') ?>
                    <div class="ps-profile-rating">
                        <?= PsPrintServiceReviewStarsWidget::widget(['ps' => $ps]); ?>
                    </div>
                </h1>

                <?php echo $this->render('reviews/searchPanel', ['searchModel' => $storeOrderReviewSearch, 'action' => PsFacade::getPsReviewsRoute($ps)]); ?>

                <a name="reviews"></a>
                <?=$listView?>
            <?php else:

                echo ' <p>' . _t('site.ps', 'No reviews yet') . '</p>';
            endif; ?>

        </div>

        <div class="col-sm-4 ps-pub-profile__sidebar">
            <?php
            if (!empty($model3dViewedState)) {
                echo Model3dViewedStateWidget::widget(['model3dViewedState' => $model3dViewedState, 'psPrinter' => $selectedPrinter]);
            } elseif($companyPublicPageEntity->printersCount){
                echo Model3dUploadWidget::widget(['viewType'          => 'short',
                    'isPlaceOrderState' => true,
                    'additionalParams'  => ['utmSource' => 'fromPrinter', 'psId' => $ps->id]
                ]);
            }
            ?>

            <h3>
                <?= _t('site.user', 'Need something special?'); ?>
            </h3>

            <?= $this->render('@frontend/modules/workbench/views/preorder/templates/create-preorder-inline.php', ['ps'=>$ps]);?>
        </div>
    </div>
</div>