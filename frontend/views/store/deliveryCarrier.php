<?php
use common\components\ArrayHelper;
use common\models\User;
use frontend\components\UserSessionFacade;
use frontend\controllers\store\serializers\DeliveryAddressSerializer;
use lib\delivery\delivery\DeliveryFacade;
use yii\helpers\Html;

/* @var \frontend\models\delivery\DeliveryForm $deliveryForm */
/* @var int $deliverAddressId */
/* @var User $currentUser */
/* @var \common\models\CompanyService $machine */


$form = \yii\bootstrap\ActiveForm::begin(['enableClientValidation' => false, 'action' => ['store/delivery']]);

$currentLocation = UserSessionFacade::getLocation();
Yii::$app->angular->controllerParams(
    [
        'currentLocation' => ['city'=>$currentLocation->city, 'country'=>$currentLocation->country, 'region'=>$currentLocation->region],
        'userAddresses' => $currentUser->isAnonimUser() ? [] : DeliveryAddressSerializer::serialize(
            DeliveryFacade::getUserAddresses($machine->asDeliveryParams(), $currentUser)
        ),
    ]
);

?>
<div ng-controller="OrderCheckoutDeliveryDeliveryController">

<div class="row">
    <div class="col-sm-11 delivery-details">
        <h3 class="delivery-details__title"><?= _t('front.site', 'Delivery details'); ?></h3>

        <div
            ng-if="view.mode == MODE_SELECTED_ADDRESS"
            class="js-delivery-form-old">

            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div>{{deliveryForm | addressTitle}}</div>
                            <div ng-bind-html="deliveryForm | addressBody"></div>
                            <button
                                ng-click="changeSelectedAddress()"
                                class="btn btn-warning btn-sm js-change-address m-t10"
                                type="button"><?= _t("front.site", "Change")?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div
            ng-show="view.mode == MODE_INPUT_ADDRESS"
            class="row">

            <div class="col-sm-8">

                <?=
                    $form->field($deliveryForm, 'street')
                        ->textInput(['ng-model' => 'deliveryForm.street'])
                        ->label(_t('site.delivery', 'Street Address'))
                ?>

                <?=
                    $form->field($deliveryForm, 'street2')
                        ->textInput(['ng-model' => 'deliveryForm.street2'])
                        ->label(_t('site.delivery', 'Extended Address'))
                ?>

                <?=
                    $form->field($deliveryForm, 'city')
                        ->textInput(['ng-model' => 'deliveryForm.city'])
                        ->label(_t('site.delivery', 'City'))
                ?>

                <div class="row">
                    <div class="col-md-6">
                        <?=
                            $form->field($deliveryForm, 'state')
                                ->textInput(['ng-model' => 'deliveryForm.state'])
                                ->label(_t("site.delivery", "State/Region"))
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?=
                            $form->field($deliveryForm, 'zip')
                                ->textInput(['ng-model' => 'deliveryForm.zip'])
                                ->label(_t('site.delivery', 'Zip'))
                        ?>
                    </div>
                </div>


                <?php if($machine->asDeliveryParams()->onlyDomestic()):?>
                    <p class="text-danger"><?= _t("store.delivery", "Only domestic shipping is available with this 3D printer.")?></p>
                <?php endif;?>

                <?=
                    $form->field($deliveryForm, 'country')
                        ->dropDownList(DeliveryFacade::getAllowedDeliveryCoutriesCombo($machine->asDeliveryParams()), [
                            'ng-model' => 'deliveryForm.country',
                            'placeholder' => _t('front.site', 'Select'),
                        ])
                        ->label(_t('site.delivery', 'Country'));
                ?>

                <div class="row">
                    <div class="col-md-6">
                        <?=
                            $form->field($deliveryForm, 'contact_name')
                                ->textInput(['ng-model' => 'deliveryForm.contact_name'])
                                ->label(_t('site.delivery', 'Contact Name'))
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?=
                            $form->field($deliveryForm, 'company')
                                ->textInput(['ng-model' => 'deliveryForm.company'])
                                ->label(_t('site.delivery', 'Company'))
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?=
                            $form->field($deliveryForm, 'phone')
                                ->textInput(['ng-model' => 'deliveryForm.phone'])
                                ->label(_t('site.delivery', 'Phone'))
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?=
                            $currentUser->isAnonimUser()
                                ? $form->field($deliveryForm, 'email')->textInput(['ng-model' => 'deliveryForm.email'])
                                : $form->field($deliveryForm, 'email')->hiddenInput(['ng-model' => 'deliveryForm.email'])->label(false);
                        ?>
                    </div>
                    <?=
                        $form->field($deliveryForm, 'deliveryType')
                            ->hiddenInput(['ng-model' => 'deliveryForm.deliveryType', 'value' => '{{deliveryForm.deliveryType}}'])
                            ->label(false);
                    ?>
                </div>
                <div class="form-group">

                    <button
                        ng-if="isCalculateShippingShowed()"
                        ng-disabled="view.deliveryRatesLoading"
                        loader-click="updateRates()"
                        class="btn btn-primary"
                        type="button"><?=_t('front.site', 'Calculate shipping')?></button>

                </div>
            </div>

            <div
                ng-if="haveAddresses()"
                class="col-sm-4 previous-delivers">

                <h3 class="previous-delivers__title"><?= _t('front.site', 'Previous deliveries'); ?></h3>
                <div
                    ng-repeat="address in userAddresses"
                    class="previous-delivers__item">

                    <div class="previous-delivers__item__title m-b10">{{address | addressTitle}}</div>
                    <div ng-bind-html="address | addressBody "></div>

                    <button
                        type="button"
                        class="btn btn-danger btn-ghost"
                        ng-click="setExistedAddress(address)"><?= _t("front.site", "Deliver here")?></button>

                </div>
            </div>

        </div>

    </div>

</div>


<div class="row delivery-details-type">
    <div class="col-sm-12">
        <h4 class="delivery-details-type__title"><?= _t('site.store', 'Delivery options'); ?>:</h4>


        <p ng-if="view.deliveryRatesLoading">
            <?= _t('site.order', 'Loading, please wait...')?>
        </p>

        <p ng-if="!view.deliveryRatesLoading && view.rates === undefined && !view.error">
            <?= _t('site.delivery', "Please click Calculate shipping, delivery details have been changed.")?>
        </p>


        <p ng-if="view.error" ng-bind-html="view.error"></p>

        <div class="form-group" ng-repeat="rate in view.rates">
            <div>
                <div class="radio">
                    <input type="radio" ng-model="deliveryForm.deliveryType" ng-value="rate.code">
                    <label> {{rate.title}} <b>{{view.currencyLabel}}{{rate.cost.amount | number:2}}</b> </label>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row">
    <div class="col-sm-12">
        <h4 class="delivery-details-type__title">
            <?= _t('site.store', 'Comment'); ?>
        </h4>
        <?=
        $form->field($deliveryForm, 'comment', ['options'=>['class'=>'form-group']])
            ->textarea(['maxlength' => '600'])
            ->label(_t('site.delivery', 'Leave a comment if you have any requests, instructions, or additional information for the print service. If there are parts in your order that need to fit one another, please specify which parts to the print shop.'));
        ?>
    </div>
    </div>




<div class="row delivery-details-submit">
    <div class="col-sm-7 delivery-details-submit__info">
        <?= _t('site.shop', 'To proceed to checkout please click "Next: Payment".'); ?><br>
        <?= _t('site.shop', "We do not store your credit card details."); ?>
        <?=
            $currentUser->isAnonimUser() ? "<br/><br/>"._t('site.order', ' By using this Service, you agree to our {terms} and that you have read our {policy}, including our {cookie}.', [
                'terms' => Html::a('Terms', ['/site/terms'], ['target' => '_blank']),
                'policy' => Html::a('Privacy Policy', ['/site/policy'], ['target' => '_blank']),
                'cookie' => Html::a('Cookie use', ['/site/policy', '#' => 'cookie'], ['target' => '_blank']),
            ]) : '';
        ?>

    </div>
    <div class="col-sm-5">

        <button
            class="btn btn-block btn-primary js-next-pay"
            ng-disabled="!canBuy()"
            loader-click="buy($event)"
            loader-click-unrestored="true"
            title="<?= _t("site.store", 'Please choose your delivery method before making a payment.')?>"
            type="button"><?= _t("front.site", "Next: Payment")?></button>

    </div>
</div>

<?php $form->end(); ?>



<script type="text/ng-template" id="/app/store/checkuot/delivery/validate-address-modal.html">
    <div class="modal fade modal-warning" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t('site.ps', 'Confirm')?></h4>
                </div>
                <div class="modal-body">
                    <p><?= _t('site.delivery', "The address you have specified doesn't seem to be correct. We found the address:")?></p>
                    <p ng-bind-html="address | addressBody"></p>
                    <p><?= _t('site.delivery', "Would you like to use the suggested address?")?></p>
                </div>
                <div class="modal-footer">
                    <button
                        ng-click="declineAddress()"
                        type="button" class="btn btn-default"><?=_t('site.ps', 'Cancel')?></button>
                    <button
                        ng-click="confirmAddress()"
                        type="button" class="btn btn-primary"><?=_t('site.ps', 'Ok')?></button>
                </div>
            </div>
        </div>
    </div>
</script>
</div>