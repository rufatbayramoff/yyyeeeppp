<?php
use common\components\order\PriceCalculator;
use common\components\PaymentExchangeRateFacade;
use common\models\Cart;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\UserAddress;
use common\services\MeasureService;
use common\services\Model3dPartService;
use frontend\components\UserSessionFacade;
use frontend\models\ps\PsFacade;
use frontend\models\store\GetPrintedForm;
use frontend\models\user\UserFacade;
use lib\delivery\delivery\DeliveryFacade;
use lib\money\Currency;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var Cart $cart */
/* @var float $modelWeight */
/* @var GetPrintedForm $getPrintedForm */

$printer = $cart->getMachine()->asPrinter();
$storeUnit = $cart->getStoreUnit();
$colorMaterials = $printer->materials;
$currency = UserSessionFacade::getCurrency();
$measurement = UserSessionFacade::getMeasurement();
$change = isset($change) ? $change : false;

$cartItem = $cart->getFirstItem();
$model3dReplica = $cartItem->model3dReplica;
$modelWeight = MeasureService::getWeightFormatted($model3dReplica->getWeight());
$model3dParts = $model3dReplica->getActiveModel3dParts();

$formatUserName = function (\common\models\User $userObj) {
    /**
     *
     * @var \common\models\User $userObj
     */
    $fullName = H(UserFacade::getFormattedUserName($userObj));
    $link = \yii\helpers\Html::a(
        $fullName,
        \frontend\components\UserUtils::getUserPublicProfileRoute($userObj),
        ['target' => '_blank']
    );
    return sprintf('%s', $link);
}

?>
<div class="content store-getprinted">
    <?php $form = \yii\bootstrap\ActiveForm::begin(
        [
            'enableAjaxValidation' => true,
            'action'               => [
                'store/cart/checkout',
                'id'        => $storeUnit->id,
                'printerId' => $printer->id
            ]
        ]
    ); ?>


    <h3 class="store-getprinted__ps-title">
        <?php echo _t('site.print', 'Print Shop'); ?>: <strong><?php echo \H($printer->ps->title); ?></strong>
    </h3>

    <?php
    Pjax::begin(
        ['enablePushState' => false, 'id' => 'getprinted-cm', 'options' => ['class' => 'pjax-getprintedModal']]
    ); ?>
    <div class="row">

        <div class="col-sm-6 m-b20">
            <div class="store-getprinted__ps-avatar">
                <?php
                $psPicture = Ps::getCircleImage($printer);
                echo Html::img($psPicture);
                ?>
            </div>
            <div class="store-getprinted__ps-info">
                <p>
                    <span class="text-muted"><?= _t('site.store', 'Printer model'); ?>:</span><br/>
                    <strong><?= H($printer->title); ?></strong>
                </p>

                <p class="store-getprinted__ps-description">
                    <?php
                    echo UserAddress::formatPrinterAddress($printer);

                    echo '<br />';


                    $priceCalculator = new PriceCalculator();
                    $printPrice = $this->priceCalculator->calculateModelAndPrintPriceByCart($cart->getModel3d(), $cart->getMachine()->asPrinter());
                    $deliverTypes = DeliveryFacade::getAvailableDeliveriesLabels($printer->companyService->asDeliveryParams(), $printPrice);
                    echo implode(', ', $deliverTypes);
                    ?>
                </p>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="store-getprinted__ps-info-row">
                <div class="store-getprinted__ps-label"><?= _t('site.store', 'Owner'); ?>:</div>
                <div class="store-getprinted__ps-data"><?= H(UserFacade::getFormattedUserName($printer->ps->user)); ?></div>
            </div>

            <div class="store-getprinted__ps-info-row">
                <div class="store-getprinted__ps-label"><?= _t('site.store', 'Print time'); ?>:</div>
                <div class="store-getprinted__ps-data">

                    <?php
                    $totalQty = 0;
                    $hours = 0;
                    foreach ($cart->cartItems as $cartItem):
                        $totalQty += $cartItem->qty;
                        $hours += $totalQty * PsFacade::getEstimatedPrintingTime($cartItem->model3dReplica);
                    endforeach;

                    echo _t(
                        'site.print',
                        'about {hours} hours',
                        [
                            'hours' => $hours
                        ]
                    ); ?>
                </div>
            </div>

            <?php if ($printer->isCertificated()): ?>
                <div class="store-getprinted__ps-info-row">
                    <div class="store-getprinted__ps-label"><?= _t('site.ps.test', 'Certification'); ?>:</div>
                    <div class="store-getprinted__ps-data"><?= $printer->companyService->getCertificationLabel(); ?></div>
                </div>
            <?php endif; ?>

        </div>
    </div>
    <hr>  <?php if(!empty($notifyMsg)): ?>
        <div class="alert alert-warning"><?=$notifyMsg;?></div>
    <?php endif; ?>
    <?php
    $printPrice = 0;
    $qty = 1;

    foreach ($cart->cartItems as $cartItem):
        $model3dReplica = $cartItem->model3dReplica;
        $model3dParts = $model3dReplica->getActiveModel3dParts();
        $printPrice += PriceCalculator::calculateCartItemPrice($cartItem);
        $qty = $cartItem->qty;
        ?>
        <div class="row">
            <h3 class="store-getprinted__item-title col-sm-8">
                <?php echo _t('site.store', 'Model:'); ?>
                <strong>
                    <?= H($cartItem->model3dReplica->title) ?>
                </strong>
            </h3>

        </div>
        <!-- list of 3d files -->
        <?php
        foreach ($model3dParts as $model3dPart):
            $props = $model3dPart->model3dPartProperties;
            $imgSrc = Model3dPartService::getModel3dPartRenderUrlIfExists($model3dPart);
            $img = Html::img($imgSrc, ['width' => '80']);
            $urlUp = Url::toRoute(
                [
                    'store/cart/update-cart',
                    'id' => $cartItem->id,
                    'model3dPart'         => $model3dPart->id,
                    'partQty'       => $model3dPart->qty+1,
                ]
            );
            $urlDown = Url::toRoute(
                [
                    'store/cart/update-cart',
                    'id' => $cartItem->id,
                    'model3dPart'         => $model3dPart->id,
                    'partQty'       => max(0, $model3dPart->qty-1),
                ]
            );
            ?>
            <div class="store-getprinted__model-row">
                <div class="store-getprinted__model-pic">
                    <?php echo $img; ?>
                </div>
                <div class="store-getprinted__model-info">
                    <h4 class="store-getprinted__model-name">
                        <?php echo $model3dPart->file->getFileName(); ?>
                    </h4>
                    <div class="store-getprinted__model-size">
                        <?= _t('site.store', 'Size'); ?> : <?= $model3dPart->getSize()->toStringWithMeasurement(); ?>,
                        <?= _t('site.store', 'Weight'); ?> : <?= MeasureService::getWeightFormatted($model3dPart->getWeight())?>
                    </div>
                </div>
                <div class="store-getprinted__model-quantity">
                    <label for=""><?= _t('store.unit', 'Quantity'); ?></label>
                    <div class="item-rendering-external-widget__qty-btns">
                        <a href="<?=$urlDown;?>" class="btn btn-link">
                            <span class="tsi tsi-minus-c"></span>
                        </a>
                        <div class="item-rendering-external-widget__qty-value">
                            <?= $model3dPart->qty; ?>
                        </div>
                        <a href="<?=$urlUp;?>" class="btn btn-link">
                            <span class="tsi tsi-plus-c"></span>
                        </a>
                    </div>
                </div>

            </div>

        <?php endforeach; ?>

    <?php endforeach; ?>
    <!-- 3d files list ends -->
    <hr/>

    <div class="row">
        <?php
        if ($model3dReplica->isOneTextureForKit()) {
            ?>
            <div class="col-sm-6">

                <?php foreach ($cart->cartItems as $cartItem) {
                    $texture = $cartItem->model3dReplica->getKitTexture();
                    ?>

                    <!-- selected printer own materials and colors -->
                    <h3 class="store-getprinted__material-title"><?php echo _t('site.store', 'Material and color'); ?></h3>
                    <?php
                    $prevMaterial = '';

                    foreach ($colorMaterials as $psMaterial):
                        $mTitle = $psMaterial->material->title;
                        $materialCode = $psMaterial->material->filament_title;
                        echo '<div class="store-getprinted__material-row">';
                        echo '<div class="store-getprinted__material-label">' . _t('site.store', 'Material') . ':</div>';
                        echo '<div class="store-getprinted__material-data">';
                        echo $mTitle;
                        echo '</div>';
                        echo '</div>';

                        echo '<div class="store-getprinted__material-row">';
                        echo '<div class="store-getprinted__material-label store-getprinted__material-label--height">' . _t('site.store', 'Color') . ':</div>';

                        echo '<div class="store-getprinted__material-data">';
                        foreach ($psMaterial->psPrinterColors as $psColor) {
                            $price = $psColor->price;
                            $mColor = $psColor->color->rgb;
                            $colorCode = $psColor->color->render_color;

                            $selected = $kl = '';
                            if ($texture->printerColor->render_color === $colorCode && $texture->printerMaterial->filament_title === $materialCode) {
                                $selected = 'ts-color-active';
                            }
                            $cssClass = 'btn btn-xs ts-material-color ' . $kl . ' ' . $selected;

                            $url = Url::toRoute(
                                [
                                    'store/cart/update-cart',
                                    'id'         => $cartItem->id,
                                    'materialId' => $psMaterial->material_id,
                                    'colorId'    => $psColor->color_id,
                                ]
                            );

                            $colorLink = Html::a(
                                ' ',
                                $url,
                                [
                                    'title'    => $psColor->color->title,
                                    'class'    => $cssClass,
                                    'style'    => 'background-color: rgb(' . $mColor . ')',
                                    'data-url' => $url,
                                ]
                            );
                            echo $colorLink;
                        }
                        echo '</div>';
                        echo '</div>';
                    endforeach;
                    ?>
                <?php } ?>
            </div>
            <?php
        } else {
            ?>
            <div class="col-sm-6">
                <?= _t('site.store', 'Multicolor'); ?>
            </div>
            <?php
        }
        ?>
        <div class="col-sm-6">
            <!-- prices -->
            <h3 class="store-getprinted__price-title">
                <?php echo _t('site.store', 'Price'); ?>
            </h3>
            <?php foreach ($cart->cartItems as $cartItem): ?>
                <div class="row store-getprinted__price-row">
                    <div class="col-xs-8">
                        <?php echo _t('site.store', 'Model price'); ?>
                        <?php if ($cartItem->qty > 1) {
                            echo ' x ' . $cartItem->qty;
                        } ?>
                    </div>
                    <div class="col-xs-4 text-right">
                        <?php
                        echo displayAsCurrency(PriceCalculator::calculateModel3dPrice($storeUnit->model3d));
                        ?>
                    </div>
                </div>
                <div class="row store-getprinted__price-row">
                    <div class="col-xs-8">
                        <?php echo _t('site.store', 'Manufacturing Fee'); ?>
                        <br/>
                        <small>
                            <?php
                            $minPrintPrice = $printer->getMinPrintPrice();
                            if (!UserFacade::isObjectOwner($printer) && $minPrintPrice->getAmount() > 0) {
                                echo _t('site.store', 'Minimum print fee ');
                                echo displayAsMoney($minPrintPrice);
                            }

                            ?>
                        </small>
                    </div>
                    <div class="col-xs-4 text-right">
                            <span class="pricer-price print_price">                            
                                <?= displayAsMoney(PriceCalculator::calculateModel3dPrintPriceWithPackageFee($model3dReplica, $printer)); ?>
                            </span>
                    </div>
                </div>
            <?php endforeach; ?>
            <hr/>
            <div class="row">
                <div class="col-xs-8">
                    <b><?php echo _t('site.store', 'Total price'); ?></b>
                </div>
                <div class="col-xs-4 text-right">
                        <span class="pricer-price total_price">
                            <?php
                            $totalModel = PriceCalculator::calculateModel3dPrice($model3dReplica);
                            $totalPrint = PriceCalculator::calculateModel3dPrintPriceWithPackageFee($model3dReplica, $printer);
                            ?>
                            <?= displayAsCurrency(\lib\money\MoneyMath::sum($totalModel , $totalPrint), $currency); ?>
                        </span>
                </div>
            </div>
        </div>
    </div>

    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-xs-12 col-sm-7">
            <?php
            if ($change) {
                $getPrintedForm->canChangePs = $cart->canChangePs();
            } else {
                $getPrintedForm->canChangePs = true;
            }
            echo $form->field($getPrintedForm, 'canChangePs')->checkbox(['template' => '<div class="checkbox">{input}{label}</div>']) ?>
        </div>
        <div class="col-xs-12 col-sm-5 text-right">
            <?php
            $submitTitle = $change ? _t('site.store', 'Apply') : _t('site.store', 'Proceed to Checkout');
            ?>
            <?= Html::submitButton($submitTitle, ['class' => 'btn btn-primary ts-ajax-submit m-t10 m-b10']);?>
        </div>
    </div>
    <?php yii\bootstrap\ActiveForm::end(); ?>
</div>
     
