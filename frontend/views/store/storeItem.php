<?php

use common\models\model3d\CoverInfo;
use common\models\StoreUnit;
use common\services\LikeService;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\UserUtils;
use frontend\models\model3d\Model3dFacade;
use frontend\models\model3d\Model3dForm;
use frontend\models\user\FrontUser;
use frontend\models\user\UserFacade;
use yii\helpers\Html;

/** @var common\models\StoreUnit $model */
/** @var int[] $likesCounts Likes counts for all models - result of Model3dFacade::getLikesCounts */

/** @var FrontUser $user Current user */
/** @var \frontend\components\FrontendWebView $this */
$user = UserFacade::getCurrentUser();

/**
 * Resolve user like this model
 *
 * @param $user
 * @param $model
 * @return bool|null
 */
$isLikedFn = function ($user, StoreUnit $model) {
    if (is_guest() || !$user) {
        return false;
    }

    return LikeService::isModelLikedByUser($model->model3d, $user);
};

/** @var bool $isOwner current user is owner store unit */
$cover = new CoverInfo(Model3dFacade::getCover($model->model3d));
$isOwner = UserFacade::isObjectOwner($model->model3d);
$itemLink = Model3dFacade::getStoreUrl($model->model3d, ['widget' => $this->isWidgetMode() ?  Model3dForm::WIDGET_TYPE_VIEW_MODEL_BUY_WIDGET : null]);
$userLink = UserUtils::getUserStoreUrl($model->model3d->user);

$category = !empty($model->storeUnits2categories) ? $model->storeUnits2categories[0]->category : false;
$isLiked = $isLikedFn($user, $model);

$itemData = [
    'store-item-id' => $model->id,
    'model3d-id'    => $model->model3d->id,
    'is-liked'      => $isLiked,
];

?>
<div class="col-xs-12 col-sm-4 col-md-3">
    <?= Html::beginTag('div', ['class' => 'catalog-item', 'data' => $itemData]) ?>
    <div class="catalog-item__pic">
        <?php /*
        <div class="catalog-item__author">
            <a href="<?= $userLink ?>" class="catalog-item__author__avatar">
                <?= UserUtils::getAvatarForUser($model->model3d->user, 'store'); ?>
            </a>
            <a href="<?= $userLink ?>" class="catalog-item__author__name">
                <?= \H(\frontend\models\user\UserFacade::getFormattedUserName($model->model3d->user)); ?>
            </a>
        </div>
        */ ?>

        <div class="catalog-item__controls">
            <div class="catalog-item__controls__item js-store-item-like <?= $isLiked ? 'js-store-item-like-showed' : '' ?>">
                <span class="tsi tsi-heart"></span>
                <span class="catalog-item__controls__item__hint js-store-item-like-label"><?= _t(
                        'front.catalog',
                        $isLiked ? _t('front', 'Unlike') : _t('front', 'Like')
                    ) ?></span>
            </div>
        </div>

        <a class="catalog-item__pic__img" href="<?= $itemLink; ?>" title="<?= $model->getTitleLabel(); ?>">
            <img src="<?= ImageHtmlHelper::getThumbUrl($cover['image'], ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT); ?>"
                 alt="<?= $model->getTitleLabel(); ?>">
        </a>
    </div>

    <div class="catalog-item__footer">
        <h4 class="catalog-item__footer__title"><a href="<?= $itemLink; ?>"><?= $model->getTitleLabel(); ?></a></h4>

        <div class="catalog-item__footer__price js-catalog-price catalog-price-store-unit-<?= $model->id ?>" data-store-unit-id="<?= $model->id ?>">
        </div>

        <div class="catalog-item__footer__stats">

            <div class="catalog-item__footer__stats__likes">
                <span class="tsi tsi-heart"></span>
                <span
                    class="js-store-item-likes-count <?= $likesCounts[$model->model3d->id] == 0 ? 'js-store-item-likes-first' : '' ?>"><?= LikeService::getLikesCountString(
                        $likesCounts[$model->model3d->id]
                    ); ?></span>
            </div>
        </div>
    </div>
    <?= Html::endTag('div'); ?>
</div>
