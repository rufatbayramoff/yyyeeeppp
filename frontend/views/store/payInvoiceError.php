<?php

/** @var string $message */

/** @var \common\models\PaymentInvoice $paymentInvoice */

use frontend\modules\workbench\components\OrderUrlHelper;

?>
<div class="container" ng-controller="PayOrderController">
    <div class="row">
        <div class="col-sm-8 wide-padding--right">
            <h1><?php echo _t('site.store', 'Payment'); ?></h1>
            <?= $message ?> <?php
            if ($paymentInvoice->storeOrder) {
                ?>
                <a href="<?= OrderUrlHelper::viewByCustomer($paymentInvoice->storeOrder) ?>"> <?= _t('site.store', 'View order') ?> </a>.
                <?php
            } ?>
        </div>
    </div>
</div>