<?php
/** @var $invoice \common\models\PaymentBankInvoice */

use common\models\PaymentBankInvoiceItem;

?>

<div class="container invoice">
    <div class="row">
        <div class="col-sm-12">
            <div class="invoice__header">
                <img src="/static/images/ts-logo.svg" alt="Tratstock" class="invoice__logo" width="190px" height="24px">
                Invoice #<?= $invoiceId; ?>
            </div>
        </div>
    </div>
    <div class="row m-b20">
        <div class="col-sm-4">
            <h4 class="m-b0">Bill To:</h4>
            <p>
                <?= $invoice->bill_to; ?>
            </p>
        </div>
        <div class="col-sm-4">
            <h4 class="m-b0">Ship To:</h4>
            <p>
                <?= $invoice->ship_to; ?>
            </p>
        </div>
        <div class="col-sm-4">
            <div class="table-responsive">
                <table class="table--invoice-data">
                    <tbody>
                    <tr>
                        <th>Creation Date:</th>
                        <td><?= Yii::$app->formatter->asDate($invoice->created_at); ?></td>
                    </tr>
                    <tr>
                        <th>Due Date:</th>
                        <td><?= Yii::$app->formatter->asDate($invoice->date_due); ?></td>
                    </tr>
                    <tr>
                        <th>PO #:</th>
                        <td><?= $invoice->order_id; ?></td>
                    </tr>
                    <tr>
                        <th>Customer #:</th>
                        <td><?= $invoice->user_id; ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="invoice__table">
                    <tbody>
                    <tr>
                        <th>#</th>
                        <th>Desсription</th>
                        <th>Qty</th>
                        <th>Unit Price</th>
                        <th>Amount</th>
                    </tr>
                    <?php foreach ($invoice->getItemsByPosition([PaymentBankInvoiceItem::TYPE_POSITION]) as $k => $item): ?>
                        <tr>
                            <td>#<?= $k + 1; ?></td>
                            <td><?= $item->title; ?><br/><?= $item->description; ?>
                                # 12, Files: stl.stl (x2) Red, stl2.stl (x1) Blue PLA
                            </td>
                            <td><?= $item->qty; ?></td>
                            <td><?= displayAsCurrency($item->unit_price, $item->currency); ?></td>
                            <td><?= displayAsCurrency($item->unit_price * $item->qty, $item->currency); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <?php foreach ($invoice->getItemsByPosition([PaymentBankInvoiceItem::TYPE_PRICE, PaymentBankInvoiceItem::TYPE_DISCOUNT]) as $item): ?>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2"><?= $item->title; ?></td>
                            <td><?= displayAsCurrency($item->unit_price, $item->currency); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2">
                            <strong>Total</strong>
                        </td>
                        <th><?= displayAsMoney($invoice->getInvoiceTotalAmount()); ?></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <p>Please pay amount (within 10 days) to:</p>
            <?= nl2br(H($invoice->payment_details));?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <hr>
            <h4 class="m-b0">Bank details</h4>
            <?= nl2br(H($invoice->bank_details));?>
            ?>
        </div>
    </div>
</div>
