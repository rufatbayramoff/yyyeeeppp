<?php

use frontend\components\UserSessionFacade;

?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="empty-cart-head">
                <span class="tsi tsi-shopping-cart"></span>
                <h2><?= _t('store.order', 'Your shopping cart is empty'); ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="empty-cart-board empty-cart-board--shop">
                <p>
                    <?= _t('front.site', 'Upload your files to get an instant quote') ?>
                </p>
                <a href="/upload?utm_source=cart_empty" class="btn btn-primary">
                    <?= _t('front.site', 'Upload Files') ?>
                </a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="empty-cart-board">
                <p>
                    <?= _t('front.site', 'Browse and compare services for your project') ?>
                </p>
                <a href="<?= \yii\helpers\Url::toRoute(
                    [
                        '/catalogps/ps-catalog/index',
                        'type'     => '3d-printing-services',
                        'location' => UserSessionFacade::getLocationAsString(UserSessionFacade::getLocation())
                    ]
                ); ?>" class="btn btn-danger">
                    <?=_t('front.site', 'Find a Service')?>
                </a>
            </div>
        </div>
    </div>
</div>
