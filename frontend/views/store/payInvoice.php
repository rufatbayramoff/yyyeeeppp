<?php

use common\components\JsObjectFactory;
use common\models\UserAddress;
use common\modules\payment\serializers\PaymentInvoiceSerializer;
use common\modules\payment\serializers\PayOrderSerializer;
use common\modules\payment\services\PaymentAccountService;
use frontend\controllers\catalog\actions\serializers\ModelStateLiteSerializer;
use frontend\models\ps\PsFacade;
use lib\delivery\delivery\DeliveryFacade;
use lib\money\Money;
use yii\widgets\DetailView;

/**
 * @var \common\models\StoreOrder $storeOrder
 * @var \common\models\PaymentInvoice $primaryInvoice
 * @var \common\models\PaymentInvoice $invoiceBraintree
 * @var \common\models\PaymentInvoice $invoiceTs
 * @var \common\models\PaymentInvoice $invoiceBonus
 * @var \common\models\PaymentInvoice $invoiceBankTransfer
 * @var string $braintreeClientToken
 * @var string $stripeClientToken
 * @var string $stripeAlipayClientToken
 * @var Money $amountNetIncome
 * @var string $defaultPayCardVendor
 */

$serializedPrimaryInvoice = PaymentInvoiceSerializer::serialize($primaryInvoice);
$serializedStoreOrder     = PayOrderSerializer::serialize($storeOrder);
$model3d                  = $storeOrder?->getFirstReplicaItem();

Yii::$app->angular
    ->service(['notify', 'user', 'router'])
    ->controller([
        'store/filepage/models',
        'store/filepage/model-render',
    ])
    ->controller([
        'payment/controllers/payOrderController',
        'payment/models/PayOrder',
        'payment/models/PaymentInvoice',
        'payment/directives/checkoutPaymentMethods'
    ])
    ->controllerParam('model3dState', $model3d ? ModelStateLiteSerializer::serialize($model3d) : null)
    ->controllerParam('invoice', $serializedPrimaryInvoice)
    ->controllerParam('PayOrder', $serializedStoreOrder)
    ->constants(
        [
            'globalConfig' =>
                [
                    'staticUrl' => param('staticUrl')
                ]
        ]
    );

JsObjectFactory::createJsObject(
    'model3dRenderRouteClass',
    'model3dRenderRoute',
    [
        'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl . '/'
    ],
    $this
);

?>
<div class="container" ng-controller="PayOrderController">
    <div class="row">
        <div class="col-sm-8 wide-padding--right">
            <h1><?php echo _t('site.store', 'Payment'); ?></h1>
            <span ng-cloak class="ng-cloak-loader wide-padding--right">
                <img src="/static/images/preloader.gif" width="60" height="60">
            </span>
            <span ng-cloak="">
                <?php echo $this->render('@common/modules/payment/views/checkoutCard', [
                    'logUuid'                 => $logUuid,
                    'storeOrder'              => $storeOrder,
                    'invoiceBraintree'        => $invoiceBraintree,
                    'invoiceTs'               => $invoiceTs,
                    'invoiceBonus'            => $invoiceBonus,
                    'invoiceBankTransfer'     => $invoiceBankTransfer,
                    'braintreeClientToken'    => $braintreeClientToken,
                    'stripeClientToken'       => $stripeClientToken,
                    'stripeAlipayClientToken' => $stripeAlipayClientToken,
                    'widget'                  => false,
                    'posUid'                  => false,
                    'amountNetIncome'         => $amountNetIncome,
                    'defaultPayCardVendor'    => $defaultPayCardVendor
                ]); ?>
            </span>
        </div>

        <div class="col-sm-4 sidebar m-t30" ng-cloak>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><?php echo _t('site.store', 'Billing Details'); ?></h3>
                </div>

                <div class="panel-body">
                    <div ng-repeat="invoiceItem in paymentInvoice.invoiceItems" class="billing-row">
                        <div class="billing-row__block">{{invoiceItem.title}}</div>
                        <div class="billing-row__block"><strong>{{invoiceItem.total_line}}</strong></div>
                    </div>
                </div>

                <div class="panel-body" ng-if="paymentInvoice.accruedBonus && paymentInvoice.accruedBonus.amount">
                    <div class="billing-row">
                        <div class="billing-row__block"><?php echo _t('site.store', 'Accrued bonus') ?></div>
                        <div class="billing-row__block"><strong>+ {{paymentInvoice.accruedBonus.amount}}</strong></div>
                    </div>
                </div>

                <div style="padding: 15px;" ng-if="paymentInvoice.hasPromoCode()">
                    <?php echo _t('site.store', 'Promo codes:') ?>
                    <div class="row m-b10">
                        <div class="col-xs-12">
                            {{paymentInvoice.storeOrderPromocode.promocode}}
                        </div>
                    </div>
                    <div class="delivery-order__delivery-price">
                        <div class="delivery-order__delivery-price__price delivery_price price__badge">
                            <span class="price__badge">{{paymentInvoice.storeOrderPromocode.amountTotal}}</span>
                        </div>
                        <h5 class="delivery-order__delivery-price__title"><?= _t('site.store', 'Total Discount'); ?>:</h5>
                    </div>
                </div>

                <div class="panel-price">
                    <div class="panel-price__body">
                        <div class="panel-price__body__price total_price">
                            <?php echo _t('site.store', 'Total') ?>
                        </div>
                        <div class="pull-right panel-price__body__price ">
                            {{paymentInvoice.amountTotal}}
                        </div>
                    </div>
                </div>
            </div>

            <span>
                <div class="panel panel-default" ng-if="hasAddress()">
                    <div class="panel-heading">
                        <h3>
                            <span ng-if="payOrder.isPickupDelivery"><?php echo _t('site.store', 'Pickup Address'); ?></span>
                            <span ng-if="!payOrder.isPickupDelivery"><?php echo _t('site.store', 'Shipping Address'); ?></span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="detail-view--custom-view">
                            <table class="table table-striped table-bordered detail-view">
                                <tbody>
                                <tr ng-if="!payOrder.isPickupDelivery">
                                    <th>
                                        <span ng-if="payOrder.shipAddress.contactName"><?php echo _t('site.store', 'Name'); ?></span>
                                        <span ng-if="!payOrder.shipAddress.contactName"><?php echo _t('site.store', 'Company'); ?></span>
                                    </th>
                                    <td>
                                        {{payOrder.shipAddress.contactName ? payOrder.shipAddress.contactName : payOrder.shipAddress.companyName}}
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo _t('site.store', 'Address'); ?></th>
                                    <td>
                                        <span ng-bind-html="payOrder.shipAddress.address | safeHtml"></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" ng-if="(payOrder.model3d.parts|numkeys)>0">
                <div class="panel-heading printed-file__title">
                    <h3>
                        <span ng-if="(payOrder.model3d.parts|numkeys) <= 1" class="m-r10">
                            {{payOrder.model3d.parts | numkeys}} <?= _t('site.store', 'File'); ?>
                        </span>
                        <span ng-if="(payOrder.model3d.parts|numkeys) > 1" class="m-r10">
                            {{payOrder.model3d.parts | numkeys}} <?= _t('site.store', 'Files'); ?>
                        </span>
                        <span class="printed-file__toggle collapsed" data-toggle="collapse" data-target="#fileList" aria-expanded="false" aria-controls="fileList">
                            <?= _t('site.store', 'Show'); ?><span class="tsi tsi-down"></span>
                        </span>
                    </h3>
                </div>
                <div class="panel-body collapse p-b0" id="fileList">
                    <div class="printed-file-list p-b15">

                        <div class="printed-file__item" ng-repeat="model3dPart in payOrder.model3d.parts">
                            <div class="printed-file__content" ng-if="!model3dPart.isCanceled && model3dPart.qty">
                                <div class="printed-file__pic">
                                    <img ng-src="{{getModel3dPartPreviewUrl(model3dPart)}}" main-src="{{getModel3dPartPreviewUrl(model3dPart)}}" alt-src="/static/images/preloader80.gif"
                                         onerror="commonJs.checkAltSrcImage(this);"></div>
                                <div class="printed-file__body">
                                    <div class="printed-file__name" title="{{model3dPart.title}}">
                                        {{model3dPart.title}}
                                    </div>
                                    <div class="printed-file__material">
                                        <div title="{{model3dTextureState.getTexture(model3dPart).getPrinterMaterialTitle()}}" class="material-item">
                                            <div class="material-item__color" style="background-color: #{{model3dTextureState.getTexture(model3dPart).printerColor.getRgbHex()}}"></div>
                                        </div>
                                        {{model3dTextureState.getTexture(model3dPart).getPrinterMaterialTitle()}}
                                    </div>
                                </div>
                                <div class="printed-file__count">
                                    x{{model3dPart.qty}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </span>
        </div>
    </div>
</div>