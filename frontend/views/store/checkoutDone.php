<?php
/** @var \yii\web\View $this */
/** @var \common\models\PaymentInvoice $paymentInvoice */
/** @var \common\models\StoreOrder $storeOrder */

/** @var array $result */

use common\modules\googleAnalitics\widget\GoogleAnalyticsEcWidget;
use common\modules\instantPayment\helpers\InstantPaymentUrlHelper;

$this->title = 'Checkout complete';
$storeOrder  = $paymentInvoice->storeOrder;

$hrefParams = ['class' => 'btn btn-primary btn-ghost'];

if ($isWidget) {
    $hrefParams['target'] = '_top';
}

echo GoogleAnalyticsEcWidget::purchase($paymentInvoice);

?>
<div class="container <?= $isWidget ? ' aff-widget__checkout-page' : '' ?>">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="text-center m-t30"><?= _t("front.store", "Checkout complete"); ?></h1>
            <div class="text-center">
                <p><span style='font-size: 96px; color:#82c015; margin-bottom: 10px;'
                         class='tsi tsi-checkmark-c'></span></p>
            </div>
            <div class="row m-t30 m-b10">
                <div class="col-sm-6 col-sm-offset-3">

                    <table class="table checkout-table">
                        <tr>
                            <th class="p-l0 font-weight-normal">
                                <?= _t('site.store', 'Invoice code'); ?>:
                            </th>
                            <td>
                                <strong>
                                    <?= $paymentInvoice->uuid; ?>
                                </strong>
                            </td>
                        </tr>
                        <?php
                        if ($paymentInvoice->store_order_id) {
                            ?>
                            <tr>
                                <th class="p-l0 font-weight-normal">
                                    <?= _t('site.store', 'Order id'); ?>:
                                </th>
                                <td>
                                    <strong>
                                        <span id="t_store_order_id"><?= $paymentInvoice->store_order_id; ?></span>
                                    </strong>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <th class="p-l0" colspan="2">
                                <hr class="m-t10 m-b10">
                            </th>
                        </tr>
                        <tr>
                            <th class="p-l0 font-weight-normal">
                                <?= _t('site.store', 'Total Amount'); ?>:
                            </th>
                            <td>
                                <strong>
                                    <?= displayAsMoney($paymentInvoice->getAmountTotalWithRefund()); ?>
                                </strong>
                            </td>
                        </tr>
                    </table>

                    <?php if ($paymentInvoice->tsInternalPurchase && $paymentInvoice->tsInternalPurchase->tsInternalPurchaseCertification) { ?>
                        <h4 class="text-center m-b15">
                            <?= _t('site.store', 'Your payment was successful.'); ?>
                        </h4>
                        <p class="text-center m-b0">
                            <?= _t('site.store', 'Please, proceed to the next step to finish the certification process.'); ?>
                        </p>
                    <?php } elseif ($paymentInvoice->instantPayment) { ?>
                        <p class="text-center"><?= _t('site.store',
                                'Your instant payment to "<b>{title}</b>" was successfully finished.',
                                ['title' => H($paymentInvoice->instantPayment->toUser->company->getTitle())]); ?></p>
                    <?php } elseif ($storeOrder && $storeOrder->currentAttemp) { ?>
                        <p class="text-center"><?= _t('site.store',
                                'Your order was successfully placed with {printServiceName} and a confirmation email should have been sent to your email address.',
                                ['printServiceName' => H($storeOrder->currentAttemp->ps->title)]); ?></p>
                    <?php } elseif ($storeOrder && $storeOrder->isAnonim()) { ?>
                        <p class="text-center"><?= _t('site.store',
                                'Your order was successfully placed with {printServiceName} and a confirmation email should have been sent to your email address.',
                                ['printServiceName' => H($storeOrder->currentAttemp->ps->title)]); ?></p>
                    <?php } ?>
                </div>
            </div>

            <p class="text-center m-b30">
                <?php
                if ($paymentInvoice->tsInternalPurchase && $paymentInvoice->tsInternalPurchase->getCompanyService()) {
                    ?>
                    <a class="btn btn-primary" href="/mybusiness/<?=$paymentInvoice->tsInternalPurchase->getCompanyService()->company->id ?>/edit-printer/<?=$paymentInvoice->tsInternalPurchase->getCompanyService()->psPrinter->id ?>/test" target="_top"><?= _t('site.store', 'Continue'); ?></a>
                    <?php
                } elseif ($paymentInvoice->store_order_id) {
                    ?>
                    <a class="btn btn-primary" href="/workbench/order/view/<?= $paymentInvoice->store_order_id; ?>" target="_top"><?= _t('site.store', 'View My Order'); ?></a>
                    <?php
                } elseif ($paymentInvoice->instant_payment_uuid) {
                    ?>
                    <a class="btn btn-primary" href="<?= InstantPaymentUrlHelper::getView($paymentInvoice->instantPayment); ?>" target="_top"><?= _t('site.store', 'View My Payment'); ?></a>
                    <?php
                }
                ?>
            </p>
        </div>

        <?php /*
        <div class="col-md-4 sidebar m-t10">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-12 col-md-offset-0">
                    <h2 class="text-center m-t30">
                        <?= _t('site.store', 'Review your orders'); ?>
                    </h2>

                    <p class="m-b20">
                        <?= _t('site.store', 'You can review your order, communicate with the supplier and track the progress in My Purchases.'); ?> <br/>
                    </p>


                    <p class="text-center">
                        <?php
                        echo \yii\helpers\Html::a(
                            _t('front', 'My Purchases'),
                            \yii\helpers\Url::toRoute(['/workbench/orders/new']),
                            $hrefParams
                        );
                        ?>
                    </p>
                </div>
            </div>
        </div>
        */ ?>
    </div>
</div>