<?php
/* @var \frontend\models\delivery\DeliveryForm $deliveryForm */
/* @var \common\models\Cart $cart */
/* @var \common\models\CompanyService $machine */
/* @var \common\models\UserAddress[] $otherAddresses */
/* @var string $userCurrency */

/* @var \common\models\User $currentUser */

use common\components\order\PriceCalculator;
use common\components\serizaliators\porperties\DeliveryRateProperties;
use common\models\Promocode;
use common\models\Ps;
use common\models\StorePricer;
use common\models\UserAddress;
use common\modules\promocode\components\PromocodeApplyService;
use common\modules\promocode\components\PromocodeDiscountCalc;
use common\services\Model3dPartService;
use frontend\components\UserSessionFacade;
use frontend\models\model3d\Model3dFacade;
use frontend\models\user\UserFacade;
use lib\delivery\parcel\ParcelFactory;
use lib\money\Money;
use common\modules\payment\fee\FeeHelper;
use yii\helpers\Html;


throw new \common\components\exceptions\DeprecatedException('Possible is depricated');

$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);

$currencySymbol = frontend\components\UserSessionFacade::getCurrencySymbol($userCurrency);

$onlyPickup        = $machine->asDeliveryParams()->onlyPickup();
$hasDeliveryPickup = $machine->asDeliveryParams()->hasDeliveryPickup();

/**
 * get ps printer summary
 *
 * @return array
 */
$summaryFn = function () use ($machine) {
    $userLocation = UserSessionFacade::getLocation();
    $distance     = $machine->getDistance($userLocation['lat'], $userLocation['lon']);
    $responseTime = '-';
    $lastOnline   = '-';
    if ($stat = $machine->ps->user->userStatistics) {
        $lastOnline = UserFacade::getUserOnlineStatus($stat);
    }
    $reviewsHtml = \frontend\widgets\PsPrintServiceReviewStarsWidget::widget([
        'ps'              => $machine->ps,
        'fromReviewCount' => 0
    ]);
    if (empty($reviewsHtml)) {
        if ($machine->comapny->allowShowBeFirstCommentator()) {
            $reviewsHtml = _t('site.ps', 'Be the first to leave a review');
        }
    }
    $summary         = [
        'layerResolution' => '',
        'distance'        => $distance,
        'reviews'         => $reviewsHtml,
        'responseTime'    => $responseTime,
        'status'          => $lastOnline,
    ];
    $layerResolution = \frontend\models\ps\PsFacade::getPrinterLayerResolution($machine);

    if (empty($layerResolution))
        $summary['layerResolution'] = _t('site.common', 'Not set');
    else
        $summary['layerResolution'] = $layerResolution;
    return $summary;
};

$printerSummary  = $summaryFn();
$printerDistance = (int)$printerSummary['distance'];
$qty             = $cart->getQty();
$feeModelPrice   = $feePrintPrice = 0;
$feeHelper       = new FeeHelper();
$totalFee        = $feeHelper->getTsMinFee();


$promocodeService = Yii::createObject(PromocodeApplyService::class);
/** @var Promocode[] $promosApplied */
$promosApplied = $promocodeService->getAppliedPromos();

$printPrice = PriceCalculator::calculateModel3dPrintPriceWithPackageFee($cart->getModel3d(), $machine->asPrinter());
?>


    <div ng-cloak class="ng-cloak-loader">
        <img src="/static/images/preloader.gif" width="60" height="60">
    </div>


    <div class="container" ng-controller="OrderCheckoutDeliveryController" ng-cloak="">
        <div class="store-delivery row">
            <div class="col-sm-8 wide-padding--right">
                <div class="row" ng-if="havePickupAndDelivery()">
                    <div class="col-sm-12 m-b20">
                        <h3 class="delivery-options__title"><?= _t('site.delivery', 'Shipping options (select one):') ?></h3>

                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                <div class="radio radio-inline">
                                    <input
                                            ng-model="view.selectedDeliveryType"
                                            type="radio" id="delivery_type_pickup" value="pickup"/>
                                    <label for="delivery_type_pickup"><?= _t('site.delivery', 'Pickup') ?></label>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                <div class="radio radio-inline">
                                    <input
                                            ng-model="view.selectedDeliveryType"
                                            type="radio" id="delivery_type_delivery" value="standard"/>
                                    <label for="delivery_type_delivery"><?= _t('site.delivery', 'Delivery') ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <?php
                    if ($deliveryForm->hasErrors()) {
                        echo yii\helpers\Html::errorSummary($deliveryForm, ['class' => 'text-danger']);
                    }
                    ?>

                    <?php if ($hasDeliveryPickup): ?>

                        <div
                                ng-if="view.selectedDeliveryType == DELIVERY_TYPE_PICKUP"
                                class="col-sm-12 js-type-pickup">
                            <?=
                            $this->render("deliveryPickup", [
                                'deliveryForm' => $deliveryForm,
                                'machine'      => $machine,
                                'currentUser'  => $currentUser,
                            ]);
                            ?>
                        </div>

                    <?php endif; ?>


                    <?php if (!$onlyPickup): ?>

                        <div
                                ng-if="view.selectedDeliveryType == DELIVERY_TYPE_DELIVERY"
                                class="col-sm-12 js-type-delivery">
                            <?=
                            $this->render("deliveryCarrier", [
                                'deliveryForm' => $deliveryForm,
                                'machine'      => $machine,
                                'currentUser'  => $currentUser,
                            ]);
                            ?>
                        </div>

                    <?php endif; ?>

                </div>
                <a href="/store/cart/clear" class="btn btn-default btn-sm delivery-details__empty-btn m-t20">
                    <span class="tsi tsi-bin m-r10"></span>
                    <?= _t('front.site', 'Empty cart'); ?>
                </a>
            </div>

            <div class="col-sm-4 sidebar m-t30">
                <!-- right side --->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-heading__title">
                            <?= _t('front.site', 'Your Order'); ?>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <?php
                            $model3d    = $cart->getModel3d();
                            $modelPrice = 0;
                            $totalPrice = 0;
                            foreach ($cart->cartItems as $cartItem):
                            $cartItem->price = \common\components\PaymentExchangeRateFacade::convert($cartItem->price, $cartItem->currency->currency_iso, $userCurrency);
                            if ($cartItem->currency->currency_iso !== $userCurrency) {
                                $cartItem->setCurrencyIso($userCurrency);
                                $cartItem->save(0);
                            }
                            $model3d       = $cartItem->model3dReplica;
                            $model3dParts  = $model3d->getCalculatedModel3dParts();
                            $itemTitle     = $cartItem->model3dReplica->title;
                            $itemLink      = $cartItem->model3dReplica->getViewUrl();
                            $coverImage    = Model3dFacade::getCover($cartItem->model3dReplica);
                            $modelPrice    += (float)$cartItem->price * (int)$cartItem->qty;
                            $feeModelPrice = $feeHelper->getTsFee(Money::create($cartItem->price * $cartItem->qty, $cartItem->currency->currency_iso));
                            $feePrintPrice = $feeHelper->getTsFee(Money::create($printPrice, $cartItem->currency->currency_iso));
                            $totalFee      = max($totalFee, $feeModelPrice + $feePrintPrice);

                            if ($model3d->isOneTextureForKit()) {
                                $texture = $model3d->getKitTexture();
                                ?>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                            <?= _t('site.delivery', 'Material'); ?>:
                                        </div>
                                        <div class="col-xs-8 col-sm-12 col-lg-8">
                                            <b><?= H($texture->printerMaterial->title); ?> </b>

                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty($texture->printerMaterialGroup->fill_percent)): ?>
                                    <div class="col-xs-12 m-t10">
                                        <div class="row">
                                            <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                                <?= _t('site.delivery', 'Infill'); ?>:
                                            </div>
                                            <div class="col-xs-8 col-sm-12 col-lg-8">
                                                <b><?= H($texture->printerMaterialGroup->fill_percent); ?>%</b>

                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="col-xs-12 m-t10">
                                    <div class="row">
                                        <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                            <?= _t('site.delivery', 'Color'); ?>:
                                        </div>
                                        <div class="col-xs-8 col-sm-12 col-lg-8">
                                            <b><?= H($texture->printerColor->title); ?></b>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                            <?= _t('site.delivery', 'Material'); ?>:
                                        </div>
                                        <div class="col-xs-8 col-sm-12 col-lg-8">
                                            <b><?= _t('site.delivery', 'Multicolor') ?> </b>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <?php /*
                                Hide print quantity
                                <div class="col-xs-12 m-t10">
                                    <div class="row">
                                        <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                            <?= _t('site.store', 'Print quantity'); ?>:
                                        </div>
                                        <div class="col-xs-8 col-sm-12 col-lg-8">
                                            <b><?= $cartItem->qty; ?></b>
                                        </div>
                                    </div>
                                </div>
                                */
                            ?>
                            <div class="col-xs-12 col-lg-8 col-lg-offset-4 m-t10">
                                <?php
                                echo yii\helpers\Html::tag(
                                    'span',
                                    _t('front.site', 'Choose specific materials'),
                                    [
                                        'data-url'      => '/store/cart/change',
                                        'class'         => 'btn btn-sm btn-default ts-ajax-modal',
                                        'data-method'   => 'post',
                                        //'ng-click' => 'removePromocode()',
                                        'title'         => _t('site.store', 'Edit order details'),
                                        'data-target'   => '#modal-getPrinted',
                                        'oncontextmenu' => 'return false;'
                                    ]
                                );

                                ?>
                            </div>
                        </div>

                        <hr/>

                        <div class="delivery-order">

                            <div class="delivery-order__head">

                                <?php
                                $totalModelPrice = $cartItem->price * $cartItem->qty;
                                if ($totalModelPrice > 0) :
                                    ?>

                                    <div class="delivery-order__head__price price__badge">
                                        <?= displayAsCurrency($totalModelPrice, $userCurrency); ?>
                                    </div>
                                <?php endif; ?>


                                <h4 class="delivery-order__head__title">
                                    <?= H($itemTitle); ?>
                                    <div class="delivery-order__head__title-qty">
                                        <?php if ($cartItem->qty > 1) {
                                            echo _t('store.delivery', ' Print quantity: {qty}', ['qty' => $cartItem->qty]);
                                        } ?>
                                    </div>
                                </h4>
                            </div>

                            <?php foreach ($model3dParts as $model3Part):
                                $props = $model3Part->model3dPartProperties;
                                $imgSrc = Model3dPartService::getModel3dPartRenderUrlIfExists($model3Part);
                                $img = Html::img($imgSrc, ['width' => '80']);
                                ?>
                                <table class="table delivery-order__table">
                                    <tbody>
                                    <tr>
                                        <td class="delivery-order__table__pic">
                                            <?php echo $img; ?>
                                            <span class="qty-badge">x <?= $model3Part->qty; ?></span>
                                        </td>
                                        <td>
                                            <span class="delivery-order__table__model-name"><?= $model3Part['name']; ?> </span>
                                            <br/>
                                            <span class="delivery-order__table__model-size">
                                            <?= _t('site.store', 'Size'); ?>: <?= $model3Part->size->toStringWithMeasurement(); ?></span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php endforeach; ?>


                            <?php if ($printPrice) : ?>

                                <div class="delivery-order__print-price">
                                    <div class="delivery-order__print-price__price print_price price__badge">
                                        <?= displayAsCurrency($printPrice, $userCurrency); ?>
                                    </div>
                                    <h5 class="delivery-order__print-price__title"><?= _t('site.store', 'Manufacturing Fee'); ?></h5>
                                </div>

                            <?php endif; ?>


                            <?php if ($totalFee): ?>

                                <div class="delivery-order__delivery-price">
                                    <div class="delivery-order__delivery-price__price price__badge">
                                        <?= displayAsCurrency($totalFee, $userCurrency); ?></div>
                                    <h5 class="delivery-order__delivery-price__title"><?= _t('site.store', 'Service Fee'); ?></h5>
                                </div>

                            <?php endif; ?>

                            <div class="delivery-order__delivery-price" ng-if="hasShippingPrice()">
                                <div class="delivery-order__delivery-price__price delivery_price price__badge">
                                    {{getShippingPrice()}}
                                </div>
                                <h5 class="delivery-order__delivery-price__title"><?= _t('site.store', 'Shipping Fee'); ?></h5>
                            </div>

                        </div>
                        <?php endforeach; ?>

                        <div ng-if="hasPromocode()">
                            <div class="row m-b10">
                                <div class="col-xs-12">
                                    {{getPromocode()}}
                                    <button class="btn btn-default btn-sm" loader-click="removePromocode()"
                                            title="<?= _t('site.store', 'Delete code'); ?>"
                                            style="padding: 4px;font-size: 14px;">&times;
                                    </button>
                                </div>
                            </div>
                            <div class="delivery-order__delivery-price">
                                <div class="delivery-order__delivery-price__price delivery_price price__badge">
                                    <span class="price__badge js-discount">{{getTotalDiscount()}}</span>
                                </div>
                                <h5 class="delivery-order__delivery-price__title"><?= _t('site.store', 'Total Discount'); ?>
                                    :</h5>
                            </div>
                        </div>
                        <div class="delivery-order__promo" ng-if="noPromocode()">
                            <div class="delivery-order__promo-label">
                                <?= _t('site.store', 'Promo code?'); ?>
                            </div>
                            <div class="delivery-order__promo-actions input-group">
                                <input class="form-control input-sm" type="text" name="code" id="promocode"/>
                                <span class="input-group-btn">
                                        <input class="btn btn-info btn-sm" type="button"
                                               value="<?= _t('site.store', 'Apply') ?>"
                                               loader-click="applyPromocode()"/>
                                    </span>
                            </div>
                        </div>
                    </div>

                    <div class="panel-price">
                        <h5 class="panel-price__title"><?= _t('site.store', 'Pricing Summary'); ?>: </h5>

                        <div class="panel-price__body">
                            <div class="panel-price__body__price total_price">
                                {{getTotalPrice()}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-heading__title">
                            <?= _t('front.site', 'Printing at'); ?>
                        </div>
                        <div class="panel-heading__action" style="display: none;">
                            <a href="<?php echo Model3dFacade::getStoreUrl($model3d->originalModel3d); ?>" style="">
                                <?= _t('front.site', 'Select another printer'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="row delivery-printing-place">
                            <div class="delivery-printing-place__avatar">
                                <?= Html::img(Ps::getCircleImage($machine)); ?>
                            </div>
                            <div class="delivery-printing-place__description">
                                <div class="delivery-printing-place__model">
                                    <?= H($machine->ps->title); ?>
                                </div>
                                <div class="delivery-printing-place__distance">
                                    <?php ?>
                                    <?= _t('site.printer', 'Distance'); ?>: <?= $printerSummary['distance']; ?>
                                    <p>
                                        <?= UserAddress::formatMachineAddress($machine, "%city%, %region%, %country%"); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="delivery-printing-place__status">
                                <?= _t('site.printer', 'Layer Resolution'); ?>
                                : <?= $printerSummary['layerResolution']; ?>
                            </div>
                            <div class="delivery-printing-place__reviews">
                                <?= _t('site.printer', 'Rating'); ?>: <?= $printerSummary['reviews']; ?>
                            </div>
                            <div class="delivery-printing-place__status">
                                <?= _t('site.printer', 'Status'); ?>: <?= $printerSummary['status']; ?>
                            </div>
                        </div>
                        <?php if ($cart->canChangePs()): ?>
                            <div class="alert alert-info alert--text-normal delivery-printing-alert">
                                <?php echo _t('site.order', 'Chosen print shop may be changed if it\'s necessary to complete the order.'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/ng-template" id="/app/store/checkuot/delivery/offer-login-modal.html">
        <div class="modal fade modal-primary" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?= _t('site.ps', 'Please login') ?></h4>
                    </div>
                    <div class="modal-body">
                        <?= _t('store.order', 'Login to your Treatstock account to keep track of the status of your order.') ?>
                    </div>
                    <div class="modal-footer">
                        <button
                                ng-click="login()"
                                type="button" class="btn btn-primary"><?= _t('site.ps', 'Login') ?></button>
                        <button
                                ng-click="proceedWithoutLogin()"
                                type="button"
                                class="btn btn-default"><?= _t('site.ps', 'Proceed without logging in') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </script>

<?php
if (app('request')->isPost) {
    $deliveryForm->load(app('request')->post());
}
Yii::$app->angular->service(['notify', 'router', 'modal', 'user', 'discount', 'session-storage'])
    ->controllerParams([
        'defaultDeliveryType' => $deliveryForm->getDefaultDeliveryType($machine->asDeliveryParams(), $printerDistance),
        'hasDeliveryPickup'   => $hasDeliveryPickup,
        'onlyPickup'          => $onlyPickup,
        'printPrice'          => $printPrice,
        'totalFeePrice'       => $totalFee,
        'modelPrice'          => (float)$modelPrice,
        'currency'            => $currencySymbol,
        'deliveryForm'        => $deliveryForm,
        'promocode'           => count($promosApplied) > 0 ? end($promosApplied)->code : null,
        'rates'               => !$onlyPickup && ($estimateDeliveryRate = Yii::$app->deliveryService->getEstimateDeliveryRate(
            $machine->asDeliveryParams(),
            Money::create($printPrice ?? 0, $userCurrency),
            UserSessionFacade::getLocation(),
            ParcelFactory::createFromCart($cart),
            $userCurrency
        )) ? [DeliveryRateProperties::serialize($estimateDeliveryRate)] : null
    ])
    ->constant('discountSettings', PromocodeDiscountCalc::getDiscountSettings($promosApplied, $userCurrency))
    ->controller('store/checkout/delivery')
    ->controller('store/checkout/deliveryDirectives');
