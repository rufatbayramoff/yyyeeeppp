<?php

use common\modules\payment\serializers\PaymentInvoiceSerializer;
use common\modules\payment\serializers\PayOrderSerializer;
use lib\money\Money;

/**
 * @var \common\models\StoreOrder $storeOrder
 * @var \common\models\PaymentInvoice $invoiceBraintree
 * @var \common\models\PaymentInvoice $invoiceTs
 * @var \common\models\PaymentInvoice $invoiceBonus
 * @var \common\models\PaymentInvoice $invoiceBankTransfer
 * @var string $clientToken
 * @var Money $amountNetIncome
 * @var \common\models\StoreOrderPosition $storeOrderPosition
 */

Yii::$app->angular
    ->service(['notify', 'user', 'router'])
    ->controller([
        'payment/controllers/payOrderPositionController',
        'payment/models/PaymentInvoice',
    ])
    ->controllerParam('PaymentInvoice', PaymentInvoiceSerializer::serialize($storeOrderPosition->primaryPaymentInvoice));
?>

<div class="container" ng-controller="PayOrderPositionController">
    <div class="row">
        <div class="col-sm-8 wide-padding--right">
            <h1><?php echo _t('site.store', 'Payment'); ?></h1>
            <span ng-cloak class="ng-cloak-loader wide-padding--right">
                <img src="/static/images/preloader.gif" width="60" height="60">
            </span>
            <span ng-cloak="">
                <?php echo $this->render('@common/modules/payment/views/checkoutCard', [
                    'invoiceBraintree'    => $invoiceBraintree,
                    'invoiceTs'           => $invoiceTs,
                    'invoiceBonus'        => $invoiceBonus,
                    'invoiceBankTransfer' => $invoiceBankTransfer,
                    'clientToken'         => $clientToken,
                    'widget'              => false,
                    'posUid'              => false,
                    'amountNetIncome'     => $amountNetIncome
                ]); ?>
            </span>
        </div>

        <div class="col-sm-4 sidebar m-t30" ng-cloak>
            <div class="panel panel-default" ng-if="hasBillingDetails()">
                <div class="panel-heading">
                    <h3><?php echo _t('site.store', 'Billing Details'); ?></h3>
                </div>

                <div class="panel-body">
                    <div  ng-repeat="invoiceItem in paymentInvoice.invoiceItems" class="billing-row">
                        <div class="billing-row__block">{{invoiceItem.title}}</div>
                        <div class="billing-row__block"><strong>{{invoiceItem.total_line}}</strong></div>
                    </div>
                </div>

                <div class="panel-body" ng-if="paymentInvoice.accruedBonus">
                    <div class="billing-row">
                        <div class="billing-row__block"><?php echo _t('site.store', 'Accrued bonus') ?></div>
                        <div class="billing-row__block"><strong>+ {{paymentInvoice.accruedBonus}}</strong></div>
                    </div>
                </div>

                <div class="panel-price">
                    <div class="panel-price__body">
                        <div class="panel-price__body__price total_price">
                            <?php echo _t('site.store', 'Total') ?>
                        </div>
                        <div class="pull-right panel-price__body__price ">
                            {{paymentInvoice.amountTotal}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>