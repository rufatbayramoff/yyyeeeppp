<?php
use common\models\DeliveryType;
use common\models\User;
use common\models\UserAddress;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
/* @var \frontend\models\delivery\DeliveryForm $deliveryForm */
/* @var User $currentUser */
/* @var \common\models\CompanyService $machine */

$formPickup = ActiveForm::begin(['enableClientValidation' => false, 'action' => ['store/delivery']]);
$deliveryForm->deliveryType = DeliveryType::PICKUP;

Yii::$app->angular->controllerParams([
    'pickupDeliveryForm' => $deliveryForm->getAttributes(['email', 'contact_name', 'phone']),
    'mapConfig' => [
        'address' => str_replace("\n", " ", UserAddress::formatAddress($machine->asDeliveryParams()->getUserAddress(), true)),
        'lat' => $machine->location->lat,
        'lon' => $machine->location->lon,
    ]
]);
?>

<div ng-controller="OrderCheckoutDeliveryPickupController">

<div class="row">
    <div class="col-sm-12 delivery-details">
        <h3 class="delivery-details__title"><?= _t('front.site', "Recipient's details"); ?></h3>
        <div class="row">
            <?=
                $formPickup->field($deliveryForm, 'contact_name', ['options'=>['class'=>'form-group col-sm-7 col-md-7']])
                    ->textInput(['ng-model' => 'pickupDeliveryForm.contact_name'])
                    ->label(_t('site.delivery', 'Full name'));
            ?>

            <?=
                $formPickup->field($deliveryForm, 'phone', ['options'=>['class'=>'form-group col-sm-5 col-md-5']])
                    ->textInput(['ng-model' => 'pickupDeliveryForm.phone'])
                    ->label(_t('site.delivery', 'Phone'));
            ?>

            <?=
                $currentUser->isAnonimUser()
                    ? $formPickup->field($deliveryForm, 'email', ['options'=>['class'=>'form-group col-sm-5 col-md-5']])
                        ->textInput(['ng-model' => 'pickupDeliveryForm.email'])
                    : $formPickup->field($deliveryForm, 'email')
                        ->hiddenInput(['ng-model' => 'pickupDeliveryForm.email'])
                        ->label(false);
            ?>

            <?=
                $formPickup->field($deliveryForm, 'deliveryType')
                    ->hiddenInput(['value' => DeliveryType::PICKUP])
                    ->label(false);

                $deliveryForm->deliveryType = null;
            ?>
        </div>

    </div>
    <div class="col-sm-12">

        <div class="row">
            <div class="col-md-5">
                <?= Html::tag('h3', _t('site.delivery', 'Pickup address')); ?>
                <?= UserAddress::formatAddress($machine->asDeliveryParams()->getUserAddress(), true);?>
                <?php
                if ($comment = $machine->asDeliveryParams()->getPsDeliveryTypeByCode(DeliveryType::PICKUP)->comment) {
                    echo Html::tag('br');
                    echo Html::tag('br');
                    echo Html::tag('b', _t('site.delivery', 'Working hours'));
                    echo Html::tag('br');
                    echo Html::tag('p', Html::encode($comment));
                }
                ?>
            </div>
            <div class="col-md-7">

                <div
                    printer-location-map="view.printerLocation"
                    style="height:300px; width:100%; margin: 30px 0 20px;"></div>

            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <h4 class="delivery-details-type__title">
            <?= _t('site.store', 'Comment'); ?>
        </h4>
        <?=
        $formPickup->field($deliveryForm, 'comment', ['options'=>['class'=>'form-group']])
            ->textarea(['maxlength' => '600'])
            ->label(_t('site.delivery', 'Leave a comment if you have any requests, instructions, or additional information for the print service. If there are parts in your order that need to fit one another, please specify which parts to the print shop.'));
        ?>
    </div>
</div>
<div class="row delivery-details-submit">
    <div class="col-sm-7 delivery-details-submit__info">
        <?= _t('site.shop', 'To proceed to checkout please click "Next: Payment".'); ?><br>
        <?= _t('site.shop', "We do not store your credit card details."); ?>
        <?=
            $currentUser->isAnonimUser() ? "<br/><br/>"._t('site.order', ' By using this Service, you agree to our {terms} and that you have read our {policy}, including our {cookie}.', [
                'terms' => Html::a('Terms', ['/site/terms'], ['target' => '_blank']),
                'policy' => Html::a('Privacy Policy', ['/site/policy'], ['target' => '_blank']),
                'cookie' => Html::a('Cookie use', ['/site/policy', '#' => 'cookie'], ['target' => '_blank']),
            ]) : '';
        ?>
    </div>
    <div class="col-sm-5">
        <button
            type="button"
            loader-click="buy($event)"
            loader-click-unrestored="true"
            class="btn btn-block btn-primary"><?= _t("front.site", "Next: Payment")?></button>
    </div>
</div>
</div>





<?php $formPickup->end(); ?>