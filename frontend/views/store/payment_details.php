

<div class="content">
    <div class="col-sm-8">
        <h1><?= _t('site.store', 'Shipment details')?></h1>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="full_name"><?= _t('site.store', 'Full name')?></label>
                    <input type="text" class="form-control" id="full_name">
                </div>
            </div> 
        </div>
        <!-- /.row -->

        <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                    <label for="phone"><?= _t('site.store', 'Telephone number')?></label>
                    <input type="text" class="form-control" id="phone">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="street"><?= _t('site.store', 'Street Address')?></label>
                    <input type="text" class="form-control" id="street">
                </div>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label for="country"><?= _t('site.store', 'Country')?></label>
                    <select class="form-control" id="country"></select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label for="state"><?= _t('site.store', 'State')?></label>
                    <select class="form-control" id="state"></select>
                </div>
            </div>
            
            
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label for="city"><?= _t('site.store', 'Address')?></label>
                    <input type="text" class="form-control" id="city">
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group">
                    <label for="zip"><?= _t('site.store', 'ZIP')?></label>
                    <input type="text" class="form-control" id="zip">
                </div>
            </div>

           

        </div>
        <!-- /.row -->
    </div>
    <div class="col-sm-4">
       
    </div>
</div>