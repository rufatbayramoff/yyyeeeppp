<?php

use common\models\UserAddress;
use common\modules\payment\serializers\PayOrderSerializer;
use common\modules\payment\services\PaymentAccountService;
use frontend\models\ps\PsFacade;
use lib\delivery\delivery\DeliveryFacade;
use lib\money\Money;
use yii\widgets\DetailView;

/**
 * @var \common\models\StoreOrder $storeOrder
 * @var \common\models\PaymentInvoice $invoiceBraintree
 * @var \common\models\PaymentInvoice $invoiceTs
 * @var \common\models\PaymentInvoice $invoiceBonus
 * @var \common\models\PaymentInvoice $invoiceBankTransfer
 * @var string $clientToken
 * @var Money $amountNetIncome
 */

Yii::$app->angular
    ->service(['notify', 'user', 'router'])
    ->controller([
        'payment/controllers/payOrderController',
        'payment/models/PayOrder',
        'payment/models/PaymentInvoice',
        'payment/directives/checkoutPaymentMethods'
    ])
    ->controllerParam('PayOrder', PayOrderSerializer::serialize($storeOrder));
?>

<div class="container" ng-controller="PayOrderController">
    <div class="row">
        <div class="col-sm-8 wide-padding--right">
            <h1><?php echo _t('site.store', 'Payment'); ?></h1>
            <span ng-cloak class="ng-cloak-loader wide-padding--right">
                <img src="/static/images/preloader.gif" width="60" height="60">
            </span>
            <span ng-cloak="">
                <?php echo $this->render('@common/modules/payment/views/checkoutCard', [
                    'storeOrder'          => $storeOrder,
                    'invoiceBraintree'    => $invoiceBraintree,
                    'invoiceTs'           => $invoiceTs,
                    'invoiceBonus'        => $invoiceBonus,
                    'invoiceBankTransfer' => $invoiceBankTransfer,
                    'clientToken'         => $clientToken,
                    'widget'              => false,
                    'posUid'              => false,
                    'amountNetIncome'     => $amountNetIncome
                ]); ?>
            </span>
        </div>

        <div class="col-sm-4 sidebar m-t30" ng-cloak>
            <div class="panel panel-default" ng-if="hasBillingDetails()">
                <div class="panel-heading">
                    <h3><?php echo _t('site.store', 'Billing Details'); ?></h3>
                </div>

                <div class="panel-body">
                    <div  ng-repeat="invoiceItem in payOrder.primaryPaymentInvoice.invoiceItems" class="billing-row">
                        <div class="billing-row__block">{{invoiceItem.title}}</div>
                        <div class="billing-row__block"><strong>{{invoiceItem.total_line}}</strong></div>
                    </div>
                </div>

                <div class="panel-body" ng-if="payOrder.primaryPaymentInvoice.accruedBonus && payOrder.primaryPaymentInvoice.accruedBonus.amount">
                    <div class="billing-row">
                        <div class="billing-row__block"><?php echo _t('site.store', 'Accrued bonus') ?></div>
                        <div class="billing-row__block"><strong>+ {{payOrder.primaryPaymentInvoice.accruedBonus}}</strong></div>
                    </div>
                </div>

                <div style="padding: 15px;" ng-if="payOrder.primaryPaymentInvoice.hasPromoCode()">
                    <?php echo _t('site.store', 'Promo codes:') ?>
                    <div class="row m-b10">
                        <div class="col-xs-12">
                            {{payOrder.primaryPaymentInvoice.storeOrderPromocode.promocode}}
                        </div>
                    </div>
                    <div class="delivery-order__delivery-price">
                        <div class="delivery-order__delivery-price__price delivery_price price__badge">
                            <span class="price__badge">{{payOrder.primaryPaymentInvoice.storeOrderPromocode.amountTotal}}</span>
                        </div>
                        <h5 class="delivery-order__delivery-price__title"><?= _t('site.store', 'Total Discount'); ?>:</h5>
                    </div>
                </div>

                <div class="panel-price">
                    <div class="panel-price__body">
                        <div class="panel-price__body__price total_price">
                            <?php echo _t('site.store', 'Total') ?>
                        </div>
                        <div class="pull-right panel-price__body__price ">
                            {{payOrder.primaryPaymentInvoice.amountTotal}}
                        </div>
                    </div>
                </div>
            </div>

            <span ng-if="payOrder.hasDelivery">
                <div class="panel panel-default" ng-if="hasAddress()">
                    <div class="panel-heading">
                        <h3>
                            <span ng-if="payOrder.isPickupDelivery"><?php echo _t('site.store', 'Pickup Address');?></span>
                            <span ng-if="!payOrder.isPickupDelivery"><?php echo _t('site.store', 'Shipping Address');?></span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="detail-view--custom-view">
                            <table class="table table-striped table-bordered detail-view">
                                <tbody>
                                <tr ng-if="!payOrder.isPickupDelivery">
                                    <th>
                                        <span ng-if="payOrder.shipAddress.contactName"><?php echo _t('site.store', 'Name');?></span>
                                        <span ng-if="!payOrder.shipAddress.contactName"><?php echo _t('site.store', 'Company');?></span>
                                    </th>
                                    <td>
                                        {{payOrder.shipAddress.contactName ? payOrder.shipAddress.contactName : payOrder.shipAddress.companyName}}
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo _t('site.store', 'Address'); ?></th>
                                    <td>
                                        <span ng-bind-html="payOrder.shipAddress.address"></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3><?php echo _t('site.store', 'Details'); ?></h3>
                    </div>
                    <div class="panel-body">
                        <div ng-if="payOrder.machine" class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                    <?php echo _t('site.printer', 'Layer Resolution'); ?>:
                                </div>
                                <div class="col-xs-8 col-sm-12 col-lg-8">
                                    <b ng-if="payOrder.machine.layerResolution">{{payOrder.machine.layerResolution}}</b>
                                    <b ng-if="!payOrder.machine.layerResolution"><?php echo _t('site.common', 'Not set'); ?></b>
                                </div>
                            </div>
                        </div>

                        <span ng-if="payOrder.model3d && payOrder.model3d.isOneTextureForKit">
                            <div ng-if="payOrder.model3d.texture.materialTitle" class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                        <?php echo _t('site.delivery', 'Material'); ?>:
                                    </div>
                                    <div class="col-xs-8 col-sm-12 col-lg-8">
                                        <b>{{payOrder.model3d.texture.materialTitle}}</b>
                                    </div>
                                </div>
                            </div>

                            <div ng-if="payOrder.model3d.texture.fillPercent" class="col-xs-12 m-t10">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                        <?php echo _t('site.delivery', 'Infill'); ?>:
                                    </div>
                                    <div class="col-xs-8 col-sm-12 col-lg-8">
                                        <b>{{payOrder.model3d.texture.fillPercent}}</b>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 m-t10">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                        <?php echo _t('site.delivery', 'Color'); ?>:
                                    </div>
                                    <div class="col-xs-8 col-sm-12 col-lg-8">
                                        <b>{{payOrder.model3d.texture.colorTitle}}</b>
                                    </div>
                                </div>
                            </div>
                        </span>

                        <div ng-if="payOrder.model3d && !payOrder.model3d.isOneTextureForKit" class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-4 col-sm-12 col-lg-4 p-r0">
                                    <?php echo _t('site.delivery', 'Material'); ?>:
                                </div>
                                <div class="col-xs-8 col-sm-12 col-lg-8">
                                    <b><?php echo _t('site.delivery', 'Multicolor') ?> </b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </span>
        </div>
    </div>
</div>