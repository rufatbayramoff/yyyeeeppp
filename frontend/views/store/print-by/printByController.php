<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 07.12.16
 * Time: 17:51
 */

use frontend\widgets\PrintByPsPrinterWidget;

$params = [];
if (isset($ps)) {
    $params['ps'] = $ps;
}
if (isset($psPrinter)) {
    $params['psPrinter'] = $psPrinter;
}

echo PrintByPsPrinterWidget::widget($params);