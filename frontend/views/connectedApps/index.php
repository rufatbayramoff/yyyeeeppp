<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.01.18
 * Time: 13:45
 */

/* @var \common\models\ConnectedApp $connectedApp */
/* @var \yii\data\DataProviderInterface $provider */

?>
<div class="apps-page-head">
    <div class="container container--wide">
        <div class="row">
            <div class="col-md-6">
                <h1 class="apps-page-head__title">
                    <?= _t('site.page', 'Free Manufacturing Apps'); ?>
                </h1>
                <p class="apps-page-head__subtitle">
                    <?= _t('site.page', 'We’ve picked the best online apps that are free and easy-to-use for designing, 3D printing, CNC machining and injection molding.'); ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container container--wide">
    <div class="apps-grid">
        <?php
        foreach ($provider->getModels() as $connectedApp) {
            ?>
            <a href="/apps/<?=H($connectedApp->code) ?>" class="apps-grid__item">
                <?php
                if ($connectedApp->logoFile) {
                    ?>
                    <img src="<?= H($connectedApp->logoFile->getFileUrl()) ?>" alt="<?= H($connectedApp->logoFile->getFileName()) ?>" class="apps-grid__pic">
                    <?php
                }
                ?>
                <h3 class="apps-grid__title"><?= H($connectedApp->title) ?><?= $connectedApp->is_beta ? ' <sup>βeta</sup>' : '' ?></h3>
                <p class="apps-grid__text"><?= H($connectedApp->short_descr)?></p>
            </a>
        <?php } ?>
    </div>
</div>