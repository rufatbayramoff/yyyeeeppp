<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 23.01.18
 * Time: 13:45
 */

/** @var $connectedApp \common\models\ConnectedApp */

?>
<div class="container apps-page">
    <div class="row">
        <div class="col-xs-12">
            <ol class="breadcrumb apps-page__breadcrumb">
                <li><a href="/apps"><?= _t('site.page', 'Apps'); ?></a></li>
                <li><a href="/apps/<?= H($connectedApp->code) ?>"><?= H($connectedApp->title) ?></a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3 col-md-2">
            <?php if ($connectedApp->logoFile) { ?>
                <img src="<?= HL($connectedApp->logoFile->getFileUrl()) ?>" alt="<?= H($connectedApp->logoFile->getFileName()) ?>" class="apps-page__logo">
            <?php } ?>

            <a href="<?= H($connectedApp->url) ?>" class="btn btn-primary btn-block m-b30 p-l10 p-r10 hidden-xs" target="_blank"><?= _t('site.page', 'Launch'); ?></a>
        </div>

        <div class="col-sm-9 col-md-9 wide-padding wide-padding--left">

            <h1 class="h2 m-t0 m-b20"><?= H($connectedApp->title) ?></h1>

            <a href="<?= H($connectedApp->url) ?>" class="btn btn-primary btn-block m-b30 p-l10 p-r10 visible-xs" target="_blank"><?= _t('site.page', 'Launch'); ?></a>

            <div class="apps-page__card">

                <ul class="nav nav-tabs nav-tabs--secondary" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#app-info" aria-controls="app-info" role="tab" data-toggle="tab" aria-expanded="true"><?= _t('site.page', 'Info'); ?></a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#app-instuctions" aria-controls="app-instuctions" role="tab" data-toggle="tab" aria-expanded="false"><?= _t(
                                'site.page',
                                'Instructions'
                            ); ?></a>
                    </li>
                </ul>

                <div class="tab-content apps-page__info">
                    <div class="tab-pane active" id="app-info">
                        <?= $connectedApp->info ?>
                    </div>

                    <div class="tab-pane" id="app-instuctions">
                        <?= $connectedApp->instructions ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>