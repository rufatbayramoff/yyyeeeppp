<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 02.07.18
 * Time: 18:00
 */
?>
<?php if ($seo = $this->context->seo) {
    ?>
    <div class="ps-cat-head">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="h2 ps-cat-head__title"><?=H($seo->header) ?></h1>
                    <p class="m-b30"><?= $seo->header_text; ?></p>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>

