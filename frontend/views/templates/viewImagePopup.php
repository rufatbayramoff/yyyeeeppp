<script type="text/ng-template" id="/static/templates/viewImage.html">
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Preview</h4>
                </div>

                <div class="modal-body">
                    <img src="{{src}}" style="width: 100%"></img>
                </div>

                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</script>