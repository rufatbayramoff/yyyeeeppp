<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h2>Products search - <?=$numFound;?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row">
        <div class="col-lg-8 table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'uuid',
            'title',
            'category.title',
             'product_status',
             'updated_at',
             'moderated_at',
             'published_at',
             'is_active',
             'user_id',
             'single_price',

        ],
    ]); ?>
        </div>
        <div class="col-lg-4 p-l20">
                <a href="/test/products" class="btn btn-default">Clear</a>
                <?php $form = ActiveForm::begin([
                    'method' => 'get',
                    'layout' => 'default',
                    'options' => [
                            'onsubmit' => 'return doSubmit(this)'
                    ]
                ]); ?>
                <?= $form->field($model, 'title') ?>
                <?= $form->field($model, 'category_id') ?>

            <?= \common\modules\dynamicField\widgets\DynamicFieldsFilterWidget::widget(
                ['form' => $form, 'model' => $model, 'dynamicFieldFacets'  => $dynamicFieldFacets, 'classModel' => \common\models\Product::class]
            ); ?>
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                </div>

                <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<style>
    .form-group{ margin: 0;}
</style>
<script>
    function doSubmit(form){
        vals = $(form).serializeArray();
        var result1 = result = {};
        _.each(vals, function (a, v) {
                result1[a.name] = a.value;
        })
        _.each(result1, function (a, v) {
            if (a.value != "0" && a.value != "") result[a.name] = a.value;
        })
        var serialize = function (obj) {
            var str = [];
            for (var p in obj)
                if (obj.hasOwnProperty(p)) {
                    if (obj[p])
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
            return str.join("&");
        }
        var url = serialize(result);
        document.location = '/test/products?' + url;
        return false;
    }
</script>