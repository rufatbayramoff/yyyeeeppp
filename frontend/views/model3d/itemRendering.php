<?php
/** @var \yii\web\View $this */
/** @var string $redirectUrl */
/** @var integer $totalFiles */
/** @var bool $isExternalWidget */
/** @var Ps $toPs */

/** @var Ps $id Model id */

use common\models\Ps;
use frontend\assets\PreloadModel3dRendering;
use frontend\widgets\PrintByPsTitleWidget;


$this->registerAssetBundle(PreloadModel3dRendering::class);

Yii::$app->angular
    ->controller('model/preload')
    ->service(['router',  'notify', 'user', 'modelService', 'model3dParseProgress'])
    ->controllerParams(
        [
            'modelId'     => $id,
            'redirectUrl' => $redirectUrl,
            'totalFiles'  => $totalFiles
        ]
    );

?>

<div class="container item-rendering" ng-controller="PreloadModelController">
    <div class="row">
        <div class="col-xs-12">
            <?= PrintByPsTitleWidget::widget(['ps' => $toPs]); ?>

            <div
                    ng-if="isLoading"
                    class="item-rendering__message wave-anim">
                <div class="item-rendering__comment" id="loadComment"><?= _t('site.model3d', 'Analyzing files...') ?></div>
                <div class="item-rendering__line line-anim"></div>
            </div>

            <div
                    ng-if="errorMessage"
                    class="text-center"
                    ng-cloak
            >{{errorMessage}}
            </div>
        </div>
    </div>
</div>
