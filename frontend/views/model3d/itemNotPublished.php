<?php

use common\modules\product\models\ProductSearchForm;
use common\modules\product\repositories\ProductRepository;
use common\modules\product\repositories\ProductRepositoryCondition;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;
use common\services\LikeService;
use yii\widgets\ListView;

$storeModelsCount = 0;

$modelUser = $model3d->user;
if ($modelUser && UserFacade::hasStore($modelUser)) {
    $storeModelsCount = \common\models\Model3d::find()->storePublished($modelUser)->count();
}
$categoryItem = $model3d->productCategory;
$this->title = _t('site.store', 'Model {title} not found', ['title' => H($model3d->title)]);
?>

<div class="container">

    <div class="error-page__pic text-center">
        <h1>
            <?= _t('front.site', 'Model not found') ?>
        </h1>
        <div class="error-page__error-code" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 50px;margin: 0 0 30px; line-height: 60px;">
            ¯\_(ツ)_/¯
        </div>
        <div class="error-page__error-info">
            <?= _t('front.site', 'This model has either been unpublished or deleted by the designer.') ?>
        </div>
        <?php if ($storeModelsCount > 0): ?>
            <p class="m-t20">
                <?= _t('front.site', 'Check out {uname}\'s other models', ['uname' => \frontend\models\user\UserFacade::getFormattedUserName($modelUser)]) ?>
                <br>
                <?= \yii\helpers\Html::a(
                    _t('front.site', '{uname} store', ['uname' => \frontend\models\user\UserFacade::getFormattedUserName($modelUser)]),
                    UserUtils::getUserStoreUrl($modelUser),
                    ['class' => 'btn btn-primary btn-ghost m-t10']
                ); ?>
            </p>
            <?php if (false): ?>
                <h2 class="m-t30 m-b20 text-left">
                    <?= _t('front.site', '{uname}\'s latest models', ['uname' => \frontend\models\user\UserFacade::getFormattedUserName($modelUser)]) ?>
                </h2>
            <?php endif; ?>
        <?php endif; ?>

    </div>
    <?php if ($categoryItem) :
        $searchModel = new ProductSearchForm();
        $searchModel->category = $categoryItem;

        $condition = Yii::createObject(ProductRepositoryCondition::class);
        $condition->populateByForm($searchModel);
        $condition->onlyForPublicCatalog();
        $productRepository = Yii::createObject(ProductRepository::class);
        $dataProvider = $productRepository->getProductsProvider($condition, 5);
        $listView = Yii::createObject(
            [
                'class'        => ListView::class,
                'dataProvider' => $dataProvider,
                'itemView'     => '@frontend/modules/product/views/product/productListElement',
            ]
        );
        ?>
        <div class="container cat-page-models">
            <h2><?= _t('site.store', 'Most Popular 3D Models'); ?></h2>

            <div class="row store-items-list js-store-items">
                <?php echo $listView->renderItems() ?>
            </div>

        </div>
    <?php endif; ?>

</div>