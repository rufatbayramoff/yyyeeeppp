<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.04.17
 * Time: 11:22
 */

use common\components\JsObjectFactory;
use common\models\PrinterColor;
use common\modules\model3dRender\assets\Model3dRenderer2Asset;
use common\services\Model3dPartService;

/** @var \common\models\Model3dPart $model3dPart */


$imgUrl = Model3dPartService::getModel3dPartRenderUrl($model3dPart);
$this->registerAssetBundle(Model3dRenderer2Asset::class);
$color = $model3dPart->isMulticolorFormat() ? PrinterColor::MULTICOLOR : $model3dPart->getCalculatedTexture()->printerColor->getRgbHex();
JsObjectFactory::createJsObject(
    'model3dRenderRouteClass',
    'model3dRenderRoute',
    [
        'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl.'/'
    ],
    $this
);
JsObjectFactory::createJsObject(
    'model3dRenderClass',
    'model3dRenderObj',
    [
        'imgUrl'       => Yii::$app->getModule('model3dRender')->renderer->getRenderImageUrlBase($model3dPart->file),
        'imgElementId' => 'move_image',
        'fileUid'      => $model3dPart->file->md5sum . '_' . Yii::$app->getModule('model3dRender')->renderer->getRenderFileExt($model3dPart->file),
        'color'        => $color,
        'pb'           => $model3dPart->model3d->getCadFlag()
    ],
    $this
);
?>
<div id="model3dImageViewContainer">
    <img id="move_image" class="model3dRenderLiveImg" src="<?= $imgUrl ?>"/>
</div>
