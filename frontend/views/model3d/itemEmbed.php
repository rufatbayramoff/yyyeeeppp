<?php

/** @var bool $showPrice */
/** @var bool $showImage */
/** @var \common\models\Model3d $model3d */
/** @var $model3dItemForm $model3dItemForm */
/** @var bool $buyWidget */

use common\components\JsObjectFactory;
use common\components\order\PriceCalculator;
use common\services\Model3dService;
use frontend\components\UserUtils;
use frontend\models\model3d\Model3dFacade;
use frontend\models\user\UserFacade;
use frontend\widgets\assets\StoreUnitWidgetAssets;
use yii\helpers\Html;
use yii\helpers\Url;

$model3d = $model3dItemForm->model3d;
$title = $model3d->title;
$modelAuthor = $model3d->user;
$mode = 'view';
$this->title = $title;
$cover = \frontend\models\model3d\Model3dFacade::getCover($model3d);
if (!empty($modelColor)) {
    $cover = \frontend\models\model3d\Model3dFacade::getCover($model3d);
}

$renders = Model3dFacade::getRenderedImages($model3d->getActiveModel3dParts());

$embiedCssUrl = '/css/embed.css?v=' . @filemtime(Yii::getAlias('@webroot/css/embed.css'));

JsObjectFactory::createJsObject(
    'model3dRenderRouteClass',
    'model3dRenderRoute',
    [
        'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl . '/'
    ],
    $this
);

StoreUnitWidgetAssets::register($this);
JsObjectFactory::createJsObject(
    'storeUnitElementClass',
    'storeUnitElementObj_' . $model3d->id,
    [
        'uid' => $model3d->id
    ],
    $this
);


?>
<link rel="canonical" href="https://www.treatstock.com/3dmodels/<?= $model3d->id; ?>">
<link href="<?= $embiedCssUrl ?>" rel="stylesheet">

<div class="ts-e-model-slider">
    <style>
        .fotorama1458312259024 .fotorama__nav--thumbs .fotorama__nav__frame {
            padding: 2px;
            height: 64px }

        .fotorama1458312259024 .fotorama__thumb-border {
            height: 60px;
            border-width: 2px;
            margin-top: 2px }
    </style>
    <div class="fotorama--hidden"></div>
    <?php if ($showImage): ?>
        <?php
        echo \frontend\widgets\ModelFotoramaWidget::widget(
            [
                'id'                      => 'fotoramaWidgetDiv',
                'model'                   => $model3d,
                'currentSelectedFileInfo' => null,
                'disableFullImageSize'    => true,
                'disable3dView'           => true
            ]
        ); ?>
    <?php else : // no image?>

        <div class="no-img-place"></div>

    <?php endif; ?>
</div>

<div class="ts-e-model-footer">
    <div class="ts-e-model-info">
        <a href="https://www.treatstock.com/3dmodels/<?= $model3d->id; ?>" target="_blank" class="ts-e-model-title" title="<?= \H($this->title); ?>">
            <?= \H($this->title); ?>
        </a>

        <div class="ts-e-model-info__author hidden">
            <div>by</div>

            <div class="ts-e-model-info__username">
                <?php echo Html::a(H(UserFacade::getSalesTitle($modelAuthor)), HL(Url::toRoute(UserUtils::getUserPublicProfileRoute($modelAuthor))), ['target' => '_blank']);?>
            </div>
        </div>
    </div>

    <div class="ts-e-model-price">
        <?php if ($showPrice): ?>
            <div class="ts-e-model-price__value js-catalog-price catalog-price-store-unit-<?= $model3d->storeUnit->id ?>" data-store-unit-id="<?=$model3d->storeUnit->id?>">
            </div>
        <?php else: // no price: ?>

        <?php endif; ?>

        <?php if($buyWidget):?>
            <a class="ts-e-model-print-btn" href="<?php echo Model3dFacade::getPrintUrl($model3d); ?>&widget=widget" title="Make it" data-categoryga="Filepage"
               data-actionga="Print"><?= _t('store.item', 'Buy') ?>
            </a>
        <?php else: ?>
            <a class="ts-e-model-print-btn" href="<?php echo Model3dFacade::getPrintUrl($model3d); ?>" target="_blank" title="Make it" data-categoryga="Filepage"
               data-actionga="Print"><?= _t('store.item', 'Buy') ?>
            </a>
        <?php endif; ?>
    </div>
</div>
