<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 22.08.16
 * Time: 15:26
 */


use common\components\order\TestOrderFactory;
use common\models\Model3dReplica;
use common\services\Model3dService;
use frontend\components\UserUtils;
use frontend\models\model3d\Model3dFacade;
use frontend\models\model3d\Model3dItemForm;
use frontend\models\user\UserFacade;
use frontend\widgets\Model3dInfoJs;
use frontend\widgets\ModelFotoramaWidget;
use yii\bootstrap\Html;


/** @var  Model3dItemForm $model3dItemForm */
/** @var  Model3dReplica $model3dReplica */

/** @var \common\models\Model3d|Model3dReplica $model3d */
$model3d = $model3dReplica = $model3dItemForm->model3d;
$modelAuthor = $model3d->originalModel3d->user;
$authorUserName = $modelAuthor ? H(UserFacade::getFormattedUserName($modelAuthor)) : 'Unknown';

$this->title = $model3d->title;

$files = Model3dService::getModel3dActivePartsAndImagesList($model3d);
$selectedFotoramaFileId = $model3dItemForm->selectedFotoramaFileId;
if (!array_key_exists($model3dItemForm->selectedFotoramaFileId, $files)) {
    $selectedFotoramaFileId = $model3d->cover_file_id;
    if (!array_key_exists($selectedFotoramaFileId, $files)) {
        $selectedFotoramaFileId = key($files);
    }
}
$currentSelectedFileInfo = $files[$selectedFotoramaFileId];

Model3dInfoJs::widget(['model3d' => $model3d]);
\common\components\JsObjectFactory::createJsObject(
    'model3dRenderRouteClass',
    'model3dRenderRoute',
    [
        'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl.'/'
    ],
    $this
);
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 m-t30">
            <h1 class="model-title">
                <?= H($this->title); ?>
            </h1>

            <div class="model-info">
                <div class="model-date">
                   <span>
                      <?php
                      echo _t('front.model3d', 'Published at: ');
                      echo app('formatter')->asDate($model3d->published_at, 'long');
                      ?>
                   </span>
                </div>
                <?php if($modelAuthor): ?>
                <div class="model-author">
                    <div>by</div>
                    <div class="model-author__userpic">
                        <?php
                        $avatar_url = HL($modelAuthor->userProfile->avatar_url);
                        $ava = UserUtils::getAvatar($avatar_url, $modelAuthor->email, 'model3d');
                        echo UserFacade::getUserLink($modelAuthor, $ava);
                        ?>
                    </div>
                    <div class="model-author__username">
                        <?= UserFacade::getUserLink($modelAuthor, H($authorUserName));?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row user-my-modelView">
        <div class="col-sm-8 wide-padding wide-padding--right">
            <div class='model-area'>

                <div class="model-slider">
                    <?php
                    echo ModelFotoramaWidget::widget(
                        [
                            'id'                      => 'fotoramaWidgetDiv',
                            'model'                   => $model3d,
                            'currentSelectedFileInfo' => $currentSelectedFileInfo
                        ]
                    ); ?>
                </div>

            </div>
        </div>
        <div class="col-sm-4 sidebar sidebar--breadcrumbs model-page__sidebar">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="model-page__sidebar-title">
                        <?= _t('store.model', 'Description'); ?>
                    </div>
                    <div class="model-description">
                        <?= frontend\components\StoreUnitUtils::displayMore($model3d->description); ?>
                    </div>
                    <?php
                    if ($model3dReplica->getStoreOrder()) { ?>
                        <div class="panel-body">
                            <div>
                                <?= _t('front.model3d', 'Order: ') . $model3dReplica->getStoreOrder()->id ?>
                            </div>
                            <div>
                                <?= _t('front.model3d', 'Quantity: ') . $model3dReplica->getStoreOrder()->storeOrderItems[0]->qty; ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-warning" role="alert">

                        <?php echo _t('store.item', 'This is a snapshot of the 3D Model taken when the order was placed. '); ?>

                    </div>

                    <?php if ($model3dReplica->isPublished() && $model3dReplica->store_unit_id != TestOrderFactory::getTestOrderStoreUnitId()) {
                        echo Html::a(
                            _t('store.item', 'View current product'),
                            Model3dFacade::getStoreUrl($model3dReplica->originalModel3d),
                            ['class' => 'btn btn-success btn-sm']
                        );
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>
