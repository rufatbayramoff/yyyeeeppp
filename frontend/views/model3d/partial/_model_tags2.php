<?php ;
use common\models\SiteTag;
use frontend\models\model3d\Model3dEditForm;


/* @var Model3dEditForm $model3dEditForm */
/* @var SiteTag[] $tags */

if ($mode == 'view'): ?>
    <?php if (!empty($tags)): ?>
        <div class="model-tags">
            <?php
            $items = [];
            foreach ($tags as $tag) {
                $tagName = '#'.\H($tag->text);
                $items[] = \yii\helpers\Html::a(
                    $tagName,
                    \frontend\modules\mybusiness\modules\product\helper\ProductUrlHelper::productsCatalogTag($tag),
                    ['class' => 'label label-default', 'data-pjax' => 0]
                );
            }
            echo implode(" ", $items);
            unset($items, $tag);
            ?>
        </div>
    <?php endif; ?>

    <?php
endif;

