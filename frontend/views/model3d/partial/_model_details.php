<?php
/* @var common\models\Model3d $model3d */
use common\interfaces\Model3dBasePartInterface;

/* @var mixed[] $files Array of arrays represent Model3dPart */

$colors = app('setting')->get('model3d.rendercolors'); 

?>
<div class="panel panel-default"> 
    <div class="panel-heading"><?php echo _t('front.site', '3D Model Details'); ?> </div>
    <div class="panel-body">
        <div class="detail-view--custom-view">
            <?php
            $propertyObject = new stdClass();
            $propertyObject->id = $model3d->id;
            $propertyObject->title = $model3d->title;
            $propertyObject->files_count = count($model3d->getActiveModel3dParts());
            $propertyObject->created_at = $model3d->created_at;
            $propertyObject->faces = 0;
            $propertyObject->vertices = 0;
            $propertyObject->size = 0;
            $propertyObject->format = 0;

            $maxFileSize = 0;
            foreach($files as $modelFile){
                if($modelFile->file->size > $maxFileSize){
                    $maxFileSize = $modelFile->file->size;
                    $propertyObject->format = $modelFile->file->extension;
                    $propertyObject->size = (new \yii\i18n\Formatter(['sizeFormatBase'=>1000]))->asShortSize($modelFile->file->size, 2);

                    if($modelFile instanceof Model3dBasePartInterface){
                        if(!empty($modelFile->model3dPartProperties)) {
                            if (!empty($modelFile->model3dPartProperties->faces)) {
                                $propertyObject->faces = $modelFile->model3dPartProperties->faces;
                            }
                            if (!empty($modelFile->model3dPartProperties->vertices)) {
                                $propertyObject->vertices = $modelFile->model3dPartProperties->vertices;
                            }
                        }
                    }
                }
            }
            $showAttributes  = [
                [
                    'label' => _t('site.model3d', 'Files count'),
                    'value' => $propertyObject->files_count,
                ],
                [
                    'label' => _t('site.model3d', 'Created at'),
                    'format' => 'datetime',
                    'value' => $propertyObject->created_at,
                ],
                [
                    'label' => _t('site.model3d', 'Size'),
                    'value' => $propertyObject->size,
                ],
                [
                    'label' => _t('site.model3d', 'Format'),
                    'value' => $propertyObject->format,
                ],

                [
                    'label' => _t('site.model3d', 'Faces'),
                    'value' => $propertyObject->faces,
                ],
            ];
            if(isset($propertyObject->faces)){
              #  $showAttributes = array_merge($showAttributes, ['faces', 'vertices']); //, 'dimension'
            }

            echo \yii\widgets\DetailView::widget([
                'model' => $propertyObject,
                'attributes' => $showAttributes,
            ]);
            ?>

        </div>
    </div>

</div>
    <?php if(isset($isOwner)): ?>
    <p style="background:#efefef; padding: 2px;">
        <?=_t('front.model3d', 'Color')?>:
        <?php foreach ($colors as $colorRow):
                $mColor = $colorRow['code'];
                $mTitle = $colorRow['title'];
                $mColorCode = str_replace("#", "", $mColor);
                $url = \frontend\models\model3d\Model3dFacade::getStoreUrl($model3dObj, ['color' => $mColorCode]);

            ?>
            <a href="<?php echo $url; ?>" title="<?php echo $mTitle; ?>" class="btn btn-sm " style="padding: 8px;background: <?php echo $mColor; ?>">  </a>
        <?php endforeach; ?>
       </p>
    <?php endif; ?>