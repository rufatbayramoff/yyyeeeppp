<?php

use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\ApiExternalSystem;
use common\models\Model3d;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\StoreUnit;
use frontend\components\StoreUnitUtils;
use frontend\models\model3d\Model3dFacade;
use frontend\models\model3d\Model3dItemForm;
use frontend\models\user\UserFacade;
use frontend\widgets\AddToCollectionWidget;
use frontend\widgets\CalculateCncButton;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var  \yii\web\View $this */
/** @var Model3d|\common\interfaces\Model3dBaseInterface $model3d */
$sourceDetails = $model3d instanceof Model3d ? $model3d->source_details : [];
?>
<div>
    <?php if ($model3d instanceof Model3d && $model3d->thingiverseThing): ?>
        <div class="model-page__sidebar-title m-r10" style="display: inline;">
            <?= _t('site.model', 'Model from'); ?>
        </div>
        <a href="https://www.thingiverse.com/">
            <img src="https://www.thingiverse.com/img/thingiverse-logo-2015.png" style="width: 128px;"/>
        </a>
        <div class="m-t10 m-b20" style="color: #707478;">
            Original model: <a target="_blank" href="https://www.thingiverse.com/thing:<?= $model3d->thingiverseThing->thing_id; ?>"><?= \H($model3d->title); ?></a>
            <br/>
            Licensed under the <a rel="license" target="_blank" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons - Attribution</a> license.
        </div>

    <?php endif; ?>
    <?php if (!empty($sourceDetails['vendor']) && $sourceDetails['vendor'] == ApiExternalSystem::SYSTEM_TINKERCAD): ?>
        <div class="model-page__sidebar-title">
            <?= _t('site.model', 'Made with'); ?>
        </div>
        <img src="https://tinkercad.com/resources/logo-tinkercad-128.png" width="32" height="32" alt="Tinkercad, the free, online 3D design and 3D printing app."
             style="width: 32px;height: 32px;margin-right: 20px;">
        <?php if (!empty($sourceDetails['link'])): ?>
            <a href="<?= $sourceDetails['link']; ?>" class="btn btn-default btn-sm"><?= _t('site.model', 'Edit it online'); ?></a>
        <?php endif; ?>
        <br/>
        <hr/>
    <?php endif; ?>

    <div class="model-price">
        <?php if ($model3d->getAuthor() && $model3d->getAuthor()->id == UserFacade::getCurrentUserId() && $model3d->price_per_produce > 0): ?>
            <div class='model-price__order-hint'>
                <?= _t('store.model',
                    'As the author of this model, it is free for you, so you will only see a manufacturing fee. Everyone else will see both the model fee and manufacturing fee.'); ?>
            </div>
        <?php endif; ?>
    </div>

    <hr class="m-t10"/>

    <?php if ($model3d->canShare()) : ?>
        <model-social-buttons model="model"></model-social-buttons>
    <?php endif; ?>

    <?php if (!is_guest() && $model3d->canAddToCollection()) : ?>
        <div class="panel-body model-action-btns">
            <div class="model-action__collection">
                <?=
                AddToCollectionWidget::widget([
                    'model3dId'  => $model3d->getSourceModel3d()->id,
                    'jsCallback' => new JsExpression('function(res){ new TS.Notify({ type : "success", text: res.message});}')
                ]);
                ?>
            </div>
        </div>

    <?php endif; ?>
</div>