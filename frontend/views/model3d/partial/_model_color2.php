<div class="m-b20 hide-in-widget">
    <a href="#printers-list" class="btn btn-danger ts-ga" data-categoryga="modelpage" data-actionga="click" data-labelga="OrderNow">
        <span class="tsi tsi-shopping-cart m-r10"></span>
        Buy Now</a>
</div>
<div class="color-selector-block">

    <div class="model-page__sidebar-title hide-in-widget">
        <?= _t('site.model3d', "Product's Application") ?>
    </div>
    <select2 items="usageTypes" class="hide-in-widget" model="selectedUsageTypeId" change="changeUsageType">
        <select2-item-template>
            <div>
                <span ng-if="item.image" class="color-selector-material-image-block"><img ng-src="{{item.image}}"></span>
                <span class="color-selector-material-title">{{item.title}}</span>
                <span class="color-selector-material-short-description">{{item.description}}</span>
            </div>
        </select2-item-template>

        <select2-result-template>
            <div class="color-selector-material-selected-block">
                <span ng-if="item.image" class="color-selector-material-image-block"><img ng-src="{{item.image}}"></span>

                <div class="color-selector-material-text-block">
                    <span class="color-selector-material-title">{{item.title}}</span>
                    <span class="color-selector-material-short-description">{{item.description}}</span>

                </div>

            </div>
        </select2-result-template>
    </select2>

    <div class="model-page__sidebar-title">
        <?= _t('site.model3d', 'Material & color') ?>
    </div>

    <div ng-if="model.isKit()" class="color-selector-is-one-material-for-kit-group">
        <div class="checkbox">
            <input ng-model="model3dTextureState.isOneTextureForKit" class="color-selector-is-one-material-for-kit" type="checkbox" id="isOneTextureForKitCheckbox">
            <label for="isOneTextureForKitCheckbox"><?= _t('site.colorSelector', 'Set one material and color') ?></label>
        </div>
    </div>
    <div ng-if="!(allowedMaterials.list.length && (model3dTextureState.isOneTextureForKit || selectedModel3dPartId)) && offersBundle.totalCount">
        <?=_t('site.printModel3d', 'Select a file to change its material or color'); ?>
    </div>
    <div ng-if="allowedMaterials.list.length && (model3dTextureState.isOneTextureForKit || selectedModel3dPartId)">
        <select2 items="allowedMaterials.list" model="selectedMaterialGroupId" change="changeMaterialGroup">
            <select2-item-template>
                <div>
                    <span class="color-selector-material-image-block"><img ng-src="{{item.materialGroup.photoUrl}}"></span>
                    <span class="color-selector-material-title">{{item.materialGroup.title}}</span>
                    <span class="color-selector-material-short-description">{{item.materialGroup.shortDescription}}</span>
                </div>
            </select2-item-template>

            <select2-result-template>
                <div class="color-selector-material-selected-block">
                    <span class="color-selector-material-image-block"><img ng-src="{{item.materialGroup.photoUrl}}"/></span>
                    <div class="color-selector-material-text-block">
                        <span class="color-selector-material-title">{{item.materialGroup.title}}</span>
                        <span ng-if="!item.materialGroup.longDescription">{{item.materialGroup.shortDescription}}</span>
                        <show-more-text
                                ng-if="item.materialGroup.longDescription"
                                short-text="item.materialGroup.shortDescription"
                                long-text="item.materialGroup.longDescription"
                        ></show-more-text>
                    </div>
                </div>
            </select2-result-template>
        </select2>

        <div class="color-selector-printer-color-block color-switcher__material-list color-selector-printer-color-block-active">
            <input type="checkbox" class="showhideColorSwitcher" id="color-switcher__aff-widget-switcher">
            <label class="color-switcher__mobile" for="color-switcher__aff-widget-switcher">
                <div title="Biodegradable and flexible plastic" class="material-item">
                    <div class="material-item__color" style="background-color: #{{getColorById(selectedMaterialColorId).rgbHex}}"></div>
                    <div class="material-item__label">{{getColorById(selectedMaterialColorId).title}}</div>
                </div>
                <span class="tsi tsi-down"></span>
            </label>

            <div class="color-switcher__color-list">
                <span ng-repeat="printerColor in allowedMaterials.getGroupMaterialsItemByGroupId(selectedMaterialGroupId).colorsLongList">
                    <div ng-click="changeColor(printerColor.id)" class="material-item"
                         ng-class="{'material-item--active':printerColor.id == selectedMaterialColorId}"
                         title="{{printerColor.title}}">
                        <div class="material-item__color" ng-style="{'background-color': '#{{printerColor.rgbHex}}'}"></div>
                        <div class="material-item__label">{{printerColor.title}}</div>
                    </div>
                </span>
            </div>
        </div>
    </div>
</div>
