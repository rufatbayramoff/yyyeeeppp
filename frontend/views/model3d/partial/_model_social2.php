
<script type="text/ng-template" id="/js/ts/app/store/filepage/filepage-social.html">

    <div class="model-share">

        <div class="model-share__list js-social-likes">

            <div class="model-share__btn">
                <a href="/site/sendemail"
                   oncontextmenu="return false"
                   title="<?= _t("site.email", "Send email"); ?>"
                   class="ts-ajax-modal">
                    <span class="social-likes__icon social-likes__icon_mail"></span>
                </a>
            </div>

            <div class="model-share__btn shareembed"
                 data-placement="bottom"
                 data-container="body"
                 data-original-title="<?= _t("site.share", "HTML Code"); ?>">
                            <span class="social-likes__icon social-likes__icon_code"
                                  title="<?= _t("site.email", "HTML Code"); ?>"></span>
            </div>

            <div class="model-share__btn sharelink"
                 data-placement="bottom"
                 data-container="body"
                 data-original-title="<?= _t("site.share", "Share this link"); ?>">
                            <span class="social-likes__icon social-likes__icon_link"
                                  title="<?= _t("site.email", "Share this link"); ?>"></span>
            </div>


            <div
                ng-click="openFacebookModal()"
                class="model-share__btn"
                title="<?= _t("site.email", "Facebook Messenger"); ?>">
                <span class="social-likes__icon social-likes__icon_facebook-m"></span><?= _t('site.email', 'Facebook Send')?>
            </div>

            <div class="model-share__btn facebook"
                 title="<?= _t("site.email", "Share this link"); ?>"
                 data-display="popup">
                Facebook
            </div>

            <div class="model-share__btn twitter"
                 title="<?= _t("site.email", "Tweet this link"); ?>"
                 data-via="treatstock">
                Twitter
            </div>

            <div class="model-share__btn pinterest"
                 title="<?= _t("site.email", "Pin this model"); ?>"
                 data-media="{{model.coverImgUrl}}">
                Pinterest
            </div>
        </div>

    </div>

</script>