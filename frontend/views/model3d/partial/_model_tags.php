<?php
use common\components\ArrayHelper;
use common\models\SiteTag;
use frontend\models\model3d\Model3dEditForm;
use kartik\select2\Select2;


/* @var Model3dEditForm $model3dEditForm */
/* @var SiteTag[] $tags */

if ($mode == 'view'): ?>
    <?php if (!empty($tags)): ?>
        <div class="model-tags">
            <?php
            $items = [];
            foreach ($tags as $tag) {
                $tagName = \H($tag->text);
                $items[] = \yii\helpers\Html::a(
                    $tagName,
                    str_replace('??', '?', \yii\helpers\Url::toRoute(['/product/product/index/', 'tags' => [$tagName]])),
                    ['class' => 'label label-default', 'data-pjax' => 0]
                );
            }
            echo implode(" ", $items);
            unset($items, $tag);
            ?>
        </div>
    <?php endif; ?>

    <?php
elseif ($mode == 'edit'):
    $selectTags = [];
    foreach ($tags as $tag) {
        $selectTags[$tag->text] = $tag->text;
    }
    $model3dEditForm->tags = $selectTags;
    echo $form->field($model3dEditForm, 'tags')->widget(
        Select2::class,
        common\components\SimpleSelect2::getAjax(
            ['model3d/tags'],
            'id,text',
            $model3dEditForm->tags,
            [
                'options'       => ['placeholder' => _t('front.model3d', 'Add tags') . '... ', 'multiple' => true],
                'pluginOptions' => [
                    'tags'               => true,
                    //'allowClear' => true,
                    'minimumInputLength' => 2,
                    'maximumInputLength' => 45
                ]
            ]
        )
    )->label(false);
endif;

