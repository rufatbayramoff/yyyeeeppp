<?php
/** @var \common\models\Model3d $model3d */

use common\modules\product\interfaces\ProductInterface;
use frontend\models\user\UserFacade;

/** @var \yii\web\View $this */
$this->registerAssetBundle(\frontend\assets\StoreAsset::class);
?>

<?php if (!$model3d->isActive()): ?>
    <!-- edit buttons -->
    <div class="row model-status-btns">
    </div>
    <?php if ($model3d->product_status ==  ProductInterface::STATUS_REJECTED): ?>
        <div class="model-status bg-danger">
            <h4><?= _t('front.publish', 'Publish is rejected.'); ?></h4>
            <p><?= str_replace("\n", "</p><p>", $model3d->getRejectMessage()); ?></p>
        </div>
    <?php elseif ($model3d->isPublished()): ?>
        <div class="model-status bg-success">
            <?php echo _t('front.publish', 'Your model is available in store.'); ?>
        </div>
    <?php elseif ($model3d->isPublished()) : ?>
        <div class="model-status bg-info">
            <?php echo _t(
                'front.publish',
                'Your model is published and will be available in store after moderation. Thank you!'
            ); ?>
        </div>
    <?php endif; ?>
    <!-- publish status end -->
<?php else: ?>
    <div class="row model-status-btns">
        <?php
        if (false) {
            echo \yii\helpers\Html::button(
                _t("front.user", "Publish Model"),
                [
                    'class'       => 'btn btn-primary btn-block ts-ajax-modal model-status-btns__action model-status-btns__action--publish-btn',
                    'title'       => _t("front.user", "Publish"),
                    'value'       => yii\helpers\Url::toRoute(['my/store/publish', 'id' => $model3d->id]),
                    'data-target' => '#modal-publishModel',
                ]
            );
        }
        ?>
    </div>
<?php endif; ?>