<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.04.17
 * Time: 11:22
 */

use common\components\JsObjectFactory;
use common\models\PrinterColor;
use common\modules\model3dRender\assets\Model3dRenderer2Asset;
use common\services\Model3dPartService;

/** @var \common\models\File $file */


$this->registerAssetBundle(Model3dRenderer2Asset::class);
$renderParams          = new \common\modules\model3dRender\models\RenderFileParams();
$renderParams->color   = 'FFFFFF';
$renderParams->ambient = \common\models\PrinterColor::WHITE_COLOR_AMBIENT;
$color                 = 'FFFFFF';
$imgUrl                = Yii::$app->getModule('model3dRender')->renderer->getRenderImageUrl($file, $renderParams);
JsObjectFactory::createJsObject(
    'model3dRenderRouteClass',
    'model3dRenderRoute',
    [
        'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl . '/'
    ],
    $this
);
JsObjectFactory::createJsObject(
    'model3dRenderClass',
    'model3dRenderObj',
    [
        'imgUrl'       => Yii::$app->getModule('model3dRender')->renderer->getRenderImageUrlBase($file),
        'imgElementId' => 'move_image',
        'fileUid'      => $file->md5sum . '_' . Yii::$app->getModule('model3dRender')->renderer->getRenderFileExt($file),
        'color'        => $color,
        'pb'           => 0
    ],
    $this
);
$this->title = _t('site.ps', '3D Model Preview');
?>
<div id="model3dImageViewContainer">
    <img id="move_image" class="model3dRenderLiveImg" src="<?= $imgUrl ?>"/>
</div>
