<?php

use common\components\ArrayHelper;
use common\components\JsObjectFactory;
use common\models\Model3d;
use common\models\SeoPage;
use frontend\components\image\ImageHtmlHelper;
use frontend\components\StoreUnitUtils;
use frontend\components\UserUtils;
use frontend\controllers\catalog\actions\serializers\Model3dSerializer;
use frontend\controllers\catalog\actions\serializers\Model3dTextureStateSerializer;
use frontend\models\model3d\Model3dFacade;
use frontend\models\user\UserFacade;
use frontend\modules\product\widgets\ProductSchemaWidget;
use kartik\select2\Select2Asset;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var  \yii\web\View $this */
/** @var  \common\models\PrinterMaterial $modelMaterial */
/** @var  \common\models\base\PrinterColor $modelColor */
/** @var  \common\models\Model3dViewedState $model3dViewedState */
/** @var  \frontend\models\model3d\Model3dItemForm $model3dItemForm */
/** @var  int $modelWeight */
/** @var  string $userCurrency */
/** @var  array $breadcrumbs */
/** @var  \common\models\PsPrinter $manualSettedPsPrinter */
/** @var  \common\models\Ps $manualSettedPs */
/** @var  SeoPage|null $seo */
/** @var array $groupMaterials */
/** @var array $objectStorage */

/** @var Model3d|\common\models\Model3dReplica $model3d */
$model3d = $model3dItemForm->model3d;

$storeUnit = $model3d->storeUnit;
$cover     = Model3dFacade::getCover($model3d);

$userCurrency = UserFacade::getCurrentCurrency();
$this->title  = $model3d->title . _t('front.model3d', ' - 3D Printable Model on Treatstock');
$mode         = 'view';
$modelAuthor  = $model3d->user;

/* @var yii\web\View $this */
$ogTags = [
    'og:url'         => param('server') . app('request')->url,
    'og:title'       => H($this->title),
    'og:description' => H($model3d->description),
    'og:image'       => $cover['image'],
    'og:type'        => 'website'
];

if (!empty($model3d->description)) {

    if (!$seo || !$seo->meta_description) {
        $description = !empty($model3d->description) ? \H(strip_tags($model3d->description)) : \H($model3d->title);
        if (mb_strlen($description) < 31) {
            $description .= _t('front.model3d', ' - Get this model 3D printed with print services on Treatstock');
        }
        $this->params['meta_description'] = $description;
    }
} else {
    $description                      = $model3d->title;
    $description                      .= _t('front.model3d', ' - Get this model 3D printed with print services on Treatstock');
    $this->params['meta_description'] = $description;
}

if ($seo && $seo->header) {
    $modelTitle = H($seo->header);
} else {
    $modelTitle = H($model3d->title);
}

$tags = $model3d->tags;

if (!empty($tags)) {
    $tagsArray                     = ArrayHelper::map($tags, 'id', 'text');
    $this->params['meta_keywords'] = \H(implode(', ', $tagsArray));
}

$this->registerLinkTag(
    [
        'rel'  => 'image_src',
        'href' => $cover['image']
    ],
    true
);
$this->params['ogtags'] = $ogTags;

if ($modelAuthor) {
    $authorUserName  = H($modelAuthor->company ? $modelAuthor->company->title : UserFacade::getFormattedUserName($modelAuthor));
    $linkAuthorStore = UserUtils::getUserStoreUrl($modelAuthor);
} else {
    $authorUserName  = null;
    $linkAuthorStore = null;
}

Yii::$app->angular
    ->controller(
        [
            'store/filepage/filepage2',
            'store/filepage/filepage-fotorama',
            'store/filepage/model-render',
            'store/filepage/filepage-scale',
            'store/filepage/printer-directives',
            'store/filepage/filepage-social',
            'store/filepage/models',
            'store/common-models',
            'store/filepage/printer-models',
            'store/filepage/color-service',
        ]
    )
    ->service(
        [
            'modelService',
            'notify',
            'router',
            'user',
            'measure',
            'mathUtils',
            'modal'
        ]
    );


Select2Asset::register($this);
JsObjectFactory::createJsObject(
    'model3dRenderRouteClass',
    'model3dRenderRoute',
    [
        'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl . '/'
    ],
    $this
);


$s                 = Model3dTextureStateSerializer::serialize($model3d);
$model3dSerialized = Model3dSerializer::serialize($model3d);

Yii::$app->angular->controllerParams(
    [
        'model'               => $model3dSerialized,
        'model3dTextureState' => $s,
        'objectStorage'       => $objectStorage,
    ]
);

Yii::$app->angular->constant('objectStorageData', $objectStorage);

Yii::$app->angular->constant(
    'globalConfig',
    [
        'renderUrl'       => reset(Yii::$app->getModule('model3dRender')->renderServerAddress)['renderImageParams']->imgUrl,
        'staticUrl'       => param('staticUrl'),
        'facebook_app_id' => param('facebook_app_id')
    ]
);

echo ProductSchemaWidget::widget([
    'product' => $model3d,
    'price'   => $model3d->getPriceMoneyByQty(1)
]);

$categories      = [];
$currentCategory = $model3d->productCommon->category;
if ($currentCategory) {
    do {
        $categories[]    = $currentCategory;
        $currentCategory = $currentCategory->parent;
    } while ($currentCategory);
    $categories = array_reverse($categories);
}

$breadcrumps                               = [];
$breadcrumps[_t('site.store', 'Products')] = '/products';
foreach ($categories as $category) {
    if (empty($category->parent_id)) {
        continue;
    }
    $code                          = rtrim($category->code, '-');
    $url                           = '/products/' . H($code);
    $breadcrumps[$category->title] = $url;
}
$breadcrumps[$this->title] = \Yii::$app->request->url;


?>

<div ng-controller="FilePageController">
    <div class="store-filter__container m-b30">
        <div class="container container--wide">

            <div class="row">
                <div class="col-xs-12">

                    <ol class="breadcrumb m-t10 m-b0">
                        <?php
                        foreach ($breadcrumps as $breadcrumpText => $breadcrumpUrl) {
                            echo '<li>';
                            echo $breadcrumpUrl ? '<a href="' . HL($breadcrumpUrl) . '">' : '';
                            echo H($breadcrumpText);
                            echo $breadcrumpUrl ? '</a>' : '';
                            echo '</li>';
                        }
                        ?>
                    </ol>
                    <?php
                    echo \frontend\widgets\BreadcrumbsSchemaWidget::widget([
                        'breadcrumpsItems' => $breadcrumps
                    ]);
                    ?>

                    <h1 class="model-title">
                        <?php echo $modelTitle; ?>

                        <?php if (UserFacade::isObjectOwner($model3d) and $model3d instanceof Model3d and !$model3d->originalModel3d) {
                            echo Html::a(
                                '<span class="tsi tsi-pencil"></span>',
                                ['/my/model/edit/' . $model3d->id],
                                [
                                    'class'     => 'btn btn-default btn-sm btn-circle',
                                    'data-pjax' => 'false',
                                    'title'     => _t('front.model', 'Edit details'),
                                    'ng-click'  => 'goToEditModel($event)'
                                ]
                            );
                        }
                        ?>
                    </h1>
                    <div class="model-info">
                        <?php
                        if ($modelAuthor) {
                            ?>
                            <div class="model-author">
                                <?php if ($model3d->isPublished()): ?>
                                    <div><?= _t('site.store', 'Model by'); ?></div>
                                    <div class="model-author__userpic">
                                        <?php
                                        $ava = $modelAuthor->company ?
                                            Html::img(HL($modelAuthor->company->getCompanyLogoOrDefault(false, 20))) :
                                            UserUtils::getAvatar(HL($modelAuthor->userProfile->avatar_url), HL($modelAuthor->email), 'model3d');
                                        ?>
                                        <?= Html::a($ava, $linkAuthorStore); ?>
                                    </div>
                                    <div class="model-author__username">
                                        <?= Html::a($authorUserName, H($linkAuthorStore)); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="container">

        <div class="ng-cloak ng-cloak-loader">
            <img src="/static/images/preloader.gif" width="60" height="60" alt="loading...">
        </div>

        <div class="ng-cloak">

            <div class="row user-my-modelView">
                <div class="col-sm-8 wide-padding wide-padding--right">
                    <div class='model-area'>

                        <div class="model-slider">
                            <div filepage-fotorama="model" model-state="model3dTextureState" order-form="orderForm"></div>
                        </div>

                    </div>

                    <?php echo $this->render('partial/_model_tags2', ['tags' => $tags, 'mode' => $mode]); ?>

                </div>
                <div class="col-sm-4 sidebar sidebar--breadcrumbs model-page__sidebar">

                    <div class="m-b20 hide-in-widget">
                        <a href="/my/print-model3d?model3d=<?= $model3d->uid ?>" class="btn btn-danger ts-ga">
                            <span class="tsi tsi-shopping-cart m-r10"></span>
                            <?= _t('front.store', ' Order now '); ?>
                            <span class="tsi tsi-right m-l20" style="margin-right: -10px;"></span>
                        </a>
                    </div>
                    <div>
                        <?= $this->render(
                            'partial/_model_store2',
                            compact(
                                'model3d'
                            )
                        ); ?>
                    </div>
                    <?php
                    if (UserFacade::isObjectOwner($model3d)) {
                        echo '<div>';
                        echo $this->render('partial/_model_owner', ['model3d' => $model3d]);
                        echo '</div>';
                    }
                    ?>
                    <?php if ($seo && $seo->header_text) {
                        echo $seo->header_text;
                    } ?>
                </div>

                <?= $this->render('partial/_model_social2'); ?>

                <div class="col-sm-8 wide-padding wide-padding--right model-page__2ndcontainer p-t20">
                    <?php if ($model3d->description): ?>
                        <div class="model-page__sidebar-title">
                            <?= _t('store.unit', 'Model Description'); ?>
                        </div>
                        <div class="model-description">
                            <?= StoreUnitUtils::displayMore($model3d->description); ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-sm-8 wide-padding wide-padding--right model-page__2ndcontainer">
                    <div class='model-area'>

                        <!--/** ABOUT AUTHOR  **/-->
                        <?php if ($modelAuthor && $model3d->isPublished()): ?>
                            <div class="about-author">
                                <div class="about-author__title"><?php echo _t('site.store', 'About The Model’s Author'); ?></div>

                                <div class="row about-author__body">
                                    <div class="col-sm-12 col-md-8 about-author__body__info">
                                        <div class="about-author__body__info__pic">
                                            <?php
                                            $ava = $modelAuthor->company ?
                                                Html::img($modelAuthor->company->getCompanyLogoOrDefault(false, 75)) :
                                                UserUtils::getAvatar($modelAuthor->userProfile->avatar_url, $modelAuthor->email, 'author');
                                            ?>
                                            <?= Html::a($ava, $linkAuthorStore); ?>
                                        </div>
                                        <div class="about-author__body__info__text">
                                            <div class="about-author__body__info__text__name">
                                                <?= Html::a($authorUserName, $linkAuthorStore); ?>
                                            </div>
                                            <div class="about-author__body__info__text__uploads">
                                                <?php
                                                $uploadsCount = UserFacade::getStoreModelsCount($modelAuthor);
                                                echo _t(
                                                    "site.user",
                                                    "{n, plural, =0{No uploads} =1{1 model upload} other{{count} models uploaded}}",
                                                    ['count' => $uploadsCount, 'n' => $uploadsCount]
                                                ); ?>
                                            </div>
                                            <div class="about-author__body__status__approve">
                                                <span class="tsi tsi-checkmark-c text-success"></span>
                                                <?php echo _t("site.user", "Approved profile"); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <a href="<?= $linkAuthorStore ?>"
                                           class="btn btn-primary btn-ghost about-author__profile-btn">
                                            <span class="tsi tsi-user"></span> <?php echo _t('site.model3d', 'View Store'); ?></a>
                                    </div>
                                </div>
                            </div> <!-- about-author -->
                        <?php endif; ?>

                        <!-- Recently uploaded models-->
                        <div class="recent-uploaded recent-uploaded--file-page m-t20">
                            <div class="container">
                                <?php
                                $recentModels = \common\models\repositories\Model3dRepository::findRecentModels(null, 4);
                                if ($recentModels):
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h3 class="recent-uploaded__heading"><?= _t('front.upload', 'Recently uploaded models'); ?></h3>
                                        </div>
                                    </div>
                                    <div class="row recent-uploaded__row">
                                        <?php $i = 0;
                                        foreach ($recentModels as $recentModel):
                                            $i++;
                                            /** @var $recentModel \common\models\Model3d */
                                            $coverImage = Model3dFacade::getCover($recentModel);
                                            $imgThumb   = ImageHtmlHelper::getThumbUrl(
                                                $coverImage['image'],
                                                ImageHtmlHelper::IMG_CATALOG_WIDTH,
                                                ImageHtmlHelper::IMG_CATALOG_HEIGHT);
                                            $itemLink   = Model3dFacade::getStoreUrl($recentModel);
                                            ?>
                                            <div class="col-xs-12 col-sm-3">
                                                <a class="recent-uploaded__card" href="<?= $itemLink; ?>">
                                                    <img class="recent-uploaded__pic" src="<?= $imgThumb; ?>" alt="<?= H($recentModel->title); ?>">
                                                    <h4 class="recent-uploaded__title">
                                                        <?= H($recentModel->title); ?>
                                                    </h4>
                                                </a>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
