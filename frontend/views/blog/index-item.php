<?php

/** @var \common\models\BlogPost $model */
use yii\helpers\Html;
use yii\helpers\Url;

$postViewUrl = Url::toRoute(['/blog/view', 'alias' => $model->alias], true);
?>
<div class="post-list__item">
    <div class="post">
        <a href="<?= $postViewUrl?>" class="post__image-container">
            <img class="post__image" alt="<?=H($model->title);?>" src="<?= $model->imagePreviewFile->getFileUrl()?>">
        </a>
        <h3 class="post__title">
            <?= Html::a($model->title, $postViewUrl)?>
        </h3>
        <p class="post__short-text"><?= $model->description?></p>
        <div class="post__footer">
            <div class="post__date">
                <span class="tsi tsi-calendar"></span><?= Yii::$app->formatter->asDate($model->date)?>
            </div>
        </div>
    </div>
</div>


