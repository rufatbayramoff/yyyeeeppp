<?php

/** @var \yii\data\ActiveDataProvider $dataProvider */
use yii\widgets\ListView;


/** @var ListView $listView */
$listView = Yii::createObject([
    'class' => ListView::class,
    'dataProvider' => $dataProvider,
    'itemView' => 'index-item',
    'layout' => '{items}',
    'options' => [
        'class' => 'post-list',
    ],
    'itemOptions' => [
        'tag' => null,
    ]
]);

?>

<div class="container post-list-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="blog-page-title"><?= _t('site.blog', 'Treatstock Blog');?></h1>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-9 col-lg-10">
            <div class="post-list">
                <?= $listView->renderItems(); ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-lg-2 p-l0">
            <div class="tsadelement">
                <div id="amzn-assoc-ad-36ecd97a-d7c3-42bd-b069-67da508d2ac4"></div>
                <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=36ecd97a-d7c3-42bd-b069-67da508d2ac4"></script>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?= $listView->renderPager() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <a href="/blog/rss" target="_blank" class="btn btn-xs btn-info">RSS</a>
        </div>
    </div>

    <div class="p-l0 p-r0 p-0">
        <div class="tsadelement tsadelement--horizontal">
            <div id="amzn-assoc-ad-f75163cb-f6d9-470a-8686-adc365ba9c2c"></div>
            <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=f75163cb-f6d9-470a-8686-adc365ba9c2c"></script>
        </div>
    </div>
</div>