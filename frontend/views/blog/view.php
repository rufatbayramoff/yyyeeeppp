<?php

/** @var \common\models\BlogPost $post */
/** @var \yii\web\View $this */

use common\components\JsObjectFactory;
use frontend\assets\SocialButtonsAsset;
use yii\helpers\Url;
$postViewUrl = Url::toRoute(['/blog/view', 'alias' => $post->alias], true);

$ogTags = [
    'og:url' => param('server') . app('request')->url,
    'og:title' => $post->title,
    'og:description' => \H(strip_tags($post->description)),
    'og:image' => $post->imageFile->getFileUrl(),
    'og:type' => 'website'
];

$this->params['meta_description'] = strip_tags($post->description);
$this->registerLinkTag([
    'rel' => 'image_src',
    'href' => $post->imageFile->getFileUrl()
], true);
$this->params['ogtags'] = $ogTags;

$this->registerMetaTag([
    'property' => 'fb:app_id',
    'content' => param('facebook_app_id')
]);

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);
?>
<div class="post-inner">

    <div class="post-hero">

        <a href="/blog" class="btn btn-default post-back-btn post-back-btn--hero"><span class="tsi tsi-arrow-left-l"></span> <?= _t('site.blog', 'Back to blog')?></a>

        <div class="container post-hero__text">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h1 class="post-hero__title">
                    <?= $post->title?>
                </h1>
                <div class="post__footer post__footer--hero">
                    <a href="#comments" class="post__comments post__comments--hero">
                        <span class="tsi tsi-comment"></span> <span class="fb-comments-count" data-href="<?= $postViewUrl?>"></span> <?= _t('site.blog', 'comments')?>
                    </a>

                    <div class="post__date post__date--hero">
                        <span class="tsi tsi-calendar"></span><?= Yii::$app->formatter->asDate($post->date)?>
                    </div>
                </div>

                <div class="m-t10 text-center">
                    <p class="post-share__label m-b10"><?= _t('site.blog', 'Share with friends')?>:</p>

                    <div class="model-share__list js-social-likes post-share__list">

                        <div class="model-share__btn"><a href="<?= Url::toRoute(['/site/sendemail'])?>" title="<?= _t("site.email", "Send email"); ?>" class="ts-ajax-modal">
                                <span class="social-likes__icon social-likes__icon_mail"></span></a>
                        </div>

                        <div class="model-share__btn sharelink" id="sharelink" data-placement="bottom" data-container="body" data-toggle="popover" data-original-title="<?= _t("site.share", "Share this link"); ?>">
                            <span class="social-likes__icon social-likes__icon_link" title="<?= _t("site.email", "Share this link"); ?>"></span>
                        </div>

                        <div class="model-share__btn facebook-send"  title="<?= _t("site.email", "Facebook Messenger"); ?>" >
                            <span class="social-likes__icon social-likes__icon_facebook-m"></span> Facebook Send
                        </div>

                        <div class="model-share__btn facebook" title="<?= _t("site.email", "Share this link"); ?>" data-display="popup">Facebook</div>
                        <div class="model-share__btn twitter" title="<?= _t("site.email", "Tweet this link"); ?>" data-via="treatstock"> Twitter</div>
                        <div class="model-share__btn pinterest" title="<?= _t("site.email", "Pin this model"); ?>" data-media="<?= $post->imagePreviewFile->getFileUrl()?>"> Pinterest</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="post-hero__bg" style="background-image: url('<?= $post->imageFile->getFileUrl()?>')"></div>

    </div>

    <div class="container post-content">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-lg-8 col-lg-offset-2 post-content__body">
                <?= $post->content?>
            </div>
            <div class="col-xs-12 col-sm-3 col-lg-2 p-l0">
                <div class="tsadelement">
                    <div id="amzn-assoc-ad-36ecd97a-d7c3-42bd-b069-67da508d2ac4"></div>
                    <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=36ecd97a-d7c3-42bd-b069-67da508d2ac4"></script>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2 post-share">
                <hr class="m-t0">

                <p class="post-share__label"><?= _t('site.blog', 'Share with friends')?>:</p>

                <div class="model-share__list js-social-likes post-share__list">

                    <div class="model-share__btn"><a href="<?= Url::toRoute(['/site/sendemail'])?>" title="<?= _t("site.email", "Send email"); ?>" class="ts-ajax-modal">
                        <span class="social-likes__icon social-likes__icon_mail"></span></a>
                    </div>
 
                    <div class="model-share__btn sharelink" id="sharelink" data-placement="bottom" data-container="body" data-toggle="popover" data-original-title="<?= _t("site.share", "Share this link"); ?>">
                        <span class="social-likes__icon social-likes__icon_link" title="<?= _t("site.email", "Share this link"); ?>"></span>
                    </div>

                    <div class="model-share__btn facebook-send"  title="<?= _t("site.email", "Facebook Messenger"); ?>" >
                        <span class="social-likes__icon social-likes__icon_facebook-m"></span> Facebook Send
                    </div>

                    <div class="model-share__btn facebook" title="<?= _t("site.email", "Share this link"); ?>" data-display="popup">Facebook</div>
                    <div class="model-share__btn twitter" title="<?= _t("site.email", "Tweet this link"); ?>" data-via="treatstock"> Twitter</div>
                    <div class="model-share__btn pinterest" title="<?= _t("site.email", "Pin this model"); ?>" data-media="<?= $post->imagePreviewFile->getFileUrl()?>"> Pinterest</div>
                </div>

            </div>
        </div>

        <div class="p-l0 p-r0 p-0">
            <div class="tsadelement tsadelement--horizontal">
                <div id="amzn-assoc-ad-f75163cb-f6d9-470a-8686-adc365ba9c2c"></div>
                <script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=f75163cb-f6d9-470a-8686-adc365ba9c2c"></script>
            </div>
        </div>

        <div class="row text-center">
            <a href="/blog" class="btn btn-default post-back-btn m-t30 m-b20"><span class="tsi tsi-arrow-left-l"></span> Back to blog</a>
        </div>


    </div>

</div>

<?php

$this->registerJs("var facebook_app_id = '".param('facebook_app_id')."';", \yii\web\View::POS_HEAD);
$this->registerAssetBundle(SocialButtonsAsset::class);
JsObjectFactory::createJsObject(
    'socialButtonsClass',
    'socialButtonsObj',
    []
    ,
    $this
);
?>

<script>
    <?php $this->beginBlock('js1', false); ?>

    //Init Picture Slider
    var swiperPictureSlider = new Swiper('.picture-slider', {
        scrollbar: '.picture-slider__scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>
