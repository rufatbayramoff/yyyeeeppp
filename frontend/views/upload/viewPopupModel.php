<?php
/** @var \yii\web\View $this */
$modelColors = \Yii::$app->setting->get('model3d.colors', ['#afafaf']);

?>
<div class="modal-dialog upload-view-model">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" >
                <span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
            <div class="modal-title " id="loginAjax-modal">
                <?php echo _t('front.upload', '3D Preview'); ?>
            </div>
        </div>

        <div class="modal-body">
             <?php foreach ($modelColors as $mColor): ?>
                <input type="button" class="btn btn-sm ts-material-color"  value=" " onclick="TS.changecolor('<?php echo $mColor;?>')" style="background: <?php echo $mColor; ?>">
            <?php endforeach; ?>
                <!--
                <button type="button" class="btn btn-sm btn-default" title="<?= _t('front.upload', 'Take screenshot')?>" value=" " onclick="TS.takeScreen()">
                    <span class="tsi tsi-camera" aria-hidden="true"></span>
                </button>
                -->
                <div id="viewer"></div>
                <div id="model3d-screens"></div>
        </div>
    </div>
</div>
 
<div id="model3d-screens" style="height:200px"></div>
 
<script>
<?php $this->beginBlock('js_1', false); ?>
        TS.takeScreen = function() {
            var dataUrl = thingiview.getCurrentImage();
            $('#model3d-screens').append($('<img/>', {width: 250, src: dataUrl}).addClass('imageshot'));
            return false;
        };
        TS.changecolor = function(val) { 
            if (thingiview) {
                thingiview.setObjectColor(val);
            }
            return false;
        };
        
<?php $this->endBlock(); ?>
</script>        

<?php
$this->registerJs($this->blocks['js_1']);
$this->registerAssetBundle(\frontend\assets\ModelViewerAsset::class);

