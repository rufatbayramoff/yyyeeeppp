<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.12.16
 * Time: 11:11
 */

/** @var string $utmSource */
?>

<div class="panel m-t30">
    <div class="panel-body">

        <?php if ($utmSource) { ?>
            <p>
                <strong>
                    <?= _t('site.upload', 'Do you have 3D models that you would like to get printed?'); ?>
                </strong>
            </p>
            <p>
                <?= _t(
                    'site.upload',
                    'Upload your designs to Treatstock’s secure platform and choose from the best 3D print services near you. Select the material, color, and quantity of your prints and compare prices and reviews when placing an order.'
                ); ?>
            </p>

            <?php
            if ($utmSource=='cnc') {
                ?>
                <p>
                    <?= _t('front.upload', 'For now only STEP, IGES, STL and PLY files are supported'); ?>
                </p>
                <p>
                    <?= _t('front.upload', 'Recommended file formats include STEP (ap203, ap203is, ap214, ap214, ap214is) and IGES (v 5.3 mode 186).'); ?>
                </p>
            <?php
            } else {
                ?>
                <p>
                    <?= _t('site.upload', 'Currently, only STL and PLY files are supported, but we are working on adding more options.');?>
                </p>
                <?php
            }
            ?>
        <?php } else { ?>
            <p>
                <?php
                $linkHelp = frontend\widgets\SiteHelpWidget::widget(
                    [
                        'triggerHtml' => _t('site.payments', 'comply with all the laws'),
                        'alias'       => 'site.payments.laws'
                    ]
                );
                $techHelp = yii\helpers\Html::a("technical solutions", ['/help/article/49-how-are-my-intellectual-property-rights-for-my-designs-protected'], ['target' => '_blank']);
                echo _t(
                    'site.upload',
                    'Upload your 3D models to share or sell them as finished products to customers from all over the world. To make your designs easy to find, please upload images of the finished product, provide a detailed description and assign relevant tags. We apply {tech} to protect your intellectual property and are required to {link}.',
                    ['link' => $linkHelp, 'tech' => $techHelp]
                ); ?>
            </p>
            <p>
                <?= _t('site.upload', 'Currently, only STL, PLY, OBJ, 3MF, JPEG, PNG and GIF files are supported but we\'re working on adding more.'); ?>
            </p>
            <p>
                <?= _t('site.upload', 'If your 3MF file exceeds 5 MB, please, upload STL instead.');?>
            </p>
        <?php }; ?>
    </div>
</div>