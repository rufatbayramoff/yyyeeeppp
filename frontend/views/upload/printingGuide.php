<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.12.16
 * Time: 11:14
 */
?>
<div class="printing-guide">
    <h2 class="m-t30 m-b20"><?= _t('site.upload', 'Orders Guide'); ?></h2>

    <div class="how-to__pane m-b20">
        <div class="how-to-steps-wrap">
            <div class="how-to-steps" style="margin-top: 60px;">
                <div class="how-to-steps__text">
                    <div class="how-to-steps__count how-to-steps__count--one">1</div>
                    <h3><?= _t('site.page', 'Upload') ?></h3>
                    <p><?= _t(
                            'site.page',
                            'Upload your files to our secure network to find manufacturers on a global-scale.'
                        ) ?></p>
                </div>
            </div>

            <div class="how-to-steps" style="margin-top: 60px;">
                <div class="how-to-steps__text">
                    <div class="how-to-steps__count">2</div>
                    <h3><?= _t('site.page', 'Customize') ?></h3>
                    <p><?= _t(
                            'site.page',
                            'Choose from a large variety of materials and colors, set different quantities and adjust sizes using our scale feature.') ?></p>
                </div>
            </div>

            <div class="how-to-steps" style="margin-top: 60px;">
                <div class="how-to-steps__text">
                    <div class="how-to-steps__count">3</div>
                    <h3><?= _t('site.page', 'Checkout') ?></h3>
                    <p><?= _t(
                            'site.page',
                            'Compare prices, reviews, and delivery options and place an order with a manufacturer of your choice.'
                        ) ?></p>
                </div>
            </div>
        </div>
    </div>
</div>