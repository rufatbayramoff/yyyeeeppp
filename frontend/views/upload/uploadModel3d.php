<?php

use common\interfaces\Model3dBaseInterface;
use common\models\Model3d;
use common\models\Ps;
use common\models\PsPrinter;
use common\models\repositories\Model3dRepository;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\model3d\Model3dFacade;
use frontend\models\ps\PsFacade;
use frontend\widgets\Model3dUploadWidget;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var string $utmSource */
/** @var Ps $ps */
/** @var PsPrinter $psPrinter */
/** @var bool $psPrinterIdOnly */

$additionalParams = [];
if ($utmSource) {
    $additionalParams['utmSource'] = $utmSource;
}
if ($ps) {
    $additionalParams['psId'] = $ps->id;
}
if ($psPrinter) {
    $additionalParams['psPrinterId'] = $psPrinter->id;
}
if ($psPrinterIdOnly) {
    $additionalParams['psPrinterIdOnly'] = $psPrinterIdOnly;
}

$isCnc = $utmSource == 'cnc';

/**
 * Is need show recent models block
 * @param array $recentModels
 * @param bool $isCnc
 * @return bool
 */
$isShowRecentModelsFn = function (array $recentModels, bool $isCnc)
{
    if (!$recentModels) {
        return false;
    }

    if (!$isCnc) {
        return true;
    }

    $user = \frontend\models\user\UserFacade::getCurrentUser();

    if (!$user) {
        return false;
    }

    $ps = PsFacade::getPsByUserId($user->id);

    if ($ps && $ps->haveUndeletedCncMachine()) {
        return true;
    }

    return false;
};

/**
 * Retrun route to recent model
 *
 * @param Model3dBaseInterface $model
 * @param bool $isCnc
 * @return string
 */
$resolveRecentModelUrlFn = function (Model3dBaseInterface $model, bool $isCnc)
{
    if ($isCnc) {
        $modelRoute = ['/workbench/cncm/filepage/index', 'modelId' => $model->getSourceModel3d()->id];
        return Url::toRoute($modelRoute);
    }

    return Model3dFacade::getStoreUrl($model);
}

?>

<?= $this->render('/common/ie-alert.php'); ?>

<div class="container">
    <?php
    if (!empty($link) && filter_var($link, FILTER_VALIDATE_URL) !== false): ?>
        <div class="row">
            <div class="col-md-8 wide-padding wide-padding--right">
                <h3 class="m-t30"><?= _t(
                        'front.upload',
                        'Welcome to Treatstock!'
                    ); ?>
                </h3>
                <p>
                    <?= _t(
                        'front.upload',
                        'The manufacturing platform where digital designs are transformed into high-quality products by fast and affordable manufacturers from all over the world.'
                    ); ?>
                </p>

                <div class="m-b10 clearfix">
                    <div style="float: left;width: 20px;height: 20px;margin: 0 10px 0 0;border-radius: 50%;font-size: 14px;line-height: 20px;font-weight: bold;text-align: center;background-color: #e00457;color: #ffffff;">1</div>
                    <div style="overflow: hidden;">
                        <?= _t(
                            'front.upload',
                            'Click the link to be redirected to the source site and download the files'
                        ); ?>
                    </div>
                </div>

                <div style="display: inline-block;margin: 0 0 15px; padding: 10px 15px; border: 1px solid #e0e4e8; border-radius: 5px;background: #f4f8fc;">
                    <?= Html::a(htmlspecialchars($link), $link, ['target' => '_blank']); ?>
                </div>

                <div class="clearfix" style="margin: 10px 0 -20px">
                    <div style="float: left;width: 20px;height: 20px;margin: 0 10px 0 0;border-radius: 50%;font-size: 14px;line-height: 20px;font-weight: bold;text-align: center;background-color: #e00457;color: #ffffff;">2</div>
                    <div style="overflow: hidden;">
                        <?= _t('front.upload', 'Upload the files to the drag-and-drop area below'); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 wide-padding wide-padding--right">
            <h1 class="m-b20"><?=_t('front.upload', 'Upload Files');?></h1>
            <?= Model3dUploadWidget::widget(
                [
                    'additionalParams' => $additionalParams,
                    'authRequired'     => $utmSource ? false : true
                ]
            ); ?>
        </div>

        <div class="col-md-4 sidebar upload-sidebar">
            <?= $this->render('rightPanelInfo', compact('utmSource')); ?>
        </div>
    </div>
</div>

<?php
$recentModels = Model3dRepository::findRecentModels();

if($isShowRecentModelsFn($recentModels, $isCnc)):
?>
<div class="recent-uploaded">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="recent-uploaded__heading"><?= _t('front.upload', 'Recently uploaded models'); ?></h3>
            </div>
        </div>
        <div class="row recent-uploaded__row">
            <?php foreach($recentModels as $recentModel):
                 /** @var $recentModel Model3d */
                 $coverImage = Model3dFacade::getCover($recentModel);
                 $imgThumb = ImageHtmlHelper::getThumbUrl($coverImage['image'], ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
                 $itemLink = $resolveRecentModelUrlFn($recentModel, $isCnc);
                ?>
            <div class="col-xs-12 col-sm-3">
                <a class="recent-uploaded__card" href="<?=$itemLink;?>">
                    <img class="recent-uploaded__pic"
                         src="<?=$imgThumb; ?>" alt="<?=H($recentModel->title);?>">
                    <h4 class="recent-uploaded__title">
                        <?=H($recentModel->title);?>
                    </h4>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if(!$isCnc): ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?= $utmSource ? $this->render('printingGuide') : $this->render('uploadGuide') ?>
            </div>
        </div>
    </div>
<?php endif; ?>

