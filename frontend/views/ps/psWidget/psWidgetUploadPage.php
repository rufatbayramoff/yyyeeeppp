<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 10.01.17
 * Time: 15:54
 *
 */


use frontend\widgets\Model3dUploadWidget;
use frontend\widgets\PrintByPsTitleWidget;

/** @var \common\models\Ps $ps */

$toPs = $ps ? $ps : $psPrinter->ps;
?>
<div class="item-rendering-external-widget__header--upload">
    <?= PrintByPsTitleWidget::widget(
        [
            'ps' => $toPs
        ]
    );
    ?>
</div>
<?php

$additionalParams = [
    'isExternalWidget' => 'facebookPs',
    'utmSource'        => 'external_printer_widget',
    'psId'             => $ps ? $ps->id : null,
    'psPrinterId'      => $psPrinter ? $psPrinter->id : null,
    'psPrinterIdOnly'  => true,
    'model3dSource'    => $model3dSource,
    'widget'           => true,
];
if (!empty($utmSource)) {
    $additionalParams['utmSource'] = $utmSource;
    if(!empty($cncMachine))
        $additionalParams['machineId'] = $cncMachine->id;
}

echo Model3dUploadWidget::widget(
    [
        'isPlaceOrderState' => true,
        'additionalParams' => $additionalParams,
    ]
);
?>
