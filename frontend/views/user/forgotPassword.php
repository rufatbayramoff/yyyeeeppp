<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\user\PasswordResetRequestForm */

$this->title = _t('front.site', 'Request password reset');
#$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
<div class="container">

    <div class="row">
        <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 m-t30">
            <div class="panel panel-default box-shadow border-0">
                <div class="panel-body">

                    <h1 class="text-center m-t0"><?= \H($this->title) ?></h1>

                    <p class="m-b20"><?php echo _t('front.site', 'Please enter your email. A link to reset password will be sent there.'); ?></p>

                    <?php
                        $form = ActiveForm::begin(['id' => 'request-password-reset-form', 'layout'=>'horizontal']);
                        /* echo $form->errorSummary($model, ['header'=>'']); */ //turned OFF by Ilnar: this function duplicated
                    ?>

                    <?= $form->field($model, 'email') ?>

                    <?= Html::submitButton(_t('front.site', 'Send'), ['class' => 'btn btn-block btn-primary']) ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>