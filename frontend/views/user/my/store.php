<?php

use common\services\LikeService;
use frontend\models\model3d\Model3dFacade;
use yii\helpers\Url;
use frontend\components\image\ImageHtmlHelper;

echo $this->renderFile('@app/views/user/my/_top.php', $params);


$this->title = _t('front.user', 'My Store');
#$this->params['breadcrumbs'][] = ['label' => _t('front.user', 'Private profile'), 'url' => ['/user-profile/']];
#$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="container">

    <div class="row wide-padding">
        <h1><?php echo $this->title; ?></h1>
        <?php foreach ($items as $k => $item):
            $model3d = $item->model3d;
            $likesCount = LikeService::getModelLikesCount($model3d);
            ?>
            <div class='col-sm-6 col-md-4'>
                <div class='catalog-item' data-collection-item-id="<?= $item->id ?>">

                    <div class='catalog-item__pic'>


                        <div class="catalog-item__controls">
                            <?php

                            if (frontend\models\user\UserFacade::isObjectOwner($model3d)) {
                                echo \yii\helpers\Html::a(
                                    '<span class="tsi tsi-edit"></span> <span class="catalog-item__controls__item__hint">' .
                                    _t('app', 'Edit') . '</span>', ['my/model3d/edit', 'id' => $model3d->id], [
                                    'class' => 'catalog-item__controls__item',
                                    'title' => 'Edit'
                                ]);
                            }
                            ?>
                        </div>
                        <?php
                        $itemTitle = \yii\helpers\StringHelper::truncate(str_replace("_", " ", $model3d->title), 55);
                        $itemTitle = \H($itemTitle);
                        $coverImage = Model3dFacade::getCover($model3d);
                        if ($coverImage) {
                            $coverImage['image'] = ImageHtmlHelper::getThumbUrl($coverImage['image'], ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
                            $img = yii\helpers\Html::img($coverImage['image'], ['alt' => $itemTitle]);
                            echo yii\helpers\Html::a($img, Model3dFacade::getStoreUrl($model3d), ['title' => $itemTitle, 'class' => 'catalog-item__pic__img']);
                        }
                        $status = $item->status;
                        $statusLabel = '';
                        if ($status == 'rejected' || $status == 'banned') {
                            $statusLabel = 'label-danger';
                        } else {
                            if ($status == 'deleted') {
                                $statusLabel = 'label-warning';
                            } else {
                                if ($status == 'published') {
                                    $statusLabel = 'label-success';
                                } else {
                                    if ($status == 'new') {
                                        $statusLabel = 'label-primary';
                                    }
                                }
                            }
                        }
                        $status = '<span class="label ' . $statusLabel . '">' . $status . '</span>';
                        ?>
                    </div>

                    <div class="catalog-item__footer">
                        <h4 class="catalog-item__footer__title">
                            <?php echo yii\helpers\Html::a($itemTitle, Model3dFacade::getStoreUrl($model3d)); ?>
                        </h4>

                        <div class="catalog-item__footer__price">
                            <?=displayAsMoney($model3d->getPriceMoneyByQty(1));?>
                        </div>

                        <div class="catalog-item__footer__stats">

                            <?php
                            echo $status;
                            /*    <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment-o"></span> 0 reviews
                            </div> */
                            ?>

                            <div class="catalog-item__footer__stats__likes js-hide-like-if-not-liked" data-likes="<?= $likesCount ?>">
                                <span class="tsi tsi-heart"></span> <span class="js-store-item-likes-count"><?= LikeService::getLikesCountString($likesCount) ?></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>

<?php if (false): ?>
    <div class="row wide-padding">

        <div class="col-xs-12">

            <h1><?php echo $this->title; ?></h1>

            <div class="user-my-store store-index">
                <div class="row">

                    <?php
                    foreach ($items as $item):

                        $model3d = $item->model3d;
                        $itemLink = Model3dFacade::getStoreUrl($model3d);
                        $itemTitle = \yii\helpers\StringHelper::truncate(str_replace("_", " ", $model3d->title), 55);
                        $itemTitle = \H($itemTitle);
                        $coverImage = frontend\models\model3d\Model3dFacade::getCover($model3d);
                        $price = displayAsCurrency($item['price_per_produce'], $item['price_currency']);
                        if (floatval($item['price_per_produce']) < 0) {
                            $price = _t('site.store', 'Free');
                        }
                        $status = $item->status;
                        $statusLabel = '';
                        if ($status == 'rejected' || $status == 'banned') {
                            $statusLabel = 'label-danger';
                        } else {
                            if ($status == 'deleted') {
                                $statusLabel = 'label-warning';
                            } else {
                                if ($status == 'published') {
                                    $statusLabel = 'label-success';
                                } else {
                                    if ($status == 'new') {
                                        $statusLabel = 'label-primary';
                                    }
                                }
                            }
                        }
                        $status = '<span class="label ' . $statusLabel . '">' . $status . '</span>';
                        ?>
                        <div class='col-xs-12 col-sm-6 col-md-4'>
                            <div class='catalog-item'>
                                <div class='catalog-item__pic'>

                                    <div class="catalog-item__controls"></div>

                                    <?php
                                    if ($coverImage) {
                                        $img = yii\helpers\Html::img($coverImage['image'], ['alt' => $itemTitle]);
                                        echo yii\helpers\Html::a($img, $itemLink, ['title' => $itemTitle, 'class' => 'catalog-item__pic__img']);
                                    }
                                    ?>
                                </div>

                                <div class="catalog-item__footer">
                                    <h4 class="catalog-item__footer__title catalog-item__footer__title--wide">
                                        <?php echo yii\helpers\Html::a($itemTitle, $itemLink); ?>
                                    </h4>

                                    <div class="catalog-item__footer__price">
                                        <?php echo $price; ?>
                                    </div>

                                    <div class="catalog-item__footer__stats">
                                        <div class="catalog-item__footer__stats__publish">
                                            <?php echo _t('front.store', 'Publish status'); ?>: <br><?php echo $status; ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>

    </div>
<?php endif; ?>