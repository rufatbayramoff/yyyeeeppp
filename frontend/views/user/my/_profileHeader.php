<?php

$links = [
    '@web/user-profile' => _t('front.user', 'Profile'),
    #'@web/user/profile-update' => _t('front.user', 'Update profile'),
    '/my/delivery' => _t('front.user', 'Delivery'),
    //'my/payments' => _t('front.user', 'Payments'),    
    '/my/payments/transactions' => _t('front.user', 'Earnings')  ,
];
$user =  frontend\models\user\UserFacade::getCurrentUser();
if(frontend\models\user\UserFacade::hasStore($user) || frontend\models\user\UserFacade::hasPs($user) 
    || !empty($user->model3ds)){

    $links['/my/taxes'] = _t('front.user', 'Taxes');

}
$links['/my/settings'] = _t('front.user', 'Settings');
$links['/my/notifications'] = _t('front.user', 'Notifications');
$links['/my/widgets'] = _t('front.user', 'Partnership');

if(!empty($user->access_token) || !empty($user->apiExternalSystems)){
    $links['/my/orders-report'] = _t('front.user', 'Orders Report');
}
if(!empty($user->affiliates)){
    $links['/affiliate/cabinet'] = _t('front.user', 'Affiliate\'s Cabinet');
}
$items = [];
if(!isset($curUrl)){
    $curUrl = \Yii::$app->request->pathInfo;
}
$curUrl = str_replace("/index", "", $curUrl);
foreach($links as $k=>$v){
    $item = ['label'=>$v, 'url'=>[$k]];
    if('/'.$curUrl==\Yii::getAlias($k)){
        $item['active'] = true;
    }
    $items[] = $item;
    
}

echo '<div class="nav-tabs__container">';
echo '<div class="container">';

echo \yii\bootstrap\Tabs::widget([
    'items' => $items
]);

echo '</div>';
echo '</div>';