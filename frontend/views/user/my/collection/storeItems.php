<?php

use common\modules\product\interfaces\ProductInterface;
use common\models\StoreUnit;
use common\services\LikeService;
use frontend\models\model3d\Model3dFacade;
use yii\helpers\Url;
use frontend\components\image\ImageHtmlHelper;
use yii\helpers\Html;
use common\models\model3d\CoverInfo;

/** @var array $params */
/** @var \common\models\Model3d[] $items */
/** @var \yii\web\View $this */

/**
 * Resolve user like this model
 *
 * @param $user
 * @param $model3d
 * @return bool|null
 * @internal param $model
 */
$isLikedFn = function ($user, $model3d) {
    if (is_guest() || !$user) {
        return false;
    }

    return LikeService::isModelLikedByUser($model3d, $user);
};

$user = $params['user'];

$this->title = _t('front.user', 'My Store');
$this->params['is_store'] = true;
$this->registerAssetBundle(\frontend\assets\CatalogAsset::class);
?>

<?php $this->beginBlock('control-buttons') ?>
    <a class="btn btn-primary" href="<?= Url::to('@web/upload') ?>"
       data-toggle="tooltip"
       data-placement="bottom" title=""
       data-original-title="<?= _t('front.collection', 'Upload 3D Model') ?>">
        <span class="tsi tsi-plus"></span>
        <?= _t('front.collection', 'Upload 3D Model') ?>
    </a>

<?php $this->endBlock() ?>

    <h2 class="collection__title"><?= $category->title; ?></h2>
    <div class="row wide-padding">
<?php foreach ($items as $k => $v):
    $model3d = $v;
    $item = $model3d->storeUnit;
    $likesCount = LikeService::getModelLikesCount($model3d);
    $printsCnt = $item ? $item->printed_count : 0;
    $itemLink = Model3dFacade::getPrintUrl($model3d);

    $isLiked = $isLikedFn($user, $v);
    $itemData = [
        // 'store-item-id' => $item->id,
        'model3d-id'         => $model3d->id,
        'is-liked'           => $isLiked,
        'collection-item-id' => $v->id
    ];
    /** @var bool $isOwner current user is owner store unit */
    $cover = new CoverInfo(Model3dFacade::getCover($model3d));
    $status = \common\modules\product\models\ProductBase::getProductStatusLabels()[$model3d->product_status];

    ?>
    <div class='col-sm-6 col-md-4'>
        <?= Html::beginTag('div', ['class' => 'catalog-item', 'data' => $itemData]) ?>
        <div class='catalog-item__pic'>
            <div class="catalog-item__controls catalog-item__controls--left">
                <?php if (empty($category->id)): ?>
                    <a href="<?= Url::toRoute(['my/model3d/delete', 'id' => $model3d->id]); ?>" class="catalog-item__controls__item "
                       data-confirm="<?= _t('site', 'Are you sure?') ?>" data-method="post">
                        <span class="tsi tsi-bin"></span>
                        <span class="catalog-item__controls__item__hint"><?= _t('site.store', 'Delete model'); ?></span>
                    </a>
                <?php endif; ?>
                <a href="<?= Url::toRoute(['my/model3d/download', 'id' => $model3d->id]); ?>" target="_blank" class="catalog-item__controls__item">
                    <span class="tsi tsi-download"></span>
                    <span class="catalog-item__controls__item__hint"><?= _t('site.store', 'Download model'); ?></span>
                </a>
            </div>
            <div class="catalog-item__controls">
                <?php if ($model3d->isPublished()): ?>

                    <div class="catalog-item__controls__item js-store-item-like <?= $isLiked ? 'js-store-item-like-showed' : '' ?>">
                        <span class="tsi <?= $isLiked ? 'tsi-heart' : 'tsi-heart' ?>"></span>
                        <span class="catalog-item__controls__item__hint js-store-item-like-label"><?= _t(
                                'front.catalog',
                                $isLiked ? _t('front', 'Unlike') : _t('front', 'Like')
                            ) ?></span>
                    </div>

                <?php endif; ?>

                <?php if (!($status == StoreUnit::STATUS_REJECTED || $status == StoreUnit::STATUS_BANNED)): ?>
                    <a href="<?= $itemLink; ?>" class="catalog-item__controls__item">
                        <span class="tsi tsi-printer3d"></span>
                        <span class="catalog-item__controls__item__hint"><?php
                            $msg = _t('front.catalog', '{n, plural, =0{Print It} =1{Printed 1 time} other{Printed # times}}', ['n' => $printsCnt]);
                            echo $msg; ?></span>
                    </a>
                <?php endif; ?>
                <?php
                if ($category->id > 0) {
                    echo \yii\helpers\Html::a(
                        '<span class="tsi tsi tsi-remove-c"></span> <span class="catalog-item__controls__item__hint">' .
                        _t('app', 'Unpublish') . '</span>',
                        ['my/model3d/unpublish', 'id' => $model3d->id],
                        [
                            'class'        => 'catalog-item__controls__item',
                            'title'        => 'Unpublish',
                            'data-confirm' => _t('site.store', 'Do you want to unpublish model?'),
                            'data-method'  => 'post'
                        ]
                    );
                }
                ?>

            </div>
            <?php
            $itemTitle = \yii\helpers\StringHelper::truncate(str_replace("_", " ", $model3d->title), 55);
            $itemTitle = \H($itemTitle);
            $coverImage = Model3dFacade::getCover($model3d);
            if ($coverImage) {
                $coverImage['image'] = ImageHtmlHelper::getThumbUrl($coverImage['image'], ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
                $img = yii\helpers\Html::img($coverImage['image'], ['alt' => $itemTitle]);
                echo yii\helpers\Html::a($img, ['my/model3d/edit', 'id' => $model3d->id], ['title' => $itemTitle, 'class' => 'catalog-item__pic__img']);
            }


            $statusLabel = '';
            if ($status == StoreUnit::STATUS_REJECTED || $status == StoreUnit::STATUS_BANNED) {
                $statusLabel = 'label-danger';
            } else {
                if ($status == 'deleted') {
                    $statusLabel = 'label-warning';
                } else {
                    if ($status == 'published_public') {
                        $statusLabel = 'label-success';
                    } else {
                        if ($status == 'draft') {
                            $statusLabel = 'label-primary';
                        } else {
                            $statusLabel = 'label-default';
                        }
                    }
                }
            }
            $statusTxt = \common\modules\product\models\ProductBase::getProductStatusLabels()[$model3d->product_status];
            $status = '<span class="label ' . $statusLabel . '">' . $statusTxt . '</span>';
            ?>
        </div>

        <div class="catalog-item__footer">
            <h4 class="catalog-item__footer__title">
                <?php echo yii\helpers\Html::a($itemTitle, ['my/model3d/edit', 'id' => $model3d->id]); ?>
            </h4>

            <div class="catalog-item__footer__price">
                <?=displayAsMoney($model3d->getPriceMoneyByQty(1));?>
            </div>

            <div class="catalog-item__footer__stats">

                <?php
                echo $status;
                /*    <div class="catalog-item__footer__stats__reviews">
                    <span class="tsi tsi-comment-o"></span> 0 reviews
                </div> */
                ?>

                <div class="catalog-item__footer__stats__likes js-hide-like-if-not-liked" data-likes="<?= $likesCount ?>">
                    <span class="tsi tsi-heart"></span> <span class="js-store-item-likes-count"><?= LikeService::getLikesCountString($likesCount) ?></span>
                </div>
            </div>

        </div>
    </div>
    </div>
<?php endforeach; ?>

    </div>

<?php if (false): ?>
    <div class="row wide-padding">

        <div class="col-xs-12">

            <h1><?php echo $this->title; ?></h1>

            <div class="user-my-store store-index">
                <div class="row">

                    <?php
                    foreach ($items as $item):

                        $model3d = $item->model3d;
                        $itemLink = Model3dFacade::getStoreUrl($model3d);

                        $itemTitle = \yii\helpers\StringHelper::truncate(str_replace("_", " ", $model3d->title), 55);
                        $itemTitle = \H($itemTitle);
                        $coverImage = frontend\models\model3d\Model3dFacade::getCover($model3d);
                        $price = displayAsCurrency($item['price_per_produce'], $item['price_currency']);
                        if (floatval($item['price_per_produce']) < 0) {
                            $price = _t('site.store', 'Free');
                        }
                        $status = $item->product_status;
                        $statusLabel = '';
                        if ($status ==  ProductInterface::STATUS_REJECTED) {
                            $statusLabel = 'label-danger';
                        } else {
                            if (!$model3d->is_active) {
                                $statusLabel = 'label-warning';
                            } else {
                                if ($status == ProductInterface::STATUS_PUBLISHED_PUBLIC) {
                                    $statusLabel = 'label-success';
                                } else {
                                    if ($status == 'new') {
                                        $statusLabel = 'label-primary';
                                    }
                                }
                            }
                        }
                        $status = '<span class="label ' . $statusLabel . '">' . $status . '</span>';
                        ?>
                        <div class='col-xs-12 col-sm-6 col-md-4'>
                            <div class='catalog-item'>
                                <div class='catalog-item__pic'>

                                    <div class="catalog-item__controls"></div>

                                    <?php
                                    if ($coverImage) {
                                        $img = yii\helpers\Html::img($coverImage['image'], ['alt' => $itemTitle]);
                                        echo yii\helpers\Html::a($img, $itemLink, ['title' => $itemTitle, 'class' => 'catalog-item__pic__img']);
                                    }
                                    ?>
                                </div>

                                <div class="catalog-item__footer">
                                    <h4 class="catalog-item__footer__title catalog-item__footer__title--wide">
                                        <?php echo yii\helpers\Html::a($itemTitle, $itemLink); ?>
                                    </h4>

                                    <div class="catalog-item__footer__price">
                                        <?php echo $price; ?>
                                    </div>

                                    <div class="catalog-item__footer__stats">
                                        <div class="catalog-item__footer__stats__publish">
                                            <?php echo _t('front.store', 'Publish status'); ?>: <br><?php echo $status; ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>

<?php endif; ?>