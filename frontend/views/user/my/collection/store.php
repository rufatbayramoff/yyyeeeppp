<?php
/** @var \yii\web\View $this  **/
/** @var array $items **/

use frontend\components\UserUtils;
use yii\helpers\Html;
use frontend\models\model3d\Model3dFacade;
use yii\helpers\Url;
use frontend\components\image\ImageHtmlHelper;

$user = \frontend\models\user\UserFacade::getCurrentUser();
$this->title = _t('front.user', 'My Store');
$this->params['page-class'] = 'js-collection-list';
$this->params['is_store'] = true;

$this->registerAssetBundle(\frontend\assets\CollectionAsset::class);
?>

<?php $this->beginBlock('control-buttons')?>
    <a  class="btn btn-primary" href="<?= Url::to('@web/upload')?>"
        data-toggle="tooltip"
        data-placement="bottom" title=""
        data-original-title="<?= _t('front.collection', 'Upload 3D Model')?>">
        <span class="tsi tsi-plus"></span>
        <?= _t('front.collection', 'Upload 3D Model')?>
    </a>
<?php $this->endBlock()?>
    
<div class="row my-collection-list">
<?php
if(empty($items)){
    echo '<div class="alert alert-info" role="alert">';
    echo _t('site.store', "You haven't uploaded any models yet");
    echo '</div>';
}
foreach($items as $collectionArray):
    $collection = json_decode(json_encode($collectionArray), false);
    $coverImg = \frontend\models\model3d\CollectionFacade::getCollectionCover($collection, $collectionArray['userCollectionModel3ds']);    
    
    $total = count($collectionArray['userCollectionModel3ds']);
?>
    <div class='col-sm-6 col-md-4'>
        <div
            class='catalog-item collection-item js-collection'
            data-id="<?php echo $collection->id; ?>"
            data-size="<?=$total?>"
            data-title="<?=  \H($collection->title);?>"
            >
            <div class='catalog-item__pic'>
                <div class="catalog-item__pic__img">
                    <div class="catalog-item__pic__img__fotorama js-catalog-item-slider">
                        <?php foreach($coverImg as $img):
                            $img = ImageHtmlHelper::getThumbUrl($img, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
                        ?>
                            <?= Html::beginTag('div', ['data-img' => $img]); ?>
                                <?= Html::a('', ['my/store/category', 'id'=> $collection->id])?>
                            <?= Html::endTag('div'); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>        
            <div class='catalog-item__footer'>
              
                <div class='catalog-item__footer__price'>  
                    <?php echo _t('front.model3d', '{n, plural, =0{empty} =1{1 item} other{# items}}', ['n'=>$total]); ?>
                </div>

                <h4 class='catalog-item__footer__title'>
                    <?= Html::a(\H($collection->title), ['my/store/category', 'id'=>$collection->id]); ?>
                </h4>
            </div>

            <div class="collection-item__decor-1"></div>
            <div class="collection-item__decor-2"></div>
            <div class="collection-item__decor-3"></div>

        </div>
    </div>    
<?php endforeach; ?>
</div>

    <div class="model-edit-tips m-t20 m-b20">
        <h4 class="model-edit-tips__title">
            <?= _t('site.model3d', 'Essential Tips for 3D designers') ?>:
        </h4>
        <ol class="model-edit-tips__list">
            <li>
                <?= _t(
                    'site.model3d',
                    'Always pay attention to the title, description, and tags of your model. They should reflect the main idea of your model. The title should be clear and self-explanatory.'
                ) ?>
            </li>
            <li>
                <?= _t(
                    'site.model3d',
                    'Choose the color and material that you recommend for the model. If you have a kit (a model that is assembled of several parts), set a material and color for each part. For duplicate parts, upload only one file and set the quantity required for the kit.'
                ) ?>
            </li>
            <li>
                <?= _t('site.model3d', 'Set the lowest possible price. You can always increase it later.') ?>
            </li>
            <li>
                <?= _t('site.model3d', 'Make sure you upload good quality photos of the model as a manufactured product.') ?>
            </li>
            <li>
                <?= _t(
                    'site.model3d',
                    'To create a buzz for your model, share it on dedicated online platforms such as social medias, communities, and forums.'
                ) ?>
            </li>
            <li>
                <?= _t(
                    'site.model3d',
                    'For models that require post-processing, write a step-by-step set of instructions. You can also post it on popular sites adding links to your store.'
                ) ?>
            </li>
            <li>
                <?= _t(
                    'site.model3d',
                    'Use our store widget for your Treatstock models on your own site or blog, so visitors to your sites can order your designs as finished products.'
                ) ?>
            </li>
        </ol>
    </div>
</div>
