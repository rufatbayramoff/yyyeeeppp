<?php
/** @var string $content */
use frontend\models\user\UserFacade;

/** @var \yii\web\View $this */

$this->beginContent('@frontend/views/layouts/main.php');
$storeActive = false;
if (!isset($this->params['page-class'])) {
    $this->params['page-class'] = 'js-collection-view';
}
if (isset($this->params['is_store'])) {
    $storeActive = true;
}

$tabs = [
    [
        'label' => _t('app', 'My Collections'),
        'url'   => ['/profile/collection'],
    ],
    [
        'label'  => _t('app', 'My Store'),
        'url'    => ['my/store/index'],
        'visible' => UserFacade::hasUploads(UserFacade::getCurrentUser()),
        'active' => $storeActive
    ]
];
$user = UserFacade::getCurrentUser();
$publishedModels = $user->getModel3ds()->storePublished($user)->all();
if ($publishedModels) {
    $tabs[] = [
        'label' => _t('app', 'My Business Tools'),
        'url'   => ['/my/store/widget'],
    ];
}

?>

<?= $this->renderFile('@app/views/user/my/_top.php', ['user' => UserFacade::getCurrentUser()]); ?>

<div class="<?= $this->params['page-class'] ?>">

    <div class="nav-tabs__container">
        <div class="container relative">

            <?=\yii\widgets\Menu::widget(
                [
                    'options'         => ['class' => 'nav nav-tabs'],
                    'activateParents' => true,
                    'items'           => $tabs
                ]
            );?>

            <div class="collection-control">
                <?= isset($this->blocks['control-buttons']) ? $this->blocks['control-buttons'] : '' ?>
            </div>

        </div>
    </div>


    <div class="container">

        <?= $content; ?>

    </div>

</div>

<?php $this->endContent(); ?>




