<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
/** @var \frontend\models\user\ChangeCoverForm $model */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'layout'=>'inline']) ?>

    <?php # $form->field($model, 'file')->fileInput() ?>

    <?= '<div class="text-center">'?>
        <?php
            echo $form->field($model,'file', [
                'template' => '{input}{label}{error}'
            ])->fileInput(['class'=>'inputfile', 'accept' => 'image/jpeg,image/png,image/gif'])->label('<span>
                   <i class="tsi tsi-upload-l"></i> ' .  _t('front.user','Choose a file') . '
               </span>', ['class'=>'uploadlabel']);
        ?>

        <?= Html::submitInput(_t('front.user', 'Upload cover'), ['class' => 'btn btn-primary']) ?>
    <?= '</div>'?>

<div class="upload-hint">
    <?= _t(
        'site.user',
        'For better results please upload pictures with a size like 1000х250 pixels. Max size of file is {size}.',
        ['size' =>  Yii::$app->formatter->asShortSize(\frontend\models\user\ChangeCoverForm::MAX_FILE_SIZE, 1)]);
    ?>
</div>

<?php ActiveForm::end() ?>


<script>
    TS.uploadInit();
</script>