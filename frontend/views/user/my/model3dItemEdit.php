<?php

use common\components\JsObjectFactory;
use common\interfaces\Model3dBasePartInterface;
use common\models\factories\Model3dTextureFactory;
use common\models\StoreUnit;
use common\services\Model3dService;
use common\services\PrinterMaterialService;
use frontend\assets\Model3dFormAsset;
use frontend\controllers\catalog\actions\serializers\Model3dSerializer;
use frontend\models\model3d\Model3dEditForm;
use frontend\models\user\UserFacade;
use frontend\widgets\CalculateCncButton;
use frontend\widgets\ColorSelectorWidget;
use frontend\widgets\Model3dInfoJs;
use frontend\widgets\ModelFotoramaWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\helpers\ArrayHelper;
use common\models\PaymentCurrency;
use yii\web\GoneHttpException;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var Model3dEditForm $model3dEditForm */
/** @var \common\components\ps\locator\materials\MaterialColorsItem[] $groupMaterials * */

$model3d       = $model3dEditForm->model3d;
$this->title   = _t('site.model3d', 'Edit 3D Model #') . (int)$model3d->id;
$breadCrumbs[] = $this->title;
$tags          = $model3d->tags;
$files         = Model3dService::getModel3dActivePartsAndImagesList($model3d);

$selectedFotoramaFileId = $model3dEditForm->selectedFotoramaFileId;
if (!array_key_exists($model3dEditForm->selectedFotoramaFileId, $files)) {
    $selectedFotoramaFileId = $model3d->cover_file_id;
    if (!array_key_exists($selectedFotoramaFileId, $files)) {
        $selectedFotoramaFileId = key($files);
    }
}
if (empty($files[$selectedFotoramaFileId])) {
    throw new GoneHttpException(_t('site.error', 'Error. Model is corrupted'));
}
$currentSelectedFileInfo = $files[$selectedFotoramaFileId];
if ($currentSelectedFileInfo instanceof Model3dBasePartInterface) {
    $isCantSelectMaterial = true;
    $currentTexture       = $currentSelectedFileInfo->getCalculatedTexture();
} elseif ($model3d->isOneTextureForKit()) {
    $currentTexture       = $model3d->getKitTexture();
    $isCantSelectMaterial = true;
} else {
    $currentTexture       = Model3dTextureFactory::createModel3dTexture();
    $isCantSelectMaterial = false;
}
$imgExt                  = app('setting')->get('model3d.allowscreens', ['jpg', 'jpeg', 'png', 'gif']);
$currencyList            = [$model3d->company->paymentCurrency->currency_iso => $model3d->company->paymentCurrency->title_original];

Yii::$app->angular
    ->service(['router', 'user', 'notify', 'modal', 'modelService'])
    ->controller(
        [
            'my/model3d/DropzoneModel',
            'my/model3d/ActiveFrameModel',
            'my/model3d/CropImage',
            'my/model3d/Model3dForm',
            'my/model3d/Model3dEditController',
            'store/filepage/models',
            'my/model3d/ColorContainer',
            'my/model3d/FotoramaController',
            'product/productCategorySelect'
        ])
    ->controllerParams([
        'model3d' => [
            'id'            => $model3d->id,
            'categoryId'    => $model3d->productCategory->id ?? null,
            'categoryLabel' => !empty($model3d->productCategory) ? $model3d->productCategory->getFullTitle() : ''
        ],
        'colors' => PrinterMaterialService::colors($groupMaterials)
    ]);

$autoScaleText = _t('model3d.upload', 'Part appears to be extremely small and may be in inches format.    Would you like to automatically scale to millimeter format?');

?>

<script type="text/ng-template" id="/product/select-product-category.html">
    <?= $this->renderFile('@frontend/modules/mybusiness/modules/product/views/common/productCategoryModal.php', ['category' => !empty($model3d->productCategory) ? $model3d->productCategory : null]) ?>
</script>

<script type="text/ng-template" id="/product/model-preview.html">
    <?= $this->render('partials/_fotoramaPreview') ?>
</script>
<script type="text/ng-template" id="/product/model-qty.html">
    <?= $this->render('partials/_fotoramaQty') ?>
</script>


<div class="container" ng-controller="Model3dEditController">

    <h2 class="m-t0 m-b0"><?php echo $this->title; ?></h2>

    <div class="row user-my-modelView">
        <div class="col-sm-8 wide-padding wide-padding--right m-t30">
            <div class="model-slider">
                <div class='model-cover' ng-controller="FotoramaController" ng-init="init('fotoramaWidgetDiv')">
                    <?=
                    ModelFotoramaWidget::widget(
                        [
                            'id'                      => 'fotoramaWidgetDiv',
                            'model'                   => $model3d,
                            'currentSelectedFileInfo' => $currentSelectedFileInfo,
                            'edit'                    => true,
                        ]
                    );
                    ?>
                </div>
            </div>

            <div class="clearfix"></div>

            <div id="dz-total-progress" class="model-download-progress">
                <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
            </div>


        </div>
        <div class="col-sm-4 sidebar sidebar--breadcrumbs model-page__sidebar">

            <?php
            $form = ActiveForm::begin(
                [
                    'id'      => 'model3dEditForm',
                    'options' => ['enctype' => 'multipart/form-data']
                ]
            ); ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row model-edit-btns">
                        <?php
                        echo Html::buttonInput(
                            $model3d->isPublished() ? _t('front.user', 'Save') : _t('front.user', 'Publish'),
                            [
                                'class'    => 'col-xs-12 col-lg-12 btn btn-danger t-model-edit-publish',
                                'ng-click' => "publish()",
                            ]
                        );
                        if (!$model3d->isPublished()) {
                            $txtUpload = _t('site.store.', 'Save unpublished');
                            echo Html::buttonInput(
                                $txtUpload,
                                [
                                    'class'    => 'col-xs-12 col-sm-12 col-lg-6 btn btn-default t-model-edit-unpublish',
                                    'ng-click' => "save()",
                                    'style'    => '',
                                ]
                            );
                        } else {
                            echo Html::buttonInput(
                                _t('front.user', 'Unpublish'),
                                [
                                    'class'    => 'col-xs-12 col-sm-12 col-lg-6 btn btn-default t-model-edit-unpublish',
                                    'ng-click' => "unpublish()",
                                ]
                            );
                        }
                        ?>
                        <button type="button" class="col-xs-12 col-sm-12 col-lg-5 col-lg-offset-1 btn btn-info btn-ghost t-model-edit-del"
                                ng-click = "delete()"
                                title="<?= _t('front.user', 'Delete'); ?>">
                            <span class="tsi tsi-bin m-r10"></span><?= _t('front.user', 'Delete'); ?>
                        </button>
                    </div>
                    <?php
                    $userId = UserFacade::getCurrentUserId();

                    if ($model3d->storeUnit) {
                        $rejectMsg     = '';
                        $productStatus = $model3d->product_status;
                        if ($productStatus === \common\modules\product\interfaces\ProductInterface::STATUS_REJECTED) {
                            $rejectMsg = $model3d->getRejectMessage();
                        }
                        $statusLabel = \frontend\models\store\StoreFacade::getStatusLabel($model3d->storeUnit);

                        $statusBtn = "<span class='label $statusLabel' title='$rejectMsg'>" . $model3d->getProductStatusLabel() . "</span>";
                        echo "<div class='m-b20'>";
                        echo _t('site.model3d', 'Publishing status: ') . $statusBtn;
                        echo "</div>";
                        if ($model3d->isPublished()) {
                            echo "<div class='m-t20 m-b20'>" . _t('site.model3d', 'Public url: ') . '<a href="' . param('server') . H($model3d->getPublicPageUrl()) . '">' . H($model3d->getPublicPageUrl()) . '</a></div>';
                        }
                    }
                    echo '<div class="model-page__sidebar-title">'._t('site.model3d', 'Title').' <span class="form-required">*</span></div>';
                    echo $form->field($model3dEditForm, 'title')->textInput(['placeholder' => 'Set model name'])->label(false);
                    echo '<div class="model-page__sidebar-title">'._t('site.model3d', 'Description').' <span class="form-required">*</span></div>';
                    echo $form->field($model3dEditForm, 'description')
                        ->textarea(
                            [
                                'rows'        => 4,
                                'placeholder' => _t(
                                    'site.model',
                                    'Please provide information on the size and dimensions of the model, appropriate filament for it and other information that the user can find useful and/or interesting.'
                                )
                            ]
                        )
                        ->label(false);

                    echo "<div class='marketplace-fields-off t-model-edit__tags m-b20'>";
                    echo $this->render('@frontend/views/model3d/partial/_model_tags', array_merge(['mode' => 'edit'], compact('model3dEditForm', 'tags', 'form')));
                    echo "</div>";
                    ?>

                    <div class='marketplace-fields-off t-model-edit__category'>
                        <label class="model-page__sidebar-title m-b0" for="category-title"><?= _t('site.model3d', 'Category') ?> <span class="form-required">*</span></label>
                        <div id="model_category_label" class="m-b10">{{model3d.getCategoryLabel()}}</div>
                        <button class="btn btn-primary btn-block m-b20"
                                ng-click="changeCategoryPressed()"
                                type="button">
                            <?= _t('site.model3d', 'Choose category'); ?>
                        </button>
                    </div>

                    <button class="btn btn-primary btn-block m-b20 js-add-image fotorama-ignore-click dz-clickable"
                            type="button">
                        <?= _t('site.model3d', 'Add cover image'); ?>
                    </button>

                    <div class="model-page__sidebar-title">
                        <?= _t('site.model3d', 'Price of Model per Print') ?>
                    </div>

                    <div class="model-price-per-print marketplace-fields-off">
                        <div class="model-price-per-print__price">
                            <?php
                            if (empty($model3dEditForm->pricePerPrint)) {
                                $model3dEditForm->pricePerPrint = 0;
                            }
                            echo $form->field($model3dEditForm, 'pricePerPrint')->input('number', ['step' => 'any', 'placeholder' => 'Price per copy'])->label(
                                "Price of Model per Print",
                                ['class' => 'hide']
                            ); ?>
                        </div>
                        <div class="model-price-per-print__currency">
                            <?php echo $form->field($model3dEditForm, 'priceCurrency')->dropDownList($currencyList)->label(false); ?>
                        </div>
                    </div>
                    <p class="model-price-per-print__price-hint">
                        <?= _t(
                            'site.model3d',
                            'Treatstock fee is 30%. If your model costs $1, you will get $0.7'
                        ) ?>
                    </p>

                    <div class="alert alert-info alert--text-normal">
                        <?= _t(
                            'site.model3d',
                            'Customers can\'t download your designs. Instead, they purchase them as printed products. Keeping your prices low will help to increase the number of times your model gets printed, and you get paid.'
                        ) ?>
                    </div>

                    <?php

                    echo "<div style='display:none'>";
                    echo $form->field($model3dEditForm, 'model_units', [])->radioList(
                        $model3dEditForm->getModelMeasureLabels(),
                        [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                $lid    = 'dtrmunit' . $index;
                                $return = '<div class="radio radio-inline"><input type="radio" ' .
                                    ($checked ? 'checked=checked' : '') . ' id="' . $lid . '" name="' . $name . '" value="' . $value . '" tabindex="3">';
                                $return .= '<label class="modal-radio" for="' . $lid . '">';
                                $return .= '<span> ' . ucwords($label) . '</span>';
                                $return .= '</label></div>';
                                return $return;
                            }
                        ]
                    );
                    echo "</div>";
                    foreach ($model3d->getActiveModel3dParts() as $model3dPart) {
                        if (!array_key_exists($model3dPart->id, $model3dEditForm->qty)) {
                            $model3dEditForm->qty[$model3dPart->id] = $model3dPart->qty;
                        }
                        echo sprintf("<div class='row js-model3dqty js-fileqty-%d' >", $model3dPart->file->id);
                        echo "<div class='col-xs-12 model-edit-quantity'>";
                        echo $form->field($model3dEditForm, 'qty[' . $model3dPart->id . ']')
                            ->textInput(
                                [
                                    'type'    => 'number',
                                    'min'     => 1,
                                    'max'     => 100,
                                    'pattern' => '[0-9]{2}',
                                    'class'   => 'form-control model-edit-quantity__input modelfileqty'
                                ]
                            )
                            ->label(_t('site.model3d', 'Quantity of "{title}" in KIT', ['title' => H($model3dPart->file->getFileName())]));
                        echo '<div class="model-edit-quantity__controls modelfileqtybtn"><a href="#" data-fileid="' . $model3dPart->file->id . '" class="tsi tsi-plus-c"></a> '
                            . ' <a href="#" data-fileid="' . $model3dPart->file->id . '" class="tsi tsi-minus-c"></a></div>';
                        echo '</div>';
                        echo '</div>';

                    }
                    ?>
                    <!-- colors & materials -->
                    <?php

                    if ($currentSelectedFileInfo instanceof Model3dBasePartInterface) {
                        $isCantSelectMaterial = true;
                        $currentTexture       = $currentSelectedFileInfo->getCalculatedTexture();
                    } elseif ($model3d->isOneTextureForKit()) {
                        $currentTexture       = $model3d->getKitTexture();
                        $isCantSelectMaterial = true;
                    } else {
                        $currentTexture       = Model3dTextureFactory::createModel3dTexture();
                        $isCantSelectMaterial = false;
                    }
                    echo ColorSelectorWidget::widget(
                        [
                            'isOneMaterialForKit'   => $model3d->isOneTextureForKit(),
                            'isCantSelectMaterial'  => $isCantSelectMaterial,
                            'isModelKit'            => $model3d->isKit(),
                            'selectedMaterialGroup' => $currentTexture->calculatePrinterMaterialGroup(),
                            'selectedColor'         => $currentTexture->printerColor,
                            'materialList'          => $groupMaterials,
                            'formName'              => 'Model3dEditForm'
                        ]
                    );
                    ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-sm-8 wide-padding wide-padding--right model-page__2ndcontainer">
            <?php if ($model3d->product_status === \common\modules\product\interfaces\ProductInterface::STATUS_PUBLISHED_PUBLIC): ?>
                <h2>
                    <?= _t('site.model3d', 'Embed this model onto your site') ?>
                </h2>
                <p><?php echo _t('site.model3d', 'Embedding is available only if your model is published.'); ?></p>
            <?php else: ?>

                <div class="model-edit-tips">
                    <h4 class="model-edit-tips__title">
                        <?= _t('site.model3d', 'Essential Tips for 3D designers') ?>:
                    </h4>
                    <ol class="model-edit-tips__list">
                        <li>
                            <?= _t(
                                'site.model3d',
                                'Always pay attention to the title, description, and tags of your model. They should reflect the main idea of your model. The title should be clear and self-explanatory.'
                            ) ?>
                        </li>
                        <li>
                            <?= _t(
                                'site.model3d',
                                'Choose the color and material that you recommend for the model. If you have a kit (a model that is assembled of several parts), set a material and color for each parts. For duplicate parts, upload only one file and set the quantity required for the kit.'
                            ) ?>
                        </li>
                        <li>
                            <?= _t('site.model3d', 'Set the lowest possible price. You can always increase it later.') ?>
                        </li>
                        <li>
                            <?= _t('site.model3d', 'Make sure you upload good quality photos of the model as a manufactured product.') ?>
                        </li>
                        <li>
                            <?= _t(
                                'site.model3d',
                                'To create a buzz for your model, share it on dedicated online platforms such as social medias, communities, and forums.'
                            ) ?>
                        </li>
                        <li>
                            <?= _t(
                                'site.model3d',
                                'For models that require post-processing, write a step by step set of instructions. You can also post it on popular sites adding links to your store.'
                            ) ?>
                        </li>
                        <li>
                            <?= _t(
                                'site.model3d',
                                'Use our store widget for your Treatstock models on your own site or blog, so visitors to your sites can order your designs as finished products.'
                            ) ?>
                        </li>
                    </ol>
                </div>
                <?php
                if ($model3d->isPublished()) {
                    ?>
                    <div class="model-embed-title" data-toggle="collapse" data-target="#model-embed"
                         aria-expanded="false" aria-controls="model-embed">
                        <div>
                            <?= _t('site.model3d', 'Embed this model onto your site') ?>
                        </div>
                        <span class="tsi tsi-up-c"></span>
                    </div>

                    <div class="model-embed embed-configurator collapse in" id="model-embed">
                        <?= \frontend\widgets\Model3dEmbedWidget::widget([
                            'model3d' => $model3d
                        ]); ?>
                    </div>
                <?php } ?>
            <?php endif; ?>
        </div>
    </div>
</div>
