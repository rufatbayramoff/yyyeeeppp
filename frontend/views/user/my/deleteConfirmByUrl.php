<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.05.18
 * Time: 17:18
 */

use frontend\models\user\UserDeleteRequestForm;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="row">
    <div class="col-sm-12 text-center">
        <h3><?= _t('user.profile', 'Attention! Your account will be permanently deleted.'); ?></h3>
        <?= _t('user.profile', 'Do you confirm the deletion of your Treatstock account?'); ?><br><br>
    </div>
</div>
<div class="row text-center">
    <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
        <?php $form = ActiveForm::begin(['action' => ['/my/profile/delete-confirm']]); ?>
        <?php echo Html::hiddenInput('UserProfileDeleteRequest[approve_code]', \H($approveCode));?>

        <?php
        echo $form->field($userDeactivatingLog, 'reason_id')->widget(Select2::class, [
            'data' => (new UserDeleteRequestForm)->getSuggestList(),
            'options' => [
                'placeholder' => _t('front.site', 'Select')
            ],
            'hideSearch' => true
        ]);

        echo $form
            ->field($userDeactivatingLog, 'reason_desc')
            ->textarea();
        ?>
        <?= Html::submitButton(_t('app', 'Delete account'), ['class' => 'btn btn-danger',]) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
