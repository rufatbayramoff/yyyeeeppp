<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 07.05.18
 * Time: 16:24
 */

use frontend\models\user\UserDeleteRequestForm;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var bool $confirmationCodeExists */
$confirmationCodeExists;

/** @var int $openOrdersCount */
/** @var int $allowClaimOrdersCount */
/** @var \frontend\models\user\UserDeleteRequestForm $userDeleteRequestForm */
/** @var \common\models\base\UserDeactivatingLog $userDeactivatingLog */
if ($currentUser->isEmptyEmail()) {
    ?>
    <div class="row">
        <div class="col-sm-12">
            <?= _t('user.profile', 'Please confirm your email at first.'); ?><br>
        </div>
    </div>
    <?php
} elseif ($openOrdersCount) {
    ?>
    <div class="row">
        <div class="col-sm-12">
            <?= _t('user.profile', 'You still have incomplete orders. Please complete them (or reject them) before deleting your account. Thank you.'); ?><br>
        </div>
    </div>
    <?php
} elseif ($allowClaimOrdersCount) {
    ?>
    <div class="row">
        <div class="col-sm-12">
            <?= _t('user.profile',
                'You have orders still awaiting the completion of the dispute periods.'); ?><br>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="m-t0"><?= _t('user.profile', 'Attention! Your account will be permanently deleted.'); ?></h3>
            <p>
                <?= _t('user.profile', 'All your personal data will be destroyed and you will not be able to recover it. '); ?>
                <?= _t('user.profile', 'By confirming the deletion of your account, you also confirm that you have no incomplete orders or pending earnings.'); ?>
                <?= _t('user.profile', 'You should confirm the deletion of your account by using the code sent to your email address.'); ?>
            </p>
        </div>
    </div>
    <?php if ($confirmationCodeExists) {
        $approveCodeForm = \Yii::createObject(\common\models\UserProfileDeleteRequest::class);
        ?>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                <?php $form = ActiveForm::begin(['action' => ['/my/profile/delete-confirm']]); ?>

                <?= $form
                    ->field($approveCodeForm, 'approve_code')
                    ->textInput()->label(_t('user.profile', 'Code')); ?>

                <?php
                echo $form->field($userDeactivatingLog, 'reason_id')->widget(Select2::class, [
                    'data'       => (new UserDeleteRequestForm)->getSuggestList(),
                    'options'    => [
                        'placeholder' => _t('front.site', 'Select')
                    ],
                    'hideSearch' => true
                ]);

                echo $form
                    ->field($userDeactivatingLog, 'reason_desc')
                    ->textarea(['rows' => '3']);
                ?>

                <?= Html::submitButton(_t('app', 'Approve and delete account'), ['class' => 'btn btn-danger btn-block']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <br>
                <?= Html::button(_t('app', 'Resend code to email'), [
                    'class'      => 'btn btn-default ts-ajax',
                    'value'      => Url::toRoute('/my/profile/delete-request?deleteSubmit=1'),
                    'jsCallback' => 'window.location.reload()'
                ]) ?>
            </div>
        </div>
        <?php
    } else {
        ?>

        <div class="row">
            <div class="col-sm-12 text-center">
                <br>
                <?= Html::button(_t('app', 'Send code to email'), [
                    'class'      => 'btn btn-danger ts-ajax',
                    'value'      => Url::toRoute('/my/profile/delete-request?deleteSubmit=1'),
                    'jsCallback' => 'window.location.reload()'
                ]) ?>
            </div>
        </div>
        <?php
    }
}
?>