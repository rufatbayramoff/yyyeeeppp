<?php

use common\components\JsObjectFactory;
use frontend\assets\ImageCropClassAsset;
use yii\widgets\ActiveForm;
use frontend\assets\ImageCropAsset;

$width = 720;
$height = 540;
$defaultWidth = 800;
$defaultHeight = 600;
$imageSizes = getimagesize($imgPath);
$imgWidth = $imageSizes[0];
$imgHeight = $imageSizes[1];


if ($imgHeight > $imgWidth - 200) {
    $style = "height: {$defaultHeight}px";
    $k = $defaultHeight / $imgHeight;
} else {
    $style = "width: {$defaultWidth}px";
    $k = $defaultWidth / $imgWidth;
}
$x2 = 400 * $k;
$y2 = 300 * $k;
$ajaxSubmit = empty($submitUrl) ? "ts-ajax-submit" : "";
$submitUrl = !empty($submitUrl) ? $submitUrl : Yii::$app->urlManager->createUrl(['my/model/crop-submit/' . $img->id]);
ImageCropClassAsset::register($this);
JsObjectFactory::createJsObject(
    'imageCropClass',
    'imageCropObj',
    [
        'imgId' => 'js-image-crop'
    ]
    ,
    $this
)

?>
    <div class="form-message" style="padding:10px;"></div>
    <div>
        <img src='<?php echo $imgUrl; ?>' id='js-image-crop' style='max-width:100%;min-height:400px;max-height:600px;'/>
    </div>
<?php
$form = ActiveForm::begin(
    [
        'action'  => $submitUrl,
        'options' => ['id' => 'crop_form'],
    ]
);
?>
    <div style='display:none'>
        <input type='text' name='clientWidth' value='<?php echo $defaultWidth; ?>'/>
        <div>x1 <input type="text" id="x1" value="" name="x1"></div>
        <div>y1 <input type="text" id="y1" value="" name="y1"></div>
        <div>x2 <input type="text" id="x2" value="" name="x2"></div>
        <div>y2 <input type="text" id="y2" value="" name="y2"></div>
        <div>w <input type="text" id="w" value="" name="w"></div>
        <div>h <input type="text" id="h" value="" name="h"></div>
        <div>h <input type="text" id="rotate" value="" name="rotate"></div>
        <div>h <input type="text" id="scaleX" value="" name="scaleX"></div>
        <div>h <input type="text" id="scaleY" value="" name="scaleY"></div>
        <div>w <input type="text" value="<?php echo $imgWidth; ?>" name="width"></div>
        <div>h <input type="text" value="<?php echo $imgHeight; ?>" name="height"></div>
    </div>
    <div style="margin-top:10px;">
        <input type="button" class="btn btn-default" onclick="imageCropObj.rotateLeft();" value="<?= _t('site.model3d', 'Rotate 90 degrees left') ?>">
        <input type="button" class="btn btn-default" onclick="imageCropObj.rotateRight();" value="<?= _t('site.model3d', 'Rotate 90 degrees right') ?>">
        <input type="button" class="btn btn-default" onclick="imageCropObj.flipHorizontal();" value="<?= _t('site.model3d', 'Flip horizontally') ?>">
        <input type="button" class="btn btn-default" onclick="imageCropObj.flipVertical();" value="<?= _t('site.model3d', 'Flip vertically') ?>">
        <input type="button" class="btn btn-default" onclick="imageCropObj.resetImage();" value="<?= _t('site.model3d', 'Reset') ?>">
        <br><br>
        <input type="submit" class="btn btn-default <?= $ajaxSubmit; ?>" onDblClick="" value="Save Crop">
    </div>
<?php ActiveForm::end(); ?>
