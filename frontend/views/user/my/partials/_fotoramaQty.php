<div class="row js-model3dqty js-fileqty-{{model.fileId}}" style="opacity: 1; display: none;">
    <div class="col-xs-12 model-edit-quantity">
        <div class="form-group field-model3deditform-qty-{{model.fileId}}">
            <label class="control-label" for="model3deditform-qty-{{model.fileId}}">Quantity of {{model.result.fileTitle}} in KIT</label>
            <input type="number" id="model3deditform-qty-{{model.fileId}}"
                   class="form-control model-edit-quantity__input modelfileqty" name="Model3dEditForm[qty][{{model.result.part_id}}]" value="1" min="1" max="100" pattern="[0-9]{2}">
            <p class="help-block help-block-error"></p>
        </div>
        <div class="model-edit-quantity__controls modelfileqtybtn">
            <a href="#" data-fileid="{{model.fileId}}" class="tsi tsi-plus-c fotorama-ignore-click"></a>
            <a href="#" data-fileid="{{model.fileId}}" class="tsi tsi-minus-c"></a>
        </div>
    </div>
</div>