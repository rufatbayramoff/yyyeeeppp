<div
        data-img="{{ model.fileUrl }}"
        data-type="{{model.fileType}}"
        data-id="{{model.id}}"
        data-caption="{{model.sizes || _t('site.model3d', 'Calculating...')}}"
        data-file-id="{{model.fileId}}"
        data-model3did="{{model.model3did}}"
        data-model-part-id="{{model.result.part_id || ''}}"
>
    <div class="model-slider__controls">
        <div class="js-add-image fotorama-ignore-click dz-clickable model-slider__controls__item">
            <span class="tsi tsi-plus"> </span>
            <span class="model-slider__controls__item__hint">Add</span>
        </div>
        <div ng-if="model.fileType === 'image'" class="js-set-cover fotorama-ignore-click model-slider__controls__item">
            <span class="tsi tsi-checkmark fotorama-ignore-click"></span>
            <span class="model-slider__controls__item__hint">Set as cover model</span>
        </div>
        <div class="js-remove-file fotorama-ignore-click model-slider__controls__item">
            <span class="tsi tsi-bin fotorama-ignore-click"> </span>
            <span class="model-slider__controls__item__hint">Remove</span>
        </div>
        <div ng-if="model.fileType === 'image'" class="js-crop-image fotorama-ignore-click model-slider__controls__item">
            <span class="tsi tsi-crop fotorama-ignore-click"></span>
            <span class="model-slider__controls__item__hint">Crop model</span>
        </div>
    </div>
</div>