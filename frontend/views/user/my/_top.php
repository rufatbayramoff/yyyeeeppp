<?php


use frontend\models\user\UserFacade;

Yii::$app->angular
    ->controller('profile/header')
    ->service(['notify', 'modal', 'router', 'user']);

\frontend\assets\ImageCropAsset::register($this);


$this->params['fluid-layout'] = true;


$user = \frontend\models\user\UserFacade::getCurrentUser();

$userLinks = \frontend\models\user\UserFacade::getUserLinks($user);
$currentUrl = $this->context->id;
foreach ($userLinks as $k => $v) {
    if ($currentUrl == str_replace("@web/", "", $v['url'])){
        $userLinks[$k]['active'] = true;
    }
}
if(empty($this->title))
    $this->title = _t('front.my', 'User profile');

$fullName = H(UserFacade::getFormattedUserName($user, true));

$avatarUrl = '';
$bgUrl = frontend\components\UserUtils::getCover($user->userProfile->background_url);
if(empty($bgUrl)){
    $bgUrl = '/static/images/profile-default-bg.jpg';
}
$avatar = frontend\components\UserUtils::getAvatar($user->userProfile->avatar_url, $user->email, 'profile', true);

$ps = \common\models\Ps::findOne(['user_id'=>$user->id]);
$printersCount = 0;
$storeModelsCount = 0;
if($ps){
    $subTitle =  $ps->title;
    $printersCount = $ps->getNotDeletedPrinters()->count();
}
if(\frontend\models\user\UserFacade::hasStore($user)){
    $storeModelsCount = \common\models\Model3d::find()->myStore($user->id)->count();
}
?>

<div class="user-profile" ng-controller="ProfileHeaderController">

    <div class="user-profile__cover" style="background-image: url('<?php echo HL($bgUrl); ?>')" title="<?= _t('front.my', 'Profile image')?>"></div>

    <div class="container relative">
        <div class="row">
            <div class="col-sm-9">
                <div class="user-profile__userinfo">
                    <a class="user-profile__avatar" href="<?php echo yii\helpers\Url::toRoute(['/user/user-profile/index']); ?>">
                        <?php echo $avatar; ?>
                    </a>
                    <div class="user-profile__username">
                        <?php echo $fullName; ?>
                    </div>
                    <div class="user-profile__user-rating">
                        <?php if(false): #turned off rating ?>
                            <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                            <div class="user-profile__user-rating-count">239</div>
                        <?php endif; ?>
                    </div>
                    <div class="user-profile__btns">
                        <div class="user-profile__send-msg">
                            <a class="btn btn-primary" href="/workbench/messages"><span class="tsi tsi-message"></span></a>
                        </div>

                        <div class="user-profile__nav user-profile__nav--desktop btn-group">

                            <?php
                            $menu = [
                                [
                                    'link' =>  yii\helpers\Url::toRoute(['/user/user-profile/index']),
                                    'title' => _t('front.my', 'Profile'),
                                    'badge' => false,
                                    'display' => true
                                ],
                                [
                                    'link' =>  yii\helpers\Url::toRoute(['/mybusiness/company']),
                                    'title' => _t('front.my', 'My Services'),
                                    'badge' => $printersCount,
                                    'display' => $printersCount>0
                                ],
                                [
                                    'link' =>  yii\helpers\Url::toRoute(['/my/store']),
                                    'title' => _t('front.my', 'My Store'),
                                    'badge' => $storeModelsCount,
                                    'display' => $storeModelsCount > 0
                                ]
                            ];
                            $r = '/'. Yii::$app->request->getPathInfo();

                            $data = [];
                            foreach($menu as $menuItem):
                                $menuActive = $menuItem['link']===$r ? 'active' : '';
                                if(empty($menuItem['display'])){ continue; }
                                $data[$menuItem['link']] = $menuItem['title'];
                                ?>
                                <a href="<?=$menuItem['link']; ?>" class="btn btn-default <?=$menuActive; ?>">
                                    <?php echo $menuItem['title']; ?>
                                    <?php if(!empty($menuItem['badge'])): ?>
                                        <span class="user-profile__nav-badge"><?php echo $menuItem['badge']; ?></span>
                                    <?php endif; ?>
                                </a>
                            <?php endforeach; ?>

                        </div>

                        <div class="user-profile__nav user-profile__nav--mobile">
                            <?php
                            echo kartik\select2\Select2::widget([
                                'name' => 'user-profile__nav--mobile',
                                'hideSearch' => true,
                                'value' => $r,
                                'data' => $data,
                                'options' => ['multiple' => false, 'class' => 'user-profile__nav-select'],
                                'pluginEvents' => [
                                    'change' => 'function(t){ location.href = t.target.value; }'
                                ],
                                'pluginOptions' => [
                                    'dropdownCssClass' => 'user-profile__nav-dropdown'
                                ],
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        echo yii\helpers\Html::a(_t('front.user', '<span class="tsi tsi-picture"></span>'),
            '@web/user-profile/change-cover', [
                'title' => _t('front.user', 'Change cover picture'),
                'class' => 'ts-ajax-modal change-cover user-profile__controls__item user-profile__change-cover-btn',
                'data-target' => '#modal_uploadcover',
                'data-toggle' => 'tooltip',
                'data-placement' => 'bottom',
                'data-original-title' => _t('front.user', 'Change cover')
            ]
        );
        ?>

        <div
            class="user-profile__controls__item user-profile__change-avatar-btn"
            ng-click="showChangeAvatarModal()"
            data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?= _t('front.user', 'Change avatar')?>">
            <span class="tsi tsi-camera"></span>
        </div>

        <?php
        echo yii\helpers\Html::a(_t('front.user', '<span class="tsi tsi-lock"></span>'),
            '@web/user-profile/change-password', [
                'class' => 'user-profile__controls__item user-profile__change-pass-btn',
                'data-toggle' => 'tooltip',
                'data-placement' => 'bottom',
                'data-original-title' => _t('front.user', 'Change password')
            ]
        );
        ?>

    </div>
</div>




<script type="text/ng-template" id="/app/profile/change-avatar-modal.html">

    <div class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= _t('front.user', 'Change profile picture')?></h4>
                </div>
                <div class="modal-body">

                    <div ng-if="!form.isAvatarLoaded" class="text-center m-t30 m-b30">
                        <input type="file" class="inputfile" id="avatar-file"
                               accept="image/jpeg,image/png,image/gif"
                               onchange="angular.element(this).scope().onAvatarFileChange(event)">
                        <label for="avatar-file">
                            <strong>
                                <i class="tsi tsi-upload-l"></i> <?= _t('site.profile', 'Choose a file'); ?>
                            </strong>
                        </label>
                    </div>

                    <div><?=_t('front.user', 'Please upload pictures with a size that is not less than 100x100 pixels. Max size of file is 1.5 MB');?></div>

                    <div ng-if="form.isAvatarLoaded">

                        <div class="row avatar-preview">
                            <div class="col-md-6 m-b20">
                                <img src="" id="crop-image" class="avatar-preview__crop-img">
                            </div>

                            <div class="col-md-6">

                                <h4 class="avatar-preview__title"><?=_t('site.profile', 'Preview of profile picture')?></h4>

                                <div class="avatar-preview__block">
                                    <div style="width: 100px; height: 100px;" class="img-circle avatar-preview__img"></div>
                                    <div style="width: 60px; height: 60px;" class="img-circle avatar-preview__img"></div>
                                    <div style="width: 34px; height: 34px;" class="img-circle avatar-preview__img"></div>
                                </div>

                                <button
                                    type="button"
                                    class="btn btn-primary"
                                    loader-click="saveAvatar()">
                                    <?= _t('site.profile', 'Save profile picture')?>
                                </button>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</script>