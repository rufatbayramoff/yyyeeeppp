<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = _t('site.user', 'Sign in by e-mail');
$confirmed = app('request')->get('confirmed', false);
/* @var $this yii\web\View */
/* @var $emailLogin common\models\UserEmailLogin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-login">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 panel box-shadow m-t30">
                <div class="user-email-login text-center p-b30">
                    <h2><?=$this->title;?></h2>
                    <?php if (!empty($hash)) { ?>
                        <br /> <br />
                        <p class="text-center">
                            <?php
                            echo _t('site.user','By using this Service, you agree to our Terms and that you have read our Privacy Policy, including our Cookie use.');
                            echo Html::a(
                                    _t('site.user','Sign UP'),
                                    Url::to(['/user/email-login','hash' => $hash,'confirmed' => 'true']),
                                    [
                                            'data-method' => 'POST',
                                            'class' => 'btn btn-primary',
                                            'data-params' => [
                                                'agree' => true
                                            ]
                                    ]);
                            ?>
                            </p>
                        <?php
                    } else {
                    $form = ActiveForm::begin([
                        'layout' => 'inline',
                        'id' => 'user-change-email-form'
                    ]);
                    echo $form->field($emailLogin, 'requested_url')->hiddenInput()->label(false);
                    ?>
                    <p><i class="tsi tsi-mail text-success" style="font-size: 64px;"></i></p>
                    <p><?=_t('site.user', 'Get a link sent to your email that will sign you in instantly!'); ?></p>
                    <div class="form-group">
                        <label class="control-label p-r10">
                            <?=_t('site.user', 'E-mail'); ?>:
                        </label>
                        <?= $form->field($emailLogin, 'email')->textInput() ?>
                    </div>

                    <?= Html::submitButton(_t('site.user', 'Send sign-in link'),
                        ['class' => 'btn btn-success m-t20', 'name' => 'submit-button']) ?>
                    <?php ActiveForm::end(); ?>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (!empty($hash) && !$confirmed) {
    $this->registerJs('function doredir(){ document.location.href="/user/email-login?hash=' . $hash . '&confirmed=true"; }; setTimeout(function(){doredir();},2000);');
}