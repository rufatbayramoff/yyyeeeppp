<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\user\ResetPasswordForm */

$this->title = _t('front.site', 'Reset password');
#$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-reset-password">
<div class="container">
    <h1><?= \H($this->title) ?></h1>

    <p><?php echo _t('front.site', 'Please choose your new password:'); ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <div class="form-group">
                    <?= Html::submitButton(_t('site.ps', 'Save'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>