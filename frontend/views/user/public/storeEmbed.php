<?php
/**
 * @var \common\models\Model3d[] $storeUnits
 */

use common\components\JsObjectFactory;
use frontend\components\UserUtils;
use frontend\widgets\assets\StoreUnitWidgetAssets;
use yii\widgets\ListView;

$mode        = 'view';
$this->title = $title;

$this->beginPage();
$embiedCssUrl = '/css/embed.css?v=' . @filemtime(Yii::getAlias('@webroot/css/embed.css'));
$categoryId   = $currentCategory ? $currentCategory->id : '';

JsObjectFactory::createJsObject(
    'model3dRenderRouteClass',
    'model3dRenderRoute',
    [
        'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl . '/'
    ],
    $this
);

StoreUnitWidgetAssets::register($this);
$listView = Yii::createObject([
    'class'        => ListView::class,
    'dataProvider' => $dataProvider,
    'itemView'     => 'modelListItem',
    'pager'        => ['linkOptions' => ['target'=>'_self']],
    'viewParams'   => []
]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $this->title; ?></title>

    <link rel="canonical" href="//www.treatstock.com/<?= UserUtils::getUserStoreUrl($userModel); ?>">
    <link href="<?= $embiedCssUrl ?>" rel="stylesheet">
    <base target="_blank">
</head>

<body class="ts-embed ts-embed--store">
<div class="ts-e-store-list">
    <?= $listView->renderItems() ?>
    <div class="row">
        <?= $listView->renderPager() ?>
    </div>
</div>
</body>
</html>

