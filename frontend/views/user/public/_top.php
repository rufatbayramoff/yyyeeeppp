<?php

use common\models\message\helpers\UrlHelper;
use frontend\components\UserUtils;
use frontend\models\model3d\Model3dFacade;
use frontend\models\ps\PsFacade;
use yii\helpers\Url;

$this->params['fluid-layout'] = true;

/** @var \common\models\User $userModel */
$userProfile = $userModel->userProfile;
$userLink = \frontend\components\UserUtils::getUserPublicProfileUrl($userModel);

$fullName = H(\frontend\models\user\UserFacade::getFormattedUserName($userModel, true));

$avatarUrl = '';
$bgUrl = frontend\components\UserUtils::getCover($userProfile->background_url);
if (empty($bgUrl)) {
    $bgUrl = '/static/images/profile-default-bg.jpg';
}
$avatar = frontend\components\UserUtils::getAvatar($userProfile->avatar_url, $userModel->email);
$subTitle = '';
$ps = \common\models\Ps::findOne(['user_id' => $userModel->id]);
$printersCount = 0;
$storeModelsCount = 0;
if ($ps) {
    $subTitle = $ps->title;
    $printersCount = $ps->getNotDeletedPrinters()->visible()->moderated()->count();
}
if (\frontend\models\user\UserFacade::hasStore($userModel)) {
    $storeModelsCount = \common\models\Model3d::find()->storePublished($userModel)->count();
}
$sendMessageUrl = Url::toRoute(
    array_merge(
        UrlHelper::personalMessageRoute($userModel),
        ['utm_source' => 'treatstock', 'utm_medium' => 'public-profile', 'utm_campaign' => 'hire_designer', 'utm_content' => 'send_msg']
    )
);
?>
<div class="user-profile">

    <div class="user-profile__cover" style="background-image: url('<?php echo HL($bgUrl); ?>')" title="<?= _t('front.my', 'Profile image') ?>"></div>

    <div class="container relative">
        <div class="row">
            <div class="col-sm-9">
                <div class="user-profile__userinfo">
                    <a class="user-profile__avatar" href="<?= $userLink ?>">
                        <?php if($ps): ?>
                        <img src="<?php echo $ps->getCircleImageByPs($ps, 100,100); ?>" alt="<?= H($ps->title); ?> Logo">
                        <?php else:?>
                        <?php echo $avatar; ?>
                        <?php endif; ?>
                    </a>
                    <div class="user-profile__username">
                        <?=$fullName;?>
                    </div>

                    <?php /*
                    <div class="user-profile__btns">
                        <div class="user-profile__send-msg">
                            <a class="btn btn-primary ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner" href="<?php echo yii\helpers\Url::toRoute(
                                array_merge(UrlHelper::personalMessageRoute($userModel), ['utm_source'=>'treatstock', 'utm_medium'=>'public-profile', 'utm_campaign'=>'hire_designer', 'utm_content'=>'send_msg'])); ?>"><span class="tsi tsi-message"></span></a>
                        </div>

                        <div class="user-profile__nav user-profile__nav--desktop btn-group">
                            <?php
                            $menu = [
                                [
                                    'link' => $userLink,
                                    'title' => _t('front.my', 'About'),
                                    'badge' => false,
                                    'display' => true
                                ],
                                [
                                    'link' =>  '/',
                                    'title' => _t('front.my', 'Reviews'),
                                    'badge' => $printersCount,

                                ],
                                [
                                    'link' => $userModel->ps ? PsFacade::getPsLink($userModel->ps[0]) : '/',
                                    'title' => _t('front.my', '3D Printing'),
                                    'badge' => $printersCount,
                                    'display' => $printersCount>0
                                ],
                                [
                                    'link' => UserUtils::getUserStoreUrl($userModel),
                                    'title' => _t('front.my', '3D Designing'),
                                    'badge' => $storeModelsCount,
                                    'display' => $storeModelsCount > 0
                                ]
                            ];
                            $r = '/'. Yii::$app->request->getPathInfo();

                            $data = [];
                            foreach($menu as $menuItem):
                                $menuActive = $menuItem['link']===$r ? 'active' : '';
                                if(empty($menuItem['display'])){ continue; }
                                $data[$menuItem['link']] = $menuItem['title'];
                                ?>
                                <a href="<?=$menuItem['link']; ?>" class="btn btn-default <?=$menuActive; ?>">
                                    <?php echo $menuItem['title']; ?>
                                    <?php if(!empty($menuItem['badge'])): ?>
                                        <span class="user-profile__nav-badge"><?php echo $menuItem['badge']; ?></span>
                                    <?php endif; ?>
                                </a>
                            <?php endforeach; ?>
                        </div>

                        <div class="user-profile__nav user-profile__nav--mobile">
                            <?php
                            echo kartik\select2\Select2::widget([
                                'name' => 'user-profile__nav--mobile',
                                'hideSearch' => true,
                                'value' => $r,
                                'data' => $data,
                                'options' => ['multiple' => false, 'class' => 'user-profile__nav-select'],
                                'pluginEvents' => [
                                    'change' => 'function(t){ location.href = t.target.value; }'
                                ],
                                'pluginOptions' => [
                                    'dropdownCssClass' => 'user-profile__nav-dropdown'
                                ],
                            ]);
                            ?>
                        </div>
                    </div>
                    */?>
                </div>
            </div>

            <div class="col-sm-3">

            </div>
        </div>

    </div>
</div>