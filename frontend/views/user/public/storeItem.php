<div class="model-card">
    <div class="model-card__pic">
        <div class="model-card__like  is-liked js-store-item-like">
            <div class="model-card__like-count">2k</div>
            <div class="model-card__like-icon"></div>
        </div>
        <a class="model-card__link" href="/3dmodels/2" title="35mm spacer">
            <img class="model-card__img" src="/static/images/cat1.png" alt="35mm spacer">
        </a>
    </div>

    <div class="model-card__footer">
        <a class="model-card__title" href="#go-to-model">Darth Vader Pencil Holder Tank Some Long Model Name</a>
        <div class="model-card__price">
            <div class="model-card__price-label">From</div>
            $1,426.00
        </div>

        <?php if(false): #TODO, rating and shipping? ?>
            <div class="model-card__rating">
                <input value="4" type="number" class="star-rating" min=0 max=5 step=1 data-size="xs" data-symbol="&#xea18;" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                <div class="model-card__rating-count">
                    359
                </div>
            </div>
            <div class="model-card__shipping">
                free shipping avail
                <span class="tsi tsi-truck"></span>
            </div>
        <?php endif; ?>
    </div>
</div>