<?php
/** @var \yii\web\View $this  **/
/** @var \common\models\UserCollection[] $items */
/** @var array $params */

use frontend\components\UserUtils;
use frontend\models\model3d\CollectionFacade;
use yii\helpers\Html;
use frontend\components\image\ImageHtmlHelper;

$this->registerAssetBundle(\frontend\assets\CollectionAsset::class);
$fullName = H(\frontend\models\user\UserFacade::getFormattedUserName($user, true));

$this->title = _t('front.model', '3D Models Collection - {username}', ['username'=>$fullName]);
$this->params['page-class'] = 'js-collection-list';

$items = $params['items'];

echo $this->renderFile('@app/views/user/public/_top.php', $params);

?>

<div class="<?= $this->params['page-class']?>">

    <div class="nav-tabs__container">
    <div class="container">
    <div class="row"> 
    <h2><?= _t('front.model', 'Collections by {username}', ['username' => $fullName]); ?></h2>
<?php 
foreach($items as $collection):
    $coverImg = CollectionFacade::getCollectionCover($collection->id, $collection->userCollectionModel3ds);
    $total =  count($collection->userCollectionModel3ds);
    $collectionLink = UserUtils::getUserCollectionRoute($collection);
?>
    <div class='col-sm-6 col-md-4'>
        <div
            class='catalog-item collection-item js-collection'
            data-id="<?php echo $collection->id; ?>"
            data-size="<?=$total?>"
            data-title="<?=  \H($collection->title);?>"
            >
            <div class='catalog-item__pic'>
                <div class="catalog-item__pic__img">
                    <div class="catalog-item__pic__img__fotorama js-catalog-item-slider">

                        <?php foreach($coverImg as $img):
                            $img = ImageHtmlHelper::getThumbUrl($img, ImageHtmlHelper::IMG_CATALOG_WIDTH, ImageHtmlHelper::IMG_CATALOG_HEIGHT);
                        ?>

                            <?= Html::beginTag('div', ['data-img' => $img]); ?>
                                <?= Html::a('', $collectionLink)?>

                            <?= Html::endTag('div'); ?>

                        <?php endforeach; ?>

                    </div>

                </div>
            </div>        
            <div class='catalog-item__footer'>
                <div class='catalog-item__footer__price'>  <?php 
                  if(empty($collection->is_visible)){
                            echo sprintf("<span class='tsi tsi-lock'>%s</span> ", _t("site.collection", ""));
                      }
                      ?>
                    <?php echo _t('front.model3d', '{n, plural, =0{empty} =1{1 item} other{# items}}', ['n'=>$total]); ?>
                </div>

                <h4 class='catalog-item__footer__title'>
                    <?= Html::a(\H($collection->title), $collectionLink); ?>
                </h4>
            </div>
            <div class="collection-item__decor-1"></div>
            <div class="collection-item__decor-2"></div>
            <div class="collection-item__decor-3"></div>
        </div>
    </div>    
<?php endforeach; ?>
</div>
</div>
    </div>
</div>
     