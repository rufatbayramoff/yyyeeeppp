<?php
/** @var \yii\web\View $this **/
/** @var \common\models\base\UserCollectionModel3d[] $items **/
/** @var common\models\base\UserCollection $collection **/
/** @var array $params **/

use common\services\LikeService;
use frontend\assets\CollectionAsset;
use frontend\components\UserSessionFacade;
use frontend\models\model3d\Model3dFacade;
use frontend\components\image\ImageHtmlHelper;
use frontend\models\store\StoreFacade;
use yii\helpers\Html;
use yii\helpers\StringHelper;

$items = $params['items'];
$collection = $params['collection'];
$fullName = H(\frontend\models\user\UserFacade::getFormattedUserName($params['userModel'], true));

$this->title = _t('site.seo', '{collection} by {username} #{collectionId}', ['collection'=>\H($collection->title), 'username'=>$fullName , 'collectionId'=>$collection->id]);

$this->registerAssetBundle(CollectionAsset::class);
echo $this->renderFile('@app/views/user/public/_top.php', $params);

$this->params['page-class'] = 'js-collection-view';
?>
<div class="container">


    <div class='row collection'>
        <?php

        if (empty($collection->is_visible)) {
            echo "<div class='col-sm-6 col-md-4'> <h2>";
            echo _t("site.collection", "This collection is private.");
            echo "</h2></div>";
        } else {
            ?>
            <div class="col-xs-12">
                <h2><?= \H($collection->title); ?></h2>
            </div>
            <?php
            if (empty($items)) {
                echo "<div class='col-sm-6 col-md-4'>";
                echo '<p>';
                echo _t("site.collection", "This collection is empty.");
                echo '</p>';
                echo "</div>";
            }
            foreach ($items as $k => $item):
                $model3d = $item->model3d;
                $likesCount = LikeService::getModelLikesCount($model3d);
                ?>
                <div class='col-sm-6 col-md-4'>
                    <div class='catalog-item' data-collection-item-id="<?= $item->id ?>">

                        <div class='catalog-item__pic'>


                            <?php
                            $itemTitle = StringHelper::truncate(str_replace("_", " ", $model3d->title), 55);
                            $itemTitle = \H($itemTitle);
                            $coverImage = Model3dFacade::getCover($model3d);
                            if ($coverImage) {
                                $coverImage['image'] = ImageHtmlHelper::getThumbUrl(
                                    $coverImage['image'],
                                    ImageHtmlHelper::IMG_CATALOG_WIDTH,
                                    ImageHtmlHelper::IMG_CATALOG_HEIGHT
                                );
                                $img = Html::img($coverImage['image'], ['alt' => $itemTitle]);
                                echo Html::a($img,  Model3dFacade::getStoreUrl($model3d), ['title' => $itemTitle, 'class' => 'catalog-item__pic__img']);
                            }

                            ?>
                        </div>

                        <div class="catalog-item__footer">
                            <h4 class="catalog-item__footer__title">
                                <?php echo Html::a($itemTitle,   Model3dFacade::getStoreUrl($model3d)); ?>
                            </h4>

                            <div class="catalog-item__footer__price">
                                <?=displayAsMoney($model3d->getPriceMoneyByQty(1));?>
                            </div>

                            <div class="catalog-item__footer__stats">

                                <?php /*    <div class="catalog-item__footer__stats__reviews">
                        <span class="tsi tsi-comment-o"></span> 0 reviews
                    </div> */
                                ?>

                                <div class="catalog-item__footer__stats__likes js-hide-like-if-not-liked" data-likes="<?=$likesCount?>">
                                    <span class="tsi tsi-heart"></span> <span class="js-store-item-likes-count"><?= LikeService::getLikesCountString($likesCount) ?></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endforeach;

        } ?>

    </div>
</div>