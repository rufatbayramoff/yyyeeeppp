<?php

use frontend\modules\preorder\components\PreorderUrlHelper;
use frontend\widgets\StoreUnitWidget;
use frontend\components\UserUtils;
use frontend\models\user\UserFacade;
use frontend\widgets\ReviewStarsWidget;
use frontend\widgets\SwipeGalleryWidget;

/* @var $userModel common\models\User */
/* @var $storeUnits \common\models\StoreUnit[] */
/** @var array $params */
/** @var \common\models\StoreOrderReview $review */

$userModel = $params['user'];
$userProfile = $userModel->userProfile;
echo $this->renderFile('@app/views/user/public/_top.php', ['userModel' => $userModel]);
$this->title = _t('site.seo', '{username} at Treatstock', ['username' => $userModel->username]);
$currentUser = \frontend\models\user\UserFacade::getCurrentUser();

//$userModel = $params['userModel'];
$ps = \common\models\Ps::findOne(['user_id' => $userModel->id]);

$reviews =  \common\models\StoreOrderReview::find()
    ->joinWith('order')
    ->where([\common\models\StoreOrder::column('user_id') => $userModel->id])
    ->isCanShow()
    ->orderBy('id DESC')
    ->limit(100)
    ->all();

?>


<div class="container">
    <div class="row">

        <div class="col-sm-4 sidebar">

            <div class="panel panel-default m-t20">
                <?php
                $attributes = [];

                if (!empty($userModel->created_at)):
                    $attributes = [
                        [
                            'value' => app('formatter')->asDate($userModel->created_at, 'MMMM Y'),
                            'label' => _t('front.profile', 'Joined on')
                        ]
                    ];
                endif;
                if (false && $userProfile->website) { // hide for now
                    $attributes[] = [
                        'format' => 'raw',
                        'value' => yii\helpers\Html::a(
                            H($userProfile->website),
                            '#',
                            ['target' => '_blank', 'data-go' => H($userProfile->website), 'class' => 'externallink']
                        ),
                        'label' => _t('front.profile', 'Website')
                    ];
                }

                echo \yii\widgets\DetailView::widget(
                    [
                        'model' => $userProfile,
                        'attributes' => $attributes,
                    ]
                );
                ?>
            </div>

            <?php
            $userStatObj = \frontend\models\user\UserFacade::getUserStatistics($userModel);
            if ($userStatObj) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo _t('front.site', 'User statistics'); ?></div>
                    <?php
                    $statFields = [
                        'downloads',
                        'prints',
                        'favorites',
                        'views',
                    ];
                    foreach ($statFields as $k => $statField) {
                        if ($userStatObj->$statField <= 0) {
                            unset($statFields[$k]);
                        }
                    }
                    $onlineStatus = \frontend\models\user\UserFacade::getUserOnlineStatus($userStatObj);
                    $statFields[] = [
                        'attribute' => _t('front.site', 'Status'),
                        'format' => 'raw',
                        'value' => $onlineStatus
                    ];
                    echo \yii\widgets\DetailView::widget(
                        [
                            'model' => $userStatObj,
                            'attributes' => $statFields,
                            'options' => ['class' => 'table table-striped detail-view']

                        ]
                    );
                    ?>

                </div>
            <?php } ?>

            <?php if ($currentUser && $currentUser->company && $currentUser->company->canMakeOffer() && ($userModel->allow_incoming_quotes)) {
                ?>
                <a class="btn btn-block btn-danger public-profile__msg-btn"
                   href="<?= HL(PreorderUrlHelper::getCreateOfferUrl($userModel)); ?>"><?= _t('site.ps', 'Make offer'); ?></a>
            <?php } ?>

            <?php $collections = frontend\models\model3d\CollectionFacade::getUserCollections($userModel->id);
            if (!empty($collections)) {
                echo yii\helpers\Html::a(
                    '<span class="tsi tsi-stack"></span> ' . _t('front.site', 'User Collections'),
                    \frontend\components\UserUtils::getUserCollectionsRoute($userModel),
                    ['class' => 'btn btn-block btn-primary public-profile__collections-btn']
                );
            } ?>
        </div>

        <div class="col-sm-8 wide-padding wide-padding--left">

            <?php if (!empty($userProfile->info_about)): ?>
                <h1 class="page-header"><?php echo _t('front.user', 'About '); ?><?php echo \frontend\models\user\UserFacade::getFormattedUserName($userModel, true); ?></h1>
                <div class="public-profile__about">
                    <?= frontend\components\StoreUnitUtils::displayMore($userProfile->info_about); ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($userProfile->info_skills)): ?>
                <h2 class="page-header m-t30"><?php echo _t('front.user', 'Skills and professional achievements'); ?></h2>
                <div class="public-profile__skills">
                    <?php echo nl2br(\H($userProfile->info_skills)); ?>
                </div>
            <?php endif; ?>

            <?php if (count($reviews)): ?>
                <h2 class="page-header m-t30"><?= _t('site.user', 'User order reviews'); ?></h2>

            <?php endif; ?>
            <?php foreach ($reviews as $review): ?>

                <?php
                $printer = '-';
                if ($review->order->currentAttemp) {
                    $orderAttemp = $review->order->currentAttemp;
                    if ($orderAttemp->machine) {
                        $printer = $orderAttemp->machine->getTitleLabel();
                    }
                }
                if (!$review->order->getFirstItem() || !$review->order->getFirstItem()->model3dReplica) {
                    continue;
                }

                $model3dReplica = $review->order->getFirstItem()->model3dReplica;
                $material = $color = null;
                if ($model3dReplica && $model3dReplica->isOneTextureForKit()) {
                    $material = $model3dReplica->model3dTexture->printerMaterial;
                    $color = $model3dReplica->model3dTexture->printerColor;
                }
                ?>

                <div class="ps-profile-user-review">
                    <div class="ps-profile-user-review__user-bar">
                        <div class="ps-profile-user-review__user-info">
                            <?php $userProfileLink = $link = UserUtils::getUserPublicProfileUrl($review->order->user); ?>
                            <a href="<?= $userProfileLink ?>" class="ps-profile-user-review__user-avatar">
                                <?= UserUtils::getAvatarForUser($review->order->user, 'store'); ?>
                            </a>
                            <a href="<?= $userProfileLink ?>" class="ps-profile-user-review__user-name">
                                <?= \H(UserFacade::getFormattedUserName($review->order->user)); ?>
                            </a>
                        </div>
                        <div class="ps-profile-user-review__user-date">
                            <?= Yii::$app->formatter->asDate($review->created_at) ?>
                        </div>
                        <div class="ps-profile-user-review__user-rate">
                            <?= ReviewStarsWidget::widget(['rating' => $review->getRating(), 'withSchema' => true, 'company' => $userModel->company]); ?>
                        </div>
                    </div>

                    <?php if ($review->comment): ?>
                        <div class="ps-profile-user-review__text">
                            <?= H($review->comment) ?>
                        </div>
                    <?php endif; ?>


                    <div class="ps-profile-user-review__data">
                        <div class="ps-profile-user-review__data-ps">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="ps-profile-user-review__data-label">
                                        <?= _t('ps.profile', 'Printed on') ?>:
                                    </td>
                                    <td><?= H($printer); ?></td>
                                </tr>
                                <tr>
                                    <td class="ps-profile-user-review__data-label">
                                        <?= _t('ps.profile', 'Material') ?>:
                                    </td>
                                    <td>
                                        <?= $material ? $material->title : _t('site.model3d', 'Different materials'); ?>
                                        <?php if ($color) { ?>
                                            <div title="Biodegradable and flexible plastic" class="material-item">

                                                <div class="material-item__color"
                                                     style="background-color: rgb(<?= $color->rgb; ?>)"></div>
                                                <div class="material-item__label"><?= $color->title ?></div>

                                            </div>
                                        <?php } else { ?>
                                            <?= _t('site.model3d', 'Different colors'); ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <?=
                        SwipeGalleryWidget::widget([
                            'files' => $review->reviewFilesSort,
                            'thumbSize' => [120, 120],
                            'containerOptions' => ['class' => 'ps-profile-user-review__user-models'],
                            'itemOptions' => ['class' => 'ps-profile-user-review__user-models-pic'],
                            'scrollbarOptions' => ['class' => 'ps-profile-user-review__user-models-scrollbar'],
                            'alt' => _t('ps.public', 'Review image #{id}')
                        ]);
                        ?>
                    </div>

                </div>

            <?php endforeach; ?>


        </div>

    </div>
</div>
