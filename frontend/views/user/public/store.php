<?php
/** @var \yii\web\View $this */
/** @var array $params */
/** @var \common\models\Model3d[] $models */

use frontend\components\UserUtils;

use frontend\models\store\StoreSearchForm;
use frontend\widgets\StoreUnitWidget;



echo $this->renderFile('@app/views/user/public/_top.php', $params);

$this->title = $title;
#$this->params['breadcrumbs'][] = $this->title;
$userModel = $params['userModel'];
$usernameTxt = \frontend\models\user\UserFacade::getFormattedUserName($userModel, true);


$categoryId =  (isset($currentCategory) && $currentCategory)  ? $currentCategory->id : '';
?>

<div class="store-filter__container store-filter--public-store">
    <div class="container">
        <div class="store-filter">
            <div class="store-filter__sort-by">
                <h4><?= _t('public.store','Sort by')?>:</h4>
                <?php
                echo kartik\select2\Select2::widget([
                    'name' => 'delivery-schedule__time-system',
                    'value' => \Yii::$app->request->getUrl(),
                    'data' => [
                        UserUtils::getUserStoreUrl($userModel, false, 120, $categoryId) => _t('public.store', 'Best rated'),
                        UserUtils::getUserStoreUrl($userModel, false, 120, $categoryId, StoreSearchForm::SORT_MODEL_DATE) => _t('public.store', 'Upload date')
                    ],
                    'hideSearch' => true,
                    'pluginEvents' => [
                        'change' => 'function(t){ location.href = t.target.value; }'
                    ],
                    'options' => [
                        'multiple' => false,
                        'class' => 'delivery-schedule__time-system',
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <?php /*
// *** Old public store content ***

<h1><?php echo $this->title; ?></h1>
<div class="row">
        <?php foreach ($storeUnits as $model3dObj): ?>

            <?php
            echo frontend\widgets\StoreUnitWidget::widget([
                'model3dObj' => $model3dObj,
                'mode' => 'public',
                'hideAuthor' => true
            ]);
            ?>

        <?php endforeach; ?>
</div>
*/?>

    <div class="row public-store-container">

        <div class="col-xs-12 col-md-4 col-lg-3 sidebar public-store">
            <h4 class="public-store__heading"><?= _t('front.user','About')?></h4>

            <div class="public-store__ps-text">
                <?= frontend\components\StoreUnitUtils::displayMore($userModel->userProfile->info_about, 30); ?>
            </div>

            <?php if(!empty($userModel->userProfile->info_skills)): ?>
                <h4 class="public-store__heading"><?=_t('front.user', 'Skills and professional achievements');?></h4>
                <div class="public-store__ps-text">
                    <?= frontend\components\StoreUnitUtils::displayMore($userModel->userProfile->info_skills, 30); ?>
                </div>
            <?php endif; ?>

            <div class="public-store__hire">
                <a class="btn btn-primary btn-block ts-ga" data-categoryga="PublicStore" data-actionga="HireDesigner"  href="<?php echo yii\helpers\Url::toRoute(
                    array_merge(\common\models\message\helpers\UrlHelper::personalMessageRoute($userModel), ['utm_source'=>'treatstock', 'utm_medium'=>'public-profile', 'utm_campaign'=>'hire_designer', 'utm_content'=>'send_msg'])); ?>"><?=_t('public.store', 'Hire Designer');?></a>
            </div>
        </div>

        <div class="col-xs-12 col-md-8 col-lg-9 mainbar wide-padding wide-padding--left">

            <h1 class="public-store__h1">
                <?= _t('front.my', '3D Models by ')?>
                <?php echo $usernameTxt; ?>
            </h1>

            <div class="model-card-list">

                <?php
                if(isset($storeUnits)) foreach($storeUnits as $model3d): ?>
                    <?php
                    if (!$model3d->isPublished()) {
                        continue ;
                    }
                    echo StoreUnitWidget::widget([
                        'storeUnit' => $model3d->storeUnit,
                        'mode' => 'public',
                        'hideAuthor' => true
                    ]);

                    ?>
                <?php endforeach; ?>

            </div>
            <br clear="all" />
            <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>
        </div>
    </div>

</div>


<script>
    <?php $this->beginBlock('js1', false); ?>
    var baseStoreUrl = '<?=UserUtils::getUserStoreUrl($userModel, false, 120, null, $sort);?>';
    var withSort = <?=!empty($sort)?'true':'false';?>;
    $("#js-public-store-category-select").on('change', function(a,b){
        if(parseInt(b.activeCategoryId) > 0){
            location.href = baseStoreUrl + (withSort?'&':'?') + 'categoryId=' + b.activeCategoryId;
        }else{
            location.href = baseStoreUrl
        }
    });
    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>
