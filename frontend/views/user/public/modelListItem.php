<?php

use common\components\JsObjectFactory;
use frontend\components\UserUtils;
use frontend\models\model3d\Model3dFacade;
use yii\helpers\Html;

/** @var \common\models\Model3d $model3d */
$model3d = $model;

$itemLink        = param('server') . Model3dFacade::getStoreUrl($model3d);
$cover           = Model3dFacade::getCover($model3d);
$userProfileLink = param('server') . UserUtils::getUserStoreUrl($model3d->user);
JsObjectFactory::createJsObject(
    'storeUnitElementClass',
    'storeUnitElementObj_' . $model3d->id,
    [
        'uid' => $model3d->id
    ],
    $this
);
?>
<div class="ts-e-store-item">
    <div class='catalog-item'>
        <div class="catalog-item__pic">
            <div class="catalog-item__author">
                <a href="<?= $userProfileLink ?>" class="catalog-item__author__avatar">
                    <?= \frontend\components\UserUtils::getAvatarForUser($model3d->user, 'store'); ?>
                </a>
                <a href="<?= $userProfileLink ?>" class="catalog-item__author__name">
                    <?= \H(\frontend\models\user\UserFacade::getFormattedUserName($model3d->user)); ?>
                </a>
            </div>

            <?= \frontend\widgets\ModelFotoramaWidget::widget(
                [
                    'id'                      => 'fotoramaWidgetDiv' . $model3d->id,
                    'model'                   => $model3d,
                    'currentSelectedFileInfo' => null,
                    'disableFullImageSize'    => true,
                    'disable3dView'           => true,
                    'allowAutoRotate'         => false
                ]
            ); ?>
        </div>
        <div class="catalog-item__footer">
            <h4 class="catalog-item__footer__title"><a href="<?= $itemLink; ?>" title="<?= H($model3d->title); ?>"><?= H($model3d->title); ?></a></h4>

            <div class="catalog-item__footer__price">
                <div class="ts-e-model-price__value js-catalog-price catalog-price-store-unit-<?= $model3d->storeUnit->id ?>" data-store-unit-id="<?= $model3d->storeUnit->id ?>">
                </div>
            </div>

            <a class="ts-e-model-print-btn" href="<?php echo Model3dFacade::getPrintUrl($model3d); ?>" target="_blank" title="<?= H($model3d->title); ?>" data-actionga="Print"><?= _t('public.store', 'Buy') ?></a>
        </div>
    </div>
</div>

