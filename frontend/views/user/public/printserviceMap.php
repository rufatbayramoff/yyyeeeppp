<?php

?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?=
            \frontend\widgets\MapWidget::widget([
                'height' => 300,
                'width' => 500,
                'readonly' => true,
                'data_locations' => $dataLocations
            ]);
            ?>

            <script>
                initialize();
            </script>

        </div>
    </div>
</div>