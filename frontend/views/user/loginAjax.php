<?php

use common\modules\captcha\widgets\GoogleRecaptcha2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\authclient\widgets\AuthChoice;
/** @var \frontend\models\user\SignupForm $modelSignup */
/** @var \frontend\models\user\LoginForm $model */

?>
<div class="modal-dialog loginAjax-modal-dialog">
    <div class="modal-content">
        <div class="modal-header our-modal-header">
            <?php /*?>
            <button type="button" class="close" data-dismiss="modal" >
                <span aria-hidden="true">&times;</span><span class="sr-only"></span>
            </button>
            <?php */?>
            <div class="modal-title " id="loginAjax-modal">
                <ul class="nav nav-tabs" id="tabContent">
                    <li ><a href="#user-loginAjax-signup" id="loginAjax-modal_signup_tab" data-toggle="tab"><?= _t('front', 'Sign up for free'); ?></a></li>
                    <li class="active"><a href="#user-loginAjax-login" id="loginAjax-modal_signin_tab" data-toggle="tab"><?= _t('front', 'Sign in'); ?></a></li>
                </ul> 
            </div>
        </div>

        <div class="modal-body">
            <div class="tab-content">
                <div class="omb_login">
                    <?php $authAuthChoice = AuthChoice::begin([
                        'baseAuthUrl' => ['/user/auth-remote']
                    ]); ?>
                    <ul class="auth-clients">
                        <?php foreach ($authAuthChoice->getClients() as $client): ?>
                            <li><?=$authAuthChoice->clientLink($client, $client->getTitle()) ?></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php AuthChoice::end(); ?>

                    <div class="row omb_row-sm-offset-3 omb_loginOr">
                        <div class="col-xs-12 col-sm-12">
                            <hr class="omb_hrOr">
                            <span class="omb_spanOr"><?= _t('front.site', 'or')?></span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="user-loginAjax-signup"> 

                    <?php
                    $form = ActiveForm::begin([
                            'method' => 'post',
                            'action' => ['/user/signup'],
                            'id' => 'loginAjax-signup-form',
                            'enableClientValidation' => 'true'
                    ]);

                    echo $form->field($modelSignup, 'redirectTo', ['template' => "{input}"])
                        ->hiddenInput();

                    echo $form->field($modelSignup, 'email', [
                        'template' => "{label}\n{input}\n{error}\n{hint}\n",
                        ]
                    );

                    echo $form->field($modelSignup, 'password', [
                        'template' => "{label}\n{input}\n{error}\n{hint}\n",
                    ])->passwordInput()

                    ?>
                    <p class="text-muted">
                        <?php
                        $terms = _t('front.site', 'Terms');
                        $policy = _t('front.site', 'Privacy Policy');
                        $cookie = _t('front.site', 'Cookie use');
                        echo _t('app.site', 'By using this Service, you agree to our {terms} and that
                    you have read our {policy}, including our {cookie}', [
                            'terms' => sprintf("<a href='%s/site/terms'>$terms</a> ", Yii::getAlias('@web')),
                            'policy' => sprintf("<a href='%s/site/policy'>$policy</a>", Yii::getAlias('@web')),
                            'cookie' => sprintf("<a href='%s/site/policy#cookie'>$cookie</a>.", Yii::getAlias('@web')),
                            ]
                        );
                        echo GoogleRecaptcha2::widget();
                        ?>
                        <div class="form-group required has-error hidden" id="recaptureRequired">
                            <div class="help-block"><?=_t('front.site', 'Please, fill recaptcha')?></div>
                        </div>
                    </p>

                    <div class="modal-footer our-modal-footer">
                        <div>
                            <?php
                            echo Html::submitButton(_t('front', 'Sign up'), ['class' => 'btn btn-primary signup-btn__submit', 'name' => 'signup-button']);
                            ?>
                            <button type="button" class="btn btn-default signup-btn__cancel" data-dismiss="modal" >
                                <?= _t('front', 'Cancel'); ?>
                            </button>
                        </div>                        
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>

                <div class="tab-pane active" id="user-loginAjax-login">
                    <?php
                    $form2 = ActiveForm::begin(
                            [
                                'id' => 'login-form', 'enableClientValidation' => 'true',
                                'action' => ['/user/login'],
                            ]
                    );
                    echo $form2->field($model, 'redirectTo', ['template' => "{input}"])
                        ->hiddenInput();

                    echo $form2->field($model, 'email', [
                        'template' => "{label}\n{error}\n{input}\n{hint}\n",
                        'errorOptions' => ['class' => 'help-block alert alert-danger', 'style' => 'display:none;']
                        ]
                    );
                    echo '<div class="relative">';
                        echo $form2->field($model, 'password', [
                            'template' => "{label}\n{error}\n{input}\n{hint}\n",
                            'errorOptions' => ['class' => 'help-block alert alert-danger', 'style' => 'display:none;']
                        ])->passwordInput();
                    echo '</div>';
                    echo '<div class="row">';
                    echo '<div class="col-xs-6 p-r0">';
                    echo Html::a(_t('front', 'Forgot password?'), ['/user/forgot-password'], ['class' => 'password-recovery']);
                    echo '</div>';
                    echo '<div class="col-xs-6 text-right p-l0">';
                    echo '<div class="checkbox m-t0 m-b0 p-l0">';
                    echo $form->field($model, 'rememberMe', ['template'=>'{input} {label}'])->checkbox([], false);
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';

                    ?>
                    <p class="text-muted">
                        <?php
                        $terms = _t('front.site', 'Terms');
                        $policy = _t('front.site', 'Privacy Policy');
                        $cookie = _t('front.site', 'Cookie use');
                        echo _t('app.site', 'By using this Service, you agree to our {terms} and that
                    you have read our {policy}, including our {cookie}', [
                            'terms' => sprintf("<a href='%s/site/terms'>$terms</a> ", Yii::getAlias('@web')),
                            'policy' => sprintf("<a href='%s/site/policy'>$policy</a>", Yii::getAlias('@web')),
                            'cookie' => sprintf("<a href='%s/site/policy#cookie'>$cookie</a>.", Yii::getAlias('@web')),
                            ]
                        );

                        ?>
                    </p>
                    <div class="modal-footer"> 
                        <div>
                            <?= Html::submitButton(_t('front.site', 'Sign in'), ['class' => 'btn btn-primary signup-btn__submit', 'name' => 'login-button']) ?>

                            <button type="button" class="btn btn-default signup-btn__cancel" data-dismiss="modal" >
                            <?= _t('front.site', 'Cancel'); ?>
                            </button>
                        </div>

                    </div>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>  
        </div>
    </div>
</div>

<?php
$this->registerJs("
$('#loginform-rememberme').on('click', function(){
    if( $('#loginform-rememberme').prop('checked') == true ){
        $('.auth-client .auth-link.google').attr('href', '/user/auth-remote?authclient=google&rememberMe=1');
        $('.auth-client .auth-link.facebook').attr('href', '/user/auth-remote?authclient=facebook&rememberMe=1');
    }else{
        $('.auth-client .auth-link.google').attr('href', '/user/auth-remote?authclient=google');
        $('.auth-client .auth-link.facebook').attr('href', '/user/auth-remote?authclient=facebook');
    }
});
$('#loginform-rememberme').click();
 ");
    
