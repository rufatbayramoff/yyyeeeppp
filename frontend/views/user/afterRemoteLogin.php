<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 08.02.17
 * Time: 14:23
 */
use yii\web\View;

/** @var $this View */
?>
<script>
  window.opener.location.href = '<?=H($redirectUrl)?>';
  window.close();
</script>
