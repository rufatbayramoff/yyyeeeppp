<?php
use common\components\JsObjectFactory;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model frontend\models\user\LoginForm */

$this->title = _t('front.site', 'Login');
JsObjectFactory::createJsObject(
    'loginFormClass',
    'loginFormObj',
    [
        'redirectTo' => $model->redirectTo?:'/',
    ]
    ,
    $this
);
?>
<div class="user-login">
<div class="container">

    <div class="row"> 
        <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 m-t30">

            <div class="panel box-shadow loginAjax-modal-dialog border-0">
                <div class="modal-header our-modal-header">
                    <div class="modal-title">
                        <ul class="nav nav-tabs">
                            <li><a href="/user/signup"><?= _t('front', 'Sign up for free'); ?></a></li>
                            <li class="active"><a href="/user/login"><?= _t('front', 'Sign in'); ?></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">

                    <?php $authAuthChoice = \yii\authclient\widgets\AuthChoice::begin([
                        'baseAuthUrl' => ['/user/auth-remote'],
                        'options' => [
                            'popupWidth' => 700,
                            'popupHeight' => 300
                        ]
                    ]); ?>
                    <ul class="auth-clients">
                        <?php foreach ($authAuthChoice->getClients() as $client): ?>
                            <li><?=$authAuthChoice->clientLink($client, $client->getTitle()) ?></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php \yii\authclient\widgets\AuthChoice::end(); ?>



                    <div class="row omb_loginOr">
                        <div class="col-xs-12 col-sm-12">
                            <hr class="omb_hrOr">
                            <span class="omb_spanOr"><?=_t('front.site', 'or');?></span>
                        </div>
                    </div>

                    <?php $form = ActiveForm::begin(['id' => 'login-form', 'action'=> ['/user/login']]); ?>
                        <?= $form->field($model, 'redirectTo', ['template' => "{input}"])->hiddenInput(); ?>
                        <?= $form->field($model, 'email', ['errorOptions'=>['encode' => false]]) ?>
                        <?= $form->field($model, 'password', ['errorOptions'=>['encode' => false]])->passwordInput() ?>
                        <div class="row">
                            <div class="col-xs-6 p-r0">
                            <?= Html::a(_t('front', 'Forgot password?'), ['/user/forgot-password'], ['class' => 'password-recovery']);?>
                            </div>
                            <div class="col-xs-6 text-right p-l0">
                                <div class="checkbox m-t0 m-b0 p-l0">
                                <?= $form->field($model, 'rememberMe', ['template'=>'{input} {label}'])->checkbox([], false);?>
                                </div>
                            </div>
                        </div>
                        <div style="color:#999;margin:1em 0">
                            <?= _t('front.site', 'Create a')?> <a href="/user/signup?redirectTo=<?=urlencode($model->redirectTo)?>"><?=_t('front.site', 'new account')?></a>.
                        </div>
                        <p class="text-muted">
                            <?php
                            $terms = _t('front.site', 'Terms');
                            $policy = _t('front.site', 'Privacy Policy');
                            $cookie = _t('front.site', 'Cookie use');
                            echo _t('app.site', 'By using this Service, you agree to our {terms} and that
                            you have read our {policy}, including our {cookie}', [
                                    'terms' => sprintf("<a href='%s/site/terms'>$terms</a> ", Yii::getAlias('@web')),
                                    'policy' => sprintf("<a href='%s/site/policy'>$policy</a>", Yii::getAlias('@web')),
                                    'cookie' => sprintf("<a href='%s/site/policy#cookie'>$cookie</a>.", Yii::getAlias('@web')),
                                ]
                            );

                            ?>
                        </p>
                        <div class="border-t text-center">
                            <?= Html::submitButton(_t('front.site', 'Sign In'), ['class' => 'btn btn-primary m-t10', 'name' => 'login-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </div>
</div>
</div>
