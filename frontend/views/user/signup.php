<?php
use common\components\JsObjectFactory;
use common\modules\captcha\widgets\GoogleRecaptcha2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\user\SignupForm */

$this->title = _t('front.site', 'Sign up');
JsObjectFactory::createJsObject(
    'loginFormClass',
    'loginFormObj',
    [
        'redirectTo' => $model->redirectTo ?: '/',
    ]
    ,
    $this
);
?>
<div class="user-signup">
    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4 m-t30">

                <div class="panel box-shadow loginAjax-modal-dialog border-0">
                    <div class="modal-header our-modal-header">
                        <div class="modal-title">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="/user/signup"><?= _t('front', 'Sign up for free'); ?></a></li>
                                <li><a href="/user/login"><?= _t('front', 'Sign in'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">

                        <?php $authAuthChoice = \yii\authclient\widgets\AuthChoice::begin(
                            [
                                'baseAuthUrl' => ['/user/auth-remote'],
                                'options'     => [
                                    'popupWidth'  => 700,
                                    'popupHeight' => 300
                                ]
                            ]
                        ); ?>
                        <ul class="auth-clients">
                            <?php foreach ($authAuthChoice->getClients() as $client): ?>
                                <li><?= $authAuthChoice->clientLink($client, $client->getTitle()) ?></li>
                            <?php endforeach; ?>
                        </ul>
                        <?php \yii\authclient\widgets\AuthChoice::end(); ?>

                        <div class="row omb_loginOr">
                            <div class="col-xs-12 col-sm-12">
                                <hr class="omb_hrOr">
                                <span class="omb_spanOr"><?= _t('front.site', 'or'); ?></span>
                            </div>
                        </div>

                        <p><?php echo _t('front.site', 'Please fill out the following fields to sign up:'); ?></p>
                        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                        <?= $form->field($model, 'redirectTo', ['template' => "{input}"])->hiddenInput(); ?>
                        <?= $form->field($model, 'email', ['errorOptions' => ['encode' => false]]) ?>
                        <?= $form->field($model, 'password', ['errorOptions' => ['encode' => false]])->passwordInput() ?>
                        <p class="text-muted">
                            <?php
                            $terms = _t('front.site', 'Terms');
                            $policy = _t('front.site', 'Privacy Policy');
                            $cookie = _t('front.site', 'Cookie use');
                            echo _t('app.site', 'By using this Service, you agree to our {terms} and that
                        you have read our {policy}, including our {cookie}', [
                                    'terms' => sprintf("<a href='%s/site/terms'>$terms</a> ", Yii::getAlias('@web')),
                                    'policy' => sprintf("<a href='%s/site/policy'>$policy</a>", Yii::getAlias('@web')),
                                    'cookie' => sprintf("<a href='%s/site/policy#cookie'>$cookie</a>.", Yii::getAlias('@web')),
                                ]
                            );
                            echo GoogleRecaptcha2::widget();
                            ?>
                        </p>
                        <div class="border-t text-center">
                            <?= Html::submitButton(_t('front.site', 'Sign up'), ['class' => 'btn btn-primary m-t10', 'name' => 'signup-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>