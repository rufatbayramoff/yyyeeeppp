<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\user\ChangeEmailForm */

?>
<div class="user-change-email"> 
    <?php
    $form = ActiveForm::begin([
        'layout' => 'inline', 
        'id' => 'user-change-email-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false
    ]);
    ?>
    
    E-mail : 
    <?= $form->field($model, 'email')->textInput() ?>

    <?= Html::submitButton(_t('front', 'Change e-mail'),
        ['class' => 'btn btn-primary ts-ajax-submit', 'name' => 'submit-button']) ?>
<?php ActiveForm::end(); ?>
</div>
