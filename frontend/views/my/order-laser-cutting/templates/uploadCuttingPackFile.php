<?php
?>
<div class="models__row" ng-if="!cuttingPackFile.isCanceled">
    <div class="models__pic">
        <img ng-src="{{cuttingPackFile.getPreviewUrl()}}">
    </div>
    <div class="models__data">
        <div class="models__info">
            <h4 class="models__title">{{cuttingPackFile.title}}</h4>
            <div ng-if="cuttingPackFile.uploadFailedReason" class="model_loading_error_reason">
                {{cuttingPackFile.uploadFailedReason}}
            </div>
            <div ng-if="cuttingPackFile.uploadPercent>0 && cuttingPackFile.uploadPercent<100"
                 class="models_loading_progress" style="width: {{model3dImg.uploadPercent}}%"></div>
            <div ng-if="cuttingPackFile.isConverting()" class="models__loading-state"><?= _t('site.cutting', 'Converting...') ?></div>
        </div>

        <button ng-click="cancelUploadItem(cuttingPackFile)" class="models__del"
                title="<?= _t('site.printModel3d', 'Delete') ?>">&times;
        </button>
    </div>
</div>
