<?php


?>
<div class="model-printers-available"
     ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId && (offersBundleData.activeOfferItemId == offerItem.serviceMachine.id)}">

    <div class="model-printers-available__wrap">
        <div class="model-printers-available__data">
            <div class="model-printers-available__top-service" ng-if="offerItem.serviceMachine.topService"><?= _t('site.printModel3d', 'Top Service'); ?></div>

            <div class="model-printers-available__reviews" ng-if="offerItem.reviewsInfo.reviewsCount > 0">
                <span class="tsi tsi-rating-star"></span> {{ offerItem.companyRating() }}
            </div>

            <div class="model-printers-available__cert">
                <?= _t('site.printModel3d', 'Technology'); ?>: {{offerItem.serviceMachine.technology}}
            </div>

        </div>

        <div class="model-printers-available__info">
            <div class="model-printers-available__info__img">
                <a
                        href="{{offerItem.serviceMachine.publicLink}}"
                        target="_blank"
                        data-pjax="0"><img
                            ng-src="{{offerItem.serviceMachine.companyLogoUrl}}"
                            width="30" height="30" alt="" align="left"></a>
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a
                            href="{{offerItem.serviceMachine.publicLink}}" target="_blank"
                            data-pjax="0"><span>{{offerItem.serviceMachine.companyTitle}}</span></a>
                </div>
                <div class="model-printers-available__delivery-title">
                    {{offerItem.serviceMachine.address}}
                </div>
            </div>
        </div>

        <div class="model-printers-available__delivery">
            <div ng-if="offerItem.serviceMachine.responseTime"><?php echo _t('site.printModel3d', 'Response rate: ')?><b>{{offerItem.serviceMachine.responseTime}}</b></div>
            <div ng-if="offerItem.serviceMachine.peliability"><?php echo _t('site.printModel3d', 'Completion rate: ')?><span ng-class="{'text-success': !offerItem.topCheckNoInfo(),'text-muted':offerItem.topCheckNoInfo()}" class="text-bold">{{offerItem.serviceMachine.peliability}}</span></div>
        </div>

    </div>
    <div class="model-printers-available__price-btn">

        <div class="model-printers-available__price">
            <span class="ts-print-price">{{offerItem.getProducePrice().amountWithCurrency()}}</span>
            <div ng-if="offerItem.tsBonusAmount" class="ts-print-bonus" title="<?= _t('site.printModel3d', 'Treatstock Bonus') ?>">+{{offerItem.tsBonusAmount}} <?= _t('site.printModel3d', 'Bonus');?></div>
        </div>

        <div class="model-printers-available__price-delivery">
            <span>{{offerItem.getDeliveriesString()}}</span><span ng-if="offerItem.deliveryPrice.amount">: <strong><span class="tsi tsi-truck" title="<?= _t('site.printModel3d', 'Delivery Price'); ?>"></span> {{offerItem.deliveryPrice.amountWithCurrency()}}</strong>
            </span>
            <span ng-if="offerItem.deliveryPrice === 'TBC' ">
                : <strong><span class="tsi tsi-truck" title="<?= _t('site.printModel3d', 'Delivery Price'); ?>"></span> <?= _t('site.printModel3d', 'TBC'); ?></strong>
            </span>
            <span ng-if="offerItem.isFreeDelivery()">
                <strong><span class="tsi tsi-truck" title="<?= _t('site.printModel3d', 'Free delivery'); ?>"></span> <?= _t('site.printModel3d', 'Free delivery'); ?></strong>
            </span>
        </div>

        <div class="model-printers-available__print-btn">
            <button
                    class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                    title="<?= _t('site.printModel3d', 'Select manufacturer') ?>"
                    ng-click="offerHere(offerItem)"
                    oncontextmenu="return false;"><?= _t('site.printModel3d', 'Buy'); ?></button>
        </div>

<!--        <div class="model-printers-available__ts-bonus">-->
<!--            <span>+$7.89 Bonus</span>-->
<!--        </div>-->
    </div>
</div>