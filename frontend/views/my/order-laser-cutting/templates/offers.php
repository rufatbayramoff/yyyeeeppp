<?php

use common\components\ArchiveManager;
use common\models\CuttingPack;
use common\modules\cutting\serializers\CuttingPackSerializer;
use frontend\assets\JqueryDamnUploader;
use frontend\widgets\UserLocationWidget;

/** @var \frontend\components\FrontendWebView $this */
/** @var CuttingPack $cuttingPack */


$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);

Yii::$app->angular
    ->controller(
        [
            'cutting/offers-step',
            'cutting/offers-models',
            'cutting/parts-preview',
            'print-model3d/materials-guide',
            'print-model3d/wikimaterials-models',
        ]
    );
?>
<div class="tab-pane">
    <div class="row m-t20">
        <div class="col-sm-5 col-md-4">
            <div class="preview">

                <div class="preview__hint">
                    <?= _t('site.printModel3d', 'Choose a part to change its material'); ?>
                </div>

                <!-- Slider main container -->
                <div class="preview__slider">
                    <span ng-repeat="cuttingPart in cuttingPack.getActivePackParts()">
                        <?= $this->render('cuttingPreviewPart.php') ?>
                    </span>
                </div>

                <button class="btn btn-default btn-sm preview__view3d" ng-click="previewParts()">
                    <span class="tsi tsi-cube m-r5"></span>
                    <?= _t('site.printModel3d', 'Viewer') ?>
                </button>

                <span ng-if="!cuttingPack.isSameMaterialForAllParts">
                    <button class="btn btn-default btn-sm " ng-click="setOneMaterialForPack();"
                            title="<?= _t('site.cutting', 'Click to set the same material for all parts'); ?>">
                       <?= _t('site.printModel3d', 'Set for all'); ?>
                    </button>
                </span>
            </div>
            <h4 class="material__title clearfix">
                <?= _t('site.printModel3d', 'Materials') ?>
                <a class="material__guide-btn" href="#materialsGuideModal"
                   ng-click="materialsGuide()"><?= _t('site.printModel3d', 'Need help?') ?></a>
            </h4>
            <?= $this->render('material-selector') ?>
        </div>
        <div class="col-sm-7 col-md-8">
            <div class="printers">
                <div class="printers__control">

                    <div class="printers__loc" ng-if="!offersBundleData.fixedLocation">
                        <label><?= _t('site.printers', 'Location'); ?></label>
                        <?= UserLocationWidget::widget([]); ?>
                    </div>

                    <div ng-if="!offersBundleData.psIdOnly" class="printers__delivery printers__intl_only">
                        <label title="<?= _t('site.printers', 'International delivery'); ?>"><?= _t('site.printers', 'International delivery'); ?></label>
                        <label class="checkbox-switch checkbox-switch--xs">
                            <input type="checkbox" ng-model="offersBundleData.intlOnly"
                                   ng-change="onChangeWithInternationalDelivery()">
                            <span class="slider"></span>
                        </label>
                    </div>

                    <?php /*
                    <div class="printers__sort">
                        <label><?= _t('site.store', 'Sort by') ?>:</label>
                        <select ng-options="key as value for (key , value) in sortOptions "
                                ng-model="offersBundleData.sortOffersBy" ng-change="onChangePrintersListSort()"
                                class="form-control input-sm"></select>
                    </div>*/?>
                </div>

                <div ng-if="(startedUpdateOffersRequest)">
                    <img src="/static/images/preloader.gif" class="preloader__loading">
                </div>
                <div ng-if="!(startedUpdateOffersRequest)">
                    <div ng-if="offersBundleData.offersBundle.emptyReason">
                        <div class="alert alert-info alert--text-normal m-t10 m-b20">{{offersBundleData.offersBundle.getEmptyListComment()}}</div>
                        <div class="text-center" ng-if="offersBundleData.offersBundle.isNoDeliveryReason() && psCountryOnly">
                            <button ng-click="setPsCountry()" class="btn btn-primary m-b20">
                                <?= _t('site.printers', ' Change location to '); ?> {{psCountryOnly}}
                            </button>
                        </div>
                    </div>
                    <div ng-repeat="offerItem in offersBundleData.offersBundle.offers">
                        <div>
                            <?= $this->render('offerItem') ?>
                        </div>
                    </div>
                </div>

                <nav ng-if="offersPageInfo.numPages>1">
                     <ul class="pagination">
                        <li ng-class="{'active': offersPageInfo.currentPage==0}"><a href="javascript:;" ng-click="selectPage(0)">1</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==1}" ng-if="(offersPageInfo.numPages>1) && (offersPageInfo.currentPage<6 || offersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(1)">2</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==2}" ng-if="(offersPageInfo.numPages>2) && (offersPageInfo.currentPage<6 || offersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(2)">3</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==3}" ng-if="(offersPageInfo.numPages>3) && (offersPageInfo.currentPage<6 || offersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(3)">4</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==4}" ng-if="(offersPageInfo.numPages>4) && (offersPageInfo.currentPage<6 || offersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(4)">5</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==5}" ng-if="(offersPageInfo.numPages>5) && (offersPageInfo.currentPage<6 || offersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(5)">6</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==6}" ng-if="(offersPageInfo.numPages>6) && (offersPageInfo.currentPage<6 || offersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(6)">7</a></li>

                        <li ng-if="offersPageInfo.numPages>9"><a class="pagination__dots" href="javascript:;"><span class="pagination__dots-body"></span></a></li>

                        <li ng-if="offersPageInfo.currentPage>=6 && (offersPageInfo.currentPage==(offersPageInfo.numPages-4)) && offersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(offersPageInfo.currentPage-3)">{{offersPageInfo.currentPage - 2}}</a></li>
                        <li ng-if="offersPageInfo.currentPage>=6 && (offersPageInfo.currentPage<(offersPageInfo.numPages-3)) && offersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(offersPageInfo.currentPage-2)">{{offersPageInfo.currentPage - 1}}</a></li>
                        <li ng-if="offersPageInfo.currentPage>=6 && (offersPageInfo.currentPage<(offersPageInfo.numPages-3)) && offersPageInfo.numPages>7"><a href="javascript:;" ng-click="prevPage()">{{offersPageInfo.currentPage}}</a></li>
                        <li ng-if="offersPageInfo.currentPage>=6 && (offersPageInfo.currentPage<(offersPageInfo.numPages-3)) && offersPageInfo.numPages>7" class='active'><a href="javascript:;" ng-click="">{{offersPageInfo.currentPage + 1}}</a></li>
                        <li ng-if="offersPageInfo.currentPage>=6 && (offersPageInfo.currentPage<(offersPageInfo.numPages-3)) && offersPageInfo.numPages>7"><a href="javascript:;" ng-click="nextPage()">{{offersPageInfo.currentPage + 2}}</a></li>
                        <li ng-if="offersPageInfo.currentPage>=6 && (offersPageInfo.currentPage<(offersPageInfo.numPages-3)) && offersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(offersPageInfo.currentPage+2)">{{offersPageInfo.currentPage + 3}}</a></li>
                        <li ng-if="offersPageInfo.currentPage>=6 && (offersPageInfo.currentPage==(offersPageInfo.numPages-5)) && offersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(offersPageInfo.currentPage+3)">{{offersPageInfo.currentPage + 4}}</a></li>

                        <li ng-if="offersPageInfo.currentPage>=6 && (offersPageInfo.currentPage<(offersPageInfo.numPages-5))"><a class="pagination__dots" href="#dots"><span class="pagination__dots-body"></span></a></li>

                        <li ng-class="{'active': offersPageInfo.currentPage==(offersPageInfo.numPages-7)}" ng-if="offersPageInfo.currentPage>6 && offersPageInfo.currentPage>=(offersPageInfo.numPages-3) && offersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(offersPageInfo.numPages-7)">{{offersPageInfo.numPages - 6}}</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==(offersPageInfo.numPages-6)}" ng-if="offersPageInfo.currentPage>6 && offersPageInfo.currentPage>=(offersPageInfo.numPages-3) && offersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(offersPageInfo.numPages-6)">{{offersPageInfo.numPages - 5}}</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==(offersPageInfo.numPages-5)}" ng-if="offersPageInfo.currentPage>6 && offersPageInfo.currentPage>=(offersPageInfo.numPages-3) && offersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(offersPageInfo.numPages-5)">{{offersPageInfo.numPages - 4}}</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==(offersPageInfo.numPages-4)}" ng-if="offersPageInfo.currentPage>6 && offersPageInfo.currentPage>=(offersPageInfo.numPages-3) && offersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(offersPageInfo.numPages-4)">{{offersPageInfo.numPages - 3}}</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==(offersPageInfo.numPages-3)}" ng-if="offersPageInfo.currentPage>6 && offersPageInfo.currentPage>=(offersPageInfo.numPages-3) && offersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(offersPageInfo.numPages-3)">{{offersPageInfo.numPages - 2}}</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==(offersPageInfo.numPages-2)}" ng-if="((offersPageInfo.currentPage>=7) && (offersPageInfo.currentPage>=(offersPageInfo.numPages-3)) || offersPageInfo.numPages<10) && offersPageInfo.numPages>8"><a href="javascript:;" ng-click="selectPage(offersPageInfo.numPages-2)">{{offersPageInfo.numPages - 1}}</a></li>
                        <li ng-class="{'active': offersPageInfo.currentPage==(offersPageInfo.numPages-1)}" ng-if="offersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(offersPageInfo.numPages-1)">{{offersPageInfo.numPages}}</a></li>
                    </ul>
                </nav>

            </div>
        </div>
    </div>

    <div class="aff-widget-bottom">
        <button ng-click="prevStep()" class="btn btn-default">
            <span class="tsi tsi-left"></span>
            <?= _t('site.common', 'Back'); ?>
        </button>
        <button ng-click="nextStep()" class="btn btn-primary">
            <?= _t('site.common', 'Next'); ?>
            <span class="tsi tsi-right"></span>
        </button>
    </div>
</div>
