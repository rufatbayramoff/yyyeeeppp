<?php
?>
<div class="modal fade modal-primary" tabindex="-1" role="dialog" ng-controller="parts-preview">
    <div class="modal-dialog modal-model3d-preview">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= _t('site.printModel3d', 'Parts preview') ?></h4>
            </div>
            <div class="modal-body">
                <div ng-repeat="packPart in activeParts">
                    <div class="modal-part-info" ng-show="selectedPartUuid==packPart.uuid">
                        <img src="{{packPart.image}}" id="pack_part_img_{{packPart.uuid}}" class="modal-part-image">
                        <div class="modal-part-title p-t10">{{packPart.viewTitle()}} - <input type="number" class="input-sm form-control" ng-model="packPart.qty">&nbsp;<?= _t('site.order', 'items');?></div>
                    </div>
                </div>
                <div class="modal-cutting-part-positions" ng-if="(cuttingPack.getActivePackParts()|numkeys)>1">
                    <button class="btn btn-default btn-sm" ng-click="viewPrev()">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" width="20px" height="20px">
                            <path d="M88.6 121.3c.8.8 1.8 1.2 2.9 1.2s2.1-.4 2.9-1.2c1.6-1.6 1.6-4.2 0-5.8l-51-51 51-51c1.6-1.6 1.6-4.2 0-5.8s-4.2-1.6-5.8 0l-54 53.9c-1.6 1.6-1.6 4.2 0 5.8l54 53.9z" fill="#404448"/>
                        </svg>
                    </button>
                    <div class="modal-cutting-part-positions-wrap">
                        <div ng-repeat="cuttingPart in cuttingPack.getActivePackParts()" class="modal-part-position-element">
                            <button class="btn btn-default btn-sm modal-part-position" ng-class="{'model-part-position-active' : selectedPartUuid==cuttingPart.uuid}" ng-click="setSelectedCuttingPartUuId(cuttingPart.uuid)">
                                •
                            </button>
                        </div>
                    </div>
                    <button class="btn btn-default btn-sm" ng-click="viewNext()">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" width="20px" height="20px">
                            <path d="M40.4 121.3c-.8.8-1.8 1.2-2.9 1.2s-2.1-.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8 0l53.9 53.9c1.6 1.6 1.6 4.2 0 5.8l-53.9 53.9z" fill="#404448"/>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

