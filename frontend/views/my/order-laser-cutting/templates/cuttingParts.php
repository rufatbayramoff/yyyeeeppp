<?php

use common\components\ArchiveManager;
use common\models\CuttingPack;
use common\modules\cutting\serializers\CuttingPackSerializer;
use frontend\assets\JqueryDamnUploader;

/** @var \frontend\components\FrontendWebView $this */
/** @var CuttingPack $cuttingPack */


$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);

Yii::$app->angular
    ->controller(
        [
            'cutting/select-parts-step',
            'cutting/cutting-parts-validator'
        ]
    )
    ->controllerParams([
//        'cuttingPack'              => $cuttingPackSerialized,
    ]);

?>
<div class="tab-pane">
    <div class="part-selector" ng-if="cuttingPack.hasSeveralPages()">
        <label class="part-selector__label">
            <?= _t('site.cutting', 'Cutting file') ?>:
        </label>
        <div class="part-selector__dropdown">
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{currentPackPage.title}}
                    <span class="tsi tsi-down"></span></button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li ng-repeat="cuttingPackPage in cuttingPack.getActivePages()">
                        <a ng-click="changeCurrentCuttingPage(cuttingPackPage)">{{cuttingPackPage.title}}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="parts-view">
        <div id='stage_view'>
            <div id="worktable">
                <svg id="snap"></svg>
            </div>
            <div id="view3d"></div>
            <div id="infotable">
                <div id="info">
                    <span id="info1"></span><br>
                    <span id="info2"></span>
                </div>
            </div>
        </div>
    </div>

    <div id="worktable_menu">
        <div class="menuitem_info"><?= _t('site.cutting', 'Area size'); ?>:
            {{selectedArea.sizeX}} {{selectedArea.units}} x {{selectedArea.sizeY}} {{selectedArea.units}}
        </div>
        <div class="menuitem" ng-click="wt.markSelected('cut',false)">
            <?= _t('site.cutting', 'Mark this section as CUT'); ?>
        </div>
        <div class="menuitem" ng-click="wt.markSelected('engrave',false)">
            <?= _t('site.cutting', 'Mark this section as ENGRAVE'); ?>
        </div>
        <div class="menuitem" ng-click="wt.markSelected('cut',true)">
            <?= _t('site.cutting', 'Mark <b>all similar lines</b> as CUT'); ?>
        </div>
        <div class="menuitem" ng-click="wt.markSelected('engrave',true)">
            <?= _t('site.cutting', 'Mark <b>all similar lines</b> as ENGRAVE'); ?>
        </div>
        <div class="menuitem" ng-click="wt.deleteSelected()">
            <?= _t('site.cutting', 'Delete selected'); ?>
        </div>
    </div>

    <div class="aff-widget-bottom">
        <div class="aff-widget-bottom-status" ng-if="selectedArea || 1">
            <?= _t('site.cutting', 'Selected area box'); ?>:
            {{selectedArea.sizeX}} {{selectedArea.units}} x {{selectedArea.sizeY}} {{selectedArea.units}}
        </div>
        <button ng-click="resetState()" class="btn btn-default m-l0 pull-left">
            <span class="tsi tsi-remove"></span>
            <span class="buttons-text"><?= _t('site.common', 'Clear'); ?></span>
        </button>
        <button ng-click="prevStep()" class="btn btn-default">
            <span class="tsi tsi-left"></span>
            <span class="buttons-text"><?= _t('site.common', 'Back'); ?></span>
        </button>
        <button class="btn btn-primary" ng-click="nextStep()" ng-disabled="showProgress">
            <span>
                <span class="buttons-text m-l0 m-r10"><?= _t('site.common', 'Next'); ?></span>
                <span class="tsi tsi-right"></span>
            </span>
        </button>
    </div>
</div>
