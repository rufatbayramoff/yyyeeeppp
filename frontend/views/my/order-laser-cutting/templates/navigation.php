<?php
/** @var string $currentStepName */

Yii::$app->angular
    ->controllerParams(
        [
            'currentStepName' => $currentStepName
        ]
    )
    ->controller(
        [
            'cutting/navigation',
            'cutting/delivery-step',
            'cutting/delivery-step-validator'
        ]
    );
?>
<div class="aff-widget" ng-controller="laser-cutting-navigation">
    <!-- Nav tabs -->
    <ul class="aff-widget-tabs clearfix">
        <li class="{{steps['upload'].status}}" ui-sref="upload" ui-sref-active="active">
            <a href="#upload" aria-controls="upload">
                <span class="aff-widget-tabs__step">1</span>
                <?= _t('site.printModel3d', 'Upload Files') ?>
            </a>
        </li>
        <li class="{{steps['parts'].status}}" ui-sref="parts" ui-sref-active="active">
            <a href="#parts" aria-controls="parts">
                <span class="aff-widget-tabs__step">2</span>
                <?= _t('site.printModel3d', 'Customize Parts') ?>
            </a>
        </li>
        <li class="{{steps['offers'].status}}" ui-sref="offers" ui-sref-active="active">
            <a href="#offers" aria-controls="offers">
                <span class="aff-widget-tabs__step">3</span>
                <?= _t('site.printModel3d', 'Select Vendor') ?>
            </a>
        </li>
        <li class="{{steps['delivery'].status}}" ui-sref="delivery" ui-sref-active="active">
            <a href="#delivery" aria-controls="delivery">
                <span class="aff-widget-tabs__step">4</span>
                <?= _t('site.printModel3d', 'Delivery Options') ?>
            </a>
        </li>
        <li class="{{steps['checkout'].status}}" ui-sref="checkout" ui-sref-active="active">
            <a href="#payment" aria-controls="payment">
                <span class="aff-widget-tabs__step">5</span>
                <?= _t('site.printModel3d', 'Checkout') ?>
            </a>
        </li>
    </ul>
    <div class="aff-widget-content clearfix">
        <ui-view></ui-view>
    </div>
    <div ng-cloak ng-if="showProgressTop.status" class="cutting-widget-progress-backdrop"></div>
    <div ng-cloak ng-if="showProgressTop.status" class="cutting-widget-progress-window">
        <h3 class="cutting-widget-progress-window__text"><?= _t('site.printModel3d', 'Generate cutting preview') ?></h3>
        <div class="cutting-widget-progress-window__loader">
            <img src="/static/images/preloader.gif">
        </div>
    </div>

    <div ng-if="cuttingPack.isLoadSvg()" class="cutting-widget-progress-window cutting-widget-progress-window--white">
        <h3 class="cutting-widget-progress-window__text m-b10"><?= _t('site.printModel3d', 'File is converting') ?></h3>
        <p class="m-b30"><?= _t('site.printModel3d', 'It could take 30-60 seconds') ?></p>
        <div class="cutting-widget-progress-window__loader">
            <img src="/static/images/preloader.gif">
        </div>
    </div>
</div>

