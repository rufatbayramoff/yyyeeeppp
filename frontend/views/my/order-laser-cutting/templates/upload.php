<?php

use common\components\ArchiveManager;
use common\models\CuttingPack;
use common\modules\cutting\serializers\CuttingPackSerializer;
use frontend\assets\JqueryDamnUploader;

/** @var \frontend\components\FrontendWebView $this */
/** @var CuttingPack $cuttingPack */


$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);

$allowedExts           = array_merge(CuttingPack::ALLOWED_FORMATS, \common\components\FileTypesHelper::ALLOW_IMAGES_EXTENSIONS); // TODO: ArchiveManager::ARCHIVE_FORMATS
$cuttingPackSerialized = CuttingPackSerializer::serialize($cuttingPack);

Yii::$app->angular
    ->controller(
        [
            'cutting/upload-step',
            'print-model3d/common-uploader'
        ]
    )
    ->service([
        'measure',
        'mathUtils',
    ])
    ->controllerParams([
        'cuttingPack'              => $cuttingPackSerialized,
        'upload.allowedExtensions' => $allowedExts
    ]);

JqueryDamnUploader::register($this);
?>
<div class="tab-pane">
    <?php if ($this->isWidgetMode()): ?>
        <div class="row">
            <div class="col-md-12 ">
                <div class="pull-right header-bottom__lang t-header-bottom__lang hidden-xs">
                    <?php echo \frontend\widgets\LangWidget::widget(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div ng-show="cuttingPack.isEmpty()">
        <form class="upload-drop-file-zone upload-block">
            <h2 class="m-t0 m-b20"><?= _t('site.printModel3d', 'Upload files and configure parts to get an instant quote') ?></h2>
            <div class="upload-block__explainer">

                <div class="upload-block__explainer-item">
                    <a class="t-widget-upload-link" ng-click="uploader.addFileClick();">
                        <span class="tsi tsi-upload-l upload-block__explainer-ico"></span>
                        <div class="upload-block__explainer-count">1
                        </div><?= _t('site.printModel3d', 'Upload your files') ?>
                    </a>
                </div>

                <div class="upload-block__explainer-item">
                    <span class="tsi tsi-printer3d upload-block__explainer-ico"></span>
                    <div class="upload-block__explainer-count">2
                    </div><?= _t('site.printModel3d', 'Get it produced') ?>
                </div>

                <div class="upload-block__explainer-item">
                    <span class="tsi tsi-truck upload-block__explainer-ico"></span>
                    <div class="upload-block__explainer-count">3
                    </div><?= _t('site.printModel3d', 'Delivery in 5-7 days') ?>
                </div>

            </div>

            <div class="btn btn-danger upload-block__btn t-widget-upload-button" ng-click="uploader.addFileClick();">
                <?= _t('site.printModel3d', 'Upload Files...') ?>
            </div>

            <div class="upload-block__hint">
                <strong><?= _t('site.printModel3d', 'Upload CDR, DXF, EPS or SVG files.') ?></strong>
                <br>
                <br>
                <?= _t('site.printModel3d', 'JPG, PNG and GIF files are also supported.') ?>
                <br>
                <small>
                    <?= _t('site.printModel3d', 'All files are protected by Treatstock security') ?>
                </small>
            </div>
        </form>
    </div>
    <div ng-show="!cuttingPack.isEmpty()">
        <p class="tagline">
            <?= _t('site.printModel3d',
                'Files for 2D cutting') ?>
        </p>
        <div class="models">
            <div ng-repeat="cuttingPackFile in cuttingPack.cuttingPackFiles">
                <?= $this->render('uploadCuttingPackFile.php') ?>
            </div>
        </div>
        <div class="models__controls">
            <button class="btn btn-primary btn-sm models__add-btn" ng-click="uploader.addFileClick()">
                <span class="tsi tsi-plus m-r10"></span>
                <?= _t('site.printModel3d', 'Upload Files'); ?>
            </button>
        </div>
    </div>

    <div class="aff-widget-bottom" ng-show="(cuttingPack.hasModels())">
        <button ng-click="nextStep()" class="btn btn-primary">
            <?= _t('site.common', 'Next'); ?>
            <span class="tsi tsi-right"></span>
        </button>
    </div>
</div>
