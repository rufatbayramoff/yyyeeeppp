<?php
?>
<div ng-if="getCurrentMaterial()">
    <div class="material-switcher">
    <span ng-repeat="materialItem in offersBundleData.allowedMaterials.list">
        <div ng-if="materialItem.material.title" class="material-switcher__item"
             ng-click="changeMaterial(materialItem.material.id)"
             ng-class="{'material-switcher__item--active':materialItem.material.id == offersBundleData.selectedMaterialId}"
             title="{{materialItem.material.title}}">
            {{materialItem.material.title}}
        </div>
    </span>
    </div>

    <select class="form-control color-switcher__material-selector" id="materialSelector"
            ng-model='offersBundleData.selectedThickness' ng-change="changeThickness()">
        <option ng-repeat="thicknessItem in getCurrentMaterial().thickness" value="{{thicknessItem.thickness}}">
            {{thicknessItem.thickness}} mm
        </option>
    </select>

    <div class="color-switcher__material-list m-b10">
        <div ng-if="getCurrentMaterial()">
            <div class="color-switcher__color-list">
        <span ng-repeat="color in getCurrentMaterialColors()">
            <div ng-click="changeColor(color.id)" class="material-item"
                 ng-class="{'material-item--active':color.id == offersBundleData.selectedMaterialColorId}"
                 title="{{color.title}}">
                <div class="material-item__color" ng-style="{'background-color': '#{{color.rgbHex}}'}"></div>
                <div class="material-item__label">{{color.title}}</div>
            </div>
        </span>
            </div>
        </div>
    </div>
</div>
