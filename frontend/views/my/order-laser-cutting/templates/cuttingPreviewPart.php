<?php
?>
<div class="preview__slide" ng-if="1" ng-click="setPackActivePart(cuttingPart)"
     ng-class="{'preview__slide--selected': cuttingPart.id && (cuttingPart.id == offersBundleData.selectedCuttingPartId)}"
>
    <img class="preview__pic" title="{{cuttingPart.title}}" ng-src="{{cuttingPart.image}}">
    <div class="preview__cut-qty">
        <label for="">
            <?= _t('site.printModel3d', 'Qty') ?>
        </label>
        <input type="number" class="input-sm form-control" ng-model="cuttingPart.qty" ng-change="changeQty(cuttingPart)">
    </div>
</div>

