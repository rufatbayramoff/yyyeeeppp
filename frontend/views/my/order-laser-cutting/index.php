<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.09.17
 * Time: 14:58
 */

/** @var $this \frontend\components\FrontendWebView */
/** @var $currentStepName string */

use common\models\GeoCountry;
use common\modules\cutting\assets\LaserCuttingPartsAsset;
use frontend\components\angular\AngularSanitizeAsset;
use kartik\select2\Select2Asset;
use kartik\select2\ThemeKrajeeAsset;
use lib\geo\GeoNames;
use yii\helpers\ArrayHelper;

$psOnlyCountry    = '';
$psOnlyCountryIso = '';
if ($this->params['psId']) {
    $psOnly           = \common\models\Company::findByPk($this->params['psId']);
    if ($psOnly) {
        $country      = $psOnly->getPossibleCountry();
        if ($country) {
            $psOnlyCountry    = $country?->title;
            $psOnlyCountryIso = $country?->iso_code;
        }
    }
}
$this->title = _t('site.printModel3d','Laser Cutting & Engraving service – Order Online');
$stateData = $stateData ?? [];
Yii::$app->angular
    ->service(
        [
            'maps',
            'geo',
            'router',
            'notify',
            'user',
            'measure',
            'modal'
        ]
    )
    ->controller(
        [
            'store/filepage/delivery-models',
            'store/common-models',
            'cutting/cutting-models',
            'cutting/checkout-step',
        ]
    )
    ->directive(['google-map', 'google-address-input'])
    ->constants(
        [
            'globalConfig' =>
                [
                    'staticUrl' => param('staticUrl')
                ],
            'countries'    => ArrayHelper::toArray(
                GeoNames::getAllCountries(),
                [GeoCountry::class => ['id', 'iso_code', 'title', 'is_easypost_domestic', 'is_easypost_intl']]
            ),
        ]
    )
    ->controllerParam('posUid', $this->params['posUid'] ?? '')
    ->controllerParam('psId', $this->params['psId'] ?? '')
    ->controllerParam('psCountry', $psOnlyCountry ?? '')
    ->controllerParam('psCountryIso', $psOnlyCountryIso ?? '');

Select2Asset::register($this);
ThemeKrajeeAsset::register($this);
AngularSanitizeAsset::register($this);
LaserCuttingPartsAsset::register($this);

?>
<script type="text/ng-template" id="/laser-cutting/templates/upload.html">
    <?= $this->render('templates/upload.php', $stateData) ?>
</script>
<script type="text/ng-template" id="/laser-cutting/templates/cutting-parts.html">
    <?= $this->render('templates/cuttingParts.php', $stateData) ?>
</script>
<script type="text/ng-template" id="/laser-cutting/templates/offers.html">
    <?= $this->render('templates/offers.php', $stateData) ?>
</script>
<script type="text/ng-template" id="/common/templates/delivery.html">
    <?= $this->render('../common/delivery.php', $stateData) ?>
</script>
<script type="text/ng-template" id="/common/templates/checkout.html">
    <?= $this->render('../common/checkout.php', $stateData) ?>
</script>
<script type="text/ng-template" id="/app/store/checkuot/delivery/validate-address-modal.html">
    <?= $this->render('../common/deliveryAddressWarning.php') ?>
</script>
<script type="text/ng-template" id="/app/store/checkuot/delivery/invalid-address-modal.html">
    <?= $this->render('../common/deliveryAddressInvalid.php') ?>
</script>
<script type="text/ng-template" id="/wiki/materials-guide.html">
    <?= $this->render('../common/materials-guide.php') ?>
</script>
<script type="text/ng-template" id="/laser-cutting/templates/previewParts.html">
    <?= $this->render('templates/parts-preview.php') ?>
</script>


<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
<?= $this->render('templates/navigation.php', compact('currentStepName')) ?>


