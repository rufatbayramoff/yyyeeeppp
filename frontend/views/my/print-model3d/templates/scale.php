<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.03.18
 * Time: 15:16
 */

?>
<div>
    <div class="model-zoom-title">
            <span class="model-zoom-title__text collapsed" data-toggle="collapse" data-target="#modelZoom" aria-expanded="false" aria-controls="modelZoom">
                <?= _t('site.store', 'Scale'); ?><span class="tsi tsi-down"></span>
            </span>
    </div>

    <div ng-if="isSmall && !model.isKit()" class="model-zoom-alert">
        <?= _t('site.store', 'Your model is too small. Each dimension for your model should be no less than 10 mm (0.4 in).'); ?><br/>
        <?= _t('site.store', 'Please'); ?>
        <?= _t('site.store', 'adjust the scale'); ?>
        <?= _t('site.store', 'of your model to ensure it is 3D printable.'); ?>
    </div>

    <div ng-if="isLarge && !model.isKit()" class="model-zoom-alert">
        <?= _t('site.store', 'We recommend not to scale your model more than  {{getMaxModelSizeLabel()}} as this may exceed the build volume capabilities of most 3D printers.'); ?>
        <?= _t('site.store', 'Adjust the scale to find more manufacturers that can accept your order.'); ?>
    </div>

    <div class="collapse" id="modelZoom">
        <div class="model-zoom">
            <div class="model-zoom__sizes">
                <div class="model-zoom__sizes-item model-zoom__sizes-item--type">
                    <label class="control-label" for=""><?= _t('site.store', 'Scale in'); ?></label>
                    <div class="dropdown model-zoom__measure">
                        <button class="btn btn-default btn-sm btn-block dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            {{scaleType.name}}
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
                            <li ng-repeat="scale in scaleTypes">
                                <a ng-click="scaleTypeChange(scale)">{{scale.name}}</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div ng-if="scaleType.type == 'dimension'" class="model-zoom__sizes-item model-zoom__sizes-item--measure js-measure-types">
                    <label class="control-label" for="">&nbsp;</label>
                    <div class="dropdown model-zoom__measure">
                        <button class="btn btn-default btn-sm btn-block dropdown-toggle dropdown-menu-right-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            {{currentMeasure.title}}
                            <span class="tsi tsi-down"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li ng-repeat="measure in allowedMeasures">
                                <a href="#" ng-click="setCurrentMeasure(measure)">{{measure.title}}</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div ng-if="scaleType.type == 'dimension'" class="model-zoom__sizes-item">
                    <label class="control-label" for="zoomLength"><?= _t('site.store', 'length'); ?> <span
                                class="tsi tsi-arrow-right-l model-zoom__sizes-arrow"></span></label>
                    <input type="number" step="0.1" class="form-control" id="zoomLength" ng-model="size.width" ng-change="onChangeWidth()" min="0.1">
                </div>

                <div ng-if="scaleType.type == 'dimension'" class="model-zoom__sizes-item">
                    <label class="control-label" for="zoomWidth"><?= _t('site.store', 'width'); ?> <span
                                class="tsi tsi-arrow-right-l model-zoom__sizes-arrow model-zoom__sizes-arrow--rotate"></span></label>
                    <input type="number" step="0.1" class="form-control" id="zoomWidth" ng-model="size.height" ng-change="onChangeHeight()" min="0.1">
                </div>

                <div ng-if="scaleType.type == 'dimension'" class="model-zoom__sizes-item">
                    <label class="control-label" for="zoomHeight"><?= _t('site.store', 'height'); ?> <span
                                class="tsi tsi-arrow-up-l model-zoom__sizes-arrow"></span></label>
                    <input type="number" step="0.1" class="form-control" id="zoomHeight" ng-model="size.length" ng-change="onChangeLength()" min="0.1">
                </div>

                <div ng-if="scaleType.type == 'percent'" class="model-zoom__sizes-item model-zoom__sizes-item--single">
                    <label class="control-label" for="zoomHeight"><?= _t('site.store', 'Percentage'); ?> <span
                                class="tsi tsi-percent model-zoom__sizes-arrow"></span></label>
                    <input type="number" step="0.1" class="form-control" id="zoomHeight" ng-model="percent" ng-change="onChangePercent(percent)" min="0.1">
                </div>

                <div ng-if="scaleType.type == 'ration'" class="model-zoom__sizes-item model-zoom__sizes-item--single">
                    <label class="control-label" for="zoomRatio"><?= _t('site.store', 'Ratio'); ?></label>
                    <div class="input-group">
                        <span class="input-group-addon">1:</span>
                        <input type="number" step="0.1" id="zoomRatio" class="form-control" ng-model="ratio" ng-change="onChangeRatio(ratio)" min="0.1">
                    </div>
                </div>

            </div>

            <div class="model-zoom__btns">

                <button class="btn btn-primary btn-sm" ng-click="apply()">
                    <span ng-if="showProgress">
                    <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Please wait...'); ?>
                    </span>
                    <span ng-if="!showProgress">
                        <?= _t('site.store', 'Apply'); ?>
                    </span>
                </button>

            </div>

            <div class="model-zoom__original-sizes" ng-if="isSizeChanged()">
                <button ng-click="revertSize()" type="button" class="btn btn-link btn-sm p-l0 p-r0"><?= _t('site.store', 'Revert to original size:') ?></button>
                {{originalSize.width | number:2 }} x {{originalSize.height | number:2 }} x {{originalSize.length | number:2 }} x {{currentMeasure.title}}
            </div>
        </div>
    </div>
</div>
