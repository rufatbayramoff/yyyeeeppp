<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 06.10.17
 * Time: 15:05
 */
//                <option ng-repeat="infillItem in  getInfill().list" ng-selected="(infillItem>40 && infillItem<50)" ng-value="infillItem">{{infillItem}} - {{(infillItem>40 && infillItem<50)}}</option>

?>
<div class="material-switcher">
    <span ng-repeat="materialColorItem in offersBundleData.allowedMaterials.list">
        <div ng-if="materialColorItem.materialGroup" class="material-switcher__item" ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
             ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId == offersBundleData.selectedMaterialGroupId}"
             title="{{materialColorItem.materialGroup.title}}">
            {{materialColorItem.materialGroup.title}}
        </div>
    </span>
</div>

<select class="form-control color-switcher__material-selector" id="materialSelector" ng-if="isShowMaterialColors()" ng-model='offersBundleData.selectedMaterialId' ng-change="changeMaterial()">
    <option ng-repeat="materialColorItem in getMaterialsColors()"
            value="{{materialColorItem.materialId}}"
    >{{materialColorItem.getTitleSelectedMaterial()}}
    </option>
</select>

<div class="color-switcher__material-list m-b10">
    <input type="checkbox" class="showhideColorSwitcher" id="color-switcher__aff-widget-switcher">
    <label class="color-switcher__mobile" for="color-switcher__aff-widget-switcher">
        <div title="Biodegradable and flexible plastic" class="material-item">
            <div class="material-item__color" style="background-color: #{{getColorById(offersBundleData.selectedMaterialColorId).rgbHex}}"></div>
            <div class="material-item__label">{{getColorById(offersBundleData.selectedMaterialColorId).title}}</div>
        </div>
        <span class="tsi tsi-down"></span>
    </label>

    <div ng-if="getColors()">
        <div class="color-switcher__color-list" ng-if="isShortColors() || !longColorsNeed()">
        <span ng-repeat="printerColor in getColors().shortColorsList">
            <div ng-click="changeColor(printerColor.id)" class="material-item" ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
                 title="{{printerColor.title}}">
                <div class="material-item__color" ng-style="{'background-color': '#{{printerColor.rgbHex}}'}"></div>
                <div class="material-item__label">{{printerColor.title}}</div>
            </div>
        </span>
            <div class="material-item" ng-if="isShortColors() && longColorsNeed()" ng-click="setColorsShortMode(0)">
            <span class="material-item__spoiler">
                <span class="tsi tsi-plus"></span>
                <?= _t('site.model3d', 'More'); ?>
            </span>
            </div>
        </div>
        <div class="color-switcher__color-list" ng-if="!isShortColors() && longColorsNeed()">
        <span ng-repeat="printerColor in getColors().longColorsList">
            <div ng-click="changeColor(printerColor.id)" class="material-item" ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
                 title="{{printerColor.title}}">
                <div class="material-item__color" ng-style="{'background-color': '#{{printerColor.rgbHex}}'}"></div>
                <div class="material-item__label">{{printerColor.title}}</div>
            </div>
        </span>
            <div class="material-item" ng-if="!isShortColors()" ng-click="setColorsShortMode(1)">
            <span class="material-item__spoiler">
                <span class="tsi">–</span>
                <?= _t('site.model3d', 'Less'); ?>
            </span>
            </div>
        </div>
    </div>
</div>

<div ng-if="getInfill()">
    <div class="infill-switcher">
        <label for="materialitem-infill" class="infill-switcher__label">
            <?=_t('site.model3d', 'Infill');?>
        </label>
        <select class="form-control infill-switcher__select" id="materialitem-infill" name="materialitem-infill" ng-model="offersBundleData.currentInfill"
                ng-change="onChangeInfill()"
                ng-options="infillItem for infillItem in getInfill().list"
        >
        </select>
    </div>
</div>
