<?php

use frontend\widgets\UserLocationWidget;


Yii::$app->angular
    ->controller(
        [
            'print-model3d/printers-step',
            'print-model3d/materials-guide',
            'print-model3d/wikimaterials-models',
            'print-model3d/model3d-preview',
        ]
    );
if (isset($activeOfferItemId) && ($activeOfferItemId > 0)) {
    Yii::$app->angular->controllerParam('printer.activeOfferItemId', $activeOfferItemId);
}
?>
<div class="tab-pane" id="print">
    <div class="row m-t20">
        <div class="col-sm-5 col-md-4">
            <div class="preview">

                <div class="preview__hint">
                    <?= _t('site.printModel3d', 'Select a file to change its material or color'); ?>
                </div>

                <!-- Slider main container -->
                <div class="preview__slider">
                    <span ng-repeat="model3dPart in model3d.parts">
                        <?= $this->render('printPreviewModel3dPart.php') ?>
                    </span>
                </div>
                <button class="btn btn-default btn-sm preview__view3d" ng-click="preview3d()" name="view3d">
                    <span class="tsi tsi-cube m-r5"></span>
                    <?= _t('site.printModel3d', '3D Viewer') ?>
                </button>
                <span ng-if="!model3dTextureState.isOneTextureForKit">
                    <button class="btn btn-default btn-sm " ng-click="setOneTextureForKit();" title="<?= _t('site.printModel3d', 'Click to set the same material and color for all files'); ?>">
                       <?= _t('site.printModel3d', 'Set for all'); ?>
                    </button>
                </span>

            </div>
            <h4 class="material__title clearfix">
                <?= _t('site.printModel3d', 'Materials & Colors') ?>
                <a class="material__guide-btn" href="#materialsGuideModal" ng-click="materialsGuide()"><?= _t('site.printModel3d', 'Need help?') ?></a>
            </h4>
            <?= $this->render('color-selector') ?>
        </div>
        <div class="col-sm-7 col-md-8">
            <div class="printers">
                <div class="printers__control">

                    <div class="printers__loc" ng-if="!offersBundleData.fixedLocation">
                        <label><?= _t('site.printers', 'Location'); ?></label>
                        <?= UserLocationWidget::widget([]); ?>
                    </div>

                    <div ng-if="!offersBundleData.psIdOnly" class="printers__delivery printers__intl_only">
                        <label title="<?= _t('site.printers', 'International delivery'); ?>"><?= _t('site.printers', 'International delivery'); ?></label>
                        <label class="checkbox-switch checkbox-switch--xs">
                            <input type="checkbox" ng-model="offersBundleData.intlOnly" ng-change="onChangeWithInternationalDelivery()">
                            <span class="slider"></span>
                        </label>
                    </div>

                    <div class="printers__cert">
                        <label title="<?= _t('site.printers', 'Certified only'); ?>"><?= _t('site.printers', 'Certified only'); ?></label>
                        <label class="checkbox-switch checkbox-switch--xs">
                            <input type="checkbox" ng-model="offersBundleData.certOnly" ng-change="onChangeWithInternationalDelivery()">
                            <span class="slider"></span>
                        </label>
                    </div>

                    <div class="printers__currency">
                        <label title="<?= _t('site.printers', 'Currency'); ?>"><?= _t('site.printers', 'Currency'); ?></label>
                        <select ng-options="key as value for (key , value) in currencies " ng-model="offersBundleData.currency" ng-change="onChangePrintersListSort()"
                                class="form-control input-sm"></select>
                    </div>

                    <div class="printers__sort">
                        <label><?= _t('site.store', 'Sort by') ?></label>
                        <select ng-options="key as value for (key , value) in sortOptions " ng-model="offersBundleData.sortPrintersBy" ng-change="onChangePrintersListSort()"
                                class="form-control input-sm"></select>
                    </div>
                </div>
                <div ng-if="(startedUpdatePrintersRequest || model3d.isCalculating())">
                    <img src="/static/images/preloader.gif" class="preloader__loading">
                </div>
                <div ng-if="!(startedUpdatePrintersRequest || model3d.isCalculating())">

                    <div ng-if="offersBundleData.offersBundle.messages">
                        <div class="alert alert-info alert--text-normal m-t10 m-b20">{{offersBundleData.offersBundle.getEmptyListComment(offersBundleData.psIdOnly)}}</div>
                        <div class="text-center" ng-if="offersBundleData.offersBundle.isNoDeliveryReason() && psCountryOnly">
                            <button ng-click="setPsCountry()" class="btn btn-primary m-b20">
                                <?= _t('site.printers', ' Change location to '); ?> {{psCountryOnly}}
                            </button>
                        </div>
                        <div class="text-center" ng-if="!offersBundleData.offersBundle.isNoDeliveryReason() && !offersBundleData.psIdOnly">
                            <button ng-click="setOneTextureForKit()" class="btn btn-primary m-b20">
                                <?= _t('site.printers', 'Reset Materials & Colors'); ?>
                            </button>
                        </div>
                    </div>
                    <div ng-repeat="offerItem in offersBundleData.offersBundle.offers">
                        <div>
                            <?= $this->render('offerItem') ?>
                        </div>
                    </div>
                    <div class="alert alert-info alert--text-normal m-t10 m-b20" ng-if="(offersBundleData.currency=='eur') && (offersBundleData.offersBundle.offers.length > 0) && (offersBundleData.offersBundle.offers.length <5)">
                        <?= _t('site.printers', 'We accept payments in EUR in beta mode.'); ?>
                    </div>
                    <div ng-if="offersBundleData.psIdOnly && !isWidget()">
                        <button class="btn btn-default" ng-click="allVendors()"><?= _t('site.printers', 'Show all vendors'); ?></button>
                    </div>
                </div>

                <nav ng-if="printersPageInfo.numPages>1">
                    <ul class="pagination">
                        <li ng-class="{'active': printersPageInfo.currentPage==0}"><a href="javascript:;" ng-click="selectPage(0)">1</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==1}" ng-if="(printersPageInfo.numPages>1) && (printersPageInfo.currentPage<6 || printersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(1)">2</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==2}" ng-if="(printersPageInfo.numPages>2) && (printersPageInfo.currentPage<6 || printersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(2)">3</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==3}" ng-if="(printersPageInfo.numPages>3) && (printersPageInfo.currentPage<6 || printersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(3)">4</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==4}" ng-if="(printersPageInfo.numPages>4) && (printersPageInfo.currentPage<6 || printersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(4)">5</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==5}" ng-if="(printersPageInfo.numPages>5) && (printersPageInfo.currentPage<6 || printersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(5)">6</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==6}" ng-if="(printersPageInfo.numPages>6) && (printersPageInfo.currentPage<6 || printersPageInfo.numPages<=9)"><a href="javascript:;" ng-click="selectPage(6)">7</a></li>

                        <li ng-if="printersPageInfo.numPages>9"><a class="pagination__dots" href="javascript:;"><span class="pagination__dots-body"></span></a></li>

                        <li ng-if="printersPageInfo.currentPage>=6 && (printersPageInfo.currentPage==(printersPageInfo.numPages-4)) && printersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(printersPageInfo.currentPage-3)">{{printersPageInfo.currentPage - 2}}</a></li>
                        <li ng-if="printersPageInfo.currentPage>=6 && (printersPageInfo.currentPage<(printersPageInfo.numPages-3)) && printersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(printersPageInfo.currentPage-2)">{{printersPageInfo.currentPage - 1}}</a></li>
                        <li ng-if="printersPageInfo.currentPage>=6 && (printersPageInfo.currentPage<(printersPageInfo.numPages-3)) && printersPageInfo.numPages>7"><a href="javascript:;" ng-click="prevPage()">{{printersPageInfo.currentPage}}</a></li>
                        <li ng-if="printersPageInfo.currentPage>=6 && (printersPageInfo.currentPage<(printersPageInfo.numPages-3)) && printersPageInfo.numPages>7" class='active'><a href="javascript:;" ng-click="">{{printersPageInfo.currentPage + 1}}</a></li>
                        <li ng-if="printersPageInfo.currentPage>=6 && (printersPageInfo.currentPage<(printersPageInfo.numPages-3)) && printersPageInfo.numPages>7"><a href="javascript:;" ng-click="nextPage()">{{printersPageInfo.currentPage + 2}}</a></li>
                        <li ng-if="printersPageInfo.currentPage>=6 && (printersPageInfo.currentPage<(printersPageInfo.numPages-3)) && printersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(printersPageInfo.currentPage+2)">{{printersPageInfo.currentPage + 3}}</a></li>
                        <li ng-if="printersPageInfo.currentPage>=6 && (printersPageInfo.currentPage==(printersPageInfo.numPages-5)) && printersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(printersPageInfo.currentPage+3)">{{printersPageInfo.currentPage + 4}}</a></li>

                        <li ng-if="printersPageInfo.currentPage>=6 && (printersPageInfo.currentPage<(printersPageInfo.numPages-5))"><a class="pagination__dots" href="#dots"><span class="pagination__dots-body"></span></a></li>

                        <li ng-class="{'active': printersPageInfo.currentPage==(printersPageInfo.numPages-7)}" ng-if="printersPageInfo.currentPage>6 && printersPageInfo.currentPage>=(printersPageInfo.numPages-3) && printersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(printersPageInfo.numPages-7)">{{printersPageInfo.numPages - 6}}</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==(printersPageInfo.numPages-6)}" ng-if="printersPageInfo.currentPage>6 && printersPageInfo.currentPage>=(printersPageInfo.numPages-3) && printersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(printersPageInfo.numPages-6)">{{printersPageInfo.numPages - 5}}</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==(printersPageInfo.numPages-5)}" ng-if="printersPageInfo.currentPage>6 && printersPageInfo.currentPage>=(printersPageInfo.numPages-3) && printersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(printersPageInfo.numPages-5)">{{printersPageInfo.numPages - 4}}</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==(printersPageInfo.numPages-4)}" ng-if="printersPageInfo.currentPage>6 && printersPageInfo.currentPage>=(printersPageInfo.numPages-3) && printersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(printersPageInfo.numPages-4)">{{printersPageInfo.numPages - 3}}</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==(printersPageInfo.numPages-3)}" ng-if="printersPageInfo.currentPage>6 && printersPageInfo.currentPage>=(printersPageInfo.numPages-3) && printersPageInfo.numPages>9"><a href="javascript:;" ng-click="selectPage(printersPageInfo.numPages-3)">{{printersPageInfo.numPages - 2}}</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==(printersPageInfo.numPages-2)}" ng-if="((printersPageInfo.currentPage>=7) && (printersPageInfo.currentPage>=(printersPageInfo.numPages-3)) || printersPageInfo.numPages<10) && printersPageInfo.numPages>8"><a href="javascript:;" ng-click="selectPage(printersPageInfo.numPages-2)">{{printersPageInfo.numPages - 1}}</a></li>
                        <li ng-class="{'active': printersPageInfo.currentPage==(printersPageInfo.numPages-1)}" ng-if="printersPageInfo.numPages>7"><a href="javascript:;" ng-click="selectPage(printersPageInfo.numPages-1)">{{printersPageInfo.numPages}}</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="aff-widget-bottom">
        <button ng-click="prevStep()" class="btn btn-default">
            <span class="tsi tsi-left"></span>
            <?= _t('site.common', 'Back'); ?>
        </button>
        <button ng-click="nextStep()" class="btn btn-primary">
            <?= _t('site.common', 'Next'); ?>
            <span class="tsi tsi-right"></span>
        </button>
    </div>
</div>

