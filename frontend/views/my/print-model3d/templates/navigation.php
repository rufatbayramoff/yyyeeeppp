<?php
Yii::$app->angular
    ->controllerParams(
        [
            'currentStepName' => $currentStepName
        ]
    )
    ->controller(
        [
            'print-model3d/navigation',
            'print-model3d/delivery-step',
            'print-model3d/delivery-step-validator'
        ]
    );
?>

<div class="aff-widget" id="print-navigation" ng-controller="print-model3d-navigation">
    <!-- Nav tabs -->
    <ul class="aff-widget-tabs clearfix">
        <li class="{{steps['upload'].status}}" ui-sref="upload" ui-sref-active="active">
            <a href="#upload" aria-controls="upload">
                <span class="aff-widget-tabs__step">1</span>
                <?= _t('site.printModel3d', 'Upload Files') ?>
            </a>
        </li>
        <li class="{{steps['printers'].status}}" ui-sref="printers" ui-sref-active="active">
            <a href="#print" aria-controls="print">
                <span class="aff-widget-tabs__step">2</span>
                <?= _t('site.printModel3d', 'Customize Order') ?>
            </a>
        </li>
        <li class="{{steps['delivery'].status}}" ui-sref="delivery" ui-sref-active="active">
            <a href="#delivery" aria-controls="delivery">
                <span class="aff-widget-tabs__step">3</span>
                <?= _t('site.printModel3d', 'Delivery Options') ?>
            </a>
        </li>
        <li class="{{steps['checkout'].status}}" ui-sref="checkout" ui-sref-active="active">
            <a href="#payment" aria-controls="payment">
                <span class="aff-widget-tabs__step">4</span>
                <?= _t('site.printModel3d', 'Checkout') ?>
            </a>
        </li>
    </ul>
    <div class="aff-widget-content clearfix">
        <ui-view></ui-view>
    </div>
</div>


<?php /*
    Temp hide this block from widget

    <div class="col-md-3 m-t30 p-t20">
        <?= _t('site.printModel3d', 'Do you run a 3D printing business?') ?>
        <?php if(is_guest()) { ?>
            <a href="/mybusiness/company/create-ps" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company/create-ps', 'signUp');return false;" : "return true;" ?>"><?= _t('site.printModel3d', 'Sign up') ?></a> <?= _t('site.printModel3d', 'to offer your services on our platform.') ?>
        <?php } else { ?>
            <a href="/mybusiness/company/create-ps"><?= _t('site.printModel3d', 'Create or edit') ?></a>  <?= _t('site.printModel3d', 'your service on our platform.') ?>
        <?php } ?>
    </div>

  */ ?>
