<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 25.10.17
 * Time: 10:55
 */

?>
<div class="model-printers-available"
     ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId && (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}">

    <div class="model-printers-available__wrap">
        <div class="model-printers-available__data">
            <div class="model-printers-available__top-service" ng-if="offerItem.psPrinter.topService"><?= _t('site.printModel3d', 'Top Service'); ?></div>

            <div class="model-printers-available__reviews" ng-if="offerItem.reviewsInfo.reviewsCount > 0">
                <span class="tsi tsi-rating-star"></span> {{ offerItem.companyRating() }}
            </div>

            <div class="model-printers-available__cert" ng-if="offerItem.certificatedMachine > 0">
                <div
                        class="cert-label cert-label--common"
                        data-toggle="tooltip"
                        data-placement="bottom"
                        data-original-title="<?= _t(
                            'site.ps.test',
                            'This machine is certified by Treatstock. It meets the technology requirements and necessary quality standards.'
                        ); ?>">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                {{offerItem.certificatedMachineFormat()}}
            </div>

        </div>

        <div class="model-printers-available__info">
            <div class="model-printers-available__info__img">
                <a
                        href="{{offerItem.psPrinter.publicLink}}"
                        target="_blank"
                        data-pjax="0"><img
                            ng-src="{{offerItem.psPrinter.ps.logoUrl}}"
                            width="30" height="30" alt="" align="left"></a>
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a
                            href="{{offerItem.psPrinter.publicLink}}" target="_blank"
                            data-pjax="0"><span>{{offerItem.psPrinter.ps.title}}</span></a>
                </div>

                <div class="model-printers-available__delivery-title">
                    {{offerItem.psPrinter.address}}
                </div>

            </div>
        </div>
        <div class="model-printers-available__delivery" ng-click="printHere(offerItem)">
            <div ng-if="offerItem.responseTime"><?php echo _t('site.printModel3d', 'Response rate: ') ?><b>{{offerItem.responseTime}}</b></div>
            <div ng-if="offerItem.peliability"><?php echo _t('site.printModel3d', 'Completion rate: ') ?><span
                        ng-class="{'text-success': !offerItem.topCheckNoInfo(),'text-muted':offerItem.topCheckNoInfo()}" class="text-bold">{{offerItem.peliability}}</span></div>
        </div>
    </div>
    <div class="model-printers-available__price-btn" ng-click="printHere(offerItem)">

        <div class="model-printers-available__price">
            <span class="ts-print-price" ng-if="isWidget()">{{offerItem.anonimysPrice.amountWithCurrency()}}</span>
            <span class="ts-print-price" ng-if="!isWidget()">{{offerItem.userPrice.amountWithCurrency()}}</span>
        </div>

        <div class="model-printers-available__price-delivery">
            <span>{{offerItem.getDeliveriesString()}}</span><span ng-if="offerItem.estimateDeliveryCost.amount">: <strong><span class="tsi tsi-truck"
                                                                                                                                title="<?= _t('site.printModel3d', 'Delivery Price'); ?>"></span> {{offerItem.estimateDeliveryCost.amountWithCurrency()}}</strong>
                </span>
            <span ng-if="offerItem.estimateDeliveryCost === null && offerItem.hasDelivery()">
                    : <strong><span class="tsi tsi-truck" title="<?= _t('site.printModel3d', 'Delivery Price'); ?>"></span> <?= _t('site.printModel3d', 'TBC'); ?></strong>
                </span>
            <span ng-if="offerItem.estimateDeliveryCost.amount === 0">
                    <strong><span class="tsi tsi-truck" title="<?= _t('site.printModel3d', 'Free delivery'); ?>"></span> <?= _t('site.printModel3d', 'Free delivery'); ?></strong>
                </span>
        </div>

        <div ng-if="offerItem.tsBonusAmount" class="model-printers-available__ts-bonus" title="<?= _t('site.printModel3d', 'Treatstock Bonus') ?>">
            <span>+{{offerItem.tsBonusAmount}} <?= _t('site.printModel3d', 'Bonus'); ?></span>
        </div>

        <div class="model-printers-available__print-btn">
            <button
                    class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                    title="<?= _t('site.printModel3d', 'Select manufacturer') ?>"
                    oncontextmenu="return false;"><?= _t('site.printModel3d', 'Buy'); ?></button>
        </div>

    </div>
</div>

