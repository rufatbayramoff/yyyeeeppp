<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.09.17
 * Time: 14:03
 */

?>

<div class="models__row" ng-if="!model3dImg.isCanceled">
    <div class="models__pic">
        <img ng-src="{{model3dImg.previewUrl}}">
    </div>
    <div class="models__data">
        <div class="models__info">
            <h4 class="models__title">{{model3dImg.title}}</h4> <div ng-if="model3dImg.uploadFailedReason" class="model_loading_error_reason">{{model3dImg.uploadFailedReason}}</div>
            <div ng-if="model3dImg.uploadPercent>0 && model3dImg.uploadPercent<100" class="models_loading_progress" style="width: {{model3dImg.uploadPercent}}%"></div>
        </div>
        <button ng-click="cancelUploadItem(model3dImg)" class="models__del" title="<?=_t('site.printModel3d', 'Delete')?>">&times;</button>
    </div>
</div>
