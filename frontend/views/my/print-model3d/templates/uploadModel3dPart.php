<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 27.09.17
 * Time: 14:03
 */
?>
<div class="models__row" ng-if="!model3dPart.isCanceled">
    <div class="models__data">
        <div class="models__pic">
            <img ng-src="{{getModel3dPartPreviewUrl(model3dPart)}}" main-src="{{getModel3dPartPreviewUrl(model3dPart)}}" alt-src="/static/images/preloader80.gif"
                 onerror="commonJs.checkAltSrcImage(this);">
            <div class="models__pic-qty" title="<?=_t('site.printModel3d', 'Quantity'); ?>: {{model3dPart.qty|intFormatted}}">×{{model3dPart.qty|intFormatted}}</div>
        </div>
        <div class="models__info">
            <h4 class="models__title">{{model3dPart.title}}</h4>
            <div ng-if="model3dPart.uploadPercent>0 && model3dPart.uploadPercent<100" class="models_loading_progress" style="width: {{model3dPart.uploadPercent}}%"></div>
            <div ng-if="model3dPart.uploadFailedReason" class="model_loading_error_reason">{{model3dPart.uploadFailedReason}}</div>
            <div class="models__size" ng-if="!model3dPart.uploadFailedReason">
                <div ng-if="model3dPart.isParsed">
                    <div ng-if="model3dPart.isCalculated()" class="js_models_parsed">
                        <?= _t('site.printModel3d', 'Size') ?>: {{model3dPart.getSizeTitle()}}
                    </div>
                    <div ng-if="!model3dPart.isCalculated()" class="js_models_failed">
                        <?= _t('site.printModel3d', 'Parsing failed. Possible invalid file format.') ?>
                    </div>
                </div>
                <div ng-if="!model3dPart.isParsed" class="js_models_parsing">
                    <?= _t('site.printModel3d', 'Processing model. Please wait...') ?>
                </div>
            </div>
        </div>
        <div class="models__scaling" ng-if="model3d.isCalculated() && !model3d.isKit()">
            <model-scale model="model3d" model-state="model3dTextureState" max-model-array-size="[<?=implode(',', $maxModelSize->toArray())?>]">
            </model-scale>
        </div>
    </div>

    <div class="models__qty">
        <label class="control-label" for="getprintedform-qty"><?= _t('site.printModel3d', 'Quantity') ?></label>
        <input type="number"
               class="form-control input-sm"
               ng-model="model3dPart.qty"
               name="qty"
               style="-moz-appearance: textfield;"
               ng-blur="updateModel3dPartQty(model3dPart)"
               min="0"
               max="1000000"
               ng-disabled="!model3dPart.uid"
        >
    </div>

    <button ng-disabled="!model3dPart.isCalculated()" ng-click="duplicate(model3dPart)" class="models__duplicate" title="<?= _t('site.printModel3d', 'Duplicate') ?>"><i class="tsi tsi-duplicate"></i></button>
    <button ng-click="cancelUploadItem(model3dPart)" class="models__del" title="<?= _t('site.printModel3d', 'Delete') ?>">&times;</button>

</div>
