<?php

use common\components\ArchiveManager;
use common\components\ps\locator\Size;
use common\interfaces\Model3dBaseImgInterface;
use common\interfaces\Model3dBasePartInterface;
use common\models\serializers\ReviewSerializer;
use common\modules\onshape\OnshapeModule;
use frontend\assets\JqueryDamnUploader;
use frontend\controllers\catalog\actions\serializers\Model3dSerializer;
use frontend\controllers\catalog\actions\serializers\ModelStateLiteSerializer;
use yii\rest\Serializer;

/** @var \common\interfaces\Model3dBaseInterface $model3d */
/** @var \frontend\components\FrontendWebView $this */
/** @var \common\models\StoreOrderReview[] $reviews */

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);

$allowedExts = array_merge(Model3dBaseImgInterface::ALLOWED_FORMATS, Model3dBasePartInterface::ALLOWED_FORMATS, ArchiveManager::ARCHIVE_FORMATS);

$model3dInfo = Model3dSerializer::serialize($model3d);
$model3dState = ModelStateLiteSerializer::serialize($model3d);
$reviews = ReviewSerializer::serialize($reviews ?? []);
Yii::$app->angular
    ->controller(
        [
            'print-model3d/upload-step',
            'print-model3d/common-uploader',
            'review/review-models'
        ]
    )
    ->service([
        'measure',
        'mathUtils',
    ])
    ->controllerParam('upload.model3d', $model3dInfo)
    ->controllerParam('upload.model3dState', $model3dState)
    ->controllerParam('upload.allowedExtensions', $allowedExts)
    ->controllerParam('upload.partsExtensions', Model3dBasePartInterface::ALLOWED_FORMATS)
    ->controllerParam('reviews', $reviews);

$maxModelSize = Size::create(350,400, 400);
if ($this->params['psPrinterId']) {
    $psPrinter = \common\models\PsPrinter::findOne(['id' => $this->params['psPrinterId']]);
    if ($psPrinter) {
        $maxModelSize = $psPrinter->getBuildVolumeSize();
    }
}

JqueryDamnUploader::register($this);
$fromGalleryUrl = \yii\helpers\Url::toRoute(['/product/product/index', 'widget' => $this->isWidgetMode() ? 'widget' : null]);

?>
<div class="tab-pane">
    <?php if($this->isWidgetMode()): ?>

    <div class="row">
            <div class="col-md-12 ">
                <div class="pull-right header-bottom__lang t-header-bottom__lang hidden-xs" >
                    <?php echo \frontend\widgets\LangWidget::widget(); ?>
                </div>
            </div>
    </div>
    <?php endif; ?>
    <div ng-show="testIsEmptyModel()">
        <?php if ($this->isThingiverseUtm()): ?>
                <div class="alert alert-warning" style="margin: 30px 0 -10px;">
                    You was redirected from Thingiverse. Please download manually 3d model from thingiverse and upload it here.
                </div>
        <?php endif; ?>

        <form class="upload-drop-file-zone upload-block">
            <h2 class="m-t0 m-b20"><?= _t('site.printModel3d', 'Upload 3D models and get it printed') ?></h2>
            <?php /*
            <img class="upload-block__pic" src="/static/images/upload.png" alt="<?= _t('site.printModel3d', 'Upload Models') ?>">
            */ ?>

            <div class="upload-block__explainer">

                <div class="upload-block__explainer-item">
                    <a class="t-widget-upload-link" ng-click="uploader.addFileClick();">
                        <span class="tsi tsi-upload-l upload-block__explainer-ico"></span>
                        <div class="upload-block__explainer-count">1</div><?= _t('site.printModel3d', 'Upload 3D models') ?>
                    </a>
                </div>

                <div class="upload-block__explainer-item">
                    <span class="tsi tsi-printer3d upload-block__explainer-ico"></span>
                    <div class="upload-block__explainer-count">2</div><?= _t('site.printModel3d', 'Get it 3D printed') ?>
                </div>

                <div class="upload-block__explainer-item">
                    <span class="tsi tsi-truck upload-block__explainer-ico"></span>
                    <div class="upload-block__explainer-count">3</div><?= _t('site.printModel3d', 'Delivery in 5-7 days') ?>
                </div>

            </div>

            <div class="btn btn-danger upload-block__btn t-widget-upload-button" ng-click="uploader.addFileClick();">
                <?= _t('site.printModel3d', 'Upload Files...') ?>
            </div>

            <?php /* if ($this->isWidgetMode() && !$this->isForPs()): ?>
                <a href="<?php echo $fromGalleryUrl; ?>" class="btn btn-danger btn-ghost upload-block__btn upload-block__btn--gallery t-widget-gallery-button">
                    <?= _t('site.printModel3d', 'Select from gallery'); ?>
                </a>
            <?php endif; */ ?>

            <div class="upload-block__hint">
                <strong><?= _t('site.printModel3d', 'Upload STL, PLY, OBJ or 3MF files.') ?></strong>
                <br>
                <?= _t('site.printModel3d', 'JPG, PNG and GIF files are also supported.') ?>
                <br>
                <small>
                <?= _t('site.printModel3d', 'All files are protected by Treatstock security and ') ?>
                    <a href="https://www.watermark3d.com/" target="_blank">Watermark 3D</a>
                </small>
            </div>
        </form>

        <?php /*
        <div ng-if='reviews.haveReviews()' class="aff-widget-reviews">
            <div class="aff-widget-reviews__slider swiper-container">
                <div class="swiper-wrapper">
                    <div ng-repeat="reviewPart in reviews.parts" class="aff-widget-reviews__slider-item swiper-slide">
                        <a ng-if="reviewPart.haveCoverImg()" class="aff-widget-reviews__pic" href="{{reviewPart.urlPs}}" target="_blank">
                            <img class="aff-widget-reviews__img" ng-src="{{reviewPart.coverImgUrl}}" alt="{{reviewPart.user.fullName}} review"/>
                        </a>

                        <div class="aff-widget-reviews__rating">
                            <div class="star-rating-block">
                                <div class="star-rating rating-xs rating-disabled">
                                    <div class="rating-container tsi" data-content="">
                                        <div class="rating-stars" data-content="" style="width: {{reviewPart.rating.getRatingAllPercent()}}%;"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="aff-widget-reviews__date">
                                {{reviewPart.createdAt}}
                            </div>
                        </div>

                        <img ng-if="reviewPart.user.haveAvatar()" class="aff-widget-reviews__avatar" ng-src="{{reviewPart.user.avatarUrl}}">

                        <div class="aff-widget-reviews__user-data">
                            <a class="aff-widget-reviews__username" href="{{reviewPart.urlPs}}" target="_blank">
                                {{reviewPart.user.fullName}}
                            </a>
                        </div>

                        <div ng-if="reviewPart.haveComment()" class="aff-widget-reviews__review" title="{{reviewPart.comment}}">
                            {{reviewPart.comment}}
                        </div>
                    </div>
                </div>

                <div class="aff-widget-reviews__scrollbar swiper-scrollbar"></div>
            </div>
        </div>
        */?>

    </div>
    <div ng-show="!testIsEmptyModel()">
        <div ng-if="hasStepFiles()" class="alert alert-warning" style="margin: 30px 0 -10px;">
            <?=_t('site.printModel3d','Please, double check that your project shows up in 3D viewer fully and correctly. If it is not, <a href="<?=CatalogPrintingUrlHelper::landingPage()?>" target="_parent">get a quote</a> instead.');?>
        </div>
        <p class="tagline">
            <?= _t('site.printModel3d',
                'Set the required quantities for each file and if their sizes appear too small, change the unit of measurement to inches. Click on 3D Viewer for a 360° preview of your files.') ?>
        </p>

        <div class="aff-widget__upload-controls">
            <div class="units">
                    <span class="units__label">
                        <?= _t('site.printModel3d', 'Unit of measurement') . ':' ?>
                    </span>
                <div class="btn-group btn-switcher" data-toggle="buttons">
                    <label class="btn btn-default btn-xs" ng-class="{'active':model3d.modelUnits=='mm'}" ng-click="changeScale('mm')">
                        <input type="radio" name="optionsPrice" id="optionsPrice1" autocomplete="off">
                        MM
                    </label>
                    <label class="btn btn-default btn-xs" ng-class="{'active':model3d.modelUnits=='in'}" ng-click="changeScale('in')">
                        <input type="radio" name="optionsPrice" id="optionsPrice2" autocomplete="off">
                        IN
                    </label>
                </div>
            </div>
            <?php /*
            <div class="aff-widget__upload-zoom">
                <div ng-if="model3d.isCalculated() && !model3d.isKit()">
                    <model-scale model="model3d" model-state="model3dTextureState">
                    </model-scale>
                </div>
            </div>
            */ ?>
        </div>

        <div class="models">
            <div ng-repeat="model3dPart in model3d.parts">
                <?= $this->render('uploadModel3dPart.php', ['maxModelSize' => $maxModelSize]) ?>
            </div>
            <div ng-repeat="model3dImg in model3d.images">
                <?= $this->render('uploadModel3dImg.php') ?>
            </div>

            <div class="models__controls">
                <?php if (!empty($launcher) && $launcher === OnshapeModule::LAUNCHER_ONSHAPE): ?>
                    <?= \yii\helpers\Html::a(' <span class="tsi tsi-refresh"></span> ' . _t('site.user', 'Reload files from Onshape'),
                        '/onshape/app/import',
                        ['class'   => 'btn btn-default btn-sm  models__add-btn',
                         'name'    => 'submit-button',
                         'onClick' => '$(this).html("<i class=\"tsi tsi-refresh tsi-spin-custom\"></i> Importing...");if ($(this).hasClass(\'disabled\')){return false;};$(this).addClass(\'disabled\');return true;'
                        ]) ?>
                <?php endif; ?>
                <button class="btn btn-primary btn-sm models__add-btn" ng-click="uploader.addFileClick()">
                    <span class="tsi tsi-plus m-r10"></span>
                    <?= _t('site.printModel3d', 'Upload Files'); ?>
                </button>
                <button class="btn btn-default btn-sm models__add-btn" ng-click="preview3d()" ng-disabled="!model3d.hasPreview3d()">
                    <span class="tsi tsi-cube m-r10"></span>
                    <?= _t('site.printModel3d', '3D Viewer'); ?>
                </button>
            </div>
        </div>
    </div>



    <div class="aff-widget-bottom" ng-show="(model3d.parts|numkeys)">
        <button ng-click="nextStep()" class="btn btn-primary">
            <?= _t('site.common', 'Next'); ?>
            <span class="tsi tsi-right"></span>
        </button>
    </div>
</div>


    <script>
        <?php $this->beginBlock('js1', false); ?>

        function initAffWidgetReviews() {
            //Init slider for .aff-widget-reviews__slider
            var swiperAffWidgetReviews = new Swiper('.aff-widget-reviews__slider', {
                scrollbar: '.aff-widget-reviews__scrollbar',
                scrollbarHide: false,
                slidesPerView: 'auto',
                grabCursor: true
            });
        }

        initAffWidgetReviews();

        <?php $this->endBlock(); ?>
    </script>
<?php $this->registerJs($this->blocks['js1']); ?>