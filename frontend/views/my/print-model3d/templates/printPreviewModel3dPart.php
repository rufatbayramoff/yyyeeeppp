<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 05.10.17
 * Time: 15:10
 */
?>
<div class="preview__slide preview_slider-clickable" ng-if="!model3dPart.isCanceled && model3dPart.qty" ng-click="setModel3dActivePart(model3dPart)"
     ng-class="{'preview__slide--selected': model3dPart.id && (model3dPart.id == offersBundleData.selectedModel3dPartId)}"
>
    <div class="preview__material"
         title="{{model3dTextureState.getTexture(model3dPart).printerColor.title}} {{model3dTextureState.getTexture(model3dPart).getPrinterMaterialTitle()}}">
        {{model3dTextureState.getTexture(model3dPart).getPrinterMaterialTitle()}}
        <div class="preview__material-color" style="background-color: #{{model3dTextureState.getTexture(model3dPart).printerColor.getRgbHex()}}"></div>
    </div>
    <img class="preview__pic" title="{{model3dpart.title}}" ng-src="{{getModel3dPartPreviewUrl(model3dPart)}}" main-src="{{getModel3dPartPreviewUrl(model3dPart)}}"
         alt-src="/static/images/preloader.gif" onerror="commonJs.checkAltSrcImage(this);">
    <div class="preview__qty" title="<?= _t('site.printModel3d', 'Quantity') ?>: {{model3dPart.qty|intFormatted}}">&times;{{model3dPart.qty|intFormatted}}</div>
    <div>
        {{model3dPart.qty|intFormatted}}
    </div>
</div>
