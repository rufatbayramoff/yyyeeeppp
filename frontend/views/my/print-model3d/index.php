<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 21.09.17
 * Time: 14:58
 */

/** @var $this \frontend\components\FrontendWebView */

/** @var $currentStepName string */

use common\components\JsObjectFactory;
use common\models\GeoCountry;
use frontend\assets\FlagsAsset;
use frontend\assets\PrintModel3dAsset;
use frontend\components\angular\AngularSanitizeAsset;
use frontend\widgets\PrintByPsTitleWidget;
use kartik\select2\Select2Asset;
use kartik\select2\ThemeKrajeeAsset;
use lib\geo\GeoNames;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

$this->title = _t('site.printModel3d', 'Order 3D Printing Service – Instant Quote');

$psOnlyCountry    = '';
$psOnlyCountryIso = '';

if ($this->params['psId']) {
    $psOnly           = \common\models\Company::findByPk($this->params['psId']);
    if ($psOnly) {
        $country      = $psOnly->getPossibleCountry();
        if ($country) {
            $psOnlyCountry    = $country?->title;
            $psOnlyCountryIso = $country?->iso_code;
        }
    }
}

$stateData = $stateData ?? [];
Yii::$app->angular
    ->service(
        [
            'maps',
            'geo',
            'router',
            'notify',
            'user',
            'measure',
            'modelService',
            'model3dParseProgress',
            'modal'
        ]
    )
    ->controller(
        [
            'store/filepage/models',
            'store/common-models',
            'store/filepage/printer-models',
            'store/filepage/delivery-models',
            'store/filepage/printer-directives',
            'store/filepage/model-render',
            'store/filepage/color-service',
            'store/filepage/filepage-scale',
            'print-model3d/checkout-step',
        ]
    )
    ->directive(['google-map', 'google-address-input'])
    ->constants(
        [
            'globalConfig' =>
                [
                    'staticUrl' => param('staticUrl')
                ],
            'countries'    => ArrayHelper::toArray(
                GeoNames::getAllCountries(),
                [GeoCountry::class => ['id', 'iso_code', 'title', 'is_easypost_domestic', 'is_easypost_intl']]
            ),
        ]
    )
    ->controllerParam('posUid', $this->params['posUid'] ?? '')
    ->controllerParam('psId', $this->params['psId'] ?? '')
    ->controllerParam('psPrinterId', $this->params['psPrinterId'] ?? '')
    ->controllerParam('psCountry', $psOnlyCountry ?? '')
    ->controllerParam('psCountryIso', $psOnlyCountryIso ?? '');

PrintModel3dAsset::register($this);
Select2Asset::register($this);
ThemeKrajeeAsset::register($this);
AngularSanitizeAsset::register($this);

JsObjectFactory::createJsObject(
    'model3dRenderRouteClass',
    'model3dRenderRoute',
    [
        'baseUrl' => Yii::$app->params['staticUrl'] . Yii::$app->getModule('model3dRender')->renderImagesUrl . '/'
    ],
    $this
);

$ps = $ps ?? false;

if ($this->isWidgetMode()) {
    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to('/my/print-model3d/widget', true)]);
} else {
    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to('/my/print-model3d', true)]);
}

?>
<?php if ($ps && $this->isForPs() && $this->isWidgetMode()): ?>
    <div class="item-rendering-external-widget">
        <div class="container">

            <?php echo PrintByPsTitleWidget::widget([
                'ps' => $ps
            ]); ?>
        </div>
    </div>
<?php endif; ?>

<script type="text/ng-template" id="/print-model3d/templates/upload.html">
    <?= $this->render('templates/upload.php', $stateData) ?>
</script>
<script type="text/ng-template" id="/print-model3d/templates/printers.html">
    <?= $this->render('templates/printers.php', $stateData) ?>
</script>
<script type="text/ng-template" id="/print-model3d/templates/delivery.html">
    <?= $this->render('../common/delivery.php', $stateData) ?>
</script>
<script type="text/ng-template" id="/print-model3d/templates/checkout.html">
    <?= $this->render('../common/checkout.php', $stateData) ?>
</script>
<script type="text/ng-template" id="/app/store/checkuot/delivery/validate-address-modal.html">
    <?= $this->render('../common/deliveryAddressWarning.php') ?>
</script>
<script type="text/ng-template" id="/app/store/checkuot/delivery/invalid-address-modal.html">
    <?= $this->render('../common/deliveryAddressInvalid.php') ?>
</script>
<script type="text/ng-template" id="/model3d/preview3d.html">
    <?= $this->render('templates/model3d-preview.php') ?>
</script>
<script type="text/ng-template" id="/wiki/materials-guide.html">
    <?= $this->render('../common/materials-guide.php') ?>
</script>
<script type="text/ng-template" id="/print/time-around-modal.html">
    <?= $this->render('../common/timeAroundModal.php') ?>
</script>
<script type="text/ng-template" id="/js/ts/app/store/filepage/filepage-scale.html">
    <?= $this->render('templates/scale.php') ?>
</script>

<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
<?= $this->render('templates/navigation.php', compact('currentStepName')) ?>


