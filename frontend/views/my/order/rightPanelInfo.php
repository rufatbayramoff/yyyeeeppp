<?php
/** @var $utmSource string */

?>
<div class="panel m-t30">
    <div class="p-t15">

        <?php if ($utmSource) { ?>
            <p>
                <strong>
                    <?= _t('site.upload', 'Do you have 3D models that you would like to get printed?'); ?>
                </strong>
            </p>
            <p>
                <?= _t(
                    'site.upload',
                    'Upload your designs to Treatstock’s secure platform and choose from the best 3D print services near you. Select the material, color, and quantity of your prints and compare prices and reviews when placing an order.'
                ); ?>
            </p>

            <?php
            if ($utmSource=='cnc') {
                ?>
                <p>
                    <?= _t('front.upload', 'For now only STEP, IGES, STL and PLY files are supported'); ?>
                </p>
                <p>
                    <?= _t('front.upload', 'Recommended file formats include STEP (ap203, ap203is, ap214, ap214, ap214is) and IGES (v 5.3 mode 186).'); ?>
                </p>
                <?php
            } else {
                ?>
                <p>
                    <?= _t('site.upload', 'Currently, only STL and PLY files are supported, but we are working on adding more options.');?>
                </p>
                <?php
            }
            ?>
        <?php } else { ?>
            <p style="font-size: 16px;line-height: 1.4;margin-bottom: 20px;">
                <?= _t('site.upload', 'Upload your files to get an online quote for manufacturing services. On Treatstock you can find suitable technology and material for any product type. All costs are transparent and you get to choose the vendor for your job.');?>
            </p>

            <ul class="checked-list">
                <li><?= _t('site.upload', 'All-round production capabilities');?></li>
                <li><?= _t('site.upload', 'Instant Quoting and Ordering');?></li>
                <li><?= _t('site.upload', 'Personal approach');?></li>
            </ul>
        <?php }; ?>
    </div>
</div>