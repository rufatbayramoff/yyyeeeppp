<?php

use frontend\widgets\UploadWidget;

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

$additionalParams = [
    'psId' => $this->params['psId'] ?? ''
];

$this->title = _t('site.printModel3d', '3D printing, Cutting and Machining service');
?>

<?= $this->render('/common/ie-alert.php'); ?>

    <div class="upload-widget-container">
        <?= UploadWidget::widget(
            [
                'additionalParams' => $additionalParams,
                'allowUploadByUrl' => $allowUploadByUrl,
                'needNewWindow'    => true,
                'isWidget'         => 0
            ]
        ); ?>
    </div>

    <div class="upload-widget-how-it-works">

        <h3 class="m-t30 m-b20"><?= _t('site.printModel3d', 'Get thousands of price offers for your order in seconds') ?></h3>

        <h3 class="upload-widget-how-it-works__title"><?= _t('site.printModel3d', 'How it works') ?></h3>

        <div class="upload-block__explainer">

            <div class="upload-block__explainer-item">
                <span class="tsi tsi-upload-l upload-block__explainer-ico"></span>
                <div class="upload-block__explainer-count">1</div>
                <?= _t('site.printModel3d', 'Upload files') ?>
            </div>

            <div class="upload-block__explainer-item">
                <span class="tsi tsi-cogwheel upload-block__explainer-ico"></span>
                <div class="upload-block__explainer-count">2</div>
                <?= _t('site.printModel3d', 'Get it manufactured') ?>
            </div>

            <div class="upload-block__explainer-item">
                <span class="tsi tsi-truck upload-block__explainer-ico"></span>
                <div class="upload-block__explainer-count">3</div>
                <?= _t('site.printModel3d', 'Get it delivered') ?>
            </div>

        </div>

        <?php /*
    <div class="p-t15 p-b30">
        <span class="m-l10 m-r10"><?= _t('site.printModel3d', 'watch video for more information') ?></span>
        <button class="btn btn-default btn-ghost p-l15 p-r15 m-t10 m-b10" data-toggle="modal" data-target=".upload-block__modal">
            <i class="tsi tsi-video m-r10"></i><?= _t('site.printModel3d', 'How it works') ?> (5:24)
        </button>
    </div>
    */ ?>

        <div class="modal fade upload-block__modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h3 class="modal-title"><?= _t('site.printModel3d', 'How it works') ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="video-container m-b0">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/CYijJ4rrktw?rel=0" frameborder="0" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row m-l0 m-r0 p-t20 p-b10">
            <div class="col-sm-3 p-t5">
                <h4 class="upload-widget-how-it-works__benefits">
                    <i class="tsi tsi-checkmark m-r10 text-success"></i>
                    <?= _t('site.printModel3d', 'Save time') ?>
                </h4>
            </div>
            <div class="col-sm-3 p-t5">
                <h4 class="upload-widget-how-it-works__benefits">
                    <i class="tsi tsi-checkmark m-r10 text-success"></i>
                    <?= _t('site.printModel3d', 'Avoid spam') ?>
                </h4>
            </div>
            <div class="col-sm-3 p-t5">
                <h4 class="upload-widget-how-it-works__benefits">
                    <i class="tsi tsi-checkmark m-r10 text-success"></i>
                    <?= _t('site.printModel3d', 'Get the best prices') ?>
                </h4>
            </div>
            <div class="col-sm-3 p-t5">
                <h4 class="upload-widget-how-it-works__benefits">
                    <i class="tsi tsi-checkmark m-r10 text-success"></i>
                    <?= _t('site.printModel3d', 'Personal approach') ?>
                </h4>
            </div>
        </div>

    </div>
<?php
if (empty($disableReviews) || !$disableReviews) {
    ?>
    <div class="customer-reviews m-b30">
        <div class="container container--wide">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="customer-reviews__title text-center">
                        <?= _t('front', 'What our customers are saying'); ?>
                    </h2>
                </div>
            </div>
        </div>

        <div class="customer-reviews__slider swiper-container">
            <div class="swiper-wrapper customer-reviews__gallery">

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                    </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                        The print was an excellent job. A fast print, but more importantly are really good quality. I am a happy customer and would buy again.
                    </span>
                    <img class="customer-reviews__avatar" src="https://secure.gravatar.com/avatar/e83e2496ad31501002461f799301c8bb?r=g&s=64&d=retro">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">mazzemotto</span>
                        <span class="customer-reviews__user-date">Jan 11, 2021</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                    <div id="star-rating-block-13955" class="star-rating-block">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                    Like always, the quality is unreal. Great communication, open and forthright, definitely one of the best!
                </span>
                    <img class="customer-reviews__avatar" src="https://secure.gravatar.com/avatar/75d9273ff688b23a26baeb1d242bf61b?r=g&s=64&d=retro">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">helgish</span>
                        <span class="customer-reviews__user-date">Jan 6, 2021</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                    <div id="star-rating-block-13955" class="star-rating-block">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                    Very happy with the delivered parts, exactly as ordered.
                </span>
                    <img class="customer-reviews__avatar" src="https://static.treatstock.com/static/user/google_114025802608639125611/avatar_64x64.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">m</span>
                        <span class="customer-reviews__user-date">Jan 6, 2021</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                    <div id="star-rating-block-13955" class="star-rating-block">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                    Really good quality and quick shipping. Would highly recommend!
                </span>
                    <img class="customer-reviews__avatar" src="https://static.treatstock.com/static/user/google_104722062749600656013/avatar_64x64.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">blacksadify</span>
                        <span class="customer-reviews__user-date">Dec 29, 2020</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                    <div id="star-rating-block-13955" class="star-rating-block">
                        <div class="star-rating rating-xs rating-disabled">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                            </div>
                        </div>
                    </div>
                </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                    Excellent work. Completely satisfied and would hire this printer again. Bravo.
                </span>
                    <img class="customer-reviews__avatar" src="https://static.treatstock.com/static/user/0095699549f68634cbf32d574ebb27aa/avatar_1589347905_64x64.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">shintownalle</span>
                        <span class="customer-reviews__user-date">Dec 1, 2020</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                    </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                        Great as always
                    </span>
                    <img class="customer-reviews__avatar" src="https://secure.gravatar.com/avatar/a7b114b45308910612758f50301bd9fe?r=g&s=64&d=retro">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">E150</span>
                        <span class="customer-reviews__user-date">Dec 1, 2020</span>
                    </div>
                </div>

                <div class="customer-reviews__slider-item swiper-slide">
                <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5" step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                    </span>
                    <span class="customer-reviews__review customer-reviews__review--big">
                        Very knowledgeable and great pricing. Fast shipping and fast communication. Will use for future projects.
                    </span>
                    <img class="customer-reviews__avatar" src="https://secure.gravatar.com/avatar/34a794a7208a92504c91e24c991d5f73?r=g&s=64&d=retro">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">mazzemotto</span>
                        <span class="customer-reviews__user-date">Nov 24, 2020</span>
                    </div>
                </div>

            </div>

            <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
        </div>

    </div>
    <?php
}
?>
    <div class="container trust-logo-container">
        <h3 class="trust-logo-title">
            <?= _t('front', 'Trusted by'); ?>
        </h3>

        <div class="trust-logo__slider swiper-container">
            <div class="swiper-wrapper">
                <div class="trust-logo__slider-item swiper-slide">
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/apple.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/tesla.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/stanford.png"/>
                </div>
                <div class="trust-logo__slider-item swiper-slide">
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/google.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/ames.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/samsung.png"/>
                </div>
                <div class="trust-logo__slider-item swiper-slide">
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/adobe.png"/
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/aerospace.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/whirlpool.png"/>
                </div>
                <div class="trust-logo__slider-item swiper-slide">
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/huawei.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/verizon.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/bayer.png"/>
                </div>
                <div class="trust-logo__slider-item swiper-slide">
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/atos.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/3dsys.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/bmw.png"/>
                </div>
                <div class="trust-logo__slider-item swiper-slide">
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/cmu.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/columbia.png"/>
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/deloitte.png"/>
                </div>
                <div class="trust-logo__slider-item swiper-slide">
                    <img class="trust-logo__img" src="https://static.treatstock.com/static/images/clients-logos/sjsu.png"/>
                </div>
            </div>

            <div class="trust-logo__pagination"></div>
        </div>
    </div>


<?php /*
    <div class="container" id="upload-guide-container">
        <div class="row">
            <div class="col-xs-12">
                <?php $this->render('uploadGuide') ?>
            </div>
        </div>
    </div>
*/ ?>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        function initPrints() {
            //Init slider for .customer-reviews__slider
            var swiperPrints = new Swiper('.customer-reviews__slider', {
                scrollbar: '.customer-reviews__scrollbar',
                scrollbarHide: true,
                slidesPerView: 'auto',
                grabCursor: true
            });
        }

        initPrints();
        $(window).resize(function () {
            initPrints()
        });


        function initTrust() {
            //Init slider for .trust-logo__slider
            var swiperTrust = new Swiper('.trust-logo__slider', {
                pagination: '.trust-logo__pagination',
                paginationClickable: true,
                slidesPerView: 'auto',
                spaceBetween: 60,
                grabCursor: true,
            });
        }

        initTrust();
        $(window).resize(function () {
            initTrust()
        });



        <?php $this->endBlock(); ?>
    </script>
<?php $this->registerJs($this->blocks['js1']); ?>