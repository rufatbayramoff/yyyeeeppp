<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 19.12.16
 * Time: 11:15
 */
?>
<div class="upload-guide">
    <h2 class="m-b20"><?= _t('site.upload', 'Upload Guide'); ?></h2>

    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <p><strong><?= _t('site.upload', 'Single File 3D Model'); ?></strong></p>

            <p class="m-b0"><img alt="" class="img-responsive" src="https://static.treatstock.com/static/uploads/ck_20170113_upload-help-1.png" style="padding:0 10px">
            </p>

            <p class="m-b30"><?= _t('site.upload', 'If your 3D model is printed as one piece, you can upload it as a single file.'); ?></p>
        </div>

        <div class="col-xs-12 col-sm-4">
            <p><strong><?= _t('site.upload', 'Multiple Files of the 3D Model'); ?></strong></p>

            <p class="m-b0"><img alt="" class="img-responsive" src="https://static.treatstock.com/static/uploads/ck_20170113_upload-help-2.png" style="padding:0 10px">
            </p>

            <p class="m-b30"><?= _t(
                    'site.upload',
                    'If your 3D model is a kit which consists of several parts, you can upload all the files by adding or dragging them into the upload window.'
                ); ?></p>
        </div>

        <div class="col-xs-12 col-sm-4">
            <p><strong><?= _t('site.upload', 'Duplicate Files of the 3D Model'); ?></strong></p>

            <p class="m-b0"><img alt="" class="img-responsive" src="https://static.treatstock.com/static/uploads/ck_20170113_upload-help-3.png" style="padding:0 10px">
            </p>

            <p class="m-b30"><?= _t(
                    'site.upload',
                    'If your 3D model is a kit which consists of duplicate parts, upload only one file for the duplicate parts and set the quantity required for the kit.'
                ); ?> </p>
        </div>
    </div>
</div>
