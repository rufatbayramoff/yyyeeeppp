<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 02.11.17
 * Time: 12:05
 */
?>
<div class="modal fade modal-warning" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><?= _t('site.ps', 'Confirm') ?></h4>
            </div>
            <div class="modal-body">
                <p><?= _t('site.delivery', "The address you have specified doesn't seem to be correct. Please confirm your address:") ?></p>
                <div><?= _t('site.delivery', "Street:") ?> {{deliveryForm.street}}</div>
                <div ng-if="deliveryForm.street2"><?= _t('site.delivery', "Street2:") ?> {{deliveryForm.street2}}</div>
                <div><?= _t('site.delivery', "City:") ?> {{deliveryForm.city}}</div>
                <div ng-if="deliveryForm.state"><?= _t('site.delivery', "State:") ?> {{deliveryForm.state}}</div>
                <div><?= _t('site.delivery', "Country:") ?> {{deliveryForm.country}}</div>
                <p class="m-t10"><?= _t('site.delivery', "Would you like to use the suggested address?") ?></p>
            </div>
            <div class="modal-footer">
                <button
                        ng-click="declineAddress()"
                        type="button" class="btn btn-default"><?= _t('site.ps', 'Cancel') ?></button>
                <button
                        ng-click="confirmAddress()"
                        type="button" class="btn btn-primary"><?= _t('site.ps', 'Confirm') ?></button>
            </div>
        </div>
    </div>
</div>
