<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.11.17
 * Time: 17:04
 */

use kartik\widgets\Select2; ?>

<div class="col-sm-4 delivery-details wide-padding wide-padding--right">
    <h3 class="delivery-details__title"><?= _t('front.site', "Recipient's details"); ?></h3>
    <div class="row">
        <div class="form-group col-sm-12 field-deliveryform-first_name">
            <label class="control-label" for="deliveryform-first_name"><?= _t('site.delivery', 'First name') ?> <span class="form-required">*</span></label>
            <input name="deliveryform-contact_name" class="form-control" ng-model="deliveryForm.firstName">
            <p class="help-block help-block-error" validation-for="deliveryform-first_name"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-last_name">
            <label class="control-label" for="deliveryform-last_name"><?= _t('site.delivery', 'Last name') ?> <span class="form-required">*</span></label>
            <input name="deliveryform-last_name" class="form-control" ng-model="deliveryForm.lastName">
            <p class="help-block help-block-error" validation-for="deliveryform-last_name"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-phone">
            <label class="control-label ps-phone-label"
                   for="deliveryform-phone"><?= _t('site.store', 'Phone'); ?></label>
            <div class="ps-phone-code" name="deliveryform-phone">
                <select class="form-control js-phone-code-select"
                        ng-model="deliveryForm.phoneCountyIso" ng-init="initSelectPhone()">
                </select>
            </div>
            <div class="ps-phone-field">
                <input name="deliveryform-phone" ng-model="deliveryForm.phoneLineNumber"
                       type="text" class="form-control"
                       placeholder="<?= _t('site.ps', 'Your phone number') ?>" autocomplete="off">
            </div>
            <p class="help-block help-block-error" validation-for="deliveryform-phone"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-email">
            <label class="control-label" for="deliveryform-email"><?= _t('site.delivery', 'Email'); ?> <span class="form-required">*</span></label>
            <input name="deliveryform-email" class="form-control" ng-model="deliveryForm.email">
            <p class="help-block help-block-error" validation-for="deliveryform-email"></p>
        </div>
        <div class="form-group field-deliveryform-deliverytype">
            <input type="hidden" name="deliveryform-deliverytype" class="form-control" value="pickup">
            <p class="help-block help-block-error"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-email">
            <span class="form-required">*</span> <?= _t('site.store', 'Required'); ?>
        </div>
    </div>
</div>

<div class="col-sm-8">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <?= _t('site.delivery', 'Pickup address'); ?>
            </h3>
            <div>
                {{psMachineDeliveryInfo.location.address}}
            </div>
            <div>
                <label><?= _t('site.delivery', 'Working hours:'); ?></label> {{psMachineDeliveryInfo.workTime}}
            </div>
        </div>
        <div class="col-md-12">
            <div printer-location-map="psMachineDeliveryInfo.location" style="height:300px; width:100%; margin: 15px 0; border-radius: 5px;"></div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <h4 class="delivery-details-type__title border-0 m-t20 m-b0">
        <?= _t('site.store', 'Comment'); ?>
    </h4>
    <div class="form-group field-deliveryform-comment">
        <textarea name="deliveryform-comment" class="form-control" ng-model="deliveryForm.comment" placeholder="<?= _t(
            'site.delivery',
            'Leave comments if you have any requests, instructions or additional information regarding the production of your order. Please specify if there are parts that need to fit one another so the manufacturer can check them before shipping your order.'
        ) ?>" maxlength="600"></textarea>
        <p class="help-block help-block-error" validation-for="deliveryform-comment"></p>
    </div>
</div>
