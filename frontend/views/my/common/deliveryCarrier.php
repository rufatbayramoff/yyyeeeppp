<?php
/**
 * Created by PhpStorm.
 * User: analitic
 * Date: 01.11.17
 * Time: 17:04
 */

use common\models\GeoCountry;

?>

<div class="row">
    <div class="col-lg-12 p-l30">
        <h3 class="delivery-details__title"><?= _t('site.delivery', 'Shipping information'); ?></h3>
        <p ng-if="deliveryForm.deliveryTypeCarrier=='treatstock' && deliveryForm.countryIso!='US'" class="text-info">
            <?= _t('order.delivery', 'Please fill in the delivery form using the Latin alphabet only'); ?>
        </p>
    </div>
</div>

<div class="row">
    <div ng-if="isRateLoad" class="form-group col-sm-6 p-l30">
        <div ng-if="standardShipping && standardShipping.amountWithCurrency()" class="radio radio-primary">
            <input ng-model="deliveryForm.shipping" type="radio" name="shipping" id="standard-shipping" value="standard">
            <label for="standard-shipping">
                <?php echo _t('site.store', 'Standard Shipping'); ?>
            </label>
            ({{standardShipping.amountWithCurrency()}})
        </div>
        <div class="radio radio-primary" ng-if="deliveryForm.needExpress() && psMachineDeliveryInfo.isUsa() && expressShipping">
            <input ng-model="deliveryForm.shipping" type="radio" name="shipping" id="express-shipping" value="express">
            <label for="express-shipping">
                <?php echo _t('site.store', 'Express Shipping'); ?> ({{expressShipping.amountWithCurrency()}})
                <span class="help-block"></span>
            </label>
        </div>
    </div>

    <div ng-if="offersBundleData.timeAround" class="form-group col-sm-6 p-l30">
        <label title="<?= _t('site.store', 'Turnaround time') ?>"><?= _t('site.store', 'Turnaround time') ?></label>
            {{timeAroundDay()}} days
            <a class="m-l5" ng-click="timeAroundModal()" href="#help">
                <span class="tsi tsi-question-c"></span>
            </a>
    </div>
</div>

<div class="col-sm-4 delivery-details wide-padding wide-padding--right">

    <div class="row">
        <div class="form-group col-sm-12 field-deliveryform-first_name">
            <label class="control-label" for="deliveryform-first_name"><?= _t('site.store', 'First Name'); ?> <span
                        class="form-required">*</span></label>
            <input name="deliveryform-first_name" class="form-control" ng-model="deliveryForm.firstName">
            <p class="help-block help-block-error" validation-for="deliveryform-first_name"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-last_name">
            <label class="control-label" for="deliveryform-last_name"><?= _t('site.store', 'Last Name'); ?> <span
                        class="form-required">*</span></label>
            <input name="deliveryform-last_name" class="form-control" ng-model="deliveryForm.lastName">
            <p class="help-block help-block-error" validation-for="deliveryform-last_name"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-company">
            <label class="control-label" for="deliveryform-company"><?= _t('site.store', 'Company'); ?></label>
            <input name="deliveryform-company" class="form-control" ng-model="deliveryForm.company">
            <p class="help-block help-block-error" validation-for="deliveryform-company"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-phone">
            <label class="control-label ps-phone-label"
                   for="deliveryform-phone"><?= _t('site.store', 'Phone'); ?></label>
            <div class="ps-phone-code" name="deliveryform-phone">
                <select class="form-control js-phone-code-select"
                        ng-model="deliveryForm.phoneCountyIso" ng-init="initSelectPhone()">
                </select>
            </div>
            <div class="ps-phone-field">
                <input name="deliveryform-phone" ng-model="deliveryForm.phoneLineNumber"
                       type="text" class="form-control"
                       placeholder="<?= _t('site.ps', 'Your phone number') ?>" autocomplete="off">
            </div>
            <p class="help-block help-block-error" validation-for="deliveryform-phone"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-email">
            <label class="control-label" for="deliveryform-email"><?= _t('site.store', 'Email'); ?> <span
                        class="form-required">*</span></label>
            <input name="deliveryform-email" class="form-control" ng-model="deliveryForm.email">
            <p class="help-block help-block-error" validation-for="deliveryform-email"></p>
        </div>
        <div class="form-group col-sm-12 field-deliveryform-email">
            <span class="form-required">*</span> <?= _t('site.store', 'Required'); ?>
        </div>

    </div>
</div>

<div class="col-sm-8 ">
    <div class="row ">
        <div class="col-sm-6 form-group field-deliveryform-street required">
            <label class="control-label" for="deliveryform-street"><?= _t('site.store', 'Street Address'); ?> <span
                        class="form-required">*</span></label>
            <input name="deliveryform-street" class="form-control" ng-model="deliveryForm.street" aria-required="true"
                   maxlength="40">
            <p class="help-block help-block-error" validation-for="deliveryform-street"></p>
        </div>

        <div class="col-sm-6 form-group field-deliveryform-street2">
            <label class="control-label" for="deliveryform-street2"><?= _t('site.store', 'Extended Address'); ?></label>
            <input name="deliveryform-street2" class="form-control" ng-model="deliveryForm.street2">
            <p class="help-block help-block-error" validation-for="deliveryform-street2"></p>
        </div>

        <div class="col-sm-6 form-group field-deliveryform-city required">
            <label class="control-label" for="deliveryform-city"><?= _t('site.store', 'City'); ?> <span
                        class="form-required">*</span></label>
            <input name="deliveryform-city" class="form-control" ng-model="deliveryForm.city" aria-required="true">
            <p class="help-block help-block-error" validation-for="deliveryform-city"></p>
        </div>

        <div class="col-sm-6 form-group field-deliveryform-state">
            <label class="control-label" for="deliveryform-state"><?= _t('site.store', 'State/Region'); ?> <span
                        class="form-required">*</span></label>
            <input name="deliveryform-state" class="form-control" ng-model="deliveryForm.state">
            <p class="help-block help-block-error" validation-for="deliveryform-state"></p>
        </div>


        <div class="col-sm-4 form-group field-deliveryform-zip required">
            <label class="control-label" for="deliveryform-zip"><?= _t('site.store', 'Zip'); ?> <span
                        class="form-required">*</span></label>
            <input name="deliveryform-zip" class="form-control" ng-model="deliveryForm.zip" aria-required="true">
            <p class="help-block help-block-error" validation-for="deliveryform-zip"></p>
        </div>
        <div class="col-sm-8 form-group field-deliveryform-country required">
            <label class="control-label" for="deliveryform-country"><?= _t('site.store', 'Country'); ?></label>
            <select name="deliveryform-country" class="form-control" ng-model="deliveryForm.countryIso"
                    placeholder="Select"
                    aria-required="true" ng-change="resetDeliveryForm()">
                <?php foreach (GeoCountry::getCombo('iso_code') as $countryIso => $countryTitle) {
                    ?>
                    <option value="<?= $countryIso ?>" ng-selected="deliveryForm.countryIso == '<?= $countryIso; ?>'"
                            ng-if="allowShowCountryIsoSelect('<?= $countryIso; ?>')"><?= $countryTitle ?></option>
                    <?php
                }
                ?>
            </select>
            <p class="help-block help-block-error" validation-for="deliveryform-country"></p>
        </div>

        <div class="col-md-12">

            <input id="autocomplete"
                   google-address-input=""
                   on-address-change="onAutocompleteAddressChange(address)"
                   type="text" class="form-control"/>
        </div>
        <div class="col-md-12">
            <div google-map="deliveryForm"
                 on-marker-position-change="onMapMarkerPositionChange(address)"
                 id="map"
                 style="height: 250px; margin-top: 15px; margin-bottom: 15px; border-radius: 5px;"></div>
        </div>
    </div>
</div>


<div class="col-sm-12">
    <h4 class="delivery-details-type__title border-0 m-t20 m-b0">
        <?= _t('site.store', 'Comment'); ?>
    </h4>
    <div class="form-group">
        <textarea name="deliveryform-comment" class="form-control" ng-model="deliveryForm.comment" placeholder="<?= _t(
            'site.store',
            'Leave comments if you have any requests, instructions or additional information regarding the production of your order. Please specify if there are parts that need to fit one another so the manufacturer can check them before shipping your order.'
        ); ?>" maxlength="600"></textarea>
        <p class="help-block help-block-error" validation-for="deliveryform-comment"></p>
    </div>
</div>




