<?php

?>
<div class="modal fade modal-primary" tabindex="-1" role="dialog" ng-controller="materials-guide">
    <div class="modal-dialog modal-model3d-preview">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><?= _t('site.printModel3d', 'Materials Guide') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row preloader" ng-if="currentMaterial===null">
                    <?= _t('site.common', 'Loading...') ?>
                </div>
                <h4 ng-if="currentMaterial===false">
                    <?= _t('site.common', 'None.') ?>
                </h4>
                <div class="row" ng-if="currentMaterial">
                    <div class="col-sm-3">
                        <h4 class="material__title">
                            <?= _t('site.printModel3d', 'Materials'); ?>
                        </h4>
                        <div class="material-switcher material-switcher--material-guide">
                            <span ng-repeat="wikiMaterial in wikiMaterialsList.list">
                                <div class="material-switcher__item"
                                     ng-class="{'material-switcher__item--active':wikiMaterial.code == currentMaterial.code}"
                                     ng-click="selectMaterial(wikiMaterial)">
                                    {{wikiMaterial.title}}
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-9 wide-padding wide-padding--left">
                        <h2 class="m-t0">{{currentMaterial.title}}</h2>

                        <div class="row">
                            <div class="col-sm-6 wide-padding wide-padding--right">
                                <div class="materials-guide-pic">
                                    <img ng-src="{{currentMaterial.firstPhotoUrl}}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4 class="materials-guide-title m-t0"><?= _t('site.printModel3d', 'About'); ?></h4>
                                <p ng-bind-html="currentMaterial.about"></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="materials-guide-title"><?= _t('site.printModel3d', 'Pros'); ?></h4>
                                <p ng-bind-html="currentMaterial.advantages"></p>
                            </div>
                            <div class="col-sm-6">
                                <h4 class="materials-guide-title"><?= _t('site.printModel3d', 'Cons'); ?></h4>
                                <p ng-bind-html="currentMaterial.disadvantages"></p>
                            </div>
                        </div>

                        <a ng-if="!currentMaterial.show_widget" ng-href="{{currentMaterial.url}}" class="materials-guide-more"
                           target="_blank"><?= _t('site.printModel3d', 'Learn more'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




