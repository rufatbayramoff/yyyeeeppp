<?php

use frontend\assets\FlagsAsset;

$selectCountyCodes = [];
foreach (\common\models\GeoCountry::getPhoneCodesIso() as $iso => $text) {
    $selectCountyCodes[] = [
        'id'   => $iso,
        'text' => $text
    ];
}

Yii::$app->angular
    ->controller('store/checkout/deliveryDirectives')
    ->controllerParams([
        'phoneCountryCodes' => $selectCountyCodes
    ]);

FlagsAsset::register($this);
$this->registerAssetBundle(\frontend\assets\GoogleMapAsset::class);

?>
<div class="tab-pane" id="delivery">
    <div style="margin: 15px 0 0 0;">
        <div class="row delivery-progress" ng-if="!deliveryForm.loaded">
            <br>
            <br>
            <br>
            <br>
            <img src="/static/images/preloader.gif" width="60" height="60">
            <br>
            <br>
            <br>
            <br>
        </div>

        <div ng-if="deliveryForm.loaded">
            <h3 class="delivery-options__title"></h3>
            <div class="row" ng-if="psMachineDeliveryInfo.havePickupAndDelivery()">
                <div class="col-sm-12 m-b10">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="radio radio-primary radio-inline">
                                <input ng-model="deliveryForm.deliveryType" type="radio" value="pickup"
                                       id="delivery_type_pickup"/>
                                <label for="delivery_type_pickup"><?= _t('site.delivery', 'Pickup') ?></label>
                            </div>
                            <div class="radio radio-primary radio-inline">
                                <input ng-model="deliveryForm.deliveryType" type="radio" value="standard"
                                       id="delivery_type_delivery"/>
                                <label for="delivery_type_delivery"><?= _t('site.delivery', 'Delivery') ?></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div ng-if="deliveryForm.deliveryType == DELIVERY_TYPE_PICKUP" class="row js-type-pickup">
                <?=
                $this->render('deliveryPickup');
                ?>
            </div>
            <div ng-if="deliveryForm.deliveryType == DELIVERY_TYPE_DELIVERY || deliveryForm.deliveryType == DELIVERY_TYPE_INTL"
                 class="row js-type-delivery">

                <?=
                $this->render('deliveryCarrier');
                ?>
            </div>
        </div>
    </div>

    <div class="aff-widget-bottom">
        <button ng-click="prevStep()" class="btn btn-default">
            <span class="tsi tsi-left"></span>
            <?= _t('site.common', 'Back'); ?>
        </button>
        <button class="btn btn-primary" ng-click="nextStep()" ng-disabled="showProgress">
            <span ng-if="showProgress">
              <i class="tsi tsi-refresh tsi-spin-custom"></i> <?= _t('front.user', 'Please wait...'); ?>
              </span>
            <span ng-if="!showProgress">
                <?= _t('site.common', 'Next'); ?>
                <span class="tsi tsi-right"></span>
            </span>
        </button>
    </div>
</div>
