<?php

use common\models\PaymentTransaction;
use common\models\UserAddress;
use lib\delivery\delivery\DeliveryFacade;
use yii\helpers\Html;
use yii\helpers\Url;

Yii::$app->angular
    ->controller([
        'payment/models/PayOrder',
        'payment/models/PaymentInvoice',
        'payment/directives/checkoutPaymentMethods'
    ]);

?>
<div class="tab-pane">
    <div class="row">
        <div class="col-sm-7 col-md-8 wide-padding wide-padding--right">
            <h1><?php echo _t('site.store', 'Payment'); ?></h1>
            <div id="checkout-step-loading" style="text-align: center;margin: 0 0 30px;"><img src="/static/images/loading.gif"> <?= _t('site.payment', 'loading...'); ?></div>
            <checkout-payment-methods content="cardHtml" payment-data="paymentData"></checkout-payment-methods>
        </div>

        <div class="col-sm-5 col-md-4 m-t30" ng-cloak>
            <div class="panel panel-default" ng-if="hasBillingDetails()">
                <div class="panel-heading">
                    <h3><?php echo _t('site.store', 'Billing Details'); ?></h3>
                </div>

                <div class="panel-body">
                    <div ng-repeat="invoiceItem in payOrder.primaryPaymentInvoice.invoiceItems" class="billing-row">
                        <div class="billing-row__block">{{invoiceItem.title}}</div>
                        <div class="billing-row__block"><strong>{{invoiceItem.total_line}}</strong></div>
                    </div>
                </div>

                <div class="panel-body" ng-if="payOrder.primaryPaymentInvoice.accruedBonus && payOrder.primaryPaymentInvoice.accruedBonus.amount">
                    <div class="billing-row">
                        <div class="billing-row__block"><?php echo _t('site.store', 'Accrued bonus') ?></div>
                        <div class="billing-row__block"><strong>+ {{payOrder.primaryPaymentInvoice.accruedBonus.amount}}</strong></div>
                    </div>
                </div>

                <div style="padding: 15px;" ng-if="payOrder.primaryPaymentInvoice.hasPromoCode()">
                    <?php echo _t('site.store', 'Promo codes:') ?>
                    <div class="row m-b10">
                        <div class="col-xs-12">
                            {{payOrder.primaryPaymentInvoice.storeOrderPromocode.promocode}}
                            <button class="btn btn-default btn-sm"
                                    title="<?= _t('site.store', 'Delete code'); ?>"
                                    style="padding: 4px;font-size: 14px;"
                                    ng-click="removePromoCode($event)">&times;
                            </button>
                        </div>
                    </div>
                    <div class="delivery-order__delivery-price">
                        <div class="delivery-order__delivery-price__price delivery_price price__badge">
                            <span class="price__badge">{{payOrder.primaryPaymentInvoice.storeOrderPromocode.amountTotal}}</span>
                        </div>
                        <h5 class="delivery-order__delivery-price__title"><?= _t('site.store', 'Total Discount'); ?>
                            :</h5>
                    </div>
                </div>

                <div class="delivery-order__promo" ng-if="payOrder.primaryPaymentInvoice.allowPromoCode && !payOrder.primaryPaymentInvoice.hasPromoCode()">
                    <div class="delivery-order__promo-label">
                        <?= _t('site.store', 'Promo code?'); ?>
                    </div>
                    <div class="delivery-order__promo-actions input-group">
                        <input class="form-control input-sm"
                               type="text"
                               ng-model="promocodeData.code"
                               name="promocode" id="promocode"
                        />
                        <span class="input-group-btn">
                        <?php echo Html::button(_t('site.store', 'Apply'), [
                            'class'    => 'btn btn-info btn-sm',
                            'ng-click' => 'applyPromoCode($event)'
                        ]); ?>
                    </span>
                    </div>
                </div>

                <div class="panel-price">
                    <div class="panel-price__body">
                        <div class="panel-price__body__price total_price">
                            <?php echo _t('site.store', 'Total') ?>
                        </div>
                        <div class="pull-right panel-price__body__price ">
                            {{payOrder.primaryPaymentInvoice.amountTotal}}
                        </div>
                    </div>
                </div>
            </div>


            <div class="panel panel-default" ng-if="(payOrder.model3d.parts|numkeys)>0">
                <div class="panel-heading printed-file__title">
                    <h3>
                        <span ng-if="(payOrder.model3d.parts|numkeys) <= 1" class="m-r10">
                            {{payOrder.model3d.parts | numkeys}} <?= _t('site.store', 'File'); ?>
                        </span>
                        <span ng-if="(payOrder.model3d.parts|numkeys) > 1" class="m-r10">
                            {{payOrder.model3d.parts | numkeys}} <?= _t('site.store', 'Files'); ?>
                        </span>
                        <span class="printed-file__toggle collapsed" data-toggle="collapse" data-target="#fileList" aria-expanded="false" aria-controls="fileList">
                            <?= _t('site.store', 'Show'); ?><span class="tsi tsi-down"></span>
                        </span>
                    </h3>
                </div>
                <div class="panel-body collapse p-b0" id="fileList">
                    <div class="printed-file-list p-b15">

                        <div class="printed-file__item" ng-repeat="model3dPart in model3d.parts">
                            <div class="printed-file__content" ng-if="!model3dPart.isCanceled && model3dPart.qty">
                                <div class="printed-file__pic">
                                    <img ng-src="{{getModel3dPartPreviewUrl(model3dPart)}}" main-src="{{getModel3dPartPreviewUrl(model3dPart)}}" alt-src="/static/images/preloader80.gif"
                                         onerror="commonJs.checkAltSrcImage(this);"></div>
                                <div class="printed-file__body">
                                    <div class="printed-file__name" title="{{model3dPart.title}}">
                                        {{model3dPart.title}}
                                    </div>
                                    <div class="printed-file__material">
                                        <div title="{{model3dTextureState.getTexture(model3dPart).getPrinterMaterialTitle()}}" class="material-item">
                                            <div class="material-item__color" style="background-color: #{{model3dTextureState.getTexture(model3dPart).printerColor.getRgbHex()}}"></div>
                                        </div>
                                        {{model3dTextureState.getTexture(model3dPart).getPrinterMaterialTitle()}}
                                    </div>
                                </div>
                                <div class="printed-file__count">
                                    x{{model3dPart.qty}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" ng-if="hasAddress()">
                <div class="panel-heading">
                    <h3>
                        <span ng-if="payOrder.isPickupDelivery"><?php echo _t('site.store', 'Pickup Address'); ?></span>
                        <span ng-if="!payOrder.isPickupDelivery"><?php echo _t('site.store', 'Shipping Address'); ?></span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="detail-view--custom-view">

                        <table class="table table-striped table-bordered detail-view">
                            <tbody>
                            <tr ng-if="!payOrder.isPickupDelivery && payOrder.shipAddress.contactName">
                                <th>
                                    <span><?php echo _t('site.store', 'Name'); ?></span>
                                </th>
                                <td>
                                    {{payOrder.shipAddress.contactName}}
                                </td>
                            </tr>
                            <tr ng-if="!payOrder.isPickupDelivery && payOrder.shipAddress.companyName">
                                <th>
                                    <span><?php echo _t('site.store', 'Company'); ?></span>
                                </th>
                                <td>
                                    {{payOrder.shipAddress.companyName}}
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo _t('site.store', 'Address'); ?></th>
                                <td>
                                    <span ng-bind-html="payOrder.shipAddress.address"></span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="alert alert-info alert--text-normal delivery-printing-alert m-t10">
                        <?php echo _t('site.order', 'Please note that the manufacturer may be changed if it\'s necessary to complete the order.'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="aff-widget-bottom padding">
        <button ng-click="prevStep()" class="btn btn-default">
            <span class="tsi tsi-left"></span>
            <?php echo _t('site.common', 'Back'); ?>
        </button>
    </div>
</div>
