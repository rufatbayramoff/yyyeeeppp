<div class="modal fade modal-primary" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= _t('site.printModel3d', 'Turnaround time') ?></h4>
            </div>
            <div class="modal-body">
                <p class="m-t15 m-b0">
                    <?= _t('site.printModel3d', 'Turnaround time is an estimated time your order will take to be manufactured and delivered using standard shipping. This is an approximate estimate based on analyzing timelines of similar orders.') ?>
                </p>
            </div>
        </div>
    </div>
</div>
