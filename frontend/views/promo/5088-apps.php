<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>

<div class="container apps-page">
    <div class="row">
        <div class="col-xs-12">
            <ol class="breadcrumb apps-page__breadcrumb">
                <li><a href="/apps/"><?= _t('site.page', 'Apps'); ?></a></li>
                <li><a href="/apps/appName">Watermark 3D</a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3 col-md-2">
            <img src="https://static.treatstock.com/static/images/apps/wm.svg" alt="Watermark3d" class="apps-page__logo">

            <a href="https://www.watermark3d.com/" class="btn btn-primary btn-block m-b30 p-l10 p-r10 hidden-xs" target="_blank"><?= _t('site.page', 'Launch'); ?></a>
        </div>

        <div class="col-sm-9 col-md-9 wide-padding wide-padding--left">

            <h2 class="m-t0 m-b20">Watermark 3D</h2>

            <a href="https://www.watermark3d.com/" class="btn btn-primary btn-block m-b30 p-l10 p-r10 visible-xs" target="_blank"><?= _t('site.page', 'Launch'); ?></a>

            <div class="apps-page__card">

                <ul class="nav nav-tabs nav-tabs--secondary" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#app-info" aria-controls="app-info" role="tab" data-toggle="tab" aria-expanded="true"><?= _t('site.page', 'Info'); ?></a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#app-instuctions" aria-controls="app-instuctions" role="tab" data-toggle="tab" aria-expanded="false"><?= _t('site.page', 'Instructions'); ?></a>
                    </li>
                </ul>

                <div class="tab-content apps-page__info">
                    <div class="tab-pane active" id="app-info">
                        <p>Watermark 3D is a free software application used to embed unique information into a 3D model and protected by a password. The watermark is practically invisible, leaving no visual or structural changes that are revealed on the materialized object (at this stage, only STL files are supported but we are working on including other formats as well). The only way the watermark can be detected is by uploading the file to watermark3D.com and entering the user-created password. If the password is correct, the watermark can be detected and the concealed information inside the file can reveal the owner of the IP and its origins without malicious users being aware of it. To start using the free watermarking service, visit www.watermark3D.com</p>

                        <p>After applying a watermark to a 3D model, it can be tracked via several ways. As mentioned earlier, anyone can use the website to upload and detect watermarks manually. However, we are also developing a system that can track and automatically notify the author of the watermark when their file appears on a website to indicate the legal and illegal distribution of the file. Nevertheless, the system already works perfectly for those who want to protect the 3D model before giving access to someone. Imagine that a designer distributes a file using a number of different channels. In this case, they can place a unique watermark each time they send the file to a customer, a manufacturer or marketplace. Then, if the model is found to be distributed online illegally, they should be able to trace the source back to the culprit and use this as grounds to file a claim.</p>

                        <img src="/static/images/howto-pic-1.png">
                    </div>

                    <div class="tab-pane" id="app-instuctions">

                        <p>Check out the short video on how watermark3d.com works.</p>

                        <div class="video-container">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/CYijJ4rrktw?rel=0" frameborder="0" allowfullscreen=""></iframe>
                        </div>

                        <h4>Step 1: Watermarking the 3D Model</h4>

                        <p>Click the “Upload STL” tab and choose the file you want to watermark. Please keep in mind that only binary and ASCII STL files are available for now, and color extensions are not supported.</p>

                        <h4>Step 2: Enter Watermark Information</h4>

                        <p>Type your name, company title or contact information. It will be hidden inside the watermark. Please try to keep it as brief as possible. ASCII characters and symbols recommended. Create and enter your password in the relevant field. You may leave the password field empty if you want the contents of your watermark to be made public to all users. Don’t worry, even without a password, your watermark cannot be changed or overwritten once it’s applied.</p>

                        <h4>Step 3: Apply Watermark</h4>

                        <p>Enter your email address and click “Apply watermark”. The download link for your first watermarked file will be sent to this email. All subsequent downloads will occur directly on the site.</p>

                        <p>Please note: Before applying the watermark, you have the option of selecting advanced mode. Advanced mode will make the watermark more robust but will cause slight changes (practically invisible) to the 3D model.</p>

                        <h4>Step 4: Check Watermark</h4>

                        <p>Upload the watermarked STL. To view the contents of a private watermark, the correct password is required. If the watermark was made public to all users, you can retrieve the information without entering password.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>