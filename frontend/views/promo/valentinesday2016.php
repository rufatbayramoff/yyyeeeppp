<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

$this->title = "Valentine's Day Contest for the Best Design";

?>

<div class="row wide-padding">

    <div class="col-md-8 col-md-offset-2">

        <h1 class="page-header text-center"><?php echo $this->title; ?></h1>

        <p>
            Valentine's Day is rapidly approaching and we invite designers to participate in our very first contest by sending us your ‘love theme’ 3D model.
        </p>

        <p>
            To participate, you will need to upload a STL file and assign the description tags “Valentinesday” and “contest.” The winner will be determined by the number of likes their 3D model receives. To vote, just click on the heart symbol at the top right corner of the model image. You can vote for an unlimited number of favorite models.
        </p>

        <p>
            The winner will be announced on February 15th 2016. The author of the model with the highest number of hearts will receive a gift card and the winning model will receive the title of "Best Valentine's Day Model 2016."
        </p>

    </div>

</div>