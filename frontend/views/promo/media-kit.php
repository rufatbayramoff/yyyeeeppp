<div class="">
    <div class="container container--wide p-t20 p-b30">
        <div class="row fb-grid fb-grid--va-center">
            <div class="col-sm-6">

                <h1 class="animated m-b20">
                    <?= _t('site.page', 'Advertise on Treatstock'); ?>
                </h1>

                <p class="lead">
                    <?= _t('site.page', 'Over 200k+ monthly visitors 6000+ 3D printing services'); ?>
                </p>

                <?php /*
                <p>
                    <?= _t('site.page', 'Describing body text block for more text content and some bla bla bla words.'); ?>
                </p>

                <ul class="checked-list">
                    <li>Choose from over 100 materials and colors</li>
                    <li>Compare instant prices to find the best deals online</li>
                    <li>Fast turnarounds with orders generally delivered within 5 days</li>
                    <li>All machines are tested and orders pass a quality control check</li>
                </ul>
                */?>

                <a href="/site/contact" target="_blank"  class="btn btn-primary m-t10 m-b10">
                    <?= _t('site.page', 'Send request'); ?>
                </a>

            </div>
            <div class="col-sm-6 wide-padding--left">
                <img class="img-responsive"
                     src="https://static.treatstock.com/static/images/common/catalog-media-kit.png"
                     alt="">
            </div>
        </div>
    </div>
</div>

<div class="bg-info affiliates-land">
    <div class="container">
        <div class="row text-center">
            <div class="col-sm-12">
                <h2 class="m-b30 animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                    <?= _t('site.ps', 'Why Advertise on Treatstock?'); ?>
                </h2>
            </div>
            <div class="col-sm-8 col-sm-offset-2">
                <p class="lead m-b30 animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                    <?= _t('site.ps', 'Advertise on Treatstock and your message will reach users and services interested in 3D printing and manufacturing only. '); ?>
                </p>
            </div>
        </div>

        <div class="row fb-grid fb-grid--va-center">
            <div class="col-xs-12 col-sm-6">

                <h3 class="m-t0 text-center animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                    <?= _t('site.ps', 'Our geo'); ?>
                </h3>

                <style>
                    .table.table-bordered {
                        width: auto;
                        margin: 0 auto 30px;
                        font-size: 16px;
                    }
                    .svg-ico {
                        width: 20px;
                        height: 20px;
                    }
                </style>

                <table class="table table-bordered animated"
                       data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                    <tr>
                        <td class="p-l0">
                            <svg class="svg-ico m-r10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <circle cx="256" cy="256" r="256" fill="#f0f0f0"/>
                                <g fill="#d80027">
                                    <path d="M244.87 256H512c0-23.1-3.08-45.49-8.82-66.78H244.87V256zM244.87 122.44h229.56a257.35 257.35 0 00-59.07-66.79H244.87v66.78zM256 512a254.9 254.9 0 00159.36-55.65H96.64A254.9 254.9 0 00256 512zM37.57 389.56h436.86a254.47 254.47 0 0028.75-66.78H8.82a254.47 254.47 0 0028.75 66.79z"/>
                                </g>
                                <path fill="#0052b4" d="M118.58 39.98h23.33l-21.7 15.76 8.3 25.51-21.7-15.76-21.7 15.76 7.15-22.04a257.4 257.4 0 00-49.65 55.34h7.48L36.27 124.6a255.58 255.58 0 00-6.19 10.94l6.6 20.3-12.31-8.95A253.57 253.57 0 0016 166.76l7.27 22.37h26.82l-21.7 15.76 8.29 25.51-21.7-15.77-13 9.45A258.47 258.47 0 000 256h256V0a254.79 254.79 0 00-137.42 39.98zm9.92 190.42l-21.7-15.77-21.7 15.77 8.3-25.5-21.7-15.77H98.5l8.3-25.51 8.28 25.5h26.82l-21.7 15.77 8.3 25.51zm-8.29-100.08l8.3 25.5-21.7-15.76-21.7 15.77 8.28-25.51-21.7-15.77h26.82l8.3-25.5 8.28 25.5h26.82l-21.7 15.77zM220.33 230.4l-21.7-15.77-21.7 15.77 8.29-25.5-21.7-15.77h26.82l8.29-25.51 8.29 25.5h26.82l-21.7 15.77 8.29 25.51zm-8.3-100.08l8.3 25.5-21.7-15.76-21.7 15.77 8.29-25.51-21.7-15.77h26.82l8.29-25.5 8.29 25.5h26.82l-21.7 15.77zm0-74.58l8.3 25.51-21.7-15.76-21.7 15.76 8.29-25.5-21.7-15.77h26.82l8.29-25.51 8.29 25.5h26.82l-21.7 15.77z"/>
                            </svg>
                            <?= _t('site.ps', 'United States'); ?>
                        </td>
                        <td class="text-right p-l20 p-r0">41.19%</td>
                    </tr>
                    <tr>
                        <td class="p-l0">
                            <svg class="svg-ico m-r10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <circle cx="256" cy="256" r="256" fill="#f0f0f0"/>
                                <g fill="#0052b4">
                                    <path d="M52.92 100.14a255.07 255.07 0 00-44.1 89.08H142l-89.08-89.08zM503.18 189.22a255.09 255.09 0 00-44.1-89.08L370 189.22h133.18zM8.82 322.78a255.1 255.1 0 0044.1 89.08l89.07-89.08H8.82zM411.86 52.92a255.08 255.08 0 00-89.08-44.1V142l89.08-89.08zM100.14 459.08a255.09 255.09 0 0089.08 44.1V370l-89.08 89.08zM189.22 8.82a255.1 255.1 0 00-89.08 44.1L189.22 142V8.81zM322.78 503.18a255.1 255.1 0 0089.08-44.1L322.78 370v133.18zM370 322.78l89.08 89.08a255.08 255.08 0 0044.1-89.08H370z"/>
                                </g>
                                <g fill="#d80027">
                                    <path d="M509.83 222.6H289.4V2.18a258.56 258.56 0 00-66.78 0V222.6H2.17a258.56 258.56 0 000 66.78H222.6v220.44a258.53 258.53 0 0066.78 0V289.4h220.44a258.53 258.53 0 000-66.78z"/>
                                    <path d="M322.78 322.78l114.24 114.24a256.64 256.64 0 0015.05-16.43l-97.8-97.8h-31.49zM189.22 322.78L74.98 437.02a256.64 256.64 0 0016.43 15.05l97.8-97.8v-31.49zM189.22 189.22L74.98 74.98a256.64 256.64 0 00-15.05 16.43l97.8 97.8h31.49zM322.78 189.22L437.02 74.98a256.33 256.33 0 00-16.43-15.05l-97.8 97.8v31.49z"/>
                                </g>
                            </svg>
                            <?= _t('site.ps', 'United Kingdom'); ?>
                        </td>
                        <td class="text-right p-l20 p-r0">8.18%</td>
                    </tr>
                    <tr>
                        <td class="p-l0">
                            <svg class="svg-ico m-r10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <circle cx="256" cy="256" r="256" fill="#f0f0f0"/>
                                <g fill="#d80027">
                                    <path d="M512 256c0-101.5-59.06-189.19-144.7-230.6v461.2C452.93 445.19 512 357.5 512 256zM0 256c0 101.5 59.06 189.19 144.7 230.6V25.4C59.07 66.81 0 154.5 0 256zM300.52 289.39l44.52-22.26L322.78 256v-22.26L278.26 256l22.26-44.52h-22.26L256 178.09l-22.26 33.39h-22.26L233.74 256l-44.52-22.26V256l-22.26 11.13 44.52 22.26-11.13 22.26h44.52v33.39h22.26v-33.39h44.52z"/>
                                </g>
                            </svg>
                            <?= _t('site.ps', 'Canada'); ?>
                        </td>
                        <td class="text-right p-l20 p-r0">4.05%</td>
                    </tr>
                    <tr>
                        <td class="p-l0">
                            <svg class="svg-ico m-r10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <path fill="#ffda44" d="M15.92 345.04C52.1 442.53 145.92 512 256 512s203.9-69.47 240.08-166.96L256 322.78 15.92 345.04z"/>
                                <path d="M256 0C145.93 0 52.1 69.47 15.92 166.96L256 189.22l240.08-22.26C459.9 69.47 366.08 0 256 0z"/>
                                <path fill="#d80027" d="M15.92 166.96C5.63 194.69 0 224.69 0 256s5.63 61.31 15.92 89.04h480.16C506.37 317.31 512 287.31 512 256s-5.63-61.31-15.92-89.04H15.92z"/>
                            </svg>
                            <?= _t('site.ps', 'Germany'); ?>
                        </td>
                        <td class="text-right p-l20 p-r0">5.73%</td>
                    </tr>
                    <tr>
                        <td class="p-l0">
                            <svg class="svg-ico m-r10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <circle cx="256" cy="256" r="256" fill="#f0f0f0"/>
                                <path fill="#d80027" d="M512 256c0-110.07-69.47-203.9-166.96-240.08v480.16C442.53 459.9 512 366.08 512 256z"/>
                                <path fill="#0052b4" d="M0 256c0 110.07 69.47 203.9 166.96 240.08V15.92C69.47 52.1 0 145.92 0 256z"/>
                            </svg>
                            <?= _t('site.ps', 'France'); ?>
                        </td>
                        <td class="text-right p-l20 p-r0">3.27%</td>
                    </tr>
                </table>

            </div>
            <div class="col-xs-12 col-sm-6">
                <img class="img-responsive m-b30 animated"
                     data-animation-in="fadeInRight" data-animation-out="fadeOutRight"
                     src="https://static.treatstock.com/static/images/common/map.png" alt="">
            </div>
        </div>

        <div class="row text-center">
            <div class="col-sm-8 col-sm-offset-2">
                <p class="lead m-b30 animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                    <?= _t('site.ps', 'Do you have any question? Message our team to get a personal offer.'); ?>
                </p>
                <a href="/site/contact" target="_blank" class="btn btn-primary m-b20 animated"
                   data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                    <?= _t('site.ps', 'Contact us'); ?>
                </a>
            </div>

        </div>

    </div>
</div>

<div class="container-fliud affiliates-land">
    <div class="container">
        <h2 class="affiliates-land__title p-b30 text-center animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
            <?= _t('site.ps', 'Advertising options'); ?>
        </h2>
        <div class="row affiliates-land-screens__row">
            <div class="col-sm-7 wide-padding--right">
                <img class="affiliates-land-screens__pic animated"
                     data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft"
                     src="https://static.treatstock.com/static/images/a-promo/promo-screens-1.png" alt="">
            </div>
            <div class="col-sm-5">
                <h3 class="m-b20 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <?= _t('site.ps', 'Messages'); ?>
                </h3>
                <p class="animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <?= _t('front', 'Place your advertising in messages section on Treatstock'); ?>
                </p>
            </div>
        </div>
        <div class="row affiliates-land-screens__row affiliates-land-screens__row--2nd">
            <div class="col-sm-7 wide-padding--left">
                <img class="affiliates-land-screens__pic animated"
                     data-animation-in="fadeInRight" data-animation-out="fadeOutRight"
                     src="https://static.treatstock.com/static/images/a-promo/promo-screens-3.png" alt="">
            </div>
            <div class="col-sm-5">
                <h3 class="m-b20 animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                    <strong><?= _t('site.ps', '3D Printers catalog'); ?></strong>
                </h3>
                <p class="animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                    <?= _t('front', 'Show your advertizing to users interested in machines and equipment'); ?>
                </p>
            </div>
        </div>
        <div class="row affiliates-land-screens__row">
            <div class="col-sm-7 wide-padding--right">
                <img class="affiliates-land-screens__pic animated"
                     data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft"
                     src="https://static.treatstock.com/static/images/a-promo/promo-screens-6.png" alt="">
            </div>
            <div class="col-sm-5">
                <h3 class="m-b20 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <strong><?= _t('site.ps', 'Instant order section'); ?></strong>
                </h3>
                <p class="animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <?= _t('front', 'Only for services registered on Treatstock. Show you service on the top position to get more orders.'); ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="affiliates-land" style="background: #404448; color: #ffffff;">
    <div class="container">
        <div class="row fb-grid fb-grid--va-center">
            <div class="col-sm-6 wide-padding--right">
                <div class="animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                <img class="img-responsive"
                     src="https://static.treatstock.com/static/images/common/catalog-media-kit.png" alt="">
                </div>
            </div>
            <div class="col-sm-6">
                <h2 class="m-b30 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <?= _t('front', 'Download document'); ?>
                </h2>
                <p class="animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <a href="#download" class="btn btn-primary">
                        <?= _t('front', 'Download'); ?>
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h2 class="affiliates-land-reviews__join animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                <?= _t('site.ps', 'Ready to start?'); ?>
            </h2>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h3 class="affiliates-land-reviews__join-sub animated" data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                        <?= _t('site.ps', 'Message our team to get a personal offer'); ?>
                    </h3>
                </div>
            </div>
            <a href="/site/contact" target="_blank" class="btn btn-primary m-b20 animated"
               data-animation-in="fadeInDown" data-animation-out="fadeOutDown">
                <?= _t('site.ps', 'Contact us'); ?>
            </a>
        </div>
    </div>
</div>