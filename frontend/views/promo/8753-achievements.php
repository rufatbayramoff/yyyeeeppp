<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>

<link rel="stylesheet" href="/css/print-model3d/print-model3d.css" type="text/css">

<style>
    .printers__loc, .printers__delivery, .printers__currency, .printers__sort {
        flex: 1 1 20%;
        max-width: 20%;
        margin-bottom: 10px;
        padding: 0 10px;
    }
</style>

<div class="aff-widget " id="print-navigation" ng-controller="print-model3d-navigation">
    <!-- Nav tabs -->
    <ul class="aff-widget-tabs clearfix">
        <li class="" ui-sref="upload" ui-sref-active="active">
            <a href="#upload" aria-controls="upload">
                <span class="aff-widget-tabs__step">1</span>
                Upload Files </a>
        </li>
        <li class="active" ui-sref="printers" ui-sref-active="active">
            <a href="#print" aria-controls="print">
                <span class="aff-widget-tabs__step">2</span>
                Customize Order </a>
        </li>
        <li class="" ui-sref="delivery" ui-sref-active="active">
            <a href="#delivery" aria-controls="delivery">
                <span class="aff-widget-tabs__step">3</span>
                Delivery Options </a>
        </li>
        <li class="" ui-sref="checkout" ui-sref-active="active">
            <a href="#payment" aria-controls="payment">
                <span class="aff-widget-tabs__step">4</span>
                Checkout </a>
        </li>
    </ul>
    <div class="aff-widget-content clearfix">

        <div class="tab-pane " id="print">


            <div class="row m-t20">
                <div class="col-sm-5 col-md-4">
                    <div class="preview">

                        <div class="preview__hint">
                            Select a file to change its material or color
                        </div>

                        <!-- Slider main container -->
                        <div class="preview__slider">
                            <!-- ngRepeat: model3dPart in model3d.parts --><span
                            "model3dPart in model3d.parts" class="">
                            <!-- ngIf: !model3dPart.isCanceled && model3dPart.qty -->
                            <div
                                    class="preview__slide preview_slider-clickable "
                            "!model3dPart.isCanceled &amp;&amp; model3dPart.qty"
                            ng-click="setModel3dActivePart(model3dPart)"
                            ng-class="{'preview__slide--selected': model3dPart.id &amp;&amp; (model3dPart.id ==
                            offersBundleData.selectedModel3dPartId)}">
                            <div class="preview__material ng-binding" title="White PLA">
                                PLA
                                <div class="preview__material-color" style="background-color: #ffffff"></div>
                            </div>
                            <img class="preview__pic" title=""
                                 ng-src="http://static.h5.tsdev.work/static/render/06dd768bd8bddcbae3f842b6f74fa030_color_ffffff_ambient_40_ax_0_ay_30_az_0_pb_0.png"
                                 main-src="http://static.h5.tsdev.work/static/render/06dd768bd8bddcbae3f842b6f74fa030_color_ffffff_ambient_40_ax_0_ay_30_az_0_pb_0.png"
                                 alt-src="/static/images/preloader.gif" onerror="commonJs.checkAltSrcImage(this);"
                                 src="http://static.h5.tsdev.work/static/render/06dd768bd8bddcbae3f842b6f74fa030_color_ffffff_ambient_40_ax_0_ay_30_az_0_pb_0.png">
                            <div class="preview__qty ng-binding" title="Quantity: 1">×1</div>
                            <div class="ng-binding">
                                1
                            </div>
                        </div><!-- end ngIf: !model3dPart.isCanceled && model3dPart.qty -->
                        </span><!-- end ngRepeat: model3dPart in model3d.parts -->
                    </div>
                    <button class="btn btn-default btn-sm preview__view3d" ng-click="preview3d()" name="view3d">
                        <span class="tsi tsi-cube m-r5"></span>
                        3D Viewer
                    </button>
                    <!-- ngIf: !model3dTextureState.isOneTextureForKit -->

                </div>
                <h4 class="material__title clearfix">
                    Materials &amp; Colors <a class="material__guide-btn" href="#materialsGuideModal"
                                              ng-click="materialsGuide()">Need help?</a>
                </h4>
                <div class="material-switcher">
                    <!-- ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                    "materialColorItem in offersBundleData.allowedMaterials.list" class="">
                    <!-- ngIf: materialColorItem.materialGroup -->
                    <div
                    "materialColorItem.materialGroup"
                    class="material-switcher__item ng-binding "
                    ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
                    ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId ==
                    offersBundleData.selectedMaterialGroupId}"
                    title="Resin">
                    Resin
                </div><!-- end ngIf: materialColorItem.materialGroup -->
                </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
                "materialColorItem in offersBundleData.allowedMaterials.list" class="">
                <!-- ngIf: materialColorItem.materialGroup -->
                <div
                "materialColorItem.materialGroup"
                class="material-switcher__item ng-binding "
                ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
                ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId ==
                offersBundleData.selectedMaterialGroupId}"
                title="Nylons">
                Nylons
            </div><!-- end ngIf: materialColorItem.materialGroup -->
            </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
            "materialColorItem in offersBundleData.allowedMaterials.list" class="">
            <!-- ngIf: materialColorItem.materialGroup -->
            <div
            "materialColorItem.materialGroup"
            class="material-switcher__item ng-binding "
            ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
            ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId ==
            offersBundleData.selectedMaterialGroupId}"
            title="Full-Color Sandstone">
            Full-Color Sandstone
        </div><!-- end ngIf: materialColorItem.materialGroup -->
        </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
        "materialColorItem in offersBundleData.allowedMaterials.list" class="">
        <!-- ngIf: materialColorItem.materialGroup -->
        <div
        "materialColorItem.materialGroup"
        class="material-switcher__item ng-binding "
        ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
        ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId ==
        offersBundleData.selectedMaterialGroupId}"
        title="Wood">
        Wood
    </div><!-- end ngIf: materialColorItem.materialGroup -->
    </span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
    "materialColorItem in offersBundleData.allowedMaterials.list" class="">
    <!-- ngIf: materialColorItem.materialGroup -->
    <div
    "materialColorItem.materialGroup"
    class="material-switcher__item ng-binding "
    ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
    ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId ==
    offersBundleData.selectedMaterialGroupId}"
    title="Polyesters/PETG">
    Polyesters/PETG
</div><!-- end ngIf: materialColorItem.materialGroup -->
</span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
"materialColorItem in offersBundleData.allowedMaterials.list" class="">
<!-- ngIf: materialColorItem.materialGroup -->
<div "materialColorItem.materialGroup"
class="material-switcher__item ng-binding "
ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId == offersBundleData.selectedMaterialGroupId}"
title="Flexible (TPU/TPE)">
Flexible (TPU/TPE)
</div><!-- end ngIf: materialColorItem.materialGroup -->
</span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
"materialColorItem in offersBundleData.allowedMaterials.list" class="">
<!-- ngIf: materialColorItem.materialGroup -->
<div "materialColorItem.materialGroup"
class="material-switcher__item ng-binding  material-switcher__item--active"
ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId == offersBundleData.selectedMaterialGroupId}"
title="PLA">
PLA
</div><!-- end ngIf: materialColorItem.materialGroup -->
</span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
"materialColorItem in offersBundleData.allowedMaterials.list" class="">
<!-- ngIf: materialColorItem.materialGroup -->
<div "materialColorItem.materialGroup"
class="material-switcher__item ng-binding "
ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId == offersBundleData.selectedMaterialGroupId}"
title="Polycarbonates">
Polycarbonates
</div><!-- end ngIf: materialColorItem.materialGroup -->
</span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
"materialColorItem in offersBundleData.allowedMaterials.list" class="">
<!-- ngIf: materialColorItem.materialGroup -->
<div "materialColorItem.materialGroup"
class="material-switcher__item ng-binding "
ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId == offersBundleData.selectedMaterialGroupId}"
title="ABS">
ABS
</div><!-- end ngIf: materialColorItem.materialGroup -->
</span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
"materialColorItem in offersBundleData.allowedMaterials.list" class="">
<!-- ngIf: materialColorItem.materialGroup -->
<div "materialColorItem.materialGroup"
class="material-switcher__item ng-binding "
ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId == offersBundleData.selectedMaterialGroupId}"
title="Nylon Powders">
Nylon Powders
</div><!-- end ngIf: materialColorItem.materialGroup -->
</span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
"materialColorItem in offersBundleData.allowedMaterials.list" class="">
<!-- ngIf: materialColorItem.materialGroup -->
<div "materialColorItem.materialGroup"
class="material-switcher__item ng-binding "
ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId == offersBundleData.selectedMaterialGroupId}"
title="Durable Plastics">
Durable Plastics
</div><!-- end ngIf: materialColorItem.materialGroup -->
</span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
"materialColorItem in offersBundleData.allowedMaterials.list" class="">
<!-- ngIf: materialColorItem.materialGroup -->
<div "materialColorItem.materialGroup"
class="material-switcher__item ng-binding "
ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId == offersBundleData.selectedMaterialGroupId}"
title="Decorative Materials">
Decorative Materials
</div><!-- end ngIf: materialColorItem.materialGroup -->
</span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list --><span
"materialColorItem in offersBundleData.allowedMaterials.list" class="">
<!-- ngIf: materialColorItem.materialGroup -->
<div "materialColorItem.materialGroup"
class="material-switcher__item ng-binding "
ng-click="changeMaterialGroup(materialColorItem.materialGroupId)"
ng-class="{'material-switcher__item--active':materialColorItem.materialGroupId == offersBundleData.selectedMaterialGroupId}"
title="High Detail Resins">
High Detail Resins
</div><!-- end ngIf: materialColorItem.materialGroup -->
</span><!-- end ngRepeat: materialColorItem in offersBundleData.allowedMaterials.list -->
</div>

<!-- ngIf: isShowMaterialColors() --><select
        class="form-control color-switcher__material-selector ng-pristine ng-untouched ng-valid  ng-not-empty"
        id="materialSelector" "isShowMaterialColors()"
ng-model="offersBundleData.selectedMaterialId" ng-change="changeMaterial()">
<!-- ngRepeat: materialColorItem in getMaterialsColors() -->
<option "materialColorItem in getMaterialsColors()" value="0" class="ng-binding ">
All
</option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
<option "materialColorItem in getMaterialsColors()" value="2" class="ng-binding ">
PLA
</option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
<option "materialColorItem in getMaterialsColors()" value="122"
class="ng-binding ">PLA+
</option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
<option "materialColorItem in getMaterialsColors()" value="164"
class="ng-binding ">PLA 3D870 (APLA)
</option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
<option "materialColorItem in getMaterialsColors()" value="143"
class="ng-binding ">PLA-CF
</option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
<option "materialColorItem in getMaterialsColors()" value="234"
class="ng-binding ">LWPLA
</option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
<option "materialColorItem in getMaterialsColors()" value="144"
class="ng-binding ">HPLA-CF
</option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
<option "materialColorItem in getMaterialsColors()" value="181"
class="ng-binding ">HTPLA
</option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
<option "materialColorItem in getMaterialsColors()" value="202"
class="ng-binding ">Matte Fiber HTPLA
</option><!-- end ngRepeat: materialColorItem in getMaterialsColors() -->
</select><!-- end ngIf: isShowMaterialColors() -->

<div class="color-switcher__material-list m-b10">
    <input type="checkbox" class="showhideColorSwitcher"
           id="color-switcher__aff-widget-switcher">
    <label class="color-switcher__mobile" for="color-switcher__aff-widget-switcher">
        <div title="Biodegradable and flexible plastic" class="material-item">
            <div class="material-item__color" style="background-color: #ffffff"></div>
            <div class="material-item__label ng-binding">White</div>
        </div>
        <span class="tsi tsi-down"></span>
    </label>

    <!-- ngIf: getColors() -->
    <div
    "getColors()" class="">
    <!-- ngIf: isShortColors() || !longColorsNeed() -->
    <div class="color-switcher__color-list "
    "isShortColors() || !longColorsNeed()">
    <!-- ngRepeat: printerColor in getColors().shortColorsList --><span
    "printerColor in getColors().shortColorsList" class="">
    <div ng-click="changeColor(printerColor.id)" class="material-item"
         ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
         title="Black">
        <div class="material-item__color" ng-style="{'background-color': '#3f3f3f'}"
             style="background-color: rgb(63, 63, 63);"></div>
        <div class="material-item__label ng-binding">Black</div>
    </div>
    </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span
    "printerColor in getColors().shortColorsList" class="">
    <div ng-click="changeColor(printerColor.id)" class="material-item material-item--active"
         ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
         title="White">
        <div class="material-item__color" ng-style="{'background-color': '#ffffff'}"
             style="background-color: rgb(255, 255, 255);"></div>
        <div class="material-item__label ng-binding">White</div>
    </div>
    </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span
    "printerColor in getColors().shortColorsList" class="">
    <div ng-click="changeColor(printerColor.id)" class="material-item"
         ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
         title="Red">
        <div class="material-item__color" ng-style="{'background-color': '#ff465b'}"
             style="background-color: rgb(255, 70, 91);"></div>
        <div class="material-item__label ng-binding">Red</div>
    </div>
    </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span
    "printerColor in getColors().shortColorsList" class="">
    <div ng-click="changeColor(printerColor.id)" class="material-item"
         ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
         title="Blue">
        <div class="material-item__color" ng-style="{'background-color': '#0066cc'}"
             style="background-color: rgb(0, 102, 204);"></div>
        <div class="material-item__label ng-binding">Blue</div>
    </div>
    </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span
    "printerColor in getColors().shortColorsList" class="">
    <div ng-click="changeColor(printerColor.id)" class="material-item"
         ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
         title="Gray">
        <div class="material-item__color" ng-style="{'background-color': '#bebebe'}"
             style="background-color: rgb(190, 190, 190);"></div>
        <div class="material-item__label ng-binding">Gray</div>
    </div>
    </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span
    "printerColor in getColors().shortColorsList" class="">
    <div ng-click="changeColor(printerColor.id)" class="material-item"
         ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
         title="Green">
        <div class="material-item__color" ng-style="{'background-color': '#00cc33'}"
             style="background-color: rgb(0, 204, 51);"></div>
        <div class="material-item__label ng-binding">Green</div>
    </div>
    </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span
    "printerColor in getColors().shortColorsList" class="">
    <div ng-click="changeColor(printerColor.id)" class="material-item"
         ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
         title="Yellow">
        <div class="material-item__color" ng-style="{'background-color': '#f2d70e'}"
             style="background-color: rgb(242, 215, 14);"></div>
        <div class="material-item__label ng-binding">Yellow</div>
    </div>
    </span><!-- end ngRepeat: printerColor in getColors().shortColorsList --><span
    "printerColor in getColors().shortColorsList" class="">
    <div ng-click="changeColor(printerColor.id)" class="material-item"
         ng-class="{'material-item--active':printerColor.id == offersBundleData.selectedMaterialColorId}"
         title="Orange">
        <div class="material-item__color" ng-style="{'background-color': '#ff8000'}"
             style="background-color: rgb(255, 128, 0);"></div>
        <div class="material-item__label ng-binding">Orange</div>
    </div>
    </span><!-- end ngRepeat: printerColor in getColors().shortColorsList -->
    <!-- ngIf: isShortColors() && longColorsNeed() -->
    <div class="material-item "
    "isShortColors() &amp;&amp; longColorsNeed()"
    ng-click="setColorsShortMode(0)">
    <span class="material-item__spoiler">
                <span class="tsi tsi-plus"></span>
                More            </span>
</div><!-- end ngIf: isShortColors() && longColorsNeed() -->
</div><!-- end ngIf: isShortColors() || !longColorsNeed() -->
<!-- ngIf: !isShortColors() && longColorsNeed() -->
</div><!-- end ngIf: getColors() -->
</div>

<!-- ngIf: getInfill() -->
<div "getInfill()" class="">
<div class="infill-switcher">
    <label for="materialitem-infill" class="infill-switcher__label">
        Infill </label>
    <select class="form-control infill-switcher__select ng-pristine ng-untouched ng-valid ng-not-empty"
            id="materialitem-infill" name="materialitem-infill"
            ng-model="offersBundleData.currentInfill" ng-change="onChangeInfill()"
            ng-options="infillItem for infillItem in getInfill().list">
        <option label="20" value="number:20" selected="selected">20</option>
        <option label="25" value="number:25">25</option>
        <option label="30" value="number:30">30</option>
        <option label="35" value="number:35">35</option>
        <option label="40" value="number:40">40</option>
        <option label="45" value="number:45">45</option>
        <option label="50" value="number:50">50</option>
        <option label="55" value="number:55">55</option>
        <option label="60" value="number:60">60</option>
        <option label="65" value="number:65">65</option>
        <option label="70" value="number:70">70</option>
        <option label="75" value="number:75">75</option>
        <option label="80" value="number:80">80</option>
        <option label="85" value="number:85">85</option>
        <option label="90" value="number:90">90</option>
        <option label="95" value="number:95">95</option>
        <option label="100" value="number:100">100</option>
    </select>
</div>
</div><!-- end ngIf: getInfill() -->
</div>
<div class="col-sm-7 col-md-8">
    <div class="printers">
        <div class="printers__control">

            <!-- ngIf: !offersBundleData.fixedLocation -->
            <div class="printers__loc "
            "!offersBundleData.fixedLocation">
            <label>Location</label>
            <div class="UserLocationWidget">
                <span class="tsi tsi-map-marker"></span>
                <button type="button" class="ts-user-location ts-ajax-modal btn-link"
                        title="Shipping Address" data-url="/geo/change-location"
                        data-target="#changelocation">Los Angeles, US
                </button>
            </div>
        </div><!-- end ngIf: !offersBundleData.fixedLocation -->

        <!-- ngIf: !psIdOnly -->
        <div
        "!psIdOnly" class="printers__delivery printers__intl_only ">
        <label title="International delivery">International delivery</label>
        <label class="checkbox-switch checkbox-switch--xs">
            <input type="checkbox" ng-model="offersBundleData.intlOnly"
                   ng-change="onChangeWithInternationalDelivery()"
                   class="ng-pristine ng-untouched ng-valid ng-empty">
            <span class="slider"></span>
        </label>
    </div><!-- end ngIf: !psIdOnly -->


    <!-- ngIf: !psIdOnly -->
    <div
    "!psIdOnly" class="printers__delivery printers__intl_only ">
    <label title="International delivery">Certified only</label>
    <label class="checkbox-switch checkbox-switch--xs">
        <input type="checkbox" ng-model="offersBundleData.intlOnly"
               ng-change="onChangeWithInternationalDelivery()"
               class="ng-pristine ng-untouched ng-valid ng-empty">
        <span class="slider"></span>
    </label>
</div><!-- end ngIf: !psIdOnly -->

    <!-- ngIf: offersBundleData.timeAround -->
    <div
    "offersBundleData.timeAround" class="printers__time ">
    <label title="Turnaround time">Turnaround time</label>
    <div class="printers__time-value ng-binding">
        7 days
        <a class="m-l5" ng-click="timeAroundModal()" href="#help">
            <span class="tsi tsi-question-c"></span>
        </a>
    </div>
</div><!-- end ngIf: offersBundleData.timeAround -->

<div class="printers__sort">
    <label>Sort by</label>
    <select ng-options="key as value for (key , value) in sortOptions "
            ng-model="offersBundleData.sortPrintersBy"
            ng-change="onChangePrintersListSort()"
            class="form-control input-sm ng-pristine ng-untouched ng-valid ng-not-empty">
        <option label="Default" value="string:default" selected="selected">Default
        </option>
        <option label="Best Rating" value="string:rating">Best Rating</option>
        <option label="Lowest print price" value="string:price">Lowest print price
        </option>
        <option label="Nearest to me" value="string:distance">Nearest to me</option>
    </select>
</div>
</div>
<!-- ngIf: (startedUpdatePrintersRequest || model3d.isCalculating()) -->
<!-- ngIf: !(startedUpdatePrintersRequest || model3d.isCalculating()) -->
<div "!(startedUpdatePrintersRequest || model3d.isCalculating())" class="">
<!-- ngIf: offersBundleData.offersBundle.messages -->
<!-- ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">

<div class="model-printers-available v3 model-printers-available--choosen" ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}" style="display: block;">
    <div class="model-printers-available__wrap" style="
    width: 75%;
">
        <div style="
    font-size: 14px;
    width: 100%;
    margin: -0px 0 5px;
"><div style="
    display: inline-block;
    background: #82c015;
    border-radius: 5px;
    color: #fff;
    padding: 0 5px;
    margin-right: 10px;
">Top Service</div><div style="
    display: inline-block;
    margin-right: 10px;
">
                <label style="
    margin: 0;
    color: #707478;
    font-weight: normal;
"></label>
                <div "offeritem.psprinter.ps.reviewsinfo"="" rating="offerItem.psPrinter.ps.reviewsInfo" class=" ng-isolate-scope" style="
                display: inline-block;
                "><!-- ngIf: showRating -->
                <div "showrating"="" class="star-rating-block " itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                <meta itemprop="ratingValue" content="5.0">
                <meta itemprop="worstRating" content="1">
                <meta itemprop="bestRating" content="5">
                <div class="star-rating rating-xs rating-disabled" style="
    margin: 0;
">
                    <div class="rating-container tsi" data-content="">
                        <div class="rating-stars" data-content="" style="width: 100%;"></div>
                        <input value="5.0" type="text" class="star-rating form-control hide" data-min="0" data-max="5" data-step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
                </div>
            </div><!-- end ngIf: showRating --><span class="star-rating-count ng-binding">5/5 (<span itemprop="reviewCount" class="ng-binding">12</span>)</span>
        </div></div><div class="model-printers-available__info__ps-printer ng-binding" style="
    font-size: 14px;
    line-height: 20px;
    padding: 0;
    display: inline-block;
    /* margin-right: 10px; */
">
        <div "offeritem.psprinter.isstatuscertificated="" &amp;&amp;="" !offeritem.psprinter.isstatuspro"="" class="cert-label cert-label--common " data-toggle="tooltip" data-placement="bottom" data-original-title="This machine is certified by Treatstock. It meets the technology requirements
        and necessary quality standards." style="
        margin-right: 2px;
        /* margin-top: -2px; */
        top: -1px;
        position: relative;
        ">
        <span class="tsi tsi-checkmark"></span>
    </div> 3  certified machines
</div></div><div class="model-printers-available__info" style="
    width: 50%;
">
    <div class="model-printers-available__info__img">
        <a href="/c/print-junkie?selectedPrinterId=7456&amp;model3dViewedStateId=1507535" target="_blank" data-pjax="0"><img ng-src="http://static.h5.tsdev.work/static/files/04/07/2623107_113587_bf3ec4c6ee6cbf49ef03e0c3ad2c43ac_64x64.png?date=1614286823" width="30" height="30" alt="" align="left" src="http://static.h5.tsdev.work/static/files/04/07/2623107_113587_bf3ec4c6ee6cbf49ef03e0c3ad2c43ac_64x64.png?date=1614286823"></a>
        <!-- ngIf: offerItem.psPrinter.isStatusPro -->

        <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->

        <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->

    </div>

    <div class="model-printers-available__info__ps">
        <div class="model-printers-available__info__ps-title" style="
    line-height: 20px;
">
            <a href="/c/print-junkie?selectedPrinterId=7456&amp;model3dViewedStateId=1507535" target="_blank" data-pjax="0"><span class="ng-binding">Print Junkie</span></a>
        </div>

        <div class="model-printers-available__delivery-title ng-binding" style="
    margin-bottom: 5px;
    line-height: 20px;
">
            Deerfield, Illinois
        </div>



    </div>
</div>
<div class="model-printers-available__delivery" style="
    width: 50%;
">
    <div style="
    /* font-weight: bold; */
            ">Response rate: <span style="
    font-weight: bold;
">less than 24h</span></div>
    <div style="
    margin: 0 0 5px;
    /* font-weight: bold; */
">Completion rate: <span style="
    color: #81cc00;
    font-weight: bold;
">high</span></div>




</div>
</div>
<div class="model-printers-available__price-btn" style="
    width: 25%;
">
    <div class="model-printers-available__price" style="
    padding: 0;
    line-height: 20px;
">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding " "!iswidget()"="">$7.89</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__price-delivery" style="
    text-align: right;
    display: block;
    margin: 0 0 0px;
">
        <span class="ng-binding">Pickup, Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span "offeritem.estimatedeliverycost.amount"="" class="">: <strong class="ng-binding"><span class="tsi tsi-truck" title="Delivery Price"></span> $7.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div><div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button" title="Select manufacturer" ng-click="printHere(offerItem)" oncontextmenu="return false;">Buy
        </button>
    </div><div class="model-printers-available__price" style="
    padding: 0;
    margin: 5px 0 -5px;
    line-height: 20px;
">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding " "!iswidget()"="" style="
        background: linear-gradient(
        115deg
        ,#2d8ee0 9.64%,#e00457 96.06%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        ">+$7.89 Bonus </span>
        <!-- end ngIf: !isWidget() -->
    </div>
</div>
</div>

<div class="model-printers-available model-printers-available--choosen" ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}" style="display: block;">
    <div class="model-printers-available__wrap" style="
    width: 75%;
">
        <div class="model-printers-available__info" style="
    width: 50%;
">
            <div class="model-printers-available__info__img">
                <a href="/c/print-junkie?selectedPrinterId=7456&amp;model3dViewedStateId=1507535" target="_blank" data-pjax="0"><img ng-src="http://static.h5.tsdev.work/static/files/04/07/2623107_113587_bf3ec4c6ee6cbf49ef03e0c3ad2c43ac_64x64.png?date=1614286823" width="30" height="30" alt="" align="left" src="http://static.h5.tsdev.work/static/files/04/07/2623107_113587_bf3ec4c6ee6cbf49ef03e0c3ad2c43ac_64x64.png?date=1614286823"></a>
                <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->

                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                <img class="svg-ico m-t5" src="https://static.treatstock.com/static/images/common/top-badge.svg" alt="">
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title" style="
    line-height: 20px;
">
                    <a href="/c/print-junkie?selectedPrinterId=7456&amp;model3dViewedStateId=1507535" target="_blank" data-pjax="0"><span class="ng-binding">Print Junkie</span></a>
                </div>

                <div class="model-printers-available__delivery-title ng-binding" style="
    margin-bottom: 5px;
    line-height: 20px;
">
                    Deerfield, Illinois
                </div>
                <div ng-show="psIdOnly" class="ng-hide">
                    <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
                    <div "offeritem.psprinter.reviewsinfo"="" rating="offerItem.psPrinter.reviewsInfo" class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div "showrating"="" class="star-rating-block " itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="5">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi" data-content="">
                            <div class="rating-stars" data-content="" style="width: 100%;"></div>
                            <input value="5" type="text" class="star-rating form-control hide" data-min="0" data-max="5" data-step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span class="star-rating-count ng-binding">(<span itemprop="reviewCount" class="ng-binding">11</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
        </div>

        <div class="model-printers-available__info__ps-printer ng-binding" style="
    font-size: 14px;
    line-height: 20px;
">
            <div "offeritem.psprinter.isstatuscertificated="" &amp;&amp;="" !offeritem.psprinter.isstatuspro"="" class="cert-label cert-label--common " data-toggle="tooltip" data-placement="bottom" data-original-title="This machine is certified by Treatstock. It meets the technology requirements
            and necessary quality standards." style="
            margin-right: 2px;
            /* margin-top: -2px; */
            top: -1px;
            position: relative;
            ">
            <span class="tsi tsi-checkmark"></span>
        </div> 3  certified machines
    </div>
</div>
</div>
<div class="model-printers-available__delivery" style="
    width: 50%;
">
    <div style="
    /* font-weight: bold; */
            ">Response rate: <span style="
    font-weight: bold;
">less than 24h</span></div>
    <div style="
    margin: 0 0 5px;
    /* font-weight: bold; */
">Completion rate: <span style="
    color: #81cc00;
    font-weight: bold;
">high</span></div>


    <div>
        <label style="
    margin: 0;
    color: #707478;
    font-weight: normal;
">Customer reviews:</label>
        <div "offeritem.psprinter.ps.reviewsinfo"="" rating="offerItem.psPrinter.ps.reviewsInfo" class=" ng-isolate-scope" style="
        display: inline-block;
        "><!-- ngIf: showRating -->
        <div "showrating"="" class="star-rating-block " itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
        <meta itemprop="ratingValue" content="5.0">
        <meta itemprop="worstRating" content="1">
        <meta itemprop="bestRating" content="5">
        <div class="star-rating rating-xs rating-disabled" style="
    margin: 0;
">
            <div class="rating-container tsi" data-content="">
                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                <input value="5.0" type="text" class="star-rating form-control hide" data-min="0" data-max="5" data-step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
        </div>
    </div><!-- end ngIf: showRating --><span class="star-rating-count ng-binding">5/5 (<span itemprop="reviewCount" class="ng-binding">12</span>)</span>
</div></div>

</div>
</div>
<div class="model-printers-available__price-btn" style="
    width: 25%;
">
    <div class="model-printers-available__price" style="
    padding: 0;
">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding " "!iswidget()"="">$7.89</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__price-delivery" style="
    text-align: right;
    display: block;
    margin: 0 0 0px;
">
        <span class="ng-binding">Pickup, Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span "offeritem.estimatedeliverycost.amount"="" class="">: <strong class="ng-binding"><span class="tsi tsi-truck" title="Delivery Price"></span> $7.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div><div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button" title="Select manufacturer" ng-click="printHere(offerItem)" oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>
<div class="v2 model-printers-available model-printers-available--choosen" ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}" style="display: block;">
    <div class="model-printers-available__wrap" style="
    width: 80%;
">
        <div class="model-printers-available__info" style="
    width: 100%;
    display: flex;
    align-items: center;
">
            <div class="model-printers-available__info__img" style="
    margin: 0 10px 0 0;
">
                <a href="/c/print-junkie?selectedPrinterId=7456&amp;model3dViewedStateId=1507535" target="_blank" data-pjax="0"><img ng-src="http://static.h5.tsdev.work/static/files/04/07/2623107_113587_bf3ec4c6ee6cbf49ef03e0c3ad2c43ac_64x64.png?date=1614286823" width="30" height="30" alt="" align="left" src="http://static.h5.tsdev.work/static/files/04/07/2623107_113587_bf3ec4c6ee6cbf49ef03e0c3ad2c43ac_64x64.png?date=1614286823"></a>
                <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->

                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->

            </div>

            <div class="model-printers-available__info__ps" style="
    vertical-align: middle;
    margin: 0;
">
                <div class="model-printers-available__info__ps-title" style="
    line-height: 20px;
">
                    <a href="/c/print-junkie?selectedPrinterId=7456&amp;model3dViewedStateId=1507535" target="_blank" data-pjax="0"><span class="ng-binding">Print Junkie</span></a>
                    <div style="
    display: inline-block;
    background: #82c015;
    border-radius: 5px;
    color: #fff;
    padding: 0 5px;
">Top Service</div> <div style="
    display: inline-block;
">
                        <label style="
    margin: 0;
    color: #707478;
    font-weight: normal;
"></label>
                        <div "offeritem.psprinter.ps.reviewsinfo"="" rating="offerItem.psPrinter.ps.reviewsInfo" class=" ng-isolate-scope" style="
                        display: inline-block;
                        "><!-- ngIf: showRating -->
                        <div "showrating"="" class="star-rating-block " itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="ratingValue" content="5.0">
                        <meta itemprop="worstRating" content="1">
                        <meta itemprop="bestRating" content="5">
                        <div class="star-rating rating-xs rating-disabled" style="
    margin: 0;
">
                            <div class="rating-container tsi" data-content="">
                                <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                <input value="5.0" type="text" class="star-rating form-control hide" data-min="0" data-max="5" data-step="0.1" data-size="xs" data-symbol="" data-glyphicon="false" data-rating-class="tsi" data-readonly="true"></div>
                        </div>
                    </div><!-- end ngIf: showRating --><span class="star-rating-count ng-binding">5/5 (<span itemprop="reviewCount" class="ng-binding">12</span>)</span>
                </div></div>
        </div>







    </div>
</div><div class="model-printers-available__info" style="
    width: 60%;
">


    <div class="model-printers-available__info__ps" style="
    margin: 0;
">






        <div style="
    /* font-weight: bold; */
            ">Response rate: <span style="
    font-weight: bold;
">less than 24h</span></div><div style="
    margin: 0;
    /* font-weight: bold; */
">Completion rate: <span style="
    color: #81cc00;
    font-weight: bold;
">high</span></div>
        <div class="model-printers-available__info__ps-printer ng-binding" style="
    font-size: 14px;
    line-height: 20px;
    padding: 0;
">
            <div "offeritem.psprinter.isstatuscertificated="" &amp;&amp;="" !offeritem.psprinter.isstatuspro"="" class="cert-label cert-label--common " data-toggle="tooltip" data-placement="bottom" data-original-title="This machine is certified by Treatstock. It meets the technology requirements
            and necessary quality standards." style="
            margin-right: 2px;
            /* margin-top: -2px; */
            top: -1px;
            position: relative;
            ">
            <span class="tsi tsi-checkmark"></span>
        </div> 3  certified machines
    </div>
</div>
</div>
<div class="model-printers-available__delivery" style="
    width: 40%;
">

    <div class="model-printers-available__delivery-title ng-binding" style="
    margin-bottom: 0;
    line-height: 20px;
">
        Deerfield, Illinois
    </div>


    <div class="model-printers-available__price-delivery" style="
    text-align: left;
    display: block;
    margin: 0 0 0px;
    line-height: 15px;
">
        <span class="ng-binding">Pickup, Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span "offeritem.estimatedeliverycost.amount"="" class="">: <strong class="ng-binding"><span class="tsi tsi-truck" title="Delivery Price"></span> $7.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>

</div>
</div>
<div class="model-printers-available__price-btn" style="
    width: 20%;
">
    <div class="model-printers-available__price" style="
    padding: 0;
    margin: 5px 0 5px;
    line-height: 20px;
">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding " "!iswidget()"="">$7.89</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__price" style="
    padding: 0;
    margin: 0 0 5px;
    line-height: 20px;
">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding " "!iswidget()"="" style="
        background: linear-gradient(
        115deg
        ,#2d8ee0 9.64%,#e00457 96.06%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        ">+$7.89 Bonus </span>
        <!-- end ngIf: !isWidget() -->
    </div><div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button" title="Select manufacturer" ng-click="printHere(offerItem)" oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>
<div>
    <div class="model-printers-available model-printers-available--choosen1"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/print-junkie?selectedPrinterId=7456&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/files/04/07/2623107_113587_bf3ec4c6ee6cbf49ef03e0c3ad2c43ac_64x64.png?date=1614286823"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/files/04/07/2623107_113587_bf3ec4c6ee6cbf49ef03e0c3ad2c43ac_64x64.png?date=1614286823"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/print-junkie?selectedPrinterId=7456&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">Print Junkie</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="5.0">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 100%;"></div>
                            <input value="5.0" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount"
                            class="ng-binding">12</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="5">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 100%;"></div>
                    <input value="5" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount"
                    class="ng-binding">11</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: Makergear M3-SE
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        Deerfield, Illinois
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $4.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$7.89</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">

<div>
    <div class="model-printers-available"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/3dprintdirect?selectedPrinterId=5945&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/files/ef/63/348269_0_7ea16e92d31c7a5ac5e402668bef76ec_64x64.png?date=1534928347"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/files/ef/63/348269_0_7ea16e92d31c7a5ac5e402668bef76ec_64x64.png?date=1534928347"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/3dprintdirect?selectedPrinterId=5945&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">3dPrint.Direct</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="4.9">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 98%;"></div>
                            <input value="4.9" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount"
                            class="ng-binding">79</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="4.9">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 98%;"></div>
                    <input value="4.9" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount"
                    class="ng-binding">48</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: Creality Ender 3 -2
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        Denton, TX
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Pickup, Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $7.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$10.34</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">
<div>
    <div class="model-printers-available"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/adm-prints?selectedPrinterId=4923&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/user/8de2f22cd87103550c31d51d12a049b4/ps_logo_circle_1575864166_64x64.png"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/user/8de2f22cd87103550c31d51d12a049b4/ps_logo_circle_1575864166_64x64.png"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/adm-prints?selectedPrinterId=4923&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">Stratum Industries</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="4.8">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 96%;"></div>
                            <input value="4.8" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount"
                            class="ng-binding">21</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="4.8">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 96%;"></div>
                    <input value="4.8" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount"
                    class="ng-binding">19</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: Creality CR-10S
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        LAFAYETTE HILL, Pennsylvania
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $9.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$10.34</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">
<div>
    <div class="model-printers-available"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/technologics-group?selectedPrinterId=1331&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/user/f655e8f9339af2b629ebed05b585423a/ps_logo_circle_1559742807_64x64.jpg"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/user/f655e8f9339af2b629ebed05b585423a/ps_logo_circle_1559742807_64x64.jpg"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/technologics-group?selectedPrinterId=1331&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">IslandFab</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="4.9">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 98%;"></div>
                            <input value="4.9" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount" class="ng-binding">139</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="4.9">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 98%;"></div>
                    <input value="4.9" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount" class="ng-binding">130</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: Original Prusa i3 MK2.5S w/ MMUv2
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        NEW PORT RICHEY, Florida
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Pickup, Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $4.89</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$11.57</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">
<div>
    <div class="model-printers-available"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/capeolive?selectedPrinterId=1901&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/user/7436b0dc99f8aed11026252aeade1a3a/ps_logo_circle_1583014731_64x64.png"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/user/7436b0dc99f8aed11026252aeade1a3a/ps_logo_circle_1583014731_64x64.png"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/capeolive?selectedPrinterId=1901&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">CapeOlive</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="5.0">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 100%;"></div>
                            <input value="5.0" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount" class="ng-binding">321</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="5">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 100%;"></div>
                    <input value="5" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount" class="ng-binding">213</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: Original Prusa i3 MK3S
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        Round Rock, Texas
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $5.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$11.57</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">
<div>
    <div class="model-printers-available"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/cw-print?selectedPrinterId=4252&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/files/f1/03/783529_29_c0d0bdfc4dca15b0f38ef987103b1ecd_64x64.png?date=1560842607"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/files/f1/03/783529_29_c0d0bdfc4dca15b0f38ef987103b1ecd_64x64.png?date=1560842607"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/cw-print?selectedPrinterId=4252&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">Cw Print</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="4.9">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 98%;"></div>
                            <input value="4.9" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount" class="ng-binding">183</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="4.9">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 98%;"></div>
                    <input value="4.9" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount"
                    class="ng-binding">96</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: Original Prusa i3 MK3S
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        Vesper, WI
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Pickup, Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $9.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$11.57</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">
<div>
    <div class="model-printers-available"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/redmongoose-prints?selectedPrinterId=1854&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/user/395af6444dfab37005c07b8264090296/ps_logo_circle_1509243080_64x64.jpg"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/user/395af6444dfab37005c07b8264090296/ps_logo_circle_1509243080_64x64.jpg"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/redmongoose-prints?selectedPrinterId=1854&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">REDMongoose Prints</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="4.9">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 98%;"></div>
                            <input value="4.9" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount" class="ng-binding">184</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="4.9">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 98%;"></div>
                    <input value="4.9" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount"
                    class="ng-binding">30</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: STARTT 3D Printer
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        Greenville, South Carolina
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $6.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$12.79</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">
<div>
    <div class="model-printers-available"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/chris-l-3d?selectedPrinterId=3491&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/files/bc/be/461050_21_d74ae0a3c16d83b0e0cd07796160d85d_64x64.png?date=1543183441"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/files/bc/be/461050_21_d74ae0a3c16d83b0e0cd07796160d85d_64x64.png?date=1543183441"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/chris-l-3d?selectedPrinterId=3491&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">Chris L3d</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="4.9">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 98%;"></div>
                            <input value="4.9" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount" class="ng-binding">348</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="4.9">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 98%;"></div>
                    <input value="4.9" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount"
                    class="ng-binding">89</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: Creality CR-10
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        Bay Shore, New York
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Pickup, Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $6.14</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$12.79</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">
<div>
    <div class="model-printers-available"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/1304-stone-street?selectedPrinterId=4336&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/user/7ad0732636df966fa9dbf2ced5eb721d/ps_logo_circle_1591482122_64x64.jpeg"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/user/7ad0732636df966fa9dbf2ced5eb721d/ps_logo_circle_1591482122_64x64.jpeg"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/1304-stone-street?selectedPrinterId=4336&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">GC-Tech 3D</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="4.9">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 98%;"></div>
                            <input value="4.9" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount"
                            class="ng-binding">39</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="4.9">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 98%;"></div>
                    <input value="4.9" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount"
                    class="ng-binding">28</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: Ender 5
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        JACKSONVILLE, Arkansas
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $5.64</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$12.79</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
<div "offerItem in offersBundleData.offersBundle.offers" class="">
<div>
    <div class="model-printers-available"
         ng-class="{'model-printers-available--choosen' :offersBundleData.activeOfferItemId &amp;&amp; (offersBundleData.activeOfferItemId == offerItem.psPrinter.id)}"
         style="display: block;">
        <div class="model-printers-available__wrap">
            <div class="model-printers-available__info">
                <div class="model-printers-available__info__img">
                    <a href="/c/shellback-workshop?selectedPrinterId=4495&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><img
                                ng-src="http://static.h5.tsdev.work/static/user/c75c5bfeffe5a3d847c98f9fd32b9d9e/ps_logo_circle_1605121871_64x64.png"
                                width="30" height="30" alt="" align="left"
                                src="http://static.h5.tsdev.work/static/user/c75c5bfeffe5a3d847c98f9fd32b9d9e/ps_logo_circle_1605121871_64x64.png"></a>
                    <!-- ngIf: offerItem.psPrinter.isStatusPro -->

                    <!-- ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
                    <div
                    "offerItem.psPrinter.isStatusCertificated &amp;&amp; !offerItem.psPrinter.isStatusPro"
                    class="cert-label cert-label--common "
                    data-toggle="tooltip" data-placement="bottom"
                    data-original-title="This machine is certified by Treatstock. It meets the technology requirements
                    and necessary quality standards.">
                    <span class="tsi tsi-checkmark"></span>
                </div>
                <!-- end ngIf: offerItem.psPrinter.isStatusCertificated && !offerItem.psPrinter.isStatusPro -->
            </div>

            <div class="model-printers-available__info__ps">
                <div class="model-printers-available__info__ps-title">
                    <a href="/c/shellback-workshop?selectedPrinterId=4495&amp;model3dViewedStateId=1507535"
                       target="_blank" data-pjax="0"><span class="ng-binding">Shellback Workshop</span></a>
                </div>

                <div ng-hide="psIdOnly">
                    <!-- ngIf: offerItem.psPrinter.ps.reviewsInfo -->
                    <div
                    "offerItem.psPrinter.ps.reviewsInfo"
                    rating="offerItem.psPrinter.ps.reviewsInfo"
                    class=" ng-isolate-scope"><!-- ngIf: showRating -->
                    <div
                    "showRating" class="star-rating-block "
                    itemprop="aggregateRating" itemscope=""
                    itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="4.9">
                    <meta itemprop="worstRating" content="1">
                    <meta itemprop="bestRating" content="5">
                    <div class="star-rating rating-xs rating-disabled">
                        <div class="rating-container tsi"
                             data-content="">
                            <div class="rating-stars"
                                 data-content=""
                                 style="width: 98%;"></div>
                            <input value="4.9" type="text"
                                   class="star-rating form-control hide"
                                   data-min="0" data-max="5"
                                   data-step="0.1" data-size="xs"
                                   data-symbol=""
                                   data-glyphicon="false"
                                   data-rating-class="tsi"
                                   data-readonly="true"></div>
                    </div>
                </div><!-- end ngIf: showRating --><span
                        class="star-rating-count ng-binding">(<span
                            itemprop="reviewCount" class="ng-binding">221</span> reviews)</span>
            </div><!-- end ngIf: offerItem.psPrinter.ps.reviewsInfo -->
        </div>
        <div ng-show="psIdOnly" class="ng-hide">
            <!-- ngIf: offerItem.psPrinter.reviewsInfo -->
            <div
            "offerItem.psPrinter.reviewsInfo"
            rating="offerItem.psPrinter.reviewsInfo"
            class=" ng-isolate-scope"><!-- ngIf: showRating -->
            <div
            "showRating" class="star-rating-block "
            itemprop="aggregateRating" itemscope=""
            itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="4.9">
            <meta itemprop="worstRating" content="1">
            <meta itemprop="bestRating" content="5">
            <div class="star-rating rating-xs rating-disabled">
                <div class="rating-container tsi"
                     data-content="">
                    <div class="rating-stars"
                         data-content=""
                         style="width: 98%;"></div>
                    <input value="4.9" type="text"
                           class="star-rating form-control hide"
                           data-min="0" data-max="5"
                           data-step="0.1" data-size="xs"
                           data-symbol=""
                           data-glyphicon="false"
                           data-rating-class="tsi"
                           data-readonly="true"></div>
            </div>
        </div><!-- end ngIf: showRating --><span
                class="star-rating-count ng-binding">(<span
                    itemprop="reviewCount" class="ng-binding">100</span> reviews)</span>
    </div><!-- end ngIf: offerItem.psPrinter.reviewsInfo -->
</div>

<div class="model-printers-available__info__ps-printer ng-binding">
    3D Printer: Creality CR-10
</div>
</div>
</div>
<div class="model-printers-available__delivery">
    <div class="model-printers-available__delivery-title ng-binding">
        Pataskala, Ohio
    </div>
    <div class="model-printers-available__price-delivery">
        <span class="ng-binding">Delivery</span>
        <!-- ngIf: offerItem.estimateDeliveryCost.amount --><span
        "offerItem.estimateDeliveryCost.amount" class="">: <strong
                class="ng-binding"><span class="tsi tsi-truck"
                                         title="Delivery Price"></span> $5.64</strong>
        </span><!-- end ngIf: offerItem.estimateDeliveryCost.amount -->
        <!-- ngIf: offerItem.estimateDeliveryCost === null && offerItem.hasDelivery() -->
        <!-- ngIf: offerItem.estimateDeliveryCost.amount === 0 -->
    </div>
</div>
</div>
<div class="model-printers-available__price-btn">
    <div class="model-printers-available__price">
        <!-- ngIf: isWidget() -->
        <!-- ngIf: !isWidget() --><span class="ts-print-price ng-binding "
        "!isWidget()">$12.79</span>
        <!-- end ngIf: !isWidget() -->
    </div>
    <div class="model-printers-available__print-btn">
        <button class="btn btn-danger btn-block t-model-printers__print-btn print-by-printer-button"
                title="Select manufacturer" ng-click="printHere(offerItem)"
                oncontextmenu="return false;">Buy
        </button>
    </div>
</div>
</div>

</div>
</div><!-- end ngRepeat: offerItem in offersBundleData.offersBundle.offers -->
</div><!-- end ngIf: !(startedUpdatePrintersRequest || model3d.isCalculating()) -->

<!-- ngIf: printersPageInfo.numPages>1 -->
<nav>
    <ul class="pagination">
        <!-- ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class=" active">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">1</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">2</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">3</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">4</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">5</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">6</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">7</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">8</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">9</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">10</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">11</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">12</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">13</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">14</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">15</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">16</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">17</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">18</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">19</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">20</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">21</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">22</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">23</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">24</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">25</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">26</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">27</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
        <li
        "n in (printersPageInfo.numPages|scalarArray) track by $index"
        ng-class="{'active' : printersPageInfo.currentPage===$index}" class="">
        <a href="javascript:;" ng-click="selectPage($index)" class="ng-binding">28</a>
        </li>
        <!-- end ngRepeat: n in (printersPageInfo.numPages|scalarArray) track by $index -->
    </ul>
</nav><!-- end ngIf: printersPageInfo.numPages>1 -->
</div>
</div>
</div>
<div class="aff-widget-bottom">
    <button ng-click="prevStep()" class="btn btn-default">
        <span class="tsi tsi-left"></span>
        Back
    </button>
    <button ng-click="nextStep()" class="btn btn-primary">
        Next <span class="tsi tsi-right"></span>
    </button>
</div>
</div>


</div>
</div>