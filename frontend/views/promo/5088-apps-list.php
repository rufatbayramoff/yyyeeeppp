<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

?>

<div class="apps-page-head">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="apps-page-head__title">
                    <?= _t('site.page', 'Free Manufacturing Apps'); ?>
                </h1>
                <p class="apps-page-head__subtitle">
                    <?= _t('site.page', 'We’ve picked the best online apps that are free and easy-to-use for designing, 3D printing, CNC machining and injection molding.'); ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="apps-grid">
        <a href="https://croudcad.com/cad.php" class="apps-grid__item" target="_blank">
            <img src="https://static.treatstock.com/static/images/apps/cad.svg" alt="Croudcad" class="apps-grid__pic">
            <h3 class="apps-grid__title">Croudcad <sup>βeta</sup></h3>
            <p class="apps-grid__text"><?= _t('site.page', 'Repository of useful and technical products that can be customized to fit your individual needs.'); ?></p>
        </a>

        <a href="https://reliefmod.com/" class="apps-grid__item" target="_blank">
            <img src="https://static.treatstock.com/static/images/apps/relief.svg" alt="Reliefmod" class="apps-grid__pic">
            <h3 class="apps-grid__title">Reliefmod <sup>βeta</sup></h3>
            <p class="apps-grid__text"><?= _t('site.page', 'Upload a photograph or picture and convert simple 2D images into a 3D bas-relief sculpture that can be 3D printed.'); ?></p>
        </a>

        <a href="http://touchsee.me" class="apps-grid__item" target="_blank">
            <img src="https://static.treatstock.com/static/images/apps/touchsee.svg" alt="TouchSee" class="apps-grid__pic">
            <h3 class="apps-grid__title">TouchSee</h3>
            <p class="apps-grid__text"><?= _t('site.page', 'Braille 3D text generator is intended to make life easier for the visually impaired.'); ?></p>
        </a>

        <a href="https://www.watermark3d.com/" class="apps-grid__item" target="_blank">
            <img src="https://static.treatstock.com/static/images/apps/wm.svg" alt="Watermark3d" class="apps-grid__pic">
            <h3 class="apps-grid__title">Watermark 3D</h3>
            <p class="apps-grid__text"><?= _t('site.page', 'Integrate hidden information into your STL files without causing any structural changes or leaving visual marks. Secure the intellectual property of your 3D models and protect them from theft.'); ?></p>
        </a>
    </div>
</div>