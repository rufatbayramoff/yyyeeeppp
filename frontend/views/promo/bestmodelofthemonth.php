<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

use frontend\widgets\SocialShareWidget;

$this->title = _t('site.bestmodel', 'The Best 3D Model of the Month');

?>

<div class="container">
<div class="row wide-padding">

    <div class="col-md-8 col-md-offset-2">

        <h1 class="page-header text-center"><?php echo $this->title; ?></h1>

        <p>
            <?= _t('site.bestmodel', 'Each month a 3D model on Treatstock will be crowned with the title "Best 3D Model of the Month”, with the winner determined by receiving the most number of likes in the month it was published. To vote, simply click on the heart symbol at the top right corner of the model\'s image. You can vote for as many of your favourite models as you like.'); ?>
        </p>

        <p>
            <?= _t('site.bestmodel', 'The winning model will be announced on the last day of each month and displayed on the home page for all to see.'); ?>
        </p>


        <hr>

        <h3 class="text-center m-b20">
            <?= _t('site.bestmodel', 'Previous Winners'); ?>
        </h3>

        <div id="accordion">

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse13" class="accordion-header">
                    <?= _t('site.bestmodel', 'For February'); ?>
                </a>
                <div id="collapse13" class="panel-collapse collapse in">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="https://www.treatstock.com/3d-printable-models/10492-floor-jack" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/files/57/63/27365_3267_f1d7ae01193cf3bbec80f72ce2157ae8_720x540.jpg" alt="Floor Jack — The Best model of the month for December">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Floor Jack', 'link'=>'https://www.treatstock.com/3d-printable-models/10492-floor-jack',
                            'imgUrl'=>'https://static.treatstock.com/static/files/57/63/27365_3267_f1d7ae01193cf3bbec80f72ce2157ae8_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="https://www.treatstock.com/3d-printable-models/10492-floor-jack">Floor Jack</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="https://www.treatstock.com/3d-printable-models-store/Aaron">Aaron W Hartman</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse12" class="accordion-header">
                    <?= _t('site.bestmodel', 'For January'); ?>
                </a>
                <div id="collapse12" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="https://www.treatstock.com/3d-printable-models/8798-go-pro-post-for-kayaks" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/files/2d/cf/23773_2794_b73277887888b20a9edd39c7420a48a8_720x540.jpg" alt="Go Pro post for kayaks — The Best model of the month for December">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Go Pro post for kayaks', 'link'=>'https://www.treatstock.com/3d-printable-models/8798-go-pro-post-for-kayaks',
                            'imgUrl'=>'https://static.treatstock.com/static/files/2d/cf/23773_2794_b73277887888b20a9edd39c7420a48a8_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="https://www.treatstock.com/3d-printable-models/8798-go-pro-post-for-kayaks">Go Pro post for kayaks</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="https://www.treatstock.com/3d-printable-models-store/louis.dionne">Louis Dionne</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse11" class="accordion-header">
                    <?= _t('site.bestmodel', 'For December'); ?>
                </a>
                <div id="collapse11" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="https://www.treatstock.com/3d-printable-models/7568-3d-printer-ginger-house-cookie-cutter" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/files/56/f8/20017_1064_a86b65b8995a96140b9beff2b3f74571_720x540.jpg" alt="3D Printer Ginger House Cookie Cutter — The Best model of the month for December">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'3D Printer Ginger House Cookie Cutter', 'link'=>'https://www.treatstock.com/3d-printable-models/7568-3d-printer-ginger-house-cookie-cutter',
                            'imgUrl'=>'https://static.treatstock.com/files/56/f8/20017_1064_a86b65b8995a96140b9beff2b3f74571_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="https://www.treatstock.com/3d-printable-models/7568-3d-printer-ginger-house-cookie-cutter">3D Printer Ginger House Cookie Cutter</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="https://www.treatstock.com/3d-printable-models-store/1064">Tamila Leto</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse10" class="accordion-header">
                    <?= _t('site.bestmodel', 'For November'); ?>
                </a>
                <div id="collapse10" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3d-printable-models/5686-martian-rover" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/files/25/b2/15740_2556_1c1d616a45f08021f9e0f0c082b3e777_720x540.jpg" alt="Martian rover — The Best model of the month for October">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Martian rover', 'link'=>'https://www.treatstock.com/3d-printable-models/5686-martian-rover',
                            'imgUrl'=>'https://static.treatstock.com/static/files/25/b2/15740_2556_1c1d616a45f08021f9e0f0c082b3e777_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3d-printable-models/5686-martian-rover">Martian rover</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/3d-printable-models-store/2556">Mikhael Larkin</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse9" class="accordion-header">
                    <?= _t('site.bestmodel', 'For October'); ?>
                </a>
                <div id="collapse9" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3d-printable-models/4618-bird-skull-ring-with-flower" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/files/25/b2/13794_2392_fb22d0f6d13e913c5b00bd1412e461a7_720x540.jpg" alt="Bird Skull Ring With Flower — The Best model of the month for October">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Bird Skull Ring With Flower', 'link'=>'https://www.treatstock.com/3d-printable-models/4618-bird-skull-ring-with-flower',
                            'imgUrl'=>'https://static.treatstock.com/static/files/25/b2/13794_2392_fb22d0f6d13e913c5b00bd1412e461a7_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3d-printable-models/4618-bird-skull-ring-with-flower">Bird Skull Ring With Flower</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/3d-printable-models-store/2392">Roman Kharikov</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" class="accordion-header">
                    <?= _t('site.bestmodel', 'For September'); ?>
                </a>
                <div id="collapse8" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3d-printable-models/2850-black-panther-mask" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/files/25/b2/10198_2060_2dd8b2aa6b6f8c37ed9a7d81ee0eeaf1_720x540.jpg" alt="Black Panther Mask — The Best model of the month for September">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Black Panther Mask', 'link'=>'https://www.treatstock.com/3d-printable-models/2850-black-panther-mask',
                            'imgUrl'=>'https://static.treatstock.com/static/files/25/b2/10198_2060_2dd8b2aa6b6f8c37ed9a7d81ee0eeaf1_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3d-printable-models/2850-black-panther-mask">Black Panther Mask</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/3d-printable-models-store/2060">MakersLab</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" class="accordion-header">
                    <?= _t('site.bestmodel', 'For August'); ?>
                </a>
                <div id="collapse7" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3d-printable-models/2349-gengar-low-poly-pokemon" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/user/f8c0c968632845cd133308b1a494967f/model/9017_720x540.jpg" alt="Gengar Low-poly Pokemon — The Best model of the month for August">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Gengar Low-poly Pokemon', 'link'=>'https://www.treatstock.com/3d-printable-models/2349-gengar-low-poly-pokemon',
                                                        'imgUrl'=>'https://static.treatstock.com/static/user/f8c0c968632845cd133308b1a494967f/model/9017_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3d-printable-models/2349-gengar-low-poly-pokemon">Gengar Low-poly Pokemon</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/u/MakersLab">adam.jech</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="accordion-header">
                    <?= _t('site.bestmodel', 'For July'); ?>
                </a>
                <div id="collapse6" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3d-printable-models/1098" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/user/9fe97fff97f089661135d0487843108e/model1098/5703_720x540.jpg" alt="Angry birds Red — The Best model of the month for July">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Angry Birds Red', 'link'=>'https://www.treatstock.com/3d-printable-models/1098',
                                                        'imgUrl'=>'https://static.treatstock.com/static/user/9fe97fff97f089661135d0487843108e/model1098/5703_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3d-printable-models/1098">Angry birds Red</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/u/deejay999">Evgeny Spiridonov</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="accordion-header">
                    <?= _t('site.bestmodel', 'For June'); ?>
                </a>
                <div id="collapse5" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3dmodels/968" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/user/89f03f7d02720160f1b04cf5b27f5ccb/model968/5269_720x540.jpg" alt="Jon Snow — The Best model of the month for June">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Jon Snow', 'link'=>'https://www.treatstock.com/3dmodels/968',
                                                        'imgUrl'=>'https://static.treatstock.com/static/user/89f03f7d02720160f1b04cf5b27f5ccb/model968/5269_720x540.jpg']);
                        ?>
                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3dmodels/968">Jon Snow</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/u/gojaehun-%28kleaf%29">Ko Jae Hoon aka KLEAF</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="accordion-header">
                    <?= _t('site.bestmodel', 'For May'); ?>
                </a>
                <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3dmodels/724" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/user/a424ed4bd3a7d6aea720b86d4a360f75/model724/3507_720x540.jpg" alt="Pirate money bank — The Best model of the month for May">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Pirate money bank', 'link'=>'https://www.treatstock.com/3dmodels/724',
                                                        'imgUrl'=>'https://static.treatstock.com/static/user/a424ed4bd3a7d6aea720b86d4a360f75/model724/3507_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3dmodels/724">Pirate money bank</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/u/3dartdigital">3dartdigital</a>
                        </p>
                    </div>
                </div>
            </div>

        	<div class="panel box-shadow border-0">
        	  	<a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="accordion-header">
                    <?= _t('site.bestmodel', 'For April'); ?>
                </a>
                <div id="collapse3" class="panel-collapse collapse">
                	<div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3dmodels/587" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="https://static.treatstock.com/static/user/2290a7385ed77cc5592dc2153229f082/model587/3008_720x540.jpg" alt="Star Salt/Sugar Shaker — The Best model of the month for April">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Star Salt/Sugar Shaker', 'link'=>'https://www.treatstock.com/3dmodels/587',
                                                        'imgUrl'=>'https://static.treatstock.com/static/user/2290a7385ed77cc5592dc2153229f082/model587/3008_720x540.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3dmodels/587">Star Salt/Sugar Shaker</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/u/tamila-leto">Tamila Leto</a>
                        </p>
    				</div>
        		</div>
        	</div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="accordion-header">
                    <?= _t('site.bestmodel', 'For March'); ?>
                </a>
                <div id="collapse2" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3dmodels/267" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="/static/images/bestmodels/2016-march.gif" alt="Howgarts Castle Lamp — The Best model of the month for March">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Howgarts Castle Lamp', 'link'=>'https://www.treatstock.com/3dmodels/267',
                                                        'imgUrl'=>'https://static.treatstock.com/static/images/bestmodels/2016-march.gif']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3dmodels/267">Howgarts Castle Lamp</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/u/MiniWorld">MiniWorld</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel box-shadow border-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="accordion-header">
                    <?= _t('site.bestmodel', 'For February'); ?>
                </a>
                <div id="collapse1" class="panel-collapse collapse">
                    <div class="panel-body p-t0">
                        <hr class="m-t0">
                        <a href="/3dmodels/125" class="text-center m-b30 show">
                            <img class="img-responsive-inline" src="/static/images/bestmodels/2016-february-ahsoka-lightsaber.jpg"  alt="Ahsoka Tanos lightsaber KIT — The Best model of the month for February">
                        </a>
                        <?php
                        echo SocialShareWidget::widget(['title'=>'Ahsoka Tanos lightsaber KIT', 'link'=>'https://www.treatstock.com/3dmodels/125',
                                                        'imgUrl'=>'https://static.treatstock.com/static/images/bestmodels/2016-february-ahsoka-lightsaber.jpg']);
                        ?>

                        <p>
                            <?= _t('site.bestmodel', 'Model'); ?> — <a href="/3dmodels/125">Ahsoka Tanos lightsaber KIT</a>
                        </p>
                        <p>
                            <?= _t('site.bestmodel', 'Author'); ?> — <a href="/u/sergey-%223dpicasso%22-kolesnik">Sergey Kolesnik</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
</div>