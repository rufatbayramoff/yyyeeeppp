<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

use common\models\Ps;
use frontend\widgets\PsPrintServiceReviewStarsWidget;

$ps=Ps::findByPk(1);

$this->title = "Manufacturing Machines Guide";

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

?>


<div class="over-nav-tabs-header">
    <div class="container">
        <ul class="breadcrumb m-t10 m-b0">
            <li><a href="#">Manufacturing Machines Guide</a></li>
        </ul>
        <h1 class="m-t10"><?= _t('site.ps', 'Manufacturing Machines Guide'); ?></h1>
    </div>
</div>
<div class="nav-tabs__container">
    <div class="container">
        <div class="nav-filter nav-filter--many1">
            <input type="checkbox" id="showhideNavFilter">
            <label  class="nav-filter__mobile-label" for="showhideNavFilter">
                <?= _t('site.ps', 'Filters'); ?>
                <span class="tsi tsi-down"></span>
            </label>
            <?php /*
            <div class="nav-filter__mobile-right-block">
                <div class="form-group field-sort">
                    <label class="" for="sort">Sort by</label> <select id="sort" class="form-control input-sm nav-filter__input" name="sort" onchange="TsCatalogSearch.changeLocation(this.form)">
                        <option value="">Distance</option>
                        <option value="price">Lowest Price</option>
                        <option value="rating">Best Rating</option>
                    </select>
                </div>
            </div>
            */?>
            <div class="nav-filter__mobile-container">
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Machine Type'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option selected="" value="">Any</option>
                        <option value="">3D Printing</option>
                        <option value="">CNC</option>
                    </select>
                </div>
                <?php /*
                <!--
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Brand'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option selected="" value="">Any</option>
                        <option value="">Ultimaker</option>
                        <option value="">Maker Bot</option>
                        <option value="">3D Systems</option>
                    </select>
                </div>
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Material'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option selected="" value="">Any</option>
                        <option value="">PLA</option>
                        <option value="">ABS</option>
                        <option value="">Resin</option>
                        <option value="">Composite</option>
                    </select>
                </div>
                <div class="nav-filter__group">
                    <label for=""><?= _t('site.ps', 'Technology'); ?></label>
                    <select class="form-control input-sm nav-filter__input">
                        <option selected="" value="">Any</option>
                        <option value="">SLA</option>
                        <option value="">FDM</option>
                    </select>
                </div>
                <div class="nav-filter__group nav-filter__group--right hidden-xs">
                    <label for=""><?= _t('site.ps', 'Sort by:'); ?></label>
                    <select id="model3d-printers-sort" name="Model3dEditForm[currentPrintersSort]" class="selectpicker form-control input-sm nav-filter__input" onchange="model3dItemFormObj.reloadPrintersList();">
                        <option selected="" value="default">Default</option>
                        <option value="distance">
                            Nearest to me            </option>
                        <option value="price">
                            Lowest price            </option>
                    </select>
                </div>
                -->
                */?>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="responsive-container-list responsive-container-list--3">

        <div class="responsive-container">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card--mach-label label label-default">
                    <?= _t('site.ps', '3D Printer'); ?>
                </div>

                <div class="designer-card__mach">
                    <a class="designer-card__mach-pic" href="#to-ps-page" target="_blank">
                        <img src="/static/images/defaultPrinter.png" alt="" align="left">
                    </a>
                    <div class="designer-card__mach-data">
                        <h4 class="designer-card__mach-title">
                            <a href="#to-ps-page" target="_blank">
                                Maker Bot Replicator Mini
                            </a>
                        </h4>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Tehnology'); ?>:</span> FDM
                        </div>
                        <div class="designer-card__data m-b0">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Materials'); ?>:</span> PLA, ABS, Wood PLA, Bronze PLA and +10 materials
                        </div>
                    </div>
                </div>

                <div class="designer-card__ps-pics">
                    <div class="designer-card__ps-portfolio swiper-container swiper-container-horizontal" style="cursor: -webkit-grab;">
                        <div class="swiper-wrapper">
                            <a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active" href="http://ts5.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_720x540.png" data-lightbox="ps1"><img src="http://ts5.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_160x90.png" alt="Designer33.png"></a><a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next" href="http://ts5.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_720x540.png" data-lightbox="ps1"><img src="http://ts5.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_160x90.png" alt="PrintServiceS1.png"></a><a class="designer-card__ps-portfolio-item swiper-slide" href="http://ts5.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_720x540.png" data-lightbox="ps1"><img src="http://ts5.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_160x90.png" alt="PrintServiceS2.png"></a>                        </div>
                        <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;"><div class="swiper-scrollbar-drag" style="width: 0px;"></div></div>
                    </div>
                </div>

                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Brand'); ?>:</span> Maker Bot
                </div>

                <div class="designer-card__about">
                    I am experienced in 3D modeling and design with my specialty being cosplay and items from<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> movies and games. I'm really passionate about what I do and if you need a 3D printable model designed for you within a short turnaround, then send me a message and I will create it for you.</span>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Avarage price per gram'); ?>:</span> $1000
                        </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Avarage price'); ?>:</span> $100
                        </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Work Area'); ?>:</span> 252 х 199 х 150 mm
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card--mach-label label label-default">
                    <?= _t('site.ps', 'CNC'); ?>
                </div>

                <div class="designer-card__mach">
                    <a class="designer-card__mach-pic" href="#to-ps-page" target="_blank">
                        <img src="/static/images/defaultPrinter.png" alt="" align="left">
                    </a>
                    <div class="designer-card__mach-data">
                        <h4 class="designer-card__mach-title">
                            <a href="#to-ps-page" target="_blank">
                                Uber Spark 5000
                            </a>
                        </h4>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Tehnology'); ?>:</span> Cutting
                        </div>
                        <div class="designer-card__data m-b0">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Materials'); ?>:</span> Plastic, Steel, Wood
                        </div>
                    </div>
                </div>

                <div class="designer-card__ps-pics">
                    <div class="designer-card__ps-portfolio swiper-container swiper-container-horizontal" style="cursor: -webkit-grab;">
                        <div class="swiper-wrapper">
                            <a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active" href="http://ts5.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_720x540.png" data-lightbox="ps1"><img src="http://ts5.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_160x90.png" alt="Designer33.png"></a><a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next" href="http://ts5.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_720x540.png" data-lightbox="ps1"><img src="http://ts5.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_160x90.png" alt="PrintServiceS1.png"></a><a class="designer-card__ps-portfolio-item swiper-slide" href="http://ts5.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_720x540.png" data-lightbox="ps1"><img src="http://ts5.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_160x90.png" alt="PrintServiceS2.png"></a>                        </div>
                        <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;"><div class="swiper-scrollbar-drag" style="width: 0px;"></div></div>
                    </div>
                </div>

                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Brand'); ?>:</span> Spark Lasers
                </div>

                <div class="designer-card__about">
                    I am experienced in 3D modeling and design with my specialty being cosplay and items from<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> movies and games. I'm really passionate about what I do and if you need a 3D printable model designed for you within a short turnaround, then send me a message and I will create it for you.</span>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Avarage price per gram'); ?>:</span> $1000
                        </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Avarage price'); ?>:</span> $100
                        </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Work Area'); ?>:</span> 2000 х 3000 х 500 mm
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="responsive-container">
            <div class="designer-card designer-card--ps-cat">
                <div class="designer-card--mach-label label label-default">
                    <?= _t('site.ps', '3D Printer'); ?>
                </div>

                <div class="designer-card__mach">
                    <a class="designer-card__mach-pic" href="#to-ps-page" target="_blank">
                        <img src="/static/images/defaultPrinter.png" alt="" align="left">
                    </a>
                    <div class="designer-card__mach-data">
                        <h4 class="designer-card__mach-title">
                            <a href="#to-ps-page" target="_blank">
                                Maker Bot Replicator Mini
                            </a>
                        </h4>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Tehnology'); ?>:</span> FDM
                        </div>
                        <div class="designer-card__data m-b0">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Materials'); ?>:</span> PLA, ABS, Wood PLA, Bronze PLA and +10 materials
                        </div>
                    </div>
                </div>

                <div class="designer-card__ps-pics">
                    <div class="designer-card__ps-portfolio swiper-container swiper-container-horizontal" style="cursor: -webkit-grab;">
                        <div class="swiper-wrapper">
                            <a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-active" href="http://ts5.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_720x540.png" data-lightbox="ps1"><img src="http://ts5.vcap.me/static/files/5e/58/1_1000_3561925daefbb59209069a45477ec694_160x90.png" alt="Designer33.png"></a><a class="designer-card__ps-portfolio-item swiper-slide swiper-slide-next" href="http://ts5.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_720x540.png" data-lightbox="ps1"><img src="http://ts5.vcap.me/static/files/60/91/2_1000_6d2bc7f6832765e96dc62f87e2e971ff_160x90.png" alt="PrintServiceS1.png"></a><a class="designer-card__ps-portfolio-item swiper-slide" href="http://ts5.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_720x540.png" data-lightbox="ps1"><img src="http://ts5.vcap.me/static/files/54/17/3_1000_b9bf90530117651fee0df0ab1dccb635_160x90.png" alt="PrintServiceS2.png"></a>                        </div>
                        <div class="designer-card__ps-portfolio-scrollbar swiper-scrollbar" style="opacity: 0;"><div class="swiper-scrollbar-drag" style="width: 0px;"></div></div>
                    </div>
                </div>

                <div class="designer-card__data">
                    <span class="designer-card__data-label"><?= _t('site.ps', 'Brand'); ?>:</span> Maker Bot
                </div>

                <div class="designer-card__about">
                    I am experienced in 3D modeling and design with my specialty being cosplay and items from<a class="tsrm-toggle" href="#" style="display:inline">...Show more</a>
                    <span class="tsrm-content hide"> movies and games. I'm really passionate about what I do and if you need a 3D printable model designed for you within a short turnaround, then send me a message and I will create it for you.</span>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Avarage price per gram'); ?>:</span> $1000
                        </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Avarage price'); ?>:</span> $100
                        </div>
                        <div class="designer-card__data">
                            <span class="designer-card__data-label"><?= _t('site.ps', 'Work Area'); ?>:</span> 252 х 199 х 150 mm
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-sm-12">
            <ul class="pagination">
                <li class="prev disabled"><span><span class="tsi tsi-left"></span></span></li>
                <li class="active"><a href="#page1" data-page="0">1</a></li>
                <li><a href="#page2" data-page="1">2</a></li>
                <li><a href="#page3" data-page="2">3</a></li>
                <li><a href="#page4" data-page="3">4</a></li>
                <li><a href="#page5" data-page="4">5</a></li>
                <li><a href="#page6" data-page="5">6</a></li>
                <li><a href="#page7" data-page="6">7</a></li>
                <li><a href="#page8" data-page="7">8</a></li>
                <li><a href="#page9" data-page="8">9</a></li>
                <li><a href="#page10" data-page="9">10</a></li>
                <li class="next"><a href="#page2" data-page="1"><span class="tsi tsi-right"></span></a></li>
            </ul>
        </div>
    </div>

</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <h2>Seo text</h2>
            <p>Some SEO bullshit text. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid cumque delectus fugit illo maxime quasi reiciendis, repellat ullam. Consequatur corporis earum eum eveniet facilis, iure magnam mollitia. Expedita, sit, voluptas.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid assumenda at deleniti distinctio earum hic in iste iusto laboriosam magnam maxime minus modi non odio officia, quia soluta, veniam voluptate.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid assumenda at deleniti distinctio earum hic in iste iusto laboriosam magnam maxime minus modi non odio officia, quia soluta, veniam voluptate. Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid amet assumenda distinctio dolore ducimus impedit, itaque, iusto laudantium maxime nisi nulla placeat quaerat quod recusandae reprehenderit sapiente tempore, ut?</p>
        </div>
    </div>
</div>

    <script>
        <?php $this->beginBlock('js1', false); ?>

        //init PS card portfolio pics
        var swiperMachCards = new Swiper('.designer-card__ps-portfolio', {
            scrollbar: '.designer-card__ps-portfolio-scrollbar',
            scrollbarHide: true,
            slidesPerView: 'auto',
            grabCursor: true
        });

        <?php $this->endBlock(); ?>
    </script>

<?php $this->registerJs($this->blocks['js1']); ?>