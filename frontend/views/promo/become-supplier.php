<?php

$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);


Yii::$app->angular
    ->service(['notify', 'router', 'user'])
    ->controller('promo/PromoContactUsController')
    ->controllerParam('type', 'business')
?>

<style>
    .become-sup-head {
        position: relative;
        padding: 160px 0 130px;
        background: rgba(43, 43, 51, 0.85);
        color: #ffffff;
        overflow: hidden;
    }
    .become-sup-head:after {
        content: '';
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: url(https://static.treatstock.com/static/images/common/business-page/head-bg-min.jpg) no-repeat 50% 50% transparent;
        background-size: cover;
    }
    .become-sup-head__title,
    .become-sup-head__subtitle,
    .become-sup-head__btn {
        position: relative;
        z-index: 1;
        margin-bottom: 40px;
    }
    .become-sup-head__title {
        font-size: 60px;
        line-height: 60px;
    }
    .become-sup-head__subtitle {
        margin-top: 0;
        font-weight: normal;
    }
    .become-sup-head__btn {
        margin-bottom: 20px;
        padding: 15px 30px;
        font-size: 20px;
    }
    @media (max-width: 767px) {
        .become-sup-head {
            padding: 40px 0;
        }
        .become-sup-head__title {
            font-size: 40px;
            line-height: 40px;
        }
        .become-sup-head__subtitle {
            font-size: 20px;
            line-height: 1.4;
        }
    }

    .become-sup-section {
        position: relative;
        padding: 100px 0;
    }
    @media (max-width: 767px) {
        .become-sup-section {
            padding: 40px 0;
        }
    }

    .become-sup-section--grey {
        background-color: #f6f8fa;
    }

    .become-sup-img {
        width: 100%;
        max-width: 100%;
        margin-bottom: 50px;
        border-radius: 5px;
        box-shadow: 0 10px 30px rgba(0, 54, 153, 0.15);
    }

    .become-sup-num {
        margin-bottom: 10px;
        font-size: 40px;
        line-height: 40px;
        font-weight: bold;
    }

    .flex-md-vmiddle {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }
    @media (max-width: 992px) {
        .flex-md-vmiddle{
            display: block;
        }
    }
    .flex-vert-box {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
    }
    .flex-vert-around {
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    .flex-horiz-box {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
    }
    .flex-horiz-around {
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    .chart-line {
        padding: 4px;
        line-height: 1.1;
    }
    .dark {
        color: #ffffff!important;
    }
    .light {
        color: #444444!important;
    }
    .pic {
        width: 100%;
        max-width: 100%;
        height: auto;
    }


    .card {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        border-radius: 1px;
        margin-bottom: 60px;
    }
    .card-block {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
    }
    .card-icon {
        display: inline-block;
        width: 40px;
    }
    .card.card-row {
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }
    .card.card-row:not(.padding-box) {
        padding-top: 5px;
    }
    .card {
        -webkit-box-direction: reverse;
        -ms-flex-direction: row-reverse;
        flex-direction: row-reverse;
    }
    .card-row>*+* {
        margin-left: 20px;
    }
    @media (max-width: 992px) {
        .card {
            margin-bottom: 30px;
        }
    }

    .social-list {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .social-list li {
        display: inline-block;
        margin: 5px 6px;
        min-width: 15px;
    }

    .social-list li i {
        vertical-align: sub;
    }


    .become-sup-h2,
    .become-sup-h3 {
        font-weight: 100;
        margin: 0 0 40px;
    }
    .become-sup-h2 {
        font-size: 40px;
        line-height: 40px;
    }
    .become-sup-h3 {
        font-size: 26px;
        line-height: 30px;
    }


    .become-sup-section__bg {
        position: absolute;
        top: 0;
        height: 100%;
        width: 100%;
        background-repeat: no-repeat;
        background-position: 50% 50%;
        background-size: cover;
        -webkit-background-size: cover;
        opacity: 0.05;
    }
    .become-sup-section--faq .become-sup-section__bg {
        background-image: url('https://static.treatstock.com/static/images/common/become-supplier/bg-work-1.jpg');
    }

    .promo-contact {
        padding: 80px 0 70px;
        margin-bottom: -40px;
        color: #fff;
        background-color: #0068b3;
        background-image: linear-gradient(20deg, #006a80 0%, #550080 100%);
    }
    .promo-contact__form {
        margin-top: 30px;
    }
    .promo-contact__form .btn {
        margin: 10px 0 0;
    }
    .promo-contact__form .form-control {
        background: rgba(0, 0, 0, 0.4);
        box-shadow: none;
        border-color: transparent;
        color: #ffffff;
        transition: all 0.3s ease;
    }
    .promo-contact__form .form-control:focus {
        border-color: rgba(0, 0, 0, 0);
        box-shadow: none;
        background: #fff;
        color: #404448;
    }
    .promo-contact__form .form-control::-webkit-input-placeholder {color: #868fbd;}
    .promo-contact__form .form-control::-moz-placeholder {color: #868fbd;}
    .promo-contact__form .form-control:-ms-input-placeholder {color: #868fbd;}
    .promo-contact__form .form-control:-moz-placeholder {color: #868fbd;}

    .promo-contact__form label {
        color: rgba(255, 255, 255, 0.8);
    }
    @media (max-width: 500px) {
        .promo-contact {
            padding: 60px 0;
        }
    }

    .promo-contact .btn-info {
        color: #1E88E5;
        background-color: #ffffff;
        border-color: transparent;
    }
    .promo-contact .btn-info:hover {
        background-color: rgba(255, 255, 255, 0.85);
    }

    .mat-cat-card {
        margin-bottom: 30px;
        border-radius: 5px;
        overflow: hidden;
        background: #ffffff;
    }
    .mat-cat-card:hover {
        transform: none;
        box-shadow: 0 10px 30px rgba(0, 54, 153, 0.15);
    }
    .mat-cat-card__title {
        margin: 5px 10px 0;
        padding: 5px 0;
        font-size: 14px;
        line-height: 20px;
        font-weight: 700;
    }
    @media (min-width: 768px) {
        .main-page-become__pic {
            right: calc(66% - -15px);
        }
    }
    .become-sup-icon {
        width: 75px;
        height: 75px;
    }
    .become-sup-link {
        margin: 20px 0 0;
        font-size: 20px;
        line-height: 1.2;
    }
    .become-sup-link a {
        border-bottom: 1px dashed;
        text-decoration: none !important;
    }
</style>

<section class="become-sup-head">
    <div class="container">
        <h1 class="become-sup-head__title"><?= _t('site.page', 'Provide 3D Printing, СNC, Cutting Service or Sell Custom Products'); ?></h1>
        <?php if(is_guest()) { ?>
            <a href="/mybusiness/company" class="btn btn-primary become-sup-head__btn" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company', 'signUp');return false;" : "return true;" ?>"><?= _t('site.page', 'Create a Business'); ?></a>
        <?php } else { ?>
            <a href="/mybusiness/company" class="btn btn-primary become-sup-head__btn" ><?= _t('site.page', 'Edit Business'); ?></a>
        <?php } ?>
        <h3 class="become-sup-head__subtitle"><?= _t('site.page', 'Register your business and join our global manufacturing network on Treatstock'); ?></h3>

    </div>
</section>

<section class="become-sup-section text-center">
    <div class="container">

        <h2 class="become-sup-h2 m-t0 m-b20">
            <strong>
                <?= _t('site.page', 'Our customers looking for'); ?>
            </strong>
        </h2>
        <div class="row">
            <div class="col-md-4">
                <h4 class="become-sup-link">
                    <a href="#additive" data-target="#additive">
                        <?= _t('site.page', 'Additive manufacturing'); ?>
                    </a>
                </h4>
            </div>
            <div class="col-md-4">
                <h4 class="become-sup-link">
                    <a href="#machine" data-target="#machine">
                        <?= _t('site.page', 'Machining and Laser Cutting'); ?>
                    </a>
                </h4>
            </div>
            <div class="col-md-4">
                <h4 class="become-sup-link">
                    <a href="#custom" data-target="#custom">
                        <?= _t('site.page', 'Custom products'); ?>
                    </a>
                </h4>
            </div>
        </div>
    </div>
</section>

<section class="become-sup-section become-sup-section--grey" id="additive">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="become-sup-h3 m-b20"><?= _t('site.page', 'Additive manufacturing'); ?></h3>
                <h2 class="become-sup-h2">
                    <strong><?= _t('site.page', 'Do You Provide a 3D Printing Service?'); ?></strong>
                </h2>
                <p class="m-b30" style="font-size: 18px;">
                    <?php if(is_guest()) { ?>
                        <a href="/mybusiness/company" class="btn btn-primary m-r10" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company', 'signUp');return false;" : "return true;" ?>"><?= _t('site.page', 'Start 3D Print Service'); ?></a>
                    <?php } else { ?>
                        <a href="/mybusiness/company" class="btn btn-primary m-r10" ><?= _t('site.page', 'Edit Business'); ?></a>
                    <?php } ?>
                    <span style="display: inline-block;margin-top: 10px;"><?= _t('site.page', 'to get more customers'); ?></span>
                </p>
            </div>
            <div class="col-md-5 col-md-offset-1">
                <img class="become-sup-img"
                     src="https://static.treatstock.com/static/files/5b/a2/47856_2484_743da437d7e59463c922bf3a54121872_1200x1200.jpg?date=1492421127"
                     alt="">
            </div>
        </div>
        <div class="row m-t30">
            <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="mat-cat-card">
                    <img class="mat-cat-card__pic"
                      src="https://static.treatstock.com/static/fxd/wikiMaterials/PLA/photos/pla_printed_parts_2_1.jpg?date=1569477817"
                      alt="PLA ">
                    <h3 class="mat-cat-card__title"><?= _t('site.page', 'PLA'); ?> </h3>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="mat-cat-card">
                    <img class="mat-cat-card__pic"
                      src="https://static.treatstock.com/static/fxd/wikiMaterials/abs-plastic/photos/abs_filament-4.jpg?date=1582015664"
                      alt="ABS">
                    <h3 class="mat-cat-card__title"><?= _t('site.page', 'ABS'); ?></h3>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="mat-cat-card">
                    <img class="mat-cat-card__pic"
                      src="https://static.treatstock.com/static/fxd/wikiMaterials/TPU/photos/tpu_material.jpg?date=1523347680"
                      alt="TPU">
                    <h3 class="mat-cat-card__title"><?= _t('site.page', 'TPU'); ?></h3>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="mat-cat-card">
                    <img class="mat-cat-card__pic"
                        src="https://static.treatstock.com/static/fxd/wikiMaterials/nylon-SLS/photos/sls_nylon-2.jpg?date=1582021770"
                        alt="Nylon (SLS 3D printing)">
                    <h3 class="mat-cat-card__title"><?= _t('site.page', 'Nylon'); ?></h3>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="mat-cat-card">
                    <img class="mat-cat-card__pic"
                       src="https://static.treatstock.com/static/fxd/wikiMaterials/aluminum/photos/aluminium_3d_print_material.jpg?date=1523347820"
                       alt="Aluminum">
                    <h3 class="mat-cat-card__title"><?= _t('site.page', 'Aluminum'); ?></h3>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="mat-cat-card">
                    <img class="mat-cat-card__pic"
                         src="https://static.treatstock.com/static/fxd/wikiMaterials/machinable-wax/photos/machinable_wax_material.jpg?date=1523347936"
                         alt="Machinable Wax">
                    <h3 class="mat-cat-card__title"><?= _t('site.page', 'Wax'); ?></h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="main-page-become m-b0" id="machine">
    <div class="main-page-become__pic"></div>
    <div class="container container--wide">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-4 main-page-become__data">
                <h3 class="become-sup-h3"><?= _t('site.page', 'CNC Machining and Laser Cutting'); ?></h3>
                <h2 class="become-sup-h2">
                    <strong><?= _t('site.page', 'Do You Provide a CNC or Cutting service?'); ?></strong>
                </h2>
                <p class="m-b30" style="font-size: 18px;">
                    <?php if(is_guest()) { ?>
                        <a href="/mybusiness/company" class="btn btn-primary m-r10" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company', 'signUp');return false;" : "return true;" ?>"><?= _t('site.page', 'Add Your Service'); ?></a>
                    <?php } else { ?>
                        <a href="/mybusiness/company" class="btn btn-primary m-r10" ><?= _t('site.page', 'Edit Your Service'); ?></a>
                    <?php } ?>
                    <span style="display: inline-block;margin-top: 10px;"><?= _t('site.page', 'to receive orders via Treatstock'); ?></span>
                </p>

                <div class="row m-t30 p-t20">
                    <div class="col-xs-6 col-sm-3">
                        <div class="mat-cat-card">
                            <img class="mat-cat-card__pic"
                                 src="https://static.treatstock.com/static/images/business-page/service-cnc.jpg"
                                 alt="<?= _t('site.page', 'CNC'); ?>">
                            <h3 class="mat-cat-card__title"><?= _t('site.page', 'CNC'); ?> </h3>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="mat-cat-card">
                            <img class="mat-cat-card__pic"
                                 src="https://static.treatstock.com/static/images/business-page/service-casting.jpg"
                                 alt="<?= _t('site.page', 'Casting'); ?>">
                            <h3 class="mat-cat-card__title"><?= _t('site.page', 'Casting'); ?></h3>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="mat-cat-card">
                            <img class="mat-cat-card__pic"
                                 src="https://static.treatstock.com/static/images/business-page/service-molding.jpg"
                                 alt="<?= _t('site.page', 'Injection Molding'); ?>">
                            <h3 class="mat-cat-card__title"><?= _t('site.page', 'Injection Molding'); ?></h3>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="mat-cat-card">
                            <img class="mat-cat-card__pic"
                                 src="https://static.treatstock.com/static/images/business-page/service-lascut.jpg"
                                 alt="<?= _t('site.page', 'Laser cutting'); ?>">
                            <h3 class="mat-cat-card__title"><?= _t('site.page', 'Laser cutting'); ?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="become-sup-section" id="custom">
    <div class="container">
        <h3 class="become-sup-h3 text-center m-b30"><?= _t('site.page', 'Custom products'); ?></h3>
        <h2 class="become-sup-h2 text-center">
            <strong><?= _t('site.page', 'Sell Your Custom Products on Treastock Globally'); ?></strong>
        </h2>

        <div class="text-center m-b30 p-b30">
            <?php if(is_guest()) { ?>
                <a class="btn btn-danger" href="/mybusiness/company" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company', 'signUp');return false;" : "return true;" ?>"><?= _t('site.page', 'Join Selling Platform'); ?></a>
            <?php } else { ?>
                <a class="btn btn-danger" href="/mybusiness/company"><?= _t('site.page', 'Edit Selling Platform'); ?></a>
            <?php } ?>
        </div>


        <div class="row m-t30">
            <div class="col-sm-6 col-md-3">
                <img class="become-sup-icon" src="https://static.treatstock.com/static/images/common/big-icons/worldwide.svg" alt="">
                <h3 class="m-b30"><?= _t('site.page', 'New international customers'); ?></h3>
            </div>
            <div class="col-sm-6 col-md-3">
                <img class="become-sup-icon" src="https://static.treatstock.com/static/images/common/big-icons/instant.svg" alt="">
                <h3 class="m-b30"><?= _t('site.page', 'Unlimited products'); ?></h3>
            </div>
            <div class="col-sm-6 col-md-3">
                <img class="become-sup-icon" src="https://static.treatstock.com/static/images/common/big-icons/wallet.svg" alt="">
                <h3 class="m-b30"><?= _t('site.page', 'Boost sales and leads'); ?></h3>
            </div>
            <div class="col-sm-6 col-md-3">
                <img class="become-sup-icon" src="https://static.treatstock.com/static/images/common/big-icons/business-tools.svg" alt="">
                <h3 class="m-b30"><?= _t('site.page', 'Free useful tools and features'); ?></h3>
            </div>
        </div>

        <h3 class="text-center">
            <?php if(is_guest()) { ?>
                <a href="/mybusiness/widgets/printers" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/widgets/printers', 'signUp');return false;" : "return true;" ?>"><?= _t('site.page', 'More tools and features to boost sales'); ?></a>
            <?php } else { ?>
                <a href="/mybusiness/widgets/printers"><?= _t('site.page', 'More tools and features to boost sales'); ?></a>
            <?php } ?>
        </h3>

    </div>
</section>


<section class="become-sup-section become-sup-section--grey">
    <div class="container">

        <h2 class="become-sup-h2 text-center">
            <strong><?= _t('site.page', 'How Treatstock Works'); ?></strong>
        </h2>

        <div class="row">
            <div class="col-sm-4 m-b30">
                <h3><?= _t('site.page', '1. Register'); ?></h3>

                <p style="font-size: 18px;line-height: 1.4">
                    <?= _t('site.page', 'Register your business on Treatstock. Reach to over 100 000+ active customers monthly'); ?>
                </p>
            </div>
            <div class="col-sm-4 m-b30">
                <h3><?= _t('site.page', '2. Use Tools'); ?></h3>

                <p style="font-size: 18px;line-height: 1.4">
                    <?= _t('site.page', 'Take advantage of our listing and orders distribution system based on instant price calculation or quote placing algorithm.'); ?>
                </p>
            </div>
            <div class="col-sm-4 m-b30">
                <h3><?= _t('site.page', '3. Grow Your Business'); ?></h3>

                <p style="font-size: 18px;line-height: 1.4">
                    <?= _t('site.page', 'Receive orders form our platform and focus on quality of parts and products you produce. We will care about tools and services for you.'); ?>
                </p>
            </div>
        </div>
    </div>
</section>


<div class="container-fliud ps-land ps-land-reviews border-0">
    <div class="container">
        <h2 class="ps-land-reviews__title">
            <?= _t('site.ps', 'What Print Services are Saying'); ?>
        </h2>
        <div class="row p-b30">
            <div class="col-xs-12 ">
                <div class="ps-land-reviews__swiper swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img class="ps-land-reviews__avatar" src="https://static.treatstock.com/static/user/fcdf25d6e191893e705819b177cddea0/avatar_1478208742_240x240.jpg">
                            <div class="ps-land-reviews__info">
                                <h4 class="ps-land-reviews__ps-name">
                                    <a href="https://www.treatstock.com/c/eds-printing" target="_blank">Ed's Printing</a>
                                </h4>
                                <div class="ps-land-reviews__ps-owner">
                                    Edward Fries
                                </div>
                                <p class="ps-land-reviews__text">
                                    "<?= _t('site.ps', 'I have had a wonderful experience printing on Treatstock! I average about 3-4 print jobs per week and their support services have been very helpful when I have had issues printing a model. Most importantly, what I earn for my services goes directly into my Treastock account. There are never any extra service fees from Treastock, my earnings are mine!'); ?>"
                                </p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <img class="ps-land-reviews__avatar" src="https://static.treatstock.com/static/user/c3614206a443012045cfd75d2600af2d/ps_logo_1476310929_240x240.jpg">
                            <div class="ps-land-reviews__info">
                                <h4 class="ps-land-reviews__ps-name">
                                    <a href="https://www.treatstock.com/c/jwcproductions" target="_blank">Jwcproductions</a>
                                </h4>
                                <div class="ps-land-reviews__ps-owner">
                                    Jim Carter
                                </div>
                                <p class="ps-land-reviews__text">
                                    "<?= _t('site.ps', 'Treatstock is great to deal with and have lots of great local printers! The registration process is much better than the competitors out there and I also like that there is a "Pro" level of printer certification.'); ?>"
                                </p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <img class="ps-land-reviews__avatar" src="https://static.treatstock.com/static/user/0e087ec55dcbe7b2d7992d6b69b519fb/ps_logo_1479425253_240x240.jpg">
                            <div class="ps-land-reviews__info">
                                <h4 class="ps-land-reviews__ps-name">
                                    <a href="https://www.treatstock.com/c/dylco3d" target="_blank">DYLCO3D</a>
                                </h4>
                                <div class="ps-land-reviews__ps-owner">
                                    Dylan Wallis
                                </div>
                                <p class="ps-land-reviews__text">
                                    "<?= _t('site.ps', 'I like the fact that treatstock.com can connect my print service with customers all over the world, from just a few states away to other countries.'); ?>"
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ps-land-reviews__pagination swiper-pagination"></div>
                </div>
            </div>
        </div>

        <h2 class="m-t30 m-b30 p-t20 text-center">
            <?= _t('site.page', 'Select type of your business and start now'); ?>
        </h2>
        <div class="row text-center">
            <div class="col-md-4">
                <?php if(is_guest()) { ?>
                    <a href="/mybusiness/company" class="btn btn-primary m-b30"  onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company', 'signUp');return false;" : "return true;" ?>">
                        <?= _t('site.page', 'Additive manufacturing'); ?>
                    </a>
                <?php } else { ?>
                    <a href="/mybusiness/company" class="btn btn-primary m-b30">
                        <?= _t('site.page', 'Additive manufacturing'); ?>
                    </a>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <?php if(is_guest()) { ?>
                    <a href="/mybusiness/company" class="btn btn-primary m-b30"  onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company', 'signUp');return false;" : "return true;" ?>">
                        <?= _t('site.page', 'Machining and Cutting'); ?>
                    </a>
                <?php } else { ?>
                    <a href="/mybusiness/company" class="btn btn-primary m-b30">
                        <?= _t('site.page', 'Machining and Cutting'); ?>
                    </a>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <?php if(is_guest()) { ?>
                    <a href="/mybusiness/company" class="btn btn-primary m-b30"  onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/mybusiness/company', 'signUp');return false;" : "return true;" ?>">
                        <?= _t('site.page', 'Custom products'); ?>
                    </a>
                <?php } else { ?>
                    <a href="/mybusiness/company" class="btn btn-primary m-b30">
                        <?= _t('site.page', 'Custom products'); ?>
                    </a>
                <?php } ?>
            </div>
        </div>

        <p class="m-t20 text-center"><?= _t('site.page', 'Register your business and join our global manufacturing network on Treatstock'); ?></p>

    </div>
</div>


<script>
    <?php $this->beginBlock('js1', false); ?>

    $('a[data-target]').click(function () {
        var scroll_elTarget = $(this).attr('data-target');
        if ($(scroll_elTarget).length != 0) {
            $('html, body').animate({scrollTop: $(scroll_elTarget).offset().top - 105}, 350);
        }
        return false;
    });

    //Init slider for material examples
    var swiperPsPortfolio = new Swiper('.designer-card__ps-portfolio', {
        scrollbar: '.designer-card__ps-portfolio-scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });

    //Init slider for .main-page-promo
    var swiperPsLand = new Swiper('.ps-land-reviews__swiper', {
        pagination: '.ps-land-reviews__pagination',
        paginationClickable: true,
        slidesPerView: 1,
        spaceBetween: 30,
        grabCursor: true,
        autoplay: 5000,
        speed: 700,
        loop: true,

        breakpoints: {
            600: {
                slidesPerView: 1
            }
        }
    });

    <?php $this->endBlock(); ?>
</script>

<?php $this->registerJs($this->blocks['js1']); ?>