<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

$this->title = "Father's Day";

?>

<style>
    .fatherday-header {
        width: 100%;
        max-height: 540px;
        margin: 0 0 10px;
        padding: 10% 0;
        background: url("https://static.treatstock.com/static/images/fathersday-header.jpg") no-repeat bottom center transparent;
        background-size: cover;
    }
    .fatherday-header__title {
        margin: 0;
        color: #ffffff;
        font-size: 50px;
        line-height: 50px;
    }
    .catalog-item__category,
    .catalog-item__controls,
    .catalog-item__footer__stats__likes {
        display: none !important;
    }
    .second-header {
        text-align: center;
    }
    .second-header:last-child{
        margin-bottom: 30px;
    }
    @media (max-width: 400px) {
        .fatherday-header__title {
            font-size: 20px;
            line-height: 30px;
        }
        .second-header {
            margin: 0 0 10px;
            font-size: 14px;
            line-height: 20px;
            text-align: left;
        }
        .second-header:last-child{
            margin: 0 0 10px;
        }
    }
</style>

<div class="fatherday-header">
    <div class="container">
        <h1 class="fatherday-header__title">Unique Gifts for Father's Day</h1>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <h3 class="second-header">Each year we rack our brains trying to find the perfect gift for Dad on Father's Day.</h3>
            <h3 class="second-header">Thankfully since 3D printing has become available our choice of Father's Day presents have become much easier and better yet, more creative! </h3>
            <h3 class="second-header">These gift ideas are sure to make your Dad feel truly special!</h3>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="694" data-model3d-id="842" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=13">
                            For Home                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/MaxCooper" class="catalog-item__author__avatar">
                            <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/MaxCooper" class="catalog-item__author__name">
                            MaxCooper                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/842-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/842-item" title="Father's day gift set">
                        <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/model842/4259_412x309.png" alt="Father's day gift set">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/842-item">Father's day gift set</a></h4>

                    <div class="catalog-item__footer__price">
                        $20.81            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count js-store-item-likes-first">Like it first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="661" data-model3d-id="811" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=11">
                            Art                 </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/mao" class="catalog-item__author__avatar">
                            <img src="/static/user/471c75ee6643a10934502bdafee198fb/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/mao" class="catalog-item__author__name">
                            mao                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="811" data-file-id="4093">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/811-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/811-item" title="American car">
                        <img src="/static/user/471c75ee6643a10934502bdafee198fb/model811/4104_412x309.jpg" alt="American car">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/811-item">American car</a></h4>

                    <div class="catalog-item__footer__price">
                        $29.84            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">4 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="616" data-model3d-id="764" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=16">
                            Technology                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/ReA" class="catalog-item__author__avatar">
                            <img src="https://secure.gravatar.com/avatar/e6d68b524108a3bb8a6dfcab05058247?r=g&amp;s=64&amp;d=retro" alt="Gravatar image">                </a>
                        <a href="/u/ReA" class="catalog-item__author__name">
                            ReA                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="764" data-file-id="3926">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/764-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/764-item" title="Port sockets">
                        <img src="/static/user/991de292e76f74f3c285b3f6d57958d5/model764/3927_412x309.jpg" alt="Port sockets">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/764-item">Port sockets</a></h4>

                    <div class="catalog-item__footer__price">
                        $3.18            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">2 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="689" data-model3d-id="837" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=11">
                            Art                 </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/MaxCooper" class="catalog-item__author__avatar">
                            <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/MaxCooper" class="catalog-item__author__name">
                            MaxCooper                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="837" data-file-id="4239">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/837-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/837-item" title="Mug for Father's day">
                        <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/model837/4240_412x309.png" alt="Mug for Father's day">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/837-item">Mug for Father's day</a></h4>

                    <div class="catalog-item__footer__price">
                        $10.70            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">2 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="502" data-model3d-id="628" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=13">
                            For Home                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/EnterpriseXDDesign" class="catalog-item__author__avatar">
                            <img src="/static/user/82965d4ed8150294d4330ace00821d77/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/EnterpriseXDDesign" class="catalog-item__author__name">
                            EnterpriseXDDesign                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="628" data-file-id="3169">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/628-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/628-item" title="Katana Sword Display Stand">
                        <img src="/static/user/82965d4ed8150294d4330ace00821d77/model628/4212_412x309.jpg" alt="Katana Sword Display Stand">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/628-item">Katana Sword Display Stand</a></h4>

                    <div class="catalog-item__footer__price">
                        $49.41            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">1 like</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="688" data-model3d-id="836" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=13">
                            For Home                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/MaxCooper" class="catalog-item__author__avatar">
                            <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/MaxCooper" class="catalog-item__author__name">
                            MaxCooper                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="836" data-file-id="4236">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/836-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/836-item" title="Iphone case for Father's day">
                        <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/model836/4238_412x309.png" alt="Iphone case for Father's day">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/836-item">Iphone case for Father's day</a></h4>

                    <div class="catalog-item__footer__price">
                        $2.01            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count js-store-item-likes-first">Like it first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="631" data-model3d-id="782" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=18">
                            Sport                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/davide-giordano" class="catalog-item__author__avatar">
                            <img src="/static/user/facebook_918169188298824/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/davide-giordano" class="catalog-item__author__name">
                            davide-giordano                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="782" data-file-id="3992">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/782-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/782-item" title="180 mm Curve Arm for GoPro/SjCAM">
                        <img src="/static/user/25df35de87aa441b88f22a6c2a830a17/model782/3998_412x309.jpg" alt="180 mm Curve Arm for GoPro/SjCAM">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/782-item">180 mm Curve Arm for GoPro/SjCAM</a></h4>

                    <div class="catalog-item__footer__price">
                        $7.82            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">1 like</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="614" data-model3d-id="762" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=16">
                            Technology                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/ReA" class="catalog-item__author__avatar">
                            <img src="https://secure.gravatar.com/avatar/e6d68b524108a3bb8a6dfcab05058247?r=g&amp;s=64&amp;d=retro" alt="Gravatar image">                </a>
                        <a href="/u/ReA" class="catalog-item__author__name">
                            ReA                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="762" data-file-id="3916">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/762-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/762-item" title="Bit holder for screwdriver">
                        <img src="/static/user/991de292e76f74f3c285b3f6d57958d5/model762/3918_412x309.jpg" alt="Bit holder for screwdriver">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/762-item">Bit holder for screwdriver</a></h4>

                    <div class="catalog-item__footer__price">
                        $2.50            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">3 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="588" data-model3d-id="731" data-is-liked="1">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=13">
                            For Home                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/olle-gellert" class="catalog-item__author__avatar">
                            <img src="/static/user/1f3202d820180a39f736f20fce790de8/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/olle-gellert" class="catalog-item__author__name">
                            olle-gellert                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like js-store-item-like-showed">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Unlike</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="731" data-file-id="3621">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/731-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/731-item" title="P2B-joint collection">
                        <img src="/static/user/1f3202d820180a39f736f20fce790de8/model731/3652_412x309.jpg" alt="P2B-joint collection">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/731-item">P2B-joint collection</a></h4>

                    <div class="catalog-item__footer__price">
                        $106.76            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">5 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="464" data-model3d-id="578" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=16">
                            Technology                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/EnterpriseXDDesign" class="catalog-item__author__avatar">
                            <img src="/static/user/82965d4ed8150294d4330ace00821d77/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/EnterpriseXDDesign" class="catalog-item__author__name">
                            EnterpriseXDDesign                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="578" data-file-id="2965">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/578-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/578-item" title="Snaplock Pint Holder for Mic Stand Mk 2">
                        <img src="/static/user/82965d4ed8150294d4330ace00821d77/model578/2970_412x309.jpg" alt="Snaplock Pint Holder for Mic Stand Mk 2">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/578-item">Snaplock Pint Holder for Mic Stand Mk 2</a></h4>

                    <div class="catalog-item__footer__price">
                        $29.25            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count js-store-item-likes-first">Like it first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="690" data-model3d-id="838" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=11">
                            Art                 </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/MaxCooper" class="catalog-item__author__avatar">
                            <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/MaxCooper" class="catalog-item__author__name">
                            MaxCooper                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="838" data-file-id="4241">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/838-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/838-item" title="Tabletop sign for Father's day">
                        <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/model838/4261_412x309.png" alt="Tabletop sign for Father's day">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/838-item">Tabletop sign for Father's day</a></h4>

                    <div class="catalog-item__footer__price">
                        $6.88            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count js-store-item-likes-first">Like it first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="382" data-model3d-id="478" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=18">
                            Sport                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/stopaginn" class="catalog-item__author__avatar">
                            <img src="/static/user/3546ab441e56fa333f8b44b610d95691/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/stopaginn" class="catalog-item__author__name">
                            stopaginn                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="478" data-file-id="2509">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/478-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/478-item" title="iPhone 6 Trim Case">
                        <img src="/static/user/3546ab441e56fa333f8b44b610d95691/model478/2508_412x309.jpg" alt="iPhone 6 Trim Case">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/478-item">iPhone 6 Trim Case</a></h4>

                    <div class="catalog-item__footer__price">
                        $29.64            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">2 likes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="309" data-model3d-id="384" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=21">
                            Miniatures                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/REDA" class="catalog-item__author__avatar">
                            <img src="/static/user/a284df1155ec3e67286080500df36a9a/avatar_64x64.png" alt="">                </a>
                        <a href="/u/REDA" class="catalog-item__author__name">
                            REDA                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="384" data-file-id="2175">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/384-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/384-item" title="Twin fuselage 1.1">
                        <img src="/static/user/a284df1155ec3e67286080500df36a9a/model384/2155_412x309.jpg" alt="Twin fuselage 1.1">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/384-item">Twin fuselage 1.1</a></h4>

                    <div class="catalog-item__footer__price">
                        $403.41            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count js-store-item-likes-first">Like it first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="257" data-model3d-id="329" data-is-liked="1">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=16">
                            Technology                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/SickSick6" class="catalog-item__author__avatar">
                            <img src="/static/user/2de5d16682c3c35007e4e92982f1a2ba/avatar_64x64.png" alt="">                </a>
                        <a href="/u/SickSick6" class="catalog-item__author__name">
                            SickSick6                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like js-store-item-like-showed">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Unlike</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="329" data-file-id="1940">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/329-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/329-item" title="The Watt, iphone charger sleeve">
                        <img src="https://static.treatstock.com/static/user/2de5d16682c3c35007e4e92982f1a2ba/model329/1937_720x540.jpg" alt="The Watt, iphone charger sleeve">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/329-item">The Watt, iphone charger sleeve</a></h4>

                    <div class="catalog-item__footer__price">
                        $6.83            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count ">1 like</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="catalog-item" data-store-item-id="691" data-model3d-id="839" data-is-liked="">        <div class="catalog-item__pic">
                    <div class="catalog-item__category">
                        <a class="label label-primary" href="/3d-printable-models?category=13">
                            For Home                </a>
                    </div>
                    <div class="catalog-item__author">
                        <a href="/u/MaxCooper" class="catalog-item__author__avatar">
                            <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/avatar_64x64.jpg" alt="">                </a>
                        <a href="/u/MaxCooper" class="catalog-item__author__name">
                            MaxCooper                </a>
                    </div>

                    <div class="catalog-item__controls">


                        <div class="catalog-item__controls__item js-store-item-like ">
                            <span class="tsi tsi-heart"></span>
                            <span class="catalog-item__controls__item__hint js-store-item-like-label">Like</span>
                        </div>



                        <div class="catalog-item__controls__item data360" data-model3d-id="839" data-file-id="4243">
                            <span class="tsi tsi-cube"></span>
                            <span class="catalog-item__controls__item__hint">Show 360 view</span>
                        </div>


                        <div class="catalog-item__controls__item"><a href="/3d-printable-models/839-item">
                                <span class="tsi tsi-printer3d"></span>
                                <span class="catalog-item__controls__item__hint">Print It</span></a>
                        </div>
                    </div>

                    <a class="catalog-item__pic__img" href="/3d-printable-models/839-item" title="Superdaddy badge">
                        <img src="/static/user/82b8a3434904411a9fdc43ca87cee70c/model839/4252_412x309.png" alt="Superdaddy badge">
                    </a>
                </div>

                <div class="catalog-item__footer">
                    <h4 class="catalog-item__footer__title"><a href="/3d-printable-models/839-item">Superdaddy badge</a></h4>

                    <div class="catalog-item__footer__price">
                        $1.22            </div>

                    <div class="catalog-item__footer__stats">

                        <!--
                            <div class="catalog-item__footer__stats__reviews">
                                <span class="tsi tsi-comment"></span> 0 reviews
                            </div>
                        -->

                        <div class="catalog-item__footer__stats__likes">
                            <span class="tsi tsi-heart"></span>
                            <span class="js-store-item-likes-count js-store-item-likes-first">Like it first</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>