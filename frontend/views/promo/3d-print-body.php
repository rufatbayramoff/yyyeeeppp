<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

/* @var $this yii\web\View */

use common\modules\catalogPs\models\CatalogSearchForm;

use common\models\Ps;

$this->registerAssetBundle(\frontend\assets\IndexPageAsset::class);
$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

?>

<?= $this->render('/common/ie-alert.php'); ?>


<div class="container p-t20 p-b30">


    <div class="row">
        <div class="col-sm-6">

            <h1 class="animated m-b20" data-animation-in="fadeInDown" data-animation-out="">
                <?= _t('site.ps', '3D Print custom cases and housing'); ?>
            </h1>

            <p class="lead">
                <?= _t('site.ps', 'Simply upload the file to get it 3D printed'); ?>
            </p>

            <p>Заказывай качественно напечатанные кастомные корпуса. Не сиди с голой железкой без корпуса! А то чё как лох?!</p>

            <a href="#go-to-upload" class="fileinput-button btn btn-danger m-t10 m-b10 dz-clickable">
                <span class="tsi tsi-upload-l m-r10"></span>
                Order 3D print
            </a>

        </div>
        <div class="col-sm-6 wide-padding--left">
            <img class="img-responsive m-t30 m-b10"
                src="http://www.papaya3dp.com/uploads/personal/C001830/images/papaya3dp-3d-printing-service-sla-electronics-case-fit-test-full.gif"
                alt="">

        </div>
    </div>
</div>



<div class="site-index">

    <div class="customer-reviews">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="customer-reviews__title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'Check out examples of 3D printed cases'); ?>
                    </h2>
                </div>
            </div>
        </div>

        <div class="customer-reviews__slider swiper-container">
            <div class="swiper-wrapper customer-reviews__gallery">
                <div class="customer-reviews__slider-item swiper-slide p-b0">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/4.jpg?08092017" alt="Maiks Printhub review"/>
                </div>
                <div class="customer-reviews__slider-item swiper-slide p-b0">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/5.jpg?08092017" alt="Keymedia prints review" />
                </div>
                <div class="customer-reviews__slider-item swiper-slide p-b0">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/2.jpg?08092017" alt="The Thing Loft review"/>
                </div>
                <div class="customer-reviews__slider-item swiper-slide p-b0">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/1.jpg?08092017" alt="Seaside 3d review"/>
                </div>
                <div class="customer-reviews__slider-item swiper-slide p-b0">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/7.jpg?08092017" alt="Northworks Digital Factory review" />
                </div>
                <div class="customer-reviews__slider-item swiper-slide p-b0">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/3.jpg?08092017" alt="Archanias Workshop review"/>
                </div>
                <div class="customer-reviews__slider-item swiper-slide p-b0">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/6.jpg?08092017" alt="Laughing Monkey Labs llc review"/>
                </div>
            </div>

            <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
        </div>

    </div>

    <div class="how-ts-customers" id="how-ts-customers">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="how-ts-customers__title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'How does Treatstock work?'); ?>
                    </h2>
                    <p class="how-ts-customers__about"><?= _t('front', 'Treatstock is an online platform that allows clients to find, compare, and place orders with professional manufacturers for customized and on-demand products and services that are made fast and at a high quality'); ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                    <div class="row">
                        <div class="col-sm-6">
                            <figure class="how-ts-customers__img">
                                <img src="/static/images/main-page-icons/mp-laptop.jpg?08092017" alt="">
                            </figure>
                        </div>
                        <div class="col-sm-6">
                            <h4 class="how-ts-customers__subtitle">
                                <span class="how-ts-customers__count">1</span><?= _t('front', 'Upload Model'); ?>
                            </h4>
                            <ul class="how-ts-customers__list">
                                <li><?= _t('front', 'Upload your files'); ?></li>
                                <li><?= _t('front', 'Find 3D models in our catalog'); ?></li>
                                <li><?= _t('front', 'Hire a designer to help bring your ideas to life'); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 how-ts-customers__step2 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <div class="row">
                        <div class="col-sm-6 col-md-5 col-md-offset-1">
                            <h4 class="how-ts-customers__subtitle">
                                <span class="how-ts-customers__count">2</span><?= _t('front', 'Select a Manufacturer'); ?>
                            </h4>
                            <ul class="how-ts-customers__list">
                                <li><?= _t('front', 'Choose from over 100 materials and colors'); ?></li>
                                <li><?= _t('front', 'Compare prices, reviews and locations'); ?></li>
                                <li><?= _t('front', 'Place an order with a manufacturer of your choice'); ?></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <figure class="how-ts-customers__img">
                                <img src="/static/images/main-page-icons/mp-3d.jpg?08092017" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 how-ts-customers__step3 animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                    <div class="row">
                        <div class="col-sm-6">
                            <figure class="how-ts-customers__img">
                                <img src="/static/images/main-page-icons/mp-box.jpg?08092017" alt="">
                            </figure>
                        </div>
                        <div class="col-sm-6">
                            <h4 class="how-ts-customers__subtitle">
                                <span class="how-ts-customers__count">3</span><?= _t('front', 'Order Delivery'); ?>
                            </h4>
                            <ul class="how-ts-customers__list">
                                <li><?= _t('front', 'Track the production process of your order'); ?></li>
                                <li><?= _t('front', 'Fast delivery within 5 days with free shipping insurance'); ?></li>
                                <li><?= _t('front', 'Our friendly customer support team is always here to help'); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<script>
    <?php $this->beginBlock('js1', false); ?>


    $('.why-ts__cta.btn-link').click(function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $('.how-ts-customers').offset().top - 65
        }, 350);
    });

    //Init slider for PS portfolio
    var swiperFeaturedPS = new Swiper('.designer-card__ps-portfolio', {
        scrollbar: '.designer-card__ps-portfolio-scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });

    function initPSSliders() {
        //Init slider for feat-manufacturers
        var swiperPs = new Swiper('.feat-manufacturers__list', {
            slidesPerView: 'auto',
            scrollbar: '.feat-manufacturers__scrollbar',
            scrollbarHide: true,
            spaceBetween: 0,
            grabCursor: true,

            breakpoints: {
                500: {
                    slidesPerView: 1.15,
                    centeredSlides: false
                },
                767: {
                    slidesPerView: 2.25
                }
            }
        });
    }

    initPSSliders();
    $(window).resize(function () {initPSSliders()});

    function initPrints() {
        //Init slider for .customer-reviews__slider
        var swiperPrints = new Swiper('.customer-reviews__slider', {
            scrollbar: '.customer-reviews__scrollbar',
            scrollbarHide: true,
            slidesPerView: 'auto',
            grabCursor: true
        });
    }

    initPrints();
    $(window).resize(function () {initPrints()});

    // Turn on sliders on mobile devices
    function initMobileSliders() {
        if ( $('html').hasClass('mobile') ) {

            //Init slider for .main-page-blog
            var swiperBlog = new Swiper('.post-list', {
                grabCursor: true,
                slidesPerView: 1.15,
                spaceBetween: 0,
                centeredSlides: false
            });

        }
    }

    initMobileSliders();
    $(window).resize(function () {initMobileSliders()});


    <?php $this->endBlock(); ?>
</script>
<?php $this->registerJs($this->blocks['js1']); ?>

