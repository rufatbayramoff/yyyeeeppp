<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

/**
 * @var \yii\web\View $this
 * @var \common\models\HomePageCategoryBlock[] $productCategoriesBlock
 * @var \common\models\HomePageCategoryCard[] $productCategoryCard
 * @var \common\models\HomePageFeatured[] $featuredCategories
 */

use yii\helpers\Html;

$this->registerAssetBundle(\frontend\assets\IndexPageAsset::class);
$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

?>

<?= $this->render('/common/ie-alert.php'); ?>


<div class="hero-banner hero-banner--light-grey">
    <div class="container container--wide">
        <div class="products-promo-hero">

            <div class="products-promo-hero__prod">
                <div class="hero-banner__prod-list">
                    <a class="hero-banner__prod-item" href="/products/tools-general-machinery">Tools &amp; General
                        Machinery</a>
                    <a class="hero-banner__prod-item" href="/products/furniture-furnishings">Furniture
                        &amp; Furnishings</a>
                    <a class="hero-banner__prod-item"
                       href="/products/consumer-electronics-appliances">Consumer Electronics
                        &amp; Appliances</a>
                    <a class="hero-banner__prod-item"
                       href="/products/distribution-conditioning-systems">Distribution &amp;
                        Conditioning Systems</a>
                    <a class="hero-banner__prod-item" href="/products/published-products">Published
                        Products</a>
                    <a class="hero-banner__prod-item" href="/products/apparel-luggage-personal-care">Apparel,
                        Luggage &amp; Personal Care</a>
                    <a class="hero-banner__prod-item"
                       href="/products/office-equipment-supplies">Office Equipment
                        &amp; Supplies</a>
                    <a class="hero-banner__prod-item"
                       href="/products/manufacturing-components-supplies">Manufacturing
                        Components &amp; Supplies</a>
                </div>
                <a href="#3Dmodels" class="btn btn-default products-promo-hero__prod-btn">
                    <?php echo _t('site.store', '3D Models')?>
                </a>
            </div>

            <div class="products-promo-hero__banner">
                <div class="products-promo-hero__banner-cont swiper-container swiper-container-horizontal" style="cursor: grab;">

                    <div class="swiper-wrapper" style="transform: translate3d(-1180px, 0px, 0px); transition-duration: 0ms;"><div class="products-promo-hero__banner-item swiper-slide swiper-slide-duplicate" data-swiper-slide-index="3" style="width: 560px; margin-right: 30px;">
                            <a href="/apps" class="products-promo-hero__banner-inner">
                                <div class="products-promo-hero__banner-item-pic" style="background-image:url('https://static.treatstock.com/static/images/main-page-temp/mp-bnr-apps.jpg')"></div>
                                <h2 class="products-promo-hero__banner-item-title">Treatstock Apps</h2>

                                <p class="products-promo-hero__banner-item-text">
                                    Free online apps for designing &amp; manufacturing                                    </p>
                                <ul class="checked-list products-promo-hero__banner-item-list">
                                    <li>Watermark 3D</li>
                                    <li>TouchSee</li>
                                    <li>CroudCad</li>
                                    <li>Facebook app &amp; more</li>
                                </ul>
                                <button class="btn btn-primary products-promo-hero__banner-cta">Explore apps</button>
                            </a>
                        </div>

                        <div class="products-promo-hero__banner-item swiper-slide swiper-slide-prev" data-swiper-slide-index="0" style="width: 560px; margin-right: 30px;">
                            <a href="/mybusiness/widgets/affiliate" class="products-promo-hero__banner-inner" onclick="return true;">
                                <div class="products-promo-hero__banner-item-pic" style="background-image:url('https://static.treatstock.com/static/images/main-page-temp/mp-bnr-tools.jpg')"></div>
                                <h2 class="products-promo-hero__banner-item-title">Widgets</h2>

                                <p class="products-promo-hero__banner-item-text">
                                    Use our free tools on your website to increase online sales                                    </p>
                                <ul class="checked-list products-promo-hero__banner-item-list">
                                    <li>Instant quotes, payments &amp; orders</li>
                                    <li>Virtual storefront for selling products securely</li>
                                    <li>Promote your business on multiple platforms</li>
                                    <li>Gain rewards for client referrals</li>
                                </ul>
                                <button class="btn btn-primary products-promo-hero__banner-cta">Try tools</button>
                            </a>
                        </div>

                        <div class="products-promo-hero__banner-item swiper-slide swiper-slide-active" data-swiper-slide-index="1" style="width: 560px; margin-right: 30px;">
                            <a href="/mybusiness/company" class="products-promo-hero__banner-inner products-promo-hero__banner-inner--dark" onclick="return true;">
                                <div class="products-promo-hero__banner-item-pic" style="background-image:url('https://static.treatstock.com/static/images/main-page-temp/welcome-to-join2.jpg')"></div>
                                <h2 class="products-promo-hero__banner-item-title">Treatstock Global Selling</h2>

                                <p class="products-promo-hero__banner-item-text">
                                    Smart B2B e-commerce platform with over 97,000 business customers                                    </p>
                                <ul class="checked-list products-promo-hero__banner-item-list">
                                    <li>Expand your market reach</li>
                                    <li>Generate sales leads</li>
                                    <li>Build new business relationships</li>
                                    <li>Trade with confidence</li>
                                </ul>
                                <button class="btn btn-primary products-promo-hero__banner-cta">Join for free</button>
                            </a>
                        </div>

                        <div class="products-promo-hero__banner-item swiper-slide swiper-slide-next" data-swiper-slide-index="2" style="width: 560px; margin-right: 30px;">
                            <a href="/my/print-model3d?utm_source=banner_upload" class="products-promo-hero__banner-inner">
                                <div class="products-promo-hero__banner-item-pic" style="background-image:url('https://static.treatstock.com/static/images/main-page-temp/mp-bnr-rapid-prot-dark.jpg')"></div>
                                <h2 class="products-promo-hero__banner-item-title">Rapid Prototyping</h2>

                                <p class="products-promo-hero__banner-item-text">
                                    Search &amp; compare professional 3D printing services worldwide                                    </p>
                                <ul class="checked-list products-promo-hero__banner-item-list">
                                    <li>Best prices online guaranteed!</li>
                                    <li>2000+ verified manufacturers</li>
                                    <li>4.8/5 average star ratings</li>
                                    <li>5 day turnarounds</li>
                                </ul>
                                <button class="btn btn-primary products-promo-hero__banner-cta">Get started</button>
                            </a>
                        </div>

                        <div class="products-promo-hero__banner-item swiper-slide" data-swiper-slide-index="3" style="width: 560px; margin-right: 30px;">
                            <a href="/apps" class="products-promo-hero__banner-inner">
                                <div class="products-promo-hero__banner-item-pic" style="background-image:url('https://static.treatstock.com/static/images/main-page-temp/mp-bnr-apps.jpg')"></div>
                                <h2 class="products-promo-hero__banner-item-title">Treatstock Apps</h2>

                                <p class="products-promo-hero__banner-item-text">
                                    Free online apps for designing &amp; manufacturing                                    </p>
                                <ul class="checked-list products-promo-hero__banner-item-list">
                                    <li>Watermark 3D</li>
                                    <li>TouchSee</li>
                                    <li>CroudCad</li>
                                    <li>Facebook app &amp; more</li>
                                </ul>
                                <button class="btn btn-primary products-promo-hero__banner-cta">Explore apps</button>
                            </a>
                        </div>

                        <div class="products-promo-hero__banner-item swiper-slide swiper-slide-duplicate" data-swiper-slide-index="0" style="width: 560px; margin-right: 30px;">
                            <a href="/mybusiness/widgets/affiliate" class="products-promo-hero__banner-inner" onclick="return true;">
                                <div class="products-promo-hero__banner-item-pic" style="background-image:url('https://static.treatstock.com/static/images/main-page-temp/mp-bnr-tools.jpg')"></div>
                                <h2 class="products-promo-hero__banner-item-title">Widgets</h2>

                                <p class="products-promo-hero__banner-item-text">
                                    Use our free tools on your website to increase online sales                                    </p>
                                <ul class="checked-list products-promo-hero__banner-item-list">
                                    <li>Instant quotes, payments &amp; orders</li>
                                    <li>Virtual storefront for selling products securely</li>
                                    <li>Promote your business on multiple platforms</li>
                                    <li>Gain rewards for client referrals</li>
                                </ul>
                                <button class="btn btn-primary products-promo-hero__banner-cta">Try tools</button>
                            </a>
                        </div></div>


                    <div class="products-promo-hero__banner-pagination swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span></div>


                </div>

            </div>

            <div class="hero-banner__features">
                <a href="https://www.treatstock.com/" class="hero-banner__features-img">
                    <img src="https://ts.h5.tsdev.work/static/fxd/product_main_promobar/images/prods-bonus-32_400x600.jpg?date=1633015404">
                    <!-- img 2:3 -->
                </a>
            </div>
        </div>
    </div>
</div>

<div class="site-index">

    <div class="promo-blocks-6">
        <div class="container container--wide js-main-page-shop-by-category">
            <div class="promo-blocks-6__list swiper-container">
                <div class="swiper-wrapper">
                    <a href="/products" class="promo-blocks-6__item swiper-slide t-promo-blocks-6__2">
                        <div class="promo-blocks-6__item-fade"></div>
                        <div class="promo-blocks-6__item-pic" style="background-image: url('http://static.treatstock.com/static/files/75/33/348271_0_d25069ee281b60a25025a4b2a0614337.jpg?date=1534953565')"></div>
                        <h4 class="promo-blocks-6__item-title">
                            All Categories&nbsp;<span class="tsi tsi-right"></span>
                        </h4>
                    </a>
                    <a href="/products/distribution-conditioning-systems" class="promo-blocks-6__item swiper-slide t-promo-blocks-6__3">
                        <div class="promo-blocks-6__item-fade"></div>
                        <div class="promo-blocks-6__item-pic" style="background-image: url('http://static.treatstock.com/static/files/df/13/348273_0_b421014f205b6efe0aff203c18412c3d.jpg?date=1534953603')"></div>
                        <h4 class="promo-blocks-6__item-title">
                            Distribution &amp; Conditioning Systems&nbsp;<span class="tsi tsi-right"></span>
                        </h4>
                    </a>
                    <a href="/products/tools-general-machinery" class="promo-blocks-6__item swiper-slide t-promo-blocks-6__4">
                        <div class="promo-blocks-6__item-fade"></div>
                        <div class="promo-blocks-6__item-pic" style="background-image: url('http://static.treatstock.com/static/files/5c/a9/348278_0_50f9545ec16a1885befee14d4d149d45.jpg?date=1534953731')"></div>
                        <h4 class="promo-blocks-6__item-title">
                            Tools &amp; General Machinery&nbsp;<span class="tsi tsi-right"></span>
                        </h4>
                    </a>
                    <a href="/products/published-products" class="promo-blocks-6__item swiper-slide t-promo-blocks-6__5">
                        <div class="promo-blocks-6__item-fade"></div>
                        <div class="promo-blocks-6__item-pic" style="background-image: url('http://static.treatstock.com/static/files/dc/58/348279_0_127a36a831471022a4e6d27045afd6fe.jpg?date=1534953750')"></div>
                        <h4 class="promo-blocks-6__item-title">
                            Published Products&nbsp;<span class="tsi tsi-right"></span>
                        </h4>
                    </a>
                    <a href="/products/manufacturing-components-supplies" class="promo-blocks-6__item swiper-slide t-promo-blocks-6__6">
                        <div class="promo-blocks-6__item-fade"></div>
                        <div class="promo-blocks-6__item-pic" style="background-image: url('http://static.treatstock.com/static/files/ba/a3/348280_0_cf94116c2f8fce96ee881a7c7e999ba9.jpg?date=1534953762')"></div>
                        <h4 class="promo-blocks-6__item-title">
                            Manufacturing Components &amp; Supplies&nbsp;<span class="tsi tsi-right"></span>
                        </h4>
                    </a>
                    <a href="/products/furniture-furnishings" class="promo-blocks-6__item swiper-slide t-promo-blocks-6__7">
                        <div class="promo-blocks-6__item-fade"></div>
                        <div class="promo-blocks-6__item-pic" style="background-image: url('http://static.treatstock.com/static/files/a7/85/348281_0_d80ed5f4ca3abc89766b567496a85818.jpg?date=1534953773')"></div>
                        <h4 class="promo-blocks-6__item-title">
                            Furniture &amp; Furnishings&nbsp;<span class="tsi tsi-right"></span>
                        </h4>
                    </a>
                </div>
                <div class="promo-blocks-6__scrollbar swiper-scrollbar"></div>
            </div>
        </div>
    </div>

    <div class="container container--wide m-t30">
        <div class="row">
            <div class="col-sm-6">
                <div class="prod-main-promo-card">
                    <h2 class="prod-main-promo-card__title">
                        <?= _t('site.products', 'Machines'); ?>
                    </h2>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="product-card">
                                <a class="product-card__pic" href="/product/1JS8pafInE9m9t-sla600-3d-printer"
                                   title="SLA600 3D printer" alt="SLA600 3D printer">
                                    <img src="https://static.treatstock.com/static/files/27/a3/2898381_123379_281d39e50603f98754d1d93f471b4a1f_720X540.png?date=1622103362"
                                         alt="SLA600 3D printer">
                                </a>
                                <a class="product-card__title" href="/product/1JS8pafInE9m9t-sla600-3d-printer"
                                   title="SLA600 3D printer">SLA600 3D printer</a>
                                <a href="https://www.treatstock.com/c/sla-3d-printing/products" target="_blank"
                                   class="product-card__supplier">SLA 3D Printing</a>
                                <div class="product-card__price"><span class="product-card__price-unit">from</span>
                                    $35,000.00<span class="product-card__price-unit">/pc.</span></div>
                                <div class="product-card__min-qty">
                                    <abbr title="Minimum order quantity">MOQ</abbr>:
                                    1 piece
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="product-card">
                                <a class="product-card__pic" href="/product/1xYkaiojDz5Wvv-solidcore-3d-printer"
                                   title="SolidCore 3D Printer" alt="SolidCore 3D Printer">
                                    <img src="https://static.treatstock.com/static/files/9e/24/2503951_108127_5368af3eed8391d3dc98b4e7558a3fcf_720X540.jpg?date=1611201229"
                                         alt="SolidCore 3D Printer">
                                </a>
                                <a class="product-card__title" href="/product/1xYkaiojDz5Wvv-solidcore-3d-printer"
                                   title="SolidCore 3D Printer">SolidCore 3D Printer</a>
                                <a href="https://www.treatstock.com/c/3d-distributed/products" target="_blank"
                                   class="product-card__supplier">3D Distributed</a>
                                <div class="product-card__price"><span class="product-card__price-unit">from</span>
                                    $150.00<span class="product-card__price-unit">/pc.</span></div>
                                <div class="product-card__min-qty">
                                    <abbr title="Minimum order quantity">MOQ</abbr>:
                                    1 piece
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="prod-main-promo-card">
                    <h2 class="prod-main-promo-card__title">
                        <?= _t('site.products', 'Materials'); ?>
                    </h2>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="product-card">
                                <a class="product-card__pic" href="/product/1zjUNY0y0vjvO0-bio-pla-white"
                                   title="BIO PLA WHITE" alt="BIO PLA WHITE">
                                    <img src="https://static.treatstock.com/static/files/27/14/2553139_110315_53e33a897fda34b4ed27e145242660c9_720X540.jpg?date=1612436397"
                                         alt="BIO PLA WHITE">
                                </a>
                                <a class="product-card__title" href="/product/1zjUNY0y0vjvO0-bio-pla-white"
                                   title="BIO PLA WHITE">BIO PLA WHITE</a>
                                <a href="https://www.treatstock.com/c/uab-fila-lab/products" target="_blank"
                                   class="product-card__supplier">filalab</a>
                                <div class="product-card__price"><span class="product-card__price-unit">from</span>
                                    $18.99<span class="product-card__price-unit">/pc.</span></div>
                                <div class="product-card__min-qty">
                                    <abbr title="Minimum order quantity">MOQ</abbr>:
                                    1 piece
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="product-card">
                                <a class="product-card__pic"
                                   href="/product/1laQzovRHPtkr7-inkrayonr-performance-pla-green"
                                   title="INKRAYON® | Performance PLA - Green"
                                   alt="INKRAYON® | Performance PLA - Green">
                                    <img src="https://static.treatstock.com/static/files/cb/aa/2092576_94038_d5fa81752847f577895584f71befedeb_720X540.jpeg?date=1599476541"
                                         alt="INKRAYON® | Performance PLA - Green">
                                </a>
                                <a class="product-card__title"
                                   href="/product/1laQzovRHPtkr7-inkrayonr-performance-pla-green"
                                   title="INKRAYON® | Performance PLA - Green">INKRAYON® | Performance PLA - Green</a>
                                <a href="https://www.treatstock.com/c/inkrayon/products" target="_blank"
                                   class="product-card__supplier">INKRAYON</a>
                                <div class="product-card__price"><span class="product-card__price-unit">from</span>
                                    $24.90<span class="product-card__price-unit">/pc.</span></div>
                                <div class="product-card__min-qty">
                                    <abbr title="Minimum order quantity">MOQ</abbr>:
                                    1 piece
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="promo-blocks-featured">
        <div class="container container--wide">

            <div class="row">
                <div class="col-xs-12">
                    <h2 class="promo-page-title animated fadeInDown" data-animation-in="fadeInDown" data-animation-out="fadeOutUp" style="opacity: 0;">
                        Featured products                               </h2>
                    <a href="/products/manufacturing-components-supplies" class="promo-page-title__link">View All</a>
                </div>
            </div>

            <div class="row">
                <div class="promo-blocks-featured__slider swiper-container">
                    <div class="swiper-wrapper">
                        <div class="col-sm-4 col-md-3 promo-blocks-featured__slider-item swiper-slide">
                            <div class="product-card">
                                <a class="product-card__pic" href="/product/cy0BKaFwwC8tb-iso-to-cf-flange" title="ISO to CF Flange" alt="ISO to CF Flange">
                                    <img src="http://static.treatstock.com/static/files/5d/f8/345497_32588_a1348bf2ece61015e850c5ea1e8ad059_210x210.jpg?date=1534798558" alt="ISO to CF Flange">
                                </a>
                                <a class="product-card__title" href="/product/cy0BKaFwwC8tb-iso-to-cf-flange" title="ISO to CF Flange">
                                    ISO to CF Flange                                            </a>
                                <a href="http://treatstock.com/c/vacuum-products-corp" target="_blank" class="product-card__supplier">
                                    Vacuum Products Corp.                                            </a>
                                <div class="product-card__price">
                                    $252.00<span class="product-card__price-unit">/piece</span>
                                </div>
                                <div class="product-card__min-qty">1 piece (<abbr title="Minimum order quantity">MOQ</abbr>)</div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-3 promo-blocks-featured__slider-item swiper-slide">
                            <div class="product-card">
                                <a class="product-card__pic" href="/product/9rnKsF5m0nbuN-stainless-steel-full-flow-quick-connect-body-22-cv-14-in-swagelok-tube-fitting" title="Stainless Steel Full Flow Quick Connect Body, 2.2 Cv, 1/4 in. Swagelok Tube Fitting" alt="Stainless Steel Full Flow Quick Connect Body, 2.2 Cv, 1/4 in. Swagelok Tube Fitting">
                                    <img src="http://static.treatstock.com/static/files/32/e2/312581_32517_e2bd32893ee266b6b0b506e373bba95e_210x210.jpg?date=1531952240" alt="Stainless Steel Full Flow Quick Connect Body, 2.2 Cv, 1/4 in. Swagelok Tube Fitting">
                                </a>
                                <a class="product-card__title" href="/product/9rnKsF5m0nbuN-stainless-steel-full-flow-quick-connect-body-22-cv-14-in-swagelok-tube-fitting" title="Stainless Steel Full Flow Quick Connect Body, 2.2 Cv, 1/4 in. Swagelok Tube Fitting">
                                    Stainless Steel Full Flow Quick Connect Body, 2.2 Cv, 1/4 in. Swagelok Tube Fitting                                            </a>
                                <a href="http://treatstock.com/c/swagelok-company" target="_blank" class="product-card__supplier">
                                    Swagelok Company                                            </a>
                                <div class="product-card__price">
                                    $38.70<span class="product-card__price-unit">/piece</span>
                                </div>
                                <div class="product-card__min-qty">1 piece (<abbr title="Minimum order quantity">MOQ</abbr>)</div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-3 promo-blocks-featured__slider-item swiper-slide">
                            <div class="product-card">
                                <a class="product-card__pic" href="/product/cy1y6140zvjj5-pneumatic-angle-valve---kf-flange" title="Pneumatic Angle Valve - KF Flange" alt="Pneumatic Angle Valve - KF Flange">
                                    <img src="http://static.treatstock.com/static/files/25/4f/345500_32588_e760a2cc0c5c1e308a6d08b6f3f8a6bc_210x210.jpg?date=1534798782" alt="Pneumatic Angle Valve - KF Flange">
                                </a>
                                <a class="product-card__title" href="/product/cy1y6140zvjj5-pneumatic-angle-valve---kf-flange" title="Pneumatic Angle Valve - KF Flange">
                                    Pneumatic Angle Valve - KF Flange                                            </a>
                                <a href="http://treatstock.com/c/vacuum-products-corp" target="_blank" class="product-card__supplier">
                                    Vacuum Products Corp.                                            </a>
                                <div class="product-card__price">
                                    $285.00<span class="product-card__price-unit">/piece</span>
                                </div>
                                <div class="product-card__min-qty">1 piece (<abbr title="Minimum order quantity">MOQ</abbr>)</div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-3 promo-blocks-featured__slider-item swiper-slide">
                            <div class="product-card">
                                <a class="product-card__pic" href="/product/9rqjIJhMzyWcr-stainless-steel-stem-protector-for-14-in-qc-series-instrumentation-quick-connect" title="Stainless Steel Stem Protector for 1/4 in. QC Series Instrumentation Quick Connect" alt="Stainless Steel Stem Protector for 1/4 in. QC Series Instrumentation Quick Connect">
                                    <img src="http://static.treatstock.com/static/files/8b/01/312595_32517_9d81b966c2f95eab61bce74718123c46_210x210.jpg?date=1531952853" alt="Stainless Steel Stem Protector for 1/4 in. QC Series Instrumentation Quick Connect">
                                </a>
                                <a class="product-card__title" href="/product/9rqjIJhMzyWcr-stainless-steel-stem-protector-for-14-in-qc-series-instrumentation-quick-connect" title="Stainless Steel Stem Protector for 1/4 in. QC Series Instrumentation Quick Connect">
                                    Stainless Steel Stem Protector for 1/4 in. QC Series Instrumentation Quick Connect                                            </a>
                                <a href="http://treatstock.com/c/swagelok-company" target="_blank" class="product-card__supplier">
                                    Swagelok Company                                            </a>
                                <div class="product-card__price">
                                    $16.90<span class="product-card__price-unit">/piece</span>
                                </div>
                                <div class="product-card__min-qty">1 piece (<abbr title="Minimum order quantity">MOQ</abbr>)</div>
                            </div>
                        </div>
                    </div>
                    <div class="promo-blocks-featured__scrollbar swiper-scrollbar"></div>
                </div>
            </div>
        </div>
    </div>

</div>




<?php #echo ShoppingCandidateWidget::widget();
function minifyJS($buffer) {

    $buffer = preg_replace("/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))/", "", $buffer);
    $buffer = str_replace(["\r\n","\r","\t","\n",'  ','    ','     '], '', $buffer);
    $buffer = preg_replace(['(( )+\))','(\)( )+)'], ')', $buffer);

    return $buffer;
}
$content = file_get_contents(Yii::getAlias('@frontend/web/js/mainpage.js'));

?>
<?php $this->registerJs(minifyJS($content)); ?>

