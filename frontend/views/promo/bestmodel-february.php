<?php

if(YII_ENV=='prod'){
    throw new \yii\web\NotFoundHttpException();
}

$this->title = "The Best Model of February";

?>

<div class="row wide-padding">

    <div class="col-md-8 col-md-offset-2">

        <h1 class="page-header text-center"><?php echo $this->title; ?></h1>

        <p>
            Every month we choose the best model on our site. The winner will be determined by the number of likes. To vote, just click on the heart symbol at the top right corner of the model's image. You can vote for an unlimited number of favorite models.
        </p>

        <p>
            The winner for February will be announced on February 29th, 2016. The author of the model with the highest number of hearts (likes) will receive a gift card and the winning model will receive the title, "Best February Model".
        </p>

    </div>

</div>