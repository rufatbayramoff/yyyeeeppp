<?php

/* @var $this yii\web\View */

use common\modules\catalogPs\helpers\CatalogPrintingUrlHelper;
use common\modules\catalogPs\models\CatalogSearchForm;

use common\models\Ps;
use frontend\components\UserSessionFacade;

$this->registerAssetBundle(\frontend\assets\IndexPageAsset::class);
$this->registerAssetBundle(\frontend\assets\SwiperAsset::class);
$this->registerAssetBundle(\frontend\assets\LightboxAsset::class);

?>

<?= $this->render('/common/ie-alert.php'); ?>

<div class="service-search-hero">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="service-search-hero__title animated" data-animation-in="fadeInDown" data-animation-out="">
                    <?= _t('front', 'Search & compare manufacturing services worldwide'); ?>
                </h1>
            </div>
        </div>
    </div>

    <div class="container">

        <form class="findps-hero">

            <div class="findps-hero__switch">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-switcher active">
                        <input type="radio" name="optionsPrice" autocomplete="off" checked="checked">
                        <?=_t('site.front', '3D Printing');?>
                    </label>
                    <label class="btn btn-switcher">
                        <input type="radio" name="optionsPrice" autocomplete="off">
                        <?=_t('site.front', 'CNC Machining');?>
                    </label>
                </div>
            </div>

            <div class="findps-hero__loc">
                <label for="" class="findps-hero__label"><?=_t('site.main', 'Location');?></label>
                <?= frontend\widgets\UserLocationWidget::widget([
                    'jsCallbackRedir' => 'function(btn, result, modalId){
                            var url = TS.Locator.getUrlPsCatalogLocationRedir(result.location); 
                            document.location = url;
                        };']); ?>
            </div>
            <div class="findps-hero__material">
                <label for="" class="findps-hero__label"><?=_t('site.main', 'Materials');?></label>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownHeroMaterial" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <?= _t('front', 'All Materials'); ?>
                        <span class="tsi tsi-down"></span>
                    </button>
                    <?php echo \frontend\widgets\TopFindServiceWidget::widget([ 'viewLayout'=>'findServiceMaterials']); ?>
                </div>
            </div>
            <div class="findps-hero__btn-find">
                <a href="<?=CatalogPrintingUrlHelper::printing3dCatalog(location:UserSessionFacade::getLocation());?>/" class="btn btn-danger">
                    <?= _t('front', 'Search'); ?>
                </a>
            </div>
            <div class="findps-hero__upload">
                <a href="/my/print-model3d?utm_source=ts_main" class="btn btn-default btn-ghost">
                    <span class="tsi tsi-upload-l"></span>
                    <?= _t('front', 'Order 3D Print'); ?>
                </a>
            </div>
        </form>
    </div>

    <div class="service-search-benefit">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 service-search-benefit__item animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                    <div class="service-search-benefit__data">
                        45K
                    </div>
                    <h4 class="service-search-benefit__title">
                        <?= _t('front', 'Manufactured'); ?>
                    </h4>
                    <p class="service-search-benefit__text">
                        <?= _t('front', 'parts'); ?>
                    </p>
                </div>
                <div class="col-sm-3 service-search-benefit__item animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                    <div class="service-search-benefit__data">
                        5
                    </div>
                    <h4 class="service-search-benefit__title">
                        <?= _t('front', 'Day'); ?>
                    </h4>
                    <p class="service-search-benefit__text">
                        <?= _t('front', 'turnarounds'); ?>
                    </p>
                </div>
                <div class="col-sm-3 service-search-benefit__item animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                    <div class="service-search-benefit__data">
                        4.8<small>/5</small>
                    </div>
                    <h4 class="service-search-benefit__title">
                        <?= _t('front', 'Ratings'); ?>
                    </h4>
                    <p class="service-search-benefit__text">
                        <?= _t('front', 'from customers'); ?>
                    </p>
                </div>
                <div class="col-sm-3 service-search-benefit__item animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                    <div class="service-search-benefit__data">
                        15x
                    </div>
                    <h4 class="service-search-benefit__title">
                        <?= _t('front', 'Growth'); ?>
                    </h4>
                    <p class="service-search-benefit__text">
                        <?= _t('front', 'in last 6 months'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="service-search-hero__fade-bg"></div>
    <div class="service-search-hero__bg"></div>
</div>

<div class="site-index">

    <div class="why-ts">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="why-ts__title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp"><?= _t('front', 'Why Treatstock?'); ?></h2>
                </div>
                <div class="col-sm-12">
                    <ul class="nav why-ts__nav">
                        <li class="active">
                            <a href="#whyCustomers" data-toggle="tab" aria-expanded="true"><?= _t('front', 'Customers'); ?></a>
                        </li>
                        <li class="">
                            <a href="#whyServices" data-toggle="tab" aria-expanded="false"><?= _t('front', 'Services'); ?></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="whyCustomers">

                            <div class="why-ts__list">
                                <div class="why-ts__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                    <img class="why-ts__icon" src="/static/images/main-page-icons/mp-low-prices.svg" alt="">
                                    <h4 class="why-ts__card-title">
                                        <?= _t('front', 'Best Prices'); ?>
                                    </h4>
                                    <p class="why-ts__card-text">
                                        <?= _t('front', 'Find and compare the most competitive prices online'); ?>
                                    </p>
                                </div>
                                <div class="why-ts__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                    <img class="why-ts__icon" src="/static/images/main-page-icons/mp-quick-delivery.svg" alt="">
                                    <h4 class="why-ts__card-title">
                                        <?= _t('front', 'Quick Delivery'); ?>
                                    </h4>
                                    <p class="why-ts__card-text">
                                        <?= _t('front', 'On average, orders are delivered within 5 days with free shipping insurance'); ?>
                                    </p>
                                </div>
                                <div class="why-ts__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                    <img class="why-ts__icon" src="/static/images/main-page-icons/mp-quality-assurance.svg" alt="">
                                    <h4 class="why-ts__card-title">
                                        <?= _t('front', 'Quality Assurance'); ?>
                                    </h4>
                                    <p class="why-ts__card-text">
                                        <?= _t('front', 'All machines are tested and orders go through a quality control check'); ?>
                                    </p>
                                </div>
                                <div class="why-ts__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                    <img class="why-ts__icon" src="/static/images/main-page-icons/mp-customer-support.svg" alt="">
                                    <h4 class="why-ts__card-title">
                                        <?= _t('front', 'Customer Support'); ?>
                                    </h4>
                                    <p class="why-ts__card-text">
                                        <?= _t('front', 'Ask questions, receive tips and get helpful information about your order'); ?>
                                    </p>
                                </div>
                            </div>

                            <a href="#" class="btn btn-primary why-ts__cta m-r20 animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft"><?= _t('front', 'Find a Service'); ?></a>
                            <a href="#how-ts-customers" class="btn btn-link why-ts__cta animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight"><?= _t('front', 'How it Works'); ?></a>
                        </div>

                        <div class="tab-pane fade" id="whyServices">
                            <div class="why-ts__list">
                                <div class="why-ts__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                    <img class="why-ts__icon" src="/static/images/main-page-icons/mp-0-commission.svg" alt="">
                                    <h4 class="why-ts__card-title">
                                        <?= _t('site.ps', 'No Commission'); ?>
                                    </h4>
                                    <p class="why-ts__card-text">
                                        <?= _t('site.ps', 'Services pay zero commission on all earnings'); ?>
                                    </p>
                                </div>
                                <div class="why-ts__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                    <img class="why-ts__icon" src="/static/images/main-page-icons/mp-constant-orders.svg" alt="">
                                    <h4 class="why-ts__card-title">
                                        <?= _t('site.ps', 'Free Marketing'); ?>
                                    </h4>
                                    <p class="why-ts__card-text">
                                        <?= _t('site.ps', 'We bring you the customers, all you need to do is complete orders and get paid'); ?>
                                    </p>
                                </div>
                                <div class="why-ts__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                    <img class="why-ts__icon" src="/static/images/main-page-icons/mp-payment-security.svg" alt="">
                                    <h4 class="why-ts__card-title">
                                        <?= _t('site.ps', 'Payment Security'); ?>
                                    </h4>
                                    <p class="why-ts__card-text">
                                        <?= _t('site.ps', 'Have the peace of mind knowing that you’ll get paid for every order'); ?>
                                    </p>
                                </div>
                                <div class="why-ts__card animated" data-animation-in="fadeInRight" data-animation-out="fadeOutLeft">
                                    <img class="why-ts__icon" src="/static/images/main-page-icons/mp-biz-tools.svg" alt="">
                                    <h4 class="why-ts__card-title">
                                        <?= _t('site.ps', 'Business Tools'); ?>
                                    </h4>
                                    <p class="why-ts__card-text">
                                        <?= _t('site.ps', 'Use our widget and Facebook app to promote your service on other platforms'); ?>
                                    </p>
                                </div>
                            </div>

                            <a href="/my/printservice" class="btn btn-danger why-ts__cta animated" data-animation-in="fadeInUp" data-animation-out="fadeOutDown"
                               onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/my/printservice', 'signUp');return false;" : "return true;" ?>">
                                <?= _t('front', 'Offer Your Services'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-page-industry">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="customer-reviews__title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'Search by industries'); ?>
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <a class="designer-card designer-card--industry" href="<?=CatalogPrintingUrlHelper::printing3dCatalog(technologyCode:'SLA', sort: 'rating', international:1)?>" target="_blank">
                        <img class="designer-card__pic" src="https://static.treatstock.com/static/images/common/main-page/industry-prosth.jpg" alt="">
                        <h2 class="designer-card__title">
                            Medicine
                        </h2>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <a class="designer-card designer-card--industry" href="<?=CatalogPrintingUrlHelper::printing3dCatalog(usageCode:'hq-prototyping', sort: 'rating', international:1)?>" target="_blank">
                        <img class="designer-card__pic" src="https://static.treatstock.com/static/images/common/main-page/industry-robo.jpg" alt="">
                        <h2 class="designer-card__title">
                            Robotics
                        </h2>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <a class="designer-card designer-card--industry" href="<?=CatalogPrintingUrlHelper::printing3dCatalog(usageCode:'metal', sort: 'rating', international:1)?>" target="_blank">
                        <img class="designer-card__pic" src="https://static.treatstock.com/static/images/common/main-page/industry-aero.jpg" alt="">
                        <h2 class="designer-card__title">
                            Aerospace
                        </h2>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <a class="designer-card designer-card--industry" href="<?=CatalogPrintingUrlHelper::printing3dCatalog(usageCode:'prototyping', sort: 'rating', international:1)?>" target="_blank">
                        <img class="designer-card__pic" src="https://static.treatstock.com/static/images/common/main-page/industry-vr.jpg" alt="">
                        <h2 class="designer-card__title">
                            Virtual Reality
                        </h2>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="customer-reviews">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="customer-reviews__title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'What our customers are saying'); ?>
                    </h2>
                </div>
            </div>
        </div>

        <div class="customer-reviews__slider swiper-container">
            <div class="swiper-wrapper customer-reviews__gallery">
                <a class="customer-reviews__slider-item swiper-slide" href="/c/maiks-printhub/reviews">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/4.jpg?08092017" alt="Maiks Printhub review"/>
                    <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                           step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                           data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                        <div class="customer-reviews__date">
                            May 27, 2017
                        </div>
                    </span>
                    <span class="customer-reviews__review">
                        "Excellent customer service"
                    </span>
                    <img class="customer-reviews__avatar" src="https://static.treatstock.com/static/user/google_103248600451756999841/avatar_64x64.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">thomas.murray09</span>
                        <span class="customer-reviews__user-loc">Germany</span>
                    </div>
                </a>
                <a class="customer-reviews__slider-item swiper-slide" href="/c/keymedia-prints/reviews">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/5.jpg?08092017" alt="Keymedia prints review" />
                    <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                           step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                           data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                        <div class="customer-reviews__date">
                            Jul 19, 2017
                        </div>
                    </span>
                    <span class="customer-reviews__review">
                        "Good communications & choice, also really good finish"
                    </span>
                    <img class="customer-reviews__avatar" src="https://www.thingiverse.com/img/default/avatar/avatar_default_thumb_medium.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">Brendan</span>
                        <span class="customer-reviews__user-loc">Great Britain</span>
                    </div>
                </a>
                <a class="customer-reviews__slider-item swiper-slide" href="/c/the-thing-loft/reviews">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/2.jpg?08092017" alt="The Thing Loft review"/>
                    <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                           step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                           data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                        <div class="customer-reviews__date">
                            Jun 16, 2017
                        </div>
                    </span>
                    <span class="customer-reviews__review">
                        "Really great outfit to work with. Will do business with them again."
                    </span>
                    <img class="customer-reviews__avatar" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAQAICRAEAOw==">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">jlewis1010</span>
                        <span class="customer-reviews__user-loc">United States</span>
                    </div>
                </a>
                <a class="customer-reviews__slider-item swiper-slide" href="/c/seaside-3d/reviews">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/1.jpg?08092017" alt="Seaside 3d review"/>
                    <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                           step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                           data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                        <div class="customer-reviews__date">
                            Jun 20, 2017
                        </div>
                    </span>
                    <span class="customer-reviews__review">
                        "I would absolutely recommend Seaside 3D to others! Amazingly fast service and delivery, and brilliant quality work."
                    </span>
                    <img class="customer-reviews__avatar" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAQAICRAEAOw==">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">dave.s.ewe</span>
                        <span class="customer-reviews__user-loc">Great Britain</span>
                    </div>
                </a>
                <a class="customer-reviews__slider-item swiper-slide" href="/c/northworks3d/reviews">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/7.jpg?08092017" alt="Northworks Digital Factory review" />
                    <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                           step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                           data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                        <div class="customer-reviews__date">
                            Sep 15, 2017
                        </div>
                    </span>
                    <span class="customer-reviews__review">
                        "Very happy with the print, thanks!"
                    </span>
                    <img class="customer-reviews__avatar" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAQAICRAEAOw==">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">ccarman999</span>
                        <span class="customer-reviews__user-loc">United States</span>
                    </div>
                </a>
                <a class="customer-reviews__slider-item swiper-slide" href="/c/archanias-workshop/reviews">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/3.jpg?08092017" alt="Archanias Workshop review"/>
                    <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 86%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                           step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                           data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                        <div class="customer-reviews__date">
                            Mar 18, 2017
                        </div>
                    </span>
                    <span class="customer-reviews__review">
                        "I Love it <3"
                    </span>
                    <img class="customer-reviews__avatar" src="https://static.treatstock.com/static/user/facebook_1078059192304866/avatar_64x64.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">charaf-nassiri</span>
                        <span class="customer-reviews__user-loc">United States</span>
                    </div>
                </a>
                <a class="customer-reviews__slider-item swiper-slide" href="/c/laughing-monkey-labs-llc/reviews">
                    <img class="customer-reviews__img" src="https://static.treatstock.com/static/images/mp-reviews/6.jpg?08092017" alt="Laughing Monkey Labs llc review"/>
                    <span class="customer-reviews__rating">
                        <div id="star-rating-block-13955" class="star-rating-block">
                            <div class="star-rating rating-xs rating-disabled">
                                <div class="rating-container tsi" data-content="">
                                    <div class="rating-stars" data-content="" style="width: 100%;"></div>
                                    <input value="5" type="number" class="star-rating form-control hide" min="0" max="5"
                                           step="0.1" data-size="xs" data-symbol="" data-glyphicon="false"
                                           data-rating-class="tsi" data-readonly="true">
                                </div>
                            </div>
                        </div>
                        <div class="customer-reviews__date">
                            Jun 9, 2017
                        </div>
                    </span>
                    <span class="customer-reviews__review">
                        "Came out great!"
                    </span>
                    <img class="customer-reviews__avatar" src="https://static.treatstock.com/static/user/google_112857140467547919111/avatar_64x64.jpg">
                    <div class="customer-reviews__user-data">
                        <span class="customer-reviews__username">justinsullivanri</span>
                        <span class="customer-reviews__user-loc">United States</span>
                    </div>
                </a>
            </div>

            <div class="customer-reviews__scrollbar swiper-scrollbar"></div>
        </div>

    </div>

    <div class="how-ts-customers" id="how-ts-customers">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="how-ts-customers__title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                        <?= _t('front', 'How does Treatstock work?'); ?>
                    </h2>
                    <p class="how-ts-customers__about"><?= _t('front', 'Treatstock is an online platform that allows clients to find, compare, and place orders with professional manufacturers for customized and on-demand products and services that are made fast and at a high quality'); ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                    <div class="row">
                        <div class="col-sm-6">
                            <figure class="how-ts-customers__img">
                                <img src="/static/images/main-page-icons/mp-laptop.jpg?08092017" alt="">
                            </figure>
                        </div>
                        <div class="col-sm-6">
                            <h4 class="how-ts-customers__subtitle">
                                <span class="how-ts-customers__count">1</span><?= _t('front', 'Upload Model'); ?>
                            </h4>
                            <ul class="how-ts-customers__list">
                                <li><?= _t('front', 'Upload your files'); ?></li>
                                <li><?= _t('front', 'Find 3D models in our catalog'); ?></li>
                                <li><?= _t('front', 'Hire a designer to help bring your ideas to life'); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 how-ts-customers__step2 animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                    <div class="row">
                        <div class="col-sm-6 col-md-5 col-md-offset-1">
                            <h4 class="how-ts-customers__subtitle">
                                <span class="how-ts-customers__count">2</span><?= _t('front', 'Select a Manufacturer'); ?>
                            </h4>
                            <ul class="how-ts-customers__list">
                                <li><?= _t('front', 'Choose from over 100 materials and colors'); ?></li>
                                <li><?= _t('front', 'Compare prices, reviews and locations'); ?></li>
                                <li><?= _t('front', 'Place an order with a manufacturer of your choice'); ?></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <figure class="how-ts-customers__img">
                                <img src="/static/images/main-page-icons/mp-3d.jpg?08092017" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 how-ts-customers__step3 animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                    <div class="row">
                        <div class="col-sm-6">
                            <figure class="how-ts-customers__img">
                                <img src="/static/images/main-page-icons/mp-box.jpg?08092017" alt="">
                            </figure>
                        </div>
                        <div class="col-sm-6">
                            <h4 class="how-ts-customers__subtitle">
                                <span class="how-ts-customers__count">3</span><?= _t('front', 'Order Delivery'); ?>
                            </h4>
                            <ul class="how-ts-customers__list">
                                <li><?= _t('front', 'Track the production process of your order'); ?></li>
                                <li><?= _t('front', 'Fast delivery within 5 days with free shipping insurance'); ?></li>
                                <li><?= _t('front', 'Our friendly customer support team is always here to help'); ?></li>
                            </ul>
                            <a href="<?=CatalogPrintingUrlHelper::printing3dCatalog()?>" class="btn btn-danger how-ts-customers__cta">
                                <?= _t('front', 'Get Started'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="feat-manufacturers">
        <div class="container js-main-page-featured-print-services">

            <h2 class="customers-page-title animated" data-animation-in="fadeInDown" data-animation-out="fadeOutUp">
                <?= _t('front', 'Featured Manufacturers'); ?>
                <a href="<?=CatalogPrintingUrlHelper::printing3dCatalog()?>">
                    <?= _t('front', 'See All Manufacturers'); ?>
                </a>
            </h2>
            <div class="feat-manufacturers__list swiper-container">
                <div class="swiper-wrapper">
                    <?php

                    if($this->beginCache('psmain'.md5(implode(',',app('setting')->get('mainpage.ps', []))), ['duration' => 36000, 'variations'=>[\Yii::$app->language]])){
                        echo \frontend\widgets\PsCardWidget::widget([
                            'perPage' => 20,
                            'containerClass' => 'feat-manufacturers__item swiper-slide',
                            'psLinks'=> \Yii::$app->setting->get('mainpage.ps', [
                                'laughing-monkey-labs-llc',
                                'modern-blacksmith-3d-design-printing-and-scanning',
                                'technologics-group',
                                'grapha-print',
                                'seaside-3d', 'pfactory',  'koekjesstekernl'
                            ])
                        ]);
                        $this->endCache();
                    }
                    ?>
                </div>
                <div class="feat-manufacturers__scrollbar swiper-scrollbar"></div>
            </div>
        </div>
    </div>

    <!-- PS promo block -->
    <div class="ps-promo ps-promo--start-ps">
        <div class="container">
            <div class="ps-promo__block">
                <div class="ps-promo__inner ps-promo__inner--start-ps">
                    <div class="ps-promo__img animated" data-animation-in="fadeInLeft" data-animation-out="fadeOutLeft">
                        <div class="ps-promo-pic">
                            <div class="ps-promo-pic__container">
                                <div class="ps-promo-pic__img"></div>
                            </div>
                        </div>
                    </div>
                    <div class="ps-promo__text animated" data-animation-in="fadeInRight" data-animation-out="fadeOutRight">
                        <h2>
                            <a class="t-ps-promo--start-ps-heading" href="/my/printservice" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/my/printservice', 'signUp');return false;" : "return true;" ?>">
                                <?= _t('front', 'Become a Manufacturer'); ?>
                            </a>
                        </h2>
                        <p>
                            <?= _t('front', 'Do you own a 3D printer? Join our manufacturing platform and deliver finished products for customers worldwide.'); ?>
                        </p>
                        <p>
                            <a href="/my/printservice" class="btn btn-primary ps-promo__btn t-ps-promo--start-ps-btn" onclick="<?php echo is_guest() ?"TS.Visitor.loginForm('/my/printservice', 'signUp');return false;" : "return true;" ?>">
                                <?= _t('front', 'Get Started'); ?>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // PS promo block -->


</div>



<script>
    <?php $this->beginBlock('js1', false); ?>


    $('.why-ts__cta.btn-link').click(function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $('.how-ts-customers').offset().top - 65
        }, 350);
    });

    //Init slider for PS portfolio
    var swiperFeaturedPS = new Swiper('.designer-card__ps-portfolio', {
        scrollbar: '.designer-card__ps-portfolio-scrollbar',
        scrollbarHide: true,
        slidesPerView: 'auto',
        grabCursor: true
    });

    function initPSSliders() {
        //Init slider for feat-manufacturers
        var swiperPs = new Swiper('.feat-manufacturers__list', {
            slidesPerView: 'auto',
            scrollbar: '.feat-manufacturers__scrollbar',
            scrollbarHide: true,
            spaceBetween: 0,
            grabCursor: true,

            breakpoints: {
                500: {
                    slidesPerView: 1.15,
                    centeredSlides: false
                },
                767: {
                    slidesPerView: 2.25
                }
            }
        });
    }

    initPSSliders();
    $(window).resize(function () {initPSSliders()});

    function initPrints() {
        //Init slider for .customer-reviews__slider
        var swiperPrints = new Swiper('.customer-reviews__slider', {
            scrollbar: '.customer-reviews__scrollbar',
            scrollbarHide: true,
            slidesPerView: 'auto',
            grabCursor: true
        });
    }

    initPrints();
    $(window).resize(function () {initPrints()});

    // Turn on sliders on mobile devices
    function initMobileSliders() {
        if ( $('html').hasClass('mobile') ) {

            //Init slider for .main-page-blog
            var swiperBlog = new Swiper('.post-list', {
                grabCursor: true,
                slidesPerView: 1.15,
                spaceBetween: 0,
                centeredSlides: false
            });

        }
    }

    initMobileSliders();
    $(window).resize(function () {initMobileSliders()});


    <?php $this->endBlock(); ?>
</script>
<?php $this->registerJs($this->blocks['js1']); ?>

